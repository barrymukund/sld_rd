<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#199" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="3017" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="-25.000000953674" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="-24.995283203125" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.7664453125" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.886805969238" Y="-29.719857421875" />
                  <Point X="0.563302062988" Y="-28.5125234375" />
                  <Point X="0.557721618652" Y="-28.497142578125" />
                  <Point X="0.54236315918" Y="-28.467375" />
                  <Point X="0.41013885498" Y="-28.276865234375" />
                  <Point X="0.378635528564" Y="-28.231474609375" />
                  <Point X="0.356749481201" Y="-28.209017578125" />
                  <Point X="0.330494750977" Y="-28.189775390625" />
                  <Point X="0.302495483398" Y="-28.175669921875" />
                  <Point X="0.097885955811" Y="-28.11216796875" />
                  <Point X="0.049136341095" Y="-28.097037109375" />
                  <Point X="0.020983621597" Y="-28.092767578125" />
                  <Point X="-0.008657402039" Y="-28.092765625" />
                  <Point X="-0.036823478699" Y="-28.09703515625" />
                  <Point X="-0.241432693481" Y="-28.1605390625" />
                  <Point X="-0.290182617188" Y="-28.17566796875" />
                  <Point X="-0.318183258057" Y="-28.189775390625" />
                  <Point X="-0.344439025879" Y="-28.20901953125" />
                  <Point X="-0.366323120117" Y="-28.2314765625" />
                  <Point X="-0.498547576904" Y="-28.42198828125" />
                  <Point X="-0.53005090332" Y="-28.467376953125" />
                  <Point X="-0.538187805176" Y="-28.4815703125" />
                  <Point X="-0.550990112305" Y="-28.5125234375" />
                  <Point X="-0.628067321777" Y="-28.8001796875" />
                  <Point X="-0.916584777832" Y="-29.87694140625" />
                  <Point X="-1.025943725586" Y="-29.85571484375" />
                  <Point X="-1.079322143555" Y="-29.845353515625" />
                  <Point X="-1.246417724609" Y="-29.802361328125" />
                  <Point X="-1.214963012695" Y="-29.563439453125" />
                  <Point X="-1.214201171875" Y="-29.5479296875" />
                  <Point X="-1.216508666992" Y="-29.5162265625" />
                  <Point X="-1.265389892578" Y="-29.270484375" />
                  <Point X="-1.277036132812" Y="-29.21193359375" />
                  <Point X="-1.287937744141" Y="-29.182966796875" />
                  <Point X="-1.304009643555" Y="-29.15512890625" />
                  <Point X="-1.32364453125" Y="-29.131203125" />
                  <Point X="-1.512022949219" Y="-28.966" />
                  <Point X="-1.556905517578" Y="-28.926638671875" />
                  <Point X="-1.583189086914" Y="-28.910294921875" />
                  <Point X="-1.612886474609" Y="-28.897994140625" />
                  <Point X="-1.643027587891" Y="-28.890966796875" />
                  <Point X="-1.893047607422" Y="-28.874580078125" />
                  <Point X="-1.952616699219" Y="-28.87067578125" />
                  <Point X="-1.983414550781" Y="-28.873708984375" />
                  <Point X="-2.014463256836" Y="-28.88202734375" />
                  <Point X="-2.042657592773" Y="-28.89480078125" />
                  <Point X="-2.250987792969" Y="-29.034001953125" />
                  <Point X="-2.300624023438" Y="-29.06716796875" />
                  <Point X="-2.312790283203" Y="-29.07682421875" />
                  <Point X="-2.335103027344" Y="-29.099462890625" />
                  <Point X="-2.378375732422" Y="-29.155857421875" />
                  <Point X="-2.480147705078" Y="-29.28848828125" />
                  <Point X="-2.723448730469" Y="-29.13784375" />
                  <Point X="-2.801726074219" Y="-29.089376953125" />
                  <Point X="-3.104721679688" Y="-28.856078125" />
                  <Point X="-3.02495703125" Y="-28.717921875" />
                  <Point X="-2.423761230469" Y="-27.676619140625" />
                  <Point X="-2.412858886719" Y="-27.64765234375" />
                  <Point X="-2.406588134766" Y="-27.616125" />
                  <Point X="-2.405575927734" Y="-27.585189453125" />
                  <Point X="-2.414560791016" Y="-27.555572265625" />
                  <Point X="-2.428778808594" Y="-27.5267421875" />
                  <Point X="-2.446807617188" Y="-27.501583984375" />
                  <Point X="-2.46415625" Y="-27.484236328125" />
                  <Point X="-2.489316650391" Y="-27.466208984375" />
                  <Point X="-2.518145263672" Y="-27.451994140625" />
                  <Point X="-2.54776171875" Y="-27.44301171875" />
                  <Point X="-2.578693847656" Y="-27.444025390625" />
                  <Point X="-2.610219238281" Y="-27.450296875" />
                  <Point X="-2.639183837891" Y="-27.46119921875" />
                  <Point X="-2.887281494141" Y="-27.604439453125" />
                  <Point X="-3.818023193359" Y="-28.141802734375" />
                  <Point X="-4.021029785156" Y="-27.87509375" />
                  <Point X="-4.082860839844" Y="-27.793861328125" />
                  <Point X="-4.306142578125" Y="-27.419451171875" />
                  <Point X="-4.157852050781" Y="-27.3056640625" />
                  <Point X="-3.105954589844" Y="-26.498515625" />
                  <Point X="-3.084576416016" Y="-26.47559375" />
                  <Point X="-3.066611083984" Y="-26.4484609375" />
                  <Point X="-3.053856201172" Y="-26.419833984375" />
                  <Point X="-3.047634033203" Y="-26.395810546875" />
                  <Point X="-3.046151611328" Y="-26.3900859375" />
                  <Point X="-3.043347412109" Y="-26.35965234375" />
                  <Point X="-3.045557128906" Y="-26.327982421875" />
                  <Point X="-3.05255859375" Y="-26.29823828125" />
                  <Point X="-3.068640625" Y="-26.2722578125" />
                  <Point X="-3.089472167969" Y="-26.248302734375" />
                  <Point X="-3.112970458984" Y="-26.22876953125" />
                  <Point X="-3.134357421875" Y="-26.216181640625" />
                  <Point X="-3.139479248047" Y="-26.21316796875" />
                  <Point X="-3.168729980469" Y="-26.201953125" />
                  <Point X="-3.200611083984" Y="-26.195474609375" />
                  <Point X="-3.231928955078" Y="-26.194384765625" />
                  <Point X="-3.545128662109" Y="-26.2356171875" />
                  <Point X="-4.732102050781" Y="-26.391884765625" />
                  <Point X="-4.80989453125" Y="-26.087330078125" />
                  <Point X="-4.834077636719" Y="-25.99265625" />
                  <Point X="-4.892424316406" Y="-25.584701171875" />
                  <Point X="-4.731145996094" Y="-25.541486328125" />
                  <Point X="-3.532875976562" Y="-25.22041015625" />
                  <Point X="-3.517493896484" Y="-25.214828125" />
                  <Point X="-3.487725585938" Y="-25.19946875" />
                  <Point X="-3.465312744141" Y="-25.183912109375" />
                  <Point X="-3.459979980469" Y="-25.1802109375" />
                  <Point X="-3.437531982422" Y="-25.1583359375" />
                  <Point X="-3.418282470703" Y="-25.132078125" />
                  <Point X="-3.404168945312" Y="-25.104068359375" />
                  <Point X="-3.396697998047" Y="-25.07999609375" />
                  <Point X="-3.39491796875" Y="-25.07426171875" />
                  <Point X="-3.390648681641" Y="-25.046107421875" />
                  <Point X="-3.390647216797" Y="-25.016466796875" />
                  <Point X="-3.394916503906" Y="-24.9883046875" />
                  <Point X="-3.402387451172" Y="-24.964232421875" />
                  <Point X="-3.404167480469" Y="-24.95849609375" />
                  <Point X="-3.418276367188" Y="-24.9304921875" />
                  <Point X="-3.437522460938" Y="-24.904234375" />
                  <Point X="-3.459981689453" Y="-24.882349609375" />
                  <Point X="-3.482390136719" Y="-24.866798828125" />
                  <Point X="-3.492541015625" Y="-24.86065625" />
                  <Point X="-3.513452148438" Y="-24.849724609375" />
                  <Point X="-3.532876220703" Y="-24.842150390625" />
                  <Point X="-3.818372802734" Y="-24.76565234375" />
                  <Point X="-4.891815917969" Y="-24.4780234375" />
                  <Point X="-4.840072265625" Y="-24.12834375" />
                  <Point X="-4.824487792969" Y="-24.0230234375" />
                  <Point X="-4.703551757812" Y="-23.576732421875" />
                  <Point X="-4.624615722656" Y="-23.587125" />
                  <Point X="-3.765666748047" Y="-23.70020703125" />
                  <Point X="-3.744985107422" Y="-23.700658203125" />
                  <Point X="-3.723423339844" Y="-23.698771484375" />
                  <Point X="-3.703138183594" Y="-23.694736328125" />
                  <Point X="-3.65353125" Y="-23.679095703125" />
                  <Point X="-3.641712158203" Y="-23.675369140625" />
                  <Point X="-3.622783935547" Y="-23.667041015625" />
                  <Point X="-3.604039794922" Y="-23.656220703125" />
                  <Point X="-3.587354736328" Y="-23.64398828125" />
                  <Point X="-3.573713623047" Y="-23.62843359375" />
                  <Point X="-3.561299316406" Y="-23.610703125" />
                  <Point X="-3.5513515625" Y="-23.5925703125" />
                  <Point X="-3.531446533203" Y="-23.544515625" />
                  <Point X="-3.526704101563" Y="-23.53306640625" />
                  <Point X="-3.520915771484" Y="-23.51320703125" />
                  <Point X="-3.517157226562" Y="-23.491890625" />
                  <Point X="-3.5158046875" Y="-23.47125" />
                  <Point X="-3.518951171875" Y="-23.450806640625" />
                  <Point X="-3.524552978516" Y="-23.429900390625" />
                  <Point X="-3.532050048828" Y="-23.41062109375" />
                  <Point X="-3.556067626953" Y="-23.364484375" />
                  <Point X="-3.561789794922" Y="-23.3534921875" />
                  <Point X="-3.573281982422" Y="-23.33629296875" />
                  <Point X="-3.587194580078" Y="-23.319712890625" />
                  <Point X="-3.602135498047" Y="-23.30541015625" />
                  <Point X="-3.765896972656" Y="-23.17975" />
                  <Point X="-4.351859863281" Y="-22.730126953125" />
                  <Point X="-4.141707519531" Y="-22.370083984375" />
                  <Point X="-4.081156005859" Y="-22.266345703125" />
                  <Point X="-3.752630371094" Y="-21.8440703125" />
                  <Point X="-3.734200195312" Y="-21.850751953125" />
                  <Point X="-3.206657470703" Y="-22.155328125" />
                  <Point X="-3.1877265625" Y="-22.163658203125" />
                  <Point X="-3.167082275391" Y="-22.17016796875" />
                  <Point X="-3.146793212891" Y="-22.174205078125" />
                  <Point X="-3.077704589844" Y="-22.18025" />
                  <Point X="-3.061243896484" Y="-22.181689453125" />
                  <Point X="-3.040557861328" Y="-22.18123828125" />
                  <Point X="-3.019100585938" Y="-22.178412109375" />
                  <Point X="-2.999010742188" Y="-22.173494140625" />
                  <Point X="-2.9804609375" Y="-22.164345703125" />
                  <Point X="-2.962208007812" Y="-22.152716796875" />
                  <Point X="-2.946078369141" Y="-22.139771484375" />
                  <Point X="-2.897038818359" Y="-22.090732421875" />
                  <Point X="-2.885354736328" Y="-22.079048828125" />
                  <Point X="-2.872409179688" Y="-22.062919921875" />
                  <Point X="-2.860779296875" Y="-22.044666015625" />
                  <Point X="-2.851629638672" Y="-22.02611328125" />
                  <Point X="-2.846712646484" Y="-22.00601953125" />
                  <Point X="-2.843887207031" Y="-21.984560546875" />
                  <Point X="-2.843435791016" Y="-21.963880859375" />
                  <Point X="-2.849480224609" Y="-21.894791015625" />
                  <Point X="-2.850920410156" Y="-21.878330078125" />
                  <Point X="-2.854955566406" Y="-21.85804296875" />
                  <Point X="-2.861464111328" Y="-21.837400390625" />
                  <Point X="-2.869795166016" Y="-21.818466796875" />
                  <Point X="-2.942362792969" Y="-21.69277734375" />
                  <Point X="-3.183332763672" Y="-21.27540625" />
                  <Point X="-2.806085693359" Y="-20.986173828125" />
                  <Point X="-2.700619873047" Y="-20.905314453125" />
                  <Point X="-2.183213378906" Y="-20.617853515625" />
                  <Point X="-2.167035888672" Y="-20.608865234375" />
                  <Point X="-2.0431953125" Y="-20.7702578125" />
                  <Point X="-2.028895507812" Y="-20.785197265625" />
                  <Point X="-2.012314941406" Y="-20.799111328125" />
                  <Point X="-1.995114379883" Y="-20.81060546875" />
                  <Point X="-1.918219116211" Y="-20.85063671875" />
                  <Point X="-1.89989831543" Y="-20.860173828125" />
                  <Point X="-1.880616821289" Y="-20.867671875" />
                  <Point X="-1.859710205078" Y="-20.8732734375" />
                  <Point X="-1.839264038086" Y="-20.876419921875" />
                  <Point X="-1.818621582031" Y="-20.87506640625" />
                  <Point X="-1.797306396484" Y="-20.871306640625" />
                  <Point X="-1.777452880859" Y="-20.865517578125" />
                  <Point X="-1.697361328125" Y="-20.832341796875" />
                  <Point X="-1.678279052734" Y="-20.8244375" />
                  <Point X="-1.660145996094" Y="-20.814490234375" />
                  <Point X="-1.642416381836" Y="-20.802076171875" />
                  <Point X="-1.626864135742" Y="-20.7884375" />
                  <Point X="-1.61463269043" Y="-20.771755859375" />
                  <Point X="-1.603810791016" Y="-20.75301171875" />
                  <Point X="-1.595480102539" Y="-20.734078125" />
                  <Point X="-1.569411987305" Y="-20.651400390625" />
                  <Point X="-1.563200805664" Y="-20.631701171875" />
                  <Point X="-1.559165527344" Y="-20.6114140625" />
                  <Point X="-1.557279174805" Y="-20.589853515625" />
                  <Point X="-1.55773034668" Y="-20.569173828125" />
                  <Point X="-1.56598034668" Y="-20.5065078125" />
                  <Point X="-1.584201904297" Y="-20.3681015625" />
                  <Point X="-1.086161254883" Y="-20.22846875" />
                  <Point X="-0.94962487793" Y="-20.19019140625" />
                  <Point X="-0.322378112793" Y="-20.116779296875" />
                  <Point X="-0.294711700439" Y="-20.113541015625" />
                  <Point X="-0.289965118408" Y="-20.131255859375" />
                  <Point X="-0.133903457642" Y="-20.713685546875" />
                  <Point X="-0.121129844666" Y="-20.741876953125" />
                  <Point X="-0.10327155304" Y="-20.768603515625" />
                  <Point X="-0.082113967896" Y="-20.791193359375" />
                  <Point X="-0.054818080902" Y="-20.805783203125" />
                  <Point X="-0.024380004883" Y="-20.816115234375" />
                  <Point X="0.006155897141" Y="-20.82115625" />
                  <Point X="0.036691802979" Y="-20.816115234375" />
                  <Point X="0.067130027771" Y="-20.805783203125" />
                  <Point X="0.094425956726" Y="-20.791193359375" />
                  <Point X="0.115583503723" Y="-20.768603515625" />
                  <Point X="0.133441802979" Y="-20.741876953125" />
                  <Point X="0.146215118408" Y="-20.713685546875" />
                  <Point X="0.183397994995" Y="-20.57491796875" />
                  <Point X="0.307419647217" Y="-20.112060546875" />
                  <Point X="0.72482635498" Y="-20.155775390625" />
                  <Point X="0.844030517578" Y="-20.168259765625" />
                  <Point X="1.362990722656" Y="-20.293552734375" />
                  <Point X="1.481039550781" Y="-20.3220546875" />
                  <Point X="1.818529418945" Y="-20.44446484375" />
                  <Point X="1.894646850586" Y="-20.47207421875" />
                  <Point X="2.221269775391" Y="-20.624822265625" />
                  <Point X="2.294555908203" Y="-20.65909765625" />
                  <Point X="2.610131347656" Y="-20.842951171875" />
                  <Point X="2.680976074219" Y="-20.884224609375" />
                  <Point X="2.943259277344" Y="-21.07074609375" />
                  <Point X="2.843420166016" Y="-21.243673828125" />
                  <Point X="2.147581054688" Y="-22.44890234375" />
                  <Point X="2.142075683594" Y="-22.4600703125" />
                  <Point X="2.133076660156" Y="-22.483943359375" />
                  <Point X="2.115738037109" Y="-22.54878125" />
                  <Point X="2.113694580078" Y="-22.558388671875" />
                  <Point X="2.108226074219" Y="-22.592740234375" />
                  <Point X="2.107728027344" Y="-22.619048828125" />
                  <Point X="2.114488769531" Y="-22.675115234375" />
                  <Point X="2.116099365234" Y="-22.68847265625" />
                  <Point X="2.121442138672" Y="-22.710396484375" />
                  <Point X="2.129708007812" Y="-22.732484375" />
                  <Point X="2.140070800781" Y="-22.752529296875" />
                  <Point X="2.174762939453" Y="-22.80365625" />
                  <Point X="2.180857666016" Y="-22.811685546875" />
                  <Point X="2.202423583984" Y="-22.83716796875" />
                  <Point X="2.221597412109" Y="-22.85440625" />
                  <Point X="2.272724609375" Y="-22.889099609375" />
                  <Point X="2.284906005859" Y="-22.897365234375" />
                  <Point X="2.304951171875" Y="-22.907728515625" />
                  <Point X="2.327037597656" Y="-22.915994140625" />
                  <Point X="2.348963378906" Y="-22.921337890625" />
                  <Point X="2.405030029297" Y="-22.92809765625" />
                  <Point X="2.415596435547" Y="-22.92877734375" />
                  <Point X="2.447858642578" Y="-22.92905078125" />
                  <Point X="2.473207519531" Y="-22.925830078125" />
                  <Point X="2.538045654297" Y="-22.908490234375" />
                  <Point X="2.543412353516" Y="-22.9068828125" />
                  <Point X="2.570944335938" Y="-22.89775" />
                  <Point X="2.588533935547" Y="-22.889853515625" />
                  <Point X="2.875688476562" Y="-22.724064453125" />
                  <Point X="3.967325439453" Y="-22.09380859375" />
                  <Point X="4.081247314453" Y="-22.252134765625" />
                  <Point X="4.123274902344" Y="-22.31054296875" />
                  <Point X="4.26219921875" Y="-22.540115234375" />
                  <Point X="4.146920898438" Y="-22.628572265625" />
                  <Point X="3.230783935547" Y="-23.331548828125" />
                  <Point X="3.221427734375" Y="-23.339755859375" />
                  <Point X="3.203974365234" Y="-23.35837109375" />
                  <Point X="3.157310302734" Y="-23.419248046875" />
                  <Point X="3.152064453125" Y="-23.426826171875" />
                  <Point X="3.132476318359" Y="-23.458283203125" />
                  <Point X="3.121629882812" Y="-23.4829140625" />
                  <Point X="3.104247314453" Y="-23.5450703125" />
                  <Point X="3.100105957031" Y="-23.55987890625" />
                  <Point X="3.096652587891" Y="-23.582177734375" />
                  <Point X="3.095836425781" Y="-23.605748046875" />
                  <Point X="3.097739501953" Y="-23.628232421875" />
                  <Point X="3.112008789062" Y="-23.697388671875" />
                  <Point X="3.114387207031" Y="-23.706572265625" />
                  <Point X="3.124985351562" Y="-23.740427734375" />
                  <Point X="3.136282958984" Y="-23.76426171875" />
                  <Point X="3.175093505859" Y="-23.823251953125" />
                  <Point X="3.184340576172" Y="-23.837306640625" />
                  <Point X="3.198893554688" Y="-23.85455078125" />
                  <Point X="3.216137207031" Y="-23.870640625" />
                  <Point X="3.234346435547" Y="-23.883966796875" />
                  <Point X="3.290588378906" Y="-23.915626953125" />
                  <Point X="3.299550537109" Y="-23.92006640625" />
                  <Point X="3.33092578125" Y="-23.93360546875" />
                  <Point X="3.356119628906" Y="-23.9405625" />
                  <Point X="3.432162597656" Y="-23.950611328125" />
                  <Point X="3.437325927734" Y="-23.951150390625" />
                  <Point X="3.468521240234" Y="-23.953548828125" />
                  <Point X="3.488203857422" Y="-23.953015625" />
                  <Point X="3.760981445312" Y="-23.917103515625" />
                  <Point X="4.77683984375" Y="-23.78336328125" />
                  <Point X="4.82788671875" Y="-23.99305078125" />
                  <Point X="4.845936035156" Y="-24.0671875" />
                  <Point X="4.890864746094" Y="-24.355759765625" />
                  <Point X="4.766492675781" Y="-24.3890859375" />
                  <Point X="3.716579833984" Y="-24.67041015625" />
                  <Point X="3.704792724609" Y="-24.6744140625" />
                  <Point X="3.681549072266" Y="-24.684931640625" />
                  <Point X="3.606839355469" Y="-24.728115234375" />
                  <Point X="3.5890390625" Y="-24.73840234375" />
                  <Point X="3.574314697266" Y="-24.748900390625" />
                  <Point X="3.547530029297" Y="-24.774423828125" />
                  <Point X="3.502704101562" Y="-24.83154296875" />
                  <Point X="3.492024169922" Y="-24.84515234375" />
                  <Point X="3.480299804688" Y="-24.86443359375" />
                  <Point X="3.470526611328" Y="-24.885896484375" />
                  <Point X="3.463680664062" Y="-24.907396484375" />
                  <Point X="3.448738525391" Y="-24.98541796875" />
                  <Point X="3.447470458984" Y="-24.99428515625" />
                  <Point X="3.443910644531" Y="-25.031685546875" />
                  <Point X="3.445178710938" Y="-25.058556640625" />
                  <Point X="3.460120849609" Y="-25.136578125" />
                  <Point X="3.463680908203" Y="-25.155166015625" />
                  <Point X="3.470527832031" Y="-25.17666796875" />
                  <Point X="3.48030078125" Y="-25.19812890625" />
                  <Point X="3.492024169922" Y="-25.217408203125" />
                  <Point X="3.536850097656" Y="-25.27452734375" />
                  <Point X="3.543055419922" Y="-25.281669921875" />
                  <Point X="3.568047607422" Y="-25.307701171875" />
                  <Point X="3.589035400391" Y="-25.32415625" />
                  <Point X="3.663745361328" Y="-25.36733984375" />
                  <Point X="3.681545410156" Y="-25.37762890625" />
                  <Point X="3.692709228516" Y="-25.383140625" />
                  <Point X="3.716579833984" Y="-25.39215234375" />
                  <Point X="3.966729248047" Y="-25.4591796875" />
                  <Point X="4.891472167969" Y="-25.706962890625" />
                  <Point X="4.865100585938" Y="-25.881880859375" />
                  <Point X="4.855021484375" Y="-25.948734375" />
                  <Point X="4.801174804688" Y="-26.18469921875" />
                  <Point X="4.641985351562" Y="-26.1637421875" />
                  <Point X="3.424381591797" Y="-26.00344140625" />
                  <Point X="3.40803515625" Y="-26.0027109375" />
                  <Point X="3.37466015625" Y="-26.005509765625" />
                  <Point X="3.22803125" Y="-26.037380859375" />
                  <Point X="3.193095947266" Y="-26.04497265625" />
                  <Point X="3.163978515625" Y="-26.056595703125" />
                  <Point X="3.136149658203" Y="-26.07348828125" />
                  <Point X="3.112397216797" Y="-26.0939609375" />
                  <Point X="3.023769287109" Y="-26.200552734375" />
                  <Point X="3.002653076172" Y="-26.22594921875" />
                  <Point X="2.987932617188" Y="-26.250330078125" />
                  <Point X="2.976589355469" Y="-26.27771484375" />
                  <Point X="2.969757324219" Y="-26.305365234375" />
                  <Point X="2.9570546875" Y="-26.44340625" />
                  <Point X="2.954028320312" Y="-26.476296875" />
                  <Point X="2.956347412109" Y="-26.50756640625" />
                  <Point X="2.964079345703" Y="-26.5391875" />
                  <Point X="2.976450683594" Y="-26.567998046875" />
                  <Point X="3.057597167969" Y="-26.69421484375" />
                  <Point X="3.076930908203" Y="-26.724287109375" />
                  <Point X="3.086930908203" Y="-26.737236328125" />
                  <Point X="3.110628417969" Y="-26.76091015625" />
                  <Point X="3.342768554688" Y="-26.939037109375" />
                  <Point X="4.213122558594" Y="-27.6068828125" />
                  <Point X="4.153218261719" Y="-27.70381640625" />
                  <Point X="4.124814453125" Y="-27.749779296875" />
                  <Point X="4.02898046875" Y="-27.8859453125" />
                  <Point X="3.885395019531" Y="-27.803046875" />
                  <Point X="2.800954589844" Y="-27.1769453125" />
                  <Point X="2.786136230469" Y="-27.170015625" />
                  <Point X="2.754223632813" Y="-27.15982421875" />
                  <Point X="2.579711914062" Y="-27.128306640625" />
                  <Point X="2.538133300781" Y="-27.120798828125" />
                  <Point X="2.506776611328" Y="-27.120396484375" />
                  <Point X="2.474604736328" Y="-27.12535546875" />
                  <Point X="2.444833251953" Y="-27.135177734375" />
                  <Point X="2.299856933594" Y="-27.2114765625" />
                  <Point X="2.265315185547" Y="-27.22965625" />
                  <Point X="2.242384033203" Y="-27.246548828125" />
                  <Point X="2.221425048828" Y="-27.2675078125" />
                  <Point X="2.204531982422" Y="-27.290439453125" />
                  <Point X="2.128231933594" Y="-27.435416015625" />
                  <Point X="2.110052978516" Y="-27.46995703125" />
                  <Point X="2.100229492188" Y="-27.499732421875" />
                  <Point X="2.095271484375" Y="-27.53190625" />
                  <Point X="2.095675537109" Y="-27.5632578125" />
                  <Point X="2.127192138672" Y="-27.73776953125" />
                  <Point X="2.134701171875" Y="-27.779349609375" />
                  <Point X="2.138985839844" Y="-27.79514453125" />
                  <Point X="2.151819091797" Y="-27.826080078125" />
                  <Point X="2.300992431641" Y="-28.084455078125" />
                  <Point X="2.861283691406" Y="-29.05490625" />
                  <Point X="2.8155625" Y="-29.087564453125" />
                  <Point X="2.781838867188" Y="-29.111650390625" />
                  <Point X="2.701764892578" Y="-29.163482421875" />
                  <Point X="2.586074707031" Y="-29.0127109375" />
                  <Point X="1.758546142578" Y="-27.934255859375" />
                  <Point X="1.747505371094" Y="-27.922181640625" />
                  <Point X="1.721923217773" Y="-27.900556640625" />
                  <Point X="1.549807617188" Y="-27.78990234375" />
                  <Point X="1.508799926758" Y="-27.7635390625" />
                  <Point X="1.479986206055" Y="-27.75116796875" />
                  <Point X="1.448365966797" Y="-27.7434375" />
                  <Point X="1.417100463867" Y="-27.741119140625" />
                  <Point X="1.228862670898" Y="-27.758439453125" />
                  <Point X="1.184013671875" Y="-27.76256640625" />
                  <Point X="1.156362182617" Y="-27.7693984375" />
                  <Point X="1.128976806641" Y="-27.7807421875" />
                  <Point X="1.104595214844" Y="-27.795462890625" />
                  <Point X="0.959242797852" Y="-27.9163203125" />
                  <Point X="0.924611328125" Y="-27.945115234375" />
                  <Point X="0.904141235352" Y="-27.96886328125" />
                  <Point X="0.887249084473" Y="-27.996689453125" />
                  <Point X="0.875624328613" Y="-28.025810546875" />
                  <Point X="0.832164611816" Y="-28.2257578125" />
                  <Point X="0.821810058594" Y="-28.273396484375" />
                  <Point X="0.819724487305" Y="-28.289626953125" />
                  <Point X="0.819742248535" Y="-28.323119140625" />
                  <Point X="0.862016784668" Y="-28.6442265625" />
                  <Point X="1.022065551758" Y="-29.859916015625" />
                  <Point X="1.007570129395" Y="-29.86309375" />
                  <Point X="0.975708129883" Y="-29.870078125" />
                  <Point X="0.929315734863" Y="-29.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.007842041016" Y="-29.762455078125" />
                  <Point X="-1.058415283203" Y="-29.752638671875" />
                  <Point X="-1.14124609375" Y="-29.731326171875" />
                  <Point X="-1.120775634766" Y="-29.57583984375" />
                  <Point X="-1.120077514648" Y="-29.568099609375" />
                  <Point X="-1.119451782227" Y="-29.541033203125" />
                  <Point X="-1.121759277344" Y="-29.509330078125" />
                  <Point X="-1.123334106445" Y="-29.497693359375" />
                  <Point X="-1.172215209961" Y="-29.251951171875" />
                  <Point X="-1.183861450195" Y="-29.193400390625" />
                  <Point X="-1.188124511719" Y="-29.17847265625" />
                  <Point X="-1.199026000977" Y="-29.149505859375" />
                  <Point X="-1.205664916992" Y="-29.135466796875" />
                  <Point X="-1.221736816406" Y="-29.10762890625" />
                  <Point X="-1.230572998047" Y="-29.09486328125" />
                  <Point X="-1.250207763672" Y="-29.0709375" />
                  <Point X="-1.261006713867" Y="-29.05977734375" />
                  <Point X="-1.449385131836" Y="-28.89457421875" />
                  <Point X="-1.494267700195" Y="-28.855212890625" />
                  <Point X="-1.506739990234" Y="-28.84596484375" />
                  <Point X="-1.53302355957" Y="-28.82962109375" />
                  <Point X="-1.546834838867" Y="-28.822525390625" />
                  <Point X="-1.576532226563" Y="-28.810224609375" />
                  <Point X="-1.591315917969" Y="-28.805474609375" />
                  <Point X="-1.62145690918" Y="-28.798447265625" />
                  <Point X="-1.636814453125" Y="-28.796169921875" />
                  <Point X="-1.886834472656" Y="-28.779783203125" />
                  <Point X="-1.946403564453" Y="-28.77587890625" />
                  <Point X="-1.961927978516" Y="-28.7761328125" />
                  <Point X="-1.992725952148" Y="-28.779166015625" />
                  <Point X="-2.007999267578" Y="-28.7819453125" />
                  <Point X="-2.039047973633" Y="-28.790263671875" />
                  <Point X="-2.053667236328" Y="-28.795494140625" />
                  <Point X="-2.081861572266" Y="-28.808267578125" />
                  <Point X="-2.095436523437" Y="-28.815810546875" />
                  <Point X="-2.303766845703" Y="-28.95501171875" />
                  <Point X="-2.353403076172" Y="-28.988177734375" />
                  <Point X="-2.359683349609" Y="-28.9927578125" />
                  <Point X="-2.380450683594" Y="-29.010138671875" />
                  <Point X="-2.402763427734" Y="-29.03277734375" />
                  <Point X="-2.410471923828" Y="-29.041630859375" />
                  <Point X="-2.453744628906" Y="-29.098025390625" />
                  <Point X="-2.503200927734" Y="-29.162478515625" />
                  <Point X="-2.673437988281" Y="-29.057072265625" />
                  <Point X="-2.747610351562" Y="-29.011146484375" />
                  <Point X="-2.980863037109" Y="-28.831548828125" />
                  <Point X="-2.942684570312" Y="-28.765421875" />
                  <Point X="-2.341488769531" Y="-27.724119140625" />
                  <Point X="-2.334850097656" Y="-27.71008203125" />
                  <Point X="-2.323947753906" Y="-27.681115234375" />
                  <Point X="-2.319684082031" Y="-27.666185546875" />
                  <Point X="-2.313413330078" Y="-27.634658203125" />
                  <Point X="-2.311638916016" Y="-27.619232421875" />
                  <Point X="-2.310626708984" Y="-27.588296875" />
                  <Point X="-2.314666992188" Y="-27.557611328125" />
                  <Point X="-2.323651855469" Y="-27.527994140625" />
                  <Point X="-2.329358642578" Y="-27.513552734375" />
                  <Point X="-2.343576660156" Y="-27.48472265625" />
                  <Point X="-2.351559326172" Y="-27.47140625" />
                  <Point X="-2.369588134766" Y="-27.446248046875" />
                  <Point X="-2.379634277344" Y="-27.43440625" />
                  <Point X="-2.396982910156" Y="-27.41705859375" />
                  <Point X="-2.408825683594" Y="-27.40701171875" />
                  <Point X="-2.433986083984" Y="-27.388984375" />
                  <Point X="-2.447303710938" Y="-27.38100390625" />
                  <Point X="-2.476132324219" Y="-27.3667890625" />
                  <Point X="-2.490572753906" Y="-27.361083984375" />
                  <Point X="-2.520189208984" Y="-27.3521015625" />
                  <Point X="-2.550873291016" Y="-27.3480625" />
                  <Point X="-2.581805419922" Y="-27.349076171875" />
                  <Point X="-2.597229492188" Y="-27.3508515625" />
                  <Point X="-2.628754882813" Y="-27.357123046875" />
                  <Point X="-2.643685302734" Y="-27.36138671875" />
                  <Point X="-2.672649902344" Y="-27.3722890625" />
                  <Point X="-2.686684082031" Y="-27.378927734375" />
                  <Point X="-2.934781738281" Y="-27.52216796875" />
                  <Point X="-3.793087158203" Y="-28.0177109375" />
                  <Point X="-3.945436523438" Y="-27.8175546875" />
                  <Point X="-4.004019287109" Y="-27.74058984375" />
                  <Point X="-4.181265136719" Y="-27.443375" />
                  <Point X="-4.100020019531" Y="-27.381033203125" />
                  <Point X="-3.048122314453" Y="-26.573884765625" />
                  <Point X="-3.036480712891" Y="-26.563310546875" />
                  <Point X="-3.015102539062" Y="-26.540388671875" />
                  <Point X="-3.005365966797" Y="-26.528041015625" />
                  <Point X="-2.987400634766" Y="-26.500908203125" />
                  <Point X="-2.979834716797" Y="-26.487125" />
                  <Point X="-2.967079833984" Y="-26.458498046875" />
                  <Point X="-2.961890869141" Y="-26.443654296875" />
                  <Point X="-2.955668701172" Y="-26.419630859375" />
                  <Point X="-2.951552246094" Y="-26.398802734375" />
                  <Point X="-2.948748046875" Y="-26.368369140625" />
                  <Point X="-2.948577880859" Y="-26.3530390625" />
                  <Point X="-2.950787597656" Y="-26.321369140625" />
                  <Point X="-2.953084472656" Y="-26.30621484375" />
                  <Point X="-2.9600859375" Y="-26.276470703125" />
                  <Point X="-2.971781738281" Y="-26.248236328125" />
                  <Point X="-2.987863769531" Y="-26.222255859375" />
                  <Point X="-2.996954589844" Y="-26.209919921875" />
                  <Point X="-3.017786132812" Y="-26.18596484375" />
                  <Point X="-3.028744140625" Y="-26.175248046875" />
                  <Point X="-3.052242431641" Y="-26.15571484375" />
                  <Point X="-3.064782714844" Y="-26.1468984375" />
                  <Point X="-3.086169677734" Y="-26.134310546875" />
                  <Point X="-3.086180664063" Y="-26.134302734375" />
                  <Point X="-3.105469970703" Y="-26.12446484375" />
                  <Point X="-3.134720703125" Y="-26.11325" />
                  <Point X="-3.149811767578" Y="-26.10885546875" />
                  <Point X="-3.181692871094" Y="-26.102376953125" />
                  <Point X="-3.197307128906" Y="-26.10053125" />
                  <Point X="-3.228625" Y="-26.09944140625" />
                  <Point X="-3.244328613281" Y="-26.100197265625" />
                  <Point X="-3.557528320312" Y="-26.1414296875" />
                  <Point X="-4.660920410156" Y="-26.286693359375" />
                  <Point X="-4.717849609375" Y="-26.063818359375" />
                  <Point X="-4.740762207031" Y="-25.974119140625" />
                  <Point X="-4.786452148438" Y="-25.654658203125" />
                  <Point X="-4.706558105469" Y="-25.63325" />
                  <Point X="-3.508287841797" Y="-25.312173828125" />
                  <Point X="-3.500468994141" Y="-25.3097109375" />
                  <Point X="-3.473933837891" Y="-25.299251953125" />
                  <Point X="-3.444165527344" Y="-25.283892578125" />
                  <Point X="-3.433556396484" Y="-25.27751171875" />
                  <Point X="-3.411143554688" Y="-25.261955078125" />
                  <Point X="-3.393678710938" Y="-25.248248046875" />
                  <Point X="-3.371230712891" Y="-25.226373046875" />
                  <Point X="-3.360914794922" Y="-25.21450390625" />
                  <Point X="-3.341665283203" Y="-25.18824609375" />
                  <Point X="-3.333443847656" Y="-25.174826171875" />
                  <Point X="-3.319330322266" Y="-25.14681640625" />
                  <Point X="-3.313438232422" Y="-25.1322265625" />
                  <Point X="-3.305967285156" Y="-25.108154296875" />
                  <Point X="-3.300991699219" Y="-25.08850390625" />
                  <Point X="-3.296722412109" Y="-25.060349609375" />
                  <Point X="-3.295648681641" Y="-25.046111328125" />
                  <Point X="-3.295647216797" Y="-25.016470703125" />
                  <Point X="-3.296720458984" Y="-25.002228515625" />
                  <Point X="-3.300989746094" Y="-24.97406640625" />
                  <Point X="-3.304185791016" Y="-24.960146484375" />
                  <Point X="-3.311656738281" Y="-24.93607421875" />
                  <Point X="-3.319326904297" Y="-24.915751953125" />
                  <Point X="-3.333435791016" Y="-24.887748046875" />
                  <Point X="-3.341654541016" Y="-24.874330078125" />
                  <Point X="-3.360900634766" Y="-24.848072265625" />
                  <Point X="-3.371223144531" Y="-24.8361953125" />
                  <Point X="-3.393682373047" Y="-24.814310546875" />
                  <Point X="-3.405819091797" Y="-24.804302734375" />
                  <Point X="-3.428227539062" Y="-24.788751953125" />
                  <Point X="-3.448529296875" Y="-24.776466796875" />
                  <Point X="-3.469440429688" Y="-24.76553515625" />
                  <Point X="-3.478938964844" Y="-24.76121484375" />
                  <Point X="-3.498363037109" Y="-24.753640625" />
                  <Point X="-3.508288574219" Y="-24.75038671875" />
                  <Point X="-3.79378515625" Y="-24.673888671875" />
                  <Point X="-4.7854453125" Y="-24.408173828125" />
                  <Point X="-4.746095703125" Y="-24.14225" />
                  <Point X="-4.731330566406" Y="-24.042466796875" />
                  <Point X="-4.633586914062" Y="-23.681763671875" />
                  <Point X="-3.778067382812" Y="-23.79439453125" />
                  <Point X="-3.767738769531" Y="-23.79518359375" />
                  <Point X="-3.747057128906" Y="-23.795634765625" />
                  <Point X="-3.736703857422" Y="-23.795296875" />
                  <Point X="-3.715142089844" Y="-23.79341015625" />
                  <Point X="-3.704888916016" Y="-23.7919453125" />
                  <Point X="-3.684603759766" Y="-23.78791015625" />
                  <Point X="-3.674571777344" Y="-23.78533984375" />
                  <Point X="-3.62496484375" Y="-23.76969921875" />
                  <Point X="-3.603453125" Y="-23.76232421875" />
                  <Point X="-3.584524902344" Y="-23.75399609375" />
                  <Point X="-3.575289306641" Y="-23.74931640625" />
                  <Point X="-3.556545166016" Y="-23.73849609375" />
                  <Point X="-3.547870117188" Y="-23.7328359375" />
                  <Point X="-3.531185058594" Y="-23.720603515625" />
                  <Point X="-3.515929931641" Y="-23.706626953125" />
                  <Point X="-3.502288818359" Y="-23.691072265625" />
                  <Point X="-3.495892822266" Y="-23.682921875" />
                  <Point X="-3.483478515625" Y="-23.66519140625" />
                  <Point X="-3.478009765625" Y="-23.656396484375" />
                  <Point X="-3.468062011719" Y="-23.638263671875" />
                  <Point X="-3.463583007813" Y="-23.62892578125" />
                  <Point X="-3.443677978516" Y="-23.58087109375" />
                  <Point X="-3.435499267578" Y="-23.559650390625" />
                  <Point X="-3.4297109375" Y="-23.539791015625" />
                  <Point X="-3.427358886719" Y="-23.529703125" />
                  <Point X="-3.423600341797" Y="-23.50838671875" />
                  <Point X="-3.422360595703" Y="-23.4981015625" />
                  <Point X="-3.421008056641" Y="-23.4774609375" />
                  <Point X="-3.421910400391" Y="-23.456798828125" />
                  <Point X="-3.425056884766" Y="-23.43635546875" />
                  <Point X="-3.427188232422" Y="-23.42621875" />
                  <Point X="-3.432790039062" Y="-23.4053125" />
                  <Point X="-3.436011962891" Y="-23.395470703125" />
                  <Point X="-3.443509033203" Y="-23.37619140625" />
                  <Point X="-3.447784179688" Y="-23.36675390625" />
                  <Point X="-3.471801757812" Y="-23.3206171875" />
                  <Point X="-3.482800292969" Y="-23.300712890625" />
                  <Point X="-3.494292480469" Y="-23.283513671875" />
                  <Point X="-3.500508300781" Y="-23.2752265625" />
                  <Point X="-3.514420898438" Y="-23.258646484375" />
                  <Point X="-3.521500976563" Y="-23.251087890625" />
                  <Point X="-3.536441894531" Y="-23.23678515625" />
                  <Point X="-3.544302734375" Y="-23.230041015625" />
                  <Point X="-3.708064208984" Y="-23.104380859375" />
                  <Point X="-4.227614257812" Y="-22.70571875" />
                  <Point X="-4.059661132812" Y="-22.41797265625" />
                  <Point X="-4.002296142578" Y="-22.319693359375" />
                  <Point X="-3.726339111328" Y="-21.96498828125" />
                  <Point X="-3.254157470703" Y="-22.2376015625" />
                  <Point X="-3.244919433594" Y="-22.242283203125" />
                  <Point X="-3.225988525391" Y="-22.25061328125" />
                  <Point X="-3.216296142578" Y="-22.254259765625" />
                  <Point X="-3.195651855469" Y="-22.26076953125" />
                  <Point X="-3.185621826172" Y="-22.263341796875" />
                  <Point X="-3.165332763672" Y="-22.26737890625" />
                  <Point X="-3.155073730469" Y="-22.26884375" />
                  <Point X="-3.085985107422" Y="-22.274888671875" />
                  <Point X="-3.059172363281" Y="-22.276666015625" />
                  <Point X="-3.038486328125" Y="-22.27621484375" />
                  <Point X="-3.02815234375" Y="-22.27542578125" />
                  <Point X="-3.006695068359" Y="-22.272599609375" />
                  <Point X="-2.99651171875" Y="-22.2706875" />
                  <Point X="-2.976421875" Y="-22.26576953125" />
                  <Point X="-2.956990722656" Y="-22.2586953125" />
                  <Point X="-2.938440917969" Y="-22.249546875" />
                  <Point X="-2.929415771484" Y="-22.244466796875" />
                  <Point X="-2.911162841797" Y="-22.232837890625" />
                  <Point X="-2.902745605469" Y="-22.226806640625" />
                  <Point X="-2.886615966797" Y="-22.213861328125" />
                  <Point X="-2.878903564453" Y="-22.206947265625" />
                  <Point X="-2.829864013672" Y="-22.157908203125" />
                  <Point X="-2.811267333984" Y="-22.138513671875" />
                  <Point X="-2.798321777344" Y="-22.122384765625" />
                  <Point X="-2.792288818359" Y="-22.113966796875" />
                  <Point X="-2.780658935547" Y="-22.095712890625" />
                  <Point X="-2.775577148438" Y="-22.086685546875" />
                  <Point X="-2.766427490234" Y="-22.0681328125" />
                  <Point X="-2.759352294922" Y="-22.048693359375" />
                  <Point X="-2.754435302734" Y="-22.028599609375" />
                  <Point X="-2.752525634766" Y="-22.018419921875" />
                  <Point X="-2.749700195312" Y="-21.9969609375" />
                  <Point X="-2.748909912109" Y="-21.986634765625" />
                  <Point X="-2.748458496094" Y="-21.965955078125" />
                  <Point X="-2.748797363281" Y="-21.9556015625" />
                  <Point X="-2.754841796875" Y="-21.88651171875" />
                  <Point X="-2.757745605469" Y="-21.859796875" />
                  <Point X="-2.761780761719" Y="-21.839509765625" />
                  <Point X="-2.764352294922" Y="-21.8294765625" />
                  <Point X="-2.770860839844" Y="-21.808833984375" />
                  <Point X="-2.774509765625" Y="-21.799138671875" />
                  <Point X="-2.782840820312" Y="-21.780205078125" />
                  <Point X="-2.787522949219" Y="-21.770966796875" />
                  <Point X="-2.860090576172" Y="-21.64527734375" />
                  <Point X="-3.059386962891" Y="-21.300087890625" />
                  <Point X="-2.748283447266" Y="-21.06156640625" />
                  <Point X="-2.648367919922" Y="-20.984962890625" />
                  <Point X="-2.192523193359" Y="-20.731703125" />
                  <Point X="-2.118563964844" Y="-20.82808984375" />
                  <Point X="-2.111823486328" Y="-20.835947265625" />
                  <Point X="-2.097523681641" Y="-20.85088671875" />
                  <Point X="-2.089963623047" Y="-20.85796875" />
                  <Point X="-2.073383056641" Y="-20.8718828125" />
                  <Point X="-2.06509765625" Y="-20.87809765625" />
                  <Point X="-2.047896972656" Y="-20.889591796875" />
                  <Point X="-2.038982299805" Y="-20.89487109375" />
                  <Point X="-1.962087036133" Y="-20.93490234375" />
                  <Point X="-1.943766235352" Y="-20.944439453125" />
                  <Point X="-1.934329467773" Y="-20.94871484375" />
                  <Point X="-1.915047973633" Y="-20.956212890625" />
                  <Point X="-1.90520324707" Y="-20.959435546875" />
                  <Point X="-1.884296630859" Y="-20.965037109375" />
                  <Point X="-1.874159790039" Y="-20.96716796875" />
                  <Point X="-1.853713745117" Y="-20.970314453125" />
                  <Point X="-1.833048217773" Y="-20.971216796875" />
                  <Point X="-1.812405883789" Y="-20.96986328125" />
                  <Point X="-1.802119384766" Y="-20.968623046875" />
                  <Point X="-1.780804199219" Y="-20.96486328125" />
                  <Point X="-1.770712890625" Y="-20.9625078125" />
                  <Point X="-1.750859375" Y="-20.95671875" />
                  <Point X="-1.741097167969" Y="-20.95328515625" />
                  <Point X="-1.661005615234" Y="-20.920109375" />
                  <Point X="-1.641923583984" Y="-20.912205078125" />
                  <Point X="-1.632588134766" Y="-20.907728515625" />
                  <Point X="-1.614455078125" Y="-20.89778125" />
                  <Point X="-1.605657226562" Y="-20.892310546875" />
                  <Point X="-1.587927612305" Y="-20.879896484375" />
                  <Point X="-1.579779296875" Y="-20.873501953125" />
                  <Point X="-1.564227050781" Y="-20.85986328125" />
                  <Point X="-1.550251831055" Y="-20.844611328125" />
                  <Point X="-1.538020385742" Y="-20.8279296875" />
                  <Point X="-1.532360229492" Y="-20.819255859375" />
                  <Point X="-1.521538330078" Y="-20.80051171875" />
                  <Point X="-1.516855712891" Y="-20.791271484375" />
                  <Point X="-1.508525024414" Y="-20.772337890625" />
                  <Point X="-1.504877075195" Y="-20.76264453125" />
                  <Point X="-1.478808837891" Y="-20.679966796875" />
                  <Point X="-1.47259765625" Y="-20.660267578125" />
                  <Point X="-1.470026123047" Y="-20.650234375" />
                  <Point X="-1.465990844727" Y="-20.629947265625" />
                  <Point X="-1.464527099609" Y="-20.619693359375" />
                  <Point X="-1.46264074707" Y="-20.5981328125" />
                  <Point X="-1.462301757812" Y="-20.58778125" />
                  <Point X="-1.462752929688" Y="-20.5671015625" />
                  <Point X="-1.46354309082" Y="-20.5567734375" />
                  <Point X="-1.47179309082" Y="-20.494107421875" />
                  <Point X="-1.479266113281" Y="-20.43734375" />
                  <Point X="-1.06051550293" Y="-20.31994140625" />
                  <Point X="-0.931166625977" Y="-20.2836796875" />
                  <Point X="-0.365223083496" Y="-20.21744140625" />
                  <Point X="-0.225666473389" Y="-20.7382734375" />
                  <Point X="-0.220435256958" Y="-20.752892578125" />
                  <Point X="-0.207661560059" Y="-20.781083984375" />
                  <Point X="-0.200119247437" Y="-20.79465625" />
                  <Point X="-0.182260864258" Y="-20.8213828125" />
                  <Point X="-0.172608840942" Y="-20.833544921875" />
                  <Point X="-0.151451187134" Y="-20.856134765625" />
                  <Point X="-0.126896453857" Y="-20.8749765625" />
                  <Point X="-0.099600524902" Y="-20.88956640625" />
                  <Point X="-0.085353973389" Y="-20.8957421875" />
                  <Point X="-0.054916004181" Y="-20.90607421875" />
                  <Point X="-0.039853668213" Y="-20.909845703125" />
                  <Point X="-0.009317756653" Y="-20.91488671875" />
                  <Point X="0.021629543304" Y="-20.91488671875" />
                  <Point X="0.052165454865" Y="-20.909845703125" />
                  <Point X="0.067227645874" Y="-20.90607421875" />
                  <Point X="0.097665908813" Y="-20.8957421875" />
                  <Point X="0.111912460327" Y="-20.88956640625" />
                  <Point X="0.139208389282" Y="-20.8749765625" />
                  <Point X="0.163763275146" Y="-20.856134765625" />
                  <Point X="0.184920776367" Y="-20.833544921875" />
                  <Point X="0.194572952271" Y="-20.8213828125" />
                  <Point X="0.212431182861" Y="-20.79465625" />
                  <Point X="0.219973937988" Y="-20.781083984375" />
                  <Point X="0.232747192383" Y="-20.752892578125" />
                  <Point X="0.237977966309" Y="-20.7382734375" />
                  <Point X="0.275160888672" Y="-20.599505859375" />
                  <Point X="0.378190795898" Y="-20.214990234375" />
                  <Point X="0.714931152344" Y="-20.2502578125" />
                  <Point X="0.82785168457" Y="-20.262083984375" />
                  <Point X="1.34069543457" Y="-20.385900390625" />
                  <Point X="1.453621826172" Y="-20.413166015625" />
                  <Point X="1.786137084961" Y="-20.533771484375" />
                  <Point X="1.858250732422" Y="-20.559927734375" />
                  <Point X="2.181025634766" Y="-20.710876953125" />
                  <Point X="2.250425292969" Y="-20.743333984375" />
                  <Point X="2.56230859375" Y="-20.925037109375" />
                  <Point X="2.629432373047" Y="-20.964142578125" />
                  <Point X="2.817779052734" Y="-21.098083984375" />
                  <Point X="2.761147705078" Y="-21.196173828125" />
                  <Point X="2.06530859375" Y="-22.40140234375" />
                  <Point X="2.062371826172" Y="-22.4068984375" />
                  <Point X="2.053181640625" Y="-22.426560546875" />
                  <Point X="2.044182617188" Y="-22.45043359375" />
                  <Point X="2.041301513672" Y="-22.459400390625" />
                  <Point X="2.023962890625" Y="-22.52423828125" />
                  <Point X="2.019876098633" Y="-22.543453125" />
                  <Point X="2.014407470703" Y="-22.5778046875" />
                  <Point X="2.013243041992" Y="-22.59094140625" />
                  <Point X="2.012744995117" Y="-22.61725" />
                  <Point X="2.013411254883" Y="-22.630421875" />
                  <Point X="2.02017199707" Y="-22.68648828125" />
                  <Point X="2.023800537109" Y="-22.71096484375" />
                  <Point X="2.029143310547" Y="-22.732888671875" />
                  <Point X="2.032468261719" Y="-22.743693359375" />
                  <Point X="2.040734130859" Y="-22.76578125" />
                  <Point X="2.045318237305" Y="-22.776111328125" />
                  <Point X="2.055681152344" Y="-22.79615625" />
                  <Point X="2.061459716797" Y="-22.80587109375" />
                  <Point X="2.096151855469" Y="-22.856998046875" />
                  <Point X="2.108341308594" Y="-22.873056640625" />
                  <Point X="2.129907226562" Y="-22.8985390625" />
                  <Point X="2.138908935547" Y="-22.907814453125" />
                  <Point X="2.158082763672" Y="-22.925052734375" />
                  <Point X="2.168254882812" Y="-22.933015625" />
                  <Point X="2.219382080078" Y="-22.967708984375" />
                  <Point X="2.241277099609" Y="-22.98175390625" />
                  <Point X="2.261322265625" Y="-22.9921171875" />
                  <Point X="2.271653808594" Y="-22.996701171875" />
                  <Point X="2.293740234375" Y="-23.004966796875" />
                  <Point X="2.304542724609" Y="-23.00829296875" />
                  <Point X="2.326468505859" Y="-23.01363671875" />
                  <Point X="2.337591796875" Y="-23.015654296875" />
                  <Point X="2.393658447266" Y="-23.0224140625" />
                  <Point X="2.414791259766" Y="-23.0237734375" />
                  <Point X="2.447053466797" Y="-23.024046875" />
                  <Point X="2.459832519531" Y="-23.02329296875" />
                  <Point X="2.485181396484" Y="-23.020072265625" />
                  <Point X="2.497751220703" Y="-23.01760546875" />
                  <Point X="2.562589355469" Y="-23.000265625" />
                  <Point X="2.573322753906" Y="-22.99705078125" />
                  <Point X="2.600854736328" Y="-22.98791796875" />
                  <Point X="2.609851806641" Y="-22.98441796875" />
                  <Point X="2.636033935547" Y="-22.972125" />
                  <Point X="2.923188476563" Y="-22.8063359375" />
                  <Point X="3.940402832031" Y="-22.219048828125" />
                  <Point X="4.004134765625" Y="-22.30762109375" />
                  <Point X="4.043959960938" Y="-22.36296875" />
                  <Point X="4.136884765625" Y="-22.51652734375" />
                  <Point X="4.089088134766" Y="-22.553203125" />
                  <Point X="3.172951171875" Y="-23.2561796875" />
                  <Point X="3.168137939453" Y="-23.260130859375" />
                  <Point X="3.152124755859" Y="-23.27477734375" />
                  <Point X="3.134671386719" Y="-23.293392578125" />
                  <Point X="3.128576904297" Y="-23.300576171875" />
                  <Point X="3.081912841797" Y="-23.361453125" />
                  <Point X="3.071421142578" Y="-23.376609375" />
                  <Point X="3.051833007812" Y="-23.40806640625" />
                  <Point X="3.045532958984" Y="-23.41999609375" />
                  <Point X="3.034686523438" Y="-23.444626953125" />
                  <Point X="3.030140136719" Y="-23.457328125" />
                  <Point X="3.012757568359" Y="-23.519484375" />
                  <Point X="3.006225097656" Y="-23.54533984375" />
                  <Point X="3.002771728516" Y="-23.567638671875" />
                  <Point X="3.001709472656" Y="-23.578890625" />
                  <Point X="3.000893310547" Y="-23.6024609375" />
                  <Point X="3.001174804688" Y="-23.613759765625" />
                  <Point X="3.003077880859" Y="-23.636244140625" />
                  <Point X="3.004699462891" Y="-23.6474296875" />
                  <Point X="3.01896875" Y="-23.7165859375" />
                  <Point X="3.023725585938" Y="-23.734953125" />
                  <Point X="3.034323730469" Y="-23.76880859375" />
                  <Point X="3.039141113281" Y="-23.781119140625" />
                  <Point X="3.050438720703" Y="-23.804953125" />
                  <Point X="3.056918945312" Y="-23.8164765625" />
                  <Point X="3.095729492188" Y="-23.875466796875" />
                  <Point X="3.111739501953" Y="-23.898578125" />
                  <Point X="3.126292480469" Y="-23.915822265625" />
                  <Point X="3.134082519531" Y="-23.924009765625" />
                  <Point X="3.151326171875" Y="-23.940099609375" />
                  <Point X="3.160032226562" Y="-23.9473046875" />
                  <Point X="3.178241455078" Y="-23.960630859375" />
                  <Point X="3.187744628906" Y="-23.966751953125" />
                  <Point X="3.243986572266" Y="-23.998412109375" />
                  <Point X="3.261910888672" Y="-24.007291015625" />
                  <Point X="3.293286132813" Y="-24.020830078125" />
                  <Point X="3.305638916016" Y="-24.025177734375" />
                  <Point X="3.330832763672" Y="-24.032134765625" />
                  <Point X="3.343673828125" Y="-24.034744140625" />
                  <Point X="3.419716796875" Y="-24.04479296875" />
                  <Point X="3.430043457031" Y="-24.04587109375" />
                  <Point X="3.461238769531" Y="-24.04826953125" />
                  <Point X="3.47109375" Y="-24.048513671875" />
                  <Point X="3.500604003906" Y="-24.047203125" />
                  <Point X="3.773381591797" Y="-24.011291015625" />
                  <Point X="4.704704101562" Y="-23.8886796875" />
                  <Point X="4.735582519531" Y="-24.015521484375" />
                  <Point X="4.752683105469" Y="-24.08576171875" />
                  <Point X="4.78387109375" Y="-24.286076171875" />
                  <Point X="4.741904296875" Y="-24.297322265625" />
                  <Point X="3.691991455078" Y="-24.578646484375" />
                  <Point X="3.686024414062" Y="-24.580458984375" />
                  <Point X="3.665628662109" Y="-24.58786328125" />
                  <Point X="3.642385009766" Y="-24.598380859375" />
                  <Point X="3.6340078125" Y="-24.60268359375" />
                  <Point X="3.559298095703" Y="-24.6458671875" />
                  <Point X="3.541497802734" Y="-24.656154296875" />
                  <Point X="3.533888671875" Y="-24.661048828125" />
                  <Point X="3.508778320312" Y="-24.680125" />
                  <Point X="3.481993652344" Y="-24.7056484375" />
                  <Point X="3.472795898438" Y="-24.7157734375" />
                  <Point X="3.427969970703" Y="-24.772892578125" />
                  <Point X="3.410852783203" Y="-24.795794921875" />
                  <Point X="3.399128417969" Y="-24.815076171875" />
                  <Point X="3.393841308594" Y="-24.825064453125" />
                  <Point X="3.384068115234" Y="-24.84652734375" />
                  <Point X="3.380004882812" Y="-24.857072265625" />
                  <Point X="3.373158935547" Y="-24.878572265625" />
                  <Point X="3.370376220703" Y="-24.88952734375" />
                  <Point X="3.355434082031" Y="-24.967548828125" />
                  <Point X="3.352897949219" Y="-24.985283203125" />
                  <Point X="3.349338134766" Y="-25.02268359375" />
                  <Point X="3.349016357422" Y="-25.0361640625" />
                  <Point X="3.350284423828" Y="-25.06303515625" />
                  <Point X="3.351874267578" Y="-25.07642578125" />
                  <Point X="3.36681640625" Y="-25.154447265625" />
                  <Point X="3.373159423828" Y="-25.183990234375" />
                  <Point X="3.380006347656" Y="-25.2054921875" />
                  <Point X="3.3840703125" Y="-25.2160390625" />
                  <Point X="3.393843261719" Y="-25.2375" />
                  <Point X="3.399129882813" Y="-25.24748828125" />
                  <Point X="3.410853271484" Y="-25.266767578125" />
                  <Point X="3.417290039063" Y="-25.27605859375" />
                  <Point X="3.462115966797" Y="-25.333177734375" />
                  <Point X="3.474526611328" Y="-25.347462890625" />
                  <Point X="3.499518798828" Y="-25.373494140625" />
                  <Point X="3.509432373047" Y="-25.382462890625" />
                  <Point X="3.530420166016" Y="-25.39891796875" />
                  <Point X="3.541494384766" Y="-25.406404296875" />
                  <Point X="3.616204345703" Y="-25.449587890625" />
                  <Point X="3.634004394531" Y="-25.459876953125" />
                  <Point X="3.639489013672" Y="-25.4628125" />
                  <Point X="3.659156005859" Y="-25.472017578125" />
                  <Point X="3.683026611328" Y="-25.481029296875" />
                  <Point X="3.691991943359" Y="-25.483916015625" />
                  <Point X="3.942141357422" Y="-25.550943359375" />
                  <Point X="4.784876464844" Y="-25.776751953125" />
                  <Point X="4.771162109375" Y="-25.86771875" />
                  <Point X="4.761612304688" Y="-25.9310625" />
                  <Point X="4.727802734375" Y="-26.079220703125" />
                  <Point X="4.654384765625" Y="-26.0695546875" />
                  <Point X="3.43678125" Y="-25.90925390625" />
                  <Point X="3.428622558594" Y="-25.90853515625" />
                  <Point X="3.400096435547" Y="-25.90804296875" />
                  <Point X="3.366721435547" Y="-25.910841796875" />
                  <Point X="3.354482177734" Y="-25.912677734375" />
                  <Point X="3.207853271484" Y="-25.944548828125" />
                  <Point X="3.17291796875" Y="-25.952140625" />
                  <Point X="3.157876220703" Y="-25.9567421875" />
                  <Point X="3.128758789062" Y="-25.968365234375" />
                  <Point X="3.114683105469" Y="-25.97538671875" />
                  <Point X="3.086854248047" Y="-25.992279296875" />
                  <Point X="3.074126708984" Y="-26.001529296875" />
                  <Point X="3.050374267578" Y="-26.022001953125" />
                  <Point X="3.039349365234" Y="-26.033224609375" />
                  <Point X="2.950721435547" Y="-26.13981640625" />
                  <Point X="2.929605224609" Y="-26.165212890625" />
                  <Point X="2.921326904297" Y="-26.17684765625" />
                  <Point X="2.906606445312" Y="-26.201228515625" />
                  <Point X="2.900164306641" Y="-26.213974609375" />
                  <Point X="2.888821044922" Y="-26.241359375" />
                  <Point X="2.884363037109" Y="-26.254927734375" />
                  <Point X="2.877531005859" Y="-26.282578125" />
                  <Point X="2.875156982422" Y="-26.29666015625" />
                  <Point X="2.862454345703" Y="-26.434701171875" />
                  <Point X="2.859427978516" Y="-26.467591796875" />
                  <Point X="2.859288574219" Y="-26.483322265625" />
                  <Point X="2.861607666016" Y="-26.514591796875" />
                  <Point X="2.864066162109" Y="-26.530130859375" />
                  <Point X="2.871798095703" Y="-26.561751953125" />
                  <Point X="2.876786865234" Y="-26.576671875" />
                  <Point X="2.889158203125" Y="-26.605482421875" />
                  <Point X="2.896540771484" Y="-26.619373046875" />
                  <Point X="2.977687255859" Y="-26.74558984375" />
                  <Point X="2.997020996094" Y="-26.775662109375" />
                  <Point X="3.001741455078" Y="-26.7823515625" />
                  <Point X="3.019789306641" Y="-26.8044453125" />
                  <Point X="3.043486816406" Y="-26.828119140625" />
                  <Point X="3.052796142578" Y="-26.836279296875" />
                  <Point X="3.284936279297" Y="-27.01440625" />
                  <Point X="4.087170654297" Y="-27.629982421875" />
                  <Point X="4.072405029297" Y="-27.653875" />
                  <Point X="4.045500244141" Y="-27.697412109375" />
                  <Point X="4.001273681641" Y="-27.760251953125" />
                  <Point X="3.932894775391" Y="-27.7207734375" />
                  <Point X="2.848454345703" Y="-27.094671875" />
                  <Point X="2.841197509766" Y="-27.090890625" />
                  <Point X="2.815036865234" Y="-27.079517578125" />
                  <Point X="2.783124267578" Y="-27.069326171875" />
                  <Point X="2.771107910156" Y="-27.0663359375" />
                  <Point X="2.596596191406" Y="-27.034818359375" />
                  <Point X="2.555017578125" Y="-27.027310546875" />
                  <Point X="2.539352050781" Y="-27.025806640625" />
                  <Point X="2.507995361328" Y="-27.025404296875" />
                  <Point X="2.492304199219" Y="-27.026505859375" />
                  <Point X="2.460132324219" Y="-27.03146484375" />
                  <Point X="2.444840332031" Y="-27.035138671875" />
                  <Point X="2.415068847656" Y="-27.0449609375" />
                  <Point X="2.400589355469" Y="-27.051109375" />
                  <Point X="2.255613037109" Y="-27.127408203125" />
                  <Point X="2.221071289062" Y="-27.145587890625" />
                  <Point X="2.208969970703" Y="-27.153169921875" />
                  <Point X="2.186038818359" Y="-27.1700625" />
                  <Point X="2.175208984375" Y="-27.179373046875" />
                  <Point X="2.15425" Y="-27.20033203125" />
                  <Point X="2.144938476562" Y="-27.211162109375" />
                  <Point X="2.128045410156" Y="-27.23409375" />
                  <Point X="2.120463867188" Y="-27.2461953125" />
                  <Point X="2.04416394043" Y="-27.391171875" />
                  <Point X="2.025984985352" Y="-27.425712890625" />
                  <Point X="2.01983605957" Y="-27.440193359375" />
                  <Point X="2.010012573242" Y="-27.46996875" />
                  <Point X="2.006337890625" Y="-27.485263671875" />
                  <Point X="2.001379760742" Y="-27.5174375" />
                  <Point X="2.000279418945" Y="-27.533130859375" />
                  <Point X="2.00068347168" Y="-27.564482421875" />
                  <Point X="2.002187866211" Y="-27.580140625" />
                  <Point X="2.033704467773" Y="-27.75465234375" />
                  <Point X="2.041213500977" Y="-27.796232421875" />
                  <Point X="2.043014648438" Y="-27.804220703125" />
                  <Point X="2.051236572266" Y="-27.831546875" />
                  <Point X="2.064069824219" Y="-27.862482421875" />
                  <Point X="2.069546630859" Y="-27.873580078125" />
                  <Point X="2.218719970703" Y="-28.131955078125" />
                  <Point X="2.735893066406" Y="-29.02772265625" />
                  <Point X="2.723753417969" Y="-29.036083984375" />
                  <Point X="2.661443359375" Y="-28.95487890625" />
                  <Point X="1.833914916992" Y="-27.876423828125" />
                  <Point X="1.828654541016" Y="-27.8701484375" />
                  <Point X="1.808834472656" Y="-27.849630859375" />
                  <Point X="1.783252319336" Y="-27.828005859375" />
                  <Point X="1.773297973633" Y="-27.820646484375" />
                  <Point X="1.601182495117" Y="-27.7099921875" />
                  <Point X="1.560174682617" Y="-27.68362890625" />
                  <Point X="1.546279418945" Y="-27.676244140625" />
                  <Point X="1.517465698242" Y="-27.663873046875" />
                  <Point X="1.502547241211" Y="-27.65888671875" />
                  <Point X="1.470927001953" Y="-27.65115625" />
                  <Point X="1.455390991211" Y="-27.648697265625" />
                  <Point X="1.424125488281" Y="-27.64637890625" />
                  <Point X="1.408395996094" Y="-27.64651953125" />
                  <Point X="1.220158203125" Y="-27.66383984375" />
                  <Point X="1.175309204102" Y="-27.667966796875" />
                  <Point X="1.16122668457" Y="-27.67033984375" />
                  <Point X="1.133575317383" Y="-27.677171875" />
                  <Point X="1.120006347656" Y="-27.681630859375" />
                  <Point X="1.092620849609" Y="-27.692974609375" />
                  <Point X="1.07987487793" Y="-27.699416015625" />
                  <Point X="1.055493286133" Y="-27.71413671875" />
                  <Point X="1.043857666016" Y="-27.722416015625" />
                  <Point X="0.898505249023" Y="-27.8432734375" />
                  <Point X="0.863873718262" Y="-27.872068359375" />
                  <Point X="0.852653930664" Y="-27.88308984375" />
                  <Point X="0.832183776855" Y="-27.906837890625" />
                  <Point X="0.82293347168" Y="-27.919564453125" />
                  <Point X="0.806041320801" Y="-27.947390625" />
                  <Point X="0.799019165039" Y="-27.96146875" />
                  <Point X="0.787394348145" Y="-27.99058984375" />
                  <Point X="0.782791931152" Y="-28.0056328125" />
                  <Point X="0.739332214355" Y="-28.205580078125" />
                  <Point X="0.728977661133" Y="-28.25321875" />
                  <Point X="0.727584777832" Y="-28.2612890625" />
                  <Point X="0.724724487305" Y="-28.289677734375" />
                  <Point X="0.7247421875" Y="-28.323169921875" />
                  <Point X="0.725554992676" Y="-28.33551953125" />
                  <Point X="0.767829467773" Y="-28.656626953125" />
                  <Point X="0.833091491699" Y="-29.15233984375" />
                  <Point X="0.655064941406" Y="-28.487935546875" />
                  <Point X="0.652605834961" Y="-28.480123046875" />
                  <Point X="0.642146972656" Y="-28.453583984375" />
                  <Point X="0.626788391113" Y="-28.42381640625" />
                  <Point X="0.620407592773" Y="-28.41320703125" />
                  <Point X="0.488183288574" Y="-28.222697265625" />
                  <Point X="0.456679840088" Y="-28.177306640625" />
                  <Point X="0.446670074463" Y="-28.165169921875" />
                  <Point X="0.4247840271" Y="-28.142712890625" />
                  <Point X="0.412907562256" Y="-28.132392578125" />
                  <Point X="0.386652893066" Y="-28.113150390625" />
                  <Point X="0.37323638916" Y="-28.10493359375" />
                  <Point X="0.345237182617" Y="-28.090828125" />
                  <Point X="0.330654449463" Y="-28.084939453125" />
                  <Point X="0.12604486084" Y="-28.0214375" />
                  <Point X="0.077295211792" Y="-28.006306640625" />
                  <Point X="0.063380836487" Y="-28.003111328125" />
                  <Point X="0.035228092194" Y="-27.998841796875" />
                  <Point X="0.020989871979" Y="-27.997767578125" />
                  <Point X="-0.008651183128" Y="-27.997765625" />
                  <Point X="-0.022895202637" Y="-27.998837890625" />
                  <Point X="-0.051061321259" Y="-28.003107421875" />
                  <Point X="-0.064983276367" Y="-28.0063046875" />
                  <Point X="-0.269592437744" Y="-28.06980859375" />
                  <Point X="-0.318342376709" Y="-28.0849375" />
                  <Point X="-0.332927337646" Y="-28.090828125" />
                  <Point X="-0.360927886963" Y="-28.104935546875" />
                  <Point X="-0.37434362793" Y="-28.11315234375" />
                  <Point X="-0.400599487305" Y="-28.132396484375" />
                  <Point X="-0.412476409912" Y="-28.14271875" />
                  <Point X="-0.434360534668" Y="-28.16517578125" />
                  <Point X="-0.444367767334" Y="-28.177310546875" />
                  <Point X="-0.576592224121" Y="-28.367822265625" />
                  <Point X="-0.60809552002" Y="-28.4132109375" />
                  <Point X="-0.612467834473" Y="-28.42012890625" />
                  <Point X="-0.625975280762" Y="-28.44526171875" />
                  <Point X="-0.638777648926" Y="-28.47621484375" />
                  <Point X="-0.642752990723" Y="-28.487935546875" />
                  <Point X="-0.719830200195" Y="-28.775591796875" />
                  <Point X="-0.985425354004" Y="-29.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.055687281294" Y="-22.382348191565" />
                  <Point X="3.903449243331" Y="-22.240383924481" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.769030083257" Y="-21.182520943708" />
                  <Point X="2.377369794921" Y="-20.817291816194" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.094860440645" Y="-22.548773862436" />
                  <Point X="3.817417554002" Y="-22.2900541851" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.720280711098" Y="-21.266957527534" />
                  <Point X="2.066544388346" Y="-20.657338544212" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.018443873217" Y="-22.607410369283" />
                  <Point X="3.731385864672" Y="-22.339724445718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.671531261269" Y="-21.351394038932" />
                  <Point X="1.8002135726" Y="-20.53887714943" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.942027295894" Y="-22.666046866903" />
                  <Point X="3.645354175342" Y="-22.389394706337" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.62278181144" Y="-21.43583055033" />
                  <Point X="1.572249027532" Y="-20.456192880854" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.865610718572" Y="-22.724683364523" />
                  <Point X="3.559322486013" Y="-22.439064966955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.574032361611" Y="-21.520267061728" />
                  <Point X="1.363469741595" Y="-20.391399155849" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.789194141249" Y="-22.783319862143" />
                  <Point X="3.473290796683" Y="-22.488735227574" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.525282911782" Y="-21.604703573126" />
                  <Point X="1.175509315996" Y="-20.346019332185" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.712777563927" Y="-22.841956359763" />
                  <Point X="3.387259107353" Y="-22.538405488192" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.476533461953" Y="-21.689140084524" />
                  <Point X="0.987549382829" Y="-20.300639967722" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.707273725795" Y="-23.899235142615" />
                  <Point X="4.697036851933" Y="-23.889689103304" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.636360986604" Y="-22.900592857383" />
                  <Point X="3.301227418024" Y="-22.588075748811" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.427784012124" Y="-21.773576595922" />
                  <Point X="0.804256695269" Y="-20.259612880198" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.748183667755" Y="-24.067280389471" />
                  <Point X="4.574973316831" Y="-23.905759124159" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.559944409282" Y="-22.959229355003" />
                  <Point X="3.215195728694" Y="-22.637746009429" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.379034562295" Y="-21.85801310732" />
                  <Point X="0.647336418203" Y="-20.243178463317" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.773740375082" Y="-24.22100851341" />
                  <Point X="4.452909781729" Y="-23.921829145014" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.483527831959" Y="-23.017865852623" />
                  <Point X="3.129164039364" Y="-22.687416270048" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.330285112466" Y="-21.942449618718" />
                  <Point X="0.490415901736" Y="-20.226743823192" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.721999734657" Y="-24.302655694451" />
                  <Point X="4.330846246628" Y="-23.937899165869" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.407111254637" Y="-23.076502350244" />
                  <Point X="3.043132350035" Y="-22.737086530666" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.281535662637" Y="-22.026886130116" />
                  <Point X="0.370259108261" Y="-20.244591909378" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.613794920704" Y="-24.331649181852" />
                  <Point X="4.208782711526" Y="-23.953969186723" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.330694677314" Y="-23.135138847864" />
                  <Point X="2.957100660705" Y="-22.786756791285" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.232786212808" Y="-22.111322641514" />
                  <Point X="0.342411852736" Y="-20.348520032298" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.505590106751" Y="-24.360642669252" />
                  <Point X="4.086719176424" Y="-23.970039207578" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.254278099991" Y="-23.193775345484" />
                  <Point X="2.871069072301" Y="-22.836427146018" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.18403676298" Y="-22.195759152912" />
                  <Point X="0.314564597211" Y="-20.452448155218" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.397385292798" Y="-24.389636156653" />
                  <Point X="3.964655641323" Y="-23.986109228433" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.177861522669" Y="-23.252411843104" />
                  <Point X="2.785037549565" Y="-22.886097561988" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.135287313151" Y="-22.28019566431" />
                  <Point X="0.286717341686" Y="-20.556376278138" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.289180478845" Y="-24.418629644053" />
                  <Point X="3.842592106221" Y="-24.002179249288" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.112586011875" Y="-23.321437553337" />
                  <Point X="2.699006026829" Y="-22.935767977957" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.086537863322" Y="-22.364632175708" />
                  <Point X="0.258869900248" Y="-20.660304227691" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.180975664892" Y="-24.447623131454" />
                  <Point X="3.720528597219" Y="-24.018249294481" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.057106265536" Y="-23.399597961706" />
                  <Point X="2.611200945934" Y="-22.983784524188" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.043045900435" Y="-22.453971372995" />
                  <Point X="0.228623093714" Y="-20.761994733096" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.072770850939" Y="-24.476616618854" />
                  <Point X="3.598465122394" Y="-24.034319371544" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.019710558165" Y="-23.494622009229" />
                  <Point X="2.505850252493" Y="-23.015439522023" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.017268953025" Y="-22.559830089465" />
                  <Point X="0.176125293141" Y="-20.842935850876" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.322116286534" Y="-20.378318061288" />
                  <Point X="-0.48020405739" Y="-20.230898830031" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.964566036987" Y="-24.505610106255" />
                  <Point X="3.474240273969" Y="-24.048373935119" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.001008338454" Y="-23.607078016009" />
                  <Point X="2.371118818446" Y="-23.019696536002" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.021194704033" Y="-22.693387020308" />
                  <Point X="0.094579946213" Y="-20.896789693466" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.275717086506" Y="-20.551482124103" />
                  <Point X="-0.603967027109" Y="-20.245384102467" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.856361223034" Y="-24.534603593656" />
                  <Point X="3.311932574798" Y="-24.02691566585" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.033984482843" Y="-23.767724876939" />
                  <Point X="-0.028750021633" Y="-20.911678746681" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.229317886478" Y="-20.724646186919" />
                  <Point X="-0.727729996827" Y="-20.259869374904" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.748156409081" Y="-24.563597081056" />
                  <Point X="-0.851492966546" Y="-20.274354647341" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.64492730276" Y="-24.597230490888" />
                  <Point X="-0.969319710947" Y="-20.294375539441" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.783105996475" Y="-25.788495402302" />
                  <Point X="4.764721353732" Y="-25.771351445591" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.558365326931" Y="-24.646406251346" />
                  <Point X="-1.076418819248" Y="-20.324400114042" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.765936429044" Y="-25.902380630455" />
                  <Point X="4.569261822226" Y="-25.718978592537" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.482292691216" Y="-24.705363479704" />
                  <Point X="-1.183516200046" Y="-20.354426299567" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.743326646842" Y="-26.011192776261" />
                  <Point X="4.373802290721" Y="-25.666605739483" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.422812378671" Y="-24.779793299732" />
                  <Point X="-1.290613580843" Y="-20.384452485092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.668626306944" Y="-26.071429691171" />
                  <Point X="4.178342759216" Y="-25.614232886429" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.376887199931" Y="-24.866863486527" />
                  <Point X="-1.397710961641" Y="-20.414478670617" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.506430933244" Y="-26.050076167099" />
                  <Point X="3.98288322771" Y="-25.561860033376" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.354272545119" Y="-24.975671088552" />
                  <Point X="-1.474616820042" Y="-20.472658906249" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.344235642469" Y="-26.028722720355" />
                  <Point X="3.787423245274" Y="-25.509486759822" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.358147202036" Y="-25.109180373385" />
                  <Point X="-1.463901848105" Y="-20.612546888032" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.182040351694" Y="-26.007369273611" />
                  <Point X="3.524994327192" Y="-25.394663943476" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.475802477152" Y="-25.348791801199" />
                  <Point X="-1.490644601853" Y="-20.717504975523" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.019845060918" Y="-25.986015826867" />
                  <Point X="-1.528323848023" Y="-20.812264618838" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.857649770143" Y="-25.964662380122" />
                  <Point X="-1.592020722737" Y="-20.882762431033" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.695454479368" Y="-25.943308933378" />
                  <Point X="-1.681959732036" Y="-20.928789056834" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.533259188592" Y="-25.921955486634" />
                  <Point X="-1.782290285783" Y="-20.965125410668" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.380780387301" Y="-25.909662812918" />
                  <Point X="-1.943774263753" Y="-20.944435273846" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.265465716919" Y="-25.932026251939" />
                  <Point X="-2.266890668861" Y="-20.773020460308" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.154233231288" Y="-25.958196389823" />
                  <Point X="-2.354180663121" Y="-20.821517332597" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.067515896105" Y="-26.00722727534" />
                  <Point X="-2.44147065738" Y="-20.870014204885" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.002990744335" Y="-26.076952706683" />
                  <Point X="-2.52876065164" Y="-20.918511077174" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.942155327487" Y="-26.150118871505" />
                  <Point X="-2.616050645899" Y="-20.967007949462" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.892131591961" Y="-26.233367092267" />
                  <Point X="-2.696511119163" Y="-21.021873453111" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.870853969155" Y="-26.343421496808" />
                  <Point X="-2.772956488599" Y="-21.08048310165" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.859845679837" Y="-26.46305220975" />
                  <Point X="-2.849401488626" Y="-21.13909309467" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.045000121433" Y="-27.698122714745" />
                  <Point X="3.436609135098" Y="-27.130788941718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.909053128534" Y="-26.638835006815" />
                  <Point X="-2.925846488652" Y="-21.19770308769" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.925277307079" Y="-27.71637549301" />
                  <Point X="-3.002291488678" Y="-21.25631308071" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.559542966782" Y="-27.505218811969" />
                  <Point X="-3.018263942848" Y="-21.371314635039" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.193808626484" Y="-27.294062130927" />
                  <Point X="-2.855797420367" Y="-21.652713227049" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.832654658569" Y="-27.087176716232" />
                  <Point X="-2.756022303458" Y="-21.875651137592" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.646966022699" Y="-27.043915370764" />
                  <Point X="-2.751398451742" Y="-22.009859057878" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.489468768208" Y="-27.02694291373" />
                  <Point X="-2.787355925473" Y="-22.106224279969" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.38492699148" Y="-27.059352238604" />
                  <Point X="-2.849823987664" Y="-22.177867978378" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.295883858932" Y="-27.10621428299" />
                  <Point X="-2.922456573414" Y="-22.240033105225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.20783650528" Y="-27.154004906219" />
                  <Point X="-3.024337867297" Y="-22.274923370488" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.138787513252" Y="-27.219511788275" />
                  <Point X="-3.17345875778" Y="-22.265761999259" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.09005879078" Y="-27.303967628246" />
                  <Point X="-3.487298035499" Y="-22.102998246968" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.044201283566" Y="-27.391100919761" />
                  <Point X="-3.746627267166" Y="-21.991065934966" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.006280602388" Y="-27.48563542129" />
                  <Point X="-3.805194996397" Y="-22.066346752702" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.009044970272" Y="-27.61810934485" />
                  <Point X="-3.863762725628" Y="-22.141627570438" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.037254756821" Y="-27.774311505188" />
                  <Point X="-3.922330454859" Y="-22.216908388174" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.145516475938" Y="-28.00516330032" />
                  <Point X="-3.980898184089" Y="-22.292189205909" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.307981930854" Y="-28.286560896809" />
                  <Point X="-4.033454890543" Y="-22.373075393068" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.470447335034" Y="-28.567958445987" />
                  <Point X="2.098161292618" Y="-28.220796095076" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.493114184845" Y="-27.656580539253" />
                  <Point X="-4.082550948253" Y="-22.457188687388" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.632912739214" Y="-28.849355995165" />
                  <Point X="2.448560325039" Y="-28.677444587781" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.348899207083" Y="-27.651994005648" />
                  <Point X="-4.131646762493" Y="-22.541302208747" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.222112915582" Y="-27.663659984913" />
                  <Point X="-4.180742576733" Y="-22.625415730106" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.10759911101" Y="-27.686770243383" />
                  <Point X="-3.421631080898" Y="-23.463194760838" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.024123764427" Y="-27.738824332177" />
                  <Point X="-3.441313659514" Y="-23.574736568149" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.950486250304" Y="-27.800052348156" />
                  <Point X="-3.483530154552" Y="-23.665265158446" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.876848575133" Y="-27.861280213956" />
                  <Point X="-3.549338492454" Y="-23.733793999363" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.814663362032" Y="-27.933187673408" />
                  <Point X="-3.643772392596" Y="-23.775629071643" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.777780354404" Y="-28.028689821178" />
                  <Point X="-3.761964167579" Y="-23.795309567218" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.754304861895" Y="-28.136694679063" />
                  <Point X="-3.922654711136" Y="-23.775359319956" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.730829364553" Y="-28.244699532441" />
                  <Point X="-4.084849862143" Y="-23.754006003547" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.730681263686" Y="-28.374457534953" />
                  <Point X="-4.247045013151" Y="-23.732652687139" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.750175640447" Y="-28.522532444182" />
                  <Point X="0.597418365469" Y="-28.380083980748" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.256102452358" Y="-28.061801742633" />
                  <Point X="-4.409240164158" Y="-23.71129937073" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.769670029022" Y="-28.670607364427" />
                  <Point X="0.682141152137" Y="-28.58898536626" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.052020645251" Y="-28.001388487503" />
                  <Point X="-4.571435315166" Y="-23.689946054322" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.789164530914" Y="-28.818682390343" />
                  <Point X="0.728540297725" Y="-28.762149378309" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.077753585409" Y="-28.010268168425" />
                  <Point X="-3.296209467002" Y="-25.009009504772" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.599860605097" Y="-24.725850237575" />
                  <Point X="-4.650918612382" Y="-23.745722789376" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.808659032806" Y="-28.966757416258" />
                  <Point X="0.774939443313" Y="-28.935313390358" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.182265608872" Y="-28.042705238668" />
                  <Point X="-3.311178818695" Y="-25.124946467292" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.795320151447" Y="-24.673477370678" />
                  <Point X="-4.679017548962" Y="-23.849416215915" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.828153534697" Y="-29.114832442173" />
                  <Point X="0.821338588901" Y="-29.108477402407" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.286778046448" Y="-28.075141922744" />
                  <Point X="-3.358284428454" Y="-25.210915884355" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.990780331231" Y="-24.621103913095" />
                  <Point X="-4.707116485543" Y="-23.953109642453" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.380485746147" Y="-28.117654187891" />
                  <Point X="-3.429319381386" Y="-25.274570827907" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.186240511016" Y="-24.568730455511" />
                  <Point X="-4.733665766814" Y="-24.058248145946" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.448863044372" Y="-28.183787434552" />
                  <Point X="-3.523826679716" Y="-25.316337455268" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.3817006908" Y="-24.516356997928" />
                  <Point X="-4.750556164557" Y="-24.172393704044" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.503594497053" Y="-28.262645638044" />
                  <Point X="-3.632031525891" Y="-25.345330912622" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.577160870584" Y="-24.463983540344" />
                  <Point X="-4.767446641267" Y="-24.286539188505" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.558325949735" Y="-28.341503841537" />
                  <Point X="-3.740236372065" Y="-25.374324369975" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.772621050368" Y="-24.41161008276" />
                  <Point X="-4.784337117976" Y="-24.400684672966" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.612748131544" Y="-28.420650444784" />
                  <Point X="-3.848441218239" Y="-25.403317827329" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.650177262464" Y="-28.515643324344" />
                  <Point X="-2.94907236827" Y="-26.371888956732" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.240617585525" Y="-26.100018643351" />
                  <Point X="-3.956646064413" Y="-25.432311284683" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.678024664299" Y="-28.619571310827" />
                  <Point X="-2.97541963965" Y="-26.477215837496" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.362972816008" Y="-26.115816653862" />
                  <Point X="-4.064850910588" Y="-25.461304742036" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.705872066135" Y="-28.723499297311" />
                  <Point X="-3.029926708166" Y="-26.556283282608" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.485036803107" Y="-26.131886253222" />
                  <Point X="-4.173055756762" Y="-25.49029819939" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.733719472598" Y="-28.827427279478" />
                  <Point X="-3.104202232933" Y="-26.616916344036" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.607100653881" Y="-26.147955979707" />
                  <Point X="-4.281260602936" Y="-25.519291656744" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.761566883713" Y="-28.931355257308" />
                  <Point X="-3.180618852925" Y="-26.675552801866" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.729164305304" Y="-26.164025892091" />
                  <Point X="-4.38946544911" Y="-25.548285114098" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.789414294827" Y="-29.035283235138" />
                  <Point X="-2.311529482387" Y="-27.6158878599" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.595909097868" Y="-27.350699578273" />
                  <Point X="-3.257035472917" Y="-26.734189259697" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.851227956727" Y="-26.180095804474" />
                  <Point X="-4.497670295285" Y="-25.577278571451" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.817261705942" Y="-29.139211212968" />
                  <Point X="-2.339430108407" Y="-27.719766214028" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.697955859134" Y="-27.385435542705" />
                  <Point X="-3.333452092909" Y="-26.792825717527" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.973291608151" Y="-26.196165716857" />
                  <Point X="-4.605875141459" Y="-25.606272028805" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.845109117057" Y="-29.243139190798" />
                  <Point X="-2.38788408217" Y="-27.804478261315" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.783987217177" Y="-27.435106112254" />
                  <Point X="-3.409868712901" Y="-26.851462175357" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.095355259574" Y="-26.21223562924" />
                  <Point X="-4.714079939218" Y="-25.635265531306" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.872956528171" Y="-29.347067168628" />
                  <Point X="-2.436633494968" Y="-27.888914807245" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.87001857522" Y="-27.484776681803" />
                  <Point X="-3.486285332893" Y="-26.910098633187" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.217418910997" Y="-26.228305541624" />
                  <Point X="-4.77935323699" Y="-25.704293305216" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.900803939286" Y="-29.450995146459" />
                  <Point X="-1.186337245459" Y="-29.184731030858" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.593614713938" Y="-28.804938647257" />
                  <Point X="-2.485382907766" Y="-27.973351353175" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.956049987531" Y="-27.534447200745" />
                  <Point X="-3.562701952885" Y="-26.968735091017" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.33948256242" Y="-26.244375454007" />
                  <Point X="-4.757916170883" Y="-25.854179801568" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.9286513504" Y="-29.554923124289" />
                  <Point X="-1.153693079759" Y="-29.345068316651" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.750290159433" Y="-28.78873253951" />
                  <Point X="-2.534132320564" Y="-28.057787899105" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.042081565096" Y="-27.584117565586" />
                  <Point X="-3.639118572877" Y="-27.027371548848" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.461546213843" Y="-26.26044536639" />
                  <Point X="-4.732059910698" Y="-26.008187263066" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.956498761515" Y="-29.658851102119" />
                  <Point X="-1.122469688961" Y="-29.504080708415" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.900117258138" Y="-28.77891261846" />
                  <Point X="-2.582881733362" Y="-28.142224445035" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.128113142661" Y="-27.633787930427" />
                  <Point X="-3.715535192869" Y="-27.086008006678" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.583609865266" Y="-26.276515278774" />
                  <Point X="-4.688505694373" Y="-26.178698335659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.98434617263" Y="-29.762779079949" />
                  <Point X="-1.127777931607" Y="-29.629026800872" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.029876303391" Y="-28.787806460003" />
                  <Point X="-2.631631146159" Y="-28.226660990965" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.214144720225" Y="-27.683458295268" />
                  <Point X="-3.791951812861" Y="-27.144644464508" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.120898117068" Y="-28.832823354385" />
                  <Point X="-2.680380558957" Y="-28.311097536895" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.30017629779" Y="-27.733128660109" />
                  <Point X="-3.868368432853" Y="-27.203280922338" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.202048170428" Y="-28.887045814191" />
                  <Point X="-2.729129971755" Y="-28.395534082825" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.386207875355" Y="-27.78279902495" />
                  <Point X="-3.944785052845" Y="-27.261917380169" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.283198223787" Y="-28.941268273997" />
                  <Point X="-2.777879384553" Y="-28.479970628755" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.472239452919" Y="-27.832469389791" />
                  <Point X="-4.021201672837" Y="-27.320553837999" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.363686314239" Y="-28.996108024201" />
                  <Point X="-2.826628797351" Y="-28.564407174685" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.558271030484" Y="-27.882139754633" />
                  <Point X="-4.097618292829" Y="-27.379190295829" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.4286962216" Y="-29.065381413642" />
                  <Point X="-2.875378210149" Y="-28.648843720615" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.644302608049" Y="-27.931810119474" />
                  <Point X="-4.174034747912" Y="-27.437826907439" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.48679579642" Y="-29.141098792429" />
                  <Point X="-2.924127622947" Y="-28.733280266545" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.730334185613" Y="-27.981480484315" />
                  <Point X="-4.023264427802" Y="-27.708318614287" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.849950862825" Y="-28.932347323203" />
                  <Point X="-2.972877082807" Y="-28.817716768588" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="-24.998374023438" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978486328125" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.79504309082" Y="-29.7444453125" />
                  <Point X="0.471539031982" Y="-28.537111328125" />
                  <Point X="0.464318817139" Y="-28.52154296875" />
                  <Point X="0.332094451904" Y="-28.331033203125" />
                  <Point X="0.300591186523" Y="-28.285642578125" />
                  <Point X="0.274336517334" Y="-28.266400390625" />
                  <Point X="0.069727012634" Y="-28.2028984375" />
                  <Point X="0.020977386475" Y="-28.187767578125" />
                  <Point X="-0.008663691521" Y="-28.187765625" />
                  <Point X="-0.213273040771" Y="-28.25126953125" />
                  <Point X="-0.262022827148" Y="-28.2663984375" />
                  <Point X="-0.288278564453" Y="-28.285642578125" />
                  <Point X="-0.42050289917" Y="-28.476154296875" />
                  <Point X="-0.452006347656" Y="-28.52154296875" />
                  <Point X="-0.459227142334" Y="-28.537111328125" />
                  <Point X="-0.536304321289" Y="-28.824767578125" />
                  <Point X="-0.84774407959" Y="-29.987076171875" />
                  <Point X="-1.044045166016" Y="-29.948974609375" />
                  <Point X="-1.100232666016" Y="-29.938068359375" />
                  <Point X="-1.337588378906" Y="-29.876998046875" />
                  <Point X="-1.351589599609" Y="-29.873396484375" />
                  <Point X="-1.348092529297" Y="-29.846833984375" />
                  <Point X="-1.309150146484" Y="-29.5510390625" />
                  <Point X="-1.309683227539" Y="-29.534759765625" />
                  <Point X="-1.358564453125" Y="-29.289017578125" />
                  <Point X="-1.370210693359" Y="-29.230466796875" />
                  <Point X="-1.386282470703" Y="-29.20262890625" />
                  <Point X="-1.574660888672" Y="-29.03742578125" />
                  <Point X="-1.619543334961" Y="-28.998064453125" />
                  <Point X="-1.649240722656" Y="-28.985763671875" />
                  <Point X="-1.899260742188" Y="-28.969376953125" />
                  <Point X="-1.958829833984" Y="-28.96547265625" />
                  <Point X="-1.989878540039" Y="-28.973791015625" />
                  <Point X="-2.198208740234" Y="-29.1129921875" />
                  <Point X="-2.247844970703" Y="-29.146158203125" />
                  <Point X="-2.259734130859" Y="-29.157294921875" />
                  <Point X="-2.303006835938" Y="-29.213689453125" />
                  <Point X="-2.457094482422" Y="-29.4145" />
                  <Point X="-2.773459716797" Y="-29.218615234375" />
                  <Point X="-2.855839599609" Y="-29.167607421875" />
                  <Point X="-3.184482666016" Y="-28.9145625" />
                  <Point X="-3.228580810547" Y="-28.880609375" />
                  <Point X="-3.107229492188" Y="-28.670421875" />
                  <Point X="-2.506033691406" Y="-27.629119140625" />
                  <Point X="-2.499762939453" Y="-27.597591796875" />
                  <Point X="-2.513980957031" Y="-27.56876171875" />
                  <Point X="-2.531329589844" Y="-27.5514140625" />
                  <Point X="-2.560158203125" Y="-27.53719921875" />
                  <Point X="-2.59168359375" Y="-27.543470703125" />
                  <Point X="-2.83978125" Y="-27.6867109375" />
                  <Point X="-3.842958984375" Y="-28.26589453125" />
                  <Point X="-4.096623046875" Y="-27.9326328125" />
                  <Point X="-4.161701660156" Y="-27.8471328125" />
                  <Point X="-4.397318847656" Y="-27.4520390625" />
                  <Point X="-4.43101953125" Y="-27.39552734375" />
                  <Point X="-4.215684570312" Y="-27.230294921875" />
                  <Point X="-3.163786865234" Y="-26.423146484375" />
                  <Point X="-3.145821533203" Y="-26.396013671875" />
                  <Point X="-3.139599365234" Y="-26.371990234375" />
                  <Point X="-3.138116943359" Y="-26.366265625" />
                  <Point X="-3.140326660156" Y="-26.334595703125" />
                  <Point X="-3.161158203125" Y="-26.310640625" />
                  <Point X="-3.182545166016" Y="-26.298052734375" />
                  <Point X="-3.187648193359" Y="-26.29505078125" />
                  <Point X="-3.219529296875" Y="-26.288572265625" />
                  <Point X="-3.532729003906" Y="-26.3298046875" />
                  <Point X="-4.803283691406" Y="-26.497076171875" />
                  <Point X="-4.901939453125" Y="-26.110841796875" />
                  <Point X="-4.927393066406" Y="-26.011193359375" />
                  <Point X="-4.989730957031" Y="-25.57533203125" />
                  <Point X="-4.998396484375" Y="-25.514744140625" />
                  <Point X="-4.755733886719" Y="-25.44972265625" />
                  <Point X="-3.557463867188" Y="-25.128646484375" />
                  <Point X="-3.541894775391" Y="-25.12142578125" />
                  <Point X="-3.519481933594" Y="-25.105869140625" />
                  <Point X="-3.514149169922" Y="-25.10216796875" />
                  <Point X="-3.494899658203" Y="-25.07591015625" />
                  <Point X="-3.487428710938" Y="-25.051837890625" />
                  <Point X="-3.485648681641" Y="-25.046103515625" />
                  <Point X="-3.485647216797" Y="-25.016462890625" />
                  <Point X="-3.493118164062" Y="-24.992390625" />
                  <Point X="-3.494898193359" Y="-24.986654296875" />
                  <Point X="-3.514144287109" Y="-24.960396484375" />
                  <Point X="-3.536552734375" Y="-24.944845703125" />
                  <Point X="-3.536552001953" Y="-24.94484375" />
                  <Point X="-3.557463867188" Y="-24.9339140625" />
                  <Point X="-3.842960449219" Y="-24.857416015625" />
                  <Point X="-4.998186523438" Y="-24.547873046875" />
                  <Point X="-4.934048828125" Y="-24.1144375" />
                  <Point X="-4.917645019531" Y="-24.003580078125" />
                  <Point X="-4.792161621094" Y="-23.5405078125" />
                  <Point X="-4.773516113281" Y="-23.471701171875" />
                  <Point X="-4.612215332031" Y="-23.4929375" />
                  <Point X="-3.753266357422" Y="-23.60601953125" />
                  <Point X="-3.731704589844" Y="-23.6041328125" />
                  <Point X="-3.68209765625" Y="-23.5884921875" />
                  <Point X="-3.670278564453" Y="-23.584765625" />
                  <Point X="-3.651534423828" Y="-23.5739453125" />
                  <Point X="-3.639120117188" Y="-23.55621484375" />
                  <Point X="-3.619215087891" Y="-23.50816015625" />
                  <Point X="-3.61447265625" Y="-23.4967109375" />
                  <Point X="-3.610714111328" Y="-23.47539453125" />
                  <Point X="-3.616315917969" Y="-23.45448828125" />
                  <Point X="-3.640333496094" Y="-23.4083515625" />
                  <Point X="-3.646055664062" Y="-23.397359375" />
                  <Point X="-3.659968261719" Y="-23.380779296875" />
                  <Point X="-3.823729736328" Y="-23.255119140625" />
                  <Point X="-4.47610546875" Y="-22.75453515625" />
                  <Point X="-4.22375390625" Y="-22.3221953125" />
                  <Point X="-4.160016113281" Y="-22.212998046875" />
                  <Point X="-3.827611083984" Y="-21.785736328125" />
                  <Point X="-3.774670898438" Y="-21.717689453125" />
                  <Point X="-3.686699707031" Y="-21.76848046875" />
                  <Point X="-3.159156982422" Y="-22.073056640625" />
                  <Point X="-3.138512695312" Y="-22.07956640625" />
                  <Point X="-3.069424072266" Y="-22.085611328125" />
                  <Point X="-3.052963378906" Y="-22.08705078125" />
                  <Point X="-3.031506103516" Y="-22.084224609375" />
                  <Point X="-3.013253173828" Y="-22.072595703125" />
                  <Point X="-2.964213623047" Y="-22.023556640625" />
                  <Point X="-2.952529541016" Y="-22.011873046875" />
                  <Point X="-2.940899658203" Y="-21.993619140625" />
                  <Point X="-2.93807421875" Y="-21.97216015625" />
                  <Point X="-2.944118652344" Y="-21.9030703125" />
                  <Point X="-2.945558837891" Y="-21.886609375" />
                  <Point X="-2.952067382812" Y="-21.865966796875" />
                  <Point X="-3.024635009766" Y="-21.74027734375" />
                  <Point X="-3.307278564453" Y="-21.250724609375" />
                  <Point X="-2.863887939453" Y="-20.91078125" />
                  <Point X="-2.752871337891" Y="-20.825666015625" />
                  <Point X="-2.229351074219" Y="-20.53480859375" />
                  <Point X="-2.141548583984" Y="-20.48602734375" />
                  <Point X="-2.129323730469" Y="-20.501958984375" />
                  <Point X="-1.967826904297" Y="-20.71242578125" />
                  <Point X="-1.951246459961" Y="-20.72633984375" />
                  <Point X="-1.874351196289" Y="-20.76637109375" />
                  <Point X="-1.856030395508" Y="-20.775908203125" />
                  <Point X="-1.835123779297" Y="-20.781509765625" />
                  <Point X="-1.81380859375" Y="-20.77775" />
                  <Point X="-1.733717163086" Y="-20.74457421875" />
                  <Point X="-1.714634765625" Y="-20.736669921875" />
                  <Point X="-1.696905151367" Y="-20.724255859375" />
                  <Point X="-1.686083251953" Y="-20.70551171875" />
                  <Point X="-1.660015014648" Y="-20.622833984375" />
                  <Point X="-1.653803955078" Y="-20.603134765625" />
                  <Point X="-1.651917602539" Y="-20.58157421875" />
                  <Point X="-1.660167724609" Y="-20.518908203125" />
                  <Point X="-1.689137573242" Y="-20.298859375" />
                  <Point X="-1.111806884766" Y="-20.13699609375" />
                  <Point X="-0.968083679199" Y="-20.096703125" />
                  <Point X="-0.333421356201" Y="-20.022423828125" />
                  <Point X="-0.22420022583" Y="-20.009640625" />
                  <Point X="-0.19820211792" Y="-20.10666796875" />
                  <Point X="-0.042140380859" Y="-20.68909765625" />
                  <Point X="-0.024282104492" Y="-20.71582421875" />
                  <Point X="0.006155934334" Y="-20.72615625" />
                  <Point X="0.036594051361" Y="-20.71582421875" />
                  <Point X="0.054452251434" Y="-20.68909765625" />
                  <Point X="0.091635101318" Y="-20.550330078125" />
                  <Point X="0.2366484375" Y="-20.009130859375" />
                  <Point X="0.734721374512" Y="-20.06129296875" />
                  <Point X="0.860209594727" Y="-20.074435546875" />
                  <Point X="1.385286010742" Y="-20.201205078125" />
                  <Point X="1.508457275391" Y="-20.230943359375" />
                  <Point X="1.850921875" Y="-20.355158203125" />
                  <Point X="1.931042114258" Y="-20.38421875" />
                  <Point X="2.261514160156" Y="-20.538767578125" />
                  <Point X="2.338686279297" Y="-20.574859375" />
                  <Point X="2.657954345703" Y="-20.760865234375" />
                  <Point X="2.732520019531" Y="-20.804306640625" />
                  <Point X="3.033625732422" Y="-21.0184375" />
                  <Point X="3.068739990234" Y="-21.043408203125" />
                  <Point X="2.925692626953" Y="-21.291173828125" />
                  <Point X="2.229853515625" Y="-22.49640234375" />
                  <Point X="2.224851806641" Y="-22.508486328125" />
                  <Point X="2.207513183594" Y="-22.57332421875" />
                  <Point X="2.202044677734" Y="-22.60767578125" />
                  <Point X="2.208805419922" Y="-22.6637421875" />
                  <Point X="2.210416015625" Y="-22.677099609375" />
                  <Point X="2.218681884766" Y="-22.6991875" />
                  <Point X="2.253374023438" Y="-22.750314453125" />
                  <Point X="2.274939941406" Y="-22.775796875" />
                  <Point X="2.326067138672" Y="-22.810490234375" />
                  <Point X="2.338248535156" Y="-22.818755859375" />
                  <Point X="2.360334960938" Y="-22.827021484375" />
                  <Point X="2.416401611328" Y="-22.83378125" />
                  <Point X="2.448663818359" Y="-22.8340546875" />
                  <Point X="2.513501953125" Y="-22.81671484375" />
                  <Point X="2.541033935547" Y="-22.80758203125" />
                  <Point X="2.828188476562" Y="-22.64179296875" />
                  <Point X="3.994247802734" Y="-21.9685703125" />
                  <Point X="4.158359863281" Y="-22.1966484375" />
                  <Point X="4.202591308594" Y="-22.258119140625" />
                  <Point X="4.370451660156" Y="-22.535509765625" />
                  <Point X="4.387512695312" Y="-22.563705078125" />
                  <Point X="4.204753417969" Y="-22.70394140625" />
                  <Point X="3.288616210938" Y="-23.40691796875" />
                  <Point X="3.279371826172" Y="-23.416166015625" />
                  <Point X="3.232707763672" Y="-23.47704296875" />
                  <Point X="3.213119628906" Y="-23.5085" />
                  <Point X="3.195737060547" Y="-23.57065625" />
                  <Point X="3.191595703125" Y="-23.58546484375" />
                  <Point X="3.190779541016" Y="-23.60903515625" />
                  <Point X="3.205048828125" Y="-23.67819140625" />
                  <Point X="3.215646972656" Y="-23.712046875" />
                  <Point X="3.254457519531" Y="-23.771037109375" />
                  <Point X="3.263704589844" Y="-23.785091796875" />
                  <Point X="3.280948242188" Y="-23.801181640625" />
                  <Point X="3.337190185547" Y="-23.832841796875" />
                  <Point X="3.368565429688" Y="-23.846380859375" />
                  <Point X="3.444608398438" Y="-23.8564296875" />
                  <Point X="3.475803710938" Y="-23.858828125" />
                  <Point X="3.748581298828" Y="-23.822916015625" />
                  <Point X="4.848975585938" Y="-23.678046875" />
                  <Point X="4.920190917969" Y="-23.970580078125" />
                  <Point X="4.939188476562" Y="-24.04861328125" />
                  <Point X="4.9920859375" Y="-24.388365234375" />
                  <Point X="4.997858398438" Y="-24.425443359375" />
                  <Point X="4.791080566406" Y="-24.480849609375" />
                  <Point X="3.741167724609" Y="-24.762173828125" />
                  <Point X="3.729090332031" Y="-24.7671796875" />
                  <Point X="3.654380615234" Y="-24.81036328125" />
                  <Point X="3.636580322266" Y="-24.820650390625" />
                  <Point X="3.622264160156" Y="-24.83307421875" />
                  <Point X="3.577438232422" Y="-24.890193359375" />
                  <Point X="3.566758300781" Y="-24.903802734375" />
                  <Point X="3.556985107422" Y="-24.925265625" />
                  <Point X="3.54204296875" Y="-25.003287109375" />
                  <Point X="3.538483154297" Y="-25.0406875" />
                  <Point X="3.553425292969" Y="-25.118708984375" />
                  <Point X="3.556985351562" Y="-25.137296875" />
                  <Point X="3.566758300781" Y="-25.1587578125" />
                  <Point X="3.611584228516" Y="-25.215876953125" />
                  <Point X="3.636576416016" Y="-25.241908203125" />
                  <Point X="3.711286376953" Y="-25.285091796875" />
                  <Point X="3.729086425781" Y="-25.295380859375" />
                  <Point X="3.741167724609" Y="-25.300388671875" />
                  <Point X="3.991317138672" Y="-25.367416015625" />
                  <Point X="4.998067871094" Y="-25.637173828125" />
                  <Point X="4.9590390625" Y="-25.89604296875" />
                  <Point X="4.948430664062" Y="-25.96640625" />
                  <Point X="4.880662109375" Y="-26.26337890625" />
                  <Point X="4.874546386719" Y="-26.290177734375" />
                  <Point X="4.6295859375" Y="-26.2579296875" />
                  <Point X="3.411982177734" Y="-26.09762890625" />
                  <Point X="3.394838134766" Y="-26.098341796875" />
                  <Point X="3.248209228516" Y="-26.130212890625" />
                  <Point X="3.213273925781" Y="-26.1378046875" />
                  <Point X="3.185445068359" Y="-26.154697265625" />
                  <Point X="3.096817138672" Y="-26.2612890625" />
                  <Point X="3.075700927734" Y="-26.286685546875" />
                  <Point X="3.064357666016" Y="-26.3140703125" />
                  <Point X="3.051655029297" Y="-26.452111328125" />
                  <Point X="3.048628662109" Y="-26.485001953125" />
                  <Point X="3.056360595703" Y="-26.516623046875" />
                  <Point X="3.137507080078" Y="-26.64283984375" />
                  <Point X="3.156840820312" Y="-26.672912109375" />
                  <Point X="3.168460693359" Y="-26.685541015625" />
                  <Point X="3.400600830078" Y="-26.86366796875" />
                  <Point X="4.33907421875" Y="-27.583783203125" />
                  <Point X="4.234031738281" Y="-27.7537578125" />
                  <Point X="4.204129882813" Y="-27.80214453125" />
                  <Point X="4.063979248047" Y="-28.001279296875" />
                  <Point X="4.056688232422" Y="-28.011638671875" />
                  <Point X="3.837895019531" Y="-27.885318359375" />
                  <Point X="2.753454589844" Y="-27.259216796875" />
                  <Point X="2.737339355469" Y="-27.2533125" />
                  <Point X="2.562827636719" Y="-27.221794921875" />
                  <Point X="2.521249023438" Y="-27.214287109375" />
                  <Point X="2.489077148438" Y="-27.21924609375" />
                  <Point X="2.344100830078" Y="-27.295544921875" />
                  <Point X="2.309559082031" Y="-27.313724609375" />
                  <Point X="2.288600097656" Y="-27.33468359375" />
                  <Point X="2.212300048828" Y="-27.47966015625" />
                  <Point X="2.19412109375" Y="-27.514201171875" />
                  <Point X="2.189163085938" Y="-27.546375" />
                  <Point X="2.2206796875" Y="-27.72088671875" />
                  <Point X="2.228188720703" Y="-27.762466796875" />
                  <Point X="2.234091552734" Y="-27.778580078125" />
                  <Point X="2.383264892578" Y="-28.036955078125" />
                  <Point X="2.986673583984" Y="-29.082087890625" />
                  <Point X="2.870780273438" Y="-29.164869140625" />
                  <Point X="2.835296630859" Y="-29.190212890625" />
                  <Point X="2.679775634766" Y="-29.290880859375" />
                  <Point X="2.510705810547" Y="-29.07054296875" />
                  <Point X="1.683177490234" Y="-27.992087890625" />
                  <Point X="1.670548583984" Y="-27.980466796875" />
                  <Point X="1.498432861328" Y="-27.8698125" />
                  <Point X="1.457425170898" Y="-27.84344921875" />
                  <Point X="1.425804931641" Y="-27.83571875" />
                  <Point X="1.237567138672" Y="-27.8530390625" />
                  <Point X="1.192718139648" Y="-27.857166015625" />
                  <Point X="1.165332763672" Y="-27.868509765625" />
                  <Point X="1.01998034668" Y="-27.9893671875" />
                  <Point X="0.985348937988" Y="-28.018162109375" />
                  <Point X="0.968456726074" Y="-28.04598828125" />
                  <Point X="0.924997070312" Y="-28.245935546875" />
                  <Point X="0.91464251709" Y="-28.29357421875" />
                  <Point X="0.91392956543" Y="-28.31071875" />
                  <Point X="0.956204040527" Y="-28.631826171875" />
                  <Point X="1.127642456055" Y="-29.934029296875" />
                  <Point X="1.027911499023" Y="-29.955890625" />
                  <Point X="0.994364929199" Y="-29.963244140625" />
                  <Point X="0.860200683594" Y="-29.9876171875" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#198" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-25.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.156061723514" Y="4.937554784567" Z="2.05" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="2.05" />
                  <Point X="-0.345389572826" Y="5.058516680955" Z="2.05" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="2.05" />
                  <Point X="-1.131460266896" Y="4.942428104167" Z="2.05" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="2.05" />
                  <Point X="-1.71589601634" Y="4.50584653185" Z="2.05" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="2.05" />
                  <Point X="-1.713856853068" Y="4.423481960811" Z="2.05" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="2.05" />
                  <Point X="-1.759007531452" Y="4.332899850776" Z="2.05" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="2.05" />
                  <Point X="-1.857419911193" Y="4.309261883632" Z="2.05" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="2.05" />
                  <Point X="-2.095811966217" Y="4.559758099315" Z="2.05" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="2.05" />
                  <Point X="-2.259789677052" Y="4.540178315111" Z="2.05" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="2.05" />
                  <Point X="-2.900466297684" Y="4.160179403398" Z="2.05" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="2.05" />
                  <Point X="-3.074092346887" Y="3.266003777046" Z="2.05" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="2.05" />
                  <Point X="-3.000084535351" Y="3.123852070152" Z="2.05" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="2.05" />
                  <Point X="-3.005724034164" Y="3.043079533723" Z="2.05" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="2.05" />
                  <Point X="-3.071224395455" Y="2.995480163822" Z="2.05" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="2.05" />
                  <Point X="-3.667855571251" Y="3.306101601859" Z="2.05" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="2.05" />
                  <Point X="-3.873230399871" Y="3.276246752366" Z="2.05" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="2.05" />
                  <Point X="-4.273091904677" Y="2.734290693657" Z="2.05" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="2.05" />
                  <Point X="-3.860323746639" Y="1.736492692725" Z="2.05" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="2.05" />
                  <Point X="-3.690839944027" Y="1.599841595554" Z="2.05" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="2.05" />
                  <Point X="-3.671564937153" Y="1.542254963542" Z="2.05" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="2.05" />
                  <Point X="-3.703289122963" Y="1.490473529865" Z="2.05" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="2.05" />
                  <Point X="-4.611845064803" Y="1.587915349362" Z="2.05" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="2.05" />
                  <Point X="-4.8465768165" Y="1.503850332546" Z="2.05" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="2.05" />
                  <Point X="-4.98966622695" Y="0.924162146069" Z="2.05" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="2.05" />
                  <Point X="-3.862057218618" Y="0.125567585289" Z="2.05" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="2.05" />
                  <Point X="-3.571220711147" Y="0.045362750081" Z="2.05" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="2.05" />
                  <Point X="-3.547027820652" Y="0.024071695047" Z="2.05" />
                  <Point X="-3.539556741714" Y="0" Z="2.05" />
                  <Point X="-3.541336780483" Y="-0.005735256013" Z="2.05" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="2.05" />
                  <Point X="-3.554147883976" Y="-0.033513233053" Z="2.05" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="2.05" />
                  <Point X="-4.774830570155" Y="-0.370144471467" Z="2.05" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="2.05" />
                  <Point X="-5.045383205101" Y="-0.551128833465" Z="2.05" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="2.05" />
                  <Point X="-4.956539992345" Y="-1.091936418944" Z="2.05" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="2.05" />
                  <Point X="-3.532358686375" Y="-1.348096835761" Z="2.05" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="2.05" />
                  <Point X="-3.214063284898" Y="-1.309862358117" Z="2.05" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="2.05" />
                  <Point X="-3.194158753913" Y="-1.328173594577" Z="2.05" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="2.05" />
                  <Point X="-4.252278371728" Y="-2.159346529909" Z="2.05" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="2.05" />
                  <Point X="-4.446418477542" Y="-2.446367620021" Z="2.05" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="2.05" />
                  <Point X="-4.142565118095" Y="-2.931635083954" Z="2.05" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="2.05" />
                  <Point X="-2.820937184329" Y="-2.698730234309" Z="2.05" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="2.05" />
                  <Point X="-2.569501409736" Y="-2.558829059395" Z="2.05" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="2.05" />
                  <Point X="-3.156686778859" Y="-3.614141198163" Z="2.05" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="2.05" />
                  <Point X="-3.22114228452" Y="-3.922899753615" Z="2.05" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="2.05" />
                  <Point X="-2.805937203153" Y="-4.229846176627" Z="2.05" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="2.05" />
                  <Point X="-2.269495174205" Y="-4.212846497244" Z="2.05" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="2.05" />
                  <Point X="-2.176586114454" Y="-4.123286306883" Z="2.05" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="2.05" />
                  <Point X="-1.908686904358" Y="-3.987988736587" Z="2.05" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="2.05" />
                  <Point X="-1.61378433819" Y="-4.043736893921" Z="2.05" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="2.05" />
                  <Point X="-1.413759689477" Y="-4.26749030341" Z="2.05" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="2.05" />
                  <Point X="-1.403820778501" Y="-4.809027807669" Z="2.05" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="2.05" />
                  <Point X="-1.356202952323" Y="-4.894142121602" Z="2.05" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="2.05" />
                  <Point X="-1.059729054718" Y="-4.966778062887" Z="2.05" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="2.05" />
                  <Point X="-0.494163979497" Y="-3.806429123842" Z="2.05" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="2.05" />
                  <Point X="-0.385583380809" Y="-3.473382686698" Z="2.05" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="2.05" />
                  <Point X="-0.204609404142" Y="-3.267742576692" Z="2.05" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="2.05" />
                  <Point X="0.048749675218" Y="-3.219369468596" Z="2.05" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="2.05" />
                  <Point X="0.284862478624" Y="-3.32826306907" Z="2.05" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="2.05" />
                  <Point X="0.740590880904" Y="-4.726106905974" Z="2.05" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="2.05" />
                  <Point X="0.852368308019" Y="-5.007459185956" Z="2.05" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="2.05" />
                  <Point X="1.032460390834" Y="-4.973449906005" Z="2.05" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="2.05" />
                  <Point X="0.999620385556" Y="-3.594021136898" Z="2.05" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="2.05" />
                  <Point X="0.96770036171" Y="-3.225274513428" Z="2.05" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="2.05" />
                  <Point X="1.04579129374" Y="-2.996531241797" Z="2.05" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="2.05" />
                  <Point X="1.235992750459" Y="-2.871548394122" Z="2.05" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="2.05" />
                  <Point X="1.465238256601" Y="-2.880590815793" Z="2.05" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="2.05" />
                  <Point X="2.464882077291" Y="-4.069700770901" Z="2.05" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="2.05" />
                  <Point X="2.699610938868" Y="-4.302335992734" Z="2.05" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="2.05" />
                  <Point X="2.893685697903" Y="-4.174275675276" Z="2.05" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="2.05" />
                  <Point X="2.420410310898" Y="-2.980674446849" Z="2.05" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="2.05" />
                  <Point X="2.263727813843" Y="-2.680719931401" Z="2.05" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="2.05" />
                  <Point X="2.250390152276" Y="-2.471666520364" Z="2.05" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="2.05" />
                  <Point X="2.361231858008" Y="-2.308511157691" Z="2.05" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="2.05" />
                  <Point X="2.547786872111" Y="-2.239720195648" Z="2.05" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="2.05" />
                  <Point X="3.806739150258" Y="-2.897339189377" Z="2.05" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="2.05" />
                  <Point X="4.098711750201" Y="-2.998776213844" Z="2.05" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="2.05" />
                  <Point X="4.270409547741" Y="-2.748763045649" Z="2.05" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="2.05" />
                  <Point X="3.424882964217" Y="-1.792720447032" Z="2.05" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="2.05" />
                  <Point X="3.173409067564" Y="-1.584520640644" Z="2.05" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="2.05" />
                  <Point X="3.095288966916" Y="-1.425413228641" Z="2.05" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="2.05" />
                  <Point X="3.129107684116" Y="-1.261975965772" Z="2.05" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="2.05" />
                  <Point X="3.252670934429" Y="-1.147790828024" Z="2.05" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="2.05" />
                  <Point X="4.616903773121" Y="-1.276221008595" Z="2.05" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="2.05" />
                  <Point X="4.923252518858" Y="-1.243222568524" Z="2.05" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="2.05" />
                  <Point X="5.00232450809" Y="-0.872217453006" Z="2.05" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="2.05" />
                  <Point X="3.998102095533" Y="-0.287837962411" Z="2.05" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="2.05" />
                  <Point X="3.730152541352" Y="-0.210521826315" Z="2.05" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="2.05" />
                  <Point X="3.644762619705" Y="-0.153729256581" Z="2.05" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="2.05" />
                  <Point X="3.596376692047" Y="-0.078021372973" Z="2.05" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="2.05" />
                  <Point X="3.584994758377" Y="0.018589158214" Z="2.05" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="2.05" />
                  <Point X="3.610616818695" Y="0.110219481994" Z="2.05" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="2.05" />
                  <Point X="3.673242873001" Y="0.177627017927" Z="2.05" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="2.05" />
                  <Point X="4.797865721773" Y="0.502133970358" Z="2.05" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="2.05" />
                  <Point X="5.035334646635" Y="0.65060589122" Z="2.05" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="2.05" />
                  <Point X="4.962616133929" Y="1.072527370675" Z="2.05" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="2.05" />
                  <Point X="3.735898937029" Y="1.25793593636" Z="2.05" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="2.05" />
                  <Point X="3.445003617907" Y="1.224418570627" Z="2.05" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="2.05" />
                  <Point X="3.355560553918" Y="1.242011655283" Z="2.05" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="2.05" />
                  <Point X="3.290071635506" Y="1.287725937644" Z="2.05" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="2.05" />
                  <Point X="3.247861251184" Y="1.363193300112" Z="2.05" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="2.05" />
                  <Point X="3.237733529699" Y="1.447158203649" Z="2.05" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="2.05" />
                  <Point X="3.266234048747" Y="1.523818091726" Z="2.05" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="2.05" />
                  <Point X="4.22903550732" Y="2.287671988631" Z="2.05" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="2.05" />
                  <Point X="4.407072872855" Y="2.521656713079" Z="2.05" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="2.05" />
                  <Point X="4.192789270756" Y="2.863835735763" Z="2.05" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="2.05" />
                  <Point X="2.797032414539" Y="2.43278748303" Z="2.05" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="2.05" />
                  <Point X="2.494429769152" Y="2.262867731022" Z="2.05" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="2.05" />
                  <Point X="2.416233382731" Y="2.247139935107" Z="2.05" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="2.05" />
                  <Point X="2.347985344505" Y="2.262166133984" Z="2.05" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="2.05" />
                  <Point X="2.288592466251" Y="2.309039515877" Z="2.05" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="2.05" />
                  <Point X="2.252289767652" Y="2.373525061101" Z="2.05" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="2.05" />
                  <Point X="2.249660211635" Y="2.44503980673" Z="2.05" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="2.05" />
                  <Point X="2.96283786983" Y="3.715106667416" Z="2.05" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="2.05" />
                  <Point X="3.056446797409" Y="4.053591439075" Z="2.05" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="2.05" />
                  <Point X="2.676968282912" Y="4.313618298063" Z="2.05" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="2.05" />
                  <Point X="2.276540076682" Y="4.537803183401" Z="2.05" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="2.05" />
                  <Point X="1.861814224208" Y="4.723127164625" Z="2.05" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="2.05" />
                  <Point X="1.390864807794" Y="4.878678580506" Z="2.05" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="2.05" />
                  <Point X="0.733773678584" Y="5.019715197532" Z="2.05" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="2.05" />
                  <Point X="0.037182838039" Y="4.493892721383" Z="2.05" />
                  <Point X="0" Y="4.355124473572" Z="2.05" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>