<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#157" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1610" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="-25.000000953674" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="-24.995284179688" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.721211425781" Y="-29.101849609375" />
                  <Point X="0.563302062988" Y="-28.5125234375" />
                  <Point X="0.557721313477" Y="-28.497140625" />
                  <Point X="0.542363037109" Y="-28.467375" />
                  <Point X="0.477821594238" Y="-28.3743828125" />
                  <Point X="0.378635528564" Y="-28.231474609375" />
                  <Point X="0.35674887085" Y="-28.209017578125" />
                  <Point X="0.330493835449" Y="-28.189775390625" />
                  <Point X="0.302494415283" Y="-28.175669921875" />
                  <Point X="0.202620117188" Y="-28.144673828125" />
                  <Point X="0.049135276794" Y="-28.097037109375" />
                  <Point X="0.020983165741" Y="-28.092767578125" />
                  <Point X="-0.008658161163" Y="-28.092765625" />
                  <Point X="-0.036824542999" Y="-28.09703515625" />
                  <Point X="-0.13669883728" Y="-28.128033203125" />
                  <Point X="-0.290183685303" Y="-28.17566796875" />
                  <Point X="-0.318184295654" Y="-28.189775390625" />
                  <Point X="-0.344439788818" Y="-28.20901953125" />
                  <Point X="-0.366323577881" Y="-28.231474609375" />
                  <Point X="-0.43086517334" Y="-28.324466796875" />
                  <Point X="-0.530051208496" Y="-28.467375" />
                  <Point X="-0.538189147949" Y="-28.481572265625" />
                  <Point X="-0.550990112305" Y="-28.5125234375" />
                  <Point X="-0.793661865234" Y="-29.4181875" />
                  <Point X="-0.916584533691" Y="-29.87694140625" />
                  <Point X="-1.079328735352" Y="-29.8453515625" />
                  <Point X="-1.1916796875" Y="-29.8164453125" />
                  <Point X="-1.246417602539" Y="-29.802361328125" />
                  <Point X="-1.233971557617" Y="-29.70782421875" />
                  <Point X="-1.214963012695" Y="-29.56344140625" />
                  <Point X="-1.214201171875" Y="-29.547931640625" />
                  <Point X="-1.216508544922" Y="-29.5162265625" />
                  <Point X="-1.240368530273" Y="-29.3962734375" />
                  <Point X="-1.277036010742" Y="-29.21193359375" />
                  <Point X="-1.287937866211" Y="-29.182966796875" />
                  <Point X="-1.304009765625" Y="-29.15512890625" />
                  <Point X="-1.323645019531" Y="-29.131203125" />
                  <Point X="-1.415596679688" Y="-29.050564453125" />
                  <Point X="-1.556905883789" Y="-28.926638671875" />
                  <Point X="-1.583189086914" Y="-28.910294921875" />
                  <Point X="-1.612886108398" Y="-28.897994140625" />
                  <Point X="-1.643027954102" Y="-28.890966796875" />
                  <Point X="-1.765068237305" Y="-28.88296875" />
                  <Point X="-1.952616943359" Y="-28.87067578125" />
                  <Point X="-1.98341418457" Y="-28.873708984375" />
                  <Point X="-2.014463134766" Y="-28.88202734375" />
                  <Point X="-2.042657592773" Y="-28.89480078125" />
                  <Point X="-2.144348144531" Y="-28.962748046875" />
                  <Point X="-2.300624023438" Y="-29.06716796875" />
                  <Point X="-2.312790283203" Y="-29.07682421875" />
                  <Point X="-2.335102539062" Y="-29.099462890625" />
                  <Point X="-2.471343994141" Y="-29.277015625" />
                  <Point X="-2.480147949219" Y="-29.28848828125" />
                  <Point X="-2.541824462891" Y="-29.25030078125" />
                  <Point X="-2.801724853516" Y="-29.089376953125" />
                  <Point X="-2.957263916016" Y="-28.9696171875" />
                  <Point X="-3.104722167969" Y="-28.856080078125" />
                  <Point X="-2.717218261719" Y="-28.18490234375" />
                  <Point X="-2.423761230469" Y="-27.676619140625" />
                  <Point X="-2.412859130859" Y="-27.64765234375" />
                  <Point X="-2.406588134766" Y="-27.616126953125" />
                  <Point X="-2.405575195312" Y="-27.5851953125" />
                  <Point X="-2.414559326172" Y="-27.555578125" />
                  <Point X="-2.428776367188" Y="-27.526748046875" />
                  <Point X="-2.446806396484" Y="-27.5015859375" />
                  <Point X="-2.464155029297" Y="-27.48423828125" />
                  <Point X="-2.489311035156" Y="-27.466212890625" />
                  <Point X="-2.518139892578" Y="-27.45199609375" />
                  <Point X="-2.547758056641" Y="-27.44301171875" />
                  <Point X="-2.578692138672" Y="-27.444025390625" />
                  <Point X="-2.610218505859" Y="-27.450296875" />
                  <Point X="-2.63918359375" Y="-27.46119921875" />
                  <Point X="-3.420300537109" Y="-27.912177734375" />
                  <Point X="-3.818022949219" Y="-28.141802734375" />
                  <Point X="-3.877537109375" Y="-28.06361328125" />
                  <Point X="-4.082862548828" Y="-27.793857421875" />
                  <Point X="-4.19437890625" Y="-27.606861328125" />
                  <Point X="-4.306142578125" Y="-27.419451171875" />
                  <Point X="-3.619408935547" Y="-26.892501953125" />
                  <Point X="-3.105954589844" Y="-26.498515625" />
                  <Point X="-3.084577636719" Y="-26.475595703125" />
                  <Point X="-3.066612792969" Y="-26.44846484375" />
                  <Point X="-3.053856445312" Y="-26.419833984375" />
                  <Point X="-3.046151855469" Y="-26.3900859375" />
                  <Point X="-3.04334765625" Y="-26.359658203125" />
                  <Point X="-3.045556884766" Y="-26.327984375" />
                  <Point X="-3.052559326172" Y="-26.298236328125" />
                  <Point X="-3.068644775391" Y="-26.27225" />
                  <Point X="-3.089479003906" Y="-26.248294921875" />
                  <Point X="-3.112975830078" Y="-26.228765625" />
                  <Point X="-3.139458496094" Y="-26.2131796875" />
                  <Point X="-3.168722412109" Y="-26.20195703125" />
                  <Point X="-3.200607910156" Y="-26.1954765625" />
                  <Point X="-3.231928955078" Y="-26.194384765625" />
                  <Point X="-4.218015625" Y="-26.324205078125" />
                  <Point X="-4.7321015625" Y="-26.391884765625" />
                  <Point X="-4.7537734375" Y="-26.307041015625" />
                  <Point X="-4.834078125" Y="-25.99265234375" />
                  <Point X="-4.863582519531" Y="-25.786359375" />
                  <Point X="-4.892424316406" Y="-25.584701171875" />
                  <Point X="-4.117777832031" Y="-25.377134765625" />
                  <Point X="-3.532875976562" Y="-25.22041015625" />
                  <Point X="-3.511116210938" Y="-25.21157421875" />
                  <Point X="-3.484609619141" Y="-25.196759765625" />
                  <Point X="-3.476793212891" Y="-25.19187890625" />
                  <Point X="-3.45998046875" Y="-25.1802109375" />
                  <Point X="-3.436024658203" Y="-25.158685546875" />
                  <Point X="-3.414675048828" Y="-25.127287109375" />
                  <Point X="-3.403602294922" Y="-25.100798828125" />
                  <Point X="-3.398850585938" Y="-25.086224609375" />
                  <Point X="-3.390999267578" Y="-25.05334765625" />
                  <Point X="-3.388404052734" Y="-25.032048828125" />
                  <Point X="-3.390654785156" Y="-25.0107109375" />
                  <Point X="-3.396548095703" Y="-24.984140625" />
                  <Point X="-3.401007080078" Y="-24.969634765625" />
                  <Point X="-3.414038085938" Y="-24.9368359375" />
                  <Point X="-3.425299804688" Y="-24.916306640625" />
                  <Point X="-3.441241210938" Y="-24.899154296875" />
                  <Point X="-3.464002685547" Y="-24.880044921875" />
                  <Point X="-3.470917724609" Y="-24.874759765625" />
                  <Point X="-3.487725341797" Y="-24.86309375" />
                  <Point X="-3.501916503906" Y="-24.854958984375" />
                  <Point X="-3.532875976562" Y="-24.842150390625" />
                  <Point X="-4.431740234375" Y="-24.60130078125" />
                  <Point X="-4.89181640625" Y="-24.4780234375" />
                  <Point X="-4.8762421875" Y="-24.372775390625" />
                  <Point X="-4.82448828125" Y="-24.02302734375" />
                  <Point X="-4.765094726562" Y="-23.80384765625" />
                  <Point X="-4.70355078125" Y="-23.576732421875" />
                  <Point X="-4.184938476563" Y="-23.645009765625" />
                  <Point X="-3.765666259766" Y="-23.70020703125" />
                  <Point X="-3.744984619141" Y="-23.700658203125" />
                  <Point X="-3.723423828125" Y="-23.698771484375" />
                  <Point X="-3.703137939453" Y="-23.694736328125" />
                  <Point X="-3.678923828125" Y="-23.6871015625" />
                  <Point X="-3.641711914063" Y="-23.675369140625" />
                  <Point X="-3.62278515625" Y="-23.667041015625" />
                  <Point X="-3.604040771484" Y="-23.656220703125" />
                  <Point X="-3.587355957031" Y="-23.64398828125" />
                  <Point X="-3.573715087891" Y="-23.62843359375" />
                  <Point X="-3.561300292969" Y="-23.610703125" />
                  <Point X="-3.551351318359" Y="-23.592568359375" />
                  <Point X="-3.541635253906" Y="-23.569111328125" />
                  <Point X="-3.526703857422" Y="-23.533064453125" />
                  <Point X="-3.520915771484" Y="-23.51320703125" />
                  <Point X="-3.517157226562" Y="-23.491890625" />
                  <Point X="-3.515804443359" Y="-23.47125" />
                  <Point X="-3.518951660156" Y="-23.4508046875" />
                  <Point X="-3.524553955078" Y="-23.4298984375" />
                  <Point X="-3.532049560547" Y="-23.410623046875" />
                  <Point X="-3.543772949219" Y="-23.3881015625" />
                  <Point X="-3.561789306641" Y="-23.353494140625" />
                  <Point X="-3.573281494141" Y="-23.336294921875" />
                  <Point X="-3.587193603516" Y="-23.31971484375" />
                  <Point X="-3.602135986328" Y="-23.30541015625" />
                  <Point X="-4.1177265625" Y="-22.909783203125" />
                  <Point X="-4.351859863281" Y="-22.730126953125" />
                  <Point X="-4.28225" Y="-22.6108671875" />
                  <Point X="-4.081156738281" Y="-22.26634765625" />
                  <Point X="-3.923825195313" Y="-22.0641171875" />
                  <Point X="-3.750504150391" Y="-21.841337890625" />
                  <Point X="-3.464162353516" Y="-22.006658203125" />
                  <Point X="-3.206657226562" Y="-22.155328125" />
                  <Point X="-3.187728271484" Y="-22.163658203125" />
                  <Point X="-3.167085205078" Y="-22.17016796875" />
                  <Point X="-3.146795654297" Y="-22.174205078125" />
                  <Point X="-3.113072021484" Y="-22.17715625" />
                  <Point X="-3.061246337891" Y="-22.181689453125" />
                  <Point X="-3.040559570312" Y="-22.18123828125" />
                  <Point X="-3.0191015625" Y="-22.178412109375" />
                  <Point X="-2.999012207031" Y="-22.173494140625" />
                  <Point X="-2.980462890625" Y="-22.16434765625" />
                  <Point X="-2.962209228516" Y="-22.15271875" />
                  <Point X="-2.946077392578" Y="-22.13976953125" />
                  <Point X="-2.922140136719" Y="-22.11583203125" />
                  <Point X="-2.885353759766" Y="-22.079046875" />
                  <Point X="-2.872408935547" Y="-22.062919921875" />
                  <Point X="-2.860779052734" Y="-22.044666015625" />
                  <Point X="-2.851629150391" Y="-22.02611328125" />
                  <Point X="-2.846712158203" Y="-22.00601953125" />
                  <Point X="-2.843886962891" Y="-21.984560546875" />
                  <Point X="-2.843435791016" Y="-21.963880859375" />
                  <Point X="-2.846386230469" Y="-21.93015625" />
                  <Point X="-2.850920410156" Y="-21.878330078125" />
                  <Point X="-2.854955566406" Y="-21.85804296875" />
                  <Point X="-2.861464111328" Y="-21.837400390625" />
                  <Point X="-2.869794921875" Y="-21.818466796875" />
                  <Point X="-3.098268798828" Y="-21.42273828125" />
                  <Point X="-3.183332275391" Y="-21.275404296875" />
                  <Point X="-3.050867675781" Y="-21.173845703125" />
                  <Point X="-2.700621582031" Y="-20.905314453125" />
                  <Point X="-2.452840576172" Y="-20.76765234375" />
                  <Point X="-2.167036376953" Y="-20.608865234375" />
                  <Point X="-2.122025634766" Y="-20.667525390625" />
                  <Point X="-2.04319543457" Y="-20.7702578125" />
                  <Point X="-2.028890869141" Y="-20.785201171875" />
                  <Point X="-2.012310791016" Y="-20.79911328125" />
                  <Point X="-1.995116210938" Y="-20.8106015625" />
                  <Point X="-1.95758215332" Y="-20.830142578125" />
                  <Point X="-1.899900268555" Y="-20.860169921875" />
                  <Point X="-1.880624755859" Y="-20.86766796875" />
                  <Point X="-1.859717895508" Y="-20.873271484375" />
                  <Point X="-1.839268066406" Y="-20.876419921875" />
                  <Point X="-1.818621704102" Y="-20.87506640625" />
                  <Point X="-1.797306274414" Y="-20.871306640625" />
                  <Point X="-1.777453613281" Y="-20.86551953125" />
                  <Point X="-1.738359008789" Y="-20.849326171875" />
                  <Point X="-1.678279663086" Y="-20.824439453125" />
                  <Point X="-1.660144897461" Y="-20.814490234375" />
                  <Point X="-1.642415649414" Y="-20.802076171875" />
                  <Point X="-1.626863525391" Y="-20.7884375" />
                  <Point X="-1.614632324219" Y="-20.771755859375" />
                  <Point X="-1.603810546875" Y="-20.75301171875" />
                  <Point X="-1.59548046875" Y="-20.734078125" />
                  <Point X="-1.582755615234" Y="-20.693720703125" />
                  <Point X="-1.563200927734" Y="-20.631701171875" />
                  <Point X="-1.559165649414" Y="-20.611416015625" />
                  <Point X="-1.557279174805" Y="-20.58985546875" />
                  <Point X="-1.55773034668" Y="-20.569173828125" />
                  <Point X="-1.583705322266" Y="-20.371875" />
                  <Point X="-1.584201782227" Y="-20.368103515625" />
                  <Point X="-1.403044799805" Y="-20.3173125" />
                  <Point X="-0.949623168945" Y="-20.19019140625" />
                  <Point X="-0.649250427246" Y="-20.15503515625" />
                  <Point X="-0.294711334229" Y="-20.113541015625" />
                  <Point X="-0.21008039856" Y="-20.429388671875" />
                  <Point X="-0.133903305054" Y="-20.713685546875" />
                  <Point X="-0.121129844666" Y="-20.741876953125" />
                  <Point X="-0.10327155304" Y="-20.768603515625" />
                  <Point X="-0.082113983154" Y="-20.791193359375" />
                  <Point X="-0.054818080902" Y="-20.805783203125" />
                  <Point X="-0.024380004883" Y="-20.816115234375" />
                  <Point X="0.006155907631" Y="-20.82115625" />
                  <Point X="0.036691802979" Y="-20.816115234375" />
                  <Point X="0.067130027771" Y="-20.805783203125" />
                  <Point X="0.094425933838" Y="-20.791193359375" />
                  <Point X="0.115583503723" Y="-20.768603515625" />
                  <Point X="0.133441802979" Y="-20.741876953125" />
                  <Point X="0.146215255737" Y="-20.713685546875" />
                  <Point X="0.263282714844" Y="-20.276783203125" />
                  <Point X="0.307419403076" Y="-20.1120625" />
                  <Point X="0.448141876221" Y="-20.12680078125" />
                  <Point X="0.844045410156" Y="-20.16826171875" />
                  <Point X="1.09256237793" Y="-20.22826171875" />
                  <Point X="1.48102355957" Y="-20.322048828125" />
                  <Point X="1.641892333984" Y="-20.380396484375" />
                  <Point X="1.894645507812" Y="-20.472072265625" />
                  <Point X="2.051066162109" Y="-20.545224609375" />
                  <Point X="2.294559570312" Y="-20.659099609375" />
                  <Point X="2.445714111328" Y="-20.74716015625" />
                  <Point X="2.680972412109" Y="-20.88422265625" />
                  <Point X="2.823495849609" Y="-20.985578125" />
                  <Point X="2.943259277344" Y="-21.07074609375" />
                  <Point X="2.487235351562" Y="-21.860603515625" />
                  <Point X="2.147581054688" Y="-22.44890234375" />
                  <Point X="2.142074951172" Y="-22.460072265625" />
                  <Point X="2.133076904297" Y="-22.483943359375" />
                  <Point X="2.124613525391" Y="-22.515591796875" />
                  <Point X="2.111607177734" Y="-22.56423046875" />
                  <Point X="2.108619384766" Y="-22.582068359375" />
                  <Point X="2.107728027344" Y="-22.619048828125" />
                  <Point X="2.111028076172" Y="-22.646416015625" />
                  <Point X="2.116099365234" Y="-22.68847265625" />
                  <Point X="2.121442871094" Y="-22.7103984375" />
                  <Point X="2.129709228516" Y="-22.732486328125" />
                  <Point X="2.140070556641" Y="-22.75252734375" />
                  <Point X="2.157004638672" Y="-22.777484375" />
                  <Point X="2.183028320312" Y="-22.8158359375" />
                  <Point X="2.194464355469" Y="-22.829669921875" />
                  <Point X="2.221598388672" Y="-22.85440625" />
                  <Point X="2.2465546875" Y="-22.87133984375" />
                  <Point X="2.284906982422" Y="-22.897365234375" />
                  <Point X="2.304945800781" Y="-22.9077265625" />
                  <Point X="2.327034423828" Y="-22.915994140625" />
                  <Point X="2.348967041016" Y="-22.921337890625" />
                  <Point X="2.376334472656" Y="-22.92463671875" />
                  <Point X="2.418391845703" Y="-22.929708984375" />
                  <Point X="2.436467529297" Y="-22.93015625" />
                  <Point X="2.473209960938" Y="-22.925828125" />
                  <Point X="2.504858886719" Y="-22.91736328125" />
                  <Point X="2.553496337891" Y="-22.904357421875" />
                  <Point X="2.565285644531" Y="-22.900361328125" />
                  <Point X="2.588533935547" Y="-22.889853515625" />
                  <Point X="3.492618408203" Y="-22.367880859375" />
                  <Point X="3.967325683594" Y="-22.09380859375" />
                  <Point X="3.983720214844" Y="-22.11659375" />
                  <Point X="4.123274902344" Y="-22.31054296875" />
                  <Point X="4.202724121094" Y="-22.441833984375" />
                  <Point X="4.262198730469" Y="-22.5401171875" />
                  <Point X="3.677970703125" Y="-22.98841015625" />
                  <Point X="3.230783935547" Y="-23.331548828125" />
                  <Point X="3.221422363281" Y="-23.33976171875" />
                  <Point X="3.203974365234" Y="-23.358373046875" />
                  <Point X="3.181196533203" Y="-23.388087890625" />
                  <Point X="3.146192138672" Y="-23.43375390625" />
                  <Point X="3.136606689453" Y="-23.449087890625" />
                  <Point X="3.121629638672" Y="-23.4829140625" />
                  <Point X="3.113144775391" Y="-23.51325390625" />
                  <Point X="3.100105712891" Y="-23.55987890625" />
                  <Point X="3.09665234375" Y="-23.582177734375" />
                  <Point X="3.095836425781" Y="-23.605748046875" />
                  <Point X="3.097739501953" Y="-23.628232421875" />
                  <Point X="3.104704589844" Y="-23.66198828125" />
                  <Point X="3.115408447266" Y="-23.713865234375" />
                  <Point X="3.120679443359" Y="-23.731021484375" />
                  <Point X="3.136282470703" Y="-23.76426171875" />
                  <Point X="3.155226806641" Y="-23.793056640625" />
                  <Point X="3.184340087891" Y="-23.837306640625" />
                  <Point X="3.198895507812" Y="-23.854552734375" />
                  <Point X="3.216138183594" Y="-23.870640625" />
                  <Point X="3.234347167969" Y="-23.88396484375" />
                  <Point X="3.261800048828" Y="-23.89941796875" />
                  <Point X="3.303989257812" Y="-23.92316796875" />
                  <Point X="3.320520751953" Y="-23.930498046875" />
                  <Point X="3.356121582031" Y="-23.9405625" />
                  <Point X="3.393239746094" Y="-23.945466796875" />
                  <Point X="3.450282226562" Y="-23.953005859375" />
                  <Point X="3.462697753906" Y="-23.95382421875" />
                  <Point X="3.488203613281" Y="-23.953015625" />
                  <Point X="4.3470234375" Y="-23.839951171875" />
                  <Point X="4.77683984375" Y="-23.78336328125" />
                  <Point X="4.785999511719" Y="-23.820990234375" />
                  <Point X="4.845935546875" Y="-24.0671875" />
                  <Point X="4.870973632812" Y="-24.22800390625" />
                  <Point X="4.890864746094" Y="-24.355759765625" />
                  <Point X="4.229065429688" Y="-24.53308984375" />
                  <Point X="3.716579833984" Y="-24.67041015625" />
                  <Point X="3.704787597656" Y="-24.674416015625" />
                  <Point X="3.681547851562" Y="-24.684931640625" />
                  <Point X="3.645080322266" Y="-24.706009765625" />
                  <Point X="3.589037841797" Y="-24.73840234375" />
                  <Point X="3.574309814453" Y="-24.748904296875" />
                  <Point X="3.547530517578" Y="-24.774423828125" />
                  <Point X="3.525650146484" Y="-24.8023046875" />
                  <Point X="3.492024658203" Y="-24.84515234375" />
                  <Point X="3.480301025391" Y="-24.864431640625" />
                  <Point X="3.47052734375" Y="-24.88589453125" />
                  <Point X="3.463680908203" Y="-24.907396484375" />
                  <Point X="3.456387207031" Y="-24.94548046875" />
                  <Point X="3.445178710938" Y="-25.0040078125" />
                  <Point X="3.443482910156" Y="-25.02187890625" />
                  <Point X="3.445178955078" Y="-25.0585546875" />
                  <Point X="3.45247265625" Y="-25.096638671875" />
                  <Point X="3.463681152344" Y="-25.1551640625" />
                  <Point X="3.47052734375" Y="-25.1766640625" />
                  <Point X="3.48030078125" Y="-25.198126953125" />
                  <Point X="3.492024658203" Y="-25.217408203125" />
                  <Point X="3.513905029297" Y="-25.2452890625" />
                  <Point X="3.547530517578" Y="-25.28813671875" />
                  <Point X="3.559993652344" Y="-25.30123046875" />
                  <Point X="3.589036621094" Y="-25.324158203125" />
                  <Point X="3.625504150391" Y="-25.345236328125" />
                  <Point X="3.681546630859" Y="-25.377630859375" />
                  <Point X="3.692709228516" Y="-25.383140625" />
                  <Point X="3.716580078125" Y="-25.39215234375" />
                  <Point X="4.504156738281" Y="-25.603181640625" />
                  <Point X="4.891472167969" Y="-25.706962890625" />
                  <Point X="4.888488769531" Y="-25.72675390625" />
                  <Point X="4.855022460938" Y="-25.9487265625" />
                  <Point X="4.8229453125" Y="-26.089294921875" />
                  <Point X="4.801173828125" Y="-26.18469921875" />
                  <Point X="4.018721435547" Y="-26.0816875" />
                  <Point X="3.424382080078" Y="-26.00344140625" />
                  <Point X="3.408035644531" Y="-26.0027109375" />
                  <Point X="3.374658935547" Y="-26.005509765625" />
                  <Point X="3.303086181641" Y="-26.02106640625" />
                  <Point X="3.193094726563" Y="-26.04497265625" />
                  <Point X="3.163973876953" Y="-26.05659765625" />
                  <Point X="3.136147216797" Y="-26.073490234375" />
                  <Point X="3.112396728516" Y="-26.0939609375" />
                  <Point X="3.069135498047" Y="-26.1459921875" />
                  <Point X="3.002652587891" Y="-26.22594921875" />
                  <Point X="2.987933349609" Y="-26.250330078125" />
                  <Point X="2.97658984375" Y="-26.27771484375" />
                  <Point X="2.969757568359" Y="-26.305365234375" />
                  <Point X="2.963557128906" Y="-26.37274609375" />
                  <Point X="2.954028564453" Y="-26.476296875" />
                  <Point X="2.956347412109" Y="-26.50756640625" />
                  <Point X="2.964079101562" Y="-26.5391875" />
                  <Point X="2.976450683594" Y="-26.567998046875" />
                  <Point X="3.016060058594" Y="-26.629607421875" />
                  <Point X="3.076930908203" Y="-26.724287109375" />
                  <Point X="3.086930908203" Y="-26.737236328125" />
                  <Point X="3.110628173828" Y="-26.76091015625" />
                  <Point X="3.841504150391" Y="-27.321732421875" />
                  <Point X="4.21312109375" Y="-27.606884765625" />
                  <Point X="4.124808105469" Y="-27.7497890625" />
                  <Point X="4.058471435547" Y="-27.84404296875" />
                  <Point X="4.028980224609" Y="-27.8859453125" />
                  <Point X="3.330293945312" Y="-27.48255859375" />
                  <Point X="2.800954589844" Y="-27.1769453125" />
                  <Point X="2.786128417969" Y="-27.17001171875" />
                  <Point X="2.754224853516" Y="-27.159826171875" />
                  <Point X="2.669041748047" Y="-27.14444140625" />
                  <Point X="2.538134521484" Y="-27.12080078125" />
                  <Point X="2.506783203125" Y="-27.120396484375" />
                  <Point X="2.474611328125" Y="-27.125353515625" />
                  <Point X="2.444833496094" Y="-27.135177734375" />
                  <Point X="2.374067138672" Y="-27.172421875" />
                  <Point X="2.265315429688" Y="-27.22965625" />
                  <Point X="2.242386962891" Y="-27.246546875" />
                  <Point X="2.221426757812" Y="-27.267505859375" />
                  <Point X="2.204531982422" Y="-27.290439453125" />
                  <Point X="2.167288330078" Y="-27.361205078125" />
                  <Point X="2.110052978516" Y="-27.46995703125" />
                  <Point X="2.100229003906" Y="-27.499732421875" />
                  <Point X="2.095271240234" Y="-27.53190625" />
                  <Point X="2.095675292969" Y="-27.5632578125" />
                  <Point X="2.111059082031" Y="-27.64844140625" />
                  <Point X="2.134700927734" Y="-27.779349609375" />
                  <Point X="2.138985839844" Y="-27.79514453125" />
                  <Point X="2.151819091797" Y="-27.826080078125" />
                  <Point X="2.62148046875" Y="-28.639556640625" />
                  <Point X="2.861283447266" Y="-29.05490625" />
                  <Point X="2.781848632813" Y="-29.11164453125" />
                  <Point X="2.707680664062" Y="-29.15965234375" />
                  <Point X="2.701763916016" Y="-29.163482421875" />
                  <Point X="2.162480957031" Y="-28.460673828125" />
                  <Point X="1.758546142578" Y="-27.934255859375" />
                  <Point X="1.747507202148" Y="-27.92218359375" />
                  <Point X="1.721923217773" Y="-27.900556640625" />
                  <Point X="1.637909912109" Y="-27.846544921875" />
                  <Point X="1.508799926758" Y="-27.7635390625" />
                  <Point X="1.479986694336" Y="-27.751166015625" />
                  <Point X="1.448366088867" Y="-27.743435546875" />
                  <Point X="1.417100219727" Y="-27.741119140625" />
                  <Point X="1.325217163086" Y="-27.74957421875" />
                  <Point X="1.184013305664" Y="-27.76256640625" />
                  <Point X="1.156368164062" Y="-27.769396484375" />
                  <Point X="1.128982543945" Y="-27.78073828125" />
                  <Point X="1.104594604492" Y="-27.7954609375" />
                  <Point X="1.033644897461" Y="-27.854455078125" />
                  <Point X="0.924610900879" Y="-27.94511328125" />
                  <Point X="0.904140014648" Y="-27.968865234375" />
                  <Point X="0.887247680664" Y="-27.996693359375" />
                  <Point X="0.875624328613" Y="-28.025810546875" />
                  <Point X="0.854410888672" Y="-28.123408203125" />
                  <Point X="0.821810058594" Y="-28.273396484375" />
                  <Point X="0.819724487305" Y="-28.289626953125" />
                  <Point X="0.819742248535" Y="-28.323119140625" />
                  <Point X="0.952840759277" Y="-29.334103515625" />
                  <Point X="1.022065368652" Y="-29.859916015625" />
                  <Point X="0.975723815918" Y="-29.87007421875" />
                  <Point X="0.929315429688" Y="-29.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058426025391" Y="-29.752634765625" />
                  <Point X="-1.14124597168" Y="-29.731326171875" />
                  <Point X="-1.139784301758" Y="-29.720224609375" />
                  <Point X="-1.120775634766" Y="-29.575841796875" />
                  <Point X="-1.120077514648" Y="-29.5681015625" />
                  <Point X="-1.119451782227" Y="-29.541037109375" />
                  <Point X="-1.121759155273" Y="-29.50933203125" />
                  <Point X="-1.123333740234" Y="-29.497693359375" />
                  <Point X="-1.147193847656" Y="-29.377740234375" />
                  <Point X="-1.183861328125" Y="-29.193400390625" />
                  <Point X="-1.188124511719" Y="-29.178470703125" />
                  <Point X="-1.199026367188" Y="-29.14950390625" />
                  <Point X="-1.205665039062" Y="-29.135466796875" />
                  <Point X="-1.221736816406" Y="-29.10762890625" />
                  <Point X="-1.230573486328" Y="-29.094861328125" />
                  <Point X="-1.250208862305" Y="-29.070935546875" />
                  <Point X="-1.261007446289" Y="-29.05977734375" />
                  <Point X="-1.352959106445" Y="-28.979138671875" />
                  <Point X="-1.494268310547" Y="-28.855212890625" />
                  <Point X="-1.506739868164" Y="-28.84596484375" />
                  <Point X="-1.533023071289" Y="-28.82962109375" />
                  <Point X="-1.546834472656" Y="-28.822525390625" />
                  <Point X="-1.576531494141" Y="-28.810224609375" />
                  <Point X="-1.591316040039" Y="-28.805474609375" />
                  <Point X="-1.621457885742" Y="-28.798447265625" />
                  <Point X="-1.636815307617" Y="-28.796169921875" />
                  <Point X="-1.75885559082" Y="-28.788171875" />
                  <Point X="-1.946404296875" Y="-28.77587890625" />
                  <Point X="-1.961928344727" Y="-28.7761328125" />
                  <Point X="-1.992725585938" Y="-28.779166015625" />
                  <Point X="-2.007998779297" Y="-28.7819453125" />
                  <Point X="-2.039047729492" Y="-28.790263671875" />
                  <Point X="-2.053666992188" Y="-28.795494140625" />
                  <Point X="-2.081861328125" Y="-28.808267578125" />
                  <Point X="-2.095436767578" Y="-28.815810546875" />
                  <Point X="-2.197127197266" Y="-28.8837578125" />
                  <Point X="-2.353403076172" Y="-28.988177734375" />
                  <Point X="-2.359683349609" Y="-28.9927578125" />
                  <Point X="-2.380451416016" Y="-29.010138671875" />
                  <Point X="-2.402763671875" Y="-29.03277734375" />
                  <Point X="-2.410470947266" Y="-29.041630859375" />
                  <Point X="-2.503201171875" Y="-29.162478515625" />
                  <Point X="-2.747597167969" Y="-29.011154296875" />
                  <Point X="-2.899306640625" Y="-28.89434375" />
                  <Point X="-2.980862548828" Y="-28.831548828125" />
                  <Point X="-2.634945800781" Y="-28.23240234375" />
                  <Point X="-2.341488769531" Y="-27.724119140625" />
                  <Point X="-2.334849853516" Y="-27.71008203125" />
                  <Point X="-2.323947753906" Y="-27.681115234375" />
                  <Point X="-2.319684570312" Y="-27.666185546875" />
                  <Point X="-2.313413574219" Y="-27.63466015625" />
                  <Point X="-2.311638916016" Y="-27.619236328125" />
                  <Point X="-2.310625976562" Y="-27.5883046875" />
                  <Point X="-2.314665771484" Y="-27.557619140625" />
                  <Point X="-2.323649902344" Y="-27.528001953125" />
                  <Point X="-2.329355957031" Y="-27.5135625" />
                  <Point X="-2.343572998047" Y="-27.484732421875" />
                  <Point X="-2.3515546875" Y="-27.4714140625" />
                  <Point X="-2.369584716797" Y="-27.446251953125" />
                  <Point X="-2.379633056641" Y="-27.434408203125" />
                  <Point X="-2.396981689453" Y="-27.417060546875" />
                  <Point X="-2.408822021484" Y="-27.407015625" />
                  <Point X="-2.433978027344" Y="-27.388990234375" />
                  <Point X="-2.447293701172" Y="-27.381009765625" />
                  <Point X="-2.476122558594" Y="-27.36679296875" />
                  <Point X="-2.490563476562" Y="-27.3610859375" />
                  <Point X="-2.520181640625" Y="-27.3521015625" />
                  <Point X="-2.550869384766" Y="-27.3480625" />
                  <Point X="-2.581803466797" Y="-27.349076171875" />
                  <Point X="-2.597227050781" Y="-27.3508515625" />
                  <Point X="-2.628753417969" Y="-27.357123046875" />
                  <Point X="-2.643684082031" Y="-27.36138671875" />
                  <Point X="-2.672649169922" Y="-27.3722890625" />
                  <Point X="-2.68668359375" Y="-27.378927734375" />
                  <Point X="-3.467800537109" Y="-27.82990625" />
                  <Point X="-3.793086669922" Y="-28.0177109375" />
                  <Point X="-3.801943603516" Y="-28.00607421875" />
                  <Point X="-4.004021240234" Y="-27.7405859375" />
                  <Point X="-4.112786132813" Y="-27.558203125" />
                  <Point X="-4.181265136719" Y="-27.443375" />
                  <Point X="-3.561576660156" Y="-26.96787109375" />
                  <Point X="-3.048122314453" Y="-26.573884765625" />
                  <Point X="-3.036481689453" Y="-26.5633125" />
                  <Point X="-3.015104736328" Y="-26.540392578125" />
                  <Point X="-3.005368408203" Y="-26.528044921875" />
                  <Point X="-2.987403564453" Y="-26.5009140625" />
                  <Point X="-2.979836181641" Y="-26.487126953125" />
                  <Point X="-2.967079833984" Y="-26.45849609375" />
                  <Point X="-2.961890869141" Y="-26.44365234375" />
                  <Point X="-2.954186279297" Y="-26.413904296875" />
                  <Point X="-2.951552734375" Y="-26.3988046875" />
                  <Point X="-2.948748535156" Y="-26.368376953125" />
                  <Point X="-2.948577880859" Y="-26.353048828125" />
                  <Point X="-2.950787109375" Y="-26.321375" />
                  <Point X="-2.953084228516" Y="-26.306216796875" />
                  <Point X="-2.960086669922" Y="-26.27646875" />
                  <Point X="-2.971782226562" Y="-26.248236328125" />
                  <Point X="-2.987867675781" Y="-26.22225" />
                  <Point X="-2.996962890625" Y="-26.20990625" />
                  <Point X="-3.017797119141" Y="-26.185951171875" />
                  <Point X="-3.028755859375" Y="-26.175236328125" />
                  <Point X="-3.052252685547" Y="-26.15570703125" />
                  <Point X="-3.064790771484" Y="-26.146892578125" />
                  <Point X="-3.0912734375" Y="-26.131306640625" />
                  <Point X="-3.105441894531" Y="-26.124478515625" />
                  <Point X="-3.134705810547" Y="-26.113255859375" />
                  <Point X="-3.149801269531" Y="-26.108861328125" />
                  <Point X="-3.181686767578" Y="-26.102380859375" />
                  <Point X="-3.197298339844" Y="-26.10053515625" />
                  <Point X="-3.228619384766" Y="-26.099443359375" />
                  <Point X="-3.244328857422" Y="-26.100197265625" />
                  <Point X="-4.230415527344" Y="-26.230017578125" />
                  <Point X="-4.660920410156" Y="-26.286693359375" />
                  <Point X="-4.661729003906" Y="-26.283529296875" />
                  <Point X="-4.74076171875" Y="-25.97412109375" />
                  <Point X="-4.769539550781" Y="-25.772908203125" />
                  <Point X="-4.786452148438" Y="-25.654658203125" />
                  <Point X="-4.093189941406" Y="-25.4688984375" />
                  <Point X="-3.508288085938" Y="-25.312173828125" />
                  <Point X="-3.497134033203" Y="-25.3084296875" />
                  <Point X="-3.475374267578" Y="-25.29959375" />
                  <Point X="-3.464768554688" Y="-25.294501953125" />
                  <Point X="-3.438261962891" Y="-25.2796875" />
                  <Point X="-3.422629150391" Y="-25.26992578125" />
                  <Point X="-3.40581640625" Y="-25.2582578125" />
                  <Point X="-3.396485595703" Y="-25.250875" />
                  <Point X="-3.372529785156" Y="-25.229349609375" />
                  <Point X="-3.357465087891" Y="-25.212103515625" />
                  <Point X="-3.336115478516" Y="-25.180705078125" />
                  <Point X="-3.327025146484" Y="-25.163927734375" />
                  <Point X="-3.315952392578" Y="-25.137439453125" />
                  <Point X="-3.313281494141" Y="-25.13024609375" />
                  <Point X="-3.306448974609" Y="-25.108291015625" />
                  <Point X="-3.29859765625" Y="-25.0754140625" />
                  <Point X="-3.296696777344" Y="-25.064837890625" />
                  <Point X="-3.2941015625" Y="-25.0435390625" />
                  <Point X="-3.293928222656" Y="-25.022083984375" />
                  <Point X="-3.296178955078" Y="-25.00074609375" />
                  <Point X="-3.297908691406" Y="-24.990140625" />
                  <Point X="-3.303802001953" Y="-24.9635703125" />
                  <Point X="-3.305741455078" Y="-24.9562265625" />
                  <Point X="-3.312719970703" Y="-24.93455859375" />
                  <Point X="-3.325750976562" Y="-24.901759765625" />
                  <Point X="-3.330747314453" Y="-24.89114453125" />
                  <Point X="-3.342009033203" Y="-24.870615234375" />
                  <Point X="-3.355713134766" Y="-24.8516328125" />
                  <Point X="-3.371654541016" Y="-24.83448046875" />
                  <Point X="-3.380157226562" Y="-24.826396484375" />
                  <Point X="-3.402918701172" Y="-24.807287109375" />
                  <Point X="-3.416748779297" Y="-24.796716796875" />
                  <Point X="-3.433556396484" Y="-24.78505078125" />
                  <Point X="-3.44048046875" Y="-24.780673828125" />
                  <Point X="-3.465598388672" Y="-24.76717578125" />
                  <Point X="-3.496557861328" Y="-24.7543671875" />
                  <Point X="-3.508288085938" Y="-24.75038671875" />
                  <Point X="-4.40715234375" Y="-24.509537109375" />
                  <Point X="-4.785445800781" Y="-24.408173828125" />
                  <Point X="-4.782265625" Y="-24.386681640625" />
                  <Point X="-4.731331542969" Y="-24.04247265625" />
                  <Point X="-4.673401855469" Y="-23.8286953125" />
                  <Point X="-4.6335859375" Y="-23.681763671875" />
                  <Point X="-4.197338378906" Y="-23.739197265625" />
                  <Point X="-3.77806640625" Y="-23.79439453125" />
                  <Point X="-3.76773828125" Y="-23.79518359375" />
                  <Point X="-3.747056640625" Y="-23.795634765625" />
                  <Point X="-3.736703125" Y="-23.795296875" />
                  <Point X="-3.715142333984" Y="-23.79341015625" />
                  <Point X="-3.704890136719" Y="-23.7919453125" />
                  <Point X="-3.684604248047" Y="-23.78791015625" />
                  <Point X="-3.674570556641" Y="-23.78533984375" />
                  <Point X="-3.650356445312" Y="-23.777705078125" />
                  <Point X="-3.61314453125" Y="-23.76597265625" />
                  <Point X="-3.603450439453" Y="-23.76232421875" />
                  <Point X="-3.584523681641" Y="-23.75399609375" />
                  <Point X="-3.575291015625" Y="-23.74931640625" />
                  <Point X="-3.556546630859" Y="-23.73849609375" />
                  <Point X="-3.547870605469" Y="-23.7328359375" />
                  <Point X="-3.531185791016" Y="-23.720603515625" />
                  <Point X="-3.515930664062" Y="-23.706625" />
                  <Point X="-3.502289794922" Y="-23.6910703125" />
                  <Point X="-3.495895263672" Y="-23.682921875" />
                  <Point X="-3.48348046875" Y="-23.66519140625" />
                  <Point X="-3.478010986328" Y="-23.656396484375" />
                  <Point X="-3.468062011719" Y="-23.63826171875" />
                  <Point X="-3.463582519531" Y="-23.628921875" />
                  <Point X="-3.453866455078" Y="-23.60546484375" />
                  <Point X="-3.438935058594" Y="-23.56941796875" />
                  <Point X="-3.435499267578" Y="-23.5596484375" />
                  <Point X="-3.429711181641" Y="-23.539791015625" />
                  <Point X="-3.427358886719" Y="-23.529703125" />
                  <Point X="-3.423600341797" Y="-23.50838671875" />
                  <Point X="-3.422360595703" Y="-23.498103515625" />
                  <Point X="-3.4210078125" Y="-23.477462890625" />
                  <Point X="-3.421910400391" Y="-23.456796875" />
                  <Point X="-3.425057617188" Y="-23.4363515625" />
                  <Point X="-3.427189208984" Y="-23.42621484375" />
                  <Point X="-3.432791503906" Y="-23.40530859375" />
                  <Point X="-3.436012939453" Y="-23.395466796875" />
                  <Point X="-3.443508544922" Y="-23.37619140625" />
                  <Point X="-3.447782714844" Y="-23.3667578125" />
                  <Point X="-3.459506103516" Y="-23.344236328125" />
                  <Point X="-3.477522460938" Y="-23.30962890625" />
                  <Point X="-3.482799804688" Y="-23.30071484375" />
                  <Point X="-3.494291992188" Y="-23.283515625" />
                  <Point X="-3.500506835938" Y="-23.27523046875" />
                  <Point X="-3.514418945312" Y="-23.258650390625" />
                  <Point X="-3.521498779297" Y="-23.251091796875" />
                  <Point X="-3.536441162109" Y="-23.236787109375" />
                  <Point X="-3.544303710938" Y="-23.230041015625" />
                  <Point X="-4.059894287109" Y="-22.8344140625" />
                  <Point X="-4.227614746094" Y="-22.70571875" />
                  <Point X="-4.200203613281" Y="-22.658755859375" />
                  <Point X="-4.002296386719" Y="-22.3196953125" />
                  <Point X="-3.848844238281" Y="-22.122451171875" />
                  <Point X="-3.726337646484" Y="-21.964986328125" />
                  <Point X="-3.511662597656" Y="-22.0889296875" />
                  <Point X="-3.254157470703" Y="-22.237599609375" />
                  <Point X="-3.244922607422" Y="-22.24228125" />
                  <Point X="-3.225993652344" Y="-22.250611328125" />
                  <Point X="-3.216299560547" Y="-22.254259765625" />
                  <Point X="-3.195656494141" Y="-22.26076953125" />
                  <Point X="-3.185624267578" Y="-22.263341796875" />
                  <Point X="-3.165334716797" Y="-22.26737890625" />
                  <Point X="-3.155077392578" Y="-22.26884375" />
                  <Point X="-3.121353759766" Y="-22.271794921875" />
                  <Point X="-3.069528076172" Y="-22.276328125" />
                  <Point X="-3.059174804688" Y="-22.276666015625" />
                  <Point X="-3.038488037109" Y="-22.27621484375" />
                  <Point X="-3.028154541016" Y="-22.27542578125" />
                  <Point X="-3.006696533203" Y="-22.272599609375" />
                  <Point X="-2.996512207031" Y="-22.2706875" />
                  <Point X="-2.976422851562" Y="-22.26576953125" />
                  <Point X="-2.956998535156" Y="-22.25869921875" />
                  <Point X="-2.93844921875" Y="-22.249552734375" />
                  <Point X="-2.929419189453" Y="-22.244470703125" />
                  <Point X="-2.911165527344" Y="-22.232841796875" />
                  <Point X="-2.902740722656" Y="-22.226802734375" />
                  <Point X="-2.886608886719" Y="-22.213853515625" />
                  <Point X="-2.878901855469" Y="-22.206943359375" />
                  <Point X="-2.854964599609" Y="-22.183005859375" />
                  <Point X="-2.818178222656" Y="-22.146220703125" />
                  <Point X="-2.811268310547" Y="-22.138513671875" />
                  <Point X="-2.798323486328" Y="-22.12238671875" />
                  <Point X="-2.792288574219" Y="-22.113966796875" />
                  <Point X="-2.780658691406" Y="-22.095712890625" />
                  <Point X="-2.775577392578" Y="-22.086685546875" />
                  <Point X="-2.766427490234" Y="-22.0681328125" />
                  <Point X="-2.759351806641" Y="-22.048693359375" />
                  <Point X="-2.754434814453" Y="-22.028599609375" />
                  <Point X="-2.752524902344" Y="-22.018419921875" />
                  <Point X="-2.749699707031" Y="-21.9969609375" />
                  <Point X="-2.748909667969" Y="-21.9866328125" />
                  <Point X="-2.748458496094" Y="-21.965953125" />
                  <Point X="-2.748797363281" Y="-21.9556015625" />
                  <Point X="-2.751747802734" Y="-21.921876953125" />
                  <Point X="-2.756281982422" Y="-21.87005078125" />
                  <Point X="-2.757745605469" Y="-21.859796875" />
                  <Point X="-2.761780761719" Y="-21.839509765625" />
                  <Point X="-2.764352294922" Y="-21.8294765625" />
                  <Point X="-2.770860839844" Y="-21.808833984375" />
                  <Point X="-2.774509277344" Y="-21.799140625" />
                  <Point X="-2.782840087891" Y="-21.78020703125" />
                  <Point X="-2.787522460938" Y="-21.770966796875" />
                  <Point X="-3.015996337891" Y="-21.37523828125" />
                  <Point X="-3.059386230469" Y="-21.3000859375" />
                  <Point X="-2.993065917969" Y="-21.24923828125" />
                  <Point X="-2.648371337891" Y="-20.984962890625" />
                  <Point X="-2.406702880859" Y="-20.850697265625" />
                  <Point X="-2.192524658203" Y="-20.731705078125" />
                  <Point X="-2.118564453125" Y="-20.82808984375" />
                  <Point X="-2.111821289062" Y="-20.83594921875" />
                  <Point X="-2.097516845703" Y="-20.850892578125" />
                  <Point X="-2.089955078125" Y="-20.8579765625" />
                  <Point X="-2.073375" Y="-20.871888671875" />
                  <Point X="-2.065087402344" Y="-20.87810546875" />
                  <Point X="-2.047892944336" Y="-20.88959375" />
                  <Point X="-2.038985839844" Y="-20.894865234375" />
                  <Point X="-2.001451782227" Y="-20.91440625" />
                  <Point X="-1.943769897461" Y="-20.94443359375" />
                  <Point X="-1.934340698242" Y="-20.94870703125" />
                  <Point X="-1.915065185547" Y="-20.956205078125" />
                  <Point X="-1.90521875" Y="-20.9594296875" />
                  <Point X="-1.884312011719" Y="-20.965033203125" />
                  <Point X="-1.874173706055" Y="-20.967166015625" />
                  <Point X="-1.853723999023" Y="-20.970314453125" />
                  <Point X="-1.833053466797" Y="-20.971216796875" />
                  <Point X="-1.812407104492" Y="-20.96986328125" />
                  <Point X="-1.802119750977" Y="-20.968623046875" />
                  <Point X="-1.780804199219" Y="-20.96486328125" />
                  <Point X="-1.770720092773" Y="-20.962509765625" />
                  <Point X="-1.750867431641" Y="-20.95672265625" />
                  <Point X="-1.741098999023" Y="-20.9532890625" />
                  <Point X="-1.702004394531" Y="-20.937095703125" />
                  <Point X="-1.641925048828" Y="-20.912208984375" />
                  <Point X="-1.632585205078" Y="-20.907728515625" />
                  <Point X="-1.614450439453" Y="-20.897779296875" />
                  <Point X="-1.605655395508" Y="-20.892310546875" />
                  <Point X="-1.587926147461" Y="-20.879896484375" />
                  <Point X="-1.579778198242" Y="-20.873501953125" />
                  <Point X="-1.564225952148" Y="-20.85986328125" />
                  <Point X="-1.550250488281" Y="-20.844611328125" />
                  <Point X="-1.53801953125" Y="-20.8279296875" />
                  <Point X="-1.532359619141" Y="-20.819255859375" />
                  <Point X="-1.521537841797" Y="-20.80051171875" />
                  <Point X="-1.516854370117" Y="-20.79126953125" />
                  <Point X="-1.508524169922" Y="-20.7723359375" />
                  <Point X="-1.504877441406" Y="-20.762646484375" />
                  <Point X="-1.49215246582" Y="-20.7222890625" />
                  <Point X="-1.472597900391" Y="-20.66026953125" />
                  <Point X="-1.470026611328" Y="-20.650236328125" />
                  <Point X="-1.465991333008" Y="-20.629951171875" />
                  <Point X="-1.46452722168" Y="-20.619697265625" />
                  <Point X="-1.46264074707" Y="-20.59813671875" />
                  <Point X="-1.462301757812" Y="-20.587783203125" />
                  <Point X="-1.462752929688" Y="-20.5671015625" />
                  <Point X="-1.46354309082" Y="-20.5567734375" />
                  <Point X="-1.479266113281" Y="-20.437345703125" />
                  <Point X="-1.37739855957" Y="-20.40878515625" />
                  <Point X="-0.931162902832" Y="-20.2836796875" />
                  <Point X="-0.638206787109" Y="-20.249390625" />
                  <Point X="-0.36522265625" Y="-20.21744140625" />
                  <Point X="-0.301843292236" Y="-20.4539765625" />
                  <Point X="-0.22566633606" Y="-20.7382734375" />
                  <Point X="-0.220435256958" Y="-20.752892578125" />
                  <Point X="-0.207661849976" Y="-20.781083984375" />
                  <Point X="-0.200119247437" Y="-20.79465625" />
                  <Point X="-0.182260864258" Y="-20.8213828125" />
                  <Point X="-0.172608840942" Y="-20.833544921875" />
                  <Point X="-0.151451339722" Y="-20.856134765625" />
                  <Point X="-0.126896453857" Y="-20.8749765625" />
                  <Point X="-0.099600524902" Y="-20.88956640625" />
                  <Point X="-0.085353973389" Y="-20.8957421875" />
                  <Point X="-0.054916004181" Y="-20.90607421875" />
                  <Point X="-0.039853668213" Y="-20.909845703125" />
                  <Point X="-0.009317756653" Y="-20.91488671875" />
                  <Point X="0.021629543304" Y="-20.91488671875" />
                  <Point X="0.052165454865" Y="-20.909845703125" />
                  <Point X="0.067227645874" Y="-20.90607421875" />
                  <Point X="0.097665908813" Y="-20.8957421875" />
                  <Point X="0.111912460327" Y="-20.88956640625" />
                  <Point X="0.139208389282" Y="-20.8749765625" />
                  <Point X="0.163763275146" Y="-20.856134765625" />
                  <Point X="0.184920776367" Y="-20.833544921875" />
                  <Point X="0.194572952271" Y="-20.8213828125" />
                  <Point X="0.212431182861" Y="-20.79465625" />
                  <Point X="0.2199737854" Y="-20.781083984375" />
                  <Point X="0.232747192383" Y="-20.752892578125" />
                  <Point X="0.237978271484" Y="-20.7382734375" />
                  <Point X="0.355045715332" Y="-20.30137109375" />
                  <Point X="0.378190490723" Y="-20.214994140625" />
                  <Point X="0.438246429443" Y="-20.221283203125" />
                  <Point X="0.827865966797" Y="-20.2620859375" />
                  <Point X="1.070266967773" Y="-20.320609375" />
                  <Point X="1.453607910156" Y="-20.41316015625" />
                  <Point X="1.609500366211" Y="-20.469703125" />
                  <Point X="1.858248291016" Y="-20.55992578125" />
                  <Point X="2.010821533203" Y="-20.631279296875" />
                  <Point X="2.250435546875" Y="-20.74333984375" />
                  <Point X="2.397892089844" Y="-20.82924609375" />
                  <Point X="2.629424804688" Y="-20.964138671875" />
                  <Point X="2.768439208984" Y="-21.062998046875" />
                  <Point X="2.817778564453" Y="-21.0980859375" />
                  <Point X="2.404962890625" Y="-21.813103515625" />
                  <Point X="2.06530859375" Y="-22.40140234375" />
                  <Point X="2.06237109375" Y="-22.4068984375" />
                  <Point X="2.053180664062" Y="-22.426564453125" />
                  <Point X="2.044182373047" Y="-22.450435546875" />
                  <Point X="2.041301757812" Y="-22.459400390625" />
                  <Point X="2.032838378906" Y="-22.491048828125" />
                  <Point X="2.01983203125" Y="-22.5396875" />
                  <Point X="2.017912231445" Y="-22.548537109375" />
                  <Point X="2.013646972656" Y="-22.579779296875" />
                  <Point X="2.012755615234" Y="-22.616759765625" />
                  <Point X="2.013411254883" Y="-22.630421875" />
                  <Point X="2.016711303711" Y="-22.6577890625" />
                  <Point X="2.021782592773" Y="-22.699845703125" />
                  <Point X="2.02380078125" Y="-22.710966796875" />
                  <Point X="2.029144287109" Y="-22.732892578125" />
                  <Point X="2.032469604492" Y="-22.743697265625" />
                  <Point X="2.040735961914" Y="-22.76578515625" />
                  <Point X="2.04532043457" Y="-22.776115234375" />
                  <Point X="2.055681884766" Y="-22.79615625" />
                  <Point X="2.061458740234" Y="-22.8058671875" />
                  <Point X="2.078392822266" Y="-22.83082421875" />
                  <Point X="2.104416503906" Y="-22.86917578125" />
                  <Point X="2.109807617188" Y="-22.876365234375" />
                  <Point X="2.130462646484" Y="-22.899875" />
                  <Point X="2.157596679688" Y="-22.924611328125" />
                  <Point X="2.168258056641" Y="-22.933017578125" />
                  <Point X="2.193214355469" Y="-22.949951171875" />
                  <Point X="2.231566650391" Y="-22.9759765625" />
                  <Point X="2.241273681641" Y="-22.981751953125" />
                  <Point X="2.2613125" Y="-22.99211328125" />
                  <Point X="2.271644287109" Y="-22.99669921875" />
                  <Point X="2.293732910156" Y="-23.004966796875" />
                  <Point X="2.304546142578" Y="-23.008294921875" />
                  <Point X="2.326478759766" Y="-23.013638671875" />
                  <Point X="2.337598144531" Y="-23.015654296875" />
                  <Point X="2.364965576172" Y="-23.018953125" />
                  <Point X="2.407022949219" Y="-23.024025390625" />
                  <Point X="2.416041992188" Y="-23.0246796875" />
                  <Point X="2.447581298828" Y="-23.02450390625" />
                  <Point X="2.484323730469" Y="-23.02017578125" />
                  <Point X="2.497755859375" Y="-23.0176015625" />
                  <Point X="2.529404785156" Y="-23.00913671875" />
                  <Point X="2.578042236328" Y="-22.996130859375" />
                  <Point X="2.583993164062" Y="-22.994330078125" />
                  <Point X="2.604412841797" Y="-22.9869296875" />
                  <Point X="2.627661132812" Y="-22.976421875" />
                  <Point X="2.636033935547" Y="-22.972125" />
                  <Point X="3.540118408203" Y="-22.45015234375" />
                  <Point X="3.940403808594" Y="-22.219046875" />
                  <Point X="4.043952392578" Y="-22.36295703125" />
                  <Point X="4.121447265625" Y="-22.491017578125" />
                  <Point X="4.136884765625" Y="-22.516529296875" />
                  <Point X="3.620138427734" Y="-22.913041015625" />
                  <Point X="3.172951660156" Y="-23.2561796875" />
                  <Point X="3.168133056641" Y="-23.260134765625" />
                  <Point X="3.152116210938" Y="-23.274787109375" />
                  <Point X="3.134668212891" Y="-23.2933984375" />
                  <Point X="3.128577392578" Y="-23.300578125" />
                  <Point X="3.105799560547" Y="-23.33029296875" />
                  <Point X="3.070795166016" Y="-23.375958984375" />
                  <Point X="3.065636230469" Y="-23.3833984375" />
                  <Point X="3.049740478516" Y="-23.410626953125" />
                  <Point X="3.034763427734" Y="-23.444453125" />
                  <Point X="3.030139892578" Y="-23.457328125" />
                  <Point X="3.021655029297" Y="-23.48766796875" />
                  <Point X="3.008615966797" Y="-23.53429296875" />
                  <Point X="3.006224853516" Y="-23.54533984375" />
                  <Point X="3.002771484375" Y="-23.567638671875" />
                  <Point X="3.001709228516" Y="-23.578890625" />
                  <Point X="3.000893310547" Y="-23.6024609375" />
                  <Point X="3.001174804688" Y="-23.613759765625" />
                  <Point X="3.003077880859" Y="-23.636244140625" />
                  <Point X="3.004699462891" Y="-23.6474296875" />
                  <Point X="3.011664550781" Y="-23.681185546875" />
                  <Point X="3.022368408203" Y="-23.7330625" />
                  <Point X="3.02459765625" Y="-23.741765625" />
                  <Point X="3.034682373047" Y="-23.771388671875" />
                  <Point X="3.050285400391" Y="-23.80462890625" />
                  <Point X="3.056918212891" Y="-23.8164765625" />
                  <Point X="3.075862548828" Y="-23.845271484375" />
                  <Point X="3.104975830078" Y="-23.889521484375" />
                  <Point X="3.111740722656" Y="-23.898580078125" />
                  <Point X="3.126296142578" Y="-23.915826171875" />
                  <Point X="3.134086669922" Y="-23.924013671875" />
                  <Point X="3.151329345703" Y="-23.9401015625" />
                  <Point X="3.160038085938" Y="-23.947306640625" />
                  <Point X="3.178247070312" Y="-23.960630859375" />
                  <Point X="3.187747314453" Y="-23.96675" />
                  <Point X="3.215200195312" Y="-23.982203125" />
                  <Point X="3.257389404297" Y="-24.005953125" />
                  <Point X="3.265481689453" Y="-24.010013671875" />
                  <Point X="3.294676757813" Y="-24.021916015625" />
                  <Point X="3.330277587891" Y="-24.03198046875" />
                  <Point X="3.343677734375" Y="-24.034744140625" />
                  <Point X="3.380795898438" Y="-24.0396484375" />
                  <Point X="3.437838378906" Y="-24.0471875" />
                  <Point X="3.444033935547" Y="-24.04780078125" />
                  <Point X="3.465708007812" Y="-24.04877734375" />
                  <Point X="3.491213867188" Y="-24.04796875" />
                  <Point X="3.500603515625" Y="-24.047203125" />
                  <Point X="4.359423339844" Y="-23.934138671875" />
                  <Point X="4.704704101562" Y="-23.8886796875" />
                  <Point X="4.752682617188" Y="-24.08576171875" />
                  <Point X="4.777104492188" Y="-24.242619140625" />
                  <Point X="4.783870605469" Y="-24.286076171875" />
                  <Point X="4.204477539062" Y="-24.441326171875" />
                  <Point X="3.691991943359" Y="-24.578646484375" />
                  <Point X="3.686022949219" Y="-24.580458984375" />
                  <Point X="3.665624267578" Y="-24.58786328125" />
                  <Point X="3.642384521484" Y="-24.59837890625" />
                  <Point X="3.634008056641" Y="-24.602681640625" />
                  <Point X="3.597540527344" Y="-24.623759765625" />
                  <Point X="3.541498046875" Y="-24.65615234375" />
                  <Point X="3.533883056641" Y="-24.661052734375" />
                  <Point X="3.508771728516" Y="-24.680130859375" />
                  <Point X="3.481992431641" Y="-24.705650390625" />
                  <Point X="3.472796386719" Y="-24.7157734375" />
                  <Point X="3.450916015625" Y="-24.743654296875" />
                  <Point X="3.417290527344" Y="-24.786501953125" />
                  <Point X="3.410854248047" Y="-24.79579296875" />
                  <Point X="3.399130615234" Y="-24.815072265625" />
                  <Point X="3.393843261719" Y="-24.825060546875" />
                  <Point X="3.384069580078" Y="-24.8465234375" />
                  <Point X="3.380005371094" Y="-24.857072265625" />
                  <Point X="3.373158935547" Y="-24.87857421875" />
                  <Point X="3.370376708984" Y="-24.88952734375" />
                  <Point X="3.363083007812" Y="-24.927611328125" />
                  <Point X="3.351874511719" Y="-24.986138671875" />
                  <Point X="3.350603515625" Y="-24.995033203125" />
                  <Point X="3.348584228516" Y="-25.026267578125" />
                  <Point X="3.350280273438" Y="-25.062943359375" />
                  <Point X="3.351874755859" Y="-25.076423828125" />
                  <Point X="3.359168457031" Y="-25.1145078125" />
                  <Point X="3.370376953125" Y="-25.173033203125" />
                  <Point X="3.373159667969" Y="-25.18398828125" />
                  <Point X="3.380005859375" Y="-25.20548828125" />
                  <Point X="3.384069335938" Y="-25.216033203125" />
                  <Point X="3.393842773438" Y="-25.23749609375" />
                  <Point X="3.399128417969" Y="-25.247484375" />
                  <Point X="3.410852294922" Y="-25.266765625" />
                  <Point X="3.417290527344" Y="-25.27605859375" />
                  <Point X="3.439170898438" Y="-25.303939453125" />
                  <Point X="3.472796386719" Y="-25.346787109375" />
                  <Point X="3.47871875" Y="-25.353634765625" />
                  <Point X="3.50112890625" Y="-25.375794921875" />
                  <Point X="3.530171875" Y="-25.39872265625" />
                  <Point X="3.541496826172" Y="-25.406408203125" />
                  <Point X="3.577964355469" Y="-25.427486328125" />
                  <Point X="3.634006835938" Y="-25.459880859375" />
                  <Point X="3.639498535156" Y="-25.462818359375" />
                  <Point X="3.65915625" Y="-25.472017578125" />
                  <Point X="3.683027099609" Y="-25.481029296875" />
                  <Point X="3.691992431641" Y="-25.483916015625" />
                  <Point X="4.479568847656" Y="-25.6949453125" />
                  <Point X="4.784876953125" Y="-25.776751953125" />
                  <Point X="4.761614257812" Y="-25.93105078125" />
                  <Point X="4.730326171875" Y="-26.06816015625" />
                  <Point X="4.727802246094" Y="-26.07921875" />
                  <Point X="4.031121337891" Y="-25.9875" />
                  <Point X="3.436781982422" Y="-25.90925390625" />
                  <Point X="3.428623046875" Y="-25.90853515625" />
                  <Point X="3.400097167969" Y="-25.90804296875" />
                  <Point X="3.366720458984" Y="-25.910841796875" />
                  <Point X="3.354481445312" Y="-25.912677734375" />
                  <Point X="3.282908691406" Y="-25.928234375" />
                  <Point X="3.172917236328" Y="-25.952140625" />
                  <Point X="3.157873535156" Y="-25.9567421875" />
                  <Point X="3.128752685547" Y="-25.9683671875" />
                  <Point X="3.114675537109" Y="-25.975390625" />
                  <Point X="3.086848876953" Y="-25.992283203125" />
                  <Point X="3.074124755859" Y="-26.00153125" />
                  <Point X="3.050374267578" Y="-26.022001953125" />
                  <Point X="3.039347900391" Y="-26.033224609375" />
                  <Point X="2.996086669922" Y="-26.085255859375" />
                  <Point X="2.929603759766" Y="-26.165212890625" />
                  <Point X="2.921324462891" Y="-26.176849609375" />
                  <Point X="2.906605224609" Y="-26.20123046875" />
                  <Point X="2.900165283203" Y="-26.213974609375" />
                  <Point X="2.888821777344" Y="-26.241359375" />
                  <Point X="2.884363525391" Y="-26.25492578125" />
                  <Point X="2.87753125" Y="-26.282576171875" />
                  <Point X="2.875157226562" Y="-26.29666015625" />
                  <Point X="2.868956787109" Y="-26.364041015625" />
                  <Point X="2.859428222656" Y="-26.467591796875" />
                  <Point X="2.859288818359" Y="-26.483322265625" />
                  <Point X="2.861607666016" Y="-26.514591796875" />
                  <Point X="2.864065917969" Y="-26.530130859375" />
                  <Point X="2.871797607422" Y="-26.561751953125" />
                  <Point X="2.876786865234" Y="-26.576671875" />
                  <Point X="2.889158447266" Y="-26.605482421875" />
                  <Point X="2.896540771484" Y="-26.619373046875" />
                  <Point X="2.936150146484" Y="-26.680982421875" />
                  <Point X="2.997020996094" Y="-26.775662109375" />
                  <Point X="3.001741455078" Y="-26.7823515625" />
                  <Point X="3.0197890625" Y="-26.8044453125" />
                  <Point X="3.043486328125" Y="-26.828119140625" />
                  <Point X="3.052795654297" Y="-26.836279296875" />
                  <Point X="3.783671630859" Y="-27.3971015625" />
                  <Point X="4.087169433594" Y="-27.629984375" />
                  <Point X="4.045490722656" Y="-27.69742578125" />
                  <Point X="4.001273193359" Y="-27.76025390625" />
                  <Point X="3.377793945312" Y="-27.400287109375" />
                  <Point X="2.848454589844" Y="-27.094673828125" />
                  <Point X="2.841198730469" Y="-27.090890625" />
                  <Point X="2.815021484375" Y="-27.07951171875" />
                  <Point X="2.783117919922" Y="-27.069326171875" />
                  <Point X="2.771109375" Y="-27.066337890625" />
                  <Point X="2.685926269531" Y="-27.050953125" />
                  <Point X="2.555019042969" Y="-27.0273125" />
                  <Point X="2.539359619141" Y="-27.02580859375" />
                  <Point X="2.508008300781" Y="-27.025404296875" />
                  <Point X="2.49231640625" Y="-27.02650390625" />
                  <Point X="2.46014453125" Y="-27.0314609375" />
                  <Point X="2.444847167969" Y="-27.03513671875" />
                  <Point X="2.415069335938" Y="-27.0449609375" />
                  <Point X="2.400588867188" Y="-27.051109375" />
                  <Point X="2.329822509766" Y="-27.088353515625" />
                  <Point X="2.221070800781" Y="-27.145587890625" />
                  <Point X="2.208970214844" Y="-27.153169921875" />
                  <Point X="2.186041748047" Y="-27.170060546875" />
                  <Point X="2.175213867187" Y="-27.179369140625" />
                  <Point X="2.154253662109" Y="-27.200328125" />
                  <Point X="2.144940673828" Y="-27.21116015625" />
                  <Point X="2.128045898438" Y="-27.23409375" />
                  <Point X="2.120464111328" Y="-27.2461953125" />
                  <Point X="2.083220458984" Y="-27.3169609375" />
                  <Point X="2.025984985352" Y="-27.425712890625" />
                  <Point X="2.019836547852" Y="-27.44019140625" />
                  <Point X="2.010012573242" Y="-27.469966796875" />
                  <Point X="2.006337036133" Y="-27.485263671875" />
                  <Point X="2.001379272461" Y="-27.5174375" />
                  <Point X="2.000279174805" Y="-27.533130859375" />
                  <Point X="2.000683227539" Y="-27.564482421875" />
                  <Point X="2.002187744141" Y="-27.580140625" />
                  <Point X="2.017571289062" Y="-27.66532421875" />
                  <Point X="2.041213256836" Y="-27.796232421875" />
                  <Point X="2.043014892578" Y="-27.80422265625" />
                  <Point X="2.051236572266" Y="-27.831546875" />
                  <Point X="2.064069824219" Y="-27.862482421875" />
                  <Point X="2.069546630859" Y="-27.873580078125" />
                  <Point X="2.539208007812" Y="-28.687056640625" />
                  <Point X="2.735893310547" Y="-29.027724609375" />
                  <Point X="2.723753173828" Y="-29.036083984375" />
                  <Point X="2.237849609375" Y="-28.402841796875" />
                  <Point X="1.833914794922" Y="-27.876423828125" />
                  <Point X="1.828654663086" Y="-27.8701484375" />
                  <Point X="1.808836914062" Y="-27.8496328125" />
                  <Point X="1.783252929688" Y="-27.828005859375" />
                  <Point X="1.773297363281" Y="-27.820646484375" />
                  <Point X="1.689284057617" Y="-27.766634765625" />
                  <Point X="1.560174072266" Y="-27.68362890625" />
                  <Point X="1.54628503418" Y="-27.676248046875" />
                  <Point X="1.517471801758" Y="-27.663875" />
                  <Point X="1.502547485352" Y="-27.6588828125" />
                  <Point X="1.470927001953" Y="-27.65115234375" />
                  <Point X="1.455385131836" Y="-27.6486953125" />
                  <Point X="1.424119262695" Y="-27.64637890625" />
                  <Point X="1.408395263672" Y="-27.64651953125" />
                  <Point X="1.316512084961" Y="-27.654974609375" />
                  <Point X="1.175308227539" Y="-27.667966796875" />
                  <Point X="1.161227539062" Y="-27.67033984375" />
                  <Point X="1.133582397461" Y="-27.677169921875" />
                  <Point X="1.120017822266" Y="-27.681626953125" />
                  <Point X="1.092632324219" Y="-27.69296875" />
                  <Point X="1.079885253906" Y="-27.699408203125" />
                  <Point X="1.055497314453" Y="-27.714130859375" />
                  <Point X="1.043856323242" Y="-27.7224140625" />
                  <Point X="0.972906799316" Y="-27.781408203125" />
                  <Point X="0.86387286377" Y="-27.87206640625" />
                  <Point X="0.852649597168" Y="-27.883091796875" />
                  <Point X="0.832178710938" Y="-27.90684375" />
                  <Point X="0.822930969238" Y="-27.9195703125" />
                  <Point X="0.806038635254" Y="-27.9473984375" />
                  <Point X="0.799017822266" Y="-27.96147265625" />
                  <Point X="0.787394470215" Y="-27.99058984375" />
                  <Point X="0.782791931152" Y="-28.0056328125" />
                  <Point X="0.761578369141" Y="-28.10323046875" />
                  <Point X="0.728977661133" Y="-28.25321875" />
                  <Point X="0.727584777832" Y="-28.2612890625" />
                  <Point X="0.724724487305" Y="-28.289677734375" />
                  <Point X="0.7247421875" Y="-28.323169921875" />
                  <Point X="0.725554992676" Y="-28.33551953125" />
                  <Point X="0.833091247559" Y="-29.15233984375" />
                  <Point X="0.81297442627" Y="-29.07726171875" />
                  <Point X="0.655064941406" Y="-28.487935546875" />
                  <Point X="0.652606567383" Y="-28.480125" />
                  <Point X="0.642145629883" Y="-28.453580078125" />
                  <Point X="0.626787353516" Y="-28.423814453125" />
                  <Point X="0.620407592773" Y="-28.413208984375" />
                  <Point X="0.555866149902" Y="-28.320216796875" />
                  <Point X="0.456679992676" Y="-28.17730859375" />
                  <Point X="0.446669036865" Y="-28.165169921875" />
                  <Point X="0.42478237915" Y="-28.142712890625" />
                  <Point X="0.412906524658" Y="-28.132392578125" />
                  <Point X="0.386651550293" Y="-28.113150390625" />
                  <Point X="0.373235351562" Y="-28.10493359375" />
                  <Point X="0.345235839844" Y="-28.090828125" />
                  <Point X="0.330652832031" Y="-28.084939453125" />
                  <Point X="0.230778533936" Y="-28.053943359375" />
                  <Point X="0.07729372406" Y="-28.006306640625" />
                  <Point X="0.063380096436" Y="-28.003111328125" />
                  <Point X="0.03522794342" Y="-27.998841796875" />
                  <Point X="0.020989425659" Y="-27.997767578125" />
                  <Point X="-0.008651926041" Y="-27.997765625" />
                  <Point X="-0.022895795822" Y="-27.998837890625" />
                  <Point X="-0.051062213898" Y="-28.003107421875" />
                  <Point X="-0.064984611511" Y="-28.0063046875" />
                  <Point X="-0.164858901978" Y="-28.037302734375" />
                  <Point X="-0.318343719482" Y="-28.0849375" />
                  <Point X="-0.332928375244" Y="-28.090828125" />
                  <Point X="-0.360929077148" Y="-28.104935546875" />
                  <Point X="-0.374345123291" Y="-28.11315234375" />
                  <Point X="-0.400600524902" Y="-28.132396484375" />
                  <Point X="-0.412474761963" Y="-28.14271484375" />
                  <Point X="-0.434358581543" Y="-28.165169921875" />
                  <Point X="-0.444368041992" Y="-28.177306640625" />
                  <Point X="-0.508909637451" Y="-28.270298828125" />
                  <Point X="-0.60809564209" Y="-28.41320703125" />
                  <Point X="-0.612471252441" Y="-28.420130859375" />
                  <Point X="-0.625977233887" Y="-28.445263671875" />
                  <Point X="-0.638778076172" Y="-28.47621484375" />
                  <Point X="-0.642752990723" Y="-28.487935546875" />
                  <Point X="-0.885424804688" Y="-29.393599609375" />
                  <Point X="-0.985424987793" Y="-29.7668046875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.75260341899" Y="-20.521607628522" />
                  <Point X="0.36174689643" Y="-20.276362097301" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.121771709388" Y="-20.683167486671" />
                  <Point X="0.337065154176" Y="-20.36847556834" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.357572496632" Y="-20.245992208686" />
                  <Point X="-0.457946654969" Y="-20.228293536405" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.395613730812" Y="-20.827918751629" />
                  <Point X="0.312383460967" Y="-20.460589048026" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.330442868801" Y="-20.347241422178" />
                  <Point X="-0.786768035257" Y="-20.266778983356" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.632178655547" Y="-20.966097058678" />
                  <Point X="0.287701767758" Y="-20.552702527713" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.30331324097" Y="-20.448490635671" />
                  <Point X="-1.049634466953" Y="-20.316894067255" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.812548690291" Y="-21.094366690444" />
                  <Point X="0.263020074548" Y="-20.6448160074" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.276183653341" Y="-20.549739842074" />
                  <Point X="-1.260864655877" Y="-20.376114013936" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.768695795526" Y="-21.183099770044" />
                  <Point X="0.238338381339" Y="-20.736929487087" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.249054068015" Y="-20.650989048072" />
                  <Point X="-1.472092352713" Y="-20.43533440004" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.718147342151" Y="-21.27065224201" />
                  <Point X="0.191582642349" Y="-20.825150716929" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.220584868126" Y="-20.752474464261" />
                  <Point X="-1.466705887564" Y="-20.532749707306" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.667598888776" Y="-21.358204713976" />
                  <Point X="0.079607436811" Y="-20.901871995151" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.142999098733" Y="-20.862620456853" />
                  <Point X="-1.465906380955" Y="-20.629356210022" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.617050435401" Y="-21.445757185942" />
                  <Point X="-1.491825334221" Y="-20.721251527378" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.566501982027" Y="-21.533309657908" />
                  <Point X="-1.527808121312" Y="-20.811372319302" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.515953528652" Y="-21.620862129874" />
                  <Point X="-1.607911587718" Y="-20.893713445056" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.140233556116" Y="-20.799850719603" />
                  <Point X="-2.27303443305" Y="-20.776434341938" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.465405075277" Y="-21.70841460184" />
                  <Point X="-1.768500644012" Y="-20.961862789754" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.982915441623" Y="-20.924055675872" />
                  <Point X="-2.404835488294" Y="-20.849659787942" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.414856621902" Y="-21.795967073806" />
                  <Point X="-2.536636150568" Y="-20.922885303237" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.364308196543" Y="-21.883519550712" />
                  <Point X="-2.66394469968" Y="-20.996902899282" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.972416798411" Y="-22.26353801326" />
                  <Point X="3.888861442031" Y="-22.248804949547" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.313759778002" Y="-21.97107202882" />
                  <Point X="-2.766238808963" Y="-21.075331215977" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.05049643763" Y="-22.373771088427" />
                  <Point X="3.7608685365" Y="-22.322701875092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.26321135946" Y="-22.058624506928" />
                  <Point X="-2.868532918246" Y="-21.153759532672" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.115844672486" Y="-22.481759273503" />
                  <Point X="3.632875630969" Y="-22.396598800637" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.212662940919" Y="-22.146176985037" />
                  <Point X="-2.97082702753" Y="-21.232187849368" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.067573693975" Y="-22.569713325736" />
                  <Point X="3.504882617936" Y="-22.470495707226" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.162114522377" Y="-22.233729463145" />
                  <Point X="-3.051060662349" Y="-21.314506022918" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.965347808109" Y="-22.64815367206" />
                  <Point X="3.376889321907" Y="-22.544392563916" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.111566103836" Y="-22.321281941253" />
                  <Point X="-2.989053227843" Y="-21.421905134755" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.863121922242" Y="-22.726594018384" />
                  <Point X="3.248896025877" Y="-22.618289420605" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.061432203022" Y="-22.40890751002" />
                  <Point X="-2.92704640821" Y="-21.529304138173" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.760896036375" Y="-22.805034364708" />
                  <Point X="3.120902729847" Y="-22.692186277294" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.030467976459" Y="-22.49991320957" />
                  <Point X="-2.865039588578" Y="-21.636703141591" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.658670150508" Y="-22.883474711032" />
                  <Point X="2.992909433817" Y="-22.766083133983" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.013319750146" Y="-22.593355042729" />
                  <Point X="-2.803032768945" Y="-21.74410214501" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.556444515334" Y="-22.96191510156" />
                  <Point X="2.864916137787" Y="-22.839979990673" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.020731322942" Y="-22.691127431111" />
                  <Point X="-2.760063322333" Y="-21.848144345923" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.454219031817" Y="-23.040355518829" />
                  <Point X="2.736922841757" Y="-22.913876847362" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.054316270157" Y="-22.79351489158" />
                  <Point X="-2.749597537699" Y="-21.946455274257" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.351993548299" Y="-23.118795936098" />
                  <Point X="2.604324488158" Y="-22.986961708154" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.135266186429" Y="-22.904254074034" />
                  <Point X="-2.757594209423" Y="-22.041510773405" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.249768064781" Y="-23.197236353367" />
                  <Point X="-2.804235602137" Y="-22.129752165581" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.72091563881" Y="-21.968116742439" />
                  <Point X="-3.727825222745" Y="-21.966898396366" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.150751678309" Y="-23.276242621029" />
                  <Point X="-2.884595286403" Y="-22.212048113213" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.480368582079" Y="-22.1069972068" />
                  <Point X="-3.793821330538" Y="-22.051727030069" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.082510507806" Y="-23.360675389603" />
                  <Point X="-3.066583392608" Y="-22.27642422805" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.234696911901" Y="-22.246781278777" />
                  <Point X="-3.859817283052" Y="-22.136555691153" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.033321784795" Y="-23.448467618719" />
                  <Point X="-3.925812456945" Y="-22.221384489528" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.007305815657" Y="-23.54034582956" />
                  <Point X="-3.991807630839" Y="-22.306213287903" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.716768236186" Y="-23.938235704935" />
                  <Point X="4.550707044626" Y="-23.908954636415" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.003062562472" Y="-23.636063157666" />
                  <Point X="-4.04523452764" Y="-22.39325821263" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.741305510708" Y="-24.039027816596" />
                  <Point X="4.237489234337" Y="-23.950191413751" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.023138400286" Y="-23.736068597662" />
                  <Point X="-4.096286463222" Y="-22.480721907099" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.760965293203" Y="-24.138959894814" />
                  <Point X="3.924267032951" Y="-23.991427416819" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.073301857371" Y="-23.841379296721" />
                  <Point X="-4.147338398803" Y="-22.568185601568" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.776408431506" Y="-24.238148464892" />
                  <Point X="3.611044831565" Y="-24.032663419887" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.170538623127" Y="-23.95499029017" />
                  <Point X="-4.198390334384" Y="-22.655649296037" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.671658425578" Y="-24.316143740747" />
                  <Point X="-4.157828809928" Y="-22.759266915306" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.454530547525" Y="-24.374323765711" />
                  <Point X="-3.99460354281" Y="-22.884513461962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.237402669472" Y="-24.432503790676" />
                  <Point X="-3.831379509468" Y="-23.009759791069" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.020273462402" Y="-24.490683581299" />
                  <Point X="-3.668155476126" Y="-23.135006120176" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.803144017778" Y="-24.548863330034" />
                  <Point X="-3.514498429584" Y="-23.258565531387" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.617007426462" Y="-24.612507955017" />
                  <Point X="-3.447764481763" Y="-23.366798055046" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.500131080129" Y="-24.688365029881" />
                  <Point X="-3.421425125583" Y="-23.467907922324" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.428512471299" Y="-24.772202264953" />
                  <Point X="-3.436242155294" Y="-23.561760808341" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.379088132967" Y="-24.859952948731" />
                  <Point X="-3.475242186487" Y="-23.651349578723" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.358269204671" Y="-24.952747538092" />
                  <Point X="-3.550492511798" Y="-23.734546444193" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.349574427785" Y="-25.047679942465" />
                  <Point X="-3.751701249832" Y="-23.795533443052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.365378198048" Y="-25.146932101688" />
                  <Point X="-4.647741842774" Y="-23.734002838835" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.40039814889" Y="-25.249572592014" />
                  <Point X="-4.672690249015" Y="-23.826069289818" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.486365835369" Y="-25.361196542738" />
                  <Point X="-4.697638554279" Y="-23.918135758607" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.801177025867" Y="-25.513171777581" />
                  <Point X="-4.722586856577" Y="-24.010202227918" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.783946127519" Y="-25.782926014138" />
                  <Point X="-4.740366854419" Y="-24.10353266271" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.769779216458" Y="-25.876893533614" />
                  <Point X="-4.754278297564" Y="-24.197545228073" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.752648874135" Y="-25.970338520203" />
                  <Point X="-4.768189740709" Y="-24.291557793435" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.731487128023" Y="-26.063072661533" />
                  <Point X="-4.782101183854" Y="-24.385570358797" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.344175642664" Y="-25.914917744147" />
                  <Point X="-3.985720309382" Y="-24.622459322016" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.121301826728" Y="-25.972084605233" />
                  <Point X="-3.381204423591" Y="-24.825517311077" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.024210780416" Y="-26.051430362312" />
                  <Point X="-3.312933627732" Y="-24.93402082251" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.954258461663" Y="-26.135561409282" />
                  <Point X="-3.294023046694" Y="-25.033820796297" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.896878518894" Y="-26.22190930535" />
                  <Point X="-3.312291552622" Y="-25.127065093934" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.873537746423" Y="-26.314259225542" />
                  <Point X="-3.360085417716" Y="-25.215103274135" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.864802757833" Y="-26.409184539506" />
                  <Point X="-3.462875781221" Y="-25.293444087821" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.860893458824" Y="-26.504960752744" />
                  <Point X="-3.665235967761" Y="-25.354228055242" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.889702486745" Y="-26.606506089784" />
                  <Point X="-3.882365087569" Y="-25.412407861252" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.958033506764" Y="-26.715020220362" />
                  <Point X="-4.099494207971" Y="-25.470587667156" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.041583101386" Y="-26.82621779625" />
                  <Point X="-4.316623348276" Y="-25.528767469552" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.202340180112" Y="-26.951029134698" />
                  <Point X="-4.53375248858" Y="-25.586947271947" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.365563891458" Y="-27.076275407029" />
                  <Point X="-4.750881628885" Y="-25.645127074342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.528787602803" Y="-27.20152167936" />
                  <Point X="2.788112016945" Y="-27.070920589621" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.531709697177" Y="-27.02570994273" />
                  <Point X="-4.774617018709" Y="-25.737407412848" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.692011314149" Y="-27.326767951691" />
                  <Point X="3.056304567811" Y="-27.214675700493" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.332350465451" Y="-27.087023059452" />
                  <Point X="-3.093285262409" Y="-26.130337093135" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.255715238689" Y="-26.101696305841" />
                  <Point X="-4.760463327219" Y="-25.836368618663" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.855234955802" Y="-27.452014211733" />
                  <Point X="3.29685407507" Y="-27.353556596948" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.199590737061" Y="-27.160079465515" />
                  <Point X="-2.971788788169" Y="-26.248225727733" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.568936349768" Y="-26.142932501159" />
                  <Point X="-4.746309718421" Y="-25.935329809898" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.01845850819" Y="-27.577260456035" />
                  <Point X="3.5374023119" Y="-27.492437269392" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.122496581474" Y="-27.242951213959" />
                  <Point X="-2.948879056294" Y="-26.348730859713" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.882157460847" Y="-26.184168696478" />
                  <Point X="-4.725073405327" Y="-26.035539872996" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.056041730914" Y="-27.680352920352" />
                  <Point X="3.777949904481" Y="-27.631317828238" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.075741015895" Y="-27.331172474378" />
                  <Point X="-2.961705034677" Y="-26.442934821799" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.195378571926" Y="-26.225404891796" />
                  <Point X="-4.699270902842" Y="-26.136555078484" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.029283096124" Y="-27.419446217784" />
                  <Point X="-3.007899607414" Y="-26.531255000392" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.508601908682" Y="-26.266640694668" />
                  <Point X="-4.673468400356" Y="-26.237570283971" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.002346274606" Y="-27.511162057505" />
                  <Point X="-3.097656907752" Y="-26.611893894756" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.007309745432" Y="-27.608502779459" />
                  <Point X="-3.199882660057" Y="-26.690334264631" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.025304015211" Y="-27.708141182848" />
                  <Point X="-3.302108412363" Y="-26.768774634505" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.044129236331" Y="-27.807926105379" />
                  <Point X="1.643681188288" Y="-27.737316310137" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.224638996367" Y="-27.663427865646" />
                  <Point X="-3.404334164668" Y="-26.84721500438" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.092232769671" Y="-27.912873584303" />
                  <Point X="1.824287637162" Y="-27.865627628092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.03829733454" Y="-27.727036331165" />
                  <Point X="-3.506559916973" Y="-26.925655374254" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.154239684207" Y="-28.020272604455" />
                  <Point X="1.911443418087" Y="-27.977461071923" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.942579651316" Y="-27.806624249211" />
                  <Point X="-3.608785549846" Y="-27.004095765188" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.216246598744" Y="-28.127671624608" />
                  <Point X="1.997046127649" Y="-28.08902066737" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.849551538709" Y="-27.886686411123" />
                  <Point X="-3.711011043535" Y="-27.082536180664" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.27825351328" Y="-28.23507064476" />
                  <Point X="2.082648837211" Y="-28.200580262817" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.794255798645" Y="-27.97340180836" />
                  <Point X="-2.406445455577" Y="-27.409031820054" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.654202064103" Y="-27.365345645322" />
                  <Point X="-3.813236537223" Y="-27.160976596139" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.340260427816" Y="-28.342469664913" />
                  <Point X="2.168251546773" Y="-28.312139858263" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.769768584973" Y="-28.065549580037" />
                  <Point X="-2.32700791773" Y="-27.519504329386" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.789056342319" Y="-27.438032725737" />
                  <Point X="-3.915462030911" Y="-27.239417011615" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.402267342352" Y="-28.449868685065" />
                  <Point X="2.253854242001" Y="-28.423699451182" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.74957515623" Y="-28.158454461845" />
                  <Point X="0.328024200873" Y="-28.084123654673" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.08249221712" Y="-28.011738534157" />
                  <Point X="-2.311620796529" Y="-27.618683022139" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.917049400826" Y="-27.511929624308" />
                  <Point X="-4.017687524599" Y="-27.317857427091" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.464274256889" Y="-28.557267705217" />
                  <Point X="2.339456874891" Y="-28.53525903311" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.729381809326" Y="-28.251359358085" />
                  <Point X="0.477214960899" Y="-28.206895539067" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.280700195775" Y="-28.073254647857" />
                  <Point X="-2.335273571943" Y="-27.710977927794" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.045042459332" Y="-27.585826522879" />
                  <Point X="-4.119913018287" Y="-27.396297842566" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.526281171425" Y="-28.66466672537" />
                  <Point X="2.425059507782" Y="-28.646818615037" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.727122575169" Y="-28.347426522276" />
                  <Point X="0.553503597137" Y="-28.316812812086" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.415610010831" Y="-28.14593193563" />
                  <Point X="-2.384576833001" Y="-27.798749960761" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.173035517839" Y="-27.65972342145" />
                  <Point X="-4.15556031814" Y="-27.486477789942" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.588288187282" Y="-28.772065763388" />
                  <Point X="2.510662140673" Y="-28.758378196965" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.740124301814" Y="-28.446184605609" />
                  <Point X="0.628141564667" Y="-28.426439027676" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.48148301773" Y="-28.230782275342" />
                  <Point X="-2.435125212643" Y="-27.886302445728" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.301028576345" Y="-27.733620320021" />
                  <Point X="-4.091272063375" Y="-27.594279071929" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.650295229825" Y="-28.879464806111" />
                  <Point X="2.596264773563" Y="-28.869937778892" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.753126028458" Y="-28.544942688941" />
                  <Point X="0.666234630627" Y="-28.529621391112" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.541135056691" Y="-28.316729539548" />
                  <Point X="-2.485673592284" Y="-27.973854930696" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.429021634852" Y="-27.807517218593" />
                  <Point X="-4.026984285677" Y="-27.702080269796" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.712302272369" Y="-28.986863848835" />
                  <Point X="2.681867406454" Y="-28.98149736082" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.766127755103" Y="-28.643700772273" />
                  <Point X="0.693364278817" Y="-28.630870608194" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.600787095937" Y="-28.402676803704" />
                  <Point X="-2.536221971926" Y="-28.061407415663" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.557014522794" Y="-27.881414147239" />
                  <Point X="-3.949506346809" Y="-27.812207248957" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.779129481748" Y="-28.742458855606" />
                  <Point X="0.720493927008" Y="-28.732119825276" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.643727068344" Y="-28.491570856147" />
                  <Point X="-2.586770351568" Y="-28.14895990063" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.685007336596" Y="-27.955311088958" />
                  <Point X="-3.864698989119" Y="-27.92362660241" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.792131208392" Y="-28.841216938938" />
                  <Point X="0.747623575198" Y="-28.833369042358" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.66840878545" Y="-28.58368433162" />
                  <Point X="-2.63731873028" Y="-28.236512385761" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.805132935037" Y="-28.939975022271" />
                  <Point X="0.774753223388" Y="-28.934618259441" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.693090502556" Y="-28.675797807093" />
                  <Point X="-2.687867090123" Y="-28.324064874219" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.818134661682" Y="-29.038733105603" />
                  <Point X="0.801882871578" Y="-29.035867476523" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.717772219661" Y="-28.767911282566" />
                  <Point X="-2.738415449966" Y="-28.411617362678" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.831136388327" Y="-29.137491188935" />
                  <Point X="0.829012265948" Y="-29.13711664885" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.742453936767" Y="-28.86002475804" />
                  <Point X="-2.788963809809" Y="-28.499169851136" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.767135653873" Y="-28.952138233513" />
                  <Point X="-1.582082897334" Y="-28.808441046637" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.660466393431" Y="-28.794619921432" />
                  <Point X="-2.839512169652" Y="-28.586722339594" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.791817370979" Y="-29.044251708986" />
                  <Point X="-1.401244407633" Y="-28.936793279651" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.09349388931" Y="-28.81473101865" />
                  <Point X="-2.890060529495" Y="-28.674274828052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.816499088085" Y="-29.136365184459" />
                  <Point X="-1.263563642274" Y="-29.057535641437" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.207980180799" Y="-28.891009524668" />
                  <Point X="-2.940608889338" Y="-28.761827316511" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.841180805191" Y="-29.228478659932" />
                  <Point X="-1.192626251877" Y="-29.166509345434" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.322207643764" Y="-28.967333669138" />
                  <Point X="-2.947767766467" Y="-28.857030541451" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.865862522297" Y="-29.320592135405" />
                  <Point X="-1.16920047395" Y="-29.267105470256" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.414935250932" Y="-29.047448818267" />
                  <Point X="-2.785268034522" Y="-28.982149156679" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.890544251213" Y="-29.412705608796" />
                  <Point X="-1.149314822539" Y="-29.367077375258" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.480134726879" Y="-29.132417919659" />
                  <Point X="-2.580265373804" Y="-29.11476218501" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.915226025256" Y="-29.50481907423" />
                  <Point X="-1.129429214634" Y="-29.467049272589" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.939907799298" Y="-29.596932539663" />
                  <Point X="-1.120009868131" Y="-29.565175685647" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.964589573341" Y="-29.689046005097" />
                  <Point X="-1.131797876047" Y="-29.659562669932" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="-24.998375" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978484375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.629448486328" Y="-29.1264375" />
                  <Point X="0.471539031982" Y="-28.537111328125" />
                  <Point X="0.464318664551" Y="-28.52154296875" />
                  <Point X="0.399777099609" Y="-28.42855078125" />
                  <Point X="0.300591033936" Y="-28.285642578125" />
                  <Point X="0.27433605957" Y="-28.266400390625" />
                  <Point X="0.174461669922" Y="-28.235404296875" />
                  <Point X="0.020976930618" Y="-28.187767578125" />
                  <Point X="-0.008664452553" Y="-28.187765625" />
                  <Point X="-0.108538841248" Y="-28.218763671875" />
                  <Point X="-0.262023590088" Y="-28.2663984375" />
                  <Point X="-0.288279144287" Y="-28.285642578125" />
                  <Point X="-0.352820739746" Y="-28.378634765625" />
                  <Point X="-0.45200680542" Y="-28.52154296875" />
                  <Point X="-0.459227142334" Y="-28.537111328125" />
                  <Point X="-0.701898864746" Y="-29.442775390625" />
                  <Point X="-0.847744140625" Y="-29.987078125" />
                  <Point X="-0.920139831543" Y="-29.973025390625" />
                  <Point X="-1.10023059082" Y="-29.938068359375" />
                  <Point X="-1.215351074219" Y="-29.90844921875" />
                  <Point X="-1.351589355469" Y="-29.873396484375" />
                  <Point X="-1.328158813477" Y="-29.695423828125" />
                  <Point X="-1.309150146484" Y="-29.551041015625" />
                  <Point X="-1.309683227539" Y="-29.534759765625" />
                  <Point X="-1.333543212891" Y="-29.414806640625" />
                  <Point X="-1.370210693359" Y="-29.230466796875" />
                  <Point X="-1.386282592773" Y="-29.20262890625" />
                  <Point X="-1.47823425293" Y="-29.121990234375" />
                  <Point X="-1.619543457031" Y="-28.998064453125" />
                  <Point X="-1.649240600586" Y="-28.985763671875" />
                  <Point X="-1.771280883789" Y="-28.977765625" />
                  <Point X="-1.958829589844" Y="-28.96547265625" />
                  <Point X="-1.989878417969" Y="-28.973791015625" />
                  <Point X="-2.091569091797" Y="-29.04173828125" />
                  <Point X="-2.247844970703" Y="-29.146158203125" />
                  <Point X="-2.259734130859" Y="-29.157294921875" />
                  <Point X="-2.395975585938" Y="-29.33484765625" />
                  <Point X="-2.457094726562" Y="-29.4145" />
                  <Point X="-2.591835449219" Y="-29.331072265625" />
                  <Point X="-2.855838134766" Y="-29.167609375" />
                  <Point X="-3.015221435547" Y="-29.044888671875" />
                  <Point X="-3.228581054688" Y="-28.880609375" />
                  <Point X="-2.799490722656" Y="-28.13740234375" />
                  <Point X="-2.506033691406" Y="-27.629119140625" />
                  <Point X="-2.499762695312" Y="-27.59759375" />
                  <Point X="-2.513979736328" Y="-27.568763671875" />
                  <Point X="-2.531328369141" Y="-27.551416015625" />
                  <Point X="-2.560157226562" Y="-27.53719921875" />
                  <Point X="-2.59168359375" Y="-27.543470703125" />
                  <Point X="-3.372800537109" Y="-27.99444921875" />
                  <Point X="-3.842958984375" Y="-28.26589453125" />
                  <Point X="-3.953130371094" Y="-28.12115234375" />
                  <Point X="-4.161704101562" Y="-27.84712890625" />
                  <Point X="-4.275971679688" Y="-27.65551953125" />
                  <Point X="-4.431020019531" Y="-27.39552734375" />
                  <Point X="-3.677241210938" Y="-26.8171328125" />
                  <Point X="-3.163786865234" Y="-26.423146484375" />
                  <Point X="-3.145822021484" Y="-26.396015625" />
                  <Point X="-3.138117431641" Y="-26.366267578125" />
                  <Point X="-3.140326660156" Y="-26.33459375" />
                  <Point X="-3.161160888672" Y="-26.310638671875" />
                  <Point X="-3.187643554688" Y="-26.295052734375" />
                  <Point X="-3.219529052734" Y="-26.288572265625" />
                  <Point X="-4.205615722656" Y="-26.418392578125" />
                  <Point X="-4.803283203125" Y="-26.497076171875" />
                  <Point X="-4.845818359375" Y="-26.330552734375" />
                  <Point X="-4.927393554688" Y="-26.011189453125" />
                  <Point X="-4.957625488281" Y="-25.799810546875" />
                  <Point X="-4.998396484375" Y="-25.514744140625" />
                  <Point X="-4.142365722656" Y="-25.28537109375" />
                  <Point X="-3.557463867188" Y="-25.128646484375" />
                  <Point X="-3.530957275391" Y="-25.11383203125" />
                  <Point X="-3.51414453125" Y="-25.1021640625" />
                  <Point X="-3.502324951172" Y="-25.090646484375" />
                  <Point X="-3.491252197266" Y="-25.064158203125" />
                  <Point X="-3.483400878906" Y="-25.03128125" />
                  <Point X="-3.489294189453" Y="-25.0047109375" />
                  <Point X="-3.502325195312" Y="-24.971912109375" />
                  <Point X="-3.525086669922" Y="-24.952802734375" />
                  <Point X="-3.541894287109" Y="-24.94113671875" />
                  <Point X="-3.557463867188" Y="-24.9339140625" />
                  <Point X="-4.456328125" Y="-24.693064453125" />
                  <Point X="-4.998186523438" Y="-24.547873046875" />
                  <Point X="-4.97021875" Y="-24.358869140625" />
                  <Point X="-4.917645507812" Y="-24.003583984375" />
                  <Point X="-4.856787597656" Y="-23.779" />
                  <Point X="-4.773515625" Y="-23.471701171875" />
                  <Point X="-4.172538574219" Y="-23.550822265625" />
                  <Point X="-3.753266113281" Y="-23.60601953125" />
                  <Point X="-3.731705322266" Y="-23.6041328125" />
                  <Point X="-3.707491210938" Y="-23.596498046875" />
                  <Point X="-3.670279296875" Y="-23.584765625" />
                  <Point X="-3.651534912109" Y="-23.5739453125" />
                  <Point X="-3.639120117188" Y="-23.55621484375" />
                  <Point X="-3.629404052734" Y="-23.5327578125" />
                  <Point X="-3.61447265625" Y="-23.4967109375" />
                  <Point X="-3.610714111328" Y="-23.47539453125" />
                  <Point X="-3.61631640625" Y="-23.45448828125" />
                  <Point X="-3.628039794922" Y="-23.431966796875" />
                  <Point X="-3.646056152344" Y="-23.397359375" />
                  <Point X="-3.659968261719" Y="-23.380779296875" />
                  <Point X="-4.175559082031" Y="-22.98515234375" />
                  <Point X="-4.47610546875" Y="-22.75453515625" />
                  <Point X="-4.364296386719" Y="-22.562978515625" />
                  <Point X="-4.160016113281" Y="-22.212998046875" />
                  <Point X="-3.998806152344" Y="-22.005783203125" />
                  <Point X="-3.774670654297" Y="-21.717689453125" />
                  <Point X="-3.416662109375" Y="-21.92438671875" />
                  <Point X="-3.159156982422" Y="-22.073056640625" />
                  <Point X="-3.138513916016" Y="-22.07956640625" />
                  <Point X="-3.104790283203" Y="-22.082517578125" />
                  <Point X="-3.052964599609" Y="-22.08705078125" />
                  <Point X="-3.031506591797" Y="-22.084224609375" />
                  <Point X="-3.013252929688" Y="-22.072595703125" />
                  <Point X="-2.989315673828" Y="-22.048658203125" />
                  <Point X="-2.952529296875" Y="-22.011873046875" />
                  <Point X="-2.940899414062" Y="-21.993619140625" />
                  <Point X="-2.93807421875" Y="-21.97216015625" />
                  <Point X="-2.941024658203" Y="-21.938435546875" />
                  <Point X="-2.945558837891" Y="-21.886609375" />
                  <Point X="-2.952067382812" Y="-21.865966796875" />
                  <Point X="-3.180541259766" Y="-21.47023828125" />
                  <Point X="-3.307278564453" Y="-21.250724609375" />
                  <Point X="-3.108669677734" Y="-21.098453125" />
                  <Point X="-2.752873291016" Y="-20.825666015625" />
                  <Point X="-2.498978027344" Y="-20.684607421875" />
                  <Point X="-2.141548583984" Y="-20.48602734375" />
                  <Point X="-2.046656860352" Y="-20.609693359375" />
                  <Point X="-1.967826660156" Y="-20.71242578125" />
                  <Point X="-1.951246582031" Y="-20.726337890625" />
                  <Point X="-1.913712524414" Y="-20.74587890625" />
                  <Point X="-1.856030639648" Y="-20.77590625" />
                  <Point X="-1.835123779297" Y="-20.781509765625" />
                  <Point X="-1.813808349609" Y="-20.77775" />
                  <Point X="-1.774713867188" Y="-20.761556640625" />
                  <Point X="-1.714634399414" Y="-20.736669921875" />
                  <Point X="-1.696905151367" Y="-20.724255859375" />
                  <Point X="-1.686083496094" Y="-20.70551171875" />
                  <Point X="-1.673358886719" Y="-20.665154296875" />
                  <Point X="-1.653804077148" Y="-20.603134765625" />
                  <Point X="-1.651917602539" Y="-20.58157421875" />
                  <Point X="-1.677892578125" Y="-20.384275390625" />
                  <Point X="-1.689137695313" Y="-20.298861328125" />
                  <Point X="-1.428691040039" Y="-20.22583984375" />
                  <Point X="-0.968083007812" Y="-20.096703125" />
                  <Point X="-0.660293701172" Y="-20.0606796875" />
                  <Point X="-0.224200012207" Y="-20.009640625" />
                  <Point X="-0.118317489624" Y="-20.40480078125" />
                  <Point X="-0.042140380859" Y="-20.68909765625" />
                  <Point X="-0.024282114029" Y="-20.71582421875" />
                  <Point X="0.006155934334" Y="-20.72615625" />
                  <Point X="0.036594032288" Y="-20.71582421875" />
                  <Point X="0.054452251434" Y="-20.68909765625" />
                  <Point X="0.171519744873" Y="-20.2521953125" />
                  <Point X="0.236648361206" Y="-20.0091328125" />
                  <Point X="0.458036865234" Y="-20.032318359375" />
                  <Point X="0.860210266113" Y="-20.074435546875" />
                  <Point X="1.114858032227" Y="-20.135916015625" />
                  <Point X="1.508456298828" Y="-20.230943359375" />
                  <Point X="1.674284423828" Y="-20.29108984375" />
                  <Point X="1.931044433594" Y="-20.38421875" />
                  <Point X="2.091310791016" Y="-20.459169921875" />
                  <Point X="2.338684814453" Y="-20.574859375" />
                  <Point X="2.493536376953" Y="-20.66507421875" />
                  <Point X="2.732520019531" Y="-20.804306640625" />
                  <Point X="2.878552734375" Y="-20.908158203125" />
                  <Point X="3.068739990234" Y="-21.043408203125" />
                  <Point X="2.5695078125" Y="-21.908103515625" />
                  <Point X="2.229853515625" Y="-22.49640234375" />
                  <Point X="2.224852050781" Y="-22.508486328125" />
                  <Point X="2.216388671875" Y="-22.540134765625" />
                  <Point X="2.203382324219" Y="-22.5887734375" />
                  <Point X="2.202044677734" Y="-22.60767578125" />
                  <Point X="2.205344726562" Y="-22.63504296875" />
                  <Point X="2.210416015625" Y="-22.677099609375" />
                  <Point X="2.218682373047" Y="-22.6991875" />
                  <Point X="2.235616455078" Y="-22.72414453125" />
                  <Point X="2.261640136719" Y="-22.76249609375" />
                  <Point X="2.274938720703" Y="-22.775794921875" />
                  <Point X="2.299895019531" Y="-22.792728515625" />
                  <Point X="2.338247314453" Y="-22.81875390625" />
                  <Point X="2.3603359375" Y="-22.827021484375" />
                  <Point X="2.387703369141" Y="-22.8303203125" />
                  <Point X="2.429760742188" Y="-22.835392578125" />
                  <Point X="2.4486640625" Y="-22.8340546875" />
                  <Point X="2.480312988281" Y="-22.82558984375" />
                  <Point X="2.528950439453" Y="-22.812583984375" />
                  <Point X="2.541033935547" Y="-22.80758203125" />
                  <Point X="3.445118408203" Y="-22.285609375" />
                  <Point X="3.994247802734" Y="-21.9685703125" />
                  <Point X="4.060832519531" Y="-22.061107421875" />
                  <Point X="4.202591308594" Y="-22.258119140625" />
                  <Point X="4.284000976562" Y="-22.392650390625" />
                  <Point X="4.387512695312" Y="-22.563705078125" />
                  <Point X="3.735802978516" Y="-23.063779296875" />
                  <Point X="3.288616210938" Y="-23.40691796875" />
                  <Point X="3.279371337891" Y="-23.41616796875" />
                  <Point X="3.256593505859" Y="-23.4458828125" />
                  <Point X="3.221589111328" Y="-23.491548828125" />
                  <Point X="3.213119384766" Y="-23.5085" />
                  <Point X="3.204634521484" Y="-23.53883984375" />
                  <Point X="3.191595458984" Y="-23.58546484375" />
                  <Point X="3.190779541016" Y="-23.60903515625" />
                  <Point X="3.197744628906" Y="-23.642791015625" />
                  <Point X="3.208448486328" Y="-23.69466796875" />
                  <Point X="3.215646728516" Y="-23.712046875" />
                  <Point X="3.234591064453" Y="-23.740841796875" />
                  <Point X="3.263704345703" Y="-23.785091796875" />
                  <Point X="3.280947021484" Y="-23.8011796875" />
                  <Point X="3.308399902344" Y="-23.8166328125" />
                  <Point X="3.350589111328" Y="-23.8403828125" />
                  <Point X="3.368565429688" Y="-23.846380859375" />
                  <Point X="3.40568359375" Y="-23.85128515625" />
                  <Point X="3.462726074219" Y="-23.85882421875" />
                  <Point X="3.475803710938" Y="-23.858828125" />
                  <Point X="4.334623535156" Y="-23.745763671875" />
                  <Point X="4.848975585938" Y="-23.678046875" />
                  <Point X="4.878303710938" Y="-23.79851953125" />
                  <Point X="4.939188476562" Y="-24.04861328125" />
                  <Point X="4.964842773438" Y="-24.213388671875" />
                  <Point X="4.997858398438" Y="-24.425443359375" />
                  <Point X="4.253653320312" Y="-24.624853515625" />
                  <Point X="3.741167724609" Y="-24.762173828125" />
                  <Point X="3.729087646484" Y="-24.767181640625" />
                  <Point X="3.692620117188" Y="-24.788259765625" />
                  <Point X="3.636577636719" Y="-24.82065234375" />
                  <Point X="3.622264648438" Y="-24.83307421875" />
                  <Point X="3.600384277344" Y="-24.860955078125" />
                  <Point X="3.566758789062" Y="-24.903802734375" />
                  <Point X="3.556985107422" Y="-24.925265625" />
                  <Point X="3.54969140625" Y="-24.963349609375" />
                  <Point X="3.538482910156" Y="-25.021876953125" />
                  <Point X="3.538483154297" Y="-25.040685546875" />
                  <Point X="3.545776855469" Y="-25.07876953125" />
                  <Point X="3.556985351562" Y="-25.137294921875" />
                  <Point X="3.566758789062" Y="-25.1587578125" />
                  <Point X="3.588639160156" Y="-25.186638671875" />
                  <Point X="3.622264648438" Y="-25.229486328125" />
                  <Point X="3.636576416016" Y="-25.241908203125" />
                  <Point X="3.673043945312" Y="-25.262986328125" />
                  <Point X="3.729086425781" Y="-25.295380859375" />
                  <Point X="3.741167724609" Y="-25.300388671875" />
                  <Point X="4.528744628906" Y="-25.51141796875" />
                  <Point X="4.998067871094" Y="-25.637173828125" />
                  <Point X="4.982427246094" Y="-25.740916015625" />
                  <Point X="4.948431640625" Y="-25.9663984375" />
                  <Point X="4.915564453125" Y="-26.1104296875" />
                  <Point X="4.874545410156" Y="-26.290177734375" />
                  <Point X="4.006321533203" Y="-26.175875" />
                  <Point X="3.411982177734" Y="-26.09762890625" />
                  <Point X="3.394836425781" Y="-26.098341796875" />
                  <Point X="3.323263671875" Y="-26.1138984375" />
                  <Point X="3.213272216797" Y="-26.1378046875" />
                  <Point X="3.185445556641" Y="-26.154697265625" />
                  <Point X="3.142184326172" Y="-26.206728515625" />
                  <Point X="3.075701416016" Y="-26.286685546875" />
                  <Point X="3.064357910156" Y="-26.3140703125" />
                  <Point X="3.058157470703" Y="-26.381451171875" />
                  <Point X="3.04862890625" Y="-26.485001953125" />
                  <Point X="3.056360595703" Y="-26.516623046875" />
                  <Point X="3.095969970703" Y="-26.578232421875" />
                  <Point X="3.156840820312" Y="-26.672912109375" />
                  <Point X="3.168460693359" Y="-26.685541015625" />
                  <Point X="3.899336669922" Y="-27.24636328125" />
                  <Point X="4.33907421875" Y="-27.58378515625" />
                  <Point X="4.299961425781" Y="-27.64707421875" />
                  <Point X="4.204130371094" Y="-27.80214453125" />
                  <Point X="4.136159179687" Y="-27.898720703125" />
                  <Point X="4.056687988281" Y="-28.011638671875" />
                  <Point X="3.282793945312" Y="-27.564830078125" />
                  <Point X="2.753454589844" Y="-27.259216796875" />
                  <Point X="2.737340332031" Y="-27.253314453125" />
                  <Point X="2.652157226562" Y="-27.2379296875" />
                  <Point X="2.52125" Y="-27.2142890625" />
                  <Point X="2.489078125" Y="-27.21924609375" />
                  <Point X="2.418311767578" Y="-27.256490234375" />
                  <Point X="2.309560058594" Y="-27.313724609375" />
                  <Point X="2.288599853516" Y="-27.33468359375" />
                  <Point X="2.251356201172" Y="-27.40544921875" />
                  <Point X="2.194120849609" Y="-27.514201171875" />
                  <Point X="2.189163085938" Y="-27.546375" />
                  <Point X="2.204546875" Y="-27.63155859375" />
                  <Point X="2.228188720703" Y="-27.762466796875" />
                  <Point X="2.234091552734" Y="-27.778580078125" />
                  <Point X="2.703752929688" Y="-28.592056640625" />
                  <Point X="2.986673583984" Y="-29.082087890625" />
                  <Point X="2.949008789062" Y="-29.1089921875" />
                  <Point X="2.835296142578" Y="-29.19021484375" />
                  <Point X="2.759301757812" Y="-29.239404296875" />
                  <Point X="2.679775390625" Y="-29.290880859375" />
                  <Point X="2.087112304688" Y="-28.518505859375" />
                  <Point X="1.683177612305" Y="-27.992087890625" />
                  <Point X="1.670549316406" Y="-27.980466796875" />
                  <Point X="1.586535766602" Y="-27.926455078125" />
                  <Point X="1.45742590332" Y="-27.84344921875" />
                  <Point X="1.425805297852" Y="-27.83571875" />
                  <Point X="1.333922241211" Y="-27.844173828125" />
                  <Point X="1.192718383789" Y="-27.857166015625" />
                  <Point X="1.165332763672" Y="-27.8685078125" />
                  <Point X="1.094383056641" Y="-27.927501953125" />
                  <Point X="0.985349060059" Y="-28.01816015625" />
                  <Point X="0.968456726074" Y="-28.04598828125" />
                  <Point X="0.947243225098" Y="-28.1435859375" />
                  <Point X="0.91464251709" Y="-28.29357421875" />
                  <Point X="0.91392956543" Y="-28.31071875" />
                  <Point X="1.047027954102" Y="-29.321703125" />
                  <Point X="1.127642333984" Y="-29.934029296875" />
                  <Point X="1.101924316406" Y="-29.939666015625" />
                  <Point X="0.994362365723" Y="-29.963244140625" />
                  <Point X="0.92413885498" Y="-29.976001953125" />
                  <Point X="0.860200500488" Y="-29.9876171875" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#156" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-25.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.076177159468" Y="4.639421517591" Z="1" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1" />
                  <Point X="-0.672261821919" Y="5.020261058313" Z="1" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1" />
                  <Point X="-1.448344850338" Y="4.853584779719" Z="1" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1" />
                  <Point X="-1.733620792968" Y="4.640479712658" Z="1" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1" />
                  <Point X="-1.72720063896" Y="4.381160982191" Z="1" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1" />
                  <Point X="-1.800004618146" Y="4.315918158695" Z="1" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1" />
                  <Point X="-1.896780920762" Y="4.329752074116" Z="1" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1" />
                  <Point X="-2.013145325707" Y="4.452024786082" Z="1" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1" />
                  <Point X="-2.529416961224" Y="4.390379290713" Z="1" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1" />
                  <Point X="-3.145247878103" Y="3.972508051775" Z="1" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1" />
                  <Point X="-3.229998571226" Y="3.536041263206" Z="1" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1" />
                  <Point X="-2.996990476967" Y="3.088487178959" Z="1" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1" />
                  <Point X="-3.030826286637" Y="3.017977281249" Z="1" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1" />
                  <Point X="-3.106589286648" Y="2.998574222206" Z="1" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1" />
                  <Point X="-3.397818085091" Y="3.150195377519" Z="1" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1" />
                  <Point X="-4.044425469262" Y="3.056199602745" Z="1" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1" />
                  <Point X="-4.413634280671" Y="2.493507941984" Z="1" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1" />
                  <Point X="-4.212153064841" Y="2.006460793445" Z="1" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1" />
                  <Point X="-3.678545908584" Y="1.576224970101" Z="1" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1" />
                  <Point X="-3.681753853843" Y="1.517656741093" Z="1" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1" />
                  <Point X="-3.728681828406" Y="1.482467248474" Z="1" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1" />
                  <Point X="-4.172167965227" Y="1.53003074358" Z="1" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1" />
                  <Point X="-4.911203439858" Y="1.265358285965" Z="1" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1" />
                  <Point X="-5.025835949818" Y="0.679730609772" Z="1" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1" />
                  <Point X="-4.475425193671" Y="0.289919041395" Z="1" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1" />
                  <Point X="-3.559748065268" Y="0.037400078677" Z="1" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1" />
                  <Point X="-3.543203539653" Y="0.011749923755" Z="1" />
                  <Point X="-3.539556741714" Y="0" Z="1" />
                  <Point X="-3.545161061482" Y="-0.018057027306" Z="1" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1" />
                  <Point X="-3.565620529855" Y="-0.041475904457" Z="1" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1" />
                  <Point X="-4.161462595102" Y="-0.205793015361" Z="1" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1" />
                  <Point X="-5.013277458387" Y="-0.775608828104" Z="1" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1" />
                  <Point X="-4.900419393092" Y="-1.311646472614" Z="1" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1" />
                  <Point X="-4.205245176039" Y="-1.436684005725" Z="1" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1" />
                  <Point X="-3.203115760641" Y="-1.316305594501" Z="1" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1" />
                  <Point X="-3.197343683568" Y="-1.340470734625" Z="1" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1" />
                  <Point X="-3.713835139944" Y="-1.746184524107" Z="1" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1" />
                  <Point X="-4.325070834337" Y="-2.649849094522" Z="1" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1" />
                  <Point X="-3.999072283982" Y="-3.120155125946" Z="1" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1" />
                  <Point X="-3.353956623328" Y="-3.006469146625" Z="1" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1" />
                  <Point X="-2.562329774076" Y="-2.566000695056" Z="1" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1" />
                  <Point X="-2.848947866543" Y="-3.081121759164" Z="1" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1" />
                  <Point X="-3.051881235696" Y="-4.053225123642" Z="1" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1" />
                  <Point X="-2.624312930565" Y="-4.34230341616" Z="1" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1" />
                  <Point X="-2.36246377842" Y="-4.334005498785" Z="1" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1" />
                  <Point X="-2.069946508922" Y="-4.05203189567" Z="1" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1" />
                  <Point X="-1.780707136709" Y="-3.996376982851" Z="1" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1" />
                  <Point X="-1.517357599152" Y="-4.128300989659" Z="1" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1" />
                  <Point X="-1.388738501668" Y="-4.393280191684" Z="1" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1" />
                  <Point X="-1.383887100637" Y="-4.657616557391" Z="1" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1" />
                  <Point X="-1.23396591394" Y="-4.925592648995" Z="1" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1" />
                  <Point X="-0.935823365506" Y="-4.990828593734" Z="1" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1" />
                  <Point X="-0.659758613981" Y="-4.424436757366" Z="1" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1" />
                  <Point X="-0.317900668868" Y="-3.375864802488" Z="1" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1" />
                  <Point X="-0.099874350078" Y="-3.235236680995" Z="1" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1" />
                  <Point X="0.153484729283" Y="-3.251875364293" Z="1" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1" />
                  <Point X="0.352545190564" Y="-3.42578095328" Z="1" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1" />
                  <Point X="0.57499624642" Y="-4.108099272449" Z="1" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1" />
                  <Point X="0.926919168241" Y="-4.993916015485" Z="1" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1" />
                  <Point X="1.106474154533" Y="-4.957226131579" Z="1" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1" />
                  <Point X="1.090444225716" Y="-4.283896611999" Z="1" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1" />
                  <Point X="0.989946402976" Y="-3.122925273758" Z="1" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1" />
                  <Point X="1.12019401999" Y="-2.934667814719" Z="1" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1" />
                  <Point X="1.332347492248" Y="-2.862681743138" Z="1" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1" />
                  <Point X="1.55334046768" Y="-2.937232397195" Z="1" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1" />
                  <Point X="2.041288601438" Y="-3.517663118036" Z="1" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1" />
                  <Point X="2.78031497674" Y="-4.250097824209" Z="1" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1" />
                  <Point X="2.971914429933" Y="-4.118398713532" Z="1" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1" />
                  <Point X="2.740898304926" Y="-3.535775703409" Z="1" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1" />
                  <Point X="2.247595107807" Y="-2.591391204553" Z="1" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1" />
                  <Point X="2.289446467121" Y="-2.397456328852" Z="1" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1" />
                  <Point X="2.43544204952" Y="-2.269454842846" Z="1" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1" />
                  <Point X="2.63711559896" Y="-2.255852901684" Z="1" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1" />
                  <Point X="3.251637893698" Y="-2.576851195348" Z="1" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1" />
                  <Point X="4.170891924813" Y="-2.896218127943" Z="1" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1" />
                  <Point X="4.336338922149" Y="-2.642079384306" Z="1" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1" />
                  <Point X="3.923618801309" Y="-2.175413965951" Z="1" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1" />
                  <Point X="3.13187192758" Y="-1.519912349234" Z="1" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1" />
                  <Point X="3.10179109222" Y="-1.354753078092" Z="1" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1" />
                  <Point X="3.174474328718" Y="-1.207413976378" Z="1" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1" />
                  <Point X="3.327727056661" Y="-1.131477069357" Z="1" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1" />
                  <Point X="3.993639108407" Y="-1.194166665022" Z="1" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1" />
                  <Point X="4.958155366102" Y="-1.090273528948" Z="1" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1" />
                  <Point X="5.025712526261" Y="-0.717089671205" Z="1" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1" />
                  <Point X="4.535529481118" Y="-0.431841189756" Z="1" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1" />
                  <Point X="3.691910224156" Y="-0.188417081499" Z="1" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1" />
                  <Point X="3.621817229388" Y="-0.12449143726" Z="1" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1" />
                  <Point X="3.588728228608" Y="-0.038083948069" Z="1" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1" />
                  <Point X="3.592643221816" Y="0.05852658315" Z="1" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1" />
                  <Point X="3.633562209012" Y="0.139457301315" Z="1" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1" />
                  <Point X="3.711485190197" Y="0.199731762743" Z="1" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1" />
                  <Point X="4.260438336189" Y="0.358130743012" Z="1" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1" />
                  <Point X="5.0080915971" Y="0.825583551211" Z="1" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1" />
                  <Point X="4.920728814306" Y="1.244587885442" Z="1" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1" />
                  <Point X="4.321941171661" Y="1.33508988399" Z="1" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1" />
                  <Point X="3.406078879144" Y="1.229562949036" Z="1" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1" />
                  <Point X="3.326771510059" Y="1.258217393436" Z="1" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1" />
                  <Point X="3.270205322625" Y="1.317921859991" Z="1" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1" />
                  <Point X="3.240557152412" Y="1.398592686907" Z="1" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1" />
                  <Point X="3.246631226726" Y="1.478974236586" Z="1" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1" />
                  <Point X="3.290120380529" Y="1.554979693812" Z="1" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1" />
                  <Point X="3.760084947073" Y="1.92783358011" Z="1" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1" />
                  <Point X="4.32062236048" Y="2.664517094969" Z="1" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1" />
                  <Point X="4.095262122014" Y="2.999376348022" Z="1" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1" />
                  <Point X="3.413962504015" Y="2.788972215943" Z="1" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1" />
                  <Point X="2.461240564858" Y="2.253992505416" Z="1" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1" />
                  <Point X="2.387534125997" Y="2.250600533901" Z="1" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1" />
                  <Point X="2.321814418281" Y="2.279924174228" Z="1" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1" />
                  <Point X="2.270834426007" Y="2.335210442101" Z="1" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1" />
                  <Point X="2.248829168857" Y="2.402224317836" Z="1" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1" />
                  <Point X="2.258535437241" Y="2.478229011024" Z="1" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1" />
                  <Point X="2.606653136917" Y="3.09817657794" Z="1" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1" />
                  <Point X="2.901373888255" Y="4.163870724856" Z="1" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1" />
                  <Point X="2.512550219494" Y="4.409408642523" Z="1" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1" />
                  <Point X="2.106336115216" Y="4.617401883626" Z="1" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1" />
                  <Point X="1.685177079362" Y="4.787194762113" Z="1" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1" />
                  <Point X="1.120436511103" Y="4.943968307894" Z="1" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1" />
                  <Point X="0.457088711277" Y="5.04869150323" Z="1" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1" />
                  <Point X="0.117067402085" Y="4.79202598836" Z="1" />
                  <Point X="0" Y="4.355124473572" Z="1" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>