<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#199" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="3017" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="0" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="0.004715820312" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.886805969238" Y="-4.719857910156" />
                  <Point X="0.563302062988" Y="-3.512524414063" />
                  <Point X="0.557721496582" Y="-3.497142333984" />
                  <Point X="0.54236315918" Y="-3.467377197266" />
                  <Point X="0.41013885498" Y="-3.2768671875" />
                  <Point X="0.378635375977" Y="-3.231476806641" />
                  <Point X="0.356751434326" Y="-3.209021240234" />
                  <Point X="0.330495941162" Y="-3.18977734375" />
                  <Point X="0.302495178223" Y="-3.175669189453" />
                  <Point X="0.097885803223" Y="-3.112165771484" />
                  <Point X="0.049136035919" Y="-3.097035888672" />
                  <Point X="0.020976488113" Y="-3.092766601562" />
                  <Point X="-0.00866468811" Y="-3.092766601562" />
                  <Point X="-0.036824237823" Y="-3.097035888672" />
                  <Point X="-0.241433609009" Y="-3.1605390625" />
                  <Point X="-0.290183380127" Y="-3.175669189453" />
                  <Point X="-0.31818460083" Y="-3.189777587891" />
                  <Point X="-0.344439788818" Y="-3.209021484375" />
                  <Point X="-0.366323577881" Y="-3.231476806641" />
                  <Point X="-0.498547729492" Y="-3.421986816406" />
                  <Point X="-0.530051208496" Y="-3.467377197266" />
                  <Point X="-0.538189025879" Y="-3.481573730469" />
                  <Point X="-0.550990112305" Y="-3.512524414063" />
                  <Point X="-0.628067321777" Y="-3.800180419922" />
                  <Point X="-0.916584594727" Y="-4.876941894531" />
                  <Point X="-1.025943847656" Y="-4.85571484375" />
                  <Point X="-1.079341430664" Y="-4.845350097656" />
                  <Point X="-1.24641796875" Y="-4.802362792969" />
                  <Point X="-1.214963012695" Y="-4.563438964844" />
                  <Point X="-1.214201171875" Y="-4.5479296875" />
                  <Point X="-1.216508666992" Y="-4.516224121094" />
                  <Point X="-1.265389892578" Y="-4.270482421875" />
                  <Point X="-1.277036132812" Y="-4.211932617188" />
                  <Point X="-1.287938354492" Y="-4.182966308594" />
                  <Point X="-1.304010375977" Y="-4.15512890625" />
                  <Point X="-1.323644775391" Y="-4.131204101562" />
                  <Point X="-1.512023193359" Y="-3.966000244141" />
                  <Point X="-1.556905517578" Y="-3.926639404297" />
                  <Point X="-1.583188476562" Y="-3.910295410156" />
                  <Point X="-1.612885742188" Y="-3.897994384766" />
                  <Point X="-1.64302734375" Y="-3.890966308594" />
                  <Point X="-1.893047485352" Y="-3.874579101562" />
                  <Point X="-1.952616577148" Y="-3.870674804688" />
                  <Point X="-1.98341796875" Y="-3.873708496094" />
                  <Point X="-2.014466674805" Y="-3.882028076172" />
                  <Point X="-2.042657836914" Y="-3.894801513672" />
                  <Point X="-2.250988037109" Y="-4.034003662109" />
                  <Point X="-2.300624267578" Y="-4.067169677734" />
                  <Point X="-2.312785644531" Y="-4.076821533203" />
                  <Point X="-2.335102783203" Y="-4.099461914062" />
                  <Point X="-2.378375488281" Y="-4.155855957031" />
                  <Point X="-2.480148925781" Y="-4.288489746094" />
                  <Point X="-2.723448486328" Y="-4.137844726562" />
                  <Point X="-2.801706787109" Y="-4.089389404297" />
                  <Point X="-3.104721923828" Y="-3.856077880859" />
                  <Point X="-3.02495703125" Y="-3.717921142578" />
                  <Point X="-2.423761230469" Y="-2.676619628906" />
                  <Point X="-2.412859130859" Y="-2.647653320312" />
                  <Point X="-2.406588134766" Y="-2.616126953125" />
                  <Point X="-2.405575439453" Y="-2.585192871094" />
                  <Point X="-2.414560058594" Y="-2.555575195312" />
                  <Point X="-2.428777099609" Y="-2.526746337891" />
                  <Point X="-2.4468046875" Y="-2.501589355469" />
                  <Point X="-2.464153320312" Y="-2.484240722656" />
                  <Point X="-2.489310302734" Y="-2.466213134766" />
                  <Point X="-2.518139160156" Y="-2.45199609375" />
                  <Point X="-2.547756835938" Y="-2.443011474609" />
                  <Point X="-2.578690917969" Y="-2.444024169922" />
                  <Point X="-2.610217285156" Y="-2.450295166016" />
                  <Point X="-2.63918359375" Y="-2.461197265625" />
                  <Point X="-2.88728125" Y="-2.604436523438" />
                  <Point X="-3.818024414062" Y="-3.141801025391" />
                  <Point X="-4.021029541016" Y="-2.87509375" />
                  <Point X="-4.082860595703" Y="-2.793860595703" />
                  <Point X="-4.306142578125" Y="-2.419450439453" />
                  <Point X="-4.157852050781" Y="-2.305662841797" />
                  <Point X="-3.105954589844" Y="-1.498513549805" />
                  <Point X="-3.084577148438" Y="-1.475593261719" />
                  <Point X="-3.066612060547" Y="-1.448461669922" />
                  <Point X="-3.053856445312" Y="-1.419832641602" />
                  <Point X="-3.047634277344" Y="-1.395809082031" />
                  <Point X="-3.046151855469" Y="-1.390085205078" />
                  <Point X="-3.043347412109" Y="-1.359655761719" />
                  <Point X="-3.045556396484" Y="-1.327985473633" />
                  <Point X="-3.052557617188" Y="-1.298240478516" />
                  <Point X="-3.068640136719" Y="-1.272256958008" />
                  <Point X="-3.08947265625" Y="-1.24830065918" />
                  <Point X="-3.112971923828" Y="-1.228767211914" />
                  <Point X="-3.134358886719" Y="-1.2161796875" />
                  <Point X="-3.139454589844" Y="-1.213180664062" />
                  <Point X="-3.168718261719" Y="-1.201956420898" />
                  <Point X="-3.200606201172" Y="-1.195474731445" />
                  <Point X="-3.231929199219" Y="-1.194383789062" />
                  <Point X="-3.54512890625" Y="-1.23561730957" />
                  <Point X="-4.732102539062" Y="-1.391885375977" />
                  <Point X="-4.80989453125" Y="-1.087331054688" />
                  <Point X="-4.834076660156" Y="-0.992659790039" />
                  <Point X="-4.892424316406" Y="-0.584698181152" />
                  <Point X="-4.731145996094" Y="-0.541483764648" />
                  <Point X="-3.532875976562" Y="-0.220408340454" />
                  <Point X="-3.517493652344" Y="-0.21482762146" />
                  <Point X="-3.487728515625" Y="-0.199469467163" />
                  <Point X="-3.465315673828" Y="-0.183913528442" />
                  <Point X="-3.459975585938" Y="-0.180207260132" />
                  <Point X="-3.437521240234" Y="-0.158324401855" />
                  <Point X="-3.418277099609" Y="-0.132069213867" />
                  <Point X="-3.404168212891" Y="-0.104067222595" />
                  <Point X="-3.396697265625" Y="-0.079995391846" />
                  <Point X="-3.394917236328" Y="-0.074260139465" />
                  <Point X="-3.390647949219" Y="-0.046100894928" />
                  <Point X="-3.390647949219" Y="-0.016459262848" />
                  <Point X="-3.394917236328" Y="0.011700135231" />
                  <Point X="-3.402388183594" Y="0.035771816254" />
                  <Point X="-3.404168212891" Y="0.041507064819" />
                  <Point X="-3.418277099609" Y="0.069509056091" />
                  <Point X="-3.437521240234" Y="0.09576424408" />
                  <Point X="-3.459975585938" Y="0.117647109985" />
                  <Point X="-3.482388427734" Y="0.133203048706" />
                  <Point X="-3.492555175781" Y="0.139354660034" />
                  <Point X="-3.513463134766" Y="0.150281143188" />
                  <Point X="-3.532875976562" Y="0.157848342896" />
                  <Point X="-3.818372558594" Y="0.234346755981" />
                  <Point X="-4.89181640625" Y="0.521975219727" />
                  <Point X="-4.840072265625" Y="0.871656860352" />
                  <Point X="-4.824487304688" Y="0.976979125977" />
                  <Point X="-4.703551757812" Y="1.423267944336" />
                  <Point X="-4.624615234375" Y="1.412875732422" />
                  <Point X="-3.765666259766" Y="1.29979284668" />
                  <Point X="-3.744985839844" Y="1.299341674805" />
                  <Point X="-3.723424072266" Y="1.301228271484" />
                  <Point X="-3.703137451172" Y="1.305263671875" />
                  <Point X="-3.653530517578" Y="1.320904663086" />
                  <Point X="-3.641711425781" Y="1.324631225586" />
                  <Point X="-3.622778320312" Y="1.332962036133" />
                  <Point X="-3.604034179688" Y="1.343784057617" />
                  <Point X="-3.587353271484" Y="1.356015136719" />
                  <Point X="-3.57371484375" Y="1.37156652832" />
                  <Point X="-3.561300292969" Y="1.389296508789" />
                  <Point X="-3.551351318359" Y="1.407431030273" />
                  <Point X="-3.531446289062" Y="1.455485839844" />
                  <Point X="-3.526703857422" Y="1.466935180664" />
                  <Point X="-3.520915527344" Y="1.486794067383" />
                  <Point X="-3.517157226562" Y="1.508108520508" />
                  <Point X="-3.515804443359" Y="1.528748535156" />
                  <Point X="-3.518950927734" Y="1.549191894531" />
                  <Point X="-3.524552490234" Y="1.570098022461" />
                  <Point X="-3.532049804688" Y="1.589377319336" />
                  <Point X="-3.556067382812" Y="1.635514526367" />
                  <Point X="-3.561789550781" Y="1.646507080078" />
                  <Point X="-3.573280761719" Y="1.663705078125" />
                  <Point X="-3.587193603516" Y="1.680286132812" />
                  <Point X="-3.602135986328" Y="1.694590332031" />
                  <Point X="-3.765897460938" Y="1.820248901367" />
                  <Point X="-4.351859863281" Y="2.269874267578" />
                  <Point X="-4.141707519531" Y="2.629916259766" />
                  <Point X="-4.081153808594" Y="2.733659423828" />
                  <Point X="-3.752630371094" Y="3.155929931641" />
                  <Point X="-3.734199707031" Y="3.149247802734" />
                  <Point X="-3.206656982422" Y="2.844670898438" />
                  <Point X="-3.187724609375" Y="2.836340332031" />
                  <Point X="-3.167082519531" Y="2.829831787109" />
                  <Point X="-3.146794677734" Y="2.825796386719" />
                  <Point X="-3.077706054688" Y="2.819751953125" />
                  <Point X="-3.061245361328" Y="2.818311767578" />
                  <Point X="-3.040566162109" Y="2.818762939453" />
                  <Point X="-3.019107666016" Y="2.821587890625" />
                  <Point X="-2.999015625" Y="2.826504150391" />
                  <Point X="-2.980464111328" Y="2.835652587891" />
                  <Point X="-2.962209716797" Y="2.847281738281" />
                  <Point X="-2.946077636719" Y="2.860229248047" />
                  <Point X="-2.897038085938" Y="2.909268798828" />
                  <Point X="-2.885354003906" Y="2.920952636719" />
                  <Point X="-2.872407226563" Y="2.937083740234" />
                  <Point X="-2.860777587891" Y="2.955338378906" />
                  <Point X="-2.851628662109" Y="2.973889892578" />
                  <Point X="-2.846712158203" Y="2.993981933594" />
                  <Point X="-2.843886962891" Y="3.015440917969" />
                  <Point X="-2.843435791016" Y="3.03612109375" />
                  <Point X="-2.849480224609" Y="3.105209716797" />
                  <Point X="-2.850920410156" Y="3.121670654297" />
                  <Point X="-2.854955810547" Y="3.141958496094" />
                  <Point X="-2.861464355469" Y="3.162600585938" />
                  <Point X="-2.869794921875" Y="3.181532958984" />
                  <Point X="-2.942362548828" Y="3.307223632812" />
                  <Point X="-3.183333007812" Y="3.724596435547" />
                  <Point X="-2.806086181641" Y="4.013827636719" />
                  <Point X="-2.700626220703" Y="4.094683349609" />
                  <Point X="-2.183213378906" Y="4.382146972656" />
                  <Point X="-2.167036865234" Y="4.391134277344" />
                  <Point X="-2.0431953125" Y="4.229740722656" />
                  <Point X="-2.028891967773" Y="4.214799316406" />
                  <Point X="-2.012312255859" Y="4.200887207031" />
                  <Point X="-1.995113647461" Y="4.189395019531" />
                  <Point X="-1.918218383789" Y="4.149365722656" />
                  <Point X="-1.899897583008" Y="4.139828125" />
                  <Point X="-1.880619506836" Y="4.132331542969" />
                  <Point X="-1.859712402344" Y="4.126729492187" />
                  <Point X="-1.839268920898" Y="4.123582519531" />
                  <Point X="-1.81862890625" Y="4.124935058594" />
                  <Point X="-1.797313110352" Y="4.128693359375" />
                  <Point X="-1.777453491211" Y="4.134481933594" />
                  <Point X="-1.697361938477" Y="4.167657226563" />
                  <Point X="-1.678279541016" Y="4.175561523437" />
                  <Point X="-1.660145507812" Y="4.185510253906" />
                  <Point X="-1.642416137695" Y="4.197924316406" />
                  <Point X="-1.626864257812" Y="4.211562988281" />
                  <Point X="-1.6146328125" Y="4.228244140625" />
                  <Point X="-1.603810913086" Y="4.24698828125" />
                  <Point X="-1.59548046875" Y="4.265921386719" />
                  <Point X="-1.569412109375" Y="4.348599609375" />
                  <Point X="-1.563201171875" Y="4.368297851562" />
                  <Point X="-1.559165771484" Y="4.388584472656" />
                  <Point X="-1.557279174805" Y="4.410146484375" />
                  <Point X="-1.55773034668" Y="4.430826660156" />
                  <Point X="-1.56598034668" Y="4.493492675781" />
                  <Point X="-1.584201904297" Y="4.631897949219" />
                  <Point X="-1.086160644531" Y="4.77153125" />
                  <Point X="-0.949634521484" Y="4.80980859375" />
                  <Point X="-0.322378570557" Y="4.883219726562" />
                  <Point X="-0.2947109375" Y="4.886458007812" />
                  <Point X="-0.289965118408" Y="4.86874609375" />
                  <Point X="-0.133903305054" Y="4.286315429688" />
                  <Point X="-0.121129844666" Y="4.258124023438" />
                  <Point X="-0.10327155304" Y="4.231397460938" />
                  <Point X="-0.0821145401" Y="4.20880859375" />
                  <Point X="-0.054819446564" Y="4.19421875" />
                  <Point X="-0.024381370544" Y="4.183886230469" />
                  <Point X="0.006155909061" Y="4.178844238281" />
                  <Point X="0.03669316864" Y="4.183886230469" />
                  <Point X="0.067131248474" Y="4.19421875" />
                  <Point X="0.094426467896" Y="4.20880859375" />
                  <Point X="0.115583503723" Y="4.231397460938" />
                  <Point X="0.133441802979" Y="4.258124023438" />
                  <Point X="0.146215255737" Y="4.286315429688" />
                  <Point X="0.183398147583" Y="4.425083984375" />
                  <Point X="0.307419403076" Y="4.8879375" />
                  <Point X="0.724826782227" Y="4.844223632812" />
                  <Point X="0.844041015625" Y="4.831738769531" />
                  <Point X="1.362990966797" Y="4.706448242188" />
                  <Point X="1.481026611328" Y="4.677950683594" />
                  <Point X="1.818529174805" Y="4.555536132812" />
                  <Point X="1.894646484375" Y="4.527927734375" />
                  <Point X="2.221269775391" Y="4.375176757813" />
                  <Point X="2.294576416016" Y="4.340893554688" />
                  <Point X="2.610131347656" Y="4.15705078125" />
                  <Point X="2.680978759766" Y="4.115774902344" />
                  <Point X="2.943260009766" Y="3.929254882812" />
                  <Point X="2.843420166016" Y="3.756326660156" />
                  <Point X="2.147581054688" Y="2.551097900391" />
                  <Point X="2.142075927734" Y="2.539931152344" />
                  <Point X="2.133076904297" Y="2.516056884766" />
                  <Point X="2.11573828125" Y="2.45121875" />
                  <Point X="2.113695068359" Y="2.441613037109" />
                  <Point X="2.108226318359" Y="2.407262695313" />
                  <Point X="2.107728027344" Y="2.380953369141" />
                  <Point X="2.114488769531" Y="2.324886962891" />
                  <Point X="2.116099365234" Y="2.311528564453" />
                  <Point X="2.121441894531" Y="2.289605957031" />
                  <Point X="2.129708007812" Y="2.267516845703" />
                  <Point X="2.140071044922" Y="2.247471191406" />
                  <Point X="2.174763183594" Y="2.196343994141" />
                  <Point X="2.180858886719" Y="2.188313232422" />
                  <Point X="2.202425048828" Y="2.162831298828" />
                  <Point X="2.221599121094" Y="2.145592285156" />
                  <Point X="2.272726318359" Y="2.110900146484" />
                  <Point X="2.284907714844" Y="2.102634765625" />
                  <Point X="2.304953125" Y="2.092271728516" />
                  <Point X="2.327040771484" Y="2.084006103516" />
                  <Point X="2.348963623047" Y="2.078663574219" />
                  <Point X="2.405030273438" Y="2.071902832031" />
                  <Point X="2.415598876953" Y="2.071222900391" />
                  <Point X="2.447860107422" Y="2.070949707031" />
                  <Point X="2.473206542969" Y="2.074171142578" />
                  <Point X="2.538044677734" Y="2.091509765625" />
                  <Point X="2.543413085938" Y="2.093116455078" />
                  <Point X="2.570944335938" Y="2.102249023438" />
                  <Point X="2.588533935547" Y="2.110145019531" />
                  <Point X="2.875688476562" Y="2.27593359375" />
                  <Point X="3.967326660156" Y="2.906191162109" />
                  <Point X="4.081247314453" Y="2.7478671875" />
                  <Point X="4.123270507813" Y="2.689464355469" />
                  <Point X="4.262198730469" Y="2.459884277344" />
                  <Point X="4.146920898438" Y="2.371428222656" />
                  <Point X="3.230783935547" Y="1.668451293945" />
                  <Point X="3.221424560547" Y="1.660241455078" />
                  <Point X="3.203973876953" Y="1.641627929688" />
                  <Point X="3.157309814453" Y="1.580750976562" />
                  <Point X="3.152063964844" Y="1.573172363281" />
                  <Point X="3.132476074219" Y="1.541715576172" />
                  <Point X="3.121629882812" Y="1.51708581543" />
                  <Point X="3.104247314453" Y="1.454930297852" />
                  <Point X="3.100105957031" Y="1.440121337891" />
                  <Point X="3.09665234375" Y="1.417821777344" />
                  <Point X="3.095836425781" Y="1.394252319336" />
                  <Point X="3.097739501953" Y="1.371768188477" />
                  <Point X="3.112008789062" Y="1.302611938477" />
                  <Point X="3.114387207031" Y="1.293428833008" />
                  <Point X="3.124985107422" Y="1.259573974609" />
                  <Point X="3.136282714844" Y="1.235739990234" />
                  <Point X="3.175093261719" Y="1.176749633789" />
                  <Point X="3.184340332031" Y="1.162694580078" />
                  <Point X="3.198894287109" Y="1.145449584961" />
                  <Point X="3.216138183594" Y="1.129359985352" />
                  <Point X="3.234347412109" Y="1.116034667969" />
                  <Point X="3.290589355469" Y="1.08437512207" />
                  <Point X="3.299546875" Y="1.079936523438" />
                  <Point X="3.330922119141" Y="1.066395874023" />
                  <Point X="3.356118164062" Y="1.059438598633" />
                  <Point X="3.432161132812" Y="1.049388427734" />
                  <Point X="3.437328125" Y="1.048848999023" />
                  <Point X="3.4685234375" Y="1.046451049805" />
                  <Point X="3.488203613281" Y="1.04698449707" />
                  <Point X="3.760981201172" Y="1.082896484375" />
                  <Point X="4.776839355469" Y="1.21663659668" />
                  <Point X="4.82788671875" Y="1.006949157715" />
                  <Point X="4.845936035156" Y="0.932809204102" />
                  <Point X="4.890864746094" Y="0.644238647461" />
                  <Point X="4.766492675781" Y="0.610913085938" />
                  <Point X="3.716579833984" Y="0.329589935303" />
                  <Point X="3.704790527344" Y="0.325586303711" />
                  <Point X="3.681545898438" Y="0.315067993164" />
                  <Point X="3.606836181641" Y="0.271884521484" />
                  <Point X="3.589035888672" Y="0.261595733643" />
                  <Point X="3.574311279297" Y="0.251096221924" />
                  <Point X="3.547530761719" Y="0.225576461792" />
                  <Point X="3.502704833984" Y="0.168457885742" />
                  <Point X="3.492024902344" Y="0.154848968506" />
                  <Point X="3.480300537109" Y="0.135568695068" />
                  <Point X="3.470526855469" Y="0.114105232239" />
                  <Point X="3.463680664062" Y="0.092604270935" />
                  <Point X="3.448738525391" Y="0.01458278656" />
                  <Point X="3.447470458984" Y="0.005716405392" />
                  <Point X="3.443910400391" Y="-0.031682170868" />
                  <Point X="3.445178466797" Y="-0.058553779602" />
                  <Point X="3.460120605469" Y="-0.136575119019" />
                  <Point X="3.463680664062" Y="-0.155164276123" />
                  <Point X="3.470526855469" Y="-0.176665084839" />
                  <Point X="3.480300537109" Y="-0.198128555298" />
                  <Point X="3.492024902344" Y="-0.217408813477" />
                  <Point X="3.536850830078" Y="-0.274527557373" />
                  <Point X="3.543057861328" Y="-0.281673248291" />
                  <Point X="3.568050048828" Y="-0.307702697754" />
                  <Point X="3.589035888672" Y="-0.324155731201" />
                  <Point X="3.663745849609" Y="-0.367339172363" />
                  <Point X="3.681545898438" Y="-0.377627990723" />
                  <Point X="3.692709228516" Y="-0.383138458252" />
                  <Point X="3.716579833984" Y="-0.392149932861" />
                  <Point X="3.966729248047" Y="-0.459177246094" />
                  <Point X="4.891472167969" Y="-0.706961486816" />
                  <Point X="4.865100585938" Y="-0.881880615234" />
                  <Point X="4.855021484375" Y="-0.948733703613" />
                  <Point X="4.801174316406" Y="-1.18469921875" />
                  <Point X="4.641985839844" Y="-1.163741577148" />
                  <Point X="3.424382080078" Y="-1.003441040039" />
                  <Point X="3.408035644531" Y="-1.002710327148" />
                  <Point X="3.374658691406" Y="-1.005508728027" />
                  <Point X="3.228029785156" Y="-1.037379150391" />
                  <Point X="3.193094482422" Y="-1.044972412109" />
                  <Point X="3.163974121094" Y="-1.056596923828" />
                  <Point X="3.136147460938" Y="-1.073489257812" />
                  <Point X="3.112397460938" Y="-1.093960083008" />
                  <Point X="3.02376953125" Y="-1.200551757812" />
                  <Point X="3.002653320312" Y="-1.225948364258" />
                  <Point X="2.987932617188" Y="-1.250330688477" />
                  <Point X="2.976589355469" Y="-1.277715942383" />
                  <Point X="2.969757568359" Y="-1.305365478516" />
                  <Point X="2.957054931641" Y="-1.443406494141" />
                  <Point X="2.954028564453" Y="-1.476295654297" />
                  <Point X="2.956347412109" Y="-1.507564819336" />
                  <Point X="2.964079101562" Y="-1.539185791016" />
                  <Point X="2.976450439453" Y="-1.567996582031" />
                  <Point X="3.057596923828" Y="-1.69421472168" />
                  <Point X="3.076930664062" Y="-1.724287109375" />
                  <Point X="3.086932373047" Y="-1.73723815918" />
                  <Point X="3.110628417969" Y="-1.760909179688" />
                  <Point X="3.342768554688" Y="-1.939036743164" />
                  <Point X="4.213122070312" Y="-2.6068828125" />
                  <Point X="4.153218261719" Y="-2.703816162109" />
                  <Point X="4.124806152344" Y="-2.749791748047" />
                  <Point X="4.028981201172" Y="-2.885945068359" />
                  <Point X="3.885395019531" Y="-2.803045410156" />
                  <Point X="2.800954589844" Y="-2.176943115234" />
                  <Point X="2.786131591797" Y="-2.170012451172" />
                  <Point X="2.754224609375" Y="-2.159825195312" />
                  <Point X="2.579712890625" Y="-2.12830859375" />
                  <Point X="2.538134277344" Y="-2.120799560547" />
                  <Point X="2.506781982422" Y="-2.120395263672" />
                  <Point X="2.474609375" Y="-2.125353027344" />
                  <Point X="2.444833496094" Y="-2.135176757812" />
                  <Point X="2.299857177734" Y="-2.211476806641" />
                  <Point X="2.265315429688" Y="-2.229655761719" />
                  <Point X="2.242384765625" Y="-2.246548828125" />
                  <Point X="2.221424804688" Y="-2.267508789062" />
                  <Point X="2.204531738281" Y="-2.290439453125" />
                  <Point X="2.128231689453" Y="-2.435415771484" />
                  <Point X="2.110052734375" Y="-2.469957519531" />
                  <Point X="2.100229003906" Y="-2.499733398438" />
                  <Point X="2.095271240234" Y="-2.531906005859" />
                  <Point X="2.095675537109" Y="-2.563258300781" />
                  <Point X="2.127192138672" Y="-2.737770019531" />
                  <Point X="2.134701171875" Y="-2.779348632812" />
                  <Point X="2.138985595703" Y="-2.795141845703" />
                  <Point X="2.151819091797" Y="-2.826078613281" />
                  <Point X="2.300992431641" Y="-3.084454589844" />
                  <Point X="2.861283447266" Y="-4.05490625" />
                  <Point X="2.815562744141" Y="-4.087563476562" />
                  <Point X="2.781845947266" Y="-4.111646484375" />
                  <Point X="2.701765136719" Y="-4.163481445312" />
                  <Point X="2.58607421875" Y="-4.012710693359" />
                  <Point X="1.758546142578" Y="-2.934255126953" />
                  <Point X="1.747503540039" Y="-2.922178955078" />
                  <Point X="1.721924194336" Y="-2.900557373047" />
                  <Point X="1.54980847168" Y="-2.789902832031" />
                  <Point X="1.50880078125" Y="-2.763538574219" />
                  <Point X="1.479989501953" Y="-2.751167236328" />
                  <Point X="1.448368164063" Y="-2.743435546875" />
                  <Point X="1.417099487305" Y="-2.741116699219" />
                  <Point X="1.228861572266" Y="-2.758438476562" />
                  <Point X="1.184012573242" Y="-2.762565673828" />
                  <Point X="1.156362670898" Y="-2.769397460938" />
                  <Point X="1.128977661133" Y="-2.780740722656" />
                  <Point X="1.104595825195" Y="-2.795461181641" />
                  <Point X="0.959243408203" Y="-2.916316894531" />
                  <Point X="0.924612243652" Y="-2.945111572266" />
                  <Point X="0.904141479492" Y="-2.968861572266" />
                  <Point X="0.887249084473" Y="-2.996688232422" />
                  <Point X="0.875624328613" Y="-3.025808837891" />
                  <Point X="0.832164611816" Y="-3.225757324219" />
                  <Point X="0.821810058594" Y="-3.273396728516" />
                  <Point X="0.819724487305" Y="-3.289627441406" />
                  <Point X="0.819742248535" Y="-3.323120117188" />
                  <Point X="0.862016784668" Y="-3.644227539062" />
                  <Point X="1.022065307617" Y="-4.859915039062" />
                  <Point X="1.007569519043" Y="-4.863092773438" />
                  <Point X="0.975678161621" Y="-4.870083007812" />
                  <Point X="0.929315551758" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.007841918945" Y="-4.762455566406" />
                  <Point X="-1.058435668945" Y="-4.752635253906" />
                  <Point X="-1.141246337891" Y="-4.731328613281" />
                  <Point X="-1.120775634766" Y="-4.575838867188" />
                  <Point X="-1.120077514648" Y="-4.568100097656" />
                  <Point X="-1.119451782227" Y="-4.541033691406" />
                  <Point X="-1.121759277344" Y="-4.509328125" />
                  <Point X="-1.123334106445" Y="-4.497690429688" />
                  <Point X="-1.172215209961" Y="-4.251948730469" />
                  <Point X="-1.183861450195" Y="-4.193398925781" />
                  <Point X="-1.188125" Y="-4.17846875" />
                  <Point X="-1.19902734375" Y="-4.149502441406" />
                  <Point X="-1.205666015625" Y="-4.135466308594" />
                  <Point X="-1.221738037109" Y="-4.10762890625" />
                  <Point X="-1.23057409668" Y="-4.094861816406" />
                  <Point X="-1.250208496094" Y="-4.070937011719" />
                  <Point X="-1.261006958008" Y="-4.059779296875" />
                  <Point X="-1.449385253906" Y="-3.894575439453" />
                  <Point X="-1.494267578125" Y="-3.855214599609" />
                  <Point X="-1.506738647461" Y="-3.845965576172" />
                  <Point X="-1.533021606445" Y="-3.829621582031" />
                  <Point X="-1.546833618164" Y="-3.822526855469" />
                  <Point X="-1.576530761719" Y="-3.810225830078" />
                  <Point X="-1.591313476562" Y="-3.805476074219" />
                  <Point X="-1.621454833984" Y="-3.798447998047" />
                  <Point X="-1.636813964844" Y="-3.796169677734" />
                  <Point X="-1.886834106445" Y="-3.779782470703" />
                  <Point X="-1.946403198242" Y="-3.775878173828" />
                  <Point X="-1.961928222656" Y="-3.776132324219" />
                  <Point X="-1.992729614258" Y="-3.779166015625" />
                  <Point X="-2.008006103516" Y="-3.781945556641" />
                  <Point X="-2.03905480957" Y="-3.790265136719" />
                  <Point X="-2.053674316406" Y="-3.795496337891" />
                  <Point X="-2.081865478516" Y="-3.808269775391" />
                  <Point X="-2.095437011719" Y="-3.815812011719" />
                  <Point X="-2.303767333984" Y="-3.955014160156" />
                  <Point X="-2.353403564453" Y="-3.988180175781" />
                  <Point X="-2.359681640625" Y="-3.992757080078" />
                  <Point X="-2.380442138672" Y="-4.010131103516" />
                  <Point X="-2.402759277344" Y="-4.032771484375" />
                  <Point X="-2.410471435547" Y="-4.041629638672" />
                  <Point X="-2.453744140625" Y="-4.0980234375" />
                  <Point X="-2.503202880859" Y="-4.162479003906" />
                  <Point X="-2.673437255859" Y="-4.05707421875" />
                  <Point X="-2.747583007812" Y="-4.011165039062" />
                  <Point X="-2.980862792969" Y="-3.831547607422" />
                  <Point X="-2.942684570312" Y="-3.765421142578" />
                  <Point X="-2.341488769531" Y="-2.724119628906" />
                  <Point X="-2.334850097656" Y="-2.710083251953" />
                  <Point X="-2.323947998047" Y="-2.681116943359" />
                  <Point X="-2.319684570312" Y="-2.666187011719" />
                  <Point X="-2.313413574219" Y="-2.634660644531" />
                  <Point X="-2.311638916016" Y="-2.619235351562" />
                  <Point X="-2.310626220703" Y="-2.588301269531" />
                  <Point X="-2.314666259766" Y="-2.557615234375" />
                  <Point X="-2.323650878906" Y="-2.527997558594" />
                  <Point X="-2.329357421875" Y="-2.513557128906" />
                  <Point X="-2.343574462891" Y="-2.484728271484" />
                  <Point X="-2.351557128906" Y="-2.47141015625" />
                  <Point X="-2.369584716797" Y="-2.446253173828" />
                  <Point X="-2.379629638672" Y="-2.434414306641" />
                  <Point X="-2.396978271484" Y="-2.417065673828" />
                  <Point X="-2.408817138672" Y="-2.407020751953" />
                  <Point X="-2.433974121094" Y="-2.388993164062" />
                  <Point X="-2.447292236328" Y="-2.381010498047" />
                  <Point X="-2.47612109375" Y="-2.366793457031" />
                  <Point X="-2.490561523438" Y="-2.361086914062" />
                  <Point X="-2.520179199219" Y="-2.352102294922" />
                  <Point X="-2.550865234375" Y="-2.348062255859" />
                  <Point X="-2.581799316406" Y="-2.349074951172" />
                  <Point X="-2.597224609375" Y="-2.350849609375" />
                  <Point X="-2.628750976562" Y="-2.357120605469" />
                  <Point X="-2.643680908203" Y="-2.361384033203" />
                  <Point X="-2.672647216797" Y="-2.372286132812" />
                  <Point X="-2.68668359375" Y="-2.378924804688" />
                  <Point X="-2.93478125" Y="-2.5221640625" />
                  <Point X="-3.793089355469" Y="-3.017708251953" />
                  <Point X="-3.945436035156" Y="-2.817555664062" />
                  <Point X="-4.004016113281" Y="-2.74059375" />
                  <Point X="-4.181265136719" Y="-2.443373535156" />
                  <Point X="-4.10001953125" Y="-2.38103125" />
                  <Point X="-3.048122070312" Y="-1.573882080078" />
                  <Point X="-3.036481933594" Y="-1.563309814453" />
                  <Point X="-3.015104492188" Y="-1.540389404297" />
                  <Point X="-3.005367431641" Y="-1.528041503906" />
                  <Point X="-2.98740234375" Y="-1.50091003418" />
                  <Point X="-2.979835449219" Y="-1.487124755859" />
                  <Point X="-2.967079833984" Y="-1.458495727539" />
                  <Point X="-2.961891113281" Y="-1.443651977539" />
                  <Point X="-2.955668945312" Y="-1.419628417969" />
                  <Point X="-2.951552734375" Y="-1.398803710938" />
                  <Point X="-2.948748291016" Y="-1.368374145508" />
                  <Point X="-2.948577636719" Y="-1.353045654297" />
                  <Point X="-2.950786621094" Y="-1.321375366211" />
                  <Point X="-2.953083496094" Y="-1.306219604492" />
                  <Point X="-2.960084716797" Y="-1.276474731445" />
                  <Point X="-2.971778808594" Y="-1.248242431641" />
                  <Point X="-2.987861328125" Y="-1.222258911133" />
                  <Point X="-2.996954101562" Y="-1.209918457031" />
                  <Point X="-3.017786621094" Y="-1.185962036133" />
                  <Point X="-3.028745605469" Y="-1.175244384766" />
                  <Point X="-3.052244873047" Y="-1.1557109375" />
                  <Point X="-3.06478515625" Y="-1.146895141602" />
                  <Point X="-3.086172119141" Y="-1.134307495117" />
                  <Point X="-3.10543359375" Y="-1.124481323242" />
                  <Point X="-3.134697265625" Y="-1.113257080078" />
                  <Point X="-3.149795166016" Y="-1.108859985352" />
                  <Point X="-3.181683105469" Y="-1.102378417969" />
                  <Point X="-3.197299560547" Y="-1.100532348633" />
                  <Point X="-3.228622558594" Y="-1.09944140625" />
                  <Point X="-3.244329101562" Y="-1.100196533203" />
                  <Point X="-3.557528808594" Y="-1.141430053711" />
                  <Point X="-4.660920898438" Y="-1.286694335938" />
                  <Point X="-4.717849609375" Y="-1.06382019043" />
                  <Point X="-4.740761230469" Y="-0.974121948242" />
                  <Point X="-4.786452148438" Y="-0.65465435791" />
                  <Point X="-4.706558105469" Y="-0.633246704102" />
                  <Point X="-3.508288085938" Y="-0.312171295166" />
                  <Point X="-3.500476318359" Y="-0.309712615967" />
                  <Point X="-3.473932617188" Y="-0.299251800537" />
                  <Point X="-3.444167480469" Y="-0.283893554688" />
                  <Point X="-3.433561035156" Y="-0.27751361084" />
                  <Point X="-3.411148193359" Y="-0.261957550049" />
                  <Point X="-3.393671875" Y="-0.248242630005" />
                  <Point X="-3.371217529297" Y="-0.226359695435" />
                  <Point X="-3.360899414062" Y="-0.214485610962" />
                  <Point X="-3.341655273438" Y="-0.188230484009" />
                  <Point X="-3.333437744141" Y="-0.17481578064" />
                  <Point X="-3.319328857422" Y="-0.14681388855" />
                  <Point X="-3.3134375" Y="-0.132226409912" />
                  <Point X="-3.305966552734" Y="-0.108154548645" />
                  <Point X="-3.300990722656" Y="-0.08850050354" />
                  <Point X="-3.296721435547" Y="-0.060341369629" />
                  <Point X="-3.295647949219" Y="-0.046100917816" />
                  <Point X="-3.295647949219" Y="-0.016459270477" />
                  <Point X="-3.296721435547" Y="-0.002218966246" />
                  <Point X="-3.300990722656" Y="0.025940465927" />
                  <Point X="-3.304186523438" Y="0.039859596252" />
                  <Point X="-3.311657470703" Y="0.063931156158" />
                  <Point X="-3.319328857422" Y="0.084253700256" />
                  <Point X="-3.333437744141" Y="0.112255592346" />
                  <Point X="-3.341655273438" Y="0.125670295715" />
                  <Point X="-3.360899414062" Y="0.151925430298" />
                  <Point X="-3.371217529297" Y="0.1637996521" />
                  <Point X="-3.393671875" Y="0.185682434082" />
                  <Point X="-3.405808105469" Y="0.195691146851" />
                  <Point X="-3.428220947266" Y="0.211247070312" />
                  <Point X="-3.448554443359" Y="0.223550521851" />
                  <Point X="-3.469462402344" Y="0.234476974487" />
                  <Point X="-3.478960449219" Y="0.238794174194" />
                  <Point X="-3.498373291016" Y="0.246361450195" />
                  <Point X="-3.508288330078" Y="0.249611251831" />
                  <Point X="-3.793784912109" Y="0.326109680176" />
                  <Point X="-4.785446289062" Y="0.591824584961" />
                  <Point X="-4.746095703125" Y="0.857750732422" />
                  <Point X="-4.731330078125" Y="0.957536499023" />
                  <Point X="-4.633586914062" Y="1.318237182617" />
                  <Point X="-3.778066162109" Y="1.20560559082" />
                  <Point X="-3.76773828125" Y="1.204815429688" />
                  <Point X="-3.747057861328" Y="1.204364257812" />
                  <Point X="-3.736705322266" Y="1.204703125" />
                  <Point X="-3.715143554688" Y="1.20658984375" />
                  <Point X="-3.704889892578" Y="1.208053833008" />
                  <Point X="-3.684603271484" Y="1.212089233398" />
                  <Point X="-3.6745703125" Y="1.214660522461" />
                  <Point X="-3.624963378906" Y="1.230301513672" />
                  <Point X="-3.603450439453" Y="1.237676757812" />
                  <Point X="-3.584517333984" Y="1.246007568359" />
                  <Point X="-3.575278076172" Y="1.250689819336" />
                  <Point X="-3.556533935547" Y="1.261511962891" />
                  <Point X="-3.547859375" Y="1.26717199707" />
                  <Point X="-3.531178466797" Y="1.279403076172" />
                  <Point X="-3.515928955078" Y="1.293376708984" />
                  <Point X="-3.502290527344" Y="1.308928100586" />
                  <Point X="-3.495895263672" Y="1.317077148438" />
                  <Point X="-3.483480712891" Y="1.334807128906" />
                  <Point X="-3.478011230469" Y="1.343602416992" />
                  <Point X="-3.468062255859" Y="1.361736938477" />
                  <Point X="-3.463582763672" Y="1.371075927734" />
                  <Point X="-3.443677734375" Y="1.419130859375" />
                  <Point X="-3.435499023438" Y="1.44035144043" />
                  <Point X="-3.429710693359" Y="1.460210327148" />
                  <Point X="-3.427358886719" Y="1.470297485352" />
                  <Point X="-3.423600585938" Y="1.491611938477" />
                  <Point X="-3.422360595703" Y="1.501895385742" />
                  <Point X="-3.4210078125" Y="1.52253527832" />
                  <Point X="-3.42191015625" Y="1.543199951172" />
                  <Point X="-3.425056640625" Y="1.563643432617" />
                  <Point X="-3.427187744141" Y="1.573778930664" />
                  <Point X="-3.432789306641" Y="1.594684936523" />
                  <Point X="-3.43601171875" Y="1.604529663086" />
                  <Point X="-3.443509033203" Y="1.623808959961" />
                  <Point X="-3.447783935547" Y="1.633243530273" />
                  <Point X="-3.471801513672" Y="1.679380737305" />
                  <Point X="-3.482799560547" Y="1.699285888672" />
                  <Point X="-3.494290771484" Y="1.716483886719" />
                  <Point X="-3.500505859375" Y="1.724769042969" />
                  <Point X="-3.514418701172" Y="1.741350097656" />
                  <Point X="-3.5215" Y="1.748910766602" />
                  <Point X="-3.536442382812" Y="1.76321496582" />
                  <Point X="-3.544303710938" Y="1.769958862305" />
                  <Point X="-3.708065185547" Y="1.895617431641" />
                  <Point X="-4.227614257812" Y="2.294282226563" />
                  <Point X="-4.059661132812" Y="2.582026855469" />
                  <Point X="-4.002293212891" Y="2.680312011719" />
                  <Point X="-3.726338623047" Y="3.035012695312" />
                  <Point X="-3.254156982422" Y="2.7623984375" />
                  <Point X="-3.244918457031" Y="2.757716552734" />
                  <Point X="-3.225986083984" Y="2.749385986328" />
                  <Point X="-3.216292236328" Y="2.745737304688" />
                  <Point X="-3.195650146484" Y="2.739228759766" />
                  <Point X="-3.185615722656" Y="2.736656982422" />
                  <Point X="-3.165327880859" Y="2.732621582031" />
                  <Point X="-3.155074462891" Y="2.731157958984" />
                  <Point X="-3.085985839844" Y="2.725113525391" />
                  <Point X="-3.059173095703" Y="2.723334472656" />
                  <Point X="-3.038493896484" Y="2.723785644531" />
                  <Point X="-3.028166748047" Y="2.724575683594" />
                  <Point X="-3.006708251953" Y="2.727400634766" />
                  <Point X="-2.996528564453" Y="2.729310058594" />
                  <Point X="-2.976436523438" Y="2.734226318359" />
                  <Point X="-2.956998779297" Y="2.741301025391" />
                  <Point X="-2.938447265625" Y="2.750449462891" />
                  <Point X="-2.929421142578" Y="2.755530029297" />
                  <Point X="-2.911166748047" Y="2.767159179688" />
                  <Point X="-2.902746582031" Y="2.773193115234" />
                  <Point X="-2.886614501953" Y="2.786140625" />
                  <Point X="-2.878902587891" Y="2.793054199219" />
                  <Point X="-2.829863037109" Y="2.84209375" />
                  <Point X="-2.811265380859" Y="2.861489257812" />
                  <Point X="-2.798318603516" Y="2.877620361328" />
                  <Point X="-2.792285400391" Y="2.886039794922" />
                  <Point X="-2.780655761719" Y="2.904294433594" />
                  <Point X="-2.775575195312" Y="2.913319824219" />
                  <Point X="-2.766426269531" Y="2.931871337891" />
                  <Point X="-2.759351074219" Y="2.951309570312" />
                  <Point X="-2.754434570312" Y="2.971401611328" />
                  <Point X="-2.752524902344" Y="2.981581542969" />
                  <Point X="-2.749699707031" Y="3.003040527344" />
                  <Point X="-2.748909667969" Y="3.013368896484" />
                  <Point X="-2.748458496094" Y="3.034049072266" />
                  <Point X="-2.748797363281" Y="3.044400878906" />
                  <Point X="-2.754841796875" Y="3.113489501953" />
                  <Point X="-2.757745605469" Y="3.140203857422" />
                  <Point X="-2.761781005859" Y="3.160491699219" />
                  <Point X="-2.764352783203" Y="3.170526123047" />
                  <Point X="-2.770861328125" Y="3.191168212891" />
                  <Point X="-2.774510009766" Y="3.200862060547" />
                  <Point X="-2.782840576172" Y="3.219794433594" />
                  <Point X="-2.787522460938" Y="3.229032958984" />
                  <Point X="-2.860090087891" Y="3.354723632812" />
                  <Point X="-3.059387207031" Y="3.699916259766" />
                  <Point X="-2.748284179688" Y="3.938435791016" />
                  <Point X="-2.648374511719" Y="4.015036132812" />
                  <Point X="-2.192525390625" Y="4.268296386719" />
                  <Point X="-2.118563964844" Y="4.171908203125" />
                  <Point X="-2.111819824219" Y="4.164046875" />
                  <Point X="-2.097516357422" Y="4.14910546875" />
                  <Point X="-2.08995703125" Y="4.142025390625" />
                  <Point X="-2.073377197266" Y="4.12811328125" />
                  <Point X="-2.065092773438" Y="4.1218984375" />
                  <Point X="-2.047894287109" Y="4.11040625" />
                  <Point X="-2.038979858398" Y="4.10512890625" />
                  <Point X="-1.962084594727" Y="4.065099853516" />
                  <Point X="-1.943763916016" Y="4.055562255859" />
                  <Point X="-1.93432824707" Y="4.051286865234" />
                  <Point X="-1.915050170898" Y="4.043790283203" />
                  <Point X="-1.905207397461" Y="4.040568603516" />
                  <Point X="-1.884300292969" Y="4.034966552734" />
                  <Point X="-1.874166015625" Y="4.032835449219" />
                  <Point X="-1.853722412109" Y="4.029688476562" />
                  <Point X="-1.833056884766" Y="4.028785888672" />
                  <Point X="-1.812416870117" Y="4.030138427734" />
                  <Point X="-1.802133422852" Y="4.031378173828" />
                  <Point X="-1.780817626953" Y="4.035136474609" />
                  <Point X="-1.770729248047" Y="4.037488769531" />
                  <Point X="-1.750869628906" Y="4.04327734375" />
                  <Point X="-1.741098266602" Y="4.046713378906" />
                  <Point X="-1.661006713867" Y="4.079888671875" />
                  <Point X="-1.641924316406" Y="4.08779296875" />
                  <Point X="-1.632585449219" Y="4.092272705078" />
                  <Point X="-1.614451538086" Y="4.102221191406" />
                  <Point X="-1.60565625" Y="4.107690429687" />
                  <Point X="-1.587926879883" Y="4.120104492188" />
                  <Point X="-1.579778198242" Y="4.126499511719" />
                  <Point X="-1.564226318359" Y="4.140138183594" />
                  <Point X="-1.550252685547" Y="4.155387695312" />
                  <Point X="-1.538021240234" Y="4.172068847656" />
                  <Point X="-1.532360351562" Y="4.180744140625" />
                  <Point X="-1.521538330078" Y="4.19948828125" />
                  <Point X="-1.516855834961" Y="4.208728515625" />
                  <Point X="-1.508525390625" Y="4.227661621094" />
                  <Point X="-1.504877319336" Y="4.237354492188" />
                  <Point X="-1.478808959961" Y="4.320032714844" />
                  <Point X="-1.472598022461" Y="4.339730957031" />
                  <Point X="-1.470026733398" Y="4.349763671875" />
                  <Point X="-1.465991333008" Y="4.370050292969" />
                  <Point X="-1.46452734375" Y="4.380304199219" />
                  <Point X="-1.46264074707" Y="4.401866210938" />
                  <Point X="-1.462301757812" Y="4.41221875" />
                  <Point X="-1.462752929688" Y="4.432898925781" />
                  <Point X="-1.46354309082" Y="4.4432265625" />
                  <Point X="-1.47179309082" Y="4.505892578125" />
                  <Point X="-1.479266113281" Y="4.562654785156" />
                  <Point X="-1.060514892578" Y="4.680058105469" />
                  <Point X="-0.931174438477" Y="4.716320800781" />
                  <Point X="-0.365222167969" Y="4.782557128906" />
                  <Point X="-0.22566633606" Y="4.261728027344" />
                  <Point X="-0.220435256958" Y="4.247107910156" />
                  <Point X="-0.207661849976" Y="4.218916503906" />
                  <Point X="-0.200119247437" Y="4.205344726562" />
                  <Point X="-0.182260864258" Y="4.178618164062" />
                  <Point X="-0.17260824585" Y="4.166456054688" />
                  <Point X="-0.151451339722" Y="4.1438671875" />
                  <Point X="-0.126897941589" Y="4.125026367188" />
                  <Point X="-0.099602897644" Y="4.110436523438" />
                  <Point X="-0.085356651306" Y="4.104260742188" />
                  <Point X="-0.054918682098" Y="4.093927978516" />
                  <Point X="-0.039857234955" Y="4.090155273438" />
                  <Point X="-0.009319986343" Y="4.08511328125" />
                  <Point X="0.021631772995" Y="4.08511328125" />
                  <Point X="0.052169025421" Y="4.090155273438" />
                  <Point X="0.06723046875" Y="4.093927978516" />
                  <Point X="0.097668441772" Y="4.104260742188" />
                  <Point X="0.111914535522" Y="4.110436523438" />
                  <Point X="0.139209732056" Y="4.125026367188" />
                  <Point X="0.163763122559" Y="4.143866699219" />
                  <Point X="0.184920181274" Y="4.166455566406" />
                  <Point X="0.194572952271" Y="4.178617675781" />
                  <Point X="0.212431182861" Y="4.205344238281" />
                  <Point X="0.2199737854" Y="4.218916503906" />
                  <Point X="0.232747192383" Y="4.247107910156" />
                  <Point X="0.237978271484" Y="4.261727539062" />
                  <Point X="0.27516104126" Y="4.40049609375" />
                  <Point X="0.378190338135" Y="4.785006347656" />
                  <Point X="0.714931884766" Y="4.749740234375" />
                  <Point X="0.827876464844" Y="4.737912109375" />
                  <Point X="1.340695678711" Y="4.6141015625" />
                  <Point X="1.453597290039" Y="4.586843261719" />
                  <Point X="1.78613684082" Y="4.466229003906" />
                  <Point X="1.858259277344" Y="4.440069335938" />
                  <Point X="2.181024902344" Y="4.289122558594" />
                  <Point X="2.250451660156" Y="4.256653808594" />
                  <Point X="2.562308349609" Y="4.074965576172" />
                  <Point X="2.629434570312" Y="4.035857666016" />
                  <Point X="2.817780029297" Y="3.901916748047" />
                  <Point X="2.761147705078" Y="3.803826660156" />
                  <Point X="2.06530859375" Y="2.598597900391" />
                  <Point X="2.062373046875" Y="2.593104980469" />
                  <Point X="2.053181396484" Y="2.573438476562" />
                  <Point X="2.044182250977" Y="2.549564208984" />
                  <Point X="2.041301635742" Y="2.540598876953" />
                  <Point X="2.023963012695" Y="2.475760742188" />
                  <Point X="2.019876586914" Y="2.456549316406" />
                  <Point X="2.014407958984" Y="2.422198974609" />
                  <Point X="2.013243408203" Y="2.409061523438" />
                  <Point X="2.012745117188" Y="2.382752197266" />
                  <Point X="2.013411254883" Y="2.369580322266" />
                  <Point X="2.02017199707" Y="2.313513916016" />
                  <Point X="2.02380078125" Y="2.289035400391" />
                  <Point X="2.029143066406" Y="2.267112792969" />
                  <Point X="2.032467773437" Y="2.256310302734" />
                  <Point X="2.040733886719" Y="2.234221191406" />
                  <Point X="2.045318115234" Y="2.223889648438" />
                  <Point X="2.055681152344" Y="2.203843994141" />
                  <Point X="2.061459960938" Y="2.194129882813" />
                  <Point X="2.096152099609" Y="2.143002685547" />
                  <Point X="2.108343505859" Y="2.126941162109" />
                  <Point X="2.129909667969" Y="2.101459228516" />
                  <Point X="2.138909423828" Y="2.092186035156" />
                  <Point X="2.158083496094" Y="2.074947021484" />
                  <Point X="2.1682578125" Y="2.066981201172" />
                  <Point X="2.219385009766" Y="2.03228894043" />
                  <Point X="2.241280029297" Y="2.018244995117" />
                  <Point X="2.261325439453" Y="2.007881958008" />
                  <Point X="2.271657226562" Y="2.003297607422" />
                  <Point X="2.293744873047" Y="1.995032104492" />
                  <Point X="2.304547851562" Y="1.99170715332" />
                  <Point X="2.326470703125" Y="1.986364868164" />
                  <Point X="2.337590576172" Y="1.984346801758" />
                  <Point X="2.393657226562" Y="1.97758605957" />
                  <Point X="2.414794433594" Y="1.976226318359" />
                  <Point X="2.447055664062" Y="1.975953125" />
                  <Point X="2.459837890625" Y="1.976707763672" />
                  <Point X="2.485184326172" Y="1.979929199219" />
                  <Point X="2.497748535156" Y="1.982395874023" />
                  <Point X="2.562586669922" Y="1.99973449707" />
                  <Point X="2.573323486328" Y="2.002947875977" />
                  <Point X="2.600854736328" Y="2.012080444336" />
                  <Point X="2.609849853516" Y="2.015580932617" />
                  <Point X="2.636033935547" Y="2.027872558594" />
                  <Point X="2.923188476563" Y="2.193661132813" />
                  <Point X="3.940405029297" Y="2.780951416016" />
                  <Point X="4.004134765625" Y="2.692381591797" />
                  <Point X="4.043949951172" Y="2.637047363281" />
                  <Point X="4.136884765625" Y="2.483472167969" />
                  <Point X="4.089088378906" Y="2.446796630859" />
                  <Point X="3.172951416016" Y="1.743819824219" />
                  <Point X="3.168137939453" Y="1.739868774414" />
                  <Point X="3.152119384766" Y="1.725217041016" />
                  <Point X="3.134668701172" Y="1.706603515625" />
                  <Point X="3.128576416016" Y="1.699422485352" />
                  <Point X="3.081912353516" Y="1.638545410156" />
                  <Point X="3.071420654297" Y="1.623388305664" />
                  <Point X="3.051832763672" Y="1.591931518555" />
                  <Point X="3.045532958984" Y="1.580002807617" />
                  <Point X="3.034686767578" Y="1.555372924805" />
                  <Point X="3.030140380859" Y="1.54267199707" />
                  <Point X="3.0127578125" Y="1.480516479492" />
                  <Point X="3.006225097656" Y="1.454661010742" />
                  <Point X="3.002771484375" Y="1.432361450195" />
                  <Point X="3.001709228516" Y="1.421108520508" />
                  <Point X="3.000893310547" Y="1.3975390625" />
                  <Point X="3.001174804688" Y="1.386240112305" />
                  <Point X="3.003077880859" Y="1.363755981445" />
                  <Point X="3.004699462891" Y="1.352570800781" />
                  <Point X="3.01896875" Y="1.283414550781" />
                  <Point X="3.023725585938" Y="1.265048217773" />
                  <Point X="3.034323486328" Y="1.231193481445" />
                  <Point X="3.039140869141" Y="1.2188828125" />
                  <Point X="3.050438476562" Y="1.195048828125" />
                  <Point X="3.056918701172" Y="1.183525512695" />
                  <Point X="3.095729248047" Y="1.12453503418" />
                  <Point X="3.111739990234" Y="1.101423217773" />
                  <Point X="3.126293945313" Y="1.084178222656" />
                  <Point X="3.134084228516" Y="1.075989746094" />
                  <Point X="3.151328125" Y="1.059900268555" />
                  <Point X="3.160035644531" Y="1.052695068359" />
                  <Point X="3.178244873047" Y="1.039369750977" />
                  <Point X="3.187746337891" Y="1.033249755859" />
                  <Point X="3.24398828125" Y="1.001590209961" />
                  <Point X="3.261903564453" Y="0.99271282959" />
                  <Point X="3.293278808594" Y="0.979172058105" />
                  <Point X="3.305636474609" Y="0.974822753906" />
                  <Point X="3.330832519531" Y="0.967865478516" />
                  <Point X="3.343670898438" Y="0.965257568359" />
                  <Point X="3.419713867188" Y="0.955207397461" />
                  <Point X="3.430047119141" Y="0.954128356934" />
                  <Point X="3.461242431641" Y="0.951730529785" />
                  <Point X="3.47109765625" Y="0.951485839844" />
                  <Point X="3.500603759766" Y="0.952797180176" />
                  <Point X="3.773381347656" Y="0.988709228516" />
                  <Point X="4.704703613281" Y="1.111319946289" />
                  <Point X="4.735582519531" Y="0.984478210449" />
                  <Point X="4.75268359375" Y="0.914233459473" />
                  <Point X="4.78387109375" Y="0.713920837402" />
                  <Point X="4.741904785156" Y="0.702675964355" />
                  <Point X="3.691991943359" Y="0.421352874756" />
                  <Point X="3.686031494141" Y="0.419544281006" />
                  <Point X="3.665625732422" Y="0.412137512207" />
                  <Point X="3.642381103516" Y="0.401619171143" />
                  <Point X="3.634004882812" Y="0.397316711426" />
                  <Point X="3.559295166016" Y="0.354133117676" />
                  <Point X="3.541494873047" Y="0.343844390869" />
                  <Point X="3.533881103516" Y="0.338945343018" />
                  <Point X="3.508774414062" Y="0.319870788574" />
                  <Point X="3.481993896484" Y="0.294350891113" />
                  <Point X="3.472796875" Y="0.284226715088" />
                  <Point X="3.427970947266" Y="0.227108108521" />
                  <Point X="3.410854736328" Y="0.204208724976" />
                  <Point X="3.399130371094" Y="0.184928482056" />
                  <Point X="3.393842285156" Y="0.174938644409" />
                  <Point X="3.384068603516" Y="0.153475112915" />
                  <Point X="3.380004882812" Y="0.142928543091" />
                  <Point X="3.373158691406" Y="0.121427711487" />
                  <Point X="3.370376220703" Y="0.110473304749" />
                  <Point X="3.355434082031" Y="0.032451782227" />
                  <Point X="3.352897949219" Y="0.01471899128" />
                  <Point X="3.349337890625" Y="-0.022679584503" />
                  <Point X="3.349016113281" Y="-0.036160274506" />
                  <Point X="3.350284179688" Y="-0.063031890869" />
                  <Point X="3.351874023438" Y="-0.076422813416" />
                  <Point X="3.366816162109" Y="-0.15444418335" />
                  <Point X="3.373158935547" Y="-0.18398789978" />
                  <Point X="3.380005126953" Y="-0.205488571167" />
                  <Point X="3.384068603516" Y="-0.216035003662" />
                  <Point X="3.393842285156" Y="-0.237498382568" />
                  <Point X="3.399130371094" Y="-0.247488372803" />
                  <Point X="3.410854736328" Y="-0.266768615723" />
                  <Point X="3.417291015625" Y="-0.276058898926" />
                  <Point X="3.462116943359" Y="-0.333177642822" />
                  <Point X="3.47453125" Y="-0.347469085693" />
                  <Point X="3.4995234375" Y="-0.373498596191" />
                  <Point X="3.509436035156" Y="-0.382464874268" />
                  <Point X="3.530421875" Y="-0.398917999268" />
                  <Point X="3.541494873047" Y="-0.406404449463" />
                  <Point X="3.616204833984" Y="-0.449587860107" />
                  <Point X="3.634004882812" Y="-0.459876739502" />
                  <Point X="3.639495849609" Y="-0.462814849854" />
                  <Point X="3.659156738281" Y="-0.472016113281" />
                  <Point X="3.68302734375" Y="-0.481027557373" />
                  <Point X="3.691991943359" Y="-0.48391293335" />
                  <Point X="3.942141357422" Y="-0.550940246582" />
                  <Point X="4.784876953125" Y="-0.776750610352" />
                  <Point X="4.771162109375" Y="-0.867718017578" />
                  <Point X="4.761612304688" Y="-0.931059448242" />
                  <Point X="4.727802246094" Y="-1.079219848633" />
                  <Point X="4.654385742188" Y="-1.069554321289" />
                  <Point X="3.436782226562" Y="-0.909253845215" />
                  <Point X="3.428624511719" Y="-0.908535827637" />
                  <Point X="3.400098388672" Y="-0.908042541504" />
                  <Point X="3.366721435547" Y="-0.910840820312" />
                  <Point X="3.354481201172" Y="-0.912676330566" />
                  <Point X="3.207852294922" Y="-0.944546691895" />
                  <Point X="3.172916992188" Y="-0.952140014648" />
                  <Point X="3.157874023438" Y="-0.956742431641" />
                  <Point X="3.128753662109" Y="-0.968366943359" />
                  <Point X="3.114676269531" Y="-0.975388916016" />
                  <Point X="3.086849609375" Y="-0.99228125" />
                  <Point X="3.074124023438" Y="-1.001530395508" />
                  <Point X="3.050374023438" Y="-1.022001098633" />
                  <Point X="3.039349609375" Y="-1.033222900391" />
                  <Point X="2.950721679688" Y="-1.139814575195" />
                  <Point X="2.92960546875" Y="-1.165211181641" />
                  <Point X="2.921326171875" Y="-1.17684753418" />
                  <Point X="2.90660546875" Y="-1.201229858398" />
                  <Point X="2.9001640625" Y="-1.213975952148" />
                  <Point X="2.888820800781" Y="-1.241361206055" />
                  <Point X="2.884362792969" Y="-1.254928222656" />
                  <Point X="2.877531005859" Y="-1.282577758789" />
                  <Point X="2.875157226562" Y="-1.29666027832" />
                  <Point X="2.862454589844" Y="-1.434701293945" />
                  <Point X="2.859428222656" Y="-1.467590332031" />
                  <Point X="2.859288818359" Y="-1.483321289062" />
                  <Point X="2.861607666016" Y="-1.514590454102" />
                  <Point X="2.864065917969" Y="-1.530128662109" />
                  <Point X="2.871797607422" Y="-1.561749633789" />
                  <Point X="2.876786621094" Y="-1.576669189453" />
                  <Point X="2.889157958984" Y="-1.605479980469" />
                  <Point X="2.896540283203" Y="-1.619371337891" />
                  <Point X="2.977686767578" Y="-1.745589477539" />
                  <Point X="2.997020507812" Y="-1.775661865234" />
                  <Point X="3.001741943359" Y="-1.782353149414" />
                  <Point X="3.019792724609" Y="-1.804448852539" />
                  <Point X="3.043488769531" Y="-1.828119873047" />
                  <Point X="3.052796142578" Y="-1.836277709961" />
                  <Point X="3.284936279297" Y="-2.014405273438" />
                  <Point X="4.087170166016" Y="-2.629981689453" />
                  <Point X="4.072404785156" Y="-2.653874267578" />
                  <Point X="4.045484863281" Y="-2.697435546875" />
                  <Point X="4.001274414063" Y="-2.760251953125" />
                  <Point X="3.932895019531" Y="-2.720772949219" />
                  <Point X="2.848454589844" Y="-2.094670654297" />
                  <Point X="2.841191894531" Y="-2.090885253906" />
                  <Point X="2.815026123047" Y="-2.079513183594" />
                  <Point X="2.783119140625" Y="-2.069325927734" />
                  <Point X="2.771108398438" Y="-2.066337646484" />
                  <Point X="2.596596679688" Y="-2.034821044922" />
                  <Point X="2.555018066406" Y="-2.027312011719" />
                  <Point X="2.539359130859" Y="-2.025807495117" />
                  <Point X="2.508006835938" Y="-2.025403198242" />
                  <Point X="2.492313476562" Y="-2.026503662109" />
                  <Point X="2.460140869141" Y="-2.031461425781" />
                  <Point X="2.444844726562" Y="-2.035136230469" />
                  <Point X="2.415068847656" Y="-2.044959838867" />
                  <Point X="2.400589111328" Y="-2.051108642578" />
                  <Point X="2.255612792969" Y="-2.127408691406" />
                  <Point X="2.221071044922" Y="-2.145587646484" />
                  <Point X="2.208968505859" Y="-2.153170410156" />
                  <Point X="2.186037841797" Y="-2.170063476562" />
                  <Point X="2.175209716797" Y="-2.179373779297" />
                  <Point X="2.154249755859" Y="-2.200333740234" />
                  <Point X="2.144939453125" Y="-2.211161865234" />
                  <Point X="2.128046386719" Y="-2.234092529297" />
                  <Point X="2.120463623047" Y="-2.246195068359" />
                  <Point X="2.044163696289" Y="-2.391171386719" />
                  <Point X="2.025984741211" Y="-2.425713134766" />
                  <Point X="2.0198359375" Y="-2.440192871094" />
                  <Point X="2.010012207031" Y="-2.46996875" />
                  <Point X="2.006337280273" Y="-2.485264892578" />
                  <Point X="2.001379516602" Y="-2.5174375" />
                  <Point X="2.000279174805" Y="-2.533130859375" />
                  <Point X="2.00068347168" Y="-2.564483154297" />
                  <Point X="2.002187866211" Y="-2.580142089844" />
                  <Point X="2.033704467773" Y="-2.754653808594" />
                  <Point X="2.041213500977" Y="-2.796232421875" />
                  <Point X="2.043015014648" Y="-2.804221435547" />
                  <Point X="2.051236083984" Y="-2.83154296875" />
                  <Point X="2.064069580078" Y="-2.862479736328" />
                  <Point X="2.069546630859" Y="-2.873578613281" />
                  <Point X="2.218719970703" Y="-3.131954589844" />
                  <Point X="2.735893310547" Y="-4.027724121094" />
                  <Point X="2.72375390625" Y="-4.036083740234" />
                  <Point X="2.661442626953" Y="-3.954878173828" />
                  <Point X="1.833914550781" Y="-2.876422607422" />
                  <Point X="1.828654418945" Y="-2.870147216797" />
                  <Point X="1.808830932617" Y="-2.849625732422" />
                  <Point X="1.783251586914" Y="-2.828004150391" />
                  <Point X="1.773299072266" Y="-2.820647216797" />
                  <Point X="1.601183349609" Y="-2.709992675781" />
                  <Point X="1.56017565918" Y="-2.683628417969" />
                  <Point X="1.546283691406" Y="-2.676245849609" />
                  <Point X="1.517472412109" Y="-2.663874511719" />
                  <Point X="1.502553222656" Y="-2.658885742188" />
                  <Point X="1.470931762695" Y="-2.651154052734" />
                  <Point X="1.455393920898" Y="-2.648695800781" />
                  <Point X="1.424125244141" Y="-2.646376953125" />
                  <Point X="1.408394287109" Y="-2.646516357422" />
                  <Point X="1.22015637207" Y="-2.663838134766" />
                  <Point X="1.175307373047" Y="-2.667965332031" />
                  <Point X="1.161225097656" Y="-2.670339111328" />
                  <Point X="1.133575317383" Y="-2.677170898438" />
                  <Point X="1.120007568359" Y="-2.68162890625" />
                  <Point X="1.092622680664" Y="-2.692972167969" />
                  <Point X="1.079876708984" Y="-2.699413574219" />
                  <Point X="1.055494873047" Y="-2.714134033203" />
                  <Point X="1.043858764648" Y="-2.722413085938" />
                  <Point X="0.898506469727" Y="-2.843268798828" />
                  <Point X="0.863875244141" Y="-2.872063476562" />
                  <Point X="0.852653320312" Y="-2.883088134766" />
                  <Point X="0.832182434082" Y="-2.906838134766" />
                  <Point X="0.822933654785" Y="-2.919563720703" />
                  <Point X="0.806041137695" Y="-2.947390380859" />
                  <Point X="0.799019287109" Y="-2.961467529297" />
                  <Point X="0.787394470215" Y="-2.990588134766" />
                  <Point X="0.782791931152" Y="-3.005631347656" />
                  <Point X="0.739332214355" Y="-3.205579833984" />
                  <Point X="0.728977661133" Y="-3.253219238281" />
                  <Point X="0.727584777832" Y="-3.261289306641" />
                  <Point X="0.724724487305" Y="-3.289677734375" />
                  <Point X="0.7247421875" Y="-3.323170410156" />
                  <Point X="0.725554992676" Y="-3.335520019531" />
                  <Point X="0.767829467773" Y="-3.656627441406" />
                  <Point X="0.833091491699" Y="-4.152340820313" />
                  <Point X="0.655064941406" Y="-3.487936523438" />
                  <Point X="0.652606445312" Y="-3.480125" />
                  <Point X="0.642145446777" Y="-3.453580810547" />
                  <Point X="0.62678704834" Y="-3.423815673828" />
                  <Point X="0.620407592773" Y="-3.413210205078" />
                  <Point X="0.488183288574" Y="-3.222700195312" />
                  <Point X="0.456679840088" Y="-3.177309814453" />
                  <Point X="0.446670837402" Y="-3.165173095703" />
                  <Point X="0.424786987305" Y="-3.142717529297" />
                  <Point X="0.412911743164" Y="-3.132398681641" />
                  <Point X="0.386656311035" Y="-3.113154785156" />
                  <Point X="0.373242340088" Y="-3.104937744141" />
                  <Point X="0.345241485596" Y="-3.090829589844" />
                  <Point X="0.330654754639" Y="-3.084938476562" />
                  <Point X="0.126045303345" Y="-3.021435058594" />
                  <Point X="0.077295509338" Y="-3.006305175781" />
                  <Point X="0.06337638092" Y="-3.003109130859" />
                  <Point X="0.035216796875" Y="-2.99883984375" />
                  <Point X="0.020976495743" Y="-2.997766601562" />
                  <Point X="-0.008664708138" Y="-2.997766601562" />
                  <Point X="-0.022905010223" Y="-2.99883984375" />
                  <Point X="-0.051064441681" Y="-3.003109130859" />
                  <Point X="-0.064983718872" Y="-3.006305175781" />
                  <Point X="-0.269593017578" Y="-3.069808349609" />
                  <Point X="-0.318342803955" Y="-3.084938476562" />
                  <Point X="-0.332929718018" Y="-3.090829589844" />
                  <Point X="-0.360930999756" Y="-3.104937988281" />
                  <Point X="-0.374345275879" Y="-3.113155273438" />
                  <Point X="-0.400600524902" Y="-3.132399169922" />
                  <Point X="-0.412475219727" Y="-3.142717773438" />
                  <Point X="-0.434358886719" Y="-3.165173095703" />
                  <Point X="-0.444368041992" Y="-3.177309814453" />
                  <Point X="-0.576592224121" Y="-3.367819824219" />
                  <Point X="-0.60809564209" Y="-3.413210205078" />
                  <Point X="-0.612470336914" Y="-3.420132324219" />
                  <Point X="-0.625976745605" Y="-3.445265136719" />
                  <Point X="-0.638777832031" Y="-3.476215820312" />
                  <Point X="-0.642752990723" Y="-3.487936523438" />
                  <Point X="-0.719830200195" Y="-3.775592529297" />
                  <Point X="-0.985425170898" Y="-4.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.055684394304" Y="2.617656143678" />
                  <Point X="3.903451055908" Y="2.759616028345" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.769030592251" Y="3.817480224729" />
                  <Point X="2.377366186398" Y="4.182713191889" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.094861665121" Y="2.451226638803" />
                  <Point X="3.817419473301" Y="2.709945668206" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.72028114987" Y="3.733043706385" />
                  <Point X="2.066548262644" Y="4.342659486028" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.018445104733" Y="2.392590125391" />
                  <Point X="3.731387890694" Y="2.660275308067" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.67153170646" Y="3.648607189001" />
                  <Point X="1.800215833489" Y="4.461122385338" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.942028539391" Y="2.333953616599" />
                  <Point X="3.645356308087" Y="2.610604947928" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.62278226305" Y="3.564170671617" />
                  <Point X="1.572251234194" Y="4.543806704482" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.865611974048" Y="2.275317107808" />
                  <Point X="3.55932472548" Y="2.560934587789" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.574032819641" Y="3.479734154233" />
                  <Point X="1.363468816209" Y="4.608603350168" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.789195408706" Y="2.216680599016" />
                  <Point X="3.473293142873" Y="2.51126422765" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.525283376231" Y="3.395297636849" />
                  <Point X="1.17550875205" Y="4.653982836784" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.712778843363" Y="2.158044090224" />
                  <Point X="3.387261560266" Y="2.461593867511" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.476533932821" Y="3.310861119466" />
                  <Point X="0.987548784723" Y="4.699362233102" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.70727251547" Y="1.100767629112" />
                  <Point X="4.697038675188" Y="1.110310839564" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.636362278021" Y="2.099407581433" />
                  <Point X="3.301229977659" Y="2.411923507372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.427784489411" Y="3.226424602082" />
                  <Point X="0.804260406758" Y="4.740385301863" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.748182471411" Y="0.932722369219" />
                  <Point X="4.574975068873" Y="1.094240885117" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.559945712678" Y="2.040771072641" />
                  <Point X="3.215198395052" Y="2.362253147233" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.379035046001" Y="3.141988084698" />
                  <Point X="0.647341055339" Y="4.756818855564" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.773739608304" Y="0.778993844704" />
                  <Point X="4.452911462558" Y="1.07817093067" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.483529147336" Y="1.982134563849" />
                  <Point X="3.129166812445" Y="2.312582787094" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.330285602592" Y="3.057551567314" />
                  <Point X="0.490421433666" Y="4.773252661281" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.722002668963" Y="0.697343212346" />
                  <Point X="4.330847856243" Y="1.062100976222" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.407112581993" Y="1.923498055057" />
                  <Point X="3.043135229837" Y="2.262912426955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.281536159182" Y="2.97311504993" />
                  <Point X="0.37025977596" Y="4.755409111063" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.613797758071" Y="0.668349815342" />
                  <Point X="4.208784249928" Y="1.046031021775" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.330696016651" Y="1.864861546266" />
                  <Point X="2.95710364723" Y="2.213242066816" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.232786715772" Y="2.888678532546" />
                  <Point X="0.342412341201" Y="4.651481155282" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.50559284718" Y="0.639356418338" />
                  <Point X="4.086720643613" Y="1.029961067328" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.254279451308" Y="1.806225037474" />
                  <Point X="2.871072042199" Y="2.163571727588" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.184037272362" Y="2.804242015162" />
                  <Point X="0.314564906442" Y="4.547553199501" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.397387936289" Y="0.610363021334" />
                  <Point X="3.964657037298" Y="1.013891112881" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.177862885966" Y="1.747588528682" />
                  <Point X="2.785040422574" Y="2.113901401969" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.135287828952" Y="2.719805497778" />
                  <Point X="0.286717471683" Y="4.443625243719" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.289183025397" Y="0.58136962433" />
                  <Point X="3.842593430983" Y="0.997821158433" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.112587080529" Y="1.678563093207" />
                  <Point X="2.699008802949" Y="2.06423107635" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.086538385543" Y="2.635368980394" />
                  <Point X="0.258870101078" Y="4.339697228113" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.180978114506" Y="0.552376227326" />
                  <Point X="3.720529870488" Y="0.981751161258" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.057107560178" Y="1.600402474102" />
                  <Point X="2.611202330647" Y="2.016215827628" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.043046573451" Y="2.546029642489" />
                  <Point X="0.228623426904" Y="4.238006599281" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.072773203614" Y="0.523382830322" />
                  <Point X="3.598466369997" Y="0.965681108128" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.019710991711" Y="1.505379229563" />
                  <Point X="2.505850031342" Y="1.984562327284" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.017269186849" Y="2.440171335572" />
                  <Point X="0.17612544806" Y="4.157065647741" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.322116173402" Y="4.621683476295" />
                  <Point X="-0.480201569914" Y="4.769100493441" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.964568292723" Y="0.494389433318" />
                  <Point X="3.474242526571" Y="0.951625607377" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.001008295273" Y="1.392923667339" />
                  <Point X="2.371120389534" Y="1.980303642016" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.021194784518" Y="2.306614547719" />
                  <Point X="0.094579676941" Y="4.103212200715" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.275716970355" Y="4.448519410666" />
                  <Point X="-0.603965159423" Y="4.754615798968" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.856363381832" Y="0.465396036314" />
                  <Point X="3.311935223163" Y="0.97308350759" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.03398432976" Y="1.232276908894" />
                  <Point X="-0.028748027575" Y="4.088321036911" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.229317767309" Y="4.275355345036" />
                  <Point X="-0.727728748931" Y="4.740131104495" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.74815847094" Y="0.43640263931" />
                  <Point X="-0.85149233844" Y="4.725646410022" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.644927165467" Y="0.402771280221" />
                  <Point X="-0.969319752964" Y="4.705626142822" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.783106419415" Y="-0.788494153619" />
                  <Point X="4.764721549754" Y="-0.771349985303" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.558365147706" Y="0.353595558865" />
                  <Point X="-1.076416895668" Y="4.675599735272" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.765936381658" Y="-0.902378943185" />
                  <Point X="4.56926144617" Y="-0.718976598778" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.482294039696" Y="0.294636905899" />
                  <Point X="-1.183514106626" Y="4.645573391369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.743326387643" Y="-1.011190891473" />
                  <Point X="4.373801342586" Y="-0.666603212252" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.422813155931" Y="0.220207618543" />
                  <Point X="-1.290611317584" Y="4.615547047466" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.66862766922" Y="-1.071429318433" />
                  <Point X="4.178341239002" Y="-0.614229825726" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.376887412033" Y="0.133137958766" />
                  <Point X="-1.397708528541" Y="4.585520703563" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.506432413381" Y="-1.050075904267" />
                  <Point X="3.982881135418" Y="-0.561856439201" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.35427258389" Y="0.024330518375" />
                  <Point X="-1.474617149706" Y="4.527343044248" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.344237173366" Y="-1.028722504858" />
                  <Point X="3.78742106214" Y="-0.509483080935" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.358147224886" Y="-0.109178751611" />
                  <Point X="-1.46390170787" Y="4.387454624277" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.182041933351" Y="-1.007369105449" />
                  <Point X="3.524997308895" Y="-0.394665080877" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.475775702941" Y="-0.348765190762" />
                  <Point X="-1.490644165675" Y="4.282496260817" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.019846693336" Y="-0.986015706039" />
                  <Point X="-1.528323281132" Y="4.187736495608" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.857651453321" Y="-0.96466230663" />
                  <Point X="-1.592019976234" Y="4.117238515924" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.695456213306" Y="-0.943308907221" />
                  <Point X="-1.681957494482" Y="4.071210499695" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.533260973291" Y="-0.921955507811" />
                  <Point X="-1.782290817807" Y="4.034876728534" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.380781338634" Y="-0.909662056969" />
                  <Point X="-1.943777355526" Y="4.05556925236" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.265465353721" Y="-0.932024270169" />
                  <Point X="-2.266890271068" Y="4.226980811825" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.154234017537" Y="-0.958195479931" />
                  <Point X="-2.354180516673" Y="4.178484173919" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.067516157497" Y="-1.007225876011" />
                  <Point X="-2.441470762277" Y="4.129987536014" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.002990827532" Y="-1.076951141185" />
                  <Point X="-2.528761007881" Y="4.081490898108" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.942155400161" Y="-1.150117296194" />
                  <Point X="-2.616051253486" Y="4.032994260202" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.89213241133" Y="-1.23336621326" />
                  <Point X="-2.696512177855" Y="3.978129177217" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.870854343595" Y="-1.343420202898" />
                  <Point X="-2.772957218902" Y="3.919519222449" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.85984593835" Y="-1.463050807737" />
                  <Point X="-2.849402447233" Y="3.860909442327" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.04500148992" Y="-2.6981223478" />
                  <Point X="3.436613878281" Y="-2.130791721727" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.909051682707" Y="-1.638832015478" />
                  <Point X="-2.925847675565" Y="3.802299662205" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.925280147172" Y="-2.716376498359" />
                  <Point X="-3.002292903896" Y="3.743689882083" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.559545111384" Y="-2.505219168761" />
                  <Point X="-3.018261827472" Y="3.628685035423" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.193810075595" Y="-2.294061839163" />
                  <Point X="-2.855797286551" Y="3.347288291247" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.832654174544" Y="-2.087174621791" />
                  <Point X="-2.756022378559" Y="3.124350575522" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.64697128735" Y="-2.043918637049" />
                  <Point X="-2.751397865255" Y="2.990142038296" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.489469465707" Y="-2.026941921077" />
                  <Point X="-2.787355948195" Y="2.893777384302" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.384927618051" Y="-2.059351179809" />
                  <Point X="-2.849823539927" Y="2.822133247182" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.295884999447" Y="-2.106213703456" />
                  <Point X="-2.922455517134" Y="2.759967552859" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.207837160752" Y="-2.154003874376" />
                  <Point X="-3.024339250991" Y="2.725079562908" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.138788547766" Y="-2.219511109894" />
                  <Point X="-3.173457707092" Y="2.734238664039" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.090059086538" Y="-2.303966260964" />
                  <Point X="-3.487295560613" Y="2.897001088244" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.044201527523" Y="-2.391099504173" />
                  <Point X="-3.746626725313" Y="3.008935202829" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.006280456382" Y="-2.485633642056" />
                  <Point X="-3.805194574777" Y="2.933654497212" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.009044295314" Y="-2.61810707236" />
                  <Point X="-3.863762424241" Y="2.858373791596" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.037254232275" Y="-2.774309372959" />
                  <Point X="-3.922330273705" Y="2.783093085979" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.145515630982" Y="-3.005160869305" />
                  <Point X="-3.980898123169" Y="2.707812380363" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.307980156173" Y="-3.286557598812" />
                  <Point X="-4.033454230024" Y="2.62692563407" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.470444959105" Y="-3.567954587317" />
                  <Point X="2.098158034147" Y="-3.220791413421" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.49311262801" Y="-2.6565774444" />
                  <Point X="-4.082550218132" Y="2.542812274845" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.632909762037" Y="-3.849351575822" />
                  <Point X="2.448555963724" Y="-3.677438877708" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.348898007943" Y="-2.65199124435" />
                  <Point X="-4.131646194217" Y="2.458698904408" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.222112672065" Y="-2.663658114747" />
                  <Point X="-4.180742170301" Y="2.374585533971" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.107599177031" Y="-2.686768661867" />
                  <Point X="-3.421630992873" Y="1.536806800159" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.024123429179" Y="-2.738822376472" />
                  <Point X="-3.441313557818" Y="1.425264980099" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.950485453388" Y="-2.800049961939" />
                  <Point X="-3.483530168788" Y="1.33473649791" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.876847455726" Y="-2.86127752701" />
                  <Point X="-3.549338018695" Y="1.266207201931" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.814663810129" Y="-2.933186448185" />
                  <Point X="-3.643771181386" Y="1.224371441967" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.777780412056" Y="-3.028688231857" />
                  <Point X="-3.76196129459" Y="1.204689396758" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.754305038713" Y="-3.136693200867" />
                  <Point X="-3.922653133238" Y="1.224640851712" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.730829726068" Y="-3.244698226478" />
                  <Point X="-4.084848419586" Y="1.245994294327" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.730680943818" Y="-3.37445559359" />
                  <Point X="-4.247043705934" Y="1.267347736943" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.750175320579" Y="-3.522530502819" />
                  <Point X="0.597408972363" Y="-3.380073578453" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.256102488586" Y="-3.061800133335" />
                  <Point X="-4.409238992282" Y="1.288701179558" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.769669707086" Y="-3.670605421136" />
                  <Point X="0.682140216395" Y="-3.588982850585" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.052021462614" Y="-3.001387606625" />
                  <Point X="-4.57143427863" Y="1.310054622174" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.789164187088" Y="-3.818680426639" />
                  <Point X="0.728539361983" Y="-3.762146862634" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.077752019969" Y="-3.010267985141" />
                  <Point X="-3.296209678599" Y="-0.009007664373" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.599855361486" Y="0.27414651576" />
                  <Point X="-4.650918466325" Y="1.254278717504" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.80865866709" Y="-3.966755432142" />
                  <Point X="0.774938507571" Y="-3.935310874683" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.182264361952" Y="-3.042704758358" />
                  <Point X="-3.311177862475" Y="-0.124945715901" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.795315297113" Y="0.326519745663" />
                  <Point X="-4.679017442674" Y="1.150585328051" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.828153147093" Y="-4.114830437644" />
                  <Point X="0.821337653159" Y="-4.108474886732" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.286776706006" Y="-3.075141529645" />
                  <Point X="-3.358282818489" Y="-0.21091574259" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.990775397989" Y="0.378893129663" />
                  <Point X="-4.707116419024" Y="1.046891938598" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.380483697213" Y="-3.117654455472" />
                  <Point X="-3.429319119963" Y="-0.274569428607" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.186235498864" Y="0.431266513664" />
                  <Point X="-4.733665567799" Y="0.941753311551" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.448862846219" Y="-3.183785976251" />
                  <Point X="-3.523827483023" Y="-0.316335063092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.38169559974" Y="0.483639897664" />
                  <Point X="-4.75055610923" Y="0.827607887444" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.503594527943" Y="-3.262643966158" />
                  <Point X="-3.632032381666" Y="-0.345328471518" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.577155700616" Y="0.536013281664" />
                  <Point X="-4.767446824838" Y="0.713462625759" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.558326209667" Y="-3.341501956065" />
                  <Point X="-3.740237280309" Y="-0.374321879943" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.772615801492" Y="0.588386665665" />
                  <Point X="-4.784337540445" Y="0.599317364074" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.612747972394" Y="-3.420648950112" />
                  <Point X="-3.848442178952" Y="-0.403315288369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.6501767059" Y="-3.515642200267" />
                  <Point X="-2.949072094431" Y="-1.37188756901" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.240616470328" Y="-1.100018040207" />
                  <Point X="-3.956647077595" Y="-0.432308696795" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.678024126645" Y="-3.619570169117" />
                  <Point X="-2.975419763741" Y="-1.477214078698" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.362971629585" Y="-1.115816117138" />
                  <Point X="-4.064851976238" Y="-0.461302105221" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.705871547391" Y="-3.723498137966" />
                  <Point X="-3.029926802852" Y="-1.556281551231" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.48503521433" Y="-1.1318860917" />
                  <Point X="-4.173056874881" Y="-0.490295513646" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.733718963883" Y="-3.827426110782" />
                  <Point X="-3.104202707151" Y="-1.61691425874" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.607098800385" Y="-1.147956065039" />
                  <Point X="-4.281261773524" Y="-0.519288922072" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.7615663761" Y="-3.931354087583" />
                  <Point X="-3.180619287835" Y="-1.675550753225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.729162388359" Y="-1.16402603659" />
                  <Point X="-4.389466672167" Y="-0.548282330498" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.789413788318" Y="-4.035282064385" />
                  <Point X="-2.311529280865" Y="-2.615886404741" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.595908786251" Y="-2.35069822578" />
                  <Point X="-3.257035868519" Y="-1.734187247711" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.851225976333" Y="-1.18009600814" />
                  <Point X="-4.49767157081" Y="-0.577275738924" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.817261200535" Y="-4.139210041187" />
                  <Point X="-2.339429334333" Y="-2.719765292782" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.697956553941" Y="-2.385433251706" />
                  <Point X="-3.333452449203" Y="-1.792823742196" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.973289564307" Y="-1.196165979691" />
                  <Point X="-4.605876469453" Y="-0.60626914735" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.845108612752" Y="-4.243138017988" />
                  <Point X="-2.387883317633" Y="-2.804477331176" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.783988136267" Y="-2.435103612108" />
                  <Point X="-3.409869029887" Y="-1.851460236681" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.095353152281" Y="-1.212235951242" />
                  <Point X="-4.714081359112" Y="-0.635262564152" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.87295602497" Y="-4.34706599479" />
                  <Point X="-2.436632767579" Y="-2.888913842464" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.870019718592" Y="-2.484773972509" />
                  <Point X="-3.486285610572" Y="-1.910096731167" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.217416740255" Y="-1.228305922792" />
                  <Point X="-4.779352869073" Y="-0.704292005223" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.900803437187" Y="-4.450993971592" />
                  <Point X="-1.186337149477" Y="-4.184729477281" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.593611154443" Y="-3.804940323458" />
                  <Point X="-2.485382217525" Y="-2.973350353753" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.956051305154" Y="-2.534444328961" />
                  <Point X="-3.562702191256" Y="-1.968733225652" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.339480328229" Y="-1.244375894343" />
                  <Point X="-4.757915789398" Y="-0.854178514227" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.928650849405" Y="-4.554921948394" />
                  <Point X="-1.153692839616" Y="-4.345066897507" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.750288834183" Y="-3.788732132244" />
                  <Point X="-2.534131667471" Y="-3.057786865042" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.042082904612" Y="-2.584114673386" />
                  <Point X="-3.63911877194" Y="-2.027369720138" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.461543916202" Y="-1.260445865894" />
                  <Point X="-4.732060483602" Y="-1.008185085744" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.956498261622" Y="-4.658849925195" />
                  <Point X="-1.122469572724" Y="-4.504079173727" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.900116235433" Y="-3.778911929066" />
                  <Point X="-2.582881117417" Y="-3.142223376331" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.128114504071" Y="-2.63378501781" />
                  <Point X="-3.715535352624" Y="-2.086006214623" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.583607504176" Y="-1.276515837444" />
                  <Point X="-4.688507041928" Y="-1.178695435962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.98434567384" Y="-4.762777901997" />
                  <Point X="-1.127777790738" Y="-4.629025289152" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.029875540666" Y="-3.787805528174" />
                  <Point X="-2.631630567363" Y="-3.22665988762" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.21414610353" Y="-2.683455362235" />
                  <Point X="-3.791951933308" Y="-2.144642709108" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.120896304719" Y="-3.832823401347" />
                  <Point X="-2.680380017309" Y="-3.311096398908" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.300177702988" Y="-2.73312570666" />
                  <Point X="-3.868368513992" Y="-2.203279203594" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.202046120434" Y="-3.88704608276" />
                  <Point X="-2.729129467255" Y="-3.395532910197" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.386209302447" Y="-2.782796051084" />
                  <Point X="-3.944785094676" Y="-2.261915698079" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.283195936149" Y="-3.941268764172" />
                  <Point X="-2.777878917201" Y="-3.479969421486" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.472240901906" Y="-2.832466395509" />
                  <Point X="-4.02120167536" Y="-2.320552192564" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.363685112733" Y="-3.996107501541" />
                  <Point X="-2.826628367147" Y="-3.564405932775" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.558272501364" Y="-2.882136739933" />
                  <Point X="-4.097618256044" Y="-2.37918868705" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.428695886026" Y="-4.065380083489" />
                  <Point X="-2.875377817093" Y="-3.648842444064" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.644304100823" Y="-2.931807084358" />
                  <Point X="-4.174034649005" Y="-2.43782535659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.486795871755" Y="-4.141097079097" />
                  <Point X="-2.924127267039" Y="-3.733278955352" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.730335700281" Y="-2.981477428783" />
                  <Point X="-4.023264726011" Y="-2.708316693122" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.849953185644" Y="-3.932343514058" />
                  <Point X="-2.972876760456" Y="-3.817715426104" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="0.001626220703" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.79504309082" Y="-4.744445800781" />
                  <Point X="0.471539031982" Y="-3.537112304688" />
                  <Point X="0.464318817139" Y="-3.521544189453" />
                  <Point X="0.332094451904" Y="-3.331034179688" />
                  <Point X="0.300591033936" Y="-3.285643798828" />
                  <Point X="0.274335601807" Y="-3.266399902344" />
                  <Point X="0.069726249695" Y="-3.202896484375" />
                  <Point X="0.020976472855" Y="-3.187766601562" />
                  <Point X="-0.008664756775" Y="-3.187766601562" />
                  <Point X="-0.213274261475" Y="-3.251269775391" />
                  <Point X="-0.262023895264" Y="-3.266399902344" />
                  <Point X="-0.288279144287" Y="-3.285643798828" />
                  <Point X="-0.420503356934" Y="-3.476153808594" />
                  <Point X="-0.45200680542" Y="-3.521544189453" />
                  <Point X="-0.459227142334" Y="-3.537112304688" />
                  <Point X="-0.536304321289" Y="-3.824768310547" />
                  <Point X="-0.84774395752" Y="-4.987077148438" />
                  <Point X="-1.044045898438" Y="-4.948974121094" />
                  <Point X="-1.100245849609" Y="-4.938065429688" />
                  <Point X="-1.337588012695" Y="-4.876999511719" />
                  <Point X="-1.35158972168" Y="-4.873396972656" />
                  <Point X="-1.348092529297" Y="-4.846833984375" />
                  <Point X="-1.309150146484" Y="-4.551038574219" />
                  <Point X="-1.309683227539" Y="-4.5347578125" />
                  <Point X="-1.358564453125" Y="-4.289016113281" />
                  <Point X="-1.370210693359" Y="-4.230466308594" />
                  <Point X="-1.386282714844" Y="-4.20262890625" />
                  <Point X="-1.574661132812" Y="-4.037424804688" />
                  <Point X="-1.619543457031" Y="-3.998063964844" />
                  <Point X="-1.649240722656" Y="-3.985762939453" />
                  <Point X="-1.899260742188" Y="-3.969375732422" />
                  <Point X="-1.958829833984" Y="-3.965471435547" />
                  <Point X="-1.989878540039" Y="-3.973791015625" />
                  <Point X="-2.198208740234" Y="-4.112993164062" />
                  <Point X="-2.247844970703" Y="-4.146159179688" />
                  <Point X="-2.259734130859" Y="-4.157294433594" />
                  <Point X="-2.303006835938" Y="-4.213688476562" />
                  <Point X="-2.457095214844" Y="-4.414500488281" />
                  <Point X="-2.773459716797" Y="-4.218615234375" />
                  <Point X="-2.855831054688" Y="-4.16761328125" />
                  <Point X="-3.184482421875" Y="-3.9145625" />
                  <Point X="-3.228581054688" Y="-3.880608154297" />
                  <Point X="-3.107229492188" Y="-3.670421142578" />
                  <Point X="-2.506033691406" Y="-2.629119628906" />
                  <Point X="-2.499762695312" Y="-2.597593261719" />
                  <Point X="-2.513979736328" Y="-2.568764404297" />
                  <Point X="-2.531328369141" Y="-2.551415771484" />
                  <Point X="-2.560157226562" Y="-2.537198730469" />
                  <Point X="-2.59168359375" Y="-2.543469726562" />
                  <Point X="-2.83978125" Y="-2.686708984375" />
                  <Point X="-3.842959472656" Y="-3.265894042969" />
                  <Point X="-4.096623046875" Y="-2.932631835938" />
                  <Point X="-4.161703613281" Y="-2.847129394531" />
                  <Point X="-4.397319335938" Y="-2.452038085938" />
                  <Point X="-4.431020019531" Y="-2.395527099609" />
                  <Point X="-4.215684570312" Y="-2.230294189453" />
                  <Point X="-3.163786865234" Y="-1.423144897461" />
                  <Point X="-3.145821777344" Y="-1.396013305664" />
                  <Point X="-3.139599609375" Y="-1.371989746094" />
                  <Point X="-3.1381171875" Y="-1.366265869141" />
                  <Point X="-3.140326171875" Y="-1.334595581055" />
                  <Point X="-3.161158691406" Y="-1.310639282227" />
                  <Point X="-3.182545654297" Y="-1.298051757812" />
                  <Point X="-3.187641357422" Y="-1.295052734375" />
                  <Point X="-3.219529296875" Y="-1.288571044922" />
                  <Point X="-3.532729003906" Y="-1.32980456543" />
                  <Point X="-4.803284179688" Y="-1.497076293945" />
                  <Point X="-4.901939453125" Y="-1.110841918945" />
                  <Point X="-4.927392578125" Y="-1.011195251465" />
                  <Point X="-4.989730957031" Y="-0.575329772949" />
                  <Point X="-4.998396484375" Y="-0.514742126465" />
                  <Point X="-4.755733886719" Y="-0.449720794678" />
                  <Point X="-3.557463867188" Y="-0.128645431519" />
                  <Point X="-3.541895996094" Y="-0.121425231934" />
                  <Point X="-3.519483154297" Y="-0.105869361877" />
                  <Point X="-3.514143066406" Y="-0.102163047791" />
                  <Point X="-3.494898925781" Y="-0.075907928467" />
                  <Point X="-3.487427978516" Y="-0.05183613205" />
                  <Point X="-3.485647949219" Y="-0.046100891113" />
                  <Point X="-3.485647949219" Y="-0.01645920372" />
                  <Point X="-3.493118896484" Y="0.007612592697" />
                  <Point X="-3.494898925781" Y="0.013347833633" />
                  <Point X="-3.514143066406" Y="0.039602954865" />
                  <Point X="-3.536555908203" Y="0.055158824921" />
                  <Point X="-3.557463867188" Y="0.066085334778" />
                  <Point X="-3.842960449219" Y="0.142583862305" />
                  <Point X="-4.998186523438" Y="0.452125793457" />
                  <Point X="-4.934048828125" Y="0.885563049316" />
                  <Point X="-4.91764453125" Y="0.996421936035" />
                  <Point X="-4.792161621094" Y="1.459492797852" />
                  <Point X="-4.773516601562" Y="1.528298706055" />
                  <Point X="-4.612215332031" Y="1.507062988281" />
                  <Point X="-3.753266357422" Y="1.393980102539" />
                  <Point X="-3.731704589844" Y="1.395866699219" />
                  <Point X="-3.68209765625" Y="1.4115078125" />
                  <Point X="-3.670278564453" Y="1.41523425293" />
                  <Point X="-3.651534423828" Y="1.426056274414" />
                  <Point X="-3.639119873047" Y="1.443786132813" />
                  <Point X="-3.61921484375" Y="1.491840942383" />
                  <Point X="-3.614472412109" Y="1.503290283203" />
                  <Point X="-3.610714111328" Y="1.524604980469" />
                  <Point X="-3.616315673828" Y="1.545511108398" />
                  <Point X="-3.640333251953" Y="1.59164831543" />
                  <Point X="-3.646055419922" Y="1.602640869141" />
                  <Point X="-3.659968261719" Y="1.619221801758" />
                  <Point X="-3.823729736328" Y="1.744880371094" />
                  <Point X="-4.47610546875" Y="2.245466308594" />
                  <Point X="-4.22375390625" Y="2.677805664062" />
                  <Point X="-4.160014648438" Y="2.787006591797" />
                  <Point X="-3.827611083984" Y="3.214264404297" />
                  <Point X="-3.774671386719" Y="3.282310791016" />
                  <Point X="-3.686699707031" Y="3.231520263672" />
                  <Point X="-3.159156982422" Y="2.926943359375" />
                  <Point X="-3.138514892578" Y="2.920434814453" />
                  <Point X="-3.069426269531" Y="2.914390380859" />
                  <Point X="-3.052965576172" Y="2.912950195312" />
                  <Point X="-3.031507080078" Y="2.915775146484" />
                  <Point X="-3.013252685547" Y="2.927404296875" />
                  <Point X="-2.964213134766" Y="2.976443847656" />
                  <Point X="-2.952529052734" Y="2.988127685547" />
                  <Point X="-2.940899414062" Y="3.006382324219" />
                  <Point X="-2.93807421875" Y="3.027841308594" />
                  <Point X="-2.944118652344" Y="3.096929931641" />
                  <Point X="-2.945558837891" Y="3.113390869141" />
                  <Point X="-2.952067382812" Y="3.134032958984" />
                  <Point X="-3.024635009766" Y="3.259723632812" />
                  <Point X="-3.307278564453" Y="3.749276611328" />
                  <Point X="-2.863888183594" Y="4.089219482422" />
                  <Point X="-2.752876220703" Y="4.174331542969" />
                  <Point X="-2.229350830078" Y="4.465190917969" />
                  <Point X="-2.141548583984" Y="4.513971679688" />
                  <Point X="-2.129323730469" Y="4.498040039062" />
                  <Point X="-1.967826904297" Y="4.287573242188" />
                  <Point X="-1.951247192383" Y="4.273661132813" />
                  <Point X="-1.874352050781" Y="4.233631835938" />
                  <Point X="-1.85603125" Y="4.224094238281" />
                  <Point X="-1.835124389648" Y="4.2184921875" />
                  <Point X="-1.81380859375" Y="4.222250488281" />
                  <Point X="-1.733717163086" Y="4.25542578125" />
                  <Point X="-1.714634765625" Y="4.263330078125" />
                  <Point X="-1.696905395508" Y="4.275744140625" />
                  <Point X="-1.686083496094" Y="4.29448828125" />
                  <Point X="-1.660015258789" Y="4.377166503906" />
                  <Point X="-1.653804199219" Y="4.396864746094" />
                  <Point X="-1.651917602539" Y="4.418426757812" />
                  <Point X="-1.660167724609" Y="4.481092773438" />
                  <Point X="-1.689137573242" Y="4.701141113281" />
                  <Point X="-1.111806396484" Y="4.863004394531" />
                  <Point X="-0.968093994141" Y="4.903296386719" />
                  <Point X="-0.333421508789" Y="4.977575683594" />
                  <Point X="-0.224199661255" Y="4.990358398438" />
                  <Point X="-0.19820211792" Y="4.893333984375" />
                  <Point X="-0.042140380859" Y="4.310903320312" />
                  <Point X="-0.024282119751" Y="4.284176757813" />
                  <Point X="0.006155934334" Y="4.273844238281" />
                  <Point X="0.036594032288" Y="4.284176757813" />
                  <Point X="0.054452251434" Y="4.310903320312" />
                  <Point X="0.091635101318" Y="4.449671875" />
                  <Point X="0.236648406982" Y="4.990868652344" />
                  <Point X="0.734721679688" Y="4.93870703125" />
                  <Point X="0.860205749512" Y="4.925565429688" />
                  <Point X="1.385286376953" Y="4.798794921875" />
                  <Point X="1.508455932617" Y="4.769058105469" />
                  <Point X="1.850921508789" Y="4.644843261719" />
                  <Point X="1.931033569336" Y="4.615786132813" />
                  <Point X="2.261514648438" Y="4.461230957031" />
                  <Point X="2.33869921875" Y="4.425134277344" />
                  <Point X="2.657954345703" Y="4.239135742188" />
                  <Point X="2.732523681641" Y="4.19569140625" />
                  <Point X="3.033625732422" Y="3.981564208984" />
                  <Point X="3.068740234375" Y="3.956592773438" />
                  <Point X="2.925692626953" Y="3.708826660156" />
                  <Point X="2.229853515625" Y="2.503597900391" />
                  <Point X="2.224852050781" Y="2.491514892578" />
                  <Point X="2.207513427734" Y="2.426676757812" />
                  <Point X="2.202044677734" Y="2.392326416016" />
                  <Point X="2.208805419922" Y="2.336260009766" />
                  <Point X="2.210416015625" Y="2.322901611328" />
                  <Point X="2.218682128906" Y="2.3008125" />
                  <Point X="2.253374267578" Y="2.249685302734" />
                  <Point X="2.274940429688" Y="2.224203369141" />
                  <Point X="2.326067626953" Y="2.189511230469" />
                  <Point X="2.338249023438" Y="2.181245849609" />
                  <Point X="2.360336669922" Y="2.172980224609" />
                  <Point X="2.416403320312" Y="2.166219482422" />
                  <Point X="2.448664550781" Y="2.165946289062" />
                  <Point X="2.513502685547" Y="2.183284912109" />
                  <Point X="2.541033935547" Y="2.192417480469" />
                  <Point X="2.828188476562" Y="2.358206054688" />
                  <Point X="3.994248046875" Y="3.031430908203" />
                  <Point X="4.158359863281" Y="2.803352783203" />
                  <Point X="4.202591796875" Y="2.741880126953" />
                  <Point X="4.370451660156" Y="2.464490478516" />
                  <Point X="4.387513183594" Y="2.436296142578" />
                  <Point X="4.204753417969" Y="2.296059570312" />
                  <Point X="3.288616210938" Y="1.593082763672" />
                  <Point X="3.279371337891" Y="1.583833374023" />
                  <Point X="3.232707275391" Y="1.522956420898" />
                  <Point X="3.213119384766" Y="1.491499633789" />
                  <Point X="3.195736816406" Y="1.429344116211" />
                  <Point X="3.191595458984" Y="1.41453515625" />
                  <Point X="3.190779541016" Y="1.390965576172" />
                  <Point X="3.205048828125" Y="1.321809448242" />
                  <Point X="3.215646728516" Y="1.287954589844" />
                  <Point X="3.254457275391" Y="1.228964233398" />
                  <Point X="3.263704345703" Y="1.214909301758" />
                  <Point X="3.280948242188" Y="1.198819702148" />
                  <Point X="3.337190185547" Y="1.16716027832" />
                  <Point X="3.368565429688" Y="1.153619628906" />
                  <Point X="3.444608398438" Y="1.143569458008" />
                  <Point X="3.475803710938" Y="1.14117175293" />
                  <Point X="3.748581298828" Y="1.177083618164" />
                  <Point X="4.848975097656" Y="1.32195324707" />
                  <Point X="4.920190917969" Y="1.029420166016" />
                  <Point X="4.939188476562" Y="0.951385192871" />
                  <Point X="4.9920859375" Y="0.611634033203" />
                  <Point X="4.997858398438" Y="0.574556213379" />
                  <Point X="4.791080566406" Y="0.51915020752" />
                  <Point X="3.741167724609" Y="0.237826919556" />
                  <Point X="3.729087158203" Y="0.232819412231" />
                  <Point X="3.654377441406" Y="0.189635864258" />
                  <Point X="3.636577148438" Y="0.17934703064" />
                  <Point X="3.622264648438" Y="0.166926269531" />
                  <Point X="3.577438720703" Y="0.109807670593" />
                  <Point X="3.566758789062" Y="0.096198738098" />
                  <Point X="3.556985107422" Y="0.074735275269" />
                  <Point X="3.54204296875" Y="-0.003286236048" />
                  <Point X="3.538482910156" Y="-0.04068478775" />
                  <Point X="3.553425048828" Y="-0.11870615387" />
                  <Point X="3.556985107422" Y="-0.137295227051" />
                  <Point X="3.566758789062" Y="-0.158758682251" />
                  <Point X="3.611584716797" Y="-0.215877441406" />
                  <Point X="3.636576904297" Y="-0.241906967163" />
                  <Point X="3.711286865234" Y="-0.285090515137" />
                  <Point X="3.729086914062" Y="-0.295379333496" />
                  <Point X="3.741167724609" Y="-0.300387023926" />
                  <Point X="3.991317138672" Y="-0.367414337158" />
                  <Point X="4.998067871094" Y="-0.637172363281" />
                  <Point X="4.9590390625" Y="-0.896043273926" />
                  <Point X="4.948430664062" Y="-0.966406799316" />
                  <Point X="4.880662109375" Y="-1.263378417969" />
                  <Point X="4.874546386719" Y="-1.290178466797" />
                  <Point X="4.6295859375" Y="-1.257928833008" />
                  <Point X="3.411982177734" Y="-1.097628295898" />
                  <Point X="3.394836181641" Y="-1.098341186523" />
                  <Point X="3.248207275391" Y="-1.130211669922" />
                  <Point X="3.213271972656" Y="-1.137804931641" />
                  <Point X="3.1854453125" Y="-1.154697265625" />
                  <Point X="3.096817382812" Y="-1.2612890625" />
                  <Point X="3.075701171875" Y="-1.286685424805" />
                  <Point X="3.064357910156" Y="-1.314070678711" />
                  <Point X="3.051655273438" Y="-1.452111694336" />
                  <Point X="3.04862890625" Y="-1.485000854492" />
                  <Point X="3.056360595703" Y="-1.516621826172" />
                  <Point X="3.137507080078" Y="-1.64283996582" />
                  <Point X="3.156840820312" Y="-1.672912353516" />
                  <Point X="3.168460693359" Y="-1.685540649414" />
                  <Point X="3.400600830078" Y="-1.863668212891" />
                  <Point X="4.33907421875" Y="-2.583783935547" />
                  <Point X="4.234031738281" Y="-2.753758056641" />
                  <Point X="4.204129394531" Y="-2.802145019531" />
                  <Point X="4.063979003906" Y="-3.001279052734" />
                  <Point X="4.056688232422" Y="-3.011638183594" />
                  <Point X="3.837895019531" Y="-2.885317871094" />
                  <Point X="2.753454589844" Y="-2.259215576172" />
                  <Point X="2.737340820313" Y="-2.253312744141" />
                  <Point X="2.562829101562" Y="-2.221796142578" />
                  <Point X="2.521250488281" Y="-2.214287109375" />
                  <Point X="2.489077880859" Y="-2.219244873047" />
                  <Point X="2.3441015625" Y="-2.295544921875" />
                  <Point X="2.309559814453" Y="-2.313723876953" />
                  <Point X="2.288599853516" Y="-2.334683837891" />
                  <Point X="2.212299804688" Y="-2.47966015625" />
                  <Point X="2.194120849609" Y="-2.514201904297" />
                  <Point X="2.189163085938" Y="-2.546374511719" />
                  <Point X="2.2206796875" Y="-2.720886230469" />
                  <Point X="2.228188720703" Y="-2.76246484375" />
                  <Point X="2.234091552734" Y="-2.778578613281" />
                  <Point X="2.383264892578" Y="-3.036954589844" />
                  <Point X="2.986673583984" Y="-4.082088623047" />
                  <Point X="2.870779785156" Y="-4.164868652344" />
                  <Point X="2.835293701172" Y="-4.190215332031" />
                  <Point X="2.679776123047" Y="-4.290879394531" />
                  <Point X="2.510705810547" Y="-4.07054296875" />
                  <Point X="1.683177612305" Y="-2.992087402344" />
                  <Point X="1.670549316406" Y="-2.980467529297" />
                  <Point X="1.49843359375" Y="-2.869812988281" />
                  <Point X="1.45742590332" Y="-2.843448730469" />
                  <Point X="1.42580456543" Y="-2.835717041016" />
                  <Point X="1.237566772461" Y="-2.853038818359" />
                  <Point X="1.192717773438" Y="-2.857166015625" />
                  <Point X="1.165332885742" Y="-2.868509277344" />
                  <Point X="1.01998046875" Y="-2.989364990234" />
                  <Point X="0.985349243164" Y="-3.018159667969" />
                  <Point X="0.968456726074" Y="-3.045986328125" />
                  <Point X="0.924997070312" Y="-3.245934814453" />
                  <Point X="0.91464251709" Y="-3.29357421875" />
                  <Point X="0.91392956543" Y="-3.310720214844" />
                  <Point X="0.956204040527" Y="-3.631827636719" />
                  <Point X="1.127642211914" Y="-4.934028320312" />
                  <Point X="1.027910766602" Y="-4.955889648438" />
                  <Point X="0.994347412109" Y="-4.963246582031" />
                  <Point X="0.860200378418" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#198" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.156061723514" Y="4.937554784567" Z="2.05" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="2.05" />
                  <Point X="-0.345389572826" Y="5.058516680955" Z="2.05" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="2.05" />
                  <Point X="-1.131460266896" Y="4.942428104167" Z="2.05" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="2.05" />
                  <Point X="-1.71589601634" Y="4.50584653185" Z="2.05" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="2.05" />
                  <Point X="-1.713856853068" Y="4.423481960811" Z="2.05" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="2.05" />
                  <Point X="-1.759007531452" Y="4.332899850776" Z="2.05" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="2.05" />
                  <Point X="-1.857419911193" Y="4.309261883632" Z="2.05" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="2.05" />
                  <Point X="-2.095811966217" Y="4.559758099315" Z="2.05" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="2.05" />
                  <Point X="-2.259789677052" Y="4.540178315111" Z="2.05" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="2.05" />
                  <Point X="-2.900466297684" Y="4.160179403398" Z="2.05" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="2.05" />
                  <Point X="-3.074092346887" Y="3.266003777046" Z="2.05" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="2.05" />
                  <Point X="-3.000084535351" Y="3.123852070152" Z="2.05" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="2.05" />
                  <Point X="-3.005724034164" Y="3.043079533723" Z="2.05" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="2.05" />
                  <Point X="-3.071224395455" Y="2.995480163822" Z="2.05" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="2.05" />
                  <Point X="-3.667855571251" Y="3.306101601859" Z="2.05" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="2.05" />
                  <Point X="-3.873230399871" Y="3.276246752366" Z="2.05" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="2.05" />
                  <Point X="-4.273091904677" Y="2.734290693657" Z="2.05" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="2.05" />
                  <Point X="-3.860323746639" Y="1.736492692725" Z="2.05" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="2.05" />
                  <Point X="-3.690839944027" Y="1.599841595554" Z="2.05" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="2.05" />
                  <Point X="-3.671564937153" Y="1.542254963542" Z="2.05" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="2.05" />
                  <Point X="-3.703289122963" Y="1.490473529865" Z="2.05" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="2.05" />
                  <Point X="-4.611845064803" Y="1.587915349362" Z="2.05" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="2.05" />
                  <Point X="-4.8465768165" Y="1.503850332546" Z="2.05" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="2.05" />
                  <Point X="-4.98966622695" Y="0.924162146069" Z="2.05" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="2.05" />
                  <Point X="-3.862057218618" Y="0.125567585289" Z="2.05" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="2.05" />
                  <Point X="-3.571220711147" Y="0.045362750081" Z="2.05" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="2.05" />
                  <Point X="-3.547027820652" Y="0.024071695047" Z="2.05" />
                  <Point X="-3.539556741714" Y="0" Z="2.05" />
                  <Point X="-3.541336780483" Y="-0.005735256013" Z="2.05" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="2.05" />
                  <Point X="-3.554147883976" Y="-0.033513233053" Z="2.05" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="2.05" />
                  <Point X="-4.774830570155" Y="-0.370144471467" Z="2.05" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="2.05" />
                  <Point X="-5.045383205101" Y="-0.551128833465" Z="2.05" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="2.05" />
                  <Point X="-4.956539992345" Y="-1.091936418944" Z="2.05" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="2.05" />
                  <Point X="-3.532358686375" Y="-1.348096835761" Z="2.05" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="2.05" />
                  <Point X="-3.214063284898" Y="-1.309862358117" Z="2.05" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="2.05" />
                  <Point X="-3.194158753913" Y="-1.328173594577" Z="2.05" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="2.05" />
                  <Point X="-4.252278371728" Y="-2.159346529909" Z="2.05" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="2.05" />
                  <Point X="-4.446418477542" Y="-2.446367620021" Z="2.05" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="2.05" />
                  <Point X="-4.142565118095" Y="-2.931635083954" Z="2.05" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="2.05" />
                  <Point X="-2.820937184329" Y="-2.698730234309" Z="2.05" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="2.05" />
                  <Point X="-2.569501409736" Y="-2.558829059395" Z="2.05" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="2.05" />
                  <Point X="-3.156686778859" Y="-3.614141198163" Z="2.05" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="2.05" />
                  <Point X="-3.22114228452" Y="-3.922899753615" Z="2.05" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="2.05" />
                  <Point X="-2.805937203153" Y="-4.229846176627" Z="2.05" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="2.05" />
                  <Point X="-2.269495174205" Y="-4.212846497244" Z="2.05" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="2.05" />
                  <Point X="-2.176586114454" Y="-4.123286306883" Z="2.05" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="2.05" />
                  <Point X="-1.908686904358" Y="-3.987988736587" Z="2.05" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="2.05" />
                  <Point X="-1.61378433819" Y="-4.043736893921" Z="2.05" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="2.05" />
                  <Point X="-1.413759689477" Y="-4.26749030341" Z="2.05" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="2.05" />
                  <Point X="-1.403820778501" Y="-4.809027807669" Z="2.05" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="2.05" />
                  <Point X="-1.356202952323" Y="-4.894142121602" Z="2.05" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="2.05" />
                  <Point X="-1.059729054718" Y="-4.966778062887" Z="2.05" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="2.05" />
                  <Point X="-0.494163979497" Y="-3.806429123842" Z="2.05" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="2.05" />
                  <Point X="-0.385583380809" Y="-3.473382686698" Z="2.05" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="2.05" />
                  <Point X="-0.204609404142" Y="-3.267742576692" Z="2.05" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="2.05" />
                  <Point X="0.048749675218" Y="-3.219369468596" Z="2.05" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="2.05" />
                  <Point X="0.284862478624" Y="-3.32826306907" Z="2.05" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="2.05" />
                  <Point X="0.740590880904" Y="-4.726106905974" Z="2.05" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="2.05" />
                  <Point X="0.852368308019" Y="-5.007459185956" Z="2.05" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="2.05" />
                  <Point X="1.032460390834" Y="-4.973449906005" Z="2.05" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="2.05" />
                  <Point X="0.999620385556" Y="-3.594021136898" Z="2.05" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="2.05" />
                  <Point X="0.96770036171" Y="-3.225274513428" Z="2.05" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="2.05" />
                  <Point X="1.04579129374" Y="-2.996531241797" Z="2.05" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="2.05" />
                  <Point X="1.235992750459" Y="-2.871548394122" Z="2.05" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="2.05" />
                  <Point X="1.465238256601" Y="-2.880590815793" Z="2.05" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="2.05" />
                  <Point X="2.464882077291" Y="-4.069700770901" Z="2.05" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="2.05" />
                  <Point X="2.699610938868" Y="-4.302335992734" Z="2.05" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="2.05" />
                  <Point X="2.893685697903" Y="-4.174275675276" Z="2.05" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="2.05" />
                  <Point X="2.420410310898" Y="-2.980674446849" Z="2.05" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="2.05" />
                  <Point X="2.263727813843" Y="-2.680719931401" Z="2.05" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="2.05" />
                  <Point X="2.250390152276" Y="-2.471666520364" Z="2.05" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="2.05" />
                  <Point X="2.361231858008" Y="-2.308511157691" Z="2.05" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="2.05" />
                  <Point X="2.547786872111" Y="-2.239720195648" Z="2.05" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="2.05" />
                  <Point X="3.806739150258" Y="-2.897339189377" Z="2.05" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="2.05" />
                  <Point X="4.098711750201" Y="-2.998776213844" Z="2.05" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="2.05" />
                  <Point X="4.270409547741" Y="-2.748763045649" Z="2.05" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="2.05" />
                  <Point X="3.424882964217" Y="-1.792720447032" Z="2.05" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="2.05" />
                  <Point X="3.173409067564" Y="-1.584520640644" Z="2.05" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="2.05" />
                  <Point X="3.095288966916" Y="-1.425413228641" Z="2.05" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="2.05" />
                  <Point X="3.129107684116" Y="-1.261975965772" Z="2.05" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="2.05" />
                  <Point X="3.252670934429" Y="-1.147790828024" Z="2.05" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="2.05" />
                  <Point X="4.616903773121" Y="-1.276221008595" Z="2.05" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="2.05" />
                  <Point X="4.923252518858" Y="-1.243222568524" Z="2.05" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="2.05" />
                  <Point X="5.00232450809" Y="-0.872217453006" Z="2.05" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="2.05" />
                  <Point X="3.998102095533" Y="-0.287837962411" Z="2.05" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="2.05" />
                  <Point X="3.730152541352" Y="-0.210521826315" Z="2.05" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="2.05" />
                  <Point X="3.644762619705" Y="-0.153729256581" Z="2.05" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="2.05" />
                  <Point X="3.596376692047" Y="-0.078021372973" Z="2.05" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="2.05" />
                  <Point X="3.584994758377" Y="0.018589158214" Z="2.05" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="2.05" />
                  <Point X="3.610616818695" Y="0.110219481994" Z="2.05" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="2.05" />
                  <Point X="3.673242873001" Y="0.177627017927" Z="2.05" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="2.05" />
                  <Point X="4.797865721773" Y="0.502133970358" Z="2.05" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="2.05" />
                  <Point X="5.035334646635" Y="0.65060589122" Z="2.05" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="2.05" />
                  <Point X="4.962616133929" Y="1.072527370675" Z="2.05" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="2.05" />
                  <Point X="3.735898937029" Y="1.25793593636" Z="2.05" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="2.05" />
                  <Point X="3.445003617907" Y="1.224418570627" Z="2.05" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="2.05" />
                  <Point X="3.355560553918" Y="1.242011655283" Z="2.05" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="2.05" />
                  <Point X="3.290071635506" Y="1.287725937644" Z="2.05" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="2.05" />
                  <Point X="3.247861251184" Y="1.363193300112" Z="2.05" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="2.05" />
                  <Point X="3.237733529699" Y="1.447158203649" Z="2.05" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="2.05" />
                  <Point X="3.266234048747" Y="1.523818091726" Z="2.05" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="2.05" />
                  <Point X="4.22903550732" Y="2.287671988631" Z="2.05" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="2.05" />
                  <Point X="4.407072872855" Y="2.521656713079" Z="2.05" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="2.05" />
                  <Point X="4.192789270756" Y="2.863835735763" Z="2.05" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="2.05" />
                  <Point X="2.797032414539" Y="2.43278748303" Z="2.05" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="2.05" />
                  <Point X="2.494429769152" Y="2.262867731022" Z="2.05" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="2.05" />
                  <Point X="2.416233382731" Y="2.247139935107" Z="2.05" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="2.05" />
                  <Point X="2.347985344505" Y="2.262166133984" Z="2.05" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="2.05" />
                  <Point X="2.288592466251" Y="2.309039515877" Z="2.05" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="2.05" />
                  <Point X="2.252289767652" Y="2.373525061101" Z="2.05" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="2.05" />
                  <Point X="2.249660211635" Y="2.44503980673" Z="2.05" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="2.05" />
                  <Point X="2.96283786983" Y="3.715106667416" Z="2.05" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="2.05" />
                  <Point X="3.056446797409" Y="4.053591439075" Z="2.05" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="2.05" />
                  <Point X="2.676968282912" Y="4.313618298063" Z="2.05" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="2.05" />
                  <Point X="2.276540076682" Y="4.537803183401" Z="2.05" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="2.05" />
                  <Point X="1.861814224208" Y="4.723127164625" Z="2.05" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="2.05" />
                  <Point X="1.390864807794" Y="4.878678580506" Z="2.05" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="2.05" />
                  <Point X="0.733773678584" Y="5.019715197532" Z="2.05" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="2.05" />
                  <Point X="0.037182838039" Y="4.493892721383" Z="2.05" />
                  <Point X="0" Y="4.355124473572" Z="2.05" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>