<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#203" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="3151" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="0" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="0.004715820312" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.902576904297" Y="-4.778715820312" />
                  <Point X="0.563302001953" Y="-3.512524658203" />
                  <Point X="0.557721130371" Y="-3.497141845703" />
                  <Point X="0.542363098145" Y="-3.467377197266" />
                  <Point X="0.403692840576" Y="-3.267579589844" />
                  <Point X="0.378635437012" Y="-3.231476806641" />
                  <Point X="0.356751373291" Y="-3.209021240234" />
                  <Point X="0.330496032715" Y="-3.18977734375" />
                  <Point X="0.302495117188" Y="-3.175669189453" />
                  <Point X="0.087910987854" Y="-3.109070068359" />
                  <Point X="0.049136127472" Y="-3.097035888672" />
                  <Point X="0.020976577759" Y="-3.092766601562" />
                  <Point X="-0.008664898872" Y="-3.092766601562" />
                  <Point X="-0.036824295044" Y="-3.097035888672" />
                  <Point X="-0.251408569336" Y="-3.163634765625" />
                  <Point X="-0.290183410645" Y="-3.175669189453" />
                  <Point X="-0.318184967041" Y="-3.189777832031" />
                  <Point X="-0.344440002441" Y="-3.209021728516" />
                  <Point X="-0.366323608398" Y="-3.231476806641" />
                  <Point X="-0.504993865967" Y="-3.431274169922" />
                  <Point X="-0.530051269531" Y="-3.467377197266" />
                  <Point X="-0.538188903809" Y="-3.481573730469" />
                  <Point X="-0.55099017334" Y="-3.512524414063" />
                  <Point X="-0.612296325684" Y="-3.741322509766" />
                  <Point X="-0.916584594727" Y="-4.876941894531" />
                  <Point X="-1.037744262695" Y="-4.853424316406" />
                  <Point X="-1.079341674805" Y="-4.845350097656" />
                  <Point X="-1.24641796875" Y="-4.802362792969" />
                  <Point X="-1.214963012695" Y="-4.563438476562" />
                  <Point X="-1.214201171875" Y="-4.5479296875" />
                  <Point X="-1.216508666992" Y="-4.516224121094" />
                  <Point X="-1.267772705078" Y="-4.258502441406" />
                  <Point X="-1.277036132812" Y="-4.211932617188" />
                  <Point X="-1.287938354492" Y="-4.182966308594" />
                  <Point X="-1.304010375977" Y="-4.15512890625" />
                  <Point X="-1.323644775391" Y="-4.131204101562" />
                  <Point X="-1.521206665039" Y="-3.957946533203" />
                  <Point X="-1.556905395508" Y="-3.926639404297" />
                  <Point X="-1.583188354492" Y="-3.910295410156" />
                  <Point X="-1.612885742188" Y="-3.897994384766" />
                  <Point X="-1.64302746582" Y="-3.890966308594" />
                  <Point X="-1.905236083984" Y="-3.873780273438" />
                  <Point X="-1.952616699219" Y="-3.870674804688" />
                  <Point X="-1.98341796875" Y="-3.873708496094" />
                  <Point X="-2.014466674805" Y="-3.882028076172" />
                  <Point X="-2.042657836914" Y="-3.894801513672" />
                  <Point X="-2.261144042969" Y="-4.040789794922" />
                  <Point X="-2.300624267578" Y="-4.067169677734" />
                  <Point X="-2.312785644531" Y="-4.076821533203" />
                  <Point X="-2.335102539062" Y="-4.099461914062" />
                  <Point X="-2.369521240234" Y="-4.144316894531" />
                  <Point X="-2.480148925781" Y="-4.288489746094" />
                  <Point X="-2.74074609375" Y="-4.127134765625" />
                  <Point X="-2.801706542969" Y="-4.089389404297" />
                  <Point X="-3.104721923828" Y="-3.856077880859" />
                  <Point X="-3.054265625" Y="-3.768685058594" />
                  <Point X="-2.423761230469" Y="-2.676619628906" />
                  <Point X="-2.412859130859" Y="-2.647653320312" />
                  <Point X="-2.406588134766" Y="-2.616126953125" />
                  <Point X="-2.405575439453" Y="-2.585192871094" />
                  <Point X="-2.414560058594" Y="-2.555575195312" />
                  <Point X="-2.428777099609" Y="-2.526746337891" />
                  <Point X="-2.4468046875" Y="-2.501589355469" />
                  <Point X="-2.461498291016" Y="-2.486895751953" />
                  <Point X="-2.464153320312" Y="-2.484240722656" />
                  <Point X="-2.489310302734" Y="-2.466213134766" />
                  <Point X="-2.518139160156" Y="-2.45199609375" />
                  <Point X="-2.547756835938" Y="-2.443011474609" />
                  <Point X="-2.578690917969" Y="-2.444024169922" />
                  <Point X="-2.610217285156" Y="-2.450295166016" />
                  <Point X="-2.63918359375" Y="-2.461197265625" />
                  <Point X="-2.836517578125" Y="-2.575128173828" />
                  <Point X="-3.818024169922" Y="-3.141801025391" />
                  <Point X="-4.034695800781" Y="-2.857139404297" />
                  <Point X="-4.082858886719" Y="-2.793862792969" />
                  <Point X="-4.306142578125" Y="-2.419450195312" />
                  <Point X="-4.209132324219" Y="-2.34501171875" />
                  <Point X="-3.105954589844" Y="-1.498513427734" />
                  <Point X="-3.084577392578" Y="-1.47559362793" />
                  <Point X="-3.066612792969" Y="-1.448463012695" />
                  <Point X="-3.053856689453" Y="-1.419833374023" />
                  <Point X="-3.047331298828" Y="-1.394638549805" />
                  <Point X="-3.046152099609" Y="-1.3900859375" />
                  <Point X="-3.04334765625" Y="-1.359659057617" />
                  <Point X="-3.045555908203" Y="-1.32798840332" />
                  <Point X="-3.052556640625" Y="-1.298242919922" />
                  <Point X="-3.068638916016" Y="-1.272258789062" />
                  <Point X="-3.089471191406" Y="-1.248302001953" />
                  <Point X="-3.112972412109" Y="-1.228766967773" />
                  <Point X="-3.135402099609" Y="-1.215566040039" />
                  <Point X="-3.139455078125" Y="-1.213180541992" />
                  <Point X="-3.168717041016" Y="-1.20195703125" />
                  <Point X="-3.20060546875" Y="-1.195474975586" />
                  <Point X="-3.231929199219" Y="-1.194383789062" />
                  <Point X="-3.481044677734" Y="-1.227180541992" />
                  <Point X="-4.732102050781" Y="-1.391885253906" />
                  <Point X="-4.815239257812" Y="-1.066406738281" />
                  <Point X="-4.834076660156" Y="-0.992658630371" />
                  <Point X="-4.892424316406" Y="-0.584698181152" />
                  <Point X="-4.789562011719" Y="-0.557136230469" />
                  <Point X="-3.532875976562" Y="-0.220408340454" />
                  <Point X="-3.517492919922" Y="-0.214827453613" />
                  <Point X="-3.487728271484" Y="-0.199469161987" />
                  <Point X="-3.46422265625" Y="-0.183155029297" />
                  <Point X="-3.459975341797" Y="-0.180207107544" />
                  <Point X="-3.437519287109" Y="-0.158322418213" />
                  <Point X="-3.418275878906" Y="-0.132067230225" />
                  <Point X="-3.404168212891" Y="-0.104066917419" />
                  <Point X="-3.396333007813" Y="-0.078821739197" />
                  <Point X="-3.394917236328" Y="-0.074260139465" />
                  <Point X="-3.390647949219" Y="-0.046100589752" />
                  <Point X="-3.390647949219" Y="-0.016459566116" />
                  <Point X="-3.394917236328" Y="0.011699982643" />
                  <Point X="-3.402752441406" Y="0.036945156097" />
                  <Point X="-3.404168212891" Y="0.041506912231" />
                  <Point X="-3.418275878906" Y="0.069507080078" />
                  <Point X="-3.437519287109" Y="0.095762420654" />
                  <Point X="-3.459975341797" Y="0.117647102356" />
                  <Point X="-3.483480957031" Y="0.133961227417" />
                  <Point X="-3.494275878906" Y="0.140438278198" />
                  <Point X="-3.514091796875" Y="0.150606719971" />
                  <Point X="-3.532876220703" Y="0.157848342896" />
                  <Point X="-3.759956787109" Y="0.218694274902" />
                  <Point X="-4.89181640625" Y="0.521975158691" />
                  <Point X="-4.836627441406" Y="0.894936157227" />
                  <Point X="-4.824487792969" Y="0.976974914551" />
                  <Point X="-4.703551757812" Y="1.423267944336" />
                  <Point X="-4.666489257812" Y="1.418388549805" />
                  <Point X="-3.765666259766" Y="1.29979284668" />
                  <Point X="-3.744985839844" Y="1.299341552734" />
                  <Point X="-3.723424072266" Y="1.301228149414" />
                  <Point X="-3.703137695312" Y="1.305263549805" />
                  <Point X="-3.651112548828" Y="1.321666992188" />
                  <Point X="-3.641711669922" Y="1.324631103516" />
                  <Point X="-3.622778320312" Y="1.332961914062" />
                  <Point X="-3.604033935547" Y="1.343784057617" />
                  <Point X="-3.587352783203" Y="1.356015380859" />
                  <Point X="-3.573714355469" Y="1.371567260742" />
                  <Point X="-3.561299804688" Y="1.389297363281" />
                  <Point X="-3.551351074219" Y="1.407431396484" />
                  <Point X="-3.530475585938" Y="1.457828979492" />
                  <Point X="-3.526703613281" Y="1.466935668945" />
                  <Point X="-3.520915283203" Y="1.486795654297" />
                  <Point X="-3.517157226562" Y="1.508110595703" />
                  <Point X="-3.5158046875" Y="1.528750732422" />
                  <Point X="-3.518951416016" Y="1.549194335938" />
                  <Point X="-3.524553466797" Y="1.570100830078" />
                  <Point X="-3.532050048828" Y="1.589378051758" />
                  <Point X="-3.55723828125" Y="1.637764404297" />
                  <Point X="-3.561789794922" Y="1.64650769043" />
                  <Point X="-3.573281738281" Y="1.663706542969" />
                  <Point X="-3.587194091797" Y="1.680286621094" />
                  <Point X="-3.602135986328" Y="1.694590332031" />
                  <Point X="-3.732389892578" Y="1.794537719727" />
                  <Point X="-4.351860351563" Y="2.269874267578" />
                  <Point X="-4.128322265625" Y="2.652847900391" />
                  <Point X="-4.081155029297" Y="2.733657470703" />
                  <Point X="-3.750504882813" Y="3.158661621094" />
                  <Point X="-3.206656982422" Y="2.844670898438" />
                  <Point X="-3.187724609375" Y="2.836340332031" />
                  <Point X="-3.167082519531" Y="2.829831787109" />
                  <Point X="-3.146794677734" Y="2.825796386719" />
                  <Point X="-3.074338134766" Y="2.819457275391" />
                  <Point X="-3.061245361328" Y="2.818311767578" />
                  <Point X="-3.040566162109" Y="2.818762939453" />
                  <Point X="-3.019107666016" Y="2.821587890625" />
                  <Point X="-2.999015625" Y="2.826504150391" />
                  <Point X="-2.980464111328" Y="2.835652587891" />
                  <Point X="-2.962209716797" Y="2.847281738281" />
                  <Point X="-2.946077636719" Y="2.860229248047" />
                  <Point X="-2.894647460938" Y="2.911659423828" />
                  <Point X="-2.885354003906" Y="2.920952636719" />
                  <Point X="-2.872407470703" Y="2.937083251953" />
                  <Point X="-2.860777832031" Y="2.955337646484" />
                  <Point X="-2.85162890625" Y="2.973889648438" />
                  <Point X="-2.846712158203" Y="2.993981933594" />
                  <Point X="-2.843886962891" Y="3.015441162109" />
                  <Point X="-2.843435791016" Y="3.03612109375" />
                  <Point X="-2.849774902344" Y="3.108577880859" />
                  <Point X="-2.850920410156" Y="3.121670654297" />
                  <Point X="-2.854955810547" Y="3.141958496094" />
                  <Point X="-2.861464355469" Y="3.162600585938" />
                  <Point X="-2.869794921875" Y="3.181532958984" />
                  <Point X="-2.927514404297" Y="3.281505859375" />
                  <Point X="-3.183332763672" Y="3.724596435547" />
                  <Point X="-2.7827734375" Y="4.031701416016" />
                  <Point X="-2.700624755859" Y="4.094684082031" />
                  <Point X="-2.167037109375" Y="4.391134277344" />
                  <Point X="-2.04319519043" Y="4.229740722656" />
                  <Point X="-2.028892944336" Y="4.214800292969" />
                  <Point X="-2.012313232422" Y="4.200887695312" />
                  <Point X="-1.99511340332" Y="4.189395019531" />
                  <Point X="-1.914469604492" Y="4.1474140625" />
                  <Point X="-1.899897216797" Y="4.139828125" />
                  <Point X="-1.880619628906" Y="4.132331054688" />
                  <Point X="-1.859713500977" Y="4.126729003906" />
                  <Point X="-1.839269287109" Y="4.12358203125" />
                  <Point X="-1.818628540039" Y="4.124935058594" />
                  <Point X="-1.797313476562" Y="4.128693359375" />
                  <Point X="-1.777454223633" Y="4.134481445312" />
                  <Point X="-1.693458007812" Y="4.169274414063" />
                  <Point X="-1.678280029297" Y="4.175561035156" />
                  <Point X="-1.660146240234" Y="4.185509765625" />
                  <Point X="-1.642416259766" Y="4.197924316406" />
                  <Point X="-1.626864746094" Y="4.2115625" />
                  <Point X="-1.614633666992" Y="4.228243652344" />
                  <Point X="-1.603811523438" Y="4.246987792969" />
                  <Point X="-1.59548034668" Y="4.265921386719" />
                  <Point X="-1.568141235352" Y="4.352629882812" />
                  <Point X="-1.563200927734" Y="4.368297851562" />
                  <Point X="-1.559165649414" Y="4.388584960938" />
                  <Point X="-1.557279174805" Y="4.410146972656" />
                  <Point X="-1.557730224609" Y="4.430827148438" />
                  <Point X="-1.564292602539" Y="4.480670898438" />
                  <Point X="-1.584201904297" Y="4.631897949219" />
                  <Point X="-1.055981201172" Y="4.779992675781" />
                  <Point X="-0.949634765625" Y="4.80980859375" />
                  <Point X="-0.294710784912" Y="4.886457519531" />
                  <Point X="-0.133903366089" Y="4.286315429688" />
                  <Point X="-0.121129745483" Y="4.258124023438" />
                  <Point X="-0.103271606445" Y="4.231397460938" />
                  <Point X="-0.082114532471" Y="4.20880859375" />
                  <Point X="-0.054819351196" Y="4.19421875" />
                  <Point X="-0.024381277084" Y="4.183886230469" />
                  <Point X="0.006155913353" Y="4.178844238281" />
                  <Point X="0.036693107605" Y="4.183886230469" />
                  <Point X="0.06713117981" Y="4.19421875" />
                  <Point X="0.094426452637" Y="4.20880859375" />
                  <Point X="0.115583435059" Y="4.231397460938" />
                  <Point X="0.133441726685" Y="4.258124023438" />
                  <Point X="0.146215194702" Y="4.286315429688" />
                  <Point X="0.175790039062" Y="4.396690429688" />
                  <Point X="0.307419281006" Y="4.8879375" />
                  <Point X="0.75117779541" Y="4.841463867188" />
                  <Point X="0.84404107666" Y="4.831738769531" />
                  <Point X="1.38874597168" Y="4.70023046875" />
                  <Point X="1.481030151367" Y="4.677949707031" />
                  <Point X="1.83535168457" Y="4.549435058594" />
                  <Point X="1.894645751953" Y="4.527928710938" />
                  <Point X="2.237479492188" Y="4.367596679688" />
                  <Point X="2.294571533203" Y="4.340896484375" />
                  <Point X="2.625790283203" Y="4.147927734375" />
                  <Point X="2.680978759766" Y="4.115774902344" />
                  <Point X="2.943260253906" Y="3.929254882812" />
                  <Point X="2.877342529297" Y="3.815081787109" />
                  <Point X="2.147581054688" Y="2.551097900391" />
                  <Point X="2.142076171875" Y="2.539931884766" />
                  <Point X="2.133076904297" Y="2.516057128906" />
                  <Point X="2.114893066406" Y="2.448058105469" />
                  <Point X="2.1126953125" Y="2.437447509766" />
                  <Point X="2.108071533203" Y="2.406258056641" />
                  <Point X="2.107727783203" Y="2.380953613281" />
                  <Point X="2.114818115234" Y="2.322153808594" />
                  <Point X="2.116099121094" Y="2.311528808594" />
                  <Point X="2.121441650391" Y="2.289605957031" />
                  <Point X="2.129708007812" Y="2.267516357422" />
                  <Point X="2.140071044922" Y="2.247471191406" />
                  <Point X="2.176454345703" Y="2.1938515625" />
                  <Point X="2.18319921875" Y="2.185061767578" />
                  <Point X="2.20307421875" Y="2.162072509766" />
                  <Point X="2.221599365234" Y="2.145592285156" />
                  <Point X="2.275218994141" Y="2.109208984375" />
                  <Point X="2.284907958984" Y="2.102634765625" />
                  <Point X="2.304953125" Y="2.092271728516" />
                  <Point X="2.327040771484" Y="2.084006103516" />
                  <Point X="2.348963623047" Y="2.078663574219" />
                  <Point X="2.407763427734" Y="2.071573242188" />
                  <Point X="2.419317871094" Y="2.070890136719" />
                  <Point X="2.448846191406" Y="2.070946533203" />
                  <Point X="2.473206787109" Y="2.074171142578" />
                  <Point X="2.541205810547" Y="2.092354980469" />
                  <Point X="2.547249511719" Y="2.094188476562" />
                  <Point X="2.571619628906" Y="2.102475830078" />
                  <Point X="2.588533935547" Y="2.110145019531" />
                  <Point X="2.816933105469" Y="2.242011474609" />
                  <Point X="3.967326171875" Y="2.906190917969" />
                  <Point X="4.090535400391" Y="2.734958251953" />
                  <Point X="4.123272949219" Y="2.689461181641" />
                  <Point X="4.26219921875" Y="2.459884277344" />
                  <Point X="4.191583007813" Y="2.405698730469" />
                  <Point X="3.230783935547" Y="1.668451293945" />
                  <Point X="3.221424560547" Y="1.660241455078" />
                  <Point X="3.203973876953" Y="1.641627929688" />
                  <Point X="3.155034912109" Y="1.577783203125" />
                  <Point X="3.149247802734" Y="1.569324951172" />
                  <Point X="3.131934814453" Y="1.5408359375" />
                  <Point X="3.121629882812" Y="1.517085693359" />
                  <Point X="3.103399902344" Y="1.451900024414" />
                  <Point X="3.100105957031" Y="1.440121337891" />
                  <Point X="3.09665234375" Y="1.417821655273" />
                  <Point X="3.095836425781" Y="1.394251953125" />
                  <Point X="3.097739501953" Y="1.371768066406" />
                  <Point X="3.112704345703" Y="1.299240722656" />
                  <Point X="3.115392089844" Y="1.289087524414" />
                  <Point X="3.125294433594" Y="1.258604125977" />
                  <Point X="3.136282714844" Y="1.235740112305" />
                  <Point X="3.176985351562" Y="1.173873779297" />
                  <Point X="3.184340332031" Y="1.162694702148" />
                  <Point X="3.198894287109" Y="1.145449462891" />
                  <Point X="3.216138183594" Y="1.129359985352" />
                  <Point X="3.234347412109" Y="1.116034667969" />
                  <Point X="3.293331054688" Y="1.08283190918" />
                  <Point X="3.303219970703" Y="1.077997070312" />
                  <Point X="3.331853759766" Y="1.065999633789" />
                  <Point X="3.356118408203" Y="1.059438598633" />
                  <Point X="3.435868408203" Y="1.04889855957" />
                  <Point X="3.441738037109" Y="1.048307617188" />
                  <Point X="3.469226074219" Y="1.046399780273" />
                  <Point X="3.488203613281" Y="1.04698449707" />
                  <Point X="3.705167724609" Y="1.075548339844" />
                  <Point X="4.776839355469" Y="1.21663671875" />
                  <Point X="4.831875976562" Y="0.990562438965" />
                  <Point X="4.845936035156" Y="0.932809387207" />
                  <Point X="4.890864746094" Y="0.644238586426" />
                  <Point X="4.817676269531" Y="0.624627807617" />
                  <Point X="3.716579833984" Y="0.329589904785" />
                  <Point X="3.704790527344" Y="0.325586303711" />
                  <Point X="3.681545898438" Y="0.315067962646" />
                  <Point X="3.603194091797" Y="0.269779174805" />
                  <Point X="3.595038818359" Y="0.264490966797" />
                  <Point X="3.566568115234" Y="0.243886779785" />
                  <Point X="3.547530761719" Y="0.225576309204" />
                  <Point X="3.50051953125" Y="0.165672973633" />
                  <Point X="3.492024902344" Y="0.154848648071" />
                  <Point X="3.48030078125" Y="0.13556854248" />
                  <Point X="3.47052734375" Y="0.114105682373" />
                  <Point X="3.463680908203" Y="0.092604568481" />
                  <Point X="3.448010498047" Y="0.010779525757" />
                  <Point X="3.446650634766" Y="0.000889925778" />
                  <Point X="3.443818847656" Y="-0.032705997467" />
                  <Point X="3.445178710938" Y="-0.058554080963" />
                  <Point X="3.460849121094" Y="-0.140379119873" />
                  <Point X="3.463680908203" Y="-0.155164718628" />
                  <Point X="3.470527587891" Y="-0.176666748047" />
                  <Point X="3.480301513672" Y="-0.198130065918" />
                  <Point X="3.492025146484" Y="-0.217409103394" />
                  <Point X="3.539036132812" Y="-0.277312286377" />
                  <Point X="3.545959472656" Y="-0.285195678711" />
                  <Point X="3.568766113281" Y="-0.308440246582" />
                  <Point X="3.589035888672" Y="-0.324155548096" />
                  <Point X="3.667387939453" Y="-0.369444488525" />
                  <Point X="3.6722578125" Y="-0.37207321167" />
                  <Point X="3.698496582031" Y="-0.385264404297" />
                  <Point X="3.716579833984" Y="-0.392150054932" />
                  <Point X="3.915545654297" Y="-0.445462646484" />
                  <Point X="4.891472167969" Y="-0.706961425781" />
                  <Point X="4.862873046875" Y="-0.896654907227" />
                  <Point X="4.855022460938" Y="-0.948725524902" />
                  <Point X="4.801173828125" Y="-1.184698974609" />
                  <Point X="4.701344726562" Y="-1.171556274414" />
                  <Point X="3.424382080078" Y="-1.003440979004" />
                  <Point X="3.40803515625" Y="-1.002710266113" />
                  <Point X="3.374658447266" Y="-1.005508850098" />
                  <Point X="3.220881347656" Y="-1.038932861328" />
                  <Point X="3.193094238281" Y="-1.04497253418" />
                  <Point X="3.163974121094" Y="-1.056596923828" />
                  <Point X="3.136147705078" Y="-1.073489013672" />
                  <Point X="3.112397460938" Y="-1.093959960937" />
                  <Point X="3.019448974609" Y="-1.205748168945" />
                  <Point X="3.002653320312" Y="-1.225947998047" />
                  <Point X="2.987932373047" Y="-1.250330444336" />
                  <Point X="2.976589111328" Y="-1.277715942383" />
                  <Point X="2.969757568359" Y="-1.305365478516" />
                  <Point X="2.956435791016" Y="-1.450136108398" />
                  <Point X="2.954028564453" Y="-1.476295654297" />
                  <Point X="2.956347412109" Y="-1.507564208984" />
                  <Point X="2.964078857422" Y="-1.539184936523" />
                  <Point X="2.976450195312" Y="-1.567996337891" />
                  <Point X="3.061552734375" Y="-1.700367797852" />
                  <Point X="3.076930419922" Y="-1.724287109375" />
                  <Point X="3.086932861328" Y="-1.737239013672" />
                  <Point X="3.110628417969" Y="-1.760909301758" />
                  <Point X="3.295270019531" Y="-1.90258984375" />
                  <Point X="4.213122558594" Y="-2.6068828125" />
                  <Point X="4.146939941406" Y="-2.713976318359" />
                  <Point X="4.124810058594" Y="-2.749786132812" />
                  <Point X="4.028981445312" Y="-2.885945068359" />
                  <Point X="3.938261962891" Y="-2.833568115234" />
                  <Point X="2.800954589844" Y="-2.176943115234" />
                  <Point X="2.786131591797" Y="-2.170012451172" />
                  <Point X="2.754224609375" Y="-2.159825195312" />
                  <Point X="2.571205322266" Y="-2.126772216797" />
                  <Point X="2.538134277344" Y="-2.120799560547" />
                  <Point X="2.506781982422" Y="-2.120395263672" />
                  <Point X="2.474609130859" Y="-2.125353027344" />
                  <Point X="2.444833251953" Y="-2.135176757812" />
                  <Point X="2.2927890625" Y="-2.215196289062" />
                  <Point X="2.265315185547" Y="-2.229655761719" />
                  <Point X="2.242384521484" Y="-2.246548828125" />
                  <Point X="2.221424560547" Y="-2.267508789062" />
                  <Point X="2.204531494141" Y="-2.290439453125" />
                  <Point X="2.12451171875" Y="-2.442483398438" />
                  <Point X="2.110052490234" Y="-2.469957519531" />
                  <Point X="2.100228759766" Y="-2.499733886719" />
                  <Point X="2.095271240234" Y="-2.531906494141" />
                  <Point X="2.095675537109" Y="-2.563258300781" />
                  <Point X="2.128728515625" Y="-2.746277587891" />
                  <Point X="2.134701171875" Y="-2.779348632812" />
                  <Point X="2.138985595703" Y="-2.795141845703" />
                  <Point X="2.151819091797" Y="-2.826078613281" />
                  <Point X="2.270469970703" Y="-3.031587646484" />
                  <Point X="2.861283447266" Y="-4.054906738281" />
                  <Point X="2.808112792969" Y="-4.092885498047" />
                  <Point X="2.78186328125" Y="-4.111634765625" />
                  <Point X="2.701764892578" Y="-4.163481445312" />
                  <Point X="2.626416748047" Y="-4.065285400391" />
                  <Point X="1.758546264648" Y="-2.934255126953" />
                  <Point X="1.747503417969" Y="-2.922178955078" />
                  <Point X="1.721924072266" Y="-2.900557373047" />
                  <Point X="1.541417724609" Y="-2.784508300781" />
                  <Point X="1.50880065918" Y="-2.763538574219" />
                  <Point X="1.479989624023" Y="-2.751167236328" />
                  <Point X="1.448368530273" Y="-2.743435546875" />
                  <Point X="1.417099487305" Y="-2.741116699219" />
                  <Point X="1.219685058594" Y="-2.759282958984" />
                  <Point X="1.184012573242" Y="-2.762565673828" />
                  <Point X="1.15636328125" Y="-2.769397216797" />
                  <Point X="1.128978393555" Y="-2.780740234375" />
                  <Point X="1.104595825195" Y="-2.7954609375" />
                  <Point X="0.952157592773" Y="-2.922208496094" />
                  <Point X="0.924612182617" Y="-2.945111328125" />
                  <Point X="0.904141418457" Y="-2.968861572266" />
                  <Point X="0.887248779297" Y="-2.996688476562" />
                  <Point X="0.875624267578" Y="-3.025808837891" />
                  <Point X="0.830045959473" Y="-3.235504882812" />
                  <Point X="0.821809997559" Y="-3.273396728516" />
                  <Point X="0.81972454834" Y="-3.289627441406" />
                  <Point X="0.81974230957" Y="-3.323120117188" />
                  <Point X="0.85336706543" Y="-3.578524902344" />
                  <Point X="1.022065307617" Y="-4.859915039062" />
                  <Point X="1.000520568848" Y="-4.864637695312" />
                  <Point X="0.975678955078" Y="-4.870083007812" />
                  <Point X="0.929315490723" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.019642272949" Y="-4.760165039062" />
                  <Point X="-1.058435913086" Y="-4.752635253906" />
                  <Point X="-1.141246337891" Y="-4.731328613281" />
                  <Point X="-1.120775634766" Y="-4.575838378906" />
                  <Point X="-1.120077514648" Y="-4.568099609375" />
                  <Point X="-1.119451782227" Y="-4.541033691406" />
                  <Point X="-1.121759277344" Y="-4.509328125" />
                  <Point X="-1.123334106445" Y="-4.497690429688" />
                  <Point X="-1.174598144531" Y="-4.23996875" />
                  <Point X="-1.183861450195" Y="-4.193398925781" />
                  <Point X="-1.188125" Y="-4.17846875" />
                  <Point X="-1.19902734375" Y="-4.149502441406" />
                  <Point X="-1.205666015625" Y="-4.135466308594" />
                  <Point X="-1.221738037109" Y="-4.10762890625" />
                  <Point X="-1.23057409668" Y="-4.094861816406" />
                  <Point X="-1.250208496094" Y="-4.070937011719" />
                  <Point X="-1.261006958008" Y="-4.059779296875" />
                  <Point X="-1.458568847656" Y="-3.886521728516" />
                  <Point X="-1.494267456055" Y="-3.855214599609" />
                  <Point X="-1.506738525391" Y="-3.845965576172" />
                  <Point X="-1.533021484375" Y="-3.829621582031" />
                  <Point X="-1.546833618164" Y="-3.822526855469" />
                  <Point X="-1.576530883789" Y="-3.810225830078" />
                  <Point X="-1.591313476562" Y="-3.805476074219" />
                  <Point X="-1.621455200195" Y="-3.798447998047" />
                  <Point X="-1.636814208984" Y="-3.796169677734" />
                  <Point X="-1.899022827148" Y="-3.778983642578" />
                  <Point X="-1.946403442383" Y="-3.775878173828" />
                  <Point X="-1.961928344727" Y="-3.776132324219" />
                  <Point X="-1.992729736328" Y="-3.779166015625" />
                  <Point X="-2.008006103516" Y="-3.781945556641" />
                  <Point X="-2.03905480957" Y="-3.790265136719" />
                  <Point X="-2.053674316406" Y="-3.795496337891" />
                  <Point X="-2.081865478516" Y="-3.808269775391" />
                  <Point X="-2.095437011719" Y="-3.815812011719" />
                  <Point X="-2.313923339844" Y="-3.961800292969" />
                  <Point X="-2.353403564453" Y="-3.988180175781" />
                  <Point X="-2.359681640625" Y="-3.992757080078" />
                  <Point X="-2.380442382812" Y="-4.010131347656" />
                  <Point X="-2.402759277344" Y="-4.032771728516" />
                  <Point X="-2.410470947266" Y="-4.041629394531" />
                  <Point X="-2.444889648438" Y="-4.086484375" />
                  <Point X="-2.503202636719" Y="-4.162479492188" />
                  <Point X="-2.690735107422" Y="-4.046364257812" />
                  <Point X="-2.747581298828" Y="-4.011166259766" />
                  <Point X="-2.980862792969" Y="-3.831547607422" />
                  <Point X="-2.971993164062" Y="-3.816185058594" />
                  <Point X="-2.341488769531" Y="-2.724119628906" />
                  <Point X="-2.334850097656" Y="-2.710083251953" />
                  <Point X="-2.323947998047" Y="-2.681116943359" />
                  <Point X="-2.319684570312" Y="-2.666187011719" />
                  <Point X="-2.313413574219" Y="-2.634660644531" />
                  <Point X="-2.311638916016" Y="-2.619235351562" />
                  <Point X="-2.310626220703" Y="-2.588301269531" />
                  <Point X="-2.314666259766" Y="-2.557615234375" />
                  <Point X="-2.323650878906" Y="-2.527997558594" />
                  <Point X="-2.329357421875" Y="-2.513557128906" />
                  <Point X="-2.343574462891" Y="-2.484728271484" />
                  <Point X="-2.351557128906" Y="-2.47141015625" />
                  <Point X="-2.369584716797" Y="-2.446253173828" />
                  <Point X="-2.379629638672" Y="-2.434414306641" />
                  <Point X="-2.394323242188" Y="-2.419720703125" />
                  <Point X="-2.408817138672" Y="-2.407020751953" />
                  <Point X="-2.433974121094" Y="-2.388993164062" />
                  <Point X="-2.447292236328" Y="-2.381010498047" />
                  <Point X="-2.47612109375" Y="-2.366793457031" />
                  <Point X="-2.490561523438" Y="-2.361086914062" />
                  <Point X="-2.520179199219" Y="-2.352102294922" />
                  <Point X="-2.550865234375" Y="-2.348062255859" />
                  <Point X="-2.581799316406" Y="-2.349074951172" />
                  <Point X="-2.597224609375" Y="-2.350849609375" />
                  <Point X="-2.628750976562" Y="-2.357120605469" />
                  <Point X="-2.643680908203" Y="-2.361384033203" />
                  <Point X="-2.672647216797" Y="-2.372286132812" />
                  <Point X="-2.68668359375" Y="-2.378924804688" />
                  <Point X="-2.884017578125" Y="-2.492855712891" />
                  <Point X="-3.793089111328" Y="-3.017708251953" />
                  <Point X="-3.959102539062" Y="-2.799601074219" />
                  <Point X="-4.004014648438" Y="-2.740595703125" />
                  <Point X="-4.181265136719" Y="-2.443373535156" />
                  <Point X="-4.151299804688" Y="-2.420380371094" />
                  <Point X="-3.048122314453" Y="-1.573882080078" />
                  <Point X="-3.036482177734" Y="-1.563309936523" />
                  <Point X="-3.015104980469" Y="-1.540390136719" />
                  <Point X="-3.005367919922" Y="-1.528042358398" />
                  <Point X="-2.987403320312" Y="-1.500911743164" />
                  <Point X="-2.979836425781" Y="-1.487126708984" />
                  <Point X="-2.967080322266" Y="-1.458497070312" />
                  <Point X="-2.961891113281" Y="-1.44365234375" />
                  <Point X="-2.955365722656" Y="-1.418457397461" />
                  <Point X="-2.951552978516" Y="-1.398805175781" />
                  <Point X="-2.948748535156" Y="-1.368378417969" />
                  <Point X="-2.948577636719" Y="-1.353051147461" />
                  <Point X="-2.950785888672" Y="-1.321380493164" />
                  <Point X="-2.953082519531" Y="-1.306224365234" />
                  <Point X="-2.960083251953" Y="-1.276478881836" />
                  <Point X="-2.971777099609" Y="-1.248246337891" />
                  <Point X="-2.987859375" Y="-1.222262084961" />
                  <Point X="-2.996951904297" Y="-1.209921264648" />
                  <Point X="-3.017784179688" Y="-1.185964355469" />
                  <Point X="-3.028744140625" Y="-1.175245605469" />
                  <Point X="-3.052245361328" Y="-1.155710571289" />
                  <Point X="-3.064786621094" Y="-1.14689440918" />
                  <Point X="-3.087216308594" Y="-1.133693481445" />
                  <Point X="-3.105434082031" Y="-1.124481201172" />
                  <Point X="-3.134696044922" Y="-1.11325769043" />
                  <Point X="-3.149793212891" Y="-1.108860961914" />
                  <Point X="-3.181681640625" Y="-1.10237890625" />
                  <Point X="-3.197298095703" Y="-1.100532592773" />
                  <Point X="-3.228621826172" Y="-1.09944140625" />
                  <Point X="-3.244329101562" Y="-1.100196533203" />
                  <Point X="-3.493444580078" Y="-1.132993286133" />
                  <Point X="-4.660920898438" Y="-1.286694335938" />
                  <Point X="-4.723194335938" Y="-1.042895751953" />
                  <Point X="-4.740761230469" Y="-0.974120788574" />
                  <Point X="-4.786452148438" Y="-0.654654296875" />
                  <Point X="-4.764974121094" Y="-0.648899230957" />
                  <Point X="-3.508288085938" Y="-0.312171295166" />
                  <Point X="-3.500476806641" Y="-0.309712768555" />
                  <Point X="-3.473931152344" Y="-0.299251190186" />
                  <Point X="-3.444166503906" Y="-0.283892944336" />
                  <Point X="-3.433561279297" Y="-0.27751361084" />
                  <Point X="-3.410055664062" Y="-0.261199432373" />
                  <Point X="-3.393671386719" Y="-0.2482421875" />
                  <Point X="-3.371215332031" Y="-0.226357467651" />
                  <Point X="-3.360896240234" Y="-0.214482192993" />
                  <Point X="-3.341652832031" Y="-0.18822706604" />
                  <Point X="-3.333436035156" Y="-0.174812957764" />
                  <Point X="-3.319328369141" Y="-0.146812545776" />
                  <Point X="-3.313437744141" Y="-0.132226409912" />
                  <Point X="-3.305602539062" Y="-0.106981315613" />
                  <Point X="-3.300990478516" Y="-0.088500358582" />
                  <Point X="-3.296721191406" Y="-0.060340923309" />
                  <Point X="-3.295647949219" Y="-0.04610062027" />
                  <Point X="-3.295647949219" Y="-0.016459566116" />
                  <Point X="-3.296721191406" Y="-0.002219263554" />
                  <Point X="-3.300990478516" Y="0.025940317154" />
                  <Point X="-3.304186767578" Y="0.039859596252" />
                  <Point X="-3.312021972656" Y="0.065104682922" />
                  <Point X="-3.319328369141" Y="0.084252807617" />
                  <Point X="-3.333436035156" Y="0.112252922058" />
                  <Point X="-3.341652832031" Y="0.125666732788" />
                  <Point X="-3.360896240234" Y="0.151922012329" />
                  <Point X="-3.371215332031" Y="0.163797576904" />
                  <Point X="-3.393671386719" Y="0.185682296753" />
                  <Point X="-3.405808349609" Y="0.195691604614" />
                  <Point X="-3.429313964844" Y="0.212005630493" />
                  <Point X="-3.450904052734" Y="0.224959762573" />
                  <Point X="-3.470719970703" Y="0.235128234863" />
                  <Point X="-3.479919433594" Y="0.239247924805" />
                  <Point X="-3.498703857422" Y="0.246489562988" />
                  <Point X="-3.508288574219" Y="0.249611251831" />
                  <Point X="-3.735369140625" Y="0.310457305908" />
                  <Point X="-4.785446289062" Y="0.591824584961" />
                  <Point X="-4.742650878906" Y="0.881029968262" />
                  <Point X="-4.731331054688" Y="0.957528686523" />
                  <Point X="-4.633586425781" Y="1.318237182617" />
                  <Point X="-3.77806640625" Y="1.20560559082" />
                  <Point X="-3.767738769531" Y="1.204815429688" />
                  <Point X="-3.747058349609" Y="1.204364135742" />
                  <Point X="-3.736705322266" Y="1.204703125" />
                  <Point X="-3.715143554688" Y="1.20658984375" />
                  <Point X="-3.704889648438" Y="1.208053710938" />
                  <Point X="-3.684603271484" Y="1.212089111328" />
                  <Point X="-3.674570800781" Y="1.214660522461" />
                  <Point X="-3.622545654297" Y="1.231063842773" />
                  <Point X="-3.603450927734" Y="1.237676391602" />
                  <Point X="-3.584517578125" Y="1.246007324219" />
                  <Point X="-3.575278076172" Y="1.250689575195" />
                  <Point X="-3.556533691406" Y="1.26151171875" />
                  <Point X="-3.547858886719" Y="1.267172241211" />
                  <Point X="-3.531177734375" Y="1.279403564453" />
                  <Point X="-3.515927490234" Y="1.293377929688" />
                  <Point X="-3.5022890625" Y="1.308929931641" />
                  <Point X="-3.49589453125" Y="1.317078125" />
                  <Point X="-3.483479980469" Y="1.334808227539" />
                  <Point X="-3.478010986328" Y="1.343603271484" />
                  <Point X="-3.468062255859" Y="1.361737182617" />
                  <Point X="-3.463582519531" Y="1.371076293945" />
                  <Point X="-3.44270703125" Y="1.421473876953" />
                  <Point X="-3.435498535156" Y="1.440353271484" />
                  <Point X="-3.429710205078" Y="1.460213256836" />
                  <Point X="-3.427358398438" Y="1.470300415039" />
                  <Point X="-3.423600341797" Y="1.491615478516" />
                  <Point X="-3.422360595703" Y="1.50189855957" />
                  <Point X="-3.421008056641" Y="1.522538696289" />
                  <Point X="-3.421910400391" Y="1.543203125" />
                  <Point X="-3.425057128906" Y="1.563646606445" />
                  <Point X="-3.427188720703" Y="1.573782836914" />
                  <Point X="-3.432790771484" Y="1.594689331055" />
                  <Point X="-3.436012939453" Y="1.604532714844" />
                  <Point X="-3.443509521484" Y="1.623809936523" />
                  <Point X="-3.447783935547" Y="1.633244140625" />
                  <Point X="-3.472972167969" Y="1.681630371094" />
                  <Point X="-3.482800292969" Y="1.699286987305" />
                  <Point X="-3.494292236328" Y="1.716485839844" />
                  <Point X="-3.500507568359" Y="1.724771362305" />
                  <Point X="-3.514419921875" Y="1.74135144043" />
                  <Point X="-3.521500488281" Y="1.748911254883" />
                  <Point X="-3.536442382812" Y="1.76321496582" />
                  <Point X="-3.544303710938" Y="1.769958862305" />
                  <Point X="-3.674557617188" Y="1.86990625" />
                  <Point X="-4.227614746094" Y="2.294282226563" />
                  <Point X="-4.046276123047" Y="2.604958251953" />
                  <Point X="-4.002294677734" Y="2.680309570312" />
                  <Point X="-3.726338623047" Y="3.035012451172" />
                  <Point X="-3.254156982422" Y="2.7623984375" />
                  <Point X="-3.244918457031" Y="2.757716552734" />
                  <Point X="-3.225986083984" Y="2.749385986328" />
                  <Point X="-3.216292236328" Y="2.745737304688" />
                  <Point X="-3.195650146484" Y="2.739228759766" />
                  <Point X="-3.185615722656" Y="2.736656982422" />
                  <Point X="-3.165327880859" Y="2.732621582031" />
                  <Point X="-3.155074462891" Y="2.731157958984" />
                  <Point X="-3.082617919922" Y="2.724818847656" />
                  <Point X="-3.059173095703" Y="2.723334472656" />
                  <Point X="-3.038493896484" Y="2.723785644531" />
                  <Point X="-3.028166748047" Y="2.724575683594" />
                  <Point X="-3.006708251953" Y="2.727400634766" />
                  <Point X="-2.996528564453" Y="2.729310058594" />
                  <Point X="-2.976436523438" Y="2.734226318359" />
                  <Point X="-2.956998779297" Y="2.741301025391" />
                  <Point X="-2.938447265625" Y="2.750449462891" />
                  <Point X="-2.929421142578" Y="2.755530029297" />
                  <Point X="-2.911166748047" Y="2.767159179688" />
                  <Point X="-2.902746582031" Y="2.773193115234" />
                  <Point X="-2.886614501953" Y="2.786140625" />
                  <Point X="-2.878902587891" Y="2.793054199219" />
                  <Point X="-2.827472412109" Y="2.844484375" />
                  <Point X="-2.811265869141" Y="2.861489013672" />
                  <Point X="-2.798319335938" Y="2.877619628906" />
                  <Point X="-2.792285888672" Y="2.886038818359" />
                  <Point X="-2.78065625" Y="2.904293212891" />
                  <Point X="-2.775575195312" Y="2.913319824219" />
                  <Point X="-2.766426269531" Y="2.931871826172" />
                  <Point X="-2.7593515625" Y="2.95130859375" />
                  <Point X="-2.754434814453" Y="2.971400878906" />
                  <Point X="-2.752524902344" Y="2.981581787109" />
                  <Point X="-2.749699707031" Y="3.003041015625" />
                  <Point X="-2.748909667969" Y="3.013369140625" />
                  <Point X="-2.748458496094" Y="3.034049072266" />
                  <Point X="-2.748797363281" Y="3.044400878906" />
                  <Point X="-2.755136474609" Y="3.116857666016" />
                  <Point X="-2.757745605469" Y="3.140203857422" />
                  <Point X="-2.761781005859" Y="3.160491699219" />
                  <Point X="-2.764352783203" Y="3.170526123047" />
                  <Point X="-2.770861328125" Y="3.191168212891" />
                  <Point X="-2.774510009766" Y="3.200862060547" />
                  <Point X="-2.782840576172" Y="3.219794433594" />
                  <Point X="-2.787522460938" Y="3.229032958984" />
                  <Point X="-2.845241943359" Y="3.329005859375" />
                  <Point X="-3.059387207031" Y="3.699916259766" />
                  <Point X="-2.724971435547" Y="3.956309570312" />
                  <Point X="-2.648373779297" Y="4.015036132812" />
                  <Point X="-2.192525146484" Y="4.268296386719" />
                  <Point X="-2.118563720703" Y="4.171908203125" />
                  <Point X="-2.111820068359" Y="4.164047363281" />
                  <Point X="-2.097517822266" Y="4.149106933594" />
                  <Point X="-2.089959228516" Y="4.14202734375" />
                  <Point X="-2.073379638672" Y="4.128114746094" />
                  <Point X="-2.065092773438" Y="4.1218984375" />
                  <Point X="-2.047892944336" Y="4.110405761719" />
                  <Point X="-2.038979858398" Y="4.105129394531" />
                  <Point X="-1.95833605957" Y="4.063148193359" />
                  <Point X="-1.934330566406" Y="4.051288085938" />
                  <Point X="-1.915052978516" Y="4.043791015625" />
                  <Point X="-1.905208496094" Y="4.040568359375" />
                  <Point X="-1.884302368164" Y="4.034966308594" />
                  <Point X="-1.874166625977" Y="4.032834960938" />
                  <Point X="-1.853722412109" Y="4.029687988281" />
                  <Point X="-1.833055297852" Y="4.028785400391" />
                  <Point X="-1.812414550781" Y="4.030138427734" />
                  <Point X="-1.802132446289" Y="4.031378173828" />
                  <Point X="-1.780817382812" Y="4.035136474609" />
                  <Point X="-1.770731079102" Y="4.03748828125" />
                  <Point X="-1.750871948242" Y="4.043276367188" />
                  <Point X="-1.741098754883" Y="4.046713134766" />
                  <Point X="-1.657102539062" Y="4.081506103516" />
                  <Point X="-1.632585449219" Y="4.092272460938" />
                  <Point X="-1.614451660156" Y="4.102221191406" />
                  <Point X="-1.605656860352" Y="4.107689941406" />
                  <Point X="-1.587926879883" Y="4.120104492188" />
                  <Point X="-1.579778686523" Y="4.126499023438" />
                  <Point X="-1.564227172852" Y="4.140137207031" />
                  <Point X="-1.550252441406" Y="4.155388183594" />
                  <Point X="-1.538021362305" Y="4.172069335938" />
                  <Point X="-1.532361694336" Y="4.180743164062" />
                  <Point X="-1.521539550781" Y="4.199487304687" />
                  <Point X="-1.516857299805" Y="4.208726074219" />
                  <Point X="-1.508526123047" Y="4.227659667969" />
                  <Point X="-1.504877197266" Y="4.237354492188" />
                  <Point X="-1.477538085938" Y="4.324062988281" />
                  <Point X="-1.470026245117" Y="4.349764648437" />
                  <Point X="-1.465990966797" Y="4.370051757812" />
                  <Point X="-1.46452722168" Y="4.380305175781" />
                  <Point X="-1.46264074707" Y="4.4018671875" />
                  <Point X="-1.462301757812" Y="4.412218261719" />
                  <Point X="-1.462752807617" Y="4.4328984375" />
                  <Point X="-1.46354309082" Y="4.443227539062" />
                  <Point X="-1.47010546875" Y="4.493071289062" />
                  <Point X="-1.479266235352" Y="4.562654785156" />
                  <Point X="-1.030335449219" Y="4.68851953125" />
                  <Point X="-0.931174804688" Y="4.716320800781" />
                  <Point X="-0.365222015381" Y="4.782556640625" />
                  <Point X="-0.22566633606" Y="4.261727539062" />
                  <Point X="-0.22043510437" Y="4.247107421875" />
                  <Point X="-0.207661560059" Y="4.218916015625" />
                  <Point X="-0.200119384766" Y="4.205344726562" />
                  <Point X="-0.182261154175" Y="4.178618164062" />
                  <Point X="-0.17260824585" Y="4.166455566406" />
                  <Point X="-0.151451187134" Y="4.143866699219" />
                  <Point X="-0.126897789001" Y="4.125026367188" />
                  <Point X="-0.099602600098" Y="4.110436523438" />
                  <Point X="-0.085356651306" Y="4.104260742188" />
                  <Point X="-0.054918533325" Y="4.093927978516" />
                  <Point X="-0.039857234955" Y="4.090155273438" />
                  <Point X="-0.009319986343" Y="4.08511328125" />
                  <Point X="0.021631772995" Y="4.08511328125" />
                  <Point X="0.052169025421" Y="4.090155273438" />
                  <Point X="0.067230316162" Y="4.093927978516" />
                  <Point X="0.097668441772" Y="4.104260742188" />
                  <Point X="0.111914390564" Y="4.110436523438" />
                  <Point X="0.139209732056" Y="4.125026367188" />
                  <Point X="0.163763275146" Y="4.1438671875" />
                  <Point X="0.184920181274" Y="4.166456054688" />
                  <Point X="0.194572799683" Y="4.178618164062" />
                  <Point X="0.212431182861" Y="4.205344726562" />
                  <Point X="0.219973648071" Y="4.218916503906" />
                  <Point X="0.232747192383" Y="4.247107910156" />
                  <Point X="0.237978118896" Y="4.261727539062" />
                  <Point X="0.267553039551" Y="4.372102539063" />
                  <Point X="0.378190216064" Y="4.785006347656" />
                  <Point X="0.741282897949" Y="4.74698046875" />
                  <Point X="0.827876708984" Y="4.737912109375" />
                  <Point X="1.366450683594" Y="4.607883789062" />
                  <Point X="1.453599975586" Y="4.586842773438" />
                  <Point X="1.802959472656" Y="4.460127929688" />
                  <Point X="1.858256713867" Y="4.440071289062" />
                  <Point X="2.197234619141" Y="4.281542480469" />
                  <Point X="2.250448730469" Y="4.256655761719" />
                  <Point X="2.577967285156" Y="4.065842529297" />
                  <Point X="2.629434814453" Y="4.035857666016" />
                  <Point X="2.817780029297" Y="3.901916992188" />
                  <Point X="2.795070068359" Y="3.862581787109" />
                  <Point X="2.06530859375" Y="2.598597900391" />
                  <Point X="2.062373291016" Y="2.59310546875" />
                  <Point X="2.053181640625" Y="2.573439453125" />
                  <Point X="2.044182373047" Y="2.549564697266" />
                  <Point X="2.041301635742" Y="2.540599121094" />
                  <Point X="2.023117797852" Y="2.472600097656" />
                  <Point X="2.018722290039" Y="2.45137890625" />
                  <Point X="2.014098510742" Y="2.420189453125" />
                  <Point X="2.013080200195" Y="2.407548583984" />
                  <Point X="2.012736694336" Y="2.382244140625" />
                  <Point X="2.013410888672" Y="2.369580566406" />
                  <Point X="2.020501342773" Y="2.310780761719" />
                  <Point X="2.023800292969" Y="2.289035888672" />
                  <Point X="2.029142822266" Y="2.267113037109" />
                  <Point X="2.032467529297" Y="2.256310058594" />
                  <Point X="2.040733886719" Y="2.234220458984" />
                  <Point X="2.045318481445" Y="2.223888183594" />
                  <Point X="2.055681640625" Y="2.203843017578" />
                  <Point X="2.061459960938" Y="2.194130126953" />
                  <Point X="2.097843261719" Y="2.140510498047" />
                  <Point X="2.111333007812" Y="2.122930908203" />
                  <Point X="2.131208007812" Y="2.099941650391" />
                  <Point X="2.139930908203" Y="2.091094238281" />
                  <Point X="2.158456054688" Y="2.074614013672" />
                  <Point X="2.168258300781" Y="2.066981201172" />
                  <Point X="2.221877929688" Y="2.03059777832" />
                  <Point X="2.241279785156" Y="2.018245117188" />
                  <Point X="2.261324951172" Y="2.007882080078" />
                  <Point X="2.271657226562" Y="2.003297607422" />
                  <Point X="2.293744873047" Y="1.995032104492" />
                  <Point X="2.304547851562" Y="1.99170715332" />
                  <Point X="2.326470703125" Y="1.986364868164" />
                  <Point X="2.337590576172" Y="1.984346801758" />
                  <Point X="2.396390380859" Y="1.977256469727" />
                  <Point X="2.419499267578" Y="1.975890258789" />
                  <Point X="2.449027587891" Y="1.975946655273" />
                  <Point X="2.4613125" Y="1.976768066406" />
                  <Point X="2.485673095703" Y="1.979992553711" />
                  <Point X="2.497748779297" Y="1.982395874023" />
                  <Point X="2.565747802734" Y="2.000579711914" />
                  <Point X="2.577835205078" Y="2.004246826172" />
                  <Point X="2.602205322266" Y="2.012534179688" />
                  <Point X="2.610849853516" Y="2.015954345703" />
                  <Point X="2.636033935547" Y="2.027872680664" />
                  <Point X="2.864433105469" Y="2.159739013672" />
                  <Point X="3.940404541016" Y="2.780951171875" />
                  <Point X="4.013422851562" Y="2.679472412109" />
                  <Point X="4.043955322266" Y="2.637040039062" />
                  <Point X="4.136885253906" Y="2.483472412109" />
                  <Point X="4.133750976562" Y="2.481067382812" />
                  <Point X="3.172951660156" Y="1.743819946289" />
                  <Point X="3.168137939453" Y="1.739868774414" />
                  <Point X="3.152119384766" Y="1.725217041016" />
                  <Point X="3.134668701172" Y="1.706603515625" />
                  <Point X="3.128576416016" Y="1.699422485352" />
                  <Point X="3.079637451172" Y="1.635577758789" />
                  <Point X="3.068063232422" Y="1.618661376953" />
                  <Point X="3.050750244141" Y="1.590172363281" />
                  <Point X="3.044784667969" Y="1.578649291992" />
                  <Point X="3.034479736328" Y="1.554899047852" />
                  <Point X="3.030140380859" Y="1.54267175293" />
                  <Point X="3.011910400391" Y="1.477486206055" />
                  <Point X="3.006225097656" Y="1.454660888672" />
                  <Point X="3.002771484375" Y="1.432361206055" />
                  <Point X="3.001709228516" Y="1.421108276367" />
                  <Point X="3.000893310547" Y="1.397538574219" />
                  <Point X="3.001174804688" Y="1.386239624023" />
                  <Point X="3.003077880859" Y="1.363755737305" />
                  <Point X="3.004699462891" Y="1.352570800781" />
                  <Point X="3.019664306641" Y="1.280043334961" />
                  <Point X="3.025039794922" Y="1.259737060547" />
                  <Point X="3.034942138672" Y="1.229253540039" />
                  <Point X="3.039669677734" Y="1.217453369141" />
                  <Point X="3.050657958984" Y="1.194589355469" />
                  <Point X="3.056918701172" Y="1.183525512695" />
                  <Point X="3.097621337891" Y="1.121659179688" />
                  <Point X="3.111739501953" Y="1.101423828125" />
                  <Point X="3.126293457031" Y="1.084178588867" />
                  <Point X="3.134084472656" Y="1.075989501953" />
                  <Point X="3.151328369141" Y="1.059900024414" />
                  <Point X="3.160035644531" Y="1.052695068359" />
                  <Point X="3.178244873047" Y="1.039369750977" />
                  <Point X="3.187746582031" Y="1.033249511719" />
                  <Point X="3.246730224609" Y="1.000046936035" />
                  <Point X="3.2665078125" Y="0.990377380371" />
                  <Point X="3.295141601562" Y="0.978379943848" />
                  <Point X="3.307056640625" Y="0.97429296875" />
                  <Point X="3.331321289062" Y="0.967731994629" />
                  <Point X="3.343671142578" Y="0.965257568359" />
                  <Point X="3.423421142578" Y="0.954717529297" />
                  <Point X="3.435160400391" Y="0.953535522461" />
                  <Point X="3.4626484375" Y="0.951627807617" />
                  <Point X="3.472151611328" Y="0.951444824219" />
                  <Point X="3.500603515625" Y="0.952797180176" />
                  <Point X="3.717567626953" Y="0.98136114502" />
                  <Point X="4.704703613281" Y="1.111320068359" />
                  <Point X="4.739571777344" Y="0.968091491699" />
                  <Point X="4.75268359375" Y="0.914233947754" />
                  <Point X="4.78387109375" Y="0.713920959473" />
                  <Point X="3.691991943359" Y="0.421352874756" />
                  <Point X="3.686031738281" Y="0.419544433594" />
                  <Point X="3.665625488281" Y="0.412137512207" />
                  <Point X="3.642380859375" Y="0.401619171143" />
                  <Point X="3.634004638672" Y="0.397316558838" />
                  <Point X="3.555652832031" Y="0.352027740479" />
                  <Point X="3.539342529297" Y="0.341451568604" />
                  <Point X="3.510871826172" Y="0.320847381592" />
                  <Point X="3.500712890625" Y="0.312356414795" />
                  <Point X="3.481675537109" Y="0.294045928955" />
                  <Point X="3.472796875" Y="0.284226409912" />
                  <Point X="3.425785644531" Y="0.224323059082" />
                  <Point X="3.410854248047" Y="0.204207992554" />
                  <Point X="3.399130126953" Y="0.184927886963" />
                  <Point X="3.393842773438" Y="0.174938491821" />
                  <Point X="3.384069335938" Y="0.153475708008" />
                  <Point X="3.380005615234" Y="0.142929870605" />
                  <Point X="3.373159179688" Y="0.121428749084" />
                  <Point X="3.370376464844" Y="0.110473457336" />
                  <Point X="3.354706054688" Y="0.028648374557" />
                  <Point X="3.351986328125" Y="0.008869194031" />
                  <Point X="3.349154541016" Y="-0.024726715088" />
                  <Point X="3.348949951172" Y="-0.037697036743" />
                  <Point X="3.350309814453" Y="-0.063545085907" />
                  <Point X="3.351874267578" Y="-0.076422966003" />
                  <Point X="3.367544677734" Y="-0.158247894287" />
                  <Point X="3.373159179688" Y="-0.183988632202" />
                  <Point X="3.380005859375" Y="-0.205490661621" />
                  <Point X="3.384069824219" Y="-0.21603767395" />
                  <Point X="3.39384375" Y="-0.237501052856" />
                  <Point X="3.399131347656" Y="-0.247489852905" />
                  <Point X="3.410854980469" Y="-0.266768920898" />
                  <Point X="3.417291015625" Y="-0.276059051514" />
                  <Point X="3.464302001953" Y="-0.335962249756" />
                  <Point X="3.478148681641" Y="-0.351729064941" />
                  <Point X="3.500955322266" Y="-0.374973510742" />
                  <Point X="3.510557373047" Y="-0.383518463135" />
                  <Point X="3.530827148438" Y="-0.399233825684" />
                  <Point X="3.541494628906" Y="-0.406404144287" />
                  <Point X="3.619846679688" Y="-0.451693115234" />
                  <Point X="3.629586914062" Y="-0.456950653076" />
                  <Point X="3.655825683594" Y="-0.470141815186" />
                  <Point X="3.664690673828" Y="-0.47404598999" />
                  <Point X="3.6919921875" Y="-0.483913085938" />
                  <Point X="3.890958007812" Y="-0.537225585938" />
                  <Point X="4.784876953125" Y="-0.776750427246" />
                  <Point X="4.768934570312" Y="-0.882492370605" />
                  <Point X="4.76161328125" Y="-0.931051635742" />
                  <Point X="4.727801757812" Y="-1.079219726563" />
                  <Point X="4.713744628906" Y="-1.077369018555" />
                  <Point X="3.436781982422" Y="-0.909253662109" />
                  <Point X="3.428624267578" Y="-0.908535827637" />
                  <Point X="3.400097412109" Y="-0.908042541504" />
                  <Point X="3.366720703125" Y="-0.910841125488" />
                  <Point X="3.354480957031" Y="-0.912676330566" />
                  <Point X="3.200703857422" Y="-0.946100402832" />
                  <Point X="3.172916748047" Y="-0.952140014648" />
                  <Point X="3.157874023438" Y="-0.956742553711" />
                  <Point X="3.12875390625" Y="-0.968366943359" />
                  <Point X="3.114676513672" Y="-0.975388793945" />
                  <Point X="3.086850097656" Y="-0.992280944824" />
                  <Point X="3.074124267578" Y="-1.001529907227" />
                  <Point X="3.050374023438" Y="-1.022000976562" />
                  <Point X="3.039349609375" Y="-1.03322277832" />
                  <Point X="2.946401123047" Y="-1.145010864258" />
                  <Point X="2.92960546875" Y="-1.16521081543" />
                  <Point X="2.921326416016" Y="-1.176846679688" />
                  <Point X="2.90660546875" Y="-1.201229125977" />
                  <Point X="2.900163574219" Y="-1.213976074219" />
                  <Point X="2.8888203125" Y="-1.241361572266" />
                  <Point X="2.884362548828" Y="-1.254928955078" />
                  <Point X="2.877531005859" Y="-1.282578491211" />
                  <Point X="2.875157226562" Y="-1.296660400391" />
                  <Point X="2.861835449219" Y="-1.441431030273" />
                  <Point X="2.859428222656" Y="-1.467590576172" />
                  <Point X="2.859288818359" Y="-1.483321533203" />
                  <Point X="2.861607666016" Y="-1.514590209961" />
                  <Point X="2.864065917969" Y="-1.530127685547" />
                  <Point X="2.871797363281" Y="-1.561748291016" />
                  <Point X="2.876785888672" Y="-1.576667602539" />
                  <Point X="2.889157226562" Y="-1.605479003906" />
                  <Point X="2.896540039062" Y="-1.61937109375" />
                  <Point X="2.981642578125" Y="-1.751742553711" />
                  <Point X="2.997020263672" Y="-1.775661865234" />
                  <Point X="3.001741943359" Y="-1.782353271484" />
                  <Point X="3.019793457031" Y="-1.804449951172" />
                  <Point X="3.043489013672" Y="-1.828120239258" />
                  <Point X="3.052796142578" Y="-1.836277832031" />
                  <Point X="3.237437744141" Y="-1.977958251953" />
                  <Point X="4.087170654297" Y="-2.629981689453" />
                  <Point X="4.066126464844" Y="-2.664034423828" />
                  <Point X="4.045486083984" Y="-2.697434082031" />
                  <Point X="4.001274902344" Y="-2.760251953125" />
                  <Point X="3.985761962891" Y="-2.751295654297" />
                  <Point X="2.848454589844" Y="-2.094670654297" />
                  <Point X="2.841191894531" Y="-2.090885253906" />
                  <Point X="2.815026123047" Y="-2.079513183594" />
                  <Point X="2.783119140625" Y="-2.069325927734" />
                  <Point X="2.771108398438" Y="-2.066337646484" />
                  <Point X="2.588089111328" Y="-2.033284545898" />
                  <Point X="2.555018066406" Y="-2.027312011719" />
                  <Point X="2.539359130859" Y="-2.025807495117" />
                  <Point X="2.508006835938" Y="-2.025403198242" />
                  <Point X="2.492313476562" Y="-2.026503662109" />
                  <Point X="2.460140625" Y="-2.031461425781" />
                  <Point X="2.444844482422" Y="-2.035136230469" />
                  <Point X="2.415068603516" Y="-2.044959838867" />
                  <Point X="2.400588867188" Y="-2.051108642578" />
                  <Point X="2.248544677734" Y="-2.131128173828" />
                  <Point X="2.221070800781" Y="-2.145587646484" />
                  <Point X="2.208968261719" Y="-2.153170410156" />
                  <Point X="2.186037597656" Y="-2.170063476562" />
                  <Point X="2.175209472656" Y="-2.179373779297" />
                  <Point X="2.154249511719" Y="-2.200333740234" />
                  <Point X="2.144939208984" Y="-2.211161865234" />
                  <Point X="2.128046142578" Y="-2.234092529297" />
                  <Point X="2.120463378906" Y="-2.246195068359" />
                  <Point X="2.040443725586" Y="-2.398239013672" />
                  <Point X="2.02598449707" Y="-2.425713134766" />
                  <Point X="2.019835571289" Y="-2.440193359375" />
                  <Point X="2.01001184082" Y="-2.469969726563" />
                  <Point X="2.006336914062" Y="-2.485265869141" />
                  <Point X="2.001379272461" Y="-2.517438476562" />
                  <Point X="2.000279174805" Y="-2.533131347656" />
                  <Point X="2.00068347168" Y="-2.564483154297" />
                  <Point X="2.002187866211" Y="-2.580142089844" />
                  <Point X="2.035240844727" Y="-2.763161376953" />
                  <Point X="2.041213500977" Y="-2.796232421875" />
                  <Point X="2.043015014648" Y="-2.804221435547" />
                  <Point X="2.051236083984" Y="-2.83154296875" />
                  <Point X="2.064069580078" Y="-2.862479736328" />
                  <Point X="2.069546630859" Y="-2.873578613281" />
                  <Point X="2.188197509766" Y="-3.079087646484" />
                  <Point X="2.735893310547" Y="-4.027724609375" />
                  <Point X="2.72375390625" Y="-4.036083251953" />
                  <Point X="2.701785400391" Y="-4.007453125" />
                  <Point X="1.833914916992" Y="-2.876422851562" />
                  <Point X="1.828653808594" Y="-2.870146484375" />
                  <Point X="1.808830810547" Y="-2.849625732422" />
                  <Point X="1.783251464844" Y="-2.828004150391" />
                  <Point X="1.773298950195" Y="-2.820647216797" />
                  <Point X="1.592792480469" Y="-2.704598144531" />
                  <Point X="1.56017565918" Y="-2.683628417969" />
                  <Point X="1.546283813477" Y="-2.676245849609" />
                  <Point X="1.51747277832" Y="-2.663874511719" />
                  <Point X="1.502553466797" Y="-2.658885742188" />
                  <Point X="1.470932250977" Y="-2.651154052734" />
                  <Point X="1.455394165039" Y="-2.648695800781" />
                  <Point X="1.424125244141" Y="-2.646376953125" />
                  <Point X="1.408394287109" Y="-2.646516357422" />
                  <Point X="1.210979858398" Y="-2.664682617188" />
                  <Point X="1.175307373047" Y="-2.667965332031" />
                  <Point X="1.161225341797" Y="-2.670339111328" />
                  <Point X="1.133576049805" Y="-2.677170654297" />
                  <Point X="1.120008789062" Y="-2.681628417969" />
                  <Point X="1.092623901367" Y="-2.692971435547" />
                  <Point X="1.079877929688" Y="-2.699412841797" />
                  <Point X="1.055495361328" Y="-2.714133544922" />
                  <Point X="1.043858764648" Y="-2.722412841797" />
                  <Point X="0.891420532227" Y="-2.849160400391" />
                  <Point X="0.863875061035" Y="-2.872063232422" />
                  <Point X="0.852652832031" Y="-2.883088378906" />
                  <Point X="0.832182128906" Y="-2.906838623047" />
                  <Point X="0.822933654785" Y="-2.919563476563" />
                  <Point X="0.806041015625" Y="-2.947390380859" />
                  <Point X="0.799018859863" Y="-2.961468017578" />
                  <Point X="0.787394348145" Y="-2.990588378906" />
                  <Point X="0.782791748047" Y="-3.005631347656" />
                  <Point X="0.737213439941" Y="-3.215327392578" />
                  <Point X="0.728977478027" Y="-3.253219238281" />
                  <Point X="0.727584594727" Y="-3.261289794922" />
                  <Point X="0.724724487305" Y="-3.289677734375" />
                  <Point X="0.724742370605" Y="-3.323170410156" />
                  <Point X="0.725554992676" Y="-3.335520019531" />
                  <Point X="0.759179748535" Y="-3.590924804688" />
                  <Point X="0.833091674805" Y="-4.152341308594" />
                  <Point X="0.655064941406" Y="-3.487937011719" />
                  <Point X="0.652606445312" Y="-3.480125" />
                  <Point X="0.642145141602" Y="-3.453580566406" />
                  <Point X="0.62678704834" Y="-3.423815917969" />
                  <Point X="0.620407592773" Y="-3.413210205078" />
                  <Point X="0.481737213135" Y="-3.213412597656" />
                  <Point X="0.456679840088" Y="-3.177309814453" />
                  <Point X="0.446670837402" Y="-3.165173095703" />
                  <Point X="0.424786712646" Y="-3.142717529297" />
                  <Point X="0.412911865234" Y="-3.132398925781" />
                  <Point X="0.386656616211" Y="-3.113155029297" />
                  <Point X="0.3732421875" Y="-3.104937744141" />
                  <Point X="0.345241210938" Y="-3.090829589844" />
                  <Point X="0.330654602051" Y="-3.084938476562" />
                  <Point X="0.116070480347" Y="-3.018339355469" />
                  <Point X="0.077295654297" Y="-3.006305175781" />
                  <Point X="0.06337638092" Y="-3.003109130859" />
                  <Point X="0.035216796875" Y="-2.99883984375" />
                  <Point X="0.020976644516" Y="-2.997766601562" />
                  <Point X="-0.008664855957" Y="-2.997766601562" />
                  <Point X="-0.02290530777" Y="-2.998840087891" />
                  <Point X="-0.051064590454" Y="-3.003109375" />
                  <Point X="-0.064983718872" Y="-3.006305175781" />
                  <Point X="-0.279567993164" Y="-3.072904052734" />
                  <Point X="-0.318342803955" Y="-3.084938476562" />
                  <Point X="-0.332929992676" Y="-3.090829833984" />
                  <Point X="-0.36093145752" Y="-3.104938476562" />
                  <Point X="-0.374345855713" Y="-3.113155761719" />
                  <Point X="-0.400600982666" Y="-3.132399658203" />
                  <Point X="-0.412475372314" Y="-3.142718017578" />
                  <Point X="-0.434358886719" Y="-3.165173095703" />
                  <Point X="-0.444368041992" Y="-3.177309814453" />
                  <Point X="-0.583038269043" Y="-3.377107177734" />
                  <Point X="-0.60809564209" Y="-3.413210205078" />
                  <Point X="-0.612470947266" Y="-3.420133300781" />
                  <Point X="-0.62597644043" Y="-3.445264648438" />
                  <Point X="-0.638777648926" Y="-3.476215332031" />
                  <Point X="-0.642753173828" Y="-3.487936767578" />
                  <Point X="-0.704059204102" Y="-3.716734863281" />
                  <Point X="-0.985425170898" Y="-4.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.720289663729" Y="0.998274900122" />
                  <Point X="-4.712855535064" Y="0.57237395415" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.691879998617" Y="-0.629313724083" />
                  <Point X="-4.681834532271" Y="-1.204818105567" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.630853875558" Y="1.317877434775" />
                  <Point X="-4.617394586379" Y="0.54679527412" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.597307847978" Y="-0.603973198146" />
                  <Point X="-4.585564076247" Y="-1.27677343005" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.535620557478" Y="1.30533970338" />
                  <Point X="-4.521933637694" Y="0.521216594089" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.502735697339" Y="-0.57863267221" />
                  <Point X="-4.490767447834" Y="-1.264293227175" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.440387239399" Y="1.292801971986" />
                  <Point X="-4.426472689009" Y="0.495637914059" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.408163546701" Y="-0.553292146274" />
                  <Point X="-4.395970819421" Y="-1.2518130243" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.345153921319" Y="1.280264240591" />
                  <Point X="-4.331011740324" Y="0.470059234028" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.313591396062" Y="-0.527951620337" />
                  <Point X="-4.301174191008" Y="-1.239332821424" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.249920603239" Y="1.267726509197" />
                  <Point X="-4.235550791639" Y="0.444480553998" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.219019245424" Y="-0.502611094401" />
                  <Point X="-4.206377562596" Y="-1.226852618549" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.174415630339" Y="2.385424916185" />
                  <Point X="-4.172080922493" Y="2.251669593272" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.15468728516" Y="1.255188777803" />
                  <Point X="-4.140089842954" Y="0.418901873967" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.124447094785" Y="-0.477270568464" />
                  <Point X="-4.111580934183" Y="-1.214372415674" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.091333165148" Y="-2.37436632681" />
                  <Point X="-4.087380689509" Y="-2.600803504511" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.082160031654" Y="2.543480614624" />
                  <Point X="-4.075776574744" Y="2.177772613219" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.05945396708" Y="1.242651046408" />
                  <Point X="-4.04462889427" Y="0.393323193936" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.029874944146" Y="-0.451930042528" />
                  <Point X="-4.01678430577" Y="-1.201892212799" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.997574474374" Y="-2.302422716457" />
                  <Point X="-3.989595470034" Y="-2.75953956892" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.989813937222" Y="2.696351815228" />
                  <Point X="-3.979472226995" Y="2.103875633165" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.964220649" Y="1.230113315014" />
                  <Point X="-3.949167945585" Y="0.367744513906" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.935302793508" Y="-0.426589516591" />
                  <Point X="-3.921987677358" Y="-1.189412009924" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.903815783599" Y="-2.230479106103" />
                  <Point X="-3.892350953203" Y="-2.887298799637" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.896884436304" Y="2.815799680684" />
                  <Point X="-3.883167879246" Y="2.029978653112" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.86898733092" Y="1.217575583619" />
                  <Point X="-3.8537069969" Y="0.342165833875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.840730642869" Y="-0.401248990655" />
                  <Point X="-3.827191048945" Y="-1.176931807049" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.810057092825" Y="-2.158535495749" />
                  <Point X="-3.795106438599" Y="-3.01505790274" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.803954935387" Y="2.93524754614" />
                  <Point X="-3.786863531497" Y="1.956081673058" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.773758169206" Y="1.205275970254" />
                  <Point X="-3.758246048215" Y="0.316587153845" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.74615849223" Y="-0.375908464718" />
                  <Point X="-3.732394420532" Y="-1.164451604174" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.716298402051" Y="-2.086591885396" />
                  <Point X="-3.700974012227" Y="-2.964525590431" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.710522477066" Y="3.025881001184" />
                  <Point X="-3.690559183748" Y="1.882184693005" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.678888190493" Y="1.213553937224" />
                  <Point X="-3.662785099477" Y="0.291008470798" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.651586341592" Y="-0.350567938782" />
                  <Point X="-3.63759779212" Y="-1.151971401299" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.622539711277" Y="-2.014648275042" />
                  <Point X="-3.606907513461" Y="-2.910216288121" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.614540732629" Y="2.970465952527" />
                  <Point X="-3.594254836263" Y="1.808287728075" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.584440873802" Y="1.246046195241" />
                  <Point X="-3.567324150723" Y="0.2654297868" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.557014190953" Y="-0.325227412845" />
                  <Point X="-3.542801163707" Y="-1.139491198424" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.528781020502" Y="-1.942704664689" />
                  <Point X="-3.512841014694" Y="-2.855906985811" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.518558988193" Y="2.91505090387" />
                  <Point X="-3.497717663314" Y="1.721052201236" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.490793433917" Y="1.324363364746" />
                  <Point X="-3.471789121233" Y="0.235607020281" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.462555591907" Y="-0.293381520526" />
                  <Point X="-3.448004535626" Y="-1.127010976563" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.435022329728" Y="-1.870761054335" />
                  <Point X="-3.418774515928" Y="-2.801597683501" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.422577243756" Y="2.859635855213" />
                  <Point X="-3.375595726315" Y="0.168066523657" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.368760344251" Y="-0.223532252531" />
                  <Point X="-3.353207907905" Y="-1.114530734081" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.341263638954" Y="-1.798817443981" />
                  <Point X="-3.324708017162" Y="-2.747288381191" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.32659549932" Y="2.804220806557" />
                  <Point X="-3.258411280183" Y="-1.102050491599" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.247504948179" Y="-1.726873833628" />
                  <Point X="-3.230641518396" Y="-2.692979078882" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.230659779245" Y="2.751442491798" />
                  <Point X="-3.163325947897" Y="-1.106110122571" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.153746257405" Y="-1.654930223274" />
                  <Point X="-3.13657501963" Y="-2.638669776572" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.135260982746" Y="2.729424508128" />
                  <Point X="-3.067628782247" Y="-1.145221663449" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.059987566631" Y="-1.582986612921" />
                  <Point X="-3.042508520864" Y="-2.584360474262" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.057215570325" Y="3.701581232449" />
                  <Point X="-3.05711790004" Y="3.69598570557" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.040147455129" Y="2.72374956774" />
                  <Point X="-2.970773724577" Y="-1.250668793743" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.967143589946" Y="-1.4586390675" />
                  <Point X="-2.948442022098" Y="-2.530051171952" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.927342400515" Y="-3.738847682832" />
                  <Point X="-2.924973172321" Y="-3.874580675166" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.963455850616" Y="3.773465895142" />
                  <Point X="-2.959141297038" Y="3.526285286212" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.945537997208" Y="2.746952760904" />
                  <Point X="-2.854375523625" Y="-2.475741852848" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.835116208725" Y="-3.579107264464" />
                  <Point X="-2.828664330152" Y="-3.948735140366" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.869696130906" Y="3.845350557834" />
                  <Point X="-2.861164694035" Y="3.356584866855" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.85180128465" Y="2.820155502459" />
                  <Point X="-2.760309025789" Y="-2.421432497245" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.742890016935" Y="-3.419366846096" />
                  <Point X="-2.732395999783" Y="-4.020568686083" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.775936411197" Y="3.917235220527" />
                  <Point X="-2.762796569077" Y="3.164454169678" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.759094439799" Y="2.952359325368" />
                  <Point X="-2.666194797376" Y="-2.369857624567" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.650663825146" Y="-3.259626427727" />
                  <Point X="-2.636343414906" Y="-4.080042180899" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.682176690395" Y="3.989119820637" />
                  <Point X="-2.571548946494" Y="-2.348739382735" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.558437633356" Y="-3.099886009359" />
                  <Point X="-2.540290834076" Y="-4.139515443815" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.5881981541" Y="4.048468489563" />
                  <Point X="-2.476220022689" Y="-2.366754362472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.466211441566" Y="-2.940145590991" />
                  <Point X="-2.446172833851" Y="-4.088156658118" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.494096253061" Y="4.100749597007" />
                  <Point X="-2.38003155834" Y="-2.434012386972" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.373985249776" Y="-2.780405172622" />
                  <Point X="-2.352909224226" Y="-3.987849867699" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.399994352022" Y="4.153030704451" />
                  <Point X="-2.258990143214" Y="-3.925095007891" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.305892450982" Y="4.205311811895" />
                  <Point X="-2.165071062702" Y="-3.862340119481" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.211790549943" Y="4.257592919339" />
                  <Point X="-2.071085649532" Y="-3.803385426485" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.115212255029" Y="4.168001516724" />
                  <Point X="-1.976521794203" Y="-3.777569662597" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.018918051622" Y="4.094685705638" />
                  <Point X="-1.881462551853" Y="-3.780134602111" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.923069629689" Y="4.046908698079" />
                  <Point X="-1.786339253577" Y="-3.786369303166" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.827744891364" Y="4.029133504321" />
                  <Point X="-1.691215955301" Y="-3.792604004221" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.733095141903" Y="4.050028396692" />
                  <Point X="-1.595995858413" Y="-3.804384294043" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.638770633185" Y="4.08955631878" />
                  <Point X="-1.500170558669" Y="-3.850836632254" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.545029595509" Y="4.16251127448" />
                  <Point X="-1.403693180481" Y="-3.934646519556" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.457108092815" Y="4.56886716593" />
                  <Point X="-1.307201643884" Y="-4.019267541494" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.362556338988" Y="4.595376224465" />
                  <Point X="-1.210298983712" Y="-4.127441817286" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.268004585162" Y="4.621885283001" />
                  <Point X="-1.104578952911" Y="-4.740762918156" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.173452831336" Y="4.648394341537" />
                  <Point X="-1.009190404913" Y="-4.762193765603" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.07890107751" Y="4.674903400072" />
                  <Point X="-0.918457877505" Y="-4.516881372127" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.984349323978" Y="4.701412475508" />
                  <Point X="-0.829254403431" Y="-4.183969571769" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.889679846153" Y="4.721177130637" />
                  <Point X="-0.740050929357" Y="-3.851057771412" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.794859078627" Y="4.732274404633" />
                  <Point X="-0.650847468692" Y="-3.518145202909" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.700038311101" Y="4.743371678629" />
                  <Point X="-0.55890184257" Y="-3.342331188174" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.605217543575" Y="4.754468952625" />
                  <Point X="-0.46621830864" Y="-3.208791883503" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.510396776048" Y="4.765566226621" />
                  <Point X="-0.372888752247" Y="-3.112263180888" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.415576008522" Y="4.776663500617" />
                  <Point X="-0.278566721504" Y="-3.072593295722" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.317559513861" Y="4.604677689616" />
                  <Point X="-0.184064207762" Y="-3.04326327466" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.216138094184" Y="4.237623855147" />
                  <Point X="-0.08956169402" Y="-3.013933253598" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.119085335634" Y="4.120850449068" />
                  <Point X="0.005170587183" Y="-2.997766601562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.023487901795" Y="4.087452539771" />
                  <Point X="0.100459587196" Y="-3.013494348736" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.071388898765" Y="4.095339683421" />
                  <Point X="0.195991594386" Y="-3.043143967802" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.165523514538" Y="4.145746565023" />
                  <Point X="0.291523602414" Y="-3.072793634891" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.257257604316" Y="4.333679488758" />
                  <Point X="0.387250178231" Y="-3.113590083105" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.346461074547" Y="4.666591509272" />
                  <Point X="0.484065615856" Y="-3.216767382535" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.439520720999" Y="4.778583341988" />
                  <Point X="0.581531292677" Y="-3.357196860592" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.534709199524" Y="4.768614466992" />
                  <Point X="0.680483699335" Y="-3.582801033916" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.629897678048" Y="4.758645591996" />
                  <Point X="0.766714733289" Y="-3.079598253166" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.778155913071" Y="-3.735063003876" />
                  <Point X="0.782119013864" Y="-3.962108896272" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.725086156573" Y="4.748676717" />
                  <Point X="0.8582039189" Y="-2.877634779104" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.82027462847" Y="4.738708221675" />
                  <Point X="0.951844415975" Y="-2.798919856243" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.915672985972" Y="4.716715388099" />
                  <Point X="1.04550303197" Y="-2.721242965615" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.011089560661" Y="4.693678892609" />
                  <Point X="1.139721713248" Y="-2.675652193575" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.10650613535" Y="4.67064239712" />
                  <Point X="1.23450691984" Y="-2.662517635061" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.201922710038" Y="4.647605901631" />
                  <Point X="1.329369020525" Y="-2.653788336123" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.297339284727" Y="4.624569406142" />
                  <Point X="1.424254292562" Y="-2.646386523127" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.392755862042" Y="4.601532760177" />
                  <Point X="1.519589885702" Y="-2.664783588766" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.488246094525" Y="4.574276412469" />
                  <Point X="1.615554760894" Y="-2.719232199089" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.583865940642" Y="4.539594504687" />
                  <Point X="1.711647586002" Y="-2.781011055116" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.679485786758" Y="4.504912596906" />
                  <Point X="1.807845188622" Y="-2.848792610826" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.775105632874" Y="4.470230689124" />
                  <Point X="1.904958027021" Y="-2.969007989205" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.870748510854" Y="4.434229286788" />
                  <Point X="2.002184186436" Y="-3.095715524226" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.96654498389" Y="4.38942842956" />
                  <Point X="2.083593066039" Y="-2.316251705707" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.094062429857" Y="-2.916041157146" />
                  <Point X="2.099410345851" Y="-3.222423059246" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.062341456926" Y="4.344627572331" />
                  <Point X="2.092011216286" Y="2.644848196951" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.100883623032" Y="2.136548354913" />
                  <Point X="2.17620340683" Y="-2.178519168913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.192039028957" Y="-3.085741352964" />
                  <Point X="2.196636505266" Y="-3.349130594266" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.158137929961" Y="4.299826715102" />
                  <Point X="2.18423740768" Y="2.804588638055" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.197458239704" Y="2.047167678648" />
                  <Point X="2.27019180488" Y="-2.119735479551" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.290015632618" Y="-3.25544181004" />
                  <Point X="2.29386266468" Y="-3.475838129286" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.2539414719" Y="4.254620880724" />
                  <Point X="2.276463599073" Y="2.964329079158" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.293380359726" Y="1.995168510421" />
                  <Point X="2.364341375861" Y="-2.070185381225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.387992236279" Y="-3.425142267116" />
                  <Point X="2.391088824095" Y="-3.602545664307" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.349932107351" Y="4.19869646615" />
                  <Point X="2.368689790466" Y="3.124069520261" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.38869128161" Y="1.978184860056" />
                  <Point X="2.458686017724" Y="-2.031810886281" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.48596883994" Y="-3.594842724192" />
                  <Point X="2.48831498351" Y="-3.729253199327" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.445922742803" Y="4.142772051577" />
                  <Point X="2.460915981859" Y="3.283809961364" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.483678807043" Y="1.979728579934" />
                  <Point X="2.553619615413" Y="-2.027177648005" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.5839454436" Y="-3.764543181268" />
                  <Point X="2.585541142925" Y="-3.855960734347" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.541913378255" Y="4.086847637004" />
                  <Point X="2.553142173252" Y="3.443550402467" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.578262772669" Y="2.00439222571" />
                  <Point X="2.648932483534" Y="-2.044272798193" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.681922047261" Y="-3.934243638344" />
                  <Point X="2.68276730234" Y="-3.982668269367" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.637923250258" Y="4.02982116112" />
                  <Point X="2.645368364645" Y="3.603290843571" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.672499898398" Y="2.048926315852" />
                  <Point X="2.744247422395" Y="-2.06148658102" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.734131967383" Y="3.961402855879" />
                  <Point X="2.737594556038" Y="3.763031284674" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.76656639681" Y="2.103235638468" />
                  <Point X="2.839764218589" Y="-2.090264762697" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.860632895222" Y="2.157544961084" />
                  <Point X="2.918904925499" Y="-1.180857417678" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.927396982918" Y="-1.667367061357" />
                  <Point X="2.935735181691" Y="-2.145062149138" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.954699394054" Y="2.211854259572" />
                  <Point X="3.011918290342" Y="-1.066214113312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.024893165644" Y="-1.809544221539" />
                  <Point X="3.031716927206" Y="-2.200477259549" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.048765892905" Y="2.266163557045" />
                  <Point X="3.060291316718" Y="1.605872469044" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.06795624718" Y="1.166748896928" />
                  <Point X="3.10544521611" Y="-0.980992694611" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.121291684951" Y="-1.888836286486" />
                  <Point X="3.12769867272" Y="-2.25589236996" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.142832391756" Y="2.320472854518" />
                  <Point X="3.153205283528" Y="1.726210282883" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.165025269952" Y="1.049043714196" />
                  <Point X="3.199853864851" Y="-0.946285151274" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.217596031136" Y="-1.962733176961" />
                  <Point X="3.223680418234" Y="-2.31130748037" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.236898890606" Y="2.374782151991" />
                  <Point X="3.246921641739" Y="1.800579124163" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.261016906169" Y="0.993061965807" />
                  <Point X="3.294509220361" Y="-0.925711429196" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.313900378496" Y="-2.036630134726" />
                  <Point X="3.319662163749" Y="-2.366722590781" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.330965389457" Y="2.429091449464" />
                  <Point X="3.340680332822" Y="1.872522716829" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.356546407133" Y="0.963555928343" />
                  <Point X="3.371368823198" Y="0.114380280673" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.376775087754" Y="-0.195344408312" />
                  <Point X="3.389231183532" Y="-0.908953657494" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.410204726161" Y="-2.110527109952" />
                  <Point X="3.415643909263" Y="-2.422137701192" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.425031888307" Y="2.483400746937" />
                  <Point X="3.434439023905" Y="1.944466309494" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.451755889075" Y="0.952383768337" />
                  <Point X="3.463622670458" Y="0.272536318251" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.474445663625" Y="-0.347512545024" />
                  <Point X="3.484360226261" Y="-0.915517458035" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.506509073825" Y="-2.184424085178" />
                  <Point X="3.511625654778" Y="-2.477552811603" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.519098387158" Y="2.53771004441" />
                  <Point X="3.528197714988" Y="2.01640990216" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.546657311989" Y="0.958860298229" />
                  <Point X="3.557233664108" Y="0.352941491161" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.570783598391" Y="-0.423333723983" />
                  <Point X="3.57959354391" Y="-0.928055164745" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.60281342149" Y="-2.258321060404" />
                  <Point X="3.607607400292" Y="-2.532967922014" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.613164886008" Y="2.592019341883" />
                  <Point X="3.621956406071" Y="2.088353494825" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.641453939484" Y="0.971340553689" />
                  <Point X="3.651327795591" Y="0.405667716163" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.666695905395" Y="-0.474770704836" />
                  <Point X="3.674826861559" Y="-0.940592871456" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.699117769155" Y="-2.33221803563" />
                  <Point X="3.703589145806" Y="-2.588383032424" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.707231384859" Y="2.646328639356" />
                  <Point X="3.715715097154" Y="2.160297087491" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.736250567124" Y="0.983820800855" />
                  <Point X="3.745816738972" Y="0.435775182716" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.762198315005" Y="-0.502724679641" />
                  <Point X="3.770060179207" Y="-0.953130578167" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.79542211682" Y="-2.406115010856" />
                  <Point X="3.799570891321" Y="-2.643798142835" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.80129788371" Y="2.700637936829" />
                  <Point X="3.809473788237" Y="2.232240680156" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.831047195353" Y="0.996301014232" />
                  <Point X="3.840388889594" Y="0.46111570961" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.857659261818" Y="-0.528303252478" />
                  <Point X="3.865293496856" Y="-0.965668284877" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.891726464484" Y="-2.480011986082" />
                  <Point X="3.895552636835" Y="-2.699213253246" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.89536438256" Y="2.754947234302" />
                  <Point X="3.90323247932" Y="2.304184272821" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.925843823582" Y="1.008781227608" />
                  <Point X="3.934961040216" Y="0.486456236504" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.953120209875" Y="-0.553881896515" />
                  <Point X="3.960526814505" Y="-0.978205991588" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.988030812149" Y="-2.553908961308" />
                  <Point X="3.991534381662" Y="-2.754628324268" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.991156112851" Y="2.71041808877" />
                  <Point X="3.996991170403" Y="2.376127865487" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.020640451812" Y="1.021261440985" />
                  <Point X="4.029533190838" Y="0.511796763397" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.048581158597" Y="-0.579460578692" />
                  <Point X="4.055760132154" Y="-0.990743698298" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.084335159814" Y="-2.627805936534" />
                  <Point X="4.084449983025" Y="-2.634384153912" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.088743296833" Y="2.563027470142" />
                  <Point X="4.090749861486" Y="2.448071458152" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.115437080041" Y="1.033741654361" />
                  <Point X="4.12410534146" Y="0.537137290291" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.14404210732" Y="-0.605039260869" />
                  <Point X="4.150993449803" Y="-1.003281405009" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.210233708271" Y="1.046221867737" />
                  <Point X="4.218677492082" Y="0.562477817184" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.239503056042" Y="-0.630617943045" />
                  <Point X="4.246226767452" Y="-1.01581911172" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.3050303365" Y="1.058702081114" />
                  <Point X="4.313249642704" Y="0.587818344078" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.334964004764" Y="-0.656196625222" />
                  <Point X="4.341460085101" Y="-1.02835681843" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.399826964729" Y="1.07118229449" />
                  <Point X="4.407821793326" Y="0.613158870971" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.430424953487" Y="-0.681775307399" />
                  <Point X="4.436693402749" Y="-1.040894525141" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.494623592959" Y="1.083662507867" />
                  <Point X="4.502393943947" Y="0.638499397865" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.525885902209" Y="-0.707353989575" />
                  <Point X="4.531926720398" Y="-1.053432231851" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.589420221188" Y="1.096142721243" />
                  <Point X="4.596966094569" Y="0.663839924759" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.621346850931" Y="-0.732932671752" />
                  <Point X="4.627160038047" Y="-1.065969938562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.684216849417" Y="1.10862293462" />
                  <Point X="4.691538245191" Y="0.689180451652" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.716807799654" Y="-0.758511353929" />
                  <Point X="4.72239335625" Y="-1.078507677029" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.0001640625" Y="0.001626220703" />
                  <Width Value="9.996463867188" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.810814025879" Y="-4.803303710938" />
                  <Point X="0.471539031982" Y="-3.537112304688" />
                  <Point X="0.464318664551" Y="-3.521544189453" />
                  <Point X="0.325648376465" Y="-3.321746582031" />
                  <Point X="0.300591033936" Y="-3.285643798828" />
                  <Point X="0.274335601807" Y="-3.266399902344" />
                  <Point X="0.059751525879" Y="-3.19980078125" />
                  <Point X="0.020976625443" Y="-3.187766601562" />
                  <Point X="-0.008664908409" Y="-3.187766601562" />
                  <Point X="-0.223249130249" Y="-3.254365478516" />
                  <Point X="-0.262024047852" Y="-3.266399902344" />
                  <Point X="-0.288279144287" Y="-3.285643798828" />
                  <Point X="-0.426949432373" Y="-3.485441162109" />
                  <Point X="-0.45200680542" Y="-3.521544189453" />
                  <Point X="-0.459227142334" Y="-3.537112304688" />
                  <Point X="-0.52053338623" Y="-3.765910400391" />
                  <Point X="-0.84774395752" Y="-4.987077148438" />
                  <Point X="-1.055846435547" Y="-4.94668359375" />
                  <Point X="-1.100246582031" Y="-4.938065429688" />
                  <Point X="-1.349229858398" Y="-4.87400390625" />
                  <Point X="-1.349990966797" Y="-4.861254394531" />
                  <Point X="-1.309150146484" Y="-4.551038574219" />
                  <Point X="-1.309683227539" Y="-4.5347578125" />
                  <Point X="-1.360947265625" Y="-4.277036132813" />
                  <Point X="-1.370210693359" Y="-4.230466308594" />
                  <Point X="-1.386282714844" Y="-4.20262890625" />
                  <Point X="-1.583844604492" Y="-4.02937109375" />
                  <Point X="-1.619543457031" Y="-3.998063964844" />
                  <Point X="-1.649240722656" Y="-3.985762939453" />
                  <Point X="-1.91144934082" Y="-3.968576904297" />
                  <Point X="-1.958829833984" Y="-3.965471435547" />
                  <Point X="-1.989878540039" Y="-3.973791015625" />
                  <Point X="-2.208364746094" Y="-4.119779296875" />
                  <Point X="-2.247844970703" Y="-4.146159179688" />
                  <Point X="-2.259734130859" Y="-4.157294433594" />
                  <Point X="-2.294152832031" Y="-4.202149414062" />
                  <Point X="-2.457095214844" Y="-4.414500488281" />
                  <Point X="-2.790757324219" Y="-4.207905273438" />
                  <Point X="-2.855830810547" Y="-4.16761328125" />
                  <Point X="-3.200602539062" Y="-3.902150634766" />
                  <Point X="-3.228581054688" Y="-3.880608154297" />
                  <Point X="-3.136538085938" Y="-3.721185058594" />
                  <Point X="-2.506033691406" Y="-2.629119628906" />
                  <Point X="-2.499762695312" Y="-2.597593261719" />
                  <Point X="-2.513979736328" Y="-2.568764404297" />
                  <Point X="-2.528673339844" Y="-2.554070800781" />
                  <Point X="-2.531328369141" Y="-2.551415771484" />
                  <Point X="-2.560157226562" Y="-2.537198730469" />
                  <Point X="-2.59168359375" Y="-2.543469726562" />
                  <Point X="-2.789017578125" Y="-2.657400634766" />
                  <Point X="-3.842959228516" Y="-3.265894042969" />
                  <Point X="-4.1102890625" Y="-2.914677734375" />
                  <Point X="-4.161703613281" Y="-2.847129394531" />
                  <Point X="-4.408875976562" Y="-2.432658935547" />
                  <Point X="-4.431020019531" Y="-2.395527099609" />
                  <Point X="-4.26696484375" Y="-2.269643066406" />
                  <Point X="-3.163786865234" Y="-1.423144897461" />
                  <Point X="-3.145822265625" Y="-1.396014282227" />
                  <Point X="-3.139296875" Y="-1.370819580078" />
                  <Point X="-3.138117675781" Y="-1.366266967773" />
                  <Point X="-3.140325927734" Y="-1.334596313477" />
                  <Point X="-3.161158203125" Y="-1.310639526367" />
                  <Point X="-3.183587890625" Y="-1.297438598633" />
                  <Point X="-3.187640869141" Y="-1.295053100586" />
                  <Point X="-3.219529296875" Y="-1.288571044922" />
                  <Point X="-3.468644775391" Y="-1.321367675781" />
                  <Point X="-4.803283691406" Y="-1.497076293945" />
                  <Point X="-4.907284179688" Y="-1.089917602539" />
                  <Point X="-4.927392089844" Y="-1.011195495605" />
                  <Point X="-4.992788574219" Y="-0.553951049805" />
                  <Point X="-4.998395996094" Y="-0.51474206543" />
                  <Point X="-4.814149902344" Y="-0.465373260498" />
                  <Point X="-3.557463867188" Y="-0.128645431519" />
                  <Point X="-3.541895263672" Y="-0.12142477417" />
                  <Point X="-3.518389648438" Y="-0.105110603333" />
                  <Point X="-3.514142333984" Y="-0.102162742615" />
                  <Point X="-3.494898925781" Y="-0.075907470703" />
                  <Point X="-3.487063720703" Y="-0.050662250519" />
                  <Point X="-3.485647949219" Y="-0.046100585938" />
                  <Point X="-3.485647949219" Y="-0.016459506989" />
                  <Point X="-3.493483154297" Y="0.008785713196" />
                  <Point X="-3.494898925781" Y="0.013347376823" />
                  <Point X="-3.514142333984" Y="0.039602649689" />
                  <Point X="-3.537647949219" Y="0.055916820526" />
                  <Point X="-3.557463867188" Y="0.066085334778" />
                  <Point X="-3.784544433594" Y="0.126931251526" />
                  <Point X="-4.998186523438" Y="0.452125793457" />
                  <Point X="-4.930604003906" Y="0.908842346191" />
                  <Point X="-4.91764453125" Y="0.996421447754" />
                  <Point X="-4.786006835938" Y="1.482206298828" />
                  <Point X="-4.773516601562" Y="1.528298706055" />
                  <Point X="-4.654089355469" Y="1.512575805664" />
                  <Point X="-3.753266357422" Y="1.393980102539" />
                  <Point X="-3.731704589844" Y="1.395866699219" />
                  <Point X="-3.679679443359" Y="1.412270141602" />
                  <Point X="-3.670278564453" Y="1.41523425293" />
                  <Point X="-3.651534179688" Y="1.426056396484" />
                  <Point X="-3.639119628906" Y="1.443786499023" />
                  <Point X="-3.618244140625" Y="1.494184082031" />
                  <Point X="-3.614472167969" Y="1.503290771484" />
                  <Point X="-3.610714111328" Y="1.524605712891" />
                  <Point X="-3.616316162109" Y="1.545512084961" />
                  <Point X="-3.641504394531" Y="1.5938984375" />
                  <Point X="-3.646055908203" Y="1.602641723633" />
                  <Point X="-3.659968261719" Y="1.619221801758" />
                  <Point X="-3.790222167969" Y="1.719169189453" />
                  <Point X="-4.476105957031" Y="2.245466308594" />
                  <Point X="-4.210368652344" Y="2.700737548828" />
                  <Point X="-4.160015136719" Y="2.787005615234" />
                  <Point X="-3.811306640625" Y="3.235221191406" />
                  <Point X="-3.774671386719" Y="3.282310791016" />
                  <Point X="-3.712417480469" Y="3.246368408203" />
                  <Point X="-3.159156982422" Y="2.926943359375" />
                  <Point X="-3.138514892578" Y="2.920434814453" />
                  <Point X="-3.066058349609" Y="2.914095703125" />
                  <Point X="-3.052965576172" Y="2.912950195312" />
                  <Point X="-3.031507080078" Y="2.915775146484" />
                  <Point X="-3.013252685547" Y="2.927404296875" />
                  <Point X="-2.961822509766" Y="2.978834472656" />
                  <Point X="-2.952529052734" Y="2.988127685547" />
                  <Point X="-2.940899414062" Y="3.006382080078" />
                  <Point X="-2.93807421875" Y="3.027841308594" />
                  <Point X="-2.944413330078" Y="3.100298095703" />
                  <Point X="-2.945558837891" Y="3.113390869141" />
                  <Point X="-2.952067382812" Y="3.134032958984" />
                  <Point X="-3.009786865234" Y="3.234005859375" />
                  <Point X="-3.307278564453" Y="3.749276611328" />
                  <Point X="-2.840575683594" Y="4.107093261719" />
                  <Point X="-2.752876220703" Y="4.174331542969" />
                  <Point X="-2.203671875" Y="4.479457519531" />
                  <Point X="-2.141548583984" Y="4.513971679688" />
                  <Point X="-2.137196777344" Y="4.508300292969" />
                  <Point X="-1.967826660156" Y="4.287573242188" />
                  <Point X="-1.951246948242" Y="4.273660644531" />
                  <Point X="-1.870603149414" Y="4.2316796875" />
                  <Point X="-1.856030761719" Y="4.22409375" />
                  <Point X="-1.835124633789" Y="4.218491699219" />
                  <Point X="-1.813809570312" Y="4.22225" />
                  <Point X="-1.729813476562" Y="4.25704296875" />
                  <Point X="-1.714635620117" Y="4.263329589844" />
                  <Point X="-1.696905639648" Y="4.275744140625" />
                  <Point X="-1.686083496094" Y="4.29448828125" />
                  <Point X="-1.658744384766" Y="4.381196777344" />
                  <Point X="-1.653804077148" Y="4.396864746094" />
                  <Point X="-1.651917602539" Y="4.418426757812" />
                  <Point X="-1.658479858398" Y="4.468270507812" />
                  <Point X="-1.689137573242" Y="4.701141113281" />
                  <Point X="-1.081626953125" Y="4.871465820312" />
                  <Point X="-0.968094177246" Y="4.903296386719" />
                  <Point X="-0.302290740967" Y="4.98121875" />
                  <Point X="-0.22419960022" Y="4.990358398438" />
                  <Point X="-0.205810073853" Y="4.921727539062" />
                  <Point X="-0.042140380859" Y="4.310903320312" />
                  <Point X="-0.024282119751" Y="4.284176757813" />
                  <Point X="0.006155934334" Y="4.273844238281" />
                  <Point X="0.036594032288" Y="4.284176757813" />
                  <Point X="0.054452251434" Y="4.310903320312" />
                  <Point X="0.084027008057" Y="4.421278320312" />
                  <Point X="0.236648406982" Y="4.990868652344" />
                  <Point X="0.761072753906" Y="4.935947265625" />
                  <Point X="0.860205566406" Y="4.925565429688" />
                  <Point X="1.411041259766" Y="4.792577148438" />
                  <Point X="1.508459472656" Y="4.769057128906" />
                  <Point X="1.867743896484" Y="4.6387421875" />
                  <Point X="1.931033447266" Y="4.615786621094" />
                  <Point X="2.277724365234" Y="4.453650878906" />
                  <Point X="2.338695800781" Y="4.425136230469" />
                  <Point X="2.67361328125" Y="4.230012695313" />
                  <Point X="2.732523681641" Y="4.19569140625" />
                  <Point X="3.04839453125" Y="3.971061523438" />
                  <Point X="3.068740234375" Y="3.956592773438" />
                  <Point X="2.959614990234" Y="3.767581787109" />
                  <Point X="2.229853515625" Y="2.503597900391" />
                  <Point X="2.224852050781" Y="2.491515136719" />
                  <Point X="2.206668212891" Y="2.423516113281" />
                  <Point X="2.202044433594" Y="2.392326660156" />
                  <Point X="2.209134765625" Y="2.333526855469" />
                  <Point X="2.210415771484" Y="2.322901855469" />
                  <Point X="2.218682128906" Y="2.300812255859" />
                  <Point X="2.255065429688" Y="2.247192626953" />
                  <Point X="2.274940429688" Y="2.224203369141" />
                  <Point X="2.328560058594" Y="2.187820068359" />
                  <Point X="2.338249023438" Y="2.181245849609" />
                  <Point X="2.360336669922" Y="2.172980224609" />
                  <Point X="2.419136474609" Y="2.165889892578" />
                  <Point X="2.448664794922" Y="2.165946289062" />
                  <Point X="2.516663818359" Y="2.184130126953" />
                  <Point X="2.541033935547" Y="2.192417480469" />
                  <Point X="2.769433105469" Y="2.324283935547" />
                  <Point X="3.994247802734" Y="3.031430664062" />
                  <Point X="4.167647949219" Y="2.790443847656" />
                  <Point X="4.202593261719" Y="2.741878173828" />
                  <Point X="4.378685058594" Y="2.450884765625" />
                  <Point X="4.387513183594" Y="2.436296142578" />
                  <Point X="4.249415527344" Y="2.330330078125" />
                  <Point X="3.288616210938" Y="1.593082763672" />
                  <Point X="3.279371337891" Y="1.583833374023" />
                  <Point X="3.230432373047" Y="1.519988647461" />
                  <Point X="3.213119384766" Y="1.491499511719" />
                  <Point X="3.194889404297" Y="1.426313842773" />
                  <Point X="3.191595458984" Y="1.41453503418" />
                  <Point X="3.190779541016" Y="1.390965332031" />
                  <Point X="3.205744384766" Y="1.318437988281" />
                  <Point X="3.215646728516" Y="1.287954589844" />
                  <Point X="3.256349365234" Y="1.226088378906" />
                  <Point X="3.263704345703" Y="1.214909301758" />
                  <Point X="3.280948242188" Y="1.198819702148" />
                  <Point X="3.339931884766" Y="1.165616943359" />
                  <Point X="3.368565673828" Y="1.153619628906" />
                  <Point X="3.448315673828" Y="1.143079589844" />
                  <Point X="3.475803710938" Y="1.14117175293" />
                  <Point X="3.692767822266" Y="1.169735595703" />
                  <Point X="4.848975097656" Y="1.32195324707" />
                  <Point X="4.924180175781" Y="1.013033508301" />
                  <Point X="4.939188476562" Y="0.951385375977" />
                  <Point X="4.994680175781" Y="0.594969238281" />
                  <Point X="4.997858398438" Y="0.574556152344" />
                  <Point X="4.842264160156" Y="0.532864868164" />
                  <Point X="3.741167724609" Y="0.237826919556" />
                  <Point X="3.729087158203" Y="0.232819412231" />
                  <Point X="3.650735351562" Y="0.187530578613" />
                  <Point X="3.622264648438" Y="0.166926269531" />
                  <Point X="3.575253417969" Y="0.107022872925" />
                  <Point X="3.566758789062" Y="0.09619858551" />
                  <Point X="3.556985351562" Y="0.074735733032" />
                  <Point X="3.541314941406" Y="-0.007089295864" />
                  <Point X="3.538483154297" Y="-0.040685245514" />
                  <Point X="3.554153564453" Y="-0.122510276794" />
                  <Point X="3.556985351562" Y="-0.137295822144" />
                  <Point X="3.566759277344" Y="-0.158759140015" />
                  <Point X="3.613770263672" Y="-0.218662384033" />
                  <Point X="3.636576904297" Y="-0.241906967163" />
                  <Point X="3.714928955078" Y="-0.287195800781" />
                  <Point X="3.741167724609" Y="-0.300387023926" />
                  <Point X="3.940133544922" Y="-0.353699676514" />
                  <Point X="4.998067871094" Y="-0.637172363281" />
                  <Point X="4.956811523438" Y="-0.910817565918" />
                  <Point X="4.948431640625" Y="-0.966398742676" />
                  <Point X="4.877337402344" Y="-1.277945068359" />
                  <Point X="4.874545898438" Y="-1.290178344727" />
                  <Point X="4.688944824219" Y="-1.265743530273" />
                  <Point X="3.411982177734" Y="-1.097628295898" />
                  <Point X="3.3948359375" Y="-1.098341308594" />
                  <Point X="3.241058837891" Y="-1.131765380859" />
                  <Point X="3.213271728516" Y="-1.137805053711" />
                  <Point X="3.1854453125" Y="-1.154697143555" />
                  <Point X="3.092496826172" Y="-1.266485351562" />
                  <Point X="3.075701171875" Y="-1.286685180664" />
                  <Point X="3.064357910156" Y="-1.314070556641" />
                  <Point X="3.051036132812" Y="-1.458841186523" />
                  <Point X="3.04862890625" Y="-1.485000854492" />
                  <Point X="3.056360351562" Y="-1.516621582031" />
                  <Point X="3.141462890625" Y="-1.648993041992" />
                  <Point X="3.156840576172" Y="-1.672912353516" />
                  <Point X="3.168460693359" Y="-1.685540649414" />
                  <Point X="3.353102294922" Y="-1.827221191406" />
                  <Point X="4.339074707031" Y="-2.583784179688" />
                  <Point X="4.227753417969" Y="-2.763918212891" />
                  <Point X="4.204133300781" Y="-2.802139404297" />
                  <Point X="4.057105224609" Y="-3.011046386719" />
                  <Point X="4.056688476562" Y="-3.011638427734" />
                  <Point X="3.890761962891" Y="-2.915840576172" />
                  <Point X="2.753454589844" Y="-2.259215576172" />
                  <Point X="2.737340820313" Y="-2.253312744141" />
                  <Point X="2.554321533203" Y="-2.220259765625" />
                  <Point X="2.521250488281" Y="-2.214287109375" />
                  <Point X="2.489077636719" Y="-2.219244873047" />
                  <Point X="2.337033447266" Y="-2.299264404297" />
                  <Point X="2.309559570312" Y="-2.313723876953" />
                  <Point X="2.288599609375" Y="-2.334683837891" />
                  <Point X="2.208579833984" Y="-2.486727783203" />
                  <Point X="2.194120605469" Y="-2.514201904297" />
                  <Point X="2.189163085938" Y="-2.546374511719" />
                  <Point X="2.222216064453" Y="-2.729393798828" />
                  <Point X="2.228188720703" Y="-2.76246484375" />
                  <Point X="2.234091552734" Y="-2.778578613281" />
                  <Point X="2.352742431641" Y="-2.984087646484" />
                  <Point X="2.986673583984" Y="-4.082088623047" />
                  <Point X="2.863330078125" Y="-4.170190429687" />
                  <Point X="2.835305664062" Y="-4.190207519531" />
                  <Point X="2.679775878906" Y="-4.290879394531" />
                  <Point X="2.551048095703" Y="-4.123117675781" />
                  <Point X="1.683177490234" Y="-2.992087402344" />
                  <Point X="1.670549194336" Y="-2.980467529297" />
                  <Point X="1.49004284668" Y="-2.864418457031" />
                  <Point X="1.45742578125" Y="-2.843448730469" />
                  <Point X="1.4258046875" Y="-2.835717041016" />
                  <Point X="1.228390380859" Y="-2.853883300781" />
                  <Point X="1.192717773438" Y="-2.857166015625" />
                  <Point X="1.165332885742" Y="-2.868509033203" />
                  <Point X="1.012894592285" Y="-2.995256591797" />
                  <Point X="0.985349243164" Y="-3.018159423828" />
                  <Point X="0.968456726074" Y="-3.045986328125" />
                  <Point X="0.922878417969" Y="-3.255682373047" />
                  <Point X="0.91464251709" Y="-3.29357421875" />
                  <Point X="0.91392956543" Y="-3.310720214844" />
                  <Point X="0.947554260254" Y="-3.566125" />
                  <Point X="1.127642211914" Y="-4.934028320312" />
                  <Point X="1.020861572266" Y="-4.957434570312" />
                  <Point X="0.994347717285" Y="-4.963246582031" />
                  <Point X="0.860200317383" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#202" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.163669777233" Y="4.965948429041" Z="2.15" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="2.15" />
                  <Point X="-0.314258882436" Y="5.062160073588" Z="2.15" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="2.15" />
                  <Point X="-1.101280782759" Y="4.950889373162" Z="2.15" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="2.15" />
                  <Point X="-1.714207942376" Y="4.493024324154" Z="2.15" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="2.15" />
                  <Point X="-1.712586016317" Y="4.427512530203" Z="2.15" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="2.15" />
                  <Point X="-1.755103047005" Y="4.334517154784" Z="2.15" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="2.15" />
                  <Point X="-1.853671243615" Y="4.30731043692" Z="2.15" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="2.15" />
                  <Point X="-2.103684979599" Y="4.570018414861" Z="2.15" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="2.15" />
                  <Point X="-2.234110888084" Y="4.554444888864" Z="2.15" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="2.15" />
                  <Point X="-2.877153766216" Y="4.178052865458" Z="2.15" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="2.15" />
                  <Point X="-3.059244135045" Y="3.240285921222" Z="2.15" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="2.15" />
                  <Point X="-3.000379207578" Y="3.127220155028" Z="2.15" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="2.15" />
                  <Point X="-3.003333343452" Y="3.045470224434" Z="2.15" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="2.15" />
                  <Point X="-3.067856310579" Y="2.995185491595" Z="2.15" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="2.15" />
                  <Point X="-3.693573427075" Y="3.320949813701" Z="2.15" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="2.15" />
                  <Point X="-3.856926107548" Y="3.297203623759" Z="2.15" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="2.15" />
                  <Point X="-4.259706916488" Y="2.757222384293" Z="2.15" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="2.15" />
                  <Point X="-3.826816192525" Y="1.710781445037" Z="2.15" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="2.15" />
                  <Point X="-3.692010804545" Y="1.602090797978" Z="2.15" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="2.15" />
                  <Point X="-3.670594564135" Y="1.544597651394" Z="2.15" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="2.15" />
                  <Point X="-3.700870770064" Y="1.491236032855" Z="2.15" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="2.15" />
                  <Point X="-4.653719074286" Y="1.59342816896" Z="2.15" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="2.15" />
                  <Point X="-4.84042189999" Y="1.526563860792" Z="2.15" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="2.15" />
                  <Point X="-4.986221491439" Y="0.947441340002" Z="2.15" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="2.15" />
                  <Point X="-3.803641220994" Y="0.10991506566" Z="2.15" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="2.15" />
                  <Point X="-3.572313344088" Y="0.046121099738" Z="2.15" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="2.15" />
                  <Point X="-3.54739203789" Y="0.025245197075" Z="2.15" />
                  <Point X="-3.539556741714" Y="0" Z="2.15" />
                  <Point X="-3.540972563245" Y="-0.004561753986" Z="2.15" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="2.15" />
                  <Point X="-3.553055251035" Y="-0.032754883395" Z="2.15" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="2.15" />
                  <Point X="-4.833246567779" Y="-0.385796991096" Z="2.15" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="2.15" />
                  <Point X="-5.048440895264" Y="-0.529749786357" Z="2.15" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="2.15" />
                  <Point X="-4.961884811321" Y="-1.071011651928" Z="2.15" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="2.15" />
                  <Point X="-3.468274258788" Y="-1.339659962431" Z="2.15" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="2.15" />
                  <Point X="-3.215105906256" Y="-1.309248716557" Z="2.15" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="2.15" />
                  <Point X="-3.193855427279" Y="-1.327002438382" Z="2.15" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="2.15" />
                  <Point X="-4.303558679517" Y="-2.198695292367" Z="2.15" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="2.15" />
                  <Point X="-4.457975395942" Y="-2.426988431973" Z="2.15" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="2.15" />
                  <Point X="-4.156231102297" Y="-2.913680794241" Z="2.15" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="2.15" />
                  <Point X="-2.770173428233" Y="-2.66942176647" Z="2.15" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="2.15" />
                  <Point X="-2.570184422656" Y="-2.558146046475" Z="2.15" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="2.15" />
                  <Point X="-3.185995246699" Y="-3.664904954259" Z="2.15" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="2.15" />
                  <Point X="-3.237262384408" Y="-3.910487813612" Z="2.15" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="2.15" />
                  <Point X="-2.823234752924" Y="-4.219135963338" Z="2.15" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="2.15" />
                  <Point X="-2.260641021423" Y="-4.201307544716" Z="2.15" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="2.15" />
                  <Point X="-2.186742267362" Y="-4.130072441285" Z="2.15" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="2.15" />
                  <Point X="-1.920875453658" Y="-3.987189855991" Z="2.15" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="2.15" />
                  <Point X="-1.622967837146" Y="-4.035683170517" Z="2.15" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="2.15" />
                  <Point X="-1.416142659744" Y="-4.255510314051" Z="2.15" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="2.15" />
                  <Point X="-1.405719224011" Y="-4.823447926743" Z="2.15" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="2.15" />
                  <Point X="-1.367844575027" Y="-4.891146833279" Z="2.15" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="2.15" />
                  <Point X="-1.071529596547" Y="-4.964487536139" Z="2.15" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="2.15" />
                  <Point X="-0.478393061928" Y="-3.747571253982" Z="2.15" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="2.15" />
                  <Point X="-0.392029353374" Y="-3.482670104242" Z="2.15" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="2.15" />
                  <Point X="-0.214584171196" Y="-3.270838376283" Z="2.15" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="2.15" />
                  <Point X="0.038774908164" Y="-3.216273669005" Z="2.15" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="2.15" />
                  <Point X="0.278416506058" Y="-3.318975651526" Z="2.15" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="2.15" />
                  <Point X="0.756361798474" Y="-4.784964775834" Z="2.15" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="2.15" />
                  <Point X="0.845268226093" Y="-5.008749011715" Z="2.15" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="2.15" />
                  <Point X="1.025411460958" Y="-4.974995027378" Z="2.15" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="2.15" />
                  <Point X="0.990970496017" Y="-3.528318710698" Z="2.15" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="2.15" />
                  <Point X="0.965581691113" Y="-3.235022060063" Z="2.15" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="2.15" />
                  <Point X="1.038705319811" Y="-3.002422996756" Z="2.15" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="2.15" />
                  <Point X="1.226816108384" Y="-2.872392837073" Z="2.15" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="2.15" />
                  <Point X="1.456847569832" Y="-2.875196379468" Z="2.15" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="2.15" />
                  <Point X="2.505224313087" Y="-4.122275785459" Z="2.15" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="2.15" />
                  <Point X="2.691924840023" Y="-4.307311056403" Z="2.15" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="2.15" />
                  <Point X="2.886235342472" Y="-4.17959729068" Z="2.15" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="2.15" />
                  <Point X="2.3898876448" Y="-2.92780766051" Z="2.15" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="2.15" />
                  <Point X="2.265264262037" Y="-2.689227429196" Z="2.15" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="2.15" />
                  <Point X="2.246670503243" Y="-2.478734157651" Z="2.15" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="2.15" />
                  <Point X="2.354164220721" Y="-2.312230806724" Z="2.15" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="2.15" />
                  <Point X="2.539279374316" Y="-2.238183747454" Z="2.15" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="2.15" />
                  <Point X="3.859605936597" Y="-2.927861855475" Z="2.15" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="2.15" />
                  <Point X="4.091837447857" Y="-3.008543650596" Z="2.15" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="2.15" />
                  <Point X="4.264130559702" Y="-2.758923394349" Z="2.15" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="2.15" />
                  <Point X="3.377384313066" Y="-1.75627344523" Z="2.15" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="2.15" />
                  <Point X="3.177364985657" Y="-1.590673811254" Z="2.15" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="2.15" />
                  <Point X="3.094669716887" Y="-1.432142766789" Z="2.15" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="2.15" />
                  <Point X="3.124787051296" Y="-1.267172345715" Z="2.15" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="2.15" />
                  <Point X="3.245522732311" Y="-1.149344519326" Z="2.15" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="2.15" />
                  <Point X="4.676262312617" Y="-1.284035707983" Z="2.15" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="2.15" />
                  <Point X="4.919928438168" Y="-1.257789143722" Z="2.15" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="2.15" />
                  <Point X="5.000097077788" Y="-0.886991527463" Z="2.15" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="2.15" />
                  <Point X="3.946918535001" Y="-0.274123369331" Z="2.15" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="2.15" />
                  <Point X="3.733794666799" Y="-0.212627040107" Z="2.15" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="2.15" />
                  <Point X="3.646947894974" Y="-0.156513810802" Z="2.15" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="2.15" />
                  <Point X="3.597105117137" Y="-0.081824937249" Z="2.15" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="2.15" />
                  <Point X="3.584266333288" Y="0.014785593935" Z="2.15" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="2.15" />
                  <Point X="3.608431543426" Y="0.107434927773" Z="2.15" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="2.15" />
                  <Point X="3.669600747553" Y="0.175521804135" Z="2.15" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="2.15" />
                  <Point X="4.849049282305" Y="0.515848563438" Z="2.15" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="2.15" />
                  <Point X="5.037929222781" Y="0.633941352173" Z="2.15" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="2.15" />
                  <Point X="4.966605402464" Y="1.056140654983" Z="2.15" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="2.15" />
                  <Point X="3.680085390874" Y="1.250587941347" Z="2.15" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="2.15" />
                  <Point X="3.448710735884" Y="1.223928629826" Z="2.15" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="2.15" />
                  <Point X="3.358302367619" Y="1.240468251649" Z="2.15" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="2.15" />
                  <Point X="3.291963665304" Y="1.284850135515" Z="2.15" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="2.15" />
                  <Point X="3.248556879639" Y="1.359821929941" Z="2.15" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="2.15" />
                  <Point X="3.236886129982" Y="1.444128105274" Z="2.15" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="2.15" />
                  <Point X="3.263959160006" Y="1.520850320099" Z="2.15" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="2.15" />
                  <Point X="4.273697465438" Y="2.321942313252" Z="2.15" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="2.15" />
                  <Point X="4.415306254986" Y="2.508050962422" Z="2.15" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="2.15" />
                  <Point X="4.202077570637" Y="2.850927106024" Z="2.15" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="2.15" />
                  <Point X="2.738277167922" Y="2.398865127514" Z="2.15" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="2.15" />
                  <Point X="2.497590645751" Y="2.263712990604" Z="2.15" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="2.15" />
                  <Point X="2.418966645278" Y="2.246810354269" Z="2.15" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="2.15" />
                  <Point X="2.350477813669" Y="2.260474892056" Z="2.15" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="2.15" />
                  <Point X="2.290283708179" Y="2.306547046713" Z="2.15" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="2.15" />
                  <Point X="2.25261934849" Y="2.370791798555" Z="2.15" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="2.15" />
                  <Point X="2.248814952054" Y="2.441878930131" Z="2.15" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="2.15" />
                  <Point X="2.996760225345" Y="3.773861914033" Z="2.15" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="2.15" />
                  <Point X="3.071215645899" Y="4.043088649953" Z="2.15" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="2.15" />
                  <Point X="2.692627146094" Y="4.304495408114" Z="2.15" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="2.15" />
                  <Point X="2.292749977774" Y="4.530222354808" Z="2.15" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="2.15" />
                  <Point X="1.878636809431" Y="4.717025488674" Z="2.15" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="2.15" />
                  <Point X="1.416619883669" Y="4.872460511231" Z="2.15" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="2.15" />
                  <Point X="0.760124627852" Y="5.01695554937" Z="2.15" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="2.15" />
                  <Point X="0.02957478432" Y="4.46549907691" Z="2.15" />
                  <Point X="0" Y="4.355124473572" Z="2.15" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>