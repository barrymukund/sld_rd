<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#137" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="940" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="0" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000475830078" Y="0.004715576172" />
                  <Width Value="9.783896972656" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.642356811523" Y="-3.807561035156" />
                  <Point X="0.563302062988" Y="-3.512524414063" />
                  <Point X="0.557721191406" Y="-3.497141845703" />
                  <Point X="0.54236315918" Y="-3.467377441406" />
                  <Point X="0.510051574707" Y="-3.420822265625" />
                  <Point X="0.378635528564" Y="-3.231477050781" />
                  <Point X="0.356751586914" Y="-3.209021484375" />
                  <Point X="0.330496398926" Y="-3.189777587891" />
                  <Point X="0.302495178223" Y="-3.175669189453" />
                  <Point X="0.252494812012" Y="-3.160150878906" />
                  <Point X="0.049136188507" Y="-3.097035888672" />
                  <Point X="0.020976791382" Y="-3.092766601562" />
                  <Point X="-0.00866468811" Y="-3.092766601562" />
                  <Point X="-0.036824237823" Y="-3.097035888672" />
                  <Point X="-0.086824760437" Y="-3.112554199219" />
                  <Point X="-0.290183380127" Y="-3.175669189453" />
                  <Point X="-0.31818460083" Y="-3.189777587891" />
                  <Point X="-0.344439788818" Y="-3.209021484375" />
                  <Point X="-0.366323577881" Y="-3.231476806641" />
                  <Point X="-0.398635192871" Y="-3.278031738281" />
                  <Point X="-0.530051208496" Y="-3.467377197266" />
                  <Point X="-0.538189025879" Y="-3.481573730469" />
                  <Point X="-0.550990112305" Y="-3.512524414063" />
                  <Point X="-0.872516479492" Y="-4.712477539062" />
                  <Point X="-0.916584472656" Y="-4.87694140625" />
                  <Point X="-1.079344116211" Y="-4.845349121094" />
                  <Point X="-1.133470947266" Y="-4.831422851562" />
                  <Point X="-1.24641796875" Y="-4.802362792969" />
                  <Point X="-1.224479248047" Y="-4.635722167969" />
                  <Point X="-1.214963012695" Y="-4.563438476562" />
                  <Point X="-1.214201171875" Y="-4.5479296875" />
                  <Point X="-1.216508666992" Y="-4.516224121094" />
                  <Point X="-1.228453735352" Y="-4.456171875" />
                  <Point X="-1.277036132812" Y="-4.211932617188" />
                  <Point X="-1.287938354492" Y="-4.182966308594" />
                  <Point X="-1.304010375977" Y="-4.15512890625" />
                  <Point X="-1.32364453125" Y="-4.131204101562" />
                  <Point X="-1.369678833008" Y="-4.090833251953" />
                  <Point X="-1.556905517578" Y="-3.926639404297" />
                  <Point X="-1.583188476562" Y="-3.910295410156" />
                  <Point X="-1.612885742188" Y="-3.897994384766" />
                  <Point X="-1.64302722168" Y="-3.890966308594" />
                  <Point X="-1.704124755859" Y="-3.886961669922" />
                  <Point X="-1.952616333008" Y="-3.870674804688" />
                  <Point X="-1.98341796875" Y="-3.873708496094" />
                  <Point X="-2.014466674805" Y="-3.882028076172" />
                  <Point X="-2.042657714844" Y="-3.894801513672" />
                  <Point X="-2.093567626953" Y="-3.928818359375" />
                  <Point X="-2.300624267578" Y="-4.067169677734" />
                  <Point X="-2.312785644531" Y="-4.076821533203" />
                  <Point X="-2.335102783203" Y="-4.099461914062" />
                  <Point X="-2.480149169922" Y="-4.288490234375" />
                  <Point X="-2.801708740234" Y="-4.089387939453" />
                  <Point X="-2.876663574219" Y="-4.031675292969" />
                  <Point X="-3.104721923828" Y="-3.856077880859" />
                  <Point X="-2.57067578125" Y="-2.931083007812" />
                  <Point X="-2.423761230469" Y="-2.676619628906" />
                  <Point X="-2.412859130859" Y="-2.647653320312" />
                  <Point X="-2.406588134766" Y="-2.616126953125" />
                  <Point X="-2.405575439453" Y="-2.585192871094" />
                  <Point X="-2.414560058594" Y="-2.555575195312" />
                  <Point X="-2.428777099609" Y="-2.526746337891" />
                  <Point X="-2.4468046875" Y="-2.501589355469" />
                  <Point X="-2.464153320312" Y="-2.484240722656" />
                  <Point X="-2.489310302734" Y="-2.466213134766" />
                  <Point X="-2.518139160156" Y="-2.45199609375" />
                  <Point X="-2.547756835938" Y="-2.443011474609" />
                  <Point X="-2.578690917969" Y="-2.444024169922" />
                  <Point X="-2.610217285156" Y="-2.450295166016" />
                  <Point X="-2.63918359375" Y="-2.461197265625" />
                  <Point X="-3.674119384766" Y="-3.058717773438" />
                  <Point X="-3.818024414062" Y="-3.141801269531" />
                  <Point X="-4.082860595703" Y="-2.793860595703" />
                  <Point X="-4.136594238281" Y="-2.703757080078" />
                  <Point X="-4.306142578125" Y="-2.419450439453" />
                  <Point X="-3.363007324219" Y="-1.695756958008" />
                  <Point X="-3.105954589844" Y="-1.498513549805" />
                  <Point X="-3.083062744141" Y="-1.475877441406" />
                  <Point X="-3.064126220703" Y="-1.445848876953" />
                  <Point X="-3.055892089844" Y="-1.42491027832" />
                  <Point X="-3.0523359375" Y="-1.413962036133" />
                  <Point X="-3.046151855469" Y="-1.390085205078" />
                  <Point X="-3.042037597656" Y="-1.358602661133" />
                  <Point X="-3.042736816406" Y="-1.335734130859" />
                  <Point X="-3.048883300781" Y="-1.313695922852" />
                  <Point X="-3.060888427734" Y="-1.284713256836" />
                  <Point X="-3.073389892578" Y="-1.263104248047" />
                  <Point X="-3.091088867188" Y="-1.245497802734" />
                  <Point X="-3.108817382812" Y="-1.231992553711" />
                  <Point X="-3.11819921875" Y="-1.225690917969" />
                  <Point X="-3.139455566406" Y="-1.213180419922" />
                  <Point X="-3.168718505859" Y="-1.201956420898" />
                  <Point X="-3.200605957031" Y="-1.195474853516" />
                  <Point X="-3.231929199219" Y="-1.194383789062" />
                  <Point X="-4.5384375" Y="-1.366388916016" />
                  <Point X="-4.732102050781" Y="-1.391885375977" />
                  <Point X="-4.834077636719" Y="-0.992654846191" />
                  <Point X="-4.848293945312" Y="-0.893254882812" />
                  <Point X="-4.892424316406" Y="-0.584698181152" />
                  <Point X="-3.825697753906" Y="-0.298869720459" />
                  <Point X="-3.532875976562" Y="-0.220408340454" />
                  <Point X="-3.513389160156" Y="-0.212802581787" />
                  <Point X="-3.492343994141" Y="-0.201780776978" />
                  <Point X="-3.482251220703" Y="-0.195667877197" />
                  <Point X="-3.459975341797" Y="-0.180207107544" />
                  <Point X="-3.436020996094" Y="-0.158681259155" />
                  <Point X="-3.415647705078" Y="-0.129531890869" />
                  <Point X="-3.406396728516" Y="-0.10891191864" />
                  <Point X="-3.402342529297" Y="-0.098184738159" />
                  <Point X="-3.394917236328" Y="-0.074260139465" />
                  <Point X="-3.389474365234" Y="-0.045520904541" />
                  <Point X="-3.3901875" Y="-0.012943037987" />
                  <Point X="-3.394260253906" Y="0.007760263443" />
                  <Point X="-3.396742919922" Y="0.017582620621" />
                  <Point X="-3.404168212891" Y="0.041507064819" />
                  <Point X="-3.417484130859" Y="0.070830543518" />
                  <Point X="-3.439335693359" Y="0.099200927734" />
                  <Point X="-3.456631347656" Y="0.114520072937" />
                  <Point X="-3.465452880859" Y="0.121448852539" />
                  <Point X="-3.487728759766" Y="0.136909469604" />
                  <Point X="-3.501925292969" Y="0.145047103882" />
                  <Point X="-3.532875976562" Y="0.157848342896" />
                  <Point X="-4.7238203125" Y="0.476960968018" />
                  <Point X="-4.89181640625" Y="0.521975402832" />
                  <Point X="-4.824488769531" Y="0.97697064209" />
                  <Point X="-4.795869140625" Y="1.082586425781" />
                  <Point X="-4.703551269531" Y="1.423267822266" />
                  <Point X="-3.975568115234" Y="1.327427124023" />
                  <Point X="-3.765666259766" Y="1.29979284668" />
                  <Point X="-3.744985839844" Y="1.299341674805" />
                  <Point X="-3.723424072266" Y="1.301228271484" />
                  <Point X="-3.703137695312" Y="1.305263671875" />
                  <Point X="-3.691015380859" Y="1.309085693359" />
                  <Point X="-3.641711669922" Y="1.324631225586" />
                  <Point X="-3.622778320312" Y="1.332962036133" />
                  <Point X="-3.604034179688" Y="1.343784057617" />
                  <Point X="-3.587353271484" Y="1.356015136719" />
                  <Point X="-3.57371484375" Y="1.37156652832" />
                  <Point X="-3.561300292969" Y="1.389296508789" />
                  <Point X="-3.5513515625" Y="1.407430419922" />
                  <Point X="-3.546487304688" Y="1.419173461914" />
                  <Point X="-3.526704101563" Y="1.466934570312" />
                  <Point X="-3.520915527344" Y="1.486794189453" />
                  <Point X="-3.517157226562" Y="1.508109130859" />
                  <Point X="-3.5158046875" Y="1.528749389648" />
                  <Point X="-3.518951171875" Y="1.549192871094" />
                  <Point X="-3.524552978516" Y="1.570099365234" />
                  <Point X="-3.532049560547" Y="1.589377441406" />
                  <Point X="-3.537918701172" Y="1.600652099609" />
                  <Point X="-3.561789306641" Y="1.646507080078" />
                  <Point X="-3.573281494141" Y="1.663706298828" />
                  <Point X="-3.587194091797" Y="1.680286743164" />
                  <Point X="-3.602135986328" Y="1.694590332031" />
                  <Point X="-4.285264160156" Y="2.2187734375" />
                  <Point X="-4.351859863281" Y="2.269874267578" />
                  <Point X="-4.349174804688" Y="2.274474853516" />
                  <Point X="-4.081153808594" Y="2.733659423828" />
                  <Point X="-4.005346923828" Y="2.831098388672" />
                  <Point X="-3.750504882813" Y="3.158661865234" />
                  <Point X="-3.335572753906" Y="2.919100585938" />
                  <Point X="-3.206656982422" Y="2.844670898438" />
                  <Point X="-3.187724609375" Y="2.836340332031" />
                  <Point X="-3.167082519531" Y="2.829831787109" />
                  <Point X="-3.146794433594" Y="2.825796386719" />
                  <Point X="-3.129911132812" Y="2.824319335938" />
                  <Point X="-3.061245117188" Y="2.818311767578" />
                  <Point X="-3.040565185547" Y="2.818762939453" />
                  <Point X="-3.019106445312" Y="2.821588134766" />
                  <Point X="-2.999015136719" Y="2.826504394531" />
                  <Point X="-2.980463867188" Y="2.835652832031" />
                  <Point X="-2.962209472656" Y="2.847281982422" />
                  <Point X="-2.946077392578" Y="2.860229492188" />
                  <Point X="-2.934093505859" Y="2.872213378906" />
                  <Point X="-2.885353759766" Y="2.920952880859" />
                  <Point X="-2.872406738281" Y="2.937084228516" />
                  <Point X="-2.86077734375" Y="2.955338623047" />
                  <Point X="-2.851628662109" Y="2.973890136719" />
                  <Point X="-2.846712158203" Y="2.993981933594" />
                  <Point X="-2.843886962891" Y="3.015440917969" />
                  <Point X="-2.843435791016" Y="3.036120849609" />
                  <Point X="-2.844912841797" Y="3.053004150391" />
                  <Point X="-2.850920410156" Y="3.121670410156" />
                  <Point X="-2.854955810547" Y="3.141958496094" />
                  <Point X="-2.861464355469" Y="3.162600585938" />
                  <Point X="-2.869794921875" Y="3.181532958984" />
                  <Point X="-3.172510009766" Y="3.705850585938" />
                  <Point X="-3.183333007812" Y="3.724596435547" />
                  <Point X="-3.167430175781" Y="3.7367890625" />
                  <Point X="-2.700625976563" Y="4.094683349609" />
                  <Point X="-2.581234375" Y="4.161015136719" />
                  <Point X="-2.167036621094" Y="4.391134277344" />
                  <Point X="-2.082660644531" Y="4.281173339844" />
                  <Point X="-2.04319543457" Y="4.229741210938" />
                  <Point X="-2.028892700195" Y="4.214799804688" />
                  <Point X="-2.012312866211" Y="4.200887207031" />
                  <Point X="-1.995112426758" Y="4.189394042969" />
                  <Point X="-1.976321533203" Y="4.179612304688" />
                  <Point X="-1.899896240234" Y="4.139827148438" />
                  <Point X="-1.88061730957" Y="4.132330566406" />
                  <Point X="-1.85971081543" Y="4.126729003906" />
                  <Point X="-1.839267822266" Y="4.123582519531" />
                  <Point X="-1.818628417969" Y="4.124935546875" />
                  <Point X="-1.797313110352" Y="4.128693847656" />
                  <Point X="-1.777452392578" Y="4.134482421875" />
                  <Point X="-1.757880371094" Y="4.14258984375" />
                  <Point X="-1.678278442383" Y="4.175562011719" />
                  <Point X="-1.660145141602" Y="4.185510742187" />
                  <Point X="-1.642415405273" Y="4.197925292969" />
                  <Point X="-1.626863647461" Y="4.211563964844" />
                  <Point X="-1.614632568359" Y="4.228245117188" />
                  <Point X="-1.603810791016" Y="4.246989257813" />
                  <Point X="-1.59548059082" Y="4.265920898438" />
                  <Point X="-1.589110107422" Y="4.286125" />
                  <Point X="-1.563201171875" Y="4.368297363281" />
                  <Point X="-1.559165649414" Y="4.388584960938" />
                  <Point X="-1.557279174805" Y="4.410146484375" />
                  <Point X="-1.55773034668" Y="4.430826660156" />
                  <Point X="-1.584201904297" Y="4.6318984375" />
                  <Point X="-1.553942626953" Y="4.640382324219" />
                  <Point X="-0.949638427734" Y="4.809808105469" />
                  <Point X="-0.804904174805" Y="4.826747070312" />
                  <Point X="-0.294710754395" Y="4.886458007812" />
                  <Point X="-0.172040039062" Y="4.42864453125" />
                  <Point X="-0.133903305054" Y="4.286315429688" />
                  <Point X="-0.121129844666" Y="4.258124023438" />
                  <Point X="-0.10327155304" Y="4.231397460938" />
                  <Point X="-0.082114555359" Y="4.20880859375" />
                  <Point X="-0.054819446564" Y="4.19421875" />
                  <Point X="-0.024381370544" Y="4.183886230469" />
                  <Point X="0.006155916214" Y="4.178844238281" />
                  <Point X="0.03669316864" Y="4.183886230469" />
                  <Point X="0.067131248474" Y="4.19421875" />
                  <Point X="0.094426452637" Y="4.20880859375" />
                  <Point X="0.115583503723" Y="4.231397460938" />
                  <Point X="0.133441650391" Y="4.258124023438" />
                  <Point X="0.146215255737" Y="4.286315429688" />
                  <Point X="0.301322906494" Y="4.865185546875" />
                  <Point X="0.307419189453" Y="4.887937011719" />
                  <Point X="0.316387023926" Y="4.886998046875" />
                  <Point X="0.844041931152" Y="4.831738769531" />
                  <Point X="0.963787536621" Y="4.802828613281" />
                  <Point X="1.481028198242" Y="4.677950195312" />
                  <Point X="1.557779907227" Y="4.650112304688" />
                  <Point X="1.894643554688" Y="4.527929199219" />
                  <Point X="1.970016235352" Y="4.4926796875" />
                  <Point X="2.294576660156" Y="4.340893554688" />
                  <Point X="2.367418945312" Y="4.298455566406" />
                  <Point X="2.680977050781" Y="4.115775878906" />
                  <Point X="2.749651855469" Y="4.066937988281" />
                  <Point X="2.943260009766" Y="3.929254638672" />
                  <Point X="2.317623535156" Y="2.845620361328" />
                  <Point X="2.147581054688" Y="2.551097900391" />
                  <Point X="2.139660400391" Y="2.533433837891" />
                  <Point X="2.130421875" Y="2.505506103516" />
                  <Point X="2.12883984375" Y="2.500212158203" />
                  <Point X="2.111607177734" Y="2.435770263672" />
                  <Point X="2.10838671875" Y="2.410312011719" />
                  <Point X="2.108701171875" Y="2.377708740234" />
                  <Point X="2.109379882812" Y="2.367252197266" />
                  <Point X="2.116099121094" Y="2.311528320312" />
                  <Point X="2.121442138672" Y="2.289604980469" />
                  <Point X="2.129708251953" Y="2.267516357422" />
                  <Point X="2.140071289062" Y="2.247470703125" />
                  <Point X="2.148549072266" Y="2.234976806641" />
                  <Point X="2.183029052734" Y="2.184162109375" />
                  <Point X="2.200353515625" Y="2.164916259766" />
                  <Point X="2.226147460938" Y="2.143138183594" />
                  <Point X="2.234093017578" Y="2.137114501953" />
                  <Point X="2.284907714844" Y="2.102634765625" />
                  <Point X="2.304952880859" Y="2.092271972656" />
                  <Point X="2.327041259766" Y="2.084006103516" />
                  <Point X="2.348963867188" Y="2.078663330078" />
                  <Point X="2.362664794922" Y="2.077011230469" />
                  <Point X="2.418388671875" Y="2.070291748047" />
                  <Point X="2.444810791016" Y="2.070808105469" />
                  <Point X="2.479557861328" Y="2.0763828125" />
                  <Point X="2.48905078125" Y="2.078408203125" />
                  <Point X="2.553492675781" Y="2.095640869141" />
                  <Point X="2.565283447266" Y="2.099638183594" />
                  <Point X="2.588533935547" Y="2.110145019531" />
                  <Point X="3.786394775391" Y="2.801730224609" />
                  <Point X="3.967326416016" Y="2.906191162109" />
                  <Point X="4.123272949219" Y="2.689460205078" />
                  <Point X="4.161557128906" Y="2.626195556641" />
                  <Point X="4.262198730469" Y="2.459884277344" />
                  <Point X="3.454660888672" Y="1.84023815918" />
                  <Point X="3.230783935547" Y="1.668451293945" />
                  <Point X="3.216440673828" Y="1.654853881836" />
                  <Point X="3.195792480469" Y="1.630727783203" />
                  <Point X="3.192570556641" Y="1.626751342773" />
                  <Point X="3.146191650391" Y="1.566246582031" />
                  <Point X="3.133252685547" Y="1.543404296875" />
                  <Point X="3.120535400391" Y="1.511263427734" />
                  <Point X="3.117382080078" Y="1.501896972656" />
                  <Point X="3.100105957031" Y="1.440121459961" />
                  <Point X="3.096652587891" Y="1.417823242188" />
                  <Point X="3.095836425781" Y="1.394252929688" />
                  <Point X="3.097739257812" Y="1.371768554688" />
                  <Point X="3.101226074219" Y="1.354869018555" />
                  <Point X="3.115408203125" Y="1.286135864258" />
                  <Point X="3.124325683594" Y="1.261192138672" />
                  <Point X="3.1410078125" Y="1.229399169922" />
                  <Point X="3.145766845703" Y="1.221324829102" />
                  <Point X="3.184340332031" Y="1.162694946289" />
                  <Point X="3.198893798828" Y="1.145450195312" />
                  <Point X="3.216137695313" Y="1.129360595703" />
                  <Point X="3.234347412109" Y="1.116034912109" />
                  <Point X="3.248091308594" Y="1.108298217773" />
                  <Point X="3.303989501953" Y="1.076832519531" />
                  <Point X="3.329186523438" Y="1.067060180664" />
                  <Point X="3.365744384766" Y="1.058606323242" />
                  <Point X="3.374700439453" Y="1.056982788086" />
                  <Point X="3.450278564453" Y="1.046994018555" />
                  <Point X="3.462702148438" Y="1.046175048828" />
                  <Point X="3.488203613281" Y="1.04698449707" />
                  <Point X="4.626091308594" Y="1.196790283203" />
                  <Point X="4.776839355469" Y="1.21663659668" />
                  <Point X="4.845936523438" Y="0.93280847168" />
                  <Point X="4.858000976562" Y="0.855319213867" />
                  <Point X="4.890864746094" Y="0.644238525391" />
                  <Point X="3.973147705078" Y="0.398337036133" />
                  <Point X="3.716579833984" Y="0.329589935303" />
                  <Point X="3.704791259766" Y="0.325586608887" />
                  <Point X="3.681545166016" Y="0.315067840576" />
                  <Point X="3.663288330078" Y="0.304514923096" />
                  <Point X="3.58903515625" Y="0.261595275879" />
                  <Point X="3.567970703125" Y="0.245059829712" />
                  <Point X="3.542704589844" Y="0.218681381226" />
                  <Point X="3.536576660156" Y="0.211618423462" />
                  <Point X="3.492024902344" Y="0.154848968506" />
                  <Point X="3.48030078125" Y="0.13556854248" />
                  <Point X="3.470527099609" Y="0.114104927063" />
                  <Point X="3.463680908203" Y="0.092604118347" />
                  <Point X="3.460029541016" Y="0.073538032532" />
                  <Point X="3.445178710938" Y="-0.004006529331" />
                  <Point X="3.443921630859" Y="-0.030991523743" />
                  <Point X="3.447572998047" Y="-0.068866874695" />
                  <Point X="3.448830078125" Y="-0.077619567871" />
                  <Point X="3.463680664062" Y="-0.155163970947" />
                  <Point X="3.470527099609" Y="-0.176665542603" />
                  <Point X="3.480301025391" Y="-0.198129302979" />
                  <Point X="3.492024902344" Y="-0.217408813477" />
                  <Point X="3.502979003906" Y="-0.231367004395" />
                  <Point X="3.547530761719" Y="-0.288136474609" />
                  <Point X="3.559999023438" Y="-0.30123614502" />
                  <Point X="3.58903515625" Y="-0.324155273438" />
                  <Point X="3.607291992188" Y="-0.334708190918" />
                  <Point X="3.681545166016" Y="-0.377627838135" />
                  <Point X="3.692710205078" Y="-0.383139038086" />
                  <Point X="3.716579833984" Y="-0.392149932861" />
                  <Point X="4.76007421875" Y="-0.671753479004" />
                  <Point X="4.89147265625" Y="-0.706961608887" />
                  <Point X="4.855022949219" Y="-0.948724182129" />
                  <Point X="4.839565429688" Y="-1.016461486816" />
                  <Point X="4.801173828125" Y="-1.184698974609" />
                  <Point X="3.721928710938" Y="-1.042613525391" />
                  <Point X="3.424382080078" Y="-1.003441040039" />
                  <Point X="3.408035644531" Y="-1.002710327148" />
                  <Point X="3.374658691406" Y="-1.005508728027" />
                  <Point X="3.338826904297" Y="-1.013296936035" />
                  <Point X="3.193094482422" Y="-1.044972412109" />
                  <Point X="3.163974121094" Y="-1.056596923828" />
                  <Point X="3.136147460938" Y="-1.073489257812" />
                  <Point X="3.112397460938" Y="-1.093960083008" />
                  <Point X="3.090739257812" Y="-1.1200078125" />
                  <Point X="3.002653320312" Y="-1.225948120117" />
                  <Point X="2.987932617188" Y="-1.250330688477" />
                  <Point X="2.976589355469" Y="-1.277715942383" />
                  <Point X="2.969757568359" Y="-1.305365234375" />
                  <Point X="2.966653320312" Y="-1.339098388672" />
                  <Point X="2.954028564453" Y="-1.476295532227" />
                  <Point X="2.956347412109" Y="-1.507564819336" />
                  <Point X="2.964079101562" Y="-1.539185791016" />
                  <Point X="2.976450439453" Y="-1.567996582031" />
                  <Point X="2.996280273438" Y="-1.598840576172" />
                  <Point X="3.076930664062" Y="-1.724287109375" />
                  <Point X="3.086932373047" Y="-1.73723815918" />
                  <Point X="3.110628417969" Y="-1.760909179688" />
                  <Point X="4.078997802734" Y="-2.503965332031" />
                  <Point X="4.213122558594" Y="-2.6068828125" />
                  <Point X="4.124811035156" Y="-2.749784423828" />
                  <Point X="4.092842773438" Y="-2.795206542969" />
                  <Point X="4.028981445312" Y="-2.885945068359" />
                  <Point X="3.065959960938" Y="-2.329944091797" />
                  <Point X="2.800954589844" Y="-2.176943115234" />
                  <Point X="2.786131591797" Y="-2.170012451172" />
                  <Point X="2.754224365234" Y="-2.159825195312" />
                  <Point X="2.711578857422" Y="-2.152123535156" />
                  <Point X="2.538134033203" Y="-2.120799560547" />
                  <Point X="2.506781982422" Y="-2.120395263672" />
                  <Point X="2.474609130859" Y="-2.125353027344" />
                  <Point X="2.444833251953" Y="-2.135176757812" />
                  <Point X="2.409405273438" Y="-2.153822265625" />
                  <Point X="2.265315185547" Y="-2.229655761719" />
                  <Point X="2.242384521484" Y="-2.246548828125" />
                  <Point X="2.221424804688" Y="-2.267508544922" />
                  <Point X="2.204531738281" Y="-2.290439208984" />
                  <Point X="2.185886230469" Y="-2.3258671875" />
                  <Point X="2.110052734375" Y="-2.469957275391" />
                  <Point X="2.100229003906" Y="-2.499733154297" />
                  <Point X="2.095271240234" Y="-2.531906005859" />
                  <Point X="2.095675537109" Y="-2.563258056641" />
                  <Point X="2.103377197266" Y="-2.605903564453" />
                  <Point X="2.134701171875" Y="-2.779348388672" />
                  <Point X="2.138985595703" Y="-2.795141845703" />
                  <Point X="2.151819091797" Y="-2.826078613281" />
                  <Point X="2.774093994141" Y="-3.903889648438" />
                  <Point X="2.861283447266" Y="-4.054906494141" />
                  <Point X="2.781847412109" Y="-4.111645507812" />
                  <Point X="2.746110839844" Y="-4.13477734375" />
                  <Point X="2.701765136719" Y="-4.163481445312" />
                  <Point X="1.96076953125" Y="-3.197797851562" />
                  <Point X="1.758546020508" Y="-2.934255126953" />
                  <Point X="1.747503540039" Y="-2.922179443359" />
                  <Point X="1.721924194336" Y="-2.900557373047" />
                  <Point X="1.679864135742" Y="-2.873516601562" />
                  <Point X="1.50880078125" Y="-2.763538574219" />
                  <Point X="1.479989746094" Y="-2.751167236328" />
                  <Point X="1.448368408203" Y="-2.743435546875" />
                  <Point X="1.417099487305" Y="-2.741116699219" />
                  <Point X="1.371099609375" Y="-2.745349609375" />
                  <Point X="1.184012573242" Y="-2.762565673828" />
                  <Point X="1.156362670898" Y="-2.769397460938" />
                  <Point X="1.128977661133" Y="-2.780740722656" />
                  <Point X="1.104595825195" Y="-2.795461181641" />
                  <Point X="1.069076171875" Y="-2.824994628906" />
                  <Point X="0.924611938477" Y="-2.945111572266" />
                  <Point X="0.904141052246" Y="-2.968861816406" />
                  <Point X="0.887248779297" Y="-2.996688476562" />
                  <Point X="0.875624206543" Y="-3.025809082031" />
                  <Point X="0.86500402832" Y="-3.074670654297" />
                  <Point X="0.821809936523" Y="-3.273396972656" />
                  <Point X="0.819724487305" Y="-3.289627441406" />
                  <Point X="0.819742248535" Y="-3.323120117188" />
                  <Point X="0.996090087891" Y="-4.662615234375" />
                  <Point X="1.022065246582" Y="-4.859915527344" />
                  <Point X="0.975680541992" Y="-4.870083007812" />
                  <Point X="0.94265802002" Y="-4.87608203125" />
                  <Point X="0.929315612793" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058439086914" Y="-4.752633789062" />
                  <Point X="-1.109799438477" Y="-4.739419433594" />
                  <Point X="-1.141246337891" Y="-4.731328613281" />
                  <Point X="-1.130291992188" Y="-4.648122070312" />
                  <Point X="-1.120775756836" Y="-4.575838378906" />
                  <Point X="-1.120077392578" Y="-4.568099609375" />
                  <Point X="-1.119451660156" Y="-4.541033691406" />
                  <Point X="-1.121759277344" Y="-4.509328125" />
                  <Point X="-1.123334106445" Y="-4.497690429688" />
                  <Point X="-1.135279174805" Y="-4.437638183594" />
                  <Point X="-1.183861572266" Y="-4.193398925781" />
                  <Point X="-1.18812512207" Y="-4.17846875" />
                  <Point X="-1.19902734375" Y="-4.149502441406" />
                  <Point X="-1.205666137695" Y="-4.135466308594" />
                  <Point X="-1.221738037109" Y="-4.10762890625" />
                  <Point X="-1.230573852539" Y="-4.094862304688" />
                  <Point X="-1.250207885742" Y="-4.0709375" />
                  <Point X="-1.261006835938" Y="-4.059779296875" />
                  <Point X="-1.307041137695" Y="-4.019408447266" />
                  <Point X="-1.494267822266" Y="-3.855214599609" />
                  <Point X="-1.506738647461" Y="-3.845965576172" />
                  <Point X="-1.533021606445" Y="-3.829621582031" />
                  <Point X="-1.546833496094" Y="-3.822526855469" />
                  <Point X="-1.576530761719" Y="-3.810225830078" />
                  <Point X="-1.591313354492" Y="-3.805476074219" />
                  <Point X="-1.621454833984" Y="-3.798447998047" />
                  <Point X="-1.636813720703" Y="-3.796169677734" />
                  <Point X="-1.697911254883" Y="-3.792165039062" />
                  <Point X="-1.946402832031" Y="-3.775878173828" />
                  <Point X="-1.961927978516" Y="-3.776132324219" />
                  <Point X="-1.992729614258" Y="-3.779166015625" />
                  <Point X="-2.008006103516" Y="-3.781945556641" />
                  <Point X="-2.03905480957" Y="-3.790265136719" />
                  <Point X="-2.053674560547" Y="-3.795496337891" />
                  <Point X="-2.081865478516" Y="-3.808269775391" />
                  <Point X="-2.095436767578" Y="-3.815811767578" />
                  <Point X="-2.146346679688" Y="-3.849828613281" />
                  <Point X="-2.353403320312" Y="-3.988179931641" />
                  <Point X="-2.359681640625" Y="-3.992757080078" />
                  <Point X="-2.380442138672" Y="-4.010131103516" />
                  <Point X="-2.402759277344" Y="-4.032771484375" />
                  <Point X="-2.410471435547" Y="-4.041629638672" />
                  <Point X="-2.503202880859" Y="-4.162479492188" />
                  <Point X="-2.7475859375" Y="-4.011163085938" />
                  <Point X="-2.818706298828" Y="-3.956402832031" />
                  <Point X="-2.980862792969" Y="-3.831547607422" />
                  <Point X="-2.488403320312" Y="-2.978583007812" />
                  <Point X="-2.341488769531" Y="-2.724119628906" />
                  <Point X="-2.334850097656" Y="-2.710083251953" />
                  <Point X="-2.323947998047" Y="-2.681116943359" />
                  <Point X="-2.319684570312" Y="-2.666187011719" />
                  <Point X="-2.313413574219" Y="-2.634660644531" />
                  <Point X="-2.311638916016" Y="-2.619235351562" />
                  <Point X="-2.310626220703" Y="-2.588301269531" />
                  <Point X="-2.314666259766" Y="-2.557615234375" />
                  <Point X="-2.323650878906" Y="-2.527997558594" />
                  <Point X="-2.329357421875" Y="-2.513557128906" />
                  <Point X="-2.343574462891" Y="-2.484728271484" />
                  <Point X="-2.351557128906" Y="-2.47141015625" />
                  <Point X="-2.369584716797" Y="-2.446253173828" />
                  <Point X="-2.379629638672" Y="-2.434414306641" />
                  <Point X="-2.396978271484" Y="-2.417065673828" />
                  <Point X="-2.408817138672" Y="-2.407020751953" />
                  <Point X="-2.433974121094" Y="-2.388993164062" />
                  <Point X="-2.447292236328" Y="-2.381010498047" />
                  <Point X="-2.47612109375" Y="-2.366793457031" />
                  <Point X="-2.490561523438" Y="-2.361086914062" />
                  <Point X="-2.520179199219" Y="-2.352102294922" />
                  <Point X="-2.550865234375" Y="-2.348062255859" />
                  <Point X="-2.581799316406" Y="-2.349074951172" />
                  <Point X="-2.597224609375" Y="-2.350849609375" />
                  <Point X="-2.628750976562" Y="-2.357120605469" />
                  <Point X="-2.643680908203" Y="-2.361384033203" />
                  <Point X="-2.672647216797" Y="-2.372286132812" />
                  <Point X="-2.68668359375" Y="-2.378924804688" />
                  <Point X="-3.721619384766" Y="-2.9764453125" />
                  <Point X="-3.793089111328" Y="-3.017708496094" />
                  <Point X="-4.004016845703" Y="-2.740592773438" />
                  <Point X="-4.055001464844" Y="-2.655098876953" />
                  <Point X="-4.181265136719" Y="-2.443373779297" />
                  <Point X="-3.305175048828" Y="-1.771125488281" />
                  <Point X="-3.048122314453" Y="-1.573882080078" />
                  <Point X="-3.039157714844" Y="-1.566064941406" />
                  <Point X="-3.016265869141" Y="-1.543428833008" />
                  <Point X="-3.002706298828" Y="-1.526551513672" />
                  <Point X="-2.983769775391" Y="-1.496522949219" />
                  <Point X="-2.975716796875" Y="-1.480615966797" />
                  <Point X="-2.967482666016" Y="-1.459677368164" />
                  <Point X="-2.960370361328" Y="-1.437781005859" />
                  <Point X="-2.954186279297" Y="-1.413904296875" />
                  <Point X="-2.951952880859" Y="-1.402395507812" />
                  <Point X="-2.947838623047" Y="-1.370912963867" />
                  <Point X="-2.94708203125" Y="-1.35569934082" />
                  <Point X="-2.94778125" Y="-1.332830688477" />
                  <Point X="-2.951229248047" Y="-1.310212524414" />
                  <Point X="-2.957375732422" Y="-1.288174316406" />
                  <Point X="-2.961114746094" Y="-1.277340698242" />
                  <Point X="-2.973119873047" Y="-1.248358032227" />
                  <Point X="-2.978657958984" Y="-1.23714050293" />
                  <Point X="-2.991159423828" Y="-1.215531494141" />
                  <Point X="-3.006391113281" Y="-1.195753295898" />
                  <Point X="-3.024090087891" Y="-1.178146850586" />
                  <Point X="-3.033520751953" Y="-1.169927246094" />
                  <Point X="-3.051249267578" Y="-1.15642199707" />
                  <Point X="-3.070012939453" Y="-1.143818725586" />
                  <Point X="-3.091269287109" Y="-1.131307983398" />
                  <Point X="-3.105434326172" Y="-1.124481201172" />
                  <Point X="-3.134697265625" Y="-1.113257202148" />
                  <Point X="-3.149795410156" Y="-1.108859985352" />
                  <Point X="-3.181682861328" Y="-1.102378540039" />
                  <Point X="-3.197298828125" Y="-1.100532348633" />
                  <Point X="-3.228622070312" Y="-1.09944140625" />
                  <Point X="-3.244329101562" Y="-1.100196533203" />
                  <Point X="-4.550837402344" Y="-1.272201660156" />
                  <Point X="-4.660920898438" Y="-1.286694458008" />
                  <Point X="-4.740762207031" Y="-0.974118164063" />
                  <Point X="-4.754250976562" Y="-0.87980480957" />
                  <Point X="-4.786452148438" Y="-0.654654418945" />
                  <Point X="-3.801109863281" Y="-0.39063269043" />
                  <Point X="-3.508288085938" Y="-0.312171234131" />
                  <Point X="-3.498334960938" Y="-0.308906433105" />
                  <Point X="-3.478848144531" Y="-0.30130065918" />
                  <Point X="-3.469314208984" Y="-0.296959686279" />
                  <Point X="-3.448269042969" Y="-0.285937805176" />
                  <Point X="-3.428083984375" Y="-0.273712097168" />
                  <Point X="-3.405808105469" Y="-0.258251312256" />
                  <Point X="-3.396477539062" Y="-0.250868453979" />
                  <Point X="-3.372523193359" Y="-0.229342651367" />
                  <Point X="-3.358154785156" Y="-0.213104125977" />
                  <Point X="-3.337781494141" Y="-0.183954711914" />
                  <Point X="-3.328971191406" Y="-0.168418563843" />
                  <Point X="-3.319720214844" Y="-0.147798614502" />
                  <Point X="-3.311611816406" Y="-0.126344161987" />
                  <Point X="-3.304186523438" Y="-0.102419441223" />
                  <Point X="-3.301576416016" Y="-0.091937805176" />
                  <Point X="-3.296133544922" Y="-0.063198596954" />
                  <Point X="-3.294497070312" Y="-0.043441856384" />
                  <Point X="-3.295210205078" Y="-0.010864010811" />
                  <Point X="-3.296974121094" Y="0.005393848896" />
                  <Point X="-3.301046875" Y="0.026097167969" />
                  <Point X="-3.306012207031" Y="0.045742145538" />
                  <Point X="-3.3134375" Y="0.069666572571" />
                  <Point X="-3.317668945312" Y="0.080786682129" />
                  <Point X="-3.330984863281" Y="0.11011013031" />
                  <Point X="-3.342220947266" Y="0.128800064087" />
                  <Point X="-3.364072509766" Y="0.157170379639" />
                  <Point X="-3.376346923828" Y="0.17031652832" />
                  <Point X="-3.393642578125" Y="0.185635681152" />
                  <Point X="-3.411285888672" Y="0.199493286133" />
                  <Point X="-3.433561767578" Y="0.214953933716" />
                  <Point X="-3.440484863281" Y="0.219329101563" />
                  <Point X="-3.465616210938" Y="0.232834609985" />
                  <Point X="-3.496566894531" Y="0.245635940552" />
                  <Point X="-3.508288085938" Y="0.249611312866" />
                  <Point X="-4.699232421875" Y="0.568723876953" />
                  <Point X="-4.785446289062" Y="0.591824829102" />
                  <Point X="-4.73133203125" Y="0.95752331543" />
                  <Point X="-4.70417578125" Y="1.057739501953" />
                  <Point X="-4.6335859375" Y="1.318237060547" />
                  <Point X="-3.987968017578" Y="1.233239868164" />
                  <Point X="-3.778066162109" Y="1.20560559082" />
                  <Point X="-3.76773828125" Y="1.204815551758" />
                  <Point X="-3.747057861328" Y="1.204364257812" />
                  <Point X="-3.736705322266" Y="1.20470324707" />
                  <Point X="-3.715143554688" Y="1.20658996582" />
                  <Point X="-3.704889648438" Y="1.208053833008" />
                  <Point X="-3.684603271484" Y="1.212089233398" />
                  <Point X="-3.66244921875" Y="1.218482299805" />
                  <Point X="-3.613145507812" Y="1.234027832031" />
                  <Point X="-3.603450927734" Y="1.237676513672" />
                  <Point X="-3.584517578125" Y="1.246007324219" />
                  <Point X="-3.575278076172" Y="1.250689941406" />
                  <Point X="-3.556533935547" Y="1.26151184082" />
                  <Point X="-3.547859375" Y="1.267172119141" />
                  <Point X="-3.531178466797" Y="1.279403198242" />
                  <Point X="-3.515928955078" Y="1.293376708984" />
                  <Point X="-3.502290527344" Y="1.308928100586" />
                  <Point X="-3.495895263672" Y="1.317077270508" />
                  <Point X="-3.483480712891" Y="1.334807128906" />
                  <Point X="-3.478011474609" Y="1.343602294922" />
                  <Point X="-3.468062744141" Y="1.361736083984" />
                  <Point X="-3.458718994141" Y="1.382817749023" />
                  <Point X="-3.438935791016" Y="1.430578857422" />
                  <Point X="-3.435499511719" Y="1.440350708008" />
                  <Point X="-3.4297109375" Y="1.460210327148" />
                  <Point X="-3.427358642578" Y="1.470297973633" />
                  <Point X="-3.423600341797" Y="1.491612915039" />
                  <Point X="-3.422360595703" Y="1.501897216797" />
                  <Point X="-3.421008056641" Y="1.522537597656" />
                  <Point X="-3.421910400391" Y="1.543200927734" />
                  <Point X="-3.425056884766" Y="1.563644287109" />
                  <Point X="-3.427188232422" Y="1.573780395508" />
                  <Point X="-3.432790039062" Y="1.594686767578" />
                  <Point X="-3.43601171875" Y="1.604530029297" />
                  <Point X="-3.443508300781" Y="1.623808105469" />
                  <Point X="-3.45365234375" Y="1.644517822266" />
                  <Point X="-3.477522949219" Y="1.690372802734" />
                  <Point X="-3.482799804688" Y="1.699286499023" />
                  <Point X="-3.494291992188" Y="1.716485473633" />
                  <Point X="-3.500507080078" Y="1.724770996094" />
                  <Point X="-3.514419677734" Y="1.74135144043" />
                  <Point X="-3.521500732422" Y="1.748911743164" />
                  <Point X="-3.536442626953" Y="1.763215454102" />
                  <Point X="-3.544303710938" Y="1.769958862305" />
                  <Point X="-4.227431640625" Y="2.294142089844" />
                  <Point X="-4.227614257812" Y="2.294282226563" />
                  <Point X="-4.00229296875" Y="2.680312011719" />
                  <Point X="-3.930366210938" Y="2.772763916016" />
                  <Point X="-3.726338378906" Y="3.035012939453" />
                  <Point X="-3.383072753906" Y="2.836828125" />
                  <Point X="-3.254156982422" Y="2.7623984375" />
                  <Point X="-3.244918457031" Y="2.757716552734" />
                  <Point X="-3.225986083984" Y="2.749385986328" />
                  <Point X="-3.216292236328" Y="2.745737304688" />
                  <Point X="-3.195650146484" Y="2.739228759766" />
                  <Point X="-3.185615478516" Y="2.736656982422" />
                  <Point X="-3.165327392578" Y="2.732621582031" />
                  <Point X="-3.138190673828" Y="2.729680908203" />
                  <Point X="-3.069524658203" Y="2.723673339844" />
                  <Point X="-3.059173095703" Y="2.723334472656" />
                  <Point X="-3.038493164062" Y="2.723785644531" />
                  <Point X="-3.028164794922" Y="2.724575683594" />
                  <Point X="-3.006706054688" Y="2.727400878906" />
                  <Point X="-2.996526611328" Y="2.729310546875" />
                  <Point X="-2.976435302734" Y="2.734226806641" />
                  <Point X="-2.956997802734" Y="2.741301513672" />
                  <Point X="-2.938446533203" Y="2.750449951172" />
                  <Point X="-2.929420898438" Y="2.755530273438" />
                  <Point X="-2.911166503906" Y="2.767159423828" />
                  <Point X="-2.902746337891" Y="2.773193359375" />
                  <Point X="-2.886614257812" Y="2.786140869141" />
                  <Point X="-2.866918457031" Y="2.805038330078" />
                  <Point X="-2.818178710938" Y="2.853777832031" />
                  <Point X="-2.811265380859" Y="2.861489501953" />
                  <Point X="-2.798318359375" Y="2.877620849609" />
                  <Point X="-2.792284667969" Y="2.886040527344" />
                  <Point X="-2.780655273438" Y="2.904294921875" />
                  <Point X="-2.775574707031" Y="2.913320800781" />
                  <Point X="-2.766426025391" Y="2.931872314453" />
                  <Point X="-2.759351318359" Y="2.951309570312" />
                  <Point X="-2.754434814453" Y="2.971401367188" />
                  <Point X="-2.752524902344" Y="2.981581542969" />
                  <Point X="-2.749699707031" Y="3.003040527344" />
                  <Point X="-2.748909667969" Y="3.013368896484" />
                  <Point X="-2.748458496094" Y="3.034048828125" />
                  <Point X="-2.750274414062" Y="3.061283691406" />
                  <Point X="-2.756281982422" Y="3.129949951172" />
                  <Point X="-2.757745605469" Y="3.140203369141" />
                  <Point X="-2.761781005859" Y="3.160491455078" />
                  <Point X="-2.764352783203" Y="3.170526123047" />
                  <Point X="-2.770861328125" Y="3.191168212891" />
                  <Point X="-2.774510009766" Y="3.200862060547" />
                  <Point X="-2.782840576172" Y="3.219794433594" />
                  <Point X="-2.787522460938" Y="3.229032958984" />
                  <Point X="-3.059387207031" Y="3.699916503906" />
                  <Point X="-2.648374023438" Y="4.015036376953" />
                  <Point X="-2.535096679688" Y="4.077971191406" />
                  <Point X="-2.192524902344" Y="4.268296386719" />
                  <Point X="-2.158029296875" Y="4.223340820312" />
                  <Point X="-2.118563964844" Y="4.171908691406" />
                  <Point X="-2.111821289062" Y="4.164048828125" />
                  <Point X="-2.097518554688" Y="4.149107421875" />
                  <Point X="-2.089958740234" Y="4.142026367188" />
                  <Point X="-2.07337890625" Y="4.128113769531" />
                  <Point X="-2.065092773438" Y="4.121897949219" />
                  <Point X="-2.047892211914" Y="4.110404785156" />
                  <Point X="-2.038977905273" Y="4.105127441406" />
                  <Point X="-2.020187011719" Y="4.095345947266" />
                  <Point X="-1.94376184082" Y="4.055560791016" />
                  <Point X="-1.934325561523" Y="4.051285400391" />
                  <Point X="-1.915046630859" Y="4.043788818359" />
                  <Point X="-1.905203857422" Y="4.040567382812" />
                  <Point X="-1.884297363281" Y="4.034965820313" />
                  <Point X="-1.874162597656" Y="4.032834716797" />
                  <Point X="-1.853719604492" Y="4.029688232422" />
                  <Point X="-1.833053344727" Y="4.028785888672" />
                  <Point X="-1.81241394043" Y="4.030138916016" />
                  <Point X="-1.802132568359" Y="4.031378662109" />
                  <Point X="-1.780817260742" Y="4.035136962891" />
                  <Point X="-1.77073059082" Y="4.037488769531" />
                  <Point X="-1.750869873047" Y="4.04327734375" />
                  <Point X="-1.741095825195" Y="4.046714599609" />
                  <Point X="-1.721523925781" Y="4.054822021484" />
                  <Point X="-1.641921875" Y="4.087794189453" />
                  <Point X="-1.632582885742" Y="4.092273925781" />
                  <Point X="-1.614449584961" Y="4.10222265625" />
                  <Point X="-1.605655273438" Y="4.10769140625" />
                  <Point X="-1.587925537109" Y="4.120105957031" />
                  <Point X="-1.57977734375" Y="4.126500976563" />
                  <Point X="-1.564225341797" Y="4.140139648437" />
                  <Point X="-1.550251220703" Y="4.155389648437" />
                  <Point X="-1.538020141602" Y="4.172070800781" />
                  <Point X="-1.532359863281" Y="4.180745605469" />
                  <Point X="-1.521538085938" Y="4.199489746094" />
                  <Point X="-1.516856323242" Y="4.208728027344" />
                  <Point X="-1.508526123047" Y="4.227659667969" />
                  <Point X="-1.504877685547" Y="4.237353027344" />
                  <Point X="-1.498507202148" Y="4.257557128906" />
                  <Point X="-1.472598388672" Y="4.339729492188" />
                  <Point X="-1.470026611328" Y="4.349763671875" />
                  <Point X="-1.465991088867" Y="4.370051269531" />
                  <Point X="-1.46452722168" Y="4.3803046875" />
                  <Point X="-1.46264074707" Y="4.401866210938" />
                  <Point X="-1.462301757812" Y="4.41221875" />
                  <Point X="-1.462752929688" Y="4.432898925781" />
                  <Point X="-1.46354309082" Y="4.4432265625" />
                  <Point X="-1.479266235352" Y="4.562655761719" />
                  <Point X="-0.93118170166" Y="4.716319824219" />
                  <Point X="-0.79386114502" Y="4.732391113281" />
                  <Point X="-0.365221923828" Y="4.782557128906" />
                  <Point X="-0.263803039551" Y="4.404056640625" />
                  <Point X="-0.225666244507" Y="4.261727539062" />
                  <Point X="-0.220435317993" Y="4.247107910156" />
                  <Point X="-0.207661773682" Y="4.218916503906" />
                  <Point X="-0.200119155884" Y="4.205344726562" />
                  <Point X="-0.182260925293" Y="4.178618164062" />
                  <Point X="-0.172608306885" Y="4.166456054688" />
                  <Point X="-0.151451248169" Y="4.1438671875" />
                  <Point X="-0.126898002625" Y="4.125026367188" />
                  <Point X="-0.099602806091" Y="4.110436523438" />
                  <Point X="-0.085356712341" Y="4.104260742188" />
                  <Point X="-0.054918586731" Y="4.093927978516" />
                  <Point X="-0.039857292175" Y="4.090155273438" />
                  <Point X="-0.009319890976" Y="4.08511328125" />
                  <Point X="0.02163187027" Y="4.08511328125" />
                  <Point X="0.052169120789" Y="4.090155273438" />
                  <Point X="0.067230415344" Y="4.093927978516" />
                  <Point X="0.097668540955" Y="4.104260742188" />
                  <Point X="0.111914489746" Y="4.110436523438" />
                  <Point X="0.139209686279" Y="4.125026367188" />
                  <Point X="0.163763076782" Y="4.143866699219" />
                  <Point X="0.184920150757" Y="4.166455566406" />
                  <Point X="0.194573059082" Y="4.178618164062" />
                  <Point X="0.212431289673" Y="4.205344726562" />
                  <Point X="0.219973449707" Y="4.218916015625" />
                  <Point X="0.232746994019" Y="4.247107421875" />
                  <Point X="0.237978225708" Y="4.261727539062" />
                  <Point X="0.378190185547" Y="4.785006347656" />
                  <Point X="0.827875976562" Y="4.737912109375" />
                  <Point X="0.941492370605" Y="4.710481933594" />
                  <Point X="1.453599853516" Y="4.586842773438" />
                  <Point X="1.525388183594" Y="4.560805175781" />
                  <Point X="1.858253540039" Y="4.440072265625" />
                  <Point X="1.929771240234" Y="4.406625488281" />
                  <Point X="2.250453369141" Y="4.256653320312" />
                  <Point X="2.319595947266" Y="4.216370605469" />
                  <Point X="2.629431884766" Y="4.035859130859" />
                  <Point X="2.694595214844" Y="3.989518554688" />
                  <Point X="2.817779785156" Y="3.901916503906" />
                  <Point X="2.235351074219" Y="2.893120361328" />
                  <Point X="2.06530859375" Y="2.598597900391" />
                  <Point X="2.060896728516" Y="2.589967529297" />
                  <Point X="2.049467285156" Y="2.563269775391" />
                  <Point X="2.040228637695" Y="2.535342041016" />
                  <Point X="2.037064575195" Y="2.524754150391" />
                  <Point X="2.01983190918" Y="2.460312255859" />
                  <Point X="2.017358276367" Y="2.447692626953" />
                  <Point X="2.014137817383" Y="2.422234375" />
                  <Point X="2.013391113281" Y="2.409395751953" />
                  <Point X="2.013705566406" Y="2.376792480469" />
                  <Point X="2.015063110352" Y="2.355879394531" />
                  <Point X="2.021782348633" Y="2.300155517578" />
                  <Point X="2.02380065918" Y="2.289033935547" />
                  <Point X="2.029143676758" Y="2.267110595703" />
                  <Point X="2.032468261719" Y="2.256308837891" />
                  <Point X="2.040734375" Y="2.234220214844" />
                  <Point X="2.045318237305" Y="2.223889160156" />
                  <Point X="2.055681396484" Y="2.203843505859" />
                  <Point X="2.069938232422" Y="2.181635009766" />
                  <Point X="2.104418212891" Y="2.1308203125" />
                  <Point X="2.112421875" Y="2.120604003906" />
                  <Point X="2.129746337891" Y="2.101358154297" />
                  <Point X="2.139067138672" Y="2.092328613281" />
                  <Point X="2.164861083984" Y="2.070550537109" />
                  <Point X="2.180752197266" Y="2.058503173828" />
                  <Point X="2.231566894531" Y="2.02402331543" />
                  <Point X="2.241280517578" Y="2.018244873047" />
                  <Point X="2.261325683594" Y="2.007882202148" />
                  <Point X="2.271657226562" Y="2.003297851562" />
                  <Point X="2.293745605469" Y="1.995031860352" />
                  <Point X="2.304547119141" Y="1.991707641602" />
                  <Point X="2.326469726562" Y="1.986364868164" />
                  <Point X="2.351291748047" Y="1.982694458008" />
                  <Point X="2.407015625" Y="1.975974975586" />
                  <Point X="2.420244873047" Y="1.975309936523" />
                  <Point X="2.446666992188" Y="1.975826293945" />
                  <Point X="2.459859863281" Y="1.97700769043" />
                  <Point X="2.494606933594" Y="1.982582397461" />
                  <Point X="2.513592773438" Y="1.98663293457" />
                  <Point X="2.578034667969" Y="2.003865600586" />
                  <Point X="2.583994384766" Y="2.005670654297" />
                  <Point X="2.604404541016" Y="2.013067260742" />
                  <Point X="2.627655029297" Y="2.02357409668" />
                  <Point X="2.636033935547" Y="2.027872558594" />
                  <Point X="3.833894775391" Y="2.719457763672" />
                  <Point X="3.940404785156" Y="2.780951416016" />
                  <Point X="4.043953369141" Y="2.637041992188" />
                  <Point X="4.080280273438" Y="2.577011474609" />
                  <Point X="4.136884277344" Y="2.483472412109" />
                  <Point X="3.396828613281" Y="1.915606689453" />
                  <Point X="3.172951660156" Y="1.743819824219" />
                  <Point X="3.165425292969" Y="1.737395019531" />
                  <Point X="3.144265136719" Y="1.71662487793" />
                  <Point X="3.123616943359" Y="1.692498779297" />
                  <Point X="3.117173095703" Y="1.684546020508" />
                  <Point X="3.070794189453" Y="1.624041137695" />
                  <Point X="3.063531738281" Y="1.613069091797" />
                  <Point X="3.050592773438" Y="1.590226806641" />
                  <Point X="3.044916259766" Y="1.578356689453" />
                  <Point X="3.032198974609" Y="1.546215820312" />
                  <Point X="3.025892333984" Y="1.527482910156" />
                  <Point X="3.008616210938" Y="1.465707397461" />
                  <Point X="3.006225097656" Y="1.454661010742" />
                  <Point X="3.002771728516" Y="1.432362792969" />
                  <Point X="3.001709472656" Y="1.421110839844" />
                  <Point X="3.000893310547" Y="1.397540527344" />
                  <Point X="3.001174804688" Y="1.386241821289" />
                  <Point X="3.003077636719" Y="1.363757446289" />
                  <Point X="3.008185791016" Y="1.335672363281" />
                  <Point X="3.022367919922" Y="1.266939331055" />
                  <Point X="3.025952880859" Y="1.254155273438" />
                  <Point X="3.034870361328" Y="1.229211547852" />
                  <Point X="3.040202880859" Y="1.217052124023" />
                  <Point X="3.056885009766" Y="1.185259155273" />
                  <Point X="3.066403076172" Y="1.169109985352" />
                  <Point X="3.1049765625" Y="1.110480224609" />
                  <Point X="3.111739257813" Y="1.101424316406" />
                  <Point X="3.126292724609" Y="1.08417956543" />
                  <Point X="3.134083740234" Y="1.075990478516" />
                  <Point X="3.151327636719" Y="1.059900878906" />
                  <Point X="3.16003515625" Y="1.052695678711" />
                  <Point X="3.178244873047" Y="1.039369995117" />
                  <Point X="3.201490234375" Y="1.025513305664" />
                  <Point X="3.257388427734" Y="0.994047729492" />
                  <Point X="3.269637939453" Y="0.988260620117" />
                  <Point X="3.294834960938" Y="0.978488220215" />
                  <Point X="3.307782958984" Y="0.974502746582" />
                  <Point X="3.344340820312" Y="0.96604876709" />
                  <Point X="3.362252929688" Y="0.962801818848" />
                  <Point X="3.437831054688" Y="0.952813049316" />
                  <Point X="3.444029785156" Y="0.952199829102" />
                  <Point X="3.465716064453" Y="0.951222900391" />
                  <Point X="3.491217529297" Y="0.952032287598" />
                  <Point X="3.500603515625" Y="0.952797241211" />
                  <Point X="4.638491210938" Y="1.102603027344" />
                  <Point X="4.704703613281" Y="1.111320068359" />
                  <Point X="4.752684082031" Y="0.914232299805" />
                  <Point X="4.764131835938" Y="0.840704528809" />
                  <Point X="4.78387109375" Y="0.713920898438" />
                  <Point X="3.948559814453" Y="0.490099975586" />
                  <Point X="3.691991943359" Y="0.421352966309" />
                  <Point X="3.686031738281" Y="0.419544525146" />
                  <Point X="3.665626953125" Y="0.412138183594" />
                  <Point X="3.642380859375" Y="0.401619415283" />
                  <Point X="3.634003417969" Y="0.397316162109" />
                  <Point X="3.615746582031" Y="0.386763214111" />
                  <Point X="3.541493408203" Y="0.343843597412" />
                  <Point X="3.530375488281" Y="0.336321624756" />
                  <Point X="3.509311035156" Y="0.3197862854" />
                  <Point X="3.499364746094" Y="0.310772918701" />
                  <Point X="3.474098632812" Y="0.28439440918" />
                  <Point X="3.461842773438" Y="0.2702684021" />
                  <Point X="3.417291015625" Y="0.213499069214" />
                  <Point X="3.410854003906" Y="0.204207748413" />
                  <Point X="3.399129882813" Y="0.184927352905" />
                  <Point X="3.393842529297" Y="0.174938262939" />
                  <Point X="3.384068847656" Y="0.153474578857" />
                  <Point X="3.380005371094" Y="0.142928451538" />
                  <Point X="3.373159179688" Y="0.121427619934" />
                  <Point X="3.366725097656" Y="0.091406829834" />
                  <Point X="3.351874267578" Y="0.013862234116" />
                  <Point X="3.350281738281" Y="0.000414238513" />
                  <Point X="3.349024658203" Y="-0.026570777893" />
                  <Point X="3.349360107422" Y="-0.040107646942" />
                  <Point X="3.353011474609" Y="-0.077983009338" />
                  <Point X="3.355525634766" Y="-0.09548841095" />
                  <Point X="3.370376220703" Y="-0.173032714844" />
                  <Point X="3.373158935547" Y="-0.183987548828" />
                  <Point X="3.380005371094" Y="-0.205489120483" />
                  <Point X="3.384069091797" Y="-0.216035858154" />
                  <Point X="3.393843017578" Y="-0.237499526978" />
                  <Point X="3.399130615234" Y="-0.247489074707" />
                  <Point X="3.410854492188" Y="-0.266768585205" />
                  <Point X="3.428244873047" Y="-0.290016784668" />
                  <Point X="3.472796630859" Y="-0.346786254883" />
                  <Point X="3.478717773438" Y="-0.353632720947" />
                  <Point X="3.501139404297" Y="-0.375805175781" />
                  <Point X="3.530175537109" Y="-0.398724334717" />
                  <Point X="3.541493408203" Y="-0.406403656006" />
                  <Point X="3.559750244141" Y="-0.416956481934" />
                  <Point X="3.634003417969" Y="-0.459876251221" />
                  <Point X="3.639495849609" Y="-0.462814971924" />
                  <Point X="3.659158447266" Y="-0.472016967773" />
                  <Point X="3.683028076172" Y="-0.481027832031" />
                  <Point X="3.691991943359" Y="-0.483912902832" />
                  <Point X="4.735486328125" Y="-0.763516418457" />
                  <Point X="4.784876953125" Y="-0.776750671387" />
                  <Point X="4.761613769531" Y="-0.93105090332" />
                  <Point X="4.746946289063" Y="-0.995325927734" />
                  <Point X="4.727801757812" Y="-1.079219604492" />
                  <Point X="3.734328613281" Y="-0.948426208496" />
                  <Point X="3.436781982422" Y="-0.909253845215" />
                  <Point X="3.428624511719" Y="-0.908535827637" />
                  <Point X="3.400098388672" Y="-0.908042541504" />
                  <Point X="3.366721435547" Y="-0.910840820312" />
                  <Point X="3.354481201172" Y="-0.912676330566" />
                  <Point X="3.318649414062" Y="-0.920464477539" />
                  <Point X="3.172916992188" Y="-0.952140014648" />
                  <Point X="3.157874023438" Y="-0.956742431641" />
                  <Point X="3.128753662109" Y="-0.968366943359" />
                  <Point X="3.114676269531" Y="-0.975388916016" />
                  <Point X="3.086849609375" Y="-0.99228125" />
                  <Point X="3.074124023438" Y="-1.001530395508" />
                  <Point X="3.050374023438" Y="-1.022001098633" />
                  <Point X="3.039349853516" Y="-1.033222412109" />
                  <Point X="3.017691650391" Y="-1.059270019531" />
                  <Point X="2.929605712891" Y="-1.165210449219" />
                  <Point X="2.921325927734" Y="-1.17684753418" />
                  <Point X="2.906605224609" Y="-1.201230224609" />
                  <Point X="2.9001640625" Y="-1.213975952148" />
                  <Point X="2.888820800781" Y="-1.241361206055" />
                  <Point X="2.884363037109" Y="-1.254927978516" />
                  <Point X="2.87753125" Y="-1.282577270508" />
                  <Point X="2.875157226562" Y="-1.296659790039" />
                  <Point X="2.872052978516" Y="-1.330392944336" />
                  <Point X="2.859428222656" Y="-1.467590087891" />
                  <Point X="2.859288818359" Y="-1.483321044922" />
                  <Point X="2.861607666016" Y="-1.514590454102" />
                  <Point X="2.864065917969" Y="-1.530128662109" />
                  <Point X="2.871797607422" Y="-1.561749511719" />
                  <Point X="2.876786621094" Y="-1.576669189453" />
                  <Point X="2.889157958984" Y="-1.605479858398" />
                  <Point X="2.896540283203" Y="-1.619371459961" />
                  <Point X="2.916370117188" Y="-1.650215209961" />
                  <Point X="2.997020507812" Y="-1.775661865234" />
                  <Point X="3.001741943359" Y="-1.782353271484" />
                  <Point X="3.019792724609" Y="-1.804448730469" />
                  <Point X="3.043488769531" Y="-1.828119750977" />
                  <Point X="3.052796142578" Y="-1.836277709961" />
                  <Point X="4.021165527344" Y="-2.579333984375" />
                  <Point X="4.087170654297" Y="-2.629981689453" />
                  <Point X="4.045488525391" Y="-2.6974296875" />
                  <Point X="4.015154785156" Y="-2.740529541016" />
                  <Point X="4.001274414063" Y="-2.760251708984" />
                  <Point X="3.113459960938" Y="-2.247671630859" />
                  <Point X="2.848454589844" Y="-2.094670654297" />
                  <Point X="2.841191894531" Y="-2.090885253906" />
                  <Point X="2.815025878906" Y="-2.079513183594" />
                  <Point X="2.783118652344" Y="-2.069325927734" />
                  <Point X="2.771107910156" Y="-2.066337646484" />
                  <Point X="2.728462402344" Y="-2.058635986328" />
                  <Point X="2.555017578125" Y="-2.027311889648" />
                  <Point X="2.539358886719" Y="-2.025807495117" />
                  <Point X="2.508006835938" Y="-2.025403198242" />
                  <Point X="2.492313476562" Y="-2.026503540039" />
                  <Point X="2.460140625" Y="-2.031461303711" />
                  <Point X="2.444844482422" Y="-2.035136230469" />
                  <Point X="2.415068603516" Y="-2.044959960938" />
                  <Point X="2.400588867188" Y="-2.051108642578" />
                  <Point X="2.365160888672" Y="-2.069754150391" />
                  <Point X="2.221070800781" Y="-2.145587646484" />
                  <Point X="2.208968261719" Y="-2.153170410156" />
                  <Point X="2.186037597656" Y="-2.170063476562" />
                  <Point X="2.175209472656" Y="-2.179373779297" />
                  <Point X="2.154249755859" Y="-2.200333496094" />
                  <Point X="2.144939453125" Y="-2.211161621094" />
                  <Point X="2.128046386719" Y="-2.234092285156" />
                  <Point X="2.120463623047" Y="-2.246194824219" />
                  <Point X="2.101818115234" Y="-2.281622802734" />
                  <Point X="2.025984741211" Y="-2.425712890625" />
                  <Point X="2.0198359375" Y="-2.440192626953" />
                  <Point X="2.010012084961" Y="-2.469968505859" />
                  <Point X="2.006337280273" Y="-2.485264648438" />
                  <Point X="2.001379516602" Y="-2.5174375" />
                  <Point X="2.000279174805" Y="-2.533130859375" />
                  <Point X="2.00068347168" Y="-2.564482910156" />
                  <Point X="2.002187866211" Y="-2.580141601562" />
                  <Point X="2.009889526367" Y="-2.622787109375" />
                  <Point X="2.041213378906" Y="-2.796231933594" />
                  <Point X="2.043015014648" Y="-2.804220947266" />
                  <Point X="2.051236083984" Y="-2.83154296875" />
                  <Point X="2.064069580078" Y="-2.862479736328" />
                  <Point X="2.069546630859" Y="-2.873578613281" />
                  <Point X="2.691821533203" Y="-3.951389648438" />
                  <Point X="2.735893066406" Y="-4.027724121094" />
                  <Point X="2.723754394531" Y="-4.036083496094" />
                  <Point X="2.036138061523" Y="-3.139965576172" />
                  <Point X="1.833914550781" Y="-2.876422851562" />
                  <Point X="1.828653442383" Y="-2.870146240234" />
                  <Point X="1.808831665039" Y="-2.849626953125" />
                  <Point X="1.783252319336" Y="-2.828004882812" />
                  <Point X="1.773299072266" Y="-2.820647216797" />
                  <Point X="1.731239013672" Y="-2.793606445312" />
                  <Point X="1.56017565918" Y="-2.683628417969" />
                  <Point X="1.546283935547" Y="-2.676245849609" />
                  <Point X="1.517472900391" Y="-2.663874511719" />
                  <Point X="1.502553344727" Y="-2.658885742188" />
                  <Point X="1.470932006836" Y="-2.651154052734" />
                  <Point X="1.455394042969" Y="-2.648695800781" />
                  <Point X="1.42412512207" Y="-2.646376953125" />
                  <Point X="1.40839453125" Y="-2.646516357422" />
                  <Point X="1.36239453125" Y="-2.650749267578" />
                  <Point X="1.175307495117" Y="-2.667965332031" />
                  <Point X="1.161225097656" Y="-2.670339111328" />
                  <Point X="1.133575317383" Y="-2.677170898438" />
                  <Point X="1.120007568359" Y="-2.68162890625" />
                  <Point X="1.092622680664" Y="-2.692972167969" />
                  <Point X="1.079876708984" Y="-2.699413574219" />
                  <Point X="1.055494873047" Y="-2.714134033203" />
                  <Point X="1.043858764648" Y="-2.722413085938" />
                  <Point X="1.008339050293" Y="-2.751946533203" />
                  <Point X="0.863874938965" Y="-2.872063476562" />
                  <Point X="0.852652832031" Y="-2.883088378906" />
                  <Point X="0.832181945801" Y="-2.906838623047" />
                  <Point X="0.822933044434" Y="-2.919564208984" />
                  <Point X="0.80604083252" Y="-2.947390869141" />
                  <Point X="0.799018859863" Y="-2.961468261719" />
                  <Point X="0.787394165039" Y="-2.990588867188" />
                  <Point X="0.782791748047" Y="-3.005631835938" />
                  <Point X="0.772171447754" Y="-3.054493408203" />
                  <Point X="0.728977478027" Y="-3.253219726562" />
                  <Point X="0.727584594727" Y="-3.261290039062" />
                  <Point X="0.724724487305" Y="-3.289677734375" />
                  <Point X="0.7247421875" Y="-3.323170410156" />
                  <Point X="0.725554992676" Y="-3.335520019531" />
                  <Point X="0.833091369629" Y="-4.152340332031" />
                  <Point X="0.734119689941" Y="-3.782973144531" />
                  <Point X="0.655064941406" Y="-3.487936523438" />
                  <Point X="0.652606262207" Y="-3.480124511719" />
                  <Point X="0.642145141602" Y="-3.453580322266" />
                  <Point X="0.62678704834" Y="-3.423815917969" />
                  <Point X="0.620407714844" Y="-3.413210693359" />
                  <Point X="0.588096191406" Y="-3.366655517578" />
                  <Point X="0.456680114746" Y="-3.177310302734" />
                  <Point X="0.446671081543" Y="-3.165173339844" />
                  <Point X="0.424787109375" Y="-3.142717773438" />
                  <Point X="0.41291229248" Y="-3.132399169922" />
                  <Point X="0.386657165527" Y="-3.113155273438" />
                  <Point X="0.373242767334" Y="-3.104937988281" />
                  <Point X="0.345241607666" Y="-3.090829589844" />
                  <Point X="0.330654724121" Y="-3.084938476562" />
                  <Point X="0.280654388428" Y="-3.069420166016" />
                  <Point X="0.077295753479" Y="-3.006305175781" />
                  <Point X="0.063376476288" Y="-3.003109375" />
                  <Point X="0.035217193604" Y="-2.998840087891" />
                  <Point X="0.020976739883" Y="-2.997766601562" />
                  <Point X="-0.008664761543" Y="-2.997766601562" />
                  <Point X="-0.022904916763" Y="-2.99883984375" />
                  <Point X="-0.051064498901" Y="-3.003109130859" />
                  <Point X="-0.064983627319" Y="-3.006305175781" />
                  <Point X="-0.114984237671" Y="-3.021823486328" />
                  <Point X="-0.31834274292" Y="-3.084938476562" />
                  <Point X="-0.332929779053" Y="-3.090829589844" />
                  <Point X="-0.360930938721" Y="-3.104937988281" />
                  <Point X="-0.374345336914" Y="-3.113155273438" />
                  <Point X="-0.400600463867" Y="-3.132399169922" />
                  <Point X="-0.412475128174" Y="-3.142717773438" />
                  <Point X="-0.434358978271" Y="-3.165173095703" />
                  <Point X="-0.444367980957" Y="-3.177309814453" />
                  <Point X="-0.476679626465" Y="-3.223864746094" />
                  <Point X="-0.608095581055" Y="-3.413210205078" />
                  <Point X="-0.612470458984" Y="-3.420132324219" />
                  <Point X="-0.62597668457" Y="-3.445265136719" />
                  <Point X="-0.638777893066" Y="-3.476215820312" />
                  <Point X="-0.642753112793" Y="-3.487936523438" />
                  <Point X="-0.964279418945" Y="-4.687889648438" />
                  <Point X="-0.985425109863" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.315937749281" Y="4.199730977217" />
                  <Point X="-3.011154442408" Y="3.616374906426" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.713102048927" Y="3.02737092869" />
                  <Point X="-3.768351183435" Y="2.981011400302" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.150970317476" Y="4.214141395914" />
                  <Point X="-2.962921677784" Y="3.532833308945" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.625549631785" Y="2.976822437136" />
                  <Point X="-4.024731854461" Y="2.641868781316" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.089539326372" Y="4.141674425406" />
                  <Point X="-2.914688913161" Y="3.449291711465" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.537997214644" Y="2.926273945581" />
                  <Point X="-4.166600921385" Y="2.398812807098" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.420356087041" Y="4.579172142232" />
                  <Point X="-1.475363963961" Y="4.533015052996" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.005851457316" Y="4.087883192978" />
                  <Point X="-2.866456148537" Y="3.365750113985" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.450444797502" Y="2.875725454027" />
                  <Point X="-4.183616404002" Y="2.260521429423" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.19840129106" Y="4.641400637191" />
                  <Point X="-1.462467649406" Y="4.419822653297" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.911852995343" Y="4.042743575264" />
                  <Point X="-2.818223383914" Y="3.282208516504" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.362892381953" Y="2.825176961137" />
                  <Point X="-4.106417975878" Y="2.201284909502" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.97644649508" Y="4.703629132149" />
                  <Point X="-1.495072664468" Y="4.268450104697" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.770104032435" Y="4.037671385273" />
                  <Point X="-2.772797332966" Y="3.196311806614" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.27533997172" Y="2.774628463785" />
                  <Point X="-4.029219547754" Y="2.142048389582" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.794458911701" Y="4.732321153755" />
                  <Point X="-2.752712530684" Y="3.089151264315" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.175254809275" Y="2.734596194192" />
                  <Point X="-3.95202111963" Y="2.082811869662" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.622710045153" Y="4.752421871844" />
                  <Point X="-2.756808627035" Y="2.961700538891" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.040393978995" Y="2.723744174653" />
                  <Point X="-3.874822691506" Y="2.023575349742" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.450961178037" Y="4.772522590409" />
                  <Point X="-3.797624263382" Y="1.964338829822" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.576564826532" Y="1.310730090574" />
                  <Point X="-4.653000007012" Y="1.246593358824" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.351635749563" Y="4.731852828322" />
                  <Point X="-3.720425835258" Y="1.905102309901" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.448814711313" Y="1.29391147265" />
                  <Point X="-4.696495294546" Y="1.08608278661" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.324506107193" Y="4.630603608742" />
                  <Point X="-3.643227407134" Y="1.845865789981" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.321064596095" Y="1.277092854726" />
                  <Point X="-4.735502763909" Y="0.929337940968" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.297376464824" Y="4.529354389162" />
                  <Point X="-3.56602897901" Y="1.786629270061" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.193314480877" Y="1.260274236802" />
                  <Point X="-4.756455285381" Y="0.787742995441" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.270246822454" Y="4.428105169581" />
                  <Point X="-3.497203554547" Y="1.720366965857" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.065564365659" Y="1.243455618877" />
                  <Point X="-4.777407806853" Y="0.646148049915" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.243117253952" Y="4.326855888019" />
                  <Point X="-3.449618293291" Y="1.63628204854" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.937814294767" Y="1.22663696376" />
                  <Point X="-4.716401866957" Y="0.573324419095" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.442635611836" Y="4.778257172293" />
                  <Point X="0.357164870041" Y="4.706538704377" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.212151549305" Y="4.22882550688" />
                  <Point X="-3.421586756627" Y="1.53578960813" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.810064292454" Y="1.209818251096" />
                  <Point X="-4.604379987382" Y="0.543308244444" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.574030181572" Y="4.764496614811" />
                  <Point X="0.314297401494" Y="4.546554934843" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.157680734163" Y="4.15051825529" />
                  <Point X="-3.460271058494" Y="1.379315932214" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.645622150918" Y="1.223787898923" />
                  <Point X="-4.492358104801" Y="0.513292072315" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.705424751309" Y="4.75073605733" />
                  <Point X="0.271429932947" Y="4.386571165309" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.070873774129" Y="4.099344250951" />
                  <Point X="-4.380336222221" Y="0.483275900185" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.835687870074" Y="4.736026099755" />
                  <Point X="0.220312901282" Y="4.219665190406" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.07266290674" Y="4.095772134442" />
                  <Point X="-4.26831433964" Y="0.453259728056" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.950459260803" Y="4.708317038898" />
                  <Point X="-4.15629245706" Y="0.423243555926" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.065230252234" Y="4.680607642992" />
                  <Point X="-4.044270574479" Y="0.393227383797" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.180001243665" Y="4.652898247085" />
                  <Point X="-3.932248691899" Y="0.363211211667" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.294772235096" Y="4.625188851178" />
                  <Point X="-3.820226809318" Y="0.333195039538" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.409543226527" Y="4.597479455271" />
                  <Point X="-3.708204926738" Y="0.303178867408" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.517178785881" Y="4.56378272094" />
                  <Point X="-3.596183044157" Y="0.273162695279" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.662251491664" Y="-0.621374945834" />
                  <Point X="-4.777393738679" Y="-0.717990762837" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.620368151087" Y="4.526355186739" />
                  <Point X="-3.486269916309" Y="0.241377067831" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.445121823084" Y="-0.563195213497" />
                  <Point X="-4.761557675007" Y="-0.828716420137" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.723557459184" Y="4.488927604618" />
                  <Point X="-3.399460417863" Y="0.190205193473" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.227992154504" Y="-0.50501548116" />
                  <Point X="-4.745721608349" Y="-0.939442074932" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.826746767281" Y="4.451500022497" />
                  <Point X="-3.336395159511" Y="0.11910953601" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.010862485923" Y="-0.446835748824" />
                  <Point X="-4.72284573004" Y="-1.044260626367" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.924178340443" Y="4.409241127116" />
                  <Point X="-3.300820518124" Y="0.024946511991" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.793732813813" Y="-0.388656013525" />
                  <Point X="-4.696759931303" Y="-1.146385734754" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.019079559739" Y="4.364859012739" />
                  <Point X="-3.304020360762" Y="-0.101752167273" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.576603041348" Y="-0.330476194019" />
                  <Point X="-4.670674132566" Y="-1.248510843141" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.113980801283" Y="4.32047691703" />
                  <Point X="-4.551165456187" Y="-1.272244849355" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.208882042827" Y="4.276094821321" />
                  <Point X="-4.375867977612" Y="-1.249166492224" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.299471831948" Y="4.228094987475" />
                  <Point X="-4.20057049915" Y="-1.226088135186" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.386700799535" Y="4.177275089518" />
                  <Point X="-4.025273020688" Y="-1.203009778149" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.473929769753" Y="4.12645519377" />
                  <Point X="-3.849975542226" Y="-1.179931421112" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.561158739972" Y="4.075635298022" />
                  <Point X="-3.674678063764" Y="-1.156853064075" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.646815950621" Y="4.023496539399" />
                  <Point X="-3.499380585302" Y="-1.133774707038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.726812165878" Y="3.96660764163" />
                  <Point X="-3.324083106841" Y="-1.110696350001" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.806808391168" Y="3.909718752279" />
                  <Point X="-3.169361290339" Y="-1.104883023326" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.697946417477" Y="3.69435901782" />
                  <Point X="-3.068877882917" Y="-1.144581125705" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.559065714731" Y="3.453810578881" />
                  <Point X="-2.996843492208" Y="-1.208150787516" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.420185011984" Y="3.213262139942" />
                  <Point X="-2.954918383394" Y="-1.29698513666" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.281304309238" Y="2.972713701004" />
                  <Point X="-2.956330703073" Y="-1.422183906067" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.088580313957" Y="-2.372254136961" />
                  <Point X="-4.178621140543" Y="-2.447807361341" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.142423752441" Y="2.732165384531" />
                  <Point X="-4.129330111101" Y="-2.530460969202" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.035337478793" Y="2.518295639322" />
                  <Point X="-4.080039081659" Y="-2.613114577063" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.013746311028" Y="2.376164805927" />
                  <Point X="-4.030748126829" Y="-2.695768247532" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.029642571437" Y="2.265489659687" />
                  <Point X="-3.977652820432" Y="-2.775229688003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.072682458442" Y="2.177590720513" />
                  <Point X="-3.920049576607" Y="-2.850908519842" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.129694464231" Y="2.101415781056" />
                  <Point X="-3.862446332783" Y="-2.92658735168" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.205961431965" Y="2.041397673066" />
                  <Point X="-3.804843088958" Y="-3.002266183519" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.297223127732" Y="1.993961635838" />
                  <Point X="-3.415977383376" Y="-2.799982805874" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.422849319133" Y="1.975360834222" />
                  <Point X="-2.942189349813" Y="-2.526441134142" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.6288036674" Y="2.024163359406" />
                  <Point X="-2.583212571269" Y="-2.349237544151" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.100814239081" Y="2.296213563529" />
                  <Point X="-2.463662977008" Y="-2.372937216186" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.574602074014" Y="2.569755068591" />
                  <Point X="-2.383966402726" Y="-2.430077542586" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.953086115986" Y="2.763327196129" />
                  <Point X="-2.331202037897" Y="-2.509816676006" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.008725669857" Y="2.686000632775" />
                  <Point X="-2.311577638205" Y="-2.617363541948" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.062212862731" Y="2.606868024101" />
                  <Point X="-2.3889225533" Y="-2.806277324165" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.111985309209" Y="2.524618373098" />
                  <Point X="3.272491761903" Y="1.820199647177" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.132058343359" Y="1.702362017472" />
                  <Point X="-2.527803348999" Y="-3.0468258411" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.011927046758" Y="1.477546198315" />
                  <Point X="-2.66668412992" Y="-3.287374345635" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.005862859053" Y="1.348444048161" />
                  <Point X="-2.805564910841" Y="-3.52792285017" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.029491502176" Y="1.244257141405" />
                  <Point X="-2.944445691761" Y="-3.768471354704" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.073988355429" Y="1.157580742072" />
                  <Point X="-2.924000642087" Y="-3.87532961355" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.129850000517" Y="1.080440535375" />
                  <Point X="-2.84692884512" Y="-3.934672389627" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.207890459775" Y="1.021910563469" />
                  <Point X="-2.769857001043" Y="-3.994015126175" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.3014930931" Y="0.976438806083" />
                  <Point X="-2.687118615913" Y="-4.048603070214" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.423403266767" Y="0.954719895357" />
                  <Point X="-2.602077357981" Y="-4.101258674535" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.581616410223" Y="0.963462793191" />
                  <Point X="-2.18706600502" Y="-3.877036493818" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.45973897365" Y="-4.105836281227" />
                  <Point X="-2.517036100049" Y="-4.153914278856" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.756913886284" Y="0.986541148214" />
                  <Point X="-1.920722376717" Y="-3.777561346029" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.932211362346" Y="1.009619503237" />
                  <Point X="-1.783636528387" Y="-3.786546353742" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.107508838407" Y="1.03269785826" />
                  <Point X="-1.646550798503" Y="-3.795531460843" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.282806314468" Y="1.055776213282" />
                  <Point X="-1.53696813172" Y="-3.827594378049" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.458103790529" Y="1.078854568305" />
                  <Point X="3.661101999551" Y="0.410090659447" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.405763298781" Y="0.195836049806" />
                  <Point X="-1.458975930485" Y="-3.886164843245" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.633401266591" Y="1.101932923328" />
                  <Point X="3.883458400484" Y="0.472656140974" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.354781729881" Y="0.029043741659" />
                  <Point X="-1.386710185144" Y="-3.949540375469" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.719576170703" Y="1.050228561099" />
                  <Point X="4.100587963015" Y="0.530835784324" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.355379172786" Y="-0.094468636706" />
                  <Point X="-0.144061467956" Y="-3.03084798768" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.616148013254" Y="-3.426975633724" />
                  <Point X="-1.314444439804" Y="-4.012915907694" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.744645819619" Y="0.947250801772" />
                  <Point X="4.31771758161" Y="0.589015474719" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.378077908543" Y="-0.199435828392" />
                  <Point X="0.040858525469" Y="-2.999695381887" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.672265216654" Y="-3.598077250885" />
                  <Point X="-1.244331615583" Y="-4.078097955236" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.764285610068" Y="0.839716850207" />
                  <Point X="4.534847200205" Y="0.647195165113" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.424263909926" Y="-0.284694864152" />
                  <Point X="0.152834908734" Y="-3.029749732475" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.715132725644" Y="-3.758061054355" />
                  <Point X="-1.194852762774" Y="-4.160593960579" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.781362640474" Y="0.730032487636" />
                  <Point X="4.7519768188" Y="0.705374855507" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.48385108227" Y="-0.358708982302" />
                  <Point X="0.260723318667" Y="-3.063234299978" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.758000234634" Y="-3.918044857825" />
                  <Point X="-1.169886093403" Y="-4.263658130005" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.561217470386" Y="-0.417804567055" />
                  <Point X="0.364229012193" Y="-3.100396403202" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.800867743625" Y="-4.078028661295" />
                  <Point X="-1.148746505624" Y="-4.369933602183" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.649686335411" Y="-0.467584067528" />
                  <Point X="0.441341758575" Y="-3.159704818641" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.843735252615" Y="-4.238012464765" />
                  <Point X="-1.127606979723" Y="-4.476209126283" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.757197951665" Y="-0.501384802469" />
                  <Point X="3.236471489071" Y="-0.938326185175" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.896008427726" Y="-1.224008614379" />
                  <Point X="1.175132709984" Y="-2.667994794439" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.786259232718" Y="-2.994298385787" />
                  <Point X="0.497707143863" Y="-3.236422337121" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.886602761605" Y="-4.397996268234" />
                  <Point X="-1.123536318005" Y="-4.596807128023" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.869219829807" Y="-0.531400978322" />
                  <Point X="3.419946850283" Y="-0.908385769739" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.868290555574" Y="-1.371280363166" />
                  <Point X="1.341148949168" Y="-2.652704321857" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.752063531267" Y="-3.147005678749" />
                  <Point X="0.552101339641" Y="-3.314793879992" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.929470270596" Y="-4.557980071704" />
                  <Point X="-1.137276309806" Y="-4.732350042562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.981241707949" Y="-0.561417154176" />
                  <Point X="3.549086049807" Y="-0.924038807534" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.8606520114" Y="-1.501703555251" />
                  <Point X="1.486309377656" Y="-2.654913952337" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.724726749155" Y="-3.293957655023" />
                  <Point X="0.6064954096" Y="-3.393165528438" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.972337822974" Y="-4.717963911581" />
                  <Point X="-1.022069071198" Y="-4.759693383624" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.093263586091" Y="-0.59143333003" />
                  <Point X="3.676836335278" Y="-0.940857282599" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.8879957022" Y="-1.602773166872" />
                  <Point X="2.274784236044" Y="-2.117318681958" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.119884699513" Y="-2.24729482593" />
                  <Point X="1.582657360934" Y="-2.698082087591" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.735247669327" Y="-3.409143247274" />
                  <Point X="0.652121475058" Y="-3.478894406226" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.205285464233" Y="-0.621449505883" />
                  <Point X="3.804586512191" Y="-0.957675848755" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.938404724179" Y="-1.684488667609" />
                  <Point X="2.531753663304" Y="-2.025709422807" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.012962028622" Y="-2.461027292126" />
                  <Point X="1.666336875173" Y="-2.751880330542" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.749950192545" Y="-3.520820057951" />
                  <Point X="0.679676169402" Y="-3.579786964852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.317307342376" Y="-0.651465681737" />
                  <Point X="3.932336600271" Y="-0.974494489451" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.990194977221" Y="-1.765045177869" />
                  <Point X="2.655920336735" Y="-2.045534905413" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.004374089411" Y="-2.592247121238" />
                  <Point X="1.750016384474" Y="-2.805678577637" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.764652715764" Y="-3.632496868627" />
                  <Point X="0.706805793998" Y="-3.681036199346" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.429329220518" Y="-0.681481857591" />
                  <Point X="4.060086688351" Y="-0.991313130147" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.052953352113" Y="-1.83639834113" />
                  <Point X="2.777135209496" Y="-2.067837242873" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.023823379145" Y="-2.699940921881" />
                  <Point X="1.825218381167" Y="-2.866590302434" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.779355238982" Y="-3.744173679304" />
                  <Point X="0.733935418594" Y="-3.78228543384" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.54135109866" Y="-0.711498033445" />
                  <Point X="4.187836776431" Y="-1.008131770843" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.130151816209" Y="-1.895634830867" />
                  <Point X="2.874813594785" Y="-2.109889038289" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.043887077221" Y="-2.807119172712" />
                  <Point X="1.883806239276" Y="-2.94144294479" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.794057762201" Y="-3.855850489981" />
                  <Point X="0.7610650732" Y="-3.883534643152" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.653372976802" Y="-0.741514209298" />
                  <Point X="4.315586864511" Y="-1.024950411538" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.207350280305" Y="-1.954871320603" />
                  <Point X="2.962366055723" Y="-2.160437493094" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.083557225265" Y="-2.897845658606" />
                  <Point X="1.941693682475" Y="-3.016883305039" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.808760285419" Y="-3.967527300657" />
                  <Point X="0.788194728012" Y="-3.984783852293" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.765394814985" Y="-0.771530418682" />
                  <Point X="4.443336952591" Y="-1.041769052234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.284548744401" Y="-2.014107810339" />
                  <Point X="3.04991851666" Y="-2.210985947899" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.131790010793" Y="-2.981387238546" />
                  <Point X="1.999581125673" Y="-3.092323665288" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.823462808638" Y="-4.079204111334" />
                  <Point X="0.815324382824" Y="-4.086033061433" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.767194671407" Y="-0.894033852309" />
                  <Point X="4.571087040671" Y="-1.05858769293" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.361747208497" Y="-2.073344300075" />
                  <Point X="3.137470977923" Y="-2.261534402432" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.180022796321" Y="-3.064928818485" />
                  <Point X="2.05746858105" Y="-3.167764015318" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.735737772704" Y="-1.044443016895" />
                  <Point X="4.698837128751" Y="-1.075406333626" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.438945672593" Y="-2.132580789812" />
                  <Point X="3.225023440047" Y="-2.312082856242" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.228255581849" Y="-3.148470398424" />
                  <Point X="2.115356057299" Y="-3.243204347835" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.516144136688" Y="-2.191817279548" />
                  <Point X="3.312575902171" Y="-2.362631310051" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.276488367377" Y="-3.232011978364" />
                  <Point X="2.173243533548" Y="-3.318644680351" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.593342600784" Y="-2.251053769284" />
                  <Point X="3.400128364294" Y="-2.413179763861" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.324721152905" Y="-3.315553558303" />
                  <Point X="2.231131009797" Y="-3.394085012867" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.67054106488" Y="-2.310290259021" />
                  <Point X="3.487680826418" Y="-2.463728217671" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.372953938433" Y="-3.399095138242" />
                  <Point X="2.289018486046" Y="-3.469525345383" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.747739528976" Y="-2.369526748757" />
                  <Point X="3.575233288542" Y="-2.514276671481" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.421186723961" Y="-3.482636718182" />
                  <Point X="2.346905962295" Y="-3.5449656779" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.824937993072" Y="-2.428763238493" />
                  <Point X="3.662785750666" Y="-2.564825125291" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.469419509489" Y="-3.566178298121" />
                  <Point X="2.404793438544" Y="-3.620406010416" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.902136457168" Y="-2.487999728229" />
                  <Point X="3.75033821279" Y="-2.6153735791" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.517652295017" Y="-3.649719878061" />
                  <Point X="2.462680914793" Y="-3.695846342932" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.979334921264" Y="-2.547236217966" />
                  <Point X="3.837890674914" Y="-2.66592203291" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.565885080545" Y="-3.733261458" />
                  <Point X="2.520568391042" Y="-3.771286675449" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.056533329117" Y="-2.606472754895" />
                  <Point X="3.925443137037" Y="-2.71647048672" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.614117866073" Y="-3.816803037939" />
                  <Point X="2.578455867291" Y="-3.846727007965" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.662350651601" Y="-3.900344617879" />
                  <Point X="2.63634334354" Y="-3.922167340481" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.71058337282" Y="-3.98388625178" />
                  <Point X="2.694230819789" Y="-3.997607672997" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="0.001626220703" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.55059387207" Y="-3.832148925781" />
                  <Point X="0.471539031982" Y="-3.537112304688" />
                  <Point X="0.464318664551" Y="-3.521544189453" />
                  <Point X="0.432007049561" Y="-3.474989013672" />
                  <Point X="0.300591033936" Y="-3.285643798828" />
                  <Point X="0.274335754395" Y="-3.266399902344" />
                  <Point X="0.224335281372" Y="-3.250881591797" />
                  <Point X="0.02097677803" Y="-3.187766601562" />
                  <Point X="-0.008664756775" Y="-3.187766601562" />
                  <Point X="-0.058665233612" Y="-3.203284912109" />
                  <Point X="-0.262023895264" Y="-3.266399902344" />
                  <Point X="-0.288279144287" Y="-3.285643798828" />
                  <Point X="-0.320590789795" Y="-3.332198730469" />
                  <Point X="-0.45200680542" Y="-3.521544189453" />
                  <Point X="-0.459227142334" Y="-3.537112304688" />
                  <Point X="-0.780753540039" Y="-4.737065429688" />
                  <Point X="-0.847743896484" Y="-4.987076660156" />
                  <Point X="-0.861137451172" Y="-4.984477050781" />
                  <Point X="-1.100246459961" Y="-4.938065429688" />
                  <Point X="-1.157142944336" Y="-4.923426269531" />
                  <Point X="-1.351589599609" Y="-4.873396972656" />
                  <Point X="-1.318666625977" Y="-4.623322265625" />
                  <Point X="-1.309150146484" Y="-4.551038574219" />
                  <Point X="-1.309683227539" Y="-4.5347578125" />
                  <Point X="-1.321628417969" Y="-4.474705566406" />
                  <Point X="-1.370210693359" Y="-4.230466308594" />
                  <Point X="-1.386282714844" Y="-4.20262890625" />
                  <Point X="-1.432316894531" Y="-4.1622578125" />
                  <Point X="-1.619543457031" Y="-3.998063964844" />
                  <Point X="-1.649240722656" Y="-3.985762939453" />
                  <Point X="-1.710338256836" Y="-3.981758300781" />
                  <Point X="-1.958829833984" Y="-3.965471435547" />
                  <Point X="-1.989878540039" Y="-3.973791015625" />
                  <Point X="-2.040788330078" Y="-4.007807861328" />
                  <Point X="-2.247844970703" Y="-4.146159179688" />
                  <Point X="-2.259734130859" Y="-4.157294433594" />
                  <Point X="-2.440246337891" Y="-4.392542480469" />
                  <Point X="-2.457095214844" Y="-4.414500488281" />
                  <Point X="-2.505347412109" Y="-4.384624023438" />
                  <Point X="-2.855832275391" Y="-4.167612304688" />
                  <Point X="-2.934620849609" Y="-4.106947753906" />
                  <Point X="-3.228581054688" Y="-3.880608154297" />
                  <Point X="-2.652948242188" Y="-2.883583007812" />
                  <Point X="-2.506033691406" Y="-2.629119628906" />
                  <Point X="-2.499762695312" Y="-2.597593261719" />
                  <Point X="-2.513979736328" Y="-2.568764404297" />
                  <Point X="-2.531328369141" Y="-2.551415771484" />
                  <Point X="-2.560157226562" Y="-2.537198730469" />
                  <Point X="-2.59168359375" Y="-2.543469726562" />
                  <Point X="-3.626619384766" Y="-3.140990234375" />
                  <Point X="-3.842959472656" Y="-3.265894042969" />
                  <Point X="-3.884800537109" Y="-3.210923339844" />
                  <Point X="-4.161703613281" Y="-2.847129394531" />
                  <Point X="-4.218187011719" Y="-2.752415527344" />
                  <Point X="-4.431020019531" Y="-2.395527099609" />
                  <Point X="-3.420839599609" Y="-1.620388427734" />
                  <Point X="-3.163786865234" Y="-1.423144897461" />
                  <Point X="-3.152535644531" Y="-1.411081542969" />
                  <Point X="-3.144301513672" Y="-1.390143066406" />
                  <Point X="-3.138117431641" Y="-1.366266235352" />
                  <Point X="-3.136651855469" Y="-1.350051147461" />
                  <Point X="-3.148656982422" Y="-1.321068481445" />
                  <Point X="-3.166385498047" Y="-1.307563232422" />
                  <Point X="-3.187641845703" Y="-1.295052734375" />
                  <Point X="-3.219529296875" Y="-1.288571044922" />
                  <Point X="-4.526037597656" Y="-1.460576171875" />
                  <Point X="-4.803283691406" Y="-1.497076293945" />
                  <Point X="-4.819094726562" Y="-1.435176147461" />
                  <Point X="-4.927393066406" Y="-1.011191650391" />
                  <Point X="-4.942336914062" Y="-0.906705078125" />
                  <Point X="-4.998396484375" Y="-0.514742126465" />
                  <Point X="-3.850285644531" Y="-0.207106719971" />
                  <Point X="-3.557463867188" Y="-0.128645431519" />
                  <Point X="-3.536418701172" Y="-0.117623695374" />
                  <Point X="-3.514142822266" Y="-0.102162895203" />
                  <Point X="-3.50232421875" Y="-0.090645294189" />
                  <Point X="-3.493073242188" Y="-0.070025283813" />
                  <Point X="-3.485647949219" Y="-0.046100738525" />
                  <Point X="-3.483400878906" Y="-0.031280042648" />
                  <Point X="-3.487473632812" Y="-0.01057686615" />
                  <Point X="-3.494898925781" Y="0.013347681046" />
                  <Point X="-3.50232421875" Y="0.028085227966" />
                  <Point X="-3.519619873047" Y="0.043404342651" />
                  <Point X="-3.541895751953" Y="0.05886498642" />
                  <Point X="-3.557463867188" Y="0.066085334778" />
                  <Point X="-4.748408203125" Y="0.385197967529" />
                  <Point X="-4.998186523438" Y="0.452125823975" />
                  <Point X="-4.987442382813" Y="0.524735595703" />
                  <Point X="-4.917645019531" Y="0.996418212891" />
                  <Point X="-4.8875625" Y="1.107433227539" />
                  <Point X="-4.773516113281" Y="1.528298583984" />
                  <Point X="-3.963168212891" Y="1.421614379883" />
                  <Point X="-3.753266357422" Y="1.393980102539" />
                  <Point X="-3.731704589844" Y="1.395866699219" />
                  <Point X="-3.719582275391" Y="1.399688842773" />
                  <Point X="-3.670278564453" Y="1.41523425293" />
                  <Point X="-3.651534423828" Y="1.426056274414" />
                  <Point X="-3.639119873047" Y="1.443786132813" />
                  <Point X="-3.634255615234" Y="1.455529174805" />
                  <Point X="-3.614472412109" Y="1.503290283203" />
                  <Point X="-3.610714111328" Y="1.52460534668" />
                  <Point X="-3.616315917969" Y="1.54551171875" />
                  <Point X="-3.622185058594" Y="1.556786376953" />
                  <Point X="-3.646055664062" Y="1.602641479492" />
                  <Point X="-3.659968261719" Y="1.619221801758" />
                  <Point X="-4.343096679688" Y="2.143404785156" />
                  <Point X="-4.47610546875" Y="2.245466308594" />
                  <Point X="-4.431221191406" Y="2.322364257812" />
                  <Point X="-4.160014648438" Y="2.787006591797" />
                  <Point X="-4.080327636719" Y="2.889432861328" />
                  <Point X="-3.774671142578" Y="3.282310791016" />
                  <Point X="-3.288072753906" Y="3.001373046875" />
                  <Point X="-3.159156982422" Y="2.926943359375" />
                  <Point X="-3.138514892578" Y="2.920434814453" />
                  <Point X="-3.121631591797" Y="2.918957763672" />
                  <Point X="-3.052965576172" Y="2.912950195312" />
                  <Point X="-3.031506835938" Y="2.915775390625" />
                  <Point X="-3.013252441406" Y="2.927404541016" />
                  <Point X="-3.001268554688" Y="2.939388427734" />
                  <Point X="-2.952528808594" Y="2.988127929688" />
                  <Point X="-2.940899414062" Y="3.006382324219" />
                  <Point X="-2.93807421875" Y="3.027841308594" />
                  <Point X="-2.939551269531" Y="3.044724609375" />
                  <Point X="-2.945558837891" Y="3.113390869141" />
                  <Point X="-2.952067382812" Y="3.134032958984" />
                  <Point X="-3.254782470703" Y="3.658350585938" />
                  <Point X="-3.307278564453" Y="3.749276611328" />
                  <Point X="-3.225232421875" Y="3.812180664062" />
                  <Point X="-2.752876220703" Y="4.174331542969" />
                  <Point X="-2.627372070312" Y="4.244059082031" />
                  <Point X="-2.141548583984" Y="4.513972167969" />
                  <Point X="-2.007292114258" Y="4.339005371094" />
                  <Point X="-1.967826904297" Y="4.287573242188" />
                  <Point X="-1.951246948242" Y="4.273660644531" />
                  <Point X="-1.932456054688" Y="4.26387890625" />
                  <Point X="-1.856030761719" Y="4.22409375" />
                  <Point X="-1.835124267578" Y="4.2184921875" />
                  <Point X="-1.813808959961" Y="4.222250488281" />
                  <Point X="-1.794236938477" Y="4.230357910156" />
                  <Point X="-1.714635009766" Y="4.263330078125" />
                  <Point X="-1.696905273438" Y="4.275744628906" />
                  <Point X="-1.686083496094" Y="4.294488769531" />
                  <Point X="-1.679713012695" Y="4.314692871094" />
                  <Point X="-1.653804077148" Y="4.396865234375" />
                  <Point X="-1.651917602539" Y="4.418426757812" />
                  <Point X="-1.686332885742" Y="4.679836914062" />
                  <Point X="-1.689137695313" Y="4.701141113281" />
                  <Point X="-1.579588500977" Y="4.731854980469" />
                  <Point X="-0.968094177246" Y="4.903296386719" />
                  <Point X="-0.81594720459" Y="4.921103027344" />
                  <Point X="-0.224199615479" Y="4.990358398438" />
                  <Point X="-0.080277153015" Y="4.453232421875" />
                  <Point X="-0.042140380859" Y="4.310903320312" />
                  <Point X="-0.024282129288" Y="4.284176757813" />
                  <Point X="0.006155934334" Y="4.273844238281" />
                  <Point X="0.036594024658" Y="4.284176757813" />
                  <Point X="0.054452251434" Y="4.310903320312" />
                  <Point X="0.209559921265" Y="4.8897734375" />
                  <Point X="0.236648361206" Y="4.990868652344" />
                  <Point X="0.326282104492" Y="4.981481445312" />
                  <Point X="0.860205871582" Y="4.925565429688" />
                  <Point X="0.986082580566" Y="4.895175292969" />
                  <Point X="1.508456054688" Y="4.769057617188" />
                  <Point X="1.590171508789" Y="4.739419433594" />
                  <Point X="1.931033569336" Y="4.615786132813" />
                  <Point X="2.010261108398" Y="4.578733886719" />
                  <Point X="2.338699462891" Y="4.425134277344" />
                  <Point X="2.415241943359" Y="4.380540527344" />
                  <Point X="2.732523681641" Y="4.19569140625" />
                  <Point X="2.804708496094" Y="4.144357421875" />
                  <Point X="3.068740234375" Y="3.956592773438" />
                  <Point X="2.399895996094" Y="2.798120361328" />
                  <Point X="2.229853515625" Y="2.503597900391" />
                  <Point X="2.220614990234" Y="2.475670166016" />
                  <Point X="2.203382324219" Y="2.411228271484" />
                  <Point X="2.203696777344" Y="2.378625" />
                  <Point X="2.210416015625" Y="2.322901123047" />
                  <Point X="2.218682128906" Y="2.3008125" />
                  <Point X="2.227159912109" Y="2.288318603516" />
                  <Point X="2.261639892578" Y="2.23750390625" />
                  <Point X="2.287433837891" Y="2.215725830078" />
                  <Point X="2.338248535156" Y="2.18124609375" />
                  <Point X="2.360336914062" Y="2.172980224609" />
                  <Point X="2.374037841797" Y="2.171328125" />
                  <Point X="2.42976171875" Y="2.164608642578" />
                  <Point X="2.464508789062" Y="2.170183349609" />
                  <Point X="2.528950683594" Y="2.187416015625" />
                  <Point X="2.541033935547" Y="2.192417480469" />
                  <Point X="3.738894775391" Y="2.884002685547" />
                  <Point X="3.994248046875" Y="3.031430908203" />
                  <Point X="4.014391113281" Y="3.003436523438" />
                  <Point X="4.202591796875" Y="2.741880126953" />
                  <Point X="4.242833984375" Y="2.675379638672" />
                  <Point X="4.387512695312" Y="2.436296142578" />
                  <Point X="3.512493164062" Y="1.764869628906" />
                  <Point X="3.288616210938" Y="1.593082763672" />
                  <Point X="3.267968017578" Y="1.568956665039" />
                  <Point X="3.221589111328" Y="1.508451904297" />
                  <Point X="3.208871826172" Y="1.476311035156" />
                  <Point X="3.191595703125" Y="1.414535522461" />
                  <Point X="3.190779541016" Y="1.390965209961" />
                  <Point X="3.194266357422" Y="1.374065551758" />
                  <Point X="3.208448486328" Y="1.305332397461" />
                  <Point X="3.225130615234" Y="1.273539550781" />
                  <Point X="3.263704101562" Y="1.214909667969" />
                  <Point X="3.280947998047" Y="1.198819946289" />
                  <Point X="3.294691894531" Y="1.191083374023" />
                  <Point X="3.350590087891" Y="1.159617675781" />
                  <Point X="3.387147949219" Y="1.151163818359" />
                  <Point X="3.462726074219" Y="1.141175048828" />
                  <Point X="3.475803710938" Y="1.14117175293" />
                  <Point X="4.61369140625" Y="1.290977539062" />
                  <Point X="4.848975097656" Y="1.32195324707" />
                  <Point X="4.858357421875" Y="1.283414550781" />
                  <Point X="4.939188476562" Y="0.951385986328" />
                  <Point X="4.951870117188" Y="0.869934204102" />
                  <Point X="4.997858398438" Y="0.574556152344" />
                  <Point X="3.997735595703" Y="0.306574066162" />
                  <Point X="3.741167724609" Y="0.237826919556" />
                  <Point X="3.729086914062" Y="0.232819412231" />
                  <Point X="3.710830078125" Y="0.222266494751" />
                  <Point X="3.636576904297" Y="0.179346878052" />
                  <Point X="3.611310791016" Y="0.152968383789" />
                  <Point X="3.566759033203" Y="0.096198890686" />
                  <Point X="3.556985351562" Y="0.074735275269" />
                  <Point X="3.553333984375" Y="0.055669174194" />
                  <Point X="3.538483154297" Y="-0.021875303268" />
                  <Point X="3.542134521484" Y="-0.059750740051" />
                  <Point X="3.556985107422" Y="-0.137295227051" />
                  <Point X="3.566759033203" Y="-0.158758987427" />
                  <Point X="3.577713134766" Y="-0.172717178345" />
                  <Point X="3.622264892578" Y="-0.229486679077" />
                  <Point X="3.636576904297" Y="-0.241906967163" />
                  <Point X="3.654833740234" Y="-0.252459884644" />
                  <Point X="3.729086914062" Y="-0.295379486084" />
                  <Point X="3.741167724609" Y="-0.300387023926" />
                  <Point X="4.784662109375" Y="-0.579990478516" />
                  <Point X="4.998067871094" Y="-0.637172424316" />
                  <Point X="4.993564453125" Y="-0.667045227051" />
                  <Point X="4.948431640625" Y="-0.966399169922" />
                  <Point X="4.932184570312" Y="-1.037596801758" />
                  <Point X="4.874545898438" Y="-1.290178344727" />
                  <Point X="3.709528808594" Y="-1.13680090332" />
                  <Point X="3.411982177734" Y="-1.097628295898" />
                  <Point X="3.394836181641" Y="-1.098341186523" />
                  <Point X="3.359004394531" Y="-1.106129394531" />
                  <Point X="3.213271972656" Y="-1.137804931641" />
                  <Point X="3.1854453125" Y="-1.154697265625" />
                  <Point X="3.163787109375" Y="-1.180745239258" />
                  <Point X="3.075701171875" Y="-1.286685424805" />
                  <Point X="3.064357910156" Y="-1.314070678711" />
                  <Point X="3.061253662109" Y="-1.347803833008" />
                  <Point X="3.04862890625" Y="-1.485000854492" />
                  <Point X="3.056360595703" Y="-1.516621826172" />
                  <Point X="3.076190429688" Y="-1.547465820312" />
                  <Point X="3.156840820312" Y="-1.672912353516" />
                  <Point X="3.168460693359" Y="-1.685540649414" />
                  <Point X="4.136830078125" Y="-2.428596679688" />
                  <Point X="4.33907421875" Y="-2.583783935547" />
                  <Point X="4.331356445312" Y="-2.596272460938" />
                  <Point X="4.204133300781" Y="-2.802139160156" />
                  <Point X="4.170530761719" Y="-2.849883544922" />
                  <Point X="4.056688476562" Y="-3.011638427734" />
                  <Point X="3.018459960938" Y="-2.412216552734" />
                  <Point X="2.753454589844" Y="-2.259215576172" />
                  <Point X="2.737340820313" Y="-2.253312744141" />
                  <Point X="2.6946953125" Y="-2.245611083984" />
                  <Point X="2.521250488281" Y="-2.214287109375" />
                  <Point X="2.489077636719" Y="-2.219244873047" />
                  <Point X="2.453649658203" Y="-2.237890380859" />
                  <Point X="2.309559570312" Y="-2.313723876953" />
                  <Point X="2.288599853516" Y="-2.33468359375" />
                  <Point X="2.269954345703" Y="-2.370111572266" />
                  <Point X="2.194120849609" Y="-2.514201660156" />
                  <Point X="2.189163085938" Y="-2.546374511719" />
                  <Point X="2.196864746094" Y="-2.589020019531" />
                  <Point X="2.228188720703" Y="-2.76246484375" />
                  <Point X="2.234091552734" Y="-2.778578613281" />
                  <Point X="2.856366455078" Y="-3.856389648438" />
                  <Point X="2.986673828125" Y="-4.082088867188" />
                  <Point X="2.986260742188" Y="-4.082383789062" />
                  <Point X="2.835298339844" Y="-4.190212402344" />
                  <Point X="2.797732177734" Y="-4.214528320312" />
                  <Point X="2.679775878906" Y="-4.290879394531" />
                  <Point X="1.885401000977" Y="-3.255630126953" />
                  <Point X="1.683177490234" Y="-2.992087402344" />
                  <Point X="1.670549316406" Y="-2.980467529297" />
                  <Point X="1.628489257812" Y="-2.953426757812" />
                  <Point X="1.45742590332" Y="-2.843448730469" />
                  <Point X="1.4258046875" Y="-2.835717041016" />
                  <Point X="1.379804931641" Y="-2.839949951172" />
                  <Point X="1.192717773438" Y="-2.857166015625" />
                  <Point X="1.165332885742" Y="-2.868509277344" />
                  <Point X="1.129813232422" Y="-2.898042724609" />
                  <Point X="0.985349060059" Y="-3.018159667969" />
                  <Point X="0.968456726074" Y="-3.045986328125" />
                  <Point X="0.957836547852" Y="-3.094847900391" />
                  <Point X="0.91464251709" Y="-3.29357421875" />
                  <Point X="0.91392956543" Y="-3.310720214844" />
                  <Point X="1.09027734375" Y="-4.650215332031" />
                  <Point X="1.127642211914" Y="-4.934028808594" />
                  <Point X="0.994345458984" Y="-4.963247070312" />
                  <Point X="0.959638671875" Y="-4.969552246094" />
                  <Point X="0.860200378418" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#136" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.038136890874" Y="4.497453295221" Z="0.5" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.5" />
                  <Point X="-0.827915273868" Y="5.00204409515" Z="0.5" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.5" />
                  <Point X="-1.599242271024" Y="4.811278434743" Z="0.5" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.5" />
                  <Point X="-1.742061162791" Y="4.704590751138" Z="0.5" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.5" />
                  <Point X="-1.733554822718" Y="4.361008135229" Z="0.5" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.5" />
                  <Point X="-1.819527040381" Y="4.307831638657" Z="0.5" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.5" />
                  <Point X="-1.915524258653" Y="4.339509307679" Z="0.5" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.5" />
                  <Point X="-1.973780258798" Y="4.400723208351" Z="0.5" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.5" />
                  <Point X="-2.657810906068" Y="4.319046421952" Z="0.5" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.5" />
                  <Point X="-3.261810535445" Y="3.883140741479" Z="0.5" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.5" />
                  <Point X="-3.304239630436" Y="3.66463054233" Z="0.5" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.5" />
                  <Point X="-2.995517115832" Y="3.071646754582" Z="0.5" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.5" />
                  <Point X="-3.042779740196" Y="3.00602382769" Z="0.5" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.5" />
                  <Point X="-3.123429711025" Y="3.000047583341" Z="0.5" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.5" />
                  <Point X="-3.269228805968" Y="3.07595431831" Z="0.5" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.5" />
                  <Point X="-4.125946930877" Y="2.951415245782" Z="0.5" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.5" />
                  <Point X="-4.48055922162" Y="2.378849488806" Z="0.5" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.5" />
                  <Point X="-4.379690835413" Y="2.135017031883" Z="0.5" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.5" />
                  <Point X="-3.672691605992" Y="1.56497895798" Z="0.5" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.5" />
                  <Point X="-3.686605718933" Y="1.505943301832" Z="0.5" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.5" />
                  <Point X="-3.740773592902" Y="1.478654733526" Z="0.5" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.5" />
                  <Point X="-3.96279791781" Y="1.502466645589" Z="0.5" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.5" />
                  <Point X="-4.94197802241" Y="1.151790644736" Z="0.5" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.5" />
                  <Point X="-5.043059627374" Y="0.563334640107" Z="0.5" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.5" />
                  <Point X="-4.767505181791" Y="0.368181639541" Z="0.5" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.5" />
                  <Point X="-3.554284900563" Y="0.033608330389" Z="0.5" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.5" />
                  <Point X="-3.541382453463" Y="0.005882413615" Z="0.5" />
                  <Point X="-3.539556741714" Y="0" Z="0.5" />
                  <Point X="-3.546982147672" Y="-0.023924537446" Z="0.5" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.5" />
                  <Point X="-3.57108369456" Y="-0.045267652745" Z="0.5" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.5" />
                  <Point X="-3.869382606982" Y="-0.127530417215" Z="0.5" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.5" />
                  <Point X="-4.997989007571" Y="-0.882504063647" Z="0.5" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.5" />
                  <Point X="-4.87369529821" Y="-1.416270307696" Z="0.5" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.5" />
                  <Point X="-4.525667313974" Y="-1.478868372375" Z="0.5" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.5" />
                  <Point X="-3.197902653851" Y="-1.319373802303" Z="0.5" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.5" />
                  <Point X="-3.198860316737" Y="-1.346326515599" Z="0.5" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.5" />
                  <Point X="-3.457433600999" Y="-1.54944071182" Z="0.5" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.5" />
                  <Point X="-4.267286242335" Y="-2.74674503476" Z="0.5" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.5" />
                  <Point X="-3.930742362975" Y="-3.209926574513" Z="0.5" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.5" />
                  <Point X="-3.607775403804" Y="-3.153011485823" Z="0.5" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.5" />
                  <Point X="-2.558914709476" Y="-2.569415759656" Z="0.5" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.5" />
                  <Point X="-2.702405527345" Y="-2.827302978688" Z="0.5" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.5" />
                  <Point X="-2.971280736256" Y="-4.115284823655" Z="0.5" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.5" />
                  <Point X="-2.537825181713" Y="-4.395854482605" Z="0.5" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.5" />
                  <Point X="-2.406734542332" Y="-4.391700261423" Z="0.5" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.5" />
                  <Point X="-2.019165744383" Y="-4.018101223664" Z="0.5" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.5" />
                  <Point X="-1.71976439021" Y="-4.000371385834" Z="0.5" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.5" />
                  <Point X="-1.471440104372" Y="-4.168569606677" Z="0.5" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.5" />
                  <Point X="-1.37682365033" Y="-4.453180138481" Z="0.5" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.5" />
                  <Point X="-1.374394873083" Y="-4.58551596202" Z="0.5" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.5" />
                  <Point X="-1.175757800425" Y="-4.940569090611" Z="0.5" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.5" />
                  <Point X="-0.876820656357" Y="-5.002281227471" Z="0.5" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.5" />
                  <Point X="-0.73861320183" Y="-4.718726106664" Z="0.5" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.5" />
                  <Point X="-0.28567080604" Y="-3.329427714769" Z="0.5" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.5" />
                  <Point X="-0.050000514809" Y="-3.219757683044" Z="0.5" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.5" />
                  <Point X="0.203358564552" Y="-3.267354362244" Z="0.5" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.5" />
                  <Point X="0.384775053392" Y="-3.472218040999" Z="0.5" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.5" />
                  <Point X="0.496141658571" Y="-3.813809923152" Z="0.5" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.5" />
                  <Point X="0.96241957787" Y="-4.987466886689" Z="0.5" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.5" />
                  <Point X="1.141718803913" Y="-4.94950052471" Z="0.5" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.5" />
                  <Point X="1.133693673411" Y="-4.612408742999" Z="0.5" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.5" />
                  <Point X="1.00053975596" Y="-3.074187540582" Z="0.5" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.5" />
                  <Point X="1.155623889634" Y="-2.90520903992" Z="0.5" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.5" />
                  <Point X="1.378230702624" Y="-2.858459528383" Z="0.5" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.5" />
                  <Point X="1.595293901527" Y="-2.964204578815" Z="0.5" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.5" />
                  <Point X="1.83957742246" Y="-3.254788045243" Z="0.5" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.5" />
                  <Point X="2.818745470965" Y="-4.225222505864" Z="0.5" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.5" />
                  <Point X="3.00916620709" Y="-4.091790636511" Z="0.5" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.5" />
                  <Point X="2.893511635416" Y="-3.800109635104" Z="0.5" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.5" />
                  <Point X="2.239912866837" Y="-2.548853715577" Z="0.5" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.5" />
                  <Point X="2.308044712285" Y="-2.362118142417" Z="0.5" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.5" />
                  <Point X="2.470780235955" Y="-2.250856597682" Z="0.5" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.5" />
                  <Point X="2.679653087935" Y="-2.263535142654" Z="0.5" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.5" />
                  <Point X="2.987303962003" Y="-2.424237864858" Z="0.5" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.5" />
                  <Point X="4.205263436533" Y="-2.847380944181" Z="0.5" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.5" />
                  <Point X="4.367733862343" Y="-2.591277640809" Z="0.5" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.5" />
                  <Point X="4.161112057066" Y="-2.35764897496" Z="0.5" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.5" />
                  <Point X="3.112092337112" Y="-1.489146496182" Z="0.5" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.5" />
                  <Point X="3.104887342365" Y="-1.321105387354" Z="0.5" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.5" />
                  <Point X="3.196077492814" Y="-1.181432076666" Z="0.5" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.5" />
                  <Point X="3.363468067248" Y="-1.123708612849" Z="0.5" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.5" />
                  <Point X="3.696846410924" Y="-1.155093168082" Z="0.5" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.5" />
                  <Point X="4.974775769551" Y="-1.017440652959" Z="0.5" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.5" />
                  <Point X="5.036849677771" Y="-0.64321929892" Z="0.5" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.5" />
                  <Point X="4.791447283777" Y="-0.500414155159" Z="0.5" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.5" />
                  <Point X="3.67369959692" Y="-0.177891012539" Z="0.5" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.5" />
                  <Point X="3.610890853046" Y="-0.110568666154" Z="0.5" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.5" />
                  <Point X="3.585086103161" Y="-0.019066126687" Z="0.5" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.5" />
                  <Point X="3.596285347263" Y="0.077544404547" Z="0.5" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.5" />
                  <Point X="3.644488585354" Y="0.153380072421" Z="0.5" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.5" />
                  <Point X="3.729695817433" Y="0.210257831703" Z="0.5" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.5" />
                  <Point X="4.004520533529" Y="0.28955777761" Z="0.5" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.5" />
                  <Point X="4.995118716369" Y="0.908906246445" Z="0.5" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.5" />
                  <Point X="4.900782471628" Y="1.326521463902" Z="0.5" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.5" />
                  <Point X="4.601008902438" Y="1.371829859051" Z="0.5" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.5" />
                  <Point X="3.387543289257" Y="1.23201265304" Z="0.5" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.5" />
                  <Point X="3.313062441555" Y="1.265934411604" Z="0.5" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.5" />
                  <Point X="3.260745173634" Y="1.332300870633" Z="0.5" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.5" />
                  <Point X="3.23707901014" Y="1.415449537762" Z="0.5" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.5" />
                  <Point X="3.25086822531" Y="1.49412472846" Z="0.5" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.5" />
                  <Point X="3.301494824235" Y="1.569818551949" Z="0.5" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.5" />
                  <Point X="3.53677515648" Y="1.756481957005" Z="0.5" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.5" />
                  <Point X="4.279455449826" Y="2.73254584825" Z="0.5" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.5" />
                  <Point X="4.048820622612" Y="3.063919496717" Z="0.5" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.5" />
                  <Point X="3.707738737099" Y="2.958583993521" Z="0.5" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.5" />
                  <Point X="2.445436181861" Y="2.249766207509" Z="0.5" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.5" />
                  <Point X="2.373867813266" Y="2.25224843809" Z="0.5" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.5" />
                  <Point X="2.30935207246" Y="2.288380383869" Z="0.5" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.5" />
                  <Point X="2.262378216366" Y="2.347672787922" Z="0.5" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.5" />
                  <Point X="2.247181264669" Y="2.415890630567" Z="0.5" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.5" />
                  <Point X="2.262761735149" Y="2.494033394021" Z="0.5" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.5" />
                  <Point X="2.437041359339" Y="2.804400344856" Z="0.5" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.5" />
                  <Point X="2.827529645801" Y="4.216384670466" Z="0.5" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.5" />
                  <Point X="2.434255903581" Y="4.455023092266" Z="0.5" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.5" />
                  <Point X="2.025286609756" Y="4.65530602659" Z="0.5" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.5" />
                  <Point X="1.601064153245" Y="4.817703141869" Z="0.5" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.5" />
                  <Point X="0.991661131727" Y="4.975058654269" Z="0.5" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.5" />
                  <Point X="0.32533396494" Y="5.062489744039" Z="0.5" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.5" />
                  <Point X="0.155107670679" Y="4.93399421073" Z="0.5" />
                  <Point X="0" Y="4.355124473572" Z="0.5" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>