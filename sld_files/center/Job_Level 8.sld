<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#131" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="739" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="0" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000475830078" Y="0.004716064453" />
                  <Width Value="9.783896972656" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.618700378418" Y="-3.719274414063" />
                  <Point X="0.563302062988" Y="-3.512524414063" />
                  <Point X="0.557721191406" Y="-3.497141845703" />
                  <Point X="0.542363342285" Y="-3.467377441406" />
                  <Point X="0.519720581055" Y="-3.434753417969" />
                  <Point X="0.378635681152" Y="-3.231477050781" />
                  <Point X="0.356751586914" Y="-3.209021484375" />
                  <Point X="0.330496398926" Y="-3.189777587891" />
                  <Point X="0.302495635986" Y="-3.175669433594" />
                  <Point X="0.267457244873" Y="-3.164794677734" />
                  <Point X="0.049136493683" Y="-3.097036132812" />
                  <Point X="0.020976638794" Y="-3.092766601562" />
                  <Point X="-0.00866468811" Y="-3.092766601562" />
                  <Point X="-0.036823932648" Y="-3.097035888672" />
                  <Point X="-0.071862319946" Y="-3.107910400391" />
                  <Point X="-0.290183074951" Y="-3.175669189453" />
                  <Point X="-0.31818460083" Y="-3.189777587891" />
                  <Point X="-0.344439788818" Y="-3.209021484375" />
                  <Point X="-0.366323425293" Y="-3.2314765625" />
                  <Point X="-0.388966156006" Y="-3.264100341797" />
                  <Point X="-0.530051086426" Y="-3.467376953125" />
                  <Point X="-0.538189025879" Y="-3.481573730469" />
                  <Point X="-0.550990112305" Y="-3.512524414063" />
                  <Point X="-0.896172912598" Y="-4.800764160156" />
                  <Point X="-0.916584655762" Y="-4.876941894531" />
                  <Point X="-1.079340087891" Y="-4.845350585938" />
                  <Point X="-1.116008300781" Y="-4.835916015625" />
                  <Point X="-1.24641809082" Y="-4.802362792969" />
                  <Point X="-1.221631713867" Y="-4.614092285156" />
                  <Point X="-1.214963012695" Y="-4.563438476562" />
                  <Point X="-1.214201171875" Y="-4.5479296875" />
                  <Point X="-1.216508666992" Y="-4.516223632812" />
                  <Point X="-1.224879516602" Y="-4.474141601562" />
                  <Point X="-1.277036132812" Y="-4.211932128906" />
                  <Point X="-1.287938354492" Y="-4.182966308594" />
                  <Point X="-1.304010375977" Y="-4.15512890625" />
                  <Point X="-1.32364453125" Y="-4.131204589844" />
                  <Point X="-1.355903442383" Y="-4.1029140625" />
                  <Point X="-1.556905273438" Y="-3.926639404297" />
                  <Point X="-1.583188476562" Y="-3.910295410156" />
                  <Point X="-1.612885742188" Y="-3.897994384766" />
                  <Point X="-1.643027587891" Y="-3.890966308594" />
                  <Point X="-1.685842285156" Y="-3.88816015625" />
                  <Point X="-1.952616699219" Y="-3.870674804688" />
                  <Point X="-1.98341796875" Y="-3.873708496094" />
                  <Point X="-2.014466674805" Y="-3.882028076172" />
                  <Point X="-2.042657714844" Y="-3.894801513672" />
                  <Point X="-2.078333251953" Y="-3.918639160156" />
                  <Point X="-2.300624267578" Y="-4.067169677734" />
                  <Point X="-2.312784912109" Y="-4.076821044922" />
                  <Point X="-2.335102783203" Y="-4.099461914062" />
                  <Point X="-2.480149169922" Y="-4.288489746094" />
                  <Point X="-2.801708007812" Y="-4.089388427734" />
                  <Point X="-2.852483398438" Y="-4.05029296875" />
                  <Point X="-3.104721923828" Y="-3.856077880859" />
                  <Point X="-2.526713134766" Y="-2.8549375" />
                  <Point X="-2.423761230469" Y="-2.676619628906" />
                  <Point X="-2.412859130859" Y="-2.647653076172" />
                  <Point X="-2.406588134766" Y="-2.616126464844" />
                  <Point X="-2.405777832031" Y="-2.5837421875" />
                  <Point X="-2.415895996094" Y="-2.552968261719" />
                  <Point X="-2.432512451172" Y="-2.521739746094" />
                  <Point X="-2.449204589844" Y="-2.499188720703" />
                  <Point X="-2.464154052734" Y="-2.484239501953" />
                  <Point X="-2.489311767578" Y="-2.466212158203" />
                  <Point X="-2.518140625" Y="-2.451995605469" />
                  <Point X="-2.5477578125" Y="-2.443011474609" />
                  <Point X="-2.578691162109" Y="-2.444024169922" />
                  <Point X="-2.610217285156" Y="-2.450295166016" />
                  <Point X="-2.63918359375" Y="-2.461197265625" />
                  <Point X="-3.750265136719" Y="-3.102680419922" />
                  <Point X="-3.818024414062" Y="-3.141801269531" />
                  <Point X="-4.082859619141" Y="-2.793862060547" />
                  <Point X="-4.119258789062" Y="-2.732825927734" />
                  <Point X="-4.306142578125" Y="-2.419450439453" />
                  <Point X="-3.286086914062" Y="-1.636733642578" />
                  <Point X="-3.105954589844" Y="-1.498513305664" />
                  <Point X="-3.083062988281" Y="-1.475877807617" />
                  <Point X="-3.064499755859" Y="-1.446784057617" />
                  <Point X="-3.056720703125" Y="-1.427602294922" />
                  <Point X="-3.052791259766" Y="-1.415719238281" />
                  <Point X="-3.046152099609" Y="-1.39008581543" />
                  <Point X="-3.042037597656" Y="-1.358602905273" />
                  <Point X="-3.042736816406" Y="-1.335734741211" />
                  <Point X="-3.048883056641" Y="-1.313696777344" />
                  <Point X="-3.060887939453" Y="-1.284714111328" />
                  <Point X="-3.073085449219" Y="-1.263501708984" />
                  <Point X="-3.090296142578" Y="-1.246108398438" />
                  <Point X="-3.106460693359" Y="-1.2335234375" />
                  <Point X="-3.116634521484" Y="-1.226611694336" />
                  <Point X="-3.139454589844" Y="-1.213180664062" />
                  <Point X="-3.168718261719" Y="-1.201956420898" />
                  <Point X="-3.200606201172" Y="-1.195474731445" />
                  <Point X="-3.231929199219" Y="-1.194383789062" />
                  <Point X="-4.634564453125" Y="-1.379044311523" />
                  <Point X="-4.732102050781" Y="-1.391885375977" />
                  <Point X="-4.834077636719" Y="-0.992654602051" />
                  <Point X="-4.843707519531" Y="-0.925323303223" />
                  <Point X="-4.892424316406" Y="-0.584698181152" />
                  <Point X="-3.738073730469" Y="-0.275390930176" />
                  <Point X="-3.532875976562" Y="-0.220408340454" />
                  <Point X="-3.514347900391" Y="-0.213297729492" />
                  <Point X="-3.494941650391" Y="-0.20341343689" />
                  <Point X="-3.483890136719" Y="-0.196805389404" />
                  <Point X="-3.459975341797" Y="-0.180207107544" />
                  <Point X="-3.436021240234" Y="-0.158681411743" />
                  <Point X="-3.416068359375" Y="-0.130456436157" />
                  <Point X="-3.407363769531" Y="-0.111596786499" />
                  <Point X="-3.402889160156" Y="-0.099945350647" />
                  <Point X="-3.394917480469" Y="-0.074260444641" />
                  <Point X="-3.390716064453" Y="-0.042503917694" />
                  <Point X="-3.391995361328" Y="-0.008740113258" />
                  <Point X="-3.396196777344" Y="0.015822610855" />
                  <Point X="-3.404168457031" Y="0.041507522583" />
                  <Point X="-3.417484375" Y="0.070831001282" />
                  <Point X="-3.438548339844" Y="0.098495864868" />
                  <Point X="-3.454205078125" Y="0.112677185059" />
                  <Point X="-3.463813476562" Y="0.120310874939" />
                  <Point X="-3.487728271484" Y="0.136909164429" />
                  <Point X="-3.501925292969" Y="0.145047103882" />
                  <Point X="-3.532875976562" Y="0.157848342896" />
                  <Point X="-4.811444335938" Y="0.500439605713" />
                  <Point X="-4.89181640625" Y="0.521975280762" />
                  <Point X="-4.824487792969" Y="0.976974487305" />
                  <Point X="-4.8051015625" Y="1.048515869141" />
                  <Point X="-4.703551269531" Y="1.423267822266" />
                  <Point X="-3.912757324219" Y="1.319157714844" />
                  <Point X="-3.765666259766" Y="1.29979284668" />
                  <Point X="-3.744986083984" Y="1.299341674805" />
                  <Point X="-3.723424560547" Y="1.301228149414" />
                  <Point X="-3.703137451172" Y="1.305263671875" />
                  <Point X="-3.694642578125" Y="1.307942016602" />
                  <Point X="-3.641711425781" Y="1.324631225586" />
                  <Point X="-3.622779052734" Y="1.332961669922" />
                  <Point X="-3.604034667969" Y="1.343783569336" />
                  <Point X="-3.587353515625" Y="1.356014648438" />
                  <Point X="-3.573715087891" Y="1.37156628418" />
                  <Point X="-3.561300537109" Y="1.389296020508" />
                  <Point X="-3.5513515625" Y="1.407430297852" />
                  <Point X="-3.547942871094" Y="1.415659423828" />
                  <Point X="-3.526704101563" Y="1.466934570312" />
                  <Point X="-3.520915527344" Y="1.486794189453" />
                  <Point X="-3.517157226562" Y="1.508109375" />
                  <Point X="-3.5158046875" Y="1.528749389648" />
                  <Point X="-3.518951171875" Y="1.549192871094" />
                  <Point X="-3.524552978516" Y="1.570099365234" />
                  <Point X="-3.532049560547" Y="1.589377197266" />
                  <Point X="-3.536162353516" Y="1.597277954102" />
                  <Point X="-3.561789306641" Y="1.646506958008" />
                  <Point X="-3.57328125" Y="1.663706054688" />
                  <Point X="-3.587193603516" Y="1.680286254883" />
                  <Point X="-3.602135742188" Y="1.694590209961" />
                  <Point X="-4.335525878906" Y="2.257340332031" />
                  <Point X="-4.351860351563" Y="2.269874267578" />
                  <Point X="-4.081154541016" Y="2.733658447266" />
                  <Point X="-4.029803466797" Y="2.799663085938" />
                  <Point X="-3.750504882813" Y="3.158661865234" />
                  <Point X="-3.29699609375" Y="2.896828125" />
                  <Point X="-3.206656982422" Y="2.844670898438" />
                  <Point X="-3.187724121094" Y="2.836340332031" />
                  <Point X="-3.167081787109" Y="2.829831787109" />
                  <Point X="-3.146794921875" Y="2.825796386719" />
                  <Point X="-3.134963867188" Y="2.824761230469" />
                  <Point X="-3.061245605469" Y="2.818311767578" />
                  <Point X="-3.040564941406" Y="2.818762939453" />
                  <Point X="-3.019106201172" Y="2.821588134766" />
                  <Point X="-2.999014160156" Y="2.826504882812" />
                  <Point X="-2.980462402344" Y="2.835653564453" />
                  <Point X="-2.962208251953" Y="2.847282958984" />
                  <Point X="-2.946078369141" Y="2.860228759766" />
                  <Point X="-2.937680419922" Y="2.868626464844" />
                  <Point X="-2.885354736328" Y="2.920952148438" />
                  <Point X="-2.872406982422" Y="2.937083984375" />
                  <Point X="-2.860777587891" Y="2.955338134766" />
                  <Point X="-2.85162890625" Y="2.973889892578" />
                  <Point X="-2.846712158203" Y="2.993981933594" />
                  <Point X="-2.843886962891" Y="3.015440673828" />
                  <Point X="-2.843435791016" Y="3.036121337891" />
                  <Point X="-2.844470947266" Y="3.047952636719" />
                  <Point X="-2.850920410156" Y="3.121670898438" />
                  <Point X="-2.854955810547" Y="3.141957763672" />
                  <Point X="-2.861464355469" Y="3.162600097656" />
                  <Point X="-2.869794921875" Y="3.181532958984" />
                  <Point X="-3.183333007812" Y="3.724596679688" />
                  <Point X="-2.700625244141" Y="4.094683837891" />
                  <Point X="-2.619752685547" Y="4.139615234375" />
                  <Point X="-2.167036865234" Y="4.391134277344" />
                  <Point X="-2.070851074219" Y="4.265782226562" />
                  <Point X="-2.0431953125" Y="4.229740722656" />
                  <Point X="-2.028892333984" Y="4.214799804688" />
                  <Point X="-2.012312255859" Y="4.200887207031" />
                  <Point X="-1.995114257812" Y="4.189395507812" />
                  <Point X="-1.981946411133" Y="4.182540527344" />
                  <Point X="-1.899898193359" Y="4.139828613281" />
                  <Point X="-1.88061706543" Y="4.132330566406" />
                  <Point X="-1.859710449219" Y="4.126729003906" />
                  <Point X="-1.839267333984" Y="4.123582519531" />
                  <Point X="-1.818628051758" Y="4.124935546875" />
                  <Point X="-1.797312988281" Y="4.128693847656" />
                  <Point X="-1.777453491211" Y="4.134481933594" />
                  <Point X="-1.76373815918" Y="4.140163085938" />
                  <Point X="-1.678279541016" Y="4.175561523437" />
                  <Point X="-1.66014465332" Y="4.185510742187" />
                  <Point X="-1.642415039062" Y="4.197925292969" />
                  <Point X="-1.626863525391" Y="4.211563964844" />
                  <Point X="-1.614632568359" Y="4.228245117188" />
                  <Point X="-1.603810791016" Y="4.246989257813" />
                  <Point X="-1.595480712891" Y="4.265920898438" />
                  <Point X="-1.591016357422" Y="4.280079101563" />
                  <Point X="-1.563201293945" Y="4.368297363281" />
                  <Point X="-1.559165649414" Y="4.388584960938" />
                  <Point X="-1.557279174805" Y="4.410146484375" />
                  <Point X="-1.55773034668" Y="4.430826660156" />
                  <Point X="-1.584201904297" Y="4.631898925781" />
                  <Point X="-0.949638427734" Y="4.809808105469" />
                  <Point X="-0.851600402832" Y="4.821281738281" />
                  <Point X="-0.294710754395" Y="4.886458007812" />
                  <Point X="-0.160627990723" Y="4.386054199219" />
                  <Point X="-0.133903305054" Y="4.286315429688" />
                  <Point X="-0.121129844666" Y="4.258124023438" />
                  <Point X="-0.10327155304" Y="4.231397460938" />
                  <Point X="-0.082114570618" Y="4.20880859375" />
                  <Point X="-0.054819446564" Y="4.19421875" />
                  <Point X="-0.024381370544" Y="4.183886230469" />
                  <Point X="0.006155920982" Y="4.178844238281" />
                  <Point X="0.03669316864" Y="4.183886230469" />
                  <Point X="0.067131248474" Y="4.19421875" />
                  <Point X="0.094426452637" Y="4.20880859375" />
                  <Point X="0.115583503723" Y="4.231397460938" />
                  <Point X="0.133441650391" Y="4.258124023438" />
                  <Point X="0.146215255737" Y="4.286315429688" />
                  <Point X="0.307419250488" Y="4.8879375" />
                  <Point X="0.844042175293" Y="4.831738769531" />
                  <Point X="0.925155212402" Y="4.812155761719" />
                  <Point X="1.481025634766" Y="4.677951171875" />
                  <Point X="1.532545288086" Y="4.659264648438" />
                  <Point X="1.894647216797" Y="4.527927734375" />
                  <Point X="1.945701538086" Y="4.504051269531" />
                  <Point X="2.294574951172" Y="4.34089453125" />
                  <Point X="2.343930664062" Y="4.312139648438" />
                  <Point X="2.680978759766" Y="4.115774902344" />
                  <Point X="2.727498779297" Y="4.082692138672" />
                  <Point X="2.943260009766" Y="3.929254638672" />
                  <Point X="2.266739990234" Y="2.757487548828" />
                  <Point X="2.147581054688" Y="2.551097900391" />
                  <Point X="2.140013671875" Y="2.534481689453" />
                  <Point X="2.13204296875" Y="2.511295166016" />
                  <Point X="2.130107666016" Y="2.504953369141" />
                  <Point X="2.111607177734" Y="2.435770263672" />
                  <Point X="2.108384277344" Y="2.411827880859" />
                  <Point X="2.108204345703" Y="2.383324462891" />
                  <Point X="2.108885498047" Y="2.371352050781" />
                  <Point X="2.116099121094" Y="2.311528320312" />
                  <Point X="2.121442138672" Y="2.289604980469" />
                  <Point X="2.129708251953" Y="2.267516357422" />
                  <Point X="2.140071044922" Y="2.247471191406" />
                  <Point X="2.146011962891" Y="2.238715820312" />
                  <Point X="2.183028808594" Y="2.184162597656" />
                  <Point X="2.199187011719" Y="2.165917480469" />
                  <Point X="2.2212421875" Y="2.146676269531" />
                  <Point X="2.230354248047" Y="2.139651367188" />
                  <Point X="2.284907714844" Y="2.102634765625" />
                  <Point X="2.304952880859" Y="2.092271972656" />
                  <Point X="2.327041259766" Y="2.084006103516" />
                  <Point X="2.348964111328" Y="2.078663330078" />
                  <Point X="2.358565185547" Y="2.077505615234" />
                  <Point X="2.418388916016" Y="2.070291748047" />
                  <Point X="2.443259277344" Y="2.070572265625" />
                  <Point X="2.473265136719" Y="2.074879150391" />
                  <Point X="2.484309570312" Y="2.077140380859" />
                  <Point X="2.553492675781" Y="2.095640869141" />
                  <Point X="2.565283447266" Y="2.099638183594" />
                  <Point X="2.588533935547" Y="2.110145019531" />
                  <Point X="3.874527587891" Y="2.852613769531" />
                  <Point X="3.967326416016" Y="2.906191162109" />
                  <Point X="4.123273925781" Y="2.689459228516" />
                  <Point X="4.14920703125" Y="2.646604492188" />
                  <Point X="4.262198730469" Y="2.459884521484" />
                  <Point X="3.38766796875" Y="1.788832641602" />
                  <Point X="3.230783935547" Y="1.668451293945" />
                  <Point X="3.217158691406" Y="1.655683105469" />
                  <Point X="3.199922851562" Y="1.636008789063" />
                  <Point X="3.195982910156" Y="1.63120300293" />
                  <Point X="3.146191650391" Y="1.566246582031" />
                  <Point X="3.133838134766" Y="1.544849121094" />
                  <Point X="3.122392089844" Y="1.517253540039" />
                  <Point X="3.118653564453" Y="1.506442504883" />
                  <Point X="3.100106201172" Y="1.440121826172" />
                  <Point X="3.096652587891" Y="1.417823242188" />
                  <Point X="3.095836425781" Y="1.394253173828" />
                  <Point X="3.097739501953" Y="1.371768066406" />
                  <Point X="3.100183105469" Y="1.359925170898" />
                  <Point X="3.115408447266" Y="1.286135131836" />
                  <Point X="3.123606933594" Y="1.262590454102" />
                  <Point X="3.137450927734" Y="1.235110595703" />
                  <Point X="3.142928710938" Y="1.225638183594" />
                  <Point X="3.184340332031" Y="1.162694824219" />
                  <Point X="3.198893554688" Y="1.145450439453" />
                  <Point X="3.216137451172" Y="1.129360595703" />
                  <Point X="3.234346191406" Y="1.116035522461" />
                  <Point X="3.243977294922" Y="1.110613769531" />
                  <Point X="3.30398828125" Y="1.076833007813" />
                  <Point X="3.327634765625" Y="1.067432495117" />
                  <Point X="3.358632080078" Y="1.059713745117" />
                  <Point X="3.369140136719" Y="1.057717651367" />
                  <Point X="3.450278808594" Y="1.046994018555" />
                  <Point X="3.462702148438" Y="1.046175048828" />
                  <Point X="3.488203613281" Y="1.04698449707" />
                  <Point X="4.709811523438" Y="1.207812255859" />
                  <Point X="4.776839355469" Y="1.21663659668" />
                  <Point X="4.845936523438" Y="0.93280682373" />
                  <Point X="4.854108886719" Y="0.880316345215" />
                  <Point X="4.890864746094" Y="0.644238525391" />
                  <Point X="3.896372314453" Y="0.377765167236" />
                  <Point X="3.716579833984" Y="0.329589935303" />
                  <Point X="3.698777587891" Y="0.322844970703" />
                  <Point X="3.673903076172" Y="0.310442474365" />
                  <Point X="3.668751953125" Y="0.307673065186" />
                  <Point X="3.589035644531" Y="0.261595428467" />
                  <Point X="3.569078125" Y="0.246196884155" />
                  <Point X="3.54708984375" Y="0.223995117188" />
                  <Point X="3.539854492188" Y="0.21579524231" />
                  <Point X="3.492024902344" Y="0.154848815918" />
                  <Point X="3.480301025391" Y="0.135569152832" />
                  <Point X="3.470527099609" Y="0.114105384827" />
                  <Point X="3.463680908203" Y="0.092604728699" />
                  <Point X="3.461122070312" Y="0.079244132996" />
                  <Point X="3.445178710938" Y="-0.004005618572" />
                  <Point X="3.443781982422" Y="-0.029407896042" />
                  <Point X="3.446340820312" Y="-0.061578052521" />
                  <Point X="3.447737304688" Y="-0.071914367676" />
                  <Point X="3.463680664062" Y="-0.155164123535" />
                  <Point X="3.470527099609" Y="-0.176665542603" />
                  <Point X="3.480301025391" Y="-0.198129302979" />
                  <Point X="3.492024169922" Y="-0.217407913208" />
                  <Point X="3.499700195312" Y="-0.227189285278" />
                  <Point X="3.547530029297" Y="-0.288135559082" />
                  <Point X="3.566199951172" Y="-0.306179046631" />
                  <Point X="3.593305664062" Y="-0.325994354248" />
                  <Point X="3.601829345703" Y="-0.331550506592" />
                  <Point X="3.681545654297" Y="-0.377628143311" />
                  <Point X="3.692710205078" Y="-0.383139038086" />
                  <Point X="3.716579833984" Y="-0.392149932861" />
                  <Point X="4.836849609375" Y="-0.692325317383" />
                  <Point X="4.89147265625" Y="-0.706961486816" />
                  <Point X="4.855022460938" Y="-0.948725646973" />
                  <Point X="4.844551757812" Y="-0.994611694336" />
                  <Point X="4.801173828125" Y="-1.18469909668" />
                  <Point X="3.632890869141" Y="-1.030891601562" />
                  <Point X="3.424382080078" Y="-1.003441040039" />
                  <Point X="3.408035644531" Y="-1.002710327148" />
                  <Point X="3.374658691406" Y="-1.005508728027" />
                  <Point X="3.349549316406" Y="-1.010966369629" />
                  <Point X="3.193094482422" Y="-1.044972412109" />
                  <Point X="3.163974121094" Y="-1.056596923828" />
                  <Point X="3.136147460938" Y="-1.073489257812" />
                  <Point X="3.112397460938" Y="-1.093960205078" />
                  <Point X="3.097220214844" Y="-1.112213745117" />
                  <Point X="3.002653320312" Y="-1.225948364258" />
                  <Point X="2.987932617188" Y="-1.250330566406" />
                  <Point X="2.976589355469" Y="-1.277715698242" />
                  <Point X="2.969757568359" Y="-1.305365234375" />
                  <Point X="2.967582275391" Y="-1.329004150391" />
                  <Point X="2.954028564453" Y="-1.476295532227" />
                  <Point X="2.956347412109" Y="-1.507564819336" />
                  <Point X="2.964079101562" Y="-1.539185791016" />
                  <Point X="2.976450439453" Y="-1.567996826172" />
                  <Point X="2.990346435547" Y="-1.589610961914" />
                  <Point X="3.076930664062" Y="-1.724287109375" />
                  <Point X="3.086932373047" Y="-1.73723815918" />
                  <Point X="3.110628417969" Y="-1.760909179688" />
                  <Point X="4.150245605469" Y="-2.558635986328" />
                  <Point X="4.213122558594" Y="-2.606883056641" />
                  <Point X="4.124811523438" Y="-2.749783447266" />
                  <Point X="4.103154296875" Y="-2.780555419922" />
                  <Point X="4.028981445312" Y="-2.885945068359" />
                  <Point X="2.986659667969" Y="-2.28416015625" />
                  <Point X="2.800954589844" Y="-2.176943115234" />
                  <Point X="2.786131591797" Y="-2.170012451172" />
                  <Point X="2.754224365234" Y="-2.159824951172" />
                  <Point X="2.724340087891" Y="-2.154427978516" />
                  <Point X="2.538134033203" Y="-2.120799316406" />
                  <Point X="2.506781982422" Y="-2.120395263672" />
                  <Point X="2.474609130859" Y="-2.125353027344" />
                  <Point X="2.444833740234" Y="-2.135176513672" />
                  <Point X="2.420007080078" Y="-2.148242431641" />
                  <Point X="2.265315673828" Y="-2.229655517578" />
                  <Point X="2.242384521484" Y="-2.246548828125" />
                  <Point X="2.221424804688" Y="-2.267508544922" />
                  <Point X="2.204531738281" Y="-2.290439208984" />
                  <Point X="2.191465820313" Y="-2.315265625" />
                  <Point X="2.110052734375" Y="-2.469957275391" />
                  <Point X="2.100229003906" Y="-2.499733154297" />
                  <Point X="2.095271240234" Y="-2.531906005859" />
                  <Point X="2.095675292969" Y="-2.563258056641" />
                  <Point X="2.101072265625" Y="-2.593142333984" />
                  <Point X="2.134700927734" Y="-2.779348388672" />
                  <Point X="2.138985595703" Y="-2.795141845703" />
                  <Point X="2.151819091797" Y="-2.826078613281" />
                  <Point X="2.819877929688" Y="-3.983189941406" />
                  <Point X="2.861283447266" Y="-4.054906494141" />
                  <Point X="2.78184765625" Y="-4.111645507812" />
                  <Point X="2.757639404297" Y="-4.127315429688" />
                  <Point X="2.701765136719" Y="-4.163481933594" />
                  <Point X="1.900256347656" Y="-3.118935302734" />
                  <Point X="1.758546020508" Y="-2.934255126953" />
                  <Point X="1.747503540039" Y="-2.922179443359" />
                  <Point X="1.721924438477" Y="-2.900557617188" />
                  <Point X="1.692450439453" Y="-2.881608398438" />
                  <Point X="1.508801025391" Y="-2.763538818359" />
                  <Point X="1.479989746094" Y="-2.751167236328" />
                  <Point X="1.448368774414" Y="-2.743435546875" />
                  <Point X="1.417099731445" Y="-2.741116699219" />
                  <Point X="1.384864746094" Y="-2.744083007813" />
                  <Point X="1.184012817383" Y="-2.762565673828" />
                  <Point X="1.15636315918" Y="-2.769397460938" />
                  <Point X="1.128978027344" Y="-2.780740722656" />
                  <Point X="1.104596069336" Y="-2.7954609375" />
                  <Point X="1.079705200195" Y="-2.816156738281" />
                  <Point X="0.924612243652" Y="-2.945111328125" />
                  <Point X="0.904141052246" Y="-2.968861816406" />
                  <Point X="0.887248779297" Y="-2.996688476562" />
                  <Point X="0.875624206543" Y="-3.025809082031" />
                  <Point X="0.868182067871" Y="-3.060049316406" />
                  <Point X="0.821809936523" Y="-3.273396972656" />
                  <Point X="0.819724487305" Y="-3.289627441406" />
                  <Point X="0.819742248535" Y="-3.323120117188" />
                  <Point X="1.009064941406" Y="-4.761168457031" />
                  <Point X="1.022065246582" Y="-4.859915039062" />
                  <Point X="0.975682006836" Y="-4.87008203125" />
                  <Point X="0.953307800293" Y="-4.874146972656" />
                  <Point X="0.929315551758" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058431884766" Y="-4.752635742188" />
                  <Point X="-1.092336181641" Y="-4.743912597656" />
                  <Point X="-1.141246459961" Y="-4.731328613281" />
                  <Point X="-1.127444458008" Y="-4.6264921875" />
                  <Point X="-1.120775756836" Y="-4.575838378906" />
                  <Point X="-1.120077392578" Y="-4.568099609375" />
                  <Point X="-1.119451660156" Y="-4.541034179688" />
                  <Point X="-1.121759277344" Y="-4.509328125" />
                  <Point X="-1.123334106445" Y="-4.497689453125" />
                  <Point X="-1.131704956055" Y="-4.455607421875" />
                  <Point X="-1.183861572266" Y="-4.193397949219" />
                  <Point X="-1.188125366211" Y="-4.178467773438" />
                  <Point X="-1.199027587891" Y="-4.149501953125" />
                  <Point X="-1.205666137695" Y="-4.135466308594" />
                  <Point X="-1.221738037109" Y="-4.10762890625" />
                  <Point X="-1.23057434082" Y="-4.094861572266" />
                  <Point X="-1.250208496094" Y="-4.070937255859" />
                  <Point X="-1.261006469727" Y="-4.059780029297" />
                  <Point X="-1.293265380859" Y="-4.031489501953" />
                  <Point X="-1.494267089844" Y="-3.85521484375" />
                  <Point X="-1.506738769531" Y="-3.845965332031" />
                  <Point X="-1.533021972656" Y="-3.829621337891" />
                  <Point X="-1.546833496094" Y="-3.822526855469" />
                  <Point X="-1.576530761719" Y="-3.810225830078" />
                  <Point X="-1.591313598633" Y="-3.805476074219" />
                  <Point X="-1.621455444336" Y="-3.798447998047" />
                  <Point X="-1.636814453125" Y="-3.796169677734" />
                  <Point X="-1.679629150391" Y="-3.793363525391" />
                  <Point X="-1.946403564453" Y="-3.775878173828" />
                  <Point X="-1.961928344727" Y="-3.776132324219" />
                  <Point X="-1.992729736328" Y="-3.779166015625" />
                  <Point X="-2.008006103516" Y="-3.781945556641" />
                  <Point X="-2.03905480957" Y="-3.790265136719" />
                  <Point X="-2.053674560547" Y="-3.795496337891" />
                  <Point X="-2.081865478516" Y="-3.808269775391" />
                  <Point X="-2.095437011719" Y="-3.815812011719" />
                  <Point X="-2.131112548828" Y="-3.839649658203" />
                  <Point X="-2.353403564453" Y="-3.988180175781" />
                  <Point X="-2.359681884766" Y="-3.992757324219" />
                  <Point X="-2.380440917969" Y="-4.010130126953" />
                  <Point X="-2.402758789062" Y="-4.032770996094" />
                  <Point X="-2.410471191406" Y="-4.041629638672" />
                  <Point X="-2.503202636719" Y="-4.162479492188" />
                  <Point X="-2.747582763672" Y="-4.011165283203" />
                  <Point X="-2.794525878906" Y="-3.975020507813" />
                  <Point X="-2.980862792969" Y="-3.831547607422" />
                  <Point X="-2.444440673828" Y="-2.9024375" />
                  <Point X="-2.341488769531" Y="-2.724119628906" />
                  <Point X="-2.334850097656" Y="-2.710083007812" />
                  <Point X="-2.323947998047" Y="-2.681116455078" />
                  <Point X="-2.319684570312" Y="-2.666186523438" />
                  <Point X="-2.313413574219" Y="-2.634659912109" />
                  <Point X="-2.311617919922" Y="-2.618502685547" />
                  <Point X="-2.310807617188" Y="-2.586118408203" />
                  <Point X="-2.315530761719" Y="-2.554069824219" />
                  <Point X="-2.325648925781" Y="-2.523295898438" />
                  <Point X="-2.332029296875" Y="-2.508343505859" />
                  <Point X="-2.348645751953" Y="-2.477114990234" />
                  <Point X="-2.356154541016" Y="-2.465219970703" />
                  <Point X="-2.372846679688" Y="-2.442668945312" />
                  <Point X="-2.382030029297" Y="-2.432012939453" />
                  <Point X="-2.396979492188" Y="-2.417063720703" />
                  <Point X="-2.408819580078" Y="-2.407018554688" />
                  <Point X="-2.433977294922" Y="-2.388991210938" />
                  <Point X="-2.447294921875" Y="-2.381009033203" />
                  <Point X="-2.476123779297" Y="-2.366792480469" />
                  <Point X="-2.490563964844" Y="-2.361086181641" />
                  <Point X="-2.520181152344" Y="-2.352102050781" />
                  <Point X="-2.550866210938" Y="-2.348062255859" />
                  <Point X="-2.581799560547" Y="-2.349074951172" />
                  <Point X="-2.597224853516" Y="-2.350849609375" />
                  <Point X="-2.628750976562" Y="-2.357120605469" />
                  <Point X="-2.643680908203" Y="-2.361384033203" />
                  <Point X="-2.672647216797" Y="-2.372286132812" />
                  <Point X="-2.68668359375" Y="-2.378924804688" />
                  <Point X="-3.793089599609" Y="-3.017708496094" />
                  <Point X="-4.004015380859" Y="-2.740595214844" />
                  <Point X="-4.037666015625" Y="-2.684167724609" />
                  <Point X="-4.181265136719" Y="-2.443373779297" />
                  <Point X="-3.228254638672" Y="-1.712102050781" />
                  <Point X="-3.048122314453" Y="-1.573881958008" />
                  <Point X="-3.039158447266" Y="-1.566065429688" />
                  <Point X="-3.016266845703" Y="-1.54342980957" />
                  <Point X="-3.002976318359" Y="-1.526977050781" />
                  <Point X="-2.984413085938" Y="-1.497883300781" />
                  <Point X="-2.976463867188" Y="-1.482486572266" />
                  <Point X="-2.968684814453" Y="-1.46330480957" />
                  <Point X="-2.960825927734" Y="-1.439538696289" />
                  <Point X="-2.954186767578" Y="-1.413905273438" />
                  <Point X="-2.951953125" Y="-1.402396728516" />
                  <Point X="-2.947838623047" Y="-1.370913818359" />
                  <Point X="-2.94708203125" Y="-1.355699584961" />
                  <Point X="-2.94778125" Y="-1.332831420898" />
                  <Point X="-2.951229003906" Y="-1.310213867188" />
                  <Point X="-2.957375244141" Y="-1.28817590332" />
                  <Point X="-2.961114257812" Y="-1.277342163086" />
                  <Point X="-2.973119140625" Y="-1.24835949707" />
                  <Point X="-2.978532470703" Y="-1.237358276367" />
                  <Point X="-2.990729980469" Y="-1.216145874023" />
                  <Point X="-3.005556640625" Y="-1.196682006836" />
                  <Point X="-3.022767333984" Y="-1.179288696289" />
                  <Point X="-3.031935546875" Y="-1.171147949219" />
                  <Point X="-3.048100097656" Y="-1.158563110352" />
                  <Point X="-3.068447753906" Y="-1.144739624023" />
                  <Point X="-3.091267822266" Y="-1.13130859375" />
                  <Point X="-3.10543359375" Y="-1.124481323242" />
                  <Point X="-3.134697265625" Y="-1.113257202148" />
                  <Point X="-3.149795166016" Y="-1.108860107422" />
                  <Point X="-3.181683105469" Y="-1.102378417969" />
                  <Point X="-3.197299560547" Y="-1.100532348633" />
                  <Point X="-3.228622558594" Y="-1.09944140625" />
                  <Point X="-3.244329101562" Y="-1.100196411133" />
                  <Point X="-4.646964355469" Y="-1.284857055664" />
                  <Point X="-4.660920898438" Y="-1.286694458008" />
                  <Point X="-4.740762207031" Y="-0.974118164063" />
                  <Point X="-4.749664550781" Y="-0.91187310791" />
                  <Point X="-4.786452148438" Y="-0.65465435791" />
                  <Point X="-3.713485839844" Y="-0.367153808594" />
                  <Point X="-3.508288085938" Y="-0.312171356201" />
                  <Point X="-3.498837890625" Y="-0.309101104736" />
                  <Point X="-3.471231445312" Y="-0.297949920654" />
                  <Point X="-3.451825195312" Y="-0.288065612793" />
                  <Point X="-3.429722900391" Y="-0.274849609375" />
                  <Point X="-3.405808105469" Y="-0.258251403809" />
                  <Point X="-3.396477539062" Y="-0.250868408203" />
                  <Point X="-3.3725234375" Y="-0.229342758179" />
                  <Point X="-3.358447265625" Y="-0.213520217896" />
                  <Point X="-3.338494384766" Y="-0.18529524231" />
                  <Point X="-3.3298125" Y="-0.170267532349" />
                  <Point X="-3.321107910156" Y="-0.15140788269" />
                  <Point X="-3.312158447266" Y="-0.128104858398" />
                  <Point X="-3.304186767578" Y="-0.10241999054" />
                  <Point X="-3.300738037109" Y="-0.086720504761" />
                  <Point X="-3.296536621094" Y="-0.054963951111" />
                  <Point X="-3.295784179688" Y="-0.038907032013" />
                  <Point X="-3.297063476562" Y="-0.005143175602" />
                  <Point X="-3.298355224609" Y="0.007276943684" />
                  <Point X="-3.302556640625" Y="0.031839553833" />
                  <Point X="-3.305466064453" Y="0.043982192993" />
                  <Point X="-3.313437744141" Y="0.069667060852" />
                  <Point X="-3.317669189453" Y="0.08078717041" />
                  <Point X="-3.330985107422" Y="0.110110618591" />
                  <Point X="-3.341899902344" Y="0.128380828857" />
                  <Point X="-3.362963867188" Y="0.156045654297" />
                  <Point X="-3.374772705078" Y="0.168906585693" />
                  <Point X="-3.390429443359" Y="0.183087890625" />
                  <Point X="-3.409646240234" Y="0.198355178833" />
                  <Point X="-3.433561035156" Y="0.214953384399" />
                  <Point X="-3.440484130859" Y="0.219328842163" />
                  <Point X="-3.465616210938" Y="0.232834655762" />
                  <Point X="-3.496566894531" Y="0.24563583374" />
                  <Point X="-3.508288085938" Y="0.249611358643" />
                  <Point X="-4.785446289062" Y="0.591824707031" />
                  <Point X="-4.731331054688" Y="0.957528686523" />
                  <Point X="-4.713408203125" Y="1.023668884277" />
                  <Point X="-4.6335859375" Y="1.318237060547" />
                  <Point X="-3.925157226562" Y="1.224970458984" />
                  <Point X="-3.778066162109" Y="1.20560559082" />
                  <Point X="-3.76773828125" Y="1.204815429688" />
                  <Point X="-3.747058105469" Y="1.204364379883" />
                  <Point X="-3.736705810547" Y="1.204703125" />
                  <Point X="-3.715144287109" Y="1.206589599609" />
                  <Point X="-3.704890136719" Y="1.208053710938" />
                  <Point X="-3.684603027344" Y="1.212089233398" />
                  <Point X="-3.666076171875" Y="1.217338623047" />
                  <Point X="-3.613145019531" Y="1.234027832031" />
                  <Point X="-3.603450439453" Y="1.237676635742" />
                  <Point X="-3.584518066406" Y="1.246007080078" />
                  <Point X="-3.575279541016" Y="1.250688842773" />
                  <Point X="-3.55653515625" Y="1.261510864258" />
                  <Point X="-3.547860351562" Y="1.267171264648" />
                  <Point X="-3.531179199219" Y="1.27940234375" />
                  <Point X="-3.515928710938" Y="1.293376831055" />
                  <Point X="-3.502290283203" Y="1.308928466797" />
                  <Point X="-3.495895751953" Y="1.317076416016" />
                  <Point X="-3.483481201172" Y="1.334806152344" />
                  <Point X="-3.47801171875" Y="1.34360144043" />
                  <Point X="-3.468062744141" Y="1.361735717773" />
                  <Point X="-3.460174560547" Y="1.379303833008" />
                  <Point X="-3.438935791016" Y="1.430578979492" />
                  <Point X="-3.435499511719" Y="1.440350708008" />
                  <Point X="-3.4297109375" Y="1.460210327148" />
                  <Point X="-3.427358642578" Y="1.470298217773" />
                  <Point X="-3.423600341797" Y="1.49161340332" />
                  <Point X="-3.422360595703" Y="1.501897216797" />
                  <Point X="-3.421008056641" Y="1.522537231445" />
                  <Point X="-3.421910400391" Y="1.543200805664" />
                  <Point X="-3.425056884766" Y="1.563644287109" />
                  <Point X="-3.427188232422" Y="1.573780395508" />
                  <Point X="-3.432790039062" Y="1.594686889648" />
                  <Point X="-3.436011962891" Y="1.604530517578" />
                  <Point X="-3.443508544922" Y="1.623808227539" />
                  <Point X="-3.451895996094" Y="1.641143432617" />
                  <Point X="-3.477522949219" Y="1.690372436523" />
                  <Point X="-3.482799316406" Y="1.699285766602" />
                  <Point X="-3.494291259766" Y="1.716484863281" />
                  <Point X="-3.500506835938" Y="1.724770507812" />
                  <Point X="-3.514419189453" Y="1.741350830078" />
                  <Point X="-3.5215" Y="1.748910888672" />
                  <Point X="-3.536442138672" Y="1.76321484375" />
                  <Point X="-3.544303466797" Y="1.769958740234" />
                  <Point X="-4.227614746094" Y="2.294282226563" />
                  <Point X="-4.002293212891" Y="2.680312011719" />
                  <Point X="-3.954822753906" Y="2.741328613281" />
                  <Point X="-3.726338623047" Y="3.035012695312" />
                  <Point X="-3.34449609375" Y="2.814555664062" />
                  <Point X="-3.254156982422" Y="2.7623984375" />
                  <Point X="-3.244917480469" Y="2.757716064453" />
                  <Point X="-3.225984619141" Y="2.749385498047" />
                  <Point X="-3.216291259766" Y="2.745737304688" />
                  <Point X="-3.195648925781" Y="2.739228759766" />
                  <Point X="-3.185615722656" Y="2.736657226562" />
                  <Point X="-3.165328857422" Y="2.732621826172" />
                  <Point X="-3.143244140625" Y="2.730122802734" />
                  <Point X="-3.069525878906" Y="2.723673339844" />
                  <Point X="-3.059173583984" Y="2.723334472656" />
                  <Point X="-3.038492919922" Y="2.723785644531" />
                  <Point X="-3.028164550781" Y="2.724575683594" />
                  <Point X="-3.006705810547" Y="2.727400878906" />
                  <Point X="-2.996524902344" Y="2.729310791016" />
                  <Point X="-2.976432861328" Y="2.734227539062" />
                  <Point X="-2.956996826172" Y="2.741302001953" />
                  <Point X="-2.938445068359" Y="2.750450683594" />
                  <Point X="-2.929418212891" Y="2.755531738281" />
                  <Point X="-2.9111640625" Y="2.767161132812" />
                  <Point X="-2.902744873047" Y="2.773194335938" />
                  <Point X="-2.886614990234" Y="2.786140136719" />
                  <Point X="-2.870506347656" Y="2.801450439453" />
                  <Point X="-2.818180664062" Y="2.853776123047" />
                  <Point X="-2.811267089844" Y="2.861487792969" />
                  <Point X="-2.798319335938" Y="2.877619628906" />
                  <Point X="-2.79228515625" Y="2.886039794922" />
                  <Point X="-2.780655761719" Y="2.904293945312" />
                  <Point X="-2.775574707031" Y="2.913320800781" />
                  <Point X="-2.766426025391" Y="2.931872558594" />
                  <Point X="-2.7593515625" Y="2.95130859375" />
                  <Point X="-2.754434814453" Y="2.971400634766" />
                  <Point X="-2.752524902344" Y="2.981581542969" />
                  <Point X="-2.749699707031" Y="3.003040283203" />
                  <Point X="-2.748909667969" Y="3.013368652344" />
                  <Point X="-2.748458496094" Y="3.034049316406" />
                  <Point X="-2.749832519531" Y="3.056232910156" />
                  <Point X="-2.756281982422" Y="3.129951171875" />
                  <Point X="-2.757745849609" Y="3.140204833984" />
                  <Point X="-2.76178125" Y="3.160491699219" />
                  <Point X="-2.764352783203" Y="3.170524902344" />
                  <Point X="-2.770861328125" Y="3.191167236328" />
                  <Point X="-2.774509521484" Y="3.200860595703" />
                  <Point X="-2.782840087891" Y="3.219793457031" />
                  <Point X="-2.787522460938" Y="3.229032958984" />
                  <Point X="-3.059387207031" Y="3.699916503906" />
                  <Point X="-2.648373535156" Y="4.015036621094" />
                  <Point X="-2.573614990234" Y="4.056571289062" />
                  <Point X="-2.192525146484" Y="4.268296386719" />
                  <Point X="-2.146219726563" Y="4.207950195312" />
                  <Point X="-2.118563964844" Y="4.171908691406" />
                  <Point X="-2.111819580078" Y="4.164046386719" />
                  <Point X="-2.097516601562" Y="4.14910546875" />
                  <Point X="-2.089957763672" Y="4.142026367188" />
                  <Point X="-2.073377685547" Y="4.128113769531" />
                  <Point X="-2.065092529297" Y="4.1218984375" />
                  <Point X="-2.04789453125" Y="4.110406738281" />
                  <Point X="-2.025813720703" Y="4.098274902344" />
                  <Point X="-1.94376550293" Y="4.055563232422" />
                  <Point X="-1.934330078125" Y="4.051287841797" />
                  <Point X="-1.915048828125" Y="4.043789794922" />
                  <Point X="-1.905203491211" Y="4.040567138672" />
                  <Point X="-1.884296875" Y="4.034965576172" />
                  <Point X="-1.874162109375" Y="4.032834716797" />
                  <Point X="-1.853718994141" Y="4.029688232422" />
                  <Point X="-1.833052856445" Y="4.028785888672" />
                  <Point X="-1.812413574219" Y="4.030138916016" />
                  <Point X="-1.802132080078" Y="4.031378662109" />
                  <Point X="-1.780816772461" Y="4.035136962891" />
                  <Point X="-1.770731079102" Y="4.037488525391" />
                  <Point X="-1.750871582031" Y="4.043276611328" />
                  <Point X="-1.72738293457" Y="4.052394775391" />
                  <Point X="-1.641924316406" Y="4.087793212891" />
                  <Point X="-1.632585449219" Y="4.092272705078" />
                  <Point X="-1.614450439453" Y="4.102221679688" />
                  <Point X="-1.605654418945" Y="4.10769140625" />
                  <Point X="-1.587924804688" Y="4.120105957031" />
                  <Point X="-1.579776245117" Y="4.126501464844" />
                  <Point X="-1.564224853516" Y="4.140140136719" />
                  <Point X="-1.550250976562" Y="4.155390136719" />
                  <Point X="-1.538020019531" Y="4.172071289062" />
                  <Point X="-1.532359863281" Y="4.180745605469" />
                  <Point X="-1.521538085938" Y="4.199489746094" />
                  <Point X="-1.516856201172" Y="4.208728515625" />
                  <Point X="-1.508526000977" Y="4.22766015625" />
                  <Point X="-1.500413818359" Y="4.251510253906" />
                  <Point X="-1.472598754883" Y="4.339728515625" />
                  <Point X="-1.470026855469" Y="4.349763183594" />
                  <Point X="-1.465991088867" Y="4.37005078125" />
                  <Point X="-1.46452722168" Y="4.3803046875" />
                  <Point X="-1.46264074707" Y="4.401866210938" />
                  <Point X="-1.462301757812" Y="4.41221875" />
                  <Point X="-1.462752929688" Y="4.432898925781" />
                  <Point X="-1.46354309082" Y="4.4432265625" />
                  <Point X="-1.479266235352" Y="4.562655761719" />
                  <Point X="-0.931179260254" Y="4.7163203125" />
                  <Point X="-0.840557739258" Y="4.72692578125" />
                  <Point X="-0.365221893311" Y="4.782557128906" />
                  <Point X="-0.252390869141" Y="4.361466308594" />
                  <Point X="-0.225666244507" Y="4.261727539062" />
                  <Point X="-0.220435317993" Y="4.247107910156" />
                  <Point X="-0.207661773682" Y="4.218916503906" />
                  <Point X="-0.200119155884" Y="4.205344726562" />
                  <Point X="-0.182260925293" Y="4.178618164062" />
                  <Point X="-0.172608306885" Y="4.166456054688" />
                  <Point X="-0.151451400757" Y="4.1438671875" />
                  <Point X="-0.126898002625" Y="4.125026367188" />
                  <Point X="-0.099602806091" Y="4.110436523438" />
                  <Point X="-0.085356712341" Y="4.104260742188" />
                  <Point X="-0.054918586731" Y="4.093927978516" />
                  <Point X="-0.039857292175" Y="4.090155273438" />
                  <Point X="-0.009319890976" Y="4.08511328125" />
                  <Point X="0.02163187027" Y="4.08511328125" />
                  <Point X="0.052169120789" Y="4.090155273438" />
                  <Point X="0.067230415344" Y="4.093927978516" />
                  <Point X="0.097668540955" Y="4.104260742188" />
                  <Point X="0.111914489746" Y="4.110436523438" />
                  <Point X="0.139209686279" Y="4.125026367188" />
                  <Point X="0.163763076782" Y="4.143866699219" />
                  <Point X="0.184920150757" Y="4.166455566406" />
                  <Point X="0.194573059082" Y="4.178618164062" />
                  <Point X="0.212431289673" Y="4.205344726562" />
                  <Point X="0.219973449707" Y="4.218916015625" />
                  <Point X="0.232746994019" Y="4.247107421875" />
                  <Point X="0.237978225708" Y="4.261727539062" />
                  <Point X="0.378190185547" Y="4.785006347656" />
                  <Point X="0.827878051758" Y="4.737912109375" />
                  <Point X="0.902860046387" Y="4.719809082031" />
                  <Point X="1.453596191406" Y="4.586844238281" />
                  <Point X="1.500153076172" Y="4.569957519531" />
                  <Point X="1.858258056641" Y="4.4400703125" />
                  <Point X="1.905456665039" Y="4.417997070312" />
                  <Point X="2.250450683594" Y="4.256654296875" />
                  <Point X="2.296107421875" Y="4.2300546875" />
                  <Point X="2.629432617188" Y="4.035858886719" />
                  <Point X="2.672441894531" Y="4.005272949219" />
                  <Point X="2.817779785156" Y="3.901916748047" />
                  <Point X="2.184467529297" Y="2.804987548828" />
                  <Point X="2.06530859375" Y="2.598597900391" />
                  <Point X="2.061124755859" Y="2.590471923828" />
                  <Point X="2.050173828125" Y="2.565365478516" />
                  <Point X="2.042203125" Y="2.542178955078" />
                  <Point X="2.038332397461" Y="2.529495361328" />
                  <Point X="2.01983190918" Y="2.460312255859" />
                  <Point X="2.017456298828" Y="2.448443847656" />
                  <Point X="2.014233520508" Y="2.424501464844" />
                  <Point X="2.013386230469" Y="2.412427490234" />
                  <Point X="2.013206298828" Y="2.383924072266" />
                  <Point X="2.014568725586" Y="2.359979248047" />
                  <Point X="2.021782348633" Y="2.300155517578" />
                  <Point X="2.02380065918" Y="2.289033935547" />
                  <Point X="2.029143676758" Y="2.267110595703" />
                  <Point X="2.032468261719" Y="2.256308837891" />
                  <Point X="2.040734375" Y="2.234220214844" />
                  <Point X="2.045318359375" Y="2.223889160156" />
                  <Point X="2.055681152344" Y="2.203843994141" />
                  <Point X="2.067400878906" Y="2.185374511719" />
                  <Point X="2.104417724609" Y="2.130821289063" />
                  <Point X="2.111909423828" Y="2.121177978516" />
                  <Point X="2.128067626953" Y="2.102932861328" />
                  <Point X="2.136734130859" Y="2.094331054688" />
                  <Point X="2.158789306641" Y="2.07508984375" />
                  <Point X="2.177013427734" Y="2.061040039062" />
                  <Point X="2.231566894531" Y="2.0240234375" />
                  <Point X="2.241280517578" Y="2.018244873047" />
                  <Point X="2.261325683594" Y="2.007882080078" />
                  <Point X="2.271657226562" Y="2.003297851562" />
                  <Point X="2.293745605469" Y="1.995031982422" />
                  <Point X="2.304547363281" Y="1.991707519531" />
                  <Point X="2.326470214844" Y="1.986364746094" />
                  <Point X="2.347192382812" Y="1.983188842773" />
                  <Point X="2.407016113281" Y="1.975974975586" />
                  <Point X="2.419460449219" Y="1.975297851563" />
                  <Point X="2.444330810547" Y="1.97557824707" />
                  <Point X="2.456756835938" Y="1.976536010742" />
                  <Point X="2.486762695312" Y="1.980842895508" />
                  <Point X="2.5088515625" Y="1.985365112305" />
                  <Point X="2.578034667969" Y="2.003865600586" />
                  <Point X="2.583994384766" Y="2.005670654297" />
                  <Point X="2.604404541016" Y="2.013067138672" />
                  <Point X="2.627655029297" Y="2.02357409668" />
                  <Point X="2.636033935547" Y="2.027872558594" />
                  <Point X="3.922027587891" Y="2.770341308594" />
                  <Point X="3.940404785156" Y="2.780951416016" />
                  <Point X="4.043954101562" Y="2.637041259766" />
                  <Point X="4.067930175781" Y="2.597420654297" />
                  <Point X="4.136884765625" Y="2.48347265625" />
                  <Point X="3.329835693359" Y="1.864201171875" />
                  <Point X="3.172951660156" Y="1.743819824219" />
                  <Point X="3.165824462891" Y="1.737771240234" />
                  <Point X="3.145701416016" Y="1.718283813477" />
                  <Point X="3.128465576172" Y="1.69860949707" />
                  <Point X="3.120585449219" Y="1.688997558594" />
                  <Point X="3.070794189453" Y="1.624041259766" />
                  <Point X="3.063918701172" Y="1.613745727539" />
                  <Point X="3.051565185547" Y="1.592348266602" />
                  <Point X="3.046087158203" Y="1.581246337891" />
                  <Point X="3.034641113281" Y="1.553650756836" />
                  <Point X="3.0271640625" Y="1.532028686523" />
                  <Point X="3.008616699219" Y="1.465708007812" />
                  <Point X="3.006225585938" Y="1.454662109375" />
                  <Point X="3.002771972656" Y="1.43236340332" />
                  <Point X="3.001709472656" Y="1.421110717773" />
                  <Point X="3.000893310547" Y="1.397540771484" />
                  <Point X="3.001174804688" Y="1.386241333008" />
                  <Point X="3.003077880859" Y="1.363756347656" />
                  <Point X="3.007143066406" Y="1.340727783203" />
                  <Point X="3.022368408203" Y="1.266937744141" />
                  <Point X="3.025691894531" Y="1.254895019531" />
                  <Point X="3.033890380859" Y="1.231350341797" />
                  <Point X="3.038765136719" Y="1.219848266602" />
                  <Point X="3.052609130859" Y="1.192368408203" />
                  <Point X="3.063564941406" Y="1.173423461914" />
                  <Point X="3.1049765625" Y="1.110479980469" />
                  <Point X="3.111739501953" Y="1.101424072266" />
                  <Point X="3.126292724609" Y="1.0841796875" />
                  <Point X="3.134083007812" Y="1.075991333008" />
                  <Point X="3.151326904297" Y="1.059901367188" />
                  <Point X="3.160034667969" Y="1.052695922852" />
                  <Point X="3.178243408203" Y="1.039370849609" />
                  <Point X="3.197374755859" Y="1.027829711914" />
                  <Point X="3.257385742188" Y="0.994048950195" />
                  <Point X="3.268893066406" Y="0.988553161621" />
                  <Point X="3.292539550781" Y="0.979152587891" />
                  <Point X="3.304679443359" Y="0.975247680664" />
                  <Point X="3.335676757812" Y="0.967528808594" />
                  <Point X="3.356692871094" Y="0.963536682129" />
                  <Point X="3.437831542969" Y="0.952812927246" />
                  <Point X="3.444029785156" Y="0.952199707031" />
                  <Point X="3.465716064453" Y="0.95122277832" />
                  <Point X="3.491217529297" Y="0.952032348633" />
                  <Point X="3.500603515625" Y="0.952797302246" />
                  <Point X="4.704703613281" Y="1.111320068359" />
                  <Point X="4.752684082031" Y="0.914230957031" />
                  <Point X="4.760239746094" Y="0.865701599121" />
                  <Point X="4.78387109375" Y="0.713920959473" />
                  <Point X="3.871784423828" Y="0.469528076172" />
                  <Point X="3.691991943359" Y="0.421352844238" />
                  <Point X="3.682920898438" Y="0.418427215576" />
                  <Point X="3.656387451172" Y="0.407863098145" />
                  <Point X="3.631512939453" Y="0.395460510254" />
                  <Point X="3.621210693359" Y="0.389921630859" />
                  <Point X="3.541494384766" Y="0.343843933105" />
                  <Point X="3.531002929688" Y="0.336809753418" />
                  <Point X="3.511045410156" Y="0.321411224365" />
                  <Point X="3.501579345703" Y="0.313046722412" />
                  <Point X="3.479591064453" Y="0.290845001221" />
                  <Point X="3.465120361328" Y="0.27444519043" />
                  <Point X="3.417290771484" Y="0.213498657227" />
                  <Point X="3.410854492188" Y="0.204208236694" />
                  <Point X="3.399130615234" Y="0.184928588867" />
                  <Point X="3.393843017578" Y="0.174939331055" />
                  <Point X="3.384069091797" Y="0.153475662231" />
                  <Point X="3.380005371094" Y="0.142929092407" />
                  <Point X="3.373159179688" Y="0.121428405762" />
                  <Point X="3.367817871094" Y="0.097113845825" />
                  <Point X="3.351874511719" Y="0.013864059448" />
                  <Point X="3.350322021484" Y="0.001210008025" />
                  <Point X="3.348925292969" Y="-0.024192321777" />
                  <Point X="3.349081054688" Y="-0.036940452576" />
                  <Point X="3.351639892578" Y="-0.069110626221" />
                  <Point X="3.354432861328" Y="-0.089783325195" />
                  <Point X="3.370376220703" Y="-0.173033111572" />
                  <Point X="3.373158935547" Y="-0.183987808228" />
                  <Point X="3.380005371094" Y="-0.205489227295" />
                  <Point X="3.384069091797" Y="-0.216035812378" />
                  <Point X="3.393843017578" Y="-0.237499633789" />
                  <Point X="3.399130371094" Y="-0.247488433838" />
                  <Point X="3.410853515625" Y="-0.266767059326" />
                  <Point X="3.424965332031" Y="-0.285838195801" />
                  <Point X="3.472795166016" Y="-0.346784423828" />
                  <Point X="3.481510742188" Y="-0.356447021484" />
                  <Point X="3.500180664062" Y="-0.37449041748" />
                  <Point X="3.510135009766" Y="-0.382871429443" />
                  <Point X="3.537240722656" Y="-0.402686706543" />
                  <Point X="3.554288085938" Y="-0.413799102783" />
                  <Point X="3.634004394531" Y="-0.459876647949" />
                  <Point X="3.639496582031" Y="-0.46281552124" />
                  <Point X="3.659158447266" Y="-0.472016937256" />
                  <Point X="3.683028076172" Y="-0.481027770996" />
                  <Point X="3.691991943359" Y="-0.483912841797" />
                  <Point X="4.784876953125" Y="-0.776750610352" />
                  <Point X="4.761612792969" Y="-0.931054992676" />
                  <Point X="4.751932617188" Y="-0.973476989746" />
                  <Point X="4.727802246094" Y="-1.079219848633" />
                  <Point X="3.645290771484" Y="-0.936704284668" />
                  <Point X="3.436781982422" Y="-0.90925378418" />
                  <Point X="3.428624511719" Y="-0.908535766602" />
                  <Point X="3.400098388672" Y="-0.908042480469" />
                  <Point X="3.366721435547" Y="-0.910840942383" />
                  <Point X="3.354481201172" Y="-0.912676269531" />
                  <Point X="3.329371826172" Y="-0.918133850098" />
                  <Point X="3.172916992188" Y="-0.952139953613" />
                  <Point X="3.157874023438" Y="-0.956742370605" />
                  <Point X="3.128753662109" Y="-0.968366882324" />
                  <Point X="3.114676269531" Y="-0.975389038086" />
                  <Point X="3.086849609375" Y="-0.99228137207" />
                  <Point X="3.074123779297" Y="-1.001530456543" />
                  <Point X="3.050373779297" Y="-1.022001525879" />
                  <Point X="3.039349609375" Y="-1.033223144531" />
                  <Point X="3.024172363281" Y="-1.05147668457" />
                  <Point X="2.92960546875" Y="-1.165211303711" />
                  <Point X="2.921326171875" Y="-1.176847290039" />
                  <Point X="2.90660546875" Y="-1.201229370117" />
                  <Point X="2.9001640625" Y="-1.213975830078" />
                  <Point X="2.888820800781" Y="-1.241360839844" />
                  <Point X="2.884362792969" Y="-1.254927978516" />
                  <Point X="2.877531005859" Y="-1.282577636719" />
                  <Point X="2.875157226562" Y="-1.296659912109" />
                  <Point X="2.872981933594" Y="-1.320298828125" />
                  <Point X="2.859428222656" Y="-1.467590209961" />
                  <Point X="2.859288818359" Y="-1.483321166992" />
                  <Point X="2.861607666016" Y="-1.514590454102" />
                  <Point X="2.864065917969" Y="-1.530128662109" />
                  <Point X="2.871797607422" Y="-1.561749633789" />
                  <Point X="2.876786376953" Y="-1.576668823242" />
                  <Point X="2.889157714844" Y="-1.605479980469" />
                  <Point X="2.896540527344" Y="-1.619371948242" />
                  <Point X="2.910436523438" Y="-1.640986083984" />
                  <Point X="2.997020751953" Y="-1.775662231445" />
                  <Point X="3.001741943359" Y="-1.782353149414" />
                  <Point X="3.019792724609" Y="-1.804448852539" />
                  <Point X="3.043488769531" Y="-1.828119873047" />
                  <Point X="3.052796142578" Y="-1.836277709961" />
                  <Point X="4.087170654297" Y="-2.629981933594" />
                  <Point X="4.045490478516" Y="-2.697426757812" />
                  <Point X="4.025466064453" Y="-2.725878662109" />
                  <Point X="4.001274414063" Y="-2.760251708984" />
                  <Point X="3.034159667969" Y="-2.201887695312" />
                  <Point X="2.848454589844" Y="-2.094670654297" />
                  <Point X="2.841191894531" Y="-2.090885253906" />
                  <Point X="2.815026611328" Y="-2.079513427734" />
                  <Point X="2.783119384766" Y="-2.069325927734" />
                  <Point X="2.771107910156" Y="-2.066337158203" />
                  <Point X="2.741223632812" Y="-2.060940185547" />
                  <Point X="2.555017578125" Y="-2.027311645508" />
                  <Point X="2.539358154297" Y="-2.025807250977" />
                  <Point X="2.508006103516" Y="-2.025403198242" />
                  <Point X="2.492313476562" Y="-2.026503662109" />
                  <Point X="2.460140625" Y="-2.031461425781" />
                  <Point X="2.444844726562" Y="-2.035136108398" />
                  <Point X="2.415069335938" Y="-2.044959594727" />
                  <Point X="2.40058984375" Y="-2.051108154297" />
                  <Point X="2.375763183594" Y="-2.064174072266" />
                  <Point X="2.221071777344" Y="-2.145587158203" />
                  <Point X="2.208968994141" Y="-2.153169921875" />
                  <Point X="2.186037841797" Y="-2.170063232422" />
                  <Point X="2.175209472656" Y="-2.179373779297" />
                  <Point X="2.154249755859" Y="-2.200333496094" />
                  <Point X="2.144939453125" Y="-2.211161621094" />
                  <Point X="2.128046386719" Y="-2.234092285156" />
                  <Point X="2.120463623047" Y="-2.246194824219" />
                  <Point X="2.107397705078" Y="-2.271021240234" />
                  <Point X="2.025984619141" Y="-2.425712890625" />
                  <Point X="2.0198359375" Y="-2.440192626953" />
                  <Point X="2.010012084961" Y="-2.469968505859" />
                  <Point X="2.006337280273" Y="-2.485264648438" />
                  <Point X="2.001379516602" Y="-2.5174375" />
                  <Point X="2.000279174805" Y="-2.533130126953" />
                  <Point X="2.000683227539" Y="-2.564482177734" />
                  <Point X="2.00218762207" Y="-2.580141601562" />
                  <Point X="2.007584472656" Y="-2.610025878906" />
                  <Point X="2.041213256836" Y="-2.796231933594" />
                  <Point X="2.043015136719" Y="-2.804222167969" />
                  <Point X="2.051236083984" Y="-2.83154296875" />
                  <Point X="2.064069580078" Y="-2.862479736328" />
                  <Point X="2.069546630859" Y="-2.873578613281" />
                  <Point X="2.735893310547" Y="-4.027724365234" />
                  <Point X="2.723754150391" Y="-4.036083740234" />
                  <Point X="1.975624755859" Y="-3.061103027344" />
                  <Point X="1.833914550781" Y="-2.876422851562" />
                  <Point X="1.828653442383" Y="-2.870146240234" />
                  <Point X="1.808831665039" Y="-2.849626953125" />
                  <Point X="1.783252563477" Y="-2.828005126953" />
                  <Point X="1.773299560547" Y="-2.820647705078" />
                  <Point X="1.743825561523" Y="-2.801698486328" />
                  <Point X="1.560176269531" Y="-2.68362890625" />
                  <Point X="1.546284545898" Y="-2.676246337891" />
                  <Point X="1.517473266602" Y="-2.663874755859" />
                  <Point X="1.502553588867" Y="-2.658885742188" />
                  <Point X="1.470932617188" Y="-2.651154052734" />
                  <Point X="1.45539453125" Y="-2.648695800781" />
                  <Point X="1.424125488281" Y="-2.646376953125" />
                  <Point X="1.40839453125" Y="-2.646516357422" />
                  <Point X="1.376159545898" Y="-2.649482666016" />
                  <Point X="1.175307739258" Y="-2.667965332031" />
                  <Point X="1.161225097656" Y="-2.670339111328" />
                  <Point X="1.133575439453" Y="-2.677170898438" />
                  <Point X="1.120008422852" Y="-2.68162890625" />
                  <Point X="1.092623168945" Y="-2.692972167969" />
                  <Point X="1.079877807617" Y="-2.699413085938" />
                  <Point X="1.05549597168" Y="-2.714133300781" />
                  <Point X="1.04385925293" Y="-2.722412597656" />
                  <Point X="1.018968444824" Y="-2.743108398438" />
                  <Point X="0.863875366211" Y="-2.872062988281" />
                  <Point X="0.852653320312" Y="-2.883087890625" />
                  <Point X="0.832182128906" Y="-2.906838378906" />
                  <Point X="0.822933044434" Y="-2.919564208984" />
                  <Point X="0.80604083252" Y="-2.947390869141" />
                  <Point X="0.799018859863" Y="-2.961468261719" />
                  <Point X="0.787394165039" Y="-2.990588867188" />
                  <Point X="0.782791748047" Y="-3.005631835938" />
                  <Point X="0.775349609375" Y="-3.039872070312" />
                  <Point X="0.728977478027" Y="-3.253219726562" />
                  <Point X="0.727584594727" Y="-3.261290039062" />
                  <Point X="0.724724487305" Y="-3.289677734375" />
                  <Point X="0.7247421875" Y="-3.323170410156" />
                  <Point X="0.725554992676" Y="-3.335520019531" />
                  <Point X="0.833091552734" Y="-4.152340820313" />
                  <Point X="0.710463378906" Y="-3.694686523438" />
                  <Point X="0.655064941406" Y="-3.487936523438" />
                  <Point X="0.652606262207" Y="-3.480124511719" />
                  <Point X="0.642145324707" Y="-3.453580566406" />
                  <Point X="0.626787353516" Y="-3.423816162109" />
                  <Point X="0.620407836914" Y="-3.413210449219" />
                  <Point X="0.597765014648" Y="-3.380586425781" />
                  <Point X="0.456680114746" Y="-3.177310058594" />
                  <Point X="0.446670959473" Y="-3.165173095703" />
                  <Point X="0.424786804199" Y="-3.142717529297" />
                  <Point X="0.41291229248" Y="-3.132399169922" />
                  <Point X="0.386657165527" Y="-3.113155273438" />
                  <Point X="0.373242767334" Y="-3.104937988281" />
                  <Point X="0.345241912842" Y="-3.090829833984" />
                  <Point X="0.330655456543" Y="-3.084938964844" />
                  <Point X="0.295617004395" Y="-3.074064208984" />
                  <Point X="0.077296203613" Y="-3.006305664062" />
                  <Point X="0.063377368927" Y="-3.003109619141" />
                  <Point X="0.03521749115" Y="-2.998840087891" />
                  <Point X="0.02097659111" Y="-2.997766601562" />
                  <Point X="-0.008664761543" Y="-2.997766601562" />
                  <Point X="-0.022905065536" Y="-2.998840087891" />
                  <Point X="-0.051064350128" Y="-3.003109375" />
                  <Point X="-0.064983032227" Y="-3.006305175781" />
                  <Point X="-0.100021476746" Y="-3.0171796875" />
                  <Point X="-0.318342285156" Y="-3.084938476562" />
                  <Point X="-0.332929046631" Y="-3.090829345703" />
                  <Point X="-0.360930633545" Y="-3.104937744141" />
                  <Point X="-0.374345336914" Y="-3.113155273438" />
                  <Point X="-0.400600463867" Y="-3.132399169922" />
                  <Point X="-0.412474975586" Y="-3.142717529297" />
                  <Point X="-0.434358673096" Y="-3.165172607422" />
                  <Point X="-0.444367828369" Y="-3.177309326172" />
                  <Point X="-0.467010528564" Y="-3.209933105469" />
                  <Point X="-0.608095458984" Y="-3.413209716797" />
                  <Point X="-0.612470275879" Y="-3.420132324219" />
                  <Point X="-0.62597668457" Y="-3.445265136719" />
                  <Point X="-0.638777893066" Y="-3.476215820312" />
                  <Point X="-0.642753112793" Y="-3.487936523438" />
                  <Point X="-0.985425354004" Y="-4.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.662770372625" Y="-3.956608174169" />
                  <Point X="2.689517141705" Y="-3.947398523008" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.60178659486" Y="-3.877132608104" />
                  <Point X="2.641128079364" Y="-3.863586248641" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.540802817094" Y="-3.797657042038" />
                  <Point X="2.592739017023" Y="-3.779773974273" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.479819039329" Y="-3.718181475973" />
                  <Point X="2.544349954682" Y="-3.695961699906" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.418835261563" Y="-3.638705909908" />
                  <Point X="2.495960892341" Y="-3.612149425538" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.086829950942" Y="-4.745329280809" />
                  <Point X="-0.968778844194" Y="-4.704681024977" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.816387125558" Y="-4.089999087286" />
                  <Point X="0.824515625155" Y="-4.08720022042" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.357851483798" Y="-3.559230343842" />
                  <Point X="2.44757183" Y="-3.528337151171" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.131905357587" Y="-4.660376023285" />
                  <Point X="-0.939120584834" Y="-4.593994902604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.791739261627" Y="-3.998012062733" />
                  <Point X="0.811861596746" Y="-3.991083387108" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.296867706033" Y="-3.479754777777" />
                  <Point X="2.399182767659" Y="-3.444524876803" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.119791437331" Y="-4.555730901323" />
                  <Point X="-0.909462325473" Y="-4.483308780231" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.767091397696" Y="-3.906025038181" />
                  <Point X="0.799207568337" Y="-3.894966553796" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.235883928267" Y="-3.400279211711" />
                  <Point X="2.350793705318" Y="-3.360712602436" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.131006521558" Y="-4.459118599795" />
                  <Point X="-0.879804066112" Y="-4.372622657858" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.742443533765" Y="-3.814038013628" />
                  <Point X="0.786553539928" Y="-3.798849720484" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.174900150502" Y="-3.320803645646" />
                  <Point X="2.302404642977" Y="-3.276900328068" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.927149655737" Y="-2.717455755621" />
                  <Point X="4.061755694786" Y="-2.67110717946" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.149710937646" Y="-4.365085082032" />
                  <Point X="-0.850145806751" Y="-4.261936535486" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.717795669834" Y="-3.722050989075" />
                  <Point X="0.773899511519" Y="-3.702732887171" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.113916372736" Y="-3.241328079581" />
                  <Point X="2.254015580636" Y="-3.193088053701" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.818137645728" Y="-2.654517636134" />
                  <Point X="4.025910846628" Y="-2.582975585762" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.168415335952" Y="-4.271051558146" />
                  <Point X="-0.82048754739" Y="-4.151250413113" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.69314783215" Y="-3.630063955485" />
                  <Point X="0.76124548311" Y="-3.606616053859" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.052932594971" Y="-3.161852513515" />
                  <Point X="2.205626518295" Y="-3.109275779333" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.709125635719" Y="-2.591579516647" />
                  <Point X="3.935528546684" Y="-2.513622742673" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.188493043153" Y="-4.177490902434" />
                  <Point X="-0.790829288029" Y="-4.04056429074" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.668500005581" Y="-3.538076918068" />
                  <Point X="0.748591454702" Y="-3.510499220547" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.991948817205" Y="-3.08237694745" />
                  <Point X="2.157237455954" Y="-3.025463504966" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.600113625711" Y="-2.528641397161" />
                  <Point X="3.845146246741" Y="-2.444269899583" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.232721008174" Y="-4.092245847357" />
                  <Point X="-0.761171028668" Y="-3.929878168367" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.639120851168" Y="-3.447719007474" />
                  <Point X="0.735937426293" Y="-3.414382387235" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.930965026993" Y="-3.00290138567" />
                  <Point X="2.108848393613" Y="-2.941651230598" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.491101615702" Y="-2.465703277674" />
                  <Point X="3.754763946797" Y="-2.374917056494" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.308716448194" Y="-4.017939211127" />
                  <Point X="-0.731512769307" Y="-3.819192045994" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.587064753172" Y="-3.365169394742" />
                  <Point X="0.724739330447" Y="-3.317764236138" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.869981232231" Y="-2.923425825457" />
                  <Point X="2.061933831928" Y="-2.857331244939" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.382089605693" Y="-2.402765158188" />
                  <Point X="3.664381646853" Y="-2.305564213405" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.390983921326" Y="-3.94579220909" />
                  <Point X="-0.701854509946" Y="-3.708505923622" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.530781119483" Y="-3.284075439285" />
                  <Point X="0.737761513007" Y="-3.212806374384" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.804107798022" Y="-2.845633903125" />
                  <Point X="2.035731599869" Y="-2.765879432254" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.273077595685" Y="-2.339827038701" />
                  <Point X="3.573999346909" Y="-2.236211370315" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.473251394459" Y="-3.873645207052" />
                  <Point X="-0.672196250586" Y="-3.597819801249" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.474497485794" Y="-3.202981483827" />
                  <Point X="0.761366646477" Y="-3.104204510403" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.707581211589" Y="-2.778396707538" />
                  <Point X="2.018648348403" Y="-2.671287702746" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.164065585676" Y="-2.276888919215" />
                  <Point X="3.483617046966" Y="-2.166858527226" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.539183416775" Y="-4.140201071513" />
                  <Point X="-2.467048998025" Y="-4.115363199268" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.578773224431" Y="-3.809505322204" />
                  <Point X="-0.64247325858" Y="-3.487111389639" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.4048492767" Y="-3.126489320722" />
                  <Point X="0.785964805108" Y="-2.995260720437" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.605808165869" Y="-2.712966012755" />
                  <Point X="2.001847267897" Y="-2.576598813984" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.055053575668" Y="-2.213950799728" />
                  <Point X="3.393234747022" Y="-2.097505684136" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.643463261928" Y="-4.075633536996" />
                  <Point X="-2.315046015715" Y="-3.962550410444" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.800654213384" Y="-3.785431108852" />
                  <Point X="-0.573065703929" Y="-3.362738487289" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.279674853438" Y="-3.069116366416" />
                  <Point X="0.872298007402" Y="-2.865059850231" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.484143987896" Y="-2.654384384067" />
                  <Point X="2.009136004367" Y="-2.473615136039" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.946041644773" Y="-2.151012653" />
                  <Point X="3.302852447078" Y="-2.028152841047" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.747721408349" Y="-4.011058531006" />
                  <Point X="-0.481432728632" Y="-3.230712758894" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.126206832412" Y="-3.021485679099" />
                  <Point X="2.063505394813" Y="-2.354420288878" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.835743609952" Y="-2.088517347368" />
                  <Point X="3.212470147134" Y="-1.958799997957" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.837889596558" Y="-3.941631963334" />
                  <Point X="2.13038501972" Y="-2.230917822543" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.664353838551" Y="-2.047057613584" />
                  <Point X="3.122087847191" Y="-1.889447154868" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.928057981694" Y="-3.872205463469" />
                  <Point X="3.03449367838" Y="-1.81913428124" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.950861155331" Y="-3.77958326111" />
                  <Point X="2.973807878328" Y="-1.739556113219" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.878459108694" Y="-3.654179272482" />
                  <Point X="2.920920239033" Y="-1.657292823118" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.635179940514" Y="-1.067025871548" />
                  <Point X="4.738720610365" Y="-1.03137395982" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.806057062056" Y="-3.528775283853" />
                  <Point X="2.875390988257" Y="-1.572495836659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.424091454657" Y="-1.039235501363" />
                  <Point X="4.762890929406" Y="-0.922577486839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.733655015419" Y="-3.403371295224" />
                  <Point X="2.859339972877" Y="-1.477548679763" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.213002968799" Y="-1.011445131179" />
                  <Point X="4.77886864668" Y="-0.816601952872" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.661252968782" Y="-3.277967306596" />
                  <Point X="2.868032866387" Y="-1.374081511776" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.001914482942" Y="-0.983654760994" />
                  <Point X="4.682486274824" Y="-0.749315100223" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.588850922144" Y="-3.152563317967" />
                  <Point X="2.880836692948" Y="-1.269198836022" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.790825997085" Y="-0.95586439081" />
                  <Point X="4.518387378963" Y="-0.705344916666" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.516448875507" Y="-3.027159329338" />
                  <Point X="2.945075479088" Y="-1.146605683397" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.579737348982" Y="-0.928074076491" />
                  <Point X="4.354288483101" Y="-0.661374733109" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.444046828846" Y="-2.901755340702" />
                  <Point X="3.073802363093" Y="-1.001807497949" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.295301915573" Y="-0.925539085699" />
                  <Point X="4.19018958724" Y="-0.617404549552" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.371644777893" Y="-2.776351350587" />
                  <Point X="4.026090691378" Y="-0.573434365994" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.317933470726" Y="-2.657383099671" />
                  <Point X="3.861991795516" Y="-0.529464182437" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.3152485655" Y="-2.555984647949" />
                  <Point X="3.697892899655" Y="-0.48549399888" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.825256297989" Y="-2.975448041814" />
                  <Point X="-3.564200438083" Y="-2.885559300637" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.353885508654" Y="-2.468814449658" />
                  <Point X="3.57669618358" Y="-0.426751410152" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.885851305337" Y="-2.895838611359" />
                  <Point X="-3.133023440651" Y="-2.636619189493" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.427447855359" Y="-2.393670032214" />
                  <Point X="3.483498429307" Y="-0.358368005732" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.946446312686" Y="-2.816229180903" />
                  <Point X="-2.70184644322" Y="-2.38767907835" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.593712248303" Y="-2.350445489099" />
                  <Point X="3.420371755924" Y="-0.2796302978" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.006497769121" Y="-2.736432590859" />
                  <Point X="3.37643746572" Y="-0.194284122375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.056208196872" Y="-2.65307529909" />
                  <Point X="3.356517623095" Y="-0.10066910953" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.105918844489" Y="-2.569718083026" />
                  <Point X="3.35012370671" Y="-0.002396746786" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.155629492105" Y="-2.486360866962" />
                  <Point X="3.369494729701" Y="0.10474719604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.066230239589" Y="-2.355104271001" />
                  <Point X="3.426087268689" Y="0.224707534633" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.82870340588" Y="-2.172843258545" />
                  <Point X="3.634460333335" Y="0.396930099369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.75238880412" Y="0.437536128253" />
                  <Point X="4.772229167185" Y="0.788695326404" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.591176572171" Y="-1.990582246089" />
                  <Point X="4.757381947763" Y="0.884056983489" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.353649738462" Y="-1.808321233633" />
                  <Point X="4.737256848445" Y="0.977601320786" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.116122168731" Y="-1.626059967744" />
                  <Point X="4.714688684945" Y="1.070304443625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.974112327675" Y="-1.476688093397" />
                  <Point X="4.44129621029" Y="1.076641830048" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.94764906921" Y="-1.367102098057" />
                  <Point X="3.968866904719" Y="1.014445339526" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.963317427992" Y="-1.272023181928" />
                  <Point X="3.497232540676" Y="0.952522569322" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.013371507462" Y="-1.188784218935" />
                  <Point X="3.288007113185" Y="0.980954441947" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.111576854855" Y="-1.122125067102" />
                  <Point X="3.174282074563" Y="1.04226973554" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.398719837401" Y="-1.120522360242" />
                  <Point X="3.100470833118" Y="1.117328451652" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.871149511004" Y="-1.182718977488" />
                  <Point X="3.048765088956" Y="1.199998700884" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.343579184606" Y="-1.244915594734" />
                  <Point X="3.01765900784" Y="1.289761982927" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.668664737764" Y="-1.256377562655" />
                  <Point X="3.001313080549" Y="1.384607593509" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.692254067242" Y="-1.164026055461" />
                  <Point X="3.01539031333" Y="1.489928738187" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.71584339672" Y="-1.071674548266" />
                  <Point X="3.059138821344" Y="1.605466522249" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.739432726199" Y="-0.979323041072" />
                  <Point X="3.176047565423" Y="1.746195395783" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.753685765193" Y="-0.883756791258" />
                  <Point X="3.413574798311" Y="1.928456545688" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.767381175139" Y="-0.787998534365" />
                  <Point X="3.651101591371" Y="2.110717544147" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.781076585084" Y="-0.692240277472" />
                  <Point X="-4.270116264881" Y="-0.516302529931" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.34804739788" Y="-0.198808757668" />
                  <Point X="3.88862838443" Y="2.292978542606" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.300089922871" Y="-0.081821710046" />
                  <Point X="4.126155177489" Y="2.475239541066" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.300289224731" Y="0.018583629533" />
                  <Point X="2.390257107969" Y="1.977995866586" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.785165615196" Y="2.113973770347" />
                  <Point X="4.08884107508" Y="2.562865229954" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.330347963016" Y="0.108707540634" />
                  <Point X="2.231774544027" Y="2.023899908309" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.216342467241" Y="2.362913831431" />
                  <Point X="4.037701809402" Y="2.64573053337" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.395176443517" Y="0.186859269383" />
                  <Point X="2.138938562421" Y="2.092407881048" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.647519319287" Y="2.611853892514" />
                  <Point X="3.979761744566" Y="2.726254133844" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.506494986756" Y="0.249003185787" />
                  <Point X="2.076820254438" Y="2.171492797031" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.670385355947" Y="0.293045170835" />
                  <Point X="2.032360756299" Y="2.256658128861" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.834484313196" Y="0.337015333255" />
                  <Point X="2.015607620493" Y="2.351363526307" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.998583270445" Y="0.380985495675" />
                  <Point X="2.018322706755" Y="2.452772370192" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.162682227694" Y="0.424955658095" />
                  <Point X="2.049724672431" Y="2.564058898799" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.326781184943" Y="0.468925820514" />
                  <Point X="2.116688293085" Y="2.687590287189" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.490880142192" Y="0.512895982934" />
                  <Point X="2.189090183376" Y="2.812994221983" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.654979099441" Y="0.556866145354" />
                  <Point X="2.261492188929" Y="2.938398196465" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.782235593581" Y="0.613522185164" />
                  <Point X="2.333894194482" Y="3.063802170947" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.766569697385" Y="0.719390350524" />
                  <Point X="2.406296200035" Y="3.189206145429" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.750903801188" Y="0.825258515884" />
                  <Point X="2.478698205588" Y="3.314610119912" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.735237904992" Y="0.931126681244" />
                  <Point X="-3.893831045314" Y="1.220846297043" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.465027735314" Y="1.368495117346" />
                  <Point X="2.551100211141" Y="3.440014094394" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.708791211405" Y="1.040706972839" />
                  <Point X="-4.104919537572" Y="1.248636665023" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.425173358845" Y="1.482692044387" />
                  <Point X="2.623502216694" Y="3.565418068876" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.678762888699" Y="1.15152051824" />
                  <Point X="-4.316007995023" Y="1.276427044989" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.429320452349" Y="1.581738050292" />
                  <Point X="2.695904222247" Y="3.690822043358" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.648734565993" Y="1.262334063641" />
                  <Point X="-4.527096452474" Y="1.304217424955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.466593831872" Y="1.669377761194" />
                  <Point X="2.7683062278" Y="3.81622601784" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.523063537475" Y="1.750407646953" />
                  <Point X="2.787633697535" Y="3.923354964078" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.61051949114" Y="1.820768111872" />
                  <Point X="2.692440286074" Y="3.991051208621" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.700901813892" Y="1.890120947108" />
                  <Point X="2.592783642349" Y="4.057210639052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.791284136644" Y="1.959473782344" />
                  <Point X="2.484389214325" Y="4.120361409069" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.881666459397" Y="2.02882661758" />
                  <Point X="2.375994786302" Y="4.183512179087" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.972048782149" Y="2.098179452816" />
                  <Point X="2.267600303996" Y="4.246662930413" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.062431104901" Y="2.167532288052" />
                  <Point X="2.146290666517" Y="4.305366637184" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.152813427653" Y="2.236885123288" />
                  <Point X="2.022553637228" Y="4.363234525926" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.214961849854" Y="2.315959670114" />
                  <Point X="-3.02828054725" Y="2.724566810776" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.918910763548" Y="2.762225847364" />
                  <Point X="1.898816598972" Y="4.42110241158" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.141564902597" Y="2.441706230499" />
                  <Point X="-3.235649268537" Y="2.753637998617" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.776458932641" Y="2.911749911021" />
                  <Point X="1.762731430258" Y="4.474718494946" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.068167955339" Y="2.567452790884" />
                  <Point X="-3.346749951541" Y="2.815856930316" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.748726318122" Y="3.021772980702" />
                  <Point X="1.620625296629" Y="4.526261393832" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.991350716938" Y="2.694377051954" />
                  <Point X="-3.455761923579" Y="2.878795062877" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.755406711375" Y="3.11994670155" />
                  <Point X="1.478519299466" Y="4.577804339708" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.884580669942" Y="2.83161489212" />
                  <Point X="-3.564773895617" Y="2.941733195437" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.779470285204" Y="3.212134913319" />
                  <Point X="-0.127313980384" Y="4.125345563829" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.233651758445" Y="4.249636035159" />
                  <Point X="1.312151690007" Y="4.620993342527" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.777810597873" Y="2.968852740919" />
                  <Point X="-3.673785867655" Y="3.004671327998" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.826437975057" Y="3.296436605483" />
                  <Point X="-0.197623653376" Y="4.201609966649" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.264506975165" Y="4.360734303002" />
                  <Point X="1.140623790926" Y="4.662405515136" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.874827018762" Y="3.380248886267" />
                  <Point X="-0.233197498261" Y="4.289834874257" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.294165191413" Y="4.47142041053" />
                  <Point X="0.969095891846" Y="4.703817687746" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.923216062466" Y="3.464061167052" />
                  <Point X="-0.257845155945" Y="4.381821969826" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.323823407662" Y="4.582106518058" />
                  <Point X="0.788340854448" Y="4.742052701841" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.971605106171" Y="3.547873447836" />
                  <Point X="-0.282493021939" Y="4.473808993668" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.35348162391" Y="4.692792625586" />
                  <Point X="0.56459507245" Y="4.765484815455" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.019994149876" Y="3.631685728621" />
                  <Point X="-1.860900722754" Y="4.030793601961" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.577727998941" Y="4.1282977901" />
                  <Point X="-0.307140887933" Y="4.565796017511" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.015161992353" Y="3.7338235386" />
                  <Point X="-1.998305849768" Y="4.083955187236" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.499040442422" Y="4.255866053345" />
                  <Point X="-0.331788753927" Y="4.657783041353" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.777276371444" Y="3.916208091397" />
                  <Point X="-2.098353708012" Y="4.149979911704" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.4664888779" Y="4.367548420578" />
                  <Point X="-0.356436619922" Y="4.749770065195" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.430481440597" Y="4.136093126949" />
                  <Point X="-2.162014156189" Y="4.228533826235" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.466793702995" Y="4.467917425594" />
                  <Point X="-0.649710409579" Y="4.749261765875" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="0.001626220703" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.5269375" Y="-3.743862304688" />
                  <Point X="0.471539031982" Y="-3.537112304688" />
                  <Point X="0.464318664551" Y="-3.521544189453" />
                  <Point X="0.441675994873" Y="-3.488920166016" />
                  <Point X="0.300591033936" Y="-3.285643798828" />
                  <Point X="0.274335754395" Y="-3.266399902344" />
                  <Point X="0.239297439575" Y="-3.255525146484" />
                  <Point X="0.020976625443" Y="-3.187766601562" />
                  <Point X="-0.008664756775" Y="-3.187766601562" />
                  <Point X="-0.043703224182" Y="-3.198641113281" />
                  <Point X="-0.262023895264" Y="-3.266399902344" />
                  <Point X="-0.288279144287" Y="-3.285643798828" />
                  <Point X="-0.310921966553" Y="-3.318267578125" />
                  <Point X="-0.45200680542" Y="-3.521544189453" />
                  <Point X="-0.459227142334" Y="-3.537112304688" />
                  <Point X="-0.804409912109" Y="-4.825352050781" />
                  <Point X="-0.847744018555" Y="-4.987077148438" />
                  <Point X="-1.100246948242" Y="-4.938065429688" />
                  <Point X="-1.139680419922" Y="-4.927919433594" />
                  <Point X="-1.35158972168" Y="-4.873396972656" />
                  <Point X="-1.315818969727" Y="-4.601692382812" />
                  <Point X="-1.309150146484" Y="-4.551038574219" />
                  <Point X="-1.309683227539" Y="-4.5347578125" />
                  <Point X="-1.318054077148" Y="-4.49267578125" />
                  <Point X="-1.370210693359" Y="-4.230466308594" />
                  <Point X="-1.386282714844" Y="-4.20262890625" />
                  <Point X="-1.418541625977" Y="-4.174338378906" />
                  <Point X="-1.619543457031" Y="-3.998063964844" />
                  <Point X="-1.649240722656" Y="-3.985762939453" />
                  <Point X="-1.692055419922" Y="-3.982956787109" />
                  <Point X="-1.958829833984" Y="-3.965471435547" />
                  <Point X="-1.989878540039" Y="-3.973791015625" />
                  <Point X="-2.025554077148" Y="-3.997628662109" />
                  <Point X="-2.247844970703" Y="-4.146159179688" />
                  <Point X="-2.259734375" Y="-4.157294433594" />
                  <Point X="-2.453527832031" Y="-4.409850585938" />
                  <Point X="-2.457095703125" Y="-4.414500488281" />
                  <Point X="-2.479401367188" Y="-4.400688964844" />
                  <Point X="-2.855830810547" Y="-4.16761328125" />
                  <Point X="-2.910440673828" Y="-4.125565429687" />
                  <Point X="-3.228581054688" Y="-3.880608154297" />
                  <Point X="-2.608985595703" Y="-2.8074375" />
                  <Point X="-2.506033691406" Y="-2.629119628906" />
                  <Point X="-2.499762695312" Y="-2.597593017578" />
                  <Point X="-2.516379150391" Y="-2.566364501953" />
                  <Point X="-2.531328613281" Y="-2.551415283203" />
                  <Point X="-2.560157470703" Y="-2.537198730469" />
                  <Point X="-2.59168359375" Y="-2.543469726562" />
                  <Point X="-3.702765136719" Y="-3.184952880859" />
                  <Point X="-3.842959228516" Y="-3.265894042969" />
                  <Point X="-3.864301513672" Y="-3.237854736328" />
                  <Point X="-4.161703613281" Y="-2.847129394531" />
                  <Point X="-4.2008515625" Y="-2.781484130859" />
                  <Point X="-4.431020019531" Y="-2.395527099609" />
                  <Point X="-3.343919189453" Y="-1.561365234375" />
                  <Point X="-3.163786865234" Y="-1.423144775391" />
                  <Point X="-3.152535644531" Y="-1.411081542969" />
                  <Point X="-3.144756591797" Y="-1.391899780273" />
                  <Point X="-3.138117431641" Y="-1.366266357422" />
                  <Point X="-3.136651855469" Y="-1.350051391602" />
                  <Point X="-3.148656738281" Y="-1.321068725586" />
                  <Point X="-3.164821289062" Y="-1.308483764648" />
                  <Point X="-3.187641357422" Y="-1.295052734375" />
                  <Point X="-3.219529296875" Y="-1.288571044922" />
                  <Point X="-4.622164550781" Y="-1.473231567383" />
                  <Point X="-4.803283203125" Y="-1.497076293945" />
                  <Point X="-4.811077148438" Y="-1.466563232422" />
                  <Point X="-4.927393066406" Y="-1.011191589355" />
                  <Point X="-4.937750488281" Y="-0.938773498535" />
                  <Point X="-4.998396484375" Y="-0.514742126465" />
                  <Point X="-3.762661621094" Y="-0.18362802124" />
                  <Point X="-3.557463867188" Y="-0.128645431519" />
                  <Point X="-3.538057617188" Y="-0.11876121521" />
                  <Point X="-3.514142822266" Y="-0.102162895203" />
                  <Point X="-3.50232421875" Y="-0.090645332336" />
                  <Point X="-3.493619628906" Y="-0.071785728455" />
                  <Point X="-3.485647949219" Y="-0.046100891113" />
                  <Point X="-3.486927246094" Y="-0.012337003708" />
                  <Point X="-3.494898925781" Y="0.013347833633" />
                  <Point X="-3.50232421875" Y="0.028085250854" />
                  <Point X="-3.517980957031" Y="0.042266662598" />
                  <Point X="-3.541895751953" Y="0.05886498642" />
                  <Point X="-3.557463867188" Y="0.066085334778" />
                  <Point X="-4.836032226562" Y="0.408676635742" />
                  <Point X="-4.998186523438" Y="0.452125823975" />
                  <Point X="-4.992609375" Y="0.489816711426" />
                  <Point X="-4.917645019531" Y="0.996418151855" />
                  <Point X="-4.896794433594" Y="1.073362915039" />
                  <Point X="-4.773516113281" Y="1.528298583984" />
                  <Point X="-3.900357421875" Y="1.413344970703" />
                  <Point X="-3.753266357422" Y="1.393980102539" />
                  <Point X="-3.731704833984" Y="1.395866577148" />
                  <Point X="-3.723209960938" Y="1.398545043945" />
                  <Point X="-3.670278808594" Y="1.415234130859" />
                  <Point X="-3.651534423828" Y="1.426056152344" />
                  <Point X="-3.639119873047" Y="1.443785888672" />
                  <Point X="-3.635711181641" Y="1.452015014648" />
                  <Point X="-3.614472412109" Y="1.503290161133" />
                  <Point X="-3.610714111328" Y="1.52460534668" />
                  <Point X="-3.616315917969" Y="1.54551171875" />
                  <Point X="-3.620428710938" Y="1.553412475586" />
                  <Point X="-3.646055664062" Y="1.602641479492" />
                  <Point X="-3.659968017578" Y="1.619221679688" />
                  <Point X="-4.393358398438" Y="2.181971679688" />
                  <Point X="-4.476105957031" Y="2.245466308594" />
                  <Point X="-4.451298828125" Y="2.287966796875" />
                  <Point X="-4.160014648438" Y="2.787006591797" />
                  <Point X="-4.104784179688" Y="2.857997558594" />
                  <Point X="-3.774671142578" Y="3.282310791016" />
                  <Point X="-3.24949609375" Y="2.979100585938" />
                  <Point X="-3.159156982422" Y="2.926943359375" />
                  <Point X="-3.138514648438" Y="2.920434814453" />
                  <Point X="-3.12668359375" Y="2.919399658203" />
                  <Point X="-3.052965332031" Y="2.912950195312" />
                  <Point X="-3.031506591797" Y="2.915775390625" />
                  <Point X="-3.013252441406" Y="2.927404785156" />
                  <Point X="-3.004854492188" Y="2.935802490234" />
                  <Point X="-2.952528808594" Y="2.988128173828" />
                  <Point X="-2.940899414062" Y="3.006382324219" />
                  <Point X="-2.93807421875" Y="3.027841064453" />
                  <Point X="-2.939109375" Y="3.039672363281" />
                  <Point X="-2.945558837891" Y="3.113390625" />
                  <Point X="-2.952067382812" Y="3.134032958984" />
                  <Point X="-3.2770546875" Y="3.696927246094" />
                  <Point X="-3.307278564453" Y="3.749276611328" />
                  <Point X="-3.260201171875" Y="3.785370605469" />
                  <Point X="-2.752876220703" Y="4.174331542969" />
                  <Point X="-2.665890136719" Y="4.222659179687" />
                  <Point X="-2.141548583984" Y="4.513972167969" />
                  <Point X="-1.995482421875" Y="4.323614746094" />
                  <Point X="-1.967826660156" Y="4.287573242188" />
                  <Point X="-1.951246582031" Y="4.273660644531" />
                  <Point X="-1.938078857422" Y="4.266805664062" />
                  <Point X="-1.856030639648" Y="4.22409375" />
                  <Point X="-1.835124023438" Y="4.2184921875" />
                  <Point X="-1.813808837891" Y="4.222250488281" />
                  <Point X="-1.800093505859" Y="4.227931640625" />
                  <Point X="-1.714634887695" Y="4.263330078125" />
                  <Point X="-1.696905273438" Y="4.275744628906" />
                  <Point X="-1.686083496094" Y="4.294488769531" />
                  <Point X="-1.681619262695" Y="4.308646972656" />
                  <Point X="-1.653804077148" Y="4.396865234375" />
                  <Point X="-1.651917602539" Y="4.418426757812" />
                  <Point X="-1.688864990234" Y="4.6990703125" />
                  <Point X="-1.689137695313" Y="4.701141113281" />
                  <Point X="-1.624857666016" Y="4.719163085938" />
                  <Point X="-0.968094238281" Y="4.903296386719" />
                  <Point X="-0.862643249512" Y="4.915637695312" />
                  <Point X="-0.22419960022" Y="4.990358398438" />
                  <Point X="-0.068865089417" Y="4.410642089844" />
                  <Point X="-0.042140380859" Y="4.310903320312" />
                  <Point X="-0.024282142639" Y="4.284176757813" />
                  <Point X="0.006155934334" Y="4.273844238281" />
                  <Point X="0.036594024658" Y="4.284176757813" />
                  <Point X="0.054452251434" Y="4.310903320312" />
                  <Point X="0.220971984863" Y="4.932363769531" />
                  <Point X="0.236648376465" Y="4.990868652344" />
                  <Point X="0.286755584717" Y="4.98562109375" />
                  <Point X="0.860205871582" Y="4.925565429688" />
                  <Point X="0.947450073242" Y="4.904502441406" />
                  <Point X="1.508455932617" Y="4.769058105469" />
                  <Point X="1.56493762207" Y="4.748571777344" />
                  <Point X="1.931033691406" Y="4.615786132813" />
                  <Point X="1.985946166992" Y="4.59010546875" />
                  <Point X="2.33869921875" Y="4.425134277344" />
                  <Point X="2.391753662109" Y="4.394224609375" />
                  <Point X="2.732523681641" Y="4.19569140625" />
                  <Point X="2.782555175781" Y="4.160111816406" />
                  <Point X="3.068740234375" Y="3.956592773438" />
                  <Point X="2.349012451172" Y="2.709987548828" />
                  <Point X="2.229853515625" Y="2.503597900391" />
                  <Point X="2.2218828125" Y="2.480411376953" />
                  <Point X="2.203382324219" Y="2.411228271484" />
                  <Point X="2.203202392578" Y="2.382724853516" />
                  <Point X="2.210416015625" Y="2.322901123047" />
                  <Point X="2.218682128906" Y="2.3008125" />
                  <Point X="2.224623046875" Y="2.292057128906" />
                  <Point X="2.261639892578" Y="2.23750390625" />
                  <Point X="2.283695068359" Y="2.218262695312" />
                  <Point X="2.338248535156" Y="2.18124609375" />
                  <Point X="2.360336914062" Y="2.172980224609" />
                  <Point X="2.369937988281" Y="2.171822509766" />
                  <Point X="2.42976171875" Y="2.164608642578" />
                  <Point X="2.459767578125" Y="2.168915527344" />
                  <Point X="2.528950683594" Y="2.187416015625" />
                  <Point X="2.541033935547" Y="2.192417480469" />
                  <Point X="3.827027587891" Y="2.934886230469" />
                  <Point X="3.994248046875" Y="3.031430908203" />
                  <Point X="4.000458740234" Y="3.022799560547" />
                  <Point X="4.202591796875" Y="2.741880371094" />
                  <Point X="4.230483886719" Y="2.695788085938" />
                  <Point X="4.387512695312" Y="2.436296142578" />
                  <Point X="3.445500244141" Y="1.713464111328" />
                  <Point X="3.288616210938" Y="1.593082763672" />
                  <Point X="3.271380371094" Y="1.573408325195" />
                  <Point X="3.221589111328" Y="1.508451904297" />
                  <Point X="3.210143066406" Y="1.480856323242" />
                  <Point X="3.191595703125" Y="1.414535644531" />
                  <Point X="3.190779541016" Y="1.390965576172" />
                  <Point X="3.193223144531" Y="1.379122802734" />
                  <Point X="3.208448486328" Y="1.305332763672" />
                  <Point X="3.222292480469" Y="1.277853027344" />
                  <Point X="3.263704101562" Y="1.214909667969" />
                  <Point X="3.280947998047" Y="1.198819824219" />
                  <Point X="3.290579101562" Y="1.193398193359" />
                  <Point X="3.350590087891" Y="1.159617431641" />
                  <Point X="3.381587402344" Y="1.151898681641" />
                  <Point X="3.462726074219" Y="1.141175048828" />
                  <Point X="3.475803710938" Y="1.14117175293" />
                  <Point X="4.697411621094" Y="1.301999511719" />
                  <Point X="4.848975585938" Y="1.32195324707" />
                  <Point X="4.852373535156" Y="1.307994628906" />
                  <Point X="4.939188476562" Y="0.951386047363" />
                  <Point X="4.947978027344" Y="0.894931030273" />
                  <Point X="4.997858398438" Y="0.574556152344" />
                  <Point X="3.920960205078" Y="0.286002166748" />
                  <Point X="3.741167724609" Y="0.237826919556" />
                  <Point X="3.716293212891" Y="0.225424423218" />
                  <Point X="3.636576904297" Y="0.179346878052" />
                  <Point X="3.614588623047" Y="0.157145202637" />
                  <Point X="3.566759033203" Y="0.096198890686" />
                  <Point X="3.556985107422" Y="0.07473513031" />
                  <Point X="3.554426269531" Y="0.061374446869" />
                  <Point X="3.538482910156" Y="-0.021875303268" />
                  <Point X="3.541041748047" Y="-0.054045467377" />
                  <Point X="3.556985107422" Y="-0.137295227051" />
                  <Point X="3.566759033203" Y="-0.158758987427" />
                  <Point X="3.574435058594" Y="-0.168540359497" />
                  <Point X="3.622264892578" Y="-0.229486679077" />
                  <Point X="3.649370605469" Y="-0.249301956177" />
                  <Point X="3.729086914062" Y="-0.295379486084" />
                  <Point X="3.741167724609" Y="-0.300387023926" />
                  <Point X="4.8614375" Y="-0.60056237793" />
                  <Point X="4.998067871094" Y="-0.637172363281" />
                  <Point X="4.996905273438" Y="-0.644884216309" />
                  <Point X="4.948431640625" Y="-0.966399230957" />
                  <Point X="4.937170898438" Y="-1.015746765137" />
                  <Point X="4.874545898438" Y="-1.290178344727" />
                  <Point X="3.620490966797" Y="-1.125078857422" />
                  <Point X="3.411982177734" Y="-1.097628295898" />
                  <Point X="3.394836181641" Y="-1.098341186523" />
                  <Point X="3.369726806641" Y="-1.103798828125" />
                  <Point X="3.213271972656" Y="-1.137804931641" />
                  <Point X="3.1854453125" Y="-1.154697265625" />
                  <Point X="3.170268066406" Y="-1.172950805664" />
                  <Point X="3.075701171875" Y="-1.286685424805" />
                  <Point X="3.064357910156" Y="-1.314070556641" />
                  <Point X="3.062182617188" Y="-1.337709472656" />
                  <Point X="3.04862890625" Y="-1.485000854492" />
                  <Point X="3.056360595703" Y="-1.516621826172" />
                  <Point X="3.070256591797" Y="-1.538236083984" />
                  <Point X="3.156840820312" Y="-1.672912353516" />
                  <Point X="3.168460693359" Y="-1.685540649414" />
                  <Point X="4.208078125" Y="-2.483267333984" />
                  <Point X="4.33907421875" Y="-2.583783935547" />
                  <Point X="4.204133300781" Y="-2.802139404297" />
                  <Point X="4.180842285156" Y="-2.835232421875" />
                  <Point X="4.056688476562" Y="-3.011638427734" />
                  <Point X="2.939159667969" Y="-2.366432617188" />
                  <Point X="2.753454589844" Y="-2.259215576172" />
                  <Point X="2.737340820313" Y="-2.253312744141" />
                  <Point X="2.707456542969" Y="-2.247915771484" />
                  <Point X="2.521250488281" Y="-2.214287109375" />
                  <Point X="2.489077636719" Y="-2.219244873047" />
                  <Point X="2.464250976562" Y="-2.232310791016" />
                  <Point X="2.309559570312" Y="-2.313723876953" />
                  <Point X="2.288599853516" Y="-2.33468359375" />
                  <Point X="2.275533935547" Y="-2.359510009766" />
                  <Point X="2.194120849609" Y="-2.514201660156" />
                  <Point X="2.189163085938" Y="-2.546374511719" />
                  <Point X="2.194560058594" Y="-2.576258789062" />
                  <Point X="2.228188720703" Y="-2.76246484375" />
                  <Point X="2.234091552734" Y="-2.778578613281" />
                  <Point X="2.902150390625" Y="-3.935689941406" />
                  <Point X="2.986673828125" Y="-4.082088867188" />
                  <Point X="2.835298339844" Y="-4.190212402344" />
                  <Point X="2.809261474609" Y="-4.207065917969" />
                  <Point X="2.679775878906" Y="-4.290879394531" />
                  <Point X="1.824887817383" Y="-3.176767578125" />
                  <Point X="1.683177490234" Y="-2.992087402344" />
                  <Point X="1.670549316406" Y="-2.980467529297" />
                  <Point X="1.641075317383" Y="-2.961518310547" />
                  <Point X="1.45742590332" Y="-2.843448730469" />
                  <Point X="1.425804931641" Y="-2.835717041016" />
                  <Point X="1.393569946289" Y="-2.838683349609" />
                  <Point X="1.192718017578" Y="-2.857166015625" />
                  <Point X="1.165332885742" Y="-2.868509277344" />
                  <Point X="1.140442016602" Y="-2.889205078125" />
                  <Point X="0.985349060059" Y="-3.018159667969" />
                  <Point X="0.968456726074" Y="-3.045986328125" />
                  <Point X="0.961014587402" Y="-3.0802265625" />
                  <Point X="0.91464251709" Y="-3.29357421875" />
                  <Point X="0.91392956543" Y="-3.310720214844" />
                  <Point X="1.103252197266" Y="-4.748768554688" />
                  <Point X="1.127642211914" Y="-4.934028320312" />
                  <Point X="0.994346374512" Y="-4.963246582031" />
                  <Point X="0.970288635254" Y="-4.9676171875" />
                  <Point X="0.860200378418" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#130" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.026724810296" Y="4.45486282851" Z="0.35" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.35" />
                  <Point X="-0.874611309453" Y="4.996579006201" Z="0.35" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.35" />
                  <Point X="-1.64451149723" Y="4.79858653125" Z="0.35" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.35" />
                  <Point X="-1.744593273738" Y="4.723824062682" Z="0.35" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.35" />
                  <Point X="-1.735461077846" Y="4.35496228114" Z="0.35" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.35" />
                  <Point X="-1.825383767052" Y="4.305405682645" Z="0.35" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.35" />
                  <Point X="-1.92114726002" Y="4.342436477748" Z="0.35" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.35" />
                  <Point X="-1.961970738725" Y="4.385332735032" Z="0.35" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.35" />
                  <Point X="-2.696329089521" Y="4.297646561323" Z="0.35" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.35" />
                  <Point X="-3.296779332648" Y="3.85633054839" Z="0.35" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.35" />
                  <Point X="-3.326511948198" Y="3.703207326067" Z="0.35" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.35" />
                  <Point X="-2.995075107491" Y="3.066594627268" Z="0.35" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.35" />
                  <Point X="-3.046365776264" Y="3.002437791622" Z="0.35" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.35" />
                  <Point X="-3.128481838338" Y="3.000489591682" Z="0.35" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.35" />
                  <Point X="-3.230652022231" Y="3.053682000547" Z="0.35" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.35" />
                  <Point X="-4.150403369361" Y="2.919979938693" Z="0.35" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.35" />
                  <Point X="-4.500636703905" Y="2.344451952853" Z="0.35" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.35" />
                  <Point X="-4.429952166585" Y="2.173583903414" Z="0.35" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.35" />
                  <Point X="-3.670935315214" Y="1.561605154344" Z="0.35" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.35" />
                  <Point X="-3.68806127846" Y="1.502429270054" Z="0.35" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.35" />
                  <Point X="-3.744401122251" Y="1.477510979042" Z="0.35" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.35" />
                  <Point X="-3.899986903585" Y="1.494197416192" Z="0.35" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.35" />
                  <Point X="-4.951210397175" Y="1.117720352367" Z="0.35" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.35" />
                  <Point X="-5.048226730641" Y="0.528415849207" Z="0.35" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.35" />
                  <Point X="-4.855129178227" Y="0.391660418985" Z="0.35" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.35" />
                  <Point X="-3.552645951152" Y="0.032470805903" Z="0.35" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.35" />
                  <Point X="-3.540836127606" Y="0.004122160573" Z="0.35" />
                  <Point X="-3.539556741714" Y="0" Z="0.35" />
                  <Point X="-3.547528473529" Y="-0.025684790488" Z="0.35" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.35" />
                  <Point X="-3.572722643971" Y="-0.046405177231" Z="0.35" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.35" />
                  <Point X="-3.781758610545" Y="-0.104051637771" Z="0.35" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.35" />
                  <Point X="-4.993402472326" Y="-0.91457263431" Z="0.35" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.35" />
                  <Point X="-4.865678069745" Y="-1.44765745822" Z="0.35" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.35" />
                  <Point X="-4.621793955354" Y="-1.49152368237" Z="0.35" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.35" />
                  <Point X="-3.196338721814" Y="-1.320294264643" Z="0.35" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.35" />
                  <Point X="-3.199315306688" Y="-1.348083249892" Z="0.35" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.35" />
                  <Point X="-3.380513139315" Y="-1.490417568134" Z="0.35" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.35" />
                  <Point X="-4.249950864735" Y="-2.775813816832" Z="0.35" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.35" />
                  <Point X="-3.910243386673" Y="-3.236858009084" Z="0.35" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.35" />
                  <Point X="-3.683921037946" Y="-3.196974187582" Z="0.35" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.35" />
                  <Point X="-2.557890190096" Y="-2.570440279036" Z="0.35" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.35" />
                  <Point X="-2.658442825586" Y="-2.751157344546" Z="0.35" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.35" />
                  <Point X="-2.947100586424" Y="-4.133902733659" Z="0.35" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.35" />
                  <Point X="-2.511878857058" Y="-4.411919802538" Z="0.35" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.35" />
                  <Point X="-2.420015771506" Y="-4.409008690214" Z="0.35" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.35" />
                  <Point X="-2.003931515021" Y="-4.007922022062" Z="0.35" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.35" />
                  <Point X="-1.70148156626" Y="-4.001569706729" Z="0.35" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.35" />
                  <Point X="-1.457664855938" Y="-4.180650191783" Z="0.35" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.35" />
                  <Point X="-1.373249194929" Y="-4.47115012252" Z="0.35" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.35" />
                  <Point X="-1.371547204816" Y="-4.563885783409" Z="0.35" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.35" />
                  <Point X="-1.15829536637" Y="-4.945062023096" Z="0.35" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.35" />
                  <Point X="-0.859119843613" Y="-5.005717017592" Z="0.35" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.35" />
                  <Point X="-0.762269578185" Y="-4.807012911453" Z="0.35" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.35" />
                  <Point X="-0.276001847191" Y="-3.315496588454" Z="0.35" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.35" />
                  <Point X="-0.035038364228" Y="-3.215113983659" Z="0.35" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.35" />
                  <Point X="0.218320715133" Y="-3.271998061629" Z="0.35" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.35" />
                  <Point X="0.394444012241" Y="-3.486149167314" Z="0.35" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.35" />
                  <Point X="0.472485282216" Y="-3.725523118363" Z="0.35" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.35" />
                  <Point X="0.973069700759" Y="-4.98553214805" Z="0.35" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.35" />
                  <Point X="1.152292198727" Y="-4.947182842649" Z="0.35" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.35" />
                  <Point X="1.146668507719" Y="-4.710962382299" Z="0.35" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.35" />
                  <Point X="1.003717761855" Y="-3.059566220629" Z="0.35" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.35" />
                  <Point X="1.166252850527" Y="-2.896371407481" Z="0.35" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.35" />
                  <Point X="1.391995665737" Y="-2.857192863956" Z="0.35" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.35" />
                  <Point X="1.607879931681" Y="-2.972296233301" Z="0.35" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.35" />
                  <Point X="1.779064068766" Y="-3.175925523405" Z="0.35" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.35" />
                  <Point X="2.830274619233" Y="-4.217759910361" Z="0.35" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.35" />
                  <Point X="3.020341740237" Y="-4.083808213405" Z="0.35" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.35" />
                  <Point X="2.939295634563" Y="-3.879409814613" Z="0.35" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.35" />
                  <Point X="2.237608194546" Y="-2.536092468884" Z="0.35" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.35" />
                  <Point X="2.313624185834" Y="-2.351516686487" Z="0.35" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.35" />
                  <Point X="2.481381691885" Y="-2.245277124133" Z="0.35" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.35" />
                  <Point X="2.692414334628" Y="-2.265839814944" Z="0.35" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.35" />
                  <Point X="2.908003782494" Y="-2.378453865711" Z="0.35" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.35" />
                  <Point X="4.215574890049" Y="-2.832729789052" Z="0.35" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.35" />
                  <Point X="4.377152344402" Y="-2.57603711776" Z="0.35" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.35" />
                  <Point X="4.232360033794" Y="-2.412319477663" Z="0.35" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.35" />
                  <Point X="3.106158459972" Y="-1.479916740266" Z="0.35" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.35" />
                  <Point X="3.105816217409" Y="-1.311011080133" Z="0.35" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.35" />
                  <Point X="3.202558442043" Y="-1.173637506752" Z="0.35" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.35" />
                  <Point X="3.374190370424" Y="-1.121378075896" Z="0.35" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.35" />
                  <Point X="3.607808601679" Y="-1.143371119" Z="0.35" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.35" />
                  <Point X="4.979761890586" Y="-0.995590790162" Z="0.35" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.35" />
                  <Point X="5.040190823224" Y="-0.621058187234" Z="0.35" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.35" />
                  <Point X="4.868222624575" Y="-0.52098604478" Z="0.35" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.35" />
                  <Point X="3.668236408749" Y="-0.174733191851" Z="0.35" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.35" />
                  <Point X="3.607612940144" Y="-0.106391834823" Z="0.35" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.35" />
                  <Point X="3.583993465527" Y="-0.013360780272" Z="0.35" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.35" />
                  <Point X="3.597377984898" Y="0.083249750967" Z="0.35" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.35" />
                  <Point X="3.647766498257" Y="0.157556903752" Z="0.35" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.35" />
                  <Point X="3.735159005603" Y="0.213415652391" Z="0.35" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.35" />
                  <Point X="3.927745192732" Y="0.268985887989" Z="0.35" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.35" />
                  <Point X="4.99122685215" Y="0.933903055015" Z="0.35" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.35" />
                  <Point X="4.894798568825" Y="1.35110153744" Z="0.35" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.35" />
                  <Point X="4.684729221671" Y="1.38285185157" Z="0.35" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.35" />
                  <Point X="3.381982612291" Y="1.232747564241" Z="0.35" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.35" />
                  <Point X="3.308949721004" Y="1.268249517055" Z="0.35" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.35" />
                  <Point X="3.257907128937" Y="1.336614573825" Z="0.35" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.35" />
                  <Point X="3.236035567458" Y="1.420506593018" Z="0.35" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.35" />
                  <Point X="3.252139324885" Y="1.498669876022" Z="0.35" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.35" />
                  <Point X="3.304907157347" Y="1.57427020939" Z="0.35" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.35" />
                  <Point X="3.469782219302" Y="1.705076470074" Z="0.35" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.35" />
                  <Point X="4.267105376629" Y="2.752954474234" Z="0.35" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.35" />
                  <Point X="4.034888172792" Y="3.083282441325" Z="0.35" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.35" />
                  <Point X="3.795871607025" Y="3.009467526794" Z="0.35" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.35" />
                  <Point X="2.440694866961" Y="2.248498318137" Z="0.35" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.35" />
                  <Point X="2.369767919447" Y="2.252742809346" Z="0.35" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.35" />
                  <Point X="2.305613368714" Y="2.290917246761" Z="0.35" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.35" />
                  <Point X="2.259841353474" Y="2.351411491669" Z="0.35" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.35" />
                  <Point X="2.246686893413" Y="2.419990524386" Z="0.35" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.35" />
                  <Point X="2.264029624521" Y="2.498774708921" Z="0.35" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.35" />
                  <Point X="2.386157826066" Y="2.71626747493" Z="0.35" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.35" />
                  <Point X="2.805376373065" Y="4.232138854149" Z="0.35" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.35" />
                  <Point X="2.410767608807" Y="4.468707427189" Z="0.35" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.35" />
                  <Point X="2.000971758119" Y="4.666677269479" Z="0.35" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.35" />
                  <Point X="1.57583027541" Y="4.826855655796" Z="0.35" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.35" />
                  <Point X="0.953028517914" Y="4.984385758182" Z="0.35" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.35" />
                  <Point X="0.285807541039" Y="5.066629216282" Z="0.35" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.35" />
                  <Point X="0.166519751257" Y="4.976584677441" Z="0.35" />
                  <Point X="0" Y="4.355124473572" Z="0.35" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>