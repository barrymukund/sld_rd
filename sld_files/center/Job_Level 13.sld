<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#141" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1074" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="0" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000475830078" Y="0.004715820312" />
                  <Width Value="9.783896972656" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.658127807617" Y="-3.866418945312" />
                  <Point X="0.563302062988" Y="-3.512524414063" />
                  <Point X="0.557721191406" Y="-3.497141845703" />
                  <Point X="0.54236315918" Y="-3.467377197266" />
                  <Point X="0.503605621338" Y="-3.411534667969" />
                  <Point X="0.378635528564" Y="-3.231476806641" />
                  <Point X="0.356751586914" Y="-3.209021484375" />
                  <Point X="0.330496398926" Y="-3.189777587891" />
                  <Point X="0.302495483398" Y="-3.175669433594" />
                  <Point X="0.242520202637" Y="-3.157055175781" />
                  <Point X="0.049136341095" Y="-3.097036132812" />
                  <Point X="0.020976638794" Y="-3.092766601562" />
                  <Point X="-0.00866468811" Y="-3.092766601562" />
                  <Point X="-0.036824085236" Y="-3.097035888672" />
                  <Point X="-0.096799362183" Y="-3.115649902344" />
                  <Point X="-0.290183227539" Y="-3.175669189453" />
                  <Point X="-0.31818460083" Y="-3.189777587891" />
                  <Point X="-0.344439788818" Y="-3.209021484375" />
                  <Point X="-0.366323425293" Y="-3.2314765625" />
                  <Point X="-0.405081115723" Y="-3.287318847656" />
                  <Point X="-0.530051086426" Y="-3.467376953125" />
                  <Point X="-0.538189025879" Y="-3.481573730469" />
                  <Point X="-0.550990112305" Y="-3.512524414063" />
                  <Point X="-0.856745483398" Y="-4.653619628906" />
                  <Point X="-0.916584472656" Y="-4.87694140625" />
                  <Point X="-1.079345214844" Y="-4.845349121094" />
                  <Point X="-1.145112792969" Y="-4.828427734375" />
                  <Point X="-1.24641796875" Y="-4.802362792969" />
                  <Point X="-1.226377685547" Y="-4.650142578125" />
                  <Point X="-1.214963012695" Y="-4.563438476562" />
                  <Point X="-1.214201171875" Y="-4.5479296875" />
                  <Point X="-1.216508666992" Y="-4.516224121094" />
                  <Point X="-1.230836791992" Y="-4.444191894531" />
                  <Point X="-1.277036132812" Y="-4.211932617188" />
                  <Point X="-1.287938354492" Y="-4.182966308594" />
                  <Point X="-1.304010375977" Y="-4.15512890625" />
                  <Point X="-1.32364453125" Y="-4.131204101562" />
                  <Point X="-1.378862304688" Y="-4.082779541016" />
                  <Point X="-1.556905517578" Y="-3.926639404297" />
                  <Point X="-1.583188476562" Y="-3.910295410156" />
                  <Point X="-1.612885742188" Y="-3.897994384766" />
                  <Point X="-1.64302734375" Y="-3.890966308594" />
                  <Point X="-1.716313476562" Y="-3.886162841797" />
                  <Point X="-1.952616577148" Y="-3.870674804688" />
                  <Point X="-1.98341796875" Y="-3.873708496094" />
                  <Point X="-2.014466674805" Y="-3.882028076172" />
                  <Point X="-2.042657836914" Y="-3.894801513672" />
                  <Point X="-2.103723632813" Y="-3.935604492188" />
                  <Point X="-2.300624267578" Y="-4.067169677734" />
                  <Point X="-2.312785644531" Y="-4.076821533203" />
                  <Point X="-2.335102783203" Y="-4.099461914062" />
                  <Point X="-2.480149169922" Y="-4.288490234375" />
                  <Point X="-2.801704589844" Y="-4.089390869141" />
                  <Point X="-2.892783447266" Y="-4.019263183594" />
                  <Point X="-3.104721923828" Y="-3.856077636719" />
                  <Point X="-2.599984375" Y="-2.981846679688" />
                  <Point X="-2.423761230469" Y="-2.676619628906" />
                  <Point X="-2.412859130859" Y="-2.647653076172" />
                  <Point X="-2.406588134766" Y="-2.616126464844" />
                  <Point X="-2.405575439453" Y="-2.585192382812" />
                  <Point X="-2.414560302734" Y="-2.555574707031" />
                  <Point X="-2.428777587891" Y="-2.526745605469" />
                  <Point X="-2.446804931641" Y="-2.501588867188" />
                  <Point X="-2.464153564453" Y="-2.484240234375" />
                  <Point X="-2.489311767578" Y="-2.466212158203" />
                  <Point X="-2.518140625" Y="-2.451995605469" />
                  <Point X="-2.5477578125" Y="-2.443011474609" />
                  <Point X="-2.578691162109" Y="-2.444024169922" />
                  <Point X="-2.610217285156" Y="-2.450295166016" />
                  <Point X="-2.63918359375" Y="-2.461197265625" />
                  <Point X="-3.623355712891" Y="-3.029409423828" />
                  <Point X="-3.818024414062" Y="-3.141801269531" />
                  <Point X="-3.822873291016" Y="-3.135430908203" />
                  <Point X="-4.082859619141" Y="-2.793862060547" />
                  <Point X="-4.148150878906" Y="-2.684378173828" />
                  <Point X="-4.306142578125" Y="-2.419450439453" />
                  <Point X="-3.414287597656" Y="-1.735105712891" />
                  <Point X="-3.105954589844" Y="-1.498513549805" />
                  <Point X="-3.083062744141" Y="-1.475877441406" />
                  <Point X="-3.063913085938" Y="-1.445302246094" />
                  <Point X="-3.055375732422" Y="-1.423192871094" />
                  <Point X="-3.052032958984" Y="-1.412791503906" />
                  <Point X="-3.046152099609" Y="-1.3900859375" />
                  <Point X="-3.042037597656" Y="-1.358602783203" />
                  <Point X="-3.042736816406" Y="-1.335734619141" />
                  <Point X="-3.048883056641" Y="-1.313696655273" />
                  <Point X="-3.060887939453" Y="-1.284713989258" />
                  <Point X="-3.073566650391" Y="-1.262875" />
                  <Point X="-3.091551025391" Y="-1.245147949219" />
                  <Point X="-3.110322265625" Y="-1.231028686523" />
                  <Point X="-3.1192421875" Y="-1.225076782227" />
                  <Point X="-3.139455810547" Y="-1.213180053711" />
                  <Point X="-3.168718261719" Y="-1.201956420898" />
                  <Point X="-3.200605957031" Y="-1.195474853516" />
                  <Point X="-3.231929199219" Y="-1.194383789062" />
                  <Point X="-4.474353027344" Y="-1.357952026367" />
                  <Point X="-4.732102539062" Y="-1.391885375977" />
                  <Point X="-4.732395019531" Y="-1.390740234375" />
                  <Point X="-4.834077148438" Y="-0.992657836914" />
                  <Point X="-4.8513515625" Y="-0.871875793457" />
                  <Point X="-4.892424316406" Y="-0.584698181152" />
                  <Point X="-3.884113769531" Y="-0.314522338867" />
                  <Point X="-3.532875976562" Y="-0.220408340454" />
                  <Point X="-3.512836914062" Y="-0.212511001587" />
                  <Point X="-3.49069921875" Y="-0.200731002808" />
                  <Point X="-3.481159179688" Y="-0.194909835815" />
                  <Point X="-3.459975830078" Y="-0.180207565308" />
                  <Point X="-3.436020507812" Y="-0.158680801392" />
                  <Point X="-3.415407714844" Y="-0.12899256897" />
                  <Point X="-3.405792480469" Y="-0.107198959351" />
                  <Point X="-3.401978271484" Y="-0.09701109314" />
                  <Point X="-3.394917236328" Y="-0.074260139465" />
                  <Point X="-3.389474365234" Y="-0.045520904541" />
                  <Point X="-3.390296630859" Y="-0.01239659214" />
                  <Point X="-3.394733642578" Y="0.00948004818" />
                  <Point X="-3.397107177734" Y="0.018755960464" />
                  <Point X="-3.404168212891" Y="0.041506916046" />
                  <Point X="-3.417484130859" Y="0.07083039093" />
                  <Point X="-3.439793701172" Y="0.099603935242" />
                  <Point X="-3.458182128906" Y="0.115681419373" />
                  <Point X="-3.466545166016" Y="0.122206741333" />
                  <Point X="-3.487728271484" Y="0.136909011841" />
                  <Point X="-3.501924560547" Y="0.145046646118" />
                  <Point X="-3.532875976562" Y="0.157848342896" />
                  <Point X="-4.665404296875" Y="0.461308349609" />
                  <Point X="-4.89181640625" Y="0.521975219727" />
                  <Point X="-4.890020996094" Y="0.534108703613" />
                  <Point X="-4.824487792969" Y="0.976975097656" />
                  <Point X="-4.789714355469" Y="1.105299682617" />
                  <Point X="-4.703551269531" Y="1.423267822266" />
                  <Point X="-4.017442138672" Y="1.332939819336" />
                  <Point X="-3.765666259766" Y="1.29979284668" />
                  <Point X="-3.744986083984" Y="1.299341674805" />
                  <Point X="-3.723424560547" Y="1.301228149414" />
                  <Point X="-3.703137695312" Y="1.305263671875" />
                  <Point X="-3.688596923828" Y="1.309848266602" />
                  <Point X="-3.641711669922" Y="1.324631225586" />
                  <Point X="-3.622779052734" Y="1.332961669922" />
                  <Point X="-3.604034667969" Y="1.343783569336" />
                  <Point X="-3.587353271484" Y="1.356014892578" />
                  <Point X="-3.57371484375" Y="1.37156652832" />
                  <Point X="-3.561300537109" Y="1.389296264648" />
                  <Point X="-3.551351318359" Y="1.407431518555" />
                  <Point X="-3.545516845703" Y="1.421517333984" />
                  <Point X="-3.526703857422" Y="1.466935791016" />
                  <Point X="-3.520916015625" Y="1.48679296875" />
                  <Point X="-3.517157470703" Y="1.508108154297" />
                  <Point X="-3.515804443359" Y="1.528748779297" />
                  <Point X="-3.518951171875" Y="1.549192871094" />
                  <Point X="-3.524552978516" Y="1.570099121094" />
                  <Point X="-3.532049804688" Y="1.589377319336" />
                  <Point X="-3.53908984375" Y="1.602901245117" />
                  <Point X="-3.561789550781" Y="1.646506958008" />
                  <Point X="-3.573280761719" Y="1.663705322266" />
                  <Point X="-3.587193115234" Y="1.680285766602" />
                  <Point X="-3.602135742188" Y="1.694590209961" />
                  <Point X="-4.251756835938" Y="2.193062011719" />
                  <Point X="-4.351859863281" Y="2.269874023438" />
                  <Point X="-4.335789550781" Y="2.297406738281" />
                  <Point X="-4.081153808594" Y="2.733659423828" />
                  <Point X="-3.989042724609" Y="2.852055175781" />
                  <Point X="-3.750504882813" Y="3.158661865234" />
                  <Point X="-3.361290771484" Y="2.933948730469" />
                  <Point X="-3.206656982422" Y="2.844670898438" />
                  <Point X="-3.187724609375" Y="2.836340332031" />
                  <Point X="-3.167082519531" Y="2.829831787109" />
                  <Point X="-3.146794677734" Y="2.825796386719" />
                  <Point X="-3.126543457031" Y="2.824024658203" />
                  <Point X="-3.061245361328" Y="2.818311767578" />
                  <Point X="-3.040565185547" Y="2.818762939453" />
                  <Point X="-3.019106445312" Y="2.821588134766" />
                  <Point X="-2.999015136719" Y="2.826504394531" />
                  <Point X="-2.980463867188" Y="2.835652832031" />
                  <Point X="-2.962209472656" Y="2.847281982422" />
                  <Point X="-2.946077392578" Y="2.860229492188" />
                  <Point X="-2.931702880859" Y="2.874604003906" />
                  <Point X="-2.885353759766" Y="2.920952880859" />
                  <Point X="-2.872406738281" Y="2.937084228516" />
                  <Point X="-2.86077734375" Y="2.955338623047" />
                  <Point X="-2.851628662109" Y="2.973890136719" />
                  <Point X="-2.846712158203" Y="2.993981933594" />
                  <Point X="-2.843886962891" Y="3.015440917969" />
                  <Point X="-2.843435791016" Y="3.036120849609" />
                  <Point X="-2.845207519531" Y="3.056372314453" />
                  <Point X="-2.850920410156" Y="3.121670410156" />
                  <Point X="-2.854955810547" Y="3.141958496094" />
                  <Point X="-2.861464355469" Y="3.162600585938" />
                  <Point X="-2.869794921875" Y="3.181532958984" />
                  <Point X="-3.157661621094" Y="3.680132568359" />
                  <Point X="-3.183333007812" Y="3.724596435547" />
                  <Point X="-3.144117675781" Y="3.754662597656" />
                  <Point X="-2.700625488281" Y="4.09468359375" />
                  <Point X="-2.555555664062" Y="4.175280761719" />
                  <Point X="-2.167036376953" Y="4.391134277344" />
                  <Point X="-2.090533691406" Y="4.29143359375" />
                  <Point X="-2.04319543457" Y="4.229741210938" />
                  <Point X="-2.028892456055" Y="4.214799804688" />
                  <Point X="-2.01231237793" Y="4.200887207031" />
                  <Point X="-1.995113037109" Y="4.18939453125" />
                  <Point X="-1.972573486328" Y="4.177661132812" />
                  <Point X="-1.899896972656" Y="4.139827636719" />
                  <Point X="-1.8806171875" Y="4.132330566406" />
                  <Point X="-1.859710571289" Y="4.126729003906" />
                  <Point X="-1.839267578125" Y="4.123582519531" />
                  <Point X="-1.818628295898" Y="4.124935546875" />
                  <Point X="-1.797312988281" Y="4.128693847656" />
                  <Point X="-1.77745300293" Y="4.134482421875" />
                  <Point X="-1.75397644043" Y="4.14420703125" />
                  <Point X="-1.678279052734" Y="4.175562011719" />
                  <Point X="-1.660146118164" Y="4.185509765625" />
                  <Point X="-1.642416381836" Y="4.197923828125" />
                  <Point X="-1.626864135742" Y="4.211562988281" />
                  <Point X="-1.614632568359" Y="4.228244628906" />
                  <Point X="-1.603810791016" Y="4.246988769531" />
                  <Point X="-1.595480224609" Y="4.265921386719" />
                  <Point X="-1.587839111328" Y="4.29015625" />
                  <Point X="-1.563200805664" Y="4.368297851562" />
                  <Point X="-1.559165649414" Y="4.388584960938" />
                  <Point X="-1.557279174805" Y="4.410146972656" />
                  <Point X="-1.55773034668" Y="4.430826660156" />
                  <Point X="-1.584201904297" Y="4.631897949219" />
                  <Point X="-1.523763427734" Y="4.648842773438" />
                  <Point X="-0.94963494873" Y="4.80980859375" />
                  <Point X="-0.773773498535" Y="4.830390136719" />
                  <Point X="-0.294710784912" Y="4.886457519531" />
                  <Point X="-0.179648239136" Y="4.457038085938" />
                  <Point X="-0.133903305054" Y="4.286315429688" />
                  <Point X="-0.121129844666" Y="4.258124023438" />
                  <Point X="-0.10327155304" Y="4.231397460938" />
                  <Point X="-0.0821145401" Y="4.20880859375" />
                  <Point X="-0.054819446564" Y="4.19421875" />
                  <Point X="-0.024381370544" Y="4.183886230469" />
                  <Point X="0.006155906677" Y="4.178844238281" />
                  <Point X="0.03669316864" Y="4.183886230469" />
                  <Point X="0.067131248474" Y="4.19421875" />
                  <Point X="0.094426475525" Y="4.20880859375" />
                  <Point X="0.115583503723" Y="4.231397460938" />
                  <Point X="0.133441802979" Y="4.258124023438" />
                  <Point X="0.146215255737" Y="4.286315429688" />
                  <Point X="0.29371472168" Y="4.836791503906" />
                  <Point X="0.307419311523" Y="4.8879375" />
                  <Point X="0.342738311768" Y="4.884238769531" />
                  <Point X="0.844041442871" Y="4.831738769531" />
                  <Point X="0.989542419434" Y="4.796610351562" />
                  <Point X="1.481025756836" Y="4.677951171875" />
                  <Point X="1.57460168457" Y="4.644010253906" />
                  <Point X="1.894646484375" Y="4.527927734375" />
                  <Point X="1.986226196289" Y="4.485099121094" />
                  <Point X="2.294576416016" Y="4.340893554688" />
                  <Point X="2.383077880859" Y="4.289332519531" />
                  <Point X="2.680978759766" Y="4.115774902344" />
                  <Point X="2.764420898438" Y="4.056435302734" />
                  <Point X="2.943260009766" Y="3.929254638672" />
                  <Point X="2.351545898438" Y="2.904375488281" />
                  <Point X="2.147581054688" Y="2.551097900391" />
                  <Point X="2.139488037109" Y="2.532908447266" />
                  <Point X="2.129404296875" Y="2.501819824219" />
                  <Point X="2.127994628906" Y="2.497051269531" />
                  <Point X="2.111607177734" Y="2.435770263672" />
                  <Point X="2.108398193359" Y="2.409497070313" />
                  <Point X="2.109042236328" Y="2.374160644531" />
                  <Point X="2.109709472656" Y="2.364519042969" />
                  <Point X="2.116099121094" Y="2.311528320312" />
                  <Point X="2.121442138672" Y="2.289604980469" />
                  <Point X="2.129708251953" Y="2.267516357422" />
                  <Point X="2.140071044922" Y="2.247471191406" />
                  <Point X="2.150239990234" Y="2.232484863281" />
                  <Point X="2.183028808594" Y="2.184162597656" />
                  <Point X="2.200979248047" Y="2.164392333984" />
                  <Point X="2.229265625" Y="2.140923095703" />
                  <Point X="2.236585449219" Y="2.135423339844" />
                  <Point X="2.284907714844" Y="2.102634765625" />
                  <Point X="2.304952880859" Y="2.092271972656" />
                  <Point X="2.327041259766" Y="2.084006103516" />
                  <Point X="2.348963867188" Y="2.078663330078" />
                  <Point X="2.365398193359" Y="2.076681640625" />
                  <Point X="2.418388671875" Y="2.070291748047" />
                  <Point X="2.445625244141" Y="2.070942382812" />
                  <Point X="2.483533203125" Y="2.077362548828" />
                  <Point X="2.492211425781" Y="2.079253417969" />
                  <Point X="2.553492431641" Y="2.095640625" />
                  <Point X="2.565283447266" Y="2.099638183594" />
                  <Point X="2.588533935547" Y="2.110145019531" />
                  <Point X="3.727639404297" Y="2.767807861328" />
                  <Point X="3.967326416016" Y="2.906191162109" />
                  <Point X="4.123272949219" Y="2.689460205078" />
                  <Point X="4.169790527344" Y="2.61258984375" />
                  <Point X="4.262198730469" Y="2.459884277344" />
                  <Point X="3.499322753906" Y="1.874508422852" />
                  <Point X="3.230783935547" Y="1.668451171875" />
                  <Point X="3.221425048828" Y="1.660241943359" />
                  <Point X="3.203973632812" Y="1.641627563477" />
                  <Point X="3.190295410156" Y="1.623783081055" />
                  <Point X="3.14619140625" Y="1.56624609375" />
                  <Point X="3.132952880859" Y="1.542636962891" />
                  <Point X="3.119388183594" Y="1.507465942383" />
                  <Point X="3.116534667969" Y="1.498866821289" />
                  <Point X="3.100105957031" Y="1.440121459961" />
                  <Point X="3.096652587891" Y="1.417823242188" />
                  <Point X="3.095836425781" Y="1.394252929688" />
                  <Point X="3.097739501953" Y="1.371768066406" />
                  <Point X="3.101922119141" Y="1.351496948242" />
                  <Point X="3.115408447266" Y="1.286135253906" />
                  <Point X="3.120679931641" Y="1.268977661133" />
                  <Point X="3.136282470703" Y="1.235740478516" />
                  <Point X="3.147658691406" Y="1.218448974609" />
                  <Point X="3.184340087891" Y="1.162695068359" />
                  <Point X="3.198893554688" Y="1.145450439453" />
                  <Point X="3.216137451172" Y="1.129360595703" />
                  <Point X="3.234346923828" Y="1.116034912109" />
                  <Point X="3.250832519531" Y="1.106754882812" />
                  <Point X="3.303989013672" Y="1.076832519531" />
                  <Point X="3.320521240234" Y="1.069501586914" />
                  <Point X="3.356117431641" Y="1.059438720703" />
                  <Point X="3.378407226562" Y="1.056492675781" />
                  <Point X="3.450278076172" Y="1.046994262695" />
                  <Point X="3.462702148438" Y="1.046175048828" />
                  <Point X="3.488203613281" Y="1.04698449707" />
                  <Point X="4.570277832031" Y="1.189442260742" />
                  <Point X="4.776839355469" Y="1.21663659668" />
                  <Point X="4.845936523438" Y="0.932809020996" />
                  <Point X="4.860595214844" Y="0.838655029297" />
                  <Point X="4.890864746094" Y="0.644238464355" />
                  <Point X="4.024331298828" Y="0.412051574707" />
                  <Point X="3.716579833984" Y="0.329589935303" />
                  <Point X="3.704791259766" Y="0.325586608887" />
                  <Point X="3.681545410156" Y="0.315067840576" />
                  <Point X="3.659646484375" Y="0.302409881592" />
                  <Point X="3.589035400391" Y="0.261595275879" />
                  <Point X="3.574311035156" Y="0.251096221924" />
                  <Point X="3.547531005859" Y="0.22557661438" />
                  <Point X="3.534391601562" Y="0.208833984375" />
                  <Point X="3.492025146484" Y="0.154848968506" />
                  <Point X="3.480301269531" Y="0.13556930542" />
                  <Point X="3.47052734375" Y="0.114105690002" />
                  <Point X="3.463680908203" Y="0.092604423523" />
                  <Point X="3.459301025391" Y="0.069734619141" />
                  <Point X="3.445178710938" Y="-0.004006225586" />
                  <Point X="3.443482910156" Y="-0.021875295639" />
                  <Point X="3.445178710938" Y="-0.05855393219" />
                  <Point X="3.44955859375" Y="-0.081423583984" />
                  <Point X="3.463680908203" Y="-0.155164428711" />
                  <Point X="3.470527099609" Y="-0.176665542603" />
                  <Point X="3.480301025391" Y="-0.198129302979" />
                  <Point X="3.492025146484" Y="-0.217409118652" />
                  <Point X="3.505164550781" Y="-0.234151748657" />
                  <Point X="3.547531005859" Y="-0.288136779785" />
                  <Point X="3.559999023438" Y="-0.30123614502" />
                  <Point X="3.589035888672" Y="-0.324155731201" />
                  <Point X="3.610935058594" Y="-0.336813690186" />
                  <Point X="3.681545898438" Y="-0.377627990723" />
                  <Point X="3.692709228516" Y="-0.383138458252" />
                  <Point X="3.716579833984" Y="-0.392149932861" />
                  <Point X="4.708891113281" Y="-0.658038757324" />
                  <Point X="4.89147265625" Y="-0.706961486816" />
                  <Point X="4.855022460938" Y="-0.948724731445" />
                  <Point X="4.836241210938" Y="-1.031027587891" />
                  <Point X="4.801173339844" Y="-1.184698974609" />
                  <Point X="3.781287109375" Y="-1.050428344727" />
                  <Point X="3.424382080078" Y="-1.003441040039" />
                  <Point X="3.408035644531" Y="-1.002710327148" />
                  <Point X="3.374658691406" Y="-1.005508728027" />
                  <Point X="3.331678710938" Y="-1.014850646973" />
                  <Point X="3.193094482422" Y="-1.04497253418" />
                  <Point X="3.163974121094" Y="-1.056596923828" />
                  <Point X="3.136147460938" Y="-1.073489257812" />
                  <Point X="3.112397460938" Y="-1.093960083008" />
                  <Point X="3.086418701172" Y="-1.125204223633" />
                  <Point X="3.002653320312" Y="-1.225948120117" />
                  <Point X="2.987932617188" Y="-1.250330566406" />
                  <Point X="2.976589355469" Y="-1.277715698242" />
                  <Point X="2.969757568359" Y="-1.305365478516" />
                  <Point X="2.966034179688" Y="-1.34582824707" />
                  <Point X="2.954028564453" Y="-1.476295776367" />
                  <Point X="2.956347412109" Y="-1.507564819336" />
                  <Point X="2.964079101562" Y="-1.539185791016" />
                  <Point X="2.976450195312" Y="-1.567996459961" />
                  <Point X="3.000235839844" Y="-1.604993652344" />
                  <Point X="3.076930419922" Y="-1.724286987305" />
                  <Point X="3.086932861328" Y="-1.737238647461" />
                  <Point X="3.110628417969" Y="-1.760909179688" />
                  <Point X="4.031499267578" Y="-2.467518310547" />
                  <Point X="4.213122558594" Y="-2.606883056641" />
                  <Point X="4.124812988281" Y="-2.749781738281" />
                  <Point X="4.085968505859" Y="-2.804974121094" />
                  <Point X="4.028981445312" Y="-2.885945068359" />
                  <Point X="3.118826660156" Y="-2.360466796875" />
                  <Point X="2.800954589844" Y="-2.176943115234" />
                  <Point X="2.786131591797" Y="-2.170012451172" />
                  <Point X="2.754224365234" Y="-2.159824951172" />
                  <Point X="2.703071289062" Y="-2.150586914062" />
                  <Point X="2.538134033203" Y="-2.120799316406" />
                  <Point X="2.506781982422" Y="-2.120395263672" />
                  <Point X="2.474609130859" Y="-2.125353027344" />
                  <Point X="2.444833496094" Y="-2.135176757812" />
                  <Point X="2.402337890625" Y="-2.157541748047" />
                  <Point X="2.265315429688" Y="-2.229655761719" />
                  <Point X="2.242384033203" Y="-2.246549072266" />
                  <Point X="2.221424316406" Y="-2.267509033203" />
                  <Point X="2.204531982422" Y="-2.290439208984" />
                  <Point X="2.182166748047" Y="-2.332934814453" />
                  <Point X="2.110052978516" Y="-2.469957275391" />
                  <Point X="2.100229003906" Y="-2.499733398438" />
                  <Point X="2.095271240234" Y="-2.531906005859" />
                  <Point X="2.095675292969" Y="-2.563258056641" />
                  <Point X="2.104913330078" Y="-2.614411132812" />
                  <Point X="2.134700927734" Y="-2.779348388672" />
                  <Point X="2.138985595703" Y="-2.795141845703" />
                  <Point X="2.151819091797" Y="-2.826078613281" />
                  <Point X="2.743571289062" Y="-3.851022949219" />
                  <Point X="2.861283203125" Y="-4.054906494141" />
                  <Point X="2.781848632813" Y="-4.11164453125" />
                  <Point X="2.738424560547" Y="-4.139752441406" />
                  <Point X="2.701765136719" Y="-4.163481445312" />
                  <Point X="2.001111816406" Y="-3.250372802734" />
                  <Point X="1.758546020508" Y="-2.934255126953" />
                  <Point X="1.747503540039" Y="-2.922179443359" />
                  <Point X="1.721924316406" Y="-2.900557617188" />
                  <Point X="1.671473632812" Y="-2.868122314453" />
                  <Point X="1.50880090332" Y="-2.763538818359" />
                  <Point X="1.479989746094" Y="-2.751167236328" />
                  <Point X="1.448368774414" Y="-2.743435546875" />
                  <Point X="1.417099731445" Y="-2.741116699219" />
                  <Point X="1.361923339844" Y="-2.746194091797" />
                  <Point X="1.184012817383" Y="-2.762565673828" />
                  <Point X="1.15636315918" Y="-2.769397460938" />
                  <Point X="1.128978027344" Y="-2.780740722656" />
                  <Point X="1.104595947266" Y="-2.795461181641" />
                  <Point X="1.061990234375" Y="-2.830886474609" />
                  <Point X="0.924612121582" Y="-2.945111572266" />
                  <Point X="0.904141052246" Y="-2.968861816406" />
                  <Point X="0.887248779297" Y="-2.996688476562" />
                  <Point X="0.875624206543" Y="-3.025809082031" />
                  <Point X="0.862885314941" Y="-3.084418212891" />
                  <Point X="0.821809936523" Y="-3.273396972656" />
                  <Point X="0.819724487305" Y="-3.289627441406" />
                  <Point X="0.819742248535" Y="-3.323120117188" />
                  <Point X="0.987440124512" Y="-4.596912597656" />
                  <Point X="1.022065246582" Y="-4.859915039062" />
                  <Point X="0.975681396484" Y="-4.870082519531" />
                  <Point X="0.935557861328" Y="-4.877371582031" />
                  <Point X="0.929315612793" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058439575195" Y="-4.752634277344" />
                  <Point X="-1.121441162109" Y="-4.736424316406" />
                  <Point X="-1.141246337891" Y="-4.731328613281" />
                  <Point X="-1.132190429688" Y="-4.662542480469" />
                  <Point X="-1.120775756836" Y="-4.575838378906" />
                  <Point X="-1.120077392578" Y="-4.568099609375" />
                  <Point X="-1.119451660156" Y="-4.541033691406" />
                  <Point X="-1.121759277344" Y="-4.509328125" />
                  <Point X="-1.123334106445" Y="-4.497690429688" />
                  <Point X="-1.137662231445" Y="-4.425658203125" />
                  <Point X="-1.183861572266" Y="-4.193398925781" />
                  <Point X="-1.18812512207" Y="-4.17846875" />
                  <Point X="-1.19902734375" Y="-4.149502441406" />
                  <Point X="-1.205666137695" Y="-4.135466308594" />
                  <Point X="-1.221738037109" Y="-4.10762890625" />
                  <Point X="-1.230573852539" Y="-4.094862304688" />
                  <Point X="-1.250207885742" Y="-4.0709375" />
                  <Point X="-1.261006713867" Y="-4.059779296875" />
                  <Point X="-1.316224487305" Y="-4.011354736328" />
                  <Point X="-1.494267700195" Y="-3.855214599609" />
                  <Point X="-1.506738647461" Y="-3.845965576172" />
                  <Point X="-1.533021606445" Y="-3.829621582031" />
                  <Point X="-1.546833496094" Y="-3.822526855469" />
                  <Point X="-1.576530761719" Y="-3.810225830078" />
                  <Point X="-1.591313354492" Y="-3.805476074219" />
                  <Point X="-1.621455078125" Y="-3.798447998047" />
                  <Point X="-1.636813964844" Y="-3.796169677734" />
                  <Point X="-1.710100097656" Y="-3.791366210938" />
                  <Point X="-1.946403198242" Y="-3.775878173828" />
                  <Point X="-1.961928222656" Y="-3.776132324219" />
                  <Point X="-1.992729614258" Y="-3.779166015625" />
                  <Point X="-2.008006103516" Y="-3.781945556641" />
                  <Point X="-2.03905480957" Y="-3.790265136719" />
                  <Point X="-2.053674316406" Y="-3.795496337891" />
                  <Point X="-2.081865478516" Y="-3.808269775391" />
                  <Point X="-2.095437011719" Y="-3.815812011719" />
                  <Point X="-2.156502929688" Y="-3.856614990234" />
                  <Point X="-2.353403564453" Y="-3.988180175781" />
                  <Point X="-2.359681640625" Y="-3.992757080078" />
                  <Point X="-2.380442138672" Y="-4.010131103516" />
                  <Point X="-2.402759277344" Y="-4.032771484375" />
                  <Point X="-2.410471435547" Y="-4.041629638672" />
                  <Point X="-2.503202880859" Y="-4.162479492188" />
                  <Point X="-2.747580810547" Y="-4.011166748047" />
                  <Point X="-2.834826171875" Y="-3.943990722656" />
                  <Point X="-2.980862792969" Y="-3.831547363281" />
                  <Point X="-2.517711914062" Y="-3.029346679688" />
                  <Point X="-2.341488769531" Y="-2.724119628906" />
                  <Point X="-2.334850097656" Y="-2.710083007812" />
                  <Point X="-2.323947998047" Y="-2.681116455078" />
                  <Point X="-2.319684570312" Y="-2.666186523438" />
                  <Point X="-2.313413574219" Y="-2.634659912109" />
                  <Point X="-2.311638916016" Y="-2.619234863281" />
                  <Point X="-2.310626220703" Y="-2.58830078125" />
                  <Point X="-2.314666503906" Y="-2.557614013672" />
                  <Point X="-2.323651367188" Y="-2.527996337891" />
                  <Point X="-2.329357910156" Y="-2.513556396484" />
                  <Point X="-2.343575195312" Y="-2.484727294922" />
                  <Point X="-2.351557617188" Y="-2.471409667969" />
                  <Point X="-2.369584960938" Y="-2.446252929688" />
                  <Point X="-2.379629882812" Y="-2.434413818359" />
                  <Point X="-2.396978515625" Y="-2.417065185547" />
                  <Point X="-2.408818359375" Y="-2.407019775391" />
                  <Point X="-2.4339765625" Y="-2.388991699219" />
                  <Point X="-2.447294921875" Y="-2.381009033203" />
                  <Point X="-2.476123779297" Y="-2.366792480469" />
                  <Point X="-2.490563964844" Y="-2.361086181641" />
                  <Point X="-2.520181152344" Y="-2.352102050781" />
                  <Point X="-2.550866210938" Y="-2.348062255859" />
                  <Point X="-2.581799560547" Y="-2.349074951172" />
                  <Point X="-2.597224853516" Y="-2.350849609375" />
                  <Point X="-2.628750976562" Y="-2.357120605469" />
                  <Point X="-2.643680908203" Y="-2.361384033203" />
                  <Point X="-2.672647216797" Y="-2.372286132812" />
                  <Point X="-2.68668359375" Y="-2.378924804688" />
                  <Point X="-3.670855712891" Y="-2.947136962891" />
                  <Point X="-3.793089355469" Y="-3.017708496094" />
                  <Point X="-4.004015380859" Y="-2.740594726562" />
                  <Point X="-4.066558105469" Y="-2.635719970703" />
                  <Point X="-4.181265136719" Y="-2.443373779297" />
                  <Point X="-3.356455322266" Y="-1.810474243164" />
                  <Point X="-3.048122314453" Y="-1.573882202148" />
                  <Point X="-3.039157714844" Y="-1.566064819336" />
                  <Point X="-3.016265869141" Y="-1.543428833008" />
                  <Point X="-3.002550537109" Y="-1.526303344727" />
                  <Point X="-2.983400878906" Y="-1.495728149414" />
                  <Point X="-2.975290771484" Y="-1.479523071289" />
                  <Point X="-2.966753417969" Y="-1.457413696289" />
                  <Point X="-2.960067626953" Y="-1.436610961914" />
                  <Point X="-2.954186767578" Y="-1.413905395508" />
                  <Point X="-2.951953125" Y="-1.402396728516" />
                  <Point X="-2.947838623047" Y="-1.370913696289" />
                  <Point X="-2.94708203125" Y="-1.355699462891" />
                  <Point X="-2.94778125" Y="-1.332831298828" />
                  <Point X="-2.951229003906" Y="-1.310213867188" />
                  <Point X="-2.957375244141" Y="-1.28817565918" />
                  <Point X="-2.961114257812" Y="-1.277342041016" />
                  <Point X="-2.973119140625" Y="-1.248359375" />
                  <Point X="-2.978729736328" Y="-1.237016723633" />
                  <Point X="-2.991408447266" Y="-1.215177734375" />
                  <Point X="-3.006877197266" Y="-1.195217529297" />
                  <Point X="-3.024861572266" Y="-1.177490478516" />
                  <Point X="-3.0344453125" Y="-1.169227294922" />
                  <Point X="-3.053216552734" Y="-1.155108032227" />
                  <Point X="-3.071056152344" Y="-1.143204345703" />
                  <Point X="-3.091269775391" Y="-1.131307617188" />
                  <Point X="-3.105435058594" Y="-1.124480712891" />
                  <Point X="-3.134697509766" Y="-1.113256958008" />
                  <Point X="-3.149795410156" Y="-1.108860107422" />
                  <Point X="-3.181683105469" Y="-1.102378540039" />
                  <Point X="-3.197298828125" Y="-1.100532470703" />
                  <Point X="-3.228622070312" Y="-1.09944140625" />
                  <Point X="-3.244329101562" Y="-1.100196533203" />
                  <Point X="-4.486752929688" Y="-1.263764892578" />
                  <Point X="-4.660920898438" Y="-1.286694458008" />
                  <Point X="-4.74076171875" Y="-0.974119995117" />
                  <Point X="-4.75730859375" Y="-0.858425720215" />
                  <Point X="-4.786452148438" Y="-0.654654296875" />
                  <Point X="-3.859525878906" Y="-0.40628527832" />
                  <Point X="-3.508288085938" Y="-0.312171295166" />
                  <Point X="-3.498044189453" Y="-0.308792358398" />
                  <Point X="-3.478005126953" Y="-0.300894989014" />
                  <Point X="-3.468209960938" Y="-0.296376556396" />
                  <Point X="-3.446072265625" Y="-0.284596557617" />
                  <Point X="-3.426992431641" Y="-0.272954467773" />
                  <Point X="-3.405809082031" Y="-0.258252258301" />
                  <Point X="-3.396478027344" Y="-0.25086895752" />
                  <Point X="-3.372522705078" Y="-0.22934211731" />
                  <Point X="-3.357985351562" Y="-0.212861328125" />
                  <Point X="-3.337372558594" Y="-0.18317300415" />
                  <Point X="-3.328491210938" Y="-0.167339767456" />
                  <Point X="-3.318875976562" Y="-0.145546142578" />
                  <Point X="-3.311247558594" Y="-0.125170387268" />
                  <Point X="-3.304186523438" Y="-0.102419494629" />
                  <Point X="-3.301576416016" Y="-0.091937713623" />
                  <Point X="-3.296133544922" Y="-0.063198501587" />
                  <Point X="-3.294503662109" Y="-0.043163387299" />
                  <Point X="-3.295325927734" Y="-0.010039058685" />
                  <Point X="-3.297192382813" Y="0.006486768723" />
                  <Point X="-3.301629394531" Y="0.028363466263" />
                  <Point X="-3.306376464844" Y="0.04691532135" />
                  <Point X="-3.3134375" Y="0.069666221619" />
                  <Point X="-3.317668945312" Y="0.080786628723" />
                  <Point X="-3.330984863281" Y="0.110110076904" />
                  <Point X="-3.342407470703" Y="0.12904107666" />
                  <Point X="-3.364717041016" Y="0.157814620972" />
                  <Point X="-3.377262939453" Y="0.17112260437" />
                  <Point X="-3.395651367188" Y="0.187200042725" />
                  <Point X="-3.412377929688" Y="0.200251068115" />
                  <Point X="-3.433561035156" Y="0.214953292847" />
                  <Point X="-3.440483642578" Y="0.219328292847" />
                  <Point X="-3.465615234375" Y="0.232834106445" />
                  <Point X="-3.496566650391" Y="0.245635742188" />
                  <Point X="-3.508288085938" Y="0.249611251831" />
                  <Point X="-4.64081640625" Y="0.553071289062" />
                  <Point X="-4.785446289062" Y="0.591824707031" />
                  <Point X="-4.731330566406" Y="0.957532104492" />
                  <Point X="-4.698021484375" Y="1.080452758789" />
                  <Point X="-4.633586425781" Y="1.318237060547" />
                  <Point X="-4.029842041016" Y="1.238752563477" />
                  <Point X="-3.778066162109" Y="1.20560546875" />
                  <Point X="-3.76773828125" Y="1.204815429688" />
                  <Point X="-3.747058105469" Y="1.204364257812" />
                  <Point X="-3.736705810547" Y="1.20470324707" />
                  <Point X="-3.715144287109" Y="1.20658972168" />
                  <Point X="-3.704890136719" Y="1.208053588867" />
                  <Point X="-3.684603271484" Y="1.212089233398" />
                  <Point X="-3.660030517578" Y="1.219244995117" />
                  <Point X="-3.613145263672" Y="1.234028076172" />
                  <Point X="-3.603451171875" Y="1.237676513672" />
                  <Point X="-3.584518554688" Y="1.246007080078" />
                  <Point X="-3.575279541016" Y="1.250688964844" />
                  <Point X="-3.55653515625" Y="1.261510864258" />
                  <Point X="-3.547860107422" Y="1.267171386719" />
                  <Point X="-3.531178710938" Y="1.279402709961" />
                  <Point X="-3.515928466797" Y="1.293376953125" />
                  <Point X="-3.502290039063" Y="1.308928710938" />
                  <Point X="-3.495895019531" Y="1.317077270508" />
                  <Point X="-3.483480712891" Y="1.334807006836" />
                  <Point X="-3.478011230469" Y="1.343602783203" />
                  <Point X="-3.468062011719" Y="1.361738037109" />
                  <Point X="-3.457748046875" Y="1.385162597656" />
                  <Point X="-3.438935058594" Y="1.430581176758" />
                  <Point X="-3.435499023438" Y="1.440352050781" />
                  <Point X="-3.429711181641" Y="1.460209228516" />
                  <Point X="-3.427359375" Y="1.470295898438" />
                  <Point X="-3.423600830078" Y="1.491611083984" />
                  <Point X="-3.422360839844" Y="1.501894042969" />
                  <Point X="-3.4210078125" Y="1.522534667969" />
                  <Point X="-3.42191015625" Y="1.543200927734" />
                  <Point X="-3.425056884766" Y="1.563644897461" />
                  <Point X="-3.427188232422" Y="1.573780639648" />
                  <Point X="-3.432790039062" Y="1.594686889648" />
                  <Point X="-3.436012207031" Y="1.604530517578" />
                  <Point X="-3.443509033203" Y="1.62380871582" />
                  <Point X="-3.454823730469" Y="1.646766967773" />
                  <Point X="-3.4775234375" Y="1.690372680664" />
                  <Point X="-3.482799072266" Y="1.699284912109" />
                  <Point X="-3.494290283203" Y="1.716483276367" />
                  <Point X="-3.500505859375" Y="1.72476940918" />
                  <Point X="-3.514418212891" Y="1.741349731445" />
                  <Point X="-3.521499511719" Y="1.748910400391" />
                  <Point X="-3.536442138672" Y="1.76321484375" />
                  <Point X="-3.544303466797" Y="1.769958862305" />
                  <Point X="-4.193924316406" Y="2.268430664062" />
                  <Point X="-4.227614746094" Y="2.294282226563" />
                  <Point X="-4.002293701172" Y="2.680311523438" />
                  <Point X="-3.914062011719" Y="2.793720703125" />
                  <Point X="-3.726338378906" Y="3.035012695312" />
                  <Point X="-3.408790771484" Y="2.851676269531" />
                  <Point X="-3.254156982422" Y="2.7623984375" />
                  <Point X="-3.244918457031" Y="2.757716552734" />
                  <Point X="-3.225986083984" Y="2.749385986328" />
                  <Point X="-3.216292236328" Y="2.745737304688" />
                  <Point X="-3.195650146484" Y="2.739228759766" />
                  <Point X="-3.185615722656" Y="2.736656982422" />
                  <Point X="-3.165327880859" Y="2.732621582031" />
                  <Point X="-3.155074462891" Y="2.731157958984" />
                  <Point X="-3.134823242188" Y="2.729386230469" />
                  <Point X="-3.069525146484" Y="2.723673339844" />
                  <Point X="-3.059173339844" Y="2.723334472656" />
                  <Point X="-3.038493164062" Y="2.723785644531" />
                  <Point X="-3.028164794922" Y="2.724575683594" />
                  <Point X="-3.006706054688" Y="2.727400878906" />
                  <Point X="-2.996526611328" Y="2.729310546875" />
                  <Point X="-2.976435302734" Y="2.734226806641" />
                  <Point X="-2.956997802734" Y="2.741301513672" />
                  <Point X="-2.938446533203" Y="2.750449951172" />
                  <Point X="-2.929420898438" Y="2.755530273438" />
                  <Point X="-2.911166503906" Y="2.767159423828" />
                  <Point X="-2.902746337891" Y="2.773193359375" />
                  <Point X="-2.886614257812" Y="2.786140869141" />
                  <Point X="-2.87890234375" Y="2.793054443359" />
                  <Point X="-2.864527832031" Y="2.807428955078" />
                  <Point X="-2.818178710938" Y="2.853777832031" />
                  <Point X="-2.811265380859" Y="2.861489501953" />
                  <Point X="-2.798318359375" Y="2.877620849609" />
                  <Point X="-2.792284667969" Y="2.886040527344" />
                  <Point X="-2.780655273438" Y="2.904294921875" />
                  <Point X="-2.775574707031" Y="2.913320800781" />
                  <Point X="-2.766426025391" Y="2.931872314453" />
                  <Point X="-2.759351318359" Y="2.951309570312" />
                  <Point X="-2.754434814453" Y="2.971401367188" />
                  <Point X="-2.752524902344" Y="2.981581542969" />
                  <Point X="-2.749699707031" Y="3.003040527344" />
                  <Point X="-2.748909667969" Y="3.013368896484" />
                  <Point X="-2.748458496094" Y="3.034048828125" />
                  <Point X="-2.748797363281" Y="3.044400390625" />
                  <Point X="-2.750569091797" Y="3.064651855469" />
                  <Point X="-2.756281982422" Y="3.129949951172" />
                  <Point X="-2.757745605469" Y="3.140203369141" />
                  <Point X="-2.761781005859" Y="3.160491455078" />
                  <Point X="-2.764352783203" Y="3.170526123047" />
                  <Point X="-2.770861328125" Y="3.191168212891" />
                  <Point X="-2.774510009766" Y="3.200862060547" />
                  <Point X="-2.782840576172" Y="3.219794433594" />
                  <Point X="-2.787522460938" Y="3.229032958984" />
                  <Point X="-3.059387207031" Y="3.699916503906" />
                  <Point X="-2.648373291016" Y="4.015036865234" />
                  <Point X="-2.509418457031" Y="4.092236572266" />
                  <Point X="-2.192524414062" Y="4.268296386719" />
                  <Point X="-2.16590234375" Y="4.2336015625" />
                  <Point X="-2.118564208984" Y="4.171909179688" />
                  <Point X="-2.111820800781" Y="4.164048339844" />
                  <Point X="-2.097517822266" Y="4.149106933594" />
                  <Point X="-2.089958007813" Y="4.142026367188" />
                  <Point X="-2.073377929688" Y="4.128113769531" />
                  <Point X="-2.065093017578" Y="4.1218984375" />
                  <Point X="-2.047893676758" Y="4.110405761719" />
                  <Point X="-2.038979248047" Y="4.105128417969" />
                  <Point X="-2.016439697266" Y="4.093395263672" />
                  <Point X="-1.943763061523" Y="4.055561767578" />
                  <Point X="-1.934326904297" Y="4.051286132812" />
                  <Point X="-1.915047119141" Y="4.0437890625" />
                  <Point X="-1.905203613281" Y="4.040567138672" />
                  <Point X="-1.88429699707" Y="4.034965576172" />
                  <Point X="-1.874162475586" Y="4.032834716797" />
                  <Point X="-1.853719360352" Y="4.029688232422" />
                  <Point X="-1.833053222656" Y="4.028785888672" />
                  <Point X="-1.812413818359" Y="4.030138916016" />
                  <Point X="-1.802132446289" Y="4.031378662109" />
                  <Point X="-1.780817138672" Y="4.035136962891" />
                  <Point X="-1.770729614258" Y="4.037489013672" />
                  <Point X="-1.750869628906" Y="4.043277587891" />
                  <Point X="-1.741097167969" Y="4.046714355469" />
                  <Point X="-1.717620605469" Y="4.056438964844" />
                  <Point X="-1.641923217773" Y="4.087793945312" />
                  <Point X="-1.632586181641" Y="4.092272460938" />
                  <Point X="-1.61445324707" Y="4.102220214844" />
                  <Point X="-1.605657592773" Y="4.107689453125" />
                  <Point X="-1.587927978516" Y="4.120103515625" />
                  <Point X="-1.579777832031" Y="4.126499511719" />
                  <Point X="-1.564225708008" Y="4.140138671875" />
                  <Point X="-1.550252075195" Y="4.155388183594" />
                  <Point X="-1.538020507812" Y="4.172069824219" />
                  <Point X="-1.532359863281" Y="4.180745117188" />
                  <Point X="-1.521538085938" Y="4.199489257812" />
                  <Point X="-1.516856201172" Y="4.208727539062" />
                  <Point X="-1.508525634766" Y="4.22766015625" />
                  <Point X="-1.504876953125" Y="4.237354492188" />
                  <Point X="-1.497235839844" Y="4.261589355469" />
                  <Point X="-1.47259753418" Y="4.339730957031" />
                  <Point X="-1.470026000977" Y="4.349765136719" />
                  <Point X="-1.465990844727" Y="4.370052246094" />
                  <Point X="-1.46452722168" Y="4.380305175781" />
                  <Point X="-1.46264074707" Y="4.4018671875" />
                  <Point X="-1.462301757812" Y="4.412219238281" />
                  <Point X="-1.462752929688" Y="4.432898925781" />
                  <Point X="-1.46354309082" Y="4.4432265625" />
                  <Point X="-1.479266113281" Y="4.562654785156" />
                  <Point X="-0.93117565918" Y="4.716320800781" />
                  <Point X="-0.762730712891" Y="4.736034179688" />
                  <Point X="-0.365221923828" Y="4.782557128906" />
                  <Point X="-0.271411193848" Y="4.432450195312" />
                  <Point X="-0.225666244507" Y="4.261727539062" />
                  <Point X="-0.220435317993" Y="4.247107910156" />
                  <Point X="-0.207661773682" Y="4.218916503906" />
                  <Point X="-0.200119155884" Y="4.205344726562" />
                  <Point X="-0.182260925293" Y="4.178618164062" />
                  <Point X="-0.172608306885" Y="4.166456054688" />
                  <Point X="-0.151451248169" Y="4.1438671875" />
                  <Point X="-0.126898002625" Y="4.125026367188" />
                  <Point X="-0.099602806091" Y="4.110436523438" />
                  <Point X="-0.085356712341" Y="4.104260742188" />
                  <Point X="-0.054918586731" Y="4.093927978516" />
                  <Point X="-0.039857292175" Y="4.090155273438" />
                  <Point X="-0.009319890976" Y="4.08511328125" />
                  <Point X="0.021631721497" Y="4.08511328125" />
                  <Point X="0.052168972015" Y="4.090155273438" />
                  <Point X="0.067230415344" Y="4.093927978516" />
                  <Point X="0.097668540955" Y="4.104260742188" />
                  <Point X="0.111914489746" Y="4.110436523438" />
                  <Point X="0.139209686279" Y="4.125026367188" />
                  <Point X="0.16376322937" Y="4.143866699219" />
                  <Point X="0.184920150757" Y="4.166455566406" />
                  <Point X="0.194572906494" Y="4.178617675781" />
                  <Point X="0.212431137085" Y="4.205344238281" />
                  <Point X="0.219973754883" Y="4.218916503906" />
                  <Point X="0.232747146606" Y="4.247107910156" />
                  <Point X="0.237978225708" Y="4.261727539062" />
                  <Point X="0.378190216064" Y="4.785006347656" />
                  <Point X="0.827876647949" Y="4.737912109375" />
                  <Point X="0.967247070312" Y="4.704263671875" />
                  <Point X="1.453594482422" Y="4.586844726562" />
                  <Point X="1.542209228516" Y="4.554703125" />
                  <Point X="1.858255004883" Y="4.440071289062" />
                  <Point X="1.945981567383" Y="4.399044433594" />
                  <Point X="2.250455810547" Y="4.256651367187" />
                  <Point X="2.335254882812" Y="4.207247558594" />
                  <Point X="2.62943359375" Y="4.035858154297" />
                  <Point X="2.709364257812" Y="3.979015869141" />
                  <Point X="2.817779785156" Y="3.901916748047" />
                  <Point X="2.2692734375" Y="2.951875488281" />
                  <Point X="2.06530859375" Y="2.598597900391" />
                  <Point X="2.060784667969" Y="2.589716064453" />
                  <Point X="2.049122558594" Y="2.562218994141" />
                  <Point X="2.03903894043" Y="2.531130371094" />
                  <Point X="2.036219604492" Y="2.521593261719" />
                  <Point X="2.01983215332" Y="2.460312255859" />
                  <Point X="2.017307983398" Y="2.447287841797" />
                  <Point X="2.014098999023" Y="2.421014648438" />
                  <Point X="2.01341394043" Y="2.407765869141" />
                  <Point X="2.014057983398" Y="2.372429443359" />
                  <Point X="2.015392700195" Y="2.353146240234" />
                  <Point X="2.021782348633" Y="2.300155517578" />
                  <Point X="2.02380065918" Y="2.289033935547" />
                  <Point X="2.029143676758" Y="2.267110595703" />
                  <Point X="2.032468261719" Y="2.256308837891" />
                  <Point X="2.040734375" Y="2.234220214844" />
                  <Point X="2.045318359375" Y="2.223889160156" />
                  <Point X="2.055681152344" Y="2.203843994141" />
                  <Point X="2.07162890625" Y="2.179143554688" />
                  <Point X="2.104417724609" Y="2.130821289063" />
                  <Point X="2.112694580078" Y="2.120302490234" />
                  <Point X="2.130645019531" Y="2.100532226563" />
                  <Point X="2.140318603516" Y="2.091280761719" />
                  <Point X="2.168604980469" Y="2.067811523437" />
                  <Point X="2.183244628906" Y="2.056812011719" />
                  <Point X="2.231566894531" Y="2.0240234375" />
                  <Point X="2.241280517578" Y="2.018244873047" />
                  <Point X="2.261325683594" Y="2.007882080078" />
                  <Point X="2.271657226562" Y="2.003297851562" />
                  <Point X="2.293745605469" Y="1.995031982422" />
                  <Point X="2.304547119141" Y="1.991707641602" />
                  <Point X="2.326469726562" Y="1.986364868164" />
                  <Point X="2.354025146484" Y="1.982364868164" />
                  <Point X="2.407015625" Y="1.975974975586" />
                  <Point X="2.420657470703" Y="1.975318847656" />
                  <Point X="2.447894042969" Y="1.975969482422" />
                  <Point X="2.461488769531" Y="1.977276123047" />
                  <Point X="2.499396728516" Y="1.983696411133" />
                  <Point X="2.516753173828" Y="1.987478149414" />
                  <Point X="2.578034179688" Y="2.003865356445" />
                  <Point X="2.583995361328" Y="2.005670776367" />
                  <Point X="2.604404541016" Y="2.013067260742" />
                  <Point X="2.627655029297" Y="2.02357409668" />
                  <Point X="2.636033935547" Y="2.027872558594" />
                  <Point X="3.775139404297" Y="2.685535400391" />
                  <Point X="3.940404785156" Y="2.780951416016" />
                  <Point X="4.043952880859" Y="2.637042724609" />
                  <Point X="4.088513671875" Y="2.563405761719" />
                  <Point X="4.136884277344" Y="2.483472412109" />
                  <Point X="3.441490478516" Y="1.949876953125" />
                  <Point X="3.172951660156" Y="1.743819702148" />
                  <Point X="3.168138671875" Y="1.739869506836" />
                  <Point X="3.152119873047" Y="1.725217163086" />
                  <Point X="3.134668457031" Y="1.706602783203" />
                  <Point X="3.128575927734" Y="1.69942175293" />
                  <Point X="3.114897705078" Y="1.681577270508" />
                  <Point X="3.070793701172" Y="1.624040283203" />
                  <Point X="3.063329345703" Y="1.612709960938" />
                  <Point X="3.050090820312" Y="1.589100830078" />
                  <Point X="3.044316650391" Y="1.576822021484" />
                  <Point X="3.030751953125" Y="1.541651000977" />
                  <Point X="3.025044921875" Y="1.524452758789" />
                  <Point X="3.008616210938" Y="1.465707397461" />
                  <Point X="3.006225097656" Y="1.454661010742" />
                  <Point X="3.002771728516" Y="1.432362792969" />
                  <Point X="3.001709472656" Y="1.421110839844" />
                  <Point X="3.000893310547" Y="1.397540527344" />
                  <Point X="3.001174804688" Y="1.386240966797" />
                  <Point X="3.003077880859" Y="1.363756103516" />
                  <Point X="3.004699462891" Y="1.352570800781" />
                  <Point X="3.008882080078" Y="1.332299682617" />
                  <Point X="3.022368408203" Y="1.266937988281" />
                  <Point X="3.024597900391" Y="1.258234619141" />
                  <Point X="3.034683837891" Y="1.228608520508" />
                  <Point X="3.050286376953" Y="1.195371337891" />
                  <Point X="3.056918457031" Y="1.183526123047" />
                  <Point X="3.068294677734" Y="1.166234619141" />
                  <Point X="3.104976074219" Y="1.110480712891" />
                  <Point X="3.111739257813" Y="1.101424194336" />
                  <Point X="3.126292724609" Y="1.08417956543" />
                  <Point X="3.134083007812" Y="1.075991210938" />
                  <Point X="3.151326904297" Y="1.059901367188" />
                  <Point X="3.160034423828" Y="1.052696044922" />
                  <Point X="3.178243896484" Y="1.039370361328" />
                  <Point X="3.204231445313" Y="1.023969909668" />
                  <Point X="3.257387939453" Y="0.994047668457" />
                  <Point X="3.265479248047" Y="0.989987854004" />
                  <Point X="3.294677978516" Y="0.978084350586" />
                  <Point X="3.330274169922" Y="0.96802142334" />
                  <Point X="3.343669433594" Y="0.965257751465" />
                  <Point X="3.365959228516" Y="0.962311767578" />
                  <Point X="3.437830078125" Y="0.952813293457" />
                  <Point X="3.444027587891" Y="0.952200195312" />
                  <Point X="3.465716064453" Y="0.951222839355" />
                  <Point X="3.491217529297" Y="0.952032226562" />
                  <Point X="3.500603515625" Y="0.952797241211" />
                  <Point X="4.582677734375" Y="1.095255004883" />
                  <Point X="4.704703613281" Y="1.111320068359" />
                  <Point X="4.752684082031" Y="0.91423248291" />
                  <Point X="4.766726074219" Y="0.824040710449" />
                  <Point X="4.78387109375" Y="0.713920837402" />
                  <Point X="3.999743408203" Y="0.503814544678" />
                  <Point X="3.691991943359" Y="0.421352905273" />
                  <Point X="3.686031738281" Y="0.419544464111" />
                  <Point X="3.665626708984" Y="0.412138000488" />
                  <Point X="3.642380859375" Y="0.40161920166" />
                  <Point X="3.634004150391" Y="0.397316558838" />
                  <Point X="3.612105224609" Y="0.384658508301" />
                  <Point X="3.541494140625" Y="0.343843963623" />
                  <Point X="3.533881591797" Y="0.338945526123" />
                  <Point X="3.508773925781" Y="0.319870361328" />
                  <Point X="3.481993896484" Y="0.294350769043" />
                  <Point X="3.472797119141" Y="0.284226867676" />
                  <Point X="3.459657714844" Y="0.267484191895" />
                  <Point X="3.417291259766" Y="0.213499160767" />
                  <Point X="3.410854736328" Y="0.204208450317" />
                  <Point X="3.399130859375" Y="0.184928787231" />
                  <Point X="3.393843505859" Y="0.174939834595" />
                  <Point X="3.384069580078" Y="0.153476165771" />
                  <Point X="3.380005615234" Y="0.142929580688" />
                  <Point X="3.373159179688" Y="0.12142830658" />
                  <Point X="3.370376464844" Y="0.110473464966" />
                  <Point X="3.365996582031" Y="0.087603668213" />
                  <Point X="3.351874267578" Y="0.013862774849" />
                  <Point X="3.350603759766" Y="0.004969031811" />
                  <Point X="3.348584228516" Y="-0.02626288414" />
                  <Point X="3.350280029297" Y="-0.062941532135" />
                  <Point X="3.351874511719" Y="-0.076423118591" />
                  <Point X="3.356254394531" Y="-0.099292762756" />
                  <Point X="3.370376708984" Y="-0.173033660889" />
                  <Point X="3.373158935547" Y="-0.183987609863" />
                  <Point X="3.380005126953" Y="-0.205488739014" />
                  <Point X="3.384069091797" Y="-0.216035766602" />
                  <Point X="3.393843017578" Y="-0.237499588013" />
                  <Point X="3.399130859375" Y="-0.247489120483" />
                  <Point X="3.410854980469" Y="-0.266768920898" />
                  <Point X="3.417291259766" Y="-0.276059356689" />
                  <Point X="3.430430664062" Y="-0.292801879883" />
                  <Point X="3.472797119141" Y="-0.34678704834" />
                  <Point X="3.478718017578" Y="-0.353633239746" />
                  <Point X="3.501139648438" Y="-0.375805389404" />
                  <Point X="3.530176513672" Y="-0.398724975586" />
                  <Point X="3.541495117188" Y="-0.406404602051" />
                  <Point X="3.563394287109" Y="-0.419062530518" />
                  <Point X="3.634005126953" Y="-0.45987689209" />
                  <Point X="3.639495849609" Y="-0.462814727783" />
                  <Point X="3.659156738281" Y="-0.472016143799" />
                  <Point X="3.68302734375" Y="-0.481027587891" />
                  <Point X="3.691991943359" Y="-0.48391293335" />
                  <Point X="4.684303222656" Y="-0.749801696777" />
                  <Point X="4.784876953125" Y="-0.776750427246" />
                  <Point X="4.76161328125" Y="-0.931051879883" />
                  <Point X="4.743622070312" Y="-1.009892272949" />
                  <Point X="4.727801757812" Y="-1.079219604492" />
                  <Point X="3.793687011719" Y="-0.956241149902" />
                  <Point X="3.436781982422" Y="-0.909253723145" />
                  <Point X="3.428624511719" Y="-0.908535888672" />
                  <Point X="3.400098388672" Y="-0.908042480469" />
                  <Point X="3.366721435547" Y="-0.910840881348" />
                  <Point X="3.354481201172" Y="-0.912676208496" />
                  <Point X="3.311501220703" Y="-0.922018188477" />
                  <Point X="3.172916992188" Y="-0.952140075684" />
                  <Point X="3.157874511719" Y="-0.956742492676" />
                  <Point X="3.128754150391" Y="-0.968366882324" />
                  <Point X="3.114676269531" Y="-0.975388977051" />
                  <Point X="3.086849609375" Y="-0.992281311035" />
                  <Point X="3.074124023438" Y="-1.001530273438" />
                  <Point X="3.050374023438" Y="-1.022001159668" />
                  <Point X="3.039349853516" Y="-1.03322265625" />
                  <Point X="3.01337109375" Y="-1.064466674805" />
                  <Point X="2.929605712891" Y="-1.16521081543" />
                  <Point X="2.921326171875" Y="-1.176847290039" />
                  <Point X="2.90660546875" Y="-1.201229858398" />
                  <Point X="2.9001640625" Y="-1.213975708008" />
                  <Point X="2.888820800781" Y="-1.241360839844" />
                  <Point X="2.884362792969" Y="-1.254928222656" />
                  <Point X="2.877531005859" Y="-1.282577880859" />
                  <Point X="2.875157226562" Y="-1.296660400391" />
                  <Point X="2.871433837891" Y="-1.337123168945" />
                  <Point X="2.859428222656" Y="-1.467590698242" />
                  <Point X="2.859288818359" Y="-1.483321655273" />
                  <Point X="2.861607666016" Y="-1.514590576172" />
                  <Point X="2.864065917969" Y="-1.530128662109" />
                  <Point X="2.871797607422" Y="-1.561749633789" />
                  <Point X="2.876786376953" Y="-1.576668701172" />
                  <Point X="2.889157470703" Y="-1.605479370117" />
                  <Point X="2.896540039062" Y="-1.61937121582" />
                  <Point X="2.920325683594" Y="-1.656368286133" />
                  <Point X="2.997020263672" Y="-1.775661621094" />
                  <Point X="3.001742431641" Y="-1.782353881836" />
                  <Point X="3.019793212891" Y="-1.80444921875" />
                  <Point X="3.043488769531" Y="-1.828119750977" />
                  <Point X="3.052796142578" Y="-1.836277709961" />
                  <Point X="3.973666992188" Y="-2.542886962891" />
                  <Point X="4.087170898438" Y="-2.629981933594" />
                  <Point X="4.045493164062" Y="-2.697423339844" />
                  <Point X="4.008280517578" Y="-2.750297119141" />
                  <Point X="4.001274414063" Y="-2.760251953125" />
                  <Point X="3.166326660156" Y="-2.278194335938" />
                  <Point X="2.848454589844" Y="-2.094670654297" />
                  <Point X="2.841191894531" Y="-2.090885253906" />
                  <Point X="2.815026611328" Y="-2.079513427734" />
                  <Point X="2.783119384766" Y="-2.069325927734" />
                  <Point X="2.771107910156" Y="-2.066337158203" />
                  <Point X="2.719954833984" Y="-2.057099121094" />
                  <Point X="2.555017578125" Y="-2.027311645508" />
                  <Point X="2.539358154297" Y="-2.025807250977" />
                  <Point X="2.508006103516" Y="-2.025403198242" />
                  <Point X="2.492313476562" Y="-2.026503540039" />
                  <Point X="2.460140625" Y="-2.031461303711" />
                  <Point X="2.444844482422" Y="-2.035136352539" />
                  <Point X="2.415068847656" Y="-2.044960083008" />
                  <Point X="2.400589355469" Y="-2.051108642578" />
                  <Point X="2.35809375" Y="-2.073473632812" />
                  <Point X="2.221071289062" Y="-2.145587646484" />
                  <Point X="2.208968994141" Y="-2.153169921875" />
                  <Point X="2.186037597656" Y="-2.170063232422" />
                  <Point X="2.175208496094" Y="-2.179374267578" />
                  <Point X="2.154248779297" Y="-2.200334228516" />
                  <Point X="2.144938232422" Y="-2.211162841797" />
                  <Point X="2.128045898438" Y="-2.234093017578" />
                  <Point X="2.120464111328" Y="-2.246194580078" />
                  <Point X="2.098098876953" Y="-2.288690185547" />
                  <Point X="2.025984985352" Y="-2.425712646484" />
                  <Point X="2.019836303711" Y="-2.440192382812" />
                  <Point X="2.010012329102" Y="-2.469968505859" />
                  <Point X="2.006337280273" Y="-2.485264892578" />
                  <Point X="2.001379516602" Y="-2.5174375" />
                  <Point X="2.000279174805" Y="-2.533130126953" />
                  <Point X="2.000683227539" Y="-2.564482177734" />
                  <Point X="2.00218762207" Y="-2.580141601562" />
                  <Point X="2.01142565918" Y="-2.631294677734" />
                  <Point X="2.041213256836" Y="-2.796231933594" />
                  <Point X="2.043015136719" Y="-2.804222167969" />
                  <Point X="2.051236083984" Y="-2.83154296875" />
                  <Point X="2.064069580078" Y="-2.862479736328" />
                  <Point X="2.069546630859" Y="-2.873578613281" />
                  <Point X="2.661298828125" Y="-3.898522949219" />
                  <Point X="2.735892822266" Y="-4.027724121094" />
                  <Point X="2.723754394531" Y="-4.036083496094" />
                  <Point X="2.07648046875" Y="-3.192540527344" />
                  <Point X="1.833914550781" Y="-2.876422851562" />
                  <Point X="1.828653442383" Y="-2.870146240234" />
                  <Point X="1.808831420898" Y="-2.849626708984" />
                  <Point X="1.783252197266" Y="-2.828004882812" />
                  <Point X="1.773299438477" Y="-2.820647705078" />
                  <Point X="1.722848632812" Y="-2.788212402344" />
                  <Point X="1.56017590332" Y="-2.68362890625" />
                  <Point X="1.546284545898" Y="-2.676246337891" />
                  <Point X="1.517473388672" Y="-2.663874755859" />
                  <Point X="1.502553588867" Y="-2.658885742188" />
                  <Point X="1.470932617188" Y="-2.651154052734" />
                  <Point X="1.45539453125" Y="-2.648695800781" />
                  <Point X="1.424125488281" Y="-2.646376953125" />
                  <Point X="1.40839453125" Y="-2.646516357422" />
                  <Point X="1.353218017578" Y="-2.65159375" />
                  <Point X="1.175307739258" Y="-2.667965332031" />
                  <Point X="1.161225097656" Y="-2.670339111328" />
                  <Point X="1.133575439453" Y="-2.677170898438" />
                  <Point X="1.120008422852" Y="-2.68162890625" />
                  <Point X="1.092623168945" Y="-2.692972167969" />
                  <Point X="1.079877563477" Y="-2.699413330078" />
                  <Point X="1.055495361328" Y="-2.714133789062" />
                  <Point X="1.043858886719" Y="-2.722413085938" />
                  <Point X="1.001253173828" Y="-2.757838378906" />
                  <Point X="0.863875061035" Y="-2.872063476562" />
                  <Point X="0.852653320312" Y="-2.883087890625" />
                  <Point X="0.832182128906" Y="-2.906838134766" />
                  <Point X="0.822933044434" Y="-2.919564208984" />
                  <Point X="0.80604083252" Y="-2.947390869141" />
                  <Point X="0.799018859863" Y="-2.961468261719" />
                  <Point X="0.787394165039" Y="-2.990588867188" />
                  <Point X="0.782791748047" Y="-3.005631591797" />
                  <Point X="0.770052856445" Y="-3.064240722656" />
                  <Point X="0.728977478027" Y="-3.253219482422" />
                  <Point X="0.727584594727" Y="-3.261290039062" />
                  <Point X="0.724724487305" Y="-3.289677734375" />
                  <Point X="0.7247421875" Y="-3.323170410156" />
                  <Point X="0.725554992676" Y="-3.335520019531" />
                  <Point X="0.833091308594" Y="-4.15233984375" />
                  <Point X="0.749890808105" Y="-3.841831054688" />
                  <Point X="0.655064941406" Y="-3.487936523438" />
                  <Point X="0.652606262207" Y="-3.480124511719" />
                  <Point X="0.642145141602" Y="-3.453580566406" />
                  <Point X="0.62678717041" Y="-3.423815917969" />
                  <Point X="0.620407714844" Y="-3.413210449219" />
                  <Point X="0.581650146484" Y="-3.357367919922" />
                  <Point X="0.456680114746" Y="-3.177310058594" />
                  <Point X="0.446670654297" Y="-3.165172851562" />
                  <Point X="0.424786804199" Y="-3.142717529297" />
                  <Point X="0.41291229248" Y="-3.132399169922" />
                  <Point X="0.386657165527" Y="-3.113155273438" />
                  <Point X="0.373242462158" Y="-3.104937988281" />
                  <Point X="0.345241607666" Y="-3.090829833984" />
                  <Point X="0.330655151367" Y="-3.084938964844" />
                  <Point X="0.270679870605" Y="-3.066324707031" />
                  <Point X="0.077295906067" Y="-3.006305664062" />
                  <Point X="0.063377368927" Y="-3.003109619141" />
                  <Point X="0.035217639923" Y="-2.998840087891" />
                  <Point X="0.02097659111" Y="-2.997766601562" />
                  <Point X="-0.008664761543" Y="-2.997766601562" />
                  <Point X="-0.022905065536" Y="-2.998840087891" />
                  <Point X="-0.051064498901" Y="-3.003109375" />
                  <Point X="-0.064983329773" Y="-3.006305175781" />
                  <Point X="-0.124958618164" Y="-3.024919189453" />
                  <Point X="-0.318342590332" Y="-3.084938476562" />
                  <Point X="-0.332929473877" Y="-3.090829589844" />
                  <Point X="-0.360930786133" Y="-3.104937988281" />
                  <Point X="-0.374345336914" Y="-3.113155273438" />
                  <Point X="-0.400600463867" Y="-3.132399169922" />
                  <Point X="-0.412474975586" Y="-3.142717529297" />
                  <Point X="-0.434358673096" Y="-3.165172607422" />
                  <Point X="-0.444367828369" Y="-3.177309326172" />
                  <Point X="-0.483125427246" Y="-3.233151611328" />
                  <Point X="-0.608095458984" Y="-3.413209716797" />
                  <Point X="-0.612470275879" Y="-3.420132324219" />
                  <Point X="-0.62597668457" Y="-3.445265136719" />
                  <Point X="-0.638777893066" Y="-3.476215820312" />
                  <Point X="-0.642753112793" Y="-3.487936523438" />
                  <Point X="-0.948508483887" Y="-4.629031738281" />
                  <Point X="-0.985425170898" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.131322320632" Y="-4.655948457435" />
                  <Point X="-0.960530777016" Y="-4.673899372017" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.11992765627" Y="-4.56162279836" />
                  <Point X="-0.935636438946" Y="-4.580992585823" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.129824857876" Y="-4.465059273996" />
                  <Point X="-0.910742153005" Y="-4.488085794151" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.149231381922" Y="-4.367496279568" />
                  <Point X="-0.885847867063" Y="-4.395179002479" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.567035781059" Y="-4.122955745882" />
                  <Point X="-2.47990246324" Y="-4.132113826618" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.168637909399" Y="-4.26993328478" />
                  <Point X="-0.860953581122" Y="-4.302272210807" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.751657346686" Y="-4.008027950855" />
                  <Point X="-2.412075036365" Y="-4.043719489891" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.190518298462" Y="-4.172110276662" />
                  <Point X="-0.83605929518" Y="-4.209365419135" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.895331082675" Y="-3.897403946147" />
                  <Point X="-2.309671487426" Y="-3.958959250032" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.250880321899" Y="-4.070242685791" />
                  <Point X="-0.811165009239" Y="-4.116458627463" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.959821315507" Y="-3.795102462985" />
                  <Point X="-2.186142250324" Y="-3.876419409472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.372585045528" Y="-3.961927717327" />
                  <Point X="-0.786270723297" Y="-4.02355183579" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.907826081875" Y="-3.705044095695" />
                  <Point X="-2.05221129264" Y="-3.794972833799" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.496780547339" Y="-3.853350957527" />
                  <Point X="-0.761376437355" Y="-3.930645044118" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.818172579371" Y="-4.096662335586" />
                  <Point X="0.825867708524" Y="-4.097471126251" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.855830848242" Y="-3.614985728405" />
                  <Point X="-0.736482151414" Y="-3.837738252446" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.791835496213" Y="-3.998370910043" />
                  <Point X="0.813115378691" Y="-4.000607515817" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.80383561461" Y="-3.524927361114" />
                  <Point X="-0.711587865472" Y="-3.744831460774" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.765498413055" Y="-3.9000794845" />
                  <Point X="0.800363048858" Y="-3.903743905384" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.751840380978" Y="-3.434868993824" />
                  <Point X="-0.686693579531" Y="-3.651924669102" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.739161306304" Y="-3.801788056477" />
                  <Point X="0.787610719025" Y="-3.80688029495" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.702298860846" Y="-4.008122127869" />
                  <Point X="2.726014779233" Y="-4.010614771334" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.699845147346" Y="-3.344810626534" />
                  <Point X="-0.661799293589" Y="-3.55901787743" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.712824165234" Y="-3.703496624847" />
                  <Point X="0.774858389192" Y="-3.710016684517" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.622571205308" Y="-3.904219127045" />
                  <Point X="2.667301832875" Y="-3.908920505449" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.647849913714" Y="-3.254752259244" />
                  <Point X="-0.634694659316" Y="-3.466343402729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.686487024164" Y="-3.605205193218" />
                  <Point X="0.762106059359" Y="-3.613153074083" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.542843549771" Y="-3.800316126222" />
                  <Point X="2.608588603532" Y="-3.80722620982" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.595854680082" Y="-3.164693891954" />
                  <Point X="-0.582483491556" Y="-3.37630773103" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.660149883094" Y="-3.506913761588" />
                  <Point X="0.749353729527" Y="-3.51628946365" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.463115894234" Y="-3.696413125399" />
                  <Point X="2.549875341961" Y="-3.705531910804" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.854620613929" Y="-2.93686897454" />
                  <Point X="-3.684111528618" Y="-2.954790201557" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.54385944645" Y="-3.074635524664" />
                  <Point X="-0.520692696472" Y="-3.287278918735" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.615917940922" Y="-3.406741510573" />
                  <Point X="0.736601399694" Y="-3.419425853216" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.383388238697" Y="-3.592510124575" />
                  <Point X="2.491162080389" Y="-3.603837611788" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.933650924999" Y="-2.833039267573" />
                  <Point X="-3.544141346636" Y="-2.873978373936" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.491864186931" Y="-2.984577160094" />
                  <Point X="-0.45890187655" Y="-3.198250109051" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.544402834861" Y="-3.303701683482" />
                  <Point X="0.724741915689" Y="-3.322656084661" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.30366058316" Y="-3.488607123752" />
                  <Point X="2.432448818818" Y="-3.502143312772" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.010679530605" Y="-2.729419948329" />
                  <Point X="-3.404171181845" Y="-2.793166544508" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.439868901224" Y="-2.894518798277" />
                  <Point X="-0.372197828985" Y="-3.111839785106" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.472887528636" Y="-3.200661835353" />
                  <Point X="0.73442629702" Y="-3.228150667596" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.223932927622" Y="-3.384704122929" />
                  <Point X="2.373735557246" Y="-3.400449013756" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.071454823839" Y="-2.627508921052" />
                  <Point X="-3.264201017054" Y="-2.71235471508" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.387873615518" Y="-2.80446043646" />
                  <Point X="-0.166798704194" Y="-3.037904816483" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.347480111684" Y="-3.091957698139" />
                  <Point X="0.754724989369" Y="-3.134760859574" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.144205272085" Y="-3.280801122106" />
                  <Point X="2.315022295675" Y="-3.29875471474" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.132230201412" Y="-2.525597884911" />
                  <Point X="-3.124230852263" Y="-2.631542885651" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.336844774854" Y="-2.714300497176" />
                  <Point X="0.775023656901" Y="-3.041371048943" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.064477613641" Y="-3.176898120977" />
                  <Point X="2.256309034104" Y="-3.197060415724" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.160113965444" Y="-2.427143896657" />
                  <Point X="-2.984260687472" Y="-2.550731056223" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.311888046768" Y="-2.621400268438" />
                  <Point X="0.805227070614" Y="-2.949022269086" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.984749938799" Y="-3.072995118124" />
                  <Point X="2.197595772532" Y="-3.095366116708" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.050623165076" Y="-2.34312855694" />
                  <Point X="-2.844290522681" Y="-2.469919226795" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.325034994446" Y="-2.524495181998" />
                  <Point X="0.877115236875" Y="-2.861054733266" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.905022263957" Y="-2.969092115272" />
                  <Point X="2.138882510961" Y="-2.993671817692" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.941132364709" Y="-2.259113217223" />
                  <Point X="-2.70432035789" Y="-2.389107397367" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.392123043862" Y="-2.42192065731" />
                  <Point X="0.979107998513" Y="-2.776251317922" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.823703248465" Y="-2.865021855778" />
                  <Point X="2.080169249389" Y="-2.891977518677" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.831641564341" Y="-2.175097877505" />
                  <Point X="1.093226293332" Y="-2.692722347471" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.668341548934" Y="-2.7531693966" />
                  <Point X="2.040500418848" Y="-2.79228487002" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.722150763974" Y="-2.091082537788" />
                  <Point X="2.022915154659" Y="-2.694913297717" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.612659963606" Y="-2.007067198071" />
                  <Point X="2.005330015914" Y="-2.597541738599" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.503169163239" Y="-1.923051858354" />
                  <Point X="2.003780668972" Y="-2.501855609115" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.89867693242" Y="-2.701017231792" />
                  <Point X="4.033025663123" Y="-2.715137852392" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.393678362871" Y="-1.839036518636" />
                  <Point X="2.034486209743" Y="-2.409559604937" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.696402682573" Y="-2.584234064889" />
                  <Point X="4.080109148021" Y="-2.624563239507" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.284187438677" Y="-1.755021191934" />
                  <Point X="2.082124271207" Y="-2.319043280398" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.494128432727" Y="-2.467450897987" />
                  <Point X="3.93586321224" Y="-2.513879094178" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.174696450702" Y="-1.671005871935" />
                  <Point X="2.131975011774" Y="-2.228759517805" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.29185418288" Y="-2.350667731084" />
                  <Point X="3.791616800866" Y="-2.403194898862" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.065205462728" Y="-1.586990551936" />
                  <Point X="2.225802094329" Y="-2.143097855005" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.089579953234" Y="-2.233884566305" />
                  <Point X="3.647370389492" Y="-2.292510703546" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.985949506825" Y="-1.499797402013" />
                  <Point X="2.377090852309" Y="-2.063475657659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.887305756626" Y="-2.117101404998" />
                  <Point X="3.503123978117" Y="-2.18182650823" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.676267087625" Y="-1.226614578768" />
                  <Point X="-4.41397120295" Y="-1.25418298714" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.952989839115" Y="-1.407738316123" />
                  <Point X="3.358877566743" Y="-2.071142312915" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.701339695908" Y="-1.12845605489" />
                  <Point X="-4.010505444392" Y="-1.201065660591" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.950890312565" Y="-1.312435698697" />
                  <Point X="3.214631155369" Y="-1.960458117599" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.72641230419" Y="-1.030297531012" />
                  <Point X="-3.607039685834" Y="-1.147948334041" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.993537909039" Y="-1.212429969126" />
                  <Point X="3.070384743995" Y="-1.849773922283" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.746693921754" Y="-0.932642560549" />
                  <Point X="2.976940755156" Y="-1.744429276737" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.760564360749" Y="-0.835661432107" />
                  <Point X="2.911077612688" Y="-1.641983494957" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.774434678792" Y="-0.738680316378" />
                  <Point X="2.866924467955" Y="-1.541819525887" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.752246970989" Y="-0.64548905188" />
                  <Point X="2.861440774219" Y="-1.445719879892" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.496188952226" Y="-0.576878547568" />
                  <Point X="2.870146621834" Y="-1.351111614789" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.240130933463" Y="-0.508268043255" />
                  <Point X="2.883843977031" Y="-1.257027978274" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.984072914699" Y="-0.439657538942" />
                  <Point X="2.928875100862" Y="-1.166237653549" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.728015044708" Y="-0.371047018993" />
                  <Point X="3.001803119594" Y="-1.078379410629" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.479819708737" Y="-0.301610113418" />
                  <Point X="3.08753265912" Y="-0.991866661762" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.362853655467" Y="-0.21838045444" />
                  <Point X="3.318121464241" Y="-0.920579235226" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.333160642476" Y="-1.027264151819" />
                  <Point X="4.730136581731" Y="-1.068988004334" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.312368108415" Y="-0.128163412697" />
                  <Point X="4.751424187409" Y="-0.97570213529" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.294718839378" Y="-0.034495139063" />
                  <Point X="4.769004703566" Y="-0.882026635438" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.311297361265" Y="0.06277062036" />
                  <Point X="4.7831818718" Y="-0.787993429305" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.3710938394" Y="0.164578770025" />
                  <Point X="4.268421628011" Y="-0.638366660978" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.661258758888" Y="0.290599618547" />
                  <Point X="3.684357726071" Y="-0.48145578466" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.247849183488" Y="0.447776043097" />
                  <Point X="3.490809021661" Y="-0.365589709538" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.784283768721" Y="0.599680876507" />
                  <Point X="3.407537995595" Y="-0.261314285465" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.770365140171" Y="0.693741256255" />
                  <Point X="3.36819777662" Y="-0.161656175275" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.75644651162" Y="0.787801636004" />
                  <Point X="3.350436690188" Y="-0.06426612331" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.74252788307" Y="0.881862015753" />
                  <Point X="3.355111416534" Y="0.030765829711" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.726409801907" Y="0.975691223717" />
                  <Point X="3.3740723864" Y="0.124296238032" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.701241685905" Y="1.06856923469" />
                  <Point X="3.418580189071" Y="0.215141566027" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.67607350861" Y="1.161447239221" />
                  <Point X="3.491113639774" Y="0.303041279718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.650905322321" Y="1.254325242807" />
                  <Point X="3.613846236357" Y="0.385664850571" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.599635195412" Y="1.239355586619" />
                  <Point X="3.83036847575" Y="0.45843073274" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.491396103354" Y="1.323502486181" />
                  <Point X="4.086426599218" Y="0.527041226048" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.445710416301" Y="1.414224013539" />
                  <Point X="4.342484600568" Y="0.595651732191" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.422009340429" Y="1.507256216643" />
                  <Point X="4.598542601918" Y="0.664262238334" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.435917540436" Y="1.604241313927" />
                  <Point X="4.779694622762" Y="0.740745680275" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.486684766424" Y="1.70510045095" />
                  <Point X="3.25253394738" Y="0.996780021747" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.576249174114" Y="0.962756180397" />
                  <Point X="4.764574836277" Y="0.837858120429" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.599754337539" Y="1.812507828312" />
                  <Point X="3.107138534269" Y="1.107584982012" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.979715166662" Y="1.015873482353" />
                  <Point X="4.747587668699" Y="0.935166830245" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.744000731304" Y="1.923192021777" />
                  <Point X="3.043515314735" Y="1.209795338405" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.383181159211" Y="1.068990784309" />
                  <Point X="4.723722112308" Y="1.033198487858" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.888247125068" Y="2.033876215242" />
                  <Point X="3.013804867441" Y="1.308441318805" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.032493518833" Y="2.144560408707" />
                  <Point X="3.001161768615" Y="1.405293448597" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.176739912598" Y="2.255244602172" />
                  <Point X="3.017941659873" Y="1.499053097517" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.19360975369" Y="2.352540980478" />
                  <Point X="3.051201000115" Y="1.591080686554" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.141076673437" Y="2.44254281781" />
                  <Point X="3.113714392843" Y="1.680033550776" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.088543593184" Y="2.532544655142" />
                  <Point X="3.202192967089" Y="1.766257364451" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.036010512932" Y="2.622546492475" />
                  <Point X="3.311683796144" Y="1.850272701154" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.977687127353" Y="2.711939744194" />
                  <Point X="2.171503426708" Y="2.065633773507" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.617091706279" Y="2.018800558139" />
                  <Point X="3.421174625198" Y="1.934288037856" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.908988115775" Y="2.800242473677" />
                  <Point X="2.077100959245" Y="2.171079159215" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.759793655521" Y="2.099325265451" />
                  <Point X="3.530665366945" Y="2.018303383734" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.840289150933" Y="2.888545208072" />
                  <Point X="-3.390826889824" Y="2.841304820838" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.884448619393" Y="2.788082319969" />
                  <Point X="2.028010039297" Y="2.271762109373" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.89976384668" Y="2.180137092108" />
                  <Point X="3.640156088803" Y="2.102318731703" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.771590186091" Y="2.976847942468" />
                  <Point X="-3.593101022182" Y="2.958087975392" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.800589102488" Y="2.874791616133" />
                  <Point X="2.0143144015" Y="2.368724865469" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.039734037839" Y="2.260948918765" />
                  <Point X="3.74964681066" Y="2.186334079672" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.755851296115" Y="2.965612769766" />
                  <Point X="2.020705052021" Y="2.463576467591" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.179704228998" Y="2.341760745422" />
                  <Point X="3.859137532518" Y="2.270349427641" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.750209630841" Y="3.06054309341" />
                  <Point X="2.047207350519" Y="2.556314250334" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.319674420157" Y="2.422572572078" />
                  <Point X="3.968628254375" Y="2.35436477561" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.761129116638" Y="3.157214064173" />
                  <Point X="2.093252399333" Y="2.646998007249" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.459644611316" Y="2.503384398735" />
                  <Point X="4.078118976233" Y="2.438380123579" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.803797392734" Y="3.25722196726" />
                  <Point X="2.145247606527" Y="2.737056377318" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.599614802475" Y="2.584196225392" />
                  <Point X="4.10828527853" Y="2.530732804004" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.86251062274" Y="3.358916262959" />
                  <Point X="2.197242813722" Y="2.827114747387" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.739584993634" Y="2.665008052049" />
                  <Point X="4.046554032905" Y="2.632744305926" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.921223852747" Y="3.460610558657" />
                  <Point X="2.249238020916" Y="2.917173117455" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.879555180584" Y="2.745819879148" />
                  <Point X="3.972729834101" Y="2.736026828444" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.979937082753" Y="3.562304854355" />
                  <Point X="2.301233225364" Y="3.007231487813" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.038650312759" Y="3.663999150054" />
                  <Point X="2.35322842809" Y="3.097289858351" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.988515609098" Y="3.754253066924" />
                  <Point X="2.405223630817" Y="3.18734822889" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.878944731605" Y="3.838259990196" />
                  <Point X="2.457218833543" Y="3.277406599428" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.769373854112" Y="3.922266913468" />
                  <Point X="2.509214036269" Y="3.367464969967" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.65980297662" Y="4.00627383674" />
                  <Point X="2.561209238996" Y="3.457523340505" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.518871618346" Y="4.086984640663" />
                  <Point X="2.613204441722" Y="3.547581711044" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.374289058181" Y="4.167311687802" />
                  <Point X="-2.08369233001" Y="4.136768740917" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.637574402771" Y="4.089879857336" />
                  <Point X="2.665199644449" Y="3.637640081582" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.229706574379" Y="4.247638742968" />
                  <Point X="-2.172021110936" Y="4.241575756447" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.536268146863" Y="4.17475542734" />
                  <Point X="2.717194847175" Y="3.727698452121" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.495835967207" Y="4.266029120576" />
                  <Point X="-0.12019489346" Y="4.12144341752" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.084811919701" Y="4.099896333198" />
                  <Point X="2.769190049901" Y="3.817756822659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.46825801441" Y="4.358653847495" />
                  <Point X="-0.211107847233" Y="4.22652204056" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.197860411965" Y="4.183537744429" />
                  <Point X="2.807456423606" Y="3.909258151273" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.464938832497" Y="4.453828273977" />
                  <Point X="-0.242718568967" Y="4.325367747853" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.241396624641" Y="4.274485190648" />
                  <Point X="2.649837773739" Y="4.02134782549" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.477691167728" Y="4.550691884978" />
                  <Point X="-0.269055649036" Y="4.423659173071" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.266290899226" Y="4.367391983514" />
                  <Point X="2.455281267495" Y="4.137319824853" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.262061573101" Y="4.623551587893" />
                  <Point X="-0.295392718422" Y="4.521950597167" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.29118517381" Y="4.46029877638" />
                  <Point X="2.255231224908" Y="4.253869218152" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.014251915399" Y="4.693029029887" />
                  <Point X="-0.321729786757" Y="4.620242021152" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.316079448395" Y="4.553205569246" />
                  <Point X="1.993279311342" Y="4.376924760263" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.645317672334" Y="4.749775764965" />
                  <Point X="-0.348066855093" Y="4.718533445137" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.340973722979" Y="4.646112362111" />
                  <Point X="1.677478657486" Y="4.505640033041" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.365867997563" Y="4.739019154977" />
                  <Point X="1.175952783765" Y="4.653875813023" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="0.001626220703" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.566364807129" Y="-3.891006835938" />
                  <Point X="0.471539031982" Y="-3.537112304688" />
                  <Point X="0.464318664551" Y="-3.521544189453" />
                  <Point X="0.425561096191" Y="-3.465701660156" />
                  <Point X="0.300591033936" Y="-3.285643798828" />
                  <Point X="0.274335754395" Y="-3.266399902344" />
                  <Point X="0.214360549927" Y="-3.247785644531" />
                  <Point X="0.020976625443" Y="-3.187766601562" />
                  <Point X="-0.008664756775" Y="-3.187766601562" />
                  <Point X="-0.068640106201" Y="-3.206380615234" />
                  <Point X="-0.262023895264" Y="-3.266399902344" />
                  <Point X="-0.288279144287" Y="-3.285643798828" />
                  <Point X="-0.327036865234" Y="-3.341486083984" />
                  <Point X="-0.45200680542" Y="-3.521544189453" />
                  <Point X="-0.459227142334" Y="-3.537112304688" />
                  <Point X="-0.76498260498" Y="-4.678207519531" />
                  <Point X="-0.84774395752" Y="-4.987077148438" />
                  <Point X="-0.872937988281" Y="-4.982186523438" />
                  <Point X="-1.100246459961" Y="-4.938065429688" />
                  <Point X="-1.168784545898" Y="-4.920431152344" />
                  <Point X="-1.351589599609" Y="-4.873396972656" />
                  <Point X="-1.320565063477" Y="-4.637742675781" />
                  <Point X="-1.309150146484" Y="-4.551038574219" />
                  <Point X="-1.309683227539" Y="-4.5347578125" />
                  <Point X="-1.324011352539" Y="-4.462725585938" />
                  <Point X="-1.370210693359" Y="-4.230466308594" />
                  <Point X="-1.386282714844" Y="-4.20262890625" />
                  <Point X="-1.441500366211" Y="-4.154204101562" />
                  <Point X="-1.619543457031" Y="-3.998063964844" />
                  <Point X="-1.649240722656" Y="-3.985762939453" />
                  <Point X="-1.722526855469" Y="-3.980959472656" />
                  <Point X="-1.958829833984" Y="-3.965471435547" />
                  <Point X="-1.989878540039" Y="-3.973791015625" />
                  <Point X="-2.050944335938" Y="-4.014593994141" />
                  <Point X="-2.247844970703" Y="-4.146159179688" />
                  <Point X="-2.259734130859" Y="-4.157294433594" />
                  <Point X="-2.431392089844" Y="-4.381003417969" />
                  <Point X="-2.457095214844" Y="-4.414500488281" />
                  <Point X="-2.522645263672" Y="-4.373913574219" />
                  <Point X="-2.855831054688" Y="-4.16761328125" />
                  <Point X="-2.950740966797" Y="-4.094535644531" />
                  <Point X="-3.228581054688" Y="-3.880608154297" />
                  <Point X="-2.682256835938" Y="-2.934346679688" />
                  <Point X="-2.506033691406" Y="-2.629119628906" />
                  <Point X="-2.499762695312" Y="-2.597593017578" />
                  <Point X="-2.513979980469" Y="-2.568763916016" />
                  <Point X="-2.531328613281" Y="-2.551415283203" />
                  <Point X="-2.560157470703" Y="-2.537198730469" />
                  <Point X="-2.59168359375" Y="-2.543469726562" />
                  <Point X="-3.575855712891" Y="-3.111681884766" />
                  <Point X="-3.842959472656" Y="-3.265894042969" />
                  <Point X="-3.898466552734" Y="-3.192969238281" />
                  <Point X="-4.161703613281" Y="-2.847129394531" />
                  <Point X="-4.229743652344" Y="-2.733036376953" />
                  <Point X="-4.431020019531" Y="-2.395527099609" />
                  <Point X="-3.472119873047" Y="-1.659737182617" />
                  <Point X="-3.163786865234" Y="-1.423144897461" />
                  <Point X="-3.152535644531" Y="-1.411081542969" />
                  <Point X="-3.143998291016" Y="-1.388972045898" />
                  <Point X="-3.138117431641" Y="-1.366266479492" />
                  <Point X="-3.136651855469" Y="-1.350051269531" />
                  <Point X="-3.148656738281" Y="-1.321068603516" />
                  <Point X="-3.167427978516" Y="-1.30694934082" />
                  <Point X="-3.187641601562" Y="-1.295052734375" />
                  <Point X="-3.219529296875" Y="-1.288571044922" />
                  <Point X="-4.461953125" Y="-1.452139282227" />
                  <Point X="-4.803283203125" Y="-1.497076293945" />
                  <Point X="-4.824439453125" Y="-1.414251342773" />
                  <Point X="-4.927393066406" Y="-1.011191650391" />
                  <Point X="-4.94539453125" Y="-0.885325927734" />
                  <Point X="-4.998396484375" Y="-0.514742126465" />
                  <Point X="-3.908701660156" Y="-0.22275932312" />
                  <Point X="-3.557463867188" Y="-0.128645431519" />
                  <Point X="-3.535326171875" Y="-0.116865394592" />
                  <Point X="-3.514142822266" Y="-0.102163047791" />
                  <Point X="-3.50232421875" Y="-0.090645301819" />
                  <Point X="-3.492708984375" Y="-0.068851707458" />
                  <Point X="-3.485647949219" Y="-0.046100738525" />
                  <Point X="-3.483400878906" Y="-0.031280042648" />
                  <Point X="-3.487837890625" Y="-0.009403440475" />
                  <Point X="-3.494898925781" Y="0.013347529411" />
                  <Point X="-3.50232421875" Y="0.028085111618" />
                  <Point X="-3.520712646484" Y="0.04416248703" />
                  <Point X="-3.541895751953" Y="0.058864833832" />
                  <Point X="-3.557463867188" Y="0.066085334778" />
                  <Point X="-4.6899921875" Y="0.369545349121" />
                  <Point X="-4.998186523438" Y="0.452125793457" />
                  <Point X="-4.983997558594" Y="0.548014709473" />
                  <Point X="-4.917645019531" Y="0.996418151855" />
                  <Point X="-4.881407226562" Y="1.130146728516" />
                  <Point X="-4.773516113281" Y="1.528298583984" />
                  <Point X="-4.005042236328" Y="1.427127197266" />
                  <Point X="-3.753266357422" Y="1.393980102539" />
                  <Point X="-3.731704833984" Y="1.395866577148" />
                  <Point X="-3.7171640625" Y="1.400451293945" />
                  <Point X="-3.670278808594" Y="1.415234130859" />
                  <Point X="-3.651534423828" Y="1.426056152344" />
                  <Point X="-3.639120117188" Y="1.443785766602" />
                  <Point X="-3.633285644531" Y="1.457871704102" />
                  <Point X="-3.61447265625" Y="1.503290039063" />
                  <Point X="-3.610714111328" Y="1.524605224609" />
                  <Point X="-3.616315917969" Y="1.54551159668" />
                  <Point X="-3.623355957031" Y="1.559035522461" />
                  <Point X="-3.646055664062" Y="1.602641235352" />
                  <Point X="-3.659968017578" Y="1.619221679688" />
                  <Point X="-4.309589355469" Y="2.117693359375" />
                  <Point X="-4.47610546875" Y="2.245466064453" />
                  <Point X="-4.4178359375" Y="2.345296142578" />
                  <Point X="-4.160014648438" Y="2.787006591797" />
                  <Point X="-4.0640234375" Y="2.910389648438" />
                  <Point X="-3.774671142578" Y="3.282310791016" />
                  <Point X="-3.313790771484" Y="3.016221191406" />
                  <Point X="-3.159156982422" Y="2.926943359375" />
                  <Point X="-3.138514892578" Y="2.920434814453" />
                  <Point X="-3.118263671875" Y="2.918663085938" />
                  <Point X="-3.052965576172" Y="2.912950195312" />
                  <Point X="-3.031506835938" Y="2.915775390625" />
                  <Point X="-3.013252441406" Y="2.927404541016" />
                  <Point X="-2.998877929688" Y="2.941779052734" />
                  <Point X="-2.952528808594" Y="2.988127929688" />
                  <Point X="-2.940899414062" Y="3.006382324219" />
                  <Point X="-2.93807421875" Y="3.027841308594" />
                  <Point X="-2.939845947266" Y="3.048092773438" />
                  <Point X="-2.945558837891" Y="3.113390869141" />
                  <Point X="-2.952067382812" Y="3.134032958984" />
                  <Point X="-3.239934082031" Y="3.632632568359" />
                  <Point X="-3.307278564453" Y="3.749276611328" />
                  <Point X="-3.201919921875" Y="3.830054199219" />
                  <Point X="-2.752876220703" Y="4.174331542969" />
                  <Point X="-2.601693115234" Y="4.258325195312" />
                  <Point X="-2.141548583984" Y="4.513972167969" />
                  <Point X="-2.015165039062" Y="4.349265625" />
                  <Point X="-1.967826904297" Y="4.287573242188" />
                  <Point X="-1.951246826172" Y="4.273660644531" />
                  <Point X="-1.928707275391" Y="4.261927246094" />
                  <Point X="-1.856030761719" Y="4.22409375" />
                  <Point X="-1.835124145508" Y="4.2184921875" />
                  <Point X="-1.813808837891" Y="4.222250488281" />
                  <Point X="-1.790332275391" Y="4.231975097656" />
                  <Point X="-1.714634887695" Y="4.263330078125" />
                  <Point X="-1.696905273438" Y="4.275744140625" />
                  <Point X="-1.686083496094" Y="4.29448828125" />
                  <Point X="-1.678442382813" Y="4.318723144531" />
                  <Point X="-1.653804077148" Y="4.396864746094" />
                  <Point X="-1.651917602539" Y="4.418426757812" />
                  <Point X="-1.684644897461" Y="4.667014648438" />
                  <Point X="-1.689137695313" Y="4.701141113281" />
                  <Point X="-1.549409057617" Y="4.740315917969" />
                  <Point X="-0.968094177246" Y="4.903296386719" />
                  <Point X="-0.78481640625" Y="4.92474609375" />
                  <Point X="-0.22419960022" Y="4.990358398438" />
                  <Point X="-0.087885253906" Y="4.481625976562" />
                  <Point X="-0.042140380859" Y="4.310903320312" />
                  <Point X="-0.024282119751" Y="4.284176757813" />
                  <Point X="0.006155934334" Y="4.273844238281" />
                  <Point X="0.036594039917" Y="4.284176757813" />
                  <Point X="0.054452251434" Y="4.310903320312" />
                  <Point X="0.201951828003" Y="4.861379394531" />
                  <Point X="0.236648422241" Y="4.990868652344" />
                  <Point X="0.352633056641" Y="4.978722167969" />
                  <Point X="0.860205810547" Y="4.925565429688" />
                  <Point X="1.01183770752" Y="4.88895703125" />
                  <Point X="1.508455932617" Y="4.769058105469" />
                  <Point X="1.606994140625" Y="4.733317382812" />
                  <Point X="1.931033691406" Y="4.615786132813" />
                  <Point X="2.026470947266" Y="4.571153320312" />
                  <Point X="2.33869921875" Y="4.425134277344" />
                  <Point X="2.430900878906" Y="4.371417480469" />
                  <Point X="2.732523681641" Y="4.19569140625" />
                  <Point X="2.819477294922" Y="4.133854980469" />
                  <Point X="3.068740234375" Y="3.956592773438" />
                  <Point X="2.433818359375" Y="2.856875488281" />
                  <Point X="2.229853515625" Y="2.503597900391" />
                  <Point X="2.219769775391" Y="2.472509277344" />
                  <Point X="2.203382324219" Y="2.411228271484" />
                  <Point X="2.204026367188" Y="2.375891845703" />
                  <Point X="2.210416015625" Y="2.322901123047" />
                  <Point X="2.218682128906" Y="2.3008125" />
                  <Point X="2.228851074219" Y="2.285826171875" />
                  <Point X="2.261639892578" Y="2.23750390625" />
                  <Point X="2.289926269531" Y="2.214034667969" />
                  <Point X="2.338248535156" Y="2.18124609375" />
                  <Point X="2.360336914062" Y="2.172980224609" />
                  <Point X="2.376771240234" Y="2.170998535156" />
                  <Point X="2.42976171875" Y="2.164608642578" />
                  <Point X="2.467669677734" Y="2.171028808594" />
                  <Point X="2.528950683594" Y="2.187416015625" />
                  <Point X="2.541033935547" Y="2.192417480469" />
                  <Point X="3.680139404297" Y="2.850080322266" />
                  <Point X="3.994248046875" Y="3.031430908203" />
                  <Point X="4.023679443359" Y="2.990527832031" />
                  <Point X="4.202591796875" Y="2.741880126953" />
                  <Point X="4.251067382812" Y="2.661773925781" />
                  <Point X="4.387512695312" Y="2.436296142578" />
                  <Point X="3.557155029297" Y="1.799139892578" />
                  <Point X="3.288616210938" Y="1.593082641602" />
                  <Point X="3.279371337891" Y="1.583833374023" />
                  <Point X="3.265693115234" Y="1.565988891602" />
                  <Point X="3.221589111328" Y="1.508451904297" />
                  <Point X="3.208024414063" Y="1.473280883789" />
                  <Point X="3.191595703125" Y="1.414535522461" />
                  <Point X="3.190779541016" Y="1.390965332031" />
                  <Point X="3.194962158203" Y="1.370694213867" />
                  <Point X="3.208448486328" Y="1.305332519531" />
                  <Point X="3.215646484375" Y="1.287954956055" />
                  <Point X="3.227022705078" Y="1.270663574219" />
                  <Point X="3.263704101562" Y="1.214909667969" />
                  <Point X="3.280947998047" Y="1.198819824219" />
                  <Point X="3.29743359375" Y="1.189539794922" />
                  <Point X="3.350590087891" Y="1.159617431641" />
                  <Point X="3.368565429688" Y="1.153619628906" />
                  <Point X="3.390855224609" Y="1.150673583984" />
                  <Point X="3.462726074219" Y="1.141175048828" />
                  <Point X="3.475803710938" Y="1.14117175293" />
                  <Point X="4.557877929688" Y="1.283629516602" />
                  <Point X="4.848975097656" Y="1.32195324707" />
                  <Point X="4.862346679688" Y="1.267027832031" />
                  <Point X="4.939188476562" Y="0.951386108398" />
                  <Point X="4.954464355469" Y="0.853269714355" />
                  <Point X="4.997858398438" Y="0.574556152344" />
                  <Point X="4.048919189453" Y="0.320288696289" />
                  <Point X="3.741167724609" Y="0.237826919556" />
                  <Point X="3.729086914062" Y="0.232819412231" />
                  <Point X="3.707187988281" Y="0.220161346436" />
                  <Point X="3.636576904297" Y="0.179346878052" />
                  <Point X="3.622264892578" Y="0.166926574707" />
                  <Point X="3.609125488281" Y="0.150183898926" />
                  <Point X="3.566759033203" Y="0.096198890686" />
                  <Point X="3.556985107422" Y="0.074735275269" />
                  <Point X="3.552605224609" Y="0.051865505219" />
                  <Point X="3.538482910156" Y="-0.021875303268" />
                  <Point X="3.538482910156" Y="-0.04068478775" />
                  <Point X="3.542862792969" Y="-0.063554409027" />
                  <Point X="3.556985107422" Y="-0.137295227051" />
                  <Point X="3.566759033203" Y="-0.158758987427" />
                  <Point X="3.5798984375" Y="-0.175501663208" />
                  <Point X="3.622264892578" Y="-0.229486679077" />
                  <Point X="3.636576904297" Y="-0.241906967163" />
                  <Point X="3.658476074219" Y="-0.2545650177" />
                  <Point X="3.729086914062" Y="-0.295379333496" />
                  <Point X="3.741167724609" Y="-0.300387023926" />
                  <Point X="4.733479003906" Y="-0.566275817871" />
                  <Point X="4.998067871094" Y="-0.637172363281" />
                  <Point X="4.991336914062" Y="-0.681819396973" />
                  <Point X="4.948431640625" Y="-0.966399108887" />
                  <Point X="4.928860351562" Y="-1.052163208008" />
                  <Point X="4.874545410156" Y="-1.290178344727" />
                  <Point X="3.768887207031" Y="-1.144615600586" />
                  <Point X="3.411982177734" Y="-1.097628295898" />
                  <Point X="3.394836181641" Y="-1.098341186523" />
                  <Point X="3.351856201172" Y="-1.107683105469" />
                  <Point X="3.213271972656" Y="-1.137804931641" />
                  <Point X="3.1854453125" Y="-1.154697265625" />
                  <Point X="3.159466552734" Y="-1.18594152832" />
                  <Point X="3.075701171875" Y="-1.286685424805" />
                  <Point X="3.064357910156" Y="-1.314070556641" />
                  <Point X="3.060634521484" Y="-1.354533325195" />
                  <Point X="3.04862890625" Y="-1.485000854492" />
                  <Point X="3.056360595703" Y="-1.516621948242" />
                  <Point X="3.080146240234" Y="-1.553619262695" />
                  <Point X="3.156840820312" Y="-1.672912475586" />
                  <Point X="3.168460693359" Y="-1.685540649414" />
                  <Point X="4.089331542969" Y="-2.392149658203" />
                  <Point X="4.33907421875" Y="-2.583784179688" />
                  <Point X="4.325077636719" Y="-2.606432861328" />
                  <Point X="4.204131835938" Y="-2.802141845703" />
                  <Point X="4.16365625" Y="-2.859651367188" />
                  <Point X="4.056688476562" Y="-3.011638427734" />
                  <Point X="3.071326660156" Y="-2.442739257812" />
                  <Point X="2.753454589844" Y="-2.259215576172" />
                  <Point X="2.737340820313" Y="-2.253312744141" />
                  <Point X="2.686187744141" Y="-2.244074707031" />
                  <Point X="2.521250488281" Y="-2.214287109375" />
                  <Point X="2.489077636719" Y="-2.219244873047" />
                  <Point X="2.44658203125" Y="-2.241609863281" />
                  <Point X="2.309559570312" Y="-2.313723876953" />
                  <Point X="2.288599853516" Y="-2.334683837891" />
                  <Point X="2.266234619141" Y="-2.377179443359" />
                  <Point X="2.194120849609" Y="-2.514201904297" />
                  <Point X="2.189163085938" Y="-2.546374511719" />
                  <Point X="2.198401123047" Y="-2.597527587891" />
                  <Point X="2.228188720703" Y="-2.76246484375" />
                  <Point X="2.234091552734" Y="-2.778578613281" />
                  <Point X="2.82584375" Y="-3.803522949219" />
                  <Point X="2.986673583984" Y="-4.082088623047" />
                  <Point X="2.978810058594" Y="-4.087705322266" />
                  <Point X="2.835296630859" Y="-4.190213378906" />
                  <Point X="2.790046142578" Y="-4.219503417969" />
                  <Point X="2.679775878906" Y="-4.290879394531" />
                  <Point X="1.925743286133" Y="-3.308205078125" />
                  <Point X="1.683177490234" Y="-2.992087402344" />
                  <Point X="1.670549316406" Y="-2.980467529297" />
                  <Point X="1.620098632812" Y="-2.948032226562" />
                  <Point X="1.45742590332" Y="-2.843448730469" />
                  <Point X="1.425804931641" Y="-2.835717041016" />
                  <Point X="1.370628540039" Y="-2.840794433594" />
                  <Point X="1.192718017578" Y="-2.857166015625" />
                  <Point X="1.165332885742" Y="-2.868509277344" />
                  <Point X="1.122727050781" Y="-2.903934570312" />
                  <Point X="0.985349060059" Y="-3.018159667969" />
                  <Point X="0.968456726074" Y="-3.045986328125" />
                  <Point X="0.955717895508" Y="-3.104595458984" />
                  <Point X="0.91464251709" Y="-3.29357421875" />
                  <Point X="0.91392956543" Y="-3.310720214844" />
                  <Point X="1.081627441406" Y="-4.584512695312" />
                  <Point X="1.127642211914" Y="-4.934028320312" />
                  <Point X="0.994346374512" Y="-4.963246582031" />
                  <Point X="0.952538513184" Y="-4.970841796875" />
                  <Point X="0.860200378418" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#140" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.045744944593" Y="4.525846939695" Z="0.6" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.6" />
                  <Point X="-0.796784583478" Y="5.005687487782" Z="0.6" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.6" />
                  <Point X="-1.569062786887" Y="4.819739703738" Z="0.6" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.6" />
                  <Point X="-1.740373088827" Y="4.691768543442" Z="0.6" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.6" />
                  <Point X="-1.732283985967" Y="4.365038704621" Z="0.6" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.6" />
                  <Point X="-1.815622555934" Y="4.309448942664" Z="0.6" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.6" />
                  <Point X="-1.911775591075" Y="4.337557860966" Z="0.6" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.6" />
                  <Point X="-1.98165327218" Y="4.410983523897" Z="0.6" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.6" />
                  <Point X="-2.632132117099" Y="4.333312995704" Z="0.6" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.6" />
                  <Point X="-3.238498003977" Y="3.901014203538" Z="0.6" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.6" />
                  <Point X="-3.289391418594" Y="3.638912686505" Z="0.6" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.6" />
                  <Point X="-2.995811788059" Y="3.075014839457" Z="0.6" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.6" />
                  <Point X="-3.040389049484" Y="3.008414518402" Z="0.6" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.6" />
                  <Point X="-3.12006162615" Y="2.999752911114" Z="0.6" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.6" />
                  <Point X="-3.294946661792" Y="3.090802530152" Z="0.6" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.6" />
                  <Point X="-4.109642638554" Y="2.972372117175" Z="0.6" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.6" />
                  <Point X="-4.46717423343" Y="2.401781179442" Z="0.6" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.6" />
                  <Point X="-4.346183281299" Y="2.109305784195" Z="0.6" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.6" />
                  <Point X="-3.67386246651" Y="1.567228160404" Z="0.6" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.6" />
                  <Point X="-3.685635345915" Y="1.508285989684" Z="0.6" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.6" />
                  <Point X="-3.738355240003" Y="1.479417236516" Z="0.6" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.6" />
                  <Point X="-4.004671927293" Y="1.507979465187" Z="0.6" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.6" />
                  <Point X="-4.935823105899" Y="1.174504172982" Z="0.6" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.6" />
                  <Point X="-5.039614891863" Y="0.58661383404" Z="0.6" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.6" />
                  <Point X="-4.709089184167" Y="0.352529119912" Z="0.6" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.6" />
                  <Point X="-3.555377533504" Y="0.034366680047" Z="0.6" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.6" />
                  <Point X="-3.541746670701" Y="0.007055915643" Z="0.6" />
                  <Point X="-3.539556741714" Y="0" Z="0.6" />
                  <Point X="-3.546617930434" Y="-0.022751035418" Z="0.6" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.6" />
                  <Point X="-3.569991061619" Y="-0.044509303087" Z="0.6" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.6" />
                  <Point X="-3.927798604606" Y="-0.143182936844" Z="0.6" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.6" />
                  <Point X="-5.001046697734" Y="-0.861125016538" Z="0.6" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.6" />
                  <Point X="-4.879040117186" Y="-1.395345540679" Z="0.6" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.6" />
                  <Point X="-4.461582886387" Y="-1.470431499045" Z="0.6" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.6" />
                  <Point X="-3.198945275209" Y="-1.318760160742" Z="0.6" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.6" />
                  <Point X="-3.198556990103" Y="-1.345155359404" Z="0.6" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.6" />
                  <Point X="-3.508713908788" Y="-1.588789474277" Z="0.6" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.6" />
                  <Point X="-4.278843160736" Y="-2.727365846712" Z="0.6" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.6" />
                  <Point X="-3.944408347177" Y="-3.1919722848" Z="0.6" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.6" />
                  <Point X="-3.557011647708" Y="-3.123703017983" Z="0.6" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.6" />
                  <Point X="-2.559597722396" Y="-2.568732746736" Z="0.6" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.6" />
                  <Point X="-2.731713995185" Y="-2.878066734784" Z="0.6" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.6" />
                  <Point X="-2.987400836144" Y="-4.102872883653" Z="0.6" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.6" />
                  <Point X="-2.555122731483" Y="-4.385144269316" Z="0.6" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.6" />
                  <Point X="-2.39788038955" Y="-4.380161308895" Z="0.6" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.6" />
                  <Point X="-2.029321897291" Y="-4.024887358065" Z="0.6" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.6" />
                  <Point X="-1.73195293951" Y="-3.999572505238" Z="0.6" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.6" />
                  <Point X="-1.480623603328" Y="-4.160515883274" Z="0.6" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.6" />
                  <Point X="-1.379206620598" Y="-4.441200149121" Z="0.6" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.6" />
                  <Point X="-1.376293318593" Y="-4.599936081094" Z="0.6" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.6" />
                  <Point X="-1.187399423128" Y="-4.937573802288" Z="0.6" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.6" />
                  <Point X="-0.888621198187" Y="-4.999990700724" Z="0.6" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.6" />
                  <Point X="-0.722842284261" Y="-4.659868236804" Z="0.6" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.6" />
                  <Point X="-0.292116778606" Y="-3.338715132313" Z="0.6" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.6" />
                  <Point X="-0.059975281863" Y="-3.222853482634" Z="0.6" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.6" />
                  <Point X="0.193383797498" Y="-3.264258562654" Z="0.6" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.6" />
                  <Point X="0.378329080827" Y="-3.462930623455" Z="0.6" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.6" />
                  <Point X="0.511912576141" Y="-3.872667793011" Z="0.6" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.6" />
                  <Point X="0.955319495944" Y="-4.988756712448" Z="0.6" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.6" />
                  <Point X="1.134669874037" Y="-4.951045646084" Z="0.6" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.6" />
                  <Point X="1.125043783872" Y="-4.546706316799" Z="0.6" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.6" />
                  <Point X="0.998421085363" Y="-3.083935087217" Z="0.6" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.6" />
                  <Point X="1.148537915705" Y="-2.91110079488" Z="0.6" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.6" />
                  <Point X="1.369054060549" Y="-2.859303971334" Z="0.6" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.6" />
                  <Point X="1.586903214758" Y="-2.958810142491" Z="0.6" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.6" />
                  <Point X="1.879919658255" Y="-3.307363059802" Z="0.6" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.6" />
                  <Point X="2.81105937212" Y="-4.230197569533" Z="0.6" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.6" />
                  <Point X="3.001715851659" Y="-4.097112251915" Z="0.6" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.6" />
                  <Point X="2.862988969318" Y="-3.747242848765" Z="0.6" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.6" />
                  <Point X="2.241449315031" Y="-2.557361213372" Z="0.6" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.6" />
                  <Point X="2.304325063252" Y="-2.369185779704" Z="0.6" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.6" />
                  <Point X="2.463712598668" Y="-2.254576246715" Z="0.6" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.6" />
                  <Point X="2.67114559014" Y="-2.26199869446" Z="0.6" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.6" />
                  <Point X="3.040170748342" Y="-2.454760530956" Z="0.6" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.6" />
                  <Point X="4.198389134189" Y="-2.857148380933" Z="0.6" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.6" />
                  <Point X="4.361454874305" Y="-2.601437989508" Z="0.6" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.6" />
                  <Point X="4.113613405915" Y="-2.321201973159" Z="0.6" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.6" />
                  <Point X="3.116048255206" Y="-1.495299666792" Z="0.6" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.6" />
                  <Point X="3.104268092336" Y="-1.327834925502" Z="0.6" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.6" />
                  <Point X="3.191756859995" Y="-1.186628456608" Z="0.6" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.6" />
                  <Point X="3.356319865131" Y="-1.12526230415" Z="0.6" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.6" />
                  <Point X="3.75620495042" Y="-1.16290786747" Z="0.6" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.6" />
                  <Point X="4.971451688861" Y="-1.032007228156" Z="0.6" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.6" />
                  <Point X="5.034622247469" Y="-0.657993373377" Z="0.6" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.6" />
                  <Point X="4.740263723245" Y="-0.486699562078" Z="0.6" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.6" />
                  <Point X="3.677341722367" Y="-0.179996226331" Z="0.6" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.6" />
                  <Point X="3.613076128315" Y="-0.113353220375" Z="0.6" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.6" />
                  <Point X="3.58581452825" Y="-0.022869690964" Z="0.6" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.6" />
                  <Point X="3.595556922174" Y="0.073740840268" Z="0.6" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.6" />
                  <Point X="3.642303310086" Y="0.1505955182" Z="0.6" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.6" />
                  <Point X="3.726053691985" Y="0.208152617911" Z="0.6" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.6" />
                  <Point X="4.055704094061" Y="0.30327237069" Z="0.6" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.6" />
                  <Point X="4.997713292515" Y="0.892241707398" Z="0.6" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.6" />
                  <Point X="4.904771740163" Y="1.31013474821" Z="0.6" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.6" />
                  <Point X="4.545195356283" Y="1.364481864039" Z="0.6" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.6" />
                  <Point X="3.391250407235" Y="1.231522712239" Z="0.6" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.6" />
                  <Point X="3.315804255256" Y="1.26439100797" Z="0.6" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.6" />
                  <Point X="3.262637203432" Y="1.329425068505" Z="0.6" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.6" />
                  <Point X="3.237774638594" Y="1.412078167591" Z="0.6" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.6" />
                  <Point X="3.250020825593" Y="1.491094630085" Z="0.6" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.6" />
                  <Point X="3.299219935494" Y="1.566850780321" Z="0.6" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.6" />
                  <Point X="3.581437114599" Y="1.790752281626" Z="0.6" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.6" />
                  <Point X="4.287688831957" Y="2.718940097594" Z="0.6" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.6" />
                  <Point X="4.058108922492" Y="3.051010866978" Z="0.6" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.6" />
                  <Point X="3.648983490483" Y="2.924661638005" Z="0.6" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.6" />
                  <Point X="2.44859705846" Y="2.25061146709" Z="0.6" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.6" />
                  <Point X="2.376601075812" Y="2.251918857252" Z="0.6" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.6" />
                  <Point X="2.311844541624" Y="2.286689141941" Z="0.6" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.6" />
                  <Point X="2.264069458294" Y="2.345180318758" Z="0.6" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.6" />
                  <Point X="2.247510845507" Y="2.41315736802" Z="0.6" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.6" />
                  <Point X="2.261916475567" Y="2.490872517422" Z="0.6" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.6" />
                  <Point X="2.470963714854" Y="2.863155591472" Z="0.6" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.6" />
                  <Point X="2.842298494292" Y="4.205881881344" Z="0.6" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.6" />
                  <Point X="2.449914766763" Y="4.445900202318" Z="0.6" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.6" />
                  <Point X="2.041496510848" Y="4.647725197997" Z="0.6" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.6" />
                  <Point X="1.617886738469" Y="4.811601465918" Z="0.6" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.6" />
                  <Point X="1.017416207602" Y="4.968840584994" Z="0.6" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.6" />
                  <Point X="0.351684914208" Y="5.059730095877" Z="0.6" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.6" />
                  <Point X="0.14749961696" Y="4.905600566256" Z="0.6" />
                  <Point X="0" Y="4.355124473572" Z="0.6" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>