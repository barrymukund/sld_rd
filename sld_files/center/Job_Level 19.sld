<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#153" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1476" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="0" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="0.004715820312" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.705440429688" Y="-4.042992675781" />
                  <Point X="0.563302062988" Y="-3.512524414063" />
                  <Point X="0.557721191406" Y="-3.497141845703" />
                  <Point X="0.54236315918" Y="-3.467377197266" />
                  <Point X="0.484267669678" Y="-3.383672363281" />
                  <Point X="0.378635528564" Y="-3.231476806641" />
                  <Point X="0.356751586914" Y="-3.209021484375" />
                  <Point X="0.330496398926" Y="-3.189777587891" />
                  <Point X="0.302495330811" Y="-3.175669433594" />
                  <Point X="0.212595794678" Y="-3.147767822266" />
                  <Point X="0.049136188507" Y="-3.097036132812" />
                  <Point X="0.020976638794" Y="-3.092766601562" />
                  <Point X="-0.00866468811" Y="-3.092766601562" />
                  <Point X="-0.036824085236" Y="-3.097035888672" />
                  <Point X="-0.126723625183" Y="-3.124937255859" />
                  <Point X="-0.290183227539" Y="-3.175669189453" />
                  <Point X="-0.31818460083" Y="-3.189777587891" />
                  <Point X="-0.344439788818" Y="-3.209021484375" />
                  <Point X="-0.366323425293" Y="-3.2314765625" />
                  <Point X="-0.424419219971" Y="-3.315181152344" />
                  <Point X="-0.530051086426" Y="-3.467376953125" />
                  <Point X="-0.538189025879" Y="-3.481573730469" />
                  <Point X="-0.550990112305" Y="-3.512524414063" />
                  <Point X="-0.809432861328" Y="-4.477045898438" />
                  <Point X="-0.916584655762" Y="-4.87694140625" />
                  <Point X="-1.079340820313" Y="-4.845350585938" />
                  <Point X="-1.180037597656" Y="-4.819441894531" />
                  <Point X="-1.24641796875" Y="-4.802362792969" />
                  <Point X="-1.232073120117" Y="-4.693402832031" />
                  <Point X="-1.214963012695" Y="-4.563438476562" />
                  <Point X="-1.214201171875" Y="-4.5479296875" />
                  <Point X="-1.216508666992" Y="-4.516224121094" />
                  <Point X="-1.237985595703" Y="-4.408251953125" />
                  <Point X="-1.277036132812" Y="-4.211932617188" />
                  <Point X="-1.287938354492" Y="-4.182966308594" />
                  <Point X="-1.304010375977" Y="-4.15512890625" />
                  <Point X="-1.323644775391" Y="-4.131204101562" />
                  <Point X="-1.406412963867" Y="-4.058618164062" />
                  <Point X="-1.556905517578" Y="-3.926639160156" />
                  <Point X="-1.583188476562" Y="-3.910295410156" />
                  <Point X="-1.612885742188" Y="-3.897994384766" />
                  <Point X="-1.64302734375" Y="-3.890966308594" />
                  <Point X="-1.752879150391" Y="-3.883766113281" />
                  <Point X="-1.952616577148" Y="-3.870674804688" />
                  <Point X="-1.98341796875" Y="-3.873708496094" />
                  <Point X="-2.014466674805" Y="-3.882028076172" />
                  <Point X="-2.042657836914" Y="-3.894801513672" />
                  <Point X="-2.134192138672" Y="-3.955962890625" />
                  <Point X="-2.300624267578" Y="-4.067169677734" />
                  <Point X="-2.312785644531" Y="-4.076821533203" />
                  <Point X="-2.335102783203" Y="-4.099461914062" />
                  <Point X="-2.480148925781" Y="-4.288489746094" />
                  <Point X="-2.524526611328" Y="-4.261012207031" />
                  <Point X="-2.801706787109" Y="-4.089389404297" />
                  <Point X="-2.941144042969" Y="-3.98202734375" />
                  <Point X="-3.104721923828" Y="-3.856077880859" />
                  <Point X="-2.687909667969" Y="-3.134137939453" />
                  <Point X="-2.423761230469" Y="-2.676619628906" />
                  <Point X="-2.412859130859" Y="-2.647653320312" />
                  <Point X="-2.406588134766" Y="-2.616126953125" />
                  <Point X="-2.405575439453" Y="-2.585192871094" />
                  <Point X="-2.414560058594" Y="-2.555575195312" />
                  <Point X="-2.428777099609" Y="-2.526746337891" />
                  <Point X="-2.4468046875" Y="-2.501589355469" />
                  <Point X="-2.464153320312" Y="-2.484240722656" />
                  <Point X="-2.489310302734" Y="-2.466213134766" />
                  <Point X="-2.518139160156" Y="-2.45199609375" />
                  <Point X="-2.547756835938" Y="-2.443011474609" />
                  <Point X="-2.578690917969" Y="-2.444024169922" />
                  <Point X="-2.610217285156" Y="-2.450295166016" />
                  <Point X="-2.63918359375" Y="-2.461197265625" />
                  <Point X="-3.471064453125" Y="-2.941483886719" />
                  <Point X="-3.818024414062" Y="-3.141801269531" />
                  <Point X="-3.863870849609" Y="-3.081568115234" />
                  <Point X="-4.082859863281" Y="-2.793861572266" />
                  <Point X="-4.182821777344" Y="-2.626240234375" />
                  <Point X="-4.306142578125" Y="-2.419450439453" />
                  <Point X="-3.568128662109" Y="-1.853152099609" />
                  <Point X="-3.105954589844" Y="-1.498513549805" />
                  <Point X="-3.084576904297" Y="-1.475592895508" />
                  <Point X="-3.066612060547" Y="-1.448461425781" />
                  <Point X="-3.053856445312" Y="-1.419832641602" />
                  <Point X="-3.046151855469" Y="-1.390085327148" />
                  <Point X="-3.04334765625" Y="-1.359655883789" />
                  <Point X="-3.045556640625" Y="-1.327985473633" />
                  <Point X="-3.052557861328" Y="-1.298240600586" />
                  <Point X="-3.068640136719" Y="-1.272257202148" />
                  <Point X="-3.08947265625" Y="-1.24830078125" />
                  <Point X="-3.11297265625" Y="-1.228766723633" />
                  <Point X="-3.139455322266" Y="-1.213180297852" />
                  <Point X="-3.168718261719" Y="-1.201956420898" />
                  <Point X="-3.200605957031" Y="-1.195474853516" />
                  <Point X="-3.231929199219" Y="-1.194383789062" />
                  <Point X="-4.282100097656" Y="-1.332641357422" />
                  <Point X="-4.732102050781" Y="-1.391885375977" />
                  <Point X="-4.748428710938" Y="-1.327966064453" />
                  <Point X="-4.834077636719" Y="-0.992654602051" />
                  <Point X="-4.860524902344" Y="-0.807738708496" />
                  <Point X="-4.892424316406" Y="-0.584698181152" />
                  <Point X="-4.059361816406" Y="-0.361479766846" />
                  <Point X="-3.532875976562" Y="-0.220408340454" />
                  <Point X="-3.511489501953" Y="-0.211779968262" />
                  <Point X="-3.486073730469" Y="-0.19772479248" />
                  <Point X="-3.477880859375" Y="-0.192634490967" />
                  <Point X="-3.459975585938" Y="-0.180207107544" />
                  <Point X="-3.436021240234" Y="-0.158681411743" />
                  <Point X="-3.414829833984" Y="-0.127655303955" />
                  <Point X="-3.404122070312" Y="-0.102341514587" />
                  <Point X="-3.400885986328" Y="-0.093491073608" />
                  <Point X="-3.394917480469" Y="-0.074260597229" />
                  <Point X="-3.389474365234" Y="-0.045520751953" />
                  <Point X="-3.390575439453" Y="-0.011069944382" />
                  <Point X="-3.396104980469" Y="0.014327323914" />
                  <Point X="-3.398199951172" Y="0.022276891708" />
                  <Point X="-3.404168457031" Y="0.041507522583" />
                  <Point X="-3.417484375" Y="0.070831001282" />
                  <Point X="-3.440922119141" Y="0.100575088501" />
                  <Point X="-3.462588378906" Y="0.118927307129" />
                  <Point X="-3.469822753906" Y="0.124481468201" />
                  <Point X="-3.487728027344" Y="0.136909011841" />
                  <Point X="-3.501925292969" Y="0.145047103882" />
                  <Point X="-3.532875976562" Y="0.157848342896" />
                  <Point X="-4.49015625" Y="0.414350769043" />
                  <Point X="-4.89181640625" Y="0.521975158691" />
                  <Point X="-4.879686523438" Y="0.603946105957" />
                  <Point X="-4.824487792969" Y="0.976975280762" />
                  <Point X="-4.771249511719" Y="1.173440307617" />
                  <Point X="-4.703551269531" Y="1.423267822266" />
                  <Point X="-4.143064453125" Y="1.349478393555" />
                  <Point X="-3.765666259766" Y="1.29979284668" />
                  <Point X="-3.744985839844" Y="1.299341674805" />
                  <Point X="-3.723424072266" Y="1.301228271484" />
                  <Point X="-3.703137695312" Y="1.305263671875" />
                  <Point X="-3.681341796875" Y="1.312135742188" />
                  <Point X="-3.641711669922" Y="1.324631225586" />
                  <Point X="-3.622778564453" Y="1.332961669922" />
                  <Point X="-3.604034423828" Y="1.343783569336" />
                  <Point X="-3.587353271484" Y="1.356014892578" />
                  <Point X="-3.57371484375" Y="1.37156652832" />
                  <Point X="-3.561300537109" Y="1.389296264648" />
                  <Point X="-3.551351318359" Y="1.407431274414" />
                  <Point X="-3.542605712891" Y="1.428545410156" />
                  <Point X="-3.526703857422" Y="1.466935546875" />
                  <Point X="-3.520916015625" Y="1.48679296875" />
                  <Point X="-3.517157470703" Y="1.508108154297" />
                  <Point X="-3.515804443359" Y="1.528748657227" />
                  <Point X="-3.518951171875" Y="1.549192749023" />
                  <Point X="-3.524552978516" Y="1.570099121094" />
                  <Point X="-3.532049804688" Y="1.589377441406" />
                  <Point X="-3.542602294922" Y="1.609648803711" />
                  <Point X="-3.561789550781" Y="1.646507080078" />
                  <Point X="-3.573281738281" Y="1.663706665039" />
                  <Point X="-3.587194335938" Y="1.680286865234" />
                  <Point X="-3.602135986328" Y="1.694590209961" />
                  <Point X="-4.151234375" Y="2.115928222656" />
                  <Point X="-4.351859863281" Y="2.269874023438" />
                  <Point X="-4.295634765625" Y="2.366201660156" />
                  <Point X="-4.081152832031" Y="2.733660888672" />
                  <Point X="-3.940129882812" Y="2.91492578125" />
                  <Point X="-3.750504882813" Y="3.158661865234" />
                  <Point X="-3.438444335938" Y="2.978493408203" />
                  <Point X="-3.206656982422" Y="2.844670898438" />
                  <Point X="-3.187724609375" Y="2.836340332031" />
                  <Point X="-3.167082519531" Y="2.829831787109" />
                  <Point X="-3.146794677734" Y="2.825796386719" />
                  <Point X="-3.116439208984" Y="2.823140625" />
                  <Point X="-3.061245361328" Y="2.818311767578" />
                  <Point X="-3.040565185547" Y="2.818762939453" />
                  <Point X="-3.019106445312" Y="2.821588134766" />
                  <Point X="-2.999015136719" Y="2.826504394531" />
                  <Point X="-2.980463867188" Y="2.835652832031" />
                  <Point X="-2.962209472656" Y="2.847281982422" />
                  <Point X="-2.946077392578" Y="2.860229492188" />
                  <Point X="-2.924530761719" Y="2.881776123047" />
                  <Point X="-2.885353759766" Y="2.920952880859" />
                  <Point X="-2.872406738281" Y="2.937084228516" />
                  <Point X="-2.86077734375" Y="2.955338623047" />
                  <Point X="-2.851628662109" Y="2.973890136719" />
                  <Point X="-2.846712158203" Y="2.993981933594" />
                  <Point X="-2.843886962891" Y="3.015440917969" />
                  <Point X="-2.843435791016" Y="3.03612109375" />
                  <Point X="-2.846091552734" Y="3.066476806641" />
                  <Point X="-2.850920410156" Y="3.121670654297" />
                  <Point X="-2.854955810547" Y="3.141958496094" />
                  <Point X="-2.861464355469" Y="3.162600585938" />
                  <Point X="-2.869794921875" Y="3.181532958984" />
                  <Point X="-3.1131171875" Y="3.602979003906" />
                  <Point X="-3.183333007812" Y="3.724596435547" />
                  <Point X="-3.074180175781" Y="3.808282714844" />
                  <Point X="-2.700625488281" Y="4.094683837891" />
                  <Point X="-2.478519287109" Y="4.218080566406" />
                  <Point X="-2.167036621094" Y="4.391133789062" />
                  <Point X="-2.114152587891" Y="4.322213867188" />
                  <Point X="-2.04319543457" Y="4.229740722656" />
                  <Point X="-2.028892456055" Y="4.214799804688" />
                  <Point X="-2.01231237793" Y="4.200887207031" />
                  <Point X="-1.99511340332" Y="4.189395019531" />
                  <Point X="-1.961327880859" Y="4.171807128906" />
                  <Point X="-1.899897338867" Y="4.139828125" />
                  <Point X="-1.88061730957" Y="4.132330566406" />
                  <Point X="-1.85971081543" Y="4.126729003906" />
                  <Point X="-1.839267822266" Y="4.123582519531" />
                  <Point X="-1.818628417969" Y="4.124935546875" />
                  <Point X="-1.797313110352" Y="4.128693847656" />
                  <Point X="-1.77745300293" Y="4.134482421875" />
                  <Point X="-1.742263061523" Y="4.149059082031" />
                  <Point X="-1.678279052734" Y="4.175562011719" />
                  <Point X="-1.660146362305" Y="4.185509765625" />
                  <Point X="-1.642416625977" Y="4.197923828125" />
                  <Point X="-1.626864379883" Y="4.2115625" />
                  <Point X="-1.6146328125" Y="4.228244140625" />
                  <Point X="-1.603810913086" Y="4.24698828125" />
                  <Point X="-1.59548046875" Y="4.265920898438" />
                  <Point X="-1.584026611328" Y="4.302247558594" />
                  <Point X="-1.563201171875" Y="4.368297363281" />
                  <Point X="-1.559165649414" Y="4.388584960938" />
                  <Point X="-1.557279174805" Y="4.410146972656" />
                  <Point X="-1.55773034668" Y="4.430826660156" />
                  <Point X="-1.584201904297" Y="4.631897949219" />
                  <Point X="-1.433224975586" Y="4.6742265625" />
                  <Point X="-0.949634887695" Y="4.80980859375" />
                  <Point X="-0.680381591797" Y="4.8413203125" />
                  <Point X="-0.294710754395" Y="4.886457519531" />
                  <Point X="-0.202472351074" Y="4.542219238281" />
                  <Point X="-0.133903305054" Y="4.286315429688" />
                  <Point X="-0.121129844666" Y="4.258124023438" />
                  <Point X="-0.10327155304" Y="4.231397460938" />
                  <Point X="-0.082114547729" Y="4.20880859375" />
                  <Point X="-0.054819446564" Y="4.19421875" />
                  <Point X="-0.024381370544" Y="4.183886230469" />
                  <Point X="0.006155913353" Y="4.178844238281" />
                  <Point X="0.03669316864" Y="4.183886230469" />
                  <Point X="0.067131248474" Y="4.19421875" />
                  <Point X="0.094426460266" Y="4.20880859375" />
                  <Point X="0.115583503723" Y="4.231397460938" />
                  <Point X="0.133441802979" Y="4.258124023438" />
                  <Point X="0.146215255737" Y="4.286315429688" />
                  <Point X="0.270890594482" Y="4.751610839844" />
                  <Point X="0.307419311523" Y="4.8879375" />
                  <Point X="0.421790893555" Y="4.875959472656" />
                  <Point X="0.844041320801" Y="4.831738769531" />
                  <Point X="1.066807739258" Y="4.777956054688" />
                  <Point X="1.481027099609" Y="4.677950683594" />
                  <Point X="1.625069702148" Y="4.625705566406" />
                  <Point X="1.894645874023" Y="4.527928222656" />
                  <Point X="2.034855834961" Y="4.462356445312" />
                  <Point X="2.294576416016" Y="4.340893554688" />
                  <Point X="2.430054443359" Y="4.261963867188" />
                  <Point X="2.680977050781" Y="4.115775878906" />
                  <Point X="2.808727294922" Y="4.024926757812" />
                  <Point X="2.943260009766" Y="3.929254638672" />
                  <Point X="2.453312988281" Y="3.080641357422" />
                  <Point X="2.147581054688" Y="2.551097900391" />
                  <Point X="2.142075683594" Y="2.539930664062" />
                  <Point X="2.133076904297" Y="2.516056884766" />
                  <Point X="2.125458740234" Y="2.487568847656" />
                  <Point X="2.111607177734" Y="2.435770507813" />
                  <Point X="2.108619384766" Y="2.417934326172" />
                  <Point X="2.107727783203" Y="2.380952880859" />
                  <Point X="2.110698242188" Y="2.356318847656" />
                  <Point X="2.116099121094" Y="2.311528076172" />
                  <Point X="2.121441894531" Y="2.289605224609" />
                  <Point X="2.129708007812" Y="2.267516357422" />
                  <Point X="2.140070800781" Y="2.247471191406" />
                  <Point X="2.155313476563" Y="2.225007324219" />
                  <Point X="2.183028564453" Y="2.184162597656" />
                  <Point X="2.194465332031" Y="2.170327880859" />
                  <Point X="2.221598876953" Y="2.145592529297" />
                  <Point X="2.244062744141" Y="2.130349853516" />
                  <Point X="2.284907470703" Y="2.102635009766" />
                  <Point X="2.304951904297" Y="2.092272460938" />
                  <Point X="2.327040283203" Y="2.084006347656" />
                  <Point X="2.348963867188" Y="2.078663330078" />
                  <Point X="2.373597900391" Y="2.075692871094" />
                  <Point X="2.418388671875" Y="2.070291748047" />
                  <Point X="2.436467529297" Y="2.069845703125" />
                  <Point X="2.473206542969" Y="2.074171142578" />
                  <Point X="2.501694580078" Y="2.081789306641" />
                  <Point X="2.553492919922" Y="2.095640869141" />
                  <Point X="2.565283447266" Y="2.099638183594" />
                  <Point X="2.588533935547" Y="2.110145019531" />
                  <Point X="3.551373779297" Y="2.666040771484" />
                  <Point X="3.967326660156" Y="2.906191162109" />
                  <Point X="3.974431884766" Y="2.89631640625" />
                  <Point X="4.123270507813" Y="2.689464355469" />
                  <Point X="4.194490722656" Y="2.571772460938" />
                  <Point X="4.262198730469" Y="2.459884277344" />
                  <Point X="3.63330859375" Y="1.977319335938" />
                  <Point X="3.230783935547" Y="1.668451171875" />
                  <Point X="3.221425048828" Y="1.660241943359" />
                  <Point X="3.203973876953" Y="1.641627929688" />
                  <Point X="3.183470947266" Y="1.614880249023" />
                  <Point X="3.146191650391" Y="1.566246459961" />
                  <Point X="3.13660546875" Y="1.550911254883" />
                  <Point X="3.121630126953" Y="1.517086425781" />
                  <Point X="3.113992675781" Y="1.489776855469" />
                  <Point X="3.100106201172" Y="1.440121826172" />
                  <Point X="3.096652587891" Y="1.417823242188" />
                  <Point X="3.095836425781" Y="1.394252929688" />
                  <Point X="3.097739501953" Y="1.371768066406" />
                  <Point X="3.104009033203" Y="1.3413828125" />
                  <Point X="3.115408447266" Y="1.286135131836" />
                  <Point X="3.120679931641" Y="1.268977661133" />
                  <Point X="3.136282470703" Y="1.235740600586" />
                  <Point X="3.153334716797" Y="1.209821777344" />
                  <Point X="3.184340087891" Y="1.16269543457" />
                  <Point X="3.198893554688" Y="1.145450439453" />
                  <Point X="3.216137451172" Y="1.129360595703" />
                  <Point X="3.234346679688" Y="1.11603503418" />
                  <Point X="3.259057617188" Y="1.102124755859" />
                  <Point X="3.303988769531" Y="1.076832763672" />
                  <Point X="3.320521240234" Y="1.069501586914" />
                  <Point X="3.356117919922" Y="1.059438598633" />
                  <Point X="3.389529052734" Y="1.055022827148" />
                  <Point X="3.450278564453" Y="1.046994018555" />
                  <Point X="3.462702148438" Y="1.046175048828" />
                  <Point X="3.488203613281" Y="1.04698449707" />
                  <Point X="4.402836914062" Y="1.16739831543" />
                  <Point X="4.776839355469" Y="1.21663659668" />
                  <Point X="4.782010253906" Y="1.195396484375" />
                  <Point X="4.845936035156" Y="0.932809265137" />
                  <Point X="4.86837890625" Y="0.788661376953" />
                  <Point X="4.890864746094" Y="0.644238464355" />
                  <Point X="4.177881835938" Y="0.453195281982" />
                  <Point X="3.716579833984" Y="0.329589935303" />
                  <Point X="3.704791259766" Y="0.325586608887" />
                  <Point X="3.681545654297" Y="0.315067993164" />
                  <Point X="3.648720214844" Y="0.296094177246" />
                  <Point X="3.589035644531" Y="0.261595428467" />
                  <Point X="3.574311035156" Y="0.251096221924" />
                  <Point X="3.547530761719" Y="0.225576461792" />
                  <Point X="3.527835693359" Y="0.200480194092" />
                  <Point X="3.492024902344" Y="0.154848968506" />
                  <Point X="3.480301025391" Y="0.13556930542" />
                  <Point X="3.470527099609" Y="0.114105537415" />
                  <Point X="3.463680908203" Y="0.092604270935" />
                  <Point X="3.457115722656" Y="0.058323925018" />
                  <Point X="3.445178710938" Y="-0.004006225586" />
                  <Point X="3.443482910156" Y="-0.021875295639" />
                  <Point X="3.445178710938" Y="-0.05855393219" />
                  <Point X="3.451743896484" Y="-0.092834274292" />
                  <Point X="3.463680908203" Y="-0.155164428711" />
                  <Point X="3.470527099609" Y="-0.176665542603" />
                  <Point X="3.480301025391" Y="-0.198129302979" />
                  <Point X="3.492024658203" Y="-0.217408660889" />
                  <Point X="3.511719726562" Y="-0.242505081177" />
                  <Point X="3.547530517578" Y="-0.288136322021" />
                  <Point X="3.559999023438" Y="-0.30123614502" />
                  <Point X="3.589035888672" Y="-0.324155731201" />
                  <Point X="3.621861328125" Y="-0.343129364014" />
                  <Point X="3.681545898438" Y="-0.377628143311" />
                  <Point X="3.692710205078" Y="-0.383139038086" />
                  <Point X="3.716579833984" Y="-0.392149932861" />
                  <Point X="4.555340332031" Y="-0.616895080566" />
                  <Point X="4.891472167969" Y="-0.706961425781" />
                  <Point X="4.890715820312" Y="-0.711979003906" />
                  <Point X="4.855022460938" Y="-0.948725952148" />
                  <Point X="4.826269042969" Y="-1.074727416992" />
                  <Point X="4.801173339844" Y="-1.18469909668" />
                  <Point X="3.959362792969" Y="-1.073872558594" />
                  <Point X="3.424382080078" Y="-1.003441040039" />
                  <Point X="3.408035644531" Y="-1.002710327148" />
                  <Point X="3.374658447266" Y="-1.005508728027" />
                  <Point X="3.310233886719" Y="-1.01951171875" />
                  <Point X="3.193094238281" Y="-1.044972412109" />
                  <Point X="3.163973632812" Y="-1.056597167969" />
                  <Point X="3.136147216797" Y="-1.073489624023" />
                  <Point X="3.112397460938" Y="-1.093960083008" />
                  <Point X="3.073456787109" Y="-1.140793579102" />
                  <Point X="3.002653320312" Y="-1.225948364258" />
                  <Point X="2.987932617188" Y="-1.250330566406" />
                  <Point X="2.976589355469" Y="-1.277715698242" />
                  <Point X="2.969757568359" Y="-1.305365722656" />
                  <Point X="2.964176513672" Y="-1.366016967773" />
                  <Point X="2.954028564453" Y="-1.476295898438" />
                  <Point X="2.956347167969" Y="-1.507564208984" />
                  <Point X="2.964078613281" Y="-1.539185180664" />
                  <Point X="2.976450195312" Y="-1.567996826172" />
                  <Point X="3.012103759766" Y="-1.623453369141" />
                  <Point X="3.076930419922" Y="-1.724287109375" />
                  <Point X="3.086932861328" Y="-1.737239013672" />
                  <Point X="3.110628417969" Y="-1.760909179688" />
                  <Point X="3.889003173828" Y="-2.358177490234" />
                  <Point X="4.213122558594" Y="-2.606883056641" />
                  <Point X="4.124810058594" Y="-2.749785888672" />
                  <Point X="4.065345458984" Y="-2.834276611328" />
                  <Point X="4.028981445312" Y="-2.885945068359" />
                  <Point X="3.277427001953" Y="-2.452034912109" />
                  <Point X="2.800954589844" Y="-2.176943115234" />
                  <Point X="2.786131591797" Y="-2.170012451172" />
                  <Point X="2.754224365234" Y="-2.159825195312" />
                  <Point X="2.677548828125" Y="-2.145977783203" />
                  <Point X="2.538134033203" Y="-2.120799560547" />
                  <Point X="2.506781982422" Y="-2.120395263672" />
                  <Point X="2.474609130859" Y="-2.125353027344" />
                  <Point X="2.444833251953" Y="-2.135176757812" />
                  <Point X="2.381134765625" Y="-2.168700683594" />
                  <Point X="2.265315185547" Y="-2.229655761719" />
                  <Point X="2.242384033203" Y="-2.246549072266" />
                  <Point X="2.221424316406" Y="-2.267509033203" />
                  <Point X="2.204531982422" Y="-2.290439208984" />
                  <Point X="2.1710078125" Y="-2.354137695312" />
                  <Point X="2.110052978516" Y="-2.469957275391" />
                  <Point X="2.100229003906" Y="-2.499733398438" />
                  <Point X="2.095271240234" Y="-2.531906005859" />
                  <Point X="2.095675537109" Y="-2.563258056641" />
                  <Point X="2.109522949219" Y="-2.63993359375" />
                  <Point X="2.134701171875" Y="-2.779348388672" />
                  <Point X="2.138985595703" Y="-2.795141845703" />
                  <Point X="2.151819091797" Y="-2.826078613281" />
                  <Point X="2.652003173828" Y="-3.692422607422" />
                  <Point X="2.861283203125" Y="-4.05490625" />
                  <Point X="2.781851806641" Y="-4.111642578125" />
                  <Point X="2.715366210938" Y="-4.154677734375" />
                  <Point X="2.701765136719" Y="-4.163481445312" />
                  <Point X="2.122138671875" Y="-3.408097900391" />
                  <Point X="1.758546142578" Y="-2.934255126953" />
                  <Point X="1.747503540039" Y="-2.922178955078" />
                  <Point X="1.721924072266" Y="-2.900557373047" />
                  <Point X="1.646301269531" Y="-2.851938964844" />
                  <Point X="1.50880078125" Y="-2.763538574219" />
                  <Point X="1.479989746094" Y="-2.751167236328" />
                  <Point X="1.448368774414" Y="-2.743435546875" />
                  <Point X="1.417099609375" Y="-2.741116699219" />
                  <Point X="1.334393066406" Y="-2.748727539062" />
                  <Point X="1.184012695312" Y="-2.762565673828" />
                  <Point X="1.15636315918" Y="-2.769397460938" />
                  <Point X="1.128978027344" Y="-2.780740722656" />
                  <Point X="1.104595947266" Y="-2.795461181641" />
                  <Point X="1.040732055664" Y="-2.848561767578" />
                  <Point X="0.924612121582" Y="-2.945111572266" />
                  <Point X="0.904141052246" Y="-2.968861816406" />
                  <Point X="0.887248779297" Y="-2.996688476562" />
                  <Point X="0.875624206543" Y="-3.025808837891" />
                  <Point X="0.856529418945" Y="-3.113660644531" />
                  <Point X="0.821809936523" Y="-3.273396728516" />
                  <Point X="0.819724487305" Y="-3.289627441406" />
                  <Point X="0.819742248535" Y="-3.323120117188" />
                  <Point X="0.961490661621" Y="-4.399805175781" />
                  <Point X="1.022065246582" Y="-4.859915039062" />
                  <Point X="0.975679504395" Y="-4.870082519531" />
                  <Point X="0.929315551758" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058432128906" Y="-4.752636230469" />
                  <Point X="-1.141246459961" Y="-4.731328613281" />
                  <Point X="-1.137885864258" Y="-4.705802734375" />
                  <Point X="-1.120775634766" Y="-4.575838378906" />
                  <Point X="-1.120077514648" Y="-4.568099609375" />
                  <Point X="-1.119451782227" Y="-4.541033691406" />
                  <Point X="-1.121759277344" Y="-4.509328125" />
                  <Point X="-1.123334106445" Y="-4.497690429688" />
                  <Point X="-1.144811035156" Y="-4.389718261719" />
                  <Point X="-1.183861450195" Y="-4.193398925781" />
                  <Point X="-1.188125" Y="-4.17846875" />
                  <Point X="-1.19902734375" Y="-4.149502441406" />
                  <Point X="-1.205666015625" Y="-4.135466308594" />
                  <Point X="-1.221738037109" Y="-4.10762890625" />
                  <Point X="-1.23057409668" Y="-4.094861816406" />
                  <Point X="-1.250208496094" Y="-4.070937011719" />
                  <Point X="-1.261006958008" Y="-4.059779296875" />
                  <Point X="-1.343775024414" Y="-3.987193359375" />
                  <Point X="-1.494267578125" Y="-3.855214355469" />
                  <Point X="-1.506739135742" Y="-3.84596484375" />
                  <Point X="-1.533021972656" Y="-3.82962109375" />
                  <Point X="-1.546833618164" Y="-3.822526855469" />
                  <Point X="-1.576530761719" Y="-3.810225830078" />
                  <Point X="-1.591313476562" Y="-3.805476074219" />
                  <Point X="-1.621454833984" Y="-3.798447998047" />
                  <Point X="-1.636813964844" Y="-3.796169677734" />
                  <Point X="-1.746665771484" Y="-3.788969482422" />
                  <Point X="-1.946403198242" Y="-3.775878173828" />
                  <Point X="-1.961928222656" Y="-3.776132324219" />
                  <Point X="-1.992729614258" Y="-3.779166015625" />
                  <Point X="-2.008006103516" Y="-3.781945556641" />
                  <Point X="-2.03905480957" Y="-3.790265136719" />
                  <Point X="-2.053674316406" Y="-3.795496337891" />
                  <Point X="-2.081865478516" Y="-3.808269775391" />
                  <Point X="-2.095437011719" Y="-3.815812011719" />
                  <Point X="-2.186971435547" Y="-3.876973388672" />
                  <Point X="-2.353403564453" Y="-3.988180175781" />
                  <Point X="-2.359681640625" Y="-3.992757080078" />
                  <Point X="-2.380442138672" Y="-4.010131103516" />
                  <Point X="-2.402759277344" Y="-4.032771484375" />
                  <Point X="-2.410471435547" Y="-4.041629638672" />
                  <Point X="-2.503202880859" Y="-4.162479492188" />
                  <Point X="-2.747581787109" Y="-4.011166259766" />
                  <Point X="-2.883186767578" Y="-3.906754882813" />
                  <Point X="-2.980862792969" Y="-3.831547607422" />
                  <Point X="-2.605637207031" Y="-3.181637939453" />
                  <Point X="-2.341488769531" Y="-2.724119628906" />
                  <Point X="-2.334850097656" Y="-2.710083251953" />
                  <Point X="-2.323947998047" Y="-2.681116943359" />
                  <Point X="-2.319684570312" Y="-2.666187011719" />
                  <Point X="-2.313413574219" Y="-2.634660644531" />
                  <Point X="-2.311638916016" Y="-2.619235351562" />
                  <Point X="-2.310626220703" Y="-2.588301269531" />
                  <Point X="-2.314666259766" Y="-2.557615234375" />
                  <Point X="-2.323650878906" Y="-2.527997558594" />
                  <Point X="-2.329357421875" Y="-2.513557128906" />
                  <Point X="-2.343574462891" Y="-2.484728271484" />
                  <Point X="-2.351557128906" Y="-2.47141015625" />
                  <Point X="-2.369584716797" Y="-2.446253173828" />
                  <Point X="-2.379629638672" Y="-2.434414306641" />
                  <Point X="-2.396978271484" Y="-2.417065673828" />
                  <Point X="-2.408817138672" Y="-2.407020751953" />
                  <Point X="-2.433974121094" Y="-2.388993164062" />
                  <Point X="-2.447292236328" Y="-2.381010498047" />
                  <Point X="-2.47612109375" Y="-2.366793457031" />
                  <Point X="-2.490561523438" Y="-2.361086914062" />
                  <Point X="-2.520179199219" Y="-2.352102294922" />
                  <Point X="-2.550865234375" Y="-2.348062255859" />
                  <Point X="-2.581799316406" Y="-2.349074951172" />
                  <Point X="-2.597224609375" Y="-2.350849609375" />
                  <Point X="-2.628750976562" Y="-2.357120605469" />
                  <Point X="-2.643680908203" Y="-2.361384033203" />
                  <Point X="-2.672647216797" Y="-2.372286132812" />
                  <Point X="-2.68668359375" Y="-2.378924804688" />
                  <Point X="-3.518564453125" Y="-2.859211425781" />
                  <Point X="-3.793089111328" Y="-3.017708251953" />
                  <Point X="-4.004016113281" Y="-2.74059375" />
                  <Point X="-4.101229003906" Y="-2.57758203125" />
                  <Point X="-4.181265136719" Y="-2.443373779297" />
                  <Point X="-3.510296386719" Y="-1.928520629883" />
                  <Point X="-3.048122314453" Y="-1.573882080078" />
                  <Point X="-3.036481689453" Y="-1.563309570312" />
                  <Point X="-3.015104003906" Y="-1.540388916016" />
                  <Point X="-3.005366943359" Y="-1.528040771484" />
                  <Point X="-2.987402099609" Y="-1.500909423828" />
                  <Point X="-2.979835693359" Y="-1.48712487793" />
                  <Point X="-2.967080078125" Y="-1.45849609375" />
                  <Point X="-2.961890869141" Y="-1.443651855469" />
                  <Point X="-2.954186279297" Y="-1.413904541016" />
                  <Point X="-2.951552734375" Y="-1.398802978516" />
                  <Point X="-2.948748535156" Y="-1.368373657227" />
                  <Point X="-2.948577880859" Y="-1.353045776367" />
                  <Point X="-2.950786865234" Y="-1.321375366211" />
                  <Point X="-2.953083740234" Y="-1.306219604492" />
                  <Point X="-2.960084960938" Y="-1.276474731445" />
                  <Point X="-2.971779052734" Y="-1.248242919922" />
                  <Point X="-2.987861328125" Y="-1.222259399414" />
                  <Point X="-2.996953857422" Y="-1.209918823242" />
                  <Point X="-3.017786376953" Y="-1.185962524414" />
                  <Point X="-3.028745605469" Y="-1.175244506836" />
                  <Point X="-3.052245605469" Y="-1.155710449219" />
                  <Point X="-3.064786376953" Y="-1.146894287109" />
                  <Point X="-3.091269042969" Y="-1.131307861328" />
                  <Point X="-3.105434326172" Y="-1.124480957031" />
                  <Point X="-3.134697265625" Y="-1.113257080078" />
                  <Point X="-3.149795410156" Y="-1.108859985352" />
                  <Point X="-3.181683105469" Y="-1.102378540039" />
                  <Point X="-3.197298828125" Y="-1.100532348633" />
                  <Point X="-3.228622070312" Y="-1.09944140625" />
                  <Point X="-3.244329101562" Y="-1.100196533203" />
                  <Point X="-4.2945" Y="-1.238454101563" />
                  <Point X="-4.660920410156" Y="-1.286694335938" />
                  <Point X="-4.74076171875" Y="-0.97411895752" />
                  <Point X="-4.766481933594" Y="-0.794288391113" />
                  <Point X="-4.786452636719" Y="-0.654654418945" />
                  <Point X="-4.034773925781" Y="-0.453242645264" />
                  <Point X="-3.508288085938" Y="-0.312171295166" />
                  <Point X="-3.49733203125" Y="-0.308508483887" />
                  <Point X="-3.475945556641" Y="-0.2998800354" />
                  <Point X="-3.465515136719" Y="-0.294914520264" />
                  <Point X="-3.440099365234" Y="-0.280859405518" />
                  <Point X="-3.423713378906" Y="-0.270678588867" />
                  <Point X="-3.405808105469" Y="-0.258251190186" />
                  <Point X="-3.396478027344" Y="-0.250868789673" />
                  <Point X="-3.372523681641" Y="-0.229343002319" />
                  <Point X="-3.357573486328" Y="-0.212262664795" />
                  <Point X="-3.336382080078" Y="-0.181236587524" />
                  <Point X="-3.327335449219" Y="-0.164665435791" />
                  <Point X="-3.316627685547" Y="-0.13935168457" />
                  <Point X="-3.310155517578" Y="-0.121650840759" />
                  <Point X="-3.304187011719" Y="-0.102420379639" />
                  <Point X="-3.301576660156" Y="-0.091938598633" />
                  <Point X="-3.296133544922" Y="-0.063198795319" />
                  <Point X="-3.294522949219" Y="-0.042485961914" />
                  <Point X="-3.295624023438" Y="-0.008035175323" />
                  <Point X="-3.29775" Y="0.009140133858" />
                  <Point X="-3.303279541016" Y="0.034537410736" />
                  <Point X="-3.307469482422" Y="0.05043649292" />
                  <Point X="-3.313437988281" Y="0.069667106628" />
                  <Point X="-3.317669189453" Y="0.080787223816" />
                  <Point X="-3.330985107422" Y="0.110110671997" />
                  <Point X="-3.342866455078" Y="0.129628570557" />
                  <Point X="-3.366304199219" Y="0.159372619629" />
                  <Point X="-3.379520019531" Y="0.173065093994" />
                  <Point X="-3.401186279297" Y="0.191417205811" />
                  <Point X="-3.415654785156" Y="0.202525283813" />
                  <Point X="-3.433560058594" Y="0.214952835083" />
                  <Point X="-3.440483886719" Y="0.219328582764" />
                  <Point X="-3.465616210938" Y="0.232834686279" />
                  <Point X="-3.496566894531" Y="0.245635879517" />
                  <Point X="-3.508288085938" Y="0.249611251831" />
                  <Point X="-4.465568359375" Y="0.506113708496" />
                  <Point X="-4.785445800781" Y="0.591824523926" />
                  <Point X="-4.731330566406" Y="0.957532897949" />
                  <Point X="-4.679556640625" Y="1.148593261719" />
                  <Point X="-4.633586425781" Y="1.318237182617" />
                  <Point X="-4.155464355469" Y="1.255291137695" />
                  <Point X="-3.778066162109" Y="1.20560559082" />
                  <Point X="-3.76773828125" Y="1.204815429688" />
                  <Point X="-3.747057861328" Y="1.204364257812" />
                  <Point X="-3.736705322266" Y="1.204703125" />
                  <Point X="-3.715143554688" Y="1.20658984375" />
                  <Point X="-3.704889648438" Y="1.208053833008" />
                  <Point X="-3.684603271484" Y="1.212089233398" />
                  <Point X="-3.674571289062" Y="1.214660522461" />
                  <Point X="-3.652775390625" Y="1.221532470703" />
                  <Point X="-3.613145263672" Y="1.234027954102" />
                  <Point X="-3.603451904297" Y="1.237676147461" />
                  <Point X="-3.584518798828" Y="1.246006469727" />
                  <Point X="-3.575278564453" Y="1.250689208984" />
                  <Point X="-3.556534423828" Y="1.261511108398" />
                  <Point X="-3.547859375" Y="1.26717175293" />
                  <Point X="-3.531178222656" Y="1.279403076172" />
                  <Point X="-3.515928466797" Y="1.293377075195" />
                  <Point X="-3.502290039063" Y="1.308928710938" />
                  <Point X="-3.495895019531" Y="1.317077270508" />
                  <Point X="-3.483480712891" Y="1.334807128906" />
                  <Point X="-3.478011474609" Y="1.343602294922" />
                  <Point X="-3.468062255859" Y="1.361737182617" />
                  <Point X="-3.463582519531" Y="1.371076904297" />
                  <Point X="-3.454836914062" Y="1.392190917969" />
                  <Point X="-3.438935058594" Y="1.430581176758" />
                  <Point X="-3.435499023438" Y="1.440352050781" />
                  <Point X="-3.429711181641" Y="1.460209472656" />
                  <Point X="-3.427359375" Y="1.470295898438" />
                  <Point X="-3.423600830078" Y="1.491611083984" />
                  <Point X="-3.422360839844" Y="1.501894042969" />
                  <Point X="-3.4210078125" Y="1.522534545898" />
                  <Point X="-3.42191015625" Y="1.543200805664" />
                  <Point X="-3.425056884766" Y="1.563644897461" />
                  <Point X="-3.427188232422" Y="1.573780395508" />
                  <Point X="-3.432790039062" Y="1.594686645508" />
                  <Point X="-3.436011962891" Y="1.604530273438" />
                  <Point X="-3.443508789062" Y="1.62380859375" />
                  <Point X="-3.447783691406" Y="1.633243164062" />
                  <Point X="-3.458336181641" Y="1.653514404297" />
                  <Point X="-3.4775234375" Y="1.690372802734" />
                  <Point X="-3.482799560547" Y="1.699285766602" />
                  <Point X="-3.494291748047" Y="1.716485229492" />
                  <Point X="-3.5005078125" Y="1.724771850586" />
                  <Point X="-3.514420410156" Y="1.741352050781" />
                  <Point X="-3.521500976563" Y="1.748911987305" />
                  <Point X="-3.536442626953" Y="1.763215209961" />
                  <Point X="-3.544303710938" Y="1.769958740234" />
                  <Point X="-4.093402099609" Y="2.191296875" />
                  <Point X="-4.227614257812" Y="2.294281982422" />
                  <Point X="-4.213588378906" Y="2.318312255859" />
                  <Point X="-4.002292236328" Y="2.680313476562" />
                  <Point X="-3.865149169922" Y="2.856591308594" />
                  <Point X="-3.726338378906" Y="3.035012695312" />
                  <Point X="-3.485944335938" Y="2.896220947266" />
                  <Point X="-3.254156982422" Y="2.7623984375" />
                  <Point X="-3.244918457031" Y="2.757716552734" />
                  <Point X="-3.225986083984" Y="2.749385986328" />
                  <Point X="-3.216292236328" Y="2.745737304688" />
                  <Point X="-3.195650146484" Y="2.739228759766" />
                  <Point X="-3.185615722656" Y="2.736656982422" />
                  <Point X="-3.165327880859" Y="2.732621582031" />
                  <Point X="-3.155074462891" Y="2.731157958984" />
                  <Point X="-3.124718994141" Y="2.728502197266" />
                  <Point X="-3.069525146484" Y="2.723673339844" />
                  <Point X="-3.059173339844" Y="2.723334472656" />
                  <Point X="-3.038493164062" Y="2.723785644531" />
                  <Point X="-3.028164794922" Y="2.724575683594" />
                  <Point X="-3.006706054688" Y="2.727400878906" />
                  <Point X="-2.996526611328" Y="2.729310546875" />
                  <Point X="-2.976435302734" Y="2.734226806641" />
                  <Point X="-2.956997802734" Y="2.741301513672" />
                  <Point X="-2.938446533203" Y="2.750449951172" />
                  <Point X="-2.929420898438" Y="2.755530273438" />
                  <Point X="-2.911166503906" Y="2.767159423828" />
                  <Point X="-2.902746337891" Y="2.773193359375" />
                  <Point X="-2.886614257812" Y="2.786140869141" />
                  <Point X="-2.87890234375" Y="2.793054443359" />
                  <Point X="-2.857355712891" Y="2.814601074219" />
                  <Point X="-2.818178710938" Y="2.853777832031" />
                  <Point X="-2.811265380859" Y="2.861489501953" />
                  <Point X="-2.798318359375" Y="2.877620849609" />
                  <Point X="-2.792284667969" Y="2.886040527344" />
                  <Point X="-2.780655273438" Y="2.904294921875" />
                  <Point X="-2.775574707031" Y="2.913320800781" />
                  <Point X="-2.766426025391" Y="2.931872314453" />
                  <Point X="-2.759351318359" Y="2.951309570312" />
                  <Point X="-2.754434814453" Y="2.971401367188" />
                  <Point X="-2.752524902344" Y="2.981581542969" />
                  <Point X="-2.749699707031" Y="3.003040527344" />
                  <Point X="-2.748909667969" Y="3.013368896484" />
                  <Point X="-2.748458496094" Y="3.034049072266" />
                  <Point X="-2.748797363281" Y="3.044400878906" />
                  <Point X="-2.751453125" Y="3.074756591797" />
                  <Point X="-2.756281982422" Y="3.129950439453" />
                  <Point X="-2.757745605469" Y="3.140203857422" />
                  <Point X="-2.761781005859" Y="3.160491699219" />
                  <Point X="-2.764352783203" Y="3.170526123047" />
                  <Point X="-2.770861328125" Y="3.191168212891" />
                  <Point X="-2.774510009766" Y="3.200862060547" />
                  <Point X="-2.782840576172" Y="3.219794433594" />
                  <Point X="-2.787522460938" Y="3.229032958984" />
                  <Point X="-3.030844726562" Y="3.650479003906" />
                  <Point X="-3.059387207031" Y="3.699916259766" />
                  <Point X="-3.016378173828" Y="3.732890869141" />
                  <Point X="-2.648374267578" Y="4.015036376953" />
                  <Point X="-2.432381835938" Y="4.135036132812" />
                  <Point X="-2.192524902344" Y="4.268295898437" />
                  <Point X="-2.189521240234" Y="4.264381347656" />
                  <Point X="-2.118563964844" Y="4.171908203125" />
                  <Point X="-2.111819580078" Y="4.164046386719" />
                  <Point X="-2.097516601562" Y="4.14910546875" />
                  <Point X="-2.089958007813" Y="4.142026367188" />
                  <Point X="-2.073377929688" Y="4.128113769531" />
                  <Point X="-2.065092285156" Y="4.121897949219" />
                  <Point X="-2.047893188477" Y="4.110405761719" />
                  <Point X="-2.038979980469" Y="4.105129394531" />
                  <Point X="-2.005194458008" Y="4.087541259766" />
                  <Point X="-1.943763916016" Y="4.055562255859" />
                  <Point X="-1.934328857422" Y="4.051287353516" />
                  <Point X="-1.915048828125" Y="4.043789794922" />
                  <Point X="-1.905203857422" Y="4.040567382812" />
                  <Point X="-1.884297363281" Y="4.034965820313" />
                  <Point X="-1.874162597656" Y="4.032834716797" />
                  <Point X="-1.853719604492" Y="4.029688232422" />
                  <Point X="-1.833053344727" Y="4.028785888672" />
                  <Point X="-1.81241394043" Y="4.030138916016" />
                  <Point X="-1.802132568359" Y="4.031378662109" />
                  <Point X="-1.780817260742" Y="4.035136962891" />
                  <Point X="-1.770729858398" Y="4.037489013672" />
                  <Point X="-1.750869750977" Y="4.043277587891" />
                  <Point X="-1.741097045898" Y="4.046714355469" />
                  <Point X="-1.705907226562" Y="4.061291015625" />
                  <Point X="-1.641923095703" Y="4.087793945312" />
                  <Point X="-1.63258581543" Y="4.092272705078" />
                  <Point X="-1.614453125" Y="4.102220214844" />
                  <Point X="-1.605657958984" Y="4.107689453125" />
                  <Point X="-1.587928100586" Y="4.120103515625" />
                  <Point X="-1.579779541016" Y="4.126498535156" />
                  <Point X="-1.564227294922" Y="4.140137207031" />
                  <Point X="-1.550252319336" Y="4.155387695312" />
                  <Point X="-1.538020874023" Y="4.172069335938" />
                  <Point X="-1.532360351562" Y="4.180744140625" />
                  <Point X="-1.521538330078" Y="4.19948828125" />
                  <Point X="-1.516856201172" Y="4.208727539062" />
                  <Point X="-1.508525756836" Y="4.22766015625" />
                  <Point X="-1.504877441406" Y="4.237353515625" />
                  <Point X="-1.493423583984" Y="4.273680175781" />
                  <Point X="-1.472598144531" Y="4.339729980469" />
                  <Point X="-1.470026611328" Y="4.349763671875" />
                  <Point X="-1.465990966797" Y="4.370051269531" />
                  <Point X="-1.46452722168" Y="4.380305175781" />
                  <Point X="-1.46264074707" Y="4.4018671875" />
                  <Point X="-1.462301757812" Y="4.412219238281" />
                  <Point X="-1.462752929688" Y="4.432898925781" />
                  <Point X="-1.46354309082" Y="4.4432265625" />
                  <Point X="-1.479266113281" Y="4.562654785156" />
                  <Point X="-1.407579223633" Y="4.582753417969" />
                  <Point X="-0.931174621582" Y="4.716320800781" />
                  <Point X="-0.669338684082" Y="4.746964355469" />
                  <Point X="-0.365221862793" Y="4.782556640625" />
                  <Point X="-0.294235290527" Y="4.517631347656" />
                  <Point X="-0.22566633606" Y="4.261727539062" />
                  <Point X="-0.220435256958" Y="4.247107910156" />
                  <Point X="-0.207661849976" Y="4.218916503906" />
                  <Point X="-0.200119247437" Y="4.205344726562" />
                  <Point X="-0.182260864258" Y="4.178618164062" />
                  <Point X="-0.17260824585" Y="4.166456054688" />
                  <Point X="-0.151451339722" Y="4.1438671875" />
                  <Point X="-0.126897941589" Y="4.125026367188" />
                  <Point X="-0.099602897644" Y="4.110436523438" />
                  <Point X="-0.085356651306" Y="4.104260742188" />
                  <Point X="-0.054918682098" Y="4.093927978516" />
                  <Point X="-0.039857234955" Y="4.090155273438" />
                  <Point X="-0.009319986343" Y="4.08511328125" />
                  <Point X="0.021631772995" Y="4.08511328125" />
                  <Point X="0.052169025421" Y="4.090155273438" />
                  <Point X="0.06723046875" Y="4.093927978516" />
                  <Point X="0.097668441772" Y="4.104260742188" />
                  <Point X="0.111914535522" Y="4.110436523438" />
                  <Point X="0.139209732056" Y="4.125026367188" />
                  <Point X="0.163763122559" Y="4.143866699219" />
                  <Point X="0.184920181274" Y="4.166455566406" />
                  <Point X="0.194572952271" Y="4.178617675781" />
                  <Point X="0.212431182861" Y="4.205344238281" />
                  <Point X="0.2199737854" Y="4.218916503906" />
                  <Point X="0.232747192383" Y="4.247107910156" />
                  <Point X="0.237978271484" Y="4.261727539062" />
                  <Point X="0.362653564453" Y="4.727022949219" />
                  <Point X="0.378190093994" Y="4.785005859375" />
                  <Point X="0.411895751953" Y="4.781476074219" />
                  <Point X="0.82787689209" Y="4.737912109375" />
                  <Point X="1.044512451172" Y="4.685609375" />
                  <Point X="1.453598266602" Y="4.586843261719" />
                  <Point X="1.592677490234" Y="4.5363984375" />
                  <Point X="1.858256103516" Y="4.440071289062" />
                  <Point X="1.994610839844" Y="4.376302246094" />
                  <Point X="2.250451904297" Y="4.256653808594" />
                  <Point X="2.382231445312" Y="4.17987890625" />
                  <Point X="2.629433105469" Y="4.035858398438" />
                  <Point X="2.753670654297" Y="3.947507324219" />
                  <Point X="2.817779785156" Y="3.901916503906" />
                  <Point X="2.371040527344" Y="3.128141357422" />
                  <Point X="2.06530859375" Y="2.598597900391" />
                  <Point X="2.062373046875" Y="2.593104980469" />
                  <Point X="2.053180908203" Y="2.573437988281" />
                  <Point X="2.044182250977" Y="2.549564208984" />
                  <Point X="2.041301757812" Y="2.540599121094" />
                  <Point X="2.03368359375" Y="2.512111083984" />
                  <Point X="2.01983203125" Y="2.460312744141" />
                  <Point X="2.017912597656" Y="2.451465576172" />
                  <Point X="2.013646972656" Y="2.420224121094" />
                  <Point X="2.012755371094" Y="2.383242675781" />
                  <Point X="2.013410888672" Y="2.369579833984" />
                  <Point X="2.016381469727" Y="2.344945800781" />
                  <Point X="2.021782226562" Y="2.300155029297" />
                  <Point X="2.023800537109" Y="2.289034179688" />
                  <Point X="2.029143310547" Y="2.267111328125" />
                  <Point X="2.032468017578" Y="2.256309326172" />
                  <Point X="2.040734130859" Y="2.234220458984" />
                  <Point X="2.045318115234" Y="2.223889160156" />
                  <Point X="2.055680908203" Y="2.203843994141" />
                  <Point X="2.061459472656" Y="2.194130126953" />
                  <Point X="2.076702148438" Y="2.171666259766" />
                  <Point X="2.104417236328" Y="2.130821533203" />
                  <Point X="2.109808105469" Y="2.123633300781" />
                  <Point X="2.130464355469" Y="2.100121826172" />
                  <Point X="2.157597900391" Y="2.075386474609" />
                  <Point X="2.1682578125" Y="2.066981201172" />
                  <Point X="2.190721679688" Y="2.051738525391" />
                  <Point X="2.23156640625" Y="2.024023681641" />
                  <Point X="2.241279785156" Y="2.018245117188" />
                  <Point X="2.26132421875" Y="2.00788269043" />
                  <Point X="2.271655273438" Y="2.003298706055" />
                  <Point X="2.293743652344" Y="1.995032592773" />
                  <Point X="2.304546142578" Y="1.991708007812" />
                  <Point X="2.326469726562" Y="1.986364868164" />
                  <Point X="2.337590820312" Y="1.984346557617" />
                  <Point X="2.362224853516" Y="1.981376098633" />
                  <Point X="2.407015625" Y="1.975974975586" />
                  <Point X="2.416045410156" Y="1.975320678711" />
                  <Point X="2.447575439453" Y="1.975497314453" />
                  <Point X="2.484314453125" Y="1.979822753906" />
                  <Point X="2.497748779297" Y="1.982395996094" />
                  <Point X="2.526236816406" Y="1.990014160156" />
                  <Point X="2.57803515625" Y="2.003865722656" />
                  <Point X="2.583995361328" Y="2.005670776367" />
                  <Point X="2.604404541016" Y="2.013067260742" />
                  <Point X="2.627655029297" Y="2.02357409668" />
                  <Point X="2.636033935547" Y="2.027872558594" />
                  <Point X="3.598873779297" Y="2.583768310547" />
                  <Point X="3.940405029297" Y="2.780951416016" />
                  <Point X="4.043950439453" Y="2.637046630859" />
                  <Point X="4.113213867188" Y="2.522588378906" />
                  <Point X="4.136884277344" Y="2.483472412109" />
                  <Point X="3.575476318359" Y="2.052687988281" />
                  <Point X="3.172951660156" Y="1.743819702148" />
                  <Point X="3.168138671875" Y="1.739869628906" />
                  <Point X="3.152120117188" Y="1.725217407227" />
                  <Point X="3.134668945312" Y="1.706603515625" />
                  <Point X="3.128576416016" Y="1.699422485352" />
                  <Point X="3.108073486328" Y="1.672674804688" />
                  <Point X="3.070794189453" Y="1.624041015625" />
                  <Point X="3.065635742188" Y="1.616602661133" />
                  <Point X="3.04973828125" Y="1.589370117188" />
                  <Point X="3.034762939453" Y="1.555545166016" />
                  <Point X="3.030140625" Y="1.542672485352" />
                  <Point X="3.022503173828" Y="1.515363037109" />
                  <Point X="3.008616699219" Y="1.465708007812" />
                  <Point X="3.006225585938" Y="1.454662109375" />
                  <Point X="3.002771972656" Y="1.432363647461" />
                  <Point X="3.001709472656" Y="1.421110961914" />
                  <Point X="3.000893310547" Y="1.397540527344" />
                  <Point X="3.001174804688" Y="1.386240966797" />
                  <Point X="3.003077880859" Y="1.363756103516" />
                  <Point X="3.004699462891" Y="1.352570678711" />
                  <Point X="3.010968994141" Y="1.322185424805" />
                  <Point X="3.022368408203" Y="1.26693762207" />
                  <Point X="3.024597900391" Y="1.258234375" />
                  <Point X="3.034683837891" Y="1.228608398438" />
                  <Point X="3.050286376953" Y="1.195371337891" />
                  <Point X="3.056918457031" Y="1.183526245117" />
                  <Point X="3.073970703125" Y="1.157607421875" />
                  <Point X="3.104976074219" Y="1.110480957031" />
                  <Point X="3.111738525391" Y="1.101425292969" />
                  <Point X="3.126291992188" Y="1.084180297852" />
                  <Point X="3.134083007812" Y="1.075991333008" />
                  <Point X="3.151326904297" Y="1.059901367188" />
                  <Point X="3.160034179688" Y="1.052696166992" />
                  <Point X="3.178243408203" Y="1.039370605469" />
                  <Point X="3.187745361328" Y="1.033250366211" />
                  <Point X="3.212456298828" Y="1.01933996582" />
                  <Point X="3.257387451172" Y="0.994047912598" />
                  <Point X="3.265478271484" Y="0.98998840332" />
                  <Point X="3.294677978516" Y="0.97808416748" />
                  <Point X="3.330274658203" Y="0.968021179199" />
                  <Point X="3.343670410156" Y="0.965257568359" />
                  <Point X="3.377081542969" Y="0.96084185791" />
                  <Point X="3.437831054688" Y="0.952812927246" />
                  <Point X="3.444029785156" Y="0.952199707031" />
                  <Point X="3.465716064453" Y="0.95122277832" />
                  <Point X="3.491217529297" Y="0.952032348633" />
                  <Point X="3.500603515625" Y="0.952797180176" />
                  <Point X="4.415236816406" Y="1.0732109375" />
                  <Point X="4.704703613281" Y="1.111319946289" />
                  <Point X="4.75268359375" Y="0.91423248291" />
                  <Point X="4.774509765625" Y="0.774046630859" />
                  <Point X="4.78387109375" Y="0.713920837402" />
                  <Point X="4.153293945312" Y="0.544958251953" />
                  <Point X="3.691991943359" Y="0.421352874756" />
                  <Point X="3.686031738281" Y="0.419544433594" />
                  <Point X="3.665626708984" Y="0.412137969971" />
                  <Point X="3.642381103516" Y="0.401619476318" />
                  <Point X="3.634004150391" Y="0.397316558838" />
                  <Point X="3.601178710938" Y="0.37834274292" />
                  <Point X="3.541494140625" Y="0.343843963623" />
                  <Point X="3.533881835938" Y="0.338945800781" />
                  <Point X="3.508773925781" Y="0.319870483398" />
                  <Point X="3.481993652344" Y="0.294350616455" />
                  <Point X="3.472796630859" Y="0.284226409912" />
                  <Point X="3.4531015625" Y="0.259130096436" />
                  <Point X="3.417290771484" Y="0.213498855591" />
                  <Point X="3.410854492188" Y="0.204208435059" />
                  <Point X="3.399130615234" Y="0.184928771973" />
                  <Point X="3.393843017578" Y="0.174939529419" />
                  <Point X="3.384069091797" Y="0.153475860596" />
                  <Point X="3.380005126953" Y="0.142928543091" />
                  <Point X="3.373158935547" Y="0.121427261353" />
                  <Point X="3.370376708984" Y="0.110473457336" />
                  <Point X="3.363811523438" Y="0.076192993164" />
                  <Point X="3.351874511719" Y="0.013862923622" />
                  <Point X="3.350603759766" Y="0.004969031811" />
                  <Point X="3.348584228516" Y="-0.026262882233" />
                  <Point X="3.350280029297" Y="-0.06294152832" />
                  <Point X="3.351874511719" Y="-0.076423110962" />
                  <Point X="3.358439697266" Y="-0.110703430176" />
                  <Point X="3.370376708984" Y="-0.173033493042" />
                  <Point X="3.373158935547" Y="-0.183987594604" />
                  <Point X="3.380005126953" Y="-0.205488723755" />
                  <Point X="3.384069091797" Y="-0.216035751343" />
                  <Point X="3.393843017578" Y="-0.237499572754" />
                  <Point X="3.399130371094" Y="-0.247488510132" />
                  <Point X="3.410854003906" Y="-0.266767883301" />
                  <Point X="3.417290283203" Y="-0.276058288574" />
                  <Point X="3.436985351562" Y="-0.301154754639" />
                  <Point X="3.472796142578" Y="-0.346786010742" />
                  <Point X="3.478717773438" Y="-0.353632904053" />
                  <Point X="3.501139648438" Y="-0.375805358887" />
                  <Point X="3.530176513672" Y="-0.398724945068" />
                  <Point X="3.541494873047" Y="-0.406404449463" />
                  <Point X="3.5743203125" Y="-0.425378082275" />
                  <Point X="3.634004882812" Y="-0.45987689209" />
                  <Point X="3.63949609375" Y="-0.462815155029" />
                  <Point X="3.659158447266" Y="-0.472016845703" />
                  <Point X="3.683028076172" Y="-0.481027862549" />
                  <Point X="3.691991943359" Y="-0.48391293335" />
                  <Point X="4.530752441406" Y="-0.708657958984" />
                  <Point X="4.784876464844" Y="-0.776750366211" />
                  <Point X="4.76161328125" Y="-0.931053100586" />
                  <Point X="4.733649902344" Y="-1.053591796875" />
                  <Point X="4.727801757812" Y="-1.079219726563" />
                  <Point X="3.971762695312" Y="-0.979685302734" />
                  <Point X="3.436781982422" Y="-0.909253845215" />
                  <Point X="3.428624511719" Y="-0.908535827637" />
                  <Point X="3.400098632812" Y="-0.908042541504" />
                  <Point X="3.366721435547" Y="-0.910840820312" />
                  <Point X="3.354480957031" Y="-0.912676330566" />
                  <Point X="3.290056396484" Y="-0.926679260254" />
                  <Point X="3.172916748047" Y="-0.952140014648" />
                  <Point X="3.157873535156" Y="-0.956742553711" />
                  <Point X="3.128752929688" Y="-0.968367431641" />
                  <Point X="3.114675292969" Y="-0.975389526367" />
                  <Point X="3.086848876953" Y="-0.992282043457" />
                  <Point X="3.074124023438" Y="-1.001530517578" />
                  <Point X="3.050374267578" Y="-1.022000976562" />
                  <Point X="3.039349609375" Y="-1.033222900391" />
                  <Point X="3.000408935547" Y="-1.080056274414" />
                  <Point X="2.92960546875" Y="-1.165211181641" />
                  <Point X="2.921326171875" Y="-1.176847290039" />
                  <Point X="2.90660546875" Y="-1.201229492188" />
                  <Point X="2.9001640625" Y="-1.213975708008" />
                  <Point X="2.888820800781" Y="-1.241360717773" />
                  <Point X="2.884362792969" Y="-1.254928466797" />
                  <Point X="2.877531005859" Y="-1.282578369141" />
                  <Point X="2.875157226562" Y="-1.296660766602" />
                  <Point X="2.869576171875" Y="-1.357312011719" />
                  <Point X="2.859428222656" Y="-1.467590942383" />
                  <Point X="2.859288574219" Y="-1.483321044922" />
                  <Point X="2.861607177734" Y="-1.514589355469" />
                  <Point X="2.864065429688" Y="-1.530127441406" />
                  <Point X="2.871796875" Y="-1.561748291016" />
                  <Point X="2.876785888672" Y="-1.576668212891" />
                  <Point X="2.889157470703" Y="-1.605479858398" />
                  <Point X="2.896540039062" Y="-1.619371826172" />
                  <Point X="2.932193603516" Y="-1.674828369141" />
                  <Point X="2.997020263672" Y="-1.775662231445" />
                  <Point X="3.001741943359" Y="-1.782353271484" />
                  <Point X="3.019793701172" Y="-1.804450195312" />
                  <Point X="3.043489257812" Y="-1.828120361328" />
                  <Point X="3.052796142578" Y="-1.836277709961" />
                  <Point X="3.831170898438" Y="-2.433546142578" />
                  <Point X="4.087170654297" Y="-2.629981933594" />
                  <Point X="4.045487792969" Y="-2.697431152344" />
                  <Point X="4.001274414063" Y="-2.760251953125" />
                  <Point X="3.324927001953" Y="-2.369762451172" />
                  <Point X="2.848454589844" Y="-2.094670654297" />
                  <Point X="2.841191894531" Y="-2.090885253906" />
                  <Point X="2.815025878906" Y="-2.079513183594" />
                  <Point X="2.783118652344" Y="-2.069325927734" />
                  <Point X="2.771107910156" Y="-2.066337646484" />
                  <Point X="2.694432373047" Y="-2.052490234375" />
                  <Point X="2.555017578125" Y="-2.027312011719" />
                  <Point X="2.539358886719" Y="-2.025807495117" />
                  <Point X="2.508006835938" Y="-2.025403198242" />
                  <Point X="2.492313476562" Y="-2.026503662109" />
                  <Point X="2.460140625" Y="-2.031461425781" />
                  <Point X="2.444844482422" Y="-2.035136230469" />
                  <Point X="2.415068603516" Y="-2.044959838867" />
                  <Point X="2.400588867188" Y="-2.051108642578" />
                  <Point X="2.336890380859" Y="-2.084632568359" />
                  <Point X="2.221070800781" Y="-2.145587646484" />
                  <Point X="2.208968505859" Y="-2.153170166016" />
                  <Point X="2.186037353516" Y="-2.170063476562" />
                  <Point X="2.175208496094" Y="-2.179374267578" />
                  <Point X="2.154248779297" Y="-2.200334228516" />
                  <Point X="2.144938232422" Y="-2.211162841797" />
                  <Point X="2.128045898438" Y="-2.234093017578" />
                  <Point X="2.120464111328" Y="-2.246194580078" />
                  <Point X="2.086939941406" Y="-2.309893066406" />
                  <Point X="2.025984985352" Y="-2.425712646484" />
                  <Point X="2.019836303711" Y="-2.440192382812" />
                  <Point X="2.010012329102" Y="-2.469968505859" />
                  <Point X="2.006337280273" Y="-2.485264892578" />
                  <Point X="2.001379516602" Y="-2.5174375" />
                  <Point X="2.000279174805" Y="-2.533130859375" />
                  <Point X="2.00068347168" Y="-2.564482910156" />
                  <Point X="2.002187866211" Y="-2.580141601562" />
                  <Point X="2.01603527832" Y="-2.656817138672" />
                  <Point X="2.041213500977" Y="-2.796231933594" />
                  <Point X="2.043015014648" Y="-2.804220947266" />
                  <Point X="2.051236083984" Y="-2.83154296875" />
                  <Point X="2.064069580078" Y="-2.862479736328" />
                  <Point X="2.069546630859" Y="-2.873578613281" />
                  <Point X="2.569730712891" Y="-3.739922607422" />
                  <Point X="2.735893066406" Y="-4.027724365234" />
                  <Point X="2.723754150391" Y="-4.036083496094" />
                  <Point X="2.197507324219" Y="-3.350265625" />
                  <Point X="1.833914550781" Y="-2.876422851562" />
                  <Point X="1.828654418945" Y="-2.870147216797" />
                  <Point X="1.808830688477" Y="-2.849625732422" />
                  <Point X="1.783251342773" Y="-2.828004150391" />
                  <Point X="1.773298706055" Y="-2.820647216797" />
                  <Point X="1.697676025391" Y="-2.772028808594" />
                  <Point X="1.56017565918" Y="-2.683628417969" />
                  <Point X="1.546283935547" Y="-2.676245849609" />
                  <Point X="1.517473022461" Y="-2.663874511719" />
                  <Point X="1.502553588867" Y="-2.658885742188" />
                  <Point X="1.470932617188" Y="-2.651154052734" />
                  <Point X="1.45539440918" Y="-2.648695800781" />
                  <Point X="1.424125244141" Y="-2.646376953125" />
                  <Point X="1.408394287109" Y="-2.646516357422" />
                  <Point X="1.325687744141" Y="-2.654127197266" />
                  <Point X="1.175307373047" Y="-2.667965332031" />
                  <Point X="1.161224975586" Y="-2.670339111328" />
                  <Point X="1.133575439453" Y="-2.677170898438" />
                  <Point X="1.120008422852" Y="-2.68162890625" />
                  <Point X="1.092623168945" Y="-2.692972167969" />
                  <Point X="1.079877563477" Y="-2.699413330078" />
                  <Point X="1.055495361328" Y="-2.714133789062" />
                  <Point X="1.043859008789" Y="-2.722413085938" />
                  <Point X="0.979995056152" Y="-2.775513671875" />
                  <Point X="0.863875244141" Y="-2.872063476562" />
                  <Point X="0.852653320312" Y="-2.883087890625" />
                  <Point X="0.832182128906" Y="-2.906838134766" />
                  <Point X="0.822933044434" Y="-2.919564208984" />
                  <Point X="0.806040710449" Y="-2.947390869141" />
                  <Point X="0.799018859863" Y="-2.961468017578" />
                  <Point X="0.787394348145" Y="-2.990588378906" />
                  <Point X="0.782791625977" Y="-3.005631591797" />
                  <Point X="0.763696838379" Y="-3.093483398438" />
                  <Point X="0.728977355957" Y="-3.253219482422" />
                  <Point X="0.727584472656" Y="-3.261289794922" />
                  <Point X="0.724724487305" Y="-3.289677734375" />
                  <Point X="0.7247421875" Y="-3.323170410156" />
                  <Point X="0.725554992676" Y="-3.335520019531" />
                  <Point X="0.833091674805" Y="-4.152341308594" />
                  <Point X="0.797203430176" Y="-4.018404785156" />
                  <Point X="0.655064941406" Y="-3.487936523438" />
                  <Point X="0.652606262207" Y="-3.480124511719" />
                  <Point X="0.642145141602" Y="-3.453580566406" />
                  <Point X="0.626787231445" Y="-3.423815917969" />
                  <Point X="0.620407592773" Y="-3.413210205078" />
                  <Point X="0.562312255859" Y="-3.329505371094" />
                  <Point X="0.456679992676" Y="-3.177309814453" />
                  <Point X="0.446670684814" Y="-3.165172851562" />
                  <Point X="0.424786712646" Y="-3.142717529297" />
                  <Point X="0.412912322998" Y="-3.132399169922" />
                  <Point X="0.386657196045" Y="-3.113155273438" />
                  <Point X="0.373242340088" Y="-3.104937744141" />
                  <Point X="0.345241210938" Y="-3.090829589844" />
                  <Point X="0.330654907227" Y="-3.084938720703" />
                  <Point X="0.240755279541" Y="-3.057037109375" />
                  <Point X="0.077295654297" Y="-3.006305419922" />
                  <Point X="0.063377269745" Y="-3.003109619141" />
                  <Point X="0.035217689514" Y="-2.998840087891" />
                  <Point X="0.020976644516" Y="-2.997766601562" />
                  <Point X="-0.008664708138" Y="-2.997766601562" />
                  <Point X="-0.022905010223" Y="-2.998840087891" />
                  <Point X="-0.051064441681" Y="-3.003109375" />
                  <Point X="-0.064983421326" Y="-3.006305175781" />
                  <Point X="-0.154882888794" Y="-3.034206542969" />
                  <Point X="-0.318342529297" Y="-3.084938476562" />
                  <Point X="-0.332929412842" Y="-3.090829589844" />
                  <Point X="-0.360930847168" Y="-3.104937988281" />
                  <Point X="-0.374345275879" Y="-3.113155273438" />
                  <Point X="-0.400600524902" Y="-3.132399169922" />
                  <Point X="-0.412475067139" Y="-3.142717529297" />
                  <Point X="-0.434358734131" Y="-3.165172607422" />
                  <Point X="-0.444367767334" Y="-3.177309326172" />
                  <Point X="-0.502463562012" Y="-3.261013916016" />
                  <Point X="-0.608095336914" Y="-3.413209716797" />
                  <Point X="-0.612470336914" Y="-3.420132324219" />
                  <Point X="-0.625976745605" Y="-3.445265136719" />
                  <Point X="-0.638777832031" Y="-3.476215820312" />
                  <Point X="-0.642752990723" Y="-3.487936523438" />
                  <Point X="-0.901195739746" Y="-4.452458007812" />
                  <Point X="-0.985425292969" Y="-4.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.666128750098" Y="-3.960984650107" />
                  <Point X="2.688131437065" Y="-3.944998762295" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.608276571158" Y="-3.885590260582" />
                  <Point X="2.640369807724" Y="-3.862273159356" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.550424392218" Y="-3.810195871057" />
                  <Point X="2.592608178383" Y="-3.779547556417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.998083719217" Y="-2.758409803929" />
                  <Point X="4.007266420289" Y="-2.751738181079" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.492572213278" Y="-3.734801481532" />
                  <Point X="2.54484653674" Y="-3.696821962415" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.908025365284" Y="-2.706414570201" />
                  <Point X="4.051207676187" Y="-2.602386532072" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.434720034339" Y="-3.659407092007" />
                  <Point X="2.497084883787" Y="-3.614096376631" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.817967011351" Y="-2.654419336473" />
                  <Point X="3.972602166216" Y="-2.542070320139" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.376867855399" Y="-3.584012702483" />
                  <Point X="2.449323230834" Y="-3.531370790847" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.727908657418" Y="-2.602424102745" />
                  <Point X="3.893996656245" Y="-2.481754108206" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.319015676459" Y="-3.508618312958" />
                  <Point X="2.401561577881" Y="-3.448645205062" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.637850303485" Y="-2.550428869017" />
                  <Point X="3.815391141613" Y="-2.421437899659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.261163497519" Y="-3.433223923433" />
                  <Point X="2.353799924928" Y="-3.365919619278" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.547791949552" Y="-2.498433635289" />
                  <Point X="3.73678560842" Y="-2.361121704598" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.203311318579" Y="-3.357829533908" />
                  <Point X="2.306038271975" Y="-3.283194033494" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.457733595619" Y="-2.446438401561" />
                  <Point X="3.658180075227" Y="-2.300805509537" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.145459117366" Y="-3.282435160565" />
                  <Point X="2.258276619022" Y="-3.200468447709" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.367675241686" Y="-2.394443167833" />
                  <Point X="3.579574542034" Y="-2.240489314476" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.825501412054" Y="-4.12401411078" />
                  <Point X="0.829025263544" Y="-4.121453882811" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.08760691367" Y="-3.207040789027" />
                  <Point X="2.210514966069" Y="-3.117742861925" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.277616904643" Y="-2.342447921834" />
                  <Point X="3.500969008841" Y="-2.180173119414" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.799164265556" Y="-4.025722709915" />
                  <Point X="0.814915387163" Y="-4.014278850204" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.029754709973" Y="-3.131646417489" />
                  <Point X="2.162753313116" Y="-3.035017276141" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.187558582861" Y="-2.290452664747" />
                  <Point X="3.422363475648" Y="-2.119856924353" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.772827161621" Y="-3.927431278126" />
                  <Point X="0.800805510783" Y="-3.907103817597" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.971902506276" Y="-3.056252045951" />
                  <Point X="2.114991660163" Y="-2.952291690356" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.097500261079" Y="-2.23845740766" />
                  <Point X="3.343757942455" Y="-2.059540729292" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.698384563269" Y="-1.075346879702" />
                  <Point X="4.734707653911" Y="-1.048956609603" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.74649006111" Y="-3.829139843849" />
                  <Point X="0.786695634402" Y="-3.79992878499" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.91405030258" Y="-2.980857674413" />
                  <Point X="2.067477729751" Y="-2.86938612361" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.007441939297" Y="-2.186462150572" />
                  <Point X="3.265152409262" Y="-1.999224534231" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.56155499" Y="-1.057332925909" />
                  <Point X="4.764842171633" Y="-0.909636143054" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.720152960598" Y="-3.730848409573" />
                  <Point X="0.772585758022" Y="-3.692753752382" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.856198098883" Y="-2.905463302875" />
                  <Point X="2.037191860954" Y="-2.773963637427" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.917383617515" Y="-2.134466893485" />
                  <Point X="3.186546876069" Y="-1.938908339169" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.424725416731" Y="-1.039318972115" />
                  <Point X="4.784723485523" Y="-0.777765065137" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.693815860087" Y="-3.632556975296" />
                  <Point X="0.758475881641" Y="-3.585578719775" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.791542136578" Y="-2.835012151316" />
                  <Point X="2.018444603885" Y="-2.670157859108" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.825269562749" Y="-2.083965213837" />
                  <Point X="3.107941342876" Y="-1.878592144108" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.287895843462" Y="-1.021305018321" />
                  <Point X="4.66770820143" Y="-0.745355187595" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.019788372833" Y="-4.76013686882" />
                  <Point X="-0.974899268689" Y="-4.727523025615" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.667478759575" Y="-3.53426554102" />
                  <Point X="0.744366005261" Y="-3.478403687168" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.707719914315" Y="-2.77848610272" />
                  <Point X="2.00078699128" Y="-2.565560407746" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.704471951995" Y="-2.054303357469" />
                  <Point X="3.031824330178" Y="-1.816467933075" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.151066270192" Y="-1.003291064528" />
                  <Point X="4.549631380773" Y="-0.713716561512" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.141192051562" Y="-4.73091534661" />
                  <Point X="-0.935828800624" Y="-4.581710211115" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.635264006906" Y="-3.440244471001" />
                  <Point X="0.73025612888" Y="-3.371228654561" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.621972901297" Y="-2.723358496464" />
                  <Point X="2.023458633157" Y="-2.431662037881" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.575025304317" Y="-2.030925394252" />
                  <Point X="2.974524840471" Y="-1.740671991318" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.014236696923" Y="-0.985277110734" />
                  <Point X="4.43155450806" Y="-0.68207797325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.12409730542" Y="-4.60106882867" />
                  <Point X="-0.896758338588" Y="-4.435897400994" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.583681650199" Y="-3.360294788981" />
                  <Point X="0.728679019242" Y="-3.254948033922" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.533028441013" Y="-2.670553971629" />
                  <Point X="2.123267266818" Y="-2.241720363002" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.346286140469" Y="-2.079687666746" />
                  <Point X="2.923066669603" Y="-1.660632083005" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.877407106283" Y="-0.967263169561" />
                  <Point X="4.31347762544" Y="-0.650439392185" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.125871960587" Y="-4.484931733259" />
                  <Point X="-0.857687923604" Y="-4.290084625059" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.529501991012" Y="-3.28223215767" />
                  <Point X="0.758896841444" Y="-3.115567043126" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.403923460289" Y="-2.646927772839" />
                  <Point X="2.876839029947" Y="-1.576791971322" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.740577507824" Y="-0.94924923407" />
                  <Point X="4.195400742819" Y="-0.61880081112" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.146280122746" Y="-4.382332673123" />
                  <Point X="-0.81861750862" Y="-4.144271849124" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.475322205039" Y="-3.204169618476" />
                  <Point X="0.794789427324" Y="-2.972063095182" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.218860328565" Y="-2.66395755054" />
                  <Point X="2.85938869002" Y="-1.472043927544" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.603747909364" Y="-0.931235298578" />
                  <Point X="4.077323860199" Y="-0.587162230056" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.166688369635" Y="-4.279733674548" />
                  <Point X="-0.779547093636" Y="-3.998459073189" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.412698156641" Y="-3.13224219505" />
                  <Point X="2.870571771698" Y="-1.346492485248" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.466918310904" Y="-0.913221363086" />
                  <Point X="3.959246977579" Y="-0.555523648991" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.188298465852" Y="-4.178007870622" />
                  <Point X="-0.740476678653" Y="-3.852646297254" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.320514096048" Y="-3.081791377612" />
                  <Point X="2.905282633895" Y="-1.203847109816" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.285369228927" Y="-0.927698034201" />
                  <Point X="3.841170094959" Y="-0.523885067926" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.232407132678" Y="-4.092628235062" />
                  <Point X="-0.701406263669" Y="-3.706833521319" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.207267133139" Y="-3.046643654471" />
                  <Point X="3.723093212339" Y="-0.492246486862" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.300793446709" Y="-4.024887342676" />
                  <Point X="-0.662335848685" Y="-3.561020745384" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.094020029226" Y="-3.011496033775" />
                  <Point X="3.618420792659" Y="-0.450868993406" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.37402375539" Y="-3.960665818409" />
                  <Point X="-0.595462240231" Y="-3.395007766978" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.058368631239" Y="-3.004786418526" />
                  <Point X="3.529405215188" Y="-0.398116138231" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.447254055761" Y="-3.896444288105" />
                  <Point X="-0.418870314662" Y="-3.149279765088" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.350844092641" Y="-3.09985582177" />
                  <Point X="3.460317259417" Y="-0.330885018409" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.524336925743" Y="-3.835021813465" />
                  <Point X="3.40352716267" Y="-0.254718981003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.709210731383" Y="0.693915659785" />
                  <Point X="4.779082006536" Y="0.74468011267" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.633219147779" Y="-3.796702920456" />
                  <Point X="3.368417140192" Y="-0.16280144763" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.453152852778" Y="0.62530517921" />
                  <Point X="4.762657259829" Y="0.850173293538" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.781092908635" Y="-3.786713038631" />
                  <Point X="3.350083696045" Y="-0.058695016625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.197094974173" Y="0.556694698635" />
                  <Point X="4.743143121536" Y="0.953421900033" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.929342705219" Y="-3.776996362755" />
                  <Point X="3.362145753029" Y="0.067495038611" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.941037179324" Y="0.488084278912" />
                  <Point X="4.718852571824" Y="1.053200240501" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.567091314254" Y="-4.122921391533" />
                  <Point X="3.438265888552" Y="0.240226012169" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.68412287546" Y="0.418851568965" />
                  <Point X="4.622290185021" Y="1.100470017745" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.654350700315" Y="-4.068892588611" />
                  <Point X="4.424898331751" Y="1.074482899525" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.741610086376" Y="-4.014863785689" />
                  <Point X="4.22750645925" Y="1.048495767333" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.820678701097" Y="-3.954884039053" />
                  <Point X="4.030114585759" Y="1.022508634422" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.899145596579" Y="-3.894467117798" />
                  <Point X="3.832722712269" Y="0.996521501511" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.977612514752" Y="-3.83405021303" />
                  <Point X="3.635330838778" Y="0.9705343686" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.868917001613" Y="-3.637651842269" />
                  <Point X="3.448212354222" Y="0.952011289657" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.752133783762" Y="-3.43537741008" />
                  <Point X="3.314688809803" Y="0.972427214009" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.63535056591" Y="-3.233102977891" />
                  <Point X="3.215374258226" Y="1.017697426501" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.518567306858" Y="-3.030828515768" />
                  <Point X="3.134043066343" Y="1.076033314607" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.401784033747" Y="-2.828554043431" />
                  <Point X="3.077439574091" Y="1.152334928115" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.316264690176" Y="-2.648994145497" />
                  <Point X="3.031930178039" Y="1.236696874321" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.321429540409" Y="-2.53532017098" />
                  <Point X="3.00797085986" Y="1.336715868585" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.366461587893" Y="-2.450611410737" />
                  <Point X="3.005913580151" Y="1.452647625247" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.439070545505" Y="-2.385938448494" />
                  <Point X="3.06245481565" Y="1.611153695285" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.548915419564" Y="-2.348318963119" />
                  <Point X="4.12607061702" Y="2.501342266302" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.00799956345" Y="-2.564436659722" />
                  <Point X="4.076712149339" Y="2.582907698277" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.793234727187" Y="-3.0175169428" />
                  <Point X="4.025293146327" Y="2.662976063703" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.850787239275" Y="-2.941904832563" />
                  <Point X="3.969806917882" Y="2.740089416882" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.908339751364" Y="-2.866292722327" />
                  <Point X="3.570394679174" Y="2.567325897117" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.965892263452" Y="-2.79068061209" />
                  <Point X="2.783313738195" Y="2.112904578376" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.020509860784" Y="-2.712936161467" />
                  <Point X="2.432448572239" Y="1.975412571575" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.069368428083" Y="-2.631007530604" />
                  <Point X="2.296613783273" Y="1.994149278471" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.11822704579" Y="-2.549078936365" />
                  <Point X="2.202890279457" Y="2.043481624938" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.167085757978" Y="-2.467150410771" />
                  <Point X="2.126085396947" Y="2.105106069299" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.979846112429" Y="-1.487143859483" />
                  <Point X="2.069890668273" Y="2.18170466693" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.948979249807" Y="-1.347291313219" />
                  <Point X="2.028643268886" Y="2.269163134968" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.9725531261" Y="-1.246992279034" />
                  <Point X="2.013135368989" Y="2.375322444035" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.031860591342" Y="-1.172655216898" />
                  <Point X="2.032212289175" Y="2.506609095716" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.119665314205" Y="-1.119022624355" />
                  <Point X="2.114521009452" Y="2.683836339286" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.257822053869" Y="-1.101972913389" />
                  <Point X="2.231304111387" Y="2.886110687256" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.455213910913" Y="-1.127960034352" />
                  <Point X="2.348087213322" Y="3.088385035227" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.652605767958" Y="-1.153947155314" />
                  <Point X="2.464870369418" Y="3.290659422547" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.849997625003" Y="-1.179934276276" />
                  <Point X="2.581653538762" Y="3.492933819494" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.047389482047" Y="-1.205921397239" />
                  <Point X="2.698436708106" Y="3.69520821644" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.244781339092" Y="-1.231908518201" />
                  <Point X="2.81521987745" Y="3.897482613386" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.442173257659" Y="-1.257895683862" />
                  <Point X="2.737892818184" Y="3.958727674126" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.63956519694" Y="-1.283882864573" />
                  <Point X="2.656215576985" Y="4.016812142688" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.683482608645" Y="-1.198364274034" />
                  <Point X="2.569148303973" Y="4.070980523909" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.708781852807" Y="-1.099318792981" />
                  <Point X="-3.693773208917" Y="-0.361871846903" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.29827935695" Y="-0.074528743884" />
                  <Point X="2.479451301856" Y="4.123238295099" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.734081096968" Y="-1.000273311929" />
                  <Point X="-3.949831021877" Y="-0.430482279783" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.304325178082" Y="0.03850516781" />
                  <Point X="2.389754299738" Y="4.175496066289" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.751958210888" Y="-0.895835337607" />
                  <Point X="-4.205888944423" Y="-0.499092792284" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.342149433913" Y="0.128450695221" />
                  <Point X="2.300057156771" Y="4.227753735145" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.767172148108" Y="-0.789462452153" />
                  <Point X="-4.46194692137" Y="-0.567703344308" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.408927220887" Y="0.197360250921" />
                  <Point X="2.206501445711" Y="4.277207990185" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.782385818237" Y="-0.683089372646" />
                  <Point X="-4.718004898316" Y="-0.636313896332" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.501706108677" Y="0.247378901102" />
                  <Point X="2.108171588538" Y="4.323193625038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.619311657811" Y="0.27935992599" />
                  <Point X="2.009841731364" Y="4.369179259891" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.737388518441" Y="0.310998523031" />
                  <Point X="1.911512090983" Y="4.415165052253" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.855465379071" Y="0.342637120073" />
                  <Point X="1.808839056634" Y="4.457995184182" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.973542239701" Y="0.374275717114" />
                  <Point X="1.701034109522" Y="4.497096763238" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.091619100331" Y="0.405914314155" />
                  <Point X="1.593229162409" Y="4.536198342293" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.209695960961" Y="0.437552911197" />
                  <Point X="1.485424112513" Y="4.575299846673" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.327772821591" Y="0.469191508238" />
                  <Point X="1.368099866407" Y="4.607485250173" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.445849682221" Y="0.500830105279" />
                  <Point X="1.246788260116" Y="4.636773666924" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.563926567632" Y="0.532468684317" />
                  <Point X="-3.575616628874" Y="1.250517885675" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.514703540751" Y="1.294773834708" />
                  <Point X="1.125476653825" Y="4.666062083676" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.68200345801" Y="0.564107259745" />
                  <Point X="-3.795836657591" Y="1.207945127156" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.426274623558" Y="1.476447661617" />
                  <Point X="1.0041650732" Y="4.695350519075" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.783032778945" Y="0.608131619372" />
                  <Point X="-3.932666173557" Y="1.225959122583" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.431546093505" Y="1.590044172378" />
                  <Point X="0.002621918273" Y="4.08511328125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.23526199597" Y="4.254136191415" />
                  <Point X="0.882853544082" Y="4.724638991895" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.763563634941" Y="0.739703238338" />
                  <Point X="-4.069495689522" Y="1.24397311801" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.471364227288" Y="1.678541062661" />
                  <Point X="-0.113743742302" Y="4.117995137905" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.275179412262" Y="4.400564349822" />
                  <Point X="0.750632925045" Y="4.746001546948" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.744094490937" Y="0.871274857303" />
                  <Point X="-4.206325216295" Y="1.261987105585" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.527813461018" Y="1.754954751545" />
                  <Point X="-0.185419852305" Y="4.183345853609" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.314249735958" Y="4.546377059433" />
                  <Point X="0.609371130897" Y="4.76079530378" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.717684981984" Y="1.007888946564" />
                  <Point X="-4.343154761334" Y="1.280001079889" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.604845536633" Y="1.816414130453" />
                  <Point X="-0.227865226602" Y="4.269933941928" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.353320059655" Y="4.692189769044" />
                  <Point X="0.46810933675" Y="4.775589060613" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.678063939867" Y="1.154101776528" />
                  <Point X="-4.479984306373" Y="1.298015054193" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.683451090284" Y="1.876730310651" />
                  <Point X="-0.25420225042" Y="4.368225431925" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.638443145308" Y="1.300314426631" />
                  <Point X="-4.616813851413" Y="1.316029028497" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.762056643935" Y="1.937046490848" />
                  <Point X="-0.280539274239" Y="4.466516921922" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.840662197586" Y="1.997362671046" />
                  <Point X="-0.306876346899" Y="4.564808376434" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.919267751237" Y="2.057678851244" />
                  <Point X="-2.993992578835" Y="2.729930614101" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.808954687518" Y="2.864368511436" />
                  <Point X="-0.333213472476" Y="4.663099792499" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.997873304887" Y="2.117995031442" />
                  <Point X="-3.154050268476" Y="2.731068353505" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.748642513833" Y="3.025614328437" />
                  <Point X="-0.359550598054" Y="4.761391208564" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.076478858538" Y="2.178311211639" />
                  <Point X="-3.264406742252" Y="2.768316139929" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.75725713789" Y="3.136781895559" />
                  <Point X="-0.516394068408" Y="4.764864214974" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.155084268678" Y="2.238627496104" />
                  <Point X="-3.354465105027" Y="2.820311367232" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.788818467332" Y="3.231277705341" />
                  <Point X="-0.709051871709" Y="4.742316585386" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.218416431751" Y="2.310040444104" />
                  <Point X="-3.444523467803" Y="2.872306594535" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.836580149074" Y="3.314003270209" />
                  <Point X="-1.851648756992" Y="4.029597813724" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.486804716542" Y="4.2946725252" />
                  <Point X="-0.901709300303" Y="4.719769228039" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.099407165771" Y="2.513932194927" />
                  <Point X="-3.53458180527" Y="2.924301840227" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.884341830815" Y="3.396728835077" />
                  <Point X="-1.963438484678" Y="4.065804280229" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.462681513215" Y="4.429625516192" />
                  <Point X="-1.154106451458" Y="4.65381842164" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.963633084249" Y="2.730004297216" />
                  <Point X="-3.624640121182" Y="2.976297101579" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.932103512557" Y="3.479454399945" />
                  <Point X="-2.056113459318" Y="4.115898427733" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.475943445584" Y="4.537416616185" />
                  <Point X="-1.417290003039" Y="4.580030836607" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.753498112194" Y="3.000102748897" />
                  <Point X="-3.714698437094" Y="3.028292362931" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.979865194299" Y="3.562179964814" />
                  <Point X="-2.126468195686" Y="4.182209177578" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.02762687604" Y="3.644905529682" />
                  <Point X="-2.184320427087" Y="4.257603528987" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="0.001626220703" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.61367755127" Y="-4.067580566406" />
                  <Point X="0.471539031982" Y="-3.537112304688" />
                  <Point X="0.464318664551" Y="-3.521544189453" />
                  <Point X="0.406223175049" Y="-3.437839355469" />
                  <Point X="0.300591033936" Y="-3.285643798828" />
                  <Point X="0.274335754395" Y="-3.266399902344" />
                  <Point X="0.184436233521" Y="-3.238498291016" />
                  <Point X="0.020976625443" Y="-3.187766601562" />
                  <Point X="-0.008664756775" Y="-3.187766601562" />
                  <Point X="-0.098564422607" Y="-3.21566796875" />
                  <Point X="-0.262023895264" Y="-3.266399902344" />
                  <Point X="-0.288279144287" Y="-3.285643798828" />
                  <Point X="-0.346374816895" Y="-3.369348388672" />
                  <Point X="-0.45200680542" Y="-3.521544189453" />
                  <Point X="-0.459227142334" Y="-3.537112304688" />
                  <Point X="-0.71766986084" Y="-4.501633789062" />
                  <Point X="-0.84774395752" Y="-4.987076660156" />
                  <Point X="-0.908339599609" Y="-4.975314941406" />
                  <Point X="-1.100246582031" Y="-4.938065429688" />
                  <Point X="-1.203709472656" Y="-4.9114453125" />
                  <Point X="-1.351589599609" Y="-4.873396972656" />
                  <Point X="-1.326260375977" Y="-4.681002929688" />
                  <Point X="-1.309150146484" Y="-4.551038574219" />
                  <Point X="-1.309683227539" Y="-4.5347578125" />
                  <Point X="-1.33116015625" Y="-4.426785644531" />
                  <Point X="-1.370210693359" Y="-4.230466308594" />
                  <Point X="-1.386282714844" Y="-4.20262890625" />
                  <Point X="-1.46905090332" Y="-4.13004296875" />
                  <Point X="-1.619543457031" Y="-3.998063964844" />
                  <Point X="-1.649240722656" Y="-3.985762939453" />
                  <Point X="-1.759092529297" Y="-3.978562744141" />
                  <Point X="-1.958829833984" Y="-3.965471435547" />
                  <Point X="-1.989878540039" Y="-3.973791015625" />
                  <Point X="-2.081412841797" Y="-4.034952392578" />
                  <Point X="-2.247844970703" Y="-4.146159179688" />
                  <Point X="-2.259734130859" Y="-4.157294433594" />
                  <Point X="-2.404829589844" Y="-4.34638671875" />
                  <Point X="-2.457095214844" Y="-4.414500488281" />
                  <Point X="-2.574537841797" Y="-4.341782714844" />
                  <Point X="-2.855831054688" Y="-4.16761328125" />
                  <Point X="-2.999101318359" Y="-4.057299804688" />
                  <Point X="-3.228581054688" Y="-3.880608154297" />
                  <Point X="-2.770182128906" Y="-3.086637939453" />
                  <Point X="-2.506033691406" Y="-2.629119628906" />
                  <Point X="-2.499762695312" Y="-2.597593261719" />
                  <Point X="-2.513979736328" Y="-2.568764404297" />
                  <Point X="-2.531328369141" Y="-2.551415771484" />
                  <Point X="-2.560157226562" Y="-2.537198730469" />
                  <Point X="-2.59168359375" Y="-2.543469726562" />
                  <Point X="-3.423564453125" Y="-3.023756347656" />
                  <Point X="-3.842959472656" Y="-3.265894042969" />
                  <Point X="-3.939464355469" Y="-3.139106201172" />
                  <Point X="-4.161703613281" Y="-2.847129394531" />
                  <Point X="-4.264414550781" Y="-2.674898681641" />
                  <Point X="-4.431020019531" Y="-2.395527099609" />
                  <Point X="-3.6259609375" Y="-1.777783569336" />
                  <Point X="-3.163786865234" Y="-1.423144897461" />
                  <Point X="-3.145822021484" Y="-1.396013427734" />
                  <Point X="-3.138117431641" Y="-1.366266113281" />
                  <Point X="-3.140326416016" Y="-1.334595581055" />
                  <Point X="-3.161158935547" Y="-1.310639160156" />
                  <Point X="-3.187641601562" Y="-1.295052734375" />
                  <Point X="-3.219529296875" Y="-1.288571044922" />
                  <Point X="-4.269700195312" Y="-1.426828613281" />
                  <Point X="-4.803283203125" Y="-1.497076293945" />
                  <Point X="-4.840473632812" Y="-1.351477050781" />
                  <Point X="-4.927393066406" Y="-1.011191589355" />
                  <Point X="-4.954567871094" Y="-0.821188964844" />
                  <Point X="-4.998396484375" Y="-0.514742126465" />
                  <Point X="-4.083949707031" Y="-0.26971685791" />
                  <Point X="-3.557463867188" Y="-0.128645431519" />
                  <Point X="-3.532048095703" Y="-0.114590187073" />
                  <Point X="-3.514142822266" Y="-0.102162895203" />
                  <Point X="-3.50232421875" Y="-0.090645324707" />
                  <Point X="-3.491616455078" Y="-0.065331428528" />
                  <Point X="-3.485647949219" Y="-0.046100891113" />
                  <Point X="-3.483400878906" Y="-0.031280042648" />
                  <Point X="-3.488930419922" Y="-0.005882709026" />
                  <Point X="-3.494898925781" Y="0.013347833633" />
                  <Point X="-3.50232421875" Y="0.02808523941" />
                  <Point X="-3.523990478516" Y="0.046437538147" />
                  <Point X="-3.541895751953" Y="0.05886498642" />
                  <Point X="-3.557463867188" Y="0.066085334778" />
                  <Point X="-4.514744140625" Y="0.322587799072" />
                  <Point X="-4.998186523438" Y="0.452125793457" />
                  <Point X="-4.973663085938" Y="0.617852355957" />
                  <Point X="-4.917645019531" Y="0.996418518066" />
                  <Point X="-4.862942382812" Y="1.198287353516" />
                  <Point X="-4.773516113281" Y="1.528298583984" />
                  <Point X="-4.130664550781" Y="1.443665649414" />
                  <Point X="-3.753266357422" Y="1.393980102539" />
                  <Point X="-3.731704589844" Y="1.395866699219" />
                  <Point X="-3.709908691406" Y="1.402738891602" />
                  <Point X="-3.670278564453" Y="1.41523425293" />
                  <Point X="-3.651534423828" Y="1.426056152344" />
                  <Point X="-3.639120117188" Y="1.443785766602" />
                  <Point X="-3.630374511719" Y="1.464899780273" />
                  <Point X="-3.61447265625" Y="1.503290039063" />
                  <Point X="-3.610714111328" Y="1.524605224609" />
                  <Point X="-3.616315917969" Y="1.54551171875" />
                  <Point X="-3.626868408203" Y="1.565783081055" />
                  <Point X="-3.646055664062" Y="1.602641479492" />
                  <Point X="-3.659968261719" Y="1.619221679688" />
                  <Point X="-4.209066894531" Y="2.040559570312" />
                  <Point X="-4.47610546875" Y="2.245466064453" />
                  <Point X="-4.377681152344" Y="2.414091064453" />
                  <Point X="-4.160014648438" Y="2.787006591797" />
                  <Point X="-4.015110351562" Y="2.973260253906" />
                  <Point X="-3.774671142578" Y="3.282310791016" />
                  <Point X="-3.390944335938" Y="3.060765869141" />
                  <Point X="-3.159156982422" Y="2.926943359375" />
                  <Point X="-3.138514892578" Y="2.920434814453" />
                  <Point X="-3.108159423828" Y="2.917779052734" />
                  <Point X="-3.052965576172" Y="2.912950195312" />
                  <Point X="-3.031506835938" Y="2.915775390625" />
                  <Point X="-3.013252441406" Y="2.927404541016" />
                  <Point X="-2.991705810547" Y="2.948951171875" />
                  <Point X="-2.952528808594" Y="2.988127929688" />
                  <Point X="-2.940899414062" Y="3.006382324219" />
                  <Point X="-2.93807421875" Y="3.027841308594" />
                  <Point X="-2.940729980469" Y="3.058197021484" />
                  <Point X="-2.945558837891" Y="3.113390869141" />
                  <Point X="-2.952067382812" Y="3.134032958984" />
                  <Point X="-3.195389648438" Y="3.555479003906" />
                  <Point X="-3.307278564453" Y="3.749276611328" />
                  <Point X="-3.131982421875" Y="3.883674560547" />
                  <Point X="-2.752876220703" Y="4.174331542969" />
                  <Point X="-2.524656738281" Y="4.301125" />
                  <Point X="-2.141548583984" Y="4.513971679688" />
                  <Point X="-2.038784057617" Y="4.380046386719" />
                  <Point X="-1.967826904297" Y="4.287573242188" />
                  <Point X="-1.951246826172" Y="4.273660644531" />
                  <Point X="-1.917461303711" Y="4.256072753906" />
                  <Point X="-1.856030761719" Y="4.22409375" />
                  <Point X="-1.835124267578" Y="4.2184921875" />
                  <Point X="-1.813808959961" Y="4.222250488281" />
                  <Point X="-1.778619018555" Y="4.236827148438" />
                  <Point X="-1.714635009766" Y="4.263330078125" />
                  <Point X="-1.696905395508" Y="4.275744140625" />
                  <Point X="-1.686083496094" Y="4.29448828125" />
                  <Point X="-1.674629638672" Y="4.330814941406" />
                  <Point X="-1.653804077148" Y="4.396864746094" />
                  <Point X="-1.651917602539" Y="4.418426757812" />
                  <Point X="-1.679580566406" Y="4.628548339844" />
                  <Point X="-1.689137573242" Y="4.701141113281" />
                  <Point X="-1.458870727539" Y="4.765699707031" />
                  <Point X="-0.968094177246" Y="4.903296386719" />
                  <Point X="-0.691424438477" Y="4.935676269531" />
                  <Point X="-0.22419960022" Y="4.990358398438" />
                  <Point X="-0.110709388733" Y="4.566807128906" />
                  <Point X="-0.042140380859" Y="4.310903320312" />
                  <Point X="-0.024282125473" Y="4.284176757813" />
                  <Point X="0.006155934334" Y="4.273844238281" />
                  <Point X="0.036594028473" Y="4.284176757813" />
                  <Point X="0.054452251434" Y="4.310903320312" />
                  <Point X="0.179127685547" Y="4.776198730469" />
                  <Point X="0.236648391724" Y="4.990868652344" />
                  <Point X="0.431685913086" Y="4.970442871094" />
                  <Point X="0.860205810547" Y="4.925565429688" />
                  <Point X="1.089103027344" Y="4.870302734375" />
                  <Point X="1.508455932617" Y="4.769058105469" />
                  <Point X="1.657461914062" Y="4.715012695312" />
                  <Point X="1.931033691406" Y="4.615786132813" />
                  <Point X="2.075100830078" Y="4.548410644531" />
                  <Point X="2.33869921875" Y="4.425134277344" />
                  <Point X="2.477877441406" Y="4.344048828125" />
                  <Point X="2.732523681641" Y="4.19569140625" />
                  <Point X="2.863783935547" Y="4.102346191406" />
                  <Point X="3.068740234375" Y="3.956592773438" />
                  <Point X="2.535585449219" Y="3.033141357422" />
                  <Point X="2.229853515625" Y="2.503597900391" />
                  <Point X="2.224852050781" Y="2.491514648438" />
                  <Point X="2.217233886719" Y="2.463026611328" />
                  <Point X="2.203382324219" Y="2.411228271484" />
                  <Point X="2.202044677734" Y="2.392325927734" />
                  <Point X="2.205015136719" Y="2.367691894531" />
                  <Point X="2.210416015625" Y="2.322901123047" />
                  <Point X="2.218682128906" Y="2.300812255859" />
                  <Point X="2.233924804688" Y="2.278348388672" />
                  <Point X="2.261639892578" Y="2.237503662109" />
                  <Point X="2.274939941406" Y="2.224203857422" />
                  <Point X="2.297403808594" Y="2.208961181641" />
                  <Point X="2.338248535156" Y="2.181246337891" />
                  <Point X="2.360336914062" Y="2.172980224609" />
                  <Point X="2.384970947266" Y="2.170009765625" />
                  <Point X="2.42976171875" Y="2.164608642578" />
                  <Point X="2.448664306641" Y="2.165946289062" />
                  <Point X="2.47715234375" Y="2.173564453125" />
                  <Point X="2.528950683594" Y="2.187416015625" />
                  <Point X="2.541033935547" Y="2.192417480469" />
                  <Point X="3.503873779297" Y="2.748313232422" />
                  <Point X="3.994248046875" Y="3.031430908203" />
                  <Point X="4.051544433594" Y="2.951802001953" />
                  <Point X="4.202591796875" Y="2.741880126953" />
                  <Point X="4.275767578125" Y="2.620956542969" />
                  <Point X="4.387512695312" Y="2.436296142578" />
                  <Point X="3.691140869141" Y="1.901950805664" />
                  <Point X="3.288616210938" Y="1.593082641602" />
                  <Point X="3.279371337891" Y="1.583833374023" />
                  <Point X="3.258868408203" Y="1.557085693359" />
                  <Point X="3.221589111328" Y="1.508451904297" />
                  <Point X="3.213119628906" Y="1.4915" />
                  <Point X="3.205482177734" Y="1.464190551758" />
                  <Point X="3.191595703125" Y="1.414535522461" />
                  <Point X="3.190779541016" Y="1.390965332031" />
                  <Point X="3.197049072266" Y="1.360580078125" />
                  <Point X="3.208448486328" Y="1.305332519531" />
                  <Point X="3.215646484375" Y="1.287954956055" />
                  <Point X="3.232698730469" Y="1.262036132812" />
                  <Point X="3.263704101562" Y="1.214909667969" />
                  <Point X="3.280947998047" Y="1.198819824219" />
                  <Point X="3.305658935547" Y="1.184909545898" />
                  <Point X="3.350590087891" Y="1.159617431641" />
                  <Point X="3.368565429688" Y="1.153619628906" />
                  <Point X="3.4019765625" Y="1.149203857422" />
                  <Point X="3.462726074219" Y="1.141175048828" />
                  <Point X="3.475803710938" Y="1.14117175293" />
                  <Point X="4.390437011719" Y="1.261585571289" />
                  <Point X="4.848975097656" Y="1.32195324707" />
                  <Point X="4.874314453125" Y="1.217867675781" />
                  <Point X="4.939188476562" Y="0.951385620117" />
                  <Point X="4.962248046875" Y="0.803276062012" />
                  <Point X="4.997858398438" Y="0.574556152344" />
                  <Point X="4.202469726562" Y="0.361432373047" />
                  <Point X="3.741167724609" Y="0.237826919556" />
                  <Point X="3.729086914062" Y="0.232819412231" />
                  <Point X="3.696261474609" Y="0.21384564209" />
                  <Point X="3.636576904297" Y="0.179346878052" />
                  <Point X="3.622264892578" Y="0.166926574707" />
                  <Point X="3.602569824219" Y="0.141830276489" />
                  <Point X="3.566759033203" Y="0.096199043274" />
                  <Point X="3.556985107422" Y="0.074735275269" />
                  <Point X="3.550419921875" Y="0.040454807281" />
                  <Point X="3.538482910156" Y="-0.021875303268" />
                  <Point X="3.538482910156" Y="-0.04068478775" />
                  <Point X="3.545048095703" Y="-0.074965110779" />
                  <Point X="3.556985107422" Y="-0.137295227051" />
                  <Point X="3.566759033203" Y="-0.158758987427" />
                  <Point X="3.586454101562" Y="-0.183855438232" />
                  <Point X="3.622264892578" Y="-0.229486679077" />
                  <Point X="3.636576904297" Y="-0.241906967163" />
                  <Point X="3.66940234375" Y="-0.260880584717" />
                  <Point X="3.729086914062" Y="-0.295379486084" />
                  <Point X="3.741167724609" Y="-0.300387023926" />
                  <Point X="4.579928222656" Y="-0.525132141113" />
                  <Point X="4.998067871094" Y="-0.637172363281" />
                  <Point X="4.984654296875" Y="-0.726141662598" />
                  <Point X="4.948431640625" Y="-0.966399108887" />
                  <Point X="4.918888183594" Y="-1.095862915039" />
                  <Point X="4.874545410156" Y="-1.290178344727" />
                  <Point X="3.946962890625" Y="-1.168059814453" />
                  <Point X="3.411982177734" Y="-1.097628295898" />
                  <Point X="3.3948359375" Y="-1.098341186523" />
                  <Point X="3.330411376953" Y="-1.112344116211" />
                  <Point X="3.213271728516" Y="-1.137804931641" />
                  <Point X="3.1854453125" Y="-1.154697265625" />
                  <Point X="3.146504638672" Y="-1.201530761719" />
                  <Point X="3.075701171875" Y="-1.286685424805" />
                  <Point X="3.064357910156" Y="-1.314070556641" />
                  <Point X="3.058776855469" Y="-1.374721923828" />
                  <Point X="3.04862890625" Y="-1.485000854492" />
                  <Point X="3.056360351562" Y="-1.516621826172" />
                  <Point X="3.092013916016" Y="-1.572078491211" />
                  <Point X="3.156840576172" Y="-1.672912353516" />
                  <Point X="3.168460693359" Y="-1.685540649414" />
                  <Point X="3.946835449219" Y="-2.282808837891" />
                  <Point X="4.33907421875" Y="-2.583784179688" />
                  <Point X="4.306240722656" Y="-2.636913818359" />
                  <Point X="4.204133300781" Y="-2.802139404297" />
                  <Point X="4.143033691406" Y="-2.888953369141" />
                  <Point X="4.056688476562" Y="-3.011638427734" />
                  <Point X="3.229927001953" Y="-2.534307373047" />
                  <Point X="2.753454589844" Y="-2.259215576172" />
                  <Point X="2.737340820313" Y="-2.253312744141" />
                  <Point X="2.660665283203" Y="-2.239465332031" />
                  <Point X="2.521250488281" Y="-2.214287109375" />
                  <Point X="2.489077636719" Y="-2.219244873047" />
                  <Point X="2.425379150391" Y="-2.252768798828" />
                  <Point X="2.309559570312" Y="-2.313723876953" />
                  <Point X="2.288599853516" Y="-2.334683837891" />
                  <Point X="2.255075683594" Y="-2.398382324219" />
                  <Point X="2.194120849609" Y="-2.514201904297" />
                  <Point X="2.189163085938" Y="-2.546374511719" />
                  <Point X="2.203010498047" Y="-2.623050048828" />
                  <Point X="2.228188720703" Y="-2.76246484375" />
                  <Point X="2.234091552734" Y="-2.778578613281" />
                  <Point X="2.734275634766" Y="-3.644922607422" />
                  <Point X="2.986673583984" Y="-4.082088623047" />
                  <Point X="2.956458984375" Y="-4.103669921875" />
                  <Point X="2.835296630859" Y="-4.190213378906" />
                  <Point X="2.766987792969" Y="-4.234428710938" />
                  <Point X="2.679775878906" Y="-4.290879394531" />
                  <Point X="2.046770019531" Y="-3.465930175781" />
                  <Point X="1.683177612305" Y="-2.992087402344" />
                  <Point X="1.670549316406" Y="-2.980467529297" />
                  <Point X="1.594926513672" Y="-2.931849121094" />
                  <Point X="1.45742590332" Y="-2.843448730469" />
                  <Point X="1.425804931641" Y="-2.835717041016" />
                  <Point X="1.343098388672" Y="-2.843327880859" />
                  <Point X="1.192718017578" Y="-2.857166015625" />
                  <Point X="1.165332885742" Y="-2.868509277344" />
                  <Point X="1.101468994141" Y="-2.921609863281" />
                  <Point X="0.985349060059" Y="-3.018159667969" />
                  <Point X="0.968456726074" Y="-3.045986328125" />
                  <Point X="0.949361877441" Y="-3.133838134766" />
                  <Point X="0.91464251709" Y="-3.29357421875" />
                  <Point X="0.91392956543" Y="-3.310720214844" />
                  <Point X="1.055677978516" Y="-4.387405273438" />
                  <Point X="1.127642211914" Y="-4.934028320312" />
                  <Point X="1.108973266602" Y="-4.938120605469" />
                  <Point X="0.994346252441" Y="-4.963246582031" />
                  <Point X="0.93123828125" Y="-4.974711425781" />
                  <Point X="0.860200378418" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#152" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.068569105749" Y="4.611027873117" Z="0.9" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.9" />
                  <Point X="-0.703392512309" Y="5.01661766568" Z="0.9" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.9" />
                  <Point X="-1.478524334475" Y="4.845123510723" Z="0.9" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.9" />
                  <Point X="-1.735308866933" Y="4.653301920354" Z="0.9" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.9" />
                  <Point X="-1.728471475712" Y="4.377130412799" Z="0.9" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.9" />
                  <Point X="-1.803909102593" Y="4.314300854688" Z="0.9" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.9" />
                  <Point X="-1.90052958834" Y="4.331703520828" Z="0.9" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.9" />
                  <Point X="-2.005272312325" Y="4.441764470535" Z="0.9" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.9" />
                  <Point X="-2.555095750193" Y="4.376112716961" Z="0.9" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.9" />
                  <Point X="-3.168560409571" Y="3.954634589716" Z="0.9" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.9" />
                  <Point X="-3.244846783068" Y="3.561759119031" Z="0.9" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.9" />
                  <Point X="-2.99669580474" Y="3.085119094084" Z="0.9" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.9" />
                  <Point X="-3.033216977349" Y="3.015586590537" Z="0.9" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.9" />
                  <Point X="-3.109957371523" Y="2.998868894433" Z="0.9" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.9" />
                  <Point X="-3.372100229266" Y="3.135347165678" Z="0.9" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.9" />
                  <Point X="-4.060729761585" Y="3.035242731352" Z="0.9" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.9" />
                  <Point X="-4.427019268861" Y="2.470576251348" Z="0.9" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.9" />
                  <Point X="-4.245660618956" Y="2.032172041132" Z="0.9" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.9" />
                  <Point X="-3.677375048065" Y="1.573975767677" Z="0.9" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.9" />
                  <Point X="-3.682724226861" Y="1.515314053241" Z="0.9" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.9" />
                  <Point X="-3.731100181305" Y="1.481704745484" Z="0.9" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.9" />
                  <Point X="-4.130293955744" Y="1.524517923982" Z="0.9" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.9" />
                  <Point X="-4.917358356369" Y="1.242644757719" Z="0.9" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.9" />
                  <Point X="-5.029280685329" Y="0.656451415839" Z="0.9" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.9" />
                  <Point X="-4.533841191295" Y="0.305571561024" Z="0.9" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.9" />
                  <Point X="-3.558655432327" Y="0.036641729019" Z="0.9" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.9" />
                  <Point X="-3.542839322415" Y="0.010576421727" Z="0.9" />
                  <Point X="-3.539556741714" Y="0" Z="0.9" />
                  <Point X="-3.54552527872" Y="-0.019230529334" Z="0.9" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.9" />
                  <Point X="-3.566713162796" Y="-0.042234254115" Z="0.9" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.9" />
                  <Point X="-4.103046597478" Y="-0.190140495732" Z="0.9" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.9" />
                  <Point X="-5.010219768224" Y="-0.796987875213" Z="0.9" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.9" />
                  <Point X="-4.895074574115" Y="-1.332571239631" Z="0.9" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.9" />
                  <Point X="-4.269329603626" Y="-1.445120879055" Z="0.9" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.9" />
                  <Point X="-3.202073139283" Y="-1.316919236061" Z="0.9" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.9" />
                  <Point X="-3.197647010202" Y="-1.341641890819" Z="0.9" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.9" />
                  <Point X="-3.662554832155" Y="-1.70683576165" Z="0.9" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.9" />
                  <Point X="-4.313513915937" Y="-2.669228282569" Z="0.9" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.9" />
                  <Point X="-3.985406299781" Y="-3.138109415659" Z="0.9" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.9" />
                  <Point X="-3.404720379423" Y="-3.035777614465" Z="0.9" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.9" />
                  <Point X="-2.561646761156" Y="-2.566683707976" Z="0.9" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.9" />
                  <Point X="-2.819639398704" Y="-3.030358003069" Z="0.9" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.9" />
                  <Point X="-3.035761135808" Y="-4.065637063645" Z="0.9" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.9" />
                  <Point X="-2.607015380794" Y="-4.353013629449" Z="0.9" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.9" />
                  <Point X="-2.371317931202" Y="-4.345544451312" Z="0.9" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.9" />
                  <Point X="-2.059790356014" Y="-4.045245761269" Z="0.9" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.9" />
                  <Point X="-1.768518587409" Y="-3.997175863448" Z="0.9" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.9" />
                  <Point X="-1.508174100196" Y="-4.136354713063" Z="0.9" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.9" />
                  <Point X="-1.3863555314" Y="-4.405260181043" Z="0.9" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.9" />
                  <Point X="-1.381988655126" Y="-4.643196438317" Z="0.9" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.9" />
                  <Point X="-1.222324291237" Y="-4.928587937318" Z="0.9" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.9" />
                  <Point X="-0.924022823676" Y="-4.993119120482" Z="0.9" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.9" />
                  <Point X="-0.675529531551" Y="-4.483294627226" Z="0.9" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.9" />
                  <Point X="-0.311454696303" Y="-3.366577384944" Z="0.9" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.9" />
                  <Point X="-0.089899583024" Y="-3.232140881405" Z="0.9" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.9" />
                  <Point X="0.163459496337" Y="-3.254971163883" Z="0.9" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.9" />
                  <Point X="0.358991163129" Y="-3.435068370823" Z="0.9" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.9" />
                  <Point X="0.55922532885" Y="-4.04924140259" Z="0.9" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.9" />
                  <Point X="0.934019250167" Y="-4.992626189725" Z="0.9" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.9" />
                  <Point X="1.113523084409" Y="-4.955681010205" Z="0.9" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.9" />
                  <Point X="1.099094115255" Y="-4.349599038199" Z="0.9" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.9" />
                  <Point X="0.992065073573" Y="-3.113177727123" Z="0.9" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.9" />
                  <Point X="1.127279993919" Y="-2.928776059759" Z="0.9" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.9" />
                  <Point X="1.341524134323" Y="-2.861837300187" Z="0.9" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.9" />
                  <Point X="1.56173115445" Y="-2.942626833519" Z="0.9" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.9" />
                  <Point X="2.000946365642" Y="-3.465088103477" Z="0.9" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.9" />
                  <Point X="2.788001075585" Y="-4.24512276054" Z="0.9" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.9" />
                  <Point X="2.979364785364" Y="-4.113077098128" Z="0.9" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.9" />
                  <Point X="2.771420971024" Y="-3.588642489748" Z="0.9" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.9" />
                  <Point X="2.246058659613" Y="-2.582883706757" Z="0.9" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.9" />
                  <Point X="2.293166116154" Y="-2.390388691565" Z="0.9" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.9" />
                  <Point X="2.442509686807" Y="-2.265735193814" Z="0.9" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.9" />
                  <Point X="2.645623096755" Y="-2.257389349878" Z="0.9" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.9" />
                  <Point X="3.198771107359" Y="-2.54632852925" Z="0.9" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.9" />
                  <Point X="4.177766227157" Y="-2.886450691191" Z="0.9" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.9" />
                  <Point X="4.342617910188" Y="-2.631919035607" Z="0.9" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.9" />
                  <Point X="3.97111745246" Y="-2.211860967753" Z="0.9" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.9" />
                  <Point X="3.127916009487" Y="-1.513759178623" Z="0.9" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.9" />
                  <Point X="3.102410342249" Y="-1.348023539944" Z="0.9" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.9" />
                  <Point X="3.178794961537" Y="-1.202217596435" Z="0.9" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.9" />
                  <Point X="3.334875258779" Y="-1.129923378055" Z="0.9" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.9" />
                  <Point X="3.93428056891" Y="-1.186351965634" Z="0.9" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.9" />
                  <Point X="4.961479446792" Y="-1.07570695375" Z="0.9" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.9" />
                  <Point X="5.027939956563" Y="-0.702315596748" Z="0.9" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.9" />
                  <Point X="4.58671304165" Y="-0.445555782837" Z="0.9" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.9" />
                  <Point X="3.688268098709" Y="-0.186311867707" Z="0.9" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.9" />
                  <Point X="3.61963195412" Y="-0.121706883039" Z="0.9" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.9" />
                  <Point X="3.587999803519" Y="-0.034280383793" Z="0.9" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.9" />
                  <Point X="3.593371646906" Y="0.062330147429" Z="0.9" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.9" />
                  <Point X="3.635747484281" Y="0.142241855536" Z="0.9" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.9" />
                  <Point X="3.715127315644" Y="0.201836976535" Z="0.9" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.9" />
                  <Point X="4.209254775657" Y="0.344416149932" Z="0.9" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.9" />
                  <Point X="5.005497020954" Y="0.842248090258" Z="0.9" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.9" />
                  <Point X="4.91673954577" Y="1.260974601134" Z="0.9" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.9" />
                  <Point X="4.377754717816" Y="1.342437879002" Z="0.9" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.9" />
                  <Point X="3.402371761167" Y="1.230052889837" Z="0.9" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.9" />
                  <Point X="3.324029696358" Y="1.25976079707" Z="0.9" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.9" />
                  <Point X="3.268313292827" Y="1.32079766212" Z="0.9" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.9" />
                  <Point X="3.239861523958" Y="1.401964057078" Z="0.9" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.9" />
                  <Point X="3.247478626443" Y="1.482004334961" Z="0.9" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.9" />
                  <Point X="3.29239526927" Y="1.55794746544" Z="0.9" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.9" />
                  <Point X="3.715422988955" Y="1.893563255489" Z="0.9" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.9" />
                  <Point X="4.31238897835" Y="2.678122845625" Z="0.9" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.9" />
                  <Point X="4.085973822133" Y="3.012284977761" Z="0.9" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.9" />
                  <Point X="3.472717750632" Y="2.822894571458" Z="0.9" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.9" />
                  <Point X="2.458079688258" Y="2.253147245835" Z="0.9" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.9" />
                  <Point X="2.384800863451" Y="2.250930114739" Z="0.9" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.9" />
                  <Point X="2.319321949117" Y="2.281615416156" Z="0.9" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.9" />
                  <Point X="2.269143184079" Y="2.337702911265" Z="0.9" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.9" />
                  <Point X="2.24849958802" Y="2.404957580382" Z="0.9" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.9" />
                  <Point X="2.259380696823" Y="2.481389887624" Z="0.9" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.9" />
                  <Point X="2.572730781401" Y="3.039421331323" Z="0.9" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.9" />
                  <Point X="2.886605039764" Y="4.174373513978" Z="0.9" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.9" />
                  <Point X="2.496891356311" Y="4.418531532472" Z="0.9" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.9" />
                  <Point X="2.090126214124" Y="4.624982712218" Z="0.9" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.9" />
                  <Point X="1.668354494139" Y="4.793296438064" Z="0.9" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.9" />
                  <Point X="1.094681435228" Y="4.950186377169" Z="0.9" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.9" />
                  <Point X="0.43073776201" Y="5.051451151392" Z="0.9" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.9" />
                  <Point X="0.124675455804" Y="4.820419632834" Z="0.9" />
                  <Point X="0" Y="4.355124473572" Z="0.9" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>