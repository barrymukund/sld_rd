<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#125" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="538" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="0" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="0.004715820312" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.595044006348" Y="-3.630987548828" />
                  <Point X="0.563302062988" Y="-3.512524414063" />
                  <Point X="0.557721191406" Y="-3.497141845703" />
                  <Point X="0.542363342285" Y="-3.467377685547" />
                  <Point X="0.529389648438" Y="-3.448684814453" />
                  <Point X="0.37863583374" Y="-3.231477294922" />
                  <Point X="0.356751586914" Y="-3.209021484375" />
                  <Point X="0.330496398926" Y="-3.189777587891" />
                  <Point X="0.302495483398" Y="-3.175669433594" />
                  <Point X="0.282419219971" Y="-3.169438476562" />
                  <Point X="0.049136493683" Y="-3.097036132812" />
                  <Point X="0.020976791382" Y="-3.092766601562" />
                  <Point X="-0.00866468811" Y="-3.092766601562" />
                  <Point X="-0.036823177338" Y="-3.097035644531" />
                  <Point X="-0.056899581909" Y="-3.103266357422" />
                  <Point X="-0.290182312012" Y="-3.175668945312" />
                  <Point X="-0.31818460083" Y="-3.189777587891" />
                  <Point X="-0.344439788818" Y="-3.209021484375" />
                  <Point X="-0.366323272705" Y="-3.231476318359" />
                  <Point X="-0.379297088623" Y="-3.250168945312" />
                  <Point X="-0.53005090332" Y="-3.467376708984" />
                  <Point X="-0.538189025879" Y="-3.481573730469" />
                  <Point X="-0.550990112305" Y="-3.512524414063" />
                  <Point X="-0.916584655762" Y="-4.876941894531" />
                  <Point X="-1.07933581543" Y="-4.8453515625" />
                  <Point X="-1.098545288086" Y="-4.840408691406" />
                  <Point X="-1.24641809082" Y="-4.802362304688" />
                  <Point X="-1.218784179688" Y="-4.592461914062" />
                  <Point X="-1.214963012695" Y="-4.563438476562" />
                  <Point X="-1.214201171875" Y="-4.5479296875" />
                  <Point X="-1.216508666992" Y="-4.516223632812" />
                  <Point X="-1.221304931641" Y="-4.492111816406" />
                  <Point X="-1.277036132812" Y="-4.211932128906" />
                  <Point X="-1.287938354492" Y="-4.182966308594" />
                  <Point X="-1.304010375977" Y="-4.15512890625" />
                  <Point X="-1.323644287109" Y="-4.131204589844" />
                  <Point X="-1.342127929688" Y="-4.114994628906" />
                  <Point X="-1.556905029297" Y="-3.926639648438" />
                  <Point X="-1.583188476562" Y="-3.910295410156" />
                  <Point X="-1.612885742188" Y="-3.897994384766" />
                  <Point X="-1.64302734375" Y="-3.890966308594" />
                  <Point X="-1.667559204102" Y="-3.889358398438" />
                  <Point X="-1.952616577148" Y="-3.870674804688" />
                  <Point X="-1.98341796875" Y="-3.873708496094" />
                  <Point X="-2.014466674805" Y="-3.882028076172" />
                  <Point X="-2.042657714844" Y="-3.894801513672" />
                  <Point X="-2.063099121094" Y="-3.908459960938" />
                  <Point X="-2.300624267578" Y="-4.067169677734" />
                  <Point X="-2.312786621094" Y="-4.076822265625" />
                  <Point X="-2.335103027344" Y="-4.099462402344" />
                  <Point X="-2.480149169922" Y="-4.288489746094" />
                  <Point X="-2.801707763672" Y="-4.089388916016" />
                  <Point X="-2.828303222656" Y="-4.068911132813" />
                  <Point X="-3.104721923828" Y="-3.856077880859" />
                  <Point X="-2.482750488281" Y="-2.778791748047" />
                  <Point X="-2.423761230469" Y="-2.676619628906" />
                  <Point X="-2.412859130859" Y="-2.647653320312" />
                  <Point X="-2.406588134766" Y="-2.616126953125" />
                  <Point X="-2.405693359375" Y="-2.584327636719" />
                  <Point X="-2.415346923828" Y="-2.554015869141" />
                  <Point X="-2.430938720703" Y="-2.523812255859" />
                  <Point X="-2.448179443359" Y="-2.500214599609" />
                  <Point X="-2.464153320312" Y="-2.484240722656" />
                  <Point X="-2.489310302734" Y="-2.466213134766" />
                  <Point X="-2.518139160156" Y="-2.45199609375" />
                  <Point X="-2.547756835938" Y="-2.443011474609" />
                  <Point X="-2.578690917969" Y="-2.444024169922" />
                  <Point X="-2.610217285156" Y="-2.450295166016" />
                  <Point X="-2.63918359375" Y="-2.461197265625" />
                  <Point X="-3.818024169922" Y="-3.141801513672" />
                  <Point X="-4.082859619141" Y="-2.793861816406" />
                  <Point X="-4.101923339844" Y="-2.761894775391" />
                  <Point X="-4.306142578125" Y="-2.419450439453" />
                  <Point X="-3.209166503906" Y="-1.577710693359" />
                  <Point X="-3.105954589844" Y="-1.498513427734" />
                  <Point X="-3.083405273438" Y="-1.473778930664" />
                  <Point X="-3.064829833984" Y="-1.444290161133" />
                  <Point X="-3.053245849609" Y="-1.417475097656" />
                  <Point X="-3.046151855469" Y="-1.390084960938" />
                  <Point X="-3.04334765625" Y="-1.359656005859" />
                  <Point X="-3.045556640625" Y="-1.327985351562" />
                  <Point X="-3.052976074219" Y="-1.297246704102" />
                  <Point X="-3.070073730469" Y="-1.270645874023" />
                  <Point X="-3.093004882812" Y="-1.245454589844" />
                  <Point X="-3.115070556641" Y="-1.227532348633" />
                  <Point X="-3.139454589844" Y="-1.213180908203" />
                  <Point X="-3.168718261719" Y="-1.201956542969" />
                  <Point X="-3.200605957031" Y="-1.195474853516" />
                  <Point X="-3.231929199219" Y="-1.194383789062" />
                  <Point X="-4.730690917969" Y="-1.391699462891" />
                  <Point X="-4.732102050781" Y="-1.391885131836" />
                  <Point X="-4.834077636719" Y="-0.992654541016" />
                  <Point X="-4.83912109375" Y="-0.957391540527" />
                  <Point X="-4.892424316406" Y="-0.584698242188" />
                  <Point X="-3.650449951172" Y="-0.251912078857" />
                  <Point X="-3.532875976562" Y="-0.220408432007" />
                  <Point X="-3.515505126953" Y="-0.213877197266" />
                  <Point X="-3.485529052734" Y="-0.197942840576" />
                  <Point X="-3.459975341797" Y="-0.180207046509" />
                  <Point X="-3.436245605469" Y="-0.156541259766" />
                  <Point X="-3.416268798828" Y="-0.127924362183" />
                  <Point X="-3.403435546875" Y="-0.1017059021" />
                  <Point X="-3.394917480469" Y="-0.074260986328" />
                  <Point X="-3.390672851562" Y="-0.043926128387" />
                  <Point X="-3.391405761719" Y="-0.011922182083" />
                  <Point X="-3.395650390625" Y="0.014062818527" />
                  <Point X="-3.404168457031" Y="0.041507740021" />
                  <Point X="-3.419695068359" Y="0.071394210815" />
                  <Point X="-3.441138183594" Y="0.099175498962" />
                  <Point X="-3.462174560547" Y="0.119173271179" />
                  <Point X="-3.487728271484" Y="0.136909072876" />
                  <Point X="-3.501925292969" Y="0.145047164917" />
                  <Point X="-3.532875976562" Y="0.157848251343" />
                  <Point X="-4.891815917969" Y="0.521975158691" />
                  <Point X="-4.824487304688" Y="0.976977783203" />
                  <Point X="-4.814333984375" Y="1.014445556641" />
                  <Point X="-4.703551269531" Y="1.423267822266" />
                  <Point X="-3.849946289063" Y="1.310888793945" />
                  <Point X="-3.765666259766" Y="1.29979284668" />
                  <Point X="-3.744986083984" Y="1.299341674805" />
                  <Point X="-3.723424560547" Y="1.301228149414" />
                  <Point X="-3.703136962891" Y="1.305263671875" />
                  <Point X="-3.698269775391" Y="1.306798339844" />
                  <Point X="-3.6417109375" Y="1.324631225586" />
                  <Point X="-3.622779052734" Y="1.332961669922" />
                  <Point X="-3.604034667969" Y="1.343783691406" />
                  <Point X="-3.587353515625" Y="1.356014648438" />
                  <Point X="-3.573715087891" Y="1.37156628418" />
                  <Point X="-3.561300537109" Y="1.389296020508" />
                  <Point X="-3.5513515625" Y="1.407430664062" />
                  <Point X="-3.5493984375" Y="1.412145874023" />
                  <Point X="-3.526704101563" Y="1.466934936523" />
                  <Point X="-3.520915527344" Y="1.486794189453" />
                  <Point X="-3.517157226562" Y="1.508109375" />
                  <Point X="-3.5158046875" Y="1.528749389648" />
                  <Point X="-3.518951171875" Y="1.549192993164" />
                  <Point X="-3.524552978516" Y="1.570099365234" />
                  <Point X="-3.532048828125" Y="1.589375610352" />
                  <Point X="-3.534405273438" Y="1.593902587891" />
                  <Point X="-3.561788574219" Y="1.646505371094" />
                  <Point X="-3.57328125" Y="1.663706054688" />
                  <Point X="-3.587193603516" Y="1.680286376953" />
                  <Point X="-3.602135742188" Y="1.694590087891" />
                  <Point X="-4.351860351563" Y="2.269874267578" />
                  <Point X="-4.081154541016" Y="2.733658447266" />
                  <Point X="-4.054260009766" Y="2.768227294922" />
                  <Point X="-3.750504882813" Y="3.158661621094" />
                  <Point X="-3.258419189453" Y="2.874555908203" />
                  <Point X="-3.206656982422" Y="2.844670898438" />
                  <Point X="-3.187724609375" Y="2.836340332031" />
                  <Point X="-3.167082519531" Y="2.829831787109" />
                  <Point X="-3.146793701172" Y="2.825796142578" />
                  <Point X="-3.140014648438" Y="2.825203125" />
                  <Point X="-3.061244384766" Y="2.818311523438" />
                  <Point X="-3.040565185547" Y="2.818762939453" />
                  <Point X="-3.019106445312" Y="2.821588134766" />
                  <Point X="-2.999015136719" Y="2.826504394531" />
                  <Point X="-2.980463867188" Y="2.835652832031" />
                  <Point X="-2.962209472656" Y="2.847281982422" />
                  <Point X="-2.946077392578" Y="2.860229492188" />
                  <Point X="-2.941265625" Y="2.865041259766" />
                  <Point X="-2.885353759766" Y="2.920952880859" />
                  <Point X="-2.872406738281" Y="2.937084228516" />
                  <Point X="-2.86077734375" Y="2.955338623047" />
                  <Point X="-2.851628662109" Y="2.973890136719" />
                  <Point X="-2.846712158203" Y="2.993981933594" />
                  <Point X="-2.843886962891" Y="3.015440917969" />
                  <Point X="-2.843435546875" Y="3.036120117188" />
                  <Point X="-2.844028564453" Y="3.042899169922" />
                  <Point X="-2.850920166016" Y="3.121669677734" />
                  <Point X="-2.854955810547" Y="3.141958496094" />
                  <Point X="-2.861464355469" Y="3.162600585938" />
                  <Point X="-2.869794921875" Y="3.181532958984" />
                  <Point X="-3.183333007812" Y="3.724596679688" />
                  <Point X="-2.700625732422" Y="4.094683349609" />
                  <Point X="-2.658270507812" Y="4.118215332031" />
                  <Point X="-2.167036376953" Y="4.391134277344" />
                  <Point X="-2.059041503906" Y="4.250392089844" />
                  <Point X="-2.0431953125" Y="4.229741210938" />
                  <Point X="-2.028892700195" Y="4.214799804688" />
                  <Point X="-2.012312866211" Y="4.200887207031" />
                  <Point X="-1.995114379883" Y="4.189395507812" />
                  <Point X="-1.987569580078" Y="4.185467773438" />
                  <Point X="-1.89989831543" Y="4.139828613281" />
                  <Point X="-1.8806171875" Y="4.132330566406" />
                  <Point X="-1.859710571289" Y="4.126729003906" />
                  <Point X="-1.839267578125" Y="4.123582519531" />
                  <Point X="-1.818628295898" Y="4.124935546875" />
                  <Point X="-1.797312988281" Y="4.128693847656" />
                  <Point X="-1.777451904297" Y="4.134482910156" />
                  <Point X="-1.769593261719" Y="4.13773828125" />
                  <Point X="-1.678277954102" Y="4.1755625" />
                  <Point X="-1.66014465332" Y="4.185510742187" />
                  <Point X="-1.642415039062" Y="4.197925292969" />
                  <Point X="-1.626863525391" Y="4.211563964844" />
                  <Point X="-1.614632568359" Y="4.228245117188" />
                  <Point X="-1.603810791016" Y="4.246989257813" />
                  <Point X="-1.59548059082" Y="4.265920898438" />
                  <Point X="-1.592922729492" Y="4.274033203125" />
                  <Point X="-1.563201171875" Y="4.368297363281" />
                  <Point X="-1.559165649414" Y="4.388584960938" />
                  <Point X="-1.557279174805" Y="4.410146484375" />
                  <Point X="-1.55773034668" Y="4.430826660156" />
                  <Point X="-1.584202026367" Y="4.6318984375" />
                  <Point X="-0.94963684082" Y="4.809808105469" />
                  <Point X="-0.898296142578" Y="4.815816894531" />
                  <Point X="-0.294710784912" Y="4.886457519531" />
                  <Point X="-0.149215927124" Y="4.343463378906" />
                  <Point X="-0.133903305054" Y="4.286315429688" />
                  <Point X="-0.121129844666" Y="4.258124023438" />
                  <Point X="-0.10327155304" Y="4.231397460938" />
                  <Point X="-0.0821145401" Y="4.20880859375" />
                  <Point X="-0.054819446564" Y="4.19421875" />
                  <Point X="-0.024381370544" Y="4.183886230469" />
                  <Point X="0.006155906677" Y="4.178844238281" />
                  <Point X="0.03669316864" Y="4.183886230469" />
                  <Point X="0.067131248474" Y="4.19421875" />
                  <Point X="0.094426475525" Y="4.20880859375" />
                  <Point X="0.115583503723" Y="4.231397460938" />
                  <Point X="0.133441802979" Y="4.258124023438" />
                  <Point X="0.146215255737" Y="4.286315429688" />
                  <Point X="0.307419311523" Y="4.8879375" />
                  <Point X="0.844041381836" Y="4.831738769531" />
                  <Point X="0.886522277832" Y="4.821482421875" />
                  <Point X="1.481028686523" Y="4.677950195312" />
                  <Point X="1.507311035156" Y="4.668417480469" />
                  <Point X="1.894641235352" Y="4.527930175781" />
                  <Point X="1.921386352539" Y="4.515422363281" />
                  <Point X="2.294577392578" Y="4.340893066406" />
                  <Point X="2.320442626953" Y="4.32582421875" />
                  <Point X="2.680976074219" Y="4.115776367188" />
                  <Point X="2.705345703125" Y="4.098446289062" />
                  <Point X="2.943260009766" Y="3.929254638672" />
                  <Point X="2.215856445313" Y="2.669354492188" />
                  <Point X="2.147581054688" Y="2.551097900391" />
                  <Point X="2.140566162109" Y="2.536044677734" />
                  <Point X="2.131375488281" Y="2.509694580078" />
                  <Point X="2.111607177734" Y="2.435770019531" />
                  <Point X="2.108418457031" Y="2.413852294922" />
                  <Point X="2.107744140625" Y="2.389448486328" />
                  <Point X="2.108391113281" Y="2.375451660156" />
                  <Point X="2.116099121094" Y="2.311528076172" />
                  <Point X="2.121442138672" Y="2.289604248047" />
                  <Point X="2.129708251953" Y="2.267515869141" />
                  <Point X="2.140071533203" Y="2.247470703125" />
                  <Point X="2.143475585937" Y="2.242454101562" />
                  <Point X="2.183029296875" Y="2.184162109375" />
                  <Point X="2.197624755859" Y="2.167310791016" />
                  <Point X="2.215941162109" Y="2.150606445312" />
                  <Point X="2.226615478516" Y="2.142188232422" />
                  <Point X="2.284907714844" Y="2.102634765625" />
                  <Point X="2.304952880859" Y="2.092271972656" />
                  <Point X="2.327041259766" Y="2.084006103516" />
                  <Point X="2.348960205078" Y="2.078663818359" />
                  <Point X="2.354461425781" Y="2.078000244141" />
                  <Point X="2.418385009766" Y="2.070292236328" />
                  <Point X="2.441107421875" Y="2.070288574219" />
                  <Point X="2.466372070312" Y="2.073327636719" />
                  <Point X="2.479568359375" Y="2.075872558594" />
                  <Point X="2.553492675781" Y="2.095640869141" />
                  <Point X="2.565283447266" Y="2.099638183594" />
                  <Point X="2.588533935547" Y="2.110145019531" />
                  <Point X="3.962660400391" Y="2.903497314453" />
                  <Point X="3.967326416016" Y="2.906191162109" />
                  <Point X="4.123270996094" Y="2.689463623047" />
                  <Point X="4.136856933594" Y="2.667012939453" />
                  <Point X="4.262198730469" Y="2.459884277344" />
                  <Point X="3.320675048828" Y="1.737427124023" />
                  <Point X="3.230783935547" Y="1.668451171875" />
                  <Point X="3.218287353516" Y="1.656948608398" />
                  <Point X="3.204463623047" Y="1.641725830078" />
                  <Point X="3.199394775391" Y="1.635654541016" />
                  <Point X="3.14619140625" Y="1.566246459961" />
                  <Point X="3.1346796875" Y="1.546815551758" />
                  <Point X="3.124504638672" Y="1.523764892578" />
                  <Point X="3.119924316406" Y="1.510987304688" />
                  <Point X="3.100105957031" Y="1.440121582031" />
                  <Point X="3.096652587891" Y="1.417823120117" />
                  <Point X="3.095836425781" Y="1.394252929688" />
                  <Point X="3.097739501953" Y="1.371767822266" />
                  <Point X="3.099139648438" Y="1.364982177734" />
                  <Point X="3.115408447266" Y="1.286135009766" />
                  <Point X="3.122640625" Y="1.264564941406" />
                  <Point X="3.133646728516" Y="1.241399291992" />
                  <Point X="3.140090820312" Y="1.229951904297" />
                  <Point X="3.184340332031" Y="1.162694946289" />
                  <Point X="3.198893554688" Y="1.145450439453" />
                  <Point X="3.216137451172" Y="1.129360595703" />
                  <Point X="3.234347167969" Y="1.116034667969" />
                  <Point X="3.239865478516" Y="1.112928344727" />
                  <Point X="3.303989257812" Y="1.076832275391" />
                  <Point X="3.325437011719" Y="1.0680078125" />
                  <Point X="3.350873779297" Y="1.061023681641" />
                  <Point X="3.363579833984" Y="1.058452270508" />
                  <Point X="3.450279296875" Y="1.046994018555" />
                  <Point X="3.462702392578" Y="1.046175048828" />
                  <Point X="3.488203613281" Y="1.046984619141" />
                  <Point X="4.77683984375" Y="1.21663671875" />
                  <Point X="4.845936523438" Y="0.93280859375" />
                  <Point X="4.850216796875" Y="0.905314331055" />
                  <Point X="4.890864746094" Y="0.644238586426" />
                  <Point X="3.819596923828" Y="0.357193237305" />
                  <Point X="3.716579833984" Y="0.32958984375" />
                  <Point X="3.700319580078" Y="0.323596557617" />
                  <Point X="3.680908203125" Y="0.314351898193" />
                  <Point X="3.674215087891" Y="0.310830841064" />
                  <Point X="3.589035644531" Y="0.261595489502" />
                  <Point X="3.570666748047" Y="0.247763580322" />
                  <Point X="3.551956542969" Y="0.229738922119" />
                  <Point X="3.543132568359" Y="0.219972122192" />
                  <Point X="3.492024902344" Y="0.154848876953" />
                  <Point X="3.48030078125" Y="0.135568618774" />
                  <Point X="3.470527099609" Y="0.114104995728" />
                  <Point X="3.463680908203" Y="0.092603729248" />
                  <Point X="3.46221484375" Y="0.08494833374" />
                  <Point X="3.445178710938" Y="-0.004006767273" />
                  <Point X="3.443628662109" Y="-0.027129911423" />
                  <Point X="3.445094726562" Y="-0.053594871521" />
                  <Point X="3.446644775391" Y="-0.066208961487" />
                  <Point X="3.463680664062" Y="-0.1551640625" />
                  <Point X="3.470527099609" Y="-0.176665466309" />
                  <Point X="3.480301025391" Y="-0.198129241943" />
                  <Point X="3.492023925781" Y="-0.217407531738" />
                  <Point X="3.496422119141" Y="-0.22301210022" />
                  <Point X="3.547529785156" Y="-0.288135192871" />
                  <Point X="3.564322021484" Y="-0.304770202637" />
                  <Point X="3.585964599609" Y="-0.32142767334" />
                  <Point X="3.596366210938" Y="-0.328392730713" />
                  <Point X="3.681545654297" Y="-0.377628082275" />
                  <Point X="3.692710205078" Y="-0.383138977051" />
                  <Point X="3.716579833984" Y="-0.392150024414" />
                  <Point X="4.891472167969" Y="-0.706961486816" />
                  <Point X="4.855022460938" Y="-0.948727111816" />
                  <Point X="4.849537597656" Y="-0.97276184082" />
                  <Point X="4.801173339844" Y="-1.184698974609" />
                  <Point X="3.543853027344" Y="-1.019169494629" />
                  <Point X="3.424382080078" Y="-1.003440979004" />
                  <Point X="3.400498046875" Y="-1.003325012207" />
                  <Point X="3.36896484375" Y="-1.007165161133" />
                  <Point X="3.360271484375" Y="-1.008635986328" />
                  <Point X="3.193094482422" Y="-1.044972290039" />
                  <Point X="3.162724609375" Y="-1.055693237305" />
                  <Point X="3.130712402344" Y="-1.076806030273" />
                  <Point X="3.109516113281" Y="-1.098038696289" />
                  <Point X="3.103700927734" Y="-1.104418945312" />
                  <Point X="3.002653076172" Y="-1.225948364258" />
                  <Point X="2.986626220703" Y="-1.250417602539" />
                  <Point X="2.974333740234" Y="-1.283384277344" />
                  <Point X="2.969491699219" Y="-1.311481445312" />
                  <Point X="2.968511230469" Y="-1.318909912109" />
                  <Point X="2.954028564453" Y="-1.476295776367" />
                  <Point X="2.955109130859" Y="-1.508482543945" />
                  <Point X="2.965749511719" Y="-1.545642700195" />
                  <Point X="2.980222900391" Y="-1.573190917969" />
                  <Point X="2.984412353516" Y="-1.580381103516" />
                  <Point X="3.076930419922" Y="-1.724287109375" />
                  <Point X="3.086932861328" Y="-1.737239013672" />
                  <Point X="3.110628417969" Y="-1.760909057617" />
                  <Point X="4.213122558594" Y="-2.606883056641" />
                  <Point X="4.1248125" Y="-2.749782470703" />
                  <Point X="4.113465820313" Y="-2.765904296875" />
                  <Point X="4.028981445312" Y="-2.8859453125" />
                  <Point X="2.907359619141" Y="-2.238376220703" />
                  <Point X="2.800954589844" Y="-2.176943115234" />
                  <Point X="2.778272216797" Y="-2.167514648437" />
                  <Point X="2.745035400391" Y="-2.15851953125" />
                  <Point X="2.7371015625" Y="-2.156732910156" />
                  <Point X="2.538134277344" Y="-2.120799560547" />
                  <Point X="2.505973388672" Y="-2.119082275391" />
                  <Point X="2.467712646484" Y="-2.126591308594" />
                  <Point X="2.437815917969" Y="-2.139248291016" />
                  <Point X="2.430608154297" Y="-2.142663330078" />
                  <Point X="2.265315185547" Y="-2.229655761719" />
                  <Point X="2.241143310547" Y="-2.246130126953" />
                  <Point X="2.216854248047" Y="-2.272388427734" />
                  <Point X="2.2004765625" Y="-2.298682128906" />
                  <Point X="2.197045166016" Y="-2.304664306641" />
                  <Point X="2.110052734375" Y="-2.469957275391" />
                  <Point X="2.098733642578" Y="-2.500108886719" />
                  <Point X="2.094406005859" Y="-2.539166015625" />
                  <Point X="2.0977109375" Y="-2.572790283203" />
                  <Point X="2.098767822266" Y="-2.580381347656" />
                  <Point X="2.134701171875" Y="-2.779348632812" />
                  <Point X="2.138985595703" Y="-2.795141845703" />
                  <Point X="2.151819091797" Y="-2.826078613281" />
                  <Point X="2.861283691406" Y="-4.054906494141" />
                  <Point X="2.78184765625" Y="-4.111645507812" />
                  <Point X="2.769169189453" Y="-4.119852050781" />
                  <Point X="2.701765136719" Y="-4.163481445312" />
                  <Point X="1.839742797852" Y="-3.040072753906" />
                  <Point X="1.758546020508" Y="-2.934255126953" />
                  <Point X="1.747503540039" Y="-2.922179443359" />
                  <Point X="1.721924316406" Y="-2.900557373047" />
                  <Point X="1.705036376953" Y="-2.889699951172" />
                  <Point X="1.50880090332" Y="-2.763538574219" />
                  <Point X="1.479745361328" Y="-2.749644775391" />
                  <Point X="1.440958862305" Y="-2.741946533203" />
                  <Point X="1.406031616211" Y="-2.74242578125" />
                  <Point X="1.398629760742" Y="-2.74281640625" />
                  <Point X="1.184012817383" Y="-2.762565673828" />
                  <Point X="1.155377807617" Y="-2.76853515625" />
                  <Point X="1.122474365234" Y="-2.783797607422" />
                  <Point X="1.095379638672" Y="-2.803403320312" />
                  <Point X="1.090333740234" Y="-2.807319335938" />
                  <Point X="0.924611938477" Y="-2.945111572266" />
                  <Point X="0.904141052246" Y="-2.968861816406" />
                  <Point X="0.887248779297" Y="-2.996688476562" />
                  <Point X="0.875624023438" Y="-3.025809570312" />
                  <Point X="0.871359924316" Y="-3.045428466797" />
                  <Point X="0.821809936523" Y="-3.273397460938" />
                  <Point X="0.819724487305" Y="-3.289627441406" />
                  <Point X="0.819742248535" Y="-3.323120117188" />
                  <Point X="1.022039855957" Y="-4.859722167969" />
                  <Point X="1.022065307617" Y="-4.859915527344" />
                  <Point X="0.975675537109" Y="-4.870083984375" />
                  <Point X="0.96395904541" Y="-4.872212402344" />
                  <Point X="0.929315612793" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058427246094" Y="-4.752637207031" />
                  <Point X="-1.074871582031" Y="-4.748405761719" />
                  <Point X="-1.141246582031" Y="-4.731328125" />
                  <Point X="-1.124596923828" Y="-4.604861816406" />
                  <Point X="-1.120775634766" Y="-4.575838378906" />
                  <Point X="-1.120077514648" Y="-4.568099609375" />
                  <Point X="-1.119451782227" Y="-4.541034179688" />
                  <Point X="-1.121759277344" Y="-4.509328125" />
                  <Point X="-1.123334106445" Y="-4.497689453125" />
                  <Point X="-1.128130249023" Y="-4.473577636719" />
                  <Point X="-1.183861450195" Y="-4.193397949219" />
                  <Point X="-1.188125366211" Y="-4.178467773438" />
                  <Point X="-1.199027587891" Y="-4.149501953125" />
                  <Point X="-1.205666015625" Y="-4.135466308594" />
                  <Point X="-1.221738037109" Y="-4.10762890625" />
                  <Point X="-1.230573974609" Y="-4.094862060547" />
                  <Point X="-1.250207763672" Y="-4.070937744141" />
                  <Point X="-1.261005859375" Y="-4.059780273438" />
                  <Point X="-1.279489501953" Y="-4.0435703125" />
                  <Point X="-1.494266479492" Y="-3.855215332031" />
                  <Point X="-1.50673828125" Y="-3.845965576172" />
                  <Point X="-1.533021728516" Y="-3.829621337891" />
                  <Point X="-1.546833618164" Y="-3.822526855469" />
                  <Point X="-1.576530761719" Y="-3.810225830078" />
                  <Point X="-1.591313476562" Y="-3.805476074219" />
                  <Point X="-1.621454833984" Y="-3.798447998047" />
                  <Point X="-1.636813964844" Y="-3.796169677734" />
                  <Point X="-1.661345825195" Y="-3.794561767578" />
                  <Point X="-1.946403198242" Y="-3.775878173828" />
                  <Point X="-1.961928222656" Y="-3.776132324219" />
                  <Point X="-1.992729614258" Y="-3.779166015625" />
                  <Point X="-2.008006103516" Y="-3.781945556641" />
                  <Point X="-2.03905480957" Y="-3.790265136719" />
                  <Point X="-2.053674560547" Y="-3.795496337891" />
                  <Point X="-2.081865478516" Y="-3.808269775391" />
                  <Point X="-2.095436767578" Y="-3.815811767578" />
                  <Point X="-2.115878173828" Y="-3.829470214844" />
                  <Point X="-2.353403320312" Y="-3.988179931641" />
                  <Point X="-2.359681396484" Y="-3.992757080078" />
                  <Point X="-2.380443847656" Y="-4.010132568359" />
                  <Point X="-2.402760253906" Y="-4.032772705078" />
                  <Point X="-2.410471435547" Y="-4.041629882812" />
                  <Point X="-2.503202880859" Y="-4.162479492188" />
                  <Point X="-2.747583007812" Y="-4.011165527344" />
                  <Point X="-2.770345703125" Y="-3.993638916016" />
                  <Point X="-2.980862792969" Y="-3.831547607422" />
                  <Point X="-2.400478027344" Y="-2.826291748047" />
                  <Point X="-2.341488769531" Y="-2.724119628906" />
                  <Point X="-2.334850097656" Y="-2.710083251953" />
                  <Point X="-2.323947998047" Y="-2.681116943359" />
                  <Point X="-2.319684570312" Y="-2.666187011719" />
                  <Point X="-2.313413574219" Y="-2.634660644531" />
                  <Point X="-2.311625732422" Y="-2.618799072266" />
                  <Point X="-2.310730957031" Y="-2.586999755859" />
                  <Point X="-2.315173095703" Y="-2.555499023438" />
                  <Point X="-2.324826660156" Y="-2.525187255859" />
                  <Point X="-2.330931152344" Y="-2.510438476562" />
                  <Point X="-2.346522949219" Y="-2.480234863281" />
                  <Point X="-2.354230712891" Y="-2.467768554688" />
                  <Point X="-2.371471435547" Y="-2.444170898438" />
                  <Point X="-2.381004394531" Y="-2.433039550781" />
                  <Point X="-2.396978271484" Y="-2.417065673828" />
                  <Point X="-2.408817138672" Y="-2.407020751953" />
                  <Point X="-2.433974121094" Y="-2.388993164062" />
                  <Point X="-2.447292236328" Y="-2.381010498047" />
                  <Point X="-2.47612109375" Y="-2.366793457031" />
                  <Point X="-2.490561523438" Y="-2.361086914062" />
                  <Point X="-2.520179199219" Y="-2.352102294922" />
                  <Point X="-2.550865234375" Y="-2.348062255859" />
                  <Point X="-2.581799316406" Y="-2.349074951172" />
                  <Point X="-2.597224609375" Y="-2.350849609375" />
                  <Point X="-2.628750976562" Y="-2.357120605469" />
                  <Point X="-2.643680908203" Y="-2.361384033203" />
                  <Point X="-2.672647216797" Y="-2.372286132812" />
                  <Point X="-2.68668359375" Y="-2.378924804688" />
                  <Point X="-3.793089111328" Y="-3.017708740234" />
                  <Point X="-4.004016357422" Y="-2.740593017578" />
                  <Point X="-4.020330566406" Y="-2.713236572266" />
                  <Point X="-4.181265136719" Y="-2.443373779297" />
                  <Point X="-3.151334228516" Y="-1.653079223633" />
                  <Point X="-3.048122314453" Y="-1.573881958008" />
                  <Point X="-3.03575" Y="-1.562515869141" />
                  <Point X="-3.013200683594" Y="-1.53778137207" />
                  <Point X="-3.0030234375" Y="-1.524412597656" />
                  <Point X="-2.984447998047" Y="-1.494923828125" />
                  <Point X="-2.977619628906" Y="-1.481964599609" />
                  <Point X="-2.966035644531" Y="-1.455149414062" />
                  <Point X="-2.961280273438" Y="-1.441293945312" />
                  <Point X="-2.954186279297" Y="-1.413903808594" />
                  <Point X="-2.951552734375" Y="-1.398802856445" />
                  <Point X="-2.948748535156" Y="-1.368373901367" />
                  <Point X="-2.948577880859" Y="-1.353045776367" />
                  <Point X="-2.950786865234" Y="-1.32137512207" />
                  <Point X="-2.953208740234" Y="-1.30569519043" />
                  <Point X="-2.960628173828" Y="-1.274956542969" />
                  <Point X="-2.973060302734" Y="-1.245880859375" />
                  <Point X="-2.990157958984" Y="-1.219280029297" />
                  <Point X="-2.999821044922" Y="-1.206696166992" />
                  <Point X="-3.022752197266" Y="-1.181504882812" />
                  <Point X="-3.033110839844" Y="-1.171713745117" />
                  <Point X="-3.055176513672" Y="-1.153791503906" />
                  <Point X="-3.066884033203" Y="-1.14566015625" />
                  <Point X="-3.091268066406" Y="-1.131308837891" />
                  <Point X="-3.105433105469" Y="-1.124481689453" />
                  <Point X="-3.134696777344" Y="-1.113257324219" />
                  <Point X="-3.149794921875" Y="-1.108860351562" />
                  <Point X="-3.181682617188" Y="-1.102378540039" />
                  <Point X="-3.197298828125" Y="-1.100532348633" />
                  <Point X="-3.228622070312" Y="-1.09944140625" />
                  <Point X="-3.244329101562" Y="-1.100196533203" />
                  <Point X="-4.660920898438" Y="-1.286694213867" />
                  <Point X="-4.740762207031" Y="-0.974118103027" />
                  <Point X="-4.745078125" Y="-0.943941101074" />
                  <Point X="-4.786452148438" Y="-0.654654418945" />
                  <Point X="-3.625862060547" Y="-0.343675018311" />
                  <Point X="-3.508288085938" Y="-0.312171417236" />
                  <Point X="-3.499442138672" Y="-0.309330810547" />
                  <Point X="-3.470914550781" Y="-0.297762145996" />
                  <Point X="-3.440938476562" Y="-0.281827697754" />
                  <Point X="-3.431361816406" Y="-0.275987091064" />
                  <Point X="-3.405808105469" Y="-0.258251342773" />
                  <Point X="-3.392890869141" Y="-0.247472763062" />
                  <Point X="-3.369161132812" Y="-0.223806945801" />
                  <Point X="-3.358348388672" Y="-0.210919555664" />
                  <Point X="-3.338371582031" Y="-0.182302658081" />
                  <Point X="-3.330941894531" Y="-0.16968963623" />
                  <Point X="-3.318108642578" Y="-0.143471206665" />
                  <Point X="-3.312705078125" Y="-0.129865829468" />
                  <Point X="-3.304187011719" Y="-0.102420974731" />
                  <Point X="-3.300833984375" Y="-0.087425666809" />
                  <Point X="-3.296589355469" Y="-0.057090839386" />
                  <Point X="-3.295697753906" Y="-0.041751171112" />
                  <Point X="-3.296430664062" Y="-0.009747161865" />
                  <Point X="-3.2976484375" Y="0.003393034458" />
                  <Point X="-3.301893066406" Y="0.029377965927" />
                  <Point X="-3.304919921875" Y="0.042222846985" />
                  <Point X="-3.313437988281" Y="0.069667701721" />
                  <Point X="-3.319866210938" Y="0.08530431366" />
                  <Point X="-3.335392822266" Y="0.115190895081" />
                  <Point X="-3.344491210938" Y="0.129440567017" />
                  <Point X="-3.365934326172" Y="0.157221893311" />
                  <Point X="-3.375684326172" Y="0.168028869629" />
                  <Point X="-3.396720703125" Y="0.188026672363" />
                  <Point X="-3.408007324219" Y="0.197217514038" />
                  <Point X="-3.433561035156" Y="0.214953277588" />
                  <Point X="-3.440483398438" Y="0.219328292847" />
                  <Point X="-3.465616699219" Y="0.232834838867" />
                  <Point X="-3.496567382812" Y="0.245636032104" />
                  <Point X="-3.508288085938" Y="0.249611251831" />
                  <Point X="-4.785445800781" Y="0.591824584961" />
                  <Point X="-4.731330078125" Y="0.957533203125" />
                  <Point X="-4.722641113281" Y="0.989597839355" />
                  <Point X="-4.633586425781" Y="1.318237060547" />
                  <Point X="-3.862346191406" Y="1.216701538086" />
                  <Point X="-3.778066162109" Y="1.20560559082" />
                  <Point X="-3.76773828125" Y="1.204815429688" />
                  <Point X="-3.747058105469" Y="1.204364257812" />
                  <Point X="-3.736705810547" Y="1.204703125" />
                  <Point X="-3.715144287109" Y="1.20658984375" />
                  <Point X="-3.704890625" Y="1.208053588867" />
                  <Point X="-3.684603027344" Y="1.212089111328" />
                  <Point X="-3.669701904297" Y="1.21619543457" />
                  <Point X="-3.613143066406" Y="1.234028198242" />
                  <Point X="-3.60344921875" Y="1.237677001953" />
                  <Point X="-3.584517333984" Y="1.246007446289" />
                  <Point X="-3.575279296875" Y="1.250689208984" />
                  <Point X="-3.556534912109" Y="1.261511108398" />
                  <Point X="-3.547860839844" Y="1.267171142578" />
                  <Point X="-3.5311796875" Y="1.279402099609" />
                  <Point X="-3.515928710938" Y="1.293376831055" />
                  <Point X="-3.502290283203" Y="1.308928344727" />
                  <Point X="-3.495895751953" Y="1.317076416016" />
                  <Point X="-3.483481201172" Y="1.334806152344" />
                  <Point X="-3.478011474609" Y="1.343602172852" />
                  <Point X="-3.4680625" Y="1.361736816406" />
                  <Point X="-3.461630126953" Y="1.375790527344" />
                  <Point X="-3.438935791016" Y="1.430579711914" />
                  <Point X="-3.435499511719" Y="1.440350585938" />
                  <Point X="-3.4297109375" Y="1.460209838867" />
                  <Point X="-3.427358642578" Y="1.470298217773" />
                  <Point X="-3.423600341797" Y="1.49161340332" />
                  <Point X="-3.422360595703" Y="1.501897460938" />
                  <Point X="-3.421008056641" Y="1.522537353516" />
                  <Point X="-3.421910400391" Y="1.543200805664" />
                  <Point X="-3.425056884766" Y="1.563644287109" />
                  <Point X="-3.427188232422" Y="1.573780639648" />
                  <Point X="-3.432790039062" Y="1.594687011719" />
                  <Point X="-3.43601171875" Y="1.604529785156" />
                  <Point X="-3.443507568359" Y="1.623806030273" />
                  <Point X="-3.450138183594" Y="1.637766479492" />
                  <Point X="-3.477521484375" Y="1.690369262695" />
                  <Point X="-3.482798095703" Y="1.699283081055" />
                  <Point X="-3.494290771484" Y="1.716483764648" />
                  <Point X="-3.500506591797" Y="1.724770385742" />
                  <Point X="-3.514418945312" Y="1.741350708008" />
                  <Point X="-3.521500488281" Y="1.748911621094" />
                  <Point X="-3.536442626953" Y="1.763215454102" />
                  <Point X="-3.544303466797" Y="1.769958618164" />
                  <Point X="-4.227614746094" Y="2.294282226563" />
                  <Point X="-4.002293212891" Y="2.680312011719" />
                  <Point X="-3.979279541016" Y="2.709892578125" />
                  <Point X="-3.726338623047" Y="3.035012451172" />
                  <Point X="-3.305919189453" Y="2.792283447266" />
                  <Point X="-3.254156982422" Y="2.7623984375" />
                  <Point X="-3.244918457031" Y="2.757716552734" />
                  <Point X="-3.225986083984" Y="2.749385986328" />
                  <Point X="-3.216292236328" Y="2.745737304688" />
                  <Point X="-3.195650146484" Y="2.739228759766" />
                  <Point X="-3.185615966797" Y="2.736657226562" />
                  <Point X="-3.165327148438" Y="2.732621582031" />
                  <Point X="-3.148293457031" Y="2.730564453125" />
                  <Point X="-3.069523193359" Y="2.723672851562" />
                  <Point X="-3.059171142578" Y="2.723334228516" />
                  <Point X="-3.038491943359" Y="2.723785644531" />
                  <Point X="-3.028164794922" Y="2.724575683594" />
                  <Point X="-3.006706054688" Y="2.727400878906" />
                  <Point X="-2.996526611328" Y="2.729310546875" />
                  <Point X="-2.976435302734" Y="2.734226806641" />
                  <Point X="-2.956997802734" Y="2.741301513672" />
                  <Point X="-2.938446533203" Y="2.750449951172" />
                  <Point X="-2.929420898438" Y="2.755530273438" />
                  <Point X="-2.911166503906" Y="2.767159423828" />
                  <Point X="-2.902746337891" Y="2.773193359375" />
                  <Point X="-2.886614257812" Y="2.786140869141" />
                  <Point X="-2.874090576172" Y="2.797866210938" />
                  <Point X="-2.818178710938" Y="2.853777832031" />
                  <Point X="-2.811265380859" Y="2.861489501953" />
                  <Point X="-2.798318359375" Y="2.877620849609" />
                  <Point X="-2.792284667969" Y="2.886040527344" />
                  <Point X="-2.780655273438" Y="2.904294921875" />
                  <Point X="-2.775574707031" Y="2.913320800781" />
                  <Point X="-2.766426025391" Y="2.931872314453" />
                  <Point X="-2.759351318359" Y="2.951309570312" />
                  <Point X="-2.754434814453" Y="2.971401367188" />
                  <Point X="-2.752524902344" Y="2.981581542969" />
                  <Point X="-2.749699707031" Y="3.003040527344" />
                  <Point X="-2.748909667969" Y="3.013367675781" />
                  <Point X="-2.748458251953" Y="3.034046875" />
                  <Point X="-2.749389892578" Y="3.051177978516" />
                  <Point X="-2.756281494141" Y="3.129948486328" />
                  <Point X="-2.757745605469" Y="3.140203125" />
                  <Point X="-2.76178125" Y="3.160491943359" />
                  <Point X="-2.764352783203" Y="3.170526123047" />
                  <Point X="-2.770861328125" Y="3.191168212891" />
                  <Point X="-2.774510009766" Y="3.200862060547" />
                  <Point X="-2.782840576172" Y="3.219794433594" />
                  <Point X="-2.787522460938" Y="3.229032958984" />
                  <Point X="-3.059387207031" Y="3.699916503906" />
                  <Point X="-2.648374267578" Y="4.015035888672" />
                  <Point X="-2.612132568359" Y="4.035171386719" />
                  <Point X="-2.192524658203" Y="4.268296386719" />
                  <Point X="-2.13441015625" Y="4.192560058594" />
                  <Point X="-2.118563964844" Y="4.171909179688" />
                  <Point X="-2.111821533203" Y="4.164049316406" />
                  <Point X="-2.097518798828" Y="4.149107910156" />
                  <Point X="-2.089958740234" Y="4.142026367188" />
                  <Point X="-2.07337890625" Y="4.128113769531" />
                  <Point X="-2.065092041016" Y="4.121897460938" />
                  <Point X="-2.047893676758" Y="4.110405761719" />
                  <Point X="-2.031437133789" Y="4.101202636719" />
                  <Point X="-1.943765991211" Y="4.055563232422" />
                  <Point X="-1.934330078125" Y="4.051287841797" />
                  <Point X="-1.915048828125" Y="4.043789794922" />
                  <Point X="-1.905203613281" Y="4.040567138672" />
                  <Point X="-1.88429699707" Y="4.034965576172" />
                  <Point X="-1.874162475586" Y="4.032834716797" />
                  <Point X="-1.853719360352" Y="4.029688232422" />
                  <Point X="-1.833052978516" Y="4.028785888672" />
                  <Point X="-1.812413818359" Y="4.030138916016" />
                  <Point X="-1.802132446289" Y="4.031378662109" />
                  <Point X="-1.780817138672" Y="4.035136962891" />
                  <Point X="-1.770728881836" Y="4.037489257812" />
                  <Point X="-1.750867797852" Y="4.043278320312" />
                  <Point X="-1.733236450195" Y="4.049970458984" />
                  <Point X="-1.641921020508" Y="4.087794677734" />
                  <Point X="-1.632584106445" Y="4.0922734375" />
                  <Point X="-1.614450683594" Y="4.102221679688" />
                  <Point X="-1.605654541016" Y="4.10769140625" />
                  <Point X="-1.587924804688" Y="4.120105957031" />
                  <Point X="-1.579776245117" Y="4.126501464844" />
                  <Point X="-1.564224609375" Y="4.140140136719" />
                  <Point X="-1.550250976562" Y="4.155390136719" />
                  <Point X="-1.538020019531" Y="4.172071289062" />
                  <Point X="-1.532359863281" Y="4.180745605469" />
                  <Point X="-1.521538085938" Y="4.199489746094" />
                  <Point X="-1.516856323242" Y="4.208728027344" />
                  <Point X="-1.508526123047" Y="4.227659667969" />
                  <Point X="-1.502319824219" Y="4.245465332031" />
                  <Point X="-1.472598144531" Y="4.339729492188" />
                  <Point X="-1.470026611328" Y="4.349763671875" />
                  <Point X="-1.465990966797" Y="4.370051269531" />
                  <Point X="-1.46452722168" Y="4.3803046875" />
                  <Point X="-1.46264074707" Y="4.401866210938" />
                  <Point X="-1.462301757812" Y="4.41221875" />
                  <Point X="-1.462752929688" Y="4.432898925781" />
                  <Point X="-1.46354309082" Y="4.4432265625" />
                  <Point X="-1.479266235352" Y="4.562655273438" />
                  <Point X="-0.931177856445" Y="4.716319824219" />
                  <Point X="-0.887252990723" Y="4.7214609375" />
                  <Point X="-0.365221984863" Y="4.782556640625" />
                  <Point X="-0.240978942871" Y="4.318875488281" />
                  <Point X="-0.22566633606" Y="4.261727539062" />
                  <Point X="-0.220435256958" Y="4.247107910156" />
                  <Point X="-0.207661849976" Y="4.218916503906" />
                  <Point X="-0.200119247437" Y="4.205344726562" />
                  <Point X="-0.182260864258" Y="4.178618164062" />
                  <Point X="-0.17260824585" Y="4.166456054688" />
                  <Point X="-0.151451339722" Y="4.1438671875" />
                  <Point X="-0.126897941589" Y="4.125026367188" />
                  <Point X="-0.099602897644" Y="4.110436523438" />
                  <Point X="-0.085356651306" Y="4.104260742188" />
                  <Point X="-0.054918682098" Y="4.093927978516" />
                  <Point X="-0.039857234955" Y="4.090155273438" />
                  <Point X="-0.009319986343" Y="4.08511328125" />
                  <Point X="0.021631772995" Y="4.08511328125" />
                  <Point X="0.052169025421" Y="4.090155273438" />
                  <Point X="0.06723046875" Y="4.093927978516" />
                  <Point X="0.097668441772" Y="4.104260742188" />
                  <Point X="0.111914535522" Y="4.110436523438" />
                  <Point X="0.139209732056" Y="4.125026367188" />
                  <Point X="0.163763122559" Y="4.143866699219" />
                  <Point X="0.184920181274" Y="4.166455566406" />
                  <Point X="0.194572952271" Y="4.178617675781" />
                  <Point X="0.212431182861" Y="4.205344238281" />
                  <Point X="0.2199737854" Y="4.218916503906" />
                  <Point X="0.232747192383" Y="4.247107910156" />
                  <Point X="0.237978271484" Y="4.261727539062" />
                  <Point X="0.378190246582" Y="4.785006347656" />
                  <Point X="0.827875976562" Y="4.737912109375" />
                  <Point X="0.8642265625" Y="4.729135742188" />
                  <Point X="1.453599853516" Y="4.586842773438" />
                  <Point X="1.474918945312" Y="4.579110351562" />
                  <Point X="1.858252319336" Y="4.440072753906" />
                  <Point X="1.881141601562" Y="4.429368164062" />
                  <Point X="2.250454345703" Y="4.25665234375" />
                  <Point X="2.272620361328" Y="4.243738769531" />
                  <Point X="2.629430175781" Y="4.035860351563" />
                  <Point X="2.650289794922" Y="4.021026367188" />
                  <Point X="2.817779785156" Y="3.901916748047" />
                  <Point X="2.133583984375" Y="2.716854492188" />
                  <Point X="2.06530859375" Y="2.598597900391" />
                  <Point X="2.061471923828" Y="2.591225341797" />
                  <Point X="2.050865966797" Y="2.567331298828" />
                  <Point X="2.041675170898" Y="2.540981201172" />
                  <Point X="2.039600219727" Y="2.534236328125" />
                  <Point X="2.01983190918" Y="2.460311767578" />
                  <Point X="2.017596923828" Y="2.449447265625" />
                  <Point X="2.014408203125" Y="2.427529541016" />
                  <Point X="2.013454589844" Y="2.416476318359" />
                  <Point X="2.012780395508" Y="2.392072509766" />
                  <Point X="2.012845458984" Y="2.385062011719" />
                  <Point X="2.01407434082" Y="2.364078857422" />
                  <Point X="2.021782226562" Y="2.300155273438" />
                  <Point X="2.023800537109" Y="2.289034179688" />
                  <Point X="2.029143554687" Y="2.267110351562" />
                  <Point X="2.032468383789" Y="2.256307617188" />
                  <Point X="2.04073449707" Y="2.234219238281" />
                  <Point X="2.045319091797" Y="2.223886962891" />
                  <Point X="2.055682373047" Y="2.203841796875" />
                  <Point X="2.064864990234" Y="2.189112304687" />
                  <Point X="2.104418701172" Y="2.1308203125" />
                  <Point X="2.111219726562" Y="2.121965576172" />
                  <Point X="2.125815185547" Y="2.105114257813" />
                  <Point X="2.133609619141" Y="2.097117675781" />
                  <Point X="2.151926025391" Y="2.080413330078" />
                  <Point X="2.15711328125" Y="2.076012451172" />
                  <Point X="2.173274658203" Y="2.063576904297" />
                  <Point X="2.231566894531" Y="2.02402331543" />
                  <Point X="2.241280517578" Y="2.018244873047" />
                  <Point X="2.261325683594" Y="2.007882080078" />
                  <Point X="2.271657226562" Y="2.003297851562" />
                  <Point X="2.293745605469" Y="1.995032104492" />
                  <Point X="2.304545410156" Y="1.991708007812" />
                  <Point X="2.326464355469" Y="1.986365722656" />
                  <Point X="2.343084716797" Y="1.983683959961" />
                  <Point X="2.407008300781" Y="1.975975952148" />
                  <Point X="2.418369628906" Y="1.975292236328" />
                  <Point X="2.441092041016" Y="1.975288574219" />
                  <Point X="2.452453125" Y="1.975968505859" />
                  <Point X="2.477717773438" Y="1.979007568359" />
                  <Point X="2.484361572266" Y="1.980046386719" />
                  <Point X="2.504110351562" Y="1.984097290039" />
                  <Point X="2.578034667969" Y="2.003865600586" />
                  <Point X="2.583994384766" Y="2.005670654297" />
                  <Point X="2.604404541016" Y="2.013067260742" />
                  <Point X="2.627655029297" Y="2.02357409668" />
                  <Point X="2.636033935547" Y="2.027872558594" />
                  <Point X="3.940404785156" Y="2.780951416016" />
                  <Point X="4.043951416016" Y="2.637045410156" />
                  <Point X="4.055580322266" Y="2.617828613281" />
                  <Point X="4.136884277344" Y="2.483472412109" />
                  <Point X="3.262842773438" Y="1.812795654297" />
                  <Point X="3.172951660156" Y="1.743819702148" />
                  <Point X="3.166446289063" Y="1.738348754883" />
                  <Point X="3.147958251953" Y="1.720814208984" />
                  <Point X="3.134134521484" Y="1.705591430664" />
                  <Point X="3.123997070312" Y="1.69344909668" />
                  <Point X="3.070793701172" Y="1.624041015625" />
                  <Point X="3.064458251953" Y="1.614668701172" />
                  <Point X="3.052946533203" Y="1.595237792969" />
                  <Point X="3.047770263672" Y="1.585179199219" />
                  <Point X="3.037595214844" Y="1.562128417969" />
                  <Point X="3.035076660156" Y="1.555821655273" />
                  <Point X="3.028434570312" Y="1.536573364258" />
                  <Point X="3.008616210938" Y="1.465707641602" />
                  <Point X="3.006225097656" Y="1.454660888672" />
                  <Point X="3.002771728516" Y="1.432362426758" />
                  <Point X="3.001709472656" Y="1.421110717773" />
                  <Point X="3.000893310547" Y="1.397540527344" />
                  <Point X="3.001174804688" Y="1.386241088867" />
                  <Point X="3.003077880859" Y="1.363755981445" />
                  <Point X="3.006099609375" Y="1.345784301758" />
                  <Point X="3.022368408203" Y="1.266937133789" />
                  <Point X="3.025336425781" Y="1.255934936523" />
                  <Point X="3.032568603516" Y="1.234364868164" />
                  <Point X="3.036832763672" Y="1.223797241211" />
                  <Point X="3.047838867188" Y="1.200631591797" />
                  <Point X="3.060727050781" Y="1.177737060547" />
                  <Point X="3.1049765625" Y="1.110480102539" />
                  <Point X="3.111739257813" Y="1.101424438477" />
                  <Point X="3.126292480469" Y="1.084179931641" />
                  <Point X="3.134083007812" Y="1.075991333008" />
                  <Point X="3.151326904297" Y="1.059901367188" />
                  <Point X="3.160034179688" Y="1.052696166992" />
                  <Point X="3.178243896484" Y="1.039370361328" />
                  <Point X="3.193264648438" Y="1.030143188477" />
                  <Point X="3.257388427734" Y="0.994047180176" />
                  <Point X="3.267842529297" Y="0.988977783203" />
                  <Point X="3.289290283203" Y="0.980153442383" />
                  <Point X="3.300283935547" Y="0.976398193359" />
                  <Point X="3.325720703125" Y="0.969414123535" />
                  <Point X="3.3511328125" Y="0.96427130127" />
                  <Point X="3.437832275391" Y="0.952812927246" />
                  <Point X="3.444030273438" Y="0.952199707031" />
                  <Point X="3.465716796875" Y="0.951222961426" />
                  <Point X="3.491218017578" Y="0.952032531738" />
                  <Point X="3.500603515625" Y="0.952797302246" />
                  <Point X="4.704704101562" Y="1.11132019043" />
                  <Point X="4.752684570312" Y="0.914231201172" />
                  <Point X="4.75634765625" Y="0.890700866699" />
                  <Point X="4.78387109375" Y="0.713921020508" />
                  <Point X="3.795009033203" Y="0.448956176758" />
                  <Point X="3.691991943359" Y="0.421352722168" />
                  <Point X="3.683724853516" Y="0.418727752686" />
                  <Point X="3.659471679688" Y="0.40936630249" />
                  <Point X="3.640060302734" Y="0.400121643066" />
                  <Point X="3.626673828125" Y="0.393079467773" />
                  <Point X="3.541494384766" Y="0.343844116211" />
                  <Point X="3.531889648438" Y="0.337485870361" />
                  <Point X="3.513520751953" Y="0.323653839111" />
                  <Point X="3.504756591797" Y="0.316180480957" />
                  <Point X="3.486046386719" Y="0.298155792236" />
                  <Point X="3.4683984375" Y="0.278622131348" />
                  <Point X="3.417290771484" Y="0.213498855591" />
                  <Point X="3.410854248047" Y="0.204207992554" />
                  <Point X="3.399130126953" Y="0.184927734375" />
                  <Point X="3.393842529297" Y="0.174938339233" />
                  <Point X="3.384068847656" Y="0.15347467041" />
                  <Point X="3.380005126953" Y="0.142927947998" />
                  <Point X="3.373158935547" Y="0.12142666626" />
                  <Point X="3.368910400391" Y="0.102816848755" />
                  <Point X="3.351874267578" Y="0.01386173439" />
                  <Point X="3.350391357422" Y="0.002347323656" />
                  <Point X="3.348841308594" Y="-0.020775873184" />
                  <Point X="3.348774169922" Y="-0.03238451004" />
                  <Point X="3.350240234375" Y="-0.058849494934" />
                  <Point X="3.353340332031" Y="-0.084077781677" />
                  <Point X="3.370376220703" Y="-0.173032897949" />
                  <Point X="3.373158935547" Y="-0.18398789978" />
                  <Point X="3.380005371094" Y="-0.20548916626" />
                  <Point X="3.384069091797" Y="-0.216035751343" />
                  <Point X="3.393843017578" Y="-0.237499420166" />
                  <Point X="3.399130371094" Y="-0.247488220215" />
                  <Point X="3.410853271484" Y="-0.266766540527" />
                  <Point X="3.421687011719" Y="-0.281660644531" />
                  <Point X="3.472794677734" Y="-0.346783782959" />
                  <Point X="3.480671386719" Y="-0.355625488281" />
                  <Point X="3.497463623047" Y="-0.372260559082" />
                  <Point X="3.506379150391" Y="-0.380053741455" />
                  <Point X="3.528021728516" Y="-0.396711273193" />
                  <Point X="3.548824951172" Y="-0.410641235352" />
                  <Point X="3.634004394531" Y="-0.459876586914" />
                  <Point X="3.639496582031" Y="-0.462815460205" />
                  <Point X="3.659157958984" Y="-0.472016693115" />
                  <Point X="3.683027587891" Y="-0.481027709961" />
                  <Point X="3.691991943359" Y="-0.48391293335" />
                  <Point X="4.784876953125" Y="-0.776750610352" />
                  <Point X="4.761613769531" Y="-0.931051269531" />
                  <Point X="4.756918457031" Y="-0.951625610352" />
                  <Point X="4.727801269531" Y="-1.079219604492" />
                  <Point X="3.556252929688" Y="-0.924982299805" />
                  <Point X="3.436781982422" Y="-0.909253662109" />
                  <Point X="3.424843261719" Y="-0.908442077637" />
                  <Point X="3.400959228516" Y="-0.90832611084" />
                  <Point X="3.389013671875" Y="-0.90902166748" />
                  <Point X="3.35748046875" Y="-0.912861938477" />
                  <Point X="3.340094238281" Y="-0.915803466797" />
                  <Point X="3.172917236328" Y="-0.952139709473" />
                  <Point X="3.161470947266" Y="-0.955390258789" />
                  <Point X="3.131101074219" Y="-0.96611114502" />
                  <Point X="3.110420898438" Y="-0.976388000488" />
                  <Point X="3.078408691406" Y="-0.997500732422" />
                  <Point X="3.063479736328" Y="-1.009688415527" />
                  <Point X="3.042283447266" Y="-1.030921142578" />
                  <Point X="3.030652832031" Y="-1.043681884766" />
                  <Point X="2.929604980469" Y="-1.165211303711" />
                  <Point X="2.923182128906" Y="-1.173896728516" />
                  <Point X="2.907155273438" Y="-1.198365844727" />
                  <Point X="2.897613037109" Y="-1.21722668457" />
                  <Point X="2.885320556641" Y="-1.250193481445" />
                  <Point X="2.880713867188" Y="-1.267250610352" />
                  <Point X="2.875871826172" Y="-1.29534777832" />
                  <Point X="2.873910888672" Y="-1.310204833984" />
                  <Point X="2.859428222656" Y="-1.467590698242" />
                  <Point X="2.85908203125" Y="-1.479483398438" />
                  <Point X="2.860162597656" Y="-1.511670043945" />
                  <Point X="2.863779541016" Y="-1.534633789063" />
                  <Point X="2.874419921875" Y="-1.571793945312" />
                  <Point X="2.881649902344" Y="-1.589827270508" />
                  <Point X="2.896123291016" Y="-1.617375488281" />
                  <Point X="2.904502197266" Y="-1.631755859375" />
                  <Point X="2.997020263672" Y="-1.775661865234" />
                  <Point X="3.001741943359" Y="-1.782353271484" />
                  <Point X="3.019793945312" Y="-1.804450317383" />
                  <Point X="3.043489501953" Y="-1.828120361328" />
                  <Point X="3.052796142578" Y="-1.836277587891" />
                  <Point X="4.087170898438" Y="-2.629981933594" />
                  <Point X="4.045490478516" Y="-2.697427490234" />
                  <Point X="4.035778076172" Y="-2.711227050781" />
                  <Point X="4.001274414063" Y="-2.760251953125" />
                  <Point X="2.954859619141" Y="-2.156103759766" />
                  <Point X="2.848454589844" Y="-2.094670654297" />
                  <Point X="2.837418701172" Y="-2.089219970703" />
                  <Point X="2.814736328125" Y="-2.079791503906" />
                  <Point X="2.80308984375" Y="-2.075813720703" />
                  <Point X="2.769853027344" Y="-2.066818603516" />
                  <Point X="2.753985351562" Y="-2.063245361328" />
                  <Point X="2.555018066406" Y="-2.027312011719" />
                  <Point X="2.543199707031" Y="-2.025934692383" />
                  <Point X="2.511038818359" Y="-2.024217285156" />
                  <Point X="2.487677734375" Y="-2.025860717773" />
                  <Point X="2.449416992188" Y="-2.033369628906" />
                  <Point X="2.430676025391" Y="-2.039108154297" />
                  <Point X="2.400779296875" Y="-2.051765136719" />
                  <Point X="2.386363769531" Y="-2.058595214844" />
                  <Point X="2.221070800781" Y="-2.145587646484" />
                  <Point X="2.2118125" Y="-2.151154296875" />
                  <Point X="2.187640625" Y="-2.167628662109" />
                  <Point X="2.171404052734" Y="-2.181620849609" />
                  <Point X="2.147114990234" Y="-2.207879150391" />
                  <Point X="2.136217529297" Y="-2.222161865234" />
                  <Point X="2.11983984375" Y="-2.248455566406" />
                  <Point X="2.112977050781" Y="-2.260419921875" />
                  <Point X="2.025984741211" Y="-2.425712890625" />
                  <Point X="2.02111328125" Y="-2.436568847656" />
                  <Point X="2.009794189453" Y="-2.466720458984" />
                  <Point X="2.004311523438" Y="-2.489646728516" />
                  <Point X="1.999983886719" Y="-2.528703857422" />
                  <Point X="1.999861572266" Y="-2.548458740234" />
                  <Point X="2.003166503906" Y="-2.582083007812" />
                  <Point X="2.005280029297" Y="-2.597265136719" />
                  <Point X="2.041213500977" Y="-2.796232421875" />
                  <Point X="2.043015014648" Y="-2.804221435547" />
                  <Point X="2.051236083984" Y="-2.83154296875" />
                  <Point X="2.064069580078" Y="-2.862479736328" />
                  <Point X="2.069546630859" Y="-2.873578613281" />
                  <Point X="2.735893310547" Y="-4.027724121094" />
                  <Point X="2.723754394531" Y="-4.036083740234" />
                  <Point X="1.915111328125" Y="-2.982240478516" />
                  <Point X="1.833914550781" Y="-2.876422851562" />
                  <Point X="1.828653564453" Y="-2.870146240234" />
                  <Point X="1.80883190918" Y="-2.849627197266" />
                  <Point X="1.783252685547" Y="-2.828005126953" />
                  <Point X="1.773299316406" Y="-2.820647216797" />
                  <Point X="1.756411376953" Y="-2.809789794922" />
                  <Point X="1.56017590332" Y="-2.683628417969" />
                  <Point X="1.549783569336" Y="-2.677833251953" />
                  <Point X="1.520727905273" Y="-2.663939453125" />
                  <Point X="1.498239990234" Y="-2.656462402344" />
                  <Point X="1.459453613281" Y="-2.648764160156" />
                  <Point X="1.439655517578" Y="-2.646955566406" />
                  <Point X="1.404728271484" Y="-2.647434814453" />
                  <Point X="1.389924560547" Y="-2.648216064453" />
                  <Point X="1.175307739258" Y="-2.667965332031" />
                  <Point X="1.16462512207" Y="-2.669564941406" />
                  <Point X="1.135990112305" Y="-2.675534423828" />
                  <Point X="1.115402709961" Y="-2.682355224609" />
                  <Point X="1.082499267578" Y="-2.697617675781" />
                  <Point X="1.066783203125" Y="-2.706833496094" />
                  <Point X="1.039688598633" Y="-2.726439208984" />
                  <Point X="1.029596801758" Y="-2.734271240234" />
                  <Point X="0.863874938965" Y="-2.872063476562" />
                  <Point X="0.852652832031" Y="-2.883088378906" />
                  <Point X="0.832182006836" Y="-2.906838623047" />
                  <Point X="0.822933044434" Y="-2.919564208984" />
                  <Point X="0.806040710449" Y="-2.947390869141" />
                  <Point X="0.799018859863" Y="-2.961468261719" />
                  <Point X="0.787394042969" Y="-2.990589355469" />
                  <Point X="0.782791320801" Y="-3.0056328125" />
                  <Point X="0.77852734375" Y="-3.025251708984" />
                  <Point X="0.728977355957" Y="-3.253220703125" />
                  <Point X="0.727584594727" Y="-3.261290039062" />
                  <Point X="0.724724487305" Y="-3.289677734375" />
                  <Point X="0.7247421875" Y="-3.323170410156" />
                  <Point X="0.725554992676" Y="-3.335520019531" />
                  <Point X="0.833091674805" Y="-4.152341308594" />
                  <Point X="0.686806945801" Y="-3.606399658203" />
                  <Point X="0.655064941406" Y="-3.487936523438" />
                  <Point X="0.652606262207" Y="-3.480124511719" />
                  <Point X="0.642145141602" Y="-3.453580322266" />
                  <Point X="0.626787231445" Y="-3.423816162109" />
                  <Point X="0.62040802002" Y="-3.413211181641" />
                  <Point X="0.607434448242" Y="-3.394518310547" />
                  <Point X="0.45668057251" Y="-3.177310791016" />
                  <Point X="0.446671264648" Y="-3.165173583984" />
                  <Point X="0.424786987305" Y="-3.142717773438" />
                  <Point X="0.412912322998" Y="-3.132399169922" />
                  <Point X="0.386657196045" Y="-3.113155273438" />
                  <Point X="0.373242492676" Y="-3.104937988281" />
                  <Point X="0.345241638184" Y="-3.090829833984" />
                  <Point X="0.330655059814" Y="-3.084938720703" />
                  <Point X="0.310578765869" Y="-3.078707763672" />
                  <Point X="0.077296104431" Y="-3.006305419922" />
                  <Point X="0.063377418518" Y="-3.003109619141" />
                  <Point X="0.035217838287" Y="-2.998840087891" />
                  <Point X="0.020976791382" Y="-2.997766601562" />
                  <Point X="-0.008664708138" Y="-2.997766601562" />
                  <Point X="-0.022904714584" Y="-2.99883984375" />
                  <Point X="-0.05106325531" Y="-3.003108886719" />
                  <Point X="-0.064981491089" Y="-3.0063046875" />
                  <Point X="-0.085057922363" Y="-3.012535400391" />
                  <Point X="-0.31834072876" Y="-3.084937988281" />
                  <Point X="-0.33292791748" Y="-3.090829101562" />
                  <Point X="-0.360930267334" Y="-3.104937744141" />
                  <Point X="-0.374345275879" Y="-3.113155273438" />
                  <Point X="-0.400600524902" Y="-3.132399169922" />
                  <Point X="-0.412474914551" Y="-3.142717529297" />
                  <Point X="-0.434358459473" Y="-3.165172363281" />
                  <Point X="-0.444367462158" Y="-3.177308837891" />
                  <Point X="-0.457341339111" Y="-3.196001464844" />
                  <Point X="-0.608095031738" Y="-3.413209228516" />
                  <Point X="-0.612470092773" Y="-3.420131835938" />
                  <Point X="-0.625976745605" Y="-3.445265136719" />
                  <Point X="-0.638777832031" Y="-3.476215820312" />
                  <Point X="-0.642752990723" Y="-3.487936523438" />
                  <Point X="-0.985425231934" Y="-4.766807128906" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.136174687492" Y="4.663479041009" />
                  <Point X="0.352955705277" Y="4.690829650548" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.347195110017" Y="4.715279455778" />
                  <Point X="-0.803822224302" Y="4.73122522598" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.54361751563" Y="4.554192917218" />
                  <Point X="0.327721163971" Y="4.59665295344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.321483829743" Y="4.619323691378" />
                  <Point X="-1.17129417015" Y="4.648999722387" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.833617441932" Y="4.44900798993" />
                  <Point X="0.302486622666" Y="4.502476256332" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.295772549468" Y="4.523367926978" />
                  <Point X="-1.472792214441" Y="4.564470359386" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.059253956514" Y="4.346070682507" />
                  <Point X="0.27725208136" Y="4.408299559223" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.270061269194" Y="4.427412162578" />
                  <Point X="-1.466963745102" Y="4.469208918043" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.272943382248" Y="4.24355057662" />
                  <Point X="0.252017540055" Y="4.314122862115" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.24434998892" Y="4.331456398178" />
                  <Point X="-1.465413429383" Y="4.374096873117" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.446507405162" Y="4.142431680675" />
                  <Point X="0.220539114064" Y="4.220164206265" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.215120380523" Y="4.235377771052" />
                  <Point X="-1.491447340931" Y="4.279948090633" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.620071428076" Y="4.041312784731" />
                  <Point X="0.14284955374" Y="4.127819278784" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.143572395878" Y="4.137821353664" />
                  <Point X="-1.529205768509" Y="4.18620873727" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.146065774788" Y="4.207749963358" />
                  <Point X="-2.292311530346" Y="4.212856977677" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.762422380266" Y="3.941283873234" />
                  <Point X="-1.628321385432" Y="4.094612024173" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.045715701098" Y="4.109187754858" />
                  <Point X="-2.453290489616" Y="4.123420580098" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.785168118974" Y="3.845431667827" />
                  <Point X="-2.61426940888" Y="4.033984181123" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.731371057879" Y="3.752252395889" />
                  <Point X="-2.741834432788" Y="3.943380943209" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.677573996784" Y="3.65907312395" />
                  <Point X="-2.860418097849" Y="3.852464069334" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.623776935688" Y="3.565893852012" />
                  <Point X="-2.97900176291" Y="3.761547195459" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.569979874593" Y="3.472714580073" />
                  <Point X="-3.041344927747" Y="3.668666360039" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.516182813498" Y="3.379535308134" />
                  <Point X="-2.985333952604" Y="3.571652506979" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.462385752402" Y="3.286356036196" />
                  <Point X="-2.929322977461" Y="3.474638653918" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.408588691307" Y="3.193176764257" />
                  <Point X="-2.873312002318" Y="3.377624800858" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.354791630212" Y="3.099997492319" />
                  <Point X="-2.817301027175" Y="3.280610947797" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.300994569116" Y="3.00681822038" />
                  <Point X="-2.768554102211" Y="3.183850760959" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.247197508021" Y="2.913638948442" />
                  <Point X="-2.752632158854" Y="3.088236847737" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.956272583658" Y="2.758898824973" />
                  <Point X="3.905292107844" Y="2.760679102417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.193400446926" Y="2.820459676503" />
                  <Point X="-2.751005518528" Y="2.993122137496" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.711901833741" Y="3.026677376225" />
                  <Point X="-3.732269889756" Y="3.027388644415" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.026433824769" Y="2.661390833736" />
                  <Point X="3.750037488327" Y="2.67104280649" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.13960338583" Y="2.727280404565" />
                  <Point X="-2.783893030922" Y="2.899212688028" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.536656905147" Y="2.925499781761" />
                  <Point X="-3.804268198308" Y="2.934844974043" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.088044227727" Y="2.564181444348" />
                  <Point X="3.59478286881" Y="2.581406510562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.085806368199" Y="2.634101131108" />
                  <Point X="-2.864970672341" Y="2.806986074946" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.361411976554" Y="2.824322187297" />
                  <Point X="-3.87626650686" Y="2.842301303671" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.116872528209" Y="2.468116831204" />
                  <Point X="3.439528249294" Y="2.491770214634" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.041554374443" Y="2.540588538073" />
                  <Point X="-3.948264815412" Y="2.7497576333" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.998383254104" Y="2.377196661124" />
                  <Point X="3.284273629777" Y="2.402133918707" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.017151089047" Y="2.446382812869" />
                  <Point X="-4.015864948997" Y="2.657060375274" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.879893979999" Y="2.286276491044" />
                  <Point X="3.12901901026" Y="2.312497622779" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.01560570105" Y="2.351378872299" />
                  <Point X="-4.070240912172" Y="2.563901319041" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.761404705894" Y="2.195356320963" />
                  <Point X="2.973764390744" Y="2.222861326851" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.032686607985" Y="2.255724487177" />
                  <Point X="-4.124616875347" Y="2.470742262809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.642915431789" Y="2.104436150883" />
                  <Point X="2.818509771227" Y="2.133225030923" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.085416121514" Y="2.158825225281" />
                  <Point X="-4.178992838521" Y="2.377583206576" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.524426157684" Y="2.013515980803" />
                  <Point X="2.66325515171" Y="2.043588734996" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.177745741766" Y="2.060543097186" />
                  <Point X="-4.213880559354" Y="2.283743605925" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.405936883579" Y="1.922595810723" />
                  <Point X="-4.08409216592" Y="2.184153388647" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.287447609474" Y="1.831675640643" />
                  <Point X="-3.954303772485" Y="2.084563171369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.169294113801" Y="1.740743744922" />
                  <Point X="-3.824515379051" Y="1.984972954091" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.089520470798" Y="1.648471595212" />
                  <Point X="-3.694726985616" Y="1.885382736812" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.034903876889" Y="1.55532094199" />
                  <Point X="-3.564938592182" Y="1.785792519534" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.007643756525" Y="1.461214979661" />
                  <Point X="-3.476096650004" Y="1.687632183842" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.002860521929" Y="1.366324107185" />
                  <Point X="-3.431809546266" Y="1.591027737392" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.021610275401" Y="1.270611444658" />
                  <Point X="-3.423111788258" Y="1.495666098281" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.063117249666" Y="1.174104082469" />
                  <Point X="-3.45094771782" Y="1.401580243653" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.726703738889" Y="1.020952455441" />
                  <Point X="4.166805824075" Y="1.040504521463" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.133515523603" Y="1.076587813864" />
                  <Point X="-3.502811972959" Y="1.308333476643" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.750043597806" Y="0.925079502899" />
                  <Point X="3.596138693421" Y="0.965374750081" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.30317140244" Y="0.975605393318" />
                  <Point X="-3.661398298773" Y="1.218813526463" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.956723348722" Y="1.229126504457" />
                  <Point X="-4.651162253077" Y="1.253376845362" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.765881119568" Y="0.829468537744" />
                  <Point X="-4.676679587418" Y="1.159210023605" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.780761904469" Y="0.733890982576" />
                  <Point X="-4.702196921758" Y="1.065043201847" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.535591250852" Y="0.647394623749" />
                  <Point X="-4.727714306222" Y="0.970876381839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.22173414459" Y="0.563296848702" />
                  <Point X="-4.743341063038" Y="0.876364173504" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.907877038328" Y="0.479199073655" />
                  <Point X="-4.757334942184" Y="0.781794943823" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.628245221576" Y="0.393906125162" />
                  <Point X="-4.77132882133" Y="0.687225714143" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.491714242278" Y="0.30361598531" />
                  <Point X="-4.785322700476" Y="0.592656484462" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.415706705704" Y="0.211212320266" />
                  <Point X="-4.381110048252" Y="0.4834831609" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.372301343071" Y="0.11767016222" />
                  <Point X="-3.973185899186" Y="0.374180229012" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.353674684756" Y="0.023262712753" />
                  <Point X="-3.565261750121" Y="0.264877297124" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.35182307989" Y="-0.071730534488" />
                  <Point X="-3.371185279164" Y="0.163042090709" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.369297200078" Y="-0.16739865092" />
                  <Point X="-3.312277004916" Y="0.065927061735" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.409077148107" Y="-0.263845704024" />
                  <Point X="-3.295973728787" Y="-0.029700167921" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.486717351516" Y="-0.361614866379" />
                  <Point X="-3.310957384608" Y="-0.124234833839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.637882125309" Y="-0.461951563308" />
                  <Point X="-3.363824670611" Y="-0.217446574239" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.01378138843" Y="-0.570136161536" />
                  <Point X="-3.495895290797" Y="-0.307892473263" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.421705441602" Y="-0.679439090075" />
                  <Point X="-3.8065890856" Y="-0.392100713581" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.783312914365" Y="-0.787124607986" />
                  <Point X="-4.120446047749" Y="-0.47619849366" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.769056539049" Y="-0.881684671099" />
                  <Point X="-4.434303009899" Y="-0.56029627374" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.751327996684" Y="-0.976123483466" />
                  <Point X="3.65351408288" Y="-0.937786976837" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.296337429605" Y="-0.92531409326" />
                  <Point X="-4.748159972048" Y="-0.64439405382" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.729807104695" Y="-1.070429864066" />
                  <Point X="4.636209991215" Y="-1.067161380841" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.061017922203" Y="-1.012154461693" />
                  <Point X="-4.774455664201" Y="-0.738533694724" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.980176524156" Y="-1.104389324575" />
                  <Point X="-4.760792161847" Y="-0.834068741449" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.908095654093" Y="-1.196930111835" />
                  <Point X="-4.747128659493" Y="-0.929603788173" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.876640118617" Y="-1.29088956704" />
                  <Point X="-4.727678365609" Y="-1.025340914111" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.86697210923" Y="-1.385609859421" />
                  <Point X="-3.024475518497" Y="-1.179875974841" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.67658355452" Y="-1.157103860431" />
                  <Point X="-4.703179198802" Y="-1.121254350576" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.859112578539" Y="-1.48039330527" />
                  <Point X="-2.960090953966" Y="-1.277182240086" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.247251200261" Y="-1.232233613826" />
                  <Point X="-4.678680031996" Y="-1.217167787041" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.876124379926" Y="-1.576045277173" />
                  <Point X="-2.949140072925" Y="-1.372622559987" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.931031545666" Y="-1.67302058436" />
                  <Point X="-2.971117436647" Y="-1.466913000243" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.993548417783" Y="-1.770261628349" />
                  <Point X="-3.0332743745" Y="-1.559800338852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.0952737931" Y="-1.86887186344" />
                  <Point X="-3.148412787343" Y="-1.650837523586" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.22506217966" Y="-1.968462080478" />
                  <Point X="-3.26690214375" Y="-1.741757690792" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.35485056622" Y="-2.068052297517" />
                  <Point X="2.642948117857" Y="-2.043192116217" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.44043421561" Y="-2.036120174917" />
                  <Point X="-3.385391496662" Y="-1.83267785812" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.484638952781" Y="-2.167642514555" />
                  <Point X="2.942025635839" Y="-2.148694039991" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.260396151075" Y="-2.124891013875" />
                  <Point X="-3.503880849574" Y="-1.923598025448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.614427339341" Y="-2.267232731593" />
                  <Point X="3.117270233541" Y="-2.2498716229" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.141084888737" Y="-2.215782479493" />
                  <Point X="-3.622370202485" Y="-2.014518192776" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.744215725902" Y="-2.366822948631" />
                  <Point X="3.292514853394" Y="-2.351049206583" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.087427327322" Y="-2.308966622868" />
                  <Point X="-3.740859555397" Y="-2.105438360105" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.874004112462" Y="-2.466413165669" />
                  <Point X="3.467759473247" Y="-2.452226790266" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.03830199496" Y="-2.402309035169" />
                  <Point X="-3.859348908309" Y="-2.196358527433" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.003792499023" Y="-2.566003382707" />
                  <Point X="3.6430040931" Y="-2.553404373949" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.003590412787" Y="-2.496154786717" />
                  <Point X="-3.97783826122" Y="-2.287278694761" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.066608679135" Y="-2.663254878762" />
                  <Point X="3.818248712954" Y="-2.654581957632" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.00444159813" Y="-2.591242417473" />
                  <Point X="-2.376537634257" Y="-2.43825525155" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.765896685226" Y="-2.424658533882" />
                  <Point X="-4.096327614132" Y="-2.378198862089" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.004173674801" Y="-2.756132507076" />
                  <Point X="3.993493332807" Y="-2.755759541315" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.021467150029" Y="-2.686894869554" />
                  <Point X="1.533066583656" Y="-2.669839545956" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.258960229398" Y="-2.660267541143" />
                  <Point X="-2.321628084718" Y="-2.535230641981" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.921151209563" Y="-2.514294833133" />
                  <Point X="-4.164871652691" Y="-2.470863158227" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.038743555304" Y="-2.782556081629" />
                  <Point X="1.695401931687" Y="-2.770566327934" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.014540423022" Y="-2.746790120134" />
                  <Point X="-2.312954910669" Y="-2.630591422601" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.076405733899" Y="-2.603931132385" />
                  <Point X="-4.106977543916" Y="-2.567942771763" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.072558212556" Y="-2.878794822189" />
                  <Point X="1.828766888716" Y="-2.870281441565" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.904822897235" Y="-2.838016606415" />
                  <Point X="-2.341790572626" Y="-2.724642365805" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.231660258236" Y="-2.693567431636" />
                  <Point X="-4.049083435141" Y="-2.665022385298" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.128569226183" Y="-2.975808676593" />
                  <Point X="1.904162912541" Y="-2.967972235442" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.816601724402" Y="-2.929993761883" />
                  <Point X="-2.395587790674" Y="-2.817821632263" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.386914782572" Y="-2.783203730888" />
                  <Point X="-3.987547961133" Y="-2.76222915811" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.18458023981" Y="-3.072822530997" />
                  <Point X="1.979111775453" Y="-3.065647414116" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.778857289187" Y="-3.023733603869" />
                  <Point X="-2.449384882899" Y="-2.911000903114" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.542169306909" Y="-2.872840030139" />
                  <Point X="-3.913218752978" Y="-2.859882697963" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.240591253437" Y="-3.169836385402" />
                  <Point X="2.05406064016" Y="-3.163322592852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.758351728262" Y="-3.118075440611" />
                  <Point X="0.372579577465" Y="-3.104603980257" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.305419577858" Y="-3.080927728038" />
                  <Point X="-2.503181962542" Y="-3.004180174405" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.697423831245" Y="-2.962476329391" />
                  <Point X="-3.838889544822" Y="-2.957536237816" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.296602267065" Y="-3.266850239806" />
                  <Point X="2.129009504867" Y="-3.260997771589" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.737846146612" Y="-3.21241727663" />
                  <Point X="0.474667756283" Y="-3.203226884726" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.439417172782" Y="-3.171306335622" />
                  <Point X="-2.556979042186" Y="-3.097359445696" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.352613280692" Y="-3.36386409421" />
                  <Point X="2.203958369574" Y="-3.358672950325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.724733650903" Y="-3.307017284898" />
                  <Point X="0.542281891068" Y="-3.300645929049" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.504597077073" Y="-3.264088109917" />
                  <Point X="-2.61077612183" Y="-3.190538716987" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.408624294319" Y="-3.460877948614" />
                  <Point X="2.278907234282" Y="-3.456348129062" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.734361419848" Y="-3.402411400706" />
                  <Point X="0.609895967288" Y="-3.398064971328" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.569011105156" Y="-3.356896629199" />
                  <Point X="-2.664573201474" Y="-3.283717988278" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.464635307947" Y="-3.557891803019" />
                  <Point X="2.353856098989" Y="-3.554023307798" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.746933845184" Y="-3.497908346182" />
                  <Point X="0.656894379111" Y="-3.494764098742" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.627893018664" Y="-3.449898334178" />
                  <Point X="-2.718370281118" Y="-3.376897259568" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.520646321574" Y="-3.654905657423" />
                  <Point X="2.428804963696" Y="-3.651698486535" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.75950627052" Y="-3.593405291657" />
                  <Point X="0.68260556936" Y="-3.590719859998" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.657751983288" Y="-3.543913542866" />
                  <Point X="-2.772167360762" Y="-3.470076530859" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.576657335201" Y="-3.751919511827" />
                  <Point X="2.503753828404" Y="-3.749373665271" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.772078695857" Y="-3.688902237133" />
                  <Point X="0.708316841428" Y="-3.686675624112" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.682986545317" Y="-3.63809023925" />
                  <Point X="-2.825964440405" Y="-3.56325580215" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.632668348829" Y="-3.848933366231" />
                  <Point X="2.578702693111" Y="-3.847048844008" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.784651121193" Y="-3.784399182608" />
                  <Point X="0.734028129478" Y="-3.782631388784" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.708221107345" Y="-3.732266935635" />
                  <Point X="-2.879761520049" Y="-3.656435073441" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.688679362456" Y="-3.945947220636" />
                  <Point X="2.653651557818" Y="-3.944724022744" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.797223546529" Y="-3.879896128084" />
                  <Point X="0.759739417527" Y="-3.878587153455" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.733455669373" Y="-3.82644363202" />
                  <Point X="-1.67835206938" Y="-3.793447122641" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.007959040524" Y="-3.781936993579" />
                  <Point X="-2.933558599693" Y="-3.749614344732" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.809795971866" Y="-3.975393073559" />
                  <Point X="0.785450705576" Y="-3.974542918127" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.758690231402" Y="-3.920620328404" />
                  <Point X="-1.447098613381" Y="-3.896580577981" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.178111106981" Y="-3.871053059196" />
                  <Point X="-2.965254607307" Y="-3.843565402464" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.822368397202" Y="-4.070890019035" />
                  <Point X="0.811161993626" Y="-4.070498682799" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.78392479343" Y="-4.014797024789" />
                  <Point X="-1.334211095111" Y="-3.995580603693" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.313309100279" Y="-3.961389747945" />
                  <Point X="-2.835932336821" Y="-3.943139342371" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.809159355458" Y="-4.108973721173" />
                  <Point X="-1.231084669603" Y="-4.094239764535" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.419009233766" Y="-4.052756524657" />
                  <Point X="-2.696035294738" Y="-4.043082561438" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.834393917487" Y="-4.203150417558" />
                  <Point X="-1.184568543386" Y="-4.190922050165" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.490046455928" Y="-4.145333756904" />
                  <Point X="-2.533335619672" Y="-4.143822065996" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.859628479515" Y="-4.297327113942" />
                  <Point X="-1.165311985748" Y="-4.286652410684" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.884863041543" Y="-4.391503810327" />
                  <Point X="-1.146271535511" Y="-4.382375224566" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.910097603572" Y="-4.485680506712" />
                  <Point X="-1.12723108444" Y="-4.478098038477" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.9353321656" Y="-4.579857203096" />
                  <Point X="-1.120554682628" Y="-4.573389090274" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.960566727629" Y="-4.674033899481" />
                  <Point X="-1.13291128969" Y="-4.668015494756" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="0.00137109375" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.977974609375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.50328112793" Y="-3.655575439453" />
                  <Point X="0.471539031982" Y="-3.537112304688" />
                  <Point X="0.464318664551" Y="-3.521544189453" />
                  <Point X="0.451344970703" Y="-3.502851318359" />
                  <Point X="0.300591033936" Y="-3.285643798828" />
                  <Point X="0.274335754395" Y="-3.266399902344" />
                  <Point X="0.254259597778" Y="-3.260168945312" />
                  <Point X="0.02097677803" Y="-3.187766601562" />
                  <Point X="-0.008664756775" Y="-3.187766601562" />
                  <Point X="-0.028741065979" Y="-3.193997314453" />
                  <Point X="-0.262023895264" Y="-3.266399902344" />
                  <Point X="-0.288279144287" Y="-3.285643798828" />
                  <Point X="-0.301252990723" Y="-3.304336425781" />
                  <Point X="-0.45200680542" Y="-3.521544189453" />
                  <Point X="-0.459227142334" Y="-3.537112304688" />
                  <Point X="-0.82806628418" Y="-4.913638671875" />
                  <Point X="-0.84774395752" Y="-4.987076660156" />
                  <Point X="-1.100246826172" Y="-4.938065429688" />
                  <Point X="-1.122218139648" Y="-4.932412109375" />
                  <Point X="-1.35158972168" Y="-4.873396972656" />
                  <Point X="-1.312971313477" Y="-4.580062011719" />
                  <Point X="-1.309150146484" Y="-4.551038574219" />
                  <Point X="-1.309683227539" Y="-4.5347578125" />
                  <Point X="-1.314479492188" Y="-4.510645996094" />
                  <Point X="-1.370210693359" Y="-4.230466308594" />
                  <Point X="-1.386282714844" Y="-4.20262890625" />
                  <Point X="-1.404766357422" Y="-4.186418945312" />
                  <Point X="-1.619543457031" Y="-3.998063964844" />
                  <Point X="-1.649240722656" Y="-3.985762939453" />
                  <Point X="-1.673772460938" Y="-3.984155029297" />
                  <Point X="-1.958829833984" Y="-3.965471435547" />
                  <Point X="-1.989878540039" Y="-3.973791015625" />
                  <Point X="-2.010319824219" Y="-3.987449462891" />
                  <Point X="-2.247844970703" Y="-4.146159179688" />
                  <Point X="-2.259734375" Y="-4.157294921875" />
                  <Point X="-2.457095214844" Y="-4.414500488281" />
                  <Point X="-2.855831054688" Y="-4.16761328125" />
                  <Point X="-2.886260498047" Y="-4.14418359375" />
                  <Point X="-3.228581054688" Y="-3.880608154297" />
                  <Point X="-2.565022949219" Y="-2.731291748047" />
                  <Point X="-2.506033691406" Y="-2.629119628906" />
                  <Point X="-2.499762695312" Y="-2.597593261719" />
                  <Point X="-2.515354492188" Y="-2.567389648438" />
                  <Point X="-2.531328369141" Y="-2.551415771484" />
                  <Point X="-2.560157226562" Y="-2.537198730469" />
                  <Point X="-2.59168359375" Y="-2.543469726562" />
                  <Point X="-3.778910644531" Y="-3.228915771484" />
                  <Point X="-3.842959228516" Y="-3.265894042969" />
                  <Point X="-3.843802490234" Y="-3.264786376953" />
                  <Point X="-4.161703613281" Y="-2.847129394531" />
                  <Point X="-4.183516113281" Y="-2.810552978516" />
                  <Point X="-4.431020019531" Y="-2.395527099609" />
                  <Point X="-3.266998779297" Y="-1.502342163086" />
                  <Point X="-3.163786865234" Y="-1.423144897461" />
                  <Point X="-3.145211425781" Y="-1.39365625" />
                  <Point X="-3.138117431641" Y="-1.366266113281" />
                  <Point X="-3.140326416016" Y="-1.334595581055" />
                  <Point X="-3.163257568359" Y="-1.309404296875" />
                  <Point X="-3.187641601562" Y="-1.295052734375" />
                  <Point X="-3.219529296875" Y="-1.288571044922" />
                  <Point X="-4.718291015625" Y="-1.48588671875" />
                  <Point X="-4.803283203125" Y="-1.497076171875" />
                  <Point X="-4.927393066406" Y="-1.01119152832" />
                  <Point X="-4.9331640625" Y="-0.970842163086" />
                  <Point X="-4.998396484375" Y="-0.514742126465" />
                  <Point X="-3.675037841797" Y="-0.160149169922" />
                  <Point X="-3.557463867188" Y="-0.128645431519" />
                  <Point X="-3.539696533203" Y="-0.119898742676" />
                  <Point X="-3.514142822266" Y="-0.102162895203" />
                  <Point X="-3.494166015625" Y="-0.073546012878" />
                  <Point X="-3.485647949219" Y="-0.046101043701" />
                  <Point X="-3.486380859375" Y="-0.014097141266" />
                  <Point X="-3.494898925781" Y="0.013347833633" />
                  <Point X="-3.516342041016" Y="0.041129138947" />
                  <Point X="-3.541895751953" Y="0.05886498642" />
                  <Point X="-3.557463867188" Y="0.066085334778" />
                  <Point X="-4.92365625" Y="0.432155487061" />
                  <Point X="-4.998186523438" Y="0.452125823975" />
                  <Point X="-4.997776367188" Y="0.454897949219" />
                  <Point X="-4.917645019531" Y="0.99641809082" />
                  <Point X="-4.906026855469" Y="1.039292602539" />
                  <Point X="-4.773516113281" Y="1.528298583984" />
                  <Point X="-3.837546386719" Y="1.405075927734" />
                  <Point X="-3.753266357422" Y="1.393980102539" />
                  <Point X="-3.731704833984" Y="1.395866577148" />
                  <Point X="-3.726837646484" Y="1.397401245117" />
                  <Point X="-3.670278808594" Y="1.415234130859" />
                  <Point X="-3.651534423828" Y="1.426056152344" />
                  <Point X="-3.639119873047" Y="1.443785888672" />
                  <Point X="-3.637166748047" Y="1.448501098633" />
                  <Point X="-3.614472412109" Y="1.503290161133" />
                  <Point X="-3.610714111328" Y="1.52460534668" />
                  <Point X="-3.616315917969" Y="1.54551171875" />
                  <Point X="-3.618672363281" Y="1.550038696289" />
                  <Point X="-3.646055664062" Y="1.602641479492" />
                  <Point X="-3.659968017578" Y="1.619221679688" />
                  <Point X="-4.443619628906" Y="2.220538574219" />
                  <Point X="-4.47610546875" Y="2.245466064453" />
                  <Point X="-4.471375976562" Y="2.253569335938" />
                  <Point X="-4.160014648438" Y="2.787006591797" />
                  <Point X="-4.129240234375" Y="2.826562255859" />
                  <Point X="-3.774671142578" Y="3.282310791016" />
                  <Point X="-3.210919189453" Y="2.956828369141" />
                  <Point X="-3.159156982422" Y="2.926943359375" />
                  <Point X="-3.138514892578" Y="2.920434814453" />
                  <Point X="-3.131735839844" Y="2.919841796875" />
                  <Point X="-3.052965576172" Y="2.912950195312" />
                  <Point X="-3.031506835938" Y="2.915775390625" />
                  <Point X="-3.013252441406" Y="2.927404541016" />
                  <Point X="-3.008440673828" Y="2.932216308594" />
                  <Point X="-2.952528808594" Y="2.988127929688" />
                  <Point X="-2.940899414062" Y="3.006382324219" />
                  <Point X="-2.93807421875" Y="3.027841308594" />
                  <Point X="-2.938667236328" Y="3.034620361328" />
                  <Point X="-2.945558837891" Y="3.113390869141" />
                  <Point X="-2.952067382812" Y="3.134032958984" />
                  <Point X="-3.299327148438" Y="3.735504150391" />
                  <Point X="-3.295169921875" Y="3.758560302734" />
                  <Point X="-2.752875976562" Y="4.174331542969" />
                  <Point X="-2.704408447266" Y="4.201259277344" />
                  <Point X="-2.141548339844" Y="4.513972167969" />
                  <Point X="-1.983672851562" Y="4.308224121094" />
                  <Point X="-1.967826660156" Y="4.287573242188" />
                  <Point X="-1.951246826172" Y="4.273660644531" />
                  <Point X="-1.943702026367" Y="4.269732910156" />
                  <Point X="-1.856030761719" Y="4.22409375" />
                  <Point X="-1.835124145508" Y="4.2184921875" />
                  <Point X="-1.813808837891" Y="4.222250488281" />
                  <Point X="-1.805950195312" Y="4.225505859375" />
                  <Point X="-1.714634887695" Y="4.263330078125" />
                  <Point X="-1.696905273438" Y="4.275744628906" />
                  <Point X="-1.686083496094" Y="4.294488769531" />
                  <Point X="-1.683525634766" Y="4.302601074219" />
                  <Point X="-1.653804077148" Y="4.396865234375" />
                  <Point X="-1.651917602539" Y="4.418426757812" />
                  <Point X="-1.689137695313" Y="4.701141113281" />
                  <Point X="-1.670126953125" Y="4.706471191406" />
                  <Point X="-0.968094177246" Y="4.903296386719" />
                  <Point X="-0.909339294434" Y="4.910172851562" />
                  <Point X="-0.224199615479" Y="4.990358398438" />
                  <Point X="-0.057453018188" Y="4.368051269531" />
                  <Point X="-0.042140380859" Y="4.310903320312" />
                  <Point X="-0.024282119751" Y="4.284176757813" />
                  <Point X="0.006155934334" Y="4.273844238281" />
                  <Point X="0.036594039917" Y="4.284176757813" />
                  <Point X="0.054452251434" Y="4.310903320312" />
                  <Point X="0.232384048462" Y="4.974954101562" />
                  <Point X="0.247229232788" Y="4.989760742188" />
                  <Point X="0.860205749512" Y="4.925565429688" />
                  <Point X="0.908817565918" Y="4.913829101562" />
                  <Point X="1.508455932617" Y="4.769058105469" />
                  <Point X="1.539703735352" Y="4.757724121094" />
                  <Point X="1.931033813477" Y="4.615786132813" />
                  <Point X="1.961631347656" Y="4.6014765625" />
                  <Point X="2.338699462891" Y="4.425134277344" />
                  <Point X="2.368265380859" Y="4.407909179688" />
                  <Point X="2.7325234375" Y="4.19569140625" />
                  <Point X="2.760402099609" Y="4.175865722656" />
                  <Point X="3.068740234375" Y="3.956592773438" />
                  <Point X="2.29812890625" Y="2.621854492188" />
                  <Point X="2.229853515625" Y="2.503597900391" />
                  <Point X="2.223150634766" Y="2.485152832031" />
                  <Point X="2.203382324219" Y="2.411228271484" />
                  <Point X="2.202708007812" Y="2.386824462891" />
                  <Point X="2.210416015625" Y="2.322900878906" />
                  <Point X="2.218682128906" Y="2.3008125" />
                  <Point X="2.222086181641" Y="2.295795898438" />
                  <Point X="2.261639892578" Y="2.23750390625" />
                  <Point X="2.279956298828" Y="2.220799560547" />
                  <Point X="2.338248535156" Y="2.18124609375" />
                  <Point X="2.360336914062" Y="2.172980224609" />
                  <Point X="2.365838134766" Y="2.172316650391" />
                  <Point X="2.42976171875" Y="2.164608642578" />
                  <Point X="2.455026367188" Y="2.167647705078" />
                  <Point X="2.528950683594" Y="2.187416015625" />
                  <Point X="2.541033935547" Y="2.192417480469" />
                  <Point X="3.915160400391" Y="2.985769775391" />
                  <Point X="3.994247802734" Y="3.031430908203" />
                  <Point X="4.202591796875" Y="2.741880126953" />
                  <Point X="4.218133789062" Y="2.716196777344" />
                  <Point X="4.387512695312" Y="2.436296142578" />
                  <Point X="3.378507324219" Y="1.66205847168" />
                  <Point X="3.288616210938" Y="1.593082641602" />
                  <Point X="3.274792480469" Y="1.577859985352" />
                  <Point X="3.221589111328" Y="1.508451904297" />
                  <Point X="3.2114140625" Y="1.485401245117" />
                  <Point X="3.191595703125" Y="1.414535522461" />
                  <Point X="3.190779541016" Y="1.390965332031" />
                  <Point X="3.1921796875" Y="1.38417956543" />
                  <Point X="3.208448486328" Y="1.305332519531" />
                  <Point X="3.219454589844" Y="1.282166748047" />
                  <Point X="3.263704101562" Y="1.214909667969" />
                  <Point X="3.280947998047" Y="1.198819824219" />
                  <Point X="3.286466308594" Y="1.195713500977" />
                  <Point X="3.350590087891" Y="1.159617431641" />
                  <Point X="3.376026855469" Y="1.152633300781" />
                  <Point X="3.462726318359" Y="1.141175048828" />
                  <Point X="3.475803710938" Y="1.14117175293" />
                  <Point X="4.781131835938" Y="1.313021484375" />
                  <Point X="4.848975585938" Y="1.32195324707" />
                  <Point X="4.939188476562" Y="0.95138659668" />
                  <Point X="4.9440859375" Y="0.91992767334" />
                  <Point X="4.997858398438" Y="0.574556152344" />
                  <Point X="3.844184814453" Y="0.265430236816" />
                  <Point X="3.741167724609" Y="0.237826919556" />
                  <Point X="3.721756347656" Y="0.228582199097" />
                  <Point X="3.636576904297" Y="0.179346878052" />
                  <Point X="3.617866699219" Y="0.161322158813" />
                  <Point X="3.566759033203" Y="0.096198890686" />
                  <Point X="3.556985351562" Y="0.074735275269" />
                  <Point X="3.555519287109" Y="0.067079872131" />
                  <Point X="3.538483154297" Y="-0.021875303268" />
                  <Point X="3.53994921875" Y="-0.048340194702" />
                  <Point X="3.556985107422" Y="-0.137295227051" />
                  <Point X="3.566759033203" Y="-0.158758987427" />
                  <Point X="3.571157226562" Y="-0.164363555908" />
                  <Point X="3.622264892578" Y="-0.229486679077" />
                  <Point X="3.643907470703" Y="-0.246144165039" />
                  <Point X="3.729086914062" Y="-0.295379486084" />
                  <Point X="3.741167724609" Y="-0.300387023926" />
                  <Point X="4.938212890625" Y="-0.621134277344" />
                  <Point X="4.998067871094" Y="-0.637172424316" />
                  <Point X="4.948431640625" Y="-0.966399169922" />
                  <Point X="4.942156738281" Y="-0.993897094727" />
                  <Point X="4.874545410156" Y="-1.290178344727" />
                  <Point X="3.531453125" Y="-1.113356811523" />
                  <Point X="3.411982177734" Y="-1.097628295898" />
                  <Point X="3.380448974609" Y="-1.101468383789" />
                  <Point X="3.213271972656" Y="-1.137804931641" />
                  <Point X="3.1979453125" Y="-1.143923217773" />
                  <Point X="3.176749023438" Y="-1.165156005859" />
                  <Point X="3.075701171875" Y="-1.286685424805" />
                  <Point X="3.067953613281" Y="-1.299518066406" />
                  <Point X="3.063111572266" Y="-1.327615112305" />
                  <Point X="3.04862890625" Y="-1.485000854492" />
                  <Point X="3.049849121094" Y="-1.501458129883" />
                  <Point X="3.064322509766" Y="-1.529006347656" />
                  <Point X="3.156840576172" Y="-1.672912353516" />
                  <Point X="3.168460693359" Y="-1.685540649414" />
                  <Point X="4.279326171875" Y="-2.537937744141" />
                  <Point X="4.33907421875" Y="-2.583783935547" />
                  <Point X="4.204133300781" Y="-2.802139404297" />
                  <Point X="4.191153808594" Y="-2.820581298828" />
                  <Point X="4.056688476562" Y="-3.011638427734" />
                  <Point X="2.859859619141" Y="-2.320648681641" />
                  <Point X="2.753454589844" Y="-2.259215576172" />
                  <Point X="2.720217773438" Y="-2.250220458984" />
                  <Point X="2.521250488281" Y="-2.214287109375" />
                  <Point X="2.504749267578" Y="-2.214074462891" />
                  <Point X="2.474852539063" Y="-2.226731445312" />
                  <Point X="2.309559570312" Y="-2.313723876953" />
                  <Point X="2.297490966797" Y="-2.322614990234" />
                  <Point X="2.28111328125" Y="-2.348908691406" />
                  <Point X="2.194120849609" Y="-2.514201660156" />
                  <Point X="2.188950439453" Y="-2.529873291016" />
                  <Point X="2.192255371094" Y="-2.563497558594" />
                  <Point X="2.228188720703" Y="-2.76246484375" />
                  <Point X="2.234091552734" Y="-2.778578613281" />
                  <Point X="2.947934326172" Y="-4.014989990234" />
                  <Point X="2.986673828125" Y="-4.082088867188" />
                  <Point X="2.835298339844" Y="-4.190212402344" />
                  <Point X="2.820790527344" Y="-4.199603027344" />
                  <Point X="2.679775878906" Y="-4.290879394531" />
                  <Point X="1.764374267578" Y="-3.097905029297" />
                  <Point X="1.683177490234" Y="-2.992087402344" />
                  <Point X="1.670549316406" Y="-2.980467529297" />
                  <Point X="1.653661376953" Y="-2.969610107422" />
                  <Point X="1.45742590332" Y="-2.843448730469" />
                  <Point X="1.442262207031" Y="-2.8369375" />
                  <Point X="1.407334960938" Y="-2.837416748047" />
                  <Point X="1.192718017578" Y="-2.857166015625" />
                  <Point X="1.178165527344" Y="-2.86076171875" />
                  <Point X="1.151070800781" Y="-2.880367431641" />
                  <Point X="0.985349060059" Y="-3.018159667969" />
                  <Point X="0.968456726074" Y="-3.045986328125" />
                  <Point X="0.964192626953" Y="-3.065605224609" />
                  <Point X="0.91464251709" Y="-3.29357421875" />
                  <Point X="0.91392956543" Y="-3.310720214844" />
                  <Point X="1.116227050781" Y="-4.847322265625" />
                  <Point X="1.127642211914" Y="-4.934028320312" />
                  <Point X="0.994346069336" Y="-4.963247070312" />
                  <Point X="0.980938903809" Y="-4.965682617188" />
                  <Point X="0.860200378418" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#124" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.015312729718" Y="4.412272361799" Z="0.2" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.2" />
                  <Point X="-0.921307345037" Y="4.991113917252" Z="0.2" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.2" />
                  <Point X="-1.689780723436" Y="4.785894627758" Z="0.2" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.2" />
                  <Point X="-1.747125384685" Y="4.743057374226" Z="0.2" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.2" />
                  <Point X="-1.737367332973" Y="4.348916427052" Z="0.2" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.2" />
                  <Point X="-1.831240493722" Y="4.302979726633" Z="0.2" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.2" />
                  <Point X="-1.926770261387" Y="4.345363647817" Z="0.2" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.2" />
                  <Point X="-1.950161218653" Y="4.369942261713" Z="0.2" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.2" />
                  <Point X="-2.734847272975" Y="4.276246700695" Z="0.2" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.2" />
                  <Point X="-3.33174812985" Y="3.829520355301" Z="0.2" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.2" />
                  <Point X="-3.348784265961" Y="3.741784109804" Z="0.2" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.2" />
                  <Point X="-2.994633099151" Y="3.061542499955" Z="0.2" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.2" />
                  <Point X="-3.049951812332" Y="2.998851755555" Z="0.2" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.2" />
                  <Point X="-3.133533965652" Y="3.000931600022" Z="0.2" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.2" />
                  <Point X="-3.192075238493" Y="3.031409682785" Z="0.2" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.2" />
                  <Point X="-4.174859807845" Y="2.888544631605" Z="0.2" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.2" />
                  <Point X="-4.520714186189" Y="2.310054416899" Z="0.2" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.2" />
                  <Point X="-4.480213497757" Y="2.212150774946" Z="0.2" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.2" />
                  <Point X="-3.669179024437" Y="1.558231350708" Z="0.2" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.2" />
                  <Point X="-3.689516837987" Y="1.498915238275" Z="0.2" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.2" />
                  <Point X="-3.7480286516" Y="1.476367224557" Z="0.2" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.2" />
                  <Point X="-3.83717588936" Y="1.485928186794" Z="0.2" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.2" />
                  <Point X="-4.960442771941" Y="1.083650059998" Z="0.2" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.2" />
                  <Point X="-5.053393833907" Y="0.493497058308" Z="0.2" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.2" />
                  <Point X="-4.942753174663" Y="0.415139198428" Z="0.2" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.2" />
                  <Point X="-3.551007001741" Y="0.031333281416" Z="0.2" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.2" />
                  <Point X="-3.540289801749" Y="0.002361907531" Z="0.2" />
                  <Point X="-3.539556741714" Y="0" Z="0.2" />
                  <Point X="-3.548074799386" Y="-0.02744504353" Z="0.2" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.2" />
                  <Point X="-3.574361593383" Y="-0.047542701717" Z="0.2" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.2" />
                  <Point X="-3.694134614109" Y="-0.080572858328" Z="0.2" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.2" />
                  <Point X="-4.988815937082" Y="-0.946641204972" Z="0.2" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.2" />
                  <Point X="-4.85766084128" Y="-1.479044608744" Z="0.2" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.2" />
                  <Point X="-4.717920596735" Y="-1.504178992364" Z="0.2" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.2" />
                  <Point X="-3.194774789778" Y="-1.321214726984" Z="0.2" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.2" />
                  <Point X="-3.199770296639" Y="-1.349839984184" Z="0.2" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.2" />
                  <Point X="-3.303592677632" Y="-1.431394424448" Z="0.2" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.2" />
                  <Point X="-4.232615487134" Y="-2.804882598903" Z="0.2" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.2" />
                  <Point X="-3.889744410372" Y="-3.263789443654" Z="0.2" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.2" />
                  <Point X="-3.760066672089" Y="-3.240936889342" Z="0.2" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.2" />
                  <Point X="-2.556865670716" Y="-2.571464798416" Z="0.2" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.2" />
                  <Point X="-2.614480123827" Y="-2.675011710403" Z="0.2" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.2" />
                  <Point X="-2.922920436592" Y="-4.152520643663" Z="0.2" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.2" />
                  <Point X="-2.485932532402" Y="-4.427985122472" Z="0.2" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.2" />
                  <Point X="-2.433297000679" Y="-4.426317119006" Z="0.2" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.2" />
                  <Point X="-1.98869728566" Y="-3.99774282046" Z="0.2" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.2" />
                  <Point X="-1.68319874231" Y="-4.002768027624" Z="0.2" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.2" />
                  <Point X="-1.443889607504" Y="-4.192730776888" Z="0.2" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.2" />
                  <Point X="-1.369674739528" Y="-4.489120106559" Z="0.2" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.2" />
                  <Point X="-1.36869953655" Y="-4.542255604798" Z="0.2" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.2" />
                  <Point X="-1.140832932315" Y="-4.94955495558" Z="0.2" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.2" />
                  <Point X="-0.841419030868" Y="-5.009152807713" Z="0.2" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.2" />
                  <Point X="-0.78592595454" Y="-4.895299716242" Z="0.2" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.2" />
                  <Point X="-0.266332888343" Y="-3.301565462138" Z="0.2" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.2" />
                  <Point X="-0.020076213648" Y="-3.210470284273" Z="0.2" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.2" />
                  <Point X="0.233282865714" Y="-3.276641761015" Z="0.2" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.2" />
                  <Point X="0.404112971089" Y="-3.50008029363" Z="0.2" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.2" />
                  <Point X="0.448828905861" Y="-3.637236313573" Z="0.2" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.2" />
                  <Point X="0.983719823648" Y="-4.983597409411" Z="0.2" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.2" />
                  <Point X="1.162865593541" Y="-4.944865160589" Z="0.2" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.2" />
                  <Point X="1.159643342027" Y="-4.809516021599" Z="0.2" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.2" />
                  <Point X="1.00689576775" Y="-3.044944900676" Z="0.2" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.2" />
                  <Point X="1.17688181142" Y="-2.887533775041" Z="0.2" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.2" />
                  <Point X="1.405760628849" Y="-2.85592619953" Z="0.2" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.2" />
                  <Point X="1.620465961836" Y="-2.980387887787" Z="0.2" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.2" />
                  <Point X="1.718550715073" Y="-3.097063001567" Z="0.2" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.2" />
                  <Point X="2.8418037675" Y="-4.210297314857" Z="0.2" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.2" />
                  <Point X="3.031517273384" Y="-4.075825790299" Z="0.2" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.2" />
                  <Point X="2.98507963371" Y="-3.958709994121" Z="0.2" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.2" />
                  <Point X="2.235303522256" Y="-2.523331222192" Z="0.2" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.2" />
                  <Point X="2.319203659383" Y="-2.340915230557" Z="0.2" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.2" />
                  <Point X="2.491983147815" Y="-2.239697650584" Z="0.2" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.2" />
                  <Point X="2.705175581321" Y="-2.268144487235" Z="0.2" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.2" />
                  <Point X="2.828703602986" Y="-2.332669866564" Z="0.2" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.2" />
                  <Point X="4.225886343566" Y="-2.818078633924" Z="0.2" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.2" />
                  <Point X="4.38657082646" Y="-2.560796594711" Z="0.2" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.2" />
                  <Point X="4.303608010521" Y="-2.466989980366" Z="0.2" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.2" />
                  <Point X="3.100224582831" Y="-1.47068698435" Z="0.2" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.2" />
                  <Point X="3.106745092452" Y="-1.300916772912" Z="0.2" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.2" />
                  <Point X="3.209039391271" Y="-1.165842936839" Z="0.2" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.2" />
                  <Point X="3.3849126736" Y="-1.119047538944" Z="0.2" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.2" />
                  <Point X="3.518770792434" Y="-1.131649069918" Z="0.2" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.2" />
                  <Point X="4.984748011621" Y="-0.973740927365" Z="0.2" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.2" />
                  <Point X="5.043531968677" Y="-0.598897075548" Z="0.2" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.2" />
                  <Point X="4.944997965373" Y="-0.5415579344" Z="0.2" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.2" />
                  <Point X="3.662773220578" Y="-0.171575371163" Z="0.2" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.2" />
                  <Point X="3.604335027241" Y="-0.102215003491" Z="0.2" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.2" />
                  <Point X="3.582900827892" Y="-0.007655433858" Z="0.2" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.2" />
                  <Point X="3.598470622532" Y="0.088955097386" Z="0.2" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.2" />
                  <Point X="3.651044411159" Y="0.161733735084" Z="0.2" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.2" />
                  <Point X="3.740622193774" Y="0.216573473079" Z="0.2" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.2" />
                  <Point X="3.850969851934" Y="0.248413998369" Z="0.2" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.2" />
                  <Point X="4.98733498793" Y="0.958899863585" Z="0.2" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.2" />
                  <Point X="4.888814666021" Y="1.375681610978" Z="0.2" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.2" />
                  <Point X="4.768449540904" Y="1.393873844088" Z="0.2" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.2" />
                  <Point X="3.376421935325" Y="1.233482475443" Z="0.2" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.2" />
                  <Point X="3.304837000452" Y="1.270564622505" Z="0.2" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.2" />
                  <Point X="3.255069084239" Y="1.340928277018" Z="0.2" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.2" />
                  <Point X="3.234992124776" Y="1.425563648275" Z="0.2" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.2" />
                  <Point X="3.25341042446" Y="1.503215023585" Z="0.2" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.2" />
                  <Point X="3.308319490458" Y="1.578721866831" Z="0.2" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.2" />
                  <Point X="3.402789282124" Y="1.653670983142" Z="0.2" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.2" />
                  <Point X="4.254755303433" Y="2.773363100219" Z="0.2" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.2" />
                  <Point X="4.020955722971" Y="3.102645385934" Z="0.2" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.2" />
                  <Point X="3.88400447695" Y="3.060351060067" Z="0.2" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.2" />
                  <Point X="2.435953552062" Y="2.247230428764" Z="0.2" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.2" />
                  <Point X="2.365668025627" Y="2.253237180602" Z="0.2" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.2" />
                  <Point X="2.301874664967" Y="2.293454109653" Z="0.2" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.2" />
                  <Point X="2.257304490582" Y="2.355150195415" Z="0.2" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.2" />
                  <Point X="2.246192522156" Y="2.424090418205" Z="0.2" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.2" />
                  <Point X="2.265297513893" Y="2.50351602382" Z="0.2" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.2" />
                  <Point X="2.335274292792" Y="2.628134605005" Z="0.2" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.2" />
                  <Point X="2.783223100329" Y="4.247893037832" Z="0.2" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.2" />
                  <Point X="2.387279314033" Y="4.482391762112" Z="0.2" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.2" />
                  <Point X="1.976656906481" Y="4.678048512368" Z="0.2" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.2" />
                  <Point X="1.550596397575" Y="4.836008169723" Z="0.2" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.2" />
                  <Point X="0.914395904101" Y="4.993712862095" Z="0.2" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.2" />
                  <Point X="0.246281117138" Y="5.070768688525" Z="0.2" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.2" />
                  <Point X="0.177931831835" Y="5.019175144152" Z="0.2" />
                  <Point X="0" Y="4.355124473572" Z="0.2" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>