<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#169" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2012" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="0" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="0.004716064453" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.768524230957" Y="-4.278424316406" />
                  <Point X="0.563302062988" Y="-3.512524414063" />
                  <Point X="0.557721191406" Y="-3.497141845703" />
                  <Point X="0.54236315918" Y="-3.467377197266" />
                  <Point X="0.458483795166" Y="-3.346522705078" />
                  <Point X="0.378635528564" Y="-3.231476806641" />
                  <Point X="0.356751586914" Y="-3.209021484375" />
                  <Point X="0.330496398926" Y="-3.189777587891" />
                  <Point X="0.302495330811" Y="-3.175669433594" />
                  <Point X="0.172696624756" Y="-3.135384521484" />
                  <Point X="0.049136188507" Y="-3.097036132812" />
                  <Point X="0.020976638794" Y="-3.092766601562" />
                  <Point X="-0.00866468811" Y="-3.092766601562" />
                  <Point X="-0.036824085236" Y="-3.097035888672" />
                  <Point X="-0.166622802734" Y="-3.137320556641" />
                  <Point X="-0.290183227539" Y="-3.175669189453" />
                  <Point X="-0.31818460083" Y="-3.189777587891" />
                  <Point X="-0.344439788818" Y="-3.209021484375" />
                  <Point X="-0.366323577881" Y="-3.231476806641" />
                  <Point X="-0.450203094482" Y="-3.352331054688" />
                  <Point X="-0.530051208496" Y="-3.467377197266" />
                  <Point X="-0.538189025879" Y="-3.481573730469" />
                  <Point X="-0.550990112305" Y="-3.512524414063" />
                  <Point X="-0.746349060059" Y="-4.241614257813" />
                  <Point X="-0.916584533691" Y="-4.87694140625" />
                  <Point X="-0.93743951416" Y="-4.872893554688" />
                  <Point X="-1.079341308594" Y="-4.845350097656" />
                  <Point X="-1.226604248047" Y="-4.807460449219" />
                  <Point X="-1.24641809082" Y="-4.802362792969" />
                  <Point X="-1.239666992188" Y="-4.751083007812" />
                  <Point X="-1.214963012695" Y="-4.563438476562" />
                  <Point X="-1.214201171875" Y="-4.5479296875" />
                  <Point X="-1.216508666992" Y="-4.516224121094" />
                  <Point X="-1.247517578125" Y="-4.36033203125" />
                  <Point X="-1.277036132812" Y="-4.211932617188" />
                  <Point X="-1.287938354492" Y="-4.182966308594" />
                  <Point X="-1.304010375977" Y="-4.15512890625" />
                  <Point X="-1.323644775391" Y="-4.131204101562" />
                  <Point X="-1.443146850586" Y="-4.026403320313" />
                  <Point X="-1.556905517578" Y="-3.926639160156" />
                  <Point X="-1.583188476562" Y="-3.910295410156" />
                  <Point X="-1.612885742188" Y="-3.897994384766" />
                  <Point X="-1.64302734375" Y="-3.890966308594" />
                  <Point X="-1.801633300781" Y="-3.880570800781" />
                  <Point X="-1.952616577148" Y="-3.870674804688" />
                  <Point X="-1.98341796875" Y="-3.873708496094" />
                  <Point X="-2.014466674805" Y="-3.882028076172" />
                  <Point X="-2.042657714844" Y="-3.894801513672" />
                  <Point X="-2.174816894531" Y="-3.983107421875" />
                  <Point X="-2.300624267578" Y="-4.067169677734" />
                  <Point X="-2.312785644531" Y="-4.076821533203" />
                  <Point X="-2.335102539062" Y="-4.099461914062" />
                  <Point X="-2.444781494141" Y="-4.242397949219" />
                  <Point X="-2.480148925781" Y="-4.288489746094" />
                  <Point X="-2.593716796875" Y="-4.218171386719" />
                  <Point X="-2.801706787109" Y="-4.089389404297" />
                  <Point X="-3.005624267578" Y="-3.932379638672" />
                  <Point X="-3.104721923828" Y="-3.856077880859" />
                  <Point X="-2.805143554688" Y="-3.337193115234" />
                  <Point X="-2.423761230469" Y="-2.676619628906" />
                  <Point X="-2.412859130859" Y="-2.647653320312" />
                  <Point X="-2.406588134766" Y="-2.616126953125" />
                  <Point X="-2.405575439453" Y="-2.585192871094" />
                  <Point X="-2.414560058594" Y="-2.555575195312" />
                  <Point X="-2.428777099609" Y="-2.526746337891" />
                  <Point X="-2.4468046875" Y="-2.501589355469" />
                  <Point X="-2.464153320312" Y="-2.484240722656" />
                  <Point X="-2.489310302734" Y="-2.466213134766" />
                  <Point X="-2.518139160156" Y="-2.45199609375" />
                  <Point X="-2.547756835938" Y="-2.443011474609" />
                  <Point X="-2.578690917969" Y="-2.444024169922" />
                  <Point X="-2.610217285156" Y="-2.450295166016" />
                  <Point X="-2.63918359375" Y="-2.461197265625" />
                  <Point X="-3.268009277344" Y="-2.82425" />
                  <Point X="-3.818024658203" Y="-3.141801269531" />
                  <Point X="-3.91853515625" Y="-3.009750976562" />
                  <Point X="-4.082859130859" Y="-2.793862792969" />
                  <Point X="-4.229049316406" Y="-2.548723632812" />
                  <Point X="-4.306142578125" Y="-2.419450439453" />
                  <Point X="-3.773249755859" Y="-2.010547119141" />
                  <Point X="-3.105954589844" Y="-1.498513549805" />
                  <Point X="-3.084577392578" Y="-1.47559362793" />
                  <Point X="-3.066612548828" Y="-1.448462524414" />
                  <Point X="-3.053856445312" Y="-1.419832885742" />
                  <Point X="-3.046151855469" Y="-1.390085449219" />
                  <Point X="-3.04334765625" Y="-1.359657104492" />
                  <Point X="-3.045556396484" Y="-1.327986328125" />
                  <Point X="-3.052557373047" Y="-1.298241210938" />
                  <Point X="-3.068639648438" Y="-1.272257568359" />
                  <Point X="-3.089472167969" Y="-1.248301025391" />
                  <Point X="-3.112972167969" Y="-1.228767089844" />
                  <Point X="-3.139454833984" Y="-1.213180664062" />
                  <Point X="-3.168718261719" Y="-1.201956420898" />
                  <Point X="-3.200606201172" Y="-1.195474731445" />
                  <Point X="-3.231929199219" Y="-1.194383789062" />
                  <Point X="-4.025762207031" Y="-1.298893920898" />
                  <Point X="-4.732102050781" Y="-1.391885375977" />
                  <Point X="-4.769808105469" Y="-1.244266967773" />
                  <Point X="-4.834077636719" Y="-0.992654418945" />
                  <Point X="-4.872755371094" Y="-0.722222412109" />
                  <Point X="-4.892424316406" Y="-0.584698181152" />
                  <Point X="-4.293025878906" Y="-0.424089813232" />
                  <Point X="-3.532875976562" Y="-0.220408340454" />
                  <Point X="-3.510188964844" Y="-0.211047439575" />
                  <Point X="-3.480403076172" Y="-0.193959014893" />
                  <Point X="-3.467230224609" Y="-0.184844619751" />
                  <Point X="-3.441876464844" Y="-0.163932907104" />
                  <Point X="-3.425737304688" Y="-0.146853942871" />
                  <Point X="-3.414283447266" Y="-0.126336090088" />
                  <Point X="-3.402118408203" Y="-0.096327735901" />
                  <Point X="-3.397580566406" Y="-0.081949386597" />
                  <Point X="-3.390822265625" Y="-0.052592060089" />
                  <Point X="-3.388400878906" Y="-0.031191102982" />
                  <Point X="-3.390862304688" Y="-0.009794753075" />
                  <Point X="-3.397848632812" Y="0.020296329498" />
                  <Point X="-3.402419677734" Y="0.034682113647" />
                  <Point X="-3.414356689453" Y="0.063955955505" />
                  <Point X="-3.425852294922" Y="0.084450500488" />
                  <Point X="-3.442026611328" Y="0.101496459961" />
                  <Point X="-3.468063232422" Y="0.12288205719" />
                  <Point X="-3.481261474609" Y="0.131973388672" />
                  <Point X="-3.510364501953" Y="0.148587768555" />
                  <Point X="-3.532875976562" Y="0.157848342896" />
                  <Point X="-4.2564921875" Y="0.351740692139" />
                  <Point X="-4.89181640625" Y="0.521975219727" />
                  <Point X="-4.865907714844" Y="0.697062988281" />
                  <Point X="-4.824487792969" Y="0.976975585938" />
                  <Point X="-4.746629882812" Y="1.264294433594" />
                  <Point X="-4.703551269531" Y="1.423267944336" />
                  <Point X="-4.310560058594" Y="1.371529907227" />
                  <Point X="-3.765666259766" Y="1.29979284668" />
                  <Point X="-3.744986083984" Y="1.299341674805" />
                  <Point X="-3.723424560547" Y="1.301228149414" />
                  <Point X="-3.703137695312" Y="1.305263671875" />
                  <Point X="-3.671668457031" Y="1.315185913086" />
                  <Point X="-3.641711669922" Y="1.324631225586" />
                  <Point X="-3.622779052734" Y="1.332961669922" />
                  <Point X="-3.604034667969" Y="1.343783569336" />
                  <Point X="-3.587353515625" Y="1.356014648438" />
                  <Point X="-3.573715087891" Y="1.37156628418" />
                  <Point X="-3.561300537109" Y="1.389296020508" />
                  <Point X="-3.551351318359" Y="1.407430908203" />
                  <Point X="-3.538724121094" Y="1.437915649414" />
                  <Point X="-3.526703857422" Y="1.466935180664" />
                  <Point X="-3.520915527344" Y="1.486794067383" />
                  <Point X="-3.517157226562" Y="1.508109130859" />
                  <Point X="-3.5158046875" Y="1.528749267578" />
                  <Point X="-3.518951171875" Y="1.549192871094" />
                  <Point X="-3.524552978516" Y="1.570099121094" />
                  <Point X="-3.532049804688" Y="1.589377807617" />
                  <Point X="-3.547285888672" Y="1.618645996094" />
                  <Point X="-3.561789550781" Y="1.646507446289" />
                  <Point X="-3.57328125" Y="1.663705932617" />
                  <Point X="-3.587193847656" Y="1.680286376953" />
                  <Point X="-3.602135986328" Y="1.694590209961" />
                  <Point X="-4.017204345703" Y="2.013083129883" />
                  <Point X="-4.351859863281" Y="2.269874023438" />
                  <Point X="-4.242094726562" Y="2.457928466797" />
                  <Point X="-4.081153808594" Y="2.733659423828" />
                  <Point X="-3.874912597656" Y="2.998753417969" />
                  <Point X="-3.750504882813" Y="3.158661865234" />
                  <Point X="-3.541315673828" Y="3.037886230469" />
                  <Point X="-3.206656982422" Y="2.844670898438" />
                  <Point X="-3.187724121094" Y="2.836340332031" />
                  <Point X="-3.167081787109" Y="2.829831787109" />
                  <Point X="-3.146794433594" Y="2.825796386719" />
                  <Point X="-3.102966552734" Y="2.821961914062" />
                  <Point X="-3.061245117188" Y="2.818311767578" />
                  <Point X="-3.040564697266" Y="2.818762939453" />
                  <Point X="-3.019106201172" Y="2.821588134766" />
                  <Point X="-2.999014892578" Y="2.826504638672" />
                  <Point X="-2.980463867188" Y="2.835652832031" />
                  <Point X="-2.962209472656" Y="2.847281982422" />
                  <Point X="-2.946077392578" Y="2.860229492188" />
                  <Point X="-2.914968017578" Y="2.891338867188" />
                  <Point X="-2.885353759766" Y="2.920952880859" />
                  <Point X="-2.872406738281" Y="2.937084228516" />
                  <Point X="-2.86077734375" Y="2.955338623047" />
                  <Point X="-2.851628662109" Y="2.973890136719" />
                  <Point X="-2.846712158203" Y="2.993981933594" />
                  <Point X="-2.843886962891" Y="3.015440673828" />
                  <Point X="-2.843435791016" Y="3.036120849609" />
                  <Point X="-2.847270263672" Y="3.079948730469" />
                  <Point X="-2.850920410156" Y="3.121670410156" />
                  <Point X="-2.854955810547" Y="3.141957763672" />
                  <Point X="-2.861464355469" Y="3.162600097656" />
                  <Point X="-2.869794921875" Y="3.181532958984" />
                  <Point X="-3.053724121094" Y="3.500107666016" />
                  <Point X="-3.183333007812" Y="3.724596435547" />
                  <Point X="-2.980930175781" Y="3.879776611328" />
                  <Point X="-2.700626464844" Y="4.094683105469" />
                  <Point X="-2.375804199219" Y="4.275147460938" />
                  <Point X="-2.167036865234" Y="4.391134277344" />
                  <Point X="-2.14564453125" Y="4.363255371094" />
                  <Point X="-2.04319543457" Y="4.229740722656" />
                  <Point X="-2.028892700195" Y="4.214799804688" />
                  <Point X="-2.012312866211" Y="4.200887207031" />
                  <Point X="-1.99511340332" Y="4.18939453125" />
                  <Point X="-1.946333374023" Y="4.164000976562" />
                  <Point X="-1.899897094727" Y="4.139827636719" />
                  <Point X="-1.88061730957" Y="4.132330566406" />
                  <Point X="-1.85971081543" Y="4.126729003906" />
                  <Point X="-1.839267700195" Y="4.123582519531" />
                  <Point X="-1.818628295898" Y="4.124935546875" />
                  <Point X="-1.797313110352" Y="4.128693847656" />
                  <Point X="-1.77745324707" Y="4.134482421875" />
                  <Point X="-1.726645385742" Y="4.155528320313" />
                  <Point X="-1.678279174805" Y="4.175562011719" />
                  <Point X="-1.660146362305" Y="4.185509765625" />
                  <Point X="-1.642416625977" Y="4.197923828125" />
                  <Point X="-1.626864379883" Y="4.2115625" />
                  <Point X="-1.6146328125" Y="4.228244140625" />
                  <Point X="-1.603810913086" Y="4.24698828125" />
                  <Point X="-1.59548046875" Y="4.265920898438" />
                  <Point X="-1.578943359375" Y="4.318369628906" />
                  <Point X="-1.563201171875" Y="4.368297363281" />
                  <Point X="-1.559165649414" Y="4.388584960938" />
                  <Point X="-1.557279174805" Y="4.410146972656" />
                  <Point X="-1.55773034668" Y="4.430826660156" />
                  <Point X="-1.578641113281" Y="4.589659179688" />
                  <Point X="-1.584201904297" Y="4.631897949219" />
                  <Point X="-1.312506835938" Y="4.708071777344" />
                  <Point X="-0.949635009766" Y="4.809808105469" />
                  <Point X="-0.555858642578" Y="4.855894042969" />
                  <Point X="-0.29471081543" Y="4.886457519531" />
                  <Point X="-0.232904663086" Y="4.655793457031" />
                  <Point X="-0.133903305054" Y="4.286315429688" />
                  <Point X="-0.121129844666" Y="4.258124023438" />
                  <Point X="-0.10327155304" Y="4.231397460938" />
                  <Point X="-0.082114532471" Y="4.20880859375" />
                  <Point X="-0.054819446564" Y="4.19421875" />
                  <Point X="-0.024381370544" Y="4.183886230469" />
                  <Point X="0.006155906677" Y="4.178844238281" />
                  <Point X="0.03669316864" Y="4.183886230469" />
                  <Point X="0.067131248474" Y="4.19421875" />
                  <Point X="0.094426467896" Y="4.20880859375" />
                  <Point X="0.115583503723" Y="4.231397460938" />
                  <Point X="0.133441802979" Y="4.258124023438" />
                  <Point X="0.146215255737" Y="4.286315429688" />
                  <Point X="0.240458435059" Y="4.638036132812" />
                  <Point X="0.307419372559" Y="4.8879375" />
                  <Point X="0.527194763184" Y="4.864920898438" />
                  <Point X="0.844041625977" Y="4.831738769531" />
                  <Point X="1.169828125" Y="4.753083984375" />
                  <Point X="1.481026977539" Y="4.677950683594" />
                  <Point X="1.692359985352" Y="4.601298828125" />
                  <Point X="1.894645874023" Y="4.527928222656" />
                  <Point X="2.099695556641" Y="4.432033203125" />
                  <Point X="2.294576171875" Y="4.340893554688" />
                  <Point X="2.492689941406" Y="4.225472167969" />
                  <Point X="2.680978759766" Y="4.115774902344" />
                  <Point X="2.867802978516" Y="3.982915771484" />
                  <Point X="2.943260009766" Y="3.929254882812" />
                  <Point X="2.589002441406" Y="3.315662353516" />
                  <Point X="2.147581054688" Y="2.551097900391" />
                  <Point X="2.142075683594" Y="2.539930664062" />
                  <Point X="2.133076904297" Y="2.516056884766" />
                  <Point X="2.122077636719" Y="2.474925292969" />
                  <Point X="2.111607177734" Y="2.435770507813" />
                  <Point X="2.108619384766" Y="2.417934326172" />
                  <Point X="2.107728027344" Y="2.380952880859" />
                  <Point X="2.112016845703" Y="2.345385742188" />
                  <Point X="2.116099365234" Y="2.311528076172" />
                  <Point X="2.121441894531" Y="2.289605224609" />
                  <Point X="2.129708007812" Y="2.267516357422" />
                  <Point X="2.140071044922" Y="2.247470947266" />
                  <Point X="2.162078857422" Y="2.215037109375" />
                  <Point X="2.183028808594" Y="2.184162353516" />
                  <Point X="2.194465332031" Y="2.170327880859" />
                  <Point X="2.221598632812" Y="2.145592529297" />
                  <Point X="2.254032226562" Y="2.123584716797" />
                  <Point X="2.284907226562" Y="2.102635009766" />
                  <Point X="2.304952636719" Y="2.092271972656" />
                  <Point X="2.327040771484" Y="2.084006103516" />
                  <Point X="2.348963867188" Y="2.078663574219" />
                  <Point X="2.384531005859" Y="2.074374755859" />
                  <Point X="2.418388671875" Y="2.070291992188" />
                  <Point X="2.436467529297" Y="2.069845703125" />
                  <Point X="2.473206542969" Y="2.074171142578" />
                  <Point X="2.514338134766" Y="2.085170410156" />
                  <Point X="2.553492919922" Y="2.095640869141" />
                  <Point X="2.565283447266" Y="2.099638183594" />
                  <Point X="2.588533935547" Y="2.110145019531" />
                  <Point X="3.316352783203" Y="2.530351318359" />
                  <Point X="3.967326416016" Y="2.906191162109" />
                  <Point X="4.011585205078" Y="2.844681640625" />
                  <Point X="4.123271972656" Y="2.689462402344" />
                  <Point X="4.227424316406" Y="2.517349609375" />
                  <Point X="4.262198730469" Y="2.459884277344" />
                  <Point X="3.811956542969" Y="2.114400878906" />
                  <Point X="3.230783935547" Y="1.668451293945" />
                  <Point X="3.221423828125" Y="1.660240722656" />
                  <Point X="3.203973876953" Y="1.641627807617" />
                  <Point X="3.174371337891" Y="1.603009155273" />
                  <Point X="3.146191650391" Y="1.566246459961" />
                  <Point X="3.136605712891" Y="1.550911621094" />
                  <Point X="3.121629882812" Y="1.5170859375" />
                  <Point X="3.110603027344" Y="1.47765612793" />
                  <Point X="3.100105957031" Y="1.440121459961" />
                  <Point X="3.096652587891" Y="1.417823242188" />
                  <Point X="3.095836425781" Y="1.394252929688" />
                  <Point X="3.097739501953" Y="1.371768066406" />
                  <Point X="3.106791503906" Y="1.327897338867" />
                  <Point X="3.115408447266" Y="1.286135253906" />
                  <Point X="3.120679931641" Y="1.268977661133" />
                  <Point X="3.136282470703" Y="1.235740478516" />
                  <Point X="3.160902832031" Y="1.198318481445" />
                  <Point X="3.184340087891" Y="1.16269519043" />
                  <Point X="3.198893554688" Y="1.145450439453" />
                  <Point X="3.216137451172" Y="1.129360595703" />
                  <Point X="3.234346923828" Y="1.116034912109" />
                  <Point X="3.270025146484" Y="1.095951049805" />
                  <Point X="3.303989013672" Y="1.076832519531" />
                  <Point X="3.320521240234" Y="1.069501586914" />
                  <Point X="3.356118164062" Y="1.059438598633" />
                  <Point X="3.404357666016" Y="1.053063110352" />
                  <Point X="3.450278808594" Y="1.046994018555" />
                  <Point X="3.462702148438" Y="1.046175048828" />
                  <Point X="3.488203613281" Y="1.04698449707" />
                  <Point X="4.179583007812" Y="1.138006469727" />
                  <Point X="4.776839355469" Y="1.21663659668" />
                  <Point X="4.797967285156" Y="1.129849731445" />
                  <Point X="4.845936035156" Y="0.932809204102" />
                  <Point X="4.878757324219" Y="0.722003051758" />
                  <Point X="4.890864746094" Y="0.644238464355" />
                  <Point X="4.382616210938" Y="0.508053710937" />
                  <Point X="3.716579833984" Y="0.329589935303" />
                  <Point X="3.704791259766" Y="0.325586608887" />
                  <Point X="3.681545410156" Y="0.315067840576" />
                  <Point X="3.634151611328" Y="0.287673309326" />
                  <Point X="3.589035400391" Y="0.261595428467" />
                  <Point X="3.574311035156" Y="0.251096221924" />
                  <Point X="3.547530761719" Y="0.22557661438" />
                  <Point X="3.519094482422" Y="0.189341964722" />
                  <Point X="3.492024902344" Y="0.154848968506" />
                  <Point X="3.48030078125" Y="0.135568847656" />
                  <Point X="3.470527099609" Y="0.114105384827" />
                  <Point X="3.463680908203" Y="0.092604270935" />
                  <Point X="3.454202148438" Y="0.043109516144" />
                  <Point X="3.445178710938" Y="-0.004006377697" />
                  <Point X="3.443483154297" Y="-0.021873929977" />
                  <Point X="3.445178710938" Y="-0.05855393219" />
                  <Point X="3.454657714844" Y="-0.108048530579" />
                  <Point X="3.463680908203" Y="-0.155164428711" />
                  <Point X="3.470527099609" Y="-0.176665542603" />
                  <Point X="3.480301025391" Y="-0.198129302979" />
                  <Point X="3.492024902344" Y="-0.217408966064" />
                  <Point X="3.520461181641" Y="-0.253643615723" />
                  <Point X="3.547530761719" Y="-0.288136627197" />
                  <Point X="3.559999023438" Y="-0.30123614502" />
                  <Point X="3.589035888672" Y="-0.324155731201" />
                  <Point X="3.636429931641" Y="-0.351550109863" />
                  <Point X="3.681545898438" Y="-0.377627990723" />
                  <Point X="3.692709228516" Y="-0.383138458252" />
                  <Point X="3.716579833984" Y="-0.392149932861" />
                  <Point X="4.350605957031" Y="-0.562036621094" />
                  <Point X="4.891472167969" Y="-0.706961486816" />
                  <Point X="4.881806152344" Y="-0.771075256348" />
                  <Point X="4.855022460938" Y="-0.948725891113" />
                  <Point X="4.81297265625" Y="-1.132993652344" />
                  <Point X="4.801173339844" Y="-1.184698974609" />
                  <Point X="4.196796875" Y="-1.105131225586" />
                  <Point X="3.424382080078" Y="-1.003441040039" />
                  <Point X="3.408035644531" Y="-1.002710327148" />
                  <Point X="3.374658691406" Y="-1.005508728027" />
                  <Point X="3.281641357422" Y="-1.0257265625" />
                  <Point X="3.193094482422" Y="-1.04497253418" />
                  <Point X="3.163974121094" Y="-1.056596923828" />
                  <Point X="3.136147460938" Y="-1.073489257812" />
                  <Point X="3.112397460938" Y="-1.093960083008" />
                  <Point X="3.056174316406" Y="-1.161579223633" />
                  <Point X="3.002653320312" Y="-1.225948364258" />
                  <Point X="2.987932617188" Y="-1.250330566406" />
                  <Point X="2.976589355469" Y="-1.277715698242" />
                  <Point X="2.969757568359" Y="-1.305365478516" />
                  <Point X="2.961699462891" Y="-1.392934936523" />
                  <Point X="2.954028564453" Y="-1.476295776367" />
                  <Point X="2.956347167969" Y="-1.507564208984" />
                  <Point X="2.964078613281" Y="-1.539185180664" />
                  <Point X="2.976450195312" Y="-1.567996826172" />
                  <Point X="3.027927490234" Y="-1.648066040039" />
                  <Point X="3.076930419922" Y="-1.724287353516" />
                  <Point X="3.086932861328" Y="-1.737239013672" />
                  <Point X="3.110628417969" Y="-1.760909179688" />
                  <Point X="3.699008544922" Y="-2.212389404297" />
                  <Point X="4.213122070312" Y="-2.606883056641" />
                  <Point X="4.200311035156" Y="-2.627613525391" />
                  <Point X="4.124810546875" Y="-2.749785400391" />
                  <Point X="4.037848144531" Y="-2.873346435547" />
                  <Point X="4.028981445312" Y="-2.885945068359" />
                  <Point X="3.488894287109" Y="-2.574125488281" />
                  <Point X="2.800954589844" Y="-2.176943115234" />
                  <Point X="2.786131591797" Y="-2.170012451172" />
                  <Point X="2.754224365234" Y="-2.159825195312" />
                  <Point X="2.643518798828" Y="-2.13983203125" />
                  <Point X="2.538134033203" Y="-2.120799560547" />
                  <Point X="2.506781982422" Y="-2.120395263672" />
                  <Point X="2.474609130859" Y="-2.125353027344" />
                  <Point X="2.444833251953" Y="-2.135176757812" />
                  <Point X="2.352864013672" Y="-2.183579345703" />
                  <Point X="2.265315185547" Y="-2.229655761719" />
                  <Point X="2.242384521484" Y="-2.246548828125" />
                  <Point X="2.221424804688" Y="-2.267508544922" />
                  <Point X="2.204531738281" Y="-2.290439208984" />
                  <Point X="2.156129150391" Y="-2.382408203125" />
                  <Point X="2.110052734375" Y="-2.469957275391" />
                  <Point X="2.100229003906" Y="-2.499733154297" />
                  <Point X="2.095271240234" Y="-2.531906005859" />
                  <Point X="2.095675537109" Y="-2.563258056641" />
                  <Point X="2.115668701172" Y="-2.673963623047" />
                  <Point X="2.134701171875" Y="-2.779348388672" />
                  <Point X="2.138985595703" Y="-2.795141845703" />
                  <Point X="2.151819091797" Y="-2.826078613281" />
                  <Point X="2.529912597656" Y="-3.480955322266" />
                  <Point X="2.861283203125" Y="-4.054906494141" />
                  <Point X="2.781848632813" Y="-4.11164453125" />
                  <Point X="2.701765136719" Y="-4.163481445312" />
                  <Point X="2.283507568359" Y="-3.618397949219" />
                  <Point X="1.758546142578" Y="-2.934255126953" />
                  <Point X="1.747503540039" Y="-2.922178955078" />
                  <Point X="1.721924194336" Y="-2.900557373047" />
                  <Point X="1.612738647461" Y="-2.830361083984" />
                  <Point X="1.50880078125" Y="-2.763538574219" />
                  <Point X="1.479989746094" Y="-2.751167236328" />
                  <Point X="1.448368774414" Y="-2.743435546875" />
                  <Point X="1.417099731445" Y="-2.741116699219" />
                  <Point X="1.297686889648" Y="-2.752105224609" />
                  <Point X="1.184012817383" Y="-2.762565673828" />
                  <Point X="1.15636315918" Y="-2.769397460938" />
                  <Point X="1.128978027344" Y="-2.780740722656" />
                  <Point X="1.104595947266" Y="-2.795461181641" />
                  <Point X="1.012388244629" Y="-2.872128662109" />
                  <Point X="0.924612121582" Y="-2.945111572266" />
                  <Point X="0.904141052246" Y="-2.968861816406" />
                  <Point X="0.887248779297" Y="-2.996688476562" />
                  <Point X="0.875624328613" Y="-3.025808837891" />
                  <Point X="0.848054626465" Y="-3.152650878906" />
                  <Point X="0.821810058594" Y="-3.273396728516" />
                  <Point X="0.819724487305" Y="-3.289627441406" />
                  <Point X="0.819742248535" Y="-3.323120117188" />
                  <Point X="0.926891113281" Y="-4.136995605469" />
                  <Point X="1.022065307617" Y="-4.859915039062" />
                  <Point X="0.975676757813" Y="-4.870083007812" />
                  <Point X="0.929315551758" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058441650391" Y="-4.752633300781" />
                  <Point X="-1.141246582031" Y="-4.731328613281" />
                  <Point X="-1.120775634766" Y="-4.575838378906" />
                  <Point X="-1.120077514648" Y="-4.568099609375" />
                  <Point X="-1.119451782227" Y="-4.541033691406" />
                  <Point X="-1.121759277344" Y="-4.509328125" />
                  <Point X="-1.123334106445" Y="-4.497690429688" />
                  <Point X="-1.154343017578" Y="-4.341798339844" />
                  <Point X="-1.183861450195" Y="-4.193398925781" />
                  <Point X="-1.188125" Y="-4.17846875" />
                  <Point X="-1.19902734375" Y="-4.149502441406" />
                  <Point X="-1.205666015625" Y="-4.135466308594" />
                  <Point X="-1.221738037109" Y="-4.10762890625" />
                  <Point X="-1.23057409668" Y="-4.094861816406" />
                  <Point X="-1.250208496094" Y="-4.070937011719" />
                  <Point X="-1.261006958008" Y="-4.059779296875" />
                  <Point X="-1.380508911133" Y="-3.954978515625" />
                  <Point X="-1.494267578125" Y="-3.855214355469" />
                  <Point X="-1.506739135742" Y="-3.84596484375" />
                  <Point X="-1.533021972656" Y="-3.82962109375" />
                  <Point X="-1.546833618164" Y="-3.822526855469" />
                  <Point X="-1.576530761719" Y="-3.810225830078" />
                  <Point X="-1.591313476562" Y="-3.805476074219" />
                  <Point X="-1.621454833984" Y="-3.798447998047" />
                  <Point X="-1.636813964844" Y="-3.796169677734" />
                  <Point X="-1.795420043945" Y="-3.785774169922" />
                  <Point X="-1.946403198242" Y="-3.775878173828" />
                  <Point X="-1.961928222656" Y="-3.776132324219" />
                  <Point X="-1.992729614258" Y="-3.779166015625" />
                  <Point X="-2.008006103516" Y="-3.781945556641" />
                  <Point X="-2.03905480957" Y="-3.790265136719" />
                  <Point X="-2.053674560547" Y="-3.795496337891" />
                  <Point X="-2.081865478516" Y="-3.808269775391" />
                  <Point X="-2.095436767578" Y="-3.815812011719" />
                  <Point X="-2.227595947266" Y="-3.904117919922" />
                  <Point X="-2.353403320312" Y="-3.988180175781" />
                  <Point X="-2.359681640625" Y="-3.992757080078" />
                  <Point X="-2.380442382812" Y="-4.010131347656" />
                  <Point X="-2.402759277344" Y="-4.032771728516" />
                  <Point X="-2.410470947266" Y="-4.041629394531" />
                  <Point X="-2.503202636719" Y="-4.162479492188" />
                  <Point X="-2.543705566406" Y="-4.137400878906" />
                  <Point X="-2.747583007812" Y="-4.011165283203" />
                  <Point X="-2.947666992188" Y="-3.857107177734" />
                  <Point X="-2.980862792969" Y="-3.831547607422" />
                  <Point X="-2.72287109375" Y="-3.384693115234" />
                  <Point X="-2.341488769531" Y="-2.724119628906" />
                  <Point X="-2.334850097656" Y="-2.710083251953" />
                  <Point X="-2.323947998047" Y="-2.681116943359" />
                  <Point X="-2.319684570312" Y="-2.666187011719" />
                  <Point X="-2.313413574219" Y="-2.634660644531" />
                  <Point X="-2.311638916016" Y="-2.619235351562" />
                  <Point X="-2.310626220703" Y="-2.588301269531" />
                  <Point X="-2.314666259766" Y="-2.557615234375" />
                  <Point X="-2.323650878906" Y="-2.527997558594" />
                  <Point X="-2.329357421875" Y="-2.513557128906" />
                  <Point X="-2.343574462891" Y="-2.484728271484" />
                  <Point X="-2.351557128906" Y="-2.47141015625" />
                  <Point X="-2.369584716797" Y="-2.446253173828" />
                  <Point X="-2.379629638672" Y="-2.434414306641" />
                  <Point X="-2.396978271484" Y="-2.417065673828" />
                  <Point X="-2.408817138672" Y="-2.407020751953" />
                  <Point X="-2.433974121094" Y="-2.388993164062" />
                  <Point X="-2.447292236328" Y="-2.381010498047" />
                  <Point X="-2.47612109375" Y="-2.366793457031" />
                  <Point X="-2.490561523438" Y="-2.361086914062" />
                  <Point X="-2.520179199219" Y="-2.352102294922" />
                  <Point X="-2.550865234375" Y="-2.348062255859" />
                  <Point X="-2.581799316406" Y="-2.349074951172" />
                  <Point X="-2.597224609375" Y="-2.350849609375" />
                  <Point X="-2.628750976562" Y="-2.357120605469" />
                  <Point X="-2.643680908203" Y="-2.361384033203" />
                  <Point X="-2.672647216797" Y="-2.372286132812" />
                  <Point X="-2.68668359375" Y="-2.378924804688" />
                  <Point X="-3.315509277344" Y="-2.741977539062" />
                  <Point X="-3.793089599609" Y="-3.017708496094" />
                  <Point X="-3.842941650391" Y="-2.952212890625" />
                  <Point X="-4.004015380859" Y="-2.740594970703" />
                  <Point X="-4.147456542969" Y="-2.500065429688" />
                  <Point X="-4.181265136719" Y="-2.443373535156" />
                  <Point X="-3.715417480469" Y="-2.085915771484" />
                  <Point X="-3.048122314453" Y="-1.573882080078" />
                  <Point X="-3.036482177734" Y="-1.563309936523" />
                  <Point X="-3.015104980469" Y="-1.540389892578" />
                  <Point X="-3.005367919922" Y="-1.528042114258" />
                  <Point X="-2.987403076172" Y="-1.500911010742" />
                  <Point X="-2.979836181641" Y="-1.487126220703" />
                  <Point X="-2.967080078125" Y="-1.458496704102" />
                  <Point X="-2.961890869141" Y="-1.443652099609" />
                  <Point X="-2.954186279297" Y="-1.413904541016" />
                  <Point X="-2.951552734375" Y="-1.398803466797" />
                  <Point X="-2.948748535156" Y="-1.36837512207" />
                  <Point X="-2.948577880859" Y="-1.353047851562" />
                  <Point X="-2.950786621094" Y="-1.321377075195" />
                  <Point X="-2.953083251953" Y="-1.306221313477" />
                  <Point X="-2.960084228516" Y="-1.276476196289" />
                  <Point X="-2.971778320312" Y="-1.248243896484" />
                  <Point X="-2.987860595703" Y="-1.222260253906" />
                  <Point X="-2.996953125" Y="-1.209919311523" />
                  <Point X="-3.017785644531" Y="-1.185962768555" />
                  <Point X="-3.028745361328" Y="-1.175244384766" />
                  <Point X="-3.052245361328" Y="-1.155710693359" />
                  <Point X="-3.064785888672" Y="-1.14689465332" />
                  <Point X="-3.091268554688" Y="-1.131308227539" />
                  <Point X="-3.10543359375" Y="-1.124481445312" />
                  <Point X="-3.134697021484" Y="-1.113257202148" />
                  <Point X="-3.149795166016" Y="-1.108860107422" />
                  <Point X="-3.181683105469" Y="-1.102378295898" />
                  <Point X="-3.197299560547" Y="-1.100532348633" />
                  <Point X="-3.228622558594" Y="-1.09944140625" />
                  <Point X="-3.244329101562" Y="-1.100196533203" />
                  <Point X="-4.038162109375" Y="-1.204706665039" />
                  <Point X="-4.660920410156" Y="-1.286694458008" />
                  <Point X="-4.677763183594" Y="-1.220755981445" />
                  <Point X="-4.740762207031" Y="-0.974118103027" />
                  <Point X="-4.778712402344" Y="-0.708772155762" />
                  <Point X="-4.786452636719" Y="-0.654654296875" />
                  <Point X="-4.268437988281" Y="-0.515852783203" />
                  <Point X="-3.508288085938" Y="-0.312171325684" />
                  <Point X="-3.496641357422" Y="-0.308226593018" />
                  <Point X="-3.473954345703" Y="-0.298865722656" />
                  <Point X="-3.462914306641" Y="-0.293449462891" />
                  <Point X="-3.433128417969" Y="-0.276361083984" />
                  <Point X="-3.426349365234" Y="-0.272081939697" />
                  <Point X="-3.406782714844" Y="-0.258132354736" />
                  <Point X="-3.381428955078" Y="-0.237220657349" />
                  <Point X="-3.372828369141" Y="-0.229181503296" />
                  <Point X="-3.356689208984" Y="-0.212102493286" />
                  <Point X="-3.342787109375" Y="-0.193160064697" />
                  <Point X="-3.331333251953" Y="-0.172642074585" />
                  <Point X="-3.326242675781" Y="-0.16202684021" />
                  <Point X="-3.314077636719" Y="-0.132018386841" />
                  <Point X="-3.311523193359" Y="-0.12491986084" />
                  <Point X="-3.305001953125" Y="-0.103261787415" />
                  <Point X="-3.298243652344" Y="-0.073904449463" />
                  <Point X="-3.296424560547" Y="-0.063272563934" />
                  <Point X="-3.294003173828" Y="-0.041871608734" />
                  <Point X="-3.2940234375" Y="-0.020333921432" />
                  <Point X="-3.296484863281" Y="0.001062425613" />
                  <Point X="-3.298323730469" Y="0.011690152168" />
                  <Point X="-3.305310058594" Y="0.041781234741" />
                  <Point X="-3.307309326172" Y="0.049064949036" />
                  <Point X="-3.314452148438" Y="0.070552696228" />
                  <Point X="-3.326389160156" Y="0.099826507568" />
                  <Point X="-3.331500732422" Y="0.110430747986" />
                  <Point X="-3.342996337891" Y="0.130925247192" />
                  <Point X="-3.356938232422" Y="0.149840637207" />
                  <Point X="-3.373112548828" Y="0.166886489868" />
                  <Point X="-3.381729003906" Y="0.174907669067" />
                  <Point X="-3.407765625" Y="0.196293319702" />
                  <Point X="-3.414172363281" Y="0.201117477417" />
                  <Point X="-3.434162353516" Y="0.214475997925" />
                  <Point X="-3.463265380859" Y="0.231090255737" />
                  <Point X="-3.474222900391" Y="0.236444396973" />
                  <Point X="-3.496734375" Y="0.245704940796" />
                  <Point X="-3.508288085938" Y="0.249611343384" />
                  <Point X="-4.231904296875" Y="0.443503601074" />
                  <Point X="-4.785446289062" Y="0.591824645996" />
                  <Point X="-4.771931152344" Y="0.683156677246" />
                  <Point X="-4.731330566406" Y="0.95753314209" />
                  <Point X="-4.654937011719" Y="1.239447387695" />
                  <Point X="-4.633586425781" Y="1.318237304688" />
                  <Point X="-4.322959960938" Y="1.277342773438" />
                  <Point X="-3.778066162109" Y="1.20560559082" />
                  <Point X="-3.76773828125" Y="1.204815429688" />
                  <Point X="-3.747058105469" Y="1.204364257812" />
                  <Point X="-3.736705810547" Y="1.20470324707" />
                  <Point X="-3.715144287109" Y="1.20658972168" />
                  <Point X="-3.704890136719" Y="1.208053710938" />
                  <Point X="-3.684603271484" Y="1.212089233398" />
                  <Point X="-3.674570556641" Y="1.214660766602" />
                  <Point X="-3.643101318359" Y="1.224582885742" />
                  <Point X="-3.61314453125" Y="1.234028198242" />
                  <Point X="-3.603451171875" Y="1.237676391602" />
                  <Point X="-3.584518554688" Y="1.246006958008" />
                  <Point X="-3.575279541016" Y="1.250688964844" />
                  <Point X="-3.55653515625" Y="1.261510864258" />
                  <Point X="-3.547860351562" Y="1.267171264648" />
                  <Point X="-3.531179199219" Y="1.27940234375" />
                  <Point X="-3.515928710938" Y="1.293376953125" />
                  <Point X="-3.502290283203" Y="1.308928466797" />
                  <Point X="-3.495895751953" Y="1.317076416016" />
                  <Point X="-3.483481201172" Y="1.334806152344" />
                  <Point X="-3.47801171875" Y="1.343601806641" />
                  <Point X="-3.4680625" Y="1.361736694336" />
                  <Point X="-3.463582763672" Y="1.371075927734" />
                  <Point X="-3.450955566406" Y="1.401560668945" />
                  <Point X="-3.438935302734" Y="1.430580200195" />
                  <Point X="-3.435499023438" Y="1.44035144043" />
                  <Point X="-3.429710693359" Y="1.460210205078" />
                  <Point X="-3.427358642578" Y="1.470298095703" />
                  <Point X="-3.423600341797" Y="1.491613037109" />
                  <Point X="-3.422360595703" Y="1.501897094727" />
                  <Point X="-3.421008056641" Y="1.522537231445" />
                  <Point X="-3.421910400391" Y="1.543200561523" />
                  <Point X="-3.425056884766" Y="1.563644165039" />
                  <Point X="-3.427188232422" Y="1.573780639648" />
                  <Point X="-3.432790039062" Y="1.594686889648" />
                  <Point X="-3.436011962891" Y="1.604529785156" />
                  <Point X="-3.443508789062" Y="1.623808349609" />
                  <Point X="-3.447783691406" Y="1.633244018555" />
                  <Point X="-3.463019775391" Y="1.662512207031" />
                  <Point X="-3.4775234375" Y="1.690373657227" />
                  <Point X="-3.482800048828" Y="1.699286743164" />
                  <Point X="-3.494291748047" Y="1.716485229492" />
                  <Point X="-3.500506835938" Y="1.724770629883" />
                  <Point X="-3.514419433594" Y="1.741351196289" />
                  <Point X="-3.521500488281" Y="1.748911254883" />
                  <Point X="-3.536442626953" Y="1.763215087891" />
                  <Point X="-3.544303710938" Y="1.769958618164" />
                  <Point X="-3.959372070312" Y="2.088451660156" />
                  <Point X="-4.227614257812" Y="2.294281982422" />
                  <Point X="-4.160048339844" Y="2.4100390625" />
                  <Point X="-4.002293212891" Y="2.680312011719" />
                  <Point X="-3.799931884766" Y="2.940418945312" />
                  <Point X="-3.726338623047" Y="3.035012695312" />
                  <Point X="-3.588815673828" Y="2.955613769531" />
                  <Point X="-3.254156982422" Y="2.7623984375" />
                  <Point X="-3.244917480469" Y="2.757716064453" />
                  <Point X="-3.225984619141" Y="2.749385498047" />
                  <Point X="-3.216291259766" Y="2.745737304688" />
                  <Point X="-3.195648925781" Y="2.739228759766" />
                  <Point X="-3.185615234375" Y="2.736657226562" />
                  <Point X="-3.165327880859" Y="2.732621826172" />
                  <Point X="-3.15507421875" Y="2.731157958984" />
                  <Point X="-3.111246337891" Y="2.727323486328" />
                  <Point X="-3.069524902344" Y="2.723673339844" />
                  <Point X="-3.059173095703" Y="2.723334472656" />
                  <Point X="-3.038492675781" Y="2.723785644531" />
                  <Point X="-3.0281640625" Y="2.724575683594" />
                  <Point X="-3.006705566406" Y="2.727400878906" />
                  <Point X="-2.996525146484" Y="2.729310791016" />
                  <Point X="-2.976433837891" Y="2.734227294922" />
                  <Point X="-2.956998046875" Y="2.741301513672" />
                  <Point X="-2.938447021484" Y="2.750449707031" />
                  <Point X="-2.929420898438" Y="2.755530273438" />
                  <Point X="-2.911166503906" Y="2.767159423828" />
                  <Point X="-2.902746337891" Y="2.773193359375" />
                  <Point X="-2.886614257812" Y="2.786140869141" />
                  <Point X="-2.87890234375" Y="2.793054443359" />
                  <Point X="-2.84779296875" Y="2.824163818359" />
                  <Point X="-2.818178710938" Y="2.853777832031" />
                  <Point X="-2.811265380859" Y="2.861489501953" />
                  <Point X="-2.798318359375" Y="2.877620849609" />
                  <Point X="-2.792284667969" Y="2.886040527344" />
                  <Point X="-2.780655273438" Y="2.904294921875" />
                  <Point X="-2.775574707031" Y="2.913320800781" />
                  <Point X="-2.766426025391" Y="2.931872314453" />
                  <Point X="-2.759351318359" Y="2.951309570312" />
                  <Point X="-2.754434814453" Y="2.971401367188" />
                  <Point X="-2.752524902344" Y="2.981581542969" />
                  <Point X="-2.749699707031" Y="3.003040283203" />
                  <Point X="-2.748909667969" Y="3.013368652344" />
                  <Point X="-2.748458496094" Y="3.034048828125" />
                  <Point X="-2.748797363281" Y="3.044400634766" />
                  <Point X="-2.752631835938" Y="3.088228515625" />
                  <Point X="-2.756281982422" Y="3.129950195312" />
                  <Point X="-2.757745849609" Y="3.140203857422" />
                  <Point X="-2.76178125" Y="3.160491210938" />
                  <Point X="-2.764352783203" Y="3.170524902344" />
                  <Point X="-2.770861328125" Y="3.191167236328" />
                  <Point X="-2.774509521484" Y="3.200860595703" />
                  <Point X="-2.782840087891" Y="3.219793457031" />
                  <Point X="-2.787522460938" Y="3.229032958984" />
                  <Point X="-2.971451660156" Y="3.547607666016" />
                  <Point X="-3.059387207031" Y="3.699916259766" />
                  <Point X="-2.923128173828" Y="3.804384765625" />
                  <Point X="-2.648374755859" Y="4.015035888672" />
                  <Point X="-2.329666748047" Y="4.192103515625" />
                  <Point X="-2.192525146484" Y="4.268296875" />
                  <Point X="-2.118563964844" Y="4.171908203125" />
                  <Point X="-2.1118203125" Y="4.164047363281" />
                  <Point X="-2.097517578125" Y="4.149106445313" />
                  <Point X="-2.089958740234" Y="4.142026367188" />
                  <Point X="-2.07337890625" Y="4.128113769531" />
                  <Point X="-2.065093261719" Y="4.1218984375" />
                  <Point X="-2.047893676758" Y="4.110405761719" />
                  <Point X="-2.038979858398" Y="4.10512890625" />
                  <Point X="-1.990199951172" Y="4.079735107422" />
                  <Point X="-1.943763549805" Y="4.055561767578" />
                  <Point X="-1.934327026367" Y="4.051286132812" />
                  <Point X="-1.915047241211" Y="4.0437890625" />
                  <Point X="-1.905203857422" Y="4.040567382812" />
                  <Point X="-1.884297363281" Y="4.034965820313" />
                  <Point X="-1.874162475586" Y="4.032834716797" />
                  <Point X="-1.853719360352" Y="4.029688232422" />
                  <Point X="-1.833053222656" Y="4.028785888672" />
                  <Point X="-1.812413818359" Y="4.030138916016" />
                  <Point X="-1.802132324219" Y="4.031378662109" />
                  <Point X="-1.780817138672" Y="4.035136962891" />
                  <Point X="-1.770729614258" Y="4.037489013672" />
                  <Point X="-1.750869750977" Y="4.043277587891" />
                  <Point X="-1.741097412109" Y="4.046714111328" />
                  <Point X="-1.690289550781" Y="4.067760009766" />
                  <Point X="-1.641923339844" Y="4.087793701172" />
                  <Point X="-1.63258605957" Y="4.092272460938" />
                  <Point X="-1.61445324707" Y="4.102220214844" />
                  <Point X="-1.605657958984" Y="4.107689453125" />
                  <Point X="-1.587928100586" Y="4.120103515625" />
                  <Point X="-1.579779541016" Y="4.126498535156" />
                  <Point X="-1.564227294922" Y="4.140137207031" />
                  <Point X="-1.550252319336" Y="4.155387695312" />
                  <Point X="-1.538020874023" Y="4.172069335938" />
                  <Point X="-1.532360351562" Y="4.180744140625" />
                  <Point X="-1.521538330078" Y="4.19948828125" />
                  <Point X="-1.516856201172" Y="4.208727539062" />
                  <Point X="-1.508525756836" Y="4.22766015625" />
                  <Point X="-1.504877441406" Y="4.237353515625" />
                  <Point X="-1.488340332031" Y="4.289802246094" />
                  <Point X="-1.472598144531" Y="4.339729980469" />
                  <Point X="-1.470026611328" Y="4.349763671875" />
                  <Point X="-1.465990966797" Y="4.370051269531" />
                  <Point X="-1.46452722168" Y="4.380305175781" />
                  <Point X="-1.46264074707" Y="4.4018671875" />
                  <Point X="-1.462301757812" Y="4.412219238281" />
                  <Point X="-1.462752929688" Y="4.432898925781" />
                  <Point X="-1.46354309082" Y="4.4432265625" />
                  <Point X="-1.479266235352" Y="4.562654785156" />
                  <Point X="-1.286860839844" Y="4.616598632812" />
                  <Point X="-0.931175842285" Y="4.7163203125" />
                  <Point X="-0.544815551758" Y="4.761538085938" />
                  <Point X="-0.365222015381" Y="4.782557128906" />
                  <Point X="-0.324667602539" Y="4.631205566406" />
                  <Point X="-0.22566633606" Y="4.261727539062" />
                  <Point X="-0.220435256958" Y="4.247107910156" />
                  <Point X="-0.207661849976" Y="4.218916503906" />
                  <Point X="-0.200119247437" Y="4.205344726562" />
                  <Point X="-0.182260864258" Y="4.178618164062" />
                  <Point X="-0.17260824585" Y="4.166455566406" />
                  <Point X="-0.151451187134" Y="4.143866699219" />
                  <Point X="-0.126897941589" Y="4.125026367188" />
                  <Point X="-0.099602897644" Y="4.110436523438" />
                  <Point X="-0.085356651306" Y="4.104260742188" />
                  <Point X="-0.054918682098" Y="4.093927978516" />
                  <Point X="-0.039857234955" Y="4.090155273438" />
                  <Point X="-0.009319986343" Y="4.08511328125" />
                  <Point X="0.021631772995" Y="4.08511328125" />
                  <Point X="0.052169025421" Y="4.090155273438" />
                  <Point X="0.06723046875" Y="4.093927978516" />
                  <Point X="0.097668441772" Y="4.104260742188" />
                  <Point X="0.111914535522" Y="4.110436523438" />
                  <Point X="0.139209732056" Y="4.125026367188" />
                  <Point X="0.163763122559" Y="4.143866699219" />
                  <Point X="0.184920181274" Y="4.166455566406" />
                  <Point X="0.194572952271" Y="4.178617675781" />
                  <Point X="0.212431182861" Y="4.205344238281" />
                  <Point X="0.2199737854" Y="4.218916503906" />
                  <Point X="0.232747192383" Y="4.247107910156" />
                  <Point X="0.237978271484" Y="4.261727539062" />
                  <Point X="0.332221374512" Y="4.613448242188" />
                  <Point X="0.378190246582" Y="4.785006347656" />
                  <Point X="0.517299682617" Y="4.7704375" />
                  <Point X="0.82787713623" Y="4.737912109375" />
                  <Point X="1.147532836914" Y="4.660737304688" />
                  <Point X="1.453596557617" Y="4.58684375" />
                  <Point X="1.659967651367" Y="4.511991699219" />
                  <Point X="1.858256835938" Y="4.440070800781" />
                  <Point X="2.059450683594" Y="4.345979003906" />
                  <Point X="2.250452880859" Y="4.256653320312" />
                  <Point X="2.444866943359" Y="4.143387207031" />
                  <Point X="2.629435791016" Y="4.035856933594" />
                  <Point X="2.812746582031" Y="3.905496337891" />
                  <Point X="2.817779785156" Y="3.901916992188" />
                  <Point X="2.506729980469" Y="3.363162353516" />
                  <Point X="2.06530859375" Y="2.598597900391" />
                  <Point X="2.062373046875" Y="2.593104980469" />
                  <Point X="2.053180908203" Y="2.573437988281" />
                  <Point X="2.044182250977" Y="2.549564208984" />
                  <Point X="2.041301757812" Y="2.540599121094" />
                  <Point X="2.030302490234" Y="2.499467529297" />
                  <Point X="2.01983203125" Y="2.460312744141" />
                  <Point X="2.017912597656" Y="2.451465576172" />
                  <Point X="2.013646972656" Y="2.420223388672" />
                  <Point X="2.012755615234" Y="2.383241943359" />
                  <Point X="2.013411254883" Y="2.369579833984" />
                  <Point X="2.017700073242" Y="2.334012695312" />
                  <Point X="2.021782592773" Y="2.300155029297" />
                  <Point X="2.023800537109" Y="2.28903515625" />
                  <Point X="2.029143066406" Y="2.267112304688" />
                  <Point X="2.032468017578" Y="2.256309326172" />
                  <Point X="2.040734130859" Y="2.234220458984" />
                  <Point X="2.045318237305" Y="2.223888671875" />
                  <Point X="2.055681396484" Y="2.203843261719" />
                  <Point X="2.061459960938" Y="2.194129638672" />
                  <Point X="2.083467773438" Y="2.161695800781" />
                  <Point X="2.104417724609" Y="2.130821044922" />
                  <Point X="2.109808349609" Y="2.123633300781" />
                  <Point X="2.130464111328" Y="2.100122070312" />
                  <Point X="2.157597412109" Y="2.07538671875" />
                  <Point X="2.168257080078" Y="2.066981445312" />
                  <Point X="2.200690673828" Y="2.044973754883" />
                  <Point X="2.231565673828" Y="2.024024047852" />
                  <Point X="2.241279541016" Y="2.018245239258" />
                  <Point X="2.261324951172" Y="2.007882202148" />
                  <Point X="2.271656494141" Y="2.003297973633" />
                  <Point X="2.293744628906" Y="1.995032104492" />
                  <Point X="2.304548095703" Y="1.991707275391" />
                  <Point X="2.326471191406" Y="1.986364746094" />
                  <Point X="2.337590820312" Y="1.984346801758" />
                  <Point X="2.373157958984" Y="1.980057983398" />
                  <Point X="2.407015625" Y="1.975975219727" />
                  <Point X="2.416044189453" Y="1.975320922852" />
                  <Point X="2.447575439453" Y="1.975497314453" />
                  <Point X="2.484314453125" Y="1.979822875977" />
                  <Point X="2.497748779297" Y="1.982395996094" />
                  <Point X="2.538880371094" Y="1.993395263672" />
                  <Point X="2.57803515625" Y="2.003865722656" />
                  <Point X="2.583995361328" Y="2.005670776367" />
                  <Point X="2.604404541016" Y="2.013067260742" />
                  <Point X="2.627655029297" Y="2.02357409668" />
                  <Point X="2.636033935547" Y="2.027872558594" />
                  <Point X="3.363852783203" Y="2.448078857422" />
                  <Point X="3.940404785156" Y="2.780951416016" />
                  <Point X="4.043954101562" Y="2.637041503906" />
                  <Point X="4.136884765625" Y="2.483472412109" />
                  <Point X="3.754124267578" Y="2.18976953125" />
                  <Point X="3.172951660156" Y="1.743819824219" />
                  <Point X="3.168137451172" Y="1.739868652344" />
                  <Point X="3.152118408203" Y="1.725215698242" />
                  <Point X="3.134668457031" Y="1.706602905273" />
                  <Point X="3.128576416016" Y="1.699422485352" />
                  <Point X="3.098973876953" Y="1.660803710938" />
                  <Point X="3.070794189453" Y="1.624041137695" />
                  <Point X="3.065635742188" Y="1.616602783203" />
                  <Point X="3.049738525391" Y="1.589370727539" />
                  <Point X="3.034762695312" Y="1.555545043945" />
                  <Point X="3.030140136719" Y="1.54267175293" />
                  <Point X="3.01911328125" Y="1.503241943359" />
                  <Point X="3.008616210938" Y="1.465707275391" />
                  <Point X="3.006225097656" Y="1.454661010742" />
                  <Point X="3.002771728516" Y="1.432362792969" />
                  <Point X="3.001709472656" Y="1.421110839844" />
                  <Point X="3.000893310547" Y="1.397540649414" />
                  <Point X="3.001174804688" Y="1.386240966797" />
                  <Point X="3.003077880859" Y="1.363756103516" />
                  <Point X="3.004699462891" Y="1.352570922852" />
                  <Point X="3.013751464844" Y="1.308700073242" />
                  <Point X="3.022368408203" Y="1.266937866211" />
                  <Point X="3.024597900391" Y="1.258234741211" />
                  <Point X="3.034683837891" Y="1.228608520508" />
                  <Point X="3.050286376953" Y="1.195371337891" />
                  <Point X="3.056918457031" Y="1.183526000977" />
                  <Point X="3.081538818359" Y="1.146103881836" />
                  <Point X="3.104976074219" Y="1.110480712891" />
                  <Point X="3.111739013672" Y="1.101424682617" />
                  <Point X="3.126292480469" Y="1.08417980957" />
                  <Point X="3.134083007812" Y="1.075991210938" />
                  <Point X="3.151326904297" Y="1.059901367188" />
                  <Point X="3.160034423828" Y="1.052696044922" />
                  <Point X="3.178243896484" Y="1.039370361328" />
                  <Point X="3.187745849609" Y="1.03325" />
                  <Point X="3.223424072266" Y="1.01316607666" />
                  <Point X="3.257387939453" Y="0.994047546387" />
                  <Point X="3.265479248047" Y="0.989987915039" />
                  <Point X="3.294678222656" Y="0.978084289551" />
                  <Point X="3.330275146484" Y="0.96802130127" />
                  <Point X="3.343670898438" Y="0.965257507324" />
                  <Point X="3.391910400391" Y="0.958882019043" />
                  <Point X="3.437831542969" Y="0.952812988281" />
                  <Point X="3.444029785156" Y="0.952199829102" />
                  <Point X="3.465716064453" Y="0.951222900391" />
                  <Point X="3.491217529297" Y="0.952032287598" />
                  <Point X="3.500603515625" Y="0.952797241211" />
                  <Point X="4.191982910156" Y="1.043819335938" />
                  <Point X="4.704703613281" Y="1.111320068359" />
                  <Point X="4.705663085938" Y="1.107378662109" />
                  <Point X="4.75268359375" Y="0.914233520508" />
                  <Point X="4.78387109375" Y="0.713920776367" />
                  <Point X="4.358028320312" Y="0.599816711426" />
                  <Point X="3.691991943359" Y="0.421352966309" />
                  <Point X="3.686031738281" Y="0.419544525146" />
                  <Point X="3.665626708984" Y="0.412137908936" />
                  <Point X="3.642380859375" Y="0.401619140625" />
                  <Point X="3.634004150391" Y="0.397316345215" />
                  <Point X="3.586610351562" Y="0.369921875" />
                  <Point X="3.541494140625" Y="0.343844055176" />
                  <Point X="3.533881103516" Y="0.338945281982" />
                  <Point X="3.508774169922" Y="0.319870574951" />
                  <Point X="3.481993896484" Y="0.294350982666" />
                  <Point X="3.472796630859" Y="0.284226501465" />
                  <Point X="3.444360351562" Y="0.247991943359" />
                  <Point X="3.417290771484" Y="0.213498947144" />
                  <Point X="3.410854248047" Y="0.204208236694" />
                  <Point X="3.399130126953" Y="0.184928131104" />
                  <Point X="3.393842529297" Y="0.174938735962" />
                  <Point X="3.384068847656" Y="0.153475204468" />
                  <Point X="3.380005126953" Y="0.142928634644" />
                  <Point X="3.373158935547" Y="0.121427505493" />
                  <Point X="3.370376464844" Y="0.110472961426" />
                  <Point X="3.360897705078" Y="0.060978263855" />
                  <Point X="3.351874267578" Y="0.013862423897" />
                  <Point X="3.350603515625" Y="0.004968384266" />
                  <Point X="3.348584472656" Y="-0.026260705948" />
                  <Point X="3.350280029297" Y="-0.062940689087" />
                  <Point X="3.351874511719" Y="-0.076423164368" />
                  <Point X="3.361353515625" Y="-0.125917709351" />
                  <Point X="3.370376708984" Y="-0.173033691406" />
                  <Point X="3.373158935547" Y="-0.18398765564" />
                  <Point X="3.380005126953" Y="-0.20548878479" />
                  <Point X="3.384069091797" Y="-0.216035797119" />
                  <Point X="3.393843017578" Y="-0.23749961853" />
                  <Point X="3.399130615234" Y="-0.247488723755" />
                  <Point X="3.410854492188" Y="-0.266768371582" />
                  <Point X="3.417290771484" Y="-0.276058929443" />
                  <Point X="3.445727050781" Y="-0.312293518066" />
                  <Point X="3.472796630859" Y="-0.346786499023" />
                  <Point X="3.478718017578" Y="-0.353633270264" />
                  <Point X="3.501139648438" Y="-0.375805267334" />
                  <Point X="3.530176513672" Y="-0.398724853516" />
                  <Point X="3.541495117188" Y="-0.406404510498" />
                  <Point X="3.588889160156" Y="-0.433798980713" />
                  <Point X="3.634005126953" Y="-0.459876800537" />
                  <Point X="3.639495849609" Y="-0.462814758301" />
                  <Point X="3.659156738281" Y="-0.472016021729" />
                  <Point X="3.68302734375" Y="-0.481027618408" />
                  <Point X="3.691991943359" Y="-0.483912963867" />
                  <Point X="4.326018066406" Y="-0.653799499512" />
                  <Point X="4.784876953125" Y="-0.776750488281" />
                  <Point X="4.76161328125" Y="-0.931051269531" />
                  <Point X="4.727801757812" Y="-1.079219604492" />
                  <Point X="4.209196777344" Y="-1.010943908691" />
                  <Point X="3.436781982422" Y="-0.909253723145" />
                  <Point X="3.428624511719" Y="-0.908535888672" />
                  <Point X="3.400098388672" Y="-0.908042480469" />
                  <Point X="3.366721435547" Y="-0.910840881348" />
                  <Point X="3.354480957031" Y="-0.912676208496" />
                  <Point X="3.261463623047" Y="-0.932894165039" />
                  <Point X="3.172916748047" Y="-0.952140075684" />
                  <Point X="3.157874511719" Y="-0.956742492676" />
                  <Point X="3.128754150391" Y="-0.968366882324" />
                  <Point X="3.114676269531" Y="-0.975388977051" />
                  <Point X="3.086849609375" Y="-0.992281311035" />
                  <Point X="3.074124023438" Y="-1.001530273438" />
                  <Point X="3.050374023438" Y="-1.022001159668" />
                  <Point X="3.039349365234" Y="-1.033223022461" />
                  <Point X="2.983126220703" Y="-1.100842163086" />
                  <Point X="2.929605224609" Y="-1.165211303711" />
                  <Point X="2.921326171875" Y="-1.176847290039" />
                  <Point X="2.90660546875" Y="-1.201229492188" />
                  <Point X="2.9001640625" Y="-1.213975708008" />
                  <Point X="2.888820800781" Y="-1.241360839844" />
                  <Point X="2.884362792969" Y="-1.254928222656" />
                  <Point X="2.877531005859" Y="-1.282577758789" />
                  <Point X="2.875157226562" Y="-1.296660400391" />
                  <Point X="2.867099121094" Y="-1.384229736328" />
                  <Point X="2.859428222656" Y="-1.467590698242" />
                  <Point X="2.859288574219" Y="-1.483321044922" />
                  <Point X="2.861607177734" Y="-1.514589355469" />
                  <Point X="2.864065429688" Y="-1.530127441406" />
                  <Point X="2.871796875" Y="-1.561748413086" />
                  <Point X="2.876785888672" Y="-1.576668212891" />
                  <Point X="2.889157470703" Y="-1.605479736328" />
                  <Point X="2.896540039062" Y="-1.619371826172" />
                  <Point X="2.948017333984" Y="-1.699440917969" />
                  <Point X="2.997020263672" Y="-1.775662353516" />
                  <Point X="3.001742431641" Y="-1.782354248047" />
                  <Point X="3.019793701172" Y="-1.804450195312" />
                  <Point X="3.043489257812" Y="-1.828120361328" />
                  <Point X="3.052796142578" Y="-1.836277587891" />
                  <Point X="3.641176269531" Y="-2.287758056641" />
                  <Point X="4.087170410156" Y="-2.629981933594" />
                  <Point X="4.045489013672" Y="-2.697429443359" />
                  <Point X="4.001274658203" Y="-2.760251953125" />
                  <Point X="3.536394287109" Y="-2.491853027344" />
                  <Point X="2.848454589844" Y="-2.094670654297" />
                  <Point X="2.841191894531" Y="-2.090885253906" />
                  <Point X="2.815025878906" Y="-2.079513183594" />
                  <Point X="2.783118652344" Y="-2.069325927734" />
                  <Point X="2.771107910156" Y="-2.066337646484" />
                  <Point X="2.66040234375" Y="-2.046344360352" />
                  <Point X="2.555017578125" Y="-2.027311889648" />
                  <Point X="2.539358886719" Y="-2.025807495117" />
                  <Point X="2.508006835938" Y="-2.025403198242" />
                  <Point X="2.492313476562" Y="-2.026503540039" />
                  <Point X="2.460140625" Y="-2.031461303711" />
                  <Point X="2.444844482422" Y="-2.035136230469" />
                  <Point X="2.415068603516" Y="-2.044959960938" />
                  <Point X="2.400588867188" Y="-2.051108642578" />
                  <Point X="2.308619628906" Y="-2.099511230469" />
                  <Point X="2.221070800781" Y="-2.145587646484" />
                  <Point X="2.208968261719" Y="-2.153170410156" />
                  <Point X="2.186037597656" Y="-2.170063476562" />
                  <Point X="2.175209472656" Y="-2.179373779297" />
                  <Point X="2.154249755859" Y="-2.200333496094" />
                  <Point X="2.144939453125" Y="-2.211161621094" />
                  <Point X="2.128046386719" Y="-2.234092285156" />
                  <Point X="2.120463623047" Y="-2.246194824219" />
                  <Point X="2.072061035156" Y="-2.338163818359" />
                  <Point X="2.025984741211" Y="-2.425712890625" />
                  <Point X="2.0198359375" Y="-2.440192626953" />
                  <Point X="2.010012207031" Y="-2.469968505859" />
                  <Point X="2.006337280273" Y="-2.485264648438" />
                  <Point X="2.001379516602" Y="-2.5174375" />
                  <Point X="2.000279174805" Y="-2.533130859375" />
                  <Point X="2.00068347168" Y="-2.564482910156" />
                  <Point X="2.002187866211" Y="-2.580141601562" />
                  <Point X="2.022181030273" Y="-2.690847167969" />
                  <Point X="2.041213500977" Y="-2.796231933594" />
                  <Point X="2.043015014648" Y="-2.804220947266" />
                  <Point X="2.051236083984" Y="-2.83154296875" />
                  <Point X="2.064069580078" Y="-2.862479736328" />
                  <Point X="2.069546630859" Y="-2.873578613281" />
                  <Point X="2.447640136719" Y="-3.528455322266" />
                  <Point X="2.735892822266" Y="-4.027724365234" />
                  <Point X="2.723754150391" Y="-4.036083251953" />
                  <Point X="2.358876220703" Y="-3.560565673828" />
                  <Point X="1.833914550781" Y="-2.876422851562" />
                  <Point X="1.828654418945" Y="-2.870147216797" />
                  <Point X="1.808830932617" Y="-2.849625732422" />
                  <Point X="1.783251586914" Y="-2.828004150391" />
                  <Point X="1.773299072266" Y="-2.820647216797" />
                  <Point X="1.664113525391" Y="-2.750450927734" />
                  <Point X="1.56017565918" Y="-2.683628417969" />
                  <Point X="1.546283935547" Y="-2.676245849609" />
                  <Point X="1.517473022461" Y="-2.663874511719" />
                  <Point X="1.502553588867" Y="-2.658885742188" />
                  <Point X="1.470932617188" Y="-2.651154052734" />
                  <Point X="1.45539453125" Y="-2.648695800781" />
                  <Point X="1.424125488281" Y="-2.646376953125" />
                  <Point X="1.40839440918" Y="-2.646516357422" />
                  <Point X="1.288981811523" Y="-2.657504882812" />
                  <Point X="1.175307739258" Y="-2.667965332031" />
                  <Point X="1.161225097656" Y="-2.670339111328" />
                  <Point X="1.133575439453" Y="-2.677170898438" />
                  <Point X="1.120008422852" Y="-2.68162890625" />
                  <Point X="1.092623168945" Y="-2.692972167969" />
                  <Point X="1.079877563477" Y="-2.699413330078" />
                  <Point X="1.055495361328" Y="-2.714133789062" />
                  <Point X="1.043859008789" Y="-2.722413085938" />
                  <Point X="0.951651306152" Y="-2.799080566406" />
                  <Point X="0.863875244141" Y="-2.872063476562" />
                  <Point X="0.852653320312" Y="-2.883087890625" />
                  <Point X="0.832182128906" Y="-2.906838134766" />
                  <Point X="0.822933044434" Y="-2.919564208984" />
                  <Point X="0.806040710449" Y="-2.947390869141" />
                  <Point X="0.799018737793" Y="-2.961468261719" />
                  <Point X="0.787394348145" Y="-2.990588623047" />
                  <Point X="0.782791931152" Y="-3.005631347656" />
                  <Point X="0.755222229004" Y="-3.132473388672" />
                  <Point X="0.728977661133" Y="-3.253219238281" />
                  <Point X="0.727584777832" Y="-3.261289306641" />
                  <Point X="0.724724487305" Y="-3.289677734375" />
                  <Point X="0.7247421875" Y="-3.323170410156" />
                  <Point X="0.725554992676" Y="-3.335520019531" />
                  <Point X="0.832703796387" Y="-4.149395507812" />
                  <Point X="0.83309161377" Y="-4.152341308594" />
                  <Point X="0.655064941406" Y="-3.487936523438" />
                  <Point X="0.652606262207" Y="-3.480124511719" />
                  <Point X="0.642145141602" Y="-3.453580566406" />
                  <Point X="0.626787231445" Y="-3.423815917969" />
                  <Point X="0.620407592773" Y="-3.413210205078" />
                  <Point X="0.536528259277" Y="-3.292355712891" />
                  <Point X="0.456679992676" Y="-3.177309814453" />
                  <Point X="0.446670684814" Y="-3.165172851562" />
                  <Point X="0.424786712646" Y="-3.142717529297" />
                  <Point X="0.412912322998" Y="-3.132399169922" />
                  <Point X="0.386657196045" Y="-3.113155273438" />
                  <Point X="0.373242340088" Y="-3.104937744141" />
                  <Point X="0.345241210938" Y="-3.090829589844" />
                  <Point X="0.330654907227" Y="-3.084938964844" />
                  <Point X="0.200856124878" Y="-3.044654052734" />
                  <Point X="0.077295806885" Y="-3.006305664062" />
                  <Point X="0.063377269745" Y="-3.003109619141" />
                  <Point X="0.035217689514" Y="-2.998840087891" />
                  <Point X="0.020976644516" Y="-2.997766601562" />
                  <Point X="-0.008664708138" Y="-2.997766601562" />
                  <Point X="-0.022905010223" Y="-2.998840087891" />
                  <Point X="-0.051064441681" Y="-3.003109375" />
                  <Point X="-0.064983421326" Y="-3.006305175781" />
                  <Point X="-0.194782196045" Y="-3.04658984375" />
                  <Point X="-0.318342681885" Y="-3.084938476562" />
                  <Point X="-0.332929412842" Y="-3.090829589844" />
                  <Point X="-0.360930847168" Y="-3.104937988281" />
                  <Point X="-0.374345275879" Y="-3.113155273438" />
                  <Point X="-0.400600524902" Y="-3.132399169922" />
                  <Point X="-0.412475219727" Y="-3.142717773438" />
                  <Point X="-0.434358886719" Y="-3.165173095703" />
                  <Point X="-0.444367889404" Y="-3.177309814453" />
                  <Point X="-0.528247558594" Y="-3.2981640625" />
                  <Point X="-0.60809564209" Y="-3.413210205078" />
                  <Point X="-0.612470336914" Y="-3.420132324219" />
                  <Point X="-0.625976745605" Y="-3.445265136719" />
                  <Point X="-0.638777832031" Y="-3.476215820312" />
                  <Point X="-0.642752990723" Y="-3.487936523438" />
                  <Point X="-0.838111999512" Y="-4.217026367187" />
                  <Point X="-0.985425170898" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.150807198401" Y="4.213928657512" />
                  <Point X="-3.036589885821" Y="3.660430203692" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.359898288436" Y="4.596121447182" />
                  <Point X="-1.47426381655" Y="4.524657933749" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.088526140764" Y="4.140824233315" />
                  <Point X="-2.989060877147" Y="3.578107676219" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.03472959815" Y="4.687287447623" />
                  <Point X="-1.462471611637" Y="4.420004572871" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.999373825545" Y="4.084510834428" />
                  <Point X="-2.941531902538" Y="3.495785127461" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.701693182534" Y="3.020783641084" />
                  <Point X="-3.771198132408" Y="2.977352128101" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.780836893621" Y="4.733915269037" />
                  <Point X="-1.487548800607" Y="4.292312657733" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.894860517206" Y="4.037796049356" />
                  <Point X="-2.894002947976" Y="3.413462566176" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.608513919072" Y="2.966986558735" />
                  <Point X="-3.940802557709" Y="2.759349572463" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.560249209497" Y="4.759731803936" />
                  <Point X="-2.846473993415" Y="3.33114000489" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.51533465786" Y="2.91318947498" />
                  <Point X="-4.067903014583" Y="2.567906444029" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.362242680302" Y="4.771438067188" />
                  <Point X="-2.798945038853" Y="3.248817443604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.422155397252" Y="2.859392390849" />
                  <Point X="-4.170828936694" Y="2.391569241466" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.336531473875" Y="4.675482263766" />
                  <Point X="-2.761697530082" Y="3.160070321951" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.328976136643" Y="2.805595306717" />
                  <Point X="-4.191543209493" Y="2.266603578927" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.310820242184" Y="4.57952647613" />
                  <Point X="-2.749768646608" Y="3.055502367317" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.234008192742" Y="2.752915915956" />
                  <Point X="-4.111079204961" Y="2.204861120971" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.285108988848" Y="4.483570702019" />
                  <Point X="-2.765860943974" Y="2.933424835572" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.097602355773" Y="2.726129794579" />
                  <Point X="-4.030615200429" Y="2.143118663016" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.259397735513" Y="4.387614927908" />
                  <Point X="-3.950151173061" Y="2.08137621933" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.530334461587" Y="4.769072425772" />
                  <Point X="0.342465192336" Y="4.651678677252" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.233686482177" Y="4.291659153797" />
                  <Point X="-3.869686969253" Y="2.019633885896" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.683874391779" Y="4.752992874124" />
                  <Point X="0.306412652683" Y="4.517128601845" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.197888519212" Y="4.202006255396" />
                  <Point X="-3.789222765445" Y="1.957891552463" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.835909317823" Y="4.735972891509" />
                  <Point X="0.270360140272" Y="4.38257854346" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.133590809286" Y="4.130161975407" />
                  <Point X="-3.708758561636" Y="1.896149219029" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.965220063622" Y="4.704753265112" />
                  <Point X="0.232712750192" Y="4.247031894901" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.022839048746" Y="4.087345407919" />
                  <Point X="-3.628294357828" Y="1.834406885595" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.485531579288" Y="1.298745618589" />
                  <Point X="-4.670125157863" Y="1.183398748778" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.094530809421" Y="4.673533638715" />
                  <Point X="-3.54783015402" Y="1.772664552161" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.337456473948" Y="1.279251265377" />
                  <Point X="-4.706668921714" Y="1.048541722425" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.22384147587" Y="4.642313962735" />
                  <Point X="-3.483738633967" Y="1.70069143044" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.189381672126" Y="1.259756722506" />
                  <Point X="-4.737269459885" Y="0.91739843565" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.353152087204" Y="4.611094252315" />
                  <Point X="-3.440380154463" Y="1.615762867108" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.041306903242" Y="1.240262159053" />
                  <Point X="-4.755534679914" Y="0.793963111129" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.478917939685" Y="4.577659530728" />
                  <Point X="-3.4214652545" Y="1.51556026007" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.893232134358" Y="1.2207675956" />
                  <Point X="-4.773799956278" Y="0.670527751405" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.592349222514" Y="4.536517314596" />
                  <Point X="-3.460148854224" Y="1.379366115861" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.739831680537" Y="1.204600889443" />
                  <Point X="-4.739976482731" Y="0.57964105508" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.705780457321" Y="4.495375068457" />
                  <Point X="-4.614506454958" Y="0.5460214817" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.819211621249" Y="4.454232778027" />
                  <Point X="-4.489036427185" Y="0.512401908319" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.925496510214" Y="4.408624999393" />
                  <Point X="-4.363566399412" Y="0.478782334938" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.028030316574" Y="4.360673284203" />
                  <Point X="-4.238096371639" Y="0.445162761557" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.130564058521" Y="4.312721528762" />
                  <Point X="-4.112626341886" Y="0.411543189414" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.233097772007" Y="4.264769755538" />
                  <Point X="-3.987156312029" Y="0.377923617335" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.327523695829" Y="4.211751673041" />
                  <Point X="-3.861686282173" Y="0.344304045256" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.420297660538" Y="4.157701331923" />
                  <Point X="-3.736216252317" Y="0.310684473176" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.51307162321" Y="4.103650989532" />
                  <Point X="-3.610746222461" Y="0.277064901097" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.605845585148" Y="4.049600646683" />
                  <Point X="-3.48764431355" Y="0.241965562818" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.69196313117" Y="3.991390913535" />
                  <Point X="-3.396400990127" Y="0.186958770872" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.71084023579" Y="-0.63439402869" />
                  <Point X="-4.782909388972" Y="-0.679427833731" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.775810979352" Y="3.931762915768" />
                  <Point X="-3.333602446295" Y="0.114177707937" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.396983231722" Y="-0.550296354285" />
                  <Point X="-4.768202105019" Y="-0.782259651059" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.767245240133" Y="3.814388499534" />
                  <Point X="-3.300860262489" Y="0.022615346793" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.083126199243" Y="-0.466198662127" />
                  <Point X="-4.753494927066" Y="-0.885091534622" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.666067821314" Y="3.639143883089" />
                  <Point X="-3.301972349043" Y="-0.09010151033" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.769269147057" Y="-0.382100957655" />
                  <Point X="-4.73744949786" Y="-0.987087185993" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.564890402495" Y="3.463899266644" />
                  <Point X="-4.712774065259" Y="-1.083690212734" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.463712928245" Y="3.288654615563" />
                  <Point X="-4.688098632658" Y="-1.180293239476" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.362535379052" Y="3.113409917652" />
                  <Point X="-4.663423181371" Y="-1.276896254542" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.26135782986" Y="2.93816521974" />
                  <Point X="-4.456831936494" Y="-1.259825665565" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.160180280667" Y="2.762920521829" />
                  <Point X="-4.229706714671" Y="-1.229924023722" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.060179078637" Y="2.588410887207" />
                  <Point X="-4.002581502214" Y="-1.200022387731" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.017690534989" Y="2.449839150155" />
                  <Point X="-3.775456340175" Y="-1.170120783244" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.017272788459" Y="2.337556164832" />
                  <Point X="-3.548331178135" Y="-1.140219178757" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.038922219796" Y="2.239062282641" />
                  <Point X="-3.321206016095" Y="-1.110317574271" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.086717696595" Y="2.156906262934" />
                  <Point X="-3.142841771978" Y="-1.110885172965" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.148623625634" Y="2.083567432372" />
                  <Point X="-3.044975545859" Y="-1.161753515996" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.232099068362" Y="2.023706729851" />
                  <Point X="-2.980797192994" Y="-1.233672378554" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.34663693963" Y="1.98325598692" />
                  <Point X="-2.950414750434" Y="-1.326709269681" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.544571110344" Y="1.994917035575" />
                  <Point X="-2.962897307342" Y="-1.446531185246" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.953232204045" Y="2.763124231936" />
                  <Point X="-3.314345375352" Y="-1.778162260052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.008836127538" Y="2.685847471253" />
                  <Point X="-4.100696004649" Y="-2.381550616474" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.062076593295" Y="2.607093858267" />
                  <Point X="-4.137582903663" Y="-2.516622057474" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.111265509109" Y="2.525808555893" />
                  <Point X="-4.088914124494" Y="-2.598232377296" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.760094137985" Y="2.194350380491" />
                  <Point X="-4.040245345325" Y="-2.679842697118" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.111348225539" Y="1.676946994307" />
                  <Point X="-3.989247133352" Y="-2.759997425773" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.020485158724" Y="1.508147500315" />
                  <Point X="-3.931464089407" Y="-2.835912520871" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.001350197327" Y="1.384168701069" />
                  <Point X="-3.873681045462" Y="-2.91182761597" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.019015966275" Y="1.283185550342" />
                  <Point X="-3.815898060951" Y="-2.987742748206" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.052222477558" Y="1.191913333208" />
                  <Point X="-2.62679711133" Y="-2.356731956781" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.103868852129" Y="1.112163621495" />
                  <Point X="-2.469137671856" Y="-2.370237353334" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.172902875038" Y="1.04327891833" />
                  <Point X="-2.384605935175" Y="-2.429438010138" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.266334231921" Y="0.989639361435" />
                  <Point X="-2.331853154562" Y="-2.508496362624" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.395603651478" Y="0.958393911535" />
                  <Point X="-2.31125964593" Y="-2.607650058551" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.583354364409" Y="0.963691629525" />
                  <Point X="-2.354532370808" Y="-2.74671180642" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.810479687177" Y="0.993593334447" />
                  <Point X="-2.455709976335" Y="-2.921956539533" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.037605009946" Y="1.023495039368" />
                  <Point X="-2.556887581863" Y="-3.097201272647" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.26473020918" Y="1.053396667096" />
                  <Point X="-2.658065187391" Y="-3.27244600576" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.491855146263" Y="1.083298131014" />
                  <Point X="-2.75924281675" Y="-3.447690753765" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.706191437822" Y="1.105208362292" />
                  <Point X="-2.860420488571" Y="-3.622935528302" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.729861973331" Y="1.007977406155" />
                  <Point X="3.865444433328" Y="0.467829378154" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.392697575343" Y="0.172424355389" />
                  <Point X="-2.961598160392" Y="-3.79818030284" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.753253640499" Y="0.910572193739" />
                  <Point X="4.179301378531" Y="0.551927015776" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.356449525123" Y="0.03775211142" />
                  <Point X="-2.915842537002" Y="-3.881610964426" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.769148412522" Y="0.808482401313" />
                  <Point X="4.493158272783" Y="0.636024621561" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.351995177912" Y="-0.077053221954" />
                  <Point X="-2.835530655252" Y="-3.943448479246" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.371375269378" Y="-0.176965145079" />
                  <Point X="-2.755218773502" Y="-4.005285994065" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.409774606614" Y="-0.264992524426" />
                  <Point X="-2.666097656724" Y="-4.061618887902" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.467977220994" Y="-0.340645442818" />
                  <Point X="-2.080105638698" Y="-3.807472383694" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.124075696835" Y="-3.834947925425" />
                  <Point X="-2.576050991661" Y="-4.117373434982" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.541764303895" Y="-0.406560104467" />
                  <Point X="-1.859397890587" Y="-3.781580824489" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.634924815349" Y="-0.46036890437" />
                  <Point X="-1.697144261951" Y="-3.792215453038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.751151123437" Y="-0.49976459488" />
                  <Point X="-1.558588159097" Y="-3.817657939164" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.876621185523" Y="-0.533384146819" />
                  <Point X="3.22436841667" Y="-0.940956911774" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.998944902475" Y="-1.081817156994" />
                  <Point X="-0.096566554422" Y="-3.016107394893" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.513157596417" Y="-3.276422369315" />
                  <Point X="-1.471446677149" Y="-3.875227846134" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.002091247609" Y="-0.567003698759" />
                  <Point X="3.451314824581" Y="-0.911167005264" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.881291855381" Y="-1.267356888601" />
                  <Point X="0.091391166831" Y="-3.010680323747" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.633604637047" Y="-3.463707981853" />
                  <Point X="-1.396857376396" Y="-3.940641226433" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.127561309695" Y="-0.600623250699" />
                  <Point X="3.599389830496" Y="-0.930661420603" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.866706855218" Y="-1.38849255652" />
                  <Point X="0.211171110456" Y="-3.047855456321" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.672847762761" Y="-3.600251756704" />
                  <Point X="-1.322268033379" Y="-4.006054580322" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.253031371781" Y="-0.634242802639" />
                  <Point X="3.747464836411" Y="-0.950155835942" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.860835549603" Y="-1.504183303774" />
                  <Point X="0.33092386118" Y="-3.085047580907" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.70890034996" Y="-3.734801861821" />
                  <Point X="-1.249062928768" Y="-4.072332902367" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.378501395048" Y="-0.667862378835" />
                  <Point X="3.895539842326" Y="-0.969650251281" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.886797059765" Y="-1.599982700064" />
                  <Point X="1.178075115166" Y="-2.667710674179" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.902689469407" Y="-2.839790724169" />
                  <Point X="0.421908189932" Y="-3.140216210685" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.74495293716" Y="-3.869351966939" />
                  <Point X="-1.19793442155" Y="-4.152406213516" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.50397136433" Y="-0.701481988765" />
                  <Point X="4.043614848241" Y="-0.989144666621" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.936234091056" Y="-1.68111296268" />
                  <Point X="1.388307619478" Y="-2.648364773778" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.777484558409" Y="-3.03004938408" />
                  <Point X="0.482434523163" Y="-3.214417108385" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.78100552436" Y="-4.003902072056" />
                  <Point X="-1.17285064314" Y="-4.248754077477" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.629441333613" Y="-0.735101598694" />
                  <Point X="4.191689854156" Y="-1.00863908196" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.987612975837" Y="-1.761029820564" />
                  <Point X="2.5600252641" Y="-2.028216276882" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.076008330669" Y="-2.330663624388" />
                  <Point X="1.532460410808" Y="-2.670310060803" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.749309392788" Y="-3.15967712988" />
                  <Point X="0.536664691624" Y="-3.292552286484" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.817058111559" Y="-4.138452177174" />
                  <Point X="-1.153031521833" Y="-4.34839166431" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.754911302896" Y="-0.768721208624" />
                  <Point X="4.33976480799" Y="-1.028133529843" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.050160645057" Y="-1.833967647355" />
                  <Point X="2.699101957478" Y="-2.053333461944" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.006174227097" Y="-2.486322763748" />
                  <Point X="1.624370816849" Y="-2.724900013266" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.725005805704" Y="-3.28688564491" />
                  <Point X="0.590894670731" Y="-3.370687582905" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.853110716074" Y="-4.273002293111" />
                  <Point X="-1.133212350111" Y="-4.448029219641" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.770684116459" Y="-0.870887209155" />
                  <Point X="4.48783975484" Y="-1.04762798209" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.130418166644" Y="-1.895839130174" />
                  <Point X="2.827678218775" Y="-2.085012045196" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.005546180077" Y="-2.598737159401" />
                  <Point X="1.71273170033" Y="-2.78170795359" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.733224050636" Y="-3.393772263845" />
                  <Point X="0.641015232751" Y="-3.451390728117" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.889163344893" Y="-4.407552424235" />
                  <Point X="-1.119696186199" Y="-4.551605331376" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.746302887491" Y="-0.998144240218" />
                  <Point X="4.63591470169" Y="-1.067122434337" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.210882303957" Y="-1.957581505159" />
                  <Point X="2.922801072065" Y="-2.137594637829" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.023725556917" Y="-2.699399372297" />
                  <Point X="1.797984858054" Y="-2.840457816495" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.746850998715" Y="-3.49727914995" />
                  <Point X="0.67037320989" Y="-3.545067776288" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.925215973712" Y="-4.542102555359" />
                  <Point X="-1.133466231077" Y="-4.672231758714" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.29134644127" Y="-2.019323880143" />
                  <Point X="3.01598032049" Y="-2.191391729573" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.042055821894" Y="-2.799967299819" />
                  <Point X="1.861716908029" Y="-2.91265556005" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.760477946794" Y="-3.600786036054" />
                  <Point X="0.696084472779" Y="-3.641023544429" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.96126860253" Y="-4.676652686483" />
                  <Point X="-1.075740333696" Y="-4.748182562949" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.371810578584" Y="-2.081066255128" />
                  <Point X="3.109159568916" Y="-2.245188821317" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.078555201467" Y="-2.88918190448" />
                  <Point X="1.919816755138" Y="-2.98837269456" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.774104894874" Y="-3.704292922159" />
                  <Point X="0.721795735669" Y="-3.73697931257" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.452274715897" Y="-2.142808630112" />
                  <Point X="3.202338817341" Y="-2.298985913062" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.126084178992" Y="-2.971504451417" />
                  <Point X="1.977916602247" Y="-3.064089829071" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.787731842953" Y="-3.807799808263" />
                  <Point X="0.747506998558" Y="-3.832935080711" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.53273885321" Y="-2.204551005097" />
                  <Point X="3.295518065767" Y="-2.352783004806" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.173613156516" Y="-3.053826998353" />
                  <Point X="2.036016449356" Y="-3.139806963581" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.801358791033" Y="-3.911306694368" />
                  <Point X="0.773218261448" Y="-3.928890848852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.613202990523" Y="-2.266293380081" />
                  <Point X="3.388697314192" Y="-2.40658009655" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.22114213404" Y="-3.13614954529" />
                  <Point X="2.094116296465" Y="-3.215524098092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.814985739112" Y="-4.014813580472" />
                  <Point X="0.798929524337" Y="-4.024846616993" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.693667115696" Y="-2.328035762652" />
                  <Point X="3.481876562618" Y="-2.460377188295" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.268671111565" Y="-3.218472092227" />
                  <Point X="2.152216143574" Y="-3.291241232602" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.828612687192" Y="-4.118320466577" />
                  <Point X="0.824640787227" Y="-4.120802385134" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.774131234398" Y="-2.389778149266" />
                  <Point X="3.575055812373" Y="-2.514174279208" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.316200089089" Y="-3.300794639164" />
                  <Point X="2.210315990683" Y="-3.366958367113" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.854595353101" Y="-2.45152053588" />
                  <Point X="3.668235064004" Y="-2.567971368949" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.363729066614" Y="-3.383117186101" />
                  <Point X="2.268415837792" Y="-3.442675501623" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.935059471803" Y="-2.513262922494" />
                  <Point X="3.761414315635" Y="-2.621768458691" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.411258044138" Y="-3.465439733037" />
                  <Point X="2.326515684901" Y="-3.518392636133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.015523590506" Y="-2.575005309108" />
                  <Point X="3.854593567266" Y="-2.675565548432" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.458787003751" Y="-3.547762291166" />
                  <Point X="2.384615540004" Y="-3.594109765648" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.074812173498" Y="-2.649979638997" />
                  <Point X="3.947772818896" Y="-2.729362638174" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.506315904904" Y="-3.630084885826" />
                  <Point X="2.442715405159" Y="-3.669826888883" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.553844806056" Y="-3.712407480485" />
                  <Point X="2.500815270313" Y="-3.745544012117" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.601373707209" Y="-3.794730075144" />
                  <Point X="2.558915135467" Y="-3.821261135351" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.648902608361" Y="-3.877052669804" />
                  <Point X="2.617015000622" Y="-3.896978258586" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.696431509513" Y="-3.959375264463" />
                  <Point X="2.675114865776" Y="-3.97269538182" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="0.001626220703" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.676761230469" Y="-4.303012207031" />
                  <Point X="0.471539031982" Y="-3.537112304688" />
                  <Point X="0.464318664551" Y="-3.521544189453" />
                  <Point X="0.380439300537" Y="-3.400689697266" />
                  <Point X="0.300591033936" Y="-3.285643798828" />
                  <Point X="0.274335754395" Y="-3.266399902344" />
                  <Point X="0.14453704834" Y="-3.226114990234" />
                  <Point X="0.020976625443" Y="-3.187766601562" />
                  <Point X="-0.008664756775" Y="-3.187766601562" />
                  <Point X="-0.138463470459" Y="-3.228051269531" />
                  <Point X="-0.262023895264" Y="-3.266399902344" />
                  <Point X="-0.288279144287" Y="-3.285643798828" />
                  <Point X="-0.372158660889" Y="-3.406498046875" />
                  <Point X="-0.45200680542" Y="-3.521544189453" />
                  <Point X="-0.459227142334" Y="-3.537112304688" />
                  <Point X="-0.654586181641" Y="-4.266202148438" />
                  <Point X="-0.84774395752" Y="-4.987077148438" />
                  <Point X="-0.955541870117" Y="-4.966152832031" />
                  <Point X="-1.100246459961" Y="-4.938065429688" />
                  <Point X="-1.250276123047" Y="-4.899463867188" />
                  <Point X="-1.35158972168" Y="-4.873396972656" />
                  <Point X="-1.333854248047" Y="-4.738683105469" />
                  <Point X="-1.309150146484" Y="-4.551038574219" />
                  <Point X="-1.309683227539" Y="-4.5347578125" />
                  <Point X="-1.340692138672" Y="-4.378865722656" />
                  <Point X="-1.370210693359" Y="-4.230466308594" />
                  <Point X="-1.386282714844" Y="-4.20262890625" />
                  <Point X="-1.505784912109" Y="-4.097828125" />
                  <Point X="-1.619543457031" Y="-3.998063964844" />
                  <Point X="-1.649240722656" Y="-3.985762939453" />
                  <Point X="-1.807846557617" Y="-3.975367431641" />
                  <Point X="-1.958829833984" Y="-3.965471435547" />
                  <Point X="-1.989878540039" Y="-3.973791015625" />
                  <Point X="-2.122037597656" Y="-4.062096923828" />
                  <Point X="-2.247844970703" Y="-4.146159179688" />
                  <Point X="-2.259734130859" Y="-4.157294433594" />
                  <Point X="-2.369413085938" Y="-4.30023046875" />
                  <Point X="-2.457095214844" Y="-4.414500488281" />
                  <Point X="-2.643728027344" Y="-4.298941894531" />
                  <Point X="-2.855831054688" Y="-4.16761328125" />
                  <Point X="-3.063581542969" Y="-4.007652099609" />
                  <Point X="-3.228581054688" Y="-3.880608154297" />
                  <Point X="-2.887416015625" Y="-3.289693115234" />
                  <Point X="-2.506033691406" Y="-2.629119628906" />
                  <Point X="-2.499762695312" Y="-2.597593261719" />
                  <Point X="-2.513979736328" Y="-2.568764404297" />
                  <Point X="-2.531328369141" Y="-2.551415771484" />
                  <Point X="-2.560157226562" Y="-2.537198730469" />
                  <Point X="-2.59168359375" Y="-2.543469726562" />
                  <Point X="-3.220509277344" Y="-2.906522460938" />
                  <Point X="-3.842959472656" Y="-3.265894042969" />
                  <Point X="-3.994128417969" Y="-3.0672890625" />
                  <Point X="-4.161703613281" Y="-2.847129394531" />
                  <Point X="-4.310642089844" Y="-2.597381835938" />
                  <Point X="-4.431020019531" Y="-2.395527099609" />
                  <Point X="-3.83108203125" Y="-1.935178588867" />
                  <Point X="-3.163786865234" Y="-1.423144897461" />
                  <Point X="-3.145822021484" Y="-1.396013916016" />
                  <Point X="-3.138117431641" Y="-1.366266479492" />
                  <Point X="-3.140326171875" Y="-1.334595825195" />
                  <Point X="-3.161158691406" Y="-1.310639282227" />
                  <Point X="-3.187641357422" Y="-1.295052734375" />
                  <Point X="-3.219529296875" Y="-1.288571044922" />
                  <Point X="-4.013362304688" Y="-1.393081176758" />
                  <Point X="-4.803283203125" Y="-1.497076293945" />
                  <Point X="-4.861853027344" Y="-1.267777954102" />
                  <Point X="-4.927393066406" Y="-1.011191650391" />
                  <Point X="-4.966798339844" Y="-0.735672668457" />
                  <Point X="-4.998396484375" Y="-0.514742126465" />
                  <Point X="-4.317613769531" Y="-0.332326873779" />
                  <Point X="-3.557463867188" Y="-0.128645431519" />
                  <Point X="-3.527677978516" Y="-0.111556991577" />
                  <Point X="-3.50232421875" Y="-0.090645202637" />
                  <Point X="-3.490159179688" Y="-0.06063697052" />
                  <Point X="-3.483400878906" Y="-0.031279655457" />
                  <Point X="-3.490387207031" Y="-0.00118855226" />
                  <Point X="-3.50232421875" Y="0.028085277557" />
                  <Point X="-3.528360839844" Y="0.049470890045" />
                  <Point X="-3.557463867188" Y="0.066085334778" />
                  <Point X="-4.281080078125" Y="0.259977783203" />
                  <Point X="-4.998186523438" Y="0.452125793457" />
                  <Point X="-4.959884277344" Y="0.710969177246" />
                  <Point X="-4.917645019531" Y="0.996418518066" />
                  <Point X="-4.838322753906" Y="1.289141479492" />
                  <Point X="-4.773516113281" Y="1.528298583984" />
                  <Point X="-4.29816015625" Y="1.465717041016" />
                  <Point X="-3.753266357422" Y="1.393980102539" />
                  <Point X="-3.731704833984" Y="1.395866577148" />
                  <Point X="-3.700235595703" Y="1.405788818359" />
                  <Point X="-3.670278808594" Y="1.415234130859" />
                  <Point X="-3.651534423828" Y="1.426056152344" />
                  <Point X="-3.639119873047" Y="1.443785888672" />
                  <Point X="-3.626492675781" Y="1.474270629883" />
                  <Point X="-3.614472412109" Y="1.503290161133" />
                  <Point X="-3.610714111328" Y="1.524605224609" />
                  <Point X="-3.616315917969" Y="1.54551159668" />
                  <Point X="-3.631552001953" Y="1.574779785156" />
                  <Point X="-3.646055664062" Y="1.602641235352" />
                  <Point X="-3.659968261719" Y="1.619221679688" />
                  <Point X="-4.075036621094" Y="1.937714599609" />
                  <Point X="-4.47610546875" Y="2.245466064453" />
                  <Point X="-4.324141113281" Y="2.505817871094" />
                  <Point X="-4.160014648438" Y="2.787006591797" />
                  <Point X="-3.949893310547" Y="3.057087890625" />
                  <Point X="-3.774671142578" Y="3.282310791016" />
                  <Point X="-3.493815673828" Y="3.120158691406" />
                  <Point X="-3.159156982422" Y="2.926943359375" />
                  <Point X="-3.138514648438" Y="2.920434814453" />
                  <Point X="-3.094686767578" Y="2.916600341797" />
                  <Point X="-3.052965332031" Y="2.912950195312" />
                  <Point X="-3.031506835938" Y="2.915775390625" />
                  <Point X="-3.013252441406" Y="2.927404541016" />
                  <Point X="-2.982143066406" Y="2.958513916016" />
                  <Point X="-2.952528808594" Y="2.988127929688" />
                  <Point X="-2.940899414062" Y="3.006382324219" />
                  <Point X="-2.93807421875" Y="3.027841064453" />
                  <Point X="-2.941908691406" Y="3.071668945312" />
                  <Point X="-2.945558837891" Y="3.113390625" />
                  <Point X="-2.952067382812" Y="3.134032958984" />
                  <Point X="-3.135996582031" Y="3.452607666016" />
                  <Point X="-3.307278564453" Y="3.749276611328" />
                  <Point X="-3.038732177734" Y="3.955168457031" />
                  <Point X="-2.752876220703" Y="4.174331542969" />
                  <Point X="-2.421941650391" Y="4.35819140625" />
                  <Point X="-2.141548583984" Y="4.513971679688" />
                  <Point X="-2.070276123047" Y="4.421087890625" />
                  <Point X="-1.967826904297" Y="4.287573242188" />
                  <Point X="-1.951246948242" Y="4.273660644531" />
                  <Point X="-1.902466796875" Y="4.248267089844" />
                  <Point X="-1.856030761719" Y="4.22409375" />
                  <Point X="-1.835124267578" Y="4.2184921875" />
                  <Point X="-1.813809082031" Y="4.222250488281" />
                  <Point X="-1.763001220703" Y="4.243296386719" />
                  <Point X="-1.714635009766" Y="4.263330078125" />
                  <Point X="-1.696905395508" Y="4.275744140625" />
                  <Point X="-1.686083496094" Y="4.29448828125" />
                  <Point X="-1.669546386719" Y="4.346937011719" />
                  <Point X="-1.653804077148" Y="4.396864746094" />
                  <Point X="-1.651917602539" Y="4.418426757812" />
                  <Point X="-1.672828369141" Y="4.577259277344" />
                  <Point X="-1.689137573242" Y="4.701141113281" />
                  <Point X="-1.338152587891" Y="4.799544921875" />
                  <Point X="-0.968094177246" Y="4.903296386719" />
                  <Point X="-0.566901611328" Y="4.95025" />
                  <Point X="-0.224199615479" Y="4.990358398438" />
                  <Point X="-0.14114163208" Y="4.680381347656" />
                  <Point X="-0.042140380859" Y="4.310903320312" />
                  <Point X="-0.024282115936" Y="4.284176757813" />
                  <Point X="0.006155934334" Y="4.273844238281" />
                  <Point X="0.036594036102" Y="4.284176757813" />
                  <Point X="0.054452251434" Y="4.310903320312" />
                  <Point X="0.148695449829" Y="4.662624023438" />
                  <Point X="0.236648406982" Y="4.990868652344" />
                  <Point X="0.53708972168" Y="4.959404296875" />
                  <Point X="0.860205810547" Y="4.925565429688" />
                  <Point X="1.192123413086" Y="4.845430664062" />
                  <Point X="1.508455932617" Y="4.769058105469" />
                  <Point X="1.724752197266" Y="4.690605957031" />
                  <Point X="1.931033691406" Y="4.615786132813" />
                  <Point X="2.139940429688" Y="4.518087402344" />
                  <Point X="2.33869921875" Y="4.425134277344" />
                  <Point X="2.540512939453" Y="4.307557128906" />
                  <Point X="2.732523681641" Y="4.19569140625" />
                  <Point X="2.922859375" Y="4.060335449219" />
                  <Point X="3.068740234375" Y="3.956592773438" />
                  <Point X="2.671274902344" Y="3.268162353516" />
                  <Point X="2.229853515625" Y="2.503597900391" />
                  <Point X="2.224852050781" Y="2.491514648438" />
                  <Point X="2.213852783203" Y="2.450383056641" />
                  <Point X="2.203382324219" Y="2.411228271484" />
                  <Point X="2.202044677734" Y="2.392325927734" />
                  <Point X="2.206333496094" Y="2.356758789062" />
                  <Point X="2.210416015625" Y="2.322901123047" />
                  <Point X="2.218682128906" Y="2.300812255859" />
                  <Point X="2.240689941406" Y="2.268378417969" />
                  <Point X="2.261639892578" Y="2.237503662109" />
                  <Point X="2.274940185547" Y="2.224203613281" />
                  <Point X="2.307373779297" Y="2.202195800781" />
                  <Point X="2.338248779297" Y="2.18124609375" />
                  <Point X="2.360336914062" Y="2.172980224609" />
                  <Point X="2.395904052734" Y="2.16869140625" />
                  <Point X="2.42976171875" Y="2.164608642578" />
                  <Point X="2.448664306641" Y="2.165946289062" />
                  <Point X="2.489795898438" Y="2.176945556641" />
                  <Point X="2.528950683594" Y="2.187416015625" />
                  <Point X="2.541033935547" Y="2.192417480469" />
                  <Point X="3.268852783203" Y="2.612623779297" />
                  <Point X="3.994248046875" Y="3.031430908203" />
                  <Point X="4.088697753906" Y="2.900167480469" />
                  <Point X="4.202591796875" Y="2.741880126953" />
                  <Point X="4.308701171875" Y="2.566533691406" />
                  <Point X="4.387512695312" Y="2.436296142578" />
                  <Point X="3.869788818359" Y="2.039032226562" />
                  <Point X="3.288616210938" Y="1.593082763672" />
                  <Point X="3.279371337891" Y="1.583833129883" />
                  <Point X="3.249768798828" Y="1.545214477539" />
                  <Point X="3.221589111328" Y="1.508451782227" />
                  <Point X="3.213119628906" Y="1.49150012207" />
                  <Point X="3.202092773438" Y="1.4520703125" />
                  <Point X="3.191595703125" Y="1.414535644531" />
                  <Point X="3.190779541016" Y="1.390965332031" />
                  <Point X="3.199831542969" Y="1.347094604492" />
                  <Point X="3.208448486328" Y="1.305332519531" />
                  <Point X="3.215646484375" Y="1.287954956055" />
                  <Point X="3.240266845703" Y="1.250532958984" />
                  <Point X="3.263704101562" Y="1.214909667969" />
                  <Point X="3.280947998047" Y="1.198819824219" />
                  <Point X="3.316626220703" Y="1.178735961914" />
                  <Point X="3.350590087891" Y="1.159617431641" />
                  <Point X="3.368565429688" Y="1.153619628906" />
                  <Point X="3.416804931641" Y="1.147244140625" />
                  <Point X="3.462726074219" Y="1.141175048828" />
                  <Point X="3.475803710938" Y="1.14117175293" />
                  <Point X="4.167183105469" Y="1.232193725586" />
                  <Point X="4.848975097656" Y="1.32195324707" />
                  <Point X="4.890271484375" Y="1.152320800781" />
                  <Point X="4.939188476562" Y="0.951385620117" />
                  <Point X="4.972626464844" Y="0.736617797852" />
                  <Point X="4.997858398438" Y="0.574556152344" />
                  <Point X="4.407204101562" Y="0.41629083252" />
                  <Point X="3.741167724609" Y="0.237826919556" />
                  <Point X="3.729086914062" Y="0.232819412231" />
                  <Point X="3.681693115234" Y="0.20542477417" />
                  <Point X="3.636576904297" Y="0.179346878052" />
                  <Point X="3.622264892578" Y="0.166926574707" />
                  <Point X="3.593828613281" Y="0.130692016602" />
                  <Point X="3.566759033203" Y="0.096198890686" />
                  <Point X="3.556985351562" Y="0.074735427856" />
                  <Point X="3.547506591797" Y="0.02524074173" />
                  <Point X="3.538483154297" Y="-0.021875152588" />
                  <Point X="3.538482910156" Y="-0.040684635162" />
                  <Point X="3.547961914063" Y="-0.090179328918" />
                  <Point X="3.556985107422" Y="-0.137295227051" />
                  <Point X="3.566759033203" Y="-0.158758987427" />
                  <Point X="3.5951953125" Y="-0.194993545532" />
                  <Point X="3.622264892578" Y="-0.229486679077" />
                  <Point X="3.636576904297" Y="-0.241906967163" />
                  <Point X="3.683970947266" Y="-0.269301452637" />
                  <Point X="3.729086914062" Y="-0.295379333496" />
                  <Point X="3.741167724609" Y="-0.300387023926" />
                  <Point X="4.375193847656" Y="-0.470273681641" />
                  <Point X="4.998067871094" Y="-0.637172363281" />
                  <Point X="4.975744628906" Y="-0.785237915039" />
                  <Point X="4.948431640625" Y="-0.966399108887" />
                  <Point X="4.905591796875" Y="-1.154129272461" />
                  <Point X="4.874545410156" Y="-1.290178344727" />
                  <Point X="4.184396972656" Y="-1.199318481445" />
                  <Point X="3.411982177734" Y="-1.097628295898" />
                  <Point X="3.394836181641" Y="-1.098341186523" />
                  <Point X="3.301818847656" Y="-1.118558959961" />
                  <Point X="3.213271972656" Y="-1.137804931641" />
                  <Point X="3.1854453125" Y="-1.154697265625" />
                  <Point X="3.129222167969" Y="-1.22231628418" />
                  <Point X="3.075701171875" Y="-1.286685424805" />
                  <Point X="3.064357910156" Y="-1.314070556641" />
                  <Point X="3.056299804688" Y="-1.401640014648" />
                  <Point X="3.04862890625" Y="-1.485000854492" />
                  <Point X="3.056360351562" Y="-1.516621826172" />
                  <Point X="3.107837646484" Y="-1.596691040039" />
                  <Point X="3.156840576172" Y="-1.672912353516" />
                  <Point X="3.168460693359" Y="-1.685540649414" />
                  <Point X="3.756840820312" Y="-2.137020751953" />
                  <Point X="4.33907421875" Y="-2.583784179688" />
                  <Point X="4.281124511719" Y="-2.677555419922" />
                  <Point X="4.204131835938" Y="-2.802141845703" />
                  <Point X="4.115536132812" Y="-2.9280234375" />
                  <Point X="4.056688476562" Y="-3.011638427734" />
                  <Point X="3.441394287109" Y="-2.656397949219" />
                  <Point X="2.753454589844" Y="-2.259215576172" />
                  <Point X="2.737340820313" Y="-2.253312744141" />
                  <Point X="2.626635253906" Y="-2.233319580078" />
                  <Point X="2.521250488281" Y="-2.214287109375" />
                  <Point X="2.489077636719" Y="-2.219244873047" />
                  <Point X="2.397108398438" Y="-2.267647460938" />
                  <Point X="2.309559570312" Y="-2.313723876953" />
                  <Point X="2.288599853516" Y="-2.33468359375" />
                  <Point X="2.240197265625" Y="-2.426652587891" />
                  <Point X="2.194120849609" Y="-2.514201660156" />
                  <Point X="2.189163085938" Y="-2.546374511719" />
                  <Point X="2.20915625" Y="-2.657080078125" />
                  <Point X="2.228188720703" Y="-2.76246484375" />
                  <Point X="2.234091552734" Y="-2.778578613281" />
                  <Point X="2.612185058594" Y="-3.433455322266" />
                  <Point X="2.986673583984" Y="-4.082088623047" />
                  <Point X="2.926657470703" Y="-4.124956542969" />
                  <Point X="2.835296630859" Y="-4.190213378906" />
                  <Point X="2.736243408203" Y="-4.254329101562" />
                  <Point X="2.679775878906" Y="-4.290879394531" />
                  <Point X="2.208138916016" Y="-3.676230224609" />
                  <Point X="1.683177612305" Y="-2.992087402344" />
                  <Point X="1.670549316406" Y="-2.980467529297" />
                  <Point X="1.561363769531" Y="-2.910271240234" />
                  <Point X="1.45742590332" Y="-2.843448730469" />
                  <Point X="1.425804931641" Y="-2.835717041016" />
                  <Point X="1.306391967773" Y="-2.846705566406" />
                  <Point X="1.192718017578" Y="-2.857166015625" />
                  <Point X="1.165332885742" Y="-2.868509277344" />
                  <Point X="1.073125244141" Y="-2.945176757812" />
                  <Point X="0.985349060059" Y="-3.018159667969" />
                  <Point X="0.968456726074" Y="-3.045986328125" />
                  <Point X="0.940887145996" Y="-3.172828369141" />
                  <Point X="0.91464251709" Y="-3.29357421875" />
                  <Point X="0.91392956543" Y="-3.310720214844" />
                  <Point X="1.021078308105" Y="-4.124595703125" />
                  <Point X="1.127642211914" Y="-4.934028320312" />
                  <Point X="1.080777587891" Y="-4.944301269531" />
                  <Point X="0.994346374512" Y="-4.963246582031" />
                  <Point X="0.902838012695" Y="-4.979870605469" />
                  <Point X="0.860200378418" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#168" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.099001320624" Y="4.724602451013" Z="1.3" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.3" />
                  <Point X="-0.578869750749" Y="5.03119123621" Z="1.3" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.3" />
                  <Point X="-1.357806397926" Y="4.878968586704" Z="1.3" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.3" />
                  <Point X="-1.728556571075" Y="4.60201308957" Z="1.3" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.3" />
                  <Point X="-1.723388128706" Y="4.393252690368" Z="1.3" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.3" />
                  <Point X="-1.788291164805" Y="4.320770070718" Z="1.3" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.3" />
                  <Point X="-1.885534918028" Y="4.323897733978" Z="1.3" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.3" />
                  <Point X="-2.036764365853" Y="4.48280573272" Z="1.3" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.3" />
                  <Point X="-2.452380594318" Y="4.43317901197" Z="1.3" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.3" />
                  <Point X="-3.075310283697" Y="4.026128437953" Z="1.3" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.3" />
                  <Point X="-3.185453935701" Y="3.458887695732" Z="1.3" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.3" />
                  <Point X="-2.997874493648" Y="3.098591433586" Z="1.3" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.3" />
                  <Point X="-3.023654214502" Y="3.025149353384" Z="1.3" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.3" />
                  <Point X="-3.096485032021" Y="2.997690205525" Z="1.3" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.3" />
                  <Point X="-3.474971652565" Y="3.194740013045" Z="1.3" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.3" />
                  <Point X="-3.995512592293" Y="3.119070216922" Z="1.3" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.3" />
                  <Point X="-4.373479316101" Y="2.562303013891" Z="1.3" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.3" />
                  <Point X="-4.111630402498" Y="1.929327050382" Z="1.3" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.3" />
                  <Point X="-3.682058490139" Y="1.582972577373" Z="1.3" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.3" />
                  <Point X="-3.678842734788" Y="1.52468480465" Z="1.3" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.3" />
                  <Point X="-3.721426769708" Y="1.484754757443" Z="1.3" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.3" />
                  <Point X="-4.297789993677" Y="1.546569202375" Z="1.3" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.3" />
                  <Point X="-4.892738690327" Y="1.333498870703" Z="1.3" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.3" />
                  <Point X="-5.015501743284" Y="0.749568191571" Z="1.3" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.3" />
                  <Point X="-4.300177200798" Y="0.242961482507" Z="1.3" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.3" />
                  <Point X="-3.56302596409" Y="0.039675127649" Z="1.3" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.3" />
                  <Point X="-3.544296191367" Y="0.015270429838" Z="1.3" />
                  <Point X="-3.539556741714" Y="0" Z="1.3" />
                  <Point X="-3.544068409768" Y="-0.014536521223" Z="1.3" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.3" />
                  <Point X="-3.562342631033" Y="-0.039200855485" Z="1.3" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.3" />
                  <Point X="-4.336710587974" Y="-0.252750574249" Z="1.3" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.3" />
                  <Point X="-5.022450528877" Y="-0.711471686779" Z="1.3" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.3" />
                  <Point X="-4.916453850021" Y="-1.248872171566" Z="1.3" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.3" />
                  <Point X="-4.012991893278" Y="-1.411373385736" Z="1.3" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.3" />
                  <Point X="-3.206243624714" Y="-1.31446466982" Z="1.3" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.3" />
                  <Point X="-3.196433703667" Y="-1.33695726604" Z="1.3" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.3" />
                  <Point X="-3.867676063311" Y="-1.864230811479" Z="1.3" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.3" />
                  <Point X="-4.359741589539" Y="-2.591711530379" Z="1.3" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.3" />
                  <Point X="-4.040070236586" Y="-3.066292256805" Z="1.3" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.3" />
                  <Point X="-3.201665355042" Y="-2.918543743106" Z="1.3" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.3" />
                  <Point X="-2.564378812836" Y="-2.563951656295" Z="1.3" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.3" />
                  <Point X="-2.936873270062" Y="-3.23341302745" Z="1.3" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.3" />
                  <Point X="-3.10024153536" Y="-4.015989303634" Z="1.3" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.3" />
                  <Point X="-2.676205579876" Y="-4.310172776294" Z="1.3" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.3" />
                  <Point X="-2.335901320073" Y="-4.299388641202" Z="1.3" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.3" />
                  <Point X="-2.100414967645" Y="-4.072390298874" Z="1.3" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.3" />
                  <Point X="-1.817272784609" Y="-3.993980341062" Z="1.3" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.3" />
                  <Point X="-1.54490809602" Y="-4.104139819448" Z="1.3" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.3" />
                  <Point X="-1.39588741247" Y="-4.357340223605" Z="1.3" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.3" />
                  <Point X="-1.389582437169" Y="-4.700876914613" Z="1.3" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.3" />
                  <Point X="-1.26889078205" Y="-4.916606784025" Z="1.3" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.3" />
                  <Point X="-0.971224990995" Y="-4.983957013492" Z="1.3" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.3" />
                  <Point X="-0.612445861271" Y="-4.247863147788" Z="1.3" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.3" />
                  <Point X="-0.337238586566" Y="-3.40372705512" Z="1.3" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.3" />
                  <Point X="-0.129798651239" Y="-3.244524079766" Z="1.3" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.3" />
                  <Point X="0.123560428122" Y="-3.242587965522" Z="1.3" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.3" />
                  <Point X="0.333207272867" Y="-3.397918700648" Z="1.3" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.3" />
                  <Point X="0.62230899913" Y="-4.284672882028" Z="1.3" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.3" />
                  <Point X="0.905618922463" Y="-4.997785492762" Z="1.3" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.3" />
                  <Point X="1.085327364905" Y="-4.961861495701" Z="1.3" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.3" />
                  <Point X="1.064494557099" Y="-4.086789333399" Z="1.3" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.3" />
                  <Point X="0.983590391186" Y="-3.152167913664" Z="1.3" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.3" />
                  <Point X="1.098936098204" Y="-2.952343079598" Z="1.3" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.3" />
                  <Point X="1.304817566023" Y="-2.86521507199" Z="1.3" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.3" />
                  <Point X="1.528168407372" Y="-2.921049088223" Z="1.3" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.3" />
                  <Point X="2.162315308824" Y="-3.675388161711" Z="1.3" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.3" />
                  <Point X="2.757256680205" Y="-4.265023015216" Z="1.3" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.3" />
                  <Point X="2.949563363639" Y="-4.134363559744" Z="1.3" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.3" />
                  <Point X="2.649330306633" Y="-3.377175344392" Z="1.3" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.3" />
                  <Point X="2.252204452388" Y="-2.616913697938" Z="1.3" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.3" />
                  <Point X="2.278287520022" Y="-2.418659240712" Z="1.3" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.3" />
                  <Point X="2.41423913766" Y="-2.280613789945" Z="1.3" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.3" />
                  <Point X="2.611593105575" Y="-2.251243557102" Z="1.3" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.3" />
                  <Point X="3.410238252715" Y="-2.668419193642" Z="1.3" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.3" />
                  <Point X="4.150269017781" Y="-2.9255204382" Z="1.3" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.3" />
                  <Point X="4.317501958032" Y="-2.672560430404" Z="1.3" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.3" />
                  <Point X="3.781122847854" Y="-2.066072960546" Z="1.3" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.3" />
                  <Point X="3.143739681861" Y="-1.538371861065" Z="1.3" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.3" />
                  <Point X="3.099933342133" Y="-1.374941692535" Z="1.3" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.3" />
                  <Point X="3.16151243026" Y="-1.223003116205" Z="1.3" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.3" />
                  <Point X="3.306282450309" Y="-1.136138143262" Z="1.3" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.3" />
                  <Point X="4.171714726896" Y="-1.217610763186" Z="1.3" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.3" />
                  <Point X="4.948183124032" Y="-1.133973254541" Z="1.3" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.3" />
                  <Point X="5.019030235355" Y="-0.761411894577" Z="1.3" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.3" />
                  <Point X="4.381978799522" Y="-0.390697410515" Z="1.3" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.3" />
                  <Point X="3.702836600498" Y="-0.194732722875" Z="1.3" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.3" />
                  <Point X="3.628373055193" Y="-0.132845099923" Z="1.3" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.3" />
                  <Point X="3.590913503876" Y="-0.049494640899" Z="1.3" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.3" />
                  <Point X="3.590457946548" Y="0.047115890311" Z="1.3" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.3" />
                  <Point X="3.627006383207" Y="0.131103638652" Z="1.3" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.3" />
                  <Point X="3.700558813855" Y="0.193416121367" Z="1.3" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.3" />
                  <Point X="4.413989017784" Y="0.399274522254" Z="1.3" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.3" />
                  <Point X="5.015875325539" Y="0.775589934071" Z="1.3" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.3" />
                  <Point X="4.932696619912" Y="1.195427738366" Z="1.3" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.3" />
                  <Point X="4.154500533195" Y="1.313045898952" Z="1.3" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.3" />
                  <Point X="3.417200233077" Y="1.228093126633" Z="1.3" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.3" />
                  <Point X="3.334996951162" Y="1.253587182535" Z="1.3" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.3" />
                  <Point X="3.275881412019" Y="1.309294453606" Z="1.3" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.3" />
                  <Point X="3.242644037776" Y="1.388478576394" Z="1.3" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.3" />
                  <Point X="3.244089027575" Y="1.469883941461" Z="1.3" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.3" />
                  <Point X="3.283295714306" Y="1.54607637893" Z="1.3" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.3" />
                  <Point X="3.89407082143" Y="2.030644553973" Z="1.3" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.3" />
                  <Point X="4.345322506873" Y="2.623699843" Z="1.3" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.3" />
                  <Point X="4.123127021654" Y="2.960650458805" Z="1.3" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.3" />
                  <Point X="3.237696764165" Y="2.687205149396" Z="1.3" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.3" />
                  <Point X="2.470723194656" Y="2.256528284161" Z="1.3" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.3" />
                  <Point X="2.395733913635" Y="2.249611791389" Z="1.3" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.3" />
                  <Point X="2.329291825774" Y="2.274850448444" Z="1.3" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.3" />
                  <Point X="2.275908151791" Y="2.327733034608" Z="1.3" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.3" />
                  <Point X="2.24981791137" Y="2.394024530197" Z="1.3" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.3" />
                  <Point X="2.255999658497" Y="2.468746381226" Z="1.3" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.3" />
                  <Point X="2.708420203463" Y="3.27444231779" Z="1.3" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.3" />
                  <Point X="2.945680433728" Y="4.13236235749" Z="1.3" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.3" />
                  <Point X="2.559526809042" Y="4.382039972677" Z="1.3" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.3" />
                  <Point X="2.154965818492" Y="4.594659397847" Z="1.3" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.3" />
                  <Point X="1.735644835032" Y="4.76888973426" Z="1.3" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.3" />
                  <Point X="1.197701738729" Y="4.925314100069" Z="1.3" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.3" />
                  <Point X="0.536141559079" Y="5.040412558745" Z="1.3" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.3" />
                  <Point X="0.094243240929" Y="4.706845054938" Z="1.3" />
                  <Point X="0" Y="4.355124473572" Z="1.3" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>