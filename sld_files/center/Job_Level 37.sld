<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#189" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2682" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="0" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="0.004716064453" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.84737890625" Y="-4.572713378906" />
                  <Point X="0.563302062988" Y="-3.512524414063" />
                  <Point X="0.557721191406" Y="-3.497141845703" />
                  <Point X="0.542363037109" Y="-3.467377197266" />
                  <Point X="0.426253845215" Y="-3.300085693359" />
                  <Point X="0.378635528564" Y="-3.231476806641" />
                  <Point X="0.356751434326" Y="-3.209021240234" />
                  <Point X="0.330495941162" Y="-3.18977734375" />
                  <Point X="0.302495178223" Y="-3.175669189453" />
                  <Point X="0.122822692871" Y="-3.119905273438" />
                  <Point X="0.049136188507" Y="-3.097035888672" />
                  <Point X="0.020976638794" Y="-3.092766601562" />
                  <Point X="-0.00866468811" Y="-3.092766601562" />
                  <Point X="-0.036824237823" Y="-3.097035888672" />
                  <Point X="-0.21649671936" Y="-3.152799560547" />
                  <Point X="-0.290183380127" Y="-3.175669189453" />
                  <Point X="-0.31818460083" Y="-3.189777587891" />
                  <Point X="-0.344439788818" Y="-3.209021484375" />
                  <Point X="-0.366323577881" Y="-3.231476806641" />
                  <Point X="-0.482432922363" Y="-3.398768066406" />
                  <Point X="-0.530051208496" Y="-3.467377197266" />
                  <Point X="-0.538189025879" Y="-3.481573730469" />
                  <Point X="-0.550990112305" Y="-3.512524414063" />
                  <Point X="-0.667494384766" Y="-3.947324951172" />
                  <Point X="-0.916584533691" Y="-4.876941894531" />
                  <Point X="-0.996442443848" Y="-4.861440917969" />
                  <Point X="-1.079341064453" Y="-4.845350097656" />
                  <Point X="-1.24641809082" Y="-4.802362304688" />
                  <Point X="-1.214963012695" Y="-4.563438476562" />
                  <Point X="-1.214201171875" Y="-4.5479296875" />
                  <Point X="-1.216508666992" Y="-4.516224121094" />
                  <Point X="-1.259432373047" Y="-4.300432128906" />
                  <Point X="-1.277036132812" Y="-4.211932617188" />
                  <Point X="-1.287938354492" Y="-4.182966308594" />
                  <Point X="-1.304010375977" Y="-4.15512890625" />
                  <Point X="-1.32364453125" Y="-4.131204101562" />
                  <Point X="-1.489064208984" Y="-3.986134521484" />
                  <Point X="-1.556905517578" Y="-3.926639404297" />
                  <Point X="-1.583188598633" Y="-3.910295410156" />
                  <Point X="-1.612885864258" Y="-3.897994384766" />
                  <Point X="-1.64302734375" Y="-3.890966308594" />
                  <Point X="-1.862576171875" Y="-3.876576171875" />
                  <Point X="-1.952616577148" Y="-3.870674804688" />
                  <Point X="-1.98341809082" Y="-3.873708496094" />
                  <Point X="-2.014466674805" Y="-3.882028076172" />
                  <Point X="-2.042657836914" Y="-3.894801513672" />
                  <Point X="-2.22559765625" Y="-4.017038330078" />
                  <Point X="-2.300624267578" Y="-4.067169677734" />
                  <Point X="-2.312785644531" Y="-4.076821533203" />
                  <Point X="-2.335102783203" Y="-4.099461914062" />
                  <Point X="-2.400510986328" Y="-4.184703613281" />
                  <Point X="-2.480149169922" Y="-4.288489746094" />
                  <Point X="-2.680204345703" Y="-4.164620605469" />
                  <Point X="-2.801708984375" Y="-4.089387939453" />
                  <Point X="-3.086224853516" Y="-3.870320068359" />
                  <Point X="-3.104721923828" Y="-3.856077880859" />
                  <Point X="-2.951685791016" Y="-3.591011962891" />
                  <Point X="-2.423761230469" Y="-2.676619628906" />
                  <Point X="-2.412859130859" Y="-2.647653320312" />
                  <Point X="-2.406588134766" Y="-2.616126953125" />
                  <Point X="-2.405575439453" Y="-2.585192871094" />
                  <Point X="-2.414560058594" Y="-2.555575195312" />
                  <Point X="-2.428777099609" Y="-2.526746337891" />
                  <Point X="-2.4468046875" Y="-2.501589355469" />
                  <Point X="-2.464153320312" Y="-2.484240722656" />
                  <Point X="-2.489310302734" Y="-2.466213134766" />
                  <Point X="-2.518139160156" Y="-2.45199609375" />
                  <Point X="-2.547756835938" Y="-2.443011474609" />
                  <Point X="-2.578690917969" Y="-2.444024169922" />
                  <Point X="-2.610217285156" Y="-2.450295166016" />
                  <Point X="-2.63918359375" Y="-2.461197265625" />
                  <Point X="-3.014190673828" Y="-2.677707763672" />
                  <Point X="-3.818024658203" Y="-3.141801269531" />
                  <Point X="-3.986864990234" Y="-2.919979492188" />
                  <Point X="-4.082858642578" Y="-2.79386328125" />
                  <Point X="-4.286833984375" Y="-2.451827636719" />
                  <Point X="-4.306142578125" Y="-2.419450195312" />
                  <Point X="-4.029651611328" Y="-2.207291015625" />
                  <Point X="-3.105954589844" Y="-1.498513549805" />
                  <Point X="-3.084576904297" Y="-1.475592895508" />
                  <Point X="-3.066612060547" Y="-1.448461181641" />
                  <Point X="-3.053856689453" Y="-1.419832519531" />
                  <Point X="-3.048392822266" Y="-1.398736694336" />
                  <Point X="-3.046152099609" Y="-1.390085083008" />
                  <Point X="-3.04334765625" Y="-1.359655761719" />
                  <Point X="-3.045556640625" Y="-1.327985473633" />
                  <Point X="-3.052557861328" Y="-1.298240722656" />
                  <Point X="-3.068639892578" Y="-1.272257446289" />
                  <Point X="-3.089472167969" Y="-1.248301147461" />
                  <Point X="-3.112972412109" Y="-1.228766967773" />
                  <Point X="-3.131752929688" Y="-1.217713745117" />
                  <Point X="-3.139455078125" Y="-1.213180419922" />
                  <Point X="-3.168718261719" Y="-1.201956420898" />
                  <Point X="-3.200606201172" Y="-1.195474731445" />
                  <Point X="-3.231929199219" Y="-1.194383789062" />
                  <Point X="-3.705340087891" Y="-1.256709594727" />
                  <Point X="-4.732102050781" Y="-1.391885375977" />
                  <Point X="-4.796532226562" Y="-1.139643066406" />
                  <Point X="-4.834077636719" Y="-0.99265423584" />
                  <Point X="-4.888043945312" Y="-0.615327209473" />
                  <Point X="-4.892424316406" Y="-0.584698242188" />
                  <Point X="-4.585105957031" Y="-0.502352508545" />
                  <Point X="-3.532875976562" Y="-0.220408340454" />
                  <Point X="-3.517493164062" Y="-0.214827468872" />
                  <Point X="-3.487728515625" Y="-0.199469467163" />
                  <Point X="-3.468047119141" Y="-0.185809539795" />
                  <Point X="-3.459975585938" Y="-0.180207260132" />
                  <Point X="-3.437520019531" Y="-0.158322875977" />
                  <Point X="-3.418276123047" Y="-0.132067398071" />
                  <Point X="-3.404168212891" Y="-0.104067070007" />
                  <Point X="-3.397607666016" Y="-0.082929191589" />
                  <Point X="-3.394917236328" Y="-0.074260139465" />
                  <Point X="-3.390647705078" Y="-0.046100288391" />
                  <Point X="-3.390647705078" Y="-0.016459869385" />
                  <Point X="-3.394917236328" Y="0.011700135231" />
                  <Point X="-3.401477783203" Y="0.032838012695" />
                  <Point X="-3.404168212891" Y="0.041506916046" />
                  <Point X="-3.418275878906" Y="0.069507080078" />
                  <Point X="-3.437519775391" Y="0.09576272583" />
                  <Point X="-3.459975585938" Y="0.117647262573" />
                  <Point X="-3.479656982422" Y="0.131307189941" />
                  <Point X="-3.488528808594" Y="0.136769210815" />
                  <Point X="-3.512168701172" Y="0.14959185791" />
                  <Point X="-3.532875976562" Y="0.157848342896" />
                  <Point X="-3.964412353516" Y="0.273478179932" />
                  <Point X="-4.89181640625" Y="0.521975219727" />
                  <Point X="-4.848684082031" Y="0.81345892334" />
                  <Point X="-4.824487792969" Y="0.976975341797" />
                  <Point X="-4.71585546875" Y="1.377862060547" />
                  <Point X="-4.703551269531" Y="1.423268066406" />
                  <Point X="-4.519930175781" Y="1.39909387207" />
                  <Point X="-3.765666259766" Y="1.29979284668" />
                  <Point X="-3.744985839844" Y="1.299341674805" />
                  <Point X="-3.723424072266" Y="1.301228271484" />
                  <Point X="-3.703137451172" Y="1.305263671875" />
                  <Point X="-3.659576416016" Y="1.318998168945" />
                  <Point X="-3.641711425781" Y="1.324631225586" />
                  <Point X="-3.622778564453" Y="1.332961669922" />
                  <Point X="-3.604034423828" Y="1.343783569336" />
                  <Point X="-3.587353027344" Y="1.356015014648" />
                  <Point X="-3.573714599609" Y="1.371566894531" />
                  <Point X="-3.561300292969" Y="1.389296630859" />
                  <Point X="-3.5513515625" Y="1.407431030273" />
                  <Point X="-3.533872558594" Y="1.449629150391" />
                  <Point X="-3.526704101563" Y="1.466935302734" />
                  <Point X="-3.520916015625" Y="1.48679309082" />
                  <Point X="-3.517157470703" Y="1.508108154297" />
                  <Point X="-3.515804443359" Y="1.528748779297" />
                  <Point X="-3.518951171875" Y="1.549192871094" />
                  <Point X="-3.524552978516" Y="1.570099121094" />
                  <Point X="-3.532049804688" Y="1.589377563477" />
                  <Point X="-3.553140136719" Y="1.629891845703" />
                  <Point X="-3.561789550781" Y="1.646507080078" />
                  <Point X="-3.57328125" Y="1.663705932617" />
                  <Point X="-3.587193847656" Y="1.680286376953" />
                  <Point X="-3.602135986328" Y="1.694590332031" />
                  <Point X="-3.849666503906" Y="1.884526977539" />
                  <Point X="-4.351859863281" Y="2.269874267578" />
                  <Point X="-4.175169921875" Y="2.572586914062" />
                  <Point X="-4.081153808594" Y="2.733659423828" />
                  <Point X="-3.793391113281" Y="3.103537597656" />
                  <Point X="-3.750505126953" Y="3.158661621094" />
                  <Point X="-3.669905029297" Y="3.112127197266" />
                  <Point X="-3.206656982422" Y="2.844670898438" />
                  <Point X="-3.187724121094" Y="2.836340332031" />
                  <Point X="-3.167081787109" Y="2.829831787109" />
                  <Point X="-3.146794433594" Y="2.825796386719" />
                  <Point X="-3.086125976562" Y="2.820488525391" />
                  <Point X="-3.061245117188" Y="2.818311767578" />
                  <Point X="-3.040564941406" Y="2.818762939453" />
                  <Point X="-3.019106201172" Y="2.821588134766" />
                  <Point X="-2.999014160156" Y="2.826504882812" />
                  <Point X="-2.980462402344" Y="2.835653564453" />
                  <Point X="-2.962208251953" Y="2.847282958984" />
                  <Point X="-2.946077392578" Y="2.860229492188" />
                  <Point X="-2.903014404297" Y="2.903292236328" />
                  <Point X="-2.885353759766" Y="2.920952880859" />
                  <Point X="-2.872406982422" Y="2.937083984375" />
                  <Point X="-2.860777587891" Y="2.955338134766" />
                  <Point X="-2.85162890625" Y="2.973889892578" />
                  <Point X="-2.846712158203" Y="2.993981933594" />
                  <Point X="-2.843886962891" Y="3.015440673828" />
                  <Point X="-2.843435791016" Y="3.036120849609" />
                  <Point X="-2.848743652344" Y="3.096789306641" />
                  <Point X="-2.850920410156" Y="3.121670410156" />
                  <Point X="-2.854955810547" Y="3.141957763672" />
                  <Point X="-2.861464355469" Y="3.162600097656" />
                  <Point X="-2.869794921875" Y="3.181532958984" />
                  <Point X="-2.979483154297" Y="3.371518310547" />
                  <Point X="-3.183333007812" Y="3.724596435547" />
                  <Point X="-2.864367431641" Y="3.969144042969" />
                  <Point X="-2.700626464844" Y="4.094683349609" />
                  <Point X="-2.24741015625" Y="4.34648046875" />
                  <Point X="-2.167036865234" Y="4.391134277344" />
                  <Point X="-2.04319543457" Y="4.229740722656" />
                  <Point X="-2.028892456055" Y="4.214799804688" />
                  <Point X="-2.01231237793" Y="4.200887207031" />
                  <Point X="-1.99511328125" Y="4.189395019531" />
                  <Point X="-1.92758984375" Y="4.154244140625" />
                  <Point X="-1.899897094727" Y="4.139828125" />
                  <Point X="-1.880617431641" Y="4.132330566406" />
                  <Point X="-1.85971105957" Y="4.126729003906" />
                  <Point X="-1.839267944336" Y="4.123582519531" />
                  <Point X="-1.818628417969" Y="4.124935546875" />
                  <Point X="-1.797313232422" Y="4.128693847656" />
                  <Point X="-1.777453735352" Y="4.134481933594" />
                  <Point X="-1.707123413086" Y="4.163614257812" />
                  <Point X="-1.678279785156" Y="4.175561523437" />
                  <Point X="-1.660145385742" Y="4.185510742187" />
                  <Point X="-1.642415649414" Y="4.197925292969" />
                  <Point X="-1.626864257812" Y="4.211563476563" />
                  <Point X="-1.614633300781" Y="4.228244140625" />
                  <Point X="-1.603811279297" Y="4.24698828125" />
                  <Point X="-1.59548046875" Y="4.265921386719" />
                  <Point X="-1.572589111328" Y="4.338522949219" />
                  <Point X="-1.563201171875" Y="4.368297851562" />
                  <Point X="-1.559165649414" Y="4.388584960938" />
                  <Point X="-1.557279174805" Y="4.410146484375" />
                  <Point X="-1.55773034668" Y="4.430826660156" />
                  <Point X="-1.570200805664" Y="4.525548339844" />
                  <Point X="-1.584202026367" Y="4.6318984375" />
                  <Point X="-1.161609375" Y="4.75037890625" />
                  <Point X="-0.94963848877" Y="4.809808105469" />
                  <Point X="-0.400205169678" Y="4.874110839844" />
                  <Point X="-0.294710845947" Y="4.886457519531" />
                  <Point X="-0.270945007324" Y="4.79776171875" />
                  <Point X="-0.133903305054" Y="4.286315429688" />
                  <Point X="-0.121129844666" Y="4.258124023438" />
                  <Point X="-0.10327155304" Y="4.231397460938" />
                  <Point X="-0.082114532471" Y="4.20880859375" />
                  <Point X="-0.054819446564" Y="4.19421875" />
                  <Point X="-0.024381370544" Y="4.183886230469" />
                  <Point X="0.006155906677" Y="4.178844238281" />
                  <Point X="0.03669316864" Y="4.183886230469" />
                  <Point X="0.067131248474" Y="4.19421875" />
                  <Point X="0.094426467896" Y="4.20880859375" />
                  <Point X="0.115583503723" Y="4.231397460938" />
                  <Point X="0.133441802979" Y="4.258124023438" />
                  <Point X="0.146215255737" Y="4.286315429688" />
                  <Point X="0.202418243408" Y="4.496067871094" />
                  <Point X="0.307419403076" Y="4.8879375" />
                  <Point X="0.658949462891" Y="4.851123046875" />
                  <Point X="0.84404107666" Y="4.831738769531" />
                  <Point X="1.298603393555" Y="4.721993652344" />
                  <Point X="1.48102722168" Y="4.677950683594" />
                  <Point X="1.776472900391" Y="4.570790039062" />
                  <Point X="1.894646484375" Y="4.527927734375" />
                  <Point X="2.180745117188" Y="4.39412890625" />
                  <Point X="2.294576416016" Y="4.340893554688" />
                  <Point X="2.570984130859" Y="4.179857910156" />
                  <Point X="2.680978759766" Y="4.115774902344" />
                  <Point X="2.941647216797" Y="3.930401855469" />
                  <Point X="2.943260009766" Y="3.929254882812" />
                  <Point X="2.758614257812" Y="3.609438476562" />
                  <Point X="2.147581054688" Y="2.551097900391" />
                  <Point X="2.142076171875" Y="2.539931884766" />
                  <Point X="2.133076660156" Y="2.516056884766" />
                  <Point X="2.117851318359" Y="2.459120849609" />
                  <Point X="2.111606933594" Y="2.435770507813" />
                  <Point X="2.108619384766" Y="2.417934570312" />
                  <Point X="2.107728027344" Y="2.380953125" />
                  <Point X="2.113664794922" Y="2.331719726562" />
                  <Point X="2.116099365234" Y="2.311528320312" />
                  <Point X="2.121441894531" Y="2.289605712891" />
                  <Point X="2.129708007812" Y="2.267516601562" />
                  <Point X="2.140070800781" Y="2.247471191406" />
                  <Point X="2.170534667969" Y="2.202575195312" />
                  <Point X="2.183028564453" Y="2.184162597656" />
                  <Point X="2.19446484375" Y="2.170328613281" />
                  <Point X="2.221598876953" Y="2.145592285156" />
                  <Point X="2.266494873047" Y="2.115128417969" />
                  <Point X="2.284907470703" Y="2.102634765625" />
                  <Point X="2.304952392578" Y="2.092271972656" />
                  <Point X="2.327040527344" Y="2.084006103516" />
                  <Point X="2.348963623047" Y="2.078663574219" />
                  <Point X="2.398197021484" Y="2.072726806641" />
                  <Point X="2.418388427734" Y="2.070291992188" />
                  <Point X="2.436467285156" Y="2.069845703125" />
                  <Point X="2.473206542969" Y="2.074171142578" />
                  <Point X="2.530142578125" Y="2.089396728516" />
                  <Point X="2.553492919922" Y="2.095640869141" />
                  <Point X="2.565283447266" Y="2.099638183594" />
                  <Point X="2.588533935547" Y="2.110145019531" />
                  <Point X="3.022576416016" Y="2.360739501953" />
                  <Point X="3.967326416016" Y="2.906191162109" />
                  <Point X="4.058026611328" Y="2.780138671875" />
                  <Point X="4.123270507813" Y="2.689464111328" />
                  <Point X="4.262198730469" Y="2.459884277344" />
                  <Point X="4.035266357422" Y="2.285752441406" />
                  <Point X="3.230783935547" Y="1.668451293945" />
                  <Point X="3.221424560547" Y="1.660241455078" />
                  <Point X="3.203973632812" Y="1.641627807617" />
                  <Point X="3.162996582031" Y="1.588170166016" />
                  <Point X="3.14619140625" Y="1.566246459961" />
                  <Point X="3.136604980469" Y="1.550910644531" />
                  <Point X="3.121629882812" Y="1.51708605957" />
                  <Point X="3.106365966797" Y="1.462505737305" />
                  <Point X="3.100105957031" Y="1.440121704102" />
                  <Point X="3.096652587891" Y="1.417823242188" />
                  <Point X="3.095836425781" Y="1.394252563477" />
                  <Point X="3.097739501953" Y="1.371768066406" />
                  <Point X="3.11026953125" Y="1.311040405273" />
                  <Point X="3.115408447266" Y="1.286135131836" />
                  <Point X="3.120680175781" Y="1.268977050781" />
                  <Point X="3.136282470703" Y="1.235740478516" />
                  <Point X="3.170363037109" Y="1.183939331055" />
                  <Point X="3.184340087891" Y="1.162695068359" />
                  <Point X="3.198893554688" Y="1.145450439453" />
                  <Point X="3.216137451172" Y="1.129360595703" />
                  <Point X="3.234347167969" Y="1.116034912109" />
                  <Point X="3.283734619141" Y="1.088233886719" />
                  <Point X="3.303989257812" Y="1.076832397461" />
                  <Point X="3.320521240234" Y="1.069501586914" />
                  <Point X="3.356118408203" Y="1.059438598633" />
                  <Point X="3.422893554688" Y="1.050613525391" />
                  <Point X="3.450279052734" Y="1.046994018555" />
                  <Point X="3.462702148438" Y="1.046175048828" />
                  <Point X="3.488203613281" Y="1.04698449707" />
                  <Point X="3.900515136719" Y="1.101266357422" />
                  <Point X="4.776839355469" Y="1.21663659668" />
                  <Point X="4.817913574219" Y="1.047916137695" />
                  <Point X="4.845936035156" Y="0.932809082031" />
                  <Point X="4.890865234375" Y="0.644238708496" />
                  <Point X="4.638534179688" Y="0.576626708984" />
                  <Point X="3.716579833984" Y="0.329589935303" />
                  <Point X="3.704791259766" Y="0.325586608887" />
                  <Point X="3.681545654297" Y="0.315067993164" />
                  <Point X="3.615941162109" Y="0.277147399902" />
                  <Point X="3.589035644531" Y="0.261595428467" />
                  <Point X="3.574311523438" Y="0.251096679688" />
                  <Point X="3.547530761719" Y="0.22557661438" />
                  <Point X="3.508168212891" Y="0.175419143677" />
                  <Point X="3.492024902344" Y="0.154848968506" />
                  <Point X="3.480301025391" Y="0.135569000244" />
                  <Point X="3.470527099609" Y="0.114105232239" />
                  <Point X="3.463680908203" Y="0.092604270935" />
                  <Point X="3.450559814453" Y="0.024091848373" />
                  <Point X="3.445178710938" Y="-0.004006225586" />
                  <Point X="3.443482910156" Y="-0.021875295639" />
                  <Point X="3.445178466797" Y="-0.058553627014" />
                  <Point X="3.458299560547" Y="-0.127066200256" />
                  <Point X="3.463680664062" Y="-0.155164276123" />
                  <Point X="3.470527099609" Y="-0.176665237427" />
                  <Point X="3.480301025391" Y="-0.198129165649" />
                  <Point X="3.492024902344" Y="-0.217409118652" />
                  <Point X="3.531387451172" Y="-0.267566436768" />
                  <Point X="3.547530761719" Y="-0.288136627197" />
                  <Point X="3.559999023438" Y="-0.30123614502" />
                  <Point X="3.589035644531" Y="-0.324155578613" />
                  <Point X="3.654640136719" Y="-0.362076171875" />
                  <Point X="3.681545654297" Y="-0.377628143311" />
                  <Point X="3.692710205078" Y="-0.383139038086" />
                  <Point X="3.716579833984" Y="-0.392149932861" />
                  <Point X="4.094687988281" Y="-0.493463653564" />
                  <Point X="4.891472167969" Y="-0.706961486816" />
                  <Point X="4.870668945313" Y="-0.84494543457" />
                  <Point X="4.855022460938" Y="-0.948726135254" />
                  <Point X="4.801173828125" Y="-1.184698974609" />
                  <Point X="4.49358984375" Y="-1.144204833984" />
                  <Point X="3.424382080078" Y="-1.003441040039" />
                  <Point X="3.408035644531" Y="-1.002710327148" />
                  <Point X="3.374658691406" Y="-1.005508728027" />
                  <Point X="3.245900390625" Y="-1.033494873047" />
                  <Point X="3.193094482422" Y="-1.044972412109" />
                  <Point X="3.163974121094" Y="-1.056596923828" />
                  <Point X="3.136147460938" Y="-1.073489257812" />
                  <Point X="3.112397460938" Y="-1.093960083008" />
                  <Point X="3.034571044922" Y="-1.187561035156" />
                  <Point X="3.002653320312" Y="-1.225948364258" />
                  <Point X="2.987932617188" Y="-1.250330444336" />
                  <Point X="2.976589355469" Y="-1.277715576172" />
                  <Point X="2.969757568359" Y="-1.305365356445" />
                  <Point X="2.958603271484" Y="-1.426582397461" />
                  <Point X="2.954028564453" Y="-1.476295654297" />
                  <Point X="2.956347167969" Y="-1.507563842773" />
                  <Point X="2.964078613281" Y="-1.539184936523" />
                  <Point X="2.976450195312" Y="-1.567996826172" />
                  <Point X="3.04770703125" Y="-1.67883190918" />
                  <Point X="3.076930419922" Y="-1.724287109375" />
                  <Point X="3.086932861328" Y="-1.737239013672" />
                  <Point X="3.110628417969" Y="-1.760909179688" />
                  <Point X="3.461515380859" Y="-2.030154296875" />
                  <Point X="4.213122558594" Y="-2.606883056641" />
                  <Point X="4.168916503906" Y="-2.678415283203" />
                  <Point X="4.124810058594" Y="-2.749785888672" />
                  <Point X="4.028981445312" Y="-2.885945068359" />
                  <Point X="3.753228271484" Y="-2.726738769531" />
                  <Point X="2.800954589844" Y="-2.176943115234" />
                  <Point X="2.786131591797" Y="-2.170012451172" />
                  <Point X="2.754224609375" Y="-2.159825195312" />
                  <Point X="2.600981689453" Y="-2.132149658203" />
                  <Point X="2.538134277344" Y="-2.120799560547" />
                  <Point X="2.506781982422" Y="-2.120395263672" />
                  <Point X="2.474609375" Y="-2.125353027344" />
                  <Point X="2.444833496094" Y="-2.135176757812" />
                  <Point X="2.317526123047" Y="-2.202177734375" />
                  <Point X="2.265315429688" Y="-2.229655761719" />
                  <Point X="2.242384765625" Y="-2.246548828125" />
                  <Point X="2.221424804688" Y="-2.267508789062" />
                  <Point X="2.204531738281" Y="-2.290439453125" />
                  <Point X="2.137530761719" Y="-2.417746826172" />
                  <Point X="2.110052734375" Y="-2.469957519531" />
                  <Point X="2.100229003906" Y="-2.499733398438" />
                  <Point X="2.095271240234" Y="-2.531906005859" />
                  <Point X="2.095675537109" Y="-2.563258300781" />
                  <Point X="2.123351074219" Y="-2.716501220703" />
                  <Point X="2.134701171875" Y="-2.779348632812" />
                  <Point X="2.138985595703" Y="-2.795141845703" />
                  <Point X="2.151819091797" Y="-2.826078613281" />
                  <Point X="2.377299316406" Y="-3.216621337891" />
                  <Point X="2.861283203125" Y="-4.054906494141" />
                  <Point X="2.834188720703" Y="-4.074259277344" />
                  <Point X="2.781845703125" Y="-4.111646484375" />
                  <Point X="2.701765136719" Y="-4.163481445312" />
                  <Point X="2.48521875" Y="-3.881272949219" />
                  <Point X="1.758546142578" Y="-2.934255126953" />
                  <Point X="1.747503540039" Y="-2.922178955078" />
                  <Point X="1.721924194336" Y="-2.900557373047" />
                  <Point X="1.57078527832" Y="-2.803388916016" />
                  <Point X="1.50880078125" Y="-2.763538574219" />
                  <Point X="1.479989746094" Y="-2.751167236328" />
                  <Point X="1.448368408203" Y="-2.743435546875" />
                  <Point X="1.417099487305" Y="-2.741116699219" />
                  <Point X="1.251803466797" Y="-2.756327392578" />
                  <Point X="1.184012573242" Y="-2.762565673828" />
                  <Point X="1.156362670898" Y="-2.769397460938" />
                  <Point X="1.128977661133" Y="-2.780740722656" />
                  <Point X="1.104595825195" Y="-2.795461181641" />
                  <Point X="0.976958557129" Y="-2.901587402344" />
                  <Point X="0.924612121582" Y="-2.945111572266" />
                  <Point X="0.904141052246" Y="-2.968861816406" />
                  <Point X="0.887248779297" Y="-2.996688476562" />
                  <Point X="0.875624328613" Y="-3.025808837891" />
                  <Point X="0.837461364746" Y="-3.201388671875" />
                  <Point X="0.821810058594" Y="-3.273396728516" />
                  <Point X="0.819724487305" Y="-3.289627441406" />
                  <Point X="0.819742248535" Y="-3.323120117188" />
                  <Point X="0.883641601562" Y="-3.808483398438" />
                  <Point X="1.022065307617" Y="-4.859915527344" />
                  <Point X="0.975678588867" Y="-4.870083007812" />
                  <Point X="0.929315612793" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058435546875" Y="-4.752634765625" />
                  <Point X="-1.141246459961" Y="-4.731328125" />
                  <Point X="-1.120775634766" Y="-4.575838378906" />
                  <Point X="-1.120077514648" Y="-4.568099609375" />
                  <Point X="-1.119451782227" Y="-4.541033691406" />
                  <Point X="-1.121759277344" Y="-4.509328125" />
                  <Point X="-1.123334106445" Y="-4.497690429688" />
                  <Point X="-1.1662578125" Y="-4.2818984375" />
                  <Point X="-1.183861450195" Y="-4.193398925781" />
                  <Point X="-1.188125" Y="-4.17846875" />
                  <Point X="-1.19902734375" Y="-4.149502441406" />
                  <Point X="-1.205666015625" Y="-4.135466308594" />
                  <Point X="-1.221738037109" Y="-4.10762890625" />
                  <Point X="-1.230573730469" Y="-4.094862304688" />
                  <Point X="-1.250207763672" Y="-4.0709375" />
                  <Point X="-1.261006591797" Y="-4.059779296875" />
                  <Point X="-1.426426269531" Y="-3.914709716797" />
                  <Point X="-1.494267578125" Y="-3.855214599609" />
                  <Point X="-1.506738769531" Y="-3.845965332031" />
                  <Point X="-1.533021850586" Y="-3.829621337891" />
                  <Point X="-1.546833618164" Y="-3.822526855469" />
                  <Point X="-1.576530883789" Y="-3.810225830078" />
                  <Point X="-1.591313476562" Y="-3.805476074219" />
                  <Point X="-1.621454833984" Y="-3.798447998047" />
                  <Point X="-1.636813964844" Y="-3.796169677734" />
                  <Point X="-1.856362792969" Y="-3.781779541016" />
                  <Point X="-1.946403198242" Y="-3.775878173828" />
                  <Point X="-1.961928222656" Y="-3.776132324219" />
                  <Point X="-1.992729736328" Y="-3.779166015625" />
                  <Point X="-2.008006225586" Y="-3.781945556641" />
                  <Point X="-2.03905480957" Y="-3.790265136719" />
                  <Point X="-2.053674316406" Y="-3.795496337891" />
                  <Point X="-2.081865478516" Y="-3.808269775391" />
                  <Point X="-2.095437011719" Y="-3.815812011719" />
                  <Point X="-2.278376953125" Y="-3.938048828125" />
                  <Point X="-2.353403564453" Y="-3.988180175781" />
                  <Point X="-2.359681640625" Y="-3.992757080078" />
                  <Point X="-2.380442138672" Y="-4.010131103516" />
                  <Point X="-2.402759277344" Y="-4.032771484375" />
                  <Point X="-2.410471435547" Y="-4.041629638672" />
                  <Point X="-2.475879638672" Y="-4.12687109375" />
                  <Point X="-2.503202880859" Y="-4.162479492188" />
                  <Point X="-2.630193115234" Y="-4.083850097656" />
                  <Point X="-2.747585205078" Y="-4.011163818359" />
                  <Point X="-2.980862792969" Y="-3.831547851562" />
                  <Point X="-2.869413330078" Y="-3.638511962891" />
                  <Point X="-2.341488769531" Y="-2.724119628906" />
                  <Point X="-2.334850097656" Y="-2.710083251953" />
                  <Point X="-2.323947998047" Y="-2.681116943359" />
                  <Point X="-2.319684570312" Y="-2.666187011719" />
                  <Point X="-2.313413574219" Y="-2.634660644531" />
                  <Point X="-2.311638916016" Y="-2.619235351562" />
                  <Point X="-2.310626220703" Y="-2.588301269531" />
                  <Point X="-2.314666259766" Y="-2.557615234375" />
                  <Point X="-2.323650878906" Y="-2.527997558594" />
                  <Point X="-2.329357421875" Y="-2.513557128906" />
                  <Point X="-2.343574462891" Y="-2.484728271484" />
                  <Point X="-2.351557128906" Y="-2.47141015625" />
                  <Point X="-2.369584716797" Y="-2.446253173828" />
                  <Point X="-2.379629638672" Y="-2.434414306641" />
                  <Point X="-2.396978271484" Y="-2.417065673828" />
                  <Point X="-2.408817138672" Y="-2.407020751953" />
                  <Point X="-2.433974121094" Y="-2.388993164062" />
                  <Point X="-2.447292236328" Y="-2.381010498047" />
                  <Point X="-2.47612109375" Y="-2.366793457031" />
                  <Point X="-2.490561523438" Y="-2.361086914062" />
                  <Point X="-2.520179199219" Y="-2.352102294922" />
                  <Point X="-2.550865234375" Y="-2.348062255859" />
                  <Point X="-2.581799316406" Y="-2.349074951172" />
                  <Point X="-2.597224609375" Y="-2.350849609375" />
                  <Point X="-2.628750976562" Y="-2.357120605469" />
                  <Point X="-2.643680908203" Y="-2.361384033203" />
                  <Point X="-2.672647216797" Y="-2.372286132812" />
                  <Point X="-2.68668359375" Y="-2.378924804688" />
                  <Point X="-3.061690673828" Y="-2.595435302734" />
                  <Point X="-3.793089599609" Y="-3.017708496094" />
                  <Point X="-3.911271484375" Y="-2.86244140625" />
                  <Point X="-4.004014160156" Y="-2.740596435547" />
                  <Point X="-4.181265136719" Y="-2.443373291016" />
                  <Point X="-3.971819335938" Y="-2.282659667969" />
                  <Point X="-3.048122314453" Y="-1.573882080078" />
                  <Point X="-3.036481689453" Y="-1.563309570312" />
                  <Point X="-3.015104003906" Y="-1.540388916016" />
                  <Point X="-3.005366943359" Y="-1.528040527344" />
                  <Point X="-2.987402099609" Y="-1.500908813477" />
                  <Point X="-2.979835449219" Y="-1.487124145508" />
                  <Point X="-2.967080078125" Y="-1.458495483398" />
                  <Point X="-2.961891357422" Y="-1.443651733398" />
                  <Point X="-2.956427490234" Y="-1.422555908203" />
                  <Point X="-2.951552978516" Y="-1.398803588867" />
                  <Point X="-2.948748535156" Y="-1.368374267578" />
                  <Point X="-2.948577880859" Y="-1.353045532227" />
                  <Point X="-2.950786865234" Y="-1.321375366211" />
                  <Point X="-2.953083740234" Y="-1.306219360352" />
                  <Point X="-2.960084960938" Y="-1.276474731445" />
                  <Point X="-2.971778808594" Y="-1.248243408203" />
                  <Point X="-2.987860839844" Y="-1.222260131836" />
                  <Point X="-2.996953369141" Y="-1.209919311523" />
                  <Point X="-3.017785644531" Y="-1.185963012695" />
                  <Point X="-3.028745361328" Y="-1.175244750977" />
                  <Point X="-3.052245605469" Y="-1.155710693359" />
                  <Point X="-3.064786621094" Y="-1.14689453125" />
                  <Point X="-3.083567138672" Y="-1.135841186523" />
                  <Point X="-3.105434082031" Y="-1.124481079102" />
                  <Point X="-3.134697265625" Y="-1.113257080078" />
                  <Point X="-3.149795166016" Y="-1.108860107422" />
                  <Point X="-3.181683105469" Y="-1.102378295898" />
                  <Point X="-3.197299560547" Y="-1.100532348633" />
                  <Point X="-3.228622558594" Y="-1.09944140625" />
                  <Point X="-3.244329101562" Y="-1.100196533203" />
                  <Point X="-3.717739990234" Y="-1.162522338867" />
                  <Point X="-4.660920898438" Y="-1.286694458008" />
                  <Point X="-4.704487304688" Y="-1.116132080078" />
                  <Point X="-4.740762207031" Y="-0.974117797852" />
                  <Point X="-4.786452636719" Y="-0.65465447998" />
                  <Point X="-4.560518066406" Y="-0.594115539551" />
                  <Point X="-3.508288085938" Y="-0.312171325684" />
                  <Point X="-3.500476318359" Y="-0.309712677002" />
                  <Point X="-3.473931884766" Y="-0.299251556396" />
                  <Point X="-3.444167236328" Y="-0.283893463135" />
                  <Point X="-3.433561523438" Y="-0.277513977051" />
                  <Point X="-3.413880126953" Y="-0.263854034424" />
                  <Point X="-3.393671386719" Y="-0.248242080688" />
                  <Point X="-3.371215820312" Y="-0.226357666016" />
                  <Point X="-3.360897460938" Y="-0.214483291626" />
                  <Point X="-3.341653564453" Y="-0.188227706909" />
                  <Point X="-3.333436523438" Y="-0.174813598633" />
                  <Point X="-3.319328613281" Y="-0.146813354492" />
                  <Point X="-3.313437744141" Y="-0.132227050781" />
                  <Point X="-3.306877197266" Y="-0.111089157104" />
                  <Point X="-3.300990722656" Y="-0.08850100708" />
                  <Point X="-3.296721191406" Y="-0.060341274261" />
                  <Point X="-3.295647705078" Y="-0.046100227356" />
                  <Point X="-3.295647705078" Y="-0.016459917068" />
                  <Point X="-3.296721191406" Y="-0.002219019413" />
                  <Point X="-3.300990722656" Y="0.025941007614" />
                  <Point X="-3.304186767578" Y="0.039860137939" />
                  <Point X="-3.310747314453" Y="0.060998027802" />
                  <Point X="-3.319328369141" Y="0.084252754211" />
                  <Point X="-3.333436035156" Y="0.112253013611" />
                  <Point X="-3.341653076172" Y="0.125667121887" />
                  <Point X="-3.360896972656" Y="0.151922851563" />
                  <Point X="-3.371215576172" Y="0.163797668457" />
                  <Point X="-3.393671386719" Y="0.185682235718" />
                  <Point X="-3.40580859375" Y="0.195691696167" />
                  <Point X="-3.425489990234" Y="0.209351623535" />
                  <Point X="-3.443233642578" Y="0.22027571106" />
                  <Point X="-3.466873535156" Y="0.233098449707" />
                  <Point X="-3.476983642578" Y="0.237835952759" />
                  <Point X="-3.497690917969" Y="0.246092391968" />
                  <Point X="-3.508288085938" Y="0.249611343384" />
                  <Point X="-3.939824462891" Y="0.365241149902" />
                  <Point X="-4.785446289062" Y="0.591824707031" />
                  <Point X="-4.754707519531" Y="0.79955279541" />
                  <Point X="-4.731330566406" Y="0.957532653809" />
                  <Point X="-4.633586425781" Y="1.318237304688" />
                  <Point X="-4.532330078125" Y="1.304906616211" />
                  <Point X="-3.778066162109" Y="1.20560559082" />
                  <Point X="-3.76773828125" Y="1.204815429688" />
                  <Point X="-3.747057861328" Y="1.204364257812" />
                  <Point X="-3.736705322266" Y="1.20470324707" />
                  <Point X="-3.715143554688" Y="1.20658984375" />
                  <Point X="-3.704889892578" Y="1.208053833008" />
                  <Point X="-3.684603271484" Y="1.212089233398" />
                  <Point X="-3.674570800781" Y="1.214660400391" />
                  <Point X="-3.631009765625" Y="1.228394897461" />
                  <Point X="-3.603451416016" Y="1.237676269531" />
                  <Point X="-3.584518554688" Y="1.246006713867" />
                  <Point X="-3.575278564453" Y="1.250689331055" />
                  <Point X="-3.556534423828" Y="1.261511108398" />
                  <Point X="-3.547859619141" Y="1.267171508789" />
                  <Point X="-3.531178222656" Y="1.279403076172" />
                  <Point X="-3.515927734375" Y="1.293377685547" />
                  <Point X="-3.502289306641" Y="1.30892956543" />
                  <Point X="-3.495894775391" Y="1.317077636719" />
                  <Point X="-3.48348046875" Y="1.334807373047" />
                  <Point X="-3.478010986328" Y="1.343603149414" />
                  <Point X="-3.468062255859" Y="1.361737426758" />
                  <Point X="-3.463583007813" Y="1.371076171875" />
                  <Point X="-3.446104003906" Y="1.413274291992" />
                  <Point X="-3.435499511719" Y="1.440351074219" />
                  <Point X="-3.429711425781" Y="1.460208984375" />
                  <Point X="-3.427359375" Y="1.470296020508" />
                  <Point X="-3.423600830078" Y="1.491610961914" />
                  <Point X="-3.422360839844" Y="1.501894042969" />
                  <Point X="-3.4210078125" Y="1.522534667969" />
                  <Point X="-3.42191015625" Y="1.543201049805" />
                  <Point X="-3.425056884766" Y="1.563645019531" />
                  <Point X="-3.427188232422" Y="1.573780639648" />
                  <Point X="-3.432790039062" Y="1.594686889648" />
                  <Point X="-3.436011962891" Y="1.604530029297" />
                  <Point X="-3.443508789062" Y="1.62380859375" />
                  <Point X="-3.447783691406" Y="1.633243530273" />
                  <Point X="-3.468874023438" Y="1.67375769043" />
                  <Point X="-3.482799560547" Y="1.699285644531" />
                  <Point X="-3.494291259766" Y="1.71648449707" />
                  <Point X="-3.500506835938" Y="1.724770629883" />
                  <Point X="-3.514419433594" Y="1.741351196289" />
                  <Point X="-3.521500244141" Y="1.748911010742" />
                  <Point X="-3.536442382812" Y="1.76321496582" />
                  <Point X="-3.544303710938" Y="1.769958984375" />
                  <Point X="-3.791834228516" Y="1.959895629883" />
                  <Point X="-4.227614257812" Y="2.294282226563" />
                  <Point X="-4.093123535156" Y="2.524697509766" />
                  <Point X="-4.002293212891" Y="2.680312011719" />
                  <Point X="-3.726338623047" Y="3.035012451172" />
                  <Point X="-3.717405029297" Y="3.029854736328" />
                  <Point X="-3.254156982422" Y="2.7623984375" />
                  <Point X="-3.244917480469" Y="2.757716064453" />
                  <Point X="-3.225984619141" Y="2.749385498047" />
                  <Point X="-3.216291259766" Y="2.745737304688" />
                  <Point X="-3.195648925781" Y="2.739228759766" />
                  <Point X="-3.185615234375" Y="2.736657226562" />
                  <Point X="-3.165327880859" Y="2.732621826172" />
                  <Point X="-3.15507421875" Y="2.731157958984" />
                  <Point X="-3.094405761719" Y="2.725850097656" />
                  <Point X="-3.069524902344" Y="2.723673339844" />
                  <Point X="-3.059173095703" Y="2.723334472656" />
                  <Point X="-3.038492919922" Y="2.723785644531" />
                  <Point X="-3.028164550781" Y="2.724575683594" />
                  <Point X="-3.006705810547" Y="2.727400878906" />
                  <Point X="-2.996524902344" Y="2.729310791016" />
                  <Point X="-2.976432861328" Y="2.734227539062" />
                  <Point X="-2.956996826172" Y="2.741302001953" />
                  <Point X="-2.938445068359" Y="2.750450683594" />
                  <Point X="-2.929418212891" Y="2.755531738281" />
                  <Point X="-2.9111640625" Y="2.767161132812" />
                  <Point X="-2.902745117188" Y="2.773194335938" />
                  <Point X="-2.886614257812" Y="2.786140869141" />
                  <Point X="-2.87890234375" Y="2.793054199219" />
                  <Point X="-2.835839355469" Y="2.836116943359" />
                  <Point X="-2.818178710938" Y="2.853777587891" />
                  <Point X="-2.811265136719" Y="2.861489501953" />
                  <Point X="-2.798318359375" Y="2.877620605469" />
                  <Point X="-2.79228515625" Y="2.886039794922" />
                  <Point X="-2.780655761719" Y="2.904293945312" />
                  <Point X="-2.775574707031" Y="2.913320800781" />
                  <Point X="-2.766426025391" Y="2.931872558594" />
                  <Point X="-2.7593515625" Y="2.95130859375" />
                  <Point X="-2.754434814453" Y="2.971400634766" />
                  <Point X="-2.752524902344" Y="2.981581542969" />
                  <Point X="-2.749699707031" Y="3.003040283203" />
                  <Point X="-2.748909667969" Y="3.013368652344" />
                  <Point X="-2.748458496094" Y="3.034048828125" />
                  <Point X="-2.748797363281" Y="3.044400634766" />
                  <Point X="-2.754105224609" Y="3.105069091797" />
                  <Point X="-2.756281982422" Y="3.129950195312" />
                  <Point X="-2.757745849609" Y="3.140203857422" />
                  <Point X="-2.76178125" Y="3.160491210938" />
                  <Point X="-2.764352783203" Y="3.170524902344" />
                  <Point X="-2.770861328125" Y="3.191167236328" />
                  <Point X="-2.774509521484" Y="3.200860595703" />
                  <Point X="-2.782840087891" Y="3.219793457031" />
                  <Point X="-2.787522460938" Y="3.229032958984" />
                  <Point X="-2.897210693359" Y="3.419018310547" />
                  <Point X="-3.059387207031" Y="3.699916259766" />
                  <Point X="-2.806565429688" Y="3.893752197266" />
                  <Point X="-2.648374755859" Y="4.015036132812" />
                  <Point X="-2.201272705078" Y="4.263436523438" />
                  <Point X="-2.192525390625" Y="4.268296386719" />
                  <Point X="-2.118563964844" Y="4.171908203125" />
                  <Point X="-2.111819580078" Y="4.164046386719" />
                  <Point X="-2.097516601562" Y="4.14910546875" />
                  <Point X="-2.089958007813" Y="4.142026367188" />
                  <Point X="-2.073377929688" Y="4.128113769531" />
                  <Point X="-2.065092041016" Y="4.121897949219" />
                  <Point X="-2.047892822266" Y="4.110405761719" />
                  <Point X="-2.038979736328" Y="4.105129394531" />
                  <Point X="-1.971456298828" Y="4.069978271484" />
                  <Point X="-1.943763549805" Y="4.055562255859" />
                  <Point X="-1.934329101562" Y="4.051287597656" />
                  <Point X="-1.915049438477" Y="4.043790039063" />
                  <Point X="-1.905204101562" Y="4.040567382812" />
                  <Point X="-1.884297729492" Y="4.034965820313" />
                  <Point X="-1.874162719727" Y="4.032834716797" />
                  <Point X="-1.853719604492" Y="4.029688232422" />
                  <Point X="-1.833053588867" Y="4.028785888672" />
                  <Point X="-1.8124140625" Y="4.030138916016" />
                  <Point X="-1.802132446289" Y="4.031378662109" />
                  <Point X="-1.780817260742" Y="4.035136962891" />
                  <Point X="-1.770731323242" Y="4.037488525391" />
                  <Point X="-1.750871826172" Y="4.043276611328" />
                  <Point X="-1.741098266602" Y="4.046713623047" />
                  <Point X="-1.670767944336" Y="4.075845947266" />
                  <Point X="-1.641924316406" Y="4.087793212891" />
                  <Point X="-1.632584716797" Y="4.092273193359" />
                  <Point X="-1.614450195312" Y="4.102222167969" />
                  <Point X="-1.605655517578" Y="4.10769140625" />
                  <Point X="-1.58792565918" Y="4.120105957031" />
                  <Point X="-1.579777954102" Y="4.126500488281" />
                  <Point X="-1.56422644043" Y="4.140138671875" />
                  <Point X="-1.550252441406" Y="4.155388671875" />
                  <Point X="-1.538021362305" Y="4.172069335938" />
                  <Point X="-1.532361083984" Y="4.180743652344" />
                  <Point X="-1.52153918457" Y="4.199487792969" />
                  <Point X="-1.516856811523" Y="4.208727050781" />
                  <Point X="-1.508526000977" Y="4.22766015625" />
                  <Point X="-1.504877441406" Y="4.237354003906" />
                  <Point X="-1.481986083984" Y="4.309955566406" />
                  <Point X="-1.472598144531" Y="4.33973046875" />
                  <Point X="-1.470026733398" Y="4.349763671875" />
                  <Point X="-1.465991210938" Y="4.37005078125" />
                  <Point X="-1.46452722168" Y="4.3803046875" />
                  <Point X="-1.46264074707" Y="4.401866210938" />
                  <Point X="-1.462301757812" Y="4.41221875" />
                  <Point X="-1.462752929688" Y="4.432898925781" />
                  <Point X="-1.46354309082" Y="4.4432265625" />
                  <Point X="-1.476013549805" Y="4.537948242188" />
                  <Point X="-1.479266357422" Y="4.562655761719" />
                  <Point X="-1.135963500977" Y="4.65890625" />
                  <Point X="-0.931181884766" Y="4.716319824219" />
                  <Point X="-0.389162231445" Y="4.779754882812" />
                  <Point X="-0.365222106934" Y="4.782556640625" />
                  <Point X="-0.362707946777" Y="4.773173828125" />
                  <Point X="-0.22566633606" Y="4.261727539062" />
                  <Point X="-0.220435256958" Y="4.247107910156" />
                  <Point X="-0.207661849976" Y="4.218916503906" />
                  <Point X="-0.200119247437" Y="4.205344726562" />
                  <Point X="-0.182260864258" Y="4.178618164062" />
                  <Point X="-0.17260824585" Y="4.166455566406" />
                  <Point X="-0.151451187134" Y="4.143866699219" />
                  <Point X="-0.126897941589" Y="4.125026367188" />
                  <Point X="-0.099602897644" Y="4.110436523438" />
                  <Point X="-0.085356651306" Y="4.104260742188" />
                  <Point X="-0.054918682098" Y="4.093927978516" />
                  <Point X="-0.039857234955" Y="4.090155273438" />
                  <Point X="-0.009319986343" Y="4.08511328125" />
                  <Point X="0.021631772995" Y="4.08511328125" />
                  <Point X="0.052169025421" Y="4.090155273438" />
                  <Point X="0.06723046875" Y="4.093927978516" />
                  <Point X="0.097668441772" Y="4.104260742188" />
                  <Point X="0.111914535522" Y="4.110436523438" />
                  <Point X="0.139209732056" Y="4.125026367188" />
                  <Point X="0.163763122559" Y="4.143866699219" />
                  <Point X="0.184920181274" Y="4.166455566406" />
                  <Point X="0.194572952271" Y="4.178617675781" />
                  <Point X="0.212431182861" Y="4.205344238281" />
                  <Point X="0.2199737854" Y="4.218916503906" />
                  <Point X="0.232747192383" Y="4.247107910156" />
                  <Point X="0.237978271484" Y="4.261727539062" />
                  <Point X="0.294181213379" Y="4.471479980469" />
                  <Point X="0.378190338135" Y="4.785006347656" />
                  <Point X="0.649054504395" Y="4.756639648438" />
                  <Point X="0.827876586914" Y="4.737912109375" />
                  <Point X="1.276308105469" Y="4.629646972656" />
                  <Point X="1.453596313477" Y="4.58684375" />
                  <Point X="1.744080566406" Y="4.481482910156" />
                  <Point X="1.858258056641" Y="4.440069824219" />
                  <Point X="2.140500244141" Y="4.308074707031" />
                  <Point X="2.250453125" Y="4.256653320312" />
                  <Point X="2.523161132812" Y="4.097772949219" />
                  <Point X="2.629435791016" Y="4.035856933594" />
                  <Point X="2.817780029297" Y="3.901916992188" />
                  <Point X="2.676341796875" Y="3.656938476562" />
                  <Point X="2.06530859375" Y="2.598597900391" />
                  <Point X="2.062373291016" Y="2.59310546875" />
                  <Point X="2.053181884766" Y="2.573439941406" />
                  <Point X="2.044182250977" Y="2.549564941406" />
                  <Point X="2.041301391602" Y="2.540598632812" />
                  <Point X="2.026076049805" Y="2.483662597656" />
                  <Point X="2.019831665039" Y="2.460312255859" />
                  <Point X="2.017912231445" Y="2.451464599609" />
                  <Point X="2.013646972656" Y="2.420223632812" />
                  <Point X="2.012755615234" Y="2.3832421875" />
                  <Point X="2.013411254883" Y="2.369580078125" />
                  <Point X="2.019348022461" Y="2.320346679688" />
                  <Point X="2.021782592773" Y="2.300155273438" />
                  <Point X="2.02380078125" Y="2.28903515625" />
                  <Point X="2.029143066406" Y="2.267112548828" />
                  <Point X="2.032467773437" Y="2.256310058594" />
                  <Point X="2.040733886719" Y="2.234220947266" />
                  <Point X="2.045317871094" Y="2.223889648438" />
                  <Point X="2.055680664062" Y="2.203844238281" />
                  <Point X="2.061459472656" Y="2.194130126953" />
                  <Point X="2.091923339844" Y="2.149234130859" />
                  <Point X="2.104417236328" Y="2.130821533203" />
                  <Point X="2.10980859375" Y="2.123633056641" />
                  <Point X="2.130463134766" Y="2.100123291016" />
                  <Point X="2.157597167969" Y="2.075386962891" />
                  <Point X="2.1682578125" Y="2.066980957031" />
                  <Point X="2.213153808594" Y="2.036517211914" />
                  <Point X="2.23156640625" Y="2.02402355957" />
                  <Point X="2.241279785156" Y="2.018244995117" />
                  <Point X="2.261324707031" Y="2.007882202148" />
                  <Point X="2.27165625" Y="2.003297973633" />
                  <Point X="2.293744384766" Y="1.995032104492" />
                  <Point X="2.304547851562" Y="1.991707275391" />
                  <Point X="2.326470947266" Y="1.986364746094" />
                  <Point X="2.337590576172" Y="1.984346801758" />
                  <Point X="2.386823974609" Y="1.97841003418" />
                  <Point X="2.407015380859" Y="1.975975219727" />
                  <Point X="2.416043945312" Y="1.975320922852" />
                  <Point X="2.447575195312" Y="1.975497314453" />
                  <Point X="2.484314453125" Y="1.979822875977" />
                  <Point X="2.497748779297" Y="1.982395996094" />
                  <Point X="2.554684814453" Y="1.997621582031" />
                  <Point X="2.57803515625" Y="2.003865722656" />
                  <Point X="2.583995361328" Y="2.005670776367" />
                  <Point X="2.604404541016" Y="2.013067260742" />
                  <Point X="2.627655029297" Y="2.02357409668" />
                  <Point X="2.636033935547" Y="2.027872558594" />
                  <Point X="3.070076416016" Y="2.278467041016" />
                  <Point X="3.940404785156" Y="2.780951416016" />
                  <Point X="3.9809140625" Y="2.724652832031" />
                  <Point X="4.043950683594" Y="2.637046142578" />
                  <Point X="4.136884765625" Y="2.483471923828" />
                  <Point X="3.977433837891" Y="2.361120849609" />
                  <Point X="3.172951416016" Y="1.743819824219" />
                  <Point X="3.168137939453" Y="1.739868896484" />
                  <Point X="3.152119628906" Y="1.725217285156" />
                  <Point X="3.134668701172" Y="1.706603515625" />
                  <Point X="3.128576171875" Y="1.699422485352" />
                  <Point X="3.087599121094" Y="1.64596484375" />
                  <Point X="3.070793945312" Y="1.624041137695" />
                  <Point X="3.065635253906" Y="1.616602172852" />
                  <Point X="3.049737548828" Y="1.589369262695" />
                  <Point X="3.034762451172" Y="1.555544677734" />
                  <Point X="3.030140136719" Y="1.54267199707" />
                  <Point X="3.014876220703" Y="1.488091674805" />
                  <Point X="3.008616210938" Y="1.465707641602" />
                  <Point X="3.006225097656" Y="1.454661010742" />
                  <Point X="3.002771728516" Y="1.432362548828" />
                  <Point X="3.001709472656" Y="1.421110717773" />
                  <Point X="3.000893310547" Y="1.397540039063" />
                  <Point X="3.001174804688" Y="1.386240478516" />
                  <Point X="3.003077880859" Y="1.363755981445" />
                  <Point X="3.004699462891" Y="1.352570922852" />
                  <Point X="3.017229492188" Y="1.291843261719" />
                  <Point X="3.022368408203" Y="1.266937866211" />
                  <Point X="3.024597900391" Y="1.258234130859" />
                  <Point X="3.034684082031" Y="1.228607788086" />
                  <Point X="3.050286376953" Y="1.19537121582" />
                  <Point X="3.056918457031" Y="1.183526000977" />
                  <Point X="3.090999023438" Y="1.131724853516" />
                  <Point X="3.104976074219" Y="1.110480712891" />
                  <Point X="3.111739257813" Y="1.101424194336" />
                  <Point X="3.126292724609" Y="1.08417956543" />
                  <Point X="3.134083007812" Y="1.075991210938" />
                  <Point X="3.151326904297" Y="1.059901367188" />
                  <Point X="3.160034912109" Y="1.052695678711" />
                  <Point X="3.178244628906" Y="1.039369995117" />
                  <Point X="3.18774609375" Y="1.03325" />
                  <Point X="3.237133544922" Y="1.005449035645" />
                  <Point X="3.257388183594" Y="0.994047546387" />
                  <Point X="3.265479492188" Y="0.989987609863" />
                  <Point X="3.294678466797" Y="0.978084106445" />
                  <Point X="3.330275634766" Y="0.968021179199" />
                  <Point X="3.343671386719" Y="0.965257507324" />
                  <Point X="3.410446533203" Y="0.956432434082" />
                  <Point X="3.43783203125" Y="0.952812988281" />
                  <Point X="3.444030029297" Y="0.952199829102" />
                  <Point X="3.465716064453" Y="0.951222900391" />
                  <Point X="3.491217529297" Y="0.952032287598" />
                  <Point X="3.500603515625" Y="0.952797241211" />
                  <Point X="3.912915039063" Y="1.007079101562" />
                  <Point X="4.704703613281" Y="1.111320068359" />
                  <Point X="4.725609375" Y="1.025445068359" />
                  <Point X="4.75268359375" Y="0.914233642578" />
                  <Point X="4.783871582031" Y="0.713921020508" />
                  <Point X="4.613946289062" Y="0.668389648438" />
                  <Point X="3.691991943359" Y="0.421352844238" />
                  <Point X="3.686031738281" Y="0.419544525146" />
                  <Point X="3.665626708984" Y="0.412138061523" />
                  <Point X="3.642381103516" Y="0.401619415283" />
                  <Point X="3.634004394531" Y="0.397316650391" />
                  <Point X="3.568399902344" Y="0.359395965576" />
                  <Point X="3.541494384766" Y="0.343844055176" />
                  <Point X="3.533882324219" Y="0.338946044922" />
                  <Point X="3.508774658203" Y="0.319871032715" />
                  <Point X="3.481993896484" Y="0.294350982666" />
                  <Point X="3.472796630859" Y="0.284226501465" />
                  <Point X="3.433434082031" Y="0.234068954468" />
                  <Point X="3.417290771484" Y="0.213498794556" />
                  <Point X="3.410854003906" Y="0.204207794189" />
                  <Point X="3.399130126953" Y="0.184927825928" />
                  <Point X="3.393843017578" Y="0.174939178467" />
                  <Point X="3.384069091797" Y="0.153475509644" />
                  <Point X="3.380005126953" Y="0.142928634644" />
                  <Point X="3.373158935547" Y="0.121427658081" />
                  <Point X="3.370376708984" Y="0.110473403931" />
                  <Point X="3.357255615234" Y="0.041960922241" />
                  <Point X="3.351874511719" Y="0.013862870216" />
                  <Point X="3.350603759766" Y="0.004969127178" />
                  <Point X="3.348584228516" Y="-0.026262191772" />
                  <Point X="3.350279785156" Y="-0.062940540314" />
                  <Point X="3.351874023438" Y="-0.076422721863" />
                  <Point X="3.364995117188" Y="-0.144935195923" />
                  <Point X="3.370376220703" Y="-0.173033248901" />
                  <Point X="3.373159179688" Y="-0.183988540649" />
                  <Point X="3.380005615234" Y="-0.205489517212" />
                  <Point X="3.384069091797" Y="-0.216035202026" />
                  <Point X="3.393843017578" Y="-0.237499176025" />
                  <Point X="3.399130126953" Y="-0.247487976074" />
                  <Point X="3.410854003906" Y="-0.266767944336" />
                  <Point X="3.417290771484" Y="-0.276059082031" />
                  <Point X="3.456653320313" Y="-0.326216339111" />
                  <Point X="3.472796630859" Y="-0.346786499023" />
                  <Point X="3.478718017578" Y="-0.353633270264" />
                  <Point X="3.501139404297" Y="-0.375805267334" />
                  <Point X="3.530176025391" Y="-0.398724700928" />
                  <Point X="3.541494384766" Y="-0.406404205322" />
                  <Point X="3.607098876953" Y="-0.444324737549" />
                  <Point X="3.634004394531" Y="-0.459876800537" />
                  <Point X="3.639496582031" Y="-0.46281552124" />
                  <Point X="3.659158447266" Y="-0.472016906738" />
                  <Point X="3.683028076172" Y="-0.481027770996" />
                  <Point X="3.691991943359" Y="-0.483912963867" />
                  <Point X="4.070100097656" Y="-0.58522668457" />
                  <Point X="4.784876953125" Y="-0.776750488281" />
                  <Point X="4.77673046875" Y="-0.830782775879" />
                  <Point X="4.76161328125" Y="-0.931052612305" />
                  <Point X="4.727801757812" Y="-1.079219604492" />
                  <Point X="4.505989746094" Y="-1.050017578125" />
                  <Point X="3.436781982422" Y="-0.909253723145" />
                  <Point X="3.428624511719" Y="-0.908535888672" />
                  <Point X="3.400098388672" Y="-0.908042480469" />
                  <Point X="3.366721435547" Y="-0.910840881348" />
                  <Point X="3.354481201172" Y="-0.912676208496" />
                  <Point X="3.225722900391" Y="-0.940662353516" />
                  <Point X="3.172916992188" Y="-0.952139892578" />
                  <Point X="3.157874023438" Y="-0.956742492676" />
                  <Point X="3.128753662109" Y="-0.968367004395" />
                  <Point X="3.114676269531" Y="-0.975388977051" />
                  <Point X="3.086849609375" Y="-0.992281311035" />
                  <Point X="3.074124023438" Y="-1.001530273438" />
                  <Point X="3.050374023438" Y="-1.022001159668" />
                  <Point X="3.039349609375" Y="-1.033222900391" />
                  <Point X="2.961523193359" Y="-1.126823852539" />
                  <Point X="2.92960546875" Y="-1.165211181641" />
                  <Point X="2.921326416016" Y="-1.176847167969" />
                  <Point X="2.906605712891" Y="-1.201229248047" />
                  <Point X="2.9001640625" Y="-1.213975585938" />
                  <Point X="2.888820800781" Y="-1.241360717773" />
                  <Point X="2.884362792969" Y="-1.254927978516" />
                  <Point X="2.877531005859" Y="-1.282577758789" />
                  <Point X="2.875157226562" Y="-1.29666027832" />
                  <Point X="2.864002929688" Y="-1.417877319336" />
                  <Point X="2.859428222656" Y="-1.467590576172" />
                  <Point X="2.859288574219" Y="-1.483320800781" />
                  <Point X="2.861607177734" Y="-1.514588989258" />
                  <Point X="2.864065429688" Y="-1.530126953125" />
                  <Point X="2.871796875" Y="-1.561748046875" />
                  <Point X="2.876785888672" Y="-1.576667724609" />
                  <Point X="2.889157470703" Y="-1.605479614258" />
                  <Point X="2.896540039062" Y="-1.619371704102" />
                  <Point X="2.967796875" Y="-1.730206787109" />
                  <Point X="2.997020263672" Y="-1.775661987305" />
                  <Point X="3.001741943359" Y="-1.782353393555" />
                  <Point X="3.019793701172" Y="-1.804450195312" />
                  <Point X="3.043489257812" Y="-1.828120361328" />
                  <Point X="3.052796142578" Y="-1.836277587891" />
                  <Point X="3.403683105469" Y="-2.105522949219" />
                  <Point X="4.087170898438" Y="-2.629981933594" />
                  <Point X="4.045489013672" Y="-2.697429199219" />
                  <Point X="4.001274414063" Y="-2.760251708984" />
                  <Point X="3.800728271484" Y="-2.644466308594" />
                  <Point X="2.848454589844" Y="-2.094670654297" />
                  <Point X="2.841191894531" Y="-2.090885253906" />
                  <Point X="2.815026123047" Y="-2.079513183594" />
                  <Point X="2.783119140625" Y="-2.069325927734" />
                  <Point X="2.771108398438" Y="-2.066337646484" />
                  <Point X="2.617865478516" Y="-2.038662109375" />
                  <Point X="2.555018066406" Y="-2.027311889648" />
                  <Point X="2.539359130859" Y="-2.025807495117" />
                  <Point X="2.508006835938" Y="-2.025403198242" />
                  <Point X="2.492313476562" Y="-2.026503540039" />
                  <Point X="2.460140869141" Y="-2.031461303711" />
                  <Point X="2.444844726562" Y="-2.035136230469" />
                  <Point X="2.415068847656" Y="-2.044959960938" />
                  <Point X="2.400589111328" Y="-2.051108642578" />
                  <Point X="2.273281738281" Y="-2.118109619141" />
                  <Point X="2.221071044922" Y="-2.145587646484" />
                  <Point X="2.208968505859" Y="-2.153170410156" />
                  <Point X="2.186037841797" Y="-2.170063476562" />
                  <Point X="2.175209716797" Y="-2.179373779297" />
                  <Point X="2.154249755859" Y="-2.200333740234" />
                  <Point X="2.144939453125" Y="-2.211161865234" />
                  <Point X="2.128046386719" Y="-2.234092529297" />
                  <Point X="2.120463623047" Y="-2.246195068359" />
                  <Point X="2.053462646484" Y="-2.373502441406" />
                  <Point X="2.025984741211" Y="-2.425713134766" />
                  <Point X="2.0198359375" Y="-2.440192871094" />
                  <Point X="2.010012207031" Y="-2.46996875" />
                  <Point X="2.006337280273" Y="-2.485264892578" />
                  <Point X="2.001379516602" Y="-2.5174375" />
                  <Point X="2.000279174805" Y="-2.533130859375" />
                  <Point X="2.00068347168" Y="-2.564483154297" />
                  <Point X="2.002187866211" Y="-2.580142089844" />
                  <Point X="2.02986340332" Y="-2.733385009766" />
                  <Point X="2.041213500977" Y="-2.796232421875" />
                  <Point X="2.043015014648" Y="-2.804221435547" />
                  <Point X="2.051236083984" Y="-2.83154296875" />
                  <Point X="2.064069580078" Y="-2.862479736328" />
                  <Point X="2.069546630859" Y="-2.873578613281" />
                  <Point X="2.295026855469" Y="-3.264121337891" />
                  <Point X="2.735892822266" Y="-4.027724365234" />
                  <Point X="2.723754150391" Y="-4.036083251953" />
                  <Point X="2.560587402344" Y="-3.823440673828" />
                  <Point X="1.833914550781" Y="-2.876422851562" />
                  <Point X="1.828654418945" Y="-2.870147216797" />
                  <Point X="1.808830932617" Y="-2.849625732422" />
                  <Point X="1.783251586914" Y="-2.828004150391" />
                  <Point X="1.773299072266" Y="-2.820647216797" />
                  <Point X="1.62216015625" Y="-2.723478759766" />
                  <Point X="1.56017565918" Y="-2.683628417969" />
                  <Point X="1.546283935547" Y="-2.676245849609" />
                  <Point X="1.517473022461" Y="-2.663874511719" />
                  <Point X="1.502553344727" Y="-2.658885742188" />
                  <Point X="1.470932006836" Y="-2.651154052734" />
                  <Point X="1.455394165039" Y="-2.648695800781" />
                  <Point X="1.424125244141" Y="-2.646376953125" />
                  <Point X="1.408394287109" Y="-2.646516357422" />
                  <Point X="1.243098266602" Y="-2.661727050781" />
                  <Point X="1.175307373047" Y="-2.667965332031" />
                  <Point X="1.161225097656" Y="-2.670339111328" />
                  <Point X="1.133575317383" Y="-2.677170898438" />
                  <Point X="1.120007568359" Y="-2.68162890625" />
                  <Point X="1.092622680664" Y="-2.692972167969" />
                  <Point X="1.079876708984" Y="-2.699413574219" />
                  <Point X="1.055494873047" Y="-2.714134033203" />
                  <Point X="1.043858764648" Y="-2.722413085938" />
                  <Point X="0.916221557617" Y="-2.828539306641" />
                  <Point X="0.863875061035" Y="-2.872063476562" />
                  <Point X="0.852653320312" Y="-2.883087890625" />
                  <Point X="0.832182128906" Y="-2.906838134766" />
                  <Point X="0.822933044434" Y="-2.919564208984" />
                  <Point X="0.806040710449" Y="-2.947390869141" />
                  <Point X="0.799018737793" Y="-2.961468261719" />
                  <Point X="0.787394348145" Y="-2.990588623047" />
                  <Point X="0.782791931152" Y="-3.005631347656" />
                  <Point X="0.744628845215" Y="-3.181211181641" />
                  <Point X="0.728977661133" Y="-3.253219238281" />
                  <Point X="0.727584777832" Y="-3.261289306641" />
                  <Point X="0.724724487305" Y="-3.289677734375" />
                  <Point X="0.7247421875" Y="-3.323170410156" />
                  <Point X="0.725554992676" Y="-3.335520019531" />
                  <Point X="0.789454406738" Y="-3.820883300781" />
                  <Point X="0.833091552734" Y="-4.152340820313" />
                  <Point X="0.655064941406" Y="-3.487936523438" />
                  <Point X="0.652606262207" Y="-3.480124511719" />
                  <Point X="0.642145019531" Y="-3.453580322266" />
                  <Point X="0.62678692627" Y="-3.423815673828" />
                  <Point X="0.620407409668" Y="-3.413210205078" />
                  <Point X="0.504298309326" Y="-3.245918701172" />
                  <Point X="0.456679992676" Y="-3.177309814453" />
                  <Point X="0.446670837402" Y="-3.165172851562" />
                  <Point X="0.424786712646" Y="-3.142717285156" />
                  <Point X="0.412911743164" Y="-3.132398681641" />
                  <Point X="0.386656311035" Y="-3.113154785156" />
                  <Point X="0.373242340088" Y="-3.104937744141" />
                  <Point X="0.345241485596" Y="-3.090829589844" />
                  <Point X="0.330654754639" Y="-3.084938476562" />
                  <Point X="0.150982299805" Y="-3.029174560547" />
                  <Point X="0.077295654297" Y="-3.006305175781" />
                  <Point X="0.063376529694" Y="-3.003109130859" />
                  <Point X="0.035216945648" Y="-2.99883984375" />
                  <Point X="0.020976644516" Y="-2.997766601562" />
                  <Point X="-0.008664708138" Y="-2.997766601562" />
                  <Point X="-0.022905010223" Y="-2.99883984375" />
                  <Point X="-0.051064441681" Y="-3.003109130859" />
                  <Point X="-0.064983718872" Y="-3.006305175781" />
                  <Point X="-0.244656173706" Y="-3.062068847656" />
                  <Point X="-0.318342803955" Y="-3.084938476562" />
                  <Point X="-0.332929718018" Y="-3.090829589844" />
                  <Point X="-0.360930999756" Y="-3.104937988281" />
                  <Point X="-0.374345275879" Y="-3.113155273438" />
                  <Point X="-0.400600524902" Y="-3.132399169922" />
                  <Point X="-0.412475219727" Y="-3.142717773438" />
                  <Point X="-0.434358886719" Y="-3.165173095703" />
                  <Point X="-0.444367889404" Y="-3.177309814453" />
                  <Point X="-0.560477294922" Y="-3.344601074219" />
                  <Point X="-0.60809564209" Y="-3.413210205078" />
                  <Point X="-0.612470336914" Y="-3.420132324219" />
                  <Point X="-0.625976745605" Y="-3.445265136719" />
                  <Point X="-0.638777832031" Y="-3.476215820312" />
                  <Point X="-0.642752990723" Y="-3.487936523438" />
                  <Point X="-0.759257324219" Y="-3.922737060547" />
                  <Point X="-0.985425109863" Y="-4.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.361492560775" Y="4.191961465814" />
                  <Point X="0.611481429258" Y="4.760574551071" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.709297114077" Y="3.979064004704" />
                  <Point X="0.366230716804" Y="4.74037242676" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.789588048633" Y="3.853086987335" />
                  <Point X="0.34160916781" Y="4.648483541677" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.741026922277" Y="3.768976542457" />
                  <Point X="0.316987618816" Y="4.556594656594" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.363960433168" Y="4.777848090795" />
                  <Point X="-0.374948528086" Y="4.781418339258" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.692465795921" Y="3.684866097579" />
                  <Point X="0.292366071226" Y="4.464705771055" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.334642810504" Y="4.668433306443" />
                  <Point X="-0.60096499344" Y="4.754966629222" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.643904635498" Y="3.60075566377" />
                  <Point X="0.267744541282" Y="4.372816879783" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.305325211467" Y="4.559018529766" />
                  <Point X="-0.826981371048" Y="4.728514890676" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.59534345814" Y="3.516645235463" />
                  <Point X="0.243123011338" Y="4.28092798851" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.276007612429" Y="4.44960375309" />
                  <Point X="-1.020126898858" Y="4.691382765598" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.546782280782" Y="3.432534807156" />
                  <Point X="0.204562582629" Y="4.193568119991" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.246690013391" Y="4.340188976414" />
                  <Point X="-1.185154811194" Y="4.645114673441" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.498221103424" Y="3.348424378849" />
                  <Point X="0.127443085027" Y="4.118736852422" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.212285649643" Y="4.229121409693" />
                  <Point X="-1.350182487997" Y="4.598846504755" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.941353117626" Y="2.779633451868" />
                  <Point X="3.939285572265" Y="2.780305238079" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.449659926066" Y="3.264313950542" />
                  <Point X="-1.476274089671" Y="4.539927238366" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.035158472805" Y="2.649265333056" />
                  <Point X="3.828577128112" Y="2.716387680821" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.401098748708" Y="3.180203522236" />
                  <Point X="-1.462968389074" Y="4.435715042867" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.112139408168" Y="2.524363799619" />
                  <Point X="3.717868683958" Y="2.652470123563" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.35253757135" Y="3.096093093929" />
                  <Point X="-1.472819918567" Y="4.339027087535" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.075509197283" Y="2.43637676531" />
                  <Point X="3.607160239805" Y="2.588552566305" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.303976393993" Y="3.011982665622" />
                  <Point X="-1.501388144736" Y="4.248420555601" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.984056533483" Y="2.366202625749" />
                  <Point X="3.496451795651" Y="2.524635009047" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.255415216635" Y="2.927872237315" />
                  <Point X="-1.544911128389" Y="4.162673118926" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.892603828927" Y="2.296028499431" />
                  <Point X="3.385743351498" Y="2.460717451789" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.206854039277" Y="2.843761809008" />
                  <Point X="-1.633830199718" Y="4.091675765269" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.801151121188" Y="2.225854374146" />
                  <Point X="3.275034907344" Y="2.396799894531" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.158292861919" Y="2.759651380702" />
                  <Point X="-1.772917061495" Y="4.036978914845" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.086001139541" Y="4.138706098379" />
                  <Point X="-2.300394316386" Y="4.208366664274" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.709698413449" Y="2.155680248862" />
                  <Point X="3.164326463191" Y="2.332882337274" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.109731684561" Y="2.675540952395" />
                  <Point X="-2.413840187559" Y="4.145338550972" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.618245705711" Y="2.085506123577" />
                  <Point X="3.053618015304" Y="2.268964781229" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.061535079094" Y="2.5913120675" />
                  <Point X="-2.527286058733" Y="4.08231043767" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.526792997972" Y="2.015331998293" />
                  <Point X="2.942909546036" Y="2.205047232131" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.030820054135" Y="2.501403072777" />
                  <Point X="-2.640731929906" Y="4.019282324368" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.435340290233" Y="1.945157873008" />
                  <Point X="2.832201076768" Y="2.141129683033" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.013332971286" Y="2.407196059121" />
                  <Point X="-2.733715718597" Y="3.949605677441" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.343887582495" Y="1.874983747724" />
                  <Point X="2.7214926075" Y="2.077212133936" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.021229626219" Y="2.304741369097" />
                  <Point X="-2.825221479128" Y="3.879448790054" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.252434874756" Y="1.804809622439" />
                  <Point X="2.607365573675" Y="2.014405343796" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.063504289231" Y="2.19111658713" />
                  <Point X="-2.916727544327" Y="3.809292001659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.16204044103" Y="1.734291643084" />
                  <Point X="2.42015776653" Y="1.975343936323" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.19594217971" Y="2.048195996683" />
                  <Point X="-3.008233609525" Y="3.739135213264" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.095347129152" Y="1.656072702417" />
                  <Point X="-3.028082982107" Y="3.645695754071" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.725335924335" Y="1.026568326921" />
                  <Point X="4.533760456439" Y="1.088814969755" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.042628970472" Y="1.573312959219" />
                  <Point X="-2.957095342176" Y="3.522741560366" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.751742480869" Y="0.918099405291" />
                  <Point X="4.314980368256" Y="1.060012018247" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.013427685497" Y="1.48291212056" />
                  <Point X="-2.886107685578" Y="3.399787361245" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.768480843743" Y="0.812771870207" />
                  <Point X="4.096200280073" Y="1.031209066738" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.00115561832" Y="1.387010645597" />
                  <Point X="-2.815119939084" Y="3.276833132915" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.773685832523" Y="0.711191755532" />
                  <Point X="3.877420193078" Y="1.002406114844" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.019428811773" Y="1.281184413828" />
                  <Point X="-2.761594584661" Y="3.159552779713" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.605201891286" Y="0.666046595236" />
                  <Point X="3.658640112218" Y="0.973603160956" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.0689779708" Y="1.165196004826" />
                  <Point X="-2.749797377602" Y="3.055830723476" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.436717872624" Y="0.620901460097" />
                  <Point X="3.399680727719" Y="0.957855254202" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.215196188951" Y="1.017797914498" />
                  <Point X="-2.757597557904" Y="2.958476244388" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.268233853963" Y="0.575756324958" />
                  <Point X="-2.802012022218" Y="2.873018467339" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.099749835301" Y="0.530611189819" />
                  <Point X="-2.875084332445" Y="2.796872188878" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.93126581664" Y="0.48546605468" />
                  <Point X="-2.984006411287" Y="2.732374206345" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.482944850796" Y="2.89448913255" />
                  <Point X="-3.764493463205" Y="2.985969822169" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.762781797978" Y="0.440320919541" />
                  <Point X="-3.826525565861" Y="2.906236362817" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.617345105501" Y="0.387687254179" />
                  <Point X="-3.888557668516" Y="2.826502903466" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.511728599032" Y="0.322115226075" />
                  <Point X="-3.950589771172" Y="2.746769444115" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.441936513062" Y="0.244903138145" />
                  <Point X="-4.010453505708" Y="2.666331439253" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.388171137774" Y="0.162483656249" />
                  <Point X="-4.059462774657" Y="2.58236660473" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.362786857986" Y="0.070842597424" />
                  <Point X="-4.108472051248" Y="2.49840177269" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.348700165392" Y="-0.024469269999" />
                  <Point X="-4.157481344597" Y="2.414436946095" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.361874102027" Y="-0.128638652792" />
                  <Point X="-4.206490637945" Y="2.3304721195" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.394748003164" Y="-0.239208942066" />
                  <Point X="-4.099145994016" Y="2.195704819099" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.78286076592" Y="-0.79012303058" />
                  <Point X="4.538651036085" Y="-0.710774479345" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.49791818552" Y="-0.37261987768" />
                  <Point X="-3.873361996703" Y="2.022454239975" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.768504065259" Y="-0.885347167065" />
                  <Point X="-3.647577121543" Y="1.849203375622" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.750577844096" Y="-0.979411496033" />
                  <Point X="-3.480471934716" Y="1.695018697777" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.729356781368" Y="-1.07240526608" />
                  <Point X="-3.428372870601" Y="1.578201774388" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.248831541617" Y="-1.016162062451" />
                  <Point X="-3.426077260037" Y="1.477566973998" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.731988003134" Y="-0.948118328229" />
                  <Point X="-3.456723823891" Y="1.387635734913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.331113604165" Y="-0.917755251592" />
                  <Point X="-3.506641981649" Y="1.303966216266" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.151503817085" Y="-0.959285405436" />
                  <Point X="-3.606691013534" Y="1.236585206012" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.047161307488" Y="-1.025271380216" />
                  <Point X="-3.846500753879" Y="1.214615202696" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.980827270807" Y="-1.10360705647" />
                  <Point X="-4.363345389291" Y="1.282659293331" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.917633864841" Y="-1.182963185503" />
                  <Point X="-4.64545635015" Y="1.274433789734" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.880448537491" Y="-1.270769851538" />
                  <Point X="-4.670333955653" Y="1.182628102455" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.868699234855" Y="-1.366841182997" />
                  <Point X="-4.695211561156" Y="1.090822415175" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.859774260051" Y="-1.463830194198" />
                  <Point X="-4.720089166659" Y="0.999016727896" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.874001809032" Y="-1.568341916394" />
                  <Point X="-4.739060826444" Y="0.905292082527" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.94219768054" Y="-1.690389009551" />
                  <Point X="-4.753163749433" Y="0.809985488678" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.036173608244" Y="-1.820812550736" />
                  <Point X="-4.767266812272" Y="0.714678940269" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.255832296344" Y="-1.992072896251" />
                  <Point X="-3.36805273096" Y="0.160157814702" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.278655701024" Y="0.456030655124" />
                  <Point X="-4.781369892301" Y="0.619372397446" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.481616852274" Y="-2.165323656881" />
                  <Point X="-3.304101773111" Y="0.039489977601" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.707401544199" Y="-2.338574461698" />
                  <Point X="-3.297076047417" Y="-0.06268173036" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.933186236124" Y="-2.511825266515" />
                  <Point X="3.104778076695" Y="-2.242659138996" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.457057717491" Y="-2.03220203664" />
                  <Point X="-3.323020527719" Y="-0.154140769004" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.070823053417" Y="-2.656435090683" />
                  <Point X="3.500486219609" Y="-2.471121419892" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.327006392399" Y="-2.089834710899" />
                  <Point X="-3.380459801555" Y="-0.2353665289" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.016468492929" Y="-2.738663134703" />
                  <Point X="3.896194378693" Y="-2.699583706041" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.2108599324" Y="-2.1519853497" />
                  <Point X="-3.481766127634" Y="-0.302339019506" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.133471048495" Y="-2.226729088353" />
                  <Point X="-3.64565250964" Y="-0.348978017351" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.086218008453" Y="-2.311264556239" />
                  <Point X="-3.814136480973" Y="-0.394123167868" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.041324238537" Y="-2.396566597458" />
                  <Point X="-3.982620452306" Y="-0.439268318385" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.006376800452" Y="-2.485100397793" />
                  <Point X="-4.151104423639" Y="-0.484413468902" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.002856710113" Y="-2.583845562412" />
                  <Point X="-4.319588394973" Y="-0.529558619419" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.022021132371" Y="-2.689961371973" />
                  <Point X="-4.488072366306" Y="-0.574703769936" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.041185459338" Y="-2.796077150573" />
                  <Point X="-3.11638315146" Y="-1.120281524252" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.15444119435" Y="-1.107915716517" />
                  <Point X="-4.656556376277" Y="-0.619848907898" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.092005518618" Y="-2.912478500099" />
                  <Point X="1.736433851665" Y="-2.796946262084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.303239292979" Y="-2.656192817666" />
                  <Point X="-2.962726984178" Y="-1.270096350752" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.416047211493" Y="-1.122803680197" />
                  <Point X="-4.783021179828" Y="-0.678646913647" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.162993262247" Y="-3.035432727498" />
                  <Point X="1.887155344244" Y="-2.945807554971" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.099825585236" Y="-2.689988608839" />
                  <Point X="-2.949299111802" Y="-1.374348242268" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.634827233104" Y="-1.151606653336" />
                  <Point X="-4.768038567798" Y="-0.783403970699" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.233981005875" Y="-3.158386954897" />
                  <Point X="1.989259429527" Y="-3.078872094648" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.001243538899" Y="-2.757846271592" />
                  <Point X="-2.970958198" Y="-1.467199689863" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.853607305846" Y="-1.180409609861" />
                  <Point X="-4.753055955768" Y="-0.888161027752" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.30496872205" Y="-3.281341173375" />
                  <Point X="2.091363514809" Y="-3.211936634325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.914863391275" Y="-2.829668571568" />
                  <Point X="-3.023947949238" Y="-1.54987118729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.07238740979" Y="-1.209212556249" />
                  <Point X="-4.735768755449" Y="-0.993666890931" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.375956269654" Y="-3.404295337082" />
                  <Point X="2.193467600092" Y="-3.345001174001" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.834973473928" Y="-2.903599675194" />
                  <Point X="-3.110400640865" Y="-1.62166991629" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.291167513734" Y="-1.238015502636" />
                  <Point X="-4.707944749694" Y="-1.102596369731" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.446943817258" Y="-3.527249500788" />
                  <Point X="2.295571685374" Y="-3.478065713678" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.788298731832" Y="-2.988323043473" />
                  <Point X="-3.201853349509" Y="-1.691844041281" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.509947617678" Y="-1.266818449023" />
                  <Point X="-4.680121094775" Y="-1.211525734538" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.517931364862" Y="-3.650203664494" />
                  <Point X="2.397675770657" Y="-3.611130253355" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.766389917359" Y="-3.081093349432" />
                  <Point X="-3.293306058154" Y="-1.762018166271" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.588918912466" Y="-3.773157828201" />
                  <Point X="2.49977985594" Y="-3.744194793032" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.746110772108" Y="-3.17439316702" />
                  <Point X="-3.384758766798" Y="-1.832192291261" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.65990646007" Y="-3.896111991907" />
                  <Point X="2.601884023032" Y="-3.87725935929" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.72690445048" Y="-3.268041566134" />
                  <Point X="0.45930615247" Y="-3.181093608432" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.071966952226" Y="-3.008472512637" />
                  <Point X="-3.476211475442" Y="-1.902366416252" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.730894007674" Y="-4.019066155614" />
                  <Point X="2.703988310586" Y="-4.010323964689" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.729952295164" Y="-3.368920782205" />
                  <Point X="0.548821074444" Y="-3.310067680991" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.229202338681" Y="-3.057272549936" />
                  <Point X="-3.567664184086" Y="-1.972540541242" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.743690624371" Y="-3.47327354726" />
                  <Point X="0.633899561616" Y="-3.437600268501" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.370981414185" Y="-3.111094647094" />
                  <Point X="-2.355147398217" Y="-2.466400038286" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.659745249334" Y="-2.367430197028" />
                  <Point X="-3.65911689273" Y="-2.042714666232" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.757428953578" Y="-3.577626312316" />
                  <Point X="0.671627177796" Y="-3.549747625393" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.449934454141" Y="-3.185330160637" />
                  <Point X="-2.311664175461" Y="-2.580417505118" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.774951511364" Y="-2.429886324668" />
                  <Point X="-3.750569601374" Y="-2.112888791222" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.771167282785" Y="-3.681979077371" />
                  <Point X="0.700944782021" Y="-3.659162403754" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.506505442137" Y="-3.266838043704" />
                  <Point X="-2.322693214163" Y="-2.676722864516" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.885659944084" Y="-2.493803885641" />
                  <Point X="-3.842022310018" Y="-2.183062916213" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.784905611992" Y="-3.786331842426" />
                  <Point X="0.730262386246" Y="-3.768577182116" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.563076421583" Y="-3.34834592955" />
                  <Point X="-2.364038968144" Y="-2.763177725995" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.996368376804" Y="-2.557721446614" />
                  <Point X="-3.933475018662" Y="-2.253237041203" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.798643908147" Y="-3.890684596743" />
                  <Point X="0.75957999047" Y="-3.877991960477" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.617984962181" Y="-3.430393974521" />
                  <Point X="-2.412600138683" Y="-2.847288156517" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.107076829414" Y="-2.621639001124" />
                  <Point X="-4.024927675671" Y="-2.323411182971" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.812382187941" Y="-3.995037345743" />
                  <Point X="0.788897594695" Y="-3.987406738839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.65120725721" Y="-3.519488307815" />
                  <Point X="-2.461161309223" Y="-2.931398587039" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.217785310651" Y="-2.685556546332" />
                  <Point X="-4.1163802954" Y="-2.393585336851" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.826120467735" Y="-4.099390094743" />
                  <Point X="0.81821519892" Y="-4.096821517201" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.675828786285" Y="-3.611377199369" />
                  <Point X="-2.509722479763" Y="-3.015509017561" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.328493791889" Y="-2.749474091541" />
                  <Point X="-4.159800559027" Y="-2.479366149286" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.70045031536" Y="-3.703266090924" />
                  <Point X="-2.558283650303" Y="-3.099619448083" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.439202273126" Y="-2.813391636749" />
                  <Point X="-4.085914371365" Y="-2.60326213824" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.725071844435" Y="-3.795154982479" />
                  <Point X="-2.606844820842" Y="-3.183729878605" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.549910754364" Y="-2.877309181958" />
                  <Point X="-4.012028183704" Y="-2.727158127193" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.74969337351" Y="-3.887043874034" />
                  <Point X="-2.655405991382" Y="-3.267840309127" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.660619235601" Y="-2.941226727167" />
                  <Point X="-3.913957898432" Y="-2.858912005796" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.774314918195" Y="-3.978932760516" />
                  <Point X="-2.703967161922" Y="-3.35195073965" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.771327716839" Y="-3.005144272375" />
                  <Point X="-3.812945524949" Y="-2.991621826806" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.798936472796" Y="-4.070821643777" />
                  <Point X="-1.523990923354" Y="-3.83523717195" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.646101909242" Y="-3.795560907508" />
                  <Point X="-2.752528332461" Y="-3.436061170172" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.823558027396" Y="-4.162710527038" />
                  <Point X="-1.332021473217" Y="-3.997500738677" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.999945797152" Y="-3.780478970237" />
                  <Point X="-2.801089503001" Y="-3.520171600694" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.848179581997" Y="-4.2545994103" />
                  <Point X="-1.204001349333" Y="-4.138985909744" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.129198871022" Y="-3.838371012041" />
                  <Point X="-2.849650673541" Y="-3.604282031216" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.872801136597" Y="-4.346488293561" />
                  <Point X="-1.172799121264" Y="-4.249013039512" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.229781831812" Y="-3.905578538277" />
                  <Point X="-2.898211879249" Y="-3.688392450311" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.897422691198" Y="-4.438377176822" />
                  <Point X="-1.151557098052" Y="-4.355803902544" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.330364748023" Y="-3.972786078999" />
                  <Point X="-2.946773109092" Y="-3.772502861565" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.922044245798" Y="-4.530266060083" />
                  <Point X="-1.130315069355" Y="-4.462594767359" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.413552929564" Y="-4.045645511625" />
                  <Point X="-2.913976568501" Y="-3.883048014874" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.946665800399" Y="-4.622154943344" />
                  <Point X="-1.120024977628" Y="-4.565827132139" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.474904388462" Y="-4.125600125539" />
                  <Point X="-2.659780789117" Y="-4.065530141597" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.971287354999" Y="-4.714043826605" />
                  <Point X="-1.132092178364" Y="-4.661795172244" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="0.001626220703" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.755615905762" Y="-4.597301269531" />
                  <Point X="0.471539031982" Y="-3.537112304688" />
                  <Point X="0.464318664551" Y="-3.521544189453" />
                  <Point X="0.348209350586" Y="-3.354252685547" />
                  <Point X="0.300591033936" Y="-3.285643798828" />
                  <Point X="0.274335601807" Y="-3.266399902344" />
                  <Point X="0.094663131714" Y="-3.210635986328" />
                  <Point X="0.020976625443" Y="-3.187766601562" />
                  <Point X="-0.008664756775" Y="-3.187766601562" />
                  <Point X="-0.188337234497" Y="-3.243530273438" />
                  <Point X="-0.262023895264" Y="-3.266399902344" />
                  <Point X="-0.288279144287" Y="-3.285643798828" />
                  <Point X="-0.404388458252" Y="-3.452935058594" />
                  <Point X="-0.45200680542" Y="-3.521544189453" />
                  <Point X="-0.459227142334" Y="-3.537112304688" />
                  <Point X="-0.575731506348" Y="-3.971912841797" />
                  <Point X="-0.84774395752" Y="-4.987077148438" />
                  <Point X="-1.014544616699" Y="-4.954700195312" />
                  <Point X="-1.10024609375" Y="-4.938065429688" />
                  <Point X="-1.308484130859" Y="-4.884487304688" />
                  <Point X="-1.351589599609" Y="-4.873396484375" />
                  <Point X="-1.343346435547" Y="-4.810783691406" />
                  <Point X="-1.309150146484" Y="-4.551038574219" />
                  <Point X="-1.309683227539" Y="-4.5347578125" />
                  <Point X="-1.352606933594" Y="-4.318965820312" />
                  <Point X="-1.370210693359" Y="-4.230466308594" />
                  <Point X="-1.386282714844" Y="-4.20262890625" />
                  <Point X="-1.551702392578" Y="-4.057559082031" />
                  <Point X="-1.619543457031" Y="-3.998063964844" />
                  <Point X="-1.649240844727" Y="-3.985762939453" />
                  <Point X="-1.868789550781" Y="-3.971372802734" />
                  <Point X="-1.958829956055" Y="-3.965471435547" />
                  <Point X="-1.989878540039" Y="-3.973791015625" />
                  <Point X="-2.172818359375" Y="-4.096027832031" />
                  <Point X="-2.247844970703" Y="-4.146159179688" />
                  <Point X="-2.259734130859" Y="-4.157294433594" />
                  <Point X="-2.325142333984" Y="-4.242536132812" />
                  <Point X="-2.457095214844" Y="-4.414500488281" />
                  <Point X="-2.730215576172" Y="-4.245391113281" />
                  <Point X="-2.855832275391" Y="-4.167612304688" />
                  <Point X="-3.144182128906" Y="-3.945592529297" />
                  <Point X="-3.228581054688" Y="-3.880608154297" />
                  <Point X="-3.033958251953" Y="-3.543511962891" />
                  <Point X="-2.506033691406" Y="-2.629119628906" />
                  <Point X="-2.499762695312" Y="-2.597593261719" />
                  <Point X="-2.513979736328" Y="-2.568764404297" />
                  <Point X="-2.531328369141" Y="-2.551415771484" />
                  <Point X="-2.560157226562" Y="-2.537198730469" />
                  <Point X="-2.59168359375" Y="-2.543469726562" />
                  <Point X="-2.966690673828" Y="-2.759980224609" />
                  <Point X="-3.842959472656" Y="-3.265894042969" />
                  <Point X="-4.062458251953" Y="-2.977517578125" />
                  <Point X="-4.161703613281" Y="-2.847129394531" />
                  <Point X="-4.368426757812" Y="-2.500486083984" />
                  <Point X="-4.431020019531" Y="-2.395527099609" />
                  <Point X="-4.087483886719" Y="-2.131922363281" />
                  <Point X="-3.163786865234" Y="-1.423144897461" />
                  <Point X="-3.145822021484" Y="-1.396013305664" />
                  <Point X="-3.140358154297" Y="-1.374917480469" />
                  <Point X="-3.138117431641" Y="-1.366265869141" />
                  <Point X="-3.140326416016" Y="-1.334595581055" />
                  <Point X="-3.161158691406" Y="-1.310639282227" />
                  <Point X="-3.179939208984" Y="-1.2995859375" />
                  <Point X="-3.187641357422" Y="-1.295052734375" />
                  <Point X="-3.219529296875" Y="-1.288571044922" />
                  <Point X="-3.692940185547" Y="-1.350896850586" />
                  <Point X="-4.803283203125" Y="-1.497076293945" />
                  <Point X="-4.888577148438" Y="-1.163154052734" />
                  <Point X="-4.927393066406" Y="-1.011191589355" />
                  <Point X="-4.982086914062" Y="-0.628777526855" />
                  <Point X="-4.998396484375" Y="-0.514742126465" />
                  <Point X="-4.609693847656" Y="-0.410589508057" />
                  <Point X="-3.557463867188" Y="-0.128645431519" />
                  <Point X="-3.541895507813" Y="-0.121424926758" />
                  <Point X="-3.522214111328" Y="-0.107765037537" />
                  <Point X="-3.514142578125" Y="-0.102162742615" />
                  <Point X="-3.494898681641" Y="-0.075907165527" />
                  <Point X="-3.488338134766" Y="-0.054769237518" />
                  <Point X="-3.485647705078" Y="-0.046100280762" />
                  <Point X="-3.485647705078" Y="-0.016459812164" />
                  <Point X="-3.492208251953" Y="0.004678116798" />
                  <Point X="-3.494898681641" Y="0.013347072601" />
                  <Point X="-3.514142578125" Y="0.039602802277" />
                  <Point X="-3.533823974609" Y="0.053262695313" />
                  <Point X="-3.557463867188" Y="0.066085334778" />
                  <Point X="-3.989000244141" Y="0.181715179443" />
                  <Point X="-4.998186523438" Y="0.452125793457" />
                  <Point X="-4.942660644531" Y="0.82736505127" />
                  <Point X="-4.917645019531" Y="0.996418212891" />
                  <Point X="-4.807548339844" Y="1.402709106445" />
                  <Point X="-4.773516113281" Y="1.528298706055" />
                  <Point X="-4.507530273438" Y="1.49328112793" />
                  <Point X="-3.753266357422" Y="1.393980102539" />
                  <Point X="-3.731704589844" Y="1.395866699219" />
                  <Point X="-3.688143554688" Y="1.40960144043" />
                  <Point X="-3.670278564453" Y="1.41523425293" />
                  <Point X="-3.651534423828" Y="1.426056152344" />
                  <Point X="-3.639120117188" Y="1.443785888672" />
                  <Point X="-3.621641113281" Y="1.485984008789" />
                  <Point X="-3.61447265625" Y="1.503290161133" />
                  <Point X="-3.610714111328" Y="1.524605224609" />
                  <Point X="-3.616315917969" Y="1.54551159668" />
                  <Point X="-3.63740625" Y="1.586025878906" />
                  <Point X="-3.646055664062" Y="1.602641235352" />
                  <Point X="-3.659968261719" Y="1.619221679688" />
                  <Point X="-3.907498779297" Y="1.809158325195" />
                  <Point X="-4.47610546875" Y="2.245466308594" />
                  <Point X="-4.257216308594" Y="2.620476318359" />
                  <Point X="-4.160014648438" Y="2.787006591797" />
                  <Point X="-3.868371826172" Y="3.161872070312" />
                  <Point X="-3.774671386719" Y="3.282310791016" />
                  <Point X="-3.622405029297" Y="3.194399658203" />
                  <Point X="-3.159156982422" Y="2.926943359375" />
                  <Point X="-3.138514648438" Y="2.920434814453" />
                  <Point X="-3.077846191406" Y="2.915126953125" />
                  <Point X="-3.052965332031" Y="2.912950195312" />
                  <Point X="-3.031506591797" Y="2.915775390625" />
                  <Point X="-3.013252441406" Y="2.927404785156" />
                  <Point X="-2.970189453125" Y="2.970467529297" />
                  <Point X="-2.952528808594" Y="2.988128173828" />
                  <Point X="-2.940899414062" Y="3.006382324219" />
                  <Point X="-2.93807421875" Y="3.027841064453" />
                  <Point X="-2.943382080078" Y="3.088509521484" />
                  <Point X="-2.945558837891" Y="3.113390625" />
                  <Point X="-2.952067382812" Y="3.134032958984" />
                  <Point X="-3.061755615234" Y="3.324018310547" />
                  <Point X="-3.307278564453" Y="3.749276611328" />
                  <Point X="-2.922169433594" Y="4.044535888672" />
                  <Point X="-2.752876220703" Y="4.174331542969" />
                  <Point X="-2.293547607422" Y="4.429524414063" />
                  <Point X="-2.141548583984" Y="4.513971679688" />
                  <Point X="-2.109641113281" Y="4.472389160156" />
                  <Point X="-1.967826904297" Y="4.287573242188" />
                  <Point X="-1.951246826172" Y="4.273660644531" />
                  <Point X="-1.883723266602" Y="4.238509765625" />
                  <Point X="-1.856030761719" Y="4.22409375" />
                  <Point X="-1.835124389648" Y="4.2184921875" />
                  <Point X="-1.813809204102" Y="4.222250488281" />
                  <Point X="-1.743478881836" Y="4.2513828125" />
                  <Point X="-1.714635253906" Y="4.263330078125" />
                  <Point X="-1.696905517578" Y="4.275744628906" />
                  <Point X="-1.686083496094" Y="4.294488769531" />
                  <Point X="-1.663192016602" Y="4.367090332031" />
                  <Point X="-1.653804077148" Y="4.396865234375" />
                  <Point X="-1.651917602539" Y="4.418426757812" />
                  <Point X="-1.664388061523" Y="4.5131484375" />
                  <Point X="-1.689137573242" Y="4.701141113281" />
                  <Point X="-1.187255249023" Y="4.8418515625" />
                  <Point X="-0.968094177246" Y="4.903296386719" />
                  <Point X="-0.411248138428" Y="4.968466796875" />
                  <Point X="-0.224199615479" Y="4.990358398438" />
                  <Point X="-0.17918196106" Y="4.822349609375" />
                  <Point X="-0.042140380859" Y="4.310903320312" />
                  <Point X="-0.024282115936" Y="4.284176757813" />
                  <Point X="0.006155934334" Y="4.273844238281" />
                  <Point X="0.036594036102" Y="4.284176757813" />
                  <Point X="0.054452251434" Y="4.310903320312" />
                  <Point X="0.110655265808" Y="4.520655761719" />
                  <Point X="0.236648422241" Y="4.990868652344" />
                  <Point X="0.668844360352" Y="4.945606445312" />
                  <Point X="0.860205627441" Y="4.925565429688" />
                  <Point X="1.320898681641" Y="4.814340332031" />
                  <Point X="1.508456665039" Y="4.769057617188" />
                  <Point X="1.808865234375" Y="4.660097167969" />
                  <Point X="1.931033691406" Y="4.615786132813" />
                  <Point X="2.220989990234" Y="4.480183105469" />
                  <Point X="2.33869921875" Y="4.425134277344" />
                  <Point X="2.618807128906" Y="4.261942871094" />
                  <Point X="2.732523681641" Y="4.19569140625" />
                  <Point X="2.996703613281" Y="4.007821289062" />
                  <Point X="3.068740234375" Y="3.956592773438" />
                  <Point X="2.84088671875" Y="3.561938476562" />
                  <Point X="2.229853515625" Y="2.503597900391" />
                  <Point X="2.224852050781" Y="2.491515136719" />
                  <Point X="2.209626708984" Y="2.434579101562" />
                  <Point X="2.203382324219" Y="2.411228759766" />
                  <Point X="2.202044677734" Y="2.392326171875" />
                  <Point X="2.207981445312" Y="2.343092773438" />
                  <Point X="2.210416015625" Y="2.322901367188" />
                  <Point X="2.218682128906" Y="2.300812255859" />
                  <Point X="2.249145996094" Y="2.255916259766" />
                  <Point X="2.261639892578" Y="2.237503662109" />
                  <Point X="2.274939941406" Y="2.224203613281" />
                  <Point X="2.3198359375" Y="2.193739746094" />
                  <Point X="2.338248535156" Y="2.18124609375" />
                  <Point X="2.360336669922" Y="2.172980224609" />
                  <Point X="2.409570068359" Y="2.167043457031" />
                  <Point X="2.429761474609" Y="2.164608642578" />
                  <Point X="2.448664306641" Y="2.165946289062" />
                  <Point X="2.505600341797" Y="2.181171875" />
                  <Point X="2.528950683594" Y="2.187416015625" />
                  <Point X="2.541033935547" Y="2.192417480469" />
                  <Point X="2.975076416016" Y="2.443011962891" />
                  <Point X="3.994248046875" Y="3.031430908203" />
                  <Point X="4.135139160156" Y="2.835624267578" />
                  <Point X="4.202591796875" Y="2.741880126953" />
                  <Point X="4.349868164063" Y="2.498504882812" />
                  <Point X="4.387513183594" Y="2.436296142578" />
                  <Point X="4.093098632813" Y="2.210383789062" />
                  <Point X="3.288616210938" Y="1.593082763672" />
                  <Point X="3.27937109375" Y="1.583833129883" />
                  <Point X="3.238394042969" Y="1.530375488281" />
                  <Point X="3.221588867188" Y="1.508451782227" />
                  <Point X="3.213119628906" Y="1.49150012207" />
                  <Point X="3.197855712891" Y="1.436919799805" />
                  <Point X="3.191595703125" Y="1.414535644531" />
                  <Point X="3.190779541016" Y="1.390965087891" />
                  <Point X="3.203309570312" Y="1.330237548828" />
                  <Point X="3.208448486328" Y="1.305332275391" />
                  <Point X="3.215646484375" Y="1.287954956055" />
                  <Point X="3.249727050781" Y="1.236153930664" />
                  <Point X="3.263704101562" Y="1.214909667969" />
                  <Point X="3.280947998047" Y="1.198819824219" />
                  <Point X="3.330335449219" Y="1.171018920898" />
                  <Point X="3.350590087891" Y="1.159617431641" />
                  <Point X="3.368565429688" Y="1.153619628906" />
                  <Point X="3.435340576172" Y="1.144794555664" />
                  <Point X="3.462726074219" Y="1.141175048828" />
                  <Point X="3.475803710938" Y="1.14117175293" />
                  <Point X="3.888115234375" Y="1.195453613281" />
                  <Point X="4.848975097656" Y="1.32195324707" />
                  <Point X="4.910217773438" Y="1.070387207031" />
                  <Point X="4.939188476562" Y="0.951385253906" />
                  <Point X="4.985599609375" Y="0.653295349121" />
                  <Point X="4.997858398438" Y="0.574556213379" />
                  <Point X="4.663122070312" Y="0.484863800049" />
                  <Point X="3.741167724609" Y="0.237826919556" />
                  <Point X="3.729086914062" Y="0.232819412231" />
                  <Point X="3.663482421875" Y="0.194898788452" />
                  <Point X="3.636576904297" Y="0.179346878052" />
                  <Point X="3.622264892578" Y="0.166926727295" />
                  <Point X="3.58290234375" Y="0.116769271851" />
                  <Point X="3.566759033203" Y="0.096199043274" />
                  <Point X="3.556985107422" Y="0.07473513031" />
                  <Point X="3.543864013672" Y="0.006222705841" />
                  <Point X="3.538482910156" Y="-0.021875303268" />
                  <Point X="3.538482910156" Y="-0.040684635162" />
                  <Point X="3.551604003906" Y="-0.109197212219" />
                  <Point X="3.556985107422" Y="-0.137295227051" />
                  <Point X="3.566759033203" Y="-0.158759140015" />
                  <Point X="3.606121582031" Y="-0.208916442871" />
                  <Point X="3.622264892578" Y="-0.229486679077" />
                  <Point X="3.636576904297" Y="-0.241906967163" />
                  <Point X="3.702181396484" Y="-0.279827575684" />
                  <Point X="3.729086914062" Y="-0.295379486084" />
                  <Point X="3.741167724609" Y="-0.300387023926" />
                  <Point X="4.119275878906" Y="-0.401700714111" />
                  <Point X="4.998067871094" Y="-0.637172363281" />
                  <Point X="4.964607421875" Y="-0.859108215332" />
                  <Point X="4.948431640625" Y="-0.966399169922" />
                  <Point X="4.888971679688" Y="-1.226962280273" />
                  <Point X="4.874545898438" Y="-1.290178344727" />
                  <Point X="4.481189941406" Y="-1.238392089844" />
                  <Point X="3.411982177734" Y="-1.097628295898" />
                  <Point X="3.394836181641" Y="-1.098341186523" />
                  <Point X="3.266077880859" Y="-1.126327392578" />
                  <Point X="3.213271972656" Y="-1.137804931641" />
                  <Point X="3.1854453125" Y="-1.154697265625" />
                  <Point X="3.107618896484" Y="-1.248298095703" />
                  <Point X="3.075701171875" Y="-1.286685424805" />
                  <Point X="3.064357910156" Y="-1.31407043457" />
                  <Point X="3.053203613281" Y="-1.435287597656" />
                  <Point X="3.04862890625" Y="-1.485000732422" />
                  <Point X="3.056360351562" Y="-1.516621826172" />
                  <Point X="3.1276171875" Y="-1.62745703125" />
                  <Point X="3.156840576172" Y="-1.672912353516" />
                  <Point X="3.168460693359" Y="-1.685540649414" />
                  <Point X="3.51934765625" Y="-1.954785766602" />
                  <Point X="4.339074707031" Y="-2.583784179688" />
                  <Point X="4.249729980469" Y="-2.728357177734" />
                  <Point X="4.204133300781" Y="-2.802139404297" />
                  <Point X="4.081165283203" Y="-2.976860351562" />
                  <Point X="4.056688476562" Y="-3.011638427734" />
                  <Point X="3.705728271484" Y="-2.809011230469" />
                  <Point X="2.753454589844" Y="-2.259215576172" />
                  <Point X="2.737340820313" Y="-2.253312744141" />
                  <Point X="2.584097900391" Y="-2.225637207031" />
                  <Point X="2.521250488281" Y="-2.214287109375" />
                  <Point X="2.489077880859" Y="-2.219244873047" />
                  <Point X="2.361770507812" Y="-2.286245849609" />
                  <Point X="2.309559814453" Y="-2.313723876953" />
                  <Point X="2.288599853516" Y="-2.334683837891" />
                  <Point X="2.221598876953" Y="-2.461991210938" />
                  <Point X="2.194120849609" Y="-2.514201904297" />
                  <Point X="2.189163085938" Y="-2.546374511719" />
                  <Point X="2.216838623047" Y="-2.699617431641" />
                  <Point X="2.228188720703" Y="-2.76246484375" />
                  <Point X="2.234091552734" Y="-2.778578613281" />
                  <Point X="2.459571777344" Y="-3.169121337891" />
                  <Point X="2.986673583984" Y="-4.082088623047" />
                  <Point X="2.889405761719" Y="-4.151564453125" />
                  <Point X="2.835296630859" Y="-4.190213378906" />
                  <Point X="2.697812988281" Y="-4.279204101562" />
                  <Point X="2.679775878906" Y="-4.290879394531" />
                  <Point X="2.409850097656" Y="-3.939105224609" />
                  <Point X="1.683177612305" Y="-2.992087402344" />
                  <Point X="1.670549316406" Y="-2.980467529297" />
                  <Point X="1.519410400391" Y="-2.883299072266" />
                  <Point X="1.45742590332" Y="-2.843448730469" />
                  <Point X="1.4258046875" Y="-2.835717041016" />
                  <Point X="1.260508666992" Y="-2.850927734375" />
                  <Point X="1.192717773438" Y="-2.857166015625" />
                  <Point X="1.165332885742" Y="-2.868509277344" />
                  <Point X="1.037695556641" Y="-2.974635498047" />
                  <Point X="0.985349060059" Y="-3.018159667969" />
                  <Point X="0.968456726074" Y="-3.045986328125" />
                  <Point X="0.930293762207" Y="-3.221566162109" />
                  <Point X="0.91464251709" Y="-3.29357421875" />
                  <Point X="0.91392956543" Y="-3.310720214844" />
                  <Point X="0.977828918457" Y="-3.796083496094" />
                  <Point X="1.127642211914" Y="-4.934028320312" />
                  <Point X="1.045533203125" Y="-4.952026855469" />
                  <Point X="0.994347229004" Y="-4.963246582031" />
                  <Point X="0.867337585449" Y="-4.986319824219" />
                  <Point X="0.860200378418" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#188" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.137041589218" Y="4.866570673383" Z="1.8" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.8" />
                  <Point X="-0.4232162988" Y="5.049408199373" Z="1.8" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.8" />
                  <Point X="-1.206908977239" Y="4.921274931679" Z="1.8" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.8" />
                  <Point X="-1.720116201252" Y="4.53790205109" Z="1.8" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.8" />
                  <Point X="-1.717033944948" Y="4.41340553733" Z="1.8" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.8" />
                  <Point X="-1.76876874257" Y="4.328856590757" Z="1.8" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.8" />
                  <Point X="-1.866791580138" Y="4.314140500414" Z="1.8" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.8" />
                  <Point X="-2.076129432762" Y="4.53410731045" Z="1.8" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.8" />
                  <Point X="-2.323986649474" Y="4.504511880731" Z="1.8" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.8" />
                  <Point X="-2.958747626355" Y="4.11549574825" Z="1.8" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.8" />
                  <Point X="-3.111212876491" Y="3.330298416608" Z="1.8" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.8" />
                  <Point X="-2.999347854783" Y="3.115431857963" Z="1.8" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.8" />
                  <Point X="-3.011700760943" Y="3.037102806943" Z="1.8" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.8" />
                  <Point X="-3.079644607643" Y="2.99621684439" Z="1.8" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.8" />
                  <Point X="-3.603560931689" Y="3.268981072254" Z="1.8" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.8" />
                  <Point X="-3.913991130678" Y="3.223854573885" Z="1.8" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.8" />
                  <Point X="-4.306554375152" Y="2.676961467068" Z="1.8" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.8" />
                  <Point X="-3.944092631926" Y="1.800770811944" Z="1.8" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.8" />
                  <Point X="-3.687912792731" Y="1.594218589494" Z="1.8" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.8" />
                  <Point X="-3.673990869698" Y="1.536398243911" Z="1.8" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.8" />
                  <Point X="-3.709335005212" Y="1.488567272391" Z="1.8" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.8" />
                  <Point X="-4.507160041094" Y="1.574133300366" Z="1.8" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.8" />
                  <Point X="-4.861964107776" Y="1.447066511932" Z="1.8" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.8" />
                  <Point X="-4.998278065728" Y="0.865964161236" Z="1.8" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.8" />
                  <Point X="-4.008097212678" Y="0.164698884362" Z="1.8" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.8" />
                  <Point X="-3.568489128795" Y="0.043466875937" Z="1.8" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.8" />
                  <Point X="-3.546117277557" Y="0.021137939978" Z="1.8" />
                  <Point X="-3.539556741714" Y="0" Z="1.8" />
                  <Point X="-3.542247323578" Y="-0.008669011083" Z="1.8" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.8" />
                  <Point X="-3.556879466328" Y="-0.035409107197" Z="1.8" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.8" />
                  <Point X="-4.628790576095" Y="-0.331013172394" Z="1.8" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.8" />
                  <Point X="-5.037738979693" Y="-0.604576451236" Z="1.8" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.8" />
                  <Point X="-4.943177944904" Y="-1.144248336485" Z="1.8" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.8" />
                  <Point X="-3.692569755343" Y="-1.369189019086" Z="1.8" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.8" />
                  <Point X="-3.211456731504" Y="-1.311396462018" Z="1.8" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.8" />
                  <Point X="-3.194917070497" Y="-1.331101485065" Z="1.8" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.8" />
                  <Point X="-4.124077602256" Y="-2.060974623766" Z="1.8" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.8" />
                  <Point X="-4.417526181541" Y="-2.49481559014" Z="1.8" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.8" />
                  <Point X="-4.108400157592" Y="-2.976520808238" Z="1.8" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.8" />
                  <Point X="-2.947846574567" Y="-2.772001403908" Z="1.8" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.8" />
                  <Point X="-2.567793877436" Y="-2.560536591695" Z="1.8" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.8" />
                  <Point X="-3.08341560926" Y="-3.487231807926" Z="1.8" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.8" />
                  <Point X="-3.1808420348" Y="-3.953929603621" Z="1.8" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.8" />
                  <Point X="-2.762693328728" Y="-4.256621709849" Z="1.8" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.8" />
                  <Point X="-2.291630556161" Y="-4.241693878563" Z="1.8" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.8" />
                  <Point X="-2.151195732184" Y="-4.10632097088" Z="1.8" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.8" />
                  <Point X="-1.878215531108" Y="-3.989985938079" Z="1.8" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.8" />
                  <Point X="-1.5908255908" Y="-4.06387120243" Z="1.8" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.8" />
                  <Point X="-1.407802263808" Y="-4.297440276808" Z="1.8" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.8" />
                  <Point X="-1.399074664723" Y="-4.772977509984" Z="1.8" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.8" />
                  <Point X="-1.327098895566" Y="-4.90163034241" Z="1.8" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.8" />
                  <Point X="-1.030227700143" Y="-4.972504379755" Z="1.8" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.8" />
                  <Point X="-0.533591273422" Y="-3.95357379849" Z="1.8" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.8" />
                  <Point X="-0.369468449394" Y="-3.450164142838" Z="1.8" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.8" />
                  <Point X="-0.179672486508" Y="-3.260003077717" Z="1.8" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.8" />
                  <Point X="0.073686592853" Y="-3.227108967571" Z="1.8" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.8" />
                  <Point X="0.300977410038" Y="-3.351481612929" Z="1.8" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.8" />
                  <Point X="0.701163586979" Y="-4.578962231325" Z="1.8" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.8" />
                  <Point X="0.870118512834" Y="-5.004234621558" Z="1.8" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.8" />
                  <Point X="1.050082715524" Y="-4.96958710257" Z="1.8" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.8" />
                  <Point X="1.021245109404" Y="-3.758277202398" Z="1.8" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.8" />
                  <Point X="0.972997038202" Y="-3.20090564684" Z="1.8" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.8" />
                  <Point X="1.063506228561" Y="-2.981801854397" Z="1.8" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.8" />
                  <Point X="1.258934355647" Y="-2.869437286745" Z="1.8" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.8" />
                  <Point X="1.486214973525" Y="-2.894076906603" Z="1.8" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.8" />
                  <Point X="2.364026487802" Y="-3.938263234504" Z="1.8" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.8" />
                  <Point X="2.718826185981" Y="-4.289898333562" Z="1.8" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.8" />
                  <Point X="2.912311586482" Y="-4.160971636765" Z="1.8" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.8" />
                  <Point X="2.496716976143" Y="-3.112841412697" Z="1.8" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.8" />
                  <Point X="2.259886693358" Y="-2.659451186913" Z="1.8" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.8" />
                  <Point X="2.259689274858" Y="-2.453997427147" Z="1.8" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.8" />
                  <Point X="2.378900951225" Y="-2.299212035109" Z="1.8" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.8" />
                  <Point X="2.569055616599" Y="-2.243561316133" Z="1.8" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.8" />
                  <Point X="3.67457218441" Y="-2.821032524132" Z="1.8" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.8" />
                  <Point X="4.115897506061" Y="-2.974357621963" Z="1.8" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.8" />
                  <Point X="4.286107017838" Y="-2.723362173901" Z="1.8" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.8" />
                  <Point X="3.543629592096" Y="-1.883837951537" Z="1.8" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.8" />
                  <Point X="3.16351927233" Y="-1.569137714118" Z="1.8" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.8" />
                  <Point X="3.096837091988" Y="-1.408589383272" Z="1.8" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.8" />
                  <Point X="3.139909266164" Y="-1.248985015917" Z="1.8" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.8" />
                  <Point X="3.270541439722" Y="-1.14390659977" Z="1.8" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.8" />
                  <Point X="4.468507424379" Y="-1.256684260125" Z="1.8" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.8" />
                  <Point X="4.931562720583" Y="-1.20680613053" Z="1.8" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.8" />
                  <Point X="5.007893083845" Y="-0.835282266863" Z="1.8" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.8" />
                  <Point X="4.126060996863" Y="-0.322124445112" Z="1.8" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.8" />
                  <Point X="3.721047227734" Y="-0.205258791835" Z="1.8" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.8" />
                  <Point X="3.639299431535" Y="-0.146767871028" Z="1.8" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.8" />
                  <Point X="3.594555629324" Y="-0.068512462281" Z="1.8" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.8" />
                  <Point X="3.586815821101" Y="0.028098068913" Z="1.8" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.8" />
                  <Point X="3.616080006866" Y="0.117180867547" Z="1.8" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.8" />
                  <Point X="3.682348186619" Y="0.182890052407" Z="1.8" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.8" />
                  <Point X="4.669906820444" Y="0.467847487656" Z="1.8" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.8" />
                  <Point X="5.02884820627" Y="0.692267238837" Z="1.8" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.8" />
                  <Point X="4.95264296259" Y="1.113494159906" Z="1.8" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.8" />
                  <Point X="3.875432802418" Y="1.276305923891" Z="1.8" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.8" />
                  <Point X="3.435735822964" Y="1.225643422629" Z="1.8" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.8" />
                  <Point X="3.348706019666" Y="1.245870164367" Z="1.8" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.8" />
                  <Point X="3.28534156101" Y="1.294915442964" Z="1.8" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.8" />
                  <Point X="3.246122180048" Y="1.37162172554" Z="1.8" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.8" />
                  <Point X="3.239852028991" Y="1.454733449587" Z="1.8" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.8" />
                  <Point X="3.2719212706" Y="1.531237520794" Z="1.8" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.8" />
                  <Point X="4.117380612023" Y="2.201996177078" Z="1.8" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.8" />
                  <Point X="4.386489417528" Y="2.555671089719" Z="1.8" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.8" />
                  <Point X="4.169568521056" Y="2.89610731011" Z="1.8" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.8" />
                  <Point X="2.943920531081" Y="2.517593371819" Z="1.8" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.8" />
                  <Point X="2.486527577653" Y="2.260754582069" Z="1.8" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.8" />
                  <Point X="2.409400226366" Y="2.247963887201" Z="1.8" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.8" />
                  <Point X="2.341754171595" Y="2.266394238804" Z="1.8" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.8" />
                  <Point X="2.284364361431" Y="2.315270688787" Z="1.8" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.8" />
                  <Point X="2.251465815558" Y="2.380358217466" Z="1.8" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.8" />
                  <Point X="2.251773360589" Y="2.452941998229" Z="1.8" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.8" />
                  <Point X="2.878031981041" Y="3.568218550874" Z="1.8" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.8" />
                  <Point X="3.019524676182" Y="4.07984841188" Z="1.8" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.8" />
                  <Point X="2.637821124955" Y="4.336425522934" Z="1.8" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.8" />
                  <Point X="2.236015323952" Y="4.556755254883" Z="1.8" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.8" />
                  <Point X="1.819757761149" Y="4.738381354503" Z="1.8" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.8" />
                  <Point X="1.326477118106" Y="4.894223753694" Z="1.8" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.8" />
                  <Point X="0.667896305416" Y="5.026614317936" Z="1.8" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.8" />
                  <Point X="0.056202972335" Y="4.564876832568" Z="1.8" />
                  <Point X="0" Y="4.355124473572" Z="1.8" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>