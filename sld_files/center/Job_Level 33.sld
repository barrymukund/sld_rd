<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#181" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2414" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="0" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476318359" Y="0.004715820312" />
                  <Width Value="9.783896972656" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.815836975098" Y="-4.454997558594" />
                  <Point X="0.563302062988" Y="-3.512524414063" />
                  <Point X="0.557721191406" Y="-3.497141845703" />
                  <Point X="0.54236315918" Y="-3.467377197266" />
                  <Point X="0.439145874023" Y="-3.318660400391" />
                  <Point X="0.378635528564" Y="-3.231476806641" />
                  <Point X="0.356751586914" Y="-3.209021484375" />
                  <Point X="0.330496398926" Y="-3.189777587891" />
                  <Point X="0.302495330811" Y="-3.175669189453" />
                  <Point X="0.142772201538" Y="-3.126096923828" />
                  <Point X="0.049136188507" Y="-3.097035888672" />
                  <Point X="0.020976638794" Y="-3.092766601562" />
                  <Point X="-0.00866468811" Y="-3.092766601562" />
                  <Point X="-0.036824085236" Y="-3.097035888672" />
                  <Point X="-0.196547210693" Y="-3.146607910156" />
                  <Point X="-0.290183227539" Y="-3.175669189453" />
                  <Point X="-0.31818460083" Y="-3.189777587891" />
                  <Point X="-0.344439788818" Y="-3.209021484375" />
                  <Point X="-0.366323577881" Y="-3.231476806641" />
                  <Point X="-0.469541046143" Y="-3.380193359375" />
                  <Point X="-0.530051208496" Y="-3.467377197266" />
                  <Point X="-0.538189025879" Y="-3.481573730469" />
                  <Point X="-0.550990112305" Y="-3.512524414063" />
                  <Point X="-0.699036315918" Y="-4.065040527344" />
                  <Point X="-0.916584533691" Y="-4.876941894531" />
                  <Point X="-0.972841186523" Y="-4.866021972656" />
                  <Point X="-1.079341552734" Y="-4.845350097656" />
                  <Point X="-1.24641796875" Y="-4.802362304688" />
                  <Point X="-1.245362304688" Y="-4.79434375" />
                  <Point X="-1.214963012695" Y="-4.563438476562" />
                  <Point X="-1.214201171875" Y="-4.5479296875" />
                  <Point X="-1.216508666992" Y="-4.516224121094" />
                  <Point X="-1.254666625977" Y="-4.324392089844" />
                  <Point X="-1.277036132812" Y="-4.211932617188" />
                  <Point X="-1.287938354492" Y="-4.182966308594" />
                  <Point X="-1.304010375977" Y="-4.15512890625" />
                  <Point X="-1.323644775391" Y="-4.131204101562" />
                  <Point X="-1.470697509766" Y="-4.002241943359" />
                  <Point X="-1.556905517578" Y="-3.926639160156" />
                  <Point X="-1.583188476562" Y="-3.910295410156" />
                  <Point X="-1.612885742188" Y="-3.897994384766" />
                  <Point X="-1.64302734375" Y="-3.890966308594" />
                  <Point X="-1.83819909668" Y="-3.878174072266" />
                  <Point X="-1.952616577148" Y="-3.870674804688" />
                  <Point X="-1.98341796875" Y="-3.873708496094" />
                  <Point X="-2.014466674805" Y="-3.882028076172" />
                  <Point X="-2.042657714844" Y="-3.894801513672" />
                  <Point X="-2.205285400391" Y="-4.003465820312" />
                  <Point X="-2.300624267578" Y="-4.067169677734" />
                  <Point X="-2.312785644531" Y="-4.076821533203" />
                  <Point X="-2.335102783203" Y="-4.099461914062" />
                  <Point X="-2.418219238281" Y="-4.20778125" />
                  <Point X="-2.480149169922" Y="-4.288489746094" />
                  <Point X="-2.645609375" Y="-4.186041015625" />
                  <Point X="-2.801708007812" Y="-4.089388427734" />
                  <Point X="-3.053984619141" Y="-3.895143798828" />
                  <Point X="-3.104721923828" Y="-3.856077880859" />
                  <Point X="-2.893069091797" Y="-3.489484375" />
                  <Point X="-2.423761230469" Y="-2.676619628906" />
                  <Point X="-2.412859130859" Y="-2.647653320312" />
                  <Point X="-2.406588134766" Y="-2.616126953125" />
                  <Point X="-2.405575439453" Y="-2.585192871094" />
                  <Point X="-2.414560058594" Y="-2.555575195312" />
                  <Point X="-2.428777099609" Y="-2.526746337891" />
                  <Point X="-2.4468046875" Y="-2.501589355469" />
                  <Point X="-2.464153320312" Y="-2.484240722656" />
                  <Point X="-2.489310302734" Y="-2.466213134766" />
                  <Point X="-2.518139160156" Y="-2.45199609375" />
                  <Point X="-2.547756835938" Y="-2.443011474609" />
                  <Point X="-2.578690917969" Y="-2.444024169922" />
                  <Point X="-2.610217285156" Y="-2.450295166016" />
                  <Point X="-2.63918359375" Y="-2.461197265625" />
                  <Point X="-3.115718017578" Y="-2.736324707031" />
                  <Point X="-3.818024414062" Y="-3.141801269531" />
                  <Point X="-3.959532958984" Y="-2.955888183594" />
                  <Point X="-4.082858398438" Y="-2.793864013672" />
                  <Point X="-4.263720214844" Y="-2.4905859375" />
                  <Point X="-4.306142578125" Y="-2.419450195312" />
                  <Point X="-3.927090820312" Y="-2.128593505859" />
                  <Point X="-3.105954589844" Y="-1.498513427734" />
                  <Point X="-3.084576904297" Y="-1.475592773438" />
                  <Point X="-3.066612060547" Y="-1.448461181641" />
                  <Point X="-3.053856445312" Y="-1.419832397461" />
                  <Point X="-3.046151855469" Y="-1.390084960938" />
                  <Point X="-3.04334765625" Y="-1.359655761719" />
                  <Point X="-3.045556640625" Y="-1.327985595703" />
                  <Point X="-3.052557861328" Y="-1.298240844727" />
                  <Point X="-3.068639892578" Y="-1.272257568359" />
                  <Point X="-3.089472167969" Y="-1.248301269531" />
                  <Point X="-3.112972167969" Y="-1.228767211914" />
                  <Point X="-3.139454833984" Y="-1.213180664062" />
                  <Point X="-3.168717773438" Y="-1.201956665039" />
                  <Point X="-3.200605712891" Y="-1.195474853516" />
                  <Point X="-3.231929199219" Y="-1.194383789062" />
                  <Point X="-3.833509033203" Y="-1.273583251953" />
                  <Point X="-4.732102050781" Y="-1.391885253906" />
                  <Point X="-4.785842773438" Y="-1.181492797852" />
                  <Point X="-4.834077636719" Y="-0.992654724121" />
                  <Point X="-4.881928710938" Y="-0.658085327148" />
                  <Point X="-4.892424804688" Y="-0.584698303223" />
                  <Point X="-4.468273925781" Y="-0.471047332764" />
                  <Point X="-3.532875976562" Y="-0.220408447266" />
                  <Point X="-3.517492675781" Y="-0.214827255249" />
                  <Point X="-3.487729003906" Y="-0.199469558716" />
                  <Point X="-3.470232910156" Y="-0.187326324463" />
                  <Point X="-3.459976074219" Y="-0.180207504272" />
                  <Point X="-3.437520507812" Y="-0.158323577881" />
                  <Point X="-3.418276611328" Y="-0.132068237305" />
                  <Point X="-3.404168212891" Y="-0.10406716156" />
                  <Point X="-3.394917236328" Y="-0.074260231018" />
                  <Point X="-3.390647949219" Y="-0.046100681305" />
                  <Point X="-3.390647949219" Y="-0.016459354401" />
                  <Point X="-3.394917236328" Y="0.011700194359" />
                  <Point X="-3.404168212891" Y="0.041506973267" />
                  <Point X="-3.418276611328" Y="0.069508209229" />
                  <Point X="-3.437520507812" Y="0.095763389587" />
                  <Point X="-3.459975585938" Y="0.117647171021" />
                  <Point X="-3.477471679688" Y="0.129790405273" />
                  <Point X="-3.485522460938" Y="0.1348019104" />
                  <Point X="-3.511347412109" Y="0.149141098022" />
                  <Point X="-3.532875976562" Y="0.157848251343" />
                  <Point X="-4.081244628906" Y="0.304783050537" />
                  <Point X="-4.89181640625" Y="0.521975280762" />
                  <Point X="-4.855573730469" Y="0.76690045166" />
                  <Point X="-4.824487792969" Y="0.976975280762" />
                  <Point X="-4.728165527344" Y="1.332435058594" />
                  <Point X="-4.703551757812" Y="1.423267822266" />
                  <Point X="-4.436182128906" Y="1.388068115234" />
                  <Point X="-3.765666259766" Y="1.29979284668" />
                  <Point X="-3.744985839844" Y="1.299341674805" />
                  <Point X="-3.723424072266" Y="1.301228271484" />
                  <Point X="-3.703137695312" Y="1.305263549805" />
                  <Point X="-3.664413330078" Y="1.317473266602" />
                  <Point X="-3.641711669922" Y="1.324631103516" />
                  <Point X="-3.622778320312" Y="1.332962036133" />
                  <Point X="-3.604034179688" Y="1.343783935547" />
                  <Point X="-3.587353515625" Y="1.356014892578" />
                  <Point X="-3.573715332031" Y="1.371566040039" />
                  <Point X="-3.56130078125" Y="1.389295654297" />
                  <Point X="-3.551351318359" Y="1.407430908203" />
                  <Point X="-3.535812988281" Y="1.444943847656" />
                  <Point X="-3.526703857422" Y="1.466935180664" />
                  <Point X="-3.520915527344" Y="1.486794189453" />
                  <Point X="-3.517157226562" Y="1.508109375" />
                  <Point X="-3.5158046875" Y="1.528749389648" />
                  <Point X="-3.518951171875" Y="1.549192993164" />
                  <Point X="-3.524552978516" Y="1.570099365234" />
                  <Point X="-3.532049804688" Y="1.589377563477" />
                  <Point X="-3.550798339844" Y="1.625393432617" />
                  <Point X="-3.561789550781" Y="1.646507324219" />
                  <Point X="-3.573281738281" Y="1.663706665039" />
                  <Point X="-3.587194335938" Y="1.680286987305" />
                  <Point X="-3.602135986328" Y="1.694590087891" />
                  <Point X="-3.916681396484" Y="1.93594934082" />
                  <Point X="-4.351859863281" Y="2.269874267578" />
                  <Point X="-4.201939941406" Y="2.526723632812" />
                  <Point X="-4.081153808594" Y="2.733659423828" />
                  <Point X="-3.825999755859" Y="3.061624023438" />
                  <Point X="-3.750504882813" Y="3.158661621094" />
                  <Point X="-3.618469238281" Y="3.082430908203" />
                  <Point X="-3.206656982422" Y="2.844670898438" />
                  <Point X="-3.187724121094" Y="2.836340332031" />
                  <Point X="-3.167081787109" Y="2.829831787109" />
                  <Point X="-3.146794433594" Y="2.825796386719" />
                  <Point X="-3.092862304688" Y="2.821077880859" />
                  <Point X="-3.061245117188" Y="2.818311767578" />
                  <Point X="-3.040564697266" Y="2.818762939453" />
                  <Point X="-3.019106201172" Y="2.821588134766" />
                  <Point X="-2.999014892578" Y="2.826504638672" />
                  <Point X="-2.980463867188" Y="2.835652832031" />
                  <Point X="-2.962209472656" Y="2.847281982422" />
                  <Point X="-2.946077392578" Y="2.860229492188" />
                  <Point X="-2.907795898438" Y="2.898510986328" />
                  <Point X="-2.885353759766" Y="2.920952880859" />
                  <Point X="-2.872406738281" Y="2.937084228516" />
                  <Point X="-2.86077734375" Y="2.955338623047" />
                  <Point X="-2.851628662109" Y="2.973890136719" />
                  <Point X="-2.846712158203" Y="2.993981933594" />
                  <Point X="-2.843886962891" Y="3.015440673828" />
                  <Point X="-2.843435791016" Y="3.036120849609" />
                  <Point X="-2.848154296875" Y="3.090052978516" />
                  <Point X="-2.850920410156" Y="3.121670410156" />
                  <Point X="-2.854955810547" Y="3.141957763672" />
                  <Point X="-2.861464355469" Y="3.162600097656" />
                  <Point X="-2.869794921875" Y="3.181532958984" />
                  <Point X="-3.0091796875" Y="3.422954101562" />
                  <Point X="-3.183333007812" Y="3.724596435547" />
                  <Point X="-2.910992675781" Y="3.933396972656" />
                  <Point X="-2.700625732422" Y="4.09468359375" />
                  <Point X="-2.298767822266" Y="4.317947265625" />
                  <Point X="-2.167037109375" Y="4.391133789062" />
                  <Point X="-2.04319519043" Y="4.229740722656" />
                  <Point X="-2.028892700195" Y="4.214799804688" />
                  <Point X="-2.012312866211" Y="4.200887207031" />
                  <Point X="-1.99511340332" Y="4.189395019531" />
                  <Point X="-1.935087158203" Y="4.158146972656" />
                  <Point X="-1.899897338867" Y="4.139828125" />
                  <Point X="-1.8806171875" Y="4.132330566406" />
                  <Point X="-1.859710571289" Y="4.126729003906" />
                  <Point X="-1.839267578125" Y="4.123582519531" />
                  <Point X="-1.818628295898" Y="4.124935546875" />
                  <Point X="-1.797312988281" Y="4.128693847656" />
                  <Point X="-1.777453613281" Y="4.134481933594" />
                  <Point X="-1.714932128906" Y="4.160379394531" />
                  <Point X="-1.678279663086" Y="4.175561523437" />
                  <Point X="-1.660146118164" Y="4.185509765625" />
                  <Point X="-1.642416381836" Y="4.197923828125" />
                  <Point X="-1.626864135742" Y="4.211562988281" />
                  <Point X="-1.614632568359" Y="4.228244628906" />
                  <Point X="-1.603810791016" Y="4.246988769531" />
                  <Point X="-1.59548046875" Y="4.265921386719" />
                  <Point X="-1.575130981445" Y="4.330461914062" />
                  <Point X="-1.563201171875" Y="4.368297851562" />
                  <Point X="-1.559165649414" Y="4.388584960938" />
                  <Point X="-1.557279174805" Y="4.410146972656" />
                  <Point X="-1.55773034668" Y="4.430826660156" />
                  <Point X="-1.573577026367" Y="4.551192871094" />
                  <Point X="-1.584202026367" Y="4.631897949219" />
                  <Point X="-1.221968383789" Y="4.733455566406" />
                  <Point X="-0.949635009766" Y="4.80980859375" />
                  <Point X="-0.462466705322" Y="4.86682421875" />
                  <Point X="-0.294710784912" Y="4.886457519531" />
                  <Point X="-0.255728775024" Y="4.740974609375" />
                  <Point X="-0.133903305054" Y="4.286315429688" />
                  <Point X="-0.121129844666" Y="4.258124023438" />
                  <Point X="-0.10327155304" Y="4.231397460938" />
                  <Point X="-0.0821145401" Y="4.20880859375" />
                  <Point X="-0.054819446564" Y="4.19421875" />
                  <Point X="-0.024381370544" Y="4.183886230469" />
                  <Point X="0.006155909061" Y="4.178844238281" />
                  <Point X="0.03669316864" Y="4.183886230469" />
                  <Point X="0.067131248474" Y="4.19421875" />
                  <Point X="0.094426467896" Y="4.20880859375" />
                  <Point X="0.115583503723" Y="4.231397460938" />
                  <Point X="0.133441802979" Y="4.258124023438" />
                  <Point X="0.146215255737" Y="4.286315429688" />
                  <Point X="0.21763432312" Y="4.55285546875" />
                  <Point X="0.307419342041" Y="4.8879375" />
                  <Point X="0.606247497559" Y="4.856642089844" />
                  <Point X="0.844041259766" Y="4.831738769531" />
                  <Point X="1.247093261719" Y="4.7344296875" />
                  <Point X="1.481026489258" Y="4.677951171875" />
                  <Point X="1.742827636719" Y="4.582993652344" />
                  <Point X="1.894645019531" Y="4.527928222656" />
                  <Point X="2.148325195313" Y="4.409290527344" />
                  <Point X="2.294576416016" Y="4.340893554688" />
                  <Point X="2.539666503906" Y="4.198103515625" />
                  <Point X="2.680977539062" Y="4.115775390625" />
                  <Point X="2.912109619141" Y="3.951407470703" />
                  <Point X="2.943260253906" Y="3.929254882812" />
                  <Point X="2.69076953125" Y="3.491927978516" />
                  <Point X="2.147581054688" Y="2.551097900391" />
                  <Point X="2.142075927734" Y="2.539931152344" />
                  <Point X="2.133076904297" Y="2.516056884766" />
                  <Point X="2.119541992188" Y="2.465442626953" />
                  <Point X="2.111607177734" Y="2.435770507813" />
                  <Point X="2.108619384766" Y="2.417934326172" />
                  <Point X="2.107728027344" Y="2.380952880859" />
                  <Point X="2.113005615234" Y="2.337186035156" />
                  <Point X="2.116099365234" Y="2.311528076172" />
                  <Point X="2.121442138672" Y="2.289604980469" />
                  <Point X="2.129708251953" Y="2.267516357422" />
                  <Point X="2.140071044922" Y="2.247471191406" />
                  <Point X="2.167152587891" Y="2.207560058594" />
                  <Point X="2.183028808594" Y="2.184162597656" />
                  <Point X="2.19446484375" Y="2.170328857422" />
                  <Point X="2.221598876953" Y="2.145592529297" />
                  <Point X="2.261510009766" Y="2.118510986328" />
                  <Point X="2.284907470703" Y="2.102635009766" />
                  <Point X="2.304952636719" Y="2.092271972656" />
                  <Point X="2.327040771484" Y="2.084006103516" />
                  <Point X="2.348963867188" Y="2.078663574219" />
                  <Point X="2.392730712891" Y="2.073385986328" />
                  <Point X="2.418388671875" Y="2.070291992188" />
                  <Point X="2.436467529297" Y="2.069845703125" />
                  <Point X="2.473206542969" Y="2.074171142578" />
                  <Point X="2.523820800781" Y="2.087706054688" />
                  <Point X="2.553492919922" Y="2.095640869141" />
                  <Point X="2.565284179688" Y="2.099638427734" />
                  <Point X="2.588533935547" Y="2.110145019531" />
                  <Point X="3.140086914062" Y="2.428584228516" />
                  <Point X="3.967326416016" Y="2.906191162109" />
                  <Point X="4.039449951172" Y="2.805956054688" />
                  <Point X="4.123270019531" Y="2.689465087891" />
                  <Point X="4.252124511719" Y="2.476532226562" />
                  <Point X="4.262198730469" Y="2.459884277344" />
                  <Point X="3.945942382812" Y="2.217211914062" />
                  <Point X="3.230783935547" Y="1.668451171875" />
                  <Point X="3.221423828125" Y="1.660240600586" />
                  <Point X="3.203973876953" Y="1.641627685547" />
                  <Point X="3.167546630859" Y="1.594105834961" />
                  <Point X="3.146191650391" Y="1.566246459961" />
                  <Point X="3.136605712891" Y="1.550911621094" />
                  <Point X="3.121629882812" Y="1.517086181641" />
                  <Point X="3.108060791016" Y="1.468565917969" />
                  <Point X="3.100105957031" Y="1.440121582031" />
                  <Point X="3.096652587891" Y="1.417823242188" />
                  <Point X="3.095836425781" Y="1.394253173828" />
                  <Point X="3.097739501953" Y="1.371768188477" />
                  <Point X="3.108878417969" Y="1.317783325195" />
                  <Point X="3.115408447266" Y="1.286135375977" />
                  <Point X="3.120679931641" Y="1.268977539062" />
                  <Point X="3.136282714844" Y="1.235740478516" />
                  <Point X="3.166579101562" Y="1.189691040039" />
                  <Point X="3.184340332031" Y="1.16269519043" />
                  <Point X="3.198893798828" Y="1.145450195312" />
                  <Point X="3.216137451172" Y="1.129360595703" />
                  <Point X="3.234347167969" Y="1.116034790039" />
                  <Point X="3.278250976562" Y="1.091320922852" />
                  <Point X="3.303989257812" Y="1.076832519531" />
                  <Point X="3.320521240234" Y="1.069501586914" />
                  <Point X="3.356118164062" Y="1.059438598633" />
                  <Point X="3.415479003906" Y="1.051593261719" />
                  <Point X="3.450278808594" Y="1.046994018555" />
                  <Point X="3.462702148438" Y="1.046175048828" />
                  <Point X="3.488203613281" Y="1.04698449707" />
                  <Point X="4.012142089844" Y="1.115962158203" />
                  <Point X="4.776839355469" Y="1.21663671875" />
                  <Point X="4.809935058594" Y="1.080689453125" />
                  <Point X="4.845936035156" Y="0.932809448242" />
                  <Point X="4.886541015625" Y="0.67200958252" />
                  <Point X="4.890864746094" Y="0.644238525391" />
                  <Point X="4.536166992188" Y="0.549197631836" />
                  <Point X="3.716579833984" Y="0.32958984375" />
                  <Point X="3.704791259766" Y="0.325586517334" />
                  <Point X="3.681545654297" Y="0.315068054199" />
                  <Point X="3.623225341797" Y="0.281357818604" />
                  <Point X="3.589035644531" Y="0.261595489502" />
                  <Point X="3.574310791016" Y="0.251095825195" />
                  <Point X="3.547531005859" Y="0.225576522827" />
                  <Point X="3.512538818359" Y="0.180988388062" />
                  <Point X="3.492025146484" Y="0.154848876953" />
                  <Point X="3.480301025391" Y="0.135568908691" />
                  <Point X="3.47052734375" Y="0.11410559082" />
                  <Point X="3.463680908203" Y="0.09260433197" />
                  <Point X="3.452016845703" Y="0.031698884964" />
                  <Point X="3.445178710938" Y="-0.004006318092" />
                  <Point X="3.443483154297" Y="-0.021875085831" />
                  <Point X="3.445178710938" Y="-0.058553718567" />
                  <Point X="3.456842773438" Y="-0.11945916748" />
                  <Point X="3.463680908203" Y="-0.155164367676" />
                  <Point X="3.47052734375" Y="-0.176665634155" />
                  <Point X="3.480301025391" Y="-0.198128952026" />
                  <Point X="3.492024902344" Y="-0.217408905029" />
                  <Point X="3.527017089844" Y="-0.261997192383" />
                  <Point X="3.547530761719" Y="-0.288136566162" />
                  <Point X="3.559998779297" Y="-0.301235473633" />
                  <Point X="3.589035644531" Y="-0.324155517578" />
                  <Point X="3.647355957031" Y="-0.357865722656" />
                  <Point X="3.681545654297" Y="-0.377628082275" />
                  <Point X="3.692710205078" Y="-0.383138977051" />
                  <Point X="3.716579833984" Y="-0.392150024414" />
                  <Point X="4.197055175781" Y="-0.520893066406" />
                  <Point X="4.891472167969" Y="-0.706961425781" />
                  <Point X="4.875124023438" Y="-0.815397399902" />
                  <Point X="4.855022460938" Y="-0.948725524902" />
                  <Point X="4.803000488281" Y="-1.176693237305" />
                  <Point X="4.801173339844" Y="-1.184698974609" />
                  <Point X="4.374872558594" Y="-1.128575439453" />
                  <Point X="3.424382080078" Y="-1.003440979004" />
                  <Point X="3.408035644531" Y="-1.002710266113" />
                  <Point X="3.374658691406" Y="-1.005508666992" />
                  <Point X="3.260196777344" Y="-1.030387451172" />
                  <Point X="3.193094482422" Y="-1.044972290039" />
                  <Point X="3.163974121094" Y="-1.056596923828" />
                  <Point X="3.136147460938" Y="-1.073489257812" />
                  <Point X="3.112397460938" Y="-1.093960083008" />
                  <Point X="3.043212402344" Y="-1.17716809082" />
                  <Point X="3.002653320312" Y="-1.225948242188" />
                  <Point X="2.987932617188" Y="-1.250330688477" />
                  <Point X="2.976589355469" Y="-1.277715942383" />
                  <Point X="2.969757568359" Y="-1.305365478516" />
                  <Point X="2.959841552734" Y="-1.413123535156" />
                  <Point X="2.954028564453" Y="-1.476295776367" />
                  <Point X="2.956347167969" Y="-1.507564086914" />
                  <Point X="2.964078613281" Y="-1.539185058594" />
                  <Point X="2.976450195312" Y="-1.567996582031" />
                  <Point X="3.039795166016" Y="-1.666525512695" />
                  <Point X="3.076930419922" Y="-1.724287109375" />
                  <Point X="3.086932861328" Y="-1.737239013672" />
                  <Point X="3.110628417969" Y="-1.760909057617" />
                  <Point X="3.556512695312" Y="-2.103048095703" />
                  <Point X="4.213122070312" Y="-2.606882568359" />
                  <Point X="4.181473632812" Y="-2.658094238281" />
                  <Point X="4.1248046875" Y="-2.749793701172" />
                  <Point X="4.028981445312" Y="-2.8859453125" />
                  <Point X="3.647494628906" Y="-2.665693603516" />
                  <Point X="2.800954589844" Y="-2.176943115234" />
                  <Point X="2.786131591797" Y="-2.170012451172" />
                  <Point X="2.754224609375" Y="-2.159825195312" />
                  <Point X="2.617996582031" Y="-2.13522265625" />
                  <Point X="2.538134277344" Y="-2.120799560547" />
                  <Point X="2.506781982422" Y="-2.120395263672" />
                  <Point X="2.474609130859" Y="-2.125353027344" />
                  <Point X="2.444833251953" Y="-2.135176757812" />
                  <Point X="2.331661132812" Y="-2.19473828125" />
                  <Point X="2.265315185547" Y="-2.229655761719" />
                  <Point X="2.242384033203" Y="-2.246549072266" />
                  <Point X="2.221424316406" Y="-2.267509033203" />
                  <Point X="2.204531982422" Y="-2.290439208984" />
                  <Point X="2.144970214844" Y="-2.403611083984" />
                  <Point X="2.110052978516" Y="-2.469957275391" />
                  <Point X="2.100229003906" Y="-2.499733398438" />
                  <Point X="2.095271240234" Y="-2.531906005859" />
                  <Point X="2.095675537109" Y="-2.563258300781" />
                  <Point X="2.120278076172" Y="-2.699486328125" />
                  <Point X="2.134701171875" Y="-2.779348632812" />
                  <Point X="2.138985595703" Y="-2.795141845703" />
                  <Point X="2.151819091797" Y="-2.826078613281" />
                  <Point X="2.438344482422" Y="-3.322354980469" />
                  <Point X="2.861283203125" Y="-4.054906494141" />
                  <Point X="2.849089355469" Y="-4.063616210938" />
                  <Point X="2.78184765625" Y="-4.111645507812" />
                  <Point X="2.701765380859" Y="-4.163481445312" />
                  <Point X="2.404534423828" Y="-3.776123046875" />
                  <Point X="1.758546020508" Y="-2.934255126953" />
                  <Point X="1.747503540039" Y="-2.922179443359" />
                  <Point X="1.721924194336" Y="-2.900557373047" />
                  <Point X="1.587566650391" Y="-2.814177734375" />
                  <Point X="1.50880078125" Y="-2.763538574219" />
                  <Point X="1.479989746094" Y="-2.751167236328" />
                  <Point X="1.448368408203" Y="-2.743435546875" />
                  <Point X="1.417099487305" Y="-2.741116699219" />
                  <Point X="1.270156494141" Y="-2.754638427734" />
                  <Point X="1.184012573242" Y="-2.762565673828" />
                  <Point X="1.156362670898" Y="-2.769397460938" />
                  <Point X="1.128977661133" Y="-2.780740722656" />
                  <Point X="1.104595947266" Y="-2.795461181641" />
                  <Point X="0.991130310059" Y="-2.889803955078" />
                  <Point X="0.924612121582" Y="-2.945111572266" />
                  <Point X="0.904141052246" Y="-2.968861816406" />
                  <Point X="0.887248779297" Y="-2.996688476562" />
                  <Point X="0.875624328613" Y="-3.025808837891" />
                  <Point X="0.841698730469" Y="-3.181893554688" />
                  <Point X="0.821810058594" Y="-3.273396728516" />
                  <Point X="0.819724487305" Y="-3.289627441406" />
                  <Point X="0.819742248535" Y="-3.323120117188" />
                  <Point X="0.900941162109" Y="-3.939888427734" />
                  <Point X="1.022065246582" Y="-4.859915039062" />
                  <Point X="0.975676879883" Y="-4.870083496094" />
                  <Point X="0.929315551758" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058435424805" Y="-4.752634765625" />
                  <Point X="-1.141246337891" Y="-4.731328125" />
                  <Point X="-1.120775634766" Y="-4.575838378906" />
                  <Point X="-1.120077514648" Y="-4.568099609375" />
                  <Point X="-1.119451782227" Y="-4.541033691406" />
                  <Point X="-1.121759277344" Y="-4.509328125" />
                  <Point X="-1.123334106445" Y="-4.497690429688" />
                  <Point X="-1.1614921875" Y="-4.305858398438" />
                  <Point X="-1.183861572266" Y="-4.193398925781" />
                  <Point X="-1.188125" Y="-4.17846875" />
                  <Point X="-1.19902734375" Y="-4.149502441406" />
                  <Point X="-1.205666015625" Y="-4.135466308594" />
                  <Point X="-1.221738037109" Y="-4.10762890625" />
                  <Point X="-1.23057409668" Y="-4.094861816406" />
                  <Point X="-1.250208618164" Y="-4.070937011719" />
                  <Point X="-1.261006958008" Y="-4.059779296875" />
                  <Point X="-1.408059570312" Y="-3.930817138672" />
                  <Point X="-1.494267578125" Y="-3.855214355469" />
                  <Point X="-1.506739135742" Y="-3.84596484375" />
                  <Point X="-1.533022094727" Y="-3.82962109375" />
                  <Point X="-1.546833374023" Y="-3.822526855469" />
                  <Point X="-1.576530761719" Y="-3.810225830078" />
                  <Point X="-1.591313354492" Y="-3.805476074219" />
                  <Point X="-1.621454956055" Y="-3.798447998047" />
                  <Point X="-1.636814086914" Y="-3.796169677734" />
                  <Point X="-1.831985839844" Y="-3.783377441406" />
                  <Point X="-1.946403320312" Y="-3.775878173828" />
                  <Point X="-1.961928100586" Y="-3.776132324219" />
                  <Point X="-1.992729614258" Y="-3.779166015625" />
                  <Point X="-2.008006103516" Y="-3.781945556641" />
                  <Point X="-2.03905480957" Y="-3.790265136719" />
                  <Point X="-2.053674560547" Y="-3.795496337891" />
                  <Point X="-2.081865478516" Y="-3.808269775391" />
                  <Point X="-2.095436767578" Y="-3.815812011719" />
                  <Point X="-2.258064453125" Y="-3.924476318359" />
                  <Point X="-2.353403320312" Y="-3.988180175781" />
                  <Point X="-2.359681640625" Y="-3.992757080078" />
                  <Point X="-2.380442138672" Y="-4.010131103516" />
                  <Point X="-2.402759277344" Y="-4.032771484375" />
                  <Point X="-2.410471435547" Y="-4.041629638672" />
                  <Point X="-2.493587890625" Y="-4.149948730469" />
                  <Point X="-2.503203125" Y="-4.162479492188" />
                  <Point X="-2.595598388672" Y="-4.105270507813" />
                  <Point X="-2.747583984375" Y="-4.011164550781" />
                  <Point X="-2.980862792969" Y="-3.831547607422" />
                  <Point X="-2.810796630859" Y="-3.536984375" />
                  <Point X="-2.341488769531" Y="-2.724119628906" />
                  <Point X="-2.334850097656" Y="-2.710083251953" />
                  <Point X="-2.323947998047" Y="-2.681116943359" />
                  <Point X="-2.319684570312" Y="-2.666187011719" />
                  <Point X="-2.313413574219" Y="-2.634660644531" />
                  <Point X="-2.311638916016" Y="-2.619235351562" />
                  <Point X="-2.310626220703" Y="-2.588301269531" />
                  <Point X="-2.314666259766" Y="-2.557615234375" />
                  <Point X="-2.323650878906" Y="-2.527997558594" />
                  <Point X="-2.329357421875" Y="-2.513557128906" />
                  <Point X="-2.343574462891" Y="-2.484728271484" />
                  <Point X="-2.351557128906" Y="-2.47141015625" />
                  <Point X="-2.369584716797" Y="-2.446253173828" />
                  <Point X="-2.379629638672" Y="-2.434414306641" />
                  <Point X="-2.396978271484" Y="-2.417065673828" />
                  <Point X="-2.408817138672" Y="-2.407020751953" />
                  <Point X="-2.433974121094" Y="-2.388993164062" />
                  <Point X="-2.447292236328" Y="-2.381010498047" />
                  <Point X="-2.47612109375" Y="-2.366793457031" />
                  <Point X="-2.490561523438" Y="-2.361086914062" />
                  <Point X="-2.520179199219" Y="-2.352102294922" />
                  <Point X="-2.550865234375" Y="-2.348062255859" />
                  <Point X="-2.581799316406" Y="-2.349074951172" />
                  <Point X="-2.597224609375" Y="-2.350849609375" />
                  <Point X="-2.628750976562" Y="-2.357120605469" />
                  <Point X="-2.643680908203" Y="-2.361384033203" />
                  <Point X="-2.672647216797" Y="-2.372286132812" />
                  <Point X="-2.68668359375" Y="-2.378924804688" />
                  <Point X="-3.163218017578" Y="-2.654052246094" />
                  <Point X="-3.793089599609" Y="-3.017708496094" />
                  <Point X="-3.883939697266" Y="-2.898350097656" />
                  <Point X="-4.004013183594" Y="-2.740598388672" />
                  <Point X="-4.181265136719" Y="-2.443373535156" />
                  <Point X="-3.869258544922" Y="-2.203962158203" />
                  <Point X="-3.048122314453" Y="-1.573881958008" />
                  <Point X="-3.036481689453" Y="-1.563309448242" />
                  <Point X="-3.015104003906" Y="-1.540388793945" />
                  <Point X="-3.005366943359" Y="-1.528040649414" />
                  <Point X="-2.987402099609" Y="-1.500909057617" />
                  <Point X="-2.979835693359" Y="-1.487124511719" />
                  <Point X="-2.967080078125" Y="-1.458495727539" />
                  <Point X="-2.961890869141" Y="-1.443651489258" />
                  <Point X="-2.954186279297" Y="-1.413904052734" />
                  <Point X="-2.951552734375" Y="-1.398802734375" />
                  <Point X="-2.948748535156" Y="-1.368373535156" />
                  <Point X="-2.948577880859" Y="-1.353045654297" />
                  <Point X="-2.950786865234" Y="-1.321375488281" />
                  <Point X="-2.953083740234" Y="-1.306219604492" />
                  <Point X="-2.960084960938" Y="-1.276474975586" />
                  <Point X="-2.971778808594" Y="-1.248243530273" />
                  <Point X="-2.987860839844" Y="-1.222260253906" />
                  <Point X="-2.996953369141" Y="-1.209919555664" />
                  <Point X="-3.017785644531" Y="-1.185963134766" />
                  <Point X="-3.028745117188" Y="-1.175244995117" />
                  <Point X="-3.052245117188" Y="-1.1557109375" />
                  <Point X="-3.064785644531" Y="-1.146895019531" />
                  <Point X="-3.091268310547" Y="-1.13130859375" />
                  <Point X="-3.10543359375" Y="-1.124481445312" />
                  <Point X="-3.134696533203" Y="-1.113257446289" />
                  <Point X="-3.149794189453" Y="-1.108860595703" />
                  <Point X="-3.181682128906" Y="-1.102378662109" />
                  <Point X="-3.197298583984" Y="-1.100532470703" />
                  <Point X="-3.228622070312" Y="-1.09944140625" />
                  <Point X="-3.244329101562" Y="-1.100196533203" />
                  <Point X="-3.845908935547" Y="-1.179395874023" />
                  <Point X="-4.660920410156" Y="-1.286694335938" />
                  <Point X="-4.693797851563" Y="-1.157981689453" />
                  <Point X="-4.740762207031" Y="-0.974118225098" />
                  <Point X="-4.786452636719" Y="-0.654654418945" />
                  <Point X="-4.443686035156" Y="-0.562810302734" />
                  <Point X="-3.508288085938" Y="-0.312171447754" />
                  <Point X="-3.500475585938" Y="-0.309712493896" />
                  <Point X="-3.473930908203" Y="-0.299251068115" />
                  <Point X="-3.444167236328" Y="-0.2838934021" />
                  <Point X="-3.433562011719" Y="-0.277513916016" />
                  <Point X="-3.416065917969" Y="-0.265370697021" />
                  <Point X="-3.393672363281" Y="-0.248243087769" />
                  <Point X="-3.371216796875" Y="-0.226359100342" />
                  <Point X="-3.360898193359" Y="-0.214484130859" />
                  <Point X="-3.341654296875" Y="-0.188228713989" />
                  <Point X="-3.333437255859" Y="-0.174814743042" />
                  <Point X="-3.319328857422" Y="-0.146813751221" />
                  <Point X="-3.3134375" Y="-0.1322265625" />
                  <Point X="-3.304186523438" Y="-0.102419639587" />
                  <Point X="-3.300990478516" Y="-0.088500511169" />
                  <Point X="-3.296721191406" Y="-0.060340927124" />
                  <Point X="-3.295647949219" Y="-0.046100624084" />
                  <Point X="-3.295647949219" Y="-0.01645941925" />
                  <Point X="-3.296721191406" Y="-0.002219115257" />
                  <Point X="-3.300990478516" Y="0.025940467834" />
                  <Point X="-3.304186767578" Y="0.039859745026" />
                  <Point X="-3.313437744141" Y="0.069666519165" />
                  <Point X="-3.319328613281" Y="0.084253257751" />
                  <Point X="-3.333437011719" Y="0.112254562378" />
                  <Point X="-3.341654296875" Y="0.125668968201" />
                  <Point X="-3.360898193359" Y="0.151924087524" />
                  <Point X="-3.371216308594" Y="0.163798477173" />
                  <Point X="-3.393671386719" Y="0.185682159424" />
                  <Point X="-3.40580859375" Y="0.195691604614" />
                  <Point X="-3.4233046875" Y="0.207834838867" />
                  <Point X="-3.439406005859" Y="0.217857818604" />
                  <Point X="-3.465230957031" Y="0.232196960449" />
                  <Point X="-3.475728027344" Y="0.237210754395" />
                  <Point X="-3.497256591797" Y="0.245917831421" />
                  <Point X="-3.508288085938" Y="0.249611251831" />
                  <Point X="-4.056656738281" Y="0.396545959473" />
                  <Point X="-4.785446289062" Y="0.591824768066" />
                  <Point X="-4.761597167969" Y="0.752994384766" />
                  <Point X="-4.731331054688" Y="0.957529296875" />
                  <Point X="-4.636472167969" Y="1.307588134766" />
                  <Point X="-4.633586425781" Y="1.318237060547" />
                  <Point X="-4.44858203125" Y="1.293880859375" />
                  <Point X="-3.778066162109" Y="1.20560546875" />
                  <Point X="-3.76773828125" Y="1.204815429688" />
                  <Point X="-3.747057861328" Y="1.204364257812" />
                  <Point X="-3.736705322266" Y="1.20470324707" />
                  <Point X="-3.715143554688" Y="1.20658984375" />
                  <Point X="-3.704890136719" Y="1.208053588867" />
                  <Point X="-3.684603759766" Y="1.212088989258" />
                  <Point X="-3.674570800781" Y="1.214660400391" />
                  <Point X="-3.635846435547" Y="1.226869995117" />
                  <Point X="-3.613144775391" Y="1.234028076172" />
                  <Point X="-3.603450439453" Y="1.237676513672" />
                  <Point X="-3.584517089844" Y="1.246007568359" />
                  <Point X="-3.575278320312" Y="1.250689575195" />
                  <Point X="-3.556534179688" Y="1.261511474609" />
                  <Point X="-3.547859130859" Y="1.267172119141" />
                  <Point X="-3.531178466797" Y="1.279403076172" />
                  <Point X="-3.515929199219" Y="1.293376586914" />
                  <Point X="-3.502291015625" Y="1.308927734375" />
                  <Point X="-3.495896240234" Y="1.317075805664" />
                  <Point X="-3.483481689453" Y="1.334805297852" />
                  <Point X="-3.478011962891" Y="1.343601318359" />
                  <Point X="-3.4680625" Y="1.361736572266" />
                  <Point X="-3.463582763672" Y="1.371076049805" />
                  <Point X="-3.448044433594" Y="1.408588989258" />
                  <Point X="-3.438935302734" Y="1.430580322266" />
                  <Point X="-3.435499023438" Y="1.4403515625" />
                  <Point X="-3.429710693359" Y="1.460210571289" />
                  <Point X="-3.427358642578" Y="1.470298217773" />
                  <Point X="-3.423600341797" Y="1.49161340332" />
                  <Point X="-3.422360595703" Y="1.501897338867" />
                  <Point X="-3.421008056641" Y="1.522537353516" />
                  <Point X="-3.421910400391" Y="1.543200683594" />
                  <Point X="-3.425056884766" Y="1.56364440918" />
                  <Point X="-3.427188232422" Y="1.573780639648" />
                  <Point X="-3.432790039062" Y="1.594687011719" />
                  <Point X="-3.436012207031" Y="1.604530761719" />
                  <Point X="-3.443509033203" Y="1.623808959961" />
                  <Point X="-3.447783691406" Y="1.633243408203" />
                  <Point X="-3.466532226563" Y="1.669259155273" />
                  <Point X="-3.4775234375" Y="1.690373168945" />
                  <Point X="-3.482799804688" Y="1.699286376953" />
                  <Point X="-3.494291992188" Y="1.716485717773" />
                  <Point X="-3.500507568359" Y="1.724771606445" />
                  <Point X="-3.514420166016" Y="1.741351928711" />
                  <Point X="-3.521501708984" Y="1.748912475586" />
                  <Point X="-3.536443359375" Y="1.763215698242" />
                  <Point X="-3.544303710938" Y="1.769958618164" />
                  <Point X="-3.858849121094" Y="2.011317749023" />
                  <Point X="-4.227614257812" Y="2.294282226563" />
                  <Point X="-4.119893554687" Y="2.478834228516" />
                  <Point X="-4.002293212891" Y="2.680312011719" />
                  <Point X="-3.751019042969" Y="3.003289550781" />
                  <Point X="-3.726338623047" Y="3.035012695312" />
                  <Point X="-3.665969238281" Y="3.000158447266" />
                  <Point X="-3.254156982422" Y="2.7623984375" />
                  <Point X="-3.244917480469" Y="2.757716064453" />
                  <Point X="-3.225984619141" Y="2.749385498047" />
                  <Point X="-3.216291259766" Y="2.745737304688" />
                  <Point X="-3.195648925781" Y="2.739228759766" />
                  <Point X="-3.185615234375" Y="2.736657226562" />
                  <Point X="-3.165327880859" Y="2.732621826172" />
                  <Point X="-3.15507421875" Y="2.731157958984" />
                  <Point X="-3.101142089844" Y="2.726439453125" />
                  <Point X="-3.069524902344" Y="2.723673339844" />
                  <Point X="-3.059173095703" Y="2.723334472656" />
                  <Point X="-3.038492675781" Y="2.723785644531" />
                  <Point X="-3.0281640625" Y="2.724575683594" />
                  <Point X="-3.006705566406" Y="2.727400878906" />
                  <Point X="-2.996525146484" Y="2.729310791016" />
                  <Point X="-2.976433837891" Y="2.734227294922" />
                  <Point X="-2.956998046875" Y="2.741301513672" />
                  <Point X="-2.938447021484" Y="2.750449707031" />
                  <Point X="-2.929420898438" Y="2.755530273438" />
                  <Point X="-2.911166503906" Y="2.767159423828" />
                  <Point X="-2.902746337891" Y="2.773193359375" />
                  <Point X="-2.886614257812" Y="2.786140869141" />
                  <Point X="-2.87890234375" Y="2.793054443359" />
                  <Point X="-2.840620849609" Y="2.8313359375" />
                  <Point X="-2.818178710938" Y="2.853777832031" />
                  <Point X="-2.811265380859" Y="2.861489501953" />
                  <Point X="-2.798318359375" Y="2.877620849609" />
                  <Point X="-2.792284667969" Y="2.886040527344" />
                  <Point X="-2.780655273438" Y="2.904294921875" />
                  <Point X="-2.775574707031" Y="2.913320800781" />
                  <Point X="-2.766426025391" Y="2.931872314453" />
                  <Point X="-2.759351318359" Y="2.951309570312" />
                  <Point X="-2.754434814453" Y="2.971401367188" />
                  <Point X="-2.752524902344" Y="2.981581542969" />
                  <Point X="-2.749699707031" Y="3.003040283203" />
                  <Point X="-2.748909667969" Y="3.013368652344" />
                  <Point X="-2.748458496094" Y="3.034048828125" />
                  <Point X="-2.748797363281" Y="3.044400634766" />
                  <Point X="-2.753515869141" Y="3.098332763672" />
                  <Point X="-2.756281982422" Y="3.129950195312" />
                  <Point X="-2.757745849609" Y="3.140203857422" />
                  <Point X="-2.76178125" Y="3.160491210938" />
                  <Point X="-2.764352783203" Y="3.170524902344" />
                  <Point X="-2.770861328125" Y="3.191167236328" />
                  <Point X="-2.774509521484" Y="3.200860595703" />
                  <Point X="-2.782840087891" Y="3.219793457031" />
                  <Point X="-2.787522460938" Y="3.229032958984" />
                  <Point X="-2.926907226562" Y="3.470454101562" />
                  <Point X="-3.059387207031" Y="3.699916259766" />
                  <Point X="-2.853190673828" Y="3.858005126953" />
                  <Point X="-2.648374023438" Y="4.015036376953" />
                  <Point X="-2.252630371094" Y="4.234903320312" />
                  <Point X="-2.192525390625" Y="4.268296386719" />
                  <Point X="-2.118563720703" Y="4.171908203125" />
                  <Point X="-2.111820556641" Y="4.164047851562" />
                  <Point X="-2.097518066406" Y="4.149106933594" />
                  <Point X="-2.089958740234" Y="4.142026367188" />
                  <Point X="-2.07337890625" Y="4.128113769531" />
                  <Point X="-2.065091552734" Y="4.121897460938" />
                  <Point X="-2.047892211914" Y="4.110405273438" />
                  <Point X="-2.038979858398" Y="4.105129394531" />
                  <Point X="-1.978953857422" Y="4.073881103516" />
                  <Point X="-1.943763916016" Y="4.055562255859" />
                  <Point X="-1.934328613281" Y="4.051287353516" />
                  <Point X="-1.915048461914" Y="4.043789794922" />
                  <Point X="-1.905203491211" Y="4.040567138672" />
                  <Point X="-1.88429699707" Y="4.034965576172" />
                  <Point X="-1.874162353516" Y="4.032834716797" />
                  <Point X="-1.853719360352" Y="4.029688232422" />
                  <Point X="-1.833053100586" Y="4.028785888672" />
                  <Point X="-1.812413818359" Y="4.030138916016" />
                  <Point X="-1.802132568359" Y="4.031378662109" />
                  <Point X="-1.780817138672" Y="4.035136962891" />
                  <Point X="-1.770731079102" Y="4.037488525391" />
                  <Point X="-1.750871582031" Y="4.043276611328" />
                  <Point X="-1.741098388672" Y="4.046713378906" />
                  <Point X="-1.678576904297" Y="4.072610839844" />
                  <Point X="-1.641924438477" Y="4.08779296875" />
                  <Point X="-1.632586303711" Y="4.092272216797" />
                  <Point X="-1.614452636719" Y="4.102220214844" />
                  <Point X="-1.605657592773" Y="4.107689453125" />
                  <Point X="-1.587927856445" Y="4.120103515625" />
                  <Point X="-1.579777954102" Y="4.126499511719" />
                  <Point X="-1.564225708008" Y="4.140138671875" />
                  <Point X="-1.550252075195" Y="4.155388183594" />
                  <Point X="-1.538020507812" Y="4.172069824219" />
                  <Point X="-1.532359741211" Y="4.180745117188" />
                  <Point X="-1.521538085938" Y="4.199489257812" />
                  <Point X="-1.516855834961" Y="4.208728515625" />
                  <Point X="-1.508525634766" Y="4.227661132812" />
                  <Point X="-1.504877319336" Y="4.237354492188" />
                  <Point X="-1.484527832031" Y="4.301895019531" />
                  <Point X="-1.472598022461" Y="4.339730957031" />
                  <Point X="-1.470026733398" Y="4.349763671875" />
                  <Point X="-1.465991210938" Y="4.37005078125" />
                  <Point X="-1.46452722168" Y="4.380305175781" />
                  <Point X="-1.46264074707" Y="4.4018671875" />
                  <Point X="-1.462301757812" Y="4.412219238281" />
                  <Point X="-1.462753051758" Y="4.432898925781" />
                  <Point X="-1.46354309082" Y="4.4432265625" />
                  <Point X="-1.479266357422" Y="4.562654785156" />
                  <Point X="-1.196322631836" Y="4.641982421875" />
                  <Point X="-0.931175720215" Y="4.716320800781" />
                  <Point X="-0.451423736572" Y="4.772468261719" />
                  <Point X="-0.365221984863" Y="4.782557128906" />
                  <Point X="-0.347491729736" Y="4.71638671875" />
                  <Point X="-0.225666290283" Y="4.261727539062" />
                  <Point X="-0.220435211182" Y="4.247107910156" />
                  <Point X="-0.207661819458" Y="4.218916503906" />
                  <Point X="-0.20011920166" Y="4.205344726562" />
                  <Point X="-0.182260971069" Y="4.178618164062" />
                  <Point X="-0.172608352661" Y="4.166456054688" />
                  <Point X="-0.151451293945" Y="4.1438671875" />
                  <Point X="-0.126898040771" Y="4.125026367188" />
                  <Point X="-0.099602851868" Y="4.110436523438" />
                  <Point X="-0.085356750488" Y="4.104260742188" />
                  <Point X="-0.054918632507" Y="4.093927978516" />
                  <Point X="-0.039857185364" Y="4.090155273438" />
                  <Point X="-0.009319933891" Y="4.08511328125" />
                  <Point X="0.021631828308" Y="4.08511328125" />
                  <Point X="0.052169078827" Y="4.090155273438" />
                  <Point X="0.067230377197" Y="4.093927978516" />
                  <Point X="0.097668495178" Y="4.104260742188" />
                  <Point X="0.111914451599" Y="4.110436523438" />
                  <Point X="0.139209793091" Y="4.125026367188" />
                  <Point X="0.163763183594" Y="4.143866699219" />
                  <Point X="0.184920257568" Y="4.166455566406" />
                  <Point X="0.194572860718" Y="4.178617675781" />
                  <Point X="0.212431091309" Y="4.205344238281" />
                  <Point X="0.219973709106" Y="4.218916503906" />
                  <Point X="0.232747253418" Y="4.247107910156" />
                  <Point X="0.237978179932" Y="4.261727539062" />
                  <Point X="0.309397277832" Y="4.528267578125" />
                  <Point X="0.378190246582" Y="4.785006347656" />
                  <Point X="0.596352600098" Y="4.762158691406" />
                  <Point X="0.827876708984" Y="4.737912109375" />
                  <Point X="1.224797973633" Y="4.642083007812" />
                  <Point X="1.453595703125" Y="4.586844238281" />
                  <Point X="1.710435180664" Y="4.493686523438" />
                  <Point X="1.858257568359" Y="4.440069824219" />
                  <Point X="2.108080322266" Y="4.323236328125" />
                  <Point X="2.250453125" Y="4.256653320312" />
                  <Point X="2.491843505859" Y="4.116018554688" />
                  <Point X="2.629433105469" Y="4.035858398438" />
                  <Point X="2.817780029297" Y="3.901916748047" />
                  <Point X="2.608497070312" Y="3.539427978516" />
                  <Point X="2.06530859375" Y="2.598597900391" />
                  <Point X="2.062373046875" Y="2.593104980469" />
                  <Point X="2.053181396484" Y="2.573438476562" />
                  <Point X="2.044182250977" Y="2.549564208984" />
                  <Point X="2.041301635742" Y="2.540598876953" />
                  <Point X="2.027766723633" Y="2.489984619141" />
                  <Point X="2.01983190918" Y="2.4603125" />
                  <Point X="2.017912597656" Y="2.451465576172" />
                  <Point X="2.013646972656" Y="2.420223388672" />
                  <Point X="2.012755615234" Y="2.383241943359" />
                  <Point X="2.013411254883" Y="2.369579833984" />
                  <Point X="2.018688842773" Y="2.325812988281" />
                  <Point X="2.021782592773" Y="2.300155029297" />
                  <Point X="2.02380078125" Y="2.289034423828" />
                  <Point X="2.029143554687" Y="2.267111328125" />
                  <Point X="2.032468261719" Y="2.256308837891" />
                  <Point X="2.040734375" Y="2.234220214844" />
                  <Point X="2.045318359375" Y="2.223889160156" />
                  <Point X="2.055681152344" Y="2.203843994141" />
                  <Point X="2.061459960938" Y="2.194129882813" />
                  <Point X="2.088541503906" Y="2.15421875" />
                  <Point X="2.104417724609" Y="2.130821289063" />
                  <Point X="2.10980859375" Y="2.123633300781" />
                  <Point X="2.130463134766" Y="2.100123535156" />
                  <Point X="2.157597167969" Y="2.075387207031" />
                  <Point X="2.168257568359" Y="2.066981445312" />
                  <Point X="2.208168701172" Y="2.039899902344" />
                  <Point X="2.231566162109" Y="2.024023925781" />
                  <Point X="2.241279296875" Y="2.018245361328" />
                  <Point X="2.261324462891" Y="2.007882446289" />
                  <Point X="2.271656494141" Y="2.003297973633" />
                  <Point X="2.293744628906" Y="1.995032104492" />
                  <Point X="2.304548095703" Y="1.991707275391" />
                  <Point X="2.326471191406" Y="1.986364746094" />
                  <Point X="2.337590820312" Y="1.984346801758" />
                  <Point X="2.381357666016" Y="1.979069213867" />
                  <Point X="2.407015625" Y="1.975975219727" />
                  <Point X="2.416044189453" Y="1.975320922852" />
                  <Point X="2.447575439453" Y="1.975497314453" />
                  <Point X="2.484314453125" Y="1.979822631836" />
                  <Point X="2.497748535156" Y="1.982395996094" />
                  <Point X="2.548362792969" Y="1.995930786133" />
                  <Point X="2.578034912109" Y="2.003865600586" />
                  <Point X="2.583995117188" Y="2.005670776367" />
                  <Point X="2.604405761719" Y="2.013067749023" />
                  <Point X="2.627655517578" Y="2.02357434082" />
                  <Point X="2.636033935547" Y="2.027872558594" />
                  <Point X="3.187586914062" Y="2.346311767578" />
                  <Point X="3.940404785156" Y="2.780951416016" />
                  <Point X="3.962337402344" Y="2.750470214844" />
                  <Point X="4.043950195312" Y="2.637047119141" />
                  <Point X="4.136884765625" Y="2.483472167969" />
                  <Point X="3.888110107422" Y="2.292580322266" />
                  <Point X="3.172951660156" Y="1.743819702148" />
                  <Point X="3.168137451172" Y="1.739868408203" />
                  <Point X="3.152118408203" Y="1.725215698242" />
                  <Point X="3.134668457031" Y="1.706602783203" />
                  <Point X="3.128576660156" Y="1.699422485352" />
                  <Point X="3.092149414062" Y="1.651900634766" />
                  <Point X="3.070794433594" Y="1.624041259766" />
                  <Point X="3.065635742188" Y="1.616602661133" />
                  <Point X="3.049738769531" Y="1.58937097168" />
                  <Point X="3.034762939453" Y="1.555545654297" />
                  <Point X="3.030140136719" Y="1.54267199707" />
                  <Point X="3.016571044922" Y="1.494151733398" />
                  <Point X="3.008616210938" Y="1.465707397461" />
                  <Point X="3.006225097656" Y="1.454661010742" />
                  <Point X="3.002771728516" Y="1.432362670898" />
                  <Point X="3.001709472656" Y="1.421110839844" />
                  <Point X="3.000893310547" Y="1.397540771484" />
                  <Point X="3.001174804688" Y="1.386241210938" />
                  <Point X="3.003077880859" Y="1.363756225586" />
                  <Point X="3.004699462891" Y="1.352570800781" />
                  <Point X="3.015838378906" Y="1.2985859375" />
                  <Point X="3.022368408203" Y="1.266937988281" />
                  <Point X="3.02459765625" Y="1.258235229492" />
                  <Point X="3.034684082031" Y="1.228607788086" />
                  <Point X="3.050286865234" Y="1.195370727539" />
                  <Point X="3.056918701172" Y="1.183526123047" />
                  <Point X="3.087215087891" Y="1.13747668457" />
                  <Point X="3.104976318359" Y="1.110480712891" />
                  <Point X="3.111738769531" Y="1.101425048828" />
                  <Point X="3.126292236328" Y="1.084180053711" />
                  <Point X="3.134083251953" Y="1.075991088867" />
                  <Point X="3.151326904297" Y="1.059901367188" />
                  <Point X="3.160034667969" Y="1.052695922852" />
                  <Point X="3.178244384766" Y="1.039370117188" />
                  <Point X="3.187746582031" Y="1.033249633789" />
                  <Point X="3.231650390625" Y="1.008535705566" />
                  <Point X="3.257388671875" Y="0.994047363281" />
                  <Point X="3.265479003906" Y="0.989988037109" />
                  <Point X="3.294678222656" Y="0.978084228516" />
                  <Point X="3.330275146484" Y="0.968021240234" />
                  <Point X="3.343670898438" Y="0.965257629395" />
                  <Point X="3.403031738281" Y="0.957412231445" />
                  <Point X="3.437831542969" Y="0.952812988281" />
                  <Point X="3.444029785156" Y="0.952199768066" />
                  <Point X="3.465716064453" Y="0.951222839355" />
                  <Point X="3.491217529297" Y="0.952032226562" />
                  <Point X="3.500603515625" Y="0.952797241211" />
                  <Point X="4.024541992188" Y="1.021774902344" />
                  <Point X="4.704703613281" Y="1.11132019043" />
                  <Point X="4.717630859375" Y="1.058218505859" />
                  <Point X="4.75268359375" Y="0.914234191895" />
                  <Point X="4.78387109375" Y="0.713920898438" />
                  <Point X="4.511579101562" Y="0.640960632324" />
                  <Point X="3.691991943359" Y="0.421352752686" />
                  <Point X="3.686031738281" Y="0.419544311523" />
                  <Point X="3.665627197266" Y="0.412138122559" />
                  <Point X="3.642381591797" Y="0.401619659424" />
                  <Point X="3.634004394531" Y="0.397316711426" />
                  <Point X="3.575684082031" Y="0.36360635376" />
                  <Point X="3.541494384766" Y="0.343844116211" />
                  <Point X="3.533880859375" Y="0.338945068359" />
                  <Point X="3.508773681641" Y="0.319870056152" />
                  <Point X="3.481993896484" Y="0.294350769043" />
                  <Point X="3.472797119141" Y="0.284226715088" />
                  <Point X="3.437804931641" Y="0.239638504028" />
                  <Point X="3.417291259766" Y="0.213499008179" />
                  <Point X="3.410854736328" Y="0.204208450317" />
                  <Point X="3.399130615234" Y="0.184928482056" />
                  <Point X="3.393843017578" Y="0.174939086914" />
                  <Point X="3.384069335938" Y="0.153475723267" />
                  <Point X="3.380005615234" Y="0.142929580688" />
                  <Point X="3.373159179688" Y="0.12142830658" />
                  <Point X="3.370376464844" Y="0.110473167419" />
                  <Point X="3.358712402344" Y="0.049567646027" />
                  <Point X="3.351874267578" Y="0.013862478256" />
                  <Point X="3.350603515625" Y="0.004967842579" />
                  <Point X="3.348584472656" Y="-0.026261991501" />
                  <Point X="3.350280029297" Y="-0.062940639496" />
                  <Point X="3.351874267578" Y="-0.076422523499" />
                  <Point X="3.363538330078" Y="-0.137328033447" />
                  <Point X="3.370376464844" Y="-0.173033203125" />
                  <Point X="3.373159179688" Y="-0.183988357544" />
                  <Point X="3.380005615234" Y="-0.205489624023" />
                  <Point X="3.384069335938" Y="-0.216035766602" />
                  <Point X="3.393843017578" Y="-0.237499130249" />
                  <Point X="3.399130126953" Y="-0.247487792969" />
                  <Point X="3.410854003906" Y="-0.266767730713" />
                  <Point X="3.417291015625" Y="-0.276058898926" />
                  <Point X="3.452283203125" Y="-0.320647277832" />
                  <Point X="3.472796875" Y="-0.346786621094" />
                  <Point X="3.478718994141" Y="-0.353634124756" />
                  <Point X="3.501138671875" Y="-0.375804046631" />
                  <Point X="3.530175537109" Y="-0.398724090576" />
                  <Point X="3.541494384766" Y="-0.406404174805" />
                  <Point X="3.599814697266" Y="-0.440114379883" />
                  <Point X="3.634004394531" Y="-0.45987677002" />
                  <Point X="3.639496582031" Y="-0.462815338135" />
                  <Point X="3.659157958984" Y="-0.472016723633" />
                  <Point X="3.683027587891" Y="-0.481027740479" />
                  <Point X="3.691991943359" Y="-0.48391293335" />
                  <Point X="4.172467285156" Y="-0.612656066895" />
                  <Point X="4.784876953125" Y="-0.776750549316" />
                  <Point X="4.781185546875" Y="-0.801234924316" />
                  <Point X="4.76161328125" Y="-0.931051208496" />
                  <Point X="4.727801757812" Y="-1.079219604492" />
                  <Point X="4.387272460938" Y="-1.034388183594" />
                  <Point X="3.436781982422" Y="-0.909253723145" />
                  <Point X="3.428624511719" Y="-0.908535705566" />
                  <Point X="3.400098388672" Y="-0.908042480469" />
                  <Point X="3.366721435547" Y="-0.910840881348" />
                  <Point X="3.354481201172" Y="-0.912676208496" />
                  <Point X="3.240019287109" Y="-0.937554931641" />
                  <Point X="3.172916992188" Y="-0.952139770508" />
                  <Point X="3.157873779297" Y="-0.956742492676" />
                  <Point X="3.128753417969" Y="-0.968367126465" />
                  <Point X="3.114676269531" Y="-0.975388977051" />
                  <Point X="3.086849609375" Y="-0.992281311035" />
                  <Point X="3.074124023438" Y="-1.001530273438" />
                  <Point X="3.050374023438" Y="-1.022001159668" />
                  <Point X="3.039349609375" Y="-1.033222900391" />
                  <Point X="2.970164550781" Y="-1.116430908203" />
                  <Point X="2.92960546875" Y="-1.16521105957" />
                  <Point X="2.921326171875" Y="-1.17684753418" />
                  <Point X="2.90660546875" Y="-1.201229980469" />
                  <Point X="2.9001640625" Y="-1.213976074219" />
                  <Point X="2.888820800781" Y="-1.241361328125" />
                  <Point X="2.884362792969" Y="-1.254928222656" />
                  <Point X="2.877531005859" Y="-1.282577880859" />
                  <Point X="2.875157226562" Y="-1.29666027832" />
                  <Point X="2.865241210938" Y="-1.404418334961" />
                  <Point X="2.859428222656" Y="-1.467590576172" />
                  <Point X="2.859288574219" Y="-1.483320800781" />
                  <Point X="2.861607177734" Y="-1.514589233398" />
                  <Point X="2.864065429688" Y="-1.530127319336" />
                  <Point X="2.871796875" Y="-1.561748291016" />
                  <Point X="2.876785888672" Y="-1.576668212891" />
                  <Point X="2.889157470703" Y="-1.605479614258" />
                  <Point X="2.896540039062" Y="-1.619371337891" />
                  <Point X="2.959885009766" Y="-1.717900268555" />
                  <Point X="2.997020263672" Y="-1.775661865234" />
                  <Point X="3.001741943359" Y="-1.782353393555" />
                  <Point X="3.019793945312" Y="-1.804450317383" />
                  <Point X="3.043489501953" Y="-1.828120361328" />
                  <Point X="3.052796142578" Y="-1.83627746582" />
                  <Point X="3.498680419922" Y="-2.178416748047" />
                  <Point X="4.087170166016" Y="-2.629981201172" />
                  <Point X="4.045483642578" Y="-2.697437011719" />
                  <Point X="4.001274414063" Y="-2.760251953125" />
                  <Point X="3.694994628906" Y="-2.583421142578" />
                  <Point X="2.848454589844" Y="-2.094670654297" />
                  <Point X="2.841191894531" Y="-2.090885253906" />
                  <Point X="2.815026123047" Y="-2.079513183594" />
                  <Point X="2.783119140625" Y="-2.069325927734" />
                  <Point X="2.771108398438" Y="-2.066337646484" />
                  <Point X="2.634880371094" Y="-2.041734985352" />
                  <Point X="2.555018066406" Y="-2.027311889648" />
                  <Point X="2.539359130859" Y="-2.025807495117" />
                  <Point X="2.508006835938" Y="-2.025403198242" />
                  <Point X="2.492313476562" Y="-2.026503540039" />
                  <Point X="2.460140625" Y="-2.031461303711" />
                  <Point X="2.444844482422" Y="-2.035136108398" />
                  <Point X="2.415068603516" Y="-2.044959960938" />
                  <Point X="2.400588867188" Y="-2.051108642578" />
                  <Point X="2.287416748047" Y="-2.110670166016" />
                  <Point X="2.221070800781" Y="-2.145587646484" />
                  <Point X="2.208968505859" Y="-2.153170166016" />
                  <Point X="2.186037353516" Y="-2.170063476562" />
                  <Point X="2.175208496094" Y="-2.179374267578" />
                  <Point X="2.154248779297" Y="-2.200334228516" />
                  <Point X="2.144938232422" Y="-2.211162841797" />
                  <Point X="2.128045898438" Y="-2.234093017578" />
                  <Point X="2.120464111328" Y="-2.246194580078" />
                  <Point X="2.06090234375" Y="-2.359366455078" />
                  <Point X="2.025984985352" Y="-2.425712646484" />
                  <Point X="2.019836303711" Y="-2.440192382812" />
                  <Point X="2.010012329102" Y="-2.469968505859" />
                  <Point X="2.006337280273" Y="-2.485264892578" />
                  <Point X="2.001379516602" Y="-2.5174375" />
                  <Point X="2.000279174805" Y="-2.533130859375" />
                  <Point X="2.00068347168" Y="-2.564483154297" />
                  <Point X="2.002187866211" Y="-2.580142089844" />
                  <Point X="2.026790405273" Y="-2.716370117188" />
                  <Point X="2.041213500977" Y="-2.796232421875" />
                  <Point X="2.043015014648" Y="-2.804221435547" />
                  <Point X="2.051236083984" Y="-2.83154296875" />
                  <Point X="2.064069580078" Y="-2.862479736328" />
                  <Point X="2.069546630859" Y="-2.873578613281" />
                  <Point X="2.356072021484" Y="-3.369854980469" />
                  <Point X="2.735893066406" Y="-4.027724609375" />
                  <Point X="2.723754394531" Y="-4.036083496094" />
                  <Point X="2.479902832031" Y="-3.718290771484" />
                  <Point X="1.833914550781" Y="-2.876422851562" />
                  <Point X="1.828653442383" Y="-2.870146240234" />
                  <Point X="1.808831665039" Y="-2.849626953125" />
                  <Point X="1.783252319336" Y="-2.828004882812" />
                  <Point X="1.773299072266" Y="-2.820647216797" />
                  <Point X="1.63894152832" Y="-2.734267578125" />
                  <Point X="1.56017578125" Y="-2.683628417969" />
                  <Point X="1.546283935547" Y="-2.676245849609" />
                  <Point X="1.517472900391" Y="-2.663874511719" />
                  <Point X="1.502553344727" Y="-2.658885742188" />
                  <Point X="1.470932006836" Y="-2.651154052734" />
                  <Point X="1.455394165039" Y="-2.648695800781" />
                  <Point X="1.424125244141" Y="-2.646376953125" />
                  <Point X="1.408394287109" Y="-2.646516357422" />
                  <Point X="1.261451293945" Y="-2.660038085938" />
                  <Point X="1.175307373047" Y="-2.667965332031" />
                  <Point X="1.161224975586" Y="-2.670339111328" />
                  <Point X="1.133575195312" Y="-2.677170898438" />
                  <Point X="1.12000769043" Y="-2.68162890625" />
                  <Point X="1.092622680664" Y="-2.692972167969" />
                  <Point X="1.079876464844" Y="-2.699413818359" />
                  <Point X="1.055494750977" Y="-2.714134277344" />
                  <Point X="1.043859008789" Y="-2.722413085938" />
                  <Point X="0.930393432617" Y="-2.816755859375" />
                  <Point X="0.863875183105" Y="-2.872063476562" />
                  <Point X="0.852653259277" Y="-2.883087890625" />
                  <Point X="0.832182250977" Y="-2.906838134766" />
                  <Point X="0.822932983398" Y="-2.919564208984" />
                  <Point X="0.806040771484" Y="-2.947390869141" />
                  <Point X="0.799018676758" Y="-2.961468261719" />
                  <Point X="0.787394287109" Y="-2.990588623047" />
                  <Point X="0.782791870117" Y="-3.005631347656" />
                  <Point X="0.748866333008" Y="-3.161716064453" />
                  <Point X="0.728977600098" Y="-3.253219238281" />
                  <Point X="0.727584838867" Y="-3.261289306641" />
                  <Point X="0.72472442627" Y="-3.289677734375" />
                  <Point X="0.72474230957" Y="-3.323170410156" />
                  <Point X="0.725554931641" Y="-3.335520019531" />
                  <Point X="0.80675390625" Y="-3.952288330078" />
                  <Point X="0.833091186523" Y="-4.152339355469" />
                  <Point X="0.655065063477" Y="-3.487936523438" />
                  <Point X="0.652606201172" Y="-3.480124511719" />
                  <Point X="0.642145263672" Y="-3.453580566406" />
                  <Point X="0.62678717041" Y="-3.423815917969" />
                  <Point X="0.620407653809" Y="-3.413210205078" />
                  <Point X="0.517190368652" Y="-3.264493408203" />
                  <Point X="0.456680053711" Y="-3.177309814453" />
                  <Point X="0.44667074585" Y="-3.165172851562" />
                  <Point X="0.424786773682" Y="-3.142717529297" />
                  <Point X="0.412912261963" Y="-3.132399169922" />
                  <Point X="0.38665713501" Y="-3.113155273438" />
                  <Point X="0.373243011475" Y="-3.104938232422" />
                  <Point X="0.345241851807" Y="-3.090829833984" />
                  <Point X="0.330654815674" Y="-3.084938476562" />
                  <Point X="0.170931716919" Y="-3.035366210938" />
                  <Point X="0.077295715332" Y="-3.006305175781" />
                  <Point X="0.063376434326" Y="-3.003109130859" />
                  <Point X="0.035216854095" Y="-2.99883984375" />
                  <Point X="0.020976697922" Y="-2.997766601562" />
                  <Point X="-0.008664654732" Y="-2.997766601562" />
                  <Point X="-0.022905107498" Y="-2.998840087891" />
                  <Point X="-0.05106439209" Y="-3.003109375" />
                  <Point X="-0.064983520508" Y="-3.006305175781" />
                  <Point X="-0.224706634521" Y="-3.055877197266" />
                  <Point X="-0.31834262085" Y="-3.084938476562" />
                  <Point X="-0.332929382324" Y="-3.090829589844" />
                  <Point X="-0.36093081665" Y="-3.104937988281" />
                  <Point X="-0.374345367432" Y="-3.113155273438" />
                  <Point X="-0.400600494385" Y="-3.132399169922" />
                  <Point X="-0.412475189209" Y="-3.142717773438" />
                  <Point X="-0.434359008789" Y="-3.165173095703" />
                  <Point X="-0.444368011475" Y="-3.177309814453" />
                  <Point X="-0.547585449219" Y="-3.326026367188" />
                  <Point X="-0.60809564209" Y="-3.413210205078" />
                  <Point X="-0.612470336914" Y="-3.420132324219" />
                  <Point X="-0.625976745605" Y="-3.445265136719" />
                  <Point X="-0.638777770996" Y="-3.476215820312" />
                  <Point X="-0.642752990723" Y="-3.487936523438" />
                  <Point X="-0.790799255371" Y="-4.040452636719" />
                  <Point X="-0.985425109863" Y="-4.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.538353095457" Y="1.305699400793" />
                  <Point X="-4.746078136962" Y="0.581276090701" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.086949341461" Y="2.535275621023" />
                  <Point X="-4.168958918635" Y="2.249274237052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.443119766542" Y="1.293161736125" />
                  <Point X="-4.654301213341" Y="0.556684508286" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.913984289811" Y="2.793820688963" />
                  <Point X="-4.087953872986" Y="2.18711665181" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.347886460782" Y="1.280623990707" />
                  <Point X="-4.56252428972" Y="0.532092925871" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.757468809272" Y="2.994999285017" />
                  <Point X="-4.006948827337" Y="2.124959066567" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.252653155021" Y="1.268086245288" />
                  <Point X="-4.470747366099" Y="0.507501343456" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.658412100613" Y="2.995795330092" />
                  <Point X="-3.925943781689" Y="2.062801481325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.157419849261" Y="1.255548499869" />
                  <Point X="-4.378970442479" Y="0.482909761041" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.698396046951" Y="-0.63105970573" />
                  <Point X="-4.759399857275" Y="-0.843805274982" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.573621034796" Y="2.946841166271" />
                  <Point X="-3.844938726323" Y="2.000643929971" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.062186543501" Y="1.24301075445" />
                  <Point X="-4.287193518858" Y="0.458318178627" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.591342312807" Y="-0.602374718468" />
                  <Point X="-4.720586837827" Y="-1.05310394181" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.488829968978" Y="2.89788700245" />
                  <Point X="-3.763933624087" Y="1.938486542072" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.966953237741" Y="1.230473009032" />
                  <Point X="-4.195416595237" Y="0.433726596212" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.484288578662" Y="-0.573689731206" />
                  <Point X="-4.674026568459" Y="-1.235384737371" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.404038903161" Y="2.848932838629" />
                  <Point X="-3.682928521851" Y="1.876329154173" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.871719931981" Y="1.217935263613" />
                  <Point X="-4.103639671616" Y="0.409135013797" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.377234855423" Y="-0.545004781978" />
                  <Point X="-4.587125079588" Y="-1.276978981353" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.319247837343" Y="2.799978674809" />
                  <Point X="-3.601923419615" Y="1.814171766274" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.776462180385" Y="1.205482770954" />
                  <Point X="-4.011862739115" Y="0.384543462351" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.270181138848" Y="-0.516319855989" />
                  <Point X="-4.484419411587" Y="-1.263457502761" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.935026340541" Y="3.79526252093" />
                  <Point X="-2.994562257512" Y="3.587636104157" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.233924925653" Y="2.752879277964" />
                  <Point X="-3.521741855169" Y="1.74914236079" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.675036301579" Y="1.214541094218" />
                  <Point X="-3.9200857973" Y="0.359951943386" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.163127422273" Y="-0.487634929999" />
                  <Point X="-4.381713743585" Y="-1.249936024168" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.808348369728" Y="3.892385354597" />
                  <Point X="-2.928529496588" Y="3.473263956908" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.141661456793" Y="2.729984480444" />
                  <Point X="-3.453176403878" Y="1.643602754509" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.563970395656" Y="1.257218187291" />
                  <Point X="-3.828308855485" Y="0.335360424421" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.056073705698" Y="-0.45895000401" />
                  <Point X="-4.279008075583" Y="-1.236414545576" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.681670351116" Y="3.98950835496" />
                  <Point X="-2.862496694974" Y="3.358891951564" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.044649010447" Y="2.723651335602" />
                  <Point X="-3.73653191367" Y="0.310768905456" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.949019989123" Y="-0.430265078021" />
                  <Point X="-4.176302407581" Y="-1.222893066983" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.561716466447" Y="4.063181513487" />
                  <Point X="-2.796463892335" Y="3.244519949793" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.938076442759" Y="2.750658296014" />
                  <Point X="-3.644754971855" Y="0.286177386491" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.841966272548" Y="-0.401580152032" />
                  <Point X="-4.073596739579" Y="-1.209371588391" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.44416014992" Y="4.128493358249" />
                  <Point X="-2.750251812825" Y="3.061024871893" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.805356640537" Y="2.868851499804" />
                  <Point X="-3.55297803004" Y="0.261585867527" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.734912555973" Y="-0.372895226043" />
                  <Point X="-3.970891071577" Y="-1.195850109799" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.326603833392" Y="4.193805203011" />
                  <Point X="-3.462941266881" Y="0.230925624383" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.627858839398" Y="-0.344210300054" />
                  <Point X="-3.868185403575" Y="-1.182328631206" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.209047519535" Y="4.259117038459" />
                  <Point X="-3.380708608314" Y="0.173049234163" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.520805122822" Y="-0.315525374064" />
                  <Point X="-3.765479746297" Y="-1.168807190009" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.11676364714" Y="-2.393879739699" />
                  <Point X="-4.147291344522" Y="-2.500342472486" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.130693140121" Y="4.187715481503" />
                  <Point X="-3.3124433743" Y="0.066462645816" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.405215352515" Y="-0.257071690997" />
                  <Point X="-3.662774091988" Y="-1.15528575917" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.990056003237" Y="-2.29665342367" />
                  <Point X="-4.080552743726" Y="-2.612253063571" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.053045657534" Y="4.113848682342" />
                  <Point X="-3.560068437679" Y="-1.14176432833" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.86334836086" Y="-2.199427112959" />
                  <Point X="-4.013814142931" Y="-2.724163654657" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.967402077997" Y="4.067867587182" />
                  <Point X="-3.45736278337" Y="-1.12824289749" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.736640749645" Y="-2.102200910925" />
                  <Point X="-3.942770045879" Y="-2.821059195911" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.878365725693" Y="4.03371849677" />
                  <Point X="-3.354657129061" Y="-1.11472146665" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.60993313843" Y="-2.004974708892" />
                  <Point X="-3.870984827508" Y="-2.91537013997" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.779009705405" Y="4.035558365547" />
                  <Point X="-3.251951474752" Y="-1.101200035811" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.483225527215" Y="-1.907748506858" />
                  <Point X="-3.799199632563" Y="-3.009681165727" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.668340842238" Y="4.076850805974" />
                  <Point X="-3.15501533182" Y="-1.107799282282" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.356517916001" Y="-1.810522304825" />
                  <Point X="-3.684734547188" Y="-2.955149725138" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.425921790466" Y="4.577610757124" />
                  <Point X="-1.463830297682" Y="4.445408081515" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.544898581055" Y="4.162689379143" />
                  <Point X="-3.067020280328" Y="-1.145579820186" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.229810304786" Y="-1.713296102792" />
                  <Point X="-3.566298774432" Y="-2.886770852023" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.318453601175" Y="4.60774112125" />
                  <Point X="-2.989529992298" Y="-1.219994821914" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.103102693571" Y="-1.616069900758" />
                  <Point X="-3.447863001676" Y="-2.818391978908" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.210985411884" Y="4.637871485375" />
                  <Point X="-3.329427228921" Y="-2.750013105794" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.103517184021" Y="4.668001984019" />
                  <Point X="-3.210991456165" Y="-2.681634232679" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.996048950063" Y="4.698132503917" />
                  <Point X="-3.09255566438" Y="-2.613255293205" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.890644917849" Y="4.721064296838" />
                  <Point X="-2.974119859732" Y="-2.544876308865" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.788384715221" Y="4.733032253049" />
                  <Point X="-2.855684055083" Y="-2.476497324526" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.686124512594" Y="4.74500020926" />
                  <Point X="-2.737248250434" Y="-2.408118340187" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.583864309966" Y="4.756968165472" />
                  <Point X="-2.623496752079" Y="-2.356075473278" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.481604107339" Y="4.768936121683" />
                  <Point X="-2.523407153045" Y="-2.351677311392" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.915083197629" Y="-3.717614006578" />
                  <Point X="-2.953741165191" Y="-3.852430361024" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.379343829955" Y="4.7809043386" />
                  <Point X="-2.435087498371" Y="-2.388325823467" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.718738872094" Y="-3.377535721203" />
                  <Point X="-2.872786294161" Y="-3.914762925958" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.324074957876" Y="4.628994049922" />
                  <Point X="-2.357646478431" Y="-2.462912643444" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.522394585974" Y="-3.037457573286" />
                  <Point X="-2.791831423132" Y="-3.977095490892" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.276335192794" Y="4.450826644755" />
                  <Point X="-2.709528691138" Y="-4.034727506031" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.228595427711" Y="4.272659239587" />
                  <Point X="-2.625601236526" Y="-4.086693440045" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.163123921923" Y="4.156329763072" />
                  <Point X="-2.541673778976" Y="-4.138659363809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.079769859699" Y="4.102364172162" />
                  <Point X="-2.417737948236" Y="-4.051099509039" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.014111973338" Y="4.08511328125" />
                  <Point X="-2.288416244992" Y="-3.944756884708" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.12170186411" Y="4.115668068875" />
                  <Point X="-2.166164719871" Y="-3.863070901681" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.411460850444" Y="4.781521991588" />
                  <Point X="-2.047306165988" Y="-3.793217615556" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.507407984822" Y="4.771473662401" />
                  <Point X="-1.943559168124" Y="-3.776064588161" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.603355122744" Y="4.761425345576" />
                  <Point X="-1.846553855041" Y="-3.782422609648" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.699302305695" Y="4.751377185783" />
                  <Point X="-1.749548552609" Y="-3.788780668281" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.795249488646" Y="4.74132902599" />
                  <Point X="-1.65254325206" Y="-3.795138733477" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.888875266911" Y="4.723185165965" />
                  <Point X="-1.560003912489" Y="-3.817071455494" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.981304882401" Y="4.700869790602" />
                  <Point X="-1.476564551237" Y="-3.870739573343" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.073734497891" Y="4.678554415239" />
                  <Point X="-1.397594675414" Y="-3.93999463923" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.166164113381" Y="4.656239039876" />
                  <Point X="-1.318624779748" Y="-4.009249635918" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.258593732723" Y="4.633923677947" />
                  <Point X="-1.240779462846" Y="-4.08242650483" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.351023358748" Y="4.611608339323" />
                  <Point X="-1.179729112798" Y="-4.214174383731" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.443452984773" Y="4.5892930007" />
                  <Point X="-1.13925162106" Y="-4.417668345856" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.533290605492" Y="4.557938265335" />
                  <Point X="-1.131111399299" Y="-4.733935770373" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.622808734563" Y="4.525469330181" />
                  <Point X="-1.038740986656" Y="-4.7564576106" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.71232686248" Y="4.493000391002" />
                  <Point X="-0.517181903776" Y="-3.28222068311" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.80184493694" Y="4.460531265399" />
                  <Point X="-0.368922140293" Y="-3.109833193959" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.890484497977" Y="4.424998399392" />
                  <Point X="-0.257544483836" Y="-3.066068897571" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.977627046757" Y="4.384244831219" />
                  <Point X="-0.149061588291" Y="-3.032399832201" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.064769595537" Y="4.343491263046" />
                  <Point X="-0.041414728394" Y="-3.001646369621" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.151912132451" Y="4.302737653491" />
                  <Point X="0.05726017385" Y="-3.002181841752" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.23905465764" Y="4.261984003046" />
                  <Point X="0.14856378524" Y="-3.028424060078" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.324058187252" Y="4.213770788532" />
                  <Point X="0.239315739597" Y="-3.056590135111" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.408739826647" Y="4.164435009424" />
                  <Point X="0.330067682078" Y="-3.084756251557" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.49342146593" Y="4.115099229926" />
                  <Point X="2.045558347545" Y="2.553214922005" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.032461079064" Y="2.507539318729" />
                  <Point X="0.414771483875" Y="-3.134014741181" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.57810309934" Y="4.065763429946" />
                  <Point X="2.248268661019" Y="2.915494045669" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.048300222107" Y="2.218121223494" />
                  <Point X="0.48817380371" Y="-3.222686182239" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.661763663698" Y="4.012866739008" />
                  <Point X="2.44461302996" Y="3.25557248242" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.117518349758" Y="2.114857770178" />
                  <Point X="0.558108882222" Y="-3.323449330768" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.743852746095" Y="3.954489639176" />
                  <Point X="2.640957402129" Y="3.595650930426" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.197021821383" Y="2.047463573999" />
                  <Point X="0.627668111771" Y="-3.425523220397" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.282068689695" Y="1.999401499493" />
                  <Point X="0.891874041269" Y="-2.84878339718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.733971924732" Y="-3.399453519105" />
                  <Point X="0.680925411004" Y="-3.584448697273" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.37527719128" Y="1.979802422745" />
                  <Point X="1.021641518338" Y="-2.740886174769" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.76506911669" Y="-3.635660474169" />
                  <Point X="0.728665189527" Y="-3.762616055569" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.473754956683" Y="1.978579452748" />
                  <Point X="1.13913385669" Y="-2.675797448422" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.796166308648" Y="-3.871867429233" />
                  <Point X="0.77640496805" Y="-3.940783413865" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.580005203136" Y="2.004462345429" />
                  <Point X="1.241967093078" Y="-2.661831085996" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.827263554019" Y="-4.108074198022" />
                  <Point X="0.824144746572" Y="-4.118950772162" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.69536958522" Y="2.06213000635" />
                  <Point X="1.343473954857" Y="-2.652490341543" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.813805372092" Y="2.130508928695" />
                  <Point X="1.443640406455" Y="-2.647824162913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.932241158965" Y="2.19888785104" />
                  <Point X="1.535630761558" Y="-2.671671421294" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.050676945837" Y="2.267266773385" />
                  <Point X="1.620001637033" Y="-2.722090962985" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.16911273271" Y="2.33564569573" />
                  <Point X="1.703446864663" Y="-2.775738622341" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.287548524984" Y="2.404024636914" />
                  <Point X="3.058016205585" Y="1.603550310913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.001161048452" Y="1.40527281472" />
                  <Point X="1.786500817471" Y="-2.830750819159" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.405984318257" Y="2.472403581579" />
                  <Point X="3.203868897259" Y="1.76754334307" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.043777266865" Y="1.209237478895" />
                  <Point X="1.861809028974" Y="-2.912775626087" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.524420111529" Y="2.540782526244" />
                  <Point X="3.330576512393" Y="1.864769558771" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.111704250978" Y="1.101471272958" />
                  <Point X="2.169219631572" Y="-2.185363201859" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.022989982278" Y="-2.695326592924" />
                  <Point X="1.933752612516" Y="-3.006534285162" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.642855904802" Y="2.609161470909" />
                  <Point X="3.457284127527" Y="1.961995774473" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.190522405687" Y="1.031687092662" />
                  <Point X="2.289830175725" Y="-2.109399999562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.070320925283" Y="-2.874919730111" />
                  <Point X="2.005696196058" Y="-3.100292944237" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.761291698074" Y="2.677540415574" />
                  <Point X="3.583991742661" Y="2.059221990174" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.276146786244" Y="0.985639042701" />
                  <Point X="2.406036754642" Y="-2.048795249239" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.136353703231" Y="-2.989291817988" />
                  <Point X="2.0776397796" Y="-3.194051603312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.879727491347" Y="2.74591936024" />
                  <Point X="3.710699357795" Y="2.156448205875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.368201313284" Y="0.962015578459" />
                  <Point X="2.511559626708" Y="-2.025449012503" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.20238648118" Y="-3.103663905865" />
                  <Point X="2.149583363142" Y="-3.287810262388" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.974867499779" Y="2.73305624837" />
                  <Point X="3.837406972929" Y="2.253674421577" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.463957704942" Y="0.951302050356" />
                  <Point X="2.607153986473" Y="-2.036727612969" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.268419259128" Y="-3.218035993742" />
                  <Point X="2.221526946685" Y="-3.381568921463" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.045453454328" Y="2.634562974337" />
                  <Point X="3.964114616374" Y="2.350900736011" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.565671240322" Y="0.961363551313" />
                  <Point X="2.969719932331" Y="-1.116965648" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.860394119713" Y="-1.498230066011" />
                  <Point X="2.701116490727" Y="-2.053697169917" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.334452037077" Y="-3.332408081619" />
                  <Point X="2.293470530227" Y="-3.475327580538" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.112508138072" Y="2.523754695489" />
                  <Point X="4.090822278706" Y="2.44812711631" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.668376893462" Y="0.974884978076" />
                  <Point X="3.468915587267" Y="0.279280737863" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.3578988413" Y="-0.107880665528" />
                  <Point X="3.107979047518" Y="-0.979454564167" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.915866155352" Y="-1.649431839155" />
                  <Point X="2.794428104487" Y="-2.072936651771" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.400484803762" Y="-3.446780208774" />
                  <Point X="2.365414113769" Y="-3.569086239613" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.771082546602" Y="0.988406404838" />
                  <Point X="3.595150467413" Y="0.374858330739" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.411075486146" Y="-0.267087417679" />
                  <Point X="3.217413094405" Y="-0.942468439865" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.984211584286" Y="-1.755738754581" />
                  <Point X="2.881546047439" Y="-2.113776030666" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.466517564965" Y="-3.56115235505" />
                  <Point X="2.437357697311" Y="-3.662844898688" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.873788199741" Y="1.0019278316" />
                  <Point X="3.708585945949" Y="0.425799105568" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.483680284606" Y="-0.358540146301" />
                  <Point X="3.322810439882" Y="-0.919559966367" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.058656488787" Y="-1.840774270815" />
                  <Point X="2.966337108878" Y="-2.162730209757" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.532550326168" Y="-3.675524501325" />
                  <Point X="2.509301297672" Y="-3.75660349911" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.976493852881" Y="1.015449258362" />
                  <Point X="3.815639678718" Y="0.454484088032" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.564903945845" Y="-0.419935328376" />
                  <Point X="3.424818910045" Y="-0.908469905589" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.139661577555" Y="-1.902931705683" />
                  <Point X="3.051128170317" Y="-2.211684388848" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.598583087371" Y="-3.789896647601" />
                  <Point X="2.581244922372" Y="-3.850362014649" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.079199518087" Y="1.028970727205" />
                  <Point X="3.922693411487" Y="0.483169070496" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.650024019758" Y="-0.46774210461" />
                  <Point X="3.520270833576" Y="-0.920245240239" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.220666666323" Y="-1.965089140551" />
                  <Point X="3.135919231756" Y="-2.260638567939" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.664615848574" Y="-3.904268793876" />
                  <Point X="2.653188547073" Y="-3.944120530188" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.181905193901" Y="1.042492233041" />
                  <Point X="4.029747144256" Y="0.51185405296" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.740489341189" Y="-0.49690778745" />
                  <Point X="3.615504150763" Y="-0.932782945805" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.30167175509" Y="-2.02724657542" />
                  <Point X="3.220710293194" Y="-2.30959274703" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.730648609777" Y="-4.018640940152" />
                  <Point X="2.726112703145" Y="-4.034459526457" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.284610869715" Y="1.056013738876" />
                  <Point X="4.136800877025" Y="0.540539035424" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.832266264938" Y="-0.521499369416" />
                  <Point X="3.710737467951" Y="-0.945320651371" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.382676843858" Y="-2.089404010288" />
                  <Point X="3.305501354633" Y="-2.358546926122" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.387316545529" Y="1.069535244712" />
                  <Point X="4.243854609794" Y="0.569224017888" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.924043188688" Y="-0.546090951382" />
                  <Point X="3.805970785139" Y="-0.957858356937" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.463681932626" Y="-2.151561445156" />
                  <Point X="3.390292416072" Y="-2.407501105213" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.490022221342" Y="1.083056750548" />
                  <Point X="4.350908342563" Y="0.597909000352" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.015820112437" Y="-0.570682533349" />
                  <Point X="3.901204102327" Y="-0.970396062504" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.544687020136" Y="-2.213718884411" />
                  <Point X="3.475083477511" Y="-2.456455284304" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.592727897156" Y="1.096578256383" />
                  <Point X="4.457962075332" Y="0.626593982816" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.107597036187" Y="-0.595274115315" />
                  <Point X="3.996437419514" Y="-0.98293376807" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.625692106689" Y="-2.275876327005" />
                  <Point X="3.55987453895" Y="-2.505409463395" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.69543357297" Y="1.110099762219" />
                  <Point X="4.565015795466" Y="0.655278921217" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.199373964493" Y="-0.61986568139" />
                  <Point X="4.091670736702" Y="-0.995471473636" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.706697193241" Y="-2.338033769598" />
                  <Point X="3.644665600389" Y="-2.554363642486" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.745986563298" Y="0.941743239405" />
                  <Point X="4.672069502922" Y="0.683963815406" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.291150903785" Y="-0.644457209152" />
                  <Point X="4.18690405389" Y="-1.008009179202" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.787702279794" Y="-2.400191212191" />
                  <Point X="3.729456662916" Y="-2.603317817783" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.782328691355" Y="0.723827550253" />
                  <Point X="4.779123210379" Y="0.712648709595" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.382927843078" Y="-0.669048736914" />
                  <Point X="4.282137371078" Y="-1.020546884768" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.868707366347" Y="-2.462348654784" />
                  <Point X="3.814247727032" Y="-2.652271987537" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.47470478237" Y="-0.693640264676" />
                  <Point X="4.377370688265" Y="-1.033084590334" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.949712452899" Y="-2.524506097377" />
                  <Point X="3.899038791148" Y="-2.701226157291" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.566481721662" Y="-0.718231792437" />
                  <Point X="4.472604013548" Y="-1.045622267669" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.030717539452" Y="-2.586663539971" />
                  <Point X="3.983829855264" Y="-2.750180327045" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.658258660955" Y="-0.742823320199" />
                  <Point X="4.567837339771" Y="-1.058159941727" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.750035600247" Y="-0.767414847961" />
                  <Point X="4.663070665994" Y="-1.070697615785" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="0.001625976562" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978485351562" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.724073974609" Y="-4.479585449219" />
                  <Point X="0.471539031982" Y="-3.537112304688" />
                  <Point X="0.464318664551" Y="-3.521544189453" />
                  <Point X="0.361101379395" Y="-3.372827392578" />
                  <Point X="0.300591033936" Y="-3.285643798828" />
                  <Point X="0.274335754395" Y="-3.266399902344" />
                  <Point X="0.114612724304" Y="-3.216827636719" />
                  <Point X="0.020976625443" Y="-3.187766601562" />
                  <Point X="-0.008664756775" Y="-3.187766601562" />
                  <Point X="-0.168387786865" Y="-3.237338623047" />
                  <Point X="-0.262023895264" Y="-3.266399902344" />
                  <Point X="-0.288279144287" Y="-3.285643798828" />
                  <Point X="-0.391496612549" Y="-3.434360351562" />
                  <Point X="-0.45200680542" Y="-3.521544189453" />
                  <Point X="-0.459227142334" Y="-3.537112304688" />
                  <Point X="-0.6072734375" Y="-4.089628417969" />
                  <Point X="-0.84774395752" Y="-4.987077148438" />
                  <Point X="-0.99094342041" Y="-4.95928125" />
                  <Point X="-1.100246582031" Y="-4.938065429688" />
                  <Point X="-1.285200805664" Y="-4.890478027344" />
                  <Point X="-1.351589599609" Y="-4.873396972656" />
                  <Point X="-1.339549560547" Y="-4.781943847656" />
                  <Point X="-1.309150146484" Y="-4.551038574219" />
                  <Point X="-1.309683227539" Y="-4.5347578125" />
                  <Point X="-1.347841186523" Y="-4.34292578125" />
                  <Point X="-1.370210693359" Y="-4.230466308594" />
                  <Point X="-1.386282714844" Y="-4.20262890625" />
                  <Point X="-1.533335449219" Y="-4.073666748047" />
                  <Point X="-1.619543457031" Y="-3.998063964844" />
                  <Point X="-1.649240722656" Y="-3.985762939453" />
                  <Point X="-1.844412353516" Y="-3.972970703125" />
                  <Point X="-1.958829833984" Y="-3.965471435547" />
                  <Point X="-1.989878540039" Y="-3.973791015625" />
                  <Point X="-2.152506103516" Y="-4.082455322266" />
                  <Point X="-2.247844970703" Y="-4.146159179688" />
                  <Point X="-2.259734130859" Y="-4.157294433594" />
                  <Point X="-2.342850585938" Y="-4.265613769531" />
                  <Point X="-2.457095214844" Y="-4.414500488281" />
                  <Point X="-2.695620605469" Y="-4.266811523438" />
                  <Point X="-2.855832275391" Y="-4.167612304688" />
                  <Point X="-3.111941894531" Y="-3.970416259766" />
                  <Point X="-3.228581054688" Y="-3.880608154297" />
                  <Point X="-2.975341552734" Y="-3.441984375" />
                  <Point X="-2.506033691406" Y="-2.629119628906" />
                  <Point X="-2.499762695312" Y="-2.597593261719" />
                  <Point X="-2.513979736328" Y="-2.568764404297" />
                  <Point X="-2.531328369141" Y="-2.551415771484" />
                  <Point X="-2.560157226562" Y="-2.537198730469" />
                  <Point X="-2.59168359375" Y="-2.543469726562" />
                  <Point X="-3.068218017578" Y="-2.818597167969" />
                  <Point X="-3.842959472656" Y="-3.265894042969" />
                  <Point X="-4.035126220703" Y="-3.013426269531" />
                  <Point X="-4.161703613281" Y="-2.847129394531" />
                  <Point X="-4.345312988281" Y="-2.539244384766" />
                  <Point X="-4.431020019531" Y="-2.395527099609" />
                  <Point X="-3.984923095703" Y="-2.053224853516" />
                  <Point X="-3.163786865234" Y="-1.423144897461" />
                  <Point X="-3.145822021484" Y="-1.396013305664" />
                  <Point X="-3.138117431641" Y="-1.366265869141" />
                  <Point X="-3.140326416016" Y="-1.334595703125" />
                  <Point X="-3.161158691406" Y="-1.310639404297" />
                  <Point X="-3.187641357422" Y="-1.295052856445" />
                  <Point X="-3.219529296875" Y="-1.288571044922" />
                  <Point X="-3.821109130859" Y="-1.367770507812" />
                  <Point X="-4.803283691406" Y="-1.497076293945" />
                  <Point X="-4.877887695312" Y="-1.20500378418" />
                  <Point X="-4.927393066406" Y="-1.011191650391" />
                  <Point X="-4.975971679688" Y="-0.671535583496" />
                  <Point X="-4.998396484375" Y="-0.514742126465" />
                  <Point X="-4.492861816406" Y="-0.379284423828" />
                  <Point X="-3.557463867188" Y="-0.128645431519" />
                  <Point X="-3.541895751953" Y="-0.121424926758" />
                  <Point X="-3.524399658203" Y="-0.109281784058" />
                  <Point X="-3.514142822266" Y="-0.102162895203" />
                  <Point X="-3.494898925781" Y="-0.075907623291" />
                  <Point X="-3.485647949219" Y="-0.046100738525" />
                  <Point X="-3.485647949219" Y="-0.016459356308" />
                  <Point X="-3.494898925781" Y="0.013347529411" />
                  <Point X="-3.514142822266" Y="0.039602802277" />
                  <Point X="-3.531638916016" Y="0.05174609375" />
                  <Point X="-3.557463867188" Y="0.066085334778" />
                  <Point X="-4.105832519531" Y="0.213020095825" />
                  <Point X="-4.998186523438" Y="0.452125793457" />
                  <Point X="-4.949550292969" Y="0.780806640625" />
                  <Point X="-4.91764453125" Y="0.996421875" />
                  <Point X="-4.819858886719" Y="1.357281982422" />
                  <Point X="-4.773516601562" Y="1.528298706055" />
                  <Point X="-4.423782226562" Y="1.482255371094" />
                  <Point X="-3.753266357422" Y="1.393980102539" />
                  <Point X="-3.731704589844" Y="1.395866699219" />
                  <Point X="-3.692980224609" Y="1.408076416016" />
                  <Point X="-3.670278564453" Y="1.41523425293" />
                  <Point X="-3.651534423828" Y="1.426056274414" />
                  <Point X="-3.639119873047" Y="1.443785888672" />
                  <Point X="-3.623581542969" Y="1.481298706055" />
                  <Point X="-3.614472412109" Y="1.503290161133" />
                  <Point X="-3.610714111328" Y="1.52460534668" />
                  <Point X="-3.616315917969" Y="1.54551171875" />
                  <Point X="-3.635064453125" Y="1.581527587891" />
                  <Point X="-3.646055664062" Y="1.602641479492" />
                  <Point X="-3.659968261719" Y="1.619221679688" />
                  <Point X="-3.974513671875" Y="1.860580810547" />
                  <Point X="-4.47610546875" Y="2.245466308594" />
                  <Point X="-4.283986328125" Y="2.574613037109" />
                  <Point X="-4.160014648438" Y="2.787006591797" />
                  <Point X="-3.90098046875" Y="3.119958496094" />
                  <Point X="-3.774671142578" Y="3.282310791016" />
                  <Point X="-3.570969238281" Y="3.164703369141" />
                  <Point X="-3.159156982422" Y="2.926943359375" />
                  <Point X="-3.138514648438" Y="2.920434814453" />
                  <Point X="-3.084582519531" Y="2.915716308594" />
                  <Point X="-3.052965332031" Y="2.912950195312" />
                  <Point X="-3.031506835938" Y="2.915775390625" />
                  <Point X="-3.013252441406" Y="2.927404541016" />
                  <Point X="-2.974970947266" Y="2.965686035156" />
                  <Point X="-2.952528808594" Y="2.988127929688" />
                  <Point X="-2.940899414062" Y="3.006382324219" />
                  <Point X="-2.93807421875" Y="3.027841064453" />
                  <Point X="-2.942792724609" Y="3.081773193359" />
                  <Point X="-2.945558837891" Y="3.113390625" />
                  <Point X="-2.952067382812" Y="3.134032958984" />
                  <Point X="-3.091452148438" Y="3.375454101562" />
                  <Point X="-3.307278564453" Y="3.749276611328" />
                  <Point X="-2.968794677734" Y="4.008788818359" />
                  <Point X="-2.752876220703" Y="4.174331542969" />
                  <Point X="-2.344905273438" Y="4.400991210938" />
                  <Point X="-2.141548583984" Y="4.513971679688" />
                  <Point X="-2.093895263672" Y="4.451868652344" />
                  <Point X="-1.967826660156" Y="4.287573242188" />
                  <Point X="-1.951246826172" Y="4.273660644531" />
                  <Point X="-1.891220581055" Y="4.242412597656" />
                  <Point X="-1.856030761719" Y="4.22409375" />
                  <Point X="-1.835124145508" Y="4.2184921875" />
                  <Point X="-1.813808837891" Y="4.222250488281" />
                  <Point X="-1.751287475586" Y="4.248147949219" />
                  <Point X="-1.714634887695" Y="4.263330078125" />
                  <Point X="-1.696905273438" Y="4.275744140625" />
                  <Point X="-1.686083496094" Y="4.29448828125" />
                  <Point X="-1.665733886719" Y="4.359028808594" />
                  <Point X="-1.653804077148" Y="4.396864746094" />
                  <Point X="-1.651917602539" Y="4.418426757812" />
                  <Point X="-1.667764282227" Y="4.53879296875" />
                  <Point X="-1.689137573242" Y="4.701141113281" />
                  <Point X="-1.247614135742" Y="4.824928710938" />
                  <Point X="-0.968094177246" Y="4.903296386719" />
                  <Point X="-0.473509643555" Y="4.961180175781" />
                  <Point X="-0.224199615479" Y="4.990358398438" />
                  <Point X="-0.163965759277" Y="4.7655625" />
                  <Point X="-0.042140380859" Y="4.310903320312" />
                  <Point X="-0.024282119751" Y="4.284176757813" />
                  <Point X="0.006155934334" Y="4.273844238281" />
                  <Point X="0.036594032288" Y="4.284176757813" />
                  <Point X="0.054452251434" Y="4.310903320312" />
                  <Point X="0.125871307373" Y="4.577443359375" />
                  <Point X="0.236648406982" Y="4.990868652344" />
                  <Point X="0.616142456055" Y="4.951125488281" />
                  <Point X="0.860205810547" Y="4.925565429688" />
                  <Point X="1.269388549805" Y="4.826776367188" />
                  <Point X="1.508455932617" Y="4.769058105469" />
                  <Point X="1.775219970703" Y="4.67230078125" />
                  <Point X="1.931033691406" Y="4.615786132813" />
                  <Point X="2.188570068359" Y="4.495344726562" />
                  <Point X="2.33869921875" Y="4.425134277344" />
                  <Point X="2.587489501953" Y="4.280188476563" />
                  <Point X="2.732523681641" Y="4.19569140625" />
                  <Point X="2.967166015625" Y="4.028826904297" />
                  <Point X="3.068740234375" Y="3.956592773438" />
                  <Point X="2.773041992188" Y="3.444427978516" />
                  <Point X="2.229853515625" Y="2.503597900391" />
                  <Point X="2.224852050781" Y="2.491514892578" />
                  <Point X="2.211317138672" Y="2.440900634766" />
                  <Point X="2.203382324219" Y="2.411228515625" />
                  <Point X="2.202044677734" Y="2.392325927734" />
                  <Point X="2.207322265625" Y="2.348559082031" />
                  <Point X="2.210416015625" Y="2.322901123047" />
                  <Point X="2.218682128906" Y="2.3008125" />
                  <Point X="2.245763671875" Y="2.260901367188" />
                  <Point X="2.261639892578" Y="2.23750390625" />
                  <Point X="2.274940185547" Y="2.224203613281" />
                  <Point X="2.314851318359" Y="2.197122070312" />
                  <Point X="2.338248779297" Y="2.18124609375" />
                  <Point X="2.360336914062" Y="2.172980224609" />
                  <Point X="2.404103759766" Y="2.167702636719" />
                  <Point X="2.42976171875" Y="2.164608642578" />
                  <Point X="2.448664550781" Y="2.165946289062" />
                  <Point X="2.499278808594" Y="2.179481201172" />
                  <Point X="2.528950927734" Y="2.187416015625" />
                  <Point X="2.541033935547" Y="2.192417480469" />
                  <Point X="3.092586914062" Y="2.510856689453" />
                  <Point X="3.994248046875" Y="3.031430908203" />
                  <Point X="4.1165625" Y="2.861441650391" />
                  <Point X="4.202591796875" Y="2.741880126953" />
                  <Point X="4.333401367188" Y="2.525716308594" />
                  <Point X="4.387513183594" Y="2.436296142578" />
                  <Point X="4.003774658203" Y="2.141843261719" />
                  <Point X="3.288616210938" Y="1.593082763672" />
                  <Point X="3.279371337891" Y="1.583833129883" />
                  <Point X="3.242944091797" Y="1.536311157227" />
                  <Point X="3.221589111328" Y="1.508451782227" />
                  <Point X="3.213119628906" Y="1.49150012207" />
                  <Point X="3.199550537109" Y="1.442979980469" />
                  <Point X="3.191595703125" Y="1.414535644531" />
                  <Point X="3.190779541016" Y="1.390965576172" />
                  <Point X="3.201918457031" Y="1.336980712891" />
                  <Point X="3.208448486328" Y="1.305332763672" />
                  <Point X="3.215646728516" Y="1.287954833984" />
                  <Point X="3.245943115234" Y="1.241905395508" />
                  <Point X="3.263704345703" Y="1.214909423828" />
                  <Point X="3.280947998047" Y="1.198819824219" />
                  <Point X="3.324851806641" Y="1.174105834961" />
                  <Point X="3.350590087891" Y="1.159617431641" />
                  <Point X="3.368565429688" Y="1.153619628906" />
                  <Point X="3.427926269531" Y="1.145774291992" />
                  <Point X="3.462726074219" Y="1.141175048828" />
                  <Point X="3.475803710938" Y="1.14117175293" />
                  <Point X="3.9997421875" Y="1.210149536133" />
                  <Point X="4.848975097656" Y="1.32195324707" />
                  <Point X="4.902239257812" Y="1.103160522461" />
                  <Point X="4.939188476562" Y="0.951385559082" />
                  <Point X="4.98041015625" Y="0.686624328613" />
                  <Point X="4.997858398438" Y="0.574556152344" />
                  <Point X="4.560754882812" Y="0.457434631348" />
                  <Point X="3.741167724609" Y="0.237826919556" />
                  <Point X="3.729086914062" Y="0.232819412231" />
                  <Point X="3.670766601562" Y="0.199109222412" />
                  <Point X="3.636576904297" Y="0.179346878052" />
                  <Point X="3.622264892578" Y="0.166926422119" />
                  <Point X="3.587272705078" Y="0.122338249207" />
                  <Point X="3.566759033203" Y="0.096198738098" />
                  <Point X="3.556985351562" Y="0.074735427856" />
                  <Point X="3.545321289062" Y="0.013830041885" />
                  <Point X="3.538483154297" Y="-0.021875152588" />
                  <Point X="3.538483154297" Y="-0.040684940338" />
                  <Point X="3.550147216797" Y="-0.101590332031" />
                  <Point X="3.556985351562" Y="-0.137295516968" />
                  <Point X="3.566759033203" Y="-0.158758834839" />
                  <Point X="3.601751220703" Y="-0.20334715271" />
                  <Point X="3.622264892578" Y="-0.229486526489" />
                  <Point X="3.636576904297" Y="-0.241906967163" />
                  <Point X="3.694897216797" Y="-0.275617156982" />
                  <Point X="3.729086914062" Y="-0.295379486084" />
                  <Point X="3.741167724609" Y="-0.300387023926" />
                  <Point X="4.221643066406" Y="-0.4291300354" />
                  <Point X="4.998067871094" Y="-0.637172363281" />
                  <Point X="4.9690625" Y="-0.829560058594" />
                  <Point X="4.948431640625" Y="-0.966399108887" />
                  <Point X="4.895619628906" Y="-1.197828857422" />
                  <Point X="4.874545410156" Y="-1.290178344727" />
                  <Point X="4.36247265625" Y="-1.222762695312" />
                  <Point X="3.411982177734" Y="-1.097628295898" />
                  <Point X="3.394836181641" Y="-1.098341186523" />
                  <Point X="3.280374267578" Y="-1.123219970703" />
                  <Point X="3.213271972656" Y="-1.137804931641" />
                  <Point X="3.1854453125" Y="-1.154697265625" />
                  <Point X="3.116260253906" Y="-1.237905273438" />
                  <Point X="3.075701171875" Y="-1.286685424805" />
                  <Point X="3.064357910156" Y="-1.314070678711" />
                  <Point X="3.054441894531" Y="-1.421828857422" />
                  <Point X="3.04862890625" Y="-1.485000854492" />
                  <Point X="3.056360351562" Y="-1.516621826172" />
                  <Point X="3.119705322266" Y="-1.615150756836" />
                  <Point X="3.156840576172" Y="-1.672912353516" />
                  <Point X="3.168460693359" Y="-1.685540649414" />
                  <Point X="3.614344970703" Y="-2.02767956543" />
                  <Point X="4.33907421875" Y="-2.583783935547" />
                  <Point X="4.262287109375" Y="-2.708036376953" />
                  <Point X="4.204129394531" Y="-2.802144775391" />
                  <Point X="4.094913818359" Y="-2.957325439453" />
                  <Point X="4.056688476562" Y="-3.011638427734" />
                  <Point X="3.599994628906" Y="-2.747966064453" />
                  <Point X="2.753454589844" Y="-2.259215576172" />
                  <Point X="2.737340820313" Y="-2.253312744141" />
                  <Point X="2.601112792969" Y="-2.228710205078" />
                  <Point X="2.521250488281" Y="-2.214287109375" />
                  <Point X="2.489077636719" Y="-2.219244873047" />
                  <Point X="2.375905517578" Y="-2.278806396484" />
                  <Point X="2.309559570312" Y="-2.313723876953" />
                  <Point X="2.288599853516" Y="-2.334683837891" />
                  <Point X="2.229038085938" Y="-2.447855712891" />
                  <Point X="2.194120849609" Y="-2.514201904297" />
                  <Point X="2.189163085938" Y="-2.546374511719" />
                  <Point X="2.213765625" Y="-2.682602539062" />
                  <Point X="2.228188720703" Y="-2.76246484375" />
                  <Point X="2.234091552734" Y="-2.778578613281" />
                  <Point X="2.520616943359" Y="-3.274854980469" />
                  <Point X="2.986673583984" Y="-4.082088623047" />
                  <Point X="2.904306396484" Y="-4.140921386719" />
                  <Point X="2.835296875" Y="-4.190213378906" />
                  <Point X="2.713185058594" Y="-4.26925390625" />
                  <Point X="2.679775878906" Y="-4.29087890625" />
                  <Point X="2.329165771484" Y="-3.833955322266" />
                  <Point X="1.683177490234" Y="-2.992087402344" />
                  <Point X="1.670549316406" Y="-2.980467529297" />
                  <Point X="1.536191772461" Y="-2.894087890625" />
                  <Point X="1.45742590332" Y="-2.843448730469" />
                  <Point X="1.4258046875" Y="-2.835717041016" />
                  <Point X="1.278861694336" Y="-2.849238769531" />
                  <Point X="1.192717773438" Y="-2.857166015625" />
                  <Point X="1.165332885742" Y="-2.868509277344" />
                  <Point X="1.05186730957" Y="-2.962852050781" />
                  <Point X="0.985349060059" Y="-3.018159667969" />
                  <Point X="0.968456726074" Y="-3.045986328125" />
                  <Point X="0.93453112793" Y="-3.202071044922" />
                  <Point X="0.91464251709" Y="-3.29357421875" />
                  <Point X="0.91392956543" Y="-3.310720214844" />
                  <Point X="0.995128479004" Y="-3.927488525391" />
                  <Point X="1.127642211914" Y="-4.934028320312" />
                  <Point X="1.059630859375" Y="-4.948936523438" />
                  <Point X="0.994345581055" Y="-4.963247070312" />
                  <Point X="0.881537780762" Y="-4.983740234375" />
                  <Point X="0.860200378418" Y="-4.987616699219" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#180" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.12182548178" Y="4.809783384435" Z="1.6" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.6" />
                  <Point X="-0.48547767958" Y="5.042121414108" Z="1.6" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.6" />
                  <Point X="-1.267267945514" Y="4.904352393689" Z="1.6" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.6" />
                  <Point X="-1.723492349181" Y="4.563546466482" Z="1.6" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.6" />
                  <Point X="-1.719575618451" Y="4.405344398545" Z="1.6" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.6" />
                  <Point X="-1.776577711464" Y="4.325621982742" Z="1.6" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.6" />
                  <Point X="-1.874288915294" Y="4.318043393839" Z="1.6" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.6" />
                  <Point X="-2.060383405998" Y="4.513586679358" Z="1.6" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.6" />
                  <Point X="-2.375344227412" Y="4.475978733226" Z="1.6" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.6" />
                  <Point X="-3.005372689292" Y="4.079748824131" Z="1.6" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.6" />
                  <Point X="-3.140909300175" Y="3.381734128258" Z="1.6" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.6" />
                  <Point X="-2.998758510329" Y="3.108695688212" Z="1.6" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.6" />
                  <Point X="-3.016482142367" Y="3.03232142552" Z="1.6" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.6" />
                  <Point X="-3.086380777394" Y="2.996806188844" Z="1.6" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.6" />
                  <Point X="-3.552125220039" Y="3.239284648571" Z="1.6" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.6" />
                  <Point X="-3.946599715324" Y="3.1819408311" Z="1.6" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.6" />
                  <Point X="-4.333324351532" Y="2.631098085797" Z="1.6" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.6" />
                  <Point X="-4.011107740154" Y="1.852193307319" Z="1.6" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.6" />
                  <Point X="-3.685571071694" Y="1.589720184646" Z="1.6" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.6" />
                  <Point X="-3.675931615734" Y="1.531712868207" Z="1.6" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.6" />
                  <Point X="-3.71417171101" Y="1.487042266412" Z="1.6" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.6" />
                  <Point X="-4.423412022128" Y="1.56310766117" Z="1.6" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.6" />
                  <Point X="-4.874273940796" Y="1.40163945544" Z="1.6" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.6" />
                  <Point X="-5.005167536751" Y="0.81940577337" Z="1.6" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.6" />
                  <Point X="-4.124929207926" Y="0.19600392362" Z="1.6" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.6" />
                  <Point X="-3.566303862913" Y="0.041950176622" Z="1.6" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.6" />
                  <Point X="-3.545388843081" Y="0.018790935922" Z="1.6" />
                  <Point X="-3.539556741714" Y="0" Z="1.6" />
                  <Point X="-3.542975758054" Y="-0.011016015139" Z="1.6" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.6" />
                  <Point X="-3.55906473221" Y="-0.036925806512" Z="1.6" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.6" />
                  <Point X="-4.511958580846" Y="-0.299708133136" Z="1.6" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.6" />
                  <Point X="-5.031623599367" Y="-0.647334545453" Z="1.6" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.6" />
                  <Point X="-4.932488306951" Y="-1.186097870517" Z="1.6" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.6" />
                  <Point X="-3.820738610517" Y="-1.386062765746" Z="1.6" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.6" />
                  <Point X="-3.209371488788" Y="-1.312623745139" Z="1.6" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.6" />
                  <Point X="-3.195523723765" Y="-1.333443797455" Z="1.6" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.6" />
                  <Point X="-4.021516986678" Y="-1.982277098851" Z="1.6" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.6" />
                  <Point X="-4.39441234474" Y="-2.533573966236" Z="1.6" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.6" />
                  <Point X="-4.08106818919" Y="-3.012429387665" Z="1.6" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.6" />
                  <Point X="-3.049374086757" Y="-2.830618339587" Z="1.6" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.6" />
                  <Point X="-2.566427851596" Y="-2.561902617535" Z="1.6" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.6" />
                  <Point X="-3.024798673581" Y="-3.385704295735" Z="1.6" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.6" />
                  <Point X="-3.148601835024" Y="-3.978753483626" Z="1.6" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.6" />
                  <Point X="-2.728098229187" Y="-4.278042136427" Z="1.6" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.6" />
                  <Point X="-2.309338861726" Y="-4.264771783619" Z="1.6" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.6" />
                  <Point X="-2.130883426369" Y="-4.092748702078" Z="1.6" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.6" />
                  <Point X="-1.853838432508" Y="-3.991583699272" Z="1.6" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.6" />
                  <Point X="-1.572458592888" Y="-4.079978649237" Z="1.6" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.6" />
                  <Point X="-1.403036323273" Y="-4.321400255527" Z="1.6" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.6" />
                  <Point X="-1.395277773702" Y="-4.744137271836" Z="1.6" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.6" />
                  <Point X="-1.303815650159" Y="-4.907620919056" Z="1.6" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.6" />
                  <Point X="-1.006626616484" Y="-4.97708543325" Z="1.6" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.6" />
                  <Point X="-0.565133108562" Y="-4.071289538209" Z="1.6" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.6" />
                  <Point X="-0.356576504263" Y="-3.431589307751" Z="1.6" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.6" />
                  <Point X="-0.1597229524" Y="-3.253811478536" Z="1.6" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.6" />
                  <Point X="0.09363612696" Y="-3.233300566752" Z="1.6" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.6" />
                  <Point X="0.313869355169" Y="-3.370056448017" Z="1.6" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.6" />
                  <Point X="0.669621751839" Y="-4.461246491606" Z="1.6" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.6" />
                  <Point X="0.884318676686" Y="-5.00165497004" Z="1.6" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.6" />
                  <Point X="1.064180575276" Y="-4.966496859822" Z="1.6" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.6" />
                  <Point X="1.038544888482" Y="-3.889682054799" Z="1.6" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.6" />
                  <Point X="0.977234379396" Y="-3.181410553569" Z="1.6" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.6" />
                  <Point X="1.077678176419" Y="-2.970018344478" Z="1.6" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.6" />
                  <Point X="1.277287639797" Y="-2.867748400843" Z="1.6" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.6" />
                  <Point X="1.502996347064" Y="-2.904865779251" Z="1.6" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.6" />
                  <Point X="2.283342016211" Y="-3.833113205387" Z="1.6" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.6" />
                  <Point X="2.73419838367" Y="-4.279948206223" Z="1.6" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.6" />
                  <Point X="2.927212297344" Y="-4.150328405957" Z="1.6" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.6" />
                  <Point X="2.557762308339" Y="-3.218574985375" Z="1.6" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.6" />
                  <Point X="2.25681379697" Y="-2.642436191323" Z="1.6" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.6" />
                  <Point X="2.267128572924" Y="-2.439862152573" Z="1.6" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.6" />
                  <Point X="2.393036225799" Y="-2.291772737043" Z="1.6" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.6" />
                  <Point X="2.586070612189" Y="-2.246634212521" Z="1.6" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.6" />
                  <Point X="3.568838611732" Y="-2.759987191936" Z="1.6" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.6" />
                  <Point X="4.129646110749" Y="-2.954822748458" Z="1.6" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.6" />
                  <Point X="4.298664993916" Y="-2.703041476502" Z="1.6" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.6" />
                  <Point X="3.638626894399" Y="-1.95673195514" Z="1.6" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.6" />
                  <Point X="3.155607436142" Y="-1.556831372897" Z="1.6" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.6" />
                  <Point X="3.098075592046" Y="-1.395130306977" Z="1.6" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.6" />
                  <Point X="3.148550531802" Y="-1.238592256032" Z="1.6" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.6" />
                  <Point X="3.284837843957" Y="-1.140799217167" Z="1.6" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.6" />
                  <Point X="4.349790345386" Y="-1.241054861349" Z="1.6" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.6" />
                  <Point X="4.938210881963" Y="-1.177672980134" Z="1.6" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.6" />
                  <Point X="5.012347944449" Y="-0.805734117949" Z="1.6" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.6" />
                  <Point X="4.228428117927" Y="-0.349553631273" Z="1.6" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.6" />
                  <Point X="3.713762976839" Y="-0.201048364251" Z="1.6" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.6" />
                  <Point X="3.634928880998" Y="-0.141198762586" Z="1.6" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.6" />
                  <Point X="3.593098779145" Y="-0.060905333728" Z="1.6" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.6" />
                  <Point X="3.588272671279" Y="0.035705197472" Z="1.6" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.6" />
                  <Point X="3.620450557402" Y="0.122749975989" Z="1.6" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.6" />
                  <Point X="3.689632437513" Y="0.187100479991" Z="1.6" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.6" />
                  <Point X="4.56753969938" Y="0.440418301495" Z="1.6" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.6" />
                  <Point X="5.023659053977" Y="0.725596316931" Z="1.6" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.6" />
                  <Point X="4.944664425519" Y="1.14626759129" Z="1.6" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.6" />
                  <Point X="3.987059894729" Y="1.291001913915" Z="1.6" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.6" />
                  <Point X="3.428321587009" Y="1.226623304231" Z="1.6" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.6" />
                  <Point X="3.343222392264" Y="1.248956971634" Z="1.6" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.6" />
                  <Point X="3.281557501414" Y="1.300667047221" Z="1.6" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.6" />
                  <Point X="3.244730923139" Y="1.378364465881" Z="1.6" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.6" />
                  <Point X="3.241546828425" Y="1.460793646336" Z="1.6" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.6" />
                  <Point X="3.276471048083" Y="1.537173064049" Z="1.6" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.6" />
                  <Point X="4.028056695786" Y="2.133455527836" Z="1.6" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.6" />
                  <Point X="4.370022653266" Y="2.582882591032" Z="1.6" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.6" />
                  <Point X="4.150991921295" Y="2.921924569588" Z="1.6" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.6" />
                  <Point X="3.061431024315" Y="2.58543808285" Z="1.6" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.6" />
                  <Point X="2.480205824454" Y="2.259064062905" Z="1.6" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.6" />
                  <Point X="2.403933701274" Y="2.248623048876" Z="1.6" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.6" />
                  <Point X="2.336769233266" Y="2.26977672266" Z="1.6" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.6" />
                  <Point X="2.280981877575" Y="2.320255627116" Z="1.6" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.6" />
                  <Point X="2.250806653883" Y="2.385824742559" Z="1.6" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.6" />
                  <Point X="2.253463879752" Y="2.459263751428" Z="1.6" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.6" />
                  <Point X="2.81018727001" Y="3.45070805764" Z="1.6" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.6" />
                  <Point X="2.9899869792" Y="4.100853990124" Z="1.6" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.6" />
                  <Point X="2.60650339859" Y="4.354671302832" Z="1.6" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.6" />
                  <Point X="2.203595521768" Y="4.571916912069" Z="1.6" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.6" />
                  <Point X="1.786112590703" Y="4.750584706406" Z="1.6" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.6" />
                  <Point X="1.274966966355" Y="4.906659892244" Z="1.6" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.6" />
                  <Point X="0.615194406881" Y="5.03213361426" Z="1.6" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.6" />
                  <Point X="0.071419079773" Y="4.621664121516" Z="1.6" />
                  <Point X="0" Y="4.355124473572" Z="1.6" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>