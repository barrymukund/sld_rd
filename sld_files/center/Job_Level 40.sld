<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#195" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2883" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="0" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="0.004715820312" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.87103527832" Y="-4.661000488281" />
                  <Point X="0.563302062988" Y="-3.512524414063" />
                  <Point X="0.557721191406" Y="-3.497141845703" />
                  <Point X="0.542363037109" Y="-3.467377197266" />
                  <Point X="0.41658480835" Y="-3.286154541016" />
                  <Point X="0.378635528564" Y="-3.231476806641" />
                  <Point X="0.356751586914" Y="-3.209021484375" />
                  <Point X="0.330496398926" Y="-3.189777587891" />
                  <Point X="0.302495178223" Y="-3.175669189453" />
                  <Point X="0.107860557556" Y="-3.11526171875" />
                  <Point X="0.049136035919" Y="-3.097035888672" />
                  <Point X="0.020976638794" Y="-3.092766601562" />
                  <Point X="-0.008664840698" Y="-3.092766601562" />
                  <Point X="-0.036824237823" Y="-3.097035888672" />
                  <Point X="-0.231458862305" Y="-3.157443115234" />
                  <Point X="-0.290183380127" Y="-3.175669189453" />
                  <Point X="-0.318184906006" Y="-3.189777832031" />
                  <Point X="-0.344439941406" Y="-3.209021728516" />
                  <Point X="-0.366323577881" Y="-3.231476806641" />
                  <Point X="-0.492101806641" Y="-3.41269921875" />
                  <Point X="-0.530051208496" Y="-3.467377197266" />
                  <Point X="-0.538189025879" Y="-3.481573730469" />
                  <Point X="-0.550990112305" Y="-3.512524414063" />
                  <Point X="-0.643838012695" Y="-3.859038330078" />
                  <Point X="-0.916584594727" Y="-4.876941894531" />
                  <Point X="-1.014143188477" Y="-4.858005371094" />
                  <Point X="-1.079341552734" Y="-4.845350097656" />
                  <Point X="-1.24641784668" Y="-4.802362792969" />
                  <Point X="-1.214963012695" Y="-4.563438964844" />
                  <Point X="-1.214201171875" Y="-4.5479296875" />
                  <Point X="-1.216508666992" Y="-4.516224121094" />
                  <Point X="-1.263006958008" Y="-4.282462402344" />
                  <Point X="-1.277036132812" Y="-4.211932617188" />
                  <Point X="-1.287938354492" Y="-4.182966308594" />
                  <Point X="-1.304010375977" Y="-4.15512890625" />
                  <Point X="-1.32364453125" Y="-4.131204101562" />
                  <Point X="-1.502839477539" Y="-3.974053955078" />
                  <Point X="-1.556905517578" Y="-3.926639404297" />
                  <Point X="-1.583188598633" Y="-3.910295410156" />
                  <Point X="-1.612885864258" Y="-3.897994384766" />
                  <Point X="-1.64302734375" Y="-3.890966308594" />
                  <Point X="-1.880859008789" Y="-3.875377929688" />
                  <Point X="-1.952616577148" Y="-3.870674804688" />
                  <Point X="-1.98341809082" Y="-3.873708496094" />
                  <Point X="-2.014466674805" Y="-3.882028076172" />
                  <Point X="-2.042657836914" Y="-3.894801513672" />
                  <Point X="-2.240831787109" Y="-4.027217529297" />
                  <Point X="-2.300624267578" Y="-4.067169677734" />
                  <Point X="-2.312785644531" Y="-4.076821533203" />
                  <Point X="-2.335102783203" Y="-4.099461914062" />
                  <Point X="-2.387229736328" Y="-4.167395019531" />
                  <Point X="-2.480149169922" Y="-4.288489746094" />
                  <Point X="-2.706150878906" Y="-4.148555175781" />
                  <Point X="-2.801706787109" Y="-4.089389404297" />
                  <Point X="-3.104721923828" Y="-3.856077880859" />
                  <Point X="-2.995648681641" Y="-3.667157470703" />
                  <Point X="-2.423761230469" Y="-2.676619628906" />
                  <Point X="-2.412859130859" Y="-2.647652832031" />
                  <Point X="-2.406588134766" Y="-2.616125732422" />
                  <Point X="-2.405575683594" Y="-2.585191162109" />
                  <Point X="-2.414560791016" Y="-2.555572753906" />
                  <Point X="-2.428778808594" Y="-2.526743408203" />
                  <Point X="-2.446805664062" Y="-2.501588134766" />
                  <Point X="-2.464154296875" Y="-2.484239501953" />
                  <Point X="-2.489313720703" Y="-2.466210693359" />
                  <Point X="-2.518142578125" Y="-2.451994628906" />
                  <Point X="-2.547759765625" Y="-2.443010986328" />
                  <Point X="-2.578693115234" Y="-2.444024169922" />
                  <Point X="-2.610218505859" Y="-2.450295410156" />
                  <Point X="-2.63918359375" Y="-2.461197265625" />
                  <Point X="-2.938044921875" Y="-2.633745117188" />
                  <Point X="-3.818024658203" Y="-3.141801269531" />
                  <Point X="-4.007364013672" Y="-2.893048095703" />
                  <Point X="-4.082858642578" Y="-2.79386328125" />
                  <Point X="-4.304169433594" Y="-2.422758789062" />
                  <Point X="-4.306142578125" Y="-2.419450195312" />
                  <Point X="-4.106571777344" Y="-2.266314208984" />
                  <Point X="-3.105954589844" Y="-1.498513549805" />
                  <Point X="-3.084577392578" Y="-1.47559362793" />
                  <Point X="-3.066612792969" Y="-1.448463012695" />
                  <Point X="-3.053856689453" Y="-1.419832885742" />
                  <Point X="-3.047937988281" Y="-1.39698059082" />
                  <Point X="-3.046152099609" Y="-1.390085693359" />
                  <Point X="-3.043347900391" Y="-1.359658569336" />
                  <Point X="-3.045556396484" Y="-1.327987426758" />
                  <Point X="-3.052557373047" Y="-1.298241699219" />
                  <Point X="-3.068639892578" Y="-1.27225769043" />
                  <Point X="-3.08947265625" Y="-1.248301025391" />
                  <Point X="-3.112972412109" Y="-1.228766967773" />
                  <Point X="-3.133316894531" Y="-1.21679296875" />
                  <Point X="-3.139455078125" Y="-1.213180419922" />
                  <Point X="-3.168718261719" Y="-1.201956420898" />
                  <Point X="-3.200605957031" Y="-1.195474853516" />
                  <Point X="-3.231929199219" Y="-1.194383789062" />
                  <Point X="-3.609213378906" Y="-1.244054077148" />
                  <Point X="-4.732102539062" Y="-1.391885375977" />
                  <Point X="-4.804549804688" Y="-1.108256103516" />
                  <Point X="-4.834076660156" Y="-0.992659667969" />
                  <Point X="-4.892424316406" Y="-0.584698242188" />
                  <Point X="-4.672729980469" Y="-0.525831298828" />
                  <Point X="-3.532875976562" Y="-0.220408340454" />
                  <Point X="-3.517493164062" Y="-0.214827468872" />
                  <Point X="-3.487728515625" Y="-0.199469467163" />
                  <Point X="-3.466408203125" Y="-0.184672027588" />
                  <Point X="-3.459975585938" Y="-0.18020741272" />
                  <Point X="-3.43751953125" Y="-0.158322433472" />
                  <Point X="-3.418275390625" Y="-0.132066177368" />
                  <Point X="-3.40416796875" Y="-0.104066764832" />
                  <Point X="-3.397061035156" Y="-0.081168731689" />
                  <Point X="-3.394916992188" Y="-0.07425983429" />
                  <Point X="-3.390647460938" Y="-0.04609815979" />
                  <Point X="-3.390647949219" Y="-0.016457288742" />
                  <Point X="-3.394917236328" Y="0.011700286865" />
                  <Point X="-3.402023925781" Y="0.03459847641" />
                  <Point X="-3.404168212891" Y="0.041507369995" />
                  <Point X="-3.418276855469" Y="0.069509208679" />
                  <Point X="-3.437520507812" Y="0.095763786316" />
                  <Point X="-3.459975585938" Y="0.117647262573" />
                  <Point X="-3.481295898438" Y="0.132444702148" />
                  <Point X="-3.490901855469" Y="0.13830078125" />
                  <Point X="-3.512902832031" Y="0.149985900879" />
                  <Point X="-3.532875976562" Y="0.157848342896" />
                  <Point X="-3.876788330078" Y="0.249999237061" />
                  <Point X="-4.89181640625" Y="0.521975219727" />
                  <Point X="-4.843517089844" Y="0.848377685547" />
                  <Point X="-4.824487792969" Y="0.976975097656" />
                  <Point X="-4.706623046875" Y="1.411932373047" />
                  <Point X="-4.703551269531" Y="1.423267822266" />
                  <Point X="-4.582741210938" Y="1.407362915039" />
                  <Point X="-3.765666259766" Y="1.29979284668" />
                  <Point X="-3.744986083984" Y="1.299341674805" />
                  <Point X="-3.723424560547" Y="1.301228149414" />
                  <Point X="-3.703137939453" Y="1.305263427734" />
                  <Point X="-3.655949462891" Y="1.320141845703" />
                  <Point X="-3.641711914063" Y="1.324630981445" />
                  <Point X="-3.622779052734" Y="1.332961669922" />
                  <Point X="-3.604034667969" Y="1.343783569336" />
                  <Point X="-3.587353027344" Y="1.356014892578" />
                  <Point X="-3.573714599609" Y="1.371566894531" />
                  <Point X="-3.561300292969" Y="1.389296630859" />
                  <Point X="-3.5513515625" Y="1.407431030273" />
                  <Point X="-3.532416992188" Y="1.453143188477" />
                  <Point X="-3.526704101563" Y="1.466935302734" />
                  <Point X="-3.520916015625" Y="1.48679296875" />
                  <Point X="-3.517157470703" Y="1.508108032227" />
                  <Point X="-3.515804443359" Y="1.528748046875" />
                  <Point X="-3.518950927734" Y="1.549191772461" />
                  <Point X="-3.524552490234" Y="1.570098144531" />
                  <Point X="-3.532049560547" Y="1.589377563477" />
                  <Point X="-3.554896240234" Y="1.63326574707" />
                  <Point X="-3.561789306641" Y="1.646507324219" />
                  <Point X="-3.573281494141" Y="1.663706298828" />
                  <Point X="-3.587194335938" Y="1.680286865234" />
                  <Point X="-3.602135986328" Y="1.694590332031" />
                  <Point X="-3.799405029297" Y="1.845960205078" />
                  <Point X="-4.351859863281" Y="2.269874267578" />
                  <Point X="-4.155092285156" Y="2.606984130859" />
                  <Point X="-4.081153320312" Y="2.733659912109" />
                  <Point X="-3.768934570312" Y="3.134972900391" />
                  <Point X="-3.750504638672" Y="3.158661621094" />
                  <Point X="-3.708481689453" Y="3.134399658203" />
                  <Point X="-3.206656982422" Y="2.844670898438" />
                  <Point X="-3.187724609375" Y="2.836340332031" />
                  <Point X="-3.167082519531" Y="2.829831787109" />
                  <Point X="-3.146794677734" Y="2.825796386719" />
                  <Point X="-3.08107421875" Y="2.820046630859" />
                  <Point X="-3.061245361328" Y="2.818311767578" />
                  <Point X="-3.040565185547" Y="2.818762939453" />
                  <Point X="-3.019106445312" Y="2.821588134766" />
                  <Point X="-2.999014648438" Y="2.826504638672" />
                  <Point X="-2.980463134766" Y="2.835653320313" />
                  <Point X="-2.962208740234" Y="2.847282714844" />
                  <Point X="-2.946077392578" Y="2.860229492188" />
                  <Point X="-2.899428466797" Y="2.906878173828" />
                  <Point X="-2.885353759766" Y="2.920952880859" />
                  <Point X="-2.872406738281" Y="2.937084472656" />
                  <Point X="-2.86077734375" Y="2.955338867188" />
                  <Point X="-2.851628662109" Y="2.973890380859" />
                  <Point X="-2.846712158203" Y="2.993982177734" />
                  <Point X="-2.843886962891" Y="3.015440917969" />
                  <Point X="-2.843435791016" Y="3.03612109375" />
                  <Point X="-2.849185546875" Y="3.101841552734" />
                  <Point X="-2.850920410156" Y="3.121670654297" />
                  <Point X="-2.854955810547" Y="3.141958496094" />
                  <Point X="-2.861464355469" Y="3.162600585938" />
                  <Point X="-2.869794921875" Y="3.181532958984" />
                  <Point X="-2.9572109375" Y="3.332941650391" />
                  <Point X="-3.183333007812" Y="3.724596435547" />
                  <Point X="-2.829398681641" Y="3.995954101562" />
                  <Point X="-2.700626220703" Y="4.094683349609" />
                  <Point X="-2.208892089844" Y="4.367880371094" />
                  <Point X="-2.167036865234" Y="4.391134277344" />
                  <Point X="-2.043195800781" Y="4.229741210938" />
                  <Point X="-2.028892456055" Y="4.214799804688" />
                  <Point X="-2.01231237793" Y="4.200887207031" />
                  <Point X="-1.99511328125" Y="4.189395019531" />
                  <Point X="-1.921966552734" Y="4.151316894531" />
                  <Point X="-1.899897094727" Y="4.139828125" />
                  <Point X="-1.88061730957" Y="4.132330566406" />
                  <Point X="-1.85971081543" Y="4.126729003906" />
                  <Point X="-1.839267822266" Y="4.123582519531" />
                  <Point X="-1.818628417969" Y="4.124935546875" />
                  <Point X="-1.797313110352" Y="4.128693847656" />
                  <Point X="-1.777453613281" Y="4.134481933594" />
                  <Point X="-1.701266601562" Y="4.166040039062" />
                  <Point X="-1.678279663086" Y="4.175561523437" />
                  <Point X="-1.660146362305" Y="4.185509765625" />
                  <Point X="-1.642416625977" Y="4.197923828125" />
                  <Point X="-1.626864379883" Y="4.2115625" />
                  <Point X="-1.6146328125" Y="4.228244140625" />
                  <Point X="-1.603810913086" Y="4.24698828125" />
                  <Point X="-1.59548046875" Y="4.265920898438" />
                  <Point X="-1.570682861328" Y="4.344568359375" />
                  <Point X="-1.563201171875" Y="4.368297363281" />
                  <Point X="-1.559165649414" Y="4.388584960938" />
                  <Point X="-1.557279174805" Y="4.410146972656" />
                  <Point X="-1.55773034668" Y="4.430826660156" />
                  <Point X="-1.567668579102" Y="4.506314941406" />
                  <Point X="-1.584201904297" Y="4.6318984375" />
                  <Point X="-1.11633996582" Y="4.763070800781" />
                  <Point X="-0.94963848877" Y="4.809808105469" />
                  <Point X="-0.353509277344" Y="4.879576171875" />
                  <Point X="-0.2947109375" Y="4.886457519531" />
                  <Point X="-0.282357055664" Y="4.840352050781" />
                  <Point X="-0.133903305054" Y="4.286315429688" />
                  <Point X="-0.121129844666" Y="4.258124023438" />
                  <Point X="-0.10327155304" Y="4.231397460938" />
                  <Point X="-0.082114532471" Y="4.20880859375" />
                  <Point X="-0.054819446564" Y="4.19421875" />
                  <Point X="-0.024381370544" Y="4.183886230469" />
                  <Point X="0.006155906677" Y="4.178844238281" />
                  <Point X="0.03669316864" Y="4.183886230469" />
                  <Point X="0.067131248474" Y="4.19421875" />
                  <Point X="0.094426467896" Y="4.20880859375" />
                  <Point X="0.115583503723" Y="4.231397460938" />
                  <Point X="0.133441802979" Y="4.258124023438" />
                  <Point X="0.146215255737" Y="4.286315429688" />
                  <Point X="0.19100617981" Y="4.453477539062" />
                  <Point X="0.307419403076" Y="4.8879375" />
                  <Point X="0.698475952148" Y="4.846983398438" />
                  <Point X="0.84404095459" Y="4.831738769531" />
                  <Point X="1.337235961914" Y="4.712666503906" />
                  <Point X="1.481026733398" Y="4.677950683594" />
                  <Point X="1.801706787109" Y="4.561637695312" />
                  <Point X="1.894649414062" Y="4.527926757812" />
                  <Point X="2.205059814453" Y="4.382758300781" />
                  <Point X="2.294571533203" Y="4.340896484375" />
                  <Point X="2.594472412109" Y="4.166173339844" />
                  <Point X="2.680979003906" Y="4.115774902344" />
                  <Point X="2.943260253906" Y="3.929254882812" />
                  <Point X="2.809497802734" Y="3.697571289062" />
                  <Point X="2.147581054688" Y="2.551097900391" />
                  <Point X="2.142075683594" Y="2.539930664062" />
                  <Point X="2.133076904297" Y="2.516056640625" />
                  <Point X="2.116583496094" Y="2.454379394531" />
                  <Point X="2.114676513672" Y="2.445606201172" />
                  <Point X="2.108362548828" Y="2.408094970703" />
                  <Point X="2.107728027344" Y="2.380953125" />
                  <Point X="2.114159179688" Y="2.327619873047" />
                  <Point X="2.116099365234" Y="2.311528320312" />
                  <Point X="2.121442138672" Y="2.289605224609" />
                  <Point X="2.129708496094" Y="2.267515869141" />
                  <Point X="2.140071044922" Y="2.247470947266" />
                  <Point X="2.173071777344" Y="2.198836181641" />
                  <Point X="2.178632324219" Y="2.191443603516" />
                  <Point X="2.201889160156" Y="2.163470214844" />
                  <Point X="2.221598876953" Y="2.145592529297" />
                  <Point X="2.270233642578" Y="2.112591796875" />
                  <Point X="2.284907470703" Y="2.102635009766" />
                  <Point X="2.304951660156" Y="2.092272705078" />
                  <Point X="2.327039794922" Y="2.084006591797" />
                  <Point X="2.348963623047" Y="2.078663574219" />
                  <Point X="2.402296875" Y="2.072232421875" />
                  <Point X="2.412033691406" Y="2.071563232422" />
                  <Point X="2.447028076172" Y="2.070960449219" />
                  <Point X="2.473206298828" Y="2.074171142578" />
                  <Point X="2.534883544922" Y="2.090664550781" />
                  <Point X="2.539712158203" Y="2.09209375" />
                  <Point X="2.570404541016" Y="2.102071533203" />
                  <Point X="2.588533935547" Y="2.110145019531" />
                  <Point X="2.934443603516" Y="2.309855957031" />
                  <Point X="3.967326416016" Y="2.906191162109" />
                  <Point X="4.071958740234" Y="2.760775878906" />
                  <Point X="4.123270507813" Y="2.689464111328" />
                  <Point X="4.262198730469" Y="2.459884277344" />
                  <Point X="4.102258789062" Y="2.337157958984" />
                  <Point X="3.230783935547" Y="1.668451171875" />
                  <Point X="3.221424560547" Y="1.660241333008" />
                  <Point X="3.203973632812" Y="1.641627685547" />
                  <Point X="3.159584228516" Y="1.58371862793" />
                  <Point X="3.14619140625" Y="1.566246337891" />
                  <Point X="3.136605224609" Y="1.550911010742" />
                  <Point X="3.121629882812" Y="1.5170859375" />
                  <Point X="3.105094726562" Y="1.457960571289" />
                  <Point X="3.100105957031" Y="1.440121459961" />
                  <Point X="3.09665234375" Y="1.417821777344" />
                  <Point X="3.095836425781" Y="1.394251708984" />
                  <Point X="3.097739501953" Y="1.371768066406" />
                  <Point X="3.111312988281" Y="1.305983398438" />
                  <Point X="3.115408447266" Y="1.286135131836" />
                  <Point X="3.120680175781" Y="1.268976806641" />
                  <Point X="3.136282470703" Y="1.235740478516" />
                  <Point X="3.173201171875" Y="1.179625732422" />
                  <Point X="3.184340087891" Y="1.162695068359" />
                  <Point X="3.198893798828" Y="1.145450195312" />
                  <Point X="3.216137939453" Y="1.129360229492" />
                  <Point X="3.234347412109" Y="1.116034667969" />
                  <Point X="3.287847412109" Y="1.085918701172" />
                  <Point X="3.296033691406" Y="1.081816894531" />
                  <Point X="3.330151123047" Y="1.066732788086" />
                  <Point X="3.356118408203" Y="1.059438598633" />
                  <Point X="3.428454101562" Y="1.049878417969" />
                  <Point X="3.450279052734" Y="1.046994018555" />
                  <Point X="3.462702392578" Y="1.046175048828" />
                  <Point X="3.488203613281" Y="1.04698449707" />
                  <Point X="3.816794677734" Y="1.090244506836" />
                  <Point X="4.776839355469" Y="1.21663659668" />
                  <Point X="4.823897460938" Y="1.023335876465" />
                  <Point X="4.845936035156" Y="0.932809326172" />
                  <Point X="4.890864746094" Y="0.644238708496" />
                  <Point X="4.715309082031" Y="0.597198730469" />
                  <Point X="3.716579833984" Y="0.329589935303" />
                  <Point X="3.704790527344" Y="0.325586303711" />
                  <Point X="3.681545898438" Y="0.315067993164" />
                  <Point X="3.610478271484" Y="0.273989562988" />
                  <Point X="3.589035888672" Y="0.261595581055" />
                  <Point X="3.574311035156" Y="0.251096221924" />
                  <Point X="3.547531005859" Y="0.225576461792" />
                  <Point X="3.504890380859" Y="0.171242477417" />
                  <Point X="3.492025146484" Y="0.154848815918" />
                  <Point X="3.480301025391" Y="0.135568847656" />
                  <Point X="3.47052734375" Y="0.114105537415" />
                  <Point X="3.463680908203" Y="0.092604270935" />
                  <Point X="3.449467285156" Y="0.01838650322" />
                  <Point X="3.445178710938" Y="-0.004006225586" />
                  <Point X="3.443483154297" Y="-0.021875144958" />
                  <Point X="3.445178710938" Y="-0.05855393219" />
                  <Point X="3.459392333984" Y="-0.132771697998" />
                  <Point X="3.463680908203" Y="-0.155164428711" />
                  <Point X="3.47052734375" Y="-0.17666569519" />
                  <Point X="3.480301025391" Y="-0.198129013062" />
                  <Point X="3.492025146484" Y="-0.217408966064" />
                  <Point X="3.534665771484" Y="-0.271743103027" />
                  <Point X="3.547531005859" Y="-0.288136627197" />
                  <Point X="3.559998779297" Y="-0.301235534668" />
                  <Point X="3.589035888672" Y="-0.324155578613" />
                  <Point X="3.660103759766" Y="-0.365234008789" />
                  <Point X="3.681545898438" Y="-0.377627990723" />
                  <Point X="3.692709228516" Y="-0.383138458252" />
                  <Point X="3.716579833984" Y="-0.392149932861" />
                  <Point X="4.017912841797" Y="-0.472891967773" />
                  <Point X="4.891472167969" Y="-0.706961486816" />
                  <Point X="4.867328125" Y="-0.867106689453" />
                  <Point X="4.855022460938" Y="-0.948725830078" />
                  <Point X="4.801173828125" Y="-1.18469909668" />
                  <Point X="4.582627441406" Y="-1.155927001953" />
                  <Point X="3.424382080078" Y="-1.003441040039" />
                  <Point X="3.408035644531" Y="-1.002710327148" />
                  <Point X="3.374658691406" Y="-1.005508728027" />
                  <Point X="3.235177978516" Y="-1.035825317383" />
                  <Point X="3.193094482422" Y="-1.044972412109" />
                  <Point X="3.163973876953" Y="-1.056597167969" />
                  <Point X="3.136147216797" Y="-1.073489624023" />
                  <Point X="3.112397460938" Y="-1.093960205078" />
                  <Point X="3.028090087891" Y="-1.19535559082" />
                  <Point X="3.002653320312" Y="-1.225948364258" />
                  <Point X="2.987932617188" Y="-1.250330566406" />
                  <Point X="2.976589355469" Y="-1.277715698242" />
                  <Point X="2.969757568359" Y="-1.305365478516" />
                  <Point X="2.957674316406" Y="-1.436677001953" />
                  <Point X="2.954028564453" Y="-1.476295776367" />
                  <Point X="2.956347167969" Y="-1.507564208984" />
                  <Point X="2.964078613281" Y="-1.539185180664" />
                  <Point X="2.976450195312" Y="-1.567996826172" />
                  <Point X="3.053640869141" Y="-1.688061645508" />
                  <Point X="3.076930419922" Y="-1.724287109375" />
                  <Point X="3.086932861328" Y="-1.737239013672" />
                  <Point X="3.110628417969" Y="-1.760909179688" />
                  <Point X="3.390267333984" Y="-1.975483764648" />
                  <Point X="4.213122558594" Y="-2.606883056641" />
                  <Point X="4.159498046875" Y="-2.693655761719" />
                  <Point X="4.124810058594" Y="-2.749785888672" />
                  <Point X="4.028981445312" Y="-2.8859453125" />
                  <Point X="3.832528320312" Y="-2.772522949219" />
                  <Point X="2.800954589844" Y="-2.176943115234" />
                  <Point X="2.786131347656" Y="-2.170012207031" />
                  <Point X="2.754224365234" Y="-2.159825195312" />
                  <Point X="2.588220214844" Y="-2.129844970703" />
                  <Point X="2.538134033203" Y="-2.120799560547" />
                  <Point X="2.506781494141" Y="-2.120395263672" />
                  <Point X="2.474609130859" Y="-2.125353027344" />
                  <Point X="2.444833496094" Y="-2.135176757812" />
                  <Point X="2.306924804688" Y="-2.207757080078" />
                  <Point X="2.265315429688" Y="-2.229655761719" />
                  <Point X="2.242384765625" Y="-2.246548828125" />
                  <Point X="2.221424804688" Y="-2.267508789062" />
                  <Point X="2.204531738281" Y="-2.290439453125" />
                  <Point X="2.131951416016" Y="-2.428348144531" />
                  <Point X="2.110052734375" Y="-2.469957519531" />
                  <Point X="2.100229003906" Y="-2.499733154297" />
                  <Point X="2.095271240234" Y="-2.531905517578" />
                  <Point X="2.095675537109" Y="-2.563258056641" />
                  <Point X="2.125655761719" Y="-2.729262207031" />
                  <Point X="2.134701171875" Y="-2.779348388672" />
                  <Point X="2.138985351562" Y="-2.795141357422" />
                  <Point X="2.151819091797" Y="-2.826078613281" />
                  <Point X="2.331515136719" Y="-3.137321289062" />
                  <Point X="2.861283203125" Y="-4.054906494141" />
                  <Point X="2.823013183594" Y="-4.082241699219" />
                  <Point X="2.781845703125" Y="-4.111646484375" />
                  <Point X="2.701765380859" Y="-4.163481445312" />
                  <Point X="2.545732177734" Y="-3.960135498047" />
                  <Point X="1.758546020508" Y="-2.934255126953" />
                  <Point X="1.747503540039" Y="-2.922179443359" />
                  <Point X="1.721924072266" Y="-2.900557373047" />
                  <Point X="1.55819909668" Y="-2.795297363281" />
                  <Point X="1.50880078125" Y="-2.763538574219" />
                  <Point X="1.479989501953" Y="-2.751167236328" />
                  <Point X="1.448368164063" Y="-2.743435546875" />
                  <Point X="1.417099487305" Y="-2.741116699219" />
                  <Point X="1.238038208008" Y="-2.757593994141" />
                  <Point X="1.184012573242" Y="-2.762565673828" />
                  <Point X="1.156362792969" Y="-2.769397460938" />
                  <Point X="1.128977661133" Y="-2.780740722656" />
                  <Point X="1.104595825195" Y="-2.795461181641" />
                  <Point X="0.966329284668" Y="-2.910425048828" />
                  <Point X="0.924612121582" Y="-2.945111572266" />
                  <Point X="0.904141052246" Y="-2.968861816406" />
                  <Point X="0.887248779297" Y="-2.996688476562" />
                  <Point X="0.875624328613" Y="-3.025808837891" />
                  <Point X="0.834283325195" Y="-3.216010009766" />
                  <Point X="0.821810058594" Y="-3.273396728516" />
                  <Point X="0.819724487305" Y="-3.289627441406" />
                  <Point X="0.819742248535" Y="-3.323120117188" />
                  <Point X="0.870666687012" Y="-3.709929931641" />
                  <Point X="1.022065307617" Y="-4.859915527344" />
                  <Point X="1.014618530273" Y="-4.861547851562" />
                  <Point X="0.975679016113" Y="-4.870083007812" />
                  <Point X="0.929315490723" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-0.99604107666" Y="-4.76474609375" />
                  <Point X="-1.058435791016" Y="-4.752635253906" />
                  <Point X="-1.14124621582" Y="-4.731328613281" />
                  <Point X="-1.120775634766" Y="-4.575838867188" />
                  <Point X="-1.120077514648" Y="-4.568100097656" />
                  <Point X="-1.119451782227" Y="-4.541033691406" />
                  <Point X="-1.121759277344" Y="-4.509328125" />
                  <Point X="-1.123334106445" Y="-4.497690429688" />
                  <Point X="-1.169832397461" Y="-4.263928710938" />
                  <Point X="-1.183861450195" Y="-4.193398925781" />
                  <Point X="-1.188125" Y="-4.17846875" />
                  <Point X="-1.19902734375" Y="-4.149502441406" />
                  <Point X="-1.205666015625" Y="-4.135466308594" />
                  <Point X="-1.221738037109" Y="-4.10762890625" />
                  <Point X="-1.230573730469" Y="-4.094862304688" />
                  <Point X="-1.250207763672" Y="-4.0709375" />
                  <Point X="-1.261006591797" Y="-4.059779296875" />
                  <Point X="-1.440201538086" Y="-3.902629150391" />
                  <Point X="-1.494267578125" Y="-3.855214599609" />
                  <Point X="-1.506738769531" Y="-3.845965332031" />
                  <Point X="-1.533021850586" Y="-3.829621337891" />
                  <Point X="-1.546833618164" Y="-3.822526855469" />
                  <Point X="-1.576530883789" Y="-3.810225830078" />
                  <Point X="-1.591313476562" Y="-3.805476074219" />
                  <Point X="-1.621454833984" Y="-3.798447998047" />
                  <Point X="-1.636813964844" Y="-3.796169677734" />
                  <Point X="-1.874645629883" Y="-3.780581298828" />
                  <Point X="-1.946403198242" Y="-3.775878173828" />
                  <Point X="-1.961928222656" Y="-3.776132324219" />
                  <Point X="-1.992729736328" Y="-3.779166015625" />
                  <Point X="-2.008006225586" Y="-3.781945556641" />
                  <Point X="-2.03905480957" Y="-3.790265136719" />
                  <Point X="-2.053674316406" Y="-3.795496337891" />
                  <Point X="-2.081865478516" Y="-3.808269775391" />
                  <Point X="-2.095437011719" Y="-3.815812011719" />
                  <Point X="-2.293611083984" Y="-3.948228027344" />
                  <Point X="-2.353403564453" Y="-3.988180175781" />
                  <Point X="-2.359681640625" Y="-3.992757080078" />
                  <Point X="-2.380442138672" Y="-4.010131103516" />
                  <Point X="-2.402759277344" Y="-4.032771484375" />
                  <Point X="-2.410471435547" Y="-4.041629638672" />
                  <Point X="-2.462598388672" Y="-4.1095625" />
                  <Point X="-2.503202880859" Y="-4.162479003906" />
                  <Point X="-2.656139648438" Y="-4.067784667969" />
                  <Point X="-2.747583007812" Y="-4.011165283203" />
                  <Point X="-2.980862792969" Y="-3.831547607422" />
                  <Point X="-2.913376220703" Y="-3.714657470703" />
                  <Point X="-2.341488769531" Y="-2.724119628906" />
                  <Point X="-2.334849853516" Y="-2.710082763672" />
                  <Point X="-2.323947753906" Y="-2.681115966797" />
                  <Point X="-2.319684570312" Y="-2.666186035156" />
                  <Point X="-2.313413574219" Y="-2.634658935547" />
                  <Point X="-2.311638916016" Y="-2.619233398438" />
                  <Point X="-2.310626464844" Y="-2.588298828125" />
                  <Point X="-2.314666748047" Y="-2.557612792969" />
                  <Point X="-2.323651855469" Y="-2.527994384766" />
                  <Point X="-2.329358886719" Y="-2.513552978516" />
                  <Point X="-2.343576904297" Y="-2.484723632812" />
                  <Point X="-2.351559570312" Y="-2.47140625" />
                  <Point X="-2.369586425781" Y="-2.446250976562" />
                  <Point X="-2.379630615234" Y="-2.434413085938" />
                  <Point X="-2.396979248047" Y="-2.417064453125" />
                  <Point X="-2.408819335938" Y="-2.407018798828" />
                  <Point X="-2.433978759766" Y="-2.388989990234" />
                  <Point X="-2.447298095703" Y="-2.381006835938" />
                  <Point X="-2.476126953125" Y="-2.366790771484" />
                  <Point X="-2.490567382812" Y="-2.361084716797" />
                  <Point X="-2.520184570312" Y="-2.352101074219" />
                  <Point X="-2.550869628906" Y="-2.348062011719" />
                  <Point X="-2.581802978516" Y="-2.349075195312" />
                  <Point X="-2.597228027344" Y="-2.350849853516" />
                  <Point X="-2.628753417969" Y="-2.35712109375" />
                  <Point X="-2.643682617188" Y="-2.361384521484" />
                  <Point X="-2.672647705078" Y="-2.372286376953" />
                  <Point X="-2.68668359375" Y="-2.378924804688" />
                  <Point X="-2.985544921875" Y="-2.55147265625" />
                  <Point X="-3.79308984375" Y="-3.017708496094" />
                  <Point X="-3.931770751953" Y="-2.835510009766" />
                  <Point X="-4.004013427734" Y="-2.740597412109" />
                  <Point X="-4.181265136719" Y="-2.443373535156" />
                  <Point X="-4.048739501953" Y="-2.341682861328" />
                  <Point X="-3.048122314453" Y="-1.573882080078" />
                  <Point X="-3.036482177734" Y="-1.563309936523" />
                  <Point X="-3.015104980469" Y="-1.540390014648" />
                  <Point X="-3.005367919922" Y="-1.528042358398" />
                  <Point X="-2.987403320312" Y="-1.500911743164" />
                  <Point X="-2.979836181641" Y="-1.487126098633" />
                  <Point X="-2.967080078125" Y="-1.45849597168" />
                  <Point X="-2.961891113281" Y="-1.443651733398" />
                  <Point X="-2.955972412109" Y="-1.420799438477" />
                  <Point X="-2.951552978516" Y="-1.398804199219" />
                  <Point X="-2.948748779297" Y="-1.368377075195" />
                  <Point X="-2.948578125" Y="-1.353050048828" />
                  <Point X="-2.950786621094" Y="-1.32137890625" />
                  <Point X="-2.953083251953" Y="-1.306222900391" />
                  <Point X="-2.960084228516" Y="-1.276477294922" />
                  <Point X="-2.971778320312" Y="-1.248244384766" />
                  <Point X="-2.987860839844" Y="-1.222260375977" />
                  <Point X="-2.996953613281" Y="-1.209919189453" />
                  <Point X="-3.017786376953" Y="-1.185962524414" />
                  <Point X="-3.028745117188" Y="-1.175244995117" />
                  <Point X="-3.052244873047" Y="-1.1557109375" />
                  <Point X="-3.064785644531" Y="-1.146894897461" />
                  <Point X="-3.085130126953" Y="-1.134920898438" />
                  <Point X="-3.105434082031" Y="-1.124481079102" />
                  <Point X="-3.134697265625" Y="-1.113257080078" />
                  <Point X="-3.149795410156" Y="-1.108859985352" />
                  <Point X="-3.181683105469" Y="-1.102378540039" />
                  <Point X="-3.197298828125" Y="-1.100532348633" />
                  <Point X="-3.228622070312" Y="-1.09944140625" />
                  <Point X="-3.244329101562" Y="-1.100196533203" />
                  <Point X="-3.62161328125" Y="-1.149866821289" />
                  <Point X="-4.660920898438" Y="-1.286694335938" />
                  <Point X="-4.712504882812" Y="-1.084745117188" />
                  <Point X="-4.740761230469" Y="-0.974121948242" />
                  <Point X="-4.786452148438" Y="-0.65465435791" />
                  <Point X="-4.648142089844" Y="-0.617594177246" />
                  <Point X="-3.508288085938" Y="-0.312171295166" />
                  <Point X="-3.500476318359" Y="-0.309712768555" />
                  <Point X="-3.473931884766" Y="-0.299251495361" />
                  <Point X="-3.444167236328" Y="-0.283893554688" />
                  <Point X="-3.433561523438" Y="-0.277513916016" />
                  <Point X="-3.412241210938" Y="-0.262716430664" />
                  <Point X="-3.393671142578" Y="-0.248242034912" />
                  <Point X="-3.371215087891" Y="-0.226357162476" />
                  <Point X="-3.360896484375" Y="-0.214482192993" />
                  <Point X="-3.34165234375" Y="-0.188225875854" />
                  <Point X="-3.333435791016" Y="-0.174812362671" />
                  <Point X="-3.319328369141" Y="-0.14681300354" />
                  <Point X="-3.3134375" Y="-0.132227005005" />
                  <Point X="-3.306330566406" Y="-0.109328964233" />
                  <Point X="-3.300990234375" Y="-0.088499908447" />
                  <Point X="-3.296720703125" Y="-0.060338249207" />
                  <Point X="-3.295647460938" Y="-0.046096607208" />
                  <Point X="-3.295647949219" Y="-0.016455701828" />
                  <Point X="-3.296721435547" Y="-0.002215993881" />
                  <Point X="-3.300990722656" Y="0.025941507339" />
                  <Point X="-3.304186523438" Y="0.039859447479" />
                  <Point X="-3.311293212891" Y="0.062757629395" />
                  <Point X="-3.319328613281" Y="0.084253555298" />
                  <Point X="-3.333437255859" Y="0.112255447388" />
                  <Point X="-3.341654785156" Y="0.125670295715" />
                  <Point X="-3.3608984375" Y="0.151924972534" />
                  <Point X="-3.371216796875" Y="0.163799346924" />
                  <Point X="-3.393671875" Y="0.185682739258" />
                  <Point X="-3.40580859375" Y="0.195691741943" />
                  <Point X="-3.42712890625" Y="0.210489089966" />
                  <Point X="-3.446340820312" Y="0.222201324463" />
                  <Point X="-3.468341796875" Y="0.233886489868" />
                  <Point X="-3.478104980469" Y="0.238383377075" />
                  <Point X="-3.498078125" Y="0.246245834351" />
                  <Point X="-3.508288085938" Y="0.249611251831" />
                  <Point X="-3.852200439453" Y="0.341762207031" />
                  <Point X="-4.785446289062" Y="0.591824584961" />
                  <Point X="-4.749540527344" Y="0.834471496582" />
                  <Point X="-4.731330566406" Y="0.957532348633" />
                  <Point X="-4.633586425781" Y="1.318237060547" />
                  <Point X="-4.595141113281" Y="1.31317565918" />
                  <Point X="-3.778066162109" Y="1.20560559082" />
                  <Point X="-3.76773828125" Y="1.204815429688" />
                  <Point X="-3.747058105469" Y="1.204364257812" />
                  <Point X="-3.736705810547" Y="1.204703125" />
                  <Point X="-3.715144287109" Y="1.20658984375" />
                  <Point X="-3.704890869141" Y="1.208053588867" />
                  <Point X="-3.684604248047" Y="1.212088867188" />
                  <Point X="-3.674571044922" Y="1.21466027832" />
                  <Point X="-3.627382568359" Y="1.229538696289" />
                  <Point X="-3.603450927734" Y="1.237676391602" />
                  <Point X="-3.584518066406" Y="1.246007080078" />
                  <Point X="-3.575279541016" Y="1.250689086914" />
                  <Point X="-3.55653515625" Y="1.261510864258" />
                  <Point X="-3.547860839844" Y="1.267171020508" />
                  <Point X="-3.531179199219" Y="1.27940234375" />
                  <Point X="-3.515927490234" Y="1.293377807617" />
                  <Point X="-3.5022890625" Y="1.3089296875" />
                  <Point X="-3.495894775391" Y="1.317077636719" />
                  <Point X="-3.48348046875" Y="1.334807373047" />
                  <Point X="-3.478010986328" Y="1.343603149414" />
                  <Point X="-3.468062255859" Y="1.361737548828" />
                  <Point X="-3.463583007813" Y="1.371076049805" />
                  <Point X="-3.4446484375" Y="1.416788330078" />
                  <Point X="-3.435499511719" Y="1.440351074219" />
                  <Point X="-3.429711425781" Y="1.460208740234" />
                  <Point X="-3.427359375" Y="1.470295898438" />
                  <Point X="-3.423600830078" Y="1.491610961914" />
                  <Point X="-3.422360839844" Y="1.501893798828" />
                  <Point X="-3.4210078125" Y="1.522533813477" />
                  <Point X="-3.421909912109" Y="1.543199462891" />
                  <Point X="-3.425056396484" Y="1.563643066406" />
                  <Point X="-3.427187744141" Y="1.573778442383" />
                  <Point X="-3.432789306641" Y="1.594684814453" />
                  <Point X="-3.436011230469" Y="1.604528564453" />
                  <Point X="-3.443508300781" Y="1.623807983398" />
                  <Point X="-3.447783447266" Y="1.633243530273" />
                  <Point X="-3.470630126953" Y="1.677131713867" />
                  <Point X="-3.482800048828" Y="1.699286987305" />
                  <Point X="-3.494292236328" Y="1.716486083984" />
                  <Point X="-3.500507568359" Y="1.724771362305" />
                  <Point X="-3.514420410156" Y="1.741351928711" />
                  <Point X="-3.521500732422" Y="1.748911621094" />
                  <Point X="-3.536442382812" Y="1.763215087891" />
                  <Point X="-3.544303710938" Y="1.769958862305" />
                  <Point X="-3.741572753906" Y="1.921328735352" />
                  <Point X="-4.227614257812" Y="2.294282226563" />
                  <Point X="-4.073046142578" Y="2.559094482422" />
                  <Point X="-4.002292480469" Y="2.680312744141" />
                  <Point X="-3.726338378906" Y="3.035012695312" />
                  <Point X="-3.254156982422" Y="2.7623984375" />
                  <Point X="-3.244918457031" Y="2.757716552734" />
                  <Point X="-3.225986083984" Y="2.749385986328" />
                  <Point X="-3.216292236328" Y="2.745737304688" />
                  <Point X="-3.195650146484" Y="2.739228759766" />
                  <Point X="-3.185615722656" Y="2.736656982422" />
                  <Point X="-3.165327880859" Y="2.732621582031" />
                  <Point X="-3.155074462891" Y="2.731157958984" />
                  <Point X="-3.089354003906" Y="2.725408203125" />
                  <Point X="-3.059173339844" Y="2.723334472656" />
                  <Point X="-3.038493164062" Y="2.723785644531" />
                  <Point X="-3.028164794922" Y="2.724575683594" />
                  <Point X="-3.006706054688" Y="2.727400878906" />
                  <Point X="-2.996525878906" Y="2.729310791016" />
                  <Point X="-2.976434082031" Y="2.734227294922" />
                  <Point X="-2.956996826172" Y="2.741302001953" />
                  <Point X="-2.9384453125" Y="2.750450683594" />
                  <Point X="-2.929419433594" Y="2.75553125" />
                  <Point X="-2.911165039062" Y="2.767160644531" />
                  <Point X="-2.90274609375" Y="2.773193603516" />
                  <Point X="-2.886614746094" Y="2.786140380859" />
                  <Point X="-2.87890234375" Y="2.793054199219" />
                  <Point X="-2.832253417969" Y="2.839702880859" />
                  <Point X="-2.811264892578" Y="2.861489990234" />
                  <Point X="-2.798317871094" Y="2.877621582031" />
                  <Point X="-2.792284667969" Y="2.886040771484" />
                  <Point X="-2.780655273438" Y="2.904295166016" />
                  <Point X="-2.775574707031" Y="2.913321044922" />
                  <Point X="-2.766426025391" Y="2.931872558594" />
                  <Point X="-2.759351318359" Y="2.951309814453" />
                  <Point X="-2.754434814453" Y="2.971401611328" />
                  <Point X="-2.752524902344" Y="2.981581787109" />
                  <Point X="-2.749699707031" Y="3.003040527344" />
                  <Point X="-2.748909667969" Y="3.013368896484" />
                  <Point X="-2.748458496094" Y="3.034049072266" />
                  <Point X="-2.748797363281" Y="3.044400878906" />
                  <Point X="-2.754547119141" Y="3.110121337891" />
                  <Point X="-2.757745605469" Y="3.140203857422" />
                  <Point X="-2.761781005859" Y="3.160491699219" />
                  <Point X="-2.764352783203" Y="3.170526123047" />
                  <Point X="-2.770861328125" Y="3.191168212891" />
                  <Point X="-2.774510009766" Y="3.200862060547" />
                  <Point X="-2.782840576172" Y="3.219794433594" />
                  <Point X="-2.787522460938" Y="3.229032958984" />
                  <Point X="-2.874938476562" Y="3.380441650391" />
                  <Point X="-3.059387207031" Y="3.699916259766" />
                  <Point X="-2.771596679688" Y="3.920562255859" />
                  <Point X="-2.648374755859" Y="4.015036132812" />
                  <Point X="-2.192525390625" Y="4.268296386719" />
                  <Point X="-2.118564453125" Y="4.171908691406" />
                  <Point X="-2.1118203125" Y="4.164047363281" />
                  <Point X="-2.097516845703" Y="4.149105957031" />
                  <Point X="-2.089958007813" Y="4.142026367188" />
                  <Point X="-2.073377929688" Y="4.128113769531" />
                  <Point X="-2.065092041016" Y="4.121897949219" />
                  <Point X="-2.047892822266" Y="4.110405761719" />
                  <Point X="-2.038979736328" Y="4.105129394531" />
                  <Point X="-1.965833007813" Y="4.067051025391" />
                  <Point X="-1.943763549805" Y="4.055562255859" />
                  <Point X="-1.934328979492" Y="4.051287353516" />
                  <Point X="-1.915049194336" Y="4.043789794922" />
                  <Point X="-1.905203857422" Y="4.040567382812" />
                  <Point X="-1.884297363281" Y="4.034965820313" />
                  <Point X="-1.874162597656" Y="4.032834716797" />
                  <Point X="-1.853719604492" Y="4.029688232422" />
                  <Point X="-1.833053344727" Y="4.028785888672" />
                  <Point X="-1.81241394043" Y="4.030138916016" />
                  <Point X="-1.802132568359" Y="4.031378662109" />
                  <Point X="-1.780817260742" Y="4.035136962891" />
                  <Point X="-1.770731079102" Y="4.037488525391" />
                  <Point X="-1.750871704102" Y="4.043276611328" />
                  <Point X="-1.741098266602" Y="4.046713623047" />
                  <Point X="-1.664911132813" Y="4.078271728516" />
                  <Point X="-1.641924316406" Y="4.087793212891" />
                  <Point X="-1.63258581543" Y="4.092272460938" />
                  <Point X="-1.614452514648" Y="4.102220703125" />
                  <Point X="-1.605657958984" Y="4.107689453125" />
                  <Point X="-1.587928100586" Y="4.120103515625" />
                  <Point X="-1.579779541016" Y="4.126498535156" />
                  <Point X="-1.564227294922" Y="4.140137207031" />
                  <Point X="-1.550252319336" Y="4.155387695312" />
                  <Point X="-1.538020874023" Y="4.172069335938" />
                  <Point X="-1.532360351562" Y="4.180744140625" />
                  <Point X="-1.521538330078" Y="4.19948828125" />
                  <Point X="-1.516856201172" Y="4.208727539062" />
                  <Point X="-1.508525756836" Y="4.22766015625" />
                  <Point X="-1.504877441406" Y="4.237353515625" />
                  <Point X="-1.480079833984" Y="4.316000976562" />
                  <Point X="-1.472598144531" Y="4.339729980469" />
                  <Point X="-1.470026611328" Y="4.349763671875" />
                  <Point X="-1.465990966797" Y="4.370051269531" />
                  <Point X="-1.46452722168" Y="4.380305175781" />
                  <Point X="-1.46264074707" Y="4.4018671875" />
                  <Point X="-1.462301757812" Y="4.412219238281" />
                  <Point X="-1.462752929688" Y="4.432898925781" />
                  <Point X="-1.46354309082" Y="4.4432265625" />
                  <Point X="-1.473481323242" Y="4.51871484375" />
                  <Point X="-1.479266235352" Y="4.562655761719" />
                  <Point X="-1.090694091797" Y="4.671598144531" />
                  <Point X="-0.931181884766" Y="4.716319824219" />
                  <Point X="-0.365222229004" Y="4.782557128906" />
                  <Point X="-0.22566633606" Y="4.261727539062" />
                  <Point X="-0.220435256958" Y="4.247107910156" />
                  <Point X="-0.207661849976" Y="4.218916503906" />
                  <Point X="-0.200119247437" Y="4.205344726562" />
                  <Point X="-0.182260864258" Y="4.178618164062" />
                  <Point X="-0.17260824585" Y="4.166455566406" />
                  <Point X="-0.151451187134" Y="4.143866699219" />
                  <Point X="-0.126897941589" Y="4.125026367188" />
                  <Point X="-0.099602897644" Y="4.110436523438" />
                  <Point X="-0.085356651306" Y="4.104260742188" />
                  <Point X="-0.054918682098" Y="4.093927978516" />
                  <Point X="-0.039857234955" Y="4.090155273438" />
                  <Point X="-0.009319986343" Y="4.08511328125" />
                  <Point X="0.021631772995" Y="4.08511328125" />
                  <Point X="0.052169025421" Y="4.090155273438" />
                  <Point X="0.06723046875" Y="4.093927978516" />
                  <Point X="0.097668441772" Y="4.104260742188" />
                  <Point X="0.111914535522" Y="4.110436523438" />
                  <Point X="0.139209732056" Y="4.125026367188" />
                  <Point X="0.163763122559" Y="4.143866699219" />
                  <Point X="0.184920181274" Y="4.166455566406" />
                  <Point X="0.194572952271" Y="4.178617675781" />
                  <Point X="0.212431182861" Y="4.205344238281" />
                  <Point X="0.2199737854" Y="4.218916503906" />
                  <Point X="0.232747192383" Y="4.247107910156" />
                  <Point X="0.237978271484" Y="4.261727539062" />
                  <Point X="0.282769195557" Y="4.428889648438" />
                  <Point X="0.378190368652" Y="4.785006347656" />
                  <Point X="0.688581054688" Y="4.7525" />
                  <Point X="0.827876403809" Y="4.737912109375" />
                  <Point X="1.314940673828" Y="4.620319824219" />
                  <Point X="1.453597412109" Y="4.586843261719" />
                  <Point X="1.769314453125" Y="4.472330566406" />
                  <Point X="1.858260986328" Y="4.440068847656" />
                  <Point X="2.164815185547" Y="4.296704101562" />
                  <Point X="2.250447509766" Y="4.25665625" />
                  <Point X="2.546649414062" Y="4.084088378906" />
                  <Point X="2.62943359375" Y="4.035858642578" />
                  <Point X="2.817780029297" Y="3.901916992188" />
                  <Point X="2.727225341797" Y="3.745071289062" />
                  <Point X="2.06530859375" Y="2.598597900391" />
                  <Point X="2.062373046875" Y="2.593104980469" />
                  <Point X="2.053180908203" Y="2.5734375" />
                  <Point X="2.044182128906" Y="2.549563476562" />
                  <Point X="2.041301757812" Y="2.540598632812" />
                  <Point X="2.024808349609" Y="2.478921386719" />
                  <Point X="2.020994384766" Y="2.461375" />
                  <Point X="2.014680541992" Y="2.423863769531" />
                  <Point X="2.013388549805" Y="2.410315185547" />
                  <Point X="2.01275402832" Y="2.383173339844" />
                  <Point X="2.013411254883" Y="2.369580078125" />
                  <Point X="2.019842407227" Y="2.316246826172" />
                  <Point X="2.02380078125" Y="2.289034667969" />
                  <Point X="2.029143554687" Y="2.267111572266" />
                  <Point X="2.032468139648" Y="2.256309082031" />
                  <Point X="2.04073449707" Y="2.234219726562" />
                  <Point X="2.045318359375" Y="2.223888916016" />
                  <Point X="2.055680908203" Y="2.203843994141" />
                  <Point X="2.061459716797" Y="2.194129882813" />
                  <Point X="2.094460449219" Y="2.145495117188" />
                  <Point X="2.105581542969" Y="2.130709960938" />
                  <Point X="2.128838378906" Y="2.102736572266" />
                  <Point X="2.138063964844" Y="2.093104492188" />
                  <Point X="2.157773681641" Y="2.075226806641" />
                  <Point X="2.1682578125" Y="2.066981201172" />
                  <Point X="2.216892578125" Y="2.03398046875" />
                  <Point X="2.241280273438" Y="2.018245117188" />
                  <Point X="2.261324462891" Y="2.00788293457" />
                  <Point X="2.271654785156" Y="2.003299072266" />
                  <Point X="2.293742919922" Y="1.995032958984" />
                  <Point X="2.304545898438" Y="1.991708007812" />
                  <Point X="2.326469726562" Y="1.986364990234" />
                  <Point X="2.337590576172" Y="1.984346801758" />
                  <Point X="2.390923828125" Y="1.977915649414" />
                  <Point X="2.410397460938" Y="1.976577270508" />
                  <Point X="2.445391845703" Y="1.975974487305" />
                  <Point X="2.458593017578" Y="1.976666992188" />
                  <Point X="2.484771240234" Y="1.979877685547" />
                  <Point X="2.497748291016" Y="1.982395996094" />
                  <Point X="2.559425537109" Y="1.998889404297" />
                  <Point X="2.569082763672" Y="2.001747924805" />
                  <Point X="2.599775146484" Y="2.011725708008" />
                  <Point X="2.609051513672" Y="2.015287841797" />
                  <Point X="2.636033935547" Y="2.027872558594" />
                  <Point X="2.981943603516" Y="2.227583496094" />
                  <Point X="3.940404785156" Y="2.780951416016" />
                  <Point X="3.994846191406" Y="2.705290283203" />
                  <Point X="4.043949462891" Y="2.637047851562" />
                  <Point X="4.136884277344" Y="2.483472167969" />
                  <Point X="4.044426513672" Y="2.412526611328" />
                  <Point X="3.172951660156" Y="1.743819702148" />
                  <Point X="3.168137939453" Y="1.739868774414" />
                  <Point X="3.152119628906" Y="1.725217041016" />
                  <Point X="3.134668701172" Y="1.706603515625" />
                  <Point X="3.128576171875" Y="1.699422485352" />
                  <Point X="3.084186767578" Y="1.641513427734" />
                  <Point X="3.070793945312" Y="1.624041259766" />
                  <Point X="3.065635253906" Y="1.616602294922" />
                  <Point X="3.049737792969" Y="1.589369750977" />
                  <Point X="3.034762451172" Y="1.555544677734" />
                  <Point X="3.030140380859" Y="1.542672119141" />
                  <Point X="3.013605224609" Y="1.48354675293" />
                  <Point X="3.006225097656" Y="1.454661010742" />
                  <Point X="3.002771484375" Y="1.432361328125" />
                  <Point X="3.001709228516" Y="1.421108398438" />
                  <Point X="3.000893310547" Y="1.397538330078" />
                  <Point X="3.001174804688" Y="1.386239257812" />
                  <Point X="3.003077880859" Y="1.363755737305" />
                  <Point X="3.004699462891" Y="1.352570922852" />
                  <Point X="3.018272949219" Y="1.286786254883" />
                  <Point X="3.022368408203" Y="1.266937988281" />
                  <Point X="3.024597900391" Y="1.25823449707" />
                  <Point X="3.034684326172" Y="1.228607177734" />
                  <Point X="3.050286621094" Y="1.19537097168" />
                  <Point X="3.056918457031" Y="1.183525878906" />
                  <Point X="3.093837158203" Y="1.127411132812" />
                  <Point X="3.104976074219" Y="1.11048046875" />
                  <Point X="3.111739257813" Y="1.101424072266" />
                  <Point X="3.12629296875" Y="1.084179199219" />
                  <Point X="3.134083496094" Y="1.075990722656" />
                  <Point X="3.151327636719" Y="1.059900756836" />
                  <Point X="3.16003515625" Y="1.05269543457" />
                  <Point X="3.178244628906" Y="1.039369873047" />
                  <Point X="3.187746582031" Y="1.033249511719" />
                  <Point X="3.241246582031" Y="1.003133666992" />
                  <Point X="3.257619140625" Y="0.994930114746" />
                  <Point X="3.291736572266" Y="0.979845947266" />
                  <Point X="3.304459960938" Y="0.975272521973" />
                  <Point X="3.330427246094" Y="0.967978393555" />
                  <Point X="3.343671142578" Y="0.965257568359" />
                  <Point X="3.416006835938" Y="0.955697387695" />
                  <Point X="3.437831787109" Y="0.952812927246" />
                  <Point X="3.444030029297" Y="0.952199707031" />
                  <Point X="3.465716308594" Y="0.951222961426" />
                  <Point X="3.491217529297" Y="0.952032348633" />
                  <Point X="3.500603759766" Y="0.952797180176" />
                  <Point X="3.829194824219" Y="0.996057250977" />
                  <Point X="4.704703613281" Y="1.111319946289" />
                  <Point X="4.731593261719" Y="1.000864929199" />
                  <Point X="4.75268359375" Y="0.914233642578" />
                  <Point X="4.78387109375" Y="0.713921081543" />
                  <Point X="4.690721191406" Y="0.688961669922" />
                  <Point X="3.691991943359" Y="0.421352874756" />
                  <Point X="3.686031494141" Y="0.419544281006" />
                  <Point X="3.665625732422" Y="0.412137512207" />
                  <Point X="3.642381103516" Y="0.401619171143" />
                  <Point X="3.634004638672" Y="0.397316558838" />
                  <Point X="3.562937011719" Y="0.356238220215" />
                  <Point X="3.541494628906" Y="0.343844116211" />
                  <Point X="3.533882324219" Y="0.338945953369" />
                  <Point X="3.508773681641" Y="0.319870056152" />
                  <Point X="3.481993652344" Y="0.294350311279" />
                  <Point X="3.472797119141" Y="0.284226715088" />
                  <Point X="3.430156494141" Y="0.229892700195" />
                  <Point X="3.417291259766" Y="0.21349899292" />
                  <Point X="3.410854736328" Y="0.204208435059" />
                  <Point X="3.399130615234" Y="0.184928482056" />
                  <Point X="3.393843017578" Y="0.174938934326" />
                  <Point X="3.384069335938" Y="0.153475708008" />
                  <Point X="3.380005615234" Y="0.142929428101" />
                  <Point X="3.373159179688" Y="0.121428153992" />
                  <Point X="3.370376464844" Y="0.11047315979" />
                  <Point X="3.356162841797" Y="0.036255489349" />
                  <Point X="3.351874267578" Y="0.013862774849" />
                  <Point X="3.350603515625" Y="0.004967842579" />
                  <Point X="3.348584472656" Y="-0.026261989594" />
                  <Point X="3.350280029297" Y="-0.062940784454" />
                  <Point X="3.351874267578" Y="-0.076422813416" />
                  <Point X="3.366087890625" Y="-0.150640625" />
                  <Point X="3.370376464844" Y="-0.173033340454" />
                  <Point X="3.373159179688" Y="-0.183988342285" />
                  <Point X="3.380005615234" Y="-0.205489624023" />
                  <Point X="3.384069335938" Y="-0.216035751343" />
                  <Point X="3.393843017578" Y="-0.237499130249" />
                  <Point X="3.399130615234" Y="-0.24748866272" />
                  <Point X="3.410854736328" Y="-0.266768615723" />
                  <Point X="3.417291259766" Y="-0.276059051514" />
                  <Point X="3.459931884766" Y="-0.330393188477" />
                  <Point X="3.472797119141" Y="-0.346786743164" />
                  <Point X="3.478718505859" Y="-0.353633514404" />
                  <Point X="3.501138916016" Y="-0.375804321289" />
                  <Point X="3.530176025391" Y="-0.398724365234" />
                  <Point X="3.541494873047" Y="-0.406404296875" />
                  <Point X="3.612562744141" Y="-0.447482635498" />
                  <Point X="3.634004882812" Y="-0.459876586914" />
                  <Point X="3.639495849609" Y="-0.462814849854" />
                  <Point X="3.659156738281" Y="-0.472016113281" />
                  <Point X="3.68302734375" Y="-0.481027557373" />
                  <Point X="3.691991943359" Y="-0.48391293335" />
                  <Point X="3.993324951172" Y="-0.564654846191" />
                  <Point X="4.784876953125" Y="-0.776750610352" />
                  <Point X="4.773389648438" Y="-0.852944152832" />
                  <Point X="4.76161328125" Y="-0.931051574707" />
                  <Point X="4.727801757812" Y="-1.079219726563" />
                  <Point X="4.59502734375" Y="-1.061739746094" />
                  <Point X="3.436781982422" Y="-0.909253845215" />
                  <Point X="3.428624511719" Y="-0.908535827637" />
                  <Point X="3.400098388672" Y="-0.908042541504" />
                  <Point X="3.366721435547" Y="-0.910840820312" />
                  <Point X="3.354481201172" Y="-0.912676330566" />
                  <Point X="3.215000488281" Y="-0.942992858887" />
                  <Point X="3.172916992188" Y="-0.952140014648" />
                  <Point X="3.157873779297" Y="-0.956742553711" />
                  <Point X="3.128753173828" Y="-0.968367431641" />
                  <Point X="3.11467578125" Y="-0.975389404297" />
                  <Point X="3.086849121094" Y="-0.992281860352" />
                  <Point X="3.074123779297" Y="-1.001530639648" />
                  <Point X="3.050374023438" Y="-1.022001220703" />
                  <Point X="3.039349609375" Y="-1.033223022461" />
                  <Point X="2.955042236328" Y="-1.134618408203" />
                  <Point X="2.92960546875" Y="-1.165211181641" />
                  <Point X="2.921326171875" Y="-1.176847290039" />
                  <Point X="2.90660546875" Y="-1.201229492188" />
                  <Point X="2.9001640625" Y="-1.213975708008" />
                  <Point X="2.888820800781" Y="-1.241360717773" />
                  <Point X="2.884362792969" Y="-1.254928100586" />
                  <Point X="2.877531005859" Y="-1.282577880859" />
                  <Point X="2.875157226562" Y="-1.296660400391" />
                  <Point X="2.863073974609" Y="-1.427971923828" />
                  <Point X="2.859428222656" Y="-1.467590698242" />
                  <Point X="2.859288574219" Y="-1.483320922852" />
                  <Point X="2.861607177734" Y="-1.514589355469" />
                  <Point X="2.864065429688" Y="-1.530127441406" />
                  <Point X="2.871796875" Y="-1.561748291016" />
                  <Point X="2.876785888672" Y="-1.576668212891" />
                  <Point X="2.889157470703" Y="-1.605479858398" />
                  <Point X="2.896540039062" Y="-1.619371704102" />
                  <Point X="2.973730712891" Y="-1.739436523438" />
                  <Point X="2.997020263672" Y="-1.775661987305" />
                  <Point X="3.001741943359" Y="-1.782353271484" />
                  <Point X="3.019793701172" Y="-1.804450195312" />
                  <Point X="3.043489257812" Y="-1.828120361328" />
                  <Point X="3.052796142578" Y="-1.836277709961" />
                  <Point X="3.332435058594" Y="-2.050852294922" />
                  <Point X="4.087170654297" Y="-2.629981933594" />
                  <Point X="4.078684570313" Y="-2.643713867188" />
                  <Point X="4.04548828125" Y="-2.697430175781" />
                  <Point X="4.001274414063" Y="-2.760251953125" />
                  <Point X="3.880028320312" Y="-2.690250488281" />
                  <Point X="2.848454589844" Y="-2.094670654297" />
                  <Point X="2.841192626953" Y="-2.090885498047" />
                  <Point X="2.815025390625" Y="-2.079512695312" />
                  <Point X="2.783118408203" Y="-2.069325683594" />
                  <Point X="2.771108154297" Y="-2.066337646484" />
                  <Point X="2.605104003906" Y="-2.036357299805" />
                  <Point X="2.555017822266" Y="-2.027312011719" />
                  <Point X="2.539358886719" Y="-2.025807495117" />
                  <Point X="2.508006347656" Y="-2.025403198242" />
                  <Point X="2.492312744141" Y="-2.026503662109" />
                  <Point X="2.460140380859" Y="-2.031461425781" />
                  <Point X="2.444844482422" Y="-2.035136352539" />
                  <Point X="2.415068847656" Y="-2.044960083008" />
                  <Point X="2.400589111328" Y="-2.051108642578" />
                  <Point X="2.262680419922" Y="-2.123688964844" />
                  <Point X="2.221071044922" Y="-2.145587646484" />
                  <Point X="2.208968505859" Y="-2.153170410156" />
                  <Point X="2.186037841797" Y="-2.170063476562" />
                  <Point X="2.175209716797" Y="-2.179373779297" />
                  <Point X="2.154249755859" Y="-2.200333740234" />
                  <Point X="2.144939453125" Y="-2.211161865234" />
                  <Point X="2.128046386719" Y="-2.234092529297" />
                  <Point X="2.120463623047" Y="-2.246195068359" />
                  <Point X="2.047883422852" Y="-2.384103759766" />
                  <Point X="2.025984741211" Y="-2.425713134766" />
                  <Point X="2.01983605957" Y="-2.440192871094" />
                  <Point X="2.010012329102" Y="-2.469968505859" />
                  <Point X="2.006337280273" Y="-2.485264404297" />
                  <Point X="2.001379516602" Y="-2.517436767578" />
                  <Point X="2.000279174805" Y="-2.533130371094" />
                  <Point X="2.00068347168" Y="-2.564482910156" />
                  <Point X="2.002187866211" Y="-2.580141845703" />
                  <Point X="2.03216809082" Y="-2.746145996094" />
                  <Point X="2.041213500977" Y="-2.796232177734" />
                  <Point X="2.043014648438" Y="-2.804220214844" />
                  <Point X="2.051236083984" Y="-2.831542480469" />
                  <Point X="2.064069824219" Y="-2.862479736328" />
                  <Point X="2.069546630859" Y="-2.873578613281" />
                  <Point X="2.249242675781" Y="-3.184821289062" />
                  <Point X="2.735892822266" Y="-4.027724121094" />
                  <Point X="2.723754150391" Y="-4.036083251953" />
                  <Point X="2.621100585938" Y="-3.902302978516" />
                  <Point X="1.833914550781" Y="-2.876422607422" />
                  <Point X="1.828653564453" Y="-2.870146240234" />
                  <Point X="1.808831542969" Y="-2.849626953125" />
                  <Point X="1.783252075195" Y="-2.828004882812" />
                  <Point X="1.773298706055" Y="-2.820647216797" />
                  <Point X="1.609573852539" Y="-2.715387207031" />
                  <Point X="1.56017565918" Y="-2.683628417969" />
                  <Point X="1.546283691406" Y="-2.676245849609" />
                  <Point X="1.517472412109" Y="-2.663874511719" />
                  <Point X="1.502553222656" Y="-2.658885742188" />
                  <Point X="1.470931762695" Y="-2.651154052734" />
                  <Point X="1.455393920898" Y="-2.648695800781" />
                  <Point X="1.424125244141" Y="-2.646376953125" />
                  <Point X="1.408394287109" Y="-2.646516357422" />
                  <Point X="1.229333007812" Y="-2.662993652344" />
                  <Point X="1.175307373047" Y="-2.667965332031" />
                  <Point X="1.161224975586" Y="-2.670339111328" />
                  <Point X="1.133575317383" Y="-2.677170898438" />
                  <Point X="1.12000793457" Y="-2.68162890625" />
                  <Point X="1.092622802734" Y="-2.692972167969" />
                  <Point X="1.079876708984" Y="-2.699413574219" />
                  <Point X="1.055494873047" Y="-2.714134033203" />
                  <Point X="1.043858886719" Y="-2.722413085938" />
                  <Point X="0.905592346191" Y="-2.837376953125" />
                  <Point X="0.863875244141" Y="-2.872063476562" />
                  <Point X="0.852653320312" Y="-2.883087890625" />
                  <Point X="0.832182128906" Y="-2.906838134766" />
                  <Point X="0.822933044434" Y="-2.919564208984" />
                  <Point X="0.806040710449" Y="-2.947390869141" />
                  <Point X="0.799018737793" Y="-2.961468261719" />
                  <Point X="0.787394348145" Y="-2.990588623047" />
                  <Point X="0.782791931152" Y="-3.005631347656" />
                  <Point X="0.741450805664" Y="-3.195832519531" />
                  <Point X="0.728977661133" Y="-3.253219238281" />
                  <Point X="0.727584777832" Y="-3.261289306641" />
                  <Point X="0.724724487305" Y="-3.289677734375" />
                  <Point X="0.7247421875" Y="-3.323170410156" />
                  <Point X="0.725554992676" Y="-3.335520019531" />
                  <Point X="0.776479492188" Y="-3.722329833984" />
                  <Point X="0.833091369629" Y="-4.15233984375" />
                  <Point X="0.655065124512" Y="-3.487936767578" />
                  <Point X="0.652606262207" Y="-3.480124511719" />
                  <Point X="0.642145019531" Y="-3.453580322266" />
                  <Point X="0.62678692627" Y="-3.423815673828" />
                  <Point X="0.620407409668" Y="-3.413210205078" />
                  <Point X="0.494629211426" Y="-3.231987548828" />
                  <Point X="0.456679992676" Y="-3.177309814453" />
                  <Point X="0.446670684814" Y="-3.165172851562" />
                  <Point X="0.424786712646" Y="-3.142717529297" />
                  <Point X="0.412912322998" Y="-3.132399169922" />
                  <Point X="0.386657196045" Y="-3.113155273438" />
                  <Point X="0.373242797852" Y="-3.104937988281" />
                  <Point X="0.345241485596" Y="-3.090829589844" />
                  <Point X="0.330654602051" Y="-3.084938476562" />
                  <Point X="0.136019989014" Y="-3.024531005859" />
                  <Point X="0.077295509338" Y="-3.006305175781" />
                  <Point X="0.06337638092" Y="-3.003109375" />
                  <Point X="0.035216945648" Y="-2.998840087891" />
                  <Point X="0.020976644516" Y="-2.997766601562" />
                  <Point X="-0.008664855957" Y="-2.997766601562" />
                  <Point X="-0.022905158997" Y="-2.998840087891" />
                  <Point X="-0.051064590454" Y="-3.003109375" />
                  <Point X="-0.064983573914" Y="-3.006305175781" />
                  <Point X="-0.25961819458" Y="-3.066712402344" />
                  <Point X="-0.318342803955" Y="-3.084938476562" />
                  <Point X="-0.332929992676" Y="-3.090829833984" />
                  <Point X="-0.36093145752" Y="-3.104938476562" />
                  <Point X="-0.374345855713" Y="-3.113155761719" />
                  <Point X="-0.400600830078" Y="-3.132399658203" />
                  <Point X="-0.412475219727" Y="-3.142717773438" />
                  <Point X="-0.434358886719" Y="-3.165172851562" />
                  <Point X="-0.444368041992" Y="-3.177309814453" />
                  <Point X="-0.570146118164" Y="-3.358532226562" />
                  <Point X="-0.60809564209" Y="-3.413210205078" />
                  <Point X="-0.612470336914" Y="-3.420132324219" />
                  <Point X="-0.625976745605" Y="-3.445265136719" />
                  <Point X="-0.638777832031" Y="-3.476215820312" />
                  <Point X="-0.642752990723" Y="-3.487936767578" />
                  <Point X="-0.735601013184" Y="-3.834450683594" />
                  <Point X="-0.985425170898" Y="-4.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.134420859228" Y="-4.679484800903" />
                  <Point X="-0.959572004407" Y="-4.670321360712" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.121809715951" Y="-4.58369350602" />
                  <Point X="-0.933718837916" Y="-4.573836080799" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.125115187967" Y="-4.488736365598" />
                  <Point X="-0.907865671425" Y="-4.477350800886" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.143842648385" Y="-4.39458745734" />
                  <Point X="-0.882012504934" Y="-4.380865520973" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.162570108802" Y="-4.300438549083" />
                  <Point X="-0.856159338443" Y="-4.28438024106" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.181297365098" Y="-4.206289630127" />
                  <Point X="-0.830306171951" Y="-4.187894961147" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.819451413958" Y="-4.101434829714" />
                  <Point X="0.82634205632" Y="-4.10107370645" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.698342546479" Y="-4.002966317944" />
                  <Point X="2.720915882881" Y="-4.001783299512" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.218571929762" Y="-4.113112734415" />
                  <Point X="-0.80445300546" Y="-4.091409681234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.794314301533" Y="-4.007621837084" />
                  <Point X="0.813903727962" Y="-4.006595198747" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.628168189429" Y="-3.91151362729" />
                  <Point X="2.667605389835" Y="-3.909446811195" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.618113812368" Y="-4.091329243627" />
                  <Point X="-2.441505306378" Y="-4.082073584025" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.303586326808" Y="-4.022437777302" />
                  <Point X="-0.778599838969" Y="-3.994924401321" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.769177189109" Y="-3.913808844454" />
                  <Point X="0.801465399603" Y="-3.912116691045" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.557994013132" Y="-3.820060927163" />
                  <Point X="2.614294896789" Y="-3.817110322878" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.75753129686" Y="-4.003505431512" />
                  <Point X="-2.343896510272" Y="-3.981827750913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.405944578832" Y="-3.932671773112" />
                  <Point X="-0.752746672478" Y="-3.898439121408" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.744040076684" Y="-3.819995851824" />
                  <Point X="0.789027071245" Y="-3.817638183342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.48781985708" Y="-3.728608225975" />
                  <Point X="2.560984403744" Y="-3.724773834562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.87320897241" Y="-3.914437468731" />
                  <Point X="-2.189407027677" Y="-3.878600927338" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.511398051147" Y="-3.843067982544" />
                  <Point X="-0.726893520307" Y="-3.801953842245" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.718902964259" Y="-3.726182859194" />
                  <Point X="0.776588742886" Y="-3.72315967564" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.417645701027" Y="-3.637155524787" />
                  <Point X="2.507673910698" Y="-3.632437346245" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.976934210623" Y="-3.824743105251" />
                  <Point X="-0.701040396332" Y="-3.70546856456" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.693765851834" Y="-3.632369866564" />
                  <Point X="0.764150406295" Y="-3.628681168369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.347471544974" Y="-3.545702823599" />
                  <Point X="2.454363417652" Y="-3.540100857928" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.920296936589" Y="-3.726644498625" />
                  <Point X="-0.675187272358" Y="-3.608983286875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.668628739409" Y="-3.538556873934" />
                  <Point X="0.751712069631" Y="-3.534202661101" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.277297388922" Y="-3.454250122411" />
                  <Point X="2.401052924606" Y="-3.447764369611" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.863659663542" Y="-3.62854589205" />
                  <Point X="-0.649334148383" Y="-3.512498009191" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.637741058679" Y="-3.445045255818" />
                  <Point X="0.739273732967" Y="-3.439724153834" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.207123232869" Y="-3.362797421223" />
                  <Point X="2.347742431561" Y="-3.355427881295" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.807022390632" Y="-3.530447285482" />
                  <Point X="-0.609400440488" Y="-3.415274799371" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.57862726994" Y="-3.353012905341" />
                  <Point X="0.726835396302" Y="-3.345245646567" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.136949076816" Y="-3.271344720034" />
                  <Point X="2.294431938515" Y="-3.263091392978" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.750385117722" Y="-3.432348678914" />
                  <Point X="-0.541015504821" Y="-3.316560523887" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.514919023321" Y="-3.261221340199" />
                  <Point X="0.729684772604" Y="-3.249965944213" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.066774920764" Y="-3.179892018846" />
                  <Point X="2.241121444876" Y="-3.170754904692" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.693747844812" Y="-3.334250072347" />
                  <Point X="-0.472497667708" Y="-3.217839283333" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.450224002555" Y="-3.169481489698" />
                  <Point X="0.750599932754" Y="-3.153739454246" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.996600764711" Y="-3.088439317658" />
                  <Point X="2.187810947932" Y="-3.07841841658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.637110571902" Y="-3.236151465779" />
                  <Point X="-0.38082473809" Y="-3.117904535801" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.31874353361" Y="-3.081241716225" />
                  <Point X="0.77151521749" Y="-3.05751295775" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.926426608658" Y="-2.99698661647" />
                  <Point X="2.134500450989" Y="-2.986081928467" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.580473298992" Y="-3.138052859211" />
                  <Point X="-0.064512070163" Y="-3.006196918446" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.045658394865" Y="-3.000423129032" />
                  <Point X="0.799288706696" Y="-2.960927037988" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.856252452606" Y="-2.905533915282" />
                  <Point X="2.081189954046" Y="-2.893745440354" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.523836026082" Y="-3.039954252644" />
                  <Point X="0.876264045474" Y="-2.861762558554" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.764818741479" Y="-2.815195380164" />
                  <Point X="2.042211403517" Y="-2.800657846758" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.902404302924" Y="-2.703169267862" />
                  <Point X="4.046612112795" Y="-2.695611656791" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.797749353803" Y="-3.011586848279" />
                  <Point X="-3.78096308874" Y="-3.010707117405" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.467198753172" Y="-2.941855646076" />
                  <Point X="0.998373395188" Y="-2.760232705836" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.628002660658" Y="-2.727235234261" />
                  <Point X="2.024995362761" Y="-2.706429728352" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.751345788711" Y="-2.615955536263" />
                  <Point X="4.048589638903" Y="-2.600377646169" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.867380504186" Y="-2.92010568937" />
                  <Point X="-3.599742339741" Y="-2.90607936752" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.410561480262" Y="-2.843757039509" />
                  <Point X="2.007975974227" Y="-2.61219130384" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.600287290451" Y="-2.528741803829" />
                  <Point X="3.932539573518" Y="-2.511329199512" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.937011631952" Y="-2.828624529275" />
                  <Point X="-3.418521590742" Y="-2.801451617635" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.353924207352" Y="-2.745658432941" />
                  <Point X="2.001384197581" Y="-2.517406391346" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.449228792192" Y="-2.441528071395" />
                  <Point X="3.816489508133" Y="-2.422280752855" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.006090531596" Y="-2.737114428131" />
                  <Point X="-3.237300841743" Y="-2.69682386775" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.316176578084" Y="-2.648549790648" />
                  <Point X="2.028542749686" Y="-2.420852699071" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.298170293933" Y="-2.354314338961" />
                  <Point X="3.700439442748" Y="-2.333232306197" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.061102905191" Y="-2.644867131594" />
                  <Point X="-3.056080092744" Y="-2.592196117865" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.315942580073" Y="-2.553407154462" />
                  <Point X="2.08002917421" Y="-2.323024037029" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.147111795673" Y="-2.267100606527" />
                  <Point X="3.584389377362" Y="-2.24418385954" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.116115278786" Y="-2.552619835058" />
                  <Point X="-2.874859587491" Y="-2.487568380754" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.359338643325" Y="-2.460551072897" />
                  <Point X="2.134724843692" Y="-2.225027185585" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.996053297414" Y="-2.179886874092" />
                  <Point X="3.468339311977" Y="-2.155135412883" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.171127652381" Y="-2.460372538521" />
                  <Point X="-2.693639237567" Y="-2.382940651784" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.467418778681" Y="-2.371084939906" />
                  <Point X="2.263731349842" Y="-2.123135868215" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.844656320411" Y="-2.092690880578" />
                  <Point X="3.352289246592" Y="-2.066086966225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.072721485915" Y="-2.360084916999" />
                  <Point X="3.236239121609" Y="-1.977038522692" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.939657054556" Y="-2.25798093278" />
                  <Point X="3.120188984324" Y="-1.887990079802" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.80659258522" Y="-2.155876946571" />
                  <Point X="3.014835597028" Y="-1.798381044001" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.673528115883" Y="-2.053772960362" />
                  <Point X="2.952564646557" Y="-1.706514153359" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.540463646547" Y="-1.951668974153" />
                  <Point X="2.893928098424" Y="-1.614456791762" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.40739917721" Y="-1.849564987943" />
                  <Point X="2.862616237045" Y="-1.520967404012" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.274334707874" Y="-1.747461001734" />
                  <Point X="2.863273597026" Y="-1.425802580365" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.141270238537" Y="-1.645357015525" />
                  <Point X="2.872069889553" Y="-1.330211213338" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.018267087242" Y="-1.543780320651" />
                  <Point X="2.891851434309" Y="-1.234044133637" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.962618833641" Y="-1.445733546389" />
                  <Point X="2.954187093742" Y="-1.135646887286" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.529365961943" Y="-1.05309526083" />
                  <Point X="4.736237317636" Y="-1.042253592481" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.948799254549" Y="-1.349878920068" />
                  <Point X="3.036889064613" Y="-1.036182287781" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.012522394704" Y="-0.985051511556" />
                  <Point X="4.758208540939" Y="-0.94597175659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.968653312738" Y="-1.255789054298" />
                  <Point X="3.283358816046" Y="-0.928134982578" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.495678827464" Y="-0.917007762282" />
                  <Point X="4.773830101854" Y="-0.850022692403" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.670334613908" Y="-1.24984001947" />
                  <Point X="-4.189625940807" Y="-1.224647145431" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.041688022194" Y="-1.164486268361" />
                  <Point X="4.714838746351" Y="-0.757983925472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.694312824635" Y="-1.155966291375" />
                  <Point X="4.417887643093" Y="-0.67841610048" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.718290998855" Y="-1.062092561368" />
                  <Point X="4.120936539834" Y="-0.598848275488" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.741610443984" Y="-0.968184308831" />
                  <Point X="3.823985366087" Y="-0.519280454189" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.755114964676" Y="-0.873761677901" />
                  <Point X="3.593127608412" Y="-0.43624882373" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.768619485369" Y="-0.779339046971" />
                  <Point X="3.473324933108" Y="-0.347397043025" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.782124006061" Y="-0.684916416041" />
                  <Point X="3.40423786125" Y="-0.255887370168" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.486549756927" Y="-0.574295653161" />
                  <Point X="3.368385194281" Y="-0.162635955956" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.045194188232" Y="-0.456034815062" />
                  <Point X="3.350927998086" Y="-0.068420475971" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.603838619538" Y="-0.337773976963" />
                  <Point X="3.354369242996" Y="0.026890244903" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.375655937341" Y="-0.230685056448" />
                  <Point X="3.373669915538" Y="0.123032123159" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.313465123242" Y="-0.132295401119" />
                  <Point X="3.422978999266" Y="0.220746675606" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.295647623452" Y="-0.036231252653" />
                  <Point X="3.509485235752" Y="0.320410648224" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.309864443494" Y="0.05815404825" />
                  <Point X="3.709403520071" Y="0.426018294413" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.35997015672" Y="0.15065849196" />
                  <Point X="4.150758744683" Y="0.544279114479" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.480827273631" Y="0.239455011722" />
                  <Point X="4.592113969295" Y="0.662539934546" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.769044186987" Y="0.319480576211" />
                  <Point X="4.775562727921" Y="0.767284449468" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.065995471442" Y="0.399048391707" />
                  <Point X="4.760871357993" Y="0.861644880265" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.362946719955" Y="0.478616209087" />
                  <Point X="4.742560487341" Y="0.955815621067" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.659897968468" Y="0.558184026467" />
                  <Point X="3.30371743083" Y="0.975539424608" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.917797568278" Y="1.007722000914" />
                  <Point X="4.719693061346" Y="1.049747562923" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.777257441946" Y="0.647163849954" />
                  <Point X="3.148503997248" Y="1.062535406109" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.763070470903" Y="0.743037730471" />
                  <Point X="3.076417713054" Y="1.153887896908" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.748883500287" Y="0.838911610965" />
                  <Point X="3.028588861085" Y="1.24651166586" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.73469653846" Y="0.934785490999" />
                  <Point X="3.007185881205" Y="1.340520356084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.711384945703" Y="1.031137572677" />
                  <Point X="3.003248968848" Y="1.43544440412" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.685235049485" Y="1.127638403536" />
                  <Point X="3.027106879591" Y="1.531825117111" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.659085153267" Y="1.224139234395" />
                  <Point X="-4.129613198993" Y="1.251887683711" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.526725122034" Y="1.283483708981" />
                  <Point X="3.074949701694" Y="1.629462826041" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.458990282398" Y="1.382163914376" />
                  <Point X="3.156086399837" Y="1.728845393079" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.425818784674" Y="1.479032731777" />
                  <Point X="3.286306900981" Y="1.830800333231" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.427270425069" Y="1.574087027398" />
                  <Point X="3.419371252208" Y="1.93290431325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.465468110018" Y="1.667215544426" />
                  <Point X="2.430799594614" Y="1.976225840884" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.473049800358" Y="1.978440080341" />
                  <Point X="3.552435603434" Y="2.03500829327" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.531897673958" Y="1.75886449137" />
                  <Point X="2.1810962032" Y="2.05826981353" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.73935870509" Y="2.087527111511" />
                  <Point X="3.685499954661" Y="2.137112273289" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.646026641305" Y="1.84801361851" />
                  <Point X="2.092255771533" Y="2.148744256666" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.920579368981" Y="2.192154856936" />
                  <Point X="3.818564305888" Y="2.239216253308" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.762076783268" Y="1.937062061154" />
                  <Point X="2.038181913562" Y="2.241040738722" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.10179994176" Y="2.296782597585" />
                  <Point X="3.951628657114" Y="2.341320233328" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.87812675803" Y="2.026110512561" />
                  <Point X="2.017570108838" Y="2.335090892679" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.283020467891" Y="2.40141033579" />
                  <Point X="4.084692922217" Y="2.443424208834" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.994176732791" Y="2.115158963967" />
                  <Point X="2.015734434797" Y="2.430125061949" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.464240994022" Y="2.506038073995" />
                  <Point X="4.102971993023" Y="2.539512547212" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.110226707552" Y="2.204207415374" />
                  <Point X="2.037503849627" Y="2.526396321506" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.645461520153" Y="2.6106658122" />
                  <Point X="4.047174352797" Y="2.631718689668" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.226276682314" Y="2.293255866781" />
                  <Point X="2.079827142959" Y="2.623744764191" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.826682046284" Y="2.715293550404" />
                  <Point X="3.981799011654" Y="2.723422886089" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.170995702025" Y="2.391283393064" />
                  <Point X="2.136464401882" Y="2.721843370026" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.113716959601" Y="2.489415617624" />
                  <Point X="2.193101660804" Y="2.819941975861" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.056438255006" Y="2.587547840202" />
                  <Point X="2.249738919727" Y="2.918040581695" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.998072418281" Y="2.685737036961" />
                  <Point X="-3.146201638697" Y="2.730381692755" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.960220849104" Y="2.740128532927" />
                  <Point X="2.306376178649" Y="3.01613918753" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.920915747736" Y="2.784911019591" />
                  <Point X="-3.34539184558" Y="2.815072949227" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.829957943006" Y="2.842085695428" />
                  <Point X="2.363013437572" Y="3.114237793365" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.843759077191" Y="2.884085002221" />
                  <Point X="-3.496450349166" Y="2.902286681382" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.763207840657" Y="2.940714292929" />
                  <Point X="2.419650696494" Y="3.212336399199" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.766602406645" Y="2.983258984851" />
                  <Point X="-3.647508852753" Y="2.989500413537" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.748542434557" Y="3.036613247165" />
                  <Point X="2.476287955417" Y="3.310435005034" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.756800068175" Y="3.131310855794" />
                  <Point X="2.532925214339" Y="3.408533610868" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.785448159744" Y="3.224939845804" />
                  <Point X="2.589562473262" Y="3.506632216703" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.838478375147" Y="3.31729102285" />
                  <Point X="2.646199732184" Y="3.604730822538" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.891788937111" Y="3.409627507555" />
                  <Point X="2.702836991107" Y="3.702829428372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.945099427437" Y="3.501963996014" />
                  <Point X="2.759474167285" Y="3.80092802987" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.998409917762" Y="3.594300484474" />
                  <Point X="2.816111280887" Y="3.899026628089" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.051720408087" Y="3.686636972933" />
                  <Point X="2.696861802088" Y="3.987907400594" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.945357580325" Y="3.787341585404" />
                  <Point X="2.560701145197" Y="4.075901895811" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.812174327014" Y="3.889451796818" />
                  <Point X="2.410891745454" Y="4.163181090724" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.678992034282" Y="3.991561957889" />
                  <Point X="-1.877613753582" Y="4.033560413947" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.763730863389" Y="4.03952876332" />
                  <Point X="-0.127245828664" Y="4.12529330982" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.159116045905" Y="4.140300899737" />
                  <Point X="2.261082480989" Y="4.250460292727" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.502776589196" Y="4.095927390911" />
                  <Point X="-2.060883699033" Y="4.119086015966" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.559436031629" Y="4.145365774641" />
                  <Point X="-0.206199862224" Y="4.216285877125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.229118825301" Y="4.239099962819" />
                  <Point X="2.080516622791" Y="4.336127609955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.313714889373" Y="4.200966067616" />
                  <Point X="-2.147543389306" Y="4.209674746915" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.502953233162" Y="4.243456285546" />
                  <Point X="-0.238526438432" Y="4.309722085924" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.257808198863" Y="4.335733882046" />
                  <Point X="1.897599286981" Y="4.421671691462" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.472481888004" Y="4.340183593948" />
                  <Point X="-0.263663558434" Y="4.403535078157" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.283661335514" Y="4.432219160395" />
                  <Point X="1.678379605287" Y="4.505313247639" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.462975819351" Y="4.435812158765" />
                  <Point X="-0.288800678437" Y="4.49734807039" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.309514485716" Y="4.528704439455" />
                  <Point X="1.447403540546" Y="4.588338677889" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.475008105606" Y="4.530311946233" />
                  <Point X="-0.31393779844" Y="4.591161062623" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.335367635918" Y="4.625189718514" />
                  <Point X="1.123654852033" Y="4.666502100948" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.204814966897" Y="4.63960254148" />
                  <Point X="-0.339074918442" Y="4.684974054856" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.36122078612" Y="4.721674997573" />
                  <Point X="0.775569028429" Y="4.743390068802" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.424376885561" Y="4.77563394106" />
                  <Point X="-0.364212038445" Y="4.778787047089" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="0.001626220703" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.779272277832" Y="-4.685588378906" />
                  <Point X="0.471539031982" Y="-3.537112304688" />
                  <Point X="0.464318664551" Y="-3.521544189453" />
                  <Point X="0.338540405273" Y="-3.340321533203" />
                  <Point X="0.300591033936" Y="-3.285643798828" />
                  <Point X="0.274335754395" Y="-3.266399902344" />
                  <Point X="0.079701126099" Y="-3.205992431641" />
                  <Point X="0.020976625443" Y="-3.187766601562" />
                  <Point X="-0.008664908409" Y="-3.187766601562" />
                  <Point X="-0.203299545288" Y="-3.248173828125" />
                  <Point X="-0.262024047852" Y="-3.266399902344" />
                  <Point X="-0.288279144287" Y="-3.285643798828" />
                  <Point X="-0.414057434082" Y="-3.466866210938" />
                  <Point X="-0.45200680542" Y="-3.521544189453" />
                  <Point X="-0.459227142334" Y="-3.537112304688" />
                  <Point X="-0.552075134277" Y="-3.883626220703" />
                  <Point X="-0.84774395752" Y="-4.987077148438" />
                  <Point X="-1.032245239258" Y="-4.951264648438" />
                  <Point X="-1.100246459961" Y="-4.938065429688" />
                  <Point X="-1.325946533203" Y="-4.879994628906" />
                  <Point X="-1.351589599609" Y="-4.873396972656" />
                  <Point X="-1.346193969727" Y="-4.8324140625" />
                  <Point X="-1.309150146484" Y="-4.551038574219" />
                  <Point X="-1.309683227539" Y="-4.5347578125" />
                  <Point X="-1.356181396484" Y="-4.30099609375" />
                  <Point X="-1.370210693359" Y="-4.230466308594" />
                  <Point X="-1.386282714844" Y="-4.20262890625" />
                  <Point X="-1.565477539062" Y="-4.045478515625" />
                  <Point X="-1.619543457031" Y="-3.998063964844" />
                  <Point X="-1.649240844727" Y="-3.985762939453" />
                  <Point X="-1.887072387695" Y="-3.970174560547" />
                  <Point X="-1.958829956055" Y="-3.965471435547" />
                  <Point X="-1.989878540039" Y="-3.973791015625" />
                  <Point X="-2.188052490234" Y="-4.10620703125" />
                  <Point X="-2.247844970703" Y="-4.146159179688" />
                  <Point X="-2.259734130859" Y="-4.157294433594" />
                  <Point X="-2.311861083984" Y="-4.225227539062" />
                  <Point X="-2.457095214844" Y="-4.414500488281" />
                  <Point X="-2.756162109375" Y="-4.229325683594" />
                  <Point X="-2.855831054688" Y="-4.16761328125" />
                  <Point X="-3.168362304688" Y="-3.926974609375" />
                  <Point X="-3.228581054688" Y="-3.880608154297" />
                  <Point X="-3.077921142578" Y="-3.619657470703" />
                  <Point X="-2.506033691406" Y="-2.629119628906" />
                  <Point X="-2.499762695312" Y="-2.597592529297" />
                  <Point X="-2.513980712891" Y="-2.568763183594" />
                  <Point X="-2.531329345703" Y="-2.551414550781" />
                  <Point X="-2.560158203125" Y="-2.537198486328" />
                  <Point X="-2.59168359375" Y="-2.543469726562" />
                  <Point X="-2.890544921875" Y="-2.716017578125" />
                  <Point X="-3.842959472656" Y="-3.265894042969" />
                  <Point X="-4.082957275391" Y="-2.950586181641" />
                  <Point X="-4.161703613281" Y="-2.847129394531" />
                  <Point X="-4.385762207031" Y="-2.471417236328" />
                  <Point X="-4.431020019531" Y="-2.395527099609" />
                  <Point X="-4.164404296875" Y="-2.190945556641" />
                  <Point X="-3.163786865234" Y="-1.423144897461" />
                  <Point X="-3.145822265625" Y="-1.396014282227" />
                  <Point X="-3.139903564453" Y="-1.373161865234" />
                  <Point X="-3.138117675781" Y="-1.366266967773" />
                  <Point X="-3.140326171875" Y="-1.334595947266" />
                  <Point X="-3.161158935547" Y="-1.310639160156" />
                  <Point X="-3.181503417969" Y="-1.298665283203" />
                  <Point X="-3.187641601562" Y="-1.295052734375" />
                  <Point X="-3.219529296875" Y="-1.288571044922" />
                  <Point X="-3.596813476562" Y="-1.338241333008" />
                  <Point X="-4.803284179688" Y="-1.497076293945" />
                  <Point X="-4.896594726562" Y="-1.131766967773" />
                  <Point X="-4.927392578125" Y="-1.0111953125" />
                  <Point X="-4.986673339844" Y="-0.59670880127" />
                  <Point X="-4.998396484375" Y="-0.514742126465" />
                  <Point X="-4.697317871094" Y="-0.434068328857" />
                  <Point X="-3.557463867188" Y="-0.128645431519" />
                  <Point X="-3.541895507813" Y="-0.121424926758" />
                  <Point X="-3.520575195312" Y="-0.106627510071" />
                  <Point X="-3.514142578125" Y="-0.102162895203" />
                  <Point X="-3.4948984375" Y="-0.075906555176" />
                  <Point X="-3.487791503906" Y="-0.053008491516" />
                  <Point X="-3.485647460938" Y="-0.046099674225" />
                  <Point X="-3.485647949219" Y="-0.016458898544" />
                  <Point X="-3.492754638672" Y="0.006439319611" />
                  <Point X="-3.494898925781" Y="0.013348137856" />
                  <Point X="-3.514142578125" Y="0.039602802277" />
                  <Point X="-3.535462890625" Y="0.054400218964" />
                  <Point X="-3.557463867188" Y="0.066085334778" />
                  <Point X="-3.901376220703" Y="0.158236328125" />
                  <Point X="-4.998186523438" Y="0.452125793457" />
                  <Point X="-4.937493652344" Y="0.862283813477" />
                  <Point X="-4.917645019531" Y="0.996418212891" />
                  <Point X="-4.798315917969" Y="1.436779418945" />
                  <Point X="-4.773516113281" Y="1.528298583984" />
                  <Point X="-4.570341308594" Y="1.501550170898" />
                  <Point X="-3.753266357422" Y="1.393980102539" />
                  <Point X="-3.731704833984" Y="1.395866577148" />
                  <Point X="-3.684516357422" Y="1.410744995117" />
                  <Point X="-3.670278808594" Y="1.415234130859" />
                  <Point X="-3.651534423828" Y="1.426056152344" />
                  <Point X="-3.639120117188" Y="1.443785888672" />
                  <Point X="-3.620185546875" Y="1.489498046875" />
                  <Point X="-3.61447265625" Y="1.503290161133" />
                  <Point X="-3.610714111328" Y="1.524605102539" />
                  <Point X="-3.616315673828" Y="1.545511474609" />
                  <Point X="-3.639162353516" Y="1.589399536133" />
                  <Point X="-3.646055419922" Y="1.602641235352" />
                  <Point X="-3.659968261719" Y="1.619221801758" />
                  <Point X="-3.857237304688" Y="1.770591674805" />
                  <Point X="-4.47610546875" Y="2.245466308594" />
                  <Point X="-4.237138671875" Y="2.654873779297" />
                  <Point X="-4.160014648438" Y="2.787006591797" />
                  <Point X="-3.843915283203" Y="3.193307373047" />
                  <Point X="-3.774671142578" Y="3.282310791016" />
                  <Point X="-3.660981689453" Y="3.216672119141" />
                  <Point X="-3.159156982422" Y="2.926943359375" />
                  <Point X="-3.138514892578" Y="2.920434814453" />
                  <Point X="-3.072794433594" Y="2.914685058594" />
                  <Point X="-3.052965576172" Y="2.912950195312" />
                  <Point X="-3.031506835938" Y="2.915775390625" />
                  <Point X="-3.013252441406" Y="2.927404785156" />
                  <Point X="-2.966603515625" Y="2.974053466797" />
                  <Point X="-2.952528808594" Y="2.988128173828" />
                  <Point X="-2.940899414062" Y="3.006382568359" />
                  <Point X="-2.93807421875" Y="3.027841308594" />
                  <Point X="-2.943823974609" Y="3.093561767578" />
                  <Point X="-2.945558837891" Y="3.113390869141" />
                  <Point X="-2.952067382812" Y="3.134032958984" />
                  <Point X="-3.039483398438" Y="3.285441650391" />
                  <Point X="-3.307278564453" Y="3.749276611328" />
                  <Point X="-2.887200683594" Y="4.071345947266" />
                  <Point X="-2.752876220703" Y="4.174331542969" />
                  <Point X="-2.255029541016" Y="4.450924316406" />
                  <Point X="-2.141548583984" Y="4.513972167969" />
                  <Point X="-2.121450683594" Y="4.487779785156" />
                  <Point X="-1.967826904297" Y="4.287573242188" />
                  <Point X="-1.951246826172" Y="4.273660644531" />
                  <Point X="-1.878100097656" Y="4.235582519531" />
                  <Point X="-1.856030761719" Y="4.22409375" />
                  <Point X="-1.835124267578" Y="4.2184921875" />
                  <Point X="-1.813808959961" Y="4.222250488281" />
                  <Point X="-1.737621948242" Y="4.25380859375" />
                  <Point X="-1.714635009766" Y="4.263330078125" />
                  <Point X="-1.696905395508" Y="4.275744140625" />
                  <Point X="-1.686083496094" Y="4.29448828125" />
                  <Point X="-1.661285888672" Y="4.373135742188" />
                  <Point X="-1.653804077148" Y="4.396864746094" />
                  <Point X="-1.651917602539" Y="4.418426757812" />
                  <Point X="-1.661855834961" Y="4.493915039062" />
                  <Point X="-1.689137573242" Y="4.701141113281" />
                  <Point X="-1.141985961914" Y="4.854543457031" />
                  <Point X="-0.968094177246" Y="4.903296386719" />
                  <Point X="-0.364552246094" Y="4.973932128906" />
                  <Point X="-0.224199645996" Y="4.990358398438" />
                  <Point X="-0.190594024658" Y="4.864939941406" />
                  <Point X="-0.042140380859" Y="4.310903320312" />
                  <Point X="-0.024282115936" Y="4.284176757813" />
                  <Point X="0.006155934334" Y="4.273844238281" />
                  <Point X="0.036594036102" Y="4.284176757813" />
                  <Point X="0.054452251434" Y="4.310903320312" />
                  <Point X="0.099243202209" Y="4.478065429688" />
                  <Point X="0.236648422241" Y="4.990868652344" />
                  <Point X="0.708370849609" Y="4.941466796875" />
                  <Point X="0.860205505371" Y="4.925565429688" />
                  <Point X="1.35953125" Y="4.805013183594" />
                  <Point X="1.508456176758" Y="4.769057617188" />
                  <Point X="1.834098999023" Y="4.650944824219" />
                  <Point X="1.931037963867" Y="4.615784179688" />
                  <Point X="2.245304443359" Y="4.4688125" />
                  <Point X="2.338695800781" Y="4.425136230469" />
                  <Point X="2.642295410156" Y="4.248258300781" />
                  <Point X="2.732523681641" Y="4.19569140625" />
                  <Point X="3.018856933594" Y="3.992067138672" />
                  <Point X="3.068740234375" Y="3.956592773438" />
                  <Point X="2.891770263672" Y="3.650071289062" />
                  <Point X="2.229853515625" Y="2.503597900391" />
                  <Point X="2.224852050781" Y="2.491514648438" />
                  <Point X="2.208358642578" Y="2.429837402344" />
                  <Point X="2.202044677734" Y="2.392326171875" />
                  <Point X="2.208475830078" Y="2.338992919922" />
                  <Point X="2.210416015625" Y="2.322901367188" />
                  <Point X="2.218682373047" Y="2.300812011719" />
                  <Point X="2.251683105469" Y="2.252177246094" />
                  <Point X="2.274939941406" Y="2.224203857422" />
                  <Point X="2.323574707031" Y="2.191203125" />
                  <Point X="2.338248535156" Y="2.181246337891" />
                  <Point X="2.360336669922" Y="2.172980224609" />
                  <Point X="2.413669921875" Y="2.166549072266" />
                  <Point X="2.448664306641" Y="2.165946289062" />
                  <Point X="2.510341552734" Y="2.182439697266" />
                  <Point X="2.541033935547" Y="2.192417480469" />
                  <Point X="2.886943603516" Y="2.392128417969" />
                  <Point X="3.994248046875" Y="3.031430908203" />
                  <Point X="4.149071289063" Y="2.816261474609" />
                  <Point X="4.202591796875" Y="2.741880126953" />
                  <Point X="4.362218261719" Y="2.478096191406" />
                  <Point X="4.387513183594" Y="2.436296142578" />
                  <Point X="4.160091308594" Y="2.261789306641" />
                  <Point X="3.288616210938" Y="1.593082641602" />
                  <Point X="3.27937109375" Y="1.583833007812" />
                  <Point X="3.234981689453" Y="1.525923828125" />
                  <Point X="3.221588867188" Y="1.508451660156" />
                  <Point X="3.213119384766" Y="1.491499755859" />
                  <Point X="3.196584228516" Y="1.432374267578" />
                  <Point X="3.191595458984" Y="1.41453527832" />
                  <Point X="3.190779541016" Y="1.390965087891" />
                  <Point X="3.204353027344" Y="1.325180541992" />
                  <Point X="3.208448486328" Y="1.305332275391" />
                  <Point X="3.215646484375" Y="1.287955078125" />
                  <Point X="3.252565185547" Y="1.231840332031" />
                  <Point X="3.263704101562" Y="1.214909667969" />
                  <Point X="3.280948242188" Y="1.198819702148" />
                  <Point X="3.334448242188" Y="1.168703735352" />
                  <Point X="3.368565673828" Y="1.153619628906" />
                  <Point X="3.440901367188" Y="1.144059448242" />
                  <Point X="3.462726318359" Y="1.141175048828" />
                  <Point X="3.475803710938" Y="1.14117175293" />
                  <Point X="3.804394775391" Y="1.184431640625" />
                  <Point X="4.848975097656" Y="1.32195324707" />
                  <Point X="4.916201660156" Y="1.045806884766" />
                  <Point X="4.939188476562" Y="0.951385498047" />
                  <Point X="4.989491210938" Y="0.628298522949" />
                  <Point X="4.997858398438" Y="0.574556213379" />
                  <Point X="4.739896972656" Y="0.505435699463" />
                  <Point X="3.741167724609" Y="0.237826919556" />
                  <Point X="3.729087158203" Y="0.232819412231" />
                  <Point X="3.65801953125" Y="0.191741012573" />
                  <Point X="3.636577148438" Y="0.17934703064" />
                  <Point X="3.622264892578" Y="0.166926422119" />
                  <Point X="3.579624267578" Y="0.112592308044" />
                  <Point X="3.566759033203" Y="0.096198738098" />
                  <Point X="3.556985351562" Y="0.074735427856" />
                  <Point X="3.542771728516" Y="0.000517584145" />
                  <Point X="3.538483154297" Y="-0.021875152588" />
                  <Point X="3.538483154297" Y="-0.040684940338" />
                  <Point X="3.552696777344" Y="-0.114902786255" />
                  <Point X="3.556985351562" Y="-0.137295516968" />
                  <Point X="3.566759033203" Y="-0.158758834839" />
                  <Point X="3.609399658203" Y="-0.213092941284" />
                  <Point X="3.622264892578" Y="-0.229486526489" />
                  <Point X="3.636576904297" Y="-0.241906967163" />
                  <Point X="3.707644775391" Y="-0.282985351562" />
                  <Point X="3.729086914062" Y="-0.295379333496" />
                  <Point X="3.741167724609" Y="-0.300387023926" />
                  <Point X="4.042500732422" Y="-0.381128967285" />
                  <Point X="4.998067871094" Y="-0.637172363281" />
                  <Point X="4.961266601562" Y="-0.881269287109" />
                  <Point X="4.948431640625" Y="-0.966399353027" />
                  <Point X="4.883985351562" Y="-1.248812011719" />
                  <Point X="4.874545410156" Y="-1.290178344727" />
                  <Point X="4.570227539062" Y="-1.250114135742" />
                  <Point X="3.411982177734" Y="-1.097628295898" />
                  <Point X="3.394836181641" Y="-1.098341186523" />
                  <Point X="3.25535546875" Y="-1.128657958984" />
                  <Point X="3.213271972656" Y="-1.137804931641" />
                  <Point X="3.1854453125" Y="-1.154697387695" />
                  <Point X="3.101137939453" Y="-1.256092773438" />
                  <Point X="3.075701171875" Y="-1.286685424805" />
                  <Point X="3.064357910156" Y="-1.314070556641" />
                  <Point X="3.052274658203" Y="-1.445382080078" />
                  <Point X="3.04862890625" Y="-1.485000854492" />
                  <Point X="3.056360351562" Y="-1.516621826172" />
                  <Point X="3.133551025391" Y="-1.636686767578" />
                  <Point X="3.156840576172" Y="-1.672912353516" />
                  <Point X="3.168460693359" Y="-1.685540649414" />
                  <Point X="3.448099609375" Y="-1.900115234375" />
                  <Point X="4.339074707031" Y="-2.583784179688" />
                  <Point X="4.240311523437" Y="-2.74359765625" />
                  <Point X="4.204133300781" Y="-2.802139404297" />
                  <Point X="4.070853759766" Y="-2.991511474609" />
                  <Point X="4.056688476562" Y="-3.011638427734" />
                  <Point X="3.785028320312" Y="-2.854795410156" />
                  <Point X="2.753454589844" Y="-2.259215576172" />
                  <Point X="2.737340576172" Y="-2.253312744141" />
                  <Point X="2.571336425781" Y="-2.223332519531" />
                  <Point X="2.521250244141" Y="-2.214287109375" />
                  <Point X="2.489077880859" Y="-2.219244873047" />
                  <Point X="2.351169189453" Y="-2.291825195312" />
                  <Point X="2.309559814453" Y="-2.313723876953" />
                  <Point X="2.288599853516" Y="-2.334683837891" />
                  <Point X="2.21601953125" Y="-2.472592529297" />
                  <Point X="2.194120849609" Y="-2.514201904297" />
                  <Point X="2.189163085938" Y="-2.546374267578" />
                  <Point X="2.219143310547" Y="-2.712378417969" />
                  <Point X="2.228188720703" Y="-2.762464599609" />
                  <Point X="2.234091552734" Y="-2.778578613281" />
                  <Point X="2.413787597656" Y="-3.089821289062" />
                  <Point X="2.986673583984" Y="-4.082088623047" />
                  <Point X="2.878230224609" Y="-4.159546875" />
                  <Point X="2.835293701172" Y="-4.190215332031" />
                  <Point X="2.686284179688" Y="-4.286666992188" />
                  <Point X="2.679776123047" Y="-4.290879394531" />
                  <Point X="2.470363525391" Y="-4.017967773438" />
                  <Point X="1.683177490234" Y="-2.992087402344" />
                  <Point X="1.670549316406" Y="-2.980467529297" />
                  <Point X="1.50682434082" Y="-2.875207519531" />
                  <Point X="1.45742590332" Y="-2.843448730469" />
                  <Point X="1.42580456543" Y="-2.835717041016" />
                  <Point X="1.246743408203" Y="-2.852194335938" />
                  <Point X="1.192717773438" Y="-2.857166015625" />
                  <Point X="1.165332763672" Y="-2.868509277344" />
                  <Point X="1.02706628418" Y="-2.983473144531" />
                  <Point X="0.985349060059" Y="-3.018159667969" />
                  <Point X="0.968456726074" Y="-3.045986328125" />
                  <Point X="0.927115783691" Y="-3.2361875" />
                  <Point X="0.91464251709" Y="-3.29357421875" />
                  <Point X="0.91392956543" Y="-3.310720214844" />
                  <Point X="0.964854003906" Y="-3.697530029297" />
                  <Point X="1.127642211914" Y="-4.934028320312" />
                  <Point X="1.034959960938" Y="-4.954344726562" />
                  <Point X="0.994349914551" Y="-4.96324609375" />
                  <Point X="0.860200317383" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#194" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.148453669796" Y="4.909161140093" Z="1.95" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.95" />
                  <Point X="-0.376520263215" Y="5.054873288322" Z="1.95" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.95" />
                  <Point X="-1.161639751034" Y="4.933966835172" Z="1.95" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.95" />
                  <Point X="-1.717584090305" Y="4.518668739546" Z="1.95" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.95" />
                  <Point X="-1.71512768982" Y="4.419451391419" Z="1.95" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.95" />
                  <Point X="-1.762912015899" Y="4.331282546769" Z="1.95" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.95" />
                  <Point X="-1.861168578771" Y="4.311213330345" Z="1.95" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.95" />
                  <Point X="-2.087938952835" Y="4.549497783769" Z="1.95" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.95" />
                  <Point X="-2.285468466021" Y="4.525911741359" Z="1.95" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.95" />
                  <Point X="-2.923778829153" Y="4.142305941339" Z="1.95" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.95" />
                  <Point X="-3.088940558729" Y="3.291721632871" Z="1.95" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.95" />
                  <Point X="-2.999789863124" Y="3.120483985277" Z="1.95" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.95" />
                  <Point X="-3.008114724876" Y="3.040688843011" Z="1.95" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.95" />
                  <Point X="-3.07459248033" Y="2.995774836049" Z="1.95" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.95" />
                  <Point X="-3.642137715426" Y="3.291253390017" Z="1.95" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.95" />
                  <Point X="-3.889534692194" Y="3.255289880974" Z="1.95" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.95" />
                  <Point X="-4.286476892867" Y="2.711359003022" Z="1.95" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.95" />
                  <Point X="-3.893831300754" Y="1.762203940412" Z="1.95" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.95" />
                  <Point X="-3.689669083508" Y="1.59759239313" Z="1.95" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.95" />
                  <Point X="-3.672535310171" Y="1.53991227569" Z="1.95" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.95" />
                  <Point X="-3.705707475863" Y="1.489711026875" Z="1.95" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.95" />
                  <Point X="-4.56997105532" Y="1.582402529763" Z="1.95" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.95" />
                  <Point X="-4.85273173301" Y="1.481136804301" Z="1.95" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.95" />
                  <Point X="-4.993110962462" Y="0.900882952136" Z="1.95" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.95" />
                  <Point X="-3.920473216242" Y="0.141220104918" Z="1.95" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.95" />
                  <Point X="-3.570128078206" Y="0.044604400423" Z="1.95" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.95" />
                  <Point X="-3.546663603414" Y="0.02289819302" Z="1.95" />
                  <Point X="-3.539556741714" Y="0" Z="1.95" />
                  <Point X="-3.541700997721" Y="-0.006908758041" Z="1.95" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.95" />
                  <Point X="-3.555240516917" Y="-0.034271582711" Z="1.95" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.95" />
                  <Point X="-4.716414572531" Y="-0.354491951838" Z="1.95" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.95" />
                  <Point X="-5.042325514938" Y="-0.572507880574" Z="1.95" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.95" />
                  <Point X="-4.951195173368" Y="-1.11286118596" Z="1.95" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.95" />
                  <Point X="-3.596443113962" Y="-1.356533709091" Z="1.95" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.95" />
                  <Point X="-3.21302066354" Y="-1.310475999677" Z="1.95" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.95" />
                  <Point X="-3.194462080547" Y="-1.329344750772" Z="1.95" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.95" />
                  <Point X="-4.200998063939" Y="-2.119997767452" Z="1.95" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.95" />
                  <Point X="-4.434861559141" Y="-2.465746808069" Z="1.95" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.95" />
                  <Point X="-4.128899133894" Y="-2.949589373668" Z="1.95" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.95" />
                  <Point X="-2.871700940424" Y="-2.728038702149" Z="1.95" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.95" />
                  <Point X="-2.568818396816" Y="-2.559512072315" Z="1.95" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.95" />
                  <Point X="-3.127378311019" Y="-3.563377442068" Z="1.95" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.95" />
                  <Point X="-3.205022184632" Y="-3.935311693617" Z="1.95" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.95" />
                  <Point X="-2.788639653383" Y="-4.240556389916" Z="1.95" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.95" />
                  <Point X="-2.278349326987" Y="-4.224385449772" Z="1.95" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.95" />
                  <Point X="-2.166429961546" Y="-4.116500172482" Z="1.95" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.95" />
                  <Point X="-1.896498355058" Y="-3.988787617184" Z="1.95" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.95" />
                  <Point X="-1.604600839234" Y="-4.051790617324" Z="1.95" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.95" />
                  <Point X="-1.411376719209" Y="-4.279470292769" Z="1.95" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.95" />
                  <Point X="-1.40192233299" Y="-4.794607688595" Z="1.95" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.95" />
                  <Point X="-1.34456132962" Y="-4.897137409925" Z="1.95" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.95" />
                  <Point X="-1.047928512888" Y="-4.969068589634" Z="1.95" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.95" />
                  <Point X="-0.509934897067" Y="-3.865286993701" Z="1.95" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.95" />
                  <Point X="-0.379137408243" Y="-3.464095269154" Z="1.95" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.95" />
                  <Point X="-0.194634637089" Y="-3.264646777102" Z="1.95" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.95" />
                  <Point X="0.058724442272" Y="-3.222465268186" Z="1.95" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.95" />
                  <Point X="0.291308451189" Y="-3.337550486614" Z="1.95" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.95" />
                  <Point X="0.724819963334" Y="-4.667249036115" Z="1.95" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.95" />
                  <Point X="0.859468389945" Y="-5.006169360197" Z="1.95" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.95" />
                  <Point X="1.03950932071" Y="-4.971904784631" Z="1.95" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.95" />
                  <Point X="1.008270275095" Y="-3.659723563098" Z="1.95" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.95" />
                  <Point X="0.969819032307" Y="-3.215526966793" Z="1.95" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.95" />
                  <Point X="1.052877267668" Y="-2.990639486837" Z="1.95" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.95" />
                  <Point X="1.245169392534" Y="-2.870703951172" Z="1.95" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.95" />
                  <Point X="1.473628943371" Y="-2.885985252117" Z="1.95" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.95" />
                  <Point X="2.424539841496" Y="-4.017125756342" Z="1.95" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.95" />
                  <Point X="2.707297037713" Y="-4.297360929065" Z="1.95" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.95" />
                  <Point X="2.901136053335" Y="-4.168954059871" Z="1.95" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.95" />
                  <Point X="2.450932976996" Y="-3.033541233188" Z="1.95" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.95" />
                  <Point X="2.262191365649" Y="-2.672212433606" Z="1.95" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.95" />
                  <Point X="2.254109801309" Y="-2.464598883077" Z="1.95" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.95" />
                  <Point X="2.368299495295" Y="-2.304791508658" Z="1.95" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.95" />
                  <Point X="2.556294369907" Y="-2.241256643842" Z="1.95" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.95" />
                  <Point X="3.753872363919" Y="-2.866816523279" Z="1.95" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.95" />
                  <Point X="4.105586052545" Y="-2.989008777091" Z="1.95" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.95" />
                  <Point X="4.276688535779" Y="-2.73860269695" Z="1.95" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.95" />
                  <Point X="3.472381615369" Y="-1.829167448834" Z="1.95" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.95" />
                  <Point X="3.16945314947" Y="-1.578367470033" Z="1.95" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.95" />
                  <Point X="3.095908216945" Y="-1.418683690494" Z="1.95" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.95" />
                  <Point X="3.133428316935" Y="-1.25677958583" Z="1.95" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.95" />
                  <Point X="3.259819136546" Y="-1.146237136722" Z="1.95" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.95" />
                  <Point X="4.557545233624" Y="-1.268406309207" Z="1.95" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.95" />
                  <Point X="4.926576599548" Y="-1.228655993326" Z="1.95" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.95" />
                  <Point X="5.004551938392" Y="-0.857443378549" Z="1.95" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.95" />
                  <Point X="4.049285656065" Y="-0.301552555492" Z="1.95" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.95" />
                  <Point X="3.726510415904" Y="-0.208416612523" Z="1.95" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.95" />
                  <Point X="3.642577344437" Y="-0.15094470236" Z="1.95" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.95" />
                  <Point X="3.595648266958" Y="-0.074217808696" Z="1.95" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.95" />
                  <Point X="3.585723183466" Y="0.022392722494" Z="1.95" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.95" />
                  <Point X="3.612802093963" Y="0.113004036215" Z="1.95" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.95" />
                  <Point X="3.676884998448" Y="0.179732231719" Z="1.95" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.95" />
                  <Point X="4.746682161241" Y="0.488419377277" Z="1.95" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.95" />
                  <Point X="5.032740070489" Y="0.667270430267" Z="1.95" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.95" />
                  <Point X="4.958626865393" Y="1.088914086368" Z="1.95" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.95" />
                  <Point X="3.791712483185" Y="1.265283931372" Z="1.95" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.95" />
                  <Point X="3.44129649993" Y="1.224908511428" Z="1.95" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.95" />
                  <Point X="3.352818740217" Y="1.243555058916" Z="1.95" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.95" />
                  <Point X="3.288179605708" Y="1.290601739772" Z="1.95" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.95" />
                  <Point X="3.24716562273" Y="1.366564670283" Z="1.95" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.95" />
                  <Point X="3.238580929416" Y="1.450188302024" Z="1.95" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.95" />
                  <Point X="3.268508937489" Y="1.526785863353" Z="1.95" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.95" />
                  <Point X="4.184373549201" Y="2.25340166401" Z="1.95" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.95" />
                  <Point X="4.398839490724" Y="2.535262463735" Z="1.95" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.95" />
                  <Point X="4.183500970876" Y="2.876744365502" Z="1.95" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.95" />
                  <Point X="2.855787661156" Y="2.466709838545" Z="1.95" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.95" />
                  <Point X="2.491268892552" Y="2.262022471441" Z="1.95" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.95" />
                  <Point X="2.413500120185" Y="2.247469515944" Z="1.95" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.95" />
                  <Point X="2.345492875341" Y="2.263857375912" Z="1.95" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.95" />
                  <Point X="2.286901224323" Y="2.311531985041" Z="1.95" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.95" />
                  <Point X="2.251960186815" Y="2.376258323647" Z="1.95" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.95" />
                  <Point X="2.250505471217" Y="2.44820068333" Z="1.95" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.95" />
                  <Point X="2.928915514314" Y="3.656351420799" Z="1.95" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.95" />
                  <Point X="3.041677948918" Y="4.064094228197" Z="1.95" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.95" />
                  <Point X="2.661309419729" Y="4.322741188011" Z="1.95" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.95" />
                  <Point X="2.26033017559" Y="4.545384011994" Z="1.95" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.95" />
                  <Point X="1.844991638984" Y="4.729228840577" Z="1.95" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.95" />
                  <Point X="1.365109731919" Y="4.884896649781" Z="1.95" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.95" />
                  <Point X="0.707422729317" Y="5.022474845694" Z="1.95" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.95" />
                  <Point X="0.044790891757" Y="4.522286365857" Z="1.95" />
                  <Point X="0" Y="4.355124473572" Z="1.95" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>