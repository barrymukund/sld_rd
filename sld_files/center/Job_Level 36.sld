<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#187" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2615" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="0" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="0.004715820312" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.839493225098" Y="-4.543284667969" />
                  <Point X="0.563302062988" Y="-3.512524414063" />
                  <Point X="0.557721191406" Y="-3.497141845703" />
                  <Point X="0.542363037109" Y="-3.467377197266" />
                  <Point X="0.429476654053" Y="-3.304729248047" />
                  <Point X="0.378635528564" Y="-3.231476806641" />
                  <Point X="0.356751586914" Y="-3.209021484375" />
                  <Point X="0.330496398926" Y="-3.189777587891" />
                  <Point X="0.302495178223" Y="-3.175669189453" />
                  <Point X="0.127810218811" Y="-3.121453369141" />
                  <Point X="0.049136188507" Y="-3.097035888672" />
                  <Point X="0.020976791382" Y="-3.092766601562" />
                  <Point X="-0.008664840698" Y="-3.092766601562" />
                  <Point X="-0.036824237823" Y="-3.097035888672" />
                  <Point X="-0.211509353638" Y="-3.151251464844" />
                  <Point X="-0.290183380127" Y="-3.175669189453" />
                  <Point X="-0.318184906006" Y="-3.189777832031" />
                  <Point X="-0.344439941406" Y="-3.209021728516" />
                  <Point X="-0.366323577881" Y="-3.231476806641" />
                  <Point X="-0.479209960938" Y="-3.394124511719" />
                  <Point X="-0.530051208496" Y="-3.467377197266" />
                  <Point X="-0.538189025879" Y="-3.481573730469" />
                  <Point X="-0.550990112305" Y="-3.512524414063" />
                  <Point X="-0.675380065918" Y="-3.97675390625" />
                  <Point X="-0.916584594727" Y="-4.876941894531" />
                  <Point X="-0.990542053223" Y="-4.862586425781" />
                  <Point X="-1.079341552734" Y="-4.845350097656" />
                  <Point X="-1.24641796875" Y="-4.802362792969" />
                  <Point X="-1.214963012695" Y="-4.563438476562" />
                  <Point X="-1.214201171875" Y="-4.5479296875" />
                  <Point X="-1.216508666992" Y="-4.516224121094" />
                  <Point X="-1.258240966797" Y="-4.306422363281" />
                  <Point X="-1.277036132812" Y="-4.211932617188" />
                  <Point X="-1.287938110352" Y="-4.182966308594" />
                  <Point X="-1.304010131836" Y="-4.15512890625" />
                  <Point X="-1.32364453125" Y="-4.131204101562" />
                  <Point X="-1.48447265625" Y="-3.990161621094" />
                  <Point X="-1.556905517578" Y="-3.926639404297" />
                  <Point X="-1.583188476562" Y="-3.910295410156" />
                  <Point X="-1.612885742188" Y="-3.897994384766" />
                  <Point X="-1.64302734375" Y="-3.890966308594" />
                  <Point X="-1.856481933594" Y="-3.876975830078" />
                  <Point X="-1.952616577148" Y="-3.870674804688" />
                  <Point X="-1.98341796875" Y="-3.873708496094" />
                  <Point X="-2.014466674805" Y="-3.882028076172" />
                  <Point X="-2.042657714844" Y="-3.894801513672" />
                  <Point X="-2.22051953125" Y="-4.013645019531" />
                  <Point X="-2.300624267578" Y="-4.067169677734" />
                  <Point X="-2.312785644531" Y="-4.076821533203" />
                  <Point X="-2.335102539062" Y="-4.099461914062" />
                  <Point X="-2.404937744141" Y="-4.19047265625" />
                  <Point X="-2.480148925781" Y="-4.288490234375" />
                  <Point X="-2.671555908203" Y="-4.169975585938" />
                  <Point X="-2.801706787109" Y="-4.089389404297" />
                  <Point X="-3.078164794922" Y="-3.876525878906" />
                  <Point X="-3.104721923828" Y="-3.856077880859" />
                  <Point X="-2.937031738281" Y="-3.565629882812" />
                  <Point X="-2.423761230469" Y="-2.676619628906" />
                  <Point X="-2.412859130859" Y="-2.647653320312" />
                  <Point X="-2.406588134766" Y="-2.616126953125" />
                  <Point X="-2.405575439453" Y="-2.585192871094" />
                  <Point X="-2.414560058594" Y="-2.555575195312" />
                  <Point X="-2.428777099609" Y="-2.526746337891" />
                  <Point X="-2.4468046875" Y="-2.501589355469" />
                  <Point X="-2.464153320312" Y="-2.484240722656" />
                  <Point X="-2.489310302734" Y="-2.466213134766" />
                  <Point X="-2.518139160156" Y="-2.45199609375" />
                  <Point X="-2.547756835938" Y="-2.443011474609" />
                  <Point X="-2.578690917969" Y="-2.444024169922" />
                  <Point X="-2.610217285156" Y="-2.450295166016" />
                  <Point X="-2.63918359375" Y="-2.461197265625" />
                  <Point X="-3.039572509766" Y="-2.692362060547" />
                  <Point X="-3.818024658203" Y="-3.141801269531" />
                  <Point X="-3.980031982422" Y="-2.928956787109" />
                  <Point X="-4.082858642578" Y="-2.79386328125" />
                  <Point X="-4.281055664063" Y="-2.461517089844" />
                  <Point X="-4.306142578125" Y="-2.419450439453" />
                  <Point X="-4.004011230469" Y="-2.187616699219" />
                  <Point X="-3.105954589844" Y="-1.498513549805" />
                  <Point X="-3.084577636719" Y="-1.47559375" />
                  <Point X="-3.066612792969" Y="-1.448462890625" />
                  <Point X="-3.053856445312" Y="-1.419832885742" />
                  <Point X="-3.048544433594" Y="-1.399322998047" />
                  <Point X="-3.046151855469" Y="-1.390085449219" />
                  <Point X="-3.04334765625" Y="-1.359657470703" />
                  <Point X="-3.045556396484" Y="-1.327986694336" />
                  <Point X="-3.052557617188" Y="-1.298241333008" />
                  <Point X="-3.068639892578" Y="-1.272257446289" />
                  <Point X="-3.08947265625" Y="-1.24830078125" />
                  <Point X="-3.11297265625" Y="-1.228766967773" />
                  <Point X="-3.131231933594" Y="-1.218020507812" />
                  <Point X="-3.139455322266" Y="-1.213180419922" />
                  <Point X="-3.168718261719" Y="-1.201956420898" />
                  <Point X="-3.200605957031" Y="-1.195474853516" />
                  <Point X="-3.231929199219" Y="-1.194383789062" />
                  <Point X="-3.737382324219" Y="-1.260927978516" />
                  <Point X="-4.732102050781" Y="-1.391885375977" />
                  <Point X="-4.793859863281" Y="-1.15010534668" />
                  <Point X="-4.834077636719" Y="-0.992654296875" />
                  <Point X="-4.886515136719" Y="-0.626016784668" />
                  <Point X="-4.892424316406" Y="-0.584698181152" />
                  <Point X="-4.555897949219" Y="-0.494526184082" />
                  <Point X="-3.532875976562" Y="-0.220408340454" />
                  <Point X="-3.517493408203" Y="-0.21482762146" />
                  <Point X="-3.487728515625" Y="-0.199469314575" />
                  <Point X="-3.468593505859" Y="-0.186188568115" />
                  <Point X="-3.459975585938" Y="-0.180207107544" />
                  <Point X="-3.437520507812" Y="-0.158323486328" />
                  <Point X="-3.418276367188" Y="-0.132067993164" />
                  <Point X="-3.404168212891" Y="-0.104067070007" />
                  <Point X="-3.397789794922" Y="-0.083515861511" />
                  <Point X="-3.394917236328" Y="-0.074260139465" />
                  <Point X="-3.390647705078" Y="-0.046100437164" />
                  <Point X="-3.390647705078" Y="-0.016459718704" />
                  <Point X="-3.394917236328" Y="0.011700135231" />
                  <Point X="-3.401295654297" Y="0.032251342773" />
                  <Point X="-3.404168212891" Y="0.041506916046" />
                  <Point X="-3.418276367188" Y="0.069507843018" />
                  <Point X="-3.437520507812" Y="0.09576348877" />
                  <Point X="-3.459975585938" Y="0.117647109985" />
                  <Point X="-3.479110595703" Y="0.130927856445" />
                  <Point X="-3.487762207031" Y="0.136270126343" />
                  <Point X="-3.511948242188" Y="0.149471939087" />
                  <Point X="-3.532875976562" Y="0.157848342896" />
                  <Point X="-3.993620361328" Y="0.281304351807" />
                  <Point X="-4.89181640625" Y="0.521975219727" />
                  <Point X="-4.85040625" Y="0.801819458008" />
                  <Point X="-4.824487792969" Y="0.976975341797" />
                  <Point X="-4.718933105469" Y="1.366505493164" />
                  <Point X="-4.703551757812" Y="1.423267944336" />
                  <Point X="-4.498993164062" Y="1.396337280273" />
                  <Point X="-3.765666259766" Y="1.29979284668" />
                  <Point X="-3.744986083984" Y="1.299341674805" />
                  <Point X="-3.723424560547" Y="1.301228149414" />
                  <Point X="-3.703137695312" Y="1.305263671875" />
                  <Point X="-3.660785888672" Y="1.31861706543" />
                  <Point X="-3.641711669922" Y="1.324631225586" />
                  <Point X="-3.622779052734" Y="1.332961669922" />
                  <Point X="-3.604034667969" Y="1.343783569336" />
                  <Point X="-3.587353515625" Y="1.356014648438" />
                  <Point X="-3.573715087891" Y="1.37156628418" />
                  <Point X="-3.561300537109" Y="1.389296020508" />
                  <Point X="-3.551351318359" Y="1.407430908203" />
                  <Point X="-3.534357421875" Y="1.448457885742" />
                  <Point X="-3.526703857422" Y="1.466935180664" />
                  <Point X="-3.520915527344" Y="1.486794067383" />
                  <Point X="-3.517157226562" Y="1.508109130859" />
                  <Point X="-3.5158046875" Y="1.528749267578" />
                  <Point X="-3.518951171875" Y="1.549192871094" />
                  <Point X="-3.524552978516" Y="1.570099121094" />
                  <Point X="-3.532049804688" Y="1.589377563477" />
                  <Point X="-3.5525546875" Y="1.628767211914" />
                  <Point X="-3.561789550781" Y="1.646507324219" />
                  <Point X="-3.57328125" Y="1.663705932617" />
                  <Point X="-3.587193847656" Y="1.680286376953" />
                  <Point X="-3.602135986328" Y="1.694590209961" />
                  <Point X="-3.866420166016" Y="1.897382568359" />
                  <Point X="-4.351859863281" Y="2.269874267578" />
                  <Point X="-4.181862304688" Y="2.56112109375" />
                  <Point X="-4.081153808594" Y="2.733659423828" />
                  <Point X="-3.801543212891" Y="3.093059326172" />
                  <Point X="-3.750504882813" Y="3.158661621094" />
                  <Point X="-3.657046142578" Y="3.104703125" />
                  <Point X="-3.206656982422" Y="2.844670898438" />
                  <Point X="-3.187724609375" Y="2.836340332031" />
                  <Point X="-3.167082519531" Y="2.829831787109" />
                  <Point X="-3.146794677734" Y="2.825796386719" />
                  <Point X="-3.087810302734" Y="2.820635986328" />
                  <Point X="-3.061245361328" Y="2.818311767578" />
                  <Point X="-3.040565185547" Y="2.818762939453" />
                  <Point X="-3.019106445312" Y="2.821588134766" />
                  <Point X="-2.999015136719" Y="2.826504394531" />
                  <Point X="-2.980463867188" Y="2.835652832031" />
                  <Point X="-2.962209472656" Y="2.847281982422" />
                  <Point X="-2.946077392578" Y="2.860229492188" />
                  <Point X="-2.904209960938" Y="2.902096923828" />
                  <Point X="-2.885353759766" Y="2.920952880859" />
                  <Point X="-2.872406738281" Y="2.937084228516" />
                  <Point X="-2.86077734375" Y="2.955338623047" />
                  <Point X="-2.851628662109" Y="2.973890136719" />
                  <Point X="-2.846712158203" Y="2.993981933594" />
                  <Point X="-2.843886962891" Y="3.015440917969" />
                  <Point X="-2.843435791016" Y="3.03612109375" />
                  <Point X="-2.848596191406" Y="3.09510546875" />
                  <Point X="-2.850920410156" Y="3.121670654297" />
                  <Point X="-2.854955810547" Y="3.141958496094" />
                  <Point X="-2.861464355469" Y="3.162600585938" />
                  <Point X="-2.869794921875" Y="3.181532958984" />
                  <Point X="-2.986907226562" Y="3.384377197266" />
                  <Point X="-3.183333007812" Y="3.724596435547" />
                  <Point X="-2.876023681641" Y="3.960207275391" />
                  <Point X="-2.700626220703" Y="4.094683349609" />
                  <Point X="-2.260249755859" Y="4.339347167969" />
                  <Point X="-2.167036621094" Y="4.391134277344" />
                  <Point X="-2.04319543457" Y="4.229741210938" />
                  <Point X="-2.028891967773" Y="4.214799316406" />
                  <Point X="-2.012312255859" Y="4.200887207031" />
                  <Point X="-1.99511328125" Y="4.189395019531" />
                  <Point X="-1.929463989258" Y="4.155220214844" />
                  <Point X="-1.899897338867" Y="4.139828125" />
                  <Point X="-1.880619628906" Y="4.132331542969" />
                  <Point X="-1.859713012695" Y="4.126729492187" />
                  <Point X="-1.839270263672" Y="4.123582519531" />
                  <Point X="-1.818630981445" Y="4.124935058594" />
                  <Point X="-1.797315551758" Y="4.128692871094" />
                  <Point X="-1.777453613281" Y="4.134481933594" />
                  <Point X="-1.709075439453" Y="4.162805664062" />
                  <Point X="-1.678279663086" Y="4.175561523437" />
                  <Point X="-1.660145385742" Y="4.185510253906" />
                  <Point X="-1.642415649414" Y="4.197924804688" />
                  <Point X="-1.626864257812" Y="4.211562988281" />
                  <Point X="-1.614633300781" Y="4.228243652344" />
                  <Point X="-1.603811279297" Y="4.246987792969" />
                  <Point X="-1.59548046875" Y="4.265921386719" />
                  <Point X="-1.573224487305" Y="4.3365078125" />
                  <Point X="-1.563200927734" Y="4.368297851562" />
                  <Point X="-1.559165649414" Y="4.388584960938" />
                  <Point X="-1.557279174805" Y="4.410146972656" />
                  <Point X="-1.55773034668" Y="4.430826660156" />
                  <Point X="-1.571044677734" Y="4.531959472656" />
                  <Point X="-1.584201904297" Y="4.631897949219" />
                  <Point X="-1.17669921875" Y="4.746147460938" />
                  <Point X="-0.94963494873" Y="4.80980859375" />
                  <Point X="-0.4157706604" Y="4.872289550781" />
                  <Point X="-0.29471081543" Y="4.886458007812" />
                  <Point X="-0.267140838623" Y="4.783564941406" />
                  <Point X="-0.133903305054" Y="4.286315429688" />
                  <Point X="-0.121129844666" Y="4.258124023438" />
                  <Point X="-0.10327155304" Y="4.231397460938" />
                  <Point X="-0.082114532471" Y="4.20880859375" />
                  <Point X="-0.054819446564" Y="4.19421875" />
                  <Point X="-0.024381370544" Y="4.183886230469" />
                  <Point X="0.006155906677" Y="4.178844238281" />
                  <Point X="0.03669316864" Y="4.183886230469" />
                  <Point X="0.067131248474" Y="4.19421875" />
                  <Point X="0.094426467896" Y="4.20880859375" />
                  <Point X="0.115583503723" Y="4.231397460938" />
                  <Point X="0.133441802979" Y="4.258124023438" />
                  <Point X="0.146215255737" Y="4.286315429688" />
                  <Point X="0.206222259521" Y="4.510264648438" />
                  <Point X="0.307419372559" Y="4.8879375" />
                  <Point X="0.645774047852" Y="4.852502441406" />
                  <Point X="0.844041320801" Y="4.831738769531" />
                  <Point X="1.285725952148" Y="4.725102539062" />
                  <Point X="1.481029907227" Y="4.677950195312" />
                  <Point X="1.768061523438" Y="4.573841796875" />
                  <Point X="1.894642578125" Y="4.5279296875" />
                  <Point X="2.172640136719" Y="4.397919433594" />
                  <Point X="2.294576416016" Y="4.340893554688" />
                  <Point X="2.563154785156" Y="4.184419433594" />
                  <Point X="2.680978759766" Y="4.115774902344" />
                  <Point X="2.934262695312" Y="3.935653320312" />
                  <Point X="2.943260009766" Y="3.929254882812" />
                  <Point X="2.741653076172" Y="3.580060791016" />
                  <Point X="2.147581054688" Y="2.551097900391" />
                  <Point X="2.142075927734" Y="2.539931152344" />
                  <Point X="2.133076904297" Y="2.516056640625" />
                  <Point X="2.118274169922" Y="2.460701171875" />
                  <Point X="2.111607177734" Y="2.435770263672" />
                  <Point X="2.108619384766" Y="2.417934570312" />
                  <Point X="2.107728027344" Y="2.380953125" />
                  <Point X="2.1135" Y="2.333086425781" />
                  <Point X="2.116099365234" Y="2.311528320312" />
                  <Point X="2.121441894531" Y="2.289605712891" />
                  <Point X="2.129708007812" Y="2.267516601562" />
                  <Point X="2.140071044922" Y="2.247470947266" />
                  <Point X="2.169689453125" Y="2.203821044922" />
                  <Point X="2.183028808594" Y="2.184162353516" />
                  <Point X="2.194465332031" Y="2.170327880859" />
                  <Point X="2.221599121094" Y="2.145592285156" />
                  <Point X="2.265248779297" Y="2.115973876953" />
                  <Point X="2.284907714844" Y="2.102634765625" />
                  <Point X="2.304953613281" Y="2.092271728516" />
                  <Point X="2.327041503906" Y="2.084006103516" />
                  <Point X="2.348963867188" Y="2.078663574219" />
                  <Point X="2.396830566406" Y="2.072891601562" />
                  <Point X="2.418388671875" Y="2.070291992188" />
                  <Point X="2.436467773438" Y="2.069845703125" />
                  <Point X="2.473206298828" Y="2.074171142578" />
                  <Point X="2.528561767578" Y="2.088974121094" />
                  <Point X="2.553492675781" Y="2.095640869141" />
                  <Point X="2.565282470703" Y="2.099637939453" />
                  <Point X="2.588533935547" Y="2.110145019531" />
                  <Point X="3.051954101562" Y="2.377700683594" />
                  <Point X="3.967326416016" Y="2.906191162109" />
                  <Point X="4.053382568359" Y="2.786592773438" />
                  <Point X="4.123271972656" Y="2.689462402344" />
                  <Point X="4.262198730469" Y="2.459884033203" />
                  <Point X="4.012935302734" Y="2.268617431641" />
                  <Point X="3.230783935547" Y="1.668451171875" />
                  <Point X="3.221425048828" Y="1.660241943359" />
                  <Point X="3.203973632812" Y="1.641627807617" />
                  <Point X="3.164134033203" Y="1.589654174805" />
                  <Point X="3.14619140625" Y="1.566246459961" />
                  <Point X="3.13660546875" Y="1.550911376953" />
                  <Point X="3.121629638672" Y="1.51708605957" />
                  <Point X="3.106789306641" Y="1.464020629883" />
                  <Point X="3.100105712891" Y="1.440121337891" />
                  <Point X="3.09665234375" Y="1.417821777344" />
                  <Point X="3.095836425781" Y="1.394252319336" />
                  <Point X="3.097739501953" Y="1.371768188477" />
                  <Point X="3.109921875" Y="1.312726196289" />
                  <Point X="3.115408447266" Y="1.286135498047" />
                  <Point X="3.1206796875" Y="1.268978393555" />
                  <Point X="3.136282714844" Y="1.235740234375" />
                  <Point X="3.169417236328" Y="1.185377319336" />
                  <Point X="3.184340332031" Y="1.162694946289" />
                  <Point X="3.198893798828" Y="1.145450195312" />
                  <Point X="3.216137939453" Y="1.129360229492" />
                  <Point X="3.234347167969" Y="1.116034912109" />
                  <Point X="3.282363525391" Y="1.089005493164" />
                  <Point X="3.303989257812" Y="1.076832397461" />
                  <Point X="3.320521728516" Y="1.069501342773" />
                  <Point X="3.356118164062" Y="1.059438598633" />
                  <Point X="3.421039794922" Y="1.050858398438" />
                  <Point X="3.450278808594" Y="1.046994018555" />
                  <Point X="3.462702148438" Y="1.046175048828" />
                  <Point X="3.488203613281" Y="1.04698449707" />
                  <Point X="3.928421875" Y="1.104940307617" />
                  <Point X="4.776839355469" Y="1.21663659668" />
                  <Point X="4.815918945312" Y="1.05610949707" />
                  <Point X="4.845936035156" Y="0.932809326172" />
                  <Point X="4.890433105469" Y="0.647012512207" />
                  <Point X="4.890865234375" Y="0.644238586426" />
                  <Point X="4.612942382812" Y="0.56976940918" />
                  <Point X="3.716579833984" Y="0.329589935303" />
                  <Point X="3.704791259766" Y="0.325586608887" />
                  <Point X="3.681545654297" Y="0.315067993164" />
                  <Point X="3.617762207031" Y="0.278200073242" />
                  <Point X="3.589035644531" Y="0.261595428467" />
                  <Point X="3.574311035156" Y="0.251096221924" />
                  <Point X="3.547530761719" Y="0.22557661438" />
                  <Point X="3.509260742188" Y="0.17681137085" />
                  <Point X="3.492024902344" Y="0.154848815918" />
                  <Point X="3.480301269531" Y="0.13556930542" />
                  <Point X="3.47052734375" Y="0.114105690002" />
                  <Point X="3.463680664062" Y="0.092604270935" />
                  <Point X="3.450923828125" Y="0.025993631363" />
                  <Point X="3.445178466797" Y="-0.004006225586" />
                  <Point X="3.443482910156" Y="-0.021875295639" />
                  <Point X="3.445178466797" Y="-0.058553779602" />
                  <Point X="3.457935302734" Y="-0.125164421082" />
                  <Point X="3.463680664062" Y="-0.155164428711" />
                  <Point X="3.47052734375" Y="-0.176665847778" />
                  <Point X="3.480301269531" Y="-0.198129455566" />
                  <Point X="3.492025146484" Y="-0.217409118652" />
                  <Point X="3.530295166016" Y="-0.266174072266" />
                  <Point X="3.547531005859" Y="-0.288136779785" />
                  <Point X="3.559999023438" Y="-0.30123614502" />
                  <Point X="3.589035644531" Y="-0.324155578613" />
                  <Point X="3.652819091797" Y="-0.361023498535" />
                  <Point X="3.681545654297" Y="-0.377628143311" />
                  <Point X="3.692710205078" Y="-0.383139038086" />
                  <Point X="3.716579833984" Y="-0.392149932861" />
                  <Point X="4.120279785156" Y="-0.500321105957" />
                  <Point X="4.891472167969" Y="-0.706961486816" />
                  <Point X="4.871782714844" Y="-0.837558532715" />
                  <Point X="4.855022460938" Y="-0.948726135254" />
                  <Point X="4.801173828125" Y="-1.18469909668" />
                  <Point X="4.46391015625" Y="-1.140297485352" />
                  <Point X="3.424382080078" Y="-1.003441040039" />
                  <Point X="3.408035644531" Y="-1.002710327148" />
                  <Point X="3.374658691406" Y="-1.005508728027" />
                  <Point X="3.249474365234" Y="-1.032718017578" />
                  <Point X="3.193094482422" Y="-1.044972412109" />
                  <Point X="3.163974121094" Y="-1.056596923828" />
                  <Point X="3.136147460938" Y="-1.073489257812" />
                  <Point X="3.112397460938" Y="-1.093960083008" />
                  <Point X="3.036731445313" Y="-1.184962768555" />
                  <Point X="3.002653320312" Y="-1.225948364258" />
                  <Point X="2.987932617188" Y="-1.250330566406" />
                  <Point X="2.976589355469" Y="-1.277715698242" />
                  <Point X="2.969757568359" Y="-1.305365478516" />
                  <Point X="2.958912841797" Y="-1.423217895508" />
                  <Point X="2.954028564453" Y="-1.476295776367" />
                  <Point X="2.956347167969" Y="-1.507564208984" />
                  <Point X="2.964078613281" Y="-1.539185180664" />
                  <Point X="2.976450195312" Y="-1.567996582031" />
                  <Point X="3.045729003906" Y="-1.675755249023" />
                  <Point X="3.076930419922" Y="-1.724287109375" />
                  <Point X="3.086932861328" Y="-1.737239013672" />
                  <Point X="3.110628417969" Y="-1.760909179688" />
                  <Point X="3.485264648438" Y="-2.048377929688" />
                  <Point X="4.213122070312" Y="-2.606882568359" />
                  <Point X="4.172055175781" Y="-2.673334716797" />
                  <Point X="4.1248046875" Y="-2.749793701172" />
                  <Point X="4.028981445312" Y="-2.885945068359" />
                  <Point X="3.726794677734" Y="-2.711477539062" />
                  <Point X="2.800954589844" Y="-2.176943115234" />
                  <Point X="2.786131591797" Y="-2.170012451172" />
                  <Point X="2.754224609375" Y="-2.159825195312" />
                  <Point X="2.605235351562" Y="-2.13291796875" />
                  <Point X="2.538134277344" Y="-2.120799560547" />
                  <Point X="2.506781982422" Y="-2.120395263672" />
                  <Point X="2.474609375" Y="-2.125353027344" />
                  <Point X="2.444833496094" Y="-2.135176757812" />
                  <Point X="2.321060058594" Y="-2.200317871094" />
                  <Point X="2.265315429688" Y="-2.229655761719" />
                  <Point X="2.242384765625" Y="-2.246548828125" />
                  <Point X="2.221424804688" Y="-2.267508789062" />
                  <Point X="2.204531738281" Y="-2.290439453125" />
                  <Point X="2.139390625" Y="-2.414212890625" />
                  <Point X="2.110052734375" Y="-2.469957519531" />
                  <Point X="2.100229003906" Y="-2.499733398438" />
                  <Point X="2.095271240234" Y="-2.531906005859" />
                  <Point X="2.095675537109" Y="-2.563258300781" />
                  <Point X="2.122582763672" Y="-2.712247558594" />
                  <Point X="2.134701171875" Y="-2.779348632812" />
                  <Point X="2.138985595703" Y="-2.795141845703" />
                  <Point X="2.151819091797" Y="-2.826078613281" />
                  <Point X="2.392560546875" Y="-3.243054931641" />
                  <Point X="2.861283203125" Y="-4.054906494141" />
                  <Point X="2.837913818359" Y="-4.071598632812" />
                  <Point X="2.781847412109" Y="-4.111645507812" />
                  <Point X="2.701765136719" Y="-4.163481445312" />
                  <Point X="2.465047607422" Y="-3.854985595703" />
                  <Point X="1.758546142578" Y="-2.934255126953" />
                  <Point X="1.747503540039" Y="-2.922178955078" />
                  <Point X="1.721924194336" Y="-2.900557373047" />
                  <Point X="1.57498059082" Y="-2.806086181641" />
                  <Point X="1.50880078125" Y="-2.763538574219" />
                  <Point X="1.479989746094" Y="-2.751167236328" />
                  <Point X="1.448368774414" Y="-2.743435546875" />
                  <Point X="1.417099731445" Y="-2.741116699219" />
                  <Point X="1.256391845703" Y="-2.755905273438" />
                  <Point X="1.184012817383" Y="-2.762565673828" />
                  <Point X="1.15636315918" Y="-2.769397460938" />
                  <Point X="1.128978027344" Y="-2.780740722656" />
                  <Point X="1.104595825195" Y="-2.795461181641" />
                  <Point X="0.980501342773" Y="-2.898641601563" />
                  <Point X="0.924612243652" Y="-2.945111572266" />
                  <Point X="0.904141479492" Y="-2.968861572266" />
                  <Point X="0.887249084473" Y="-2.996688232422" />
                  <Point X="0.875624328613" Y="-3.025808837891" />
                  <Point X="0.838520690918" Y="-3.196514892578" />
                  <Point X="0.821810058594" Y="-3.273396728516" />
                  <Point X="0.819724487305" Y="-3.289627441406" />
                  <Point X="0.819742248535" Y="-3.323120117188" />
                  <Point X="0.887966430664" Y="-3.841334716797" />
                  <Point X="1.022065307617" Y="-4.859915527344" />
                  <Point X="0.975677307129" Y="-4.870083496094" />
                  <Point X="0.929315490723" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058435791016" Y="-4.752635253906" />
                  <Point X="-1.141246337891" Y="-4.731328613281" />
                  <Point X="-1.120775634766" Y="-4.575838378906" />
                  <Point X="-1.120077514648" Y="-4.568099609375" />
                  <Point X="-1.119451782227" Y="-4.541033691406" />
                  <Point X="-1.121759277344" Y="-4.509328125" />
                  <Point X="-1.123334106445" Y="-4.497690429688" />
                  <Point X="-1.16506640625" Y="-4.287888671875" />
                  <Point X="-1.183861450195" Y="-4.193398925781" />
                  <Point X="-1.18812487793" Y="-4.178469238281" />
                  <Point X="-1.199026855469" Y="-4.149502929688" />
                  <Point X="-1.205665771484" Y="-4.135466308594" />
                  <Point X="-1.221737792969" Y="-4.10762890625" />
                  <Point X="-1.230573852539" Y="-4.094861816406" />
                  <Point X="-1.250208251953" Y="-4.070937011719" />
                  <Point X="-1.261006713867" Y="-4.059779296875" />
                  <Point X="-1.421834838867" Y="-3.918736816406" />
                  <Point X="-1.494267700195" Y="-3.855214599609" />
                  <Point X="-1.506738647461" Y="-3.845965576172" />
                  <Point X="-1.533021606445" Y="-3.829621582031" />
                  <Point X="-1.546833618164" Y="-3.822526855469" />
                  <Point X="-1.576530761719" Y="-3.810225830078" />
                  <Point X="-1.591313476562" Y="-3.805476074219" />
                  <Point X="-1.621454833984" Y="-3.798447998047" />
                  <Point X="-1.636813964844" Y="-3.796169677734" />
                  <Point X="-1.850268554688" Y="-3.782179199219" />
                  <Point X="-1.946403198242" Y="-3.775878173828" />
                  <Point X="-1.961928222656" Y="-3.776132324219" />
                  <Point X="-1.992729614258" Y="-3.779166015625" />
                  <Point X="-2.008006103516" Y="-3.781945556641" />
                  <Point X="-2.03905480957" Y="-3.790265136719" />
                  <Point X="-2.053674560547" Y="-3.795496337891" />
                  <Point X="-2.081865478516" Y="-3.808269775391" />
                  <Point X="-2.095437011719" Y="-3.815812011719" />
                  <Point X="-2.273298828125" Y="-3.934655517578" />
                  <Point X="-2.353403564453" Y="-3.988180175781" />
                  <Point X="-2.359681640625" Y="-3.992757080078" />
                  <Point X="-2.380442382812" Y="-4.010131347656" />
                  <Point X="-2.402759277344" Y="-4.032771728516" />
                  <Point X="-2.410470947266" Y="-4.041629394531" />
                  <Point X="-2.480306152344" Y="-4.132640136719" />
                  <Point X="-2.503202636719" Y="-4.162479492188" />
                  <Point X="-2.621544677734" Y="-4.089205078125" />
                  <Point X="-2.747583007812" Y="-4.011165283203" />
                  <Point X="-2.980862792969" Y="-3.831547607422" />
                  <Point X="-2.854759277344" Y="-3.613129882813" />
                  <Point X="-2.341488769531" Y="-2.724119628906" />
                  <Point X="-2.334850097656" Y="-2.710083251953" />
                  <Point X="-2.323947998047" Y="-2.681116943359" />
                  <Point X="-2.319684570312" Y="-2.666187011719" />
                  <Point X="-2.313413574219" Y="-2.634660644531" />
                  <Point X="-2.311638916016" Y="-2.619235351562" />
                  <Point X="-2.310626220703" Y="-2.588301269531" />
                  <Point X="-2.314666259766" Y="-2.557615234375" />
                  <Point X="-2.323650878906" Y="-2.527997558594" />
                  <Point X="-2.329357421875" Y="-2.513557128906" />
                  <Point X="-2.343574462891" Y="-2.484728271484" />
                  <Point X="-2.351557128906" Y="-2.47141015625" />
                  <Point X="-2.369584716797" Y="-2.446253173828" />
                  <Point X="-2.379629638672" Y="-2.434414306641" />
                  <Point X="-2.396978271484" Y="-2.417065673828" />
                  <Point X="-2.408817138672" Y="-2.407020751953" />
                  <Point X="-2.433974121094" Y="-2.388993164062" />
                  <Point X="-2.447292236328" Y="-2.381010498047" />
                  <Point X="-2.47612109375" Y="-2.366793457031" />
                  <Point X="-2.490561523438" Y="-2.361086914062" />
                  <Point X="-2.520179199219" Y="-2.352102294922" />
                  <Point X="-2.550865234375" Y="-2.348062255859" />
                  <Point X="-2.581799316406" Y="-2.349074951172" />
                  <Point X="-2.597224609375" Y="-2.350849609375" />
                  <Point X="-2.628750976562" Y="-2.357120605469" />
                  <Point X="-2.643680908203" Y="-2.361384033203" />
                  <Point X="-2.672647216797" Y="-2.372286132812" />
                  <Point X="-2.68668359375" Y="-2.378924804688" />
                  <Point X="-3.087072509766" Y="-2.610089599609" />
                  <Point X="-3.79308984375" Y="-3.017708496094" />
                  <Point X="-3.904438720703" Y="-2.871418701172" />
                  <Point X="-4.004013427734" Y="-2.740597412109" />
                  <Point X="-4.181265136719" Y="-2.443373535156" />
                  <Point X="-3.946178955078" Y="-2.262985351562" />
                  <Point X="-3.048122314453" Y="-1.573882080078" />
                  <Point X="-3.036481933594" Y="-1.563309570312" />
                  <Point X="-3.015104980469" Y="-1.540389892578" />
                  <Point X="-3.005368408203" Y="-1.528042602539" />
                  <Point X="-2.987403564453" Y="-1.500911743164" />
                  <Point X="-2.979836669922" Y="-1.487126708984" />
                  <Point X="-2.967080322266" Y="-1.458496704102" />
                  <Point X="-2.961890869141" Y="-1.443651733398" />
                  <Point X="-2.956578857422" Y="-1.423141845703" />
                  <Point X="-2.951552734375" Y="-1.398803588867" />
                  <Point X="-2.948748535156" Y="-1.368375488281" />
                  <Point X="-2.948577880859" Y="-1.353048217773" />
                  <Point X="-2.950786621094" Y="-1.321377441406" />
                  <Point X="-2.953083251953" Y="-1.306221191406" />
                  <Point X="-2.960084472656" Y="-1.276475952148" />
                  <Point X="-2.971778320312" Y="-1.248244384766" />
                  <Point X="-2.987860595703" Y="-1.222260498047" />
                  <Point X="-2.996953613281" Y="-1.209918945312" />
                  <Point X="-3.017786376953" Y="-1.185962280273" />
                  <Point X="-3.02874609375" Y="-1.175244140625" />
                  <Point X="-3.05224609375" Y="-1.155710327148" />
                  <Point X="-3.064786621094" Y="-1.14689440918" />
                  <Point X="-3.083045898438" Y="-1.136147949219" />
                  <Point X="-3.105434082031" Y="-1.124481201172" />
                  <Point X="-3.134697021484" Y="-1.113257202148" />
                  <Point X="-3.149795410156" Y="-1.108859985352" />
                  <Point X="-3.181683105469" Y="-1.102378540039" />
                  <Point X="-3.197298828125" Y="-1.100532348633" />
                  <Point X="-3.228622070312" Y="-1.09944140625" />
                  <Point X="-3.244329101562" Y="-1.100196533203" />
                  <Point X="-3.749782226562" Y="-1.166740722656" />
                  <Point X="-4.660920898438" Y="-1.286694335938" />
                  <Point X="-4.701814941406" Y="-1.126594360352" />
                  <Point X="-4.740762207031" Y="-0.974118041992" />
                  <Point X="-4.786452636719" Y="-0.65465447998" />
                  <Point X="-4.531310058594" Y="-0.586289123535" />
                  <Point X="-3.508288085938" Y="-0.312171295166" />
                  <Point X="-3.500476806641" Y="-0.309712768555" />
                  <Point X="-3.473931884766" Y="-0.299251495361" />
                  <Point X="-3.444166992188" Y="-0.283893096924" />
                  <Point X="-3.433561523438" Y="-0.277513763428" />
                  <Point X="-3.414426513672" Y="-0.264232971191" />
                  <Point X="-3.393671630859" Y="-0.248242324829" />
                  <Point X="-3.371216552734" Y="-0.226358795166" />
                  <Point X="-3.360898193359" Y="-0.214484268188" />
                  <Point X="-3.341654052734" Y="-0.18822869873" />
                  <Point X="-3.333436767578" Y="-0.174814147949" />
                  <Point X="-3.319328613281" Y="-0.146813140869" />
                  <Point X="-3.313437744141" Y="-0.132226852417" />
                  <Point X="-3.307059326172" Y="-0.111675575256" />
                  <Point X="-3.300990722656" Y="-0.088501098633" />
                  <Point X="-3.296721191406" Y="-0.060341369629" />
                  <Point X="-3.295647705078" Y="-0.046100471497" />
                  <Point X="-3.295647705078" Y="-0.01645971489" />
                  <Point X="-3.296721191406" Y="-0.002218817711" />
                  <Point X="-3.300990722656" Y="0.02594106102" />
                  <Point X="-3.304186767578" Y="0.039859893799" />
                  <Point X="-3.310565185547" Y="0.060411170959" />
                  <Point X="-3.319328613281" Y="0.084253105164" />
                  <Point X="-3.333436767578" Y="0.112253959656" />
                  <Point X="-3.341654052734" Y="0.125668365479" />
                  <Point X="-3.360898193359" Y="0.151924087524" />
                  <Point X="-3.371216552734" Y="0.163798751831" />
                  <Point X="-3.393671630859" Y="0.185682434082" />
                  <Point X="-3.40580859375" Y="0.195691452026" />
                  <Point X="-3.424943603516" Y="0.208972244263" />
                  <Point X="-3.44224609375" Y="0.219656600952" />
                  <Point X="-3.466432128906" Y="0.232858322144" />
                  <Point X="-3.476646728516" Y="0.237669540405" />
                  <Point X="-3.497574462891" Y="0.246045928955" />
                  <Point X="-3.508288085938" Y="0.249611251831" />
                  <Point X="-3.969032470703" Y="0.373067382812" />
                  <Point X="-4.785446289062" Y="0.591824584961" />
                  <Point X="-4.7564296875" Y="0.787913146973" />
                  <Point X="-4.731331054688" Y="0.95752935791" />
                  <Point X="-4.633586425781" Y="1.318237060547" />
                  <Point X="-4.511393066406" Y="1.302150024414" />
                  <Point X="-3.778066162109" Y="1.20560559082" />
                  <Point X="-3.76773828125" Y="1.204815429688" />
                  <Point X="-3.747058105469" Y="1.204364257812" />
                  <Point X="-3.736705810547" Y="1.204703125" />
                  <Point X="-3.715144287109" Y="1.20658984375" />
                  <Point X="-3.704890136719" Y="1.208053710938" />
                  <Point X="-3.684603271484" Y="1.212089233398" />
                  <Point X="-3.674570800781" Y="1.214660522461" />
                  <Point X="-3.632218994141" Y="1.228013916016" />
                  <Point X="-3.603451171875" Y="1.237676513672" />
                  <Point X="-3.584518554688" Y="1.246006958008" />
                  <Point X="-3.575279541016" Y="1.250689086914" />
                  <Point X="-3.55653515625" Y="1.261510864258" />
                  <Point X="-3.547860351562" Y="1.267171264648" />
                  <Point X="-3.531179199219" Y="1.27940234375" />
                  <Point X="-3.515928710938" Y="1.293376831055" />
                  <Point X="-3.502290283203" Y="1.308928344727" />
                  <Point X="-3.495895751953" Y="1.317076416016" />
                  <Point X="-3.483481201172" Y="1.334806152344" />
                  <Point X="-3.47801171875" Y="1.343601806641" />
                  <Point X="-3.4680625" Y="1.361736694336" />
                  <Point X="-3.463582763672" Y="1.371076049805" />
                  <Point X="-3.446588867188" Y="1.412103027344" />
                  <Point X="-3.435499023438" Y="1.44035144043" />
                  <Point X="-3.429710693359" Y="1.460210327148" />
                  <Point X="-3.427358642578" Y="1.470297973633" />
                  <Point X="-3.423600341797" Y="1.491613037109" />
                  <Point X="-3.422360595703" Y="1.501897094727" />
                  <Point X="-3.421008056641" Y="1.522537231445" />
                  <Point X="-3.421910400391" Y="1.543200561523" />
                  <Point X="-3.425056884766" Y="1.563644165039" />
                  <Point X="-3.427188232422" Y="1.573780639648" />
                  <Point X="-3.432790039062" Y="1.594686889648" />
                  <Point X="-3.436011962891" Y="1.604530151367" />
                  <Point X="-3.443508789062" Y="1.62380859375" />
                  <Point X="-3.447783691406" Y="1.633243530273" />
                  <Point X="-3.468288574219" Y="1.672633178711" />
                  <Point X="-3.482799804688" Y="1.699286254883" />
                  <Point X="-3.494291503906" Y="1.716484985352" />
                  <Point X="-3.500506835938" Y="1.724770629883" />
                  <Point X="-3.514419433594" Y="1.741351074219" />
                  <Point X="-3.521500488281" Y="1.748911254883" />
                  <Point X="-3.536442626953" Y="1.763215087891" />
                  <Point X="-3.544303710938" Y="1.769958740234" />
                  <Point X="-3.808587890625" Y="1.972751098633" />
                  <Point X="-4.227614257812" Y="2.294282226563" />
                  <Point X="-4.099815917969" Y="2.513231689453" />
                  <Point X="-4.002293212891" Y="2.680312011719" />
                  <Point X="-3.7265625" Y="3.034724853516" />
                  <Point X="-3.726338623047" Y="3.035012451172" />
                  <Point X="-3.704546142578" Y="3.022430664062" />
                  <Point X="-3.254156982422" Y="2.7623984375" />
                  <Point X="-3.244918457031" Y="2.757716552734" />
                  <Point X="-3.225986083984" Y="2.749385986328" />
                  <Point X="-3.216292236328" Y="2.745737304688" />
                  <Point X="-3.195650146484" Y="2.739228759766" />
                  <Point X="-3.185615722656" Y="2.736656982422" />
                  <Point X="-3.165327880859" Y="2.732621582031" />
                  <Point X="-3.155074462891" Y="2.731157958984" />
                  <Point X="-3.096090087891" Y="2.725997558594" />
                  <Point X="-3.069525146484" Y="2.723673339844" />
                  <Point X="-3.059173339844" Y="2.723334472656" />
                  <Point X="-3.038493164062" Y="2.723785644531" />
                  <Point X="-3.028164794922" Y="2.724575683594" />
                  <Point X="-3.006706054688" Y="2.727400878906" />
                  <Point X="-2.996526611328" Y="2.729310546875" />
                  <Point X="-2.976435302734" Y="2.734226806641" />
                  <Point X="-2.956997802734" Y="2.741301513672" />
                  <Point X="-2.938446533203" Y="2.750449951172" />
                  <Point X="-2.929420898438" Y="2.755530273438" />
                  <Point X="-2.911166503906" Y="2.767159423828" />
                  <Point X="-2.902746337891" Y="2.773193359375" />
                  <Point X="-2.886614257812" Y="2.786140869141" />
                  <Point X="-2.87890234375" Y="2.793054443359" />
                  <Point X="-2.837034912109" Y="2.834921875" />
                  <Point X="-2.818178710938" Y="2.853777832031" />
                  <Point X="-2.811265380859" Y="2.861489501953" />
                  <Point X="-2.798318359375" Y="2.877620849609" />
                  <Point X="-2.792284667969" Y="2.886040527344" />
                  <Point X="-2.780655273438" Y="2.904294921875" />
                  <Point X="-2.775574707031" Y="2.913320800781" />
                  <Point X="-2.766426025391" Y="2.931872314453" />
                  <Point X="-2.759351318359" Y="2.951309570312" />
                  <Point X="-2.754434814453" Y="2.971401367188" />
                  <Point X="-2.752524902344" Y="2.981581542969" />
                  <Point X="-2.749699707031" Y="3.003040527344" />
                  <Point X="-2.748909667969" Y="3.013368896484" />
                  <Point X="-2.748458496094" Y="3.034049072266" />
                  <Point X="-2.748797363281" Y="3.044400878906" />
                  <Point X="-2.753957763672" Y="3.103385253906" />
                  <Point X="-2.756281982422" Y="3.129950439453" />
                  <Point X="-2.757745605469" Y="3.140203857422" />
                  <Point X="-2.761781005859" Y="3.160491699219" />
                  <Point X="-2.764352783203" Y="3.170526123047" />
                  <Point X="-2.770861328125" Y="3.191168212891" />
                  <Point X="-2.774510009766" Y="3.200862060547" />
                  <Point X="-2.782840576172" Y="3.219794433594" />
                  <Point X="-2.787522460938" Y="3.229032958984" />
                  <Point X="-2.904634765625" Y="3.431877197266" />
                  <Point X="-3.059387207031" Y="3.699916259766" />
                  <Point X="-2.818221679688" Y="3.884815429688" />
                  <Point X="-2.648374755859" Y="4.015036132812" />
                  <Point X="-2.214112304688" Y="4.256303222656" />
                  <Point X="-2.192525146484" Y="4.268296386719" />
                  <Point X="-2.118563964844" Y="4.171908691406" />
                  <Point X="-2.111820556641" Y="4.164048339844" />
                  <Point X="-2.097517089844" Y="4.149106445313" />
                  <Point X="-2.08995703125" Y="4.142025390625" />
                  <Point X="-2.073377197266" Y="4.12811328125" />
                  <Point X="-2.065092041016" Y="4.121897949219" />
                  <Point X="-2.047893066406" Y="4.110405761719" />
                  <Point X="-2.038979370117" Y="4.10512890625" />
                  <Point X="-1.973329956055" Y="4.070954101562" />
                  <Point X="-1.943763427734" Y="4.055562011719" />
                  <Point X="-1.934328491211" Y="4.051287109375" />
                  <Point X="-1.91505078125" Y="4.043790527344" />
                  <Point X="-1.905208007812" Y="4.040568847656" />
                  <Point X="-1.884301269531" Y="4.034966796875" />
                  <Point X="-1.874167236328" Y="4.032835449219" />
                  <Point X="-1.853724365234" Y="4.029688476562" />
                  <Point X="-1.833057983398" Y="4.028785888672" />
                  <Point X="-1.812418701172" Y="4.030138427734" />
                  <Point X="-1.802137329102" Y="4.031377929688" />
                  <Point X="-1.780821899414" Y="4.035135742188" />
                  <Point X="-1.770732421875" Y="4.037488037109" />
                  <Point X="-1.750870605469" Y="4.043277099609" />
                  <Point X="-1.741097900391" Y="4.046713623047" />
                  <Point X="-1.672719848633" Y="4.075037353516" />
                  <Point X="-1.641924072266" Y="4.087793212891" />
                  <Point X="-1.63258605957" Y="4.092272216797" />
                  <Point X="-1.614451660156" Y="4.102221191406" />
                  <Point X="-1.605655517578" Y="4.107690917969" />
                  <Point X="-1.58792565918" Y="4.12010546875" />
                  <Point X="-1.579777954102" Y="4.1265" />
                  <Point X="-1.56422644043" Y="4.140138183594" />
                  <Point X="-1.550252441406" Y="4.155388183594" />
                  <Point X="-1.538021362305" Y="4.172068847656" />
                  <Point X="-1.532361083984" Y="4.180743164062" />
                  <Point X="-1.52153918457" Y="4.199487304687" />
                  <Point X="-1.516856445312" Y="4.208727539062" />
                  <Point X="-1.508525512695" Y="4.227661132812" />
                  <Point X="-1.504877441406" Y="4.237354003906" />
                  <Point X="-1.482621459961" Y="4.307940429688" />
                  <Point X="-1.472597900391" Y="4.33973046875" />
                  <Point X="-1.470026245117" Y="4.349764648437" />
                  <Point X="-1.465990966797" Y="4.370051757812" />
                  <Point X="-1.46452722168" Y="4.380305175781" />
                  <Point X="-1.46264074707" Y="4.4018671875" />
                  <Point X="-1.462301757812" Y="4.412219238281" />
                  <Point X="-1.462752929688" Y="4.432898925781" />
                  <Point X="-1.46354309082" Y="4.4432265625" />
                  <Point X="-1.476857421875" Y="4.544359375" />
                  <Point X="-1.479266113281" Y="4.562654785156" />
                  <Point X="-1.151053466797" Y="4.654674316406" />
                  <Point X="-0.931174865723" Y="4.716320800781" />
                  <Point X="-0.404727661133" Y="4.77793359375" />
                  <Point X="-0.365222045898" Y="4.782557128906" />
                  <Point X="-0.358903778076" Y="4.758977050781" />
                  <Point X="-0.22566633606" Y="4.261727539062" />
                  <Point X="-0.220435256958" Y="4.247107910156" />
                  <Point X="-0.207661849976" Y="4.218916503906" />
                  <Point X="-0.200119247437" Y="4.205344726562" />
                  <Point X="-0.182260864258" Y="4.178618164062" />
                  <Point X="-0.17260824585" Y="4.166455566406" />
                  <Point X="-0.151451187134" Y="4.143866699219" />
                  <Point X="-0.126897941589" Y="4.125026367188" />
                  <Point X="-0.099602897644" Y="4.110436523438" />
                  <Point X="-0.085356651306" Y="4.104260742188" />
                  <Point X="-0.054918682098" Y="4.093927978516" />
                  <Point X="-0.039857234955" Y="4.090155273438" />
                  <Point X="-0.009319986343" Y="4.08511328125" />
                  <Point X="0.021631772995" Y="4.08511328125" />
                  <Point X="0.052169025421" Y="4.090155273438" />
                  <Point X="0.06723046875" Y="4.093927978516" />
                  <Point X="0.097668441772" Y="4.104260742188" />
                  <Point X="0.111914535522" Y="4.110436523438" />
                  <Point X="0.139209732056" Y="4.125026367188" />
                  <Point X="0.163763122559" Y="4.143866699219" />
                  <Point X="0.184920181274" Y="4.166455566406" />
                  <Point X="0.194572952271" Y="4.178617675781" />
                  <Point X="0.212431182861" Y="4.205344238281" />
                  <Point X="0.2199737854" Y="4.218916503906" />
                  <Point X="0.232747192383" Y="4.247107910156" />
                  <Point X="0.237978271484" Y="4.261727539062" />
                  <Point X="0.297985198975" Y="4.485676757813" />
                  <Point X="0.378190216064" Y="4.785006347656" />
                  <Point X="0.635879089355" Y="4.758019042969" />
                  <Point X="0.827876831055" Y="4.737912109375" />
                  <Point X="1.263430664062" Y="4.632755859375" />
                  <Point X="1.453599121094" Y="4.586843261719" />
                  <Point X="1.735669189453" Y="4.484534667969" />
                  <Point X="1.85825402832" Y="4.440072265625" />
                  <Point X="2.132395263672" Y="4.311865234375" />
                  <Point X="2.250451660156" Y="4.256653808594" />
                  <Point X="2.515331787109" Y="4.102334472656" />
                  <Point X="2.629434570312" Y="4.035857666016" />
                  <Point X="2.817780029297" Y="3.901916748047" />
                  <Point X="2.659380615234" Y="3.627560791016" />
                  <Point X="2.06530859375" Y="2.598597900391" />
                  <Point X="2.062373046875" Y="2.593104980469" />
                  <Point X="2.053181152344" Y="2.573438232422" />
                  <Point X="2.044182128906" Y="2.549563720703" />
                  <Point X="2.041301635742" Y="2.540598388672" />
                  <Point X="2.026498779297" Y="2.485242919922" />
                  <Point X="2.01983190918" Y="2.460312011719" />
                  <Point X="2.017912719727" Y="2.451465820312" />
                  <Point X="2.013646972656" Y="2.420223632812" />
                  <Point X="2.012755615234" Y="2.3832421875" />
                  <Point X="2.013411254883" Y="2.369580078125" />
                  <Point X="2.019183105469" Y="2.321713378906" />
                  <Point X="2.021782592773" Y="2.300155273438" />
                  <Point X="2.02380078125" Y="2.28903515625" />
                  <Point X="2.029143066406" Y="2.267112548828" />
                  <Point X="2.032467773437" Y="2.256310058594" />
                  <Point X="2.040733886719" Y="2.234220947266" />
                  <Point X="2.045318115234" Y="2.223889404297" />
                  <Point X="2.055681152344" Y="2.20384375" />
                  <Point X="2.061459960938" Y="2.194129638672" />
                  <Point X="2.091078369141" Y="2.150479736328" />
                  <Point X="2.104417724609" Y="2.130821044922" />
                  <Point X="2.109808349609" Y="2.123633300781" />
                  <Point X="2.130464355469" Y="2.100121826172" />
                  <Point X="2.157598144531" Y="2.075386230469" />
                  <Point X="2.1682578125" Y="2.066981201172" />
                  <Point X="2.211907470703" Y="2.037362792969" />
                  <Point X="2.23156640625" Y="2.024023681641" />
                  <Point X="2.241281005859" Y="2.018244628906" />
                  <Point X="2.261326904297" Y="2.007881591797" />
                  <Point X="2.271658203125" Y="2.003297363281" />
                  <Point X="2.29374609375" Y="1.995031860352" />
                  <Point X="2.304548095703" Y="1.991707397461" />
                  <Point X="2.326470458984" Y="1.986364868164" />
                  <Point X="2.337590820312" Y="1.984346801758" />
                  <Point X="2.385457519531" Y="1.978574951172" />
                  <Point X="2.407015625" Y="1.975975219727" />
                  <Point X="2.416044189453" Y="1.975320922852" />
                  <Point X="2.447575927734" Y="1.975497314453" />
                  <Point X="2.484314453125" Y="1.979822753906" />
                  <Point X="2.497748535156" Y="1.982395996094" />
                  <Point X="2.553104003906" Y="1.997198974609" />
                  <Point X="2.578034912109" Y="2.003865722656" />
                  <Point X="2.583995117188" Y="2.005670898438" />
                  <Point X="2.604403076172" Y="2.013066650391" />
                  <Point X="2.627654541016" Y="2.023573730469" />
                  <Point X="2.636033935547" Y="2.027872558594" />
                  <Point X="3.099454101562" Y="2.295428222656" />
                  <Point X="3.940404785156" Y="2.780951416016" />
                  <Point X="3.976270019531" Y="2.731106933594" />
                  <Point X="4.043952392578" Y="2.637043701172" />
                  <Point X="4.136884765625" Y="2.483472167969" />
                  <Point X="3.955103027344" Y="2.343986083984" />
                  <Point X="3.172951660156" Y="1.743819702148" />
                  <Point X="3.168138671875" Y="1.739869628906" />
                  <Point X="3.152120361328" Y="1.725217529297" />
                  <Point X="3.134668945312" Y="1.706603515625" />
                  <Point X="3.128576171875" Y="1.699422485352" />
                  <Point X="3.088736572266" Y="1.647448852539" />
                  <Point X="3.070793945312" Y="1.624041259766" />
                  <Point X="3.065635253906" Y="1.616602050781" />
                  <Point X="3.049738525391" Y="1.589370849609" />
                  <Point X="3.034762695312" Y="1.555545532227" />
                  <Point X="3.030139892578" Y="1.542672119141" />
                  <Point X="3.015299560547" Y="1.489606689453" />
                  <Point X="3.008615966797" Y="1.465707397461" />
                  <Point X="3.006224853516" Y="1.45466003418" />
                  <Point X="3.002771484375" Y="1.432360473633" />
                  <Point X="3.001709228516" Y="1.421108520508" />
                  <Point X="3.000893310547" Y="1.3975390625" />
                  <Point X="3.001174804688" Y="1.386240112305" />
                  <Point X="3.003077880859" Y="1.363755981445" />
                  <Point X="3.004699462891" Y="1.352570800781" />
                  <Point X="3.016881835938" Y="1.293528808594" />
                  <Point X="3.022368408203" Y="1.266938110352" />
                  <Point X="3.02459765625" Y="1.258235351562" />
                  <Point X="3.03468359375" Y="1.228609130859" />
                  <Point X="3.050286621094" Y="1.19537097168" />
                  <Point X="3.056918945312" Y="1.183525512695" />
                  <Point X="3.090053466797" Y="1.133162597656" />
                  <Point X="3.1049765625" Y="1.110480224609" />
                  <Point X="3.111739257813" Y="1.101424438477" />
                  <Point X="3.126292724609" Y="1.08417956543" />
                  <Point X="3.134083496094" Y="1.075990722656" />
                  <Point X="3.151327636719" Y="1.059900756836" />
                  <Point X="3.160035400391" Y="1.05269519043" />
                  <Point X="3.178244628906" Y="1.039369995117" />
                  <Point X="3.187745849609" Y="1.03325012207" />
                  <Point X="3.235762207031" Y="1.006220703125" />
                  <Point X="3.257387939453" Y="0.994047607422" />
                  <Point X="3.265479492188" Y="0.989987670898" />
                  <Point X="3.294678955078" Y="0.978083862305" />
                  <Point X="3.330275390625" Y="0.968021057129" />
                  <Point X="3.343670898438" Y="0.965257568359" />
                  <Point X="3.408592529297" Y="0.956677429199" />
                  <Point X="3.437831542969" Y="0.952812927246" />
                  <Point X="3.444029785156" Y="0.952199707031" />
                  <Point X="3.465716064453" Y="0.95122277832" />
                  <Point X="3.491217529297" Y="0.952032348633" />
                  <Point X="3.500603515625" Y="0.952797180176" />
                  <Point X="3.940821777344" Y="1.010753112793" />
                  <Point X="4.704703613281" Y="1.111319946289" />
                  <Point X="4.723614746094" Y="1.033638305664" />
                  <Point X="4.75268359375" Y="0.914233886719" />
                  <Point X="4.78387109375" Y="0.713920898438" />
                  <Point X="4.588354492188" Y="0.661532409668" />
                  <Point X="3.691991943359" Y="0.421352874756" />
                  <Point X="3.686031738281" Y="0.419544433594" />
                  <Point X="3.665626708984" Y="0.412137969971" />
                  <Point X="3.642381103516" Y="0.401619476318" />
                  <Point X="3.634004638672" Y="0.397316711426" />
                  <Point X="3.570221191406" Y="0.360448699951" />
                  <Point X="3.541494628906" Y="0.343844116211" />
                  <Point X="3.533881835938" Y="0.338945800781" />
                  <Point X="3.508774169922" Y="0.319870635986" />
                  <Point X="3.481993896484" Y="0.294351043701" />
                  <Point X="3.472796630859" Y="0.2842265625" />
                  <Point X="3.434526611328" Y="0.235461303711" />
                  <Point X="3.417290771484" Y="0.213498703003" />
                  <Point X="3.410854248047" Y="0.204207687378" />
                  <Point X="3.399130615234" Y="0.18492817688" />
                  <Point X="3.393843505859" Y="0.174939834595" />
                  <Point X="3.384069580078" Y="0.153476150513" />
                  <Point X="3.380005859375" Y="0.14293031311" />
                  <Point X="3.373159179688" Y="0.121428894043" />
                  <Point X="3.370376220703" Y="0.110473304749" />
                  <Point X="3.357619384766" Y="0.043862606049" />
                  <Point X="3.351874023438" Y="0.013862774849" />
                  <Point X="3.350603271484" Y="0.004967842579" />
                  <Point X="3.348584228516" Y="-0.02626228714" />
                  <Point X="3.350279785156" Y="-0.062940784454" />
                  <Point X="3.351874023438" Y="-0.076422813416" />
                  <Point X="3.364630859375" Y="-0.14303352356" />
                  <Point X="3.370376220703" Y="-0.173033493042" />
                  <Point X="3.373159179688" Y="-0.183989089966" />
                  <Point X="3.380005859375" Y="-0.205490509033" />
                  <Point X="3.384069580078" Y="-0.216036346436" />
                  <Point X="3.393843505859" Y="-0.237500015259" />
                  <Point X="3.399130859375" Y="-0.247488815308" />
                  <Point X="3.410854736328" Y="-0.266768463135" />
                  <Point X="3.417291259766" Y="-0.276059173584" />
                  <Point X="3.455561279297" Y="-0.324824157715" />
                  <Point X="3.472797119141" Y="-0.346786895752" />
                  <Point X="3.478718017578" Y="-0.353633209229" />
                  <Point X="3.501139404297" Y="-0.375805206299" />
                  <Point X="3.530176025391" Y="-0.398724639893" />
                  <Point X="3.541494628906" Y="-0.406404296875" />
                  <Point X="3.605278076172" Y="-0.443272125244" />
                  <Point X="3.634004638672" Y="-0.459876739502" />
                  <Point X="3.639496582031" Y="-0.462815460205" />
                  <Point X="3.659158447266" Y="-0.472016845703" />
                  <Point X="3.683028076172" Y="-0.481027862549" />
                  <Point X="3.691991943359" Y="-0.48391293335" />
                  <Point X="4.095691894531" Y="-0.592084106445" />
                  <Point X="4.784876464844" Y="-0.776750610352" />
                  <Point X="4.777844238281" Y="-0.823395874023" />
                  <Point X="4.76161328125" Y="-0.931052307129" />
                  <Point X="4.727801757812" Y="-1.079219726563" />
                  <Point X="4.476310058594" Y="-1.046110229492" />
                  <Point X="3.436781982422" Y="-0.909253845215" />
                  <Point X="3.428624511719" Y="-0.908535827637" />
                  <Point X="3.400098388672" Y="-0.908042541504" />
                  <Point X="3.366721435547" Y="-0.910840820312" />
                  <Point X="3.354481201172" Y="-0.912676330566" />
                  <Point X="3.229296875" Y="-0.939885620117" />
                  <Point X="3.172916992188" Y="-0.952140014648" />
                  <Point X="3.157874023438" Y="-0.956742431641" />
                  <Point X="3.128753662109" Y="-0.968366943359" />
                  <Point X="3.114676269531" Y="-0.975388916016" />
                  <Point X="3.086849609375" Y="-0.99228125" />
                  <Point X="3.074124023438" Y="-1.001530395508" />
                  <Point X="3.050374023438" Y="-1.022001098633" />
                  <Point X="3.039349609375" Y="-1.033222900391" />
                  <Point X="2.96368359375" Y="-1.124225585938" />
                  <Point X="2.92960546875" Y="-1.165211181641" />
                  <Point X="2.921326171875" Y="-1.176847290039" />
                  <Point X="2.90660546875" Y="-1.201229492188" />
                  <Point X="2.9001640625" Y="-1.213975708008" />
                  <Point X="2.888820800781" Y="-1.241360717773" />
                  <Point X="2.884362792969" Y="-1.254928100586" />
                  <Point X="2.877531005859" Y="-1.282577880859" />
                  <Point X="2.875157226562" Y="-1.296660400391" />
                  <Point X="2.8643125" Y="-1.414512817383" />
                  <Point X="2.859428222656" Y="-1.467590698242" />
                  <Point X="2.859288574219" Y="-1.483320922852" />
                  <Point X="2.861607177734" Y="-1.514589355469" />
                  <Point X="2.864065429688" Y="-1.530127441406" />
                  <Point X="2.871796875" Y="-1.561748291016" />
                  <Point X="2.876785888672" Y="-1.576668579102" />
                  <Point X="2.889157470703" Y="-1.605479980469" />
                  <Point X="2.896540039062" Y="-1.619371337891" />
                  <Point X="2.965818847656" Y="-1.727130004883" />
                  <Point X="2.997020263672" Y="-1.775661865234" />
                  <Point X="3.001741943359" Y="-1.782353271484" />
                  <Point X="3.019793701172" Y="-1.804450195312" />
                  <Point X="3.043489257812" Y="-1.828120361328" />
                  <Point X="3.052796142578" Y="-1.836277709961" />
                  <Point X="3.427432373047" Y="-2.123746582031" />
                  <Point X="4.087170166016" Y="-2.629981445312" />
                  <Point X="4.045484130859" Y="-2.697436523438" />
                  <Point X="4.001274414063" Y="-2.760251953125" />
                  <Point X="3.774294677734" Y="-2.629205078125" />
                  <Point X="2.848454589844" Y="-2.094670654297" />
                  <Point X="2.841191894531" Y="-2.090885253906" />
                  <Point X="2.815026123047" Y="-2.079513183594" />
                  <Point X="2.783119140625" Y="-2.069325927734" />
                  <Point X="2.771108398438" Y="-2.066337646484" />
                  <Point X="2.622119140625" Y="-2.039430175781" />
                  <Point X="2.555018066406" Y="-2.027312011719" />
                  <Point X="2.539359130859" Y="-2.025807495117" />
                  <Point X="2.508006835938" Y="-2.025403198242" />
                  <Point X="2.492313476562" Y="-2.026503662109" />
                  <Point X="2.460140869141" Y="-2.031461425781" />
                  <Point X="2.444844726562" Y="-2.035136230469" />
                  <Point X="2.415068847656" Y="-2.044959838867" />
                  <Point X="2.400589111328" Y="-2.051108642578" />
                  <Point X="2.276815673828" Y="-2.116249755859" />
                  <Point X="2.221071044922" Y="-2.145587646484" />
                  <Point X="2.208968505859" Y="-2.153170410156" />
                  <Point X="2.186037841797" Y="-2.170063476562" />
                  <Point X="2.175209716797" Y="-2.179373779297" />
                  <Point X="2.154249755859" Y="-2.200333740234" />
                  <Point X="2.144939453125" Y="-2.211161865234" />
                  <Point X="2.128046386719" Y="-2.234092529297" />
                  <Point X="2.120463623047" Y="-2.246195068359" />
                  <Point X="2.055322509766" Y="-2.369968505859" />
                  <Point X="2.025984741211" Y="-2.425713134766" />
                  <Point X="2.0198359375" Y="-2.440192871094" />
                  <Point X="2.010012207031" Y="-2.46996875" />
                  <Point X="2.006337280273" Y="-2.485264892578" />
                  <Point X="2.001379516602" Y="-2.5174375" />
                  <Point X="2.000279174805" Y="-2.533130859375" />
                  <Point X="2.00068347168" Y="-2.564483154297" />
                  <Point X="2.002187866211" Y="-2.580142089844" />
                  <Point X="2.029095092773" Y="-2.729131347656" />
                  <Point X="2.041213500977" Y="-2.796232421875" />
                  <Point X="2.043015014648" Y="-2.804221435547" />
                  <Point X="2.051236083984" Y="-2.83154296875" />
                  <Point X="2.064069580078" Y="-2.862479736328" />
                  <Point X="2.069546630859" Y="-2.873578613281" />
                  <Point X="2.310288085938" Y="-3.290554931641" />
                  <Point X="2.735892822266" Y="-4.027724365234" />
                  <Point X="2.723754150391" Y="-4.036083740234" />
                  <Point X="2.540416015625" Y="-3.797153320312" />
                  <Point X="1.833914550781" Y="-2.876422851562" />
                  <Point X="1.828654418945" Y="-2.870147216797" />
                  <Point X="1.808830932617" Y="-2.849625732422" />
                  <Point X="1.783251586914" Y="-2.828004150391" />
                  <Point X="1.773299072266" Y="-2.820647216797" />
                  <Point X="1.62635546875" Y="-2.726176025391" />
                  <Point X="1.56017565918" Y="-2.683628417969" />
                  <Point X="1.546283935547" Y="-2.676245849609" />
                  <Point X="1.517473022461" Y="-2.663874511719" />
                  <Point X="1.502553588867" Y="-2.658885742188" />
                  <Point X="1.470932617188" Y="-2.651154052734" />
                  <Point X="1.45539453125" Y="-2.648695800781" />
                  <Point X="1.424125488281" Y="-2.646376953125" />
                  <Point X="1.40839440918" Y="-2.646516357422" />
                  <Point X="1.247686645508" Y="-2.661304931641" />
                  <Point X="1.175307739258" Y="-2.667965332031" />
                  <Point X="1.161225097656" Y="-2.670339111328" />
                  <Point X="1.133575439453" Y="-2.677170898438" />
                  <Point X="1.120008422852" Y="-2.68162890625" />
                  <Point X="1.092623168945" Y="-2.692972167969" />
                  <Point X="1.079877563477" Y="-2.699413330078" />
                  <Point X="1.055495361328" Y="-2.714133789062" />
                  <Point X="1.043858764648" Y="-2.722413085938" />
                  <Point X="0.919764282227" Y="-2.825593505859" />
                  <Point X="0.863875244141" Y="-2.872063476562" />
                  <Point X="0.852653320312" Y="-2.883088134766" />
                  <Point X="0.832182434082" Y="-2.906838134766" />
                  <Point X="0.822933654785" Y="-2.919563720703" />
                  <Point X="0.806041137695" Y="-2.947390380859" />
                  <Point X="0.799019287109" Y="-2.961467529297" />
                  <Point X="0.787394470215" Y="-2.990588134766" />
                  <Point X="0.782791931152" Y="-3.005631347656" />
                  <Point X="0.745688232422" Y="-3.176337402344" />
                  <Point X="0.728977661133" Y="-3.253219238281" />
                  <Point X="0.727584777832" Y="-3.261289306641" />
                  <Point X="0.724724487305" Y="-3.289677734375" />
                  <Point X="0.7247421875" Y="-3.323170410156" />
                  <Point X="0.725554992676" Y="-3.335520019531" />
                  <Point X="0.793779174805" Y="-3.853734619141" />
                  <Point X="0.833091552734" Y="-4.152341308594" />
                  <Point X="0.655064941406" Y="-3.487936523438" />
                  <Point X="0.652606262207" Y="-3.480124511719" />
                  <Point X="0.642145019531" Y="-3.453580322266" />
                  <Point X="0.62678692627" Y="-3.423815673828" />
                  <Point X="0.620407409668" Y="-3.413210205078" />
                  <Point X="0.507521057129" Y="-3.250562255859" />
                  <Point X="0.456679992676" Y="-3.177309814453" />
                  <Point X="0.446670684814" Y="-3.165172851562" />
                  <Point X="0.424786712646" Y="-3.142717529297" />
                  <Point X="0.412912322998" Y="-3.132399169922" />
                  <Point X="0.386657196045" Y="-3.113155273438" />
                  <Point X="0.373242797852" Y="-3.104937988281" />
                  <Point X="0.345241485596" Y="-3.090829589844" />
                  <Point X="0.330654602051" Y="-3.084938476562" />
                  <Point X="0.15596963501" Y="-3.03072265625" />
                  <Point X="0.077295654297" Y="-3.006305175781" />
                  <Point X="0.063376529694" Y="-3.003109375" />
                  <Point X="0.035217094421" Y="-2.998840087891" />
                  <Point X="0.020976791382" Y="-2.997766601562" />
                  <Point X="-0.008664855957" Y="-2.997766601562" />
                  <Point X="-0.022905158997" Y="-2.998840087891" />
                  <Point X="-0.051064590454" Y="-3.003109375" />
                  <Point X="-0.064983573914" Y="-3.006305175781" />
                  <Point X="-0.239668685913" Y="-3.060520751953" />
                  <Point X="-0.318342681885" Y="-3.084938476562" />
                  <Point X="-0.332929992676" Y="-3.090829833984" />
                  <Point X="-0.36093145752" Y="-3.104938476562" />
                  <Point X="-0.374345855713" Y="-3.113155761719" />
                  <Point X="-0.400600830078" Y="-3.132399658203" />
                  <Point X="-0.412475219727" Y="-3.142717773438" />
                  <Point X="-0.434358886719" Y="-3.165172851562" />
                  <Point X="-0.444368041992" Y="-3.177309814453" />
                  <Point X="-0.557254272461" Y="-3.339957519531" />
                  <Point X="-0.60809564209" Y="-3.413210205078" />
                  <Point X="-0.612470336914" Y="-3.420132324219" />
                  <Point X="-0.625976745605" Y="-3.445265136719" />
                  <Point X="-0.638777832031" Y="-3.476215820312" />
                  <Point X="-0.642752990723" Y="-3.487936523438" />
                  <Point X="-0.767142944336" Y="-3.952166015625" />
                  <Point X="-0.985425170898" Y="-4.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.690692547695" Y="0.688953795007" />
                  <Point X="4.654320613404" Y="1.104686906306" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.743865961019" Y="-1.008823858751" />
                  <Point X="4.722090316916" Y="-0.759927107721" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.597514001639" Y="0.663986691577" />
                  <Point X="4.560043619506" Y="1.092275119166" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.653809668828" Y="-1.069478487146" />
                  <Point X="4.624438220771" Y="-0.733761299647" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.504335455875" Y="0.639019584816" />
                  <Point X="4.465766625608" Y="1.079863332026" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.557335586475" Y="-1.056777438336" />
                  <Point X="4.526786124626" Y="-0.707595491573" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.411156910143" Y="0.614052477693" />
                  <Point X="4.37148963171" Y="1.067451544886" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.460861504161" Y="-1.044076389954" />
                  <Point X="4.429134028481" Y="-0.6814296835" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.317978364411" Y="0.58908537057" />
                  <Point X="4.277212637813" Y="1.055039757746" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.364387422042" Y="-1.031375343818" />
                  <Point X="4.331481932336" Y="-0.655263875426" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.224799818678" Y="0.564118263447" />
                  <Point X="4.182935643915" Y="1.042627970606" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.061911264565" Y="2.425942956485" />
                  <Point X="4.043371741458" Y="2.637850675259" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.267913339924" Y="-1.018674297682" />
                  <Point X="4.233829836191" Y="-0.629098067352" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.131621272946" Y="0.539151156324" />
                  <Point X="4.088658650017" Y="1.030216183466" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.97254758179" Y="2.357371766229" />
                  <Point X="3.935725521488" Y="2.778249841377" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.171439257805" Y="-1.005973251546" />
                  <Point X="4.136177740045" Y="-0.602932259278" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.038442727214" Y="0.514184049201" />
                  <Point X="3.994381656119" Y="1.017804396326" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.883183901373" Y="2.288800549024" />
                  <Point X="3.844947958055" Y="2.725839380998" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.074965175687" Y="-0.99327220541" />
                  <Point X="4.038525643075" Y="-0.57676644177" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.945264181481" Y="0.489216942078" />
                  <Point X="3.900104663558" Y="1.005392593914" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.793820221528" Y="2.220229325281" />
                  <Point X="3.754170394621" Y="2.673428920619" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.978491093568" Y="-0.980571159274" />
                  <Point X="3.94087354552" Y="-0.55060061758" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.852085635749" Y="0.464249834954" />
                  <Point X="3.805827672754" Y="0.992980771412" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.704456541683" Y="2.151658101538" />
                  <Point X="3.663392831188" Y="2.621018460239" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.034677784506" Y="-2.71279073375" />
                  <Point X="4.023134006528" Y="-2.58084474769" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.88201701145" Y="-0.967870113138" />
                  <Point X="3.843221447965" Y="-0.52443479339" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.758907090017" Y="0.439282727831" />
                  <Point X="3.71155068195" Y="0.980568948911" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.615092861838" Y="2.083086877795" />
                  <Point X="3.572615267755" Y="2.56860799986" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.940391946029" Y="-2.725101426883" />
                  <Point X="3.920908471793" Y="-2.502404297329" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.785542929331" Y="-0.955169067002" />
                  <Point X="3.74556935041" Y="-0.498268969201" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.665910065766" Y="0.412240820678" />
                  <Point X="3.617273691145" Y="0.968157126409" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.525729181993" Y="2.014515654052" />
                  <Point X="3.481837704322" Y="2.516197539481" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.839955871869" Y="-2.667114604497" />
                  <Point X="3.818682937058" Y="-2.423963846969" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.689068847213" Y="-0.942468020866" />
                  <Point X="3.647429482959" Y="-0.466527909593" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.574844598621" Y="0.363121114779" />
                  <Point X="3.522996700341" Y="0.955745303908" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.436365502148" Y="1.94594443031" />
                  <Point X="3.391060140888" Y="2.463787079101" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.739519797217" Y="-2.609127776479" />
                  <Point X="3.716457402323" Y="-2.345523396608" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.592594765095" Y="-0.92976697473" />
                  <Point X="3.547089389889" Y="-0.409638156074" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.48522863507" Y="0.297433506995" />
                  <Point X="3.427774067671" Y="0.954142217398" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.347001822303" Y="1.877373206567" />
                  <Point X="3.300282577455" Y="2.411376618722" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.639083721635" Y="-2.551140937828" />
                  <Point X="3.614231867588" Y="-2.267082946247" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.496120682976" Y="-0.917065928594" />
                  <Point X="3.442894089949" Y="-0.308683186399" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.399635981817" Y="0.185759252068" />
                  <Point X="3.331213880661" Y="0.967827446949" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.257638142458" Y="1.808801982824" />
                  <Point X="3.209505014022" Y="2.358966158342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.538647646053" Y="-2.493154099177" />
                  <Point X="3.512006332853" Y="-2.188642495886" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.39996930118" Y="-0.908053364025" />
                  <Point X="3.232322624706" Y="1.008156916463" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.168294843808" Y="1.739997800956" />
                  <Point X="3.118727450589" Y="2.306555697963" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.438211570471" Y="-2.435167260526" />
                  <Point X="3.409780797057" Y="-2.110202033407" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.305934046179" Y="-0.923228239399" />
                  <Point X="3.13071531699" Y="1.079530999658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.081818539394" Y="1.638423725021" />
                  <Point X="3.027949885318" Y="2.254145258591" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.337775494888" Y="-2.377180421876" />
                  <Point X="3.307555256182" Y="-2.031761512864" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.212350741489" Y="-0.943568930466" />
                  <Point X="3.016444696646" Y="1.295647408518" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.00386568926" Y="1.439426120859" />
                  <Point X="2.937172319551" Y="2.201734824882" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.792288527046" Y="3.857764151034" />
                  <Point X="2.786478163416" Y="3.924176911221" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.237339419306" Y="-2.319193583225" />
                  <Point X="3.205329715307" Y="-1.953320992322" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.119558674826" Y="-0.972953513558" />
                  <Point X="2.846394753784" Y="2.149324391172" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.709474809179" Y="3.714326519303" />
                  <Point X="2.684788437003" Y="3.996493044444" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.136903343724" Y="-2.261206744574" />
                  <Point X="3.103104174432" Y="-1.874880471779" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.0304093837" Y="-1.043975211572" />
                  <Point X="2.755617188018" Y="2.096913957462" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.626661089224" Y="3.570888911436" />
                  <Point X="2.583647782286" Y="4.062533259449" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.036467268141" Y="-2.203219905923" />
                  <Point X="2.999349590505" Y="-1.778962909176" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.944125448081" Y="-1.147748072902" />
                  <Point X="2.664839622251" Y="2.044503523753" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.543847366073" Y="3.427451340105" />
                  <Point X="2.483163072747" Y="4.121075986772" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.936031192559" Y="-2.145233067273" />
                  <Point X="2.888719727572" Y="-1.604460547949" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.868305276033" Y="-1.371122299121" />
                  <Point X="2.573146451362" Y="2.002558504489" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.461033642921" Y="3.284013768774" />
                  <Point X="2.382678388057" Y="4.179618430072" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.83570485111" Y="-2.088500495496" />
                  <Point X="2.479818992928" Y="1.979293477331" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.37821991977" Y="3.140576197443" />
                  <Point X="2.282193703366" Y="4.238160873371" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.737877911559" Y="-2.060336218158" />
                  <Point X="2.384508964438" Y="1.978689329609" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.295406196618" Y="2.997138626112" />
                  <Point X="2.182429723537" Y="4.288465622413" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.640984059664" Y="-2.04283718152" />
                  <Point X="2.28751220164" Y="1.997364643249" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.212592473467" Y="2.853701054781" />
                  <Point X="2.082998540042" Y="4.334966491951" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.719525515425" Y="-4.030572887136" />
                  <Point X="2.716308610618" Y="-3.993803496936" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.544171724994" Y="-2.026269891029" />
                  <Point X="2.187182103637" Y="2.054140152629" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.129778750315" Y="2.71026348345" />
                  <Point X="1.983567366186" Y="4.381467251304" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.611890371788" Y="-3.890300324091" />
                  <Point X="2.603914024353" Y="-3.799130255726" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.44948697106" Y="-2.034020959625" />
                  <Point X="2.082252646496" Y="2.163486577509" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.047666517325" Y="2.558807842882" />
                  <Point X="1.884136192331" Y="4.427968010656" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.504255219465" Y="-3.75002766176" />
                  <Point X="2.491519438089" Y="-3.604457014516" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.357598551809" Y="-2.073734279906" />
                  <Point X="1.785402543463" Y="4.466496022927" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.396620049973" Y="-3.609754803186" />
                  <Point X="2.379124851824" Y="-3.409783773305" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.266433338707" Y="-2.1217138843" />
                  <Point X="1.686914360045" Y="4.502218352254" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.28898488048" Y="-3.469481944612" />
                  <Point X="2.266730271962" Y="-3.215110605273" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.176051702872" Y="-2.17864981783" />
                  <Point X="1.588426180577" Y="4.537940636441" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.181349710988" Y="-3.329209086037" />
                  <Point X="2.154335702218" Y="-3.020437552888" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.091425441749" Y="-2.30136998535" />
                  <Point X="1.489938001109" Y="4.573662920627" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.073714541496" Y="-3.188936227463" />
                  <Point X="2.037613389924" Y="-2.776298176811" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.010645184999" Y="-2.468050183997" />
                  <Point X="1.392123467074" Y="4.601685402275" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.966079372004" Y="-3.048663368889" />
                  <Point X="1.294702815525" Y="4.625205786506" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.858444202511" Y="-2.908390510314" />
                  <Point X="1.197282164411" Y="4.648726165774" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.754338284256" Y="-2.808457177963" />
                  <Point X="1.099861513502" Y="4.672246542696" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.653291824758" Y="-2.743493619239" />
                  <Point X="1.002440862593" Y="4.695766919618" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.552326468236" Y="-2.67945707176" />
                  <Point X="0.905020211683" Y="4.71928729654" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.454264992762" Y="-2.648612036539" />
                  <Point X="0.807844324568" Y="4.740010010497" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.359115496701" Y="-2.651051078321" />
                  <Point X="0.711599623271" Y="4.750089221849" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.264514231841" Y="-2.659756431408" />
                  <Point X="0.615354915802" Y="4.760168503743" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.169948565539" Y="-2.668868677865" />
                  <Point X="0.519110185564" Y="4.770248045892" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.077389416256" Y="-2.700915518801" />
                  <Point X="0.422865455326" Y="4.78032758804" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.987972695894" Y="-2.768880486658" />
                  <Point X="0.339670427319" Y="4.641248351155" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.899076472861" Y="-2.842794766215" />
                  <Point X="0.267780506255" Y="4.372951150618" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.812005016067" Y="-2.937566219322" />
                  <Point X="0.18993126169" Y="4.17276932938" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.817990500557" Y="-4.095983378434" />
                  <Point X="0.802773742217" Y="-3.922055034731" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.739872738412" Y="-3.203093271346" />
                  <Point X="0.10045636446" Y="4.105469326166" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.676394906128" Y="-3.567541086613" />
                  <Point X="0.006874403058" Y="4.08511328125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.559902949328" Y="-3.326034685882" />
                  <Point X="-0.090353167867" Y="4.106426743853" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.45091694645" Y="-3.170321731046" />
                  <Point X="-0.193504107133" Y="4.195444616401" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.34875424659" Y="-3.0925994866" />
                  <Point X="-0.328117626916" Y="4.644081429849" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.250545896454" Y="-3.060075666318" />
                  <Point X="-0.434882297757" Y="4.774404443311" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.152521323468" Y="-3.02965242847" />
                  <Point X="-0.529278635275" Y="4.763356759992" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.054721418941" Y="-3.001797162862" />
                  <Point X="-0.623674972793" Y="4.752309076673" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.040664610058" Y="-3.001532620785" />
                  <Point X="-0.718071310311" Y="4.741261393353" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.133742918452" Y="-3.027645445932" />
                  <Point X="-0.812467647829" Y="4.730213710034" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.22658485349" Y="-3.056460030892" />
                  <Point X="-0.906863985347" Y="4.719166026715" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.319418194809" Y="-3.085372842508" />
                  <Point X="-1.000282806798" Y="4.696945283615" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.409955595569" Y="-3.140528374809" />
                  <Point X="-1.093362552537" Y="4.670848887406" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.495637739187" Y="-3.251179750177" />
                  <Point X="-1.186442299532" Y="4.644752505548" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.58032536241" Y="-3.37319854568" />
                  <Point X="-1.279522048574" Y="4.618656147082" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.66001403562" Y="-3.5523556013" />
                  <Point X="-1.372601797615" Y="4.592559788616" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.731903985729" Y="-3.820652469844" />
                  <Point X="-1.465681546657" Y="4.56646343015" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.803793930287" Y="-4.088949401849" />
                  <Point X="-1.527964515924" Y="4.188358268107" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.875683869506" Y="-4.35724639487" />
                  <Point X="-1.615730023358" Y="4.101519850119" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.947573808725" Y="-4.625543387891" />
                  <Point X="-1.707515020435" Y="4.060624408976" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.031357768179" Y="-4.757891107542" />
                  <Point X="-1.800346786674" Y="4.031693594106" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.12876357934" Y="-4.734540349714" />
                  <Point X="-1.133915930297" Y="-4.675648708793" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.170018783075" Y="-4.262991213264" />
                  <Point X="-1.89627678087" Y="4.038175686839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.285001411786" Y="-4.038736511525" />
                  <Point X="-1.995517924178" Y="4.082504387088" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.388289095401" Y="-3.948155643913" />
                  <Point X="-2.096635490824" Y="4.148280704242" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.491576806584" Y="-3.8575744612" />
                  <Point X="-2.202036097552" Y="4.263012393547" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.591501545973" Y="-3.805432221975" />
                  <Point X="-2.292978564289" Y="4.21248678656" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.687968130437" Y="-3.792816874427" />
                  <Point X="-2.383921021352" Y="4.161961068991" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.783881006236" Y="-3.786530445875" />
                  <Point X="-2.474863478415" Y="4.111435351421" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.879793883697" Y="-3.780243998329" />
                  <Point X="-2.565805935477" Y="4.060909633852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.975400404216" Y="-3.777459226651" />
                  <Point X="-2.656603451867" Y="4.008727236804" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.068593823685" Y="-3.802256326177" />
                  <Point X="-2.745971770776" Y="3.940209037809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.159051933537" Y="-3.858318157706" />
                  <Point X="-2.756011317652" Y="2.964958825362" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.777181311904" Y="3.206932966911" />
                  <Point X="-2.835340096172" Y="3.871690912957" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.249147976802" Y="-3.918518429254" />
                  <Point X="-2.352044735829" Y="-2.742403091789" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.378916755133" Y="-2.435254505663" />
                  <Point X="-2.839759107048" Y="2.832197680061" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.890007769987" Y="3.406542525589" />
                  <Point X="-2.924708448946" Y="3.803173101033" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.339243995111" Y="-3.978718986043" />
                  <Point X="-2.434858462833" Y="-2.885840619078" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.4804177565" Y="-2.365095509592" />
                  <Point X="-2.92846759554" Y="2.756137584885" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.002402340443" Y="3.601215586107" />
                  <Point X="-3.014076801719" Y="3.734655289109" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.427195990982" Y="-4.063425831444" />
                  <Point X="-2.517672189838" Y="-3.029278146368" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.577195444418" Y="-2.348924233282" />
                  <Point X="-3.021149971805" Y="2.725499234798" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.514505064469" Y="-4.155481313317" />
                  <Point X="-2.600485916842" Y="-3.172715673658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.670582417853" Y="-2.371509000867" />
                  <Point X="-3.116714315433" Y="2.727801922408" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.615329697982" Y="-4.093053237206" />
                  <Point X="-2.683299643847" Y="-3.316153200947" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.761516567752" Y="-2.422129669749" />
                  <Point X="-3.213571283582" Y="2.744879375897" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.716154333738" Y="-4.030625135439" />
                  <Point X="-2.766113370852" Y="-3.459590728237" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.852294129227" Y="-2.474540152513" />
                  <Point X="-2.948913957113" Y="-1.370170466294" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.955352148058" Y="-1.296581607062" />
                  <Point X="-3.313462503519" Y="2.796638486024" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.817960606352" Y="-3.956976873058" />
                  <Point X="-2.848927097856" Y="-3.603028255526" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.943071690702" Y="-2.526950635278" />
                  <Point X="-3.028159979793" Y="-1.554387040609" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.063747026249" Y="-1.147625238325" />
                  <Point X="-3.413898573537" Y="2.854625261072" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.920211451522" Y="-3.878247123097" />
                  <Point X="-2.931740819622" Y="-3.746465842701" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.033849252177" Y="-2.579361118042" />
                  <Point X="-3.11718117998" Y="-1.626872824767" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.162731470559" Y="-1.106230621039" />
                  <Point X="-3.514334643555" Y="2.912612036119" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.124626816574" Y="-2.631771567406" />
                  <Point X="-3.206544863358" Y="-1.695444008122" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.258459513833" Y="-1.102056837917" />
                  <Point X="-3.338814649784" Y="-0.1835934312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.368982603622" Y="0.161227859034" />
                  <Point X="-3.473195034402" Y="1.352381393451" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.506388080646" Y="1.731779648107" />
                  <Point X="-3.614770713572" Y="2.970598811167" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.215404385112" Y="-2.684181969434" />
                  <Point X="-3.295908546736" Y="-1.764015191478" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.352736506097" Y="-1.114468643729" />
                  <Point X="-3.426395694483" Y="-0.272540267897" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.470792005088" Y="0.234911884369" />
                  <Point X="-3.560411959656" Y="1.259272652452" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.6094656828" Y="1.819959273628" />
                  <Point X="-3.715206778917" Y="3.028585532806" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.30618195365" Y="-2.736592371463" />
                  <Point X="-3.385272230115" Y="-1.832586374833" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.447013498361" Y="-1.12688044954" />
                  <Point X="-3.51806218405" Y="-0.314790256069" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.568860897333" Y="0.265841693666" />
                  <Point X="-3.652481130057" Y="1.221625327273" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.711691212992" Y="1.898399672069" />
                  <Point X="-3.802560483934" Y="2.937040191643" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.396959522189" Y="-2.789002773491" />
                  <Point X="-3.474635913493" Y="-1.901157558189" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.541290490625" Y="-1.139292255352" />
                  <Point X="-3.611240729664" Y="-0.339757364554" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.666512993639" Y="0.292007503583" />
                  <Point X="-3.746335934887" Y="1.204387897007" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.813916744291" Y="1.976840083155" />
                  <Point X="-3.888283451683" Y="2.826855438232" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.487737090727" Y="-2.84141317552" />
                  <Point X="-3.563999596872" Y="-1.969728741545" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.63556748289" Y="-1.151704061163" />
                  <Point X="-3.704419275277" Y="-0.364724473039" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.764165089946" Y="0.3181733135" />
                  <Point X="-3.842548063804" Y="1.214094804348" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.916142295708" Y="2.055280724184" />
                  <Point X="-3.974006419433" Y="2.716670684822" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.578514659265" Y="-2.893823577548" />
                  <Point X="-3.65336328025" Y="-2.0382999249" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.729844475154" Y="-1.164115866975" />
                  <Point X="-3.79759782089" Y="-0.389691581524" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.861817186252" Y="0.344339123417" />
                  <Point X="-3.939022147947" Y="1.226795873619" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.018367847124" Y="2.133721365213" />
                  <Point X="-4.057859523196" Y="2.585113288246" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.669292227804" Y="-2.946233979576" />
                  <Point X="-3.742726963629" Y="-2.106871108256" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.824121468196" Y="-1.176527663896" />
                  <Point X="-3.890776366503" Y="-0.41465869001" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.959469282559" Y="0.370504933335" />
                  <Point X="-4.035496232089" Y="1.239496942891" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.12059339854" Y="2.212162006241" />
                  <Point X="-4.140791746937" Y="2.443030184841" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.760069796342" Y="-2.998644381605" />
                  <Point X="-3.832090647007" Y="-2.175442291611" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.918398461446" Y="-1.188939458432" />
                  <Point X="-3.983954912116" Y="-0.439625798495" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.057121375953" Y="0.396670709973" />
                  <Point X="-4.131970316232" Y="1.252198012162" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.222818949957" Y="2.29060264727" />
                  <Point X="-4.223723981923" Y="2.300947209985" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.861644633881" Y="-2.927641434229" />
                  <Point X="-3.921454330386" Y="-2.244013474967" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.012675454697" Y="-1.201351252969" />
                  <Point X="-4.077133457729" Y="-0.46459290698" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.154773469032" Y="0.422836482998" />
                  <Point X="-4.228444400374" Y="1.264899081433" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.969392306924" Y="-2.786082654177" />
                  <Point X="-4.010818010016" Y="-2.312584701163" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.106952447948" Y="-1.213763047506" />
                  <Point X="-4.170312003342" Y="-0.489560015465" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.252425562111" Y="0.449002256023" />
                  <Point X="-4.324918484517" Y="1.277600150705" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.079862003117" Y="-2.613411007161" />
                  <Point X="-4.100181688213" Y="-2.381155943746" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.201229441198" Y="-1.226174842042" />
                  <Point X="-4.263490548956" Y="-0.51452712395" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.350077655189" Y="0.475168029048" />
                  <Point X="-4.421392568659" Y="1.290301219976" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.295506434449" Y="-1.238586636579" />
                  <Point X="-4.356669094569" Y="-0.539494232436" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.447729748268" Y="0.501333802074" />
                  <Point X="-4.517866652596" Y="1.303002286889" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.3897834277" Y="-1.250998431115" />
                  <Point X="-4.449847640182" Y="-0.564461340921" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.545381841347" Y="0.527499575099" />
                  <Point X="-4.614340733664" Y="1.315703321021" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.48406042095" Y="-1.263410225652" />
                  <Point X="-4.543026185287" Y="-0.589428455212" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.643033934426" Y="0.553665348124" />
                  <Point X="-4.691293865426" Y="1.105278883588" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.578337414201" Y="-1.275822020189" />
                  <Point X="-4.636204726861" Y="-0.614395609869" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.740686027504" Y="0.579831121149" />
                  <Point X="-4.757976387957" Y="0.777460845457" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.678911033946" Y="-1.216263044567" />
                  <Point X="-4.729383268434" Y="-0.639362764526" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="0.001626220703" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.74773034668" Y="-4.567872558594" />
                  <Point X="0.471539031982" Y="-3.537112304688" />
                  <Point X="0.464318664551" Y="-3.521544189453" />
                  <Point X="0.351432250977" Y="-3.358896240234" />
                  <Point X="0.300591033936" Y="-3.285643798828" />
                  <Point X="0.274335754395" Y="-3.266399902344" />
                  <Point X="0.099650718689" Y="-3.212184082031" />
                  <Point X="0.02097677803" Y="-3.187766601562" />
                  <Point X="-0.008664908409" Y="-3.187766601562" />
                  <Point X="-0.183349945068" Y="-3.241982177734" />
                  <Point X="-0.262024047852" Y="-3.266399902344" />
                  <Point X="-0.288279144287" Y="-3.285643798828" />
                  <Point X="-0.401165588379" Y="-3.448291503906" />
                  <Point X="-0.45200680542" Y="-3.521544189453" />
                  <Point X="-0.459227142334" Y="-3.537112304688" />
                  <Point X="-0.58361706543" Y="-4.001341796875" />
                  <Point X="-0.84774395752" Y="-4.987077148438" />
                  <Point X="-1.008644165039" Y="-4.955845703125" />
                  <Point X="-1.100246459961" Y="-4.938065429688" />
                  <Point X="-1.302663208008" Y="-4.885985351562" />
                  <Point X="-1.351589599609" Y="-4.873396972656" />
                  <Point X="-1.342397216797" Y="-4.803573730469" />
                  <Point X="-1.309150146484" Y="-4.551038574219" />
                  <Point X="-1.309683227539" Y="-4.5347578125" />
                  <Point X="-1.351415527344" Y="-4.324956054688" />
                  <Point X="-1.370210693359" Y="-4.230466308594" />
                  <Point X="-1.386282592773" Y="-4.20262890625" />
                  <Point X="-1.547110473633" Y="-4.061586181641" />
                  <Point X="-1.619543457031" Y="-3.998063964844" />
                  <Point X="-1.649240722656" Y="-3.985762939453" />
                  <Point X="-1.86269519043" Y="-3.971772460938" />
                  <Point X="-1.958829833984" Y="-3.965471435547" />
                  <Point X="-1.989878540039" Y="-3.973791015625" />
                  <Point X="-2.167740234375" Y="-4.092634521484" />
                  <Point X="-2.247844970703" Y="-4.146159179688" />
                  <Point X="-2.259734130859" Y="-4.157294433594" />
                  <Point X="-2.329569335938" Y="-4.248305175781" />
                  <Point X="-2.457095214844" Y="-4.414500488281" />
                  <Point X="-2.721567138672" Y="-4.25074609375" />
                  <Point X="-2.855831054688" Y="-4.16761328125" />
                  <Point X="-3.136122070312" Y="-3.951798339844" />
                  <Point X="-3.228581054688" Y="-3.880608154297" />
                  <Point X="-3.019304199219" Y="-3.518129882812" />
                  <Point X="-2.506033691406" Y="-2.629119628906" />
                  <Point X="-2.499762695312" Y="-2.597593261719" />
                  <Point X="-2.513979736328" Y="-2.568764404297" />
                  <Point X="-2.531328369141" Y="-2.551415771484" />
                  <Point X="-2.560157226562" Y="-2.537198730469" />
                  <Point X="-2.59168359375" Y="-2.543469726562" />
                  <Point X="-2.992072509766" Y="-2.774634521484" />
                  <Point X="-3.842959472656" Y="-3.265894042969" />
                  <Point X="-4.055625244141" Y="-2.986494873047" />
                  <Point X="-4.161703613281" Y="-2.847129394531" />
                  <Point X="-4.3626484375" Y="-2.510175537109" />
                  <Point X="-4.431020019531" Y="-2.395527099609" />
                  <Point X="-4.061843505859" Y="-2.112248046875" />
                  <Point X="-3.163786865234" Y="-1.423144897461" />
                  <Point X="-3.145822021484" Y="-1.396014038086" />
                  <Point X="-3.140510009766" Y="-1.375504150391" />
                  <Point X="-3.138117431641" Y="-1.366266723633" />
                  <Point X="-3.140326171875" Y="-1.334595947266" />
                  <Point X="-3.161158935547" Y="-1.310639282227" />
                  <Point X="-3.179418212891" Y="-1.299892700195" />
                  <Point X="-3.187641601562" Y="-1.295052734375" />
                  <Point X="-3.219529296875" Y="-1.288571044922" />
                  <Point X="-3.724982421875" Y="-1.355115234375" />
                  <Point X="-4.803283203125" Y="-1.497076293945" />
                  <Point X="-4.885904785156" Y="-1.173616455078" />
                  <Point X="-4.927393066406" Y="-1.011191589355" />
                  <Point X="-4.980558105469" Y="-0.639467041016" />
                  <Point X="-4.998396484375" Y="-0.514742126465" />
                  <Point X="-4.580485839844" Y="-0.402763275146" />
                  <Point X="-3.557463867188" Y="-0.128645431519" />
                  <Point X="-3.541895751953" Y="-0.121425079346" />
                  <Point X="-3.522760742188" Y="-0.108144264221" />
                  <Point X="-3.514142822266" Y="-0.102162895203" />
                  <Point X="-3.494898681641" Y="-0.075907318115" />
                  <Point X="-3.488520263672" Y="-0.05535610199" />
                  <Point X="-3.485647705078" Y="-0.04610043335" />
                  <Point X="-3.485647705078" Y="-0.016459659576" />
                  <Point X="-3.492026123047" Y="0.004091556549" />
                  <Point X="-3.494898681641" Y="0.013347225189" />
                  <Point X="-3.514142822266" Y="0.039602802277" />
                  <Point X="-3.533277832031" Y="0.052883621216" />
                  <Point X="-3.557463867188" Y="0.066085334778" />
                  <Point X="-4.018208251953" Y="0.189541412354" />
                  <Point X="-4.998186523438" Y="0.452125793457" />
                  <Point X="-4.9443828125" Y="0.815725585938" />
                  <Point X="-4.91764453125" Y="0.99642175293" />
                  <Point X="-4.810626464844" Y="1.391352294922" />
                  <Point X="-4.773516601562" Y="1.528298706055" />
                  <Point X="-4.486593261719" Y="1.490524536133" />
                  <Point X="-3.753266357422" Y="1.393980102539" />
                  <Point X="-3.731704833984" Y="1.395866577148" />
                  <Point X="-3.689353027344" Y="1.409220092773" />
                  <Point X="-3.670278808594" Y="1.415234130859" />
                  <Point X="-3.651534423828" Y="1.426056152344" />
                  <Point X="-3.639119873047" Y="1.443785888672" />
                  <Point X="-3.622125976562" Y="1.484812744141" />
                  <Point X="-3.614472412109" Y="1.503290161133" />
                  <Point X="-3.610714111328" Y="1.524605224609" />
                  <Point X="-3.616315917969" Y="1.54551159668" />
                  <Point X="-3.636820800781" Y="1.584901123047" />
                  <Point X="-3.646055664062" Y="1.602641235352" />
                  <Point X="-3.659968261719" Y="1.619221679688" />
                  <Point X="-3.924252441406" Y="1.822014038086" />
                  <Point X="-4.47610546875" Y="2.245466308594" />
                  <Point X="-4.263908691406" Y="2.609010498047" />
                  <Point X="-4.160014648438" Y="2.787006591797" />
                  <Point X="-3.876523925781" Y="3.151393798828" />
                  <Point X="-3.774671386719" Y="3.282310791016" />
                  <Point X="-3.609546142578" Y="3.186975585938" />
                  <Point X="-3.159156982422" Y="2.926943359375" />
                  <Point X="-3.138514892578" Y="2.920434814453" />
                  <Point X="-3.079530517578" Y="2.915274414062" />
                  <Point X="-3.052965576172" Y="2.912950195312" />
                  <Point X="-3.031506835938" Y="2.915775390625" />
                  <Point X="-3.013252441406" Y="2.927404541016" />
                  <Point X="-2.971385009766" Y="2.969271972656" />
                  <Point X="-2.952528808594" Y="2.988127929688" />
                  <Point X="-2.940899414062" Y="3.006382324219" />
                  <Point X="-2.93807421875" Y="3.027841308594" />
                  <Point X="-2.943234619141" Y="3.086825683594" />
                  <Point X="-2.945558837891" Y="3.113390869141" />
                  <Point X="-2.952067382812" Y="3.134032958984" />
                  <Point X="-3.0691796875" Y="3.336877197266" />
                  <Point X="-3.307278564453" Y="3.749276611328" />
                  <Point X="-2.933825683594" Y="4.035599121094" />
                  <Point X="-2.752876220703" Y="4.174331542969" />
                  <Point X="-2.306387207031" Y="4.422391113281" />
                  <Point X="-2.141548583984" Y="4.513972167969" />
                  <Point X="-2.105704589844" Y="4.467259277344" />
                  <Point X="-1.967826904297" Y="4.287573242188" />
                  <Point X="-1.951247192383" Y="4.273661132813" />
                  <Point X="-1.885597900391" Y="4.239486328125" />
                  <Point X="-1.85603125" Y="4.224094238281" />
                  <Point X="-1.835124633789" Y="4.2184921875" />
                  <Point X="-1.813809204102" Y="4.22225" />
                  <Point X="-1.745431152344" Y="4.250573730469" />
                  <Point X="-1.714635253906" Y="4.263329589844" />
                  <Point X="-1.696905517578" Y="4.275744140625" />
                  <Point X="-1.686083496094" Y="4.29448828125" />
                  <Point X="-1.663827636719" Y="4.365074707031" />
                  <Point X="-1.653804077148" Y="4.396864746094" />
                  <Point X="-1.651917602539" Y="4.418426757812" />
                  <Point X="-1.665232055664" Y="4.519559570312" />
                  <Point X="-1.689137573242" Y="4.701141113281" />
                  <Point X="-1.202344970703" Y="4.837620605469" />
                  <Point X="-0.968094177246" Y="4.903296386719" />
                  <Point X="-0.426813598633" Y="4.966645507812" />
                  <Point X="-0.224199630737" Y="4.990358398438" />
                  <Point X="-0.175377838135" Y="4.808152832031" />
                  <Point X="-0.042140380859" Y="4.310903320312" />
                  <Point X="-0.024282115936" Y="4.284176757813" />
                  <Point X="0.006155934334" Y="4.273844238281" />
                  <Point X="0.036594036102" Y="4.284176757813" />
                  <Point X="0.054452251434" Y="4.310903320312" />
                  <Point X="0.114459243774" Y="4.534852539062" />
                  <Point X="0.236648406982" Y="4.990868652344" />
                  <Point X="0.655668945312" Y="4.946985839844" />
                  <Point X="0.860205810547" Y="4.925565429688" />
                  <Point X="1.308021118164" Y="4.81744921875" />
                  <Point X="1.508459228516" Y="4.769057128906" />
                  <Point X="1.800453735352" Y="4.663148925781" />
                  <Point X="1.931029785156" Y="4.615787597656" />
                  <Point X="2.212884765625" Y="4.483973632812" />
                  <Point X="2.338699951172" Y="4.425133789062" />
                  <Point X="2.610977783203" Y="4.266504394531" />
                  <Point X="2.732523681641" Y="4.19569140625" />
                  <Point X="2.989319091797" Y="4.013072753906" />
                  <Point X="3.068740234375" Y="3.956592773438" />
                  <Point X="2.823925537109" Y="3.532560791016" />
                  <Point X="2.229853515625" Y="2.503597900391" />
                  <Point X="2.224852050781" Y="2.491514892578" />
                  <Point X="2.210049316406" Y="2.436159423828" />
                  <Point X="2.203382324219" Y="2.411228515625" />
                  <Point X="2.202044677734" Y="2.392326171875" />
                  <Point X="2.207816650391" Y="2.344459472656" />
                  <Point X="2.210416015625" Y="2.322901367188" />
                  <Point X="2.218682128906" Y="2.300812255859" />
                  <Point X="2.248300537109" Y="2.257162353516" />
                  <Point X="2.261639892578" Y="2.237503662109" />
                  <Point X="2.274940429688" Y="2.224203369141" />
                  <Point X="2.318590087891" Y="2.194584960938" />
                  <Point X="2.338249023438" Y="2.181245849609" />
                  <Point X="2.360336914062" Y="2.172980224609" />
                  <Point X="2.408203613281" Y="2.167208251953" />
                  <Point X="2.42976171875" Y="2.164608642578" />
                  <Point X="2.4486640625" Y="2.165946289062" />
                  <Point X="2.50401953125" Y="2.180749267578" />
                  <Point X="2.528950439453" Y="2.187416015625" />
                  <Point X="2.541033935547" Y="2.192417480469" />
                  <Point X="3.004454101562" Y="2.459973144531" />
                  <Point X="3.994248046875" Y="3.031430908203" />
                  <Point X="4.130495117188" Y="2.842078613281" />
                  <Point X="4.202591796875" Y="2.741880126953" />
                  <Point X="4.345751464844" Y="2.505307617188" />
                  <Point X="4.387513183594" Y="2.436296142578" />
                  <Point X="4.070767578125" Y="2.193248779297" />
                  <Point X="3.288616210938" Y="1.593082641602" />
                  <Point X="3.27937109375" Y="1.583833129883" />
                  <Point X="3.239531494141" Y="1.53185949707" />
                  <Point X="3.221588867188" Y="1.508451782227" />
                  <Point X="3.213119384766" Y="1.4915" />
                  <Point X="3.198279052734" Y="1.438434570312" />
                  <Point X="3.191595458984" Y="1.41453527832" />
                  <Point X="3.190779541016" Y="1.390965576172" />
                  <Point X="3.202961914062" Y="1.331923583984" />
                  <Point X="3.208448486328" Y="1.305332763672" />
                  <Point X="3.215646484375" Y="1.287954956055" />
                  <Point X="3.248781005859" Y="1.237592041016" />
                  <Point X="3.263704101562" Y="1.214909667969" />
                  <Point X="3.280948242188" Y="1.198819702148" />
                  <Point X="3.328964599609" Y="1.171790527344" />
                  <Point X="3.350590332031" Y="1.15961730957" />
                  <Point X="3.368565429688" Y="1.153619628906" />
                  <Point X="3.433487060547" Y="1.145039428711" />
                  <Point X="3.462726074219" Y="1.141175048828" />
                  <Point X="3.475803710938" Y="1.14117175293" />
                  <Point X="3.916021972656" Y="1.199127563477" />
                  <Point X="4.848975097656" Y="1.32195324707" />
                  <Point X="4.908223144531" Y="1.078580566406" />
                  <Point X="4.939188476562" Y="0.951385559082" />
                  <Point X="4.984302246094" Y="0.661627502441" />
                  <Point X="4.997858398438" Y="0.574556152344" />
                  <Point X="4.637530273438" Y="0.478006378174" />
                  <Point X="3.741167724609" Y="0.237826919556" />
                  <Point X="3.729086914062" Y="0.232819412231" />
                  <Point X="3.665303466797" Y="0.195951431274" />
                  <Point X="3.636576904297" Y="0.179346878052" />
                  <Point X="3.622264892578" Y="0.166926574707" />
                  <Point X="3.583994873047" Y="0.118161437988" />
                  <Point X="3.566759033203" Y="0.096198890686" />
                  <Point X="3.556985107422" Y="0.074735275269" />
                  <Point X="3.544228271484" Y="0.008124615669" />
                  <Point X="3.538482910156" Y="-0.021875303268" />
                  <Point X="3.538482910156" Y="-0.04068478775" />
                  <Point X="3.551239746094" Y="-0.107295448303" />
                  <Point X="3.556985107422" Y="-0.13729536438" />
                  <Point X="3.566759033203" Y="-0.158758987427" />
                  <Point X="3.605029052734" Y="-0.207523971558" />
                  <Point X="3.622264892578" Y="-0.229486679077" />
                  <Point X="3.636576904297" Y="-0.241906967163" />
                  <Point X="3.700360351562" Y="-0.278774932861" />
                  <Point X="3.729086914062" Y="-0.295379486084" />
                  <Point X="3.741167724609" Y="-0.300387023926" />
                  <Point X="4.144867675781" Y="-0.408558135986" />
                  <Point X="4.998067871094" Y="-0.637172363281" />
                  <Point X="4.965721191406" Y="-0.851721313477" />
                  <Point X="4.948431640625" Y="-0.966399169922" />
                  <Point X="4.890633789062" Y="-1.219678710938" />
                  <Point X="4.874545898438" Y="-1.290178344727" />
                  <Point X="4.451510253906" Y="-1.234484741211" />
                  <Point X="3.411982177734" Y="-1.097628295898" />
                  <Point X="3.394836181641" Y="-1.098341186523" />
                  <Point X="3.269651855469" Y="-1.125550537109" />
                  <Point X="3.213271972656" Y="-1.137804931641" />
                  <Point X="3.1854453125" Y="-1.154697265625" />
                  <Point X="3.109779296875" Y="-1.245699951172" />
                  <Point X="3.075701171875" Y="-1.286685424805" />
                  <Point X="3.064357910156" Y="-1.314070556641" />
                  <Point X="3.053513183594" Y="-1.431922973633" />
                  <Point X="3.04862890625" Y="-1.485000854492" />
                  <Point X="3.056360351562" Y="-1.516621826172" />
                  <Point X="3.125639160156" Y="-1.624380493164" />
                  <Point X="3.156840576172" Y="-1.672912353516" />
                  <Point X="3.168460693359" Y="-1.685540649414" />
                  <Point X="3.543096923828" Y="-1.973009277344" />
                  <Point X="4.33907421875" Y="-2.583783935547" />
                  <Point X="4.252868652344" Y="-2.723276855469" />
                  <Point X="4.204129394531" Y="-2.802144775391" />
                  <Point X="4.084602294922" Y="-2.9719765625" />
                  <Point X="4.056688476562" Y="-3.011638427734" />
                  <Point X="3.679294677734" Y="-2.79375" />
                  <Point X="2.753454589844" Y="-2.259215576172" />
                  <Point X="2.737340820313" Y="-2.253312744141" />
                  <Point X="2.5883515625" Y="-2.226405517578" />
                  <Point X="2.521250488281" Y="-2.214287109375" />
                  <Point X="2.489077880859" Y="-2.219244873047" />
                  <Point X="2.365304443359" Y="-2.284385986328" />
                  <Point X="2.309559814453" Y="-2.313723876953" />
                  <Point X="2.288599853516" Y="-2.334683837891" />
                  <Point X="2.223458740234" Y="-2.458457275391" />
                  <Point X="2.194120849609" Y="-2.514201904297" />
                  <Point X="2.189163085938" Y="-2.546374511719" />
                  <Point X="2.2160703125" Y="-2.695363769531" />
                  <Point X="2.228188720703" Y="-2.76246484375" />
                  <Point X="2.234091552734" Y="-2.778578613281" />
                  <Point X="2.474833007813" Y="-3.195554931641" />
                  <Point X="2.986673583984" Y="-4.082088623047" />
                  <Point X="2.893130859375" Y="-4.148903808594" />
                  <Point X="2.835296875" Y="-4.190213378906" />
                  <Point X="2.701656005859" Y="-4.276716796875" />
                  <Point X="2.679775878906" Y="-4.290879394531" />
                  <Point X="2.389678955078" Y="-3.912817871094" />
                  <Point X="1.683177612305" Y="-2.992087402344" />
                  <Point X="1.670549316406" Y="-2.980467529297" />
                  <Point X="1.523605712891" Y="-2.885996337891" />
                  <Point X="1.45742590332" Y="-2.843448730469" />
                  <Point X="1.425804931641" Y="-2.835717041016" />
                  <Point X="1.265096923828" Y="-2.850505615234" />
                  <Point X="1.192718017578" Y="-2.857166015625" />
                  <Point X="1.165332885742" Y="-2.868509277344" />
                  <Point X="1.04123840332" Y="-2.971689697266" />
                  <Point X="0.985349243164" Y="-3.018159667969" />
                  <Point X="0.968456726074" Y="-3.045986328125" />
                  <Point X="0.931353149414" Y="-3.216692382812" />
                  <Point X="0.91464251709" Y="-3.29357421875" />
                  <Point X="0.91392956543" Y="-3.310720214844" />
                  <Point X="0.982153747559" Y="-3.828934814453" />
                  <Point X="1.127642211914" Y="-4.934028320312" />
                  <Point X="1.049057617188" Y="-4.951254394531" />
                  <Point X="0.994348205566" Y="-4.963246582031" />
                  <Point X="0.870887512207" Y="-4.985674804688" />
                  <Point X="0.860200317383" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#186" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.133237562358" Y="4.852373851146" Z="1.75" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.75" />
                  <Point X="-0.438781643995" Y="5.047586503057" Z="1.75" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.75" />
                  <Point X="-1.221998719308" Y="4.917044297182" Z="1.75" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.75" />
                  <Point X="-1.720960238234" Y="4.544313154938" Z="1.75" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.75" />
                  <Point X="-1.717669363323" Y="4.411390252634" Z="1.75" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.75" />
                  <Point X="-1.770720984793" Y="4.328047938753" Z="1.75" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.75" />
                  <Point X="-1.868665913927" Y="4.31511622377" Z="1.75" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.75" />
                  <Point X="-2.072192926071" Y="4.528977152677" Z="1.75" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.75" />
                  <Point X="-2.336826043959" Y="4.497378593855" Z="1.75" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.75" />
                  <Point X="-2.97040389209" Y="4.10655901722" Z="1.75" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.75" />
                  <Point X="-3.118636982412" Y="3.343157344521" Z="1.75" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.75" />
                  <Point X="-2.99920051867" Y="3.113747815525" Z="1.75" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.75" />
                  <Point X="-3.012896106299" Y="3.035907461587" Z="1.75" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.75" />
                  <Point X="-3.081328650081" Y="2.996364180503" Z="1.75" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.75" />
                  <Point X="-3.590702003777" Y="3.261556966333" Z="1.75" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.75" />
                  <Point X="-3.92214327684" Y="3.213376138189" Z="1.75" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.75" />
                  <Point X="-4.313246869247" Y="2.665495621751" Z="1.75" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.75" />
                  <Point X="-3.960846408983" Y="1.813626435788" Z="1.75" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.75" />
                  <Point X="-3.687327362472" Y="1.593093988282" Z="1.75" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.75" />
                  <Point X="-3.674476056207" Y="1.535226899985" Z="1.75" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.75" />
                  <Point X="-3.710544181661" Y="1.488186020896" Z="1.75" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.75" />
                  <Point X="-4.486223036353" Y="1.571376890567" Z="1.75" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.75" />
                  <Point X="-4.865041566031" Y="1.435709747809" Z="1.75" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.75" />
                  <Point X="-5.000000433484" Y="0.85432456427" Z="1.75" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.75" />
                  <Point X="-4.03730521149" Y="0.172525144176" Z="1.75" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.75" />
                  <Point X="-3.567942812324" Y="0.043087701108" Z="1.75" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.75" />
                  <Point X="-3.545935168938" Y="0.020551188964" Z="1.75" />
                  <Point X="-3.539556741714" Y="0" Z="1.75" />
                  <Point X="-3.542429432197" Y="-0.009255762097" Z="1.75" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.75" />
                  <Point X="-3.557425782799" Y="-0.035788282026" Z="1.75" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.75" />
                  <Point X="-4.599582577282" Y="-0.32318691258" Z="1.75" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.75" />
                  <Point X="-5.036210134611" Y="-0.615265974791" Z="1.75" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.75" />
                  <Point X="-4.940505535415" Y="-1.154710719993" Z="1.75" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.75" />
                  <Point X="-3.724611969136" Y="-1.373407455751" Z="1.75" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.75" />
                  <Point X="-3.210935420825" Y="-1.311703282798" Z="1.75" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.75" />
                  <Point X="-3.195068733814" Y="-1.331687063162" Z="1.75" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.75" />
                  <Point X="-4.098437448361" Y="-2.041300242537" Z="1.75" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.75" />
                  <Point X="-4.41174772234" Y="-2.504505184164" Z="1.75" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.75" />
                  <Point X="-4.101567165492" Y="-2.985497953095" Z="1.75" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.75" />
                  <Point X="-2.973228452614" Y="-2.786655637828" Z="1.75" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.75" />
                  <Point X="-2.567452370976" Y="-2.560878098155" Z="1.75" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.75" />
                  <Point X="-3.06876137534" Y="-3.461849929878" Z="1.75" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.75" />
                  <Point X="-3.172781984856" Y="-3.960135573622" Z="1.75" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.75" />
                  <Point X="-2.754044553842" Y="-4.261976816494" Z="1.75" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.75" />
                  <Point X="-2.296057632552" Y="-4.247463354827" Z="1.75" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.75" />
                  <Point X="-2.14611765573" Y="-4.10292790368" Z="1.75" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.75" />
                  <Point X="-1.872121256458" Y="-3.990385378377" Z="1.75" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.75" />
                  <Point X="-1.586233841322" Y="-4.067898064132" Z="1.75" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.75" />
                  <Point X="-1.406610778674" Y="-4.303430271488" Z="1.75" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.75" />
                  <Point X="-1.398125441968" Y="-4.765767450447" Z="1.75" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.75" />
                  <Point X="-1.321278084214" Y="-4.903127986571" Z="1.75" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.75" />
                  <Point X="-1.024327429229" Y="-4.973649643129" Z="1.75" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.75" />
                  <Point X="-0.541476732207" Y="-3.98300273342" Z="1.75" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.75" />
                  <Point X="-0.366245463111" Y="-3.445520434067" Z="1.75" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.75" />
                  <Point X="-0.174685102981" Y="-3.258455177922" Z="1.75" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.75" />
                  <Point X="0.07867397638" Y="-3.228656867366" Z="1.75" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.75" />
                  <Point X="0.304200396321" Y="-3.356125321701" Z="1.75" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.75" />
                  <Point X="0.693278128194" Y="-4.549533296396" Z="1.75" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.75" />
                  <Point X="0.873668553797" Y="-5.003589708679" Z="1.75" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.75" />
                  <Point X="1.053607180462" Y="-4.968814541883" Z="1.75" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.75" />
                  <Point X="1.025570054173" Y="-3.791128415499" Z="1.75" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.75" />
                  <Point X="0.9740563735" Y="-3.196031873522" Z="1.75" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.75" />
                  <Point X="1.067049215526" Y="-2.978855976917" Z="1.75" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.75" />
                  <Point X="1.263522676685" Y="-2.86901506527" Z="1.75" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.75" />
                  <Point X="1.490410316909" Y="-2.896774124765" Z="1.75" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.75" />
                  <Point X="2.343855369905" Y="-3.911975727225" Z="1.75" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.75" />
                  <Point X="2.722669235403" Y="-4.287410801727" Z="1.75" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.75" />
                  <Point X="2.916036764197" Y="-4.158310829063" Z="1.75" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.75" />
                  <Point X="2.511978309192" Y="-3.139274805866" Z="1.75" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.75" />
                  <Point X="2.259118469261" Y="-2.655197438016" Z="1.75" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.75" />
                  <Point X="2.261549099374" Y="-2.450463608503" Z="1.75" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.75" />
                  <Point X="2.382434769869" Y="-2.297352210593" Z="1.75" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.75" />
                  <Point X="2.573309365497" Y="-2.24432954023" Z="1.75" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.75" />
                  <Point X="3.648138791241" Y="-2.805771191083" Z="1.75" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.75" />
                  <Point X="4.119334657233" Y="-2.969473903586" Z="1.75" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.75" />
                  <Point X="4.289246511857" Y="-2.718281999551" Z="1.75" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.75" />
                  <Point X="3.567378917672" Y="-1.902061452437" Z="1.75" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.75" />
                  <Point X="3.161541313283" Y="-1.566061128812" Z="1.75" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.75" />
                  <Point X="3.097146717003" Y="-1.405224614199" Z="1.75" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.75" />
                  <Point X="3.142069582573" Y="-1.246386825945" Z="1.75" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.75" />
                  <Point X="3.274115540781" Y="-1.143129754119" Z="1.75" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.75" />
                  <Point X="4.438828154631" Y="-1.252776910431" Z="1.75" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.75" />
                  <Point X="4.933224760928" Y="-1.199522842931" Z="1.75" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.75" />
                  <Point X="5.009006798996" Y="-0.827895229634" Z="1.75" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.75" />
                  <Point X="4.151652777129" Y="-0.328981741653" Z="1.75" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.75" />
                  <Point X="3.71922616501" Y="-0.204206184939" Z="1.75" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.75" />
                  <Point X="3.6382067939" Y="-0.145375593918" Z="1.75" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.75" />
                  <Point X="3.594191416779" Y="-0.066610680143" Z="1.75" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.75" />
                  <Point X="3.587180033645" Y="0.029999851053" Z="1.75" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.75" />
                  <Point X="3.6171726445" Y="0.118573144657" Z="1.75" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.75" />
                  <Point X="3.684169249342" Y="0.183942659303" Z="1.75" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.75" />
                  <Point X="4.644315040178" Y="0.460990191116" Z="1.75" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.75" />
                  <Point X="5.027550918197" Y="0.700599508361" Z="1.75" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.75" />
                  <Point X="4.950648328322" Y="1.121687517752" Z="1.75" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.75" />
                  <Point X="3.903339575496" Y="1.279979921397" Z="1.75" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.75" />
                  <Point X="3.433882263975" Y="1.225888393029" Z="1.75" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.75" />
                  <Point X="3.347335112815" Y="1.246641866184" Z="1.75" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.75" />
                  <Point X="3.284395546111" Y="1.296353344029" Z="1.75" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.75" />
                  <Point X="3.245774365821" Y="1.373307410625" Z="1.75" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.75" />
                  <Point X="3.24027572885" Y="1.456248498774" Z="1.75" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.75" />
                  <Point X="3.273058714971" Y="1.532721406608" Z="1.75" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.75" />
                  <Point X="4.095049632964" Y="2.184861014768" Z="1.75" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.75" />
                  <Point X="4.382372726462" Y="2.562473965047" Z="1.75" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.75" />
                  <Point X="4.164924371116" Y="2.90256162498" Z="1.75" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.75" />
                  <Point X="2.973298154389" Y="2.534554549576" Z="1.75" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.75" />
                  <Point X="2.484947139353" Y="2.260331952278" Z="1.75" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.75" />
                  <Point X="2.408033595093" Y="2.248128677619" Z="1.75" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.75" />
                  <Point X="2.340507937013" Y="2.267239859768" Z="1.75" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.75" />
                  <Point X="2.283518740467" Y="2.316516923369" Z="1.75" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.75" />
                  <Point X="2.251301025139" Y="2.381724848739" Z="1.75" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.75" />
                  <Point X="2.25219599038" Y="2.454522436529" Z="1.75" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.75" />
                  <Point X="2.861070803283" Y="3.538840927566" Z="1.75" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.75" />
                  <Point X="3.012140251936" Y="4.085099806441" Z="1.75" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.75" />
                  <Point X="2.629991693364" Y="4.340986967909" Z="1.75" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.75" />
                  <Point X="2.227910373406" Y="4.56054566918" Z="1.75" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.75" />
                  <Point X="1.811346468538" Y="4.741432192479" Z="1.75" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.75" />
                  <Point X="1.313599580168" Y="4.897332788331" Z="1.75" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.75" />
                  <Point X="0.654720830782" Y="5.027994142017" Z="1.75" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.75" />
                  <Point X="0.060006999195" Y="4.579073654805" Z="1.75" />
                  <Point X="0" Y="4.355124473572" Z="1.75" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>