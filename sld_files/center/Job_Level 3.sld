<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#121" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="404" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="0" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000475830078" Y="0.004715820312" />
                  <Width Value="9.783896972656" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.579273254395" Y="-3.572129638672" />
                  <Point X="0.563302062988" Y="-3.512524414063" />
                  <Point X="0.55476184082" Y="-3.491297607422" />
                  <Point X="0.54101361084" Y="-3.466323974609" />
                  <Point X="0.535835266113" Y="-3.457971679688" />
                  <Point X="0.378635467529" Y="-3.231476806641" />
                  <Point X="0.357109161377" Y="-3.207521728516" />
                  <Point X="0.326243499756" Y="-3.186398681641" />
                  <Point X="0.301404510498" Y="-3.175838134766" />
                  <Point X="0.29239364624" Y="-3.172533935547" />
                  <Point X="0.049135978699" Y="-3.097035888672" />
                  <Point X="0.020396892548" Y="-3.091593017578" />
                  <Point X="-0.013897571564" Y="-3.09266015625" />
                  <Point X="-0.038819736481" Y="-3.098042236328" />
                  <Point X="-0.046925647736" Y="-3.100170898438" />
                  <Point X="-0.290183288574" Y="-3.175669189453" />
                  <Point X="-0.319507995605" Y="-3.188986083984" />
                  <Point X="-0.349136566162" Y="-3.212288085938" />
                  <Point X="-0.367182037354" Y="-3.23351171875" />
                  <Point X="-0.372851257324" Y="-3.240882080078" />
                  <Point X="-0.530051147461" Y="-3.467377197266" />
                  <Point X="-0.538188964844" Y="-3.481573730469" />
                  <Point X="-0.55099005127" Y="-3.512524414063" />
                  <Point X="-0.916584533691" Y="-4.87694140625" />
                  <Point X="-1.079338012695" Y="-4.845351074219" />
                  <Point X="-1.086902954102" Y="-4.843404296875" />
                  <Point X="-1.24641809082" Y="-4.802362792969" />
                  <Point X="-1.216885742188" Y="-4.578041992188" />
                  <Point X="-1.214962890625" Y="-4.563438476562" />
                  <Point X="-1.214656860352" Y="-4.541239746094" />
                  <Point X="-1.217603149414" Y="-4.512827148438" />
                  <Point X="-1.218921875" Y="-4.504092285156" />
                  <Point X="-1.277036132812" Y="-4.211932617188" />
                  <Point X="-1.287037719727" Y="-4.181756835937" />
                  <Point X="-1.306946899414" Y="-4.149923339844" />
                  <Point X="-1.326581054688" Y="-4.129175292969" />
                  <Point X="-1.332944946289" Y="-4.123047851562" />
                  <Point X="-1.556905395508" Y="-3.926639160156" />
                  <Point X="-1.583210327148" Y="-3.908787597656" />
                  <Point X="-1.618384155273" Y="-3.89565234375" />
                  <Point X="-1.646591430664" Y="-3.891144287109" />
                  <Point X="-1.655370727539" Y="-3.890157226562" />
                  <Point X="-1.952616577148" Y="-3.870674804688" />
                  <Point X="-1.984352661133" Y="-3.872525634766" />
                  <Point X="-2.020254760742" Y="-3.883517333984" />
                  <Point X="-2.045377441406" Y="-3.897112548828" />
                  <Point X="-2.052942871094" Y="-3.901673828125" />
                  <Point X="-2.300624267578" Y="-4.067169677734" />
                  <Point X="-2.312785644531" Y="-4.076821533203" />
                  <Point X="-2.335102783203" Y="-4.099461914062" />
                  <Point X="-2.480148925781" Y="-4.288489746094" />
                  <Point X="-2.80170703125" Y="-4.089388916016" />
                  <Point X="-2.812182617188" Y="-4.081322998047" />
                  <Point X="-3.104721923828" Y="-3.856077880859" />
                  <Point X="-2.453441894531" Y="-2.728028076172" />
                  <Point X="-2.423761230469" Y="-2.676619628906" />
                  <Point X="-2.412859863281" Y="-2.647657226562" />
                  <Point X="-2.406588378906" Y="-2.616135253906" />
                  <Point X="-2.405634277344" Y="-2.584754150391" />
                  <Point X="-2.414960693359" Y="-2.554775878906" />
                  <Point X="-2.429870117188" Y="-2.525250244141" />
                  <Point X="-2.447497070312" Y="-2.500896484375" />
                  <Point X="-2.464154052734" Y="-2.484239746094" />
                  <Point X="-2.489311767578" Y="-2.466212158203" />
                  <Point X="-2.518140625" Y="-2.451995605469" />
                  <Point X="-2.5477578125" Y="-2.443011474609" />
                  <Point X="-2.578691162109" Y="-2.444024169922" />
                  <Point X="-2.610217285156" Y="-2.450295166016" />
                  <Point X="-2.63918359375" Y="-2.461197265625" />
                  <Point X="-3.818024414062" Y="-3.141801269531" />
                  <Point X="-4.082859619141" Y="-2.793862060547" />
                  <Point X="-4.090367431641" Y="-2.781272705078" />
                  <Point X="-4.306142578125" Y="-2.419450439453" />
                  <Point X="-3.157886230469" Y="-1.538361694336" />
                  <Point X="-3.105954589844" Y="-1.498513427734" />
                  <Point X="-3.083962158203" Y="-1.47465246582" />
                  <Point X="-3.065690185547" Y="-1.446335205078" />
                  <Point X="-3.053549560547" Y="-1.418646972656" />
                  <Point X="-3.046152099609" Y="-1.390085693359" />
                  <Point X="-3.04334765625" Y="-1.35965625" />
                  <Point X="-3.045556640625" Y="-1.327985595703" />
                  <Point X="-3.052774169922" Y="-1.29772277832" />
                  <Point X="-3.069381835938" Y="-1.271414672852" />
                  <Point X="-3.091270263672" Y="-1.246836547852" />
                  <Point X="-3.114028564453" Y="-1.228145263672" />
                  <Point X="-3.139455322266" Y="-1.213180419922" />
                  <Point X="-3.168718261719" Y="-1.201956542969" />
                  <Point X="-3.200605957031" Y="-1.195474731445" />
                  <Point X="-3.231929199219" Y="-1.194383911133" />
                  <Point X="-4.732102050781" Y="-1.391885253906" />
                  <Point X="-4.834076660156" Y="-0.992658752441" />
                  <Point X="-4.8360625" Y="-0.978772705078" />
                  <Point X="-4.892424316406" Y="-0.584698181152" />
                  <Point X="-3.592033935547" Y="-0.236259674072" />
                  <Point X="-3.532875976562" Y="-0.220408355713" />
                  <Point X="-3.516418701172" Y="-0.214320953369" />
                  <Point X="-3.486622558594" Y="-0.198701568604" />
                  <Point X="-3.459976074219" Y="-0.180207580566" />
                  <Point X="-3.4368515625" Y="-0.157399551392" />
                  <Point X="-3.417238769531" Y="-0.129955841064" />
                  <Point X="-3.403799316406" Y="-0.102878555298" />
                  <Point X="-3.394917236328" Y="-0.074259994507" />
                  <Point X="-3.390654785156" Y="-0.044964897156" />
                  <Point X="-3.3910234375" Y="-0.014134744644" />
                  <Point X="-3.395286132812" Y="0.012889413834" />
                  <Point X="-3.404168457031" Y="0.041507980347" />
                  <Point X="-3.419011230469" Y="0.070497367859" />
                  <Point X="-3.439361572266" Y="0.097520309448" />
                  <Point X="-3.46108203125" Y="0.118415176392" />
                  <Point X="-3.487728515625" Y="0.136909317017" />
                  <Point X="-3.501925292969" Y="0.145047103882" />
                  <Point X="-3.532875976562" Y="0.157848358154" />
                  <Point X="-4.89181640625" Y="0.521975219727" />
                  <Point X="-4.82448828125" Y="0.976973815918" />
                  <Point X="-4.820488769531" Y="0.991733276367" />
                  <Point X="-4.703551269531" Y="1.423267822266" />
                  <Point X="-3.808072265625" Y="1.305375854492" />
                  <Point X="-3.765666259766" Y="1.29979284668" />
                  <Point X="-3.744986083984" Y="1.299341674805" />
                  <Point X="-3.723424560547" Y="1.301228149414" />
                  <Point X="-3.703135742188" Y="1.305264038086" />
                  <Point X="-3.700686767578" Y="1.306036254883" />
                  <Point X="-3.641709716797" Y="1.324631591797" />
                  <Point X="-3.622779052734" Y="1.332961669922" />
                  <Point X="-3.604034667969" Y="1.343783691406" />
                  <Point X="-3.587353515625" Y="1.356014648438" />
                  <Point X="-3.573715087891" Y="1.37156628418" />
                  <Point X="-3.561300537109" Y="1.389296020508" />
                  <Point X="-3.551350830078" Y="1.407431884766" />
                  <Point X="-3.550368164062" Y="1.409804321289" />
                  <Point X="-3.526703369141" Y="1.466936157227" />
                  <Point X="-3.520915527344" Y="1.486794067383" />
                  <Point X="-3.517157226562" Y="1.508109130859" />
                  <Point X="-3.5158046875" Y="1.528749267578" />
                  <Point X="-3.518951171875" Y="1.549192871094" />
                  <Point X="-3.524552978516" Y="1.570099243164" />
                  <Point X="-3.532051269531" Y="1.589380371094" />
                  <Point X="-3.533237060547" Y="1.591658081055" />
                  <Point X="-3.561791015625" Y="1.646509887695" />
                  <Point X="-3.573280761719" Y="1.663705322266" />
                  <Point X="-3.587193115234" Y="1.680285766602" />
                  <Point X="-3.602135742188" Y="1.694590209961" />
                  <Point X="-4.351860351563" Y="2.269874267578" />
                  <Point X="-4.081153564453" Y="2.733659912109" />
                  <Point X="-4.070564453125" Y="2.747270507812" />
                  <Point X="-3.750505126953" Y="3.158661621094" />
                  <Point X="-3.232701660156" Y="2.859707763672" />
                  <Point X="-3.206657226562" Y="2.844670898438" />
                  <Point X="-3.187724365234" Y="2.836340332031" />
                  <Point X="-3.16708203125" Y="2.829831787109" />
                  <Point X="-3.146792724609" Y="2.825796142578" />
                  <Point X="-3.143381835938" Y="2.825497802734" />
                  <Point X="-3.061243408203" Y="2.818311523438" />
                  <Point X="-3.040565185547" Y="2.818762939453" />
                  <Point X="-3.019106445312" Y="2.821588134766" />
                  <Point X="-2.999014648438" Y="2.826504638672" />
                  <Point X="-2.980463134766" Y="2.835653320313" />
                  <Point X="-2.962208740234" Y="2.847282714844" />
                  <Point X="-2.946080566406" Y="2.860226318359" />
                  <Point X="-2.943659423828" Y="2.862647216797" />
                  <Point X="-2.885356933594" Y="2.920949707031" />
                  <Point X="-2.872406738281" Y="2.937084472656" />
                  <Point X="-2.86077734375" Y="2.955338867188" />
                  <Point X="-2.851628662109" Y="2.973890380859" />
                  <Point X="-2.846712158203" Y="2.993982177734" />
                  <Point X="-2.843886962891" Y="3.015440917969" />
                  <Point X="-2.843435546875" Y="3.036119140625" />
                  <Point X="-2.843733886719" Y="3.039530029297" />
                  <Point X="-2.850920166016" Y="3.121668701172" />
                  <Point X="-2.854956054688" Y="3.141958740234" />
                  <Point X="-2.861464599609" Y="3.162600585938" />
                  <Point X="-2.869794921875" Y="3.181532714844" />
                  <Point X="-3.183333007812" Y="3.724596679688" />
                  <Point X="-2.700625732422" Y="4.094683349609" />
                  <Point X="-2.68394921875" Y="4.103948730469" />
                  <Point X="-2.167036865234" Y="4.391134277344" />
                  <Point X="-2.051168212891" Y="4.240131347656" />
                  <Point X="-2.04319519043" Y="4.229740722656" />
                  <Point X="-2.028892700195" Y="4.214799804688" />
                  <Point X="-2.012312866211" Y="4.200887207031" />
                  <Point X="-1.995110595703" Y="4.189393554688" />
                  <Point X="-1.991314331055" Y="4.187417480469" />
                  <Point X="-1.89989453125" Y="4.139826660156" />
                  <Point X="-1.88061730957" Y="4.132330566406" />
                  <Point X="-1.85971081543" Y="4.126729003906" />
                  <Point X="-1.839267822266" Y="4.123582519531" />
                  <Point X="-1.818628417969" Y="4.124935546875" />
                  <Point X="-1.797313110352" Y="4.128693847656" />
                  <Point X="-1.777447631836" Y="4.134484375" />
                  <Point X="-1.773493530273" Y="4.136122558594" />
                  <Point X="-1.678273681641" Y="4.175563964844" />
                  <Point X="-1.660145141602" Y="4.185510742187" />
                  <Point X="-1.642415405273" Y="4.197925292969" />
                  <Point X="-1.626863647461" Y="4.211563964844" />
                  <Point X="-1.614632568359" Y="4.228245117188" />
                  <Point X="-1.603810791016" Y="4.246989257813" />
                  <Point X="-1.595480712891" Y="4.265920898438" />
                  <Point X="-1.594193725586" Y="4.270002929688" />
                  <Point X="-1.563201293945" Y="4.368297363281" />
                  <Point X="-1.559165649414" Y="4.388584960938" />
                  <Point X="-1.557279174805" Y="4.410146484375" />
                  <Point X="-1.557730224609" Y="4.430826660156" />
                  <Point X="-1.584201904297" Y="4.631897949219" />
                  <Point X="-0.949635070801" Y="4.80980859375" />
                  <Point X="-0.929427368164" Y="4.812173339844" />
                  <Point X="-0.294710876465" Y="4.886457519531" />
                  <Point X="-0.141608108521" Y="4.315069824219" />
                  <Point X="-0.133903381348" Y="4.286315429688" />
                  <Point X="-0.121129760742" Y="4.258124023438" />
                  <Point X="-0.103271614075" Y="4.231397460938" />
                  <Point X="-0.082114562988" Y="4.20880859375" />
                  <Point X="-0.054819355011" Y="4.19421875" />
                  <Point X="-0.024381278992" Y="4.183886230469" />
                  <Point X="0.006155913353" Y="4.178844238281" />
                  <Point X="0.03669311142" Y="4.183886230469" />
                  <Point X="0.067131187439" Y="4.19421875" />
                  <Point X="0.094426498413" Y="4.20880859375" />
                  <Point X="0.115583602905" Y="4.231397460938" />
                  <Point X="0.133441741943" Y="4.258124023438" />
                  <Point X="0.146215209961" Y="4.286315429688" />
                  <Point X="0.307419311523" Y="4.8879375" />
                  <Point X="0.844043151855" Y="4.83173828125" />
                  <Point X="0.860767822266" Y="4.827700683594" />
                  <Point X="1.481024169922" Y="4.677951660156" />
                  <Point X="1.490487670898" Y="4.674519042969" />
                  <Point X="1.89464465332" Y="4.527928710938" />
                  <Point X="1.90517565918" Y="4.523003417969" />
                  <Point X="2.294576416016" Y="4.340893554688" />
                  <Point X="2.304782714844" Y="4.334947265625" />
                  <Point X="2.680974121094" Y="4.115777832031" />
                  <Point X="2.690576171875" Y="4.10894921875" />
                  <Point X="2.943260009766" Y="3.929254882812" />
                  <Point X="2.181934082031" Y="2.610599365234" />
                  <Point X="2.147581054688" Y="2.551097900391" />
                  <Point X="2.141144775391" Y="2.537594238281" />
                  <Point X="2.132220947266" Y="2.512855712891" />
                  <Point X="2.111607177734" Y="2.435770263672" />
                  <Point X="2.108484130859" Y="2.415624511719" />
                  <Point X="2.107480224609" Y="2.393954345703" />
                  <Point X="2.108061523438" Y="2.378185302734" />
                  <Point X="2.116099121094" Y="2.311528320312" />
                  <Point X="2.121442138672" Y="2.289604980469" />
                  <Point X="2.129708251953" Y="2.267516357422" />
                  <Point X="2.140072753906" Y="2.247468505859" />
                  <Point X="2.141785644531" Y="2.244944335938" />
                  <Point X="2.183030517578" Y="2.184159912109" />
                  <Point X="2.196254150391" Y="2.1685859375" />
                  <Point X="2.212078369141" Y="2.153572753906" />
                  <Point X="2.224123291016" Y="2.143879150391" />
                  <Point X="2.284907958984" Y="2.102634521484" />
                  <Point X="2.304952636719" Y="2.092271972656" />
                  <Point X="2.327040771484" Y="2.084006103516" />
                  <Point X="2.34895703125" Y="2.078664306641" />
                  <Point X="2.351725097656" Y="2.078330322266" />
                  <Point X="2.418381835938" Y="2.070292724609" />
                  <Point X="2.43914453125" Y="2.070073242188" />
                  <Point X="2.461248291016" Y="2.072267089844" />
                  <Point X="2.476407226562" Y="2.07502734375" />
                  <Point X="2.553492431641" Y="2.095640869141" />
                  <Point X="2.565283447266" Y="2.099638183594" />
                  <Point X="2.588533935547" Y="2.110145019531" />
                  <Point X="3.967326660156" Y="2.90619140625" />
                  <Point X="4.123271484375" Y="2.689463378906" />
                  <Point X="4.128623046875" Y="2.680619628906" />
                  <Point X="4.262198730469" Y="2.459884277344" />
                  <Point X="3.276012695312" Y="1.703156738281" />
                  <Point X="3.230783691406" Y="1.668451293945" />
                  <Point X="3.219478027344" Y="1.658235961914" />
                  <Point X="3.201669921875" Y="1.638622314453" />
                  <Point X="3.146191650391" Y="1.566246459961" />
                  <Point X="3.1354765625" Y="1.548572143555" />
                  <Point X="3.126148925781" Y="1.528551635742" />
                  <Point X="3.120771728516" Y="1.514017456055" />
                  <Point X="3.100105957031" Y="1.440121582031" />
                  <Point X="3.096652587891" Y="1.417823120117" />
                  <Point X="3.095836425781" Y="1.394252929688" />
                  <Point X="3.097738769531" Y="1.371770996094" />
                  <Point X="3.098443115234" Y="1.368356933594" />
                  <Point X="3.115407714844" Y="1.286138183594" />
                  <Point X="3.121789550781" Y="1.266406494141" />
                  <Point X="3.130903564453" Y="1.246116455078" />
                  <Point X="3.138198730469" Y="1.232827636719" />
                  <Point X="3.184340332031" Y="1.162694824219" />
                  <Point X="3.198893798828" Y="1.145450195312" />
                  <Point X="3.216137695313" Y="1.129360473633" />
                  <Point X="3.234347167969" Y="1.116034912109" />
                  <Point X="3.237123779297" Y="1.114471923828" />
                  <Point X="3.303989257812" Y="1.076832641602" />
                  <Point X="3.323386474609" Y="1.068595825195" />
                  <Point X="3.345115966797" Y="1.0621015625" />
                  <Point X="3.359872314453" Y="1.058942504883" />
                  <Point X="3.450278808594" Y="1.046994018555" />
                  <Point X="3.462702148438" Y="1.046175048828" />
                  <Point X="3.488203613281" Y="1.04698449707" />
                  <Point X="4.77683984375" Y="1.21663671875" />
                  <Point X="4.845936523438" Y="0.932808044434" />
                  <Point X="4.847622558594" Y="0.921977844238" />
                  <Point X="4.890864746094" Y="0.644238525391" />
                  <Point X="3.768413330078" Y="0.343478607178" />
                  <Point X="3.716579833984" Y="0.329589935303" />
                  <Point X="3.701985107422" Y="0.324370178223" />
                  <Point X="3.677857177734" Y="0.312935943604" />
                  <Point X="3.589035644531" Y="0.261595428467" />
                  <Point X="3.572171142578" Y="0.24918170166" />
                  <Point X="3.555646240234" Y="0.233941497803" />
                  <Point X="3.545318115234" Y="0.222756668091" />
                  <Point X="3.492025146484" Y="0.154848968506" />
                  <Point X="3.480301025391" Y="0.135569152832" />
                  <Point X="3.470527099609" Y="0.114105384827" />
                  <Point X="3.463681152344" Y="0.092606552124" />
                  <Point X="3.462943359375" Y="0.088754722595" />
                  <Point X="3.445178955078" Y="-0.004003797531" />
                  <Point X="3.443533203125" Y="-0.024966661453" />
                  <Point X="3.444270996094" Y="-0.047627906799" />
                  <Point X="3.445916259766" Y="-0.062405464172" />
                  <Point X="3.463680664062" Y="-0.155164138794" />
                  <Point X="3.470527099609" Y="-0.176665542603" />
                  <Point X="3.480301025391" Y="-0.198129318237" />
                  <Point X="3.492023193359" Y="-0.217406707764" />
                  <Point X="3.494236083984" Y="-0.220226516724" />
                  <Point X="3.547529052734" Y="-0.288134368896" />
                  <Point X="3.562539794922" Y="-0.303364257812" />
                  <Point X="3.580540283203" Y="-0.317916534424" />
                  <Point X="3.592724121094" Y="-0.326287475586" />
                  <Point X="3.681545654297" Y="-0.37762802124" />
                  <Point X="3.692710205078" Y="-0.383139068604" />
                  <Point X="3.716579833984" Y="-0.392149963379" />
                  <Point X="4.89147265625" Y="-0.706961303711" />
                  <Point X="4.855021484375" Y="-0.948730712891" />
                  <Point X="4.852861816406" Y="-0.958196166992" />
                  <Point X="4.801174316406" Y="-1.184698974609" />
                  <Point X="3.484494384766" Y="-1.011354797363" />
                  <Point X="3.424382080078" Y="-1.003441101074" />
                  <Point X="3.403113769531" Y="-1.00304309082" />
                  <Point X="3.378728759766" Y="-1.005329528809" />
                  <Point X="3.367419677734" Y="-1.007082214355" />
                  <Point X="3.193094482422" Y="-1.044972412109" />
                  <Point X="3.162724609375" Y="-1.055693359375" />
                  <Point X="3.13250390625" Y="-1.075057983398" />
                  <Point X="3.115628417969" Y="-1.091094482422" />
                  <Point X="3.108021972656" Y="-1.09922253418" />
                  <Point X="3.002653320312" Y="-1.225948242188" />
                  <Point X="2.986626220703" Y="-1.250417602539" />
                  <Point X="2.974756347656" Y="-1.281099243164" />
                  <Point X="2.970533447266" Y="-1.302467041016" />
                  <Point X="2.969130371094" Y="-1.312180541992" />
                  <Point X="2.954028564453" Y="-1.476295654297" />
                  <Point X="2.955109130859" Y="-1.508482666016" />
                  <Point X="2.96459375" Y="-1.543369506836" />
                  <Point X="2.975111328125" Y="-1.564764160156" />
                  <Point X="2.980456542969" Y="-1.574227783203" />
                  <Point X="3.076930664062" Y="-1.724287109375" />
                  <Point X="3.086932373047" Y="-1.73723828125" />
                  <Point X="3.110628417969" Y="-1.760909057617" />
                  <Point X="4.213122558594" Y="-2.606883056641" />
                  <Point X="4.124811523438" Y="-2.749782958984" />
                  <Point X="4.120340332031" Y="-2.756136230469" />
                  <Point X="4.028981445312" Y="-2.885945068359" />
                  <Point X="2.854492675781" Y="-2.207853515625" />
                  <Point X="2.800954589844" Y="-2.176943115234" />
                  <Point X="2.780887451172" Y="-2.168262695312" />
                  <Point X="2.756158203125" Y="-2.160803955078" />
                  <Point X="2.745609130859" Y="-2.158269287109" />
                  <Point X="2.538134277344" Y="-2.120799560547" />
                  <Point X="2.505973388672" Y="-2.119082275391" />
                  <Point X="2.470117431641" Y="-2.125611816406" />
                  <Point X="2.447288330078" Y="-2.134549072266" />
                  <Point X="2.43767578125" Y="-2.138943603516" />
                  <Point X="2.265315185547" Y="-2.229655761719" />
                  <Point X="2.241143310547" Y="-2.246130126953" />
                  <Point X="2.218143798828" Y="-2.270374755859" />
                  <Point X="2.205485839844" Y="-2.289600830078" />
                  <Point X="2.200764892578" Y="-2.297596679688" />
                  <Point X="2.110052734375" Y="-2.469957275391" />
                  <Point X="2.098733642578" Y="-2.500108886719" />
                  <Point X="2.094185058594" Y="-2.536546142578" />
                  <Point X="2.095953613281" Y="-2.561662841797" />
                  <Point X="2.097231445312" Y="-2.571873779297" />
                  <Point X="2.134701171875" Y="-2.779348632812" />
                  <Point X="2.138985595703" Y="-2.795141845703" />
                  <Point X="2.151819091797" Y="-2.826078613281" />
                  <Point X="2.861283447266" Y="-4.054906738281" />
                  <Point X="2.781847412109" Y="-4.111645996094" />
                  <Point X="2.776857421875" Y="-4.114875488281" />
                  <Point X="2.701765136719" Y="-4.163481445312" />
                  <Point X="1.799400634766" Y="-2.987497802734" />
                  <Point X="1.758546020508" Y="-2.934255126953" />
                  <Point X="1.742912109375" Y="-2.918217285156" />
                  <Point X="1.721786621094" Y="-2.901134277344" />
                  <Point X="1.713426879883" Y="-2.895094238281" />
                  <Point X="1.50880078125" Y="-2.763538574219" />
                  <Point X="1.479745361328" Y="-2.749644775391" />
                  <Point X="1.44360949707" Y="-2.741947021484" />
                  <Point X="1.417858886719" Y="-2.741581787109" />
                  <Point X="1.407806396484" Y="-2.741971923828" />
                  <Point X="1.184012817383" Y="-2.762565673828" />
                  <Point X="1.155377807617" Y="-2.76853515625" />
                  <Point X="1.124456420898" Y="-2.782401367188" />
                  <Point X="1.104447875977" Y="-2.796115478516" />
                  <Point X="1.097420043945" Y="-2.801427734375" />
                  <Point X="0.924612121582" Y="-2.945111572266" />
                  <Point X="0.902616027832" Y="-2.968636230469" />
                  <Point X="0.884305358887" Y="-3.001055664062" />
                  <Point X="0.876041625977" Y="-3.026253662109" />
                  <Point X="0.873478759766" Y="-3.035680175781" />
                  <Point X="0.821810058594" Y="-3.273396728516" />
                  <Point X="0.819724609375" Y="-3.289627441406" />
                  <Point X="0.819742370605" Y="-3.323120117188" />
                  <Point X="1.022065246582" Y="-4.859915039062" />
                  <Point X="0.975672851562" Y="-4.870083984375" />
                  <Point X="0.971056335449" Y="-4.870922851562" />
                  <Point X="0.929315612793" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058427612305" Y="-4.752637207031" />
                  <Point X="-1.063226928711" Y="-4.751401855469" />
                  <Point X="-1.141246459961" Y="-4.731328125" />
                  <Point X="-1.122698486328" Y="-4.590441894531" />
                  <Point X="-1.119971923828" Y="-4.564748046875" />
                  <Point X="-1.119665893555" Y="-4.542549316406" />
                  <Point X="-1.120163574219" Y="-4.531440917969" />
                  <Point X="-1.123109985352" Y="-4.503028320312" />
                  <Point X="-1.125747436523" Y="-4.48555859375" />
                  <Point X="-1.183861572266" Y="-4.193398925781" />
                  <Point X="-1.186860229492" Y="-4.182044433594" />
                  <Point X="-1.196861816406" Y="-4.151868652344" />
                  <Point X="-1.206492919922" Y="-4.1313828125" />
                  <Point X="-1.226402099609" Y="-4.099549316406" />
                  <Point X="-1.237945068359" Y="-4.084625976562" />
                  <Point X="-1.257579223633" Y="-4.063877929688" />
                  <Point X="-1.270306884766" Y="-4.051623291016" />
                  <Point X="-1.494267456055" Y="-3.855214599609" />
                  <Point X="-1.503559204102" Y="-3.848031494141" />
                  <Point X="-1.529864013672" Y="-3.830179931641" />
                  <Point X="-1.549975463867" Y="-3.819790771484" />
                  <Point X="-1.585149291992" Y="-3.806655517578" />
                  <Point X="-1.603391601562" Y="-3.801842773438" />
                  <Point X="-1.631598876953" Y="-3.797334716797" />
                  <Point X="-1.649157470703" Y="-3.795360595703" />
                  <Point X="-1.946403198242" Y="-3.775878173828" />
                  <Point X="-1.958147583008" Y="-3.7758359375" />
                  <Point X="-1.989883666992" Y="-3.777686767578" />
                  <Point X="-2.012163452148" Y="-3.7816875" />
                  <Point X="-2.048065429688" Y="-3.792679199219" />
                  <Point X="-2.065468505859" Y="-3.799966552734" />
                  <Point X="-2.090591064453" Y="-3.813561767578" />
                  <Point X="-2.105722167969" Y="-3.822684326172" />
                  <Point X="-2.353403564453" Y="-3.988180175781" />
                  <Point X="-2.359681640625" Y="-3.992757080078" />
                  <Point X="-2.380442138672" Y="-4.010131103516" />
                  <Point X="-2.402759277344" Y="-4.032771484375" />
                  <Point X="-2.410471435547" Y="-4.041629638672" />
                  <Point X="-2.503202636719" Y="-4.162479003906" />
                  <Point X="-2.747582519531" Y="-4.011165283203" />
                  <Point X="-2.754225097656" Y="-4.00605078125" />
                  <Point X="-2.980862792969" Y="-3.831547607422" />
                  <Point X="-2.371169433594" Y="-2.775528076172" />
                  <Point X="-2.341488769531" Y="-2.724119628906" />
                  <Point X="-2.334850830078" Y="-2.710085205078" />
                  <Point X="-2.323949462891" Y="-2.681122802734" />
                  <Point X="-2.319686035156" Y="-2.666194824219" />
                  <Point X="-2.313414550781" Y="-2.634672851562" />
                  <Point X="-2.311632324219" Y="-2.619022216797" />
                  <Point X="-2.310678222656" Y="-2.587641113281" />
                  <Point X="-2.314922851562" Y="-2.556533203125" />
                  <Point X="-2.324249267578" Y="-2.526554931641" />
                  <Point X="-2.330159179688" Y="-2.511954101562" />
                  <Point X="-2.345068603516" Y="-2.482428466797" />
                  <Point X="-2.352912841797" Y="-2.469549560547" />
                  <Point X="-2.370539794922" Y="-2.445195800781" />
                  <Point X="-2.380322509766" Y="-2.433720947266" />
                  <Point X="-2.396979492188" Y="-2.417064208984" />
                  <Point X="-2.408819091797" Y="-2.407019042969" />
                  <Point X="-2.433976806641" Y="-2.388991455078" />
                  <Point X="-2.447294921875" Y="-2.381009033203" />
                  <Point X="-2.476123779297" Y="-2.366792480469" />
                  <Point X="-2.490563964844" Y="-2.361086181641" />
                  <Point X="-2.520181152344" Y="-2.352102050781" />
                  <Point X="-2.550866210938" Y="-2.348062255859" />
                  <Point X="-2.581799560547" Y="-2.349074951172" />
                  <Point X="-2.597224853516" Y="-2.350849609375" />
                  <Point X="-2.628750976562" Y="-2.357120605469" />
                  <Point X="-2.643680908203" Y="-2.361384033203" />
                  <Point X="-2.672647216797" Y="-2.372286132812" />
                  <Point X="-2.68668359375" Y="-2.378924804688" />
                  <Point X="-3.793089599609" Y="-3.017708496094" />
                  <Point X="-4.004014892578" Y="-2.740595703125" />
                  <Point X="-4.008774902344" Y="-2.732614013672" />
                  <Point X="-4.181265625" Y="-2.443373779297" />
                  <Point X="-3.100053955078" Y="-1.613730224609" />
                  <Point X="-3.048122314453" Y="-1.573881958008" />
                  <Point X="-3.036100097656" Y="-1.562897705078" />
                  <Point X="-3.014107666016" Y="-1.539036743164" />
                  <Point X="-3.004137451172" Y="-1.52616003418" />
                  <Point X="-2.985865478516" Y="-1.497842773438" />
                  <Point X="-2.978686523438" Y="-1.48448425293" />
                  <Point X="-2.966545898438" Y="-1.456796020508" />
                  <Point X="-2.961584228516" Y="-1.442466308594" />
                  <Point X="-2.954186767578" Y="-1.413905029297" />
                  <Point X="-2.951552978516" Y="-1.398804199219" />
                  <Point X="-2.948748535156" Y="-1.368374755859" />
                  <Point X="-2.948577880859" Y="-1.353046142578" />
                  <Point X="-2.950786865234" Y="-1.321375488281" />
                  <Point X="-2.9531484375" Y="-1.305946655273" />
                  <Point X="-2.960365966797" Y="-1.275683837891" />
                  <Point X="-2.972441650391" Y="-1.247010864258" />
                  <Point X="-2.989049316406" Y="-1.220702636719" />
                  <Point X="-2.998437011719" Y="-1.208233642578" />
                  <Point X="-3.020325439453" Y="-1.183655517578" />
                  <Point X="-3.030975830078" Y="-1.173422729492" />
                  <Point X="-3.053734130859" Y="-1.154731567383" />
                  <Point X="-3.065842773438" Y="-1.146272583008" />
                  <Point X="-3.09126953125" Y="-1.131307739258" />
                  <Point X="-3.105434326172" Y="-1.124481079102" />
                  <Point X="-3.134697265625" Y="-1.113257202148" />
                  <Point X="-3.149794677734" Y="-1.108860351562" />
                  <Point X="-3.181682373047" Y="-1.102378540039" />
                  <Point X="-3.197299560547" Y="-1.100532104492" />
                  <Point X="-3.228622802734" Y="-1.09944140625" />
                  <Point X="-3.244329101562" Y="-1.100196655273" />
                  <Point X="-4.660920898438" Y="-1.286694335938" />
                  <Point X="-4.74076171875" Y="-0.974120239258" />
                  <Point X="-4.74201953125" Y="-0.965323669434" />
                  <Point X="-4.786452636719" Y="-0.654654296875" />
                  <Point X="-3.567446044922" Y="-0.328022674561" />
                  <Point X="-3.508288085938" Y="-0.312171295166" />
                  <Point X="-3.499918701172" Y="-0.309508422852" />
                  <Point X="-3.472311767578" Y="-0.298461120605" />
                  <Point X="-3.442515625" Y="-0.282841766357" />
                  <Point X="-3.432455810547" Y="-0.276746124268" />
                  <Point X="-3.405809326172" Y="-0.258252105713" />
                  <Point X="-3.393265380859" Y="-0.247844039917" />
                  <Point X="-3.370140869141" Y="-0.225035919189" />
                  <Point X="-3.359560302734" Y="-0.212636154175" />
                  <Point X="-3.339947509766" Y="-0.185192352295" />
                  <Point X="-3.332143798828" Y="-0.172191558838" />
                  <Point X="-3.318704345703" Y="-0.145114242554" />
                  <Point X="-3.313068603516" Y="-0.131037872314" />
                  <Point X="-3.304186523438" Y="-0.102419197083" />
                  <Point X="-3.300907226562" Y="-0.087938568115" />
                  <Point X="-3.296644775391" Y="-0.058643356323" />
                  <Point X="-3.295661621094" Y="-0.043829071045" />
                  <Point X="-3.296030273438" Y="-0.012998883247" />
                  <Point X="-3.29718359375" Y="0.000667141616" />
                  <Point X="-3.301446289062" Y="0.027691392899" />
                  <Point X="-3.304555664062" Y="0.041049324036" />
                  <Point X="-3.313437988281" Y="0.069667999268" />
                  <Point X="-3.319607910156" Y="0.084803611755" />
                  <Point X="-3.334450683594" Y="0.113792953491" />
                  <Point X="-3.343123535156" Y="0.127646690369" />
                  <Point X="-3.363473876953" Y="0.15466960144" />
                  <Point X="-3.373500244141" Y="0.165983963013" />
                  <Point X="-3.395220703125" Y="0.186878860474" />
                  <Point X="-3.406914794922" Y="0.19645954895" />
                  <Point X="-3.433561279297" Y="0.214953735352" />
                  <Point X="-3.440484375" Y="0.219328887939" />
                  <Point X="-3.465616210938" Y="0.232834701538" />
                  <Point X="-3.496566894531" Y="0.245635894775" />
                  <Point X="-3.508288085938" Y="0.249611251831" />
                  <Point X="-4.785446289062" Y="0.591824645996" />
                  <Point X="-4.731331542969" Y="0.957526550293" />
                  <Point X="-4.728795410156" Y="0.966886230469" />
                  <Point X="-4.633586425781" Y="1.318236938477" />
                  <Point X="-3.820472167969" Y="1.211188598633" />
                  <Point X="-3.778066162109" Y="1.20560546875" />
                  <Point X="-3.76773828125" Y="1.204815429688" />
                  <Point X="-3.747058105469" Y="1.204364257812" />
                  <Point X="-3.736705810547" Y="1.20470324707" />
                  <Point X="-3.715144287109" Y="1.20658972168" />
                  <Point X="-3.704890136719" Y="1.208053588867" />
                  <Point X="-3.684601318359" Y="1.212089599609" />
                  <Point X="-3.672117675781" Y="1.21543371582" />
                  <Point X="-3.613140625" Y="1.234029052734" />
                  <Point X="-3.603447265625" Y="1.237677612305" />
                  <Point X="-3.584516601562" Y="1.246007568359" />
                  <Point X="-3.575279296875" Y="1.250689208984" />
                  <Point X="-3.556534912109" Y="1.261511230469" />
                  <Point X="-3.547860839844" Y="1.267171020508" />
                  <Point X="-3.5311796875" Y="1.279402099609" />
                  <Point X="-3.515928710938" Y="1.293376831055" />
                  <Point X="-3.502290283203" Y="1.308928466797" />
                  <Point X="-3.495895751953" Y="1.317076416016" />
                  <Point X="-3.483481201172" Y="1.334806152344" />
                  <Point X="-3.478011474609" Y="1.343601928711" />
                  <Point X="-3.468061767578" Y="1.361737792969" />
                  <Point X="-3.462599121094" Y="1.373450317383" />
                  <Point X="-3.438934326172" Y="1.43058215332" />
                  <Point X="-3.435498291016" Y="1.440353271484" />
                  <Point X="-3.429710449219" Y="1.460211181641" />
                  <Point X="-3.427358642578" Y="1.470297973633" />
                  <Point X="-3.423600341797" Y="1.491613037109" />
                  <Point X="-3.422360595703" Y="1.501896972656" />
                  <Point X="-3.421008056641" Y="1.522537231445" />
                  <Point X="-3.421910400391" Y="1.543200561523" />
                  <Point X="-3.425056884766" Y="1.563644165039" />
                  <Point X="-3.427188232422" Y="1.573780639648" />
                  <Point X="-3.432790039062" Y="1.594686889648" />
                  <Point X="-3.436012695312" Y="1.604531982422" />
                  <Point X="-3.443510986328" Y="1.623813110352" />
                  <Point X="-3.448972412109" Y="1.635526855469" />
                  <Point X="-3.477526367188" Y="1.690378662109" />
                  <Point X="-3.482801757812" Y="1.699289306641" />
                  <Point X="-3.494291503906" Y="1.716484863281" />
                  <Point X="-3.500505859375" Y="1.72476940918" />
                  <Point X="-3.514418212891" Y="1.741349731445" />
                  <Point X="-3.521499511719" Y="1.748910400391" />
                  <Point X="-3.536442138672" Y="1.76321484375" />
                  <Point X="-3.544303466797" Y="1.769958862305" />
                  <Point X="-4.227614746094" Y="2.294282226563" />
                  <Point X="-4.002292236328" Y="2.680313720703" />
                  <Point X="-3.995584228516" Y="2.688935546875" />
                  <Point X="-3.726338623047" Y="3.035012451172" />
                  <Point X="-3.280201660156" Y="2.777435302734" />
                  <Point X="-3.254157226562" Y="2.7623984375" />
                  <Point X="-3.244917724609" Y="2.757716064453" />
                  <Point X="-3.225984863281" Y="2.749385498047" />
                  <Point X="-3.216291503906" Y="2.745737304688" />
                  <Point X="-3.195649169922" Y="2.739228759766" />
                  <Point X="-3.185614990234" Y="2.736656982422" />
                  <Point X="-3.165325683594" Y="2.732621337891" />
                  <Point X="-3.151659667969" Y="2.730859130859" />
                  <Point X="-3.069521240234" Y="2.723672851562" />
                  <Point X="-3.059169921875" Y="2.723334228516" />
                  <Point X="-3.038491699219" Y="2.723785644531" />
                  <Point X="-3.028164794922" Y="2.724575683594" />
                  <Point X="-3.006706054688" Y="2.727400878906" />
                  <Point X="-2.996525878906" Y="2.729310791016" />
                  <Point X="-2.976434082031" Y="2.734227294922" />
                  <Point X="-2.956996826172" Y="2.741302001953" />
                  <Point X="-2.9384453125" Y="2.750450683594" />
                  <Point X="-2.929419433594" Y="2.75553125" />
                  <Point X="-2.911165039062" Y="2.767160644531" />
                  <Point X="-2.902747802734" Y="2.773192382813" />
                  <Point X="-2.886619628906" Y="2.786135986328" />
                  <Point X="-2.876487548828" Y="2.79546875" />
                  <Point X="-2.818185058594" Y="2.853771240234" />
                  <Point X="-2.81126953125" Y="2.861485107422" />
                  <Point X="-2.798319335938" Y="2.877619873047" />
                  <Point X="-2.792284667969" Y="2.886040771484" />
                  <Point X="-2.780655273438" Y="2.904295166016" />
                  <Point X="-2.775574707031" Y="2.913321044922" />
                  <Point X="-2.766426025391" Y="2.931872558594" />
                  <Point X="-2.759351318359" Y="2.951309814453" />
                  <Point X="-2.754434814453" Y="2.971401611328" />
                  <Point X="-2.752524902344" Y="2.981581787109" />
                  <Point X="-2.749699707031" Y="3.003040527344" />
                  <Point X="-2.748909667969" Y="3.013367431641" />
                  <Point X="-2.748458251953" Y="3.034045654297" />
                  <Point X="-2.749095214844" Y="3.047807861328" />
                  <Point X="-2.756281494141" Y="3.129946533203" />
                  <Point X="-2.757745605469" Y="3.140202148438" />
                  <Point X="-2.761781494141" Y="3.1604921875" />
                  <Point X="-2.764353271484" Y="3.170526611328" />
                  <Point X="-2.770861816406" Y="3.191168457031" />
                  <Point X="-2.774510009766" Y="3.200861572266" />
                  <Point X="-2.782840332031" Y="3.219793701172" />
                  <Point X="-2.787522460938" Y="3.229032714844" />
                  <Point X="-3.059387451172" Y="3.699916503906" />
                  <Point X="-2.648373291016" Y="4.015036621094" />
                  <Point X="-2.637810546875" Y="4.020905273438" />
                  <Point X="-2.192525146484" Y="4.268296386719" />
                  <Point X="-2.126536865234" Y="4.182298828125" />
                  <Point X="-2.111820556641" Y="4.164047851562" />
                  <Point X="-2.097518066406" Y="4.149106933594" />
                  <Point X="-2.089958740234" Y="4.142026367188" />
                  <Point X="-2.07337890625" Y="4.128113769531" />
                  <Point X="-2.065090332031" Y="4.121896484375" />
                  <Point X="-2.047888061523" Y="4.110402832031" />
                  <Point X="-2.035178100586" Y="4.103150390625" />
                  <Point X="-1.943758422852" Y="4.055559326172" />
                  <Point X="-1.934324584961" Y="4.051285400391" />
                  <Point X="-1.915047241211" Y="4.043789306641" />
                  <Point X="-1.905203857422" Y="4.040567382812" />
                  <Point X="-1.884297363281" Y="4.034965820313" />
                  <Point X="-1.874162597656" Y="4.032834716797" />
                  <Point X="-1.853719604492" Y="4.029688232422" />
                  <Point X="-1.833053344727" Y="4.028785888672" />
                  <Point X="-1.81241394043" Y="4.030138916016" />
                  <Point X="-1.802132568359" Y="4.031378662109" />
                  <Point X="-1.780817260742" Y="4.035136962891" />
                  <Point X="-1.770728271484" Y="4.037489501953" />
                  <Point X="-1.750862792969" Y="4.043280029297" />
                  <Point X="-1.737132202148" Y="4.048356689453" />
                  <Point X="-1.641912353516" Y="4.087798095703" />
                  <Point X="-1.632575927734" Y="4.092277099609" />
                  <Point X="-1.614447265625" Y="4.102223632812" />
                  <Point X="-1.605655273438" Y="4.10769140625" />
                  <Point X="-1.587925537109" Y="4.120105957031" />
                  <Point X="-1.57977734375" Y="4.126500976563" />
                  <Point X="-1.564225341797" Y="4.140139648437" />
                  <Point X="-1.550251220703" Y="4.155389648437" />
                  <Point X="-1.538020141602" Y="4.172070800781" />
                  <Point X="-1.532359863281" Y="4.180745605469" />
                  <Point X="-1.521538085938" Y="4.199489746094" />
                  <Point X="-1.516856201172" Y="4.208728515625" />
                  <Point X="-1.508526000977" Y="4.22766015625" />
                  <Point X="-1.503590209961" Y="4.2414375" />
                  <Point X="-1.47259777832" Y="4.339731933594" />
                  <Point X="-1.470026855469" Y="4.349763183594" />
                  <Point X="-1.465991088867" Y="4.37005078125" />
                  <Point X="-1.46452722168" Y="4.3803046875" />
                  <Point X="-1.46264074707" Y="4.401866210938" />
                  <Point X="-1.462301757812" Y="4.412217773438" />
                  <Point X="-1.462752807617" Y="4.432897949219" />
                  <Point X="-1.46354309082" Y="4.4432265625" />
                  <Point X="-1.479266235352" Y="4.562655273438" />
                  <Point X="-0.931178344727" Y="4.7163203125" />
                  <Point X="-0.918385559082" Y="4.717817382812" />
                  <Point X="-0.365222106934" Y="4.782557128906" />
                  <Point X="-0.233371002197" Y="4.290481933594" />
                  <Point X="-0.225666397095" Y="4.261727539062" />
                  <Point X="-0.220435165405" Y="4.247107421875" />
                  <Point X="-0.207661468506" Y="4.218916015625" />
                  <Point X="-0.200119308472" Y="4.205344726562" />
                  <Point X="-0.182261230469" Y="4.178618164062" />
                  <Point X="-0.172608306885" Y="4.166455566406" />
                  <Point X="-0.151451248169" Y="4.143866699219" />
                  <Point X="-0.126897850037" Y="4.125026367188" />
                  <Point X="-0.099602661133" Y="4.110436523438" />
                  <Point X="-0.085356559753" Y="4.104260742188" />
                  <Point X="-0.054918586731" Y="4.093927978516" />
                  <Point X="-0.039857143402" Y="4.090155273438" />
                  <Point X="-0.009320039749" Y="4.08511328125" />
                  <Point X="0.02163187027" Y="4.08511328125" />
                  <Point X="0.052168972015" Y="4.090155273438" />
                  <Point X="0.067230415344" Y="4.093927978516" />
                  <Point X="0.097668388367" Y="4.104260742188" />
                  <Point X="0.111914344788" Y="4.110436523438" />
                  <Point X="0.139209686279" Y="4.125026367188" />
                  <Point X="0.163763076782" Y="4.143866699219" />
                  <Point X="0.184920150757" Y="4.166455566406" />
                  <Point X="0.194573196411" Y="4.178618164062" />
                  <Point X="0.212431289673" Y="4.205344726562" />
                  <Point X="0.219973754883" Y="4.218916503906" />
                  <Point X="0.232747146606" Y="4.247107910156" />
                  <Point X="0.237978225708" Y="4.261727539062" />
                  <Point X="0.378190155029" Y="4.785006347656" />
                  <Point X="0.827878051758" Y="4.737911621094" />
                  <Point X="0.838473754883" Y="4.735353515625" />
                  <Point X="1.453596069336" Y="4.586844238281" />
                  <Point X="1.458094238281" Y="4.585212402344" />
                  <Point X="1.858250488281" Y="4.440073242188" />
                  <Point X="1.864928955078" Y="4.436949707031" />
                  <Point X="2.250453369141" Y="4.256652832031" />
                  <Point X="2.256959228516" Y="4.252862304688" />
                  <Point X="2.629426757812" Y="4.035862792969" />
                  <Point X="2.635518798828" Y="4.031530273438" />
                  <Point X="2.817779785156" Y="3.901916748047" />
                  <Point X="2.099661621094" Y="2.658099365234" />
                  <Point X="2.06530859375" Y="2.598597900391" />
                  <Point X="2.061823974609" Y="2.591972412109" />
                  <Point X="2.05178125" Y="2.569830078125" />
                  <Point X="2.042857299805" Y="2.545091552734" />
                  <Point X="2.040445678711" Y="2.537397705078" />
                  <Point X="2.01983190918" Y="2.460312255859" />
                  <Point X="2.017728515625" Y="2.450323486328" />
                  <Point X="2.01460546875" Y="2.430177734375" />
                  <Point X="2.0135859375" Y="2.420020751953" />
                  <Point X="2.01258203125" Y="2.398350585938" />
                  <Point X="2.012544677734" Y="2.390454589844" />
                  <Point X="2.013744750977" Y="2.3668125" />
                  <Point X="2.021782348633" Y="2.300155517578" />
                  <Point X="2.02380065918" Y="2.289033935547" />
                  <Point X="2.029143676758" Y="2.267110595703" />
                  <Point X="2.032468261719" Y="2.256308837891" />
                  <Point X="2.040734375" Y="2.234220214844" />
                  <Point X="2.045318847656" Y="2.223887939453" />
                  <Point X="2.055683349609" Y="2.203840087891" />
                  <Point X="2.063176269531" Y="2.191600341797" />
                  <Point X="2.104421142578" Y="2.130815917969" />
                  <Point X="2.110613525391" Y="2.122671875" />
                  <Point X="2.123837158203" Y="2.107097900391" />
                  <Point X="2.130868408203" Y="2.09966796875" />
                  <Point X="2.146692626953" Y="2.084654785156" />
                  <Point X="2.152516601563" Y="2.079563476562" />
                  <Point X="2.170782470703" Y="2.065267578125" />
                  <Point X="2.231567138672" Y="2.024023193359" />
                  <Point X="2.241280761719" Y="2.018244628906" />
                  <Point X="2.261325439453" Y="2.007882080078" />
                  <Point X="2.271656494141" Y="2.003297973633" />
                  <Point X="2.293744628906" Y="1.995032104492" />
                  <Point X="2.304544433594" Y="1.991708251953" />
                  <Point X="2.326460693359" Y="1.986366455078" />
                  <Point X="2.340345214844" Y="1.984014404297" />
                  <Point X="2.407001953125" Y="1.975976806641" />
                  <Point X="2.417377685547" Y="1.975297973633" />
                  <Point X="2.438140380859" Y="1.975078491211" />
                  <Point X="2.44852734375" Y="1.975537719727" />
                  <Point X="2.470631103516" Y="1.977731567383" />
                  <Point X="2.478266845703" Y="1.978803833008" />
                  <Point X="2.500948974609" Y="1.983251953125" />
                  <Point X="2.578034179688" Y="2.003865600586" />
                  <Point X="2.583993652344" Y="2.005670410156" />
                  <Point X="2.604404541016" Y="2.013067260742" />
                  <Point X="2.627655029297" Y="2.02357409668" />
                  <Point X="2.636033935547" Y="2.027872558594" />
                  <Point X="3.940405029297" Y="2.780951660156" />
                  <Point X="4.043951416016" Y="2.637045654297" />
                  <Point X="4.047345458984" Y="2.631436767578" />
                  <Point X="4.136884765625" Y="2.483472412109" />
                  <Point X="3.218180419922" Y="1.778525268555" />
                  <Point X="3.172951416016" Y="1.743819702148" />
                  <Point X="3.167093505859" Y="1.738939331055" />
                  <Point X="3.149143554688" Y="1.722095703125" />
                  <Point X="3.131335449219" Y="1.702482055664" />
                  <Point X="3.126272460938" Y="1.696416870117" />
                  <Point X="3.070794189453" Y="1.624041015625" />
                  <Point X="3.064954833984" Y="1.615496459961" />
                  <Point X="3.054239746094" Y="1.597822143555" />
                  <Point X="3.049364013672" Y="1.588692382813" />
                  <Point X="3.040036376953" Y="1.568671875" />
                  <Point X="3.037051269531" Y="1.561515014648" />
                  <Point X="3.029281982422" Y="1.539603515625" />
                  <Point X="3.008616210938" Y="1.465707641602" />
                  <Point X="3.006225097656" Y="1.454661010742" />
                  <Point X="3.002771728516" Y="1.432362426758" />
                  <Point X="3.001709472656" Y="1.421110717773" />
                  <Point X="3.000893310547" Y="1.397540527344" />
                  <Point X="3.001174804688" Y="1.386243041992" />
                  <Point X="3.003077148438" Y="1.363761108398" />
                  <Point X="3.005402587891" Y="1.349161987305" />
                  <Point X="3.0223671875" Y="1.266943237305" />
                  <Point X="3.025017822266" Y="1.256903320312" />
                  <Point X="3.031399658203" Y="1.23717175293" />
                  <Point X="3.035130615234" Y="1.22748046875" />
                  <Point X="3.044244628906" Y="1.207190429688" />
                  <Point X="3.047626953125" Y="1.200400024414" />
                  <Point X="3.058834960938" Y="1.180612792969" />
                  <Point X="3.1049765625" Y="1.110479980469" />
                  <Point X="3.111739501953" Y="1.101423950195" />
                  <Point X="3.12629296875" Y="1.084179321289" />
                  <Point X="3.134083496094" Y="1.075990722656" />
                  <Point X="3.151327392578" Y="1.059901000977" />
                  <Point X="3.160034912109" Y="1.052695678711" />
                  <Point X="3.178244384766" Y="1.039370117188" />
                  <Point X="3.190522949219" Y="1.031686889648" />
                  <Point X="3.257388427734" Y="0.994047485352" />
                  <Point X="3.266857666016" Y="0.989389831543" />
                  <Point X="3.286254882812" Y="0.981152954102" />
                  <Point X="3.296182861328" Y="0.977573974609" />
                  <Point X="3.317912353516" Y="0.971079772949" />
                  <Point X="3.325228759766" Y="0.969206359863" />
                  <Point X="3.347425048828" Y="0.964761535645" />
                  <Point X="3.437831542969" Y="0.952812988281" />
                  <Point X="3.444029785156" Y="0.952199768066" />
                  <Point X="3.465716064453" Y="0.951222839355" />
                  <Point X="3.491217529297" Y="0.952032226562" />
                  <Point X="3.500603515625" Y="0.952797241211" />
                  <Point X="4.704704101562" Y="1.11132019043" />
                  <Point X="4.752684570312" Y="0.914230407715" />
                  <Point X="4.753753417969" Y="0.907364379883" />
                  <Point X="4.78387109375" Y="0.713920898438" />
                  <Point X="3.743825439453" Y="0.435241577148" />
                  <Point X="3.691991943359" Y="0.421352905273" />
                  <Point X="3.684587890625" Y="0.41904107666" />
                  <Point X="3.661301757812" Y="0.410218078613" />
                  <Point X="3.637173828125" Y="0.398783782959" />
                  <Point X="3.630315917969" Y="0.395184570312" />
                  <Point X="3.541494384766" Y="0.343843963623" />
                  <Point X="3.532719238281" Y="0.338103271484" />
                  <Point X="3.515854736328" Y="0.325689544678" />
                  <Point X="3.507765380859" Y="0.31901651001" />
                  <Point X="3.491240478516" Y="0.303776428223" />
                  <Point X="3.485851318359" Y="0.298390472412" />
                  <Point X="3.470584228516" Y="0.281406738281" />
                  <Point X="3.417291259766" Y="0.213499008179" />
                  <Point X="3.410854980469" Y="0.204208892822" />
                  <Point X="3.399130859375" Y="0.184929077148" />
                  <Point X="3.393843017578" Y="0.17493939209" />
                  <Point X="3.384069091797" Y="0.153475570679" />
                  <Point X="3.380005615234" Y="0.142930480957" />
                  <Point X="3.373159667969" Y="0.12143157959" />
                  <Point X="3.369639648438" Y="0.106626502991" />
                  <Point X="3.351875244141" Y="0.013867977142" />
                  <Point X="3.350470458984" Y="0.003431527615" />
                  <Point X="3.348824707031" Y="-0.017531288147" />
                  <Point X="3.348583496094" Y="-0.028057950974" />
                  <Point X="3.349321289062" Y="-0.050719230652" />
                  <Point X="3.349854248047" Y="-0.058139823914" />
                  <Point X="3.352611816406" Y="-0.080274383545" />
                  <Point X="3.370376220703" Y="-0.173033065796" />
                  <Point X="3.373158935547" Y="-0.18398789978" />
                  <Point X="3.380005371094" Y="-0.205489334106" />
                  <Point X="3.384069091797" Y="-0.216035766602" />
                  <Point X="3.393843017578" Y="-0.237499588013" />
                  <Point X="3.399129882813" Y="-0.247487792969" />
                  <Point X="3.410852050781" Y="-0.266765075684" />
                  <Point X="3.419501464844" Y="-0.278875762939" />
                  <Point X="3.472794433594" Y="-0.346783630371" />
                  <Point X="3.479868896484" Y="-0.354821014404" />
                  <Point X="3.494879638672" Y="-0.370050842285" />
                  <Point X="3.502814453125" Y="-0.377241668701" />
                  <Point X="3.520814941406" Y="-0.391793945312" />
                  <Point X="3.526743896484" Y="-0.396216796875" />
                  <Point X="3.545182861328" Y="-0.408536010742" />
                  <Point X="3.634004394531" Y="-0.459876617432" />
                  <Point X="3.639495605469" Y="-0.462814880371" />
                  <Point X="3.659158447266" Y="-0.472016876221" />
                  <Point X="3.683028076172" Y="-0.481027893066" />
                  <Point X="3.691991943359" Y="-0.48391293335" />
                  <Point X="4.784876953125" Y="-0.776750305176" />
                  <Point X="4.761611328125" Y="-0.931062438965" />
                  <Point X="4.7602421875" Y="-0.937063720703" />
                  <Point X="4.727802734375" Y="-1.079219604492" />
                  <Point X="3.496894287109" Y="-0.917167602539" />
                  <Point X="3.436781982422" Y="-0.90925390625" />
                  <Point X="3.426159667969" Y="-0.908457702637" />
                  <Point X="3.404891357422" Y="-0.908059692383" />
                  <Point X="3.394245117188" Y="-0.908458007812" />
                  <Point X="3.369860107422" Y="-0.910744384766" />
                  <Point X="3.3472421875" Y="-0.914249694824" />
                  <Point X="3.172916992188" Y="-0.952139892578" />
                  <Point X="3.161470947266" Y="-0.955390319824" />
                  <Point X="3.131101074219" Y="-0.966111206055" />
                  <Point X="3.111470703125" Y="-0.975705688477" />
                  <Point X="3.08125" Y="-0.995070373535" />
                  <Point X="3.0670625" Y="-1.006192871094" />
                  <Point X="3.050187011719" Y="-1.022229309082" />
                  <Point X="3.034974121094" Y="-1.038485351563" />
                  <Point X="2.92960546875" Y="-1.16521105957" />
                  <Point X="2.923182861328" Y="-1.173896118164" />
                  <Point X="2.907155761719" Y="-1.198365356445" />
                  <Point X="2.898025634766" Y="-1.21614050293" />
                  <Point X="2.886155761719" Y="-1.246822143555" />
                  <Point X="2.881558837891" Y="-1.262680664063" />
                  <Point X="2.8773359375" Y="-1.284048461914" />
                  <Point X="2.874530029297" Y="-1.303475341797" />
                  <Point X="2.859428222656" Y="-1.467590576172" />
                  <Point X="2.85908203125" Y="-1.479483154297" />
                  <Point X="2.860162597656" Y="-1.511670288086" />
                  <Point X="2.863436523438" Y="-1.533405639648" />
                  <Point X="2.872921142578" Y="-1.568292236328" />
                  <Point X="2.879338623047" Y="-1.585280761719" />
                  <Point X="2.889856201172" Y="-1.606675292969" />
                  <Point X="2.900546386719" Y="-1.625602416992" />
                  <Point X="2.997020507812" Y="-1.775661865234" />
                  <Point X="3.001741699219" Y="-1.782352783203" />
                  <Point X="3.01979296875" Y="-1.80444921875" />
                  <Point X="3.043489013672" Y="-1.828120117188" />
                  <Point X="3.052796142578" Y="-1.83627746582" />
                  <Point X="4.087170654297" Y="-2.629981933594" />
                  <Point X="4.045490234375" Y="-2.697427001953" />
                  <Point X="4.042650878906" Y="-2.701461425781" />
                  <Point X="4.001274414063" Y="-2.760251953125" />
                  <Point X="2.901992675781" Y="-2.125581054688" />
                  <Point X="2.848454589844" Y="-2.094670654297" />
                  <Point X="2.838671142578" Y="-2.089750976563" />
                  <Point X="2.818604003906" Y="-2.081070556641" />
                  <Point X="2.8083203125" Y="-2.077309814453" />
                  <Point X="2.783591064453" Y="-2.069851074219" />
                  <Point X="2.762492919922" Y="-2.064781738281" />
                  <Point X="2.555018066406" Y="-2.027311889648" />
                  <Point X="2.543199707031" Y="-2.025934570313" />
                  <Point X="2.511038818359" Y="-2.024217407227" />
                  <Point X="2.488953369141" Y="-2.025619384766" />
                  <Point X="2.453097412109" Y="-2.032149047852" />
                  <Point X="2.435485595703" Y="-2.037149169922" />
                  <Point X="2.412656494141" Y="-2.046086425781" />
                  <Point X="2.393431396484" Y="-2.054875488281" />
                  <Point X="2.221070800781" Y="-2.145587646484" />
                  <Point X="2.2118125" Y="-2.151154296875" />
                  <Point X="2.187640625" Y="-2.167628662109" />
                  <Point X="2.172221679688" Y="-2.180748046875" />
                  <Point X="2.149222167969" Y="-2.204992675781" />
                  <Point X="2.138796630859" Y="-2.218134521484" />
                  <Point X="2.126138671875" Y="-2.237360595703" />
                  <Point X="2.116696777344" Y="-2.253352294922" />
                  <Point X="2.025984741211" Y="-2.425712890625" />
                  <Point X="2.02111328125" Y="-2.436568847656" />
                  <Point X="2.009794311523" Y="-2.466720458984" />
                  <Point X="2.004465332031" Y="-2.488341064453" />
                  <Point X="1.999916748047" Y="-2.524778320312" />
                  <Point X="1.999419677734" Y="-2.543218994141" />
                  <Point X="2.001188232422" Y="-2.568335693359" />
                  <Point X="2.003743774414" Y="-2.588757568359" />
                  <Point X="2.041213378906" Y="-2.796232421875" />
                  <Point X="2.043015014648" Y="-2.804221435547" />
                  <Point X="2.051236083984" Y="-2.83154296875" />
                  <Point X="2.064069580078" Y="-2.862479736328" />
                  <Point X="2.069546630859" Y="-2.873578613281" />
                  <Point X="2.735893310547" Y="-4.027724853516" />
                  <Point X="2.723754394531" Y="-4.036083496094" />
                  <Point X="1.874769165039" Y="-2.929665527344" />
                  <Point X="1.833914550781" Y="-2.876422851562" />
                  <Point X="1.826572387695" Y="-2.867942138672" />
                  <Point X="1.810938476562" Y="-2.851904296875" />
                  <Point X="1.802646728516" Y="-2.844347167969" />
                  <Point X="1.781521240234" Y="-2.827264160156" />
                  <Point X="1.764801757812" Y="-2.815184082031" />
                  <Point X="1.56017565918" Y="-2.683628417969" />
                  <Point X="1.549783569336" Y="-2.677833251953" />
                  <Point X="1.520728149414" Y="-2.663939453125" />
                  <Point X="1.499538574219" Y="-2.656729492188" />
                  <Point X="1.463402587891" Y="-2.649031738281" />
                  <Point X="1.444956787109" Y="-2.646956542969" />
                  <Point X="1.419206176758" Y="-2.646591308594" />
                  <Point X="1.399101196289" Y="-2.647371582031" />
                  <Point X="1.175307739258" Y="-2.667965332031" />
                  <Point X="1.16462512207" Y="-2.669564941406" />
                  <Point X="1.135990112305" Y="-2.675534423828" />
                  <Point X="1.116506103516" Y="-2.681851806641" />
                  <Point X="1.085584838867" Y="-2.695718017578" />
                  <Point X="1.070747314453" Y="-2.704041015625" />
                  <Point X="1.050738769531" Y="-2.717755126953" />
                  <Point X="1.036683105469" Y="-2.728379638672" />
                  <Point X="0.863875244141" Y="-2.872063476562" />
                  <Point X="0.855220458984" Y="-2.880228759766" />
                  <Point X="0.833224243164" Y="-2.903753417969" />
                  <Point X="0.819898010254" Y="-2.921916503906" />
                  <Point X="0.801587219238" Y="-2.9543359375" />
                  <Point X="0.794035827637" Y="-2.971451660156" />
                  <Point X="0.785772094727" Y="-2.996649658203" />
                  <Point X="0.780646240234" Y="-3.015502685547" />
                  <Point X="0.728977661133" Y="-3.253219238281" />
                  <Point X="0.727584594727" Y="-3.261289794922" />
                  <Point X="0.72472467041" Y="-3.289677734375" />
                  <Point X="0.72474230957" Y="-3.323170410156" />
                  <Point X="0.725555175781" Y="-3.335520019531" />
                  <Point X="0.833091674805" Y="-4.152340820313" />
                  <Point X="0.671036254883" Y="-3.547541748047" />
                  <Point X="0.655064941406" Y="-3.487936523438" />
                  <Point X="0.651436279297" Y="-3.477065185547" />
                  <Point X="0.642895996094" Y="-3.455838378906" />
                  <Point X="0.637984436035" Y="-3.445482666016" />
                  <Point X="0.624236206055" Y="-3.420509033203" />
                  <Point X="0.613879577637" Y="-3.4038046875" />
                  <Point X="0.45667980957" Y="-3.177309814453" />
                  <Point X="0.449297119141" Y="-3.167979492188" />
                  <Point X="0.427770874023" Y="-3.144024414062" />
                  <Point X="0.410761871338" Y="-3.129122802734" />
                  <Point X="0.379896148682" Y="-3.107999755859" />
                  <Point X="0.363413726807" Y="-3.098972167969" />
                  <Point X="0.338574676514" Y="-3.088411621094" />
                  <Point X="0.320553100586" Y="-3.081803222656" />
                  <Point X="0.077295310974" Y="-3.006305175781" />
                  <Point X="0.066813682556" Y="-3.003695068359" />
                  <Point X="0.038074615479" Y="-2.998252197266" />
                  <Point X="0.01744219017" Y="-2.996638916016" />
                  <Point X="-0.016852249146" Y="-2.997706054688" />
                  <Point X="-0.033951015472" Y="-2.99980078125" />
                  <Point X="-0.05887329483" Y="-3.005182861328" />
                  <Point X="-0.075085083008" Y="-3.009440185547" />
                  <Point X="-0.31834274292" Y="-3.084938476562" />
                  <Point X="-0.329463897705" Y="-3.089170410156" />
                  <Point X="-0.358788665771" Y="-3.102487304688" />
                  <Point X="-0.378235992432" Y="-3.114313232422" />
                  <Point X="-0.407864562988" Y="-3.137615234375" />
                  <Point X="-0.42151171875" Y="-3.150750732422" />
                  <Point X="-0.439557189941" Y="-3.171974365234" />
                  <Point X="-0.450895629883" Y="-3.186715087891" />
                  <Point X="-0.608095581055" Y="-3.413210205078" />
                  <Point X="-0.612470275879" Y="-3.420132324219" />
                  <Point X="-0.62597668457" Y="-3.445265136719" />
                  <Point X="-0.638777709961" Y="-3.476215820312" />
                  <Point X="-0.642752929688" Y="-3.487936523438" />
                  <Point X="-0.985425292969" Y="-4.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.667030413155" Y="-3.962159439381" />
                  <Point X="2.686939161398" Y="-3.942933784682" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.94517929854" Y="-2.727865408232" />
                  <Point X="4.064530374315" Y="-2.612609414096" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.608823883697" Y="-3.886303290353" />
                  <Point X="2.637985012249" Y="-3.858142715849" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.859591368015" Y="-2.678451170853" />
                  <Point X="3.988324796096" Y="-2.554134744412" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.550617354239" Y="-3.810447141324" />
                  <Point X="2.5890308631" Y="-3.773351647016" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.774003437489" Y="-2.629036933474" />
                  <Point X="3.912119217878" Y="-2.495660074729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.492410824781" Y="-3.734590992296" />
                  <Point X="2.540076713951" Y="-3.688560578183" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.688415506964" Y="-2.579622696095" />
                  <Point X="3.835913639659" Y="-2.437185405046" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.434204295322" Y="-3.658734843268" />
                  <Point X="2.491122564802" Y="-3.60376950935" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.602827576438" Y="-2.530208458716" />
                  <Point X="3.75970806144" Y="-2.378710735363" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.375997765864" Y="-3.582878694239" />
                  <Point X="2.442168415653" Y="-3.518978440516" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.517239645913" Y="-2.480794221337" />
                  <Point X="3.683502483222" Y="-2.32023606568" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.317791236406" Y="-3.507022545211" />
                  <Point X="2.393214266504" Y="-3.434187371683" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.431651715387" Y="-2.431379983957" />
                  <Point X="3.607296905003" Y="-2.261761395997" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.259584706948" Y="-3.431166396183" />
                  <Point X="2.344260117355" Y="-3.34939630285" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.346063784862" Y="-2.381965746578" />
                  <Point X="3.531091326784" Y="-2.203286726314" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.699020753847" Y="-1.075430388832" />
                  <Point X="4.737047174412" Y="-1.038708701346" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.201378177489" Y="-3.355310247155" />
                  <Point X="2.295305968206" Y="-3.264605234017" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.260475854336" Y="-2.332551509199" />
                  <Point X="3.454885748566" Y="-2.14481205663" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.578670265777" Y="-1.059585963058" />
                  <Point X="4.770106327208" Y="-0.874718307441" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.143171648031" Y="-3.279454098126" />
                  <Point X="2.246351819057" Y="-3.179814165184" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.174887923811" Y="-2.28313727182" />
                  <Point X="3.378680170347" Y="-2.086337386947" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.458319777707" Y="-1.043741537283" />
                  <Point X="4.745674709033" Y="-0.766246105716" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.084965118573" Y="-3.203597949098" />
                  <Point X="2.197397669908" Y="-3.09502309635" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.089299993285" Y="-2.233723034441" />
                  <Point X="3.302474592128" Y="-2.027862717264" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.337969289636" Y="-1.027897111509" />
                  <Point X="4.638620960308" Y="-0.737561168014" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.026758589115" Y="-3.12774180007" />
                  <Point X="2.148443520759" Y="-3.010232027517" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.00371206276" Y="-2.184308797062" />
                  <Point X="3.22626901391" Y="-1.969388047581" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.217618801566" Y="-1.012052685734" />
                  <Point X="4.531567211584" Y="-0.708876230311" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.832250031446" Y="-4.149199764475" />
                  <Point X="0.832629857626" Y="-4.148832970596" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.968552059656" Y="-3.051885651041" />
                  <Point X="2.09948937161" Y="-2.925440958684" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.918124132235" Y="-2.134894559682" />
                  <Point X="3.150063435691" Y="-1.910913377898" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.097268313495" Y="-0.99620825996" />
                  <Point X="4.424513462859" Y="-0.680191292609" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.804137460104" Y="-4.044282217904" />
                  <Point X="0.817204240304" Y="-4.031663774942" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.910345530198" Y="-2.976029502013" />
                  <Point X="2.053732920316" Y="-2.837561908928" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.831379855686" Y="-2.086596992677" />
                  <Point X="3.073857857472" Y="-1.852438708215" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.976917825425" Y="-0.980363834185" />
                  <Point X="4.317459714134" Y="-0.651506354907" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.776024888762" Y="-3.939364671334" />
                  <Point X="0.801778622981" Y="-3.914494579289" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.852138990962" Y="-2.900173362428" />
                  <Point X="2.029119266541" Y="-2.729265496939" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.724346366172" Y="-2.057892490883" />
                  <Point X="3.005240745518" Y="-1.786635941842" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.856567337355" Y="-0.964519408411" />
                  <Point X="4.210405965409" Y="-0.622821417205" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.747912317419" Y="-3.834447124763" />
                  <Point X="0.786353005658" Y="-3.797325383635" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.786615227332" Y="-2.831383384301" />
                  <Point X="2.008810363332" Y="-2.616812035649" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.609134907172" Y="-2.037085362422" />
                  <Point X="2.952138471349" Y="-1.705850670778" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.736216849284" Y="-0.948674982636" />
                  <Point X="4.103352216685" Y="-0.594136479503" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.719799746077" Y="-3.729529578193" />
                  <Point X="0.770927388336" Y="-3.680156187981" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.70586763328" Y="-2.777294888323" />
                  <Point X="2.004379969553" Y="-2.489024876043" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.48315740979" Y="-2.026674876376" />
                  <Point X="2.899817131444" Y="-1.62431126026" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.615866361214" Y="-0.932830556862" />
                  <Point X="3.99629846796" Y="-0.565451541801" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.691687174735" Y="-3.624612031622" />
                  <Point X="0.755501771013" Y="-3.562986992328" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.623767768336" Y="-2.724512265167" />
                  <Point X="2.123367096275" Y="-2.242054802274" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.225886014635" Y="-2.143053433609" />
                  <Point X="2.862643902667" Y="-1.528143488867" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.495515868327" Y="-0.916986135739" />
                  <Point X="3.889244719235" Y="-0.536766604099" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.663574550696" Y="-3.51969453594" />
                  <Point X="0.740076153691" Y="-3.445817796674" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.540128404181" Y="-2.673216319117" />
                  <Point X="2.866336953824" Y="-1.392511609673" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.364334783396" Y="-0.911600695777" />
                  <Point X="3.782190970511" Y="-0.508081666397" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.626072372988" Y="-3.423844426838" />
                  <Point X="0.725075856192" Y="-3.32823787444" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.430771670596" Y="-2.646755348042" />
                  <Point X="2.889561804831" Y="-1.238018090612" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.189280346068" Y="-0.948583259738" />
                  <Point X="3.676140580441" Y="-0.478427796504" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.57222096957" Y="-3.343782581479" />
                  <Point X="0.745707524431" Y="-3.17624856287" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.282239863121" Y="-2.658125306076" />
                  <Point X="3.586829739584" Y="-0.432608731841" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.770342023991" Y="0.710295796056" />
                  <Point X="4.782593483013" Y="0.722126892509" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.029302489253" Y="-4.758290267109" />
                  <Point X="-0.967029971417" Y="-4.698154395655" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.517342244474" Y="-3.264712809134" />
                  <Point X="0.782511638452" Y="-3.008641701946" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.123139760815" Y="-2.679700947797" />
                  <Point X="3.505313790489" Y="-0.379262227704" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.581065779096" Y="0.65957939217" />
                  <Point X="4.764719278279" Y="0.836931514785" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.138793732029" Y="-4.73195919005" />
                  <Point X="-0.919290154063" Y="-4.519987048779" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.462463519377" Y="-3.185643036789" />
                  <Point X="3.442354420971" Y="-0.30799584297" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.3917895342" Y="0.608862988283" />
                  <Point X="4.744180433363" Y="0.949162923949" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.121978297515" Y="-4.58365517255" />
                  <Point X="-0.87155033671" Y="-4.341819701902" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.395236441708" Y="-3.118497929911" />
                  <Point X="3.389186570993" Y="-0.227273897727" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.202513289305" Y="0.558146584397" />
                  <Point X="4.718149446206" Y="1.056090633001" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.130808172138" Y="-4.46011654221" />
                  <Point X="-0.823810519356" Y="-4.163652355026" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.302332850488" Y="-3.076148343944" />
                  <Point X="3.360733915336" Y="-0.122684766762" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.01323704441" Y="0.507430180511" />
                  <Point X="4.628146314561" Y="1.10124116022" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.152844689847" Y="-4.349331418851" />
                  <Point X="-0.776070702002" Y="-3.985485008149" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.198837352667" Y="-3.044027243287" />
                  <Point X="3.350133888217" Y="-0.000855552818" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.823960799515" Y="0.456713776624" />
                  <Point X="4.469801218707" Y="1.080394619755" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.174881207556" Y="-4.238546295492" />
                  <Point X="-0.728330884648" Y="-3.807317661273" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.095341854845" Y="-3.011906142629" />
                  <Point X="3.392607596242" Y="0.172226371393" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.613306407717" Y="0.385352736248" />
                  <Point X="4.311456122853" Y="1.059548079289" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.204674137985" Y="-4.135251452829" />
                  <Point X="-0.680591067295" Y="-3.629150314397" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.028143849541" Y="-2.999089360058" />
                  <Point X="4.153111026999" Y="1.038701538824" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.262558249753" Y="-4.059083948656" />
                  <Point X="-0.624947926199" Y="-3.443350816499" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.223234015201" Y="-3.055420201964" />
                  <Point X="3.994765931145" Y="1.017854998359" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.333868055564" Y="-3.995881486515" />
                  <Point X="3.836420835291" Y="0.997008457893" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.405538896838" Y="-3.933027672267" />
                  <Point X="3.678075739437" Y="0.976161917428" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.477209738111" Y="-3.87017385802" />
                  <Point X="3.519730643583" Y="0.955315376963" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.558498486391" Y="-3.816607948605" />
                  <Point X="3.387297579315" Y="0.959491794532" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.671722544724" Y="-3.793881609629" />
                  <Point X="3.27702788311" Y="0.985071127853" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.799788344206" Y="-3.785487773479" />
                  <Point X="3.189320927503" Y="1.032439046497" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.927854143688" Y="-3.777093937329" />
                  <Point X="3.117244407844" Y="1.094901101682" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.125522535444" Y="-3.835914543235" />
                  <Point X="3.062878793663" Y="1.174466379478" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.562456066506" Y="-4.125790808371" />
                  <Point X="3.022286825199" Y="1.267332712332" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.645785377918" Y="-4.074195447868" />
                  <Point X="3.001740558392" Y="1.379556954258" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.729114689331" Y="-4.022600087365" />
                  <Point X="3.02875150828" Y="1.537706666509" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.510017823856" Y="2.002460145154" />
                  <Point X="4.089391720901" Y="2.561955013946" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.806808958659" Y="-3.965563029976" />
                  <Point X="4.038392545652" Y="2.64477122403" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.882898738747" Y="-3.906976535338" />
                  <Point X="3.982324761194" Y="2.722692735097" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.958988518835" Y="-3.8483900407" />
                  <Point X="3.854590185769" Y="2.731406430601" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.858075608606" Y="-3.618874034912" />
                  <Point X="3.514511709985" Y="2.53506200513" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.68574760592" Y="-3.320393275986" />
                  <Point X="3.1744332342" Y="2.338717579658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.513419603233" Y="-3.02191251706" />
                  <Point X="2.834354758416" Y="2.142373154186" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.341223809566" Y="-2.7235594309" />
                  <Point X="2.545028286762" Y="1.995039369402" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.313770016078" Y="-2.564982069556" />
                  <Point X="2.390580979118" Y="1.977956879257" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.352465383755" Y="-2.470284210613" />
                  <Point X="2.277716295065" Y="2.001030261941" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.417367194086" Y="-2.400893619167" />
                  <Point X="2.192334657834" Y="2.050643714439" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.507563400663" Y="-2.355929542242" />
                  <Point X="2.119421757176" Y="2.112298085882" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.65398585874" Y="-2.36526252524" />
                  <Point X="2.063812626567" Y="2.190662513822" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.980633209919" Y="-2.548636664447" />
                  <Point X="2.024776220545" Y="2.285031035866" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.320711528766" Y="-2.744980938366" />
                  <Point X="2.012920059674" Y="2.405647215546" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.660789847613" Y="-2.941325212286" />
                  <Point X="2.056147633855" Y="2.579457139841" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.828487266071" Y="-2.971203185708" />
                  <Point X="2.224203599462" Y="2.873812440514" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.886423842898" Y="-2.895086346454" />
                  <Point X="2.396531401038" Y="3.17229300523" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.944360419724" Y="-2.8189695072" />
                  <Point X="2.568859202613" Y="3.470773569945" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.002296996551" Y="-2.742852667946" />
                  <Point X="2.741187004189" Y="3.769254134661" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.052509911459" Y="-2.659277175076" />
                  <Point X="2.77402599811" Y="3.933031923612" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.102486768571" Y="-2.575473723842" />
                  <Point X="-3.131787061265" Y="-1.638079912788" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.985108362042" Y="-1.496433939445" />
                  <Point X="2.695267003915" Y="3.98904078815" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.152463625682" Y="-2.491670272608" />
                  <Point X="-3.797570275175" Y="-2.14895374777" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.950143259642" Y="-1.330602991402" />
                  <Point X="2.615435600123" Y="4.044014038777" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.983007268015" Y="-1.230273854236" />
                  <Point X="2.530137844613" Y="4.093708494912" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.047143742551" Y="-1.160144186604" />
                  <Point X="2.444840089104" Y="4.143402951047" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.13519777627" Y="-1.113111437397" />
                  <Point X="2.359542333594" Y="4.193097407182" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.260831814495" Y="-1.102369276697" />
                  <Point X="2.274244578085" Y="4.242791863316" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.41917687218" Y="-1.123215780304" />
                  <Point X="2.184015084812" Y="4.287723795653" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.577521929866" Y="-1.144062283911" />
                  <Point X="2.091877749217" Y="4.330813346075" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.735866987552" Y="-1.164908787518" />
                  <Point X="1.999740413622" Y="4.373902896497" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.894212045238" Y="-1.185755291125" />
                  <Point X="1.907603078027" Y="4.416992446919" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.052557102924" Y="-1.206601794732" />
                  <Point X="1.812085376043" Y="4.456817615464" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.21090216061" Y="-1.227448298338" />
                  <Point X="1.71266800257" Y="4.492876915027" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.369247218296" Y="-1.248294801945" />
                  <Point X="1.613250629097" Y="4.52893621459" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.527592275982" Y="-1.269141305552" />
                  <Point X="-3.547501338635" Y="-0.322678489067" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.300332798066" Y="-0.083990603953" />
                  <Point X="1.513833255624" Y="4.564995514153" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.665195861212" Y="-1.269958002036" />
                  <Point X="-3.736777585718" Y="-0.373394895065" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.305259549372" Y="0.043317228761" />
                  <Point X="1.410479830351" Y="4.597253812675" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.692254841478" Y="-1.164023014389" />
                  <Point X="-3.926053802732" Y="-0.424111272027" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.34799421088" Y="0.134114386994" />
                  <Point X="1.301074284613" Y="4.623667646401" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.719313821743" Y="-1.058088026743" />
                  <Point X="-4.115330019746" Y="-0.474827648989" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.414639731251" Y="0.201821097228" />
                  <Point X="1.191668738875" Y="4.650081480126" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.744202938347" Y="-0.950057626115" />
                  <Point X="-4.304606236761" Y="-0.525544025951" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.50356740154" Y="0.248010185406" />
                  <Point X="1.082263193137" Y="4.676495313852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.760799229361" Y="-0.834018936905" />
                  <Point X="-4.493882453775" Y="-0.576260402914" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.610348637996" Y="0.276958285147" />
                  <Point X="0.972857647399" Y="4.702909147578" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.777395520375" Y="-0.717980247694" />
                  <Point X="-4.68315867079" Y="-0.626976779876" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.717402371985" Y="0.305643237079" />
                  <Point X="0.863452101661" Y="4.729322981304" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.824456105974" Y="0.334328189011" />
                  <Point X="0.069532805053" Y="4.094709569613" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.235598816322" Y="4.255077652572" />
                  <Point X="0.744617514833" Y="4.746631295891" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.931509839963" Y="0.363013140943" />
                  <Point X="-0.064623019039" Y="4.097222337359" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.284144696454" Y="4.434023405226" />
                  <Point X="0.621239784126" Y="4.759552347434" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.038563573952" Y="0.391698092876" />
                  <Point X="-0.152224416716" Y="4.144692192112" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.331884391863" Y="4.612190634341" />
                  <Point X="0.49786205342" Y="4.772473398976" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.145617307941" Y="0.420383044808" />
                  <Point X="-0.209018927271" Y="4.221911911944" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.25267104193" Y="0.44906799674" />
                  <Point X="-0.241881261697" Y="4.322242665623" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.359724775919" Y="0.477752948672" />
                  <Point X="-0.269993825592" Y="4.427160219384" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.466778509908" Y="0.506437900604" />
                  <Point X="-3.743949190732" Y="1.204466060235" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.421442521674" Y="1.515907130344" />
                  <Point X="-0.298106389488" Y="4.532077773146" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.573832243898" Y="0.535122852536" />
                  <Point X="-3.867354240541" Y="1.217360730026" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.444426687843" Y="1.625777120223" />
                  <Point X="-0.326218953383" Y="4.636995326907" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.680885977887" Y="0.563807804469" />
                  <Point X="-3.987704738201" Y="1.23320514654" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.491531431864" Y="1.712354138829" />
                  <Point X="-0.354331517279" Y="4.741912880669" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.784915234157" Y="0.595413460583" />
                  <Point X="-4.108055235861" Y="1.249049563054" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.557863327518" Y="1.78036371293" />
                  <Point X="-0.460554762243" Y="4.77139982653" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.762114684541" Y="0.749497236554" />
                  <Point X="-4.228405733521" Y="1.264893979567" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.634068936307" Y="1.838838353092" />
                  <Point X="-0.616172540217" Y="4.753187026327" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.739314134924" Y="0.903581012525" />
                  <Point X="-4.348756231182" Y="1.280738396081" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.710274545095" Y="1.897312993254" />
                  <Point X="-0.771790318191" Y="4.734974226123" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.69983060416" Y="1.073775356119" />
                  <Point X="-4.469106728842" Y="1.296582812594" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.786480153884" Y="1.955787633416" />
                  <Point X="-2.981722303068" Y="2.732933256387" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.761391679837" Y="2.945704065988" />
                  <Point X="-0.927407980314" Y="4.716761537796" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.651359455542" Y="1.252648941389" />
                  <Point X="-4.589457226502" Y="1.312427229108" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.862685762673" Y="2.014262273578" />
                  <Point X="-3.123205863338" Y="2.728369711561" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.752470302734" Y="3.086384880858" />
                  <Point X="-1.766208316839" Y="4.038807009656" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.480480606165" Y="4.314731052506" />
                  <Point X="-1.119214864308" Y="4.663601324139" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.938891371462" Y="2.07273691374" />
                  <Point X="-3.234377318798" Y="2.753078226091" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.773490135916" Y="3.198151805053" />
                  <Point X="-1.902025199334" Y="4.039715711948" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.465870556334" Y="4.460905354773" />
                  <Point X="-1.311920383607" Y="4.609573308456" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.01509698025" Y="2.131211553902" />
                  <Point X="-3.32132597665" Y="2.801178424365" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.820006617486" Y="3.285296902104" />
                  <Point X="-1.994839848808" Y="4.08215118796" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.091302589039" Y="2.189686194064" />
                  <Point X="-3.406913940154" Y="2.850592629898" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.86896077735" Y="3.37008796059" />
                  <Point X="-2.079062959818" Y="4.132883416225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.167508197828" Y="2.248160834226" />
                  <Point X="-3.492501903658" Y="2.90000683543" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.917914937214" Y="3.454879019075" />
                  <Point X="-2.142758335097" Y="4.203439048457" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.190292973411" Y="2.358223373355" />
                  <Point X="-3.578089867162" Y="2.949421040962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.966869097078" Y="3.539670077561" />
                  <Point X="-2.239217378716" Y="4.242355173952" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.013628726186" Y="2.660891594957" />
                  <Point X="-3.663677830665" Y="2.998835246495" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.015823256942" Y="3.624461136047" />
                  <Point X="-2.561242050929" Y="4.063445103932" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.0001640625" Y="0.001626220703" />
                  <Width Value="9.99646484375" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.487510314941" Y="-3.596717529297" />
                  <Point X="0.471539031982" Y="-3.537112304688" />
                  <Point X="0.457790893555" Y="-3.512138671875" />
                  <Point X="0.300591033936" Y="-3.285643798828" />
                  <Point X="0.289073364258" Y="-3.273825195312" />
                  <Point X="0.264234313965" Y="-3.263264648438" />
                  <Point X="0.020976625443" Y="-3.187766601562" />
                  <Point X="0.006155941963" Y="-3.18551953125" />
                  <Point X="-0.018766191483" Y="-3.190901611328" />
                  <Point X="-0.262023895264" Y="-3.266399902344" />
                  <Point X="-0.276761444092" Y="-3.273825439453" />
                  <Point X="-0.294806915283" Y="-3.295049072266" />
                  <Point X="-0.45200680542" Y="-3.521544189453" />
                  <Point X="-0.459227142334" Y="-3.537112304688" />
                  <Point X="-0.843837219238" Y="-4.972496582031" />
                  <Point X="-0.84774395752" Y="-4.987076660156" />
                  <Point X="-1.10024609375" Y="-4.938065429688" />
                  <Point X="-1.110576660156" Y="-4.935407226562" />
                  <Point X="-1.35158972168" Y="-4.873396972656" />
                  <Point X="-1.311072998047" Y="-4.565642089844" />
                  <Point X="-1.309150146484" Y="-4.551038574219" />
                  <Point X="-1.312096435547" Y="-4.522625976562" />
                  <Point X="-1.370210693359" Y="-4.230466308594" />
                  <Point X="-1.375948730469" Y="-4.215220703125" />
                  <Point X="-1.395582885742" Y="-4.19447265625" />
                  <Point X="-1.619543457031" Y="-3.998063964844" />
                  <Point X="-1.633376708984" Y="-3.989461914063" />
                  <Point X="-1.661583984375" Y="-3.984953857422" />
                  <Point X="-1.958829833984" Y="-3.965471435547" />
                  <Point X="-1.975041015625" Y="-3.967068115234" />
                  <Point X="-2.000163696289" Y="-3.980663330078" />
                  <Point X="-2.247844970703" Y="-4.146159179688" />
                  <Point X="-2.259734130859" Y="-4.157294433594" />
                  <Point X="-2.457095214844" Y="-4.414500488281" />
                  <Point X="-2.85583203125" Y="-4.167612304688" />
                  <Point X="-2.870140380859" Y="-4.156595214844" />
                  <Point X="-3.228581054688" Y="-3.880608154297" />
                  <Point X="-2.535714355469" Y="-2.680528076172" />
                  <Point X="-2.506033691406" Y="-2.629119628906" />
                  <Point X="-2.499762207031" Y="-2.59759765625" />
                  <Point X="-2.514671630859" Y="-2.568072021484" />
                  <Point X="-2.531328613281" Y="-2.551415283203" />
                  <Point X="-2.560157470703" Y="-2.537198730469" />
                  <Point X="-2.59168359375" Y="-2.543469726562" />
                  <Point X="-3.829674560547" Y="-3.258224121094" />
                  <Point X="-3.842959228516" Y="-3.265894042969" />
                  <Point X="-4.161703613281" Y="-2.847129150391" />
                  <Point X="-4.171959472656" Y="-2.829932128906" />
                  <Point X="-4.431020019531" Y="-2.395527099609" />
                  <Point X="-3.215718505859" Y="-1.462993286133" />
                  <Point X="-3.163786865234" Y="-1.423144897461" />
                  <Point X="-3.145514892578" Y="-1.394827636719" />
                  <Point X="-3.138117431641" Y="-1.366266357422" />
                  <Point X="-3.140326416016" Y="-1.334595703125" />
                  <Point X="-3.16221484375" Y="-1.310017700195" />
                  <Point X="-3.187641601562" Y="-1.295052734375" />
                  <Point X="-3.219529296875" Y="-1.288571044922" />
                  <Point X="-4.782375488281" Y="-1.494323608398" />
                  <Point X="-4.803283203125" Y="-1.497076293945" />
                  <Point X="-4.927393066406" Y="-1.011192016602" />
                  <Point X="-4.930105957031" Y="-0.992221252441" />
                  <Point X="-4.998396484375" Y="-0.514742126465" />
                  <Point X="-3.616621826172" Y="-0.144496704102" />
                  <Point X="-3.557463867188" Y="-0.128645431519" />
                  <Point X="-3.540789306641" Y="-0.120657043457" />
                  <Point X="-3.514142822266" Y="-0.102163047791" />
                  <Point X="-3.494530029297" Y="-0.074719291687" />
                  <Point X="-3.485647949219" Y="-0.046100738525" />
                  <Point X="-3.486016601562" Y="-0.01527056694" />
                  <Point X="-3.494898925781" Y="0.013347985268" />
                  <Point X="-3.515249267578" Y="0.040370838165" />
                  <Point X="-3.541895751953" Y="0.05886498642" />
                  <Point X="-3.557463867188" Y="0.066085334778" />
                  <Point X="-4.982072265625" Y="0.447807952881" />
                  <Point X="-4.998186523438" Y="0.452125793457" />
                  <Point X="-4.917645019531" Y="0.996418457031" />
                  <Point X="-4.912182128906" Y="1.016579162598" />
                  <Point X="-4.773516113281" Y="1.528298583984" />
                  <Point X="-3.795672363281" Y="1.399563110352" />
                  <Point X="-3.753266357422" Y="1.393980102539" />
                  <Point X="-3.731704833984" Y="1.395866577148" />
                  <Point X="-3.729255859375" Y="1.396638793945" />
                  <Point X="-3.670278808594" Y="1.415234130859" />
                  <Point X="-3.651534423828" Y="1.426056152344" />
                  <Point X="-3.639119873047" Y="1.443785888672" />
                  <Point X="-3.638137207031" Y="1.446158325195" />
                  <Point X="-3.614472412109" Y="1.503290161133" />
                  <Point X="-3.610714111328" Y="1.524605224609" />
                  <Point X="-3.616315917969" Y="1.54551159668" />
                  <Point X="-3.617501708984" Y="1.547789306641" />
                  <Point X="-3.646055664062" Y="1.602641235352" />
                  <Point X="-3.659968017578" Y="1.619221679688" />
                  <Point X="-4.476105957031" Y="2.245466308594" />
                  <Point X="-4.160014648438" Y="2.787006591797" />
                  <Point X="-4.145544921875" Y="2.805605224609" />
                  <Point X="-3.774671386719" Y="3.282310791016" />
                  <Point X="-3.185201660156" Y="2.941980224609" />
                  <Point X="-3.159157226562" Y="2.926943359375" />
                  <Point X="-3.138514892578" Y="2.920434814453" />
                  <Point X="-3.135104003906" Y="2.920136474609" />
                  <Point X="-3.052965576172" Y="2.912950195312" />
                  <Point X="-3.031506835938" Y="2.915775390625" />
                  <Point X="-3.013252441406" Y="2.927404785156" />
                  <Point X="-3.010831298828" Y="2.929825683594" />
                  <Point X="-2.952528808594" Y="2.988128173828" />
                  <Point X="-2.940899414062" Y="3.006382568359" />
                  <Point X="-2.93807421875" Y="3.027841308594" />
                  <Point X="-2.938372558594" Y="3.031252197266" />
                  <Point X="-2.945558837891" Y="3.113390869141" />
                  <Point X="-2.952067382812" Y="3.134032714844" />
                  <Point X="-3.307278808594" Y="3.749276855469" />
                  <Point X="-2.752875976562" Y="4.174331542969" />
                  <Point X="-2.730087158203" Y="4.186992675781" />
                  <Point X="-2.141548583984" Y="4.513972167969" />
                  <Point X="-1.975799682617" Y="4.297963867188" />
                  <Point X="-1.967826660156" Y="4.287573242188" />
                  <Point X="-1.951246826172" Y="4.273660644531" />
                  <Point X="-1.947450561523" Y="4.271684570312" />
                  <Point X="-1.856030761719" Y="4.22409375" />
                  <Point X="-1.835124267578" Y="4.2184921875" />
                  <Point X="-1.813808959961" Y="4.222250488281" />
                  <Point X="-1.809854858398" Y="4.223888671875" />
                  <Point X="-1.714635009766" Y="4.263330078125" />
                  <Point X="-1.696905273438" Y="4.275744628906" />
                  <Point X="-1.686083496094" Y="4.294488769531" />
                  <Point X="-1.684796386719" Y="4.298570800781" />
                  <Point X="-1.653804077148" Y="4.396865234375" />
                  <Point X="-1.651917602539" Y="4.418426757812" />
                  <Point X="-1.689137573242" Y="4.701141113281" />
                  <Point X="-0.968094360352" Y="4.903296386719" />
                  <Point X="-0.940470031738" Y="4.906529296875" />
                  <Point X="-0.224199615479" Y="4.990358398438" />
                  <Point X="-0.0498450737" Y="4.339657714844" />
                  <Point X="-0.042140380859" Y="4.310903320312" />
                  <Point X="-0.024282140732" Y="4.284176757813" />
                  <Point X="0.006155934334" Y="4.273844238281" />
                  <Point X="0.036594055176" Y="4.284176757813" />
                  <Point X="0.054452251434" Y="4.310903320312" />
                  <Point X="0.236648483276" Y="4.990868652344" />
                  <Point X="0.860205932617" Y="4.925565429688" />
                  <Point X="0.883062438965" Y="4.920047363281" />
                  <Point X="1.508455810547" Y="4.769058105469" />
                  <Point X="1.522881225586" Y="4.763825683594" />
                  <Point X="1.931033569336" Y="4.615786132813" />
                  <Point X="1.945421508789" Y="4.609057128906" />
                  <Point X="2.33869921875" Y="4.425134277344" />
                  <Point X="2.352606445312" Y="4.417031738281" />
                  <Point X="2.732523681641" Y="4.19569140625" />
                  <Point X="2.745633056641" Y="4.186368652344" />
                  <Point X="3.068740234375" Y="3.956592773438" />
                  <Point X="2.264206542969" Y="2.563099365234" />
                  <Point X="2.229853515625" Y="2.503597900391" />
                  <Point X="2.22399609375" Y="2.488313720703" />
                  <Point X="2.203382324219" Y="2.411228271484" />
                  <Point X="2.202378417969" Y="2.389558105469" />
                  <Point X="2.210416015625" Y="2.322901123047" />
                  <Point X="2.218682128906" Y="2.3008125" />
                  <Point X="2.220395019531" Y="2.298288330078" />
                  <Point X="2.261639892578" Y="2.23750390625" />
                  <Point X="2.277464111328" Y="2.222490722656" />
                  <Point X="2.338248779297" Y="2.18124609375" />
                  <Point X="2.360336914062" Y="2.172980224609" />
                  <Point X="2.363104980469" Y="2.172646240234" />
                  <Point X="2.42976171875" Y="2.164608642578" />
                  <Point X="2.451865478516" Y="2.166802490234" />
                  <Point X="2.528950683594" Y="2.187416015625" />
                  <Point X="2.541033935547" Y="2.192417480469" />
                  <Point X="3.973915771484" Y="3.019692138672" />
                  <Point X="3.994248046875" Y="3.031430908203" />
                  <Point X="4.202591796875" Y="2.741880371094" />
                  <Point X="4.209900390625" Y="2.729802490234" />
                  <Point X="4.387512695312" Y="2.436296142578" />
                  <Point X="3.333844970703" Y="1.627788208008" />
                  <Point X="3.288615966797" Y="1.593082641602" />
                  <Point X="3.277067382812" Y="1.580827758789" />
                  <Point X="3.221589111328" Y="1.508451904297" />
                  <Point X="3.212261474609" Y="1.488431396484" />
                  <Point X="3.191595703125" Y="1.414535522461" />
                  <Point X="3.190779541016" Y="1.390965332031" />
                  <Point X="3.191483886719" Y="1.387551147461" />
                  <Point X="3.208448486328" Y="1.305332519531" />
                  <Point X="3.2175625" Y="1.285042480469" />
                  <Point X="3.263704101562" Y="1.214909667969" />
                  <Point X="3.280947998047" Y="1.198819946289" />
                  <Point X="3.283724609375" Y="1.197256958008" />
                  <Point X="3.350590087891" Y="1.159617675781" />
                  <Point X="3.372319580078" Y="1.153123413086" />
                  <Point X="3.462726074219" Y="1.141175048828" />
                  <Point X="3.475803710938" Y="1.14117175293" />
                  <Point X="4.8369453125" Y="1.320369506836" />
                  <Point X="4.848975585938" Y="1.321953369141" />
                  <Point X="4.939188476562" Y="0.951386047363" />
                  <Point X="4.941491699219" Y="0.936592285156" />
                  <Point X="4.997858398438" Y="0.574556152344" />
                  <Point X="3.793001220703" Y="0.251715591431" />
                  <Point X="3.741167724609" Y="0.237826919556" />
                  <Point X="3.7253984375" Y="0.230687347412" />
                  <Point X="3.636576904297" Y="0.179346878052" />
                  <Point X="3.620052001953" Y="0.164106643677" />
                  <Point X="3.566759033203" Y="0.096198890686" />
                  <Point X="3.556985107422" Y="0.07473513031" />
                  <Point X="3.556247314453" Y="0.070883239746" />
                  <Point X="3.538482910156" Y="-0.021875303268" />
                  <Point X="3.539220703125" Y="-0.044536525726" />
                  <Point X="3.556985107422" Y="-0.137295227051" />
                  <Point X="3.566759033203" Y="-0.158758987427" />
                  <Point X="3.568971923828" Y="-0.161578918457" />
                  <Point X="3.622264892578" Y="-0.229486679077" />
                  <Point X="3.640265380859" Y="-0.244038879395" />
                  <Point X="3.729086914062" Y="-0.295379486084" />
                  <Point X="3.741167724609" Y="-0.300387023926" />
                  <Point X="4.989396484375" Y="-0.634848754883" />
                  <Point X="4.998068359375" Y="-0.637172363281" />
                  <Point X="4.948431640625" Y="-0.966399291992" />
                  <Point X="4.945480957031" Y="-0.979330444336" />
                  <Point X="4.874545898438" Y="-1.290178344727" />
                  <Point X="3.472094482422" Y="-1.105542114258" />
                  <Point X="3.411982177734" Y="-1.097628295898" />
                  <Point X="3.387597167969" Y="-1.099914672852" />
                  <Point X="3.213271972656" Y="-1.137804931641" />
                  <Point X="3.1979453125" Y="-1.143923217773" />
                  <Point X="3.181069824219" Y="-1.159959716797" />
                  <Point X="3.075701171875" Y="-1.286685424805" />
                  <Point X="3.067953613281" Y="-1.299518066406" />
                  <Point X="3.063730712891" Y="-1.320885620117" />
                  <Point X="3.04862890625" Y="-1.485000854492" />
                  <Point X="3.049849121094" Y="-1.501458251953" />
                  <Point X="3.060366699219" Y="-1.522853027344" />
                  <Point X="3.156840820312" Y="-1.672912353516" />
                  <Point X="3.168460693359" Y="-1.685540649414" />
                  <Point X="4.326824707031" Y="-2.574384765625" />
                  <Point X="4.33907421875" Y="-2.583784179688" />
                  <Point X="4.204133300781" Y="-2.802139160156" />
                  <Point X="4.198027832031" Y="-2.810813964844" />
                  <Point X="4.056688476562" Y="-3.011638427734" />
                  <Point X="2.806992675781" Y="-2.290125976562" />
                  <Point X="2.753454589844" Y="-2.259215576172" />
                  <Point X="2.728725341797" Y="-2.251756835938" />
                  <Point X="2.521250488281" Y="-2.214287109375" />
                  <Point X="2.504749267578" Y="-2.214074462891" />
                  <Point X="2.481920166016" Y="-2.22301171875" />
                  <Point X="2.309559570312" Y="-2.313723876953" />
                  <Point X="2.297490966797" Y="-2.322614990234" />
                  <Point X="2.284833007812" Y="-2.341841064453" />
                  <Point X="2.194120849609" Y="-2.514201660156" />
                  <Point X="2.188950439453" Y="-2.529873291016" />
                  <Point X="2.190718994141" Y="-2.554989990234" />
                  <Point X="2.228188720703" Y="-2.76246484375" />
                  <Point X="2.234091552734" Y="-2.778578613281" />
                  <Point X="2.97845703125" Y="-4.067856933594" />
                  <Point X="2.986673828125" Y="-4.082088623047" />
                  <Point X="2.835297119141" Y="-4.190213378906" />
                  <Point X="2.8284765625" Y="-4.194627929688" />
                  <Point X="2.679775878906" Y="-4.290879394531" />
                  <Point X="1.724032104492" Y="-3.045330078125" />
                  <Point X="1.683177490234" Y="-2.992087402344" />
                  <Point X="1.662052001953" Y="-2.975004394531" />
                  <Point X="1.45742590332" Y="-2.843448730469" />
                  <Point X="1.442262207031" Y="-2.8369375" />
                  <Point X="1.41651159668" Y="-2.836572265625" />
                  <Point X="1.192718017578" Y="-2.857166015625" />
                  <Point X="1.178165527344" Y="-2.86076171875" />
                  <Point X="1.158156982422" Y="-2.874475830078" />
                  <Point X="0.985349060059" Y="-3.018159667969" />
                  <Point X="0.974574951172" Y="-3.030659667969" />
                  <Point X="0.966311279297" Y="-3.055857666016" />
                  <Point X="0.91464251709" Y="-3.29357421875" />
                  <Point X="0.91392956543" Y="-3.310720214844" />
                  <Point X="1.124876953125" Y="-4.913024902344" />
                  <Point X="1.127642211914" Y="-4.934028320312" />
                  <Point X="0.994346313477" Y="-4.963246582031" />
                  <Point X="0.98803894043" Y="-4.964392578125" />
                  <Point X="0.860200378418" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#120" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.007704675999" Y="4.383878717325" Z="0.1" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.1" />
                  <Point X="-0.952438035427" Y="4.987470524619" Z="0.1" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.1" />
                  <Point X="-1.719960207573" Y="4.777433358763" Z="0.1" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.1" />
                  <Point X="-1.74881345865" Y="4.755879581922" Z="0.1" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.1" />
                  <Point X="-1.738638169725" Y="4.34488585766" Z="0.1" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.1" />
                  <Point X="-1.835144978169" Y="4.301362422626" Z="0.1" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.1" />
                  <Point X="-1.930518928965" Y="4.34731509453" Z="0.1" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.1" />
                  <Point X="-1.942288205271" Y="4.359681946167" Z="0.1" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.1" />
                  <Point X="-2.760526061943" Y="4.261980126942" Z="0.1" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.1" />
                  <Point X="-3.355060661319" Y="3.811646893241" Z="0.1" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.1" />
                  <Point X="-3.363632477803" Y="3.767501965628" Z="0.1" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.1" />
                  <Point X="-2.994338426924" Y="3.05817441508" Z="0.1" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.1" />
                  <Point X="-3.052342503043" Y="2.996461064843" Z="0.1" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.1" />
                  <Point X="-3.136902050527" Y="3.001226272249" Z="0.1" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.1" />
                  <Point X="-3.166357382669" Y="3.016561470943" Z="0.1" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.1" />
                  <Point X="-4.191164100168" Y="2.867587760212" Z="0.1" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.1" />
                  <Point X="-4.534099174379" Y="2.287122726264" Z="0.1" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.1" />
                  <Point X="-4.513721051871" Y="2.237862022633" Z="0.1" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.1" />
                  <Point X="-3.668008163918" Y="1.555982148284" Z="0.1" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.1" />
                  <Point X="-3.690487211005" Y="1.496572550423" Z="0.1" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.1" />
                  <Point X="-3.7504470045" Y="1.475604721567" Z="0.1" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.1" />
                  <Point X="-3.795301879876" Y="1.480415367196" Z="0.1" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.1" />
                  <Point X="-4.966597688451" Y="1.060936531753" Z="0.1" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.1" />
                  <Point X="-5.056838569419" Y="0.470217864375" Z="0.1" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.1" />
                  <Point X="-5.001169172287" Y="0.430791718058" Z="0.1" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.1" />
                  <Point X="-3.5499143688" Y="0.030574931759" Z="0.1" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.1" />
                  <Point X="-3.539925584511" Y="0.001188405503" Z="0.1" />
                  <Point X="-3.539556741714" Y="0" Z="0.1" />
                  <Point X="-3.548439016624" Y="-0.028618545557" Z="0.1" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.1" />
                  <Point X="-3.575454226323" Y="-0.048301051375" Z="0.1" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.1" />
                  <Point X="-3.635718616485" Y="-0.064920338698" Z="0.1" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.1" />
                  <Point X="-4.985758246918" Y="-0.968020252081" Z="0.1" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.1" />
                  <Point X="-4.852316022304" Y="-1.49996937576" Z="0.1" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.1" />
                  <Point X="-4.782005024322" Y="-1.512615865694" Z="0.1" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.1" />
                  <Point X="-3.19373216842" Y="-1.321828368544" Z="0.1" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.1" />
                  <Point X="-3.200073623273" Y="-1.351011140379" Z="0.1" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.1" />
                  <Point X="-3.252312369843" Y="-1.392045661991" Z="0.1" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.1" />
                  <Point X="-4.221058568734" Y="-2.824261786951" Z="0.1" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.1" />
                  <Point X="-3.87607842617" Y="-3.281743733367" Z="0.1" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.1" />
                  <Point X="-3.810830428184" Y="-3.270245357181" Z="0.1" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.1" />
                  <Point X="-2.556182657796" Y="-2.572147811336" Z="0.1" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.1" />
                  <Point X="-2.585171655987" Y="-2.624247954308" Z="0.1" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.1" />
                  <Point X="-2.906800336704" Y="-4.164932583666" Z="0.1" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.1" />
                  <Point X="-2.468634982632" Y="-4.43869533576" Z="0.1" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.1" />
                  <Point X="-2.442151153462" Y="-4.437856071534" Z="0.1" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.1" />
                  <Point X="-1.978541132752" Y="-3.990956686059" Z="0.1" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.1" />
                  <Point X="-1.67101019301" Y="-4.003566908221" Z="0.1" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.1" />
                  <Point X="-1.434706108548" Y="-4.200784500292" Z="0.1" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.1" />
                  <Point X="-1.36729176926" Y="-4.501100095918" Z="0.1" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.1" />
                  <Point X="-1.366801091039" Y="-4.527835485724" Z="0.1" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.1" />
                  <Point X="-1.129191309612" Y="-4.952550243903" Z="0.1" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.1" />
                  <Point X="-0.829618489039" Y="-5.011443334461" Z="0.1" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.1" />
                  <Point X="-0.80169687211" Y="-4.954157586102" Z="0.1" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.1" />
                  <Point X="-0.259886915777" Y="-3.292278044594" Z="0.1" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.1" />
                  <Point X="-0.010101446594" Y="-3.207374484683" Z="0.1" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.1" />
                  <Point X="0.243257632767" Y="-3.279737560605" Z="0.1" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.1" />
                  <Point X="0.410558943655" Y="-3.509367711174" Z="0.1" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.1" />
                  <Point X="0.433057988291" Y="-3.578378443714" Z="0.1" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.1" />
                  <Point X="0.990819905574" Y="-4.982307583652" Z="0.1" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.1" />
                  <Point X="1.169914523417" Y="-4.943320039215" Z="0.1" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.1" />
                  <Point X="1.168293231566" Y="-4.8752184478" Z="0.1" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.1" />
                  <Point X="1.009014438347" Y="-3.035197354041" Z="0.1" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.1" />
                  <Point X="1.183967785348" Y="-2.881642020081" Z="0.1" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.1" />
                  <Point X="1.414937270924" Y="-2.855081756579" Z="0.1" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.1" />
                  <Point X="1.628856648605" Y="-2.985782324111" Z="0.1" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.1" />
                  <Point X="1.678208479277" Y="-3.044487987009" Z="0.1" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.1" />
                  <Point X="2.849489866345" Y="-4.205322251188" Z="0.1" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.1" />
                  <Point X="3.038967628816" Y="-4.070504174894" Z="0.1" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.1" />
                  <Point X="3.015602299808" Y="-4.01157678046" Z="0.1" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.1" />
                  <Point X="2.233767074062" Y="-2.514823724397" Z="0.1" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.1" />
                  <Point X="2.322923308416" Y="-2.33384759327" Z="0.1" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.1" />
                  <Point X="2.499050785102" Y="-2.235978001551" Z="0.1" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.1" />
                  <Point X="2.713683079116" Y="-2.269680935429" Z="0.1" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.1" />
                  <Point X="2.775836816647" Y="-2.302147200466" Z="0.1" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.1" />
                  <Point X="4.23276064591" Y="-2.808311197171" Z="0.1" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.1" />
                  <Point X="4.392849814499" Y="-2.550636246012" Z="0.1" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.1" />
                  <Point X="4.351106661673" Y="-2.503436982168" Z="0.1" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.1" />
                  <Point X="3.096268664738" Y="-1.46453381374" Z="0.1" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.1" />
                  <Point X="3.107364342481" Y="-1.294187234764" Z="0.1" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.1" />
                  <Point X="3.213360024091" Y="-1.160646556897" Z="0.1" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.1" />
                  <Point X="3.392060875718" Y="-1.117493847642" Z="0.1" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.1" />
                  <Point X="3.459412252937" Y="-1.12383437053" Z="0.1" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.1" />
                  <Point X="4.98807209231" Y="-0.959174352167" Z="0.1" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.1" />
                  <Point X="5.045759398979" Y="-0.584123001091" Z="0.1" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.1" />
                  <Point X="4.996181525904" Y="-0.555272527481" Z="0.1" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.1" />
                  <Point X="3.659131095131" Y="-0.169470157371" Z="0.1" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.1" />
                  <Point X="3.602149751973" Y="-0.09943044927" Z="0.1" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.1" />
                  <Point X="3.582172402803" Y="-0.003851869581" Z="0.1" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.1" />
                  <Point X="3.599199047621" Y="0.092758661666" Z="0.1" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.1" />
                  <Point X="3.653229686427" Y="0.164518289305" Z="0.1" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.1" />
                  <Point X="3.744264319222" Y="0.218678686871" Z="0.1" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.1" />
                  <Point X="3.799786291402" Y="0.234699405288" Z="0.1" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.1" />
                  <Point X="4.984740411784" Y="0.975564402632" Z="0.1" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.1" />
                  <Point X="4.884825397486" Y="1.39206832667" Z="0.1" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.1" />
                  <Point X="4.82426308706" Y="1.401221839101" Z="0.1" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.1" />
                  <Point X="3.372714817348" Y="1.233972416243" Z="0.1" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.1" />
                  <Point X="3.302095186752" Y="1.272108026139" Z="0.1" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.1" />
                  <Point X="3.253177054441" Y="1.343804079146" Z="0.1" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.1" />
                  <Point X="3.234296496322" Y="1.428935018446" Z="0.1" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.1" />
                  <Point X="3.254257824177" Y="1.50624512196" Z="0.1" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.1" />
                  <Point X="3.310594379199" Y="1.581689638458" Z="0.1" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.1" />
                  <Point X="3.358127324005" Y="1.619400658521" Z="0.1" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.1" />
                  <Point X="4.246521921302" Y="2.786968850875" Z="0.1" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.1" />
                  <Point X="4.011667423091" Y="3.115554015673" Z="0.1" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.1" />
                  <Point X="3.942759723567" Y="3.094273415583" Z="0.1" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.1" />
                  <Point X="2.432792675463" Y="2.246385169183" Z="0.1" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.1" />
                  <Point X="2.362934763081" Y="2.25356676144" Z="0.1" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.1" />
                  <Point X="2.299382195803" Y="2.295145351581" Z="0.1" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.1" />
                  <Point X="2.255613248654" Y="2.357642664579" Z="0.1" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.1" />
                  <Point X="2.245862941319" Y="2.426823680751" Z="0.1" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.1" />
                  <Point X="2.266142773475" Y="2.506676900419" Z="0.1" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.1" />
                  <Point X="2.301351937277" Y="2.569379358388" Z="0.1" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.1" />
                  <Point X="2.768454251838" Y="4.258395826954" Z="0.1" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.1" />
                  <Point X="2.37162045085" Y="4.491514652061" Z="0.1" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.1" />
                  <Point X="1.960447005389" Y="4.685629340961" Z="0.1" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.1" />
                  <Point X="1.533773812352" Y="4.842109845674" Z="0.1" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.1" />
                  <Point X="0.888640828225" Y="4.99993093137" Z="0.1" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.1" />
                  <Point X="0.219930167871" Y="5.073528336686" Z="0.1" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.1" />
                  <Point X="0.185539885554" Y="5.047568788626" Z="0.1" />
                  <Point X="0" Y="4.355124473572" Z="0.1" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>