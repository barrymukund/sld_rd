<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#177" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2280" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="0" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000475830078" Y="0.004715820312" />
                  <Width Value="9.783896972656" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.800066040039" Y="-4.396139648438" />
                  <Point X="0.563302001953" Y="-3.512524414063" />
                  <Point X="0.557721374512" Y="-3.497142333984" />
                  <Point X="0.54236328125" Y="-3.467377197266" />
                  <Point X="0.445591705322" Y="-3.327947998047" />
                  <Point X="0.378635437012" Y="-3.231476806641" />
                  <Point X="0.356751678467" Y="-3.209021484375" />
                  <Point X="0.330496337891" Y="-3.189777587891" />
                  <Point X="0.302495269775" Y="-3.175669189453" />
                  <Point X="0.152747039795" Y="-3.129192871094" />
                  <Point X="0.04913628006" Y="-3.097035888672" />
                  <Point X="0.020976730347" Y="-3.092766601562" />
                  <Point X="-0.008664898872" Y="-3.092766601562" />
                  <Point X="-0.036824295044" Y="-3.097035888672" />
                  <Point X="-0.186572357178" Y="-3.143511962891" />
                  <Point X="-0.290183258057" Y="-3.175669189453" />
                  <Point X="-0.318184509277" Y="-3.189777587891" />
                  <Point X="-0.344439849854" Y="-3.209021484375" />
                  <Point X="-0.366323608398" Y="-3.231476806641" />
                  <Point X="-0.463095001221" Y="-3.370906005859" />
                  <Point X="-0.530051269531" Y="-3.467377197266" />
                  <Point X="-0.538188903809" Y="-3.481573730469" />
                  <Point X="-0.55099017334" Y="-3.512524414063" />
                  <Point X="-0.714807189941" Y="-4.1238984375" />
                  <Point X="-0.916584533691" Y="-4.876941894531" />
                  <Point X="-0.961040710449" Y="-4.8683125" />
                  <Point X="-1.079342651367" Y="-4.845350097656" />
                  <Point X="-1.24641809082" Y="-4.802362792969" />
                  <Point X="-1.243463867188" Y="-4.779923339844" />
                  <Point X="-1.214963012695" Y="-4.563438476562" />
                  <Point X="-1.214201171875" Y="-4.5479296875" />
                  <Point X="-1.216508666992" Y="-4.516224121094" />
                  <Point X="-1.252283569336" Y="-4.336372070312" />
                  <Point X="-1.277036132812" Y="-4.211932617188" />
                  <Point X="-1.287938354492" Y="-4.182966308594" />
                  <Point X="-1.304010375977" Y="-4.15512890625" />
                  <Point X="-1.323644775391" Y="-4.131204101562" />
                  <Point X="-1.461514038086" Y="-4.010295898437" />
                  <Point X="-1.556905395508" Y="-3.926639404297" />
                  <Point X="-1.583188354492" Y="-3.910295410156" />
                  <Point X="-1.612885742188" Y="-3.897994384766" />
                  <Point X="-1.64302734375" Y="-3.890966308594" />
                  <Point X="-1.826010375977" Y="-3.878972900391" />
                  <Point X="-1.952616455078" Y="-3.870674804688" />
                  <Point X="-1.98341796875" Y="-3.873708496094" />
                  <Point X="-2.014466674805" Y="-3.882028076172" />
                  <Point X="-2.042657592773" Y="-3.894801513672" />
                  <Point X="-2.195129150391" Y="-3.9966796875" />
                  <Point X="-2.300624267578" Y="-4.067169677734" />
                  <Point X="-2.312785644531" Y="-4.076821533203" />
                  <Point X="-2.335102783203" Y="-4.099461914062" />
                  <Point X="-2.427073486328" Y="-4.2193203125" />
                  <Point X="-2.480149169922" Y="-4.288489746094" />
                  <Point X="-2.628312011719" Y="-4.196750976562" />
                  <Point X="-2.801706787109" Y="-4.089389404297" />
                  <Point X="-3.037864501953" Y="-3.907555908203" />
                  <Point X="-3.104721923828" Y="-3.856077880859" />
                  <Point X="-2.863760498047" Y="-3.438720458984" />
                  <Point X="-2.423761230469" Y="-2.676619628906" />
                  <Point X="-2.412859130859" Y="-2.647653320312" />
                  <Point X="-2.406588134766" Y="-2.616126953125" />
                  <Point X="-2.405575439453" Y="-2.585192871094" />
                  <Point X="-2.414560058594" Y="-2.555575195312" />
                  <Point X="-2.428777099609" Y="-2.526746337891" />
                  <Point X="-2.4468046875" Y="-2.501589355469" />
                  <Point X="-2.464153320312" Y="-2.484240722656" />
                  <Point X="-2.489310302734" Y="-2.466213134766" />
                  <Point X="-2.518139160156" Y="-2.45199609375" />
                  <Point X="-2.547756835938" Y="-2.443011474609" />
                  <Point X="-2.578690917969" Y="-2.444024169922" />
                  <Point X="-2.610217285156" Y="-2.450295166016" />
                  <Point X="-2.63918359375" Y="-2.461197265625" />
                  <Point X="-3.166481933594" Y="-2.765633056641" />
                  <Point X="-3.818024414062" Y="-3.141801269531" />
                  <Point X="-3.9458671875" Y="-2.973842285156" />
                  <Point X="-4.082859375" Y="-2.793862060547" />
                  <Point X="-4.252163085938" Y="-2.509965332031" />
                  <Point X="-4.306142578125" Y="-2.419450439453" />
                  <Point X="-3.875810546875" Y="-2.089244628906" />
                  <Point X="-3.105954589844" Y="-1.498513427734" />
                  <Point X="-3.084577392578" Y="-1.47559362793" />
                  <Point X="-3.066612548828" Y="-1.448462646484" />
                  <Point X="-3.053856445312" Y="-1.419833007812" />
                  <Point X="-3.046151855469" Y="-1.390085571289" />
                  <Point X="-3.04334765625" Y="-1.359657104492" />
                  <Point X="-3.045556396484" Y="-1.327986450195" />
                  <Point X="-3.052557617188" Y="-1.298241088867" />
                  <Point X="-3.068640136719" Y="-1.272257202148" />
                  <Point X="-3.089472900391" Y="-1.24830065918" />
                  <Point X="-3.112972412109" Y="-1.228767211914" />
                  <Point X="-3.139455078125" Y="-1.213180541992" />
                  <Point X="-3.168718261719" Y="-1.201956542969" />
                  <Point X="-3.200605957031" Y="-1.195474853516" />
                  <Point X="-3.231929199219" Y="-1.194383789062" />
                  <Point X="-3.897593261719" Y="-1.282020141602" />
                  <Point X="-4.732102539062" Y="-1.391885253906" />
                  <Point X="-4.780498046875" Y="-1.202417602539" />
                  <Point X="-4.834076171875" Y="-0.992663024902" />
                  <Point X="-4.878870605469" Y="-0.679464416504" />
                  <Point X="-4.892424316406" Y="-0.584698242188" />
                  <Point X="-4.409857910156" Y="-0.455394897461" />
                  <Point X="-3.532875976562" Y="-0.220408340454" />
                  <Point X="-3.509681640625" Y="-0.210754318237" />
                  <Point X="-3.477710449219" Y="-0.19214906311" />
                  <Point X="-3.464511962891" Y="-0.182884994507" />
                  <Point X="-3.44134375" Y="-0.163490127563" />
                  <Point X="-3.4253671875" Y="-0.146346252441" />
                  <Point X="-3.414073486328" Y="-0.125813011169" />
                  <Point X="-3.401180175781" Y="-0.093457977295" />
                  <Point X="-3.396713867188" Y="-0.078990226746" />
                  <Point X="-3.39068359375" Y="-0.051979885101" />
                  <Point X="-3.388403076172" Y="-0.030622747421" />
                  <Point X="-3.390978759766" Y="-0.009299157143" />
                  <Point X="-3.398693603516" Y="0.023139211655" />
                  <Point X="-3.403410888672" Y="0.037666614532" />
                  <Point X="-3.414619628906" Y="0.06459362793" />
                  <Point X="-3.426225341797" Y="0.0849529953" />
                  <Point X="-3.442461914062" Y="0.101851791382" />
                  <Point X="-3.470683837891" Y="0.124754234314" />
                  <Point X="-3.484073486328" Y="0.133844650269" />
                  <Point X="-3.510990966797" Y="0.148942184448" />
                  <Point X="-3.532875976562" Y="0.157848342896" />
                  <Point X="-4.139660644531" Y="0.32043572998" />
                  <Point X="-4.89181640625" Y="0.521975219727" />
                  <Point X="-4.859018554688" Y="0.743621337891" />
                  <Point X="-4.82448828125" Y="0.976971374512" />
                  <Point X="-4.734319824219" Y="1.309721679688" />
                  <Point X="-4.703551269531" Y="1.423267822266" />
                  <Point X="-4.394308105469" Y="1.382555175781" />
                  <Point X="-3.765666259766" Y="1.29979284668" />
                  <Point X="-3.744986083984" Y="1.299341552734" />
                  <Point X="-3.723424560547" Y="1.301228149414" />
                  <Point X="-3.703137451172" Y="1.305263549805" />
                  <Point X="-3.666831542969" Y="1.3167109375" />
                  <Point X="-3.641711425781" Y="1.324631103516" />
                  <Point X="-3.622779052734" Y="1.332961669922" />
                  <Point X="-3.604034667969" Y="1.343783691406" />
                  <Point X="-3.587353027344" Y="1.356014892578" />
                  <Point X="-3.573714599609" Y="1.371566772461" />
                  <Point X="-3.561300292969" Y="1.38929675293" />
                  <Point X="-3.5513515625" Y="1.407431152344" />
                  <Point X="-3.536783691406" Y="1.442601074219" />
                  <Point X="-3.526704101563" Y="1.466935302734" />
                  <Point X="-3.520916015625" Y="1.48679309082" />
                  <Point X="-3.517157470703" Y="1.508108154297" />
                  <Point X="-3.515804443359" Y="1.528748657227" />
                  <Point X="-3.518951171875" Y="1.549192749023" />
                  <Point X="-3.524552978516" Y="1.570099243164" />
                  <Point X="-3.532049804688" Y="1.589377563477" />
                  <Point X="-3.549627441406" Y="1.623144287109" />
                  <Point X="-3.561789550781" Y="1.646507324219" />
                  <Point X="-3.573281738281" Y="1.663706665039" />
                  <Point X="-3.587194335938" Y="1.680286743164" />
                  <Point X="-3.602135986328" Y="1.694590209961" />
                  <Point X="-3.950188964844" Y="1.961660644531" />
                  <Point X="-4.351859863281" Y="2.269874267578" />
                  <Point X="-4.215324707031" Y="2.503791992188" />
                  <Point X="-4.081153808594" Y="2.733659423828" />
                  <Point X="-3.842303955078" Y="3.040666992188" />
                  <Point X="-3.750504882813" Y="3.158661865234" />
                  <Point X="-3.592751464844" Y="3.067582763672" />
                  <Point X="-3.206656982422" Y="2.844670898438" />
                  <Point X="-3.187724121094" Y="2.836340332031" />
                  <Point X="-3.167081787109" Y="2.829831787109" />
                  <Point X="-3.146794433594" Y="2.825796386719" />
                  <Point X="-3.096230224609" Y="2.821372558594" />
                  <Point X="-3.061245117188" Y="2.818311767578" />
                  <Point X="-3.040564941406" Y="2.818762939453" />
                  <Point X="-3.019106201172" Y="2.821588134766" />
                  <Point X="-2.999014160156" Y="2.826504882812" />
                  <Point X="-2.980462402344" Y="2.835653564453" />
                  <Point X="-2.962208251953" Y="2.847282958984" />
                  <Point X="-2.946077636719" Y="2.860229492188" />
                  <Point X="-2.910186767578" Y="2.896120117188" />
                  <Point X="-2.885354003906" Y="2.920952880859" />
                  <Point X="-2.872406982422" Y="2.937083984375" />
                  <Point X="-2.860777587891" Y="2.955338134766" />
                  <Point X="-2.85162890625" Y="2.973889892578" />
                  <Point X="-2.846712158203" Y="2.993981933594" />
                  <Point X="-2.843886962891" Y="3.015440673828" />
                  <Point X="-2.843435791016" Y="3.036120849609" />
                  <Point X="-2.847859619141" Y="3.086685058594" />
                  <Point X="-2.850920410156" Y="3.121670410156" />
                  <Point X="-2.854955810547" Y="3.141957763672" />
                  <Point X="-2.861464355469" Y="3.162600097656" />
                  <Point X="-2.869794921875" Y="3.181532958984" />
                  <Point X="-3.024027832031" Y="3.448671875" />
                  <Point X="-3.183332763672" Y="3.724596435547" />
                  <Point X="-2.934305175781" Y="3.9155234375" />
                  <Point X="-2.700625976563" Y="4.09468359375" />
                  <Point X="-2.324446533203" Y="4.303680664062" />
                  <Point X="-2.167037109375" Y="4.391134277344" />
                  <Point X="-2.161390625" Y="4.383775878906" />
                  <Point X="-2.043195556641" Y="4.229740722656" />
                  <Point X="-2.028892700195" Y="4.214799804688" />
                  <Point X="-2.012312744141" Y="4.200887207031" />
                  <Point X="-1.99511315918" Y="4.18939453125" />
                  <Point X="-1.938835571289" Y="4.160098144531" />
                  <Point X="-1.899896972656" Y="4.139827636719" />
                  <Point X="-1.88061730957" Y="4.132330566406" />
                  <Point X="-1.859710693359" Y="4.126729003906" />
                  <Point X="-1.839267822266" Y="4.123582519531" />
                  <Point X="-1.818628417969" Y="4.124935546875" />
                  <Point X="-1.797313110352" Y="4.128693847656" />
                  <Point X="-1.777453613281" Y="4.134481933594" />
                  <Point X="-1.718836547852" Y="4.158762207031" />
                  <Point X="-1.678279418945" Y="4.175561523437" />
                  <Point X="-1.66014465332" Y="4.185510742187" />
                  <Point X="-1.642415039062" Y="4.197925292969" />
                  <Point X="-1.626863525391" Y="4.211563964844" />
                  <Point X="-1.614632568359" Y="4.228245117188" />
                  <Point X="-1.603810791016" Y="4.246989257813" />
                  <Point X="-1.59548034668" Y="4.265921386719" />
                  <Point X="-1.576401733398" Y="4.326431152344" />
                  <Point X="-1.563201049805" Y="4.368297851562" />
                  <Point X="-1.559165649414" Y="4.388584960938" />
                  <Point X="-1.557279174805" Y="4.410146484375" />
                  <Point X="-1.557730224609" Y="4.430826660156" />
                  <Point X="-1.575264892578" Y="4.564015136719" />
                  <Point X="-1.584201904297" Y="4.631897949219" />
                  <Point X="-1.252147949219" Y="4.724994140625" />
                  <Point X="-0.949634887695" Y="4.80980859375" />
                  <Point X="-0.493597290039" Y="4.863180664062" />
                  <Point X="-0.29471081543" Y="4.886457519531" />
                  <Point X="-0.248120788574" Y="4.712581054688" />
                  <Point X="-0.133903366089" Y="4.286315429688" />
                  <Point X="-0.121129745483" Y="4.258124023438" />
                  <Point X="-0.103271606445" Y="4.231397460938" />
                  <Point X="-0.0821145401" Y="4.20880859375" />
                  <Point X="-0.054819351196" Y="4.19421875" />
                  <Point X="-0.024381277084" Y="4.183886230469" />
                  <Point X="0.006155914307" Y="4.178844238281" />
                  <Point X="0.036693107605" Y="4.183886230469" />
                  <Point X="0.06713117981" Y="4.19421875" />
                  <Point X="0.094426452637" Y="4.20880859375" />
                  <Point X="0.115583435059" Y="4.231397460938" />
                  <Point X="0.133441726685" Y="4.258124023438" />
                  <Point X="0.146215194702" Y="4.286315429688" />
                  <Point X="0.225242294312" Y="4.581249023438" />
                  <Point X="0.307419281006" Y="4.8879375" />
                  <Point X="0.579896606445" Y="4.859401855469" />
                  <Point X="0.844041320801" Y="4.831738769531" />
                  <Point X="1.221338012695" Y="4.740647949219" />
                  <Point X="1.481025634766" Y="4.677950683594" />
                  <Point X="1.726005126953" Y="4.589095214844" />
                  <Point X="1.89464453125" Y="4.527928710938" />
                  <Point X="2.132115234375" Y="4.416871582031" />
                  <Point X="2.294574951172" Y="4.34089453125" />
                  <Point X="2.524007568359" Y="4.2072265625" />
                  <Point X="2.680978759766" Y="4.115774902344" />
                  <Point X="2.897340576172" Y="3.96191015625" />
                  <Point X="2.943260009766" Y="3.929254882812" />
                  <Point X="2.656847167969" Y="3.433172851562" />
                  <Point X="2.147581054688" Y="2.551097900391" />
                  <Point X="2.142075683594" Y="2.539930664062" />
                  <Point X="2.133076904297" Y="2.516056640625" />
                  <Point X="2.120387207031" Y="2.468603271484" />
                  <Point X="2.111607177734" Y="2.435770263672" />
                  <Point X="2.108619384766" Y="2.417934326172" />
                  <Point X="2.107728027344" Y="2.380952880859" />
                  <Point X="2.112676025391" Y="2.339919433594" />
                  <Point X="2.116099365234" Y="2.311528076172" />
                  <Point X="2.121441894531" Y="2.289605224609" />
                  <Point X="2.129708007812" Y="2.267516357422" />
                  <Point X="2.140070800781" Y="2.247471191406" />
                  <Point X="2.1654609375" Y="2.210052490234" />
                  <Point X="2.183028564453" Y="2.184162597656" />
                  <Point X="2.194465332031" Y="2.170327880859" />
                  <Point X="2.221598876953" Y="2.145592529297" />
                  <Point X="2.259017333984" Y="2.120202392578" />
                  <Point X="2.284907470703" Y="2.102635009766" />
                  <Point X="2.304952636719" Y="2.092271972656" />
                  <Point X="2.327040771484" Y="2.084006103516" />
                  <Point X="2.348963867188" Y="2.078663574219" />
                  <Point X="2.389997558594" Y="2.073715576172" />
                  <Point X="2.418388671875" Y="2.070291992188" />
                  <Point X="2.436467529297" Y="2.069845703125" />
                  <Point X="2.473206298828" Y="2.074171142578" />
                  <Point X="2.520659667969" Y="2.086860839844" />
                  <Point X="2.553492675781" Y="2.095640869141" />
                  <Point X="2.565283447266" Y="2.099638183594" />
                  <Point X="2.588533935547" Y="2.110145019531" />
                  <Point X="3.198842285156" Y="2.462506591797" />
                  <Point X="3.967326416016" Y="2.906191162109" />
                  <Point X="4.030161376953" Y="2.818864746094" />
                  <Point X="4.123272460938" Y="2.689461181641" />
                  <Point X="4.243891113281" Y="2.490137939453" />
                  <Point X="4.262198730469" Y="2.459884277344" />
                  <Point X="3.901280517578" Y="2.182941650391" />
                  <Point X="3.230783935547" Y="1.668451293945" />
                  <Point X="3.221423828125" Y="1.660240600586" />
                  <Point X="3.203973876953" Y="1.641627807617" />
                  <Point X="3.169821533203" Y="1.597073608398" />
                  <Point X="3.146191650391" Y="1.566246459961" />
                  <Point X="3.136605712891" Y="1.550911621094" />
                  <Point X="3.121629882812" Y="1.51708605957" />
                  <Point X="3.108908203125" Y="1.471596069336" />
                  <Point X="3.100105957031" Y="1.440121582031" />
                  <Point X="3.096652587891" Y="1.417823242188" />
                  <Point X="3.095836425781" Y="1.394252807617" />
                  <Point X="3.097739257812" Y="1.371768188477" />
                  <Point X="3.108182373047" Y="1.321154785156" />
                  <Point X="3.115408203125" Y="1.286135375977" />
                  <Point X="3.120680175781" Y="1.268977172852" />
                  <Point X="3.136282714844" Y="1.235740356445" />
                  <Point X="3.164687255859" Y="1.192566772461" />
                  <Point X="3.184340332031" Y="1.162694824219" />
                  <Point X="3.198893554688" Y="1.145450439453" />
                  <Point X="3.216137451172" Y="1.129360595703" />
                  <Point X="3.234347167969" Y="1.116034912109" />
                  <Point X="3.275509033203" Y="1.092864257813" />
                  <Point X="3.303989257812" Y="1.076832519531" />
                  <Point X="3.320521240234" Y="1.069501586914" />
                  <Point X="3.356118164062" Y="1.059438598633" />
                  <Point X="3.411771972656" Y="1.052083251953" />
                  <Point X="3.450278808594" Y="1.046994018555" />
                  <Point X="3.462702148438" Y="1.046175048828" />
                  <Point X="3.488203613281" Y="1.04698449707" />
                  <Point X="4.067955566406" Y="1.123310302734" />
                  <Point X="4.776839355469" Y="1.21663671875" />
                  <Point X="4.805945800781" Y="1.097076171875" />
                  <Point X="4.845936035156" Y="0.932809448242" />
                  <Point X="4.883946289062" Y="0.688674072266" />
                  <Point X="4.890864746094" Y="0.644238586426" />
                  <Point X="4.484983398438" Y="0.535482971191" />
                  <Point X="3.716579833984" Y="0.329589904785" />
                  <Point X="3.704790283203" Y="0.325586151123" />
                  <Point X="3.681545898438" Y="0.315067962646" />
                  <Point X="3.626867675781" Y="0.283463104248" />
                  <Point X="3.589035888672" Y="0.261595550537" />
                  <Point X="3.574311035156" Y="0.251096221924" />
                  <Point X="3.547531005859" Y="0.225576599121" />
                  <Point X="3.514724121094" Y="0.183772918701" />
                  <Point X="3.492025146484" Y="0.154848953247" />
                  <Point X="3.480301025391" Y="0.135569152832" />
                  <Point X="3.470527099609" Y="0.114105377197" />
                  <Point X="3.463680908203" Y="0.092604263306" />
                  <Point X="3.452745117188" Y="0.035502540588" />
                  <Point X="3.445178710938" Y="-0.004006225586" />
                  <Point X="3.443482910156" Y="-0.021875295639" />
                  <Point X="3.445178710938" Y="-0.058553928375" />
                  <Point X="3.456114501953" Y="-0.115655654907" />
                  <Point X="3.463680908203" Y="-0.155164276123" />
                  <Point X="3.470527099609" Y="-0.176665527344" />
                  <Point X="3.480301025391" Y="-0.198129302979" />
                  <Point X="3.492025146484" Y="-0.217409255981" />
                  <Point X="3.52483203125" Y="-0.259212799072" />
                  <Point X="3.547531005859" Y="-0.288136901855" />
                  <Point X="3.559999023438" Y="-0.301236114502" />
                  <Point X="3.589035888672" Y="-0.324155700684" />
                  <Point X="3.643714111328" Y="-0.3557605896" />
                  <Point X="3.681545898438" Y="-0.377628143311" />
                  <Point X="3.692710205078" Y="-0.383139038086" />
                  <Point X="3.716579833984" Y="-0.392150054932" />
                  <Point X="4.248238769531" Y="-0.53460748291" />
                  <Point X="4.89147265625" Y="-0.706961486816" />
                  <Point X="4.8773515625" Y="-0.800623352051" />
                  <Point X="4.855022460938" Y="-0.948726318359" />
                  <Point X="4.806324707031" Y="-1.162126953125" />
                  <Point X="4.801173828125" Y="-1.184698974609" />
                  <Point X="4.315514160156" Y="-1.120760498047" />
                  <Point X="3.424382080078" Y="-1.003440979004" />
                  <Point X="3.408035644531" Y="-1.002710266113" />
                  <Point X="3.374658447266" Y="-1.005508666992" />
                  <Point X="3.267344726562" Y="-1.028833740234" />
                  <Point X="3.193094238281" Y="-1.044972412109" />
                  <Point X="3.163973632812" Y="-1.056597167969" />
                  <Point X="3.136147216797" Y="-1.073489501953" />
                  <Point X="3.112397460938" Y="-1.093960083008" />
                  <Point X="3.047532958984" Y="-1.171971801758" />
                  <Point X="3.002653320312" Y="-1.225948242188" />
                  <Point X="2.987932617188" Y="-1.250330444336" />
                  <Point X="2.976589355469" Y="-1.277715820312" />
                  <Point X="2.969757568359" Y="-1.305365478516" />
                  <Point X="2.9604609375" Y="-1.406393920898" />
                  <Point X="2.954028564453" Y="-1.476295654297" />
                  <Point X="2.956347412109" Y="-1.507564697266" />
                  <Point X="2.964079101562" Y="-1.539185791016" />
                  <Point X="2.976450439453" Y="-1.567996582031" />
                  <Point X="3.035839355469" Y="-1.660372436523" />
                  <Point X="3.076930664062" Y="-1.724287109375" />
                  <Point X="3.086932861328" Y="-1.737238647461" />
                  <Point X="3.110628417969" Y="-1.760909301758" />
                  <Point X="3.604011230469" Y="-2.139495361328" />
                  <Point X="4.213122558594" Y="-2.6068828125" />
                  <Point X="4.187753417969" Y="-2.647934082031" />
                  <Point X="4.124809570313" Y="-2.749787109375" />
                  <Point X="4.028981445312" Y="-2.885945068359" />
                  <Point X="3.594627685547" Y="-2.635170898438" />
                  <Point X="2.800954589844" Y="-2.176943115234" />
                  <Point X="2.786131591797" Y="-2.170012451172" />
                  <Point X="2.754224609375" Y="-2.159825195312" />
                  <Point X="2.626504150391" Y="-2.136759033203" />
                  <Point X="2.538134277344" Y="-2.120799560547" />
                  <Point X="2.506781982422" Y="-2.120395263672" />
                  <Point X="2.474609130859" Y="-2.125353027344" />
                  <Point X="2.444833496094" Y="-2.135176757812" />
                  <Point X="2.338729003906" Y="-2.191018554688" />
                  <Point X="2.265315429688" Y="-2.229655761719" />
                  <Point X="2.242384033203" Y="-2.246549072266" />
                  <Point X="2.221424316406" Y="-2.267509033203" />
                  <Point X="2.204531982422" Y="-2.290439453125" />
                  <Point X="2.148689941406" Y="-2.396543701172" />
                  <Point X="2.110052978516" Y="-2.469957519531" />
                  <Point X="2.100229003906" Y="-2.499733398438" />
                  <Point X="2.095271240234" Y="-2.531906005859" />
                  <Point X="2.095675537109" Y="-2.563258300781" />
                  <Point X="2.118741699219" Y="-2.690978759766" />
                  <Point X="2.134701171875" Y="-2.779348632812" />
                  <Point X="2.138985595703" Y="-2.795141845703" />
                  <Point X="2.151819091797" Y="-2.826078613281" />
                  <Point X="2.4688671875" Y="-3.375221923828" />
                  <Point X="2.861283203125" Y="-4.054906494141" />
                  <Point X="2.856539794922" Y="-4.058294433594" />
                  <Point X="2.781847167969" Y="-4.111645507812" />
                  <Point X="2.701765380859" Y="-4.163481445312" />
                  <Point X="2.364192138672" Y="-3.723547851562" />
                  <Point X="1.758546020508" Y="-2.934255126953" />
                  <Point X="1.747503662109" Y="-2.922179443359" />
                  <Point X="1.721924194336" Y="-2.900557373047" />
                  <Point X="1.595957275391" Y="-2.819572265625" />
                  <Point X="1.50880078125" Y="-2.763538574219" />
                  <Point X="1.479989746094" Y="-2.751167236328" />
                  <Point X="1.448368774414" Y="-2.743435546875" />
                  <Point X="1.417099609375" Y="-2.741116699219" />
                  <Point X="1.279333251953" Y="-2.753794189453" />
                  <Point X="1.184012695312" Y="-2.762565673828" />
                  <Point X="1.15636315918" Y="-2.769397460938" />
                  <Point X="1.128978027344" Y="-2.780740722656" />
                  <Point X="1.104595825195" Y="-2.795461181641" />
                  <Point X="0.998216247559" Y="-2.883912353516" />
                  <Point X="0.924612304688" Y="-2.945111572266" />
                  <Point X="0.904141540527" Y="-2.968861572266" />
                  <Point X="0.887248962402" Y="-2.996688232422" />
                  <Point X="0.875624267578" Y="-3.025808837891" />
                  <Point X="0.843817321777" Y="-3.172145996094" />
                  <Point X="0.821809997559" Y="-3.273396728516" />
                  <Point X="0.81972454834" Y="-3.289627441406" />
                  <Point X="0.81974230957" Y="-3.323120117188" />
                  <Point X="0.909591125488" Y="-4.005590820313" />
                  <Point X="1.022065307617" Y="-4.859915039062" />
                  <Point X="0.975676269531" Y="-4.870083496094" />
                  <Point X="0.929315551758" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058437744141" Y="-4.752634765625" />
                  <Point X="-1.141246459961" Y="-4.731328613281" />
                  <Point X="-1.120775756836" Y="-4.575838378906" />
                  <Point X="-1.120077392578" Y="-4.568099609375" />
                  <Point X="-1.119451660156" Y="-4.541033691406" />
                  <Point X="-1.121759277344" Y="-4.509328125" />
                  <Point X="-1.123334106445" Y="-4.497690429688" />
                  <Point X="-1.159109008789" Y="-4.317838378906" />
                  <Point X="-1.183861572266" Y="-4.193398925781" />
                  <Point X="-1.18812512207" Y="-4.17846875" />
                  <Point X="-1.19902734375" Y="-4.149502441406" />
                  <Point X="-1.205666137695" Y="-4.135466308594" />
                  <Point X="-1.221738037109" Y="-4.10762890625" />
                  <Point X="-1.23057409668" Y="-4.094861816406" />
                  <Point X="-1.250208496094" Y="-4.070937011719" />
                  <Point X="-1.261006835938" Y="-4.059779296875" />
                  <Point X="-1.398876220703" Y="-3.93887109375" />
                  <Point X="-1.494267456055" Y="-3.855214599609" />
                  <Point X="-1.506738525391" Y="-3.845965576172" />
                  <Point X="-1.533021362305" Y="-3.829621582031" />
                  <Point X="-1.546833496094" Y="-3.822526855469" />
                  <Point X="-1.576530883789" Y="-3.810225830078" />
                  <Point X="-1.591313354492" Y="-3.805476074219" />
                  <Point X="-1.621455078125" Y="-3.798447998047" />
                  <Point X="-1.636813964844" Y="-3.796169677734" />
                  <Point X="-1.81979699707" Y="-3.784176269531" />
                  <Point X="-1.946403076172" Y="-3.775878173828" />
                  <Point X="-1.961928100586" Y="-3.776132324219" />
                  <Point X="-1.992729614258" Y="-3.779166015625" />
                  <Point X="-2.008006103516" Y="-3.781945556641" />
                  <Point X="-2.03905480957" Y="-3.790265136719" />
                  <Point X="-2.053674560547" Y="-3.795496337891" />
                  <Point X="-2.081865478516" Y="-3.808269775391" />
                  <Point X="-2.095436767578" Y="-3.815811767578" />
                  <Point X="-2.247908203125" Y="-3.917689941406" />
                  <Point X="-2.353403320312" Y="-3.988179931641" />
                  <Point X="-2.359681640625" Y="-3.992757080078" />
                  <Point X="-2.380442138672" Y="-4.010131103516" />
                  <Point X="-2.402759277344" Y="-4.032771484375" />
                  <Point X="-2.410471435547" Y="-4.041629638672" />
                  <Point X="-2.502442138672" Y="-4.161487792969" />
                  <Point X="-2.503202880859" Y="-4.162479492188" />
                  <Point X="-2.57830078125" Y="-4.11598046875" />
                  <Point X="-2.747583007812" Y="-4.011165283203" />
                  <Point X="-2.979907226562" Y="-3.832283447266" />
                  <Point X="-2.980862792969" Y="-3.831547607422" />
                  <Point X="-2.781488037109" Y="-3.486220458984" />
                  <Point X="-2.341488769531" Y="-2.724119628906" />
                  <Point X="-2.334850097656" Y="-2.710083251953" />
                  <Point X="-2.323947998047" Y="-2.681116943359" />
                  <Point X="-2.319684570312" Y="-2.666187011719" />
                  <Point X="-2.313413574219" Y="-2.634660644531" />
                  <Point X="-2.311638916016" Y="-2.619235351562" />
                  <Point X="-2.310626220703" Y="-2.588301269531" />
                  <Point X="-2.314666259766" Y="-2.557615234375" />
                  <Point X="-2.323650878906" Y="-2.527997558594" />
                  <Point X="-2.329357421875" Y="-2.513557128906" />
                  <Point X="-2.343574462891" Y="-2.484728271484" />
                  <Point X="-2.351557128906" Y="-2.47141015625" />
                  <Point X="-2.369584716797" Y="-2.446253173828" />
                  <Point X="-2.379629638672" Y="-2.434414306641" />
                  <Point X="-2.396978271484" Y="-2.417065673828" />
                  <Point X="-2.408817138672" Y="-2.407020751953" />
                  <Point X="-2.433974121094" Y="-2.388993164062" />
                  <Point X="-2.447292236328" Y="-2.381010498047" />
                  <Point X="-2.47612109375" Y="-2.366793457031" />
                  <Point X="-2.490561523438" Y="-2.361086914062" />
                  <Point X="-2.520179199219" Y="-2.352102294922" />
                  <Point X="-2.550865234375" Y="-2.348062255859" />
                  <Point X="-2.581799316406" Y="-2.349074951172" />
                  <Point X="-2.597224609375" Y="-2.350849609375" />
                  <Point X="-2.628750976562" Y="-2.357120605469" />
                  <Point X="-2.643680908203" Y="-2.361384033203" />
                  <Point X="-2.672647216797" Y="-2.372286132812" />
                  <Point X="-2.68668359375" Y="-2.378924804688" />
                  <Point X="-3.213981933594" Y="-2.683360595703" />
                  <Point X="-3.793089355469" Y="-3.017708496094" />
                  <Point X="-3.870273925781" Y="-2.916303955078" />
                  <Point X="-4.004014404297" Y="-2.740595703125" />
                  <Point X="-4.1705703125" Y="-2.461306884766" />
                  <Point X="-4.181265136719" Y="-2.443373535156" />
                  <Point X="-3.817978271484" Y="-2.16461328125" />
                  <Point X="-3.048122314453" Y="-1.573881958008" />
                  <Point X="-3.036482177734" Y="-1.563310058594" />
                  <Point X="-3.015104980469" Y="-1.540390136719" />
                  <Point X="-3.005367919922" Y="-1.528042358398" />
                  <Point X="-2.987403076172" Y="-1.500911376953" />
                  <Point X="-2.979836181641" Y="-1.487126342773" />
                  <Point X="-2.967080078125" Y="-1.458496704102" />
                  <Point X="-2.961890869141" Y="-1.443652099609" />
                  <Point X="-2.954186279297" Y="-1.413904541016" />
                  <Point X="-2.951552734375" Y="-1.398803588867" />
                  <Point X="-2.948748535156" Y="-1.36837512207" />
                  <Point X="-2.948577880859" Y="-1.353047729492" />
                  <Point X="-2.950786621094" Y="-1.321377075195" />
                  <Point X="-2.953083251953" Y="-1.306220947266" />
                  <Point X="-2.960084472656" Y="-1.276475463867" />
                  <Point X="-2.971778564453" Y="-1.248243530273" />
                  <Point X="-2.987861083984" Y="-1.222259643555" />
                  <Point X="-2.996954101562" Y="-1.209918579102" />
                  <Point X="-3.017786865234" Y="-1.185962036133" />
                  <Point X="-3.02874609375" Y="-1.175244018555" />
                  <Point X="-3.052245605469" Y="-1.155710693359" />
                  <Point X="-3.064785644531" Y="-1.146895019531" />
                  <Point X="-3.091268310547" Y="-1.13130859375" />
                  <Point X="-3.105434082031" Y="-1.124481201172" />
                  <Point X="-3.134697265625" Y="-1.113257202148" />
                  <Point X="-3.149794921875" Y="-1.108860351562" />
                  <Point X="-3.181682617188" Y="-1.102378662109" />
                  <Point X="-3.197298828125" Y="-1.100532470703" />
                  <Point X="-3.228622070312" Y="-1.09944140625" />
                  <Point X="-3.244329101562" Y="-1.100196533203" />
                  <Point X="-3.909993164062" Y="-1.187833007812" />
                  <Point X="-4.660920898438" Y="-1.286694335938" />
                  <Point X="-4.688453125" Y="-1.178906860352" />
                  <Point X="-4.740760742188" Y="-0.97412677002" />
                  <Point X="-4.784827636719" Y="-0.666014221191" />
                  <Point X="-4.786452148438" Y="-0.654654418945" />
                  <Point X="-4.385270019531" Y="-0.547157775879" />
                  <Point X="-3.508288085938" Y="-0.312171295166" />
                  <Point X="-3.496370605469" Y="-0.308114501953" />
                  <Point X="-3.473176269531" Y="-0.298460388184" />
                  <Point X="-3.461899414062" Y="-0.292863098145" />
                  <Point X="-3.429928222656" Y="-0.274257904053" />
                  <Point X="-3.423132324219" Y="-0.269906524658" />
                  <Point X="-3.40353125" Y="-0.255729675293" />
                  <Point X="-3.380363037109" Y="-0.236334823608" />
                  <Point X="-3.371844238281" Y="-0.228257461548" />
                  <Point X="-3.355867675781" Y="-0.21111366272" />
                  <Point X="-3.342127441406" Y="-0.192129760742" />
                  <Point X="-3.330833740234" Y="-0.171596618652" />
                  <Point X="-3.325822509766" Y="-0.160980636597" />
                  <Point X="-3.312929199219" Y="-0.128625579834" />
                  <Point X="-3.310407226562" Y="-0.121480377197" />
                  <Point X="-3.303996582031" Y="-0.099690177917" />
                  <Point X="-3.297966308594" Y="-0.072679748535" />
                  <Point X="-3.296220703125" Y="-0.062066589355" />
                  <Point X="-3.293940185547" Y="-0.040709476471" />
                  <Point X="-3.294088623047" Y="-0.019230493546" />
                  <Point X="-3.296664306641" Y="0.002093178511" />
                  <Point X="-3.298556640625" Y="0.012681669235" />
                  <Point X="-3.306271484375" Y="0.045120105743" />
                  <Point X="-3.308337890625" Y="0.052479167938" />
                  <Point X="-3.315706054688" Y="0.074174995422" />
                  <Point X="-3.326914794922" Y="0.101102043152" />
                  <Point X="-3.332087402344" Y="0.111640594482" />
                  <Point X="-3.343693115234" Y="0.132" />
                  <Point X="-3.357721435547" Y="0.150772415161" />
                  <Point X="-3.373958007812" Y="0.167671279907" />
                  <Point X="-3.382599609375" Y="0.175618301392" />
                  <Point X="-3.410821533203" Y="0.198520645142" />
                  <Point X="-3.417322753906" Y="0.203351928711" />
                  <Point X="-3.437600585938" Y="0.216701538086" />
                  <Point X="-3.464518066406" Y="0.231799102783" />
                  <Point X="-3.475182128906" Y="0.236934906006" />
                  <Point X="-3.497067138672" Y="0.245841140747" />
                  <Point X="-3.508288085938" Y="0.249611251831" />
                  <Point X="-4.115072753906" Y="0.412198760986" />
                  <Point X="-4.785446289062" Y="0.591824645996" />
                  <Point X="-4.765041992188" Y="0.729715270996" />
                  <Point X="-4.731331542969" Y="0.957523864746" />
                  <Point X="-4.642626464844" Y="1.284874755859" />
                  <Point X="-4.6335859375" Y="1.318236938477" />
                  <Point X="-4.406708007812" Y="1.288367919922" />
                  <Point X="-3.778066162109" Y="1.20560546875" />
                  <Point X="-3.767739013672" Y="1.204815429688" />
                  <Point X="-3.747058837891" Y="1.204364135742" />
                  <Point X="-3.736705322266" Y="1.204703125" />
                  <Point X="-3.715143798828" Y="1.20658972168" />
                  <Point X="-3.704890869141" Y="1.208053588867" />
                  <Point X="-3.684603759766" Y="1.212088989258" />
                  <Point X="-3.674570068359" Y="1.214660522461" />
                  <Point X="-3.638264160156" Y="1.226107910156" />
                  <Point X="-3.613144042969" Y="1.234028076172" />
                  <Point X="-3.603449951172" Y="1.237676757812" />
                  <Point X="-3.584517578125" Y="1.246007324219" />
                  <Point X="-3.575279296875" Y="1.250689208984" />
                  <Point X="-3.556534912109" Y="1.261511230469" />
                  <Point X="-3.547861083984" Y="1.267170776367" />
                  <Point X="-3.531179443359" Y="1.279401977539" />
                  <Point X="-3.515927734375" Y="1.293377563477" />
                  <Point X="-3.502289306641" Y="1.30892956543" />
                  <Point X="-3.49589453125" Y="1.31707800293" />
                  <Point X="-3.483480224609" Y="1.334808227539" />
                  <Point X="-3.478010986328" Y="1.343603271484" />
                  <Point X="-3.468062255859" Y="1.361737548828" />
                  <Point X="-3.463583007813" Y="1.371076171875" />
                  <Point X="-3.449015136719" Y="1.40624609375" />
                  <Point X="-3.438935546875" Y="1.430580322266" />
                  <Point X="-3.435499511719" Y="1.440351196289" />
                  <Point X="-3.429711425781" Y="1.460209106445" />
                  <Point X="-3.427359375" Y="1.470296142578" />
                  <Point X="-3.423600830078" Y="1.491611083984" />
                  <Point X="-3.422360839844" Y="1.501894042969" />
                  <Point X="-3.4210078125" Y="1.522534545898" />
                  <Point X="-3.42191015625" Y="1.543200805664" />
                  <Point X="-3.425056884766" Y="1.563644897461" />
                  <Point X="-3.427188232422" Y="1.573780273438" />
                  <Point X="-3.432790039062" Y="1.594686767578" />
                  <Point X="-3.436011962891" Y="1.604530395508" />
                  <Point X="-3.443508789062" Y="1.62380871582" />
                  <Point X="-3.447783447266" Y="1.633243286133" />
                  <Point X="-3.465361083984" Y="1.667010009766" />
                  <Point X="-3.477523193359" Y="1.690373046875" />
                  <Point X="-3.482799804688" Y="1.699286376953" />
                  <Point X="-3.494291992188" Y="1.716485717773" />
                  <Point X="-3.500508056641" Y="1.724772216797" />
                  <Point X="-3.514420654297" Y="1.741352294922" />
                  <Point X="-3.521500732422" Y="1.748911376953" />
                  <Point X="-3.536442382812" Y="1.76321484375" />
                  <Point X="-3.544303710938" Y="1.769958862305" />
                  <Point X="-3.892356689453" Y="2.037029174805" />
                  <Point X="-4.227614257812" Y="2.294282226563" />
                  <Point X="-4.133278320312" Y="2.455902587891" />
                  <Point X="-4.002293212891" Y="2.680312011719" />
                  <Point X="-3.767323242188" Y="2.982332519531" />
                  <Point X="-3.726338378906" Y="3.035012695312" />
                  <Point X="-3.640251464844" Y="2.985310302734" />
                  <Point X="-3.254156982422" Y="2.7623984375" />
                  <Point X="-3.244917480469" Y="2.757716064453" />
                  <Point X="-3.225984619141" Y="2.749385498047" />
                  <Point X="-3.216291259766" Y="2.745737304688" />
                  <Point X="-3.195648925781" Y="2.739228759766" />
                  <Point X="-3.185615234375" Y="2.736657226562" />
                  <Point X="-3.165327880859" Y="2.732621826172" />
                  <Point X="-3.15507421875" Y="2.731157958984" />
                  <Point X="-3.104510009766" Y="2.726734130859" />
                  <Point X="-3.069524902344" Y="2.723673339844" />
                  <Point X="-3.059173095703" Y="2.723334472656" />
                  <Point X="-3.038492919922" Y="2.723785644531" />
                  <Point X="-3.028164550781" Y="2.724575683594" />
                  <Point X="-3.006705810547" Y="2.727400878906" />
                  <Point X="-2.996524902344" Y="2.729310791016" />
                  <Point X="-2.976432861328" Y="2.734227539062" />
                  <Point X="-2.956996826172" Y="2.741302001953" />
                  <Point X="-2.938445068359" Y="2.750450683594" />
                  <Point X="-2.929418212891" Y="2.755531738281" />
                  <Point X="-2.9111640625" Y="2.767161132812" />
                  <Point X="-2.902744628906" Y="2.773194824219" />
                  <Point X="-2.886614013672" Y="2.786141357422" />
                  <Point X="-2.878902832031" Y="2.793054199219" />
                  <Point X="-2.843011962891" Y="2.828944824219" />
                  <Point X="-2.818179199219" Y="2.853777587891" />
                  <Point X="-2.811265869141" Y="2.861489013672" />
                  <Point X="-2.798318847656" Y="2.877620117188" />
                  <Point X="-2.79228515625" Y="2.886039794922" />
                  <Point X="-2.780655761719" Y="2.904293945312" />
                  <Point X="-2.775574707031" Y="2.913320800781" />
                  <Point X="-2.766426025391" Y="2.931872558594" />
                  <Point X="-2.7593515625" Y="2.95130859375" />
                  <Point X="-2.754434814453" Y="2.971400634766" />
                  <Point X="-2.752524902344" Y="2.981581542969" />
                  <Point X="-2.749699707031" Y="3.003040283203" />
                  <Point X="-2.748909667969" Y="3.013368652344" />
                  <Point X="-2.748458496094" Y="3.034048828125" />
                  <Point X="-2.748797363281" Y="3.044400634766" />
                  <Point X="-2.753221191406" Y="3.09496484375" />
                  <Point X="-2.756281982422" Y="3.129950195312" />
                  <Point X="-2.757745849609" Y="3.140203857422" />
                  <Point X="-2.76178125" Y="3.160491210938" />
                  <Point X="-2.764352783203" Y="3.170524902344" />
                  <Point X="-2.770861328125" Y="3.191167236328" />
                  <Point X="-2.774509521484" Y="3.200860595703" />
                  <Point X="-2.782840087891" Y="3.219793457031" />
                  <Point X="-2.787522460938" Y="3.229032958984" />
                  <Point X="-2.941755371094" Y="3.496171875" />
                  <Point X="-3.059386962891" Y="3.699916259766" />
                  <Point X="-2.876503173828" Y="3.840131591797" />
                  <Point X="-2.648374267578" Y="4.015036376953" />
                  <Point X="-2.278309082031" Y="4.22063671875" />
                  <Point X="-2.192525146484" Y="4.268296386719" />
                  <Point X="-2.118563232422" Y="4.171907226563" />
                  <Point X="-2.111820068359" Y="4.164046875" />
                  <Point X="-2.097517089844" Y="4.149105957031" />
                  <Point X="-2.089958496094" Y="4.142026367188" />
                  <Point X="-2.073378417969" Y="4.128113769531" />
                  <Point X="-2.065092773438" Y="4.121897949219" />
                  <Point X="-2.047893188477" Y="4.110405273438" />
                  <Point X="-2.038979370117" Y="4.105128417969" />
                  <Point X="-1.982701782227" Y="4.075832275391" />
                  <Point X="-1.943763061523" Y="4.055561767578" />
                  <Point X="-1.934327026367" Y="4.051286376953" />
                  <Point X="-1.915047363281" Y="4.043789306641" />
                  <Point X="-1.905203857422" Y="4.040567138672" />
                  <Point X="-1.884297119141" Y="4.034965576172" />
                  <Point X="-1.874162475586" Y="4.032834716797" />
                  <Point X="-1.853719604492" Y="4.029688232422" />
                  <Point X="-1.833053344727" Y="4.028785888672" />
                  <Point X="-1.81241394043" Y="4.030138916016" />
                  <Point X="-1.802132568359" Y="4.031378662109" />
                  <Point X="-1.780817260742" Y="4.035136962891" />
                  <Point X="-1.770731079102" Y="4.037488525391" />
                  <Point X="-1.750871704102" Y="4.043276611328" />
                  <Point X="-1.741098266602" Y="4.046713623047" />
                  <Point X="-1.682481201172" Y="4.070993896484" />
                  <Point X="-1.641924072266" Y="4.087793212891" />
                  <Point X="-1.632584960938" Y="4.092272705078" />
                  <Point X="-1.614450073242" Y="4.102222167969" />
                  <Point X="-1.605654418945" Y="4.10769140625" />
                  <Point X="-1.587924804688" Y="4.120105957031" />
                  <Point X="-1.579776245117" Y="4.126501464844" />
                  <Point X="-1.564224853516" Y="4.140140136719" />
                  <Point X="-1.550250976562" Y="4.155390136719" />
                  <Point X="-1.538020019531" Y="4.172071289062" />
                  <Point X="-1.532359863281" Y="4.180745605469" />
                  <Point X="-1.521538085938" Y="4.199489746094" />
                  <Point X="-1.516856323242" Y="4.208728027344" />
                  <Point X="-1.508525878906" Y="4.22766015625" />
                  <Point X="-1.504877197266" Y="4.237354492188" />
                  <Point X="-1.485798583984" Y="4.297864257812" />
                  <Point X="-1.472597900391" Y="4.339730957031" />
                  <Point X="-1.470026611328" Y="4.349764160156" />
                  <Point X="-1.465991088867" Y="4.370051269531" />
                  <Point X="-1.46452722168" Y="4.3803046875" />
                  <Point X="-1.46264074707" Y="4.401866210938" />
                  <Point X="-1.462301757812" Y="4.412217773438" />
                  <Point X="-1.462752807617" Y="4.432897949219" />
                  <Point X="-1.46354309082" Y="4.4432265625" />
                  <Point X="-1.479265991211" Y="4.562654785156" />
                  <Point X="-1.226502075195" Y="4.633520996094" />
                  <Point X="-0.93117578125" Y="4.716320800781" />
                  <Point X="-0.482554412842" Y="4.768824707031" />
                  <Point X="-0.365222015381" Y="4.782556640625" />
                  <Point X="-0.33988369751" Y="4.687993164062" />
                  <Point X="-0.225666397095" Y="4.261727539062" />
                  <Point X="-0.220435165405" Y="4.247107421875" />
                  <Point X="-0.207661468506" Y="4.218916015625" />
                  <Point X="-0.200119308472" Y="4.205344726562" />
                  <Point X="-0.182261230469" Y="4.178618164062" />
                  <Point X="-0.172608306885" Y="4.166455566406" />
                  <Point X="-0.151451248169" Y="4.143866699219" />
                  <Point X="-0.126897850037" Y="4.125026367188" />
                  <Point X="-0.099602661133" Y="4.110436523438" />
                  <Point X="-0.085356559753" Y="4.104260742188" />
                  <Point X="-0.054918586731" Y="4.093927978516" />
                  <Point X="-0.039857143402" Y="4.090155273438" />
                  <Point X="-0.009320039749" Y="4.08511328125" />
                  <Point X="0.02163187027" Y="4.08511328125" />
                  <Point X="0.052168972015" Y="4.090155273438" />
                  <Point X="0.067230415344" Y="4.093927978516" />
                  <Point X="0.097668388367" Y="4.104260742188" />
                  <Point X="0.111914344788" Y="4.110436523438" />
                  <Point X="0.139209686279" Y="4.125026367188" />
                  <Point X="0.16376322937" Y="4.1438671875" />
                  <Point X="0.184920150757" Y="4.166456054688" />
                  <Point X="0.194572753906" Y="4.178618164062" />
                  <Point X="0.212431137085" Y="4.205344726562" />
                  <Point X="0.219973602295" Y="4.218916503906" />
                  <Point X="0.232747146606" Y="4.247107910156" />
                  <Point X="0.237978225708" Y="4.261727539062" />
                  <Point X="0.317005187988" Y="4.556661132812" />
                  <Point X="0.378190124512" Y="4.785006347656" />
                  <Point X="0.570001647949" Y="4.764918457031" />
                  <Point X="0.827876647949" Y="4.737912109375" />
                  <Point X="1.199042724609" Y="4.648301269531" />
                  <Point X="1.453595214844" Y="4.58684375" />
                  <Point X="1.693613037109" Y="4.499788085938" />
                  <Point X="1.858256103516" Y="4.440071289062" />
                  <Point X="2.091870361328" Y="4.330817382813" />
                  <Point X="2.250451660156" Y="4.256654296875" />
                  <Point X="2.476184570312" Y="4.125141601563" />
                  <Point X="2.62943359375" Y="4.035858154297" />
                  <Point X="2.817779785156" Y="3.901916748047" />
                  <Point X="2.574574707031" Y="3.480672851563" />
                  <Point X="2.06530859375" Y="2.598597900391" />
                  <Point X="2.062373046875" Y="2.593104980469" />
                  <Point X="2.053180908203" Y="2.5734375" />
                  <Point X="2.044182128906" Y="2.549563476562" />
                  <Point X="2.041301513672" Y="2.540598632812" />
                  <Point X="2.028611938477" Y="2.493145263672" />
                  <Point X="2.01983190918" Y="2.460312255859" />
                  <Point X="2.017912719727" Y="2.451465576172" />
                  <Point X="2.013646972656" Y="2.420223388672" />
                  <Point X="2.012755615234" Y="2.383241943359" />
                  <Point X="2.013411254883" Y="2.369579833984" />
                  <Point X="2.01835925293" Y="2.328546386719" />
                  <Point X="2.021782592773" Y="2.300155029297" />
                  <Point X="2.023800537109" Y="2.28903515625" />
                  <Point X="2.029143066406" Y="2.267112304688" />
                  <Point X="2.032467895508" Y="2.256309326172" />
                  <Point X="2.040734008789" Y="2.234220458984" />
                  <Point X="2.045318115234" Y="2.223889160156" />
                  <Point X="2.055680908203" Y="2.203843994141" />
                  <Point X="2.061459472656" Y="2.194130126953" />
                  <Point X="2.086849609375" Y="2.156711425781" />
                  <Point X="2.104417236328" Y="2.130821533203" />
                  <Point X="2.109808105469" Y="2.123633300781" />
                  <Point X="2.130464355469" Y="2.100121826172" />
                  <Point X="2.157597900391" Y="2.075386474609" />
                  <Point X="2.168257568359" Y="2.066981445312" />
                  <Point X="2.205676025391" Y="2.041591308594" />
                  <Point X="2.231566162109" Y="2.024023925781" />
                  <Point X="2.241279296875" Y="2.018245361328" />
                  <Point X="2.261324462891" Y="2.007882446289" />
                  <Point X="2.271656494141" Y="2.003297973633" />
                  <Point X="2.293744628906" Y="1.995032104492" />
                  <Point X="2.304548095703" Y="1.991707275391" />
                  <Point X="2.326471191406" Y="1.986364746094" />
                  <Point X="2.337590820312" Y="1.984346801758" />
                  <Point X="2.378624511719" Y="1.979398803711" />
                  <Point X="2.407015625" Y="1.975975219727" />
                  <Point X="2.416044189453" Y="1.975320922852" />
                  <Point X="2.447575683594" Y="1.975497314453" />
                  <Point X="2.484314453125" Y="1.979822631836" />
                  <Point X="2.497748291016" Y="1.982395996094" />
                  <Point X="2.545201660156" Y="1.995085449219" />
                  <Point X="2.578034667969" Y="2.003865600586" />
                  <Point X="2.583994384766" Y="2.005670532227" />
                  <Point X="2.604404541016" Y="2.013067260742" />
                  <Point X="2.627655029297" Y="2.02357409668" />
                  <Point X="2.636033935547" Y="2.027872558594" />
                  <Point X="3.246342285156" Y="2.380234130859" />
                  <Point X="3.940404785156" Y="2.780951416016" />
                  <Point X="3.953048828125" Y="2.763379150391" />
                  <Point X="4.043951904297" Y="2.637044189453" />
                  <Point X="4.136884765625" Y="2.483472412109" />
                  <Point X="3.843448242188" Y="2.258310302734" />
                  <Point X="3.172951660156" Y="1.743819702148" />
                  <Point X="3.168136962891" Y="1.739868164062" />
                  <Point X="3.152118652344" Y="1.725215942383" />
                  <Point X="3.134668701172" Y="1.706603149414" />
                  <Point X="3.128576416016" Y="1.699422485352" />
                  <Point X="3.094424072266" Y="1.654868408203" />
                  <Point X="3.070794189453" Y="1.624041137695" />
                  <Point X="3.065635742188" Y="1.616602661133" />
                  <Point X="3.049738769531" Y="1.589370849609" />
                  <Point X="3.034762939453" Y="1.555545288086" />
                  <Point X="3.030140136719" Y="1.54267199707" />
                  <Point X="3.017418457031" Y="1.497182006836" />
                  <Point X="3.008616210938" Y="1.465707519531" />
                  <Point X="3.006225097656" Y="1.454661010742" />
                  <Point X="3.002771728516" Y="1.432362670898" />
                  <Point X="3.001709472656" Y="1.421110839844" />
                  <Point X="3.000893310547" Y="1.397540405273" />
                  <Point X="3.001174804688" Y="1.386241821289" />
                  <Point X="3.003077636719" Y="1.363757202148" />
                  <Point X="3.004698974609" Y="1.352571166992" />
                  <Point X="3.015142089844" Y="1.301957763672" />
                  <Point X="3.022367919922" Y="1.266938354492" />
                  <Point X="3.024598144531" Y="1.258233398438" />
                  <Point X="3.034684326172" Y="1.228607543945" />
                  <Point X="3.050286865234" Y="1.195370849609" />
                  <Point X="3.056918945312" Y="1.183525634766" />
                  <Point X="3.085323486328" Y="1.140352050781" />
                  <Point X="3.1049765625" Y="1.110480224609" />
                  <Point X="3.111739501953" Y="1.101424194336" />
                  <Point X="3.126292724609" Y="1.0841796875" />
                  <Point X="3.134083007812" Y="1.075991210938" />
                  <Point X="3.151326904297" Y="1.059901367188" />
                  <Point X="3.160034912109" Y="1.052695678711" />
                  <Point X="3.178244628906" Y="1.039370117188" />
                  <Point X="3.187746337891" Y="1.03325012207" />
                  <Point X="3.228908203125" Y="1.010079284668" />
                  <Point X="3.257388427734" Y="0.994047485352" />
                  <Point X="3.265479003906" Y="0.989988037109" />
                  <Point X="3.294678222656" Y="0.978084228516" />
                  <Point X="3.330275146484" Y="0.968021240234" />
                  <Point X="3.343670898438" Y="0.965257629395" />
                  <Point X="3.399324707031" Y="0.957902282715" />
                  <Point X="3.437831542969" Y="0.952812988281" />
                  <Point X="3.444029785156" Y="0.952199768066" />
                  <Point X="3.465716064453" Y="0.951222839355" />
                  <Point X="3.491217529297" Y="0.952032226562" />
                  <Point X="3.500603515625" Y="0.952797241211" />
                  <Point X="4.08035546875" Y="1.029123046875" />
                  <Point X="4.704703613281" Y="1.11132019043" />
                  <Point X="4.713641601562" Y="1.074605224609" />
                  <Point X="4.75268359375" Y="0.914233886719" />
                  <Point X="4.78387109375" Y="0.713920959473" />
                  <Point X="4.460395507812" Y="0.627245849609" />
                  <Point X="3.691991943359" Y="0.421352905273" />
                  <Point X="3.68603125" Y="0.419544158936" />
                  <Point X="3.665625488281" Y="0.412137390137" />
                  <Point X="3.642381103516" Y="0.40161920166" />
                  <Point X="3.634004882812" Y="0.397316711426" />
                  <Point X="3.579326660156" Y="0.365711761475" />
                  <Point X="3.541494873047" Y="0.343844268799" />
                  <Point X="3.533882324219" Y="0.338945953369" />
                  <Point X="3.508773925781" Y="0.319870361328" />
                  <Point X="3.481993896484" Y="0.294350616455" />
                  <Point X="3.472797119141" Y="0.284226715088" />
                  <Point X="3.439990234375" Y="0.242422958374" />
                  <Point X="3.417291259766" Y="0.213499008179" />
                  <Point X="3.410854980469" Y="0.204208892822" />
                  <Point X="3.399130859375" Y="0.184929077148" />
                  <Point X="3.393843017578" Y="0.17493939209" />
                  <Point X="3.384069091797" Y="0.153475570679" />
                  <Point X="3.380005126953" Y="0.142928543091" />
                  <Point X="3.373158935547" Y="0.12142741394" />
                  <Point X="3.370376708984" Y="0.110473312378" />
                  <Point X="3.359440917969" Y="0.053371646881" />
                  <Point X="3.351874511719" Y="0.013862923622" />
                  <Point X="3.350603759766" Y="0.004969031811" />
                  <Point X="3.348584228516" Y="-0.02626288414" />
                  <Point X="3.350280029297" Y="-0.062941532135" />
                  <Point X="3.351874511719" Y="-0.076422966003" />
                  <Point X="3.362810302734" Y="-0.133524780273" />
                  <Point X="3.370376708984" Y="-0.173033355713" />
                  <Point X="3.373158935547" Y="-0.183987304688" />
                  <Point X="3.380005126953" Y="-0.205488586426" />
                  <Point X="3.384069091797" Y="-0.216035766602" />
                  <Point X="3.393843017578" Y="-0.237499588013" />
                  <Point X="3.399130615234" Y="-0.247488967896" />
                  <Point X="3.410854736328" Y="-0.266768920898" />
                  <Point X="3.417291259766" Y="-0.276059509277" />
                  <Point X="3.450098144531" Y="-0.317862976074" />
                  <Point X="3.472797119141" Y="-0.34678704834" />
                  <Point X="3.478718505859" Y="-0.353633666992" />
                  <Point X="3.501139648438" Y="-0.375805236816" />
                  <Point X="3.530176513672" Y="-0.398724822998" />
                  <Point X="3.541494873047" Y="-0.406404327393" />
                  <Point X="3.596173095703" Y="-0.438009277344" />
                  <Point X="3.634004882812" Y="-0.45987677002" />
                  <Point X="3.63949609375" Y="-0.462815032959" />
                  <Point X="3.659157958984" Y="-0.472016723633" />
                  <Point X="3.683027587891" Y="-0.481027740479" />
                  <Point X="3.691991943359" Y="-0.483913085938" />
                  <Point X="4.223650878906" Y="-0.626370361328" />
                  <Point X="4.784876953125" Y="-0.776750488281" />
                  <Point X="4.783413085938" Y="-0.786460571289" />
                  <Point X="4.76161328125" Y="-0.931052978516" />
                  <Point X="4.727801757812" Y="-1.079219604492" />
                  <Point X="4.3279140625" Y="-1.026573242188" />
                  <Point X="3.436781982422" Y="-0.909253723145" />
                  <Point X="3.428624511719" Y="-0.908535705566" />
                  <Point X="3.400098632812" Y="-0.908042480469" />
                  <Point X="3.366721435547" Y="-0.910840881348" />
                  <Point X="3.354480957031" Y="-0.912676208496" />
                  <Point X="3.247167236328" Y="-0.936001220703" />
                  <Point X="3.172916748047" Y="-0.952139892578" />
                  <Point X="3.157873535156" Y="-0.956742614746" />
                  <Point X="3.128752929688" Y="-0.96836730957" />
                  <Point X="3.114675537109" Y="-0.975389465332" />
                  <Point X="3.086849121094" Y="-0.992281738281" />
                  <Point X="3.074123779297" Y="-1.001530578613" />
                  <Point X="3.050374023438" Y="-1.022001159668" />
                  <Point X="3.039349609375" Y="-1.033222900391" />
                  <Point X="2.974485107422" Y="-1.111234619141" />
                  <Point X="2.92960546875" Y="-1.16521105957" />
                  <Point X="2.921326171875" Y="-1.176847167969" />
                  <Point X="2.90660546875" Y="-1.201229370117" />
                  <Point X="2.900163818359" Y="-1.213975830078" />
                  <Point X="2.888820556641" Y="-1.241361328125" />
                  <Point X="2.884362792969" Y="-1.254928222656" />
                  <Point X="2.877531005859" Y="-1.282577880859" />
                  <Point X="2.875157226562" Y="-1.296660400391" />
                  <Point X="2.865860595703" Y="-1.397688842773" />
                  <Point X="2.859428222656" Y="-1.467590576172" />
                  <Point X="2.859288818359" Y="-1.483321411133" />
                  <Point X="2.861607666016" Y="-1.514590576172" />
                  <Point X="2.864065917969" Y="-1.530128662109" />
                  <Point X="2.871797607422" Y="-1.561749633789" />
                  <Point X="2.876786621094" Y="-1.576669067383" />
                  <Point X="2.889157958984" Y="-1.605479980469" />
                  <Point X="2.896540283203" Y="-1.61937121582" />
                  <Point X="2.955929199219" Y="-1.711747192383" />
                  <Point X="2.997020507812" Y="-1.775661621094" />
                  <Point X="3.0017421875" Y="-1.782353515625" />
                  <Point X="3.01979296875" Y="-1.80444909668" />
                  <Point X="3.043488525391" Y="-1.828119750977" />
                  <Point X="3.052796142578" Y="-1.836277832031" />
                  <Point X="3.546178955078" Y="-2.214864013672" />
                  <Point X="4.087170898438" Y="-2.629981689453" />
                  <Point X="4.045489013672" Y="-2.6974296875" />
                  <Point X="4.001274658203" Y="-2.760251953125" />
                  <Point X="3.642127685547" Y="-2.5528984375" />
                  <Point X="2.848454589844" Y="-2.094670654297" />
                  <Point X="2.841191894531" Y="-2.090885253906" />
                  <Point X="2.815026123047" Y="-2.079513183594" />
                  <Point X="2.783119140625" Y="-2.069325927734" />
                  <Point X="2.771108398438" Y="-2.066337646484" />
                  <Point X="2.643387939453" Y="-2.043271362305" />
                  <Point X="2.555018066406" Y="-2.027311889648" />
                  <Point X="2.539359130859" Y="-2.025807495117" />
                  <Point X="2.508006835938" Y="-2.025403198242" />
                  <Point X="2.492313476562" Y="-2.026503540039" />
                  <Point X="2.460140625" Y="-2.031461303711" />
                  <Point X="2.444844482422" Y="-2.035136352539" />
                  <Point X="2.415068847656" Y="-2.044960083008" />
                  <Point X="2.400589355469" Y="-2.051108642578" />
                  <Point X="2.294484863281" Y="-2.106950439453" />
                  <Point X="2.221071289062" Y="-2.145587646484" />
                  <Point X="2.208968994141" Y="-2.153169921875" />
                  <Point X="2.186037597656" Y="-2.170063232422" />
                  <Point X="2.175208496094" Y="-2.179374267578" />
                  <Point X="2.154248779297" Y="-2.200334228516" />
                  <Point X="2.144937988281" Y="-2.211163330078" />
                  <Point X="2.128045654297" Y="-2.23409375" />
                  <Point X="2.120464111328" Y="-2.246195068359" />
                  <Point X="2.064622070313" Y="-2.352299316406" />
                  <Point X="2.025984985352" Y="-2.425713134766" />
                  <Point X="2.019836425781" Y="-2.440192382812" />
                  <Point X="2.010012451172" Y="-2.469968261719" />
                  <Point X="2.006337280273" Y="-2.485264892578" />
                  <Point X="2.001379516602" Y="-2.5174375" />
                  <Point X="2.000279174805" Y="-2.533130859375" />
                  <Point X="2.00068347168" Y="-2.564483154297" />
                  <Point X="2.002187866211" Y="-2.580142089844" />
                  <Point X="2.02525402832" Y="-2.707862548828" />
                  <Point X="2.041213378906" Y="-2.796232421875" />
                  <Point X="2.043015014648" Y="-2.804221435547" />
                  <Point X="2.051236083984" Y="-2.83154296875" />
                  <Point X="2.064069580078" Y="-2.862479736328" />
                  <Point X="2.069546630859" Y="-2.873578613281" />
                  <Point X="2.386594726562" Y="-3.422721923828" />
                  <Point X="2.735892822266" Y="-4.027724365234" />
                  <Point X="2.723754394531" Y="-4.036083251953" />
                  <Point X="2.439560791016" Y="-3.665715576172" />
                  <Point X="1.833914550781" Y="-2.876422851562" />
                  <Point X="1.828653686523" Y="-2.870146484375" />
                  <Point X="1.808831665039" Y="-2.849626953125" />
                  <Point X="1.783252197266" Y="-2.828004882812" />
                  <Point X="1.773299072266" Y="-2.820647216797" />
                  <Point X="1.64733215332" Y="-2.739662109375" />
                  <Point X="1.56017565918" Y="-2.683628417969" />
                  <Point X="1.546283935547" Y="-2.676245849609" />
                  <Point X="1.517472900391" Y="-2.663874511719" />
                  <Point X="1.502553588867" Y="-2.658885742188" />
                  <Point X="1.470932617188" Y="-2.651154052734" />
                  <Point X="1.45539440918" Y="-2.648695800781" />
                  <Point X="1.42412512207" Y="-2.646376953125" />
                  <Point X="1.408394287109" Y="-2.646516357422" />
                  <Point X="1.270627929688" Y="-2.659193847656" />
                  <Point X="1.175307373047" Y="-2.667965332031" />
                  <Point X="1.161224975586" Y="-2.670339111328" />
                  <Point X="1.133575439453" Y="-2.677170898438" />
                  <Point X="1.120008422852" Y="-2.68162890625" />
                  <Point X="1.092623168945" Y="-2.692972167969" />
                  <Point X="1.079877563477" Y="-2.699413330078" />
                  <Point X="1.055495361328" Y="-2.714133789062" />
                  <Point X="1.043858764648" Y="-2.722413085938" />
                  <Point X="0.937479248047" Y="-2.810864257813" />
                  <Point X="0.863875244141" Y="-2.872063476562" />
                  <Point X="0.852653320312" Y="-2.883088134766" />
                  <Point X="0.832182556152" Y="-2.906838134766" />
                  <Point X="0.822933898926" Y="-2.919563232422" />
                  <Point X="0.806041259766" Y="-2.947389892578" />
                  <Point X="0.799019165039" Y="-2.961467529297" />
                  <Point X="0.787394470215" Y="-2.990588134766" />
                  <Point X="0.782791748047" Y="-3.005631347656" />
                  <Point X="0.750984802246" Y="-3.151968505859" />
                  <Point X="0.728977478027" Y="-3.253219238281" />
                  <Point X="0.727584594727" Y="-3.261289794922" />
                  <Point X="0.724724487305" Y="-3.289677734375" />
                  <Point X="0.72474230957" Y="-3.323170410156" />
                  <Point X="0.725554992676" Y="-3.335520019531" />
                  <Point X="0.815403930664" Y="-4.017990722656" />
                  <Point X="0.833091430664" Y="-4.152340332031" />
                  <Point X="0.655064941406" Y="-3.487936523438" />
                  <Point X="0.652606262207" Y="-3.480124755859" />
                  <Point X="0.642145568848" Y="-3.453581542969" />
                  <Point X="0.626787475586" Y="-3.42381640625" />
                  <Point X="0.620407714844" Y="-3.413210205078" />
                  <Point X="0.523636047363" Y="-3.273781005859" />
                  <Point X="0.45667980957" Y="-3.177309814453" />
                  <Point X="0.446670806885" Y="-3.165173095703" />
                  <Point X="0.424787109375" Y="-3.142717773438" />
                  <Point X="0.412912139893" Y="-3.132399169922" />
                  <Point X="0.386656860352" Y="-3.113155273438" />
                  <Point X="0.373242919922" Y="-3.104938232422" />
                  <Point X="0.345241760254" Y="-3.090829833984" />
                  <Point X="0.330654724121" Y="-3.084938476562" />
                  <Point X="0.180906433105" Y="-3.038462158203" />
                  <Point X="0.077295753479" Y="-3.006305175781" />
                  <Point X="0.063376625061" Y="-3.003109130859" />
                  <Point X="0.03521704483" Y="-2.99883984375" />
                  <Point X="0.020976739883" Y="-2.997766601562" />
                  <Point X="-0.008664910316" Y="-2.997766601562" />
                  <Point X="-0.02290521431" Y="-2.998840087891" />
                  <Point X="-0.051064647675" Y="-3.003109375" />
                  <Point X="-0.064983627319" Y="-3.006305175781" />
                  <Point X="-0.214731765747" Y="-3.05278125" />
                  <Point X="-0.318342590332" Y="-3.084938476562" />
                  <Point X="-0.332929626465" Y="-3.090829589844" />
                  <Point X="-0.360930786133" Y="-3.104937988281" />
                  <Point X="-0.374345031738" Y="-3.113155273438" />
                  <Point X="-0.400600311279" Y="-3.132399169922" />
                  <Point X="-0.412475280762" Y="-3.142717773438" />
                  <Point X="-0.434358978271" Y="-3.165173095703" />
                  <Point X="-0.444367980957" Y="-3.177309814453" />
                  <Point X="-0.541139465332" Y="-3.316739013672" />
                  <Point X="-0.608095703125" Y="-3.413210205078" />
                  <Point X="-0.61247088623" Y="-3.420133300781" />
                  <Point X="-0.625976379395" Y="-3.445264648438" />
                  <Point X="-0.638777709961" Y="-3.476215332031" />
                  <Point X="-0.642753112793" Y="-3.487936523438" />
                  <Point X="-0.80657019043" Y="-4.099310546875" />
                  <Point X="-0.985425109863" Y="-4.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.060865881731" Y="2.425140981348" />
                  <Point X="3.876680174441" Y="2.744159984403" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.984846997838" Y="2.366809550586" />
                  <Point X="3.79440776693" Y="2.696659974273" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.767042809431" Y="0.822006663439" />
                  <Point X="4.607403335964" Y="1.098510342378" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.908828113944" Y="2.308478119825" />
                  <Point X="3.712135359419" Y="2.649159964143" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.728340231551" Y="0.699041494712" />
                  <Point X="4.505455791618" Y="1.085088668892" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.83280922807" Y="2.250146692494" />
                  <Point X="3.629862951908" Y="2.601659954012" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.633340244449" Y="0.67358629909" />
                  <Point X="4.403508247272" Y="1.071666995406" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.756790330022" Y="2.191815286249" />
                  <Point X="3.547590544397" Y="2.554159943882" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.793621297813" Y="3.860072986312" />
                  <Point X="2.735806043219" Y="3.96021194472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.538340257348" Y="0.648131103468" />
                  <Point X="4.301560702926" Y="1.05824532192" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.680771431974" Y="2.133483880003" />
                  <Point X="3.465318136887" Y="2.506659933752" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.738773044668" Y="3.765072947464" />
                  <Point X="2.558613699855" Y="4.077118086139" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.443340266042" Y="0.622675915128" />
                  <Point X="4.19961315858" Y="1.044823648433" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.604752533926" Y="2.075152473758" />
                  <Point X="3.383045729376" Y="2.459159923622" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.683924791524" Y="3.670072908616" />
                  <Point X="2.393316880752" Y="4.173420575155" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.348340255519" Y="0.597220760073" />
                  <Point X="4.097665614235" Y="1.031401974947" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.528733635877" Y="2.016821067512" />
                  <Point X="3.300773321865" Y="2.411659913492" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.62907653838" Y="3.575072869768" />
                  <Point X="2.230059212062" Y="4.26619115205" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.253340244997" Y="0.571765605017" />
                  <Point X="3.995718059931" Y="1.017980318709" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.452714737829" Y="1.958489661267" />
                  <Point X="3.218500910051" Y="2.364159910815" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.574228285127" Y="3.480072831109" />
                  <Point X="2.079788482466" Y="4.336467690601" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.158340234475" Y="0.546310449962" />
                  <Point X="3.89377050359" Y="1.004558665998" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.376695839781" Y="1.900158255022" />
                  <Point X="3.136228489822" Y="2.316659922712" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.519380014789" Y="3.385072822041" />
                  <Point X="1.929517540233" Y="4.40674459745" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.063340223952" Y="0.520855294906" />
                  <Point X="3.79182294725" Y="0.991137013288" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.300676941733" Y="1.841826848776" />
                  <Point X="3.053956069594" Y="2.269159934609" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.464531744452" Y="3.290072812973" />
                  <Point X="1.785302849073" Y="4.466531769737" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.767885269428" Y="-0.88945252008" />
                  <Point X="4.687798649906" Y="-0.750738426061" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.96834021343" Y="0.495400139851" />
                  <Point X="3.689875390909" Y="0.977715360577" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.224658043685" Y="1.783495442531" />
                  <Point X="2.971683649366" Y="2.221659946506" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.409683474114" Y="3.195072803904" />
                  <Point X="1.646550488735" Y="4.516857907514" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.739118681558" Y="-1.029627328328" />
                  <Point X="4.558026228082" Y="-0.71596599804" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.873340202907" Y="0.469944984795" />
                  <Point X="3.587927834569" Y="0.964293707866" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.149946711328" Y="1.722899266074" />
                  <Point X="2.889411229138" Y="2.174159958403" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.354835203777" Y="3.100072794836" />
                  <Point X="1.507798101386" Y="4.567184092073" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.652316633769" Y="-1.069281771357" />
                  <Point X="4.428253806258" Y="-0.68119357002" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.778340192385" Y="0.444489829739" />
                  <Point X="3.485416748973" Y="0.951848116457" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.085927471676" Y="1.643783841815" />
                  <Point X="2.80713880891" Y="2.1266599703" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.299986933439" Y="3.005072785768" />
                  <Point X="1.375924366463" Y="4.605596101144" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.53359616094" Y="-1.053651880518" />
                  <Point X="4.298481384434" Y="-0.646421142" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.683563236244" Y="0.418648333162" />
                  <Point X="3.369986174228" Y="0.961779736663" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.031853377011" Y="1.547442921147" />
                  <Point X="2.724866388681" Y="2.079159982197" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.245138663102" Y="2.9100727767" />
                  <Point X="1.248460389297" Y="4.63637018573" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.414875688111" Y="-1.038021989679" />
                  <Point X="4.168708987866" Y="-0.611648757723" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.59814845174" Y="0.37659107964" />
                  <Point X="3.234086933541" Y="1.007164126242" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.001335198652" Y="1.41030195662" />
                  <Point X="2.642593968453" Y="2.031659994093" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.190290392764" Y="2.815072767632" />
                  <Point X="1.120996569498" Y="4.667143997749" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.296155228104" Y="-1.02239212105" />
                  <Point X="4.038936625694" Y="-0.576876433024" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.517411098869" Y="0.326432276881" />
                  <Point X="2.552835171507" Y="1.997126790831" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.135442122427" Y="2.720072758564" />
                  <Point X="0.993532849341" Y="4.697917637182" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.177434803209" Y="-1.006762313235" />
                  <Point X="3.909164263523" Y="-0.542104108325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.449339368005" Y="0.254335973297" />
                  <Point X="2.455114006315" Y="1.976384813918" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.080593852089" Y="2.625072749495" />
                  <Point X="0.866069129185" Y="4.728691276615" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.058714378314" Y="-0.99113250542" />
                  <Point X="3.779391901352" Y="-0.507331783626" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.390156503536" Y="0.166843701495" />
                  <Point X="2.341062279129" Y="1.983928200095" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.034453514095" Y="2.51499015918" />
                  <Point X="0.746104677457" Y="4.746475802108" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.939993953418" Y="-0.975502697605" />
                  <Point X="3.645658722899" Y="-0.465699123888" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.356086277117" Y="0.035855064679" />
                  <Point X="2.193177519702" Y="2.050072117087" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.014652460674" Y="2.359286589749" />
                  <Point X="0.629348601375" Y="4.758703257975" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.821273528523" Y="-0.95987288979" />
                  <Point X="3.452030648095" Y="-0.320325460557" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.373035738494" Y="-0.183502263587" />
                  <Point X="0.51259249954" Y="4.770930758448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.702553103628" Y="-0.944243081975" />
                  <Point X="0.395836371082" Y="4.783158305033" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.583832678732" Y="-0.92861327416" />
                  <Point X="0.348673125109" Y="4.674847443307" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.465112253837" Y="-0.912983466345" />
                  <Point X="0.313900711332" Y="4.545075030671" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.355177968781" Y="-0.912571699135" />
                  <Point X="0.279128374904" Y="4.415302484062" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.257688192867" Y="-0.933714454033" />
                  <Point X="0.244356038476" Y="4.285529937453" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.160774501018" Y="-0.955855015802" />
                  <Point X="0.195544976612" Y="4.180073176572" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.051221244052" Y="-2.688154016171" />
                  <Point X="3.962327972055" Y="-2.534186352621" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.076465931953" Y="-0.999828290667" />
                  <Point X="0.122718852221" Y="4.116211724136" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.974088360839" Y="-2.744555943512" />
                  <Point X="3.765380430572" Y="-2.383063204347" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.007892618164" Y="-1.071055827141" />
                  <Point X="0.030163687068" Y="4.086521972684" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.359618698347" Y="4.761644868118" />
                  <Point X="-0.371282602699" Y="4.78184734307" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.80954356813" Y="-2.649556002419" />
                  <Point X="3.56843288909" Y="-2.231940056074" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.943150960769" Y="-1.148919987167" />
                  <Point X="-0.091249184435" Y="4.106815234821" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.264618819751" Y="4.407100251677" />
                  <Point X="-0.474036111957" Y="4.769821641762" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.644998775421" Y="-2.554556061327" />
                  <Point X="3.371485247378" Y="-2.080816734198" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.888096935633" Y="-1.24356361847" />
                  <Point X="-0.576789649916" Y="4.757795990164" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.480453877364" Y="-2.459555937764" />
                  <Point X="3.174537592898" Y="-1.929693390207" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.86607010758" Y="-1.395412033153" />
                  <Point X="-0.679543190469" Y="4.745770343059" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.315908977435" Y="-2.364555810962" />
                  <Point X="-0.782296731022" Y="4.733744695955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.151364077506" Y="-2.269555684159" />
                  <Point X="-0.885050271575" Y="4.72171904885" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.986819177578" Y="-2.174555557356" />
                  <Point X="-0.983207727649" Y="4.701732749911" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.824669711855" Y="-2.083704444305" />
                  <Point X="-1.077621512331" Y="4.675262221916" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.697244719783" Y="-2.052997883881" />
                  <Point X="-1.172035297014" Y="4.648791693921" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.57477877502" Y="-2.030880645356" />
                  <Point X="-1.266449120902" Y="4.622321233833" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.464986350802" Y="-2.030714588323" />
                  <Point X="-1.360862998247" Y="4.595850866335" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.37487708815" Y="-2.064640767177" />
                  <Point X="-1.455276875592" Y="4.569380498836" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.29074455514" Y="-2.108918945435" />
                  <Point X="-1.463380104343" Y="4.393415702737" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.207304340433" Y="-2.154396254168" />
                  <Point X="-1.49714086553" Y="4.261891056415" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.136777682414" Y="-2.22224049919" />
                  <Point X="-1.547508531136" Y="4.159130412303" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.082527822625" Y="-2.318276985733" />
                  <Point X="-1.621967808308" Y="4.09809766346" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.030216987908" Y="-2.417671962216" />
                  <Point X="-1.70954395681" Y="4.059784002197" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.000578417614" Y="-2.556336452604" />
                  <Point X="-1.802794588729" Y="4.031298834518" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.054972049334" Y="-2.840548986351" />
                  <Point X="-1.921050201596" Y="4.046123564284" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.367362292003" Y="-3.571624758443" />
                  <Point X="-2.082505611622" Y="4.135772537607" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.924292961674" Y="-2.994206167036" />
                  <Point X="-2.250211264114" Y="4.236247248438" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.679620045722" Y="-2.760420245372" />
                  <Point X="-2.333266679241" Y="4.190103447283" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.513395650361" Y="-2.66251114715" />
                  <Point X="-2.416322085626" Y="4.143959630984" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.395167239384" Y="-2.647733532438" />
                  <Point X="-2.49937749201" Y="4.097815814684" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.291004711649" Y="-2.657318742158" />
                  <Point X="-2.582432898394" Y="4.051671998385" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.186842149026" Y="-2.666903891448" />
                  <Point X="-2.664042372552" Y="4.003023754006" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.092292580648" Y="-2.693139235184" />
                  <Point X="-2.740080586718" Y="3.944725804258" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.013885777261" Y="-2.747334668059" />
                  <Point X="-2.816118800884" Y="3.886427854511" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.939768905605" Y="-2.808960480651" />
                  <Point X="-2.892157026988" Y="3.82812992544" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.865651972781" Y="-2.870586187299" />
                  <Point X="-2.968195299139" Y="3.769832076126" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.803379229698" Y="-2.952726632353" />
                  <Point X="-3.044233571291" Y="3.711534226812" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.766774184483" Y="-3.079324834228" />
                  <Point X="-2.749135967549" Y="3.01041018394" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.736771822398" Y="-3.21735921875" />
                  <Point X="-2.789526893227" Y="2.890369319378" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.734493209935" Y="-3.403412546193" />
                  <Point X="-2.856165721273" Y="2.815791155312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.766895924621" Y="-3.649535694332" />
                  <Point X="-2.930665981743" Y="2.754829391623" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.799298639306" Y="-3.89565884247" />
                  <Point X="0.734016075393" Y="-3.782586124924" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.331403651169" Y="-3.085240950408" />
                  <Point X="-3.023267762354" Y="2.725220380513" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.831701407929" Y="-4.141782084029" />
                  <Point X="0.829016100583" Y="-4.137130995273" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.197710428029" Y="-3.043677495303" />
                  <Point X="-3.135398517377" Y="2.729436545303" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.064777422279" Y="-3.00343077534" />
                  <Point X="-3.269109951326" Y="2.771031542455" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.045584391602" Y="-3.002278506484" />
                  <Point X="-3.433654819336" Y="2.866031613973" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.139587985901" Y="-3.029459505063" />
                  <Point X="-3.598199687346" Y="2.961031685491" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.232615196347" Y="-3.058331650084" />
                  <Point X="-3.740270299962" Y="3.017105204804" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.325322738988" Y="-3.087757475986" />
                  <Point X="-3.803238245704" Y="2.936168886078" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.406357248936" Y="-3.13740158759" />
                  <Point X="-3.866206182265" Y="2.85523255145" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.470922971218" Y="-3.215570476169" />
                  <Point X="-3.929174118827" Y="2.774296216823" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.530805861692" Y="-3.301850267365" />
                  <Point X="-3.992142055389" Y="2.693359882195" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.59068873825" Y="-3.388130082663" />
                  <Point X="-3.421502787438" Y="1.514983677311" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.588127372076" Y="1.803585923694" />
                  <Point X="-4.04855044976" Y="2.60106208722" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.642755934345" Y="-3.487947053619" />
                  <Point X="-3.45712114333" Y="1.386676479398" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.785074852716" Y="1.954708966585" />
                  <Point X="-4.103698128368" Y="2.506580668488" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.677528333465" Y="-3.617719491642" />
                  <Point X="-3.514133069918" Y="1.295424032885" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.982022610027" Y="2.105832488684" />
                  <Point X="-4.158845814778" Y="2.41209926327" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.712300732585" Y="-3.747491929664" />
                  <Point X="-3.593114799077" Y="1.242224400659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.178970698365" Y="2.256956584139" />
                  <Point X="-4.213993510214" Y="2.317617873685" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.747073131705" Y="-3.877264367687" />
                  <Point X="-3.685329334867" Y="1.211944661843" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.781845530826" Y="-4.00703680571" />
                  <Point X="-3.792460025447" Y="1.207500460978" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.816617929462" Y="-4.13680924457" />
                  <Point X="-3.911180492224" Y="1.223130341333" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.851390326909" Y="-4.266581685492" />
                  <Point X="-3.293973397389" Y="-0.035903705712" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.440875656147" Y="0.218538470203" />
                  <Point X="-4.029900959" Y="1.238760221688" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.886162724355" Y="-4.396354126414" />
                  <Point X="-3.328356938064" Y="-0.16634966632" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.579533846925" Y="0.268701501517" />
                  <Point X="-4.148621425777" Y="1.254390102044" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.920935121801" Y="-4.526126567336" />
                  <Point X="-3.392015628404" Y="-0.246089580307" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.709306267208" Y="0.303473926867" />
                  <Point X="-4.267341892553" Y="1.270019982399" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.955707519248" Y="-4.655899008258" />
                  <Point X="-1.163430663546" Y="-4.296111968425" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.339743594307" Y="-3.990729014316" />
                  <Point X="-3.471854622546" Y="-0.297804386027" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.83907868749" Y="0.338246352218" />
                  <Point X="-4.38606235933" Y="1.285649862754" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.003384085436" Y="-4.763320773289" />
                  <Point X="-1.119924068307" Y="-4.561467601843" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.546432908392" Y="-3.822732620939" />
                  <Point X="-2.95077312462" Y="-1.390344015319" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.103819936436" Y="-1.125259161318" />
                  <Point X="-3.564552330378" Y="-0.327247246317" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.968851107773" Y="0.373018777568" />
                  <Point X="-4.504782800918" Y="1.301279699481" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.129859855441" Y="-4.734258313714" />
                  <Point X="-1.139446197815" Y="-4.717654281663" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.672828432282" Y="-3.793809151712" />
                  <Point X="-2.32106679529" Y="-2.671027371567" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.502017395965" Y="-2.357611737538" />
                  <Point X="-2.992334086119" Y="-1.508358318393" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.228418277034" Y="-1.099448504864" />
                  <Point X="-3.659552336887" Y="-0.352702408324" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.098623528055" Y="0.407791202919" />
                  <Point X="-4.623503237203" Y="1.316909527024" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.786839355856" Y="-3.786336439464" />
                  <Point X="-2.370799648386" Y="-2.7748875432" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.613723314036" Y="-2.354131411933" />
                  <Point X="-3.059264412888" Y="-1.582431591861" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.331088403176" Y="-1.111618629965" />
                  <Point X="-3.754552343396" Y="-0.378157570332" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.228395923546" Y="0.442563585329" />
                  <Point X="-4.665650089058" Y="1.199910015817" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.90085022133" Y="-3.778863827848" />
                  <Point X="-2.425647933195" Y="-2.869887527202" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.703499943993" Y="-2.388633727514" />
                  <Point X="-3.135283329825" Y="-1.64076296539" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.43303594939" Y="-1.125040300217" />
                  <Point X="-3.849552349905" Y="-0.403612732339" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.358168315438" Y="0.477335961505" />
                  <Point X="-4.700690056571" Y="1.070601019845" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.008665521874" Y="-3.782122249474" />
                  <Point X="-2.480496218004" Y="-2.964887511205" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.785772360558" Y="-2.436133721963" />
                  <Point X="-3.211302246761" Y="-1.69909433892" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.534983495603" Y="-1.138461970469" />
                  <Point X="-3.944552356414" Y="-0.429067894347" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.48794070733" Y="0.512108337682" />
                  <Point X="-4.734140805956" Y="0.93853941733" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.097944142621" Y="-3.81748714231" />
                  <Point X="-2.535344502813" Y="-3.059887495208" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.868044777122" Y="-2.483633716412" />
                  <Point X="-3.287321163698" Y="-1.757425712449" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.636931041816" Y="-1.15188364072" />
                  <Point X="-4.039552362922" Y="-0.454523056354" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.617713099222" Y="0.546880713859" />
                  <Point X="-4.756520459571" Y="0.787302114448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.177103237667" Y="-3.870379567808" />
                  <Point X="-2.590192787622" Y="-3.154887479211" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.950317193686" Y="-2.531133710861" />
                  <Point X="-3.363340080635" Y="-1.815757085978" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.73887858803" Y="-1.165305310972" />
                  <Point X="-4.134552369431" Y="-0.479978218361" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.747485491114" Y="0.581653090036" />
                  <Point X="-4.778899921598" Y="0.636064479725" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.256262319706" Y="-3.923272015837" />
                  <Point X="-2.645041072431" Y="-3.249887463214" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.032589610251" Y="-2.57863370531" />
                  <Point X="-3.439358997571" Y="-1.874088459508" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.840826134243" Y="-1.178726981223" />
                  <Point X="-4.22955237594" Y="-0.505433380369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.335421291494" Y="-3.976164654824" />
                  <Point X="-2.699889357239" Y="-3.344887447217" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.114862026815" Y="-2.626133699759" />
                  <Point X="-3.515377914508" Y="-1.932419833037" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.942773688409" Y="-1.1921486377" />
                  <Point X="-4.324552382449" Y="-0.530888542376" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.408577539221" Y="-4.03945431687" />
                  <Point X="-2.754737642048" Y="-3.43988743122" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.19713444338" Y="-2.673633694208" />
                  <Point X="-3.591396831445" Y="-1.990751206567" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.044721259357" Y="-1.20557026511" />
                  <Point X="-4.419552377396" Y="-0.556343724408" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.471271378194" Y="-4.120865402447" />
                  <Point X="-2.809585924504" Y="-3.534887419299" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.279406853752" Y="-2.721133699383" />
                  <Point X="-3.667415748381" Y="-2.049082580096" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.146668830305" Y="-1.218991892521" />
                  <Point X="-4.514552351868" Y="-0.581798941905" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.586841323438" Y="-4.110692385457" />
                  <Point X="-2.86443420472" Y="-3.629887411258" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.361679262529" Y="-2.768633707319" />
                  <Point X="-3.743434665318" Y="-2.107413953625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.248616401252" Y="-1.232413519931" />
                  <Point X="-4.60955232634" Y="-0.607254159402" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.759135359786" Y="-4.002270360661" />
                  <Point X="-2.919282484935" Y="-3.724887403217" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.443951671307" Y="-2.816133715255" />
                  <Point X="-3.819453581597" Y="-2.165745328293" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.3505639722" Y="-1.245835147342" />
                  <Point X="-4.704552300812" Y="-0.632709376898" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.956623223699" Y="-3.850211346486" />
                  <Point X="-2.97413076515" Y="-3.819887395176" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.526224080085" Y="-2.863633723191" />
                  <Point X="-3.895472464665" Y="-2.224076760485" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.452511543147" Y="-1.259256774752" />
                  <Point X="-4.781471247387" Y="-0.689481853367" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.608496488862" Y="-2.911133731127" />
                  <Point X="-3.971491347733" Y="-2.282408192677" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.554459114095" Y="-1.272678402163" />
                  <Point X="-4.745348730248" Y="-0.942047888349" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.69076889764" Y="-2.958633739063" />
                  <Point X="-4.047510230801" Y="-2.340739624869" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.656406685043" Y="-1.286100029573" />
                  <Point X="-4.664775005592" Y="-1.271605673208" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.773041306418" Y="-3.006133746999" />
                  <Point X="-4.123529113868" Y="-2.399071057061" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.0001640625" Y="0.001626220703" />
                  <Width Value="9.996463867188" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.708303039551" Y="-4.420727539062" />
                  <Point X="0.471539031982" Y="-3.537112304688" />
                  <Point X="0.464318817139" Y="-3.521544189453" />
                  <Point X="0.367547302246" Y="-3.382114990234" />
                  <Point X="0.300591033936" Y="-3.285643798828" />
                  <Point X="0.274335754395" Y="-3.266399902344" />
                  <Point X="0.124587600708" Y="-3.219923583984" />
                  <Point X="0.02097677803" Y="-3.187766601562" />
                  <Point X="-0.008664908409" Y="-3.187766601562" />
                  <Point X="-0.15841305542" Y="-3.234242675781" />
                  <Point X="-0.262023895264" Y="-3.266399902344" />
                  <Point X="-0.288279144287" Y="-3.285643798828" />
                  <Point X="-0.385050537109" Y="-3.425072998047" />
                  <Point X="-0.45200680542" Y="-3.521544189453" />
                  <Point X="-0.459227142334" Y="-3.537112304688" />
                  <Point X="-0.623044250488" Y="-4.148486328125" />
                  <Point X="-0.84774395752" Y="-4.987077148438" />
                  <Point X="-0.979142883301" Y="-4.961571777344" />
                  <Point X="-1.100246582031" Y="-4.938065429688" />
                  <Point X="-1.273559204102" Y="-4.893473632812" />
                  <Point X="-1.35158972168" Y="-4.873396972656" />
                  <Point X="-1.337651123047" Y="-4.7675234375" />
                  <Point X="-1.309150146484" Y="-4.551038574219" />
                  <Point X="-1.309683227539" Y="-4.5347578125" />
                  <Point X="-1.345458129883" Y="-4.354905761719" />
                  <Point X="-1.370210693359" Y="-4.230466308594" />
                  <Point X="-1.386282714844" Y="-4.20262890625" />
                  <Point X="-1.524151855469" Y="-4.081720458984" />
                  <Point X="-1.619543457031" Y="-3.998063964844" />
                  <Point X="-1.649240722656" Y="-3.985762939453" />
                  <Point X="-1.832223754883" Y="-3.97376953125" />
                  <Point X="-1.958829833984" Y="-3.965471435547" />
                  <Point X="-1.989878540039" Y="-3.973791015625" />
                  <Point X="-2.142349853516" Y="-4.075669189453" />
                  <Point X="-2.247844970703" Y="-4.146159179688" />
                  <Point X="-2.259734130859" Y="-4.157294433594" />
                  <Point X="-2.351704833984" Y="-4.277152832031" />
                  <Point X="-2.457095214844" Y="-4.414500488281" />
                  <Point X="-2.678323242188" Y="-4.277521484375" />
                  <Point X="-2.855831054688" Y="-4.16761328125" />
                  <Point X="-3.095821777344" Y="-3.982828369141" />
                  <Point X="-3.228581054688" Y="-3.880608154297" />
                  <Point X="-2.946032958984" Y="-3.391220458984" />
                  <Point X="-2.506033691406" Y="-2.629119628906" />
                  <Point X="-2.499762695312" Y="-2.597593261719" />
                  <Point X="-2.513979736328" Y="-2.568764404297" />
                  <Point X="-2.531328369141" Y="-2.551415771484" />
                  <Point X="-2.560157226562" Y="-2.537198730469" />
                  <Point X="-2.59168359375" Y="-2.543469726562" />
                  <Point X="-3.118981933594" Y="-2.847905517578" />
                  <Point X="-3.842959472656" Y="-3.265894042969" />
                  <Point X="-4.021460449219" Y="-3.031380615234" />
                  <Point X="-4.161703613281" Y="-2.847129394531" />
                  <Point X="-4.333755859375" Y="-2.558623535156" />
                  <Point X="-4.431020019531" Y="-2.395527099609" />
                  <Point X="-3.933642822266" Y="-2.013876098633" />
                  <Point X="-3.163786865234" Y="-1.423144897461" />
                  <Point X="-3.145822021484" Y="-1.396013916016" />
                  <Point X="-3.138117431641" Y="-1.366266479492" />
                  <Point X="-3.140326171875" Y="-1.334595825195" />
                  <Point X="-3.161158935547" Y="-1.310639282227" />
                  <Point X="-3.187641601562" Y="-1.295052734375" />
                  <Point X="-3.219529296875" Y="-1.288571044922" />
                  <Point X="-3.885193359375" Y="-1.376207275391" />
                  <Point X="-4.803284179688" Y="-1.497076293945" />
                  <Point X="-4.87254296875" Y="-1.225928466797" />
                  <Point X="-4.927391601562" Y="-1.011199707031" />
                  <Point X="-4.972913574219" Y="-0.692914611816" />
                  <Point X="-4.998395996094" Y="-0.514742004395" />
                  <Point X="-4.434445800781" Y="-0.363631958008" />
                  <Point X="-3.557463867188" Y="-0.128645431519" />
                  <Point X="-3.525492675781" Y="-0.110040237427" />
                  <Point X="-3.502324462891" Y="-0.090645393372" />
                  <Point X="-3.489431152344" Y="-0.058290424347" />
                  <Point X="-3.483400878906" Y="-0.031280042648" />
                  <Point X="-3.491115722656" Y="0.001158298016" />
                  <Point X="-3.502324462891" Y="0.028085330963" />
                  <Point X="-3.530546386719" Y="0.050987792969" />
                  <Point X="-3.557463867188" Y="0.066085334778" />
                  <Point X="-4.164248535156" Y="0.228672714233" />
                  <Point X="-4.998186523438" Y="0.452125793457" />
                  <Point X="-4.952995117188" Y="0.757527404785" />
                  <Point X="-4.917645019531" Y="0.996418151855" />
                  <Point X="-4.826013183594" Y="1.334568603516" />
                  <Point X="-4.773516113281" Y="1.528298583984" />
                  <Point X="-4.381908203125" Y="1.476742553711" />
                  <Point X="-3.753266357422" Y="1.393980102539" />
                  <Point X="-3.731704833984" Y="1.395866577148" />
                  <Point X="-3.695398925781" Y="1.407313964844" />
                  <Point X="-3.670278808594" Y="1.415234130859" />
                  <Point X="-3.651534423828" Y="1.426056152344" />
                  <Point X="-3.639120117188" Y="1.443785888672" />
                  <Point X="-3.624552246094" Y="1.478955932617" />
                  <Point X="-3.61447265625" Y="1.503290161133" />
                  <Point X="-3.610714111328" Y="1.524605224609" />
                  <Point X="-3.616315917969" Y="1.54551171875" />
                  <Point X="-3.633893554688" Y="1.579278320312" />
                  <Point X="-3.646055664062" Y="1.602641479492" />
                  <Point X="-3.659968261719" Y="1.619221679688" />
                  <Point X="-4.008021240234" Y="1.886292114258" />
                  <Point X="-4.47610546875" Y="2.245466308594" />
                  <Point X="-4.29737109375" Y="2.551681396484" />
                  <Point X="-4.160014648438" Y="2.787006591797" />
                  <Point X="-3.917284667969" Y="3.099001464844" />
                  <Point X="-3.774671142578" Y="3.282310791016" />
                  <Point X="-3.545251464844" Y="3.149855224609" />
                  <Point X="-3.159156982422" Y="2.926943359375" />
                  <Point X="-3.138514648438" Y="2.920434814453" />
                  <Point X="-3.087950439453" Y="2.916010986328" />
                  <Point X="-3.052965332031" Y="2.912950195312" />
                  <Point X="-3.031506591797" Y="2.915775390625" />
                  <Point X="-3.013252441406" Y="2.927404785156" />
                  <Point X="-2.977361572266" Y="2.963295410156" />
                  <Point X="-2.952528808594" Y="2.988128173828" />
                  <Point X="-2.940899414062" Y="3.006382324219" />
                  <Point X="-2.93807421875" Y="3.027841064453" />
                  <Point X="-2.942498046875" Y="3.078405273438" />
                  <Point X="-2.945558837891" Y="3.113390625" />
                  <Point X="-2.952067382812" Y="3.134032958984" />
                  <Point X="-3.106300292969" Y="3.401171875" />
                  <Point X="-3.307278564453" Y="3.749276611328" />
                  <Point X="-2.992107177734" Y="3.990915283203" />
                  <Point X="-2.752876220703" Y="4.174331542969" />
                  <Point X="-2.370583984375" Y="4.386724609375" />
                  <Point X="-2.141548583984" Y="4.513972167969" />
                  <Point X="-2.086021972656" Y="4.441608398438" />
                  <Point X="-1.967826904297" Y="4.287573242188" />
                  <Point X="-1.951246948242" Y="4.273660644531" />
                  <Point X="-1.894969360352" Y="4.244364257813" />
                  <Point X="-1.856030761719" Y="4.22409375" />
                  <Point X="-1.835124267578" Y="4.2184921875" />
                  <Point X="-1.813808837891" Y="4.222250488281" />
                  <Point X="-1.755191894531" Y="4.246530761719" />
                  <Point X="-1.714634887695" Y="4.263330078125" />
                  <Point X="-1.696905273438" Y="4.275744628906" />
                  <Point X="-1.686083496094" Y="4.294488769531" />
                  <Point X="-1.667004760742" Y="4.354998535156" />
                  <Point X="-1.653804077148" Y="4.396865234375" />
                  <Point X="-1.651917602539" Y="4.418426757812" />
                  <Point X="-1.669452148438" Y="4.551615234375" />
                  <Point X="-1.689137573242" Y="4.701141113281" />
                  <Point X="-1.277793701172" Y="4.816467285156" />
                  <Point X="-0.968094177246" Y="4.903296386719" />
                  <Point X="-0.504640258789" Y="4.957536621094" />
                  <Point X="-0.224199615479" Y="4.990358398438" />
                  <Point X="-0.156357818604" Y="4.737168945312" />
                  <Point X="-0.042140380859" Y="4.310903320312" />
                  <Point X="-0.024282121658" Y="4.284176757813" />
                  <Point X="0.006155934334" Y="4.273844238281" />
                  <Point X="0.036594032288" Y="4.284176757813" />
                  <Point X="0.054452251434" Y="4.310903320312" />
                  <Point X="0.133479400635" Y="4.605836914062" />
                  <Point X="0.236648406982" Y="4.990868652344" />
                  <Point X="0.589791503906" Y="4.953885253906" />
                  <Point X="0.860205810547" Y="4.925565429688" />
                  <Point X="1.243633300781" Y="4.832994628906" />
                  <Point X="1.508456176758" Y="4.769057617188" />
                  <Point X="1.758397338867" Y="4.67840234375" />
                  <Point X="1.931033691406" Y="4.615786132813" />
                  <Point X="2.172360107422" Y="4.50292578125" />
                  <Point X="2.33869921875" Y="4.425134277344" />
                  <Point X="2.571830566406" Y="4.289311523438" />
                  <Point X="2.732523681641" Y="4.19569140625" />
                  <Point X="2.952396972656" Y="4.039329589844" />
                  <Point X="3.068740234375" Y="3.956592773438" />
                  <Point X="2.739119628906" Y="3.385672851562" />
                  <Point X="2.229853515625" Y="2.503597900391" />
                  <Point X="2.224852050781" Y="2.491514648438" />
                  <Point X="2.212162353516" Y="2.444061279297" />
                  <Point X="2.203382324219" Y="2.411228271484" />
                  <Point X="2.202044677734" Y="2.392325927734" />
                  <Point X="2.206992675781" Y="2.351292480469" />
                  <Point X="2.210416015625" Y="2.322901123047" />
                  <Point X="2.218682128906" Y="2.300812255859" />
                  <Point X="2.244072265625" Y="2.263393554688" />
                  <Point X="2.261639892578" Y="2.237503662109" />
                  <Point X="2.274940185547" Y="2.224203613281" />
                  <Point X="2.312358642578" Y="2.198813476562" />
                  <Point X="2.338248779297" Y="2.18124609375" />
                  <Point X="2.360336914062" Y="2.172980224609" />
                  <Point X="2.401370605469" Y="2.168032226562" />
                  <Point X="2.42976171875" Y="2.164608642578" />
                  <Point X="2.448664306641" Y="2.165946289062" />
                  <Point X="2.496117675781" Y="2.178635986328" />
                  <Point X="2.528950683594" Y="2.187416015625" />
                  <Point X="2.541033935547" Y="2.192417480469" />
                  <Point X="3.151342285156" Y="2.544779052734" />
                  <Point X="3.994248046875" Y="3.031430908203" />
                  <Point X="4.107273925781" Y="2.874350341797" />
                  <Point X="4.202591796875" Y="2.741880126953" />
                  <Point X="4.32516796875" Y="2.539322021484" />
                  <Point X="4.387513183594" Y="2.436296142578" />
                  <Point X="3.959112792969" Y="2.107572998047" />
                  <Point X="3.288616210938" Y="1.593082763672" />
                  <Point X="3.279371337891" Y="1.583833129883" />
                  <Point X="3.245218994141" Y="1.539278930664" />
                  <Point X="3.221589111328" Y="1.508451782227" />
                  <Point X="3.213119628906" Y="1.49150012207" />
                  <Point X="3.200397949219" Y="1.446010131836" />
                  <Point X="3.191595703125" Y="1.414535644531" />
                  <Point X="3.190779541016" Y="1.390965209961" />
                  <Point X="3.20122265625" Y="1.340351806641" />
                  <Point X="3.208448486328" Y="1.305332397461" />
                  <Point X="3.215646484375" Y="1.287955078125" />
                  <Point X="3.244051025391" Y="1.244781494141" />
                  <Point X="3.263704101562" Y="1.214909667969" />
                  <Point X="3.280947998047" Y="1.198819824219" />
                  <Point X="3.322109863281" Y="1.175649169922" />
                  <Point X="3.350590087891" Y="1.159617431641" />
                  <Point X="3.368565429688" Y="1.153619628906" />
                  <Point X="3.424219238281" Y="1.146264282227" />
                  <Point X="3.462726074219" Y="1.141175048828" />
                  <Point X="3.475803710938" Y="1.14117175293" />
                  <Point X="4.055555664062" Y="1.217497558594" />
                  <Point X="4.848975097656" Y="1.32195324707" />
                  <Point X="4.89825" Y="1.119547241211" />
                  <Point X="4.939188476562" Y="0.951385864258" />
                  <Point X="4.977815429688" Y="0.703288818359" />
                  <Point X="4.997858398438" Y="0.574556152344" />
                  <Point X="4.509571289062" Y="0.443719970703" />
                  <Point X="3.741167724609" Y="0.237826919556" />
                  <Point X="3.729086914062" Y="0.232819259644" />
                  <Point X="3.674408691406" Y="0.201214355469" />
                  <Point X="3.636576904297" Y="0.179346878052" />
                  <Point X="3.622264892578" Y="0.166926574707" />
                  <Point X="3.589458007812" Y="0.125122886658" />
                  <Point X="3.566759033203" Y="0.096198890686" />
                  <Point X="3.556985107422" Y="0.07473513031" />
                  <Point X="3.546049316406" Y="0.017633405685" />
                  <Point X="3.538482910156" Y="-0.021875303268" />
                  <Point X="3.538482910156" Y="-0.04068478775" />
                  <Point X="3.549418701172" Y="-0.097786506653" />
                  <Point X="3.556985107422" Y="-0.137295227051" />
                  <Point X="3.566759033203" Y="-0.158758987427" />
                  <Point X="3.599565917969" Y="-0.200562515259" />
                  <Point X="3.622264892578" Y="-0.229486679077" />
                  <Point X="3.636576904297" Y="-0.241906967163" />
                  <Point X="3.691255126953" Y="-0.273511871338" />
                  <Point X="3.729086914062" Y="-0.295379486084" />
                  <Point X="3.741167724609" Y="-0.300387023926" />
                  <Point X="4.272826660156" Y="-0.442844543457" />
                  <Point X="4.998067871094" Y="-0.637172363281" />
                  <Point X="4.971290039062" Y="-0.814785949707" />
                  <Point X="4.948431640625" Y="-0.966399414063" />
                  <Point X="4.898943847656" Y="-1.183262451172" />
                  <Point X="4.874545898438" Y="-1.290178344727" />
                  <Point X="4.303114257812" Y="-1.214947875977" />
                  <Point X="3.411982177734" Y="-1.097628295898" />
                  <Point X="3.3948359375" Y="-1.098341186523" />
                  <Point X="3.287522216797" Y="-1.121666259766" />
                  <Point X="3.213271728516" Y="-1.137804931641" />
                  <Point X="3.1854453125" Y="-1.154697265625" />
                  <Point X="3.120580810547" Y="-1.232708984375" />
                  <Point X="3.075701171875" Y="-1.286685424805" />
                  <Point X="3.064357910156" Y="-1.314070556641" />
                  <Point X="3.055061279297" Y="-1.415098999023" />
                  <Point X="3.04862890625" Y="-1.485000854492" />
                  <Point X="3.056360595703" Y="-1.516621948242" />
                  <Point X="3.115749511719" Y="-1.608997802734" />
                  <Point X="3.156840820312" Y="-1.672912475586" />
                  <Point X="3.168460693359" Y="-1.685540649414" />
                  <Point X="3.661843505859" Y="-2.064126708984" />
                  <Point X="4.339074707031" Y="-2.583784179688" />
                  <Point X="4.268566894531" Y="-2.697875976562" />
                  <Point X="4.204131835938" Y="-2.802141845703" />
                  <Point X="4.101787597656" Y="-2.947558349609" />
                  <Point X="4.056688476562" Y="-3.011638427734" />
                  <Point X="3.547127685547" Y="-2.717443359375" />
                  <Point X="2.753454589844" Y="-2.259215576172" />
                  <Point X="2.737340820313" Y="-2.253312744141" />
                  <Point X="2.609620361328" Y="-2.230246582031" />
                  <Point X="2.521250488281" Y="-2.214287109375" />
                  <Point X="2.489077636719" Y="-2.219244873047" />
                  <Point X="2.382973144531" Y="-2.275086669922" />
                  <Point X="2.309559570312" Y="-2.313723876953" />
                  <Point X="2.288599853516" Y="-2.334683837891" />
                  <Point X="2.2327578125" Y="-2.440788085938" />
                  <Point X="2.194120849609" Y="-2.514201904297" />
                  <Point X="2.189163085938" Y="-2.546374511719" />
                  <Point X="2.212229248047" Y="-2.674094970703" />
                  <Point X="2.228188720703" Y="-2.76246484375" />
                  <Point X="2.234091552734" Y="-2.778578613281" />
                  <Point X="2.551139648438" Y="-3.327721923828" />
                  <Point X="2.986673583984" Y="-4.082088623047" />
                  <Point X="2.911756835938" Y="-4.135599609375" />
                  <Point X="2.835293701172" Y="-4.190215332031" />
                  <Point X="2.720871582031" Y="-4.264279296875" />
                  <Point X="2.679776123047" Y="-4.290879394531" />
                  <Point X="2.288823486328" Y="-3.781380126953" />
                  <Point X="1.683177490234" Y="-2.992087402344" />
                  <Point X="1.670549316406" Y="-2.980467529297" />
                  <Point X="1.544582397461" Y="-2.899482421875" />
                  <Point X="1.45742590332" Y="-2.843448730469" />
                  <Point X="1.425804931641" Y="-2.835717041016" />
                  <Point X="1.288038574219" Y="-2.84839453125" />
                  <Point X="1.192718017578" Y="-2.857166015625" />
                  <Point X="1.165332885742" Y="-2.868509277344" />
                  <Point X="1.05895324707" Y="-2.956960449219" />
                  <Point X="0.985349243164" Y="-3.018159667969" />
                  <Point X="0.968456726074" Y="-3.045986328125" />
                  <Point X="0.936649841309" Y="-3.192323486328" />
                  <Point X="0.91464251709" Y="-3.29357421875" />
                  <Point X="0.91392956543" Y="-3.310720214844" />
                  <Point X="1.003778442383" Y="-3.993190917969" />
                  <Point X="1.127642211914" Y="-4.934028320312" />
                  <Point X="1.066679931641" Y="-4.947391601562" />
                  <Point X="0.994347351074" Y="-4.963246582031" />
                  <Point X="0.888637634277" Y="-4.982450195312" />
                  <Point X="0.860200317383" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#176" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.114217428061" Y="4.781389739961" Z="1.5" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.5" />
                  <Point X="-0.51660836997" Y="5.038478021476" Z="1.5" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.5" />
                  <Point X="-1.297447429651" Y="4.895891124694" Z="1.5" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.5" />
                  <Point X="-1.725180423146" Y="4.576368674178" Z="1.5" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.5" />
                  <Point X="-1.720846455202" Y="4.401313829153" Z="1.5" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.5" />
                  <Point X="-1.780482195911" Y="4.324004678734" Z="1.5" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.5" />
                  <Point X="-1.878037582872" Y="4.319994840552" Z="1.5" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.5" />
                  <Point X="-2.052510392617" Y="4.503326363812" Z="1.5" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.5" />
                  <Point X="-2.401023016381" Y="4.461712159474" Z="1.5" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.5" />
                  <Point X="-3.028685220761" Y="4.061875362072" Z="1.5" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.5" />
                  <Point X="-3.155757512017" Y="3.407451984082" Z="1.5" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.5" />
                  <Point X="-2.998463838102" Y="3.105327603337" Z="1.5" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.5" />
                  <Point X="-3.018872833079" Y="3.029930734808" Z="1.5" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.5" />
                  <Point X="-3.08974886227" Y="2.997100861071" Z="1.5" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.5" />
                  <Point X="-3.526407364215" Y="3.224436436729" Z="1.5" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.5" />
                  <Point X="-3.962904007647" Y="3.160983959707" Z="1.5" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.5" />
                  <Point X="-4.346709339722" Y="2.608166395162" Z="1.5" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.5" />
                  <Point X="-4.044615294269" Y="1.877904555007" Z="1.5" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.5" />
                  <Point X="-3.684400211176" Y="1.587470982221" Z="1.5" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.5" />
                  <Point X="-3.676901988752" Y="1.529370180355" Z="1.5" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.5" />
                  <Point X="-3.716590063909" Y="1.486279763422" Z="1.5" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.5" />
                  <Point X="-4.381538012644" Y="1.557594841571" Z="1.5" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.5" />
                  <Point X="-4.880428857307" Y="1.378925927194" Z="1.5" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.5" />
                  <Point X="-5.008612272262" Y="0.796126579437" Z="1.5" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.5" />
                  <Point X="-4.18334520555" Y="0.211656443249" Z="1.5" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.5" />
                  <Point X="-3.565211229972" Y="0.041191826964" Z="1.5" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.5" />
                  <Point X="-3.545024625843" Y="0.017617433894" Z="1.5" />
                  <Point X="-3.539556741714" Y="0" Z="1.5" />
                  <Point X="-3.543339975292" Y="-0.012189517167" Z="1.5" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.5" />
                  <Point X="-3.560157365151" Y="-0.037684156169" Z="1.5" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.5" />
                  <Point X="-4.453542583222" Y="-0.284055613507" Z="1.5" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.5" />
                  <Point X="-5.028565909203" Y="-0.668713592562" Z="1.5" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.5" />
                  <Point X="-4.927143487974" Y="-1.207022637533" Z="1.5" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.5" />
                  <Point X="-3.884823038104" Y="-1.394499639076" Z="1.5" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.5" />
                  <Point X="-3.20832886743" Y="-1.313237386699" Z="1.5" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.5" />
                  <Point X="-3.195827050399" Y="-1.33461495365" Z="1.5" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.5" />
                  <Point X="-3.970236678889" Y="-1.942928336394" Z="1.5" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.5" />
                  <Point X="-4.382855426339" Y="-2.552953154283" Z="1.5" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.5" />
                  <Point X="-4.067402204988" Y="-3.030383677378" Z="1.5" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.5" />
                  <Point X="-3.100137842852" Y="-2.859926807427" Z="1.5" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.5" />
                  <Point X="-2.565744838676" Y="-2.562585630455" Z="1.5" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.5" />
                  <Point X="-2.995490205741" Y="-3.33494053964" Z="1.5" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.5" />
                  <Point X="-3.132481735136" Y="-3.991165423629" Z="1.5" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.5" />
                  <Point X="-2.710800679417" Y="-4.288752349716" Z="1.5" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.5" />
                  <Point X="-2.318193014508" Y="-4.276310736146" Z="1.5" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.5" />
                  <Point X="-2.120727273461" Y="-4.085962567676" Z="1.5" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.5" />
                  <Point X="-1.841649883209" Y="-3.992382579868" Z="1.5" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.5" />
                  <Point X="-1.563275093932" Y="-4.088032372641" Z="1.5" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.5" />
                  <Point X="-1.400653353005" Y="-4.333380244887" Z="1.5" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.5" />
                  <Point X="-1.393379328191" Y="-4.729717152762" Z="1.5" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.5" />
                  <Point X="-1.292174027456" Y="-4.910616207379" Z="1.5" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.5" />
                  <Point X="-0.994826074654" Y="-4.979375959997" Z="1.5" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.5" />
                  <Point X="-0.580904026132" Y="-4.130147408069" Z="1.5" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.5" />
                  <Point X="-0.350130531697" Y="-3.422301890207" Z="1.5" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.5" />
                  <Point X="-0.149748185347" Y="-3.250715678946" Z="1.5" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.5" />
                  <Point X="0.103610894014" Y="-3.236396366342" Z="1.5" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.5" />
                  <Point X="0.320315327735" Y="-3.379343865561" Z="1.5" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.5" />
                  <Point X="0.653850834269" Y="-4.402388621747" Z="1.5" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.5" />
                  <Point X="0.891418758611" Y="-5.000365144281" Z="1.5" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.5" />
                  <Point X="1.071229505152" Y="-4.964951738448" Z="1.5" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.5" />
                  <Point X="1.047194778021" Y="-3.955384480999" Z="1.5" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.5" />
                  <Point X="0.979353049992" Y="-3.171663006934" Z="1.5" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.5" />
                  <Point X="1.084764150347" Y="-2.964126589518" Z="1.5" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.5" />
                  <Point X="1.286464281873" Y="-2.866903957892" Z="1.5" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.5" />
                  <Point X="1.511387033833" Y="-2.910260215575" Z="1.5" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.5" />
                  <Point X="2.242999780416" Y="-3.780538190829" Z="1.5" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.5" />
                  <Point X="2.741884482515" Y="-4.274973142554" Z="1.5" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.5" />
                  <Point X="2.934662652776" Y="-4.145006790553" Z="1.5" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.5" />
                  <Point X="2.588284974437" Y="-3.271441771714" Z="1.5" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.5" />
                  <Point X="2.255277348776" Y="-2.633928693528" Z="1.5" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.5" />
                  <Point X="2.270848221957" Y="-2.432794515286" Z="1.5" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.5" />
                  <Point X="2.400103863086" Y="-2.28805308801" Z="1.5" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.5" />
                  <Point X="2.594578109985" Y="-2.248170660715" Z="1.5" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.5" />
                  <Point X="3.515971825393" Y="-2.729464525838" Z="1.5" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.5" />
                  <Point X="4.136520413093" Y="-2.945055311705" Z="1.5" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.5" />
                  <Point X="4.304943981954" Y="-2.692881127803" Z="1.5" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.5" />
                  <Point X="3.686125545551" Y="-1.993178956942" Z="1.5" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.5" />
                  <Point X="3.151651518049" Y="-1.550678202286" Z="1.5" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.5" />
                  <Point X="3.098694842075" Y="-1.38840076883" Z="1.5" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.5" />
                  <Point X="3.152871164621" Y="-1.23339587609" Z="1.5" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.5" />
                  <Point X="3.291986046074" Y="-1.139245525865" Z="1.5" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.5" />
                  <Point X="4.290431805889" Y="-1.233240161961" Z="1.5" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.5" />
                  <Point X="4.941534962652" Y="-1.163106404936" Z="1.5" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.5" />
                  <Point X="5.014575374751" Y="-0.790960043491" Z="1.5" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.5" />
                  <Point X="4.279611678459" Y="-0.363268224354" Z="1.5" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.5" />
                  <Point X="3.710120851392" Y="-0.198943150459" Z="1.5" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.5" />
                  <Point X="3.63274360573" Y="-0.138414208365" Z="1.5" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.5" />
                  <Point X="3.592370354055" Y="-0.057101769452" Z="1.5" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.5" />
                  <Point X="3.589001096369" Y="0.039508761752" Z="1.5" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.5" />
                  <Point X="3.622635832671" Y="0.12553453021" Z="1.5" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.5" />
                  <Point X="3.69327456296" Y="0.189205693783" Z="1.5" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.5" />
                  <Point X="4.516356138848" Y="0.426703708415" Z="1.5" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.5" />
                  <Point X="5.021064477831" Y="0.742260855977" Z="1.5" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.5" />
                  <Point X="4.940675156983" Y="1.162654306982" Z="1.5" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.5" />
                  <Point X="4.042873440884" Y="1.298349908928" Z="1.5" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.5" />
                  <Point X="3.424614469031" Y="1.227113245031" Z="1.5" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.5" />
                  <Point X="3.340480578563" Y="1.250500375268" Z="1.5" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.5" />
                  <Point X="3.279665471616" Y="1.30354284935" Z="1.5" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.5" />
                  <Point X="3.244035294685" Y="1.381735836052" Z="1.5" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.5" />
                  <Point X="3.242394228142" Y="1.463823744711" Z="1.5" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.5" />
                  <Point X="3.278745936824" Y="1.540140835676" Z="1.5" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.5" />
                  <Point X="3.983394737667" Y="2.099185203215" Z="1.5" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.5" />
                  <Point X="4.361789271135" Y="2.596488341688" Z="1.5" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.5" />
                  <Point X="4.141703621415" Y="2.934833199327" Z="1.5" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.5" />
                  <Point X="3.120186270931" Y="2.619360438365" Z="1.5" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.5" />
                  <Point X="2.477044947855" Y="2.258218803324" Z="1.5" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.5" />
                  <Point X="2.401200438728" Y="2.248952629713" Z="1.5" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.5" />
                  <Point X="2.334276764102" Y="2.271467964588" Z="1.5" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.5" />
                  <Point X="2.279290635647" Y="2.32274809628" Z="1.5" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.5" />
                  <Point X="2.250477073045" Y="2.388558005105" Z="1.5" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.5" />
                  <Point X="2.254309139334" Y="2.462424628027" Z="1.5" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.5" />
                  <Point X="2.776264914494" Y="3.391952811024" Z="1.5" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.5" />
                  <Point X="2.975218130709" Y="4.111356779246" Z="1.5" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.5" />
                  <Point X="2.590844535407" Y="4.36379419278" Z="1.5" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.5" />
                  <Point X="2.187385620676" Y="4.579497740662" Z="1.5" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.5" />
                  <Point X="1.769290005479" Y="4.756686382357" Z="1.5" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.5" />
                  <Point X="1.24921189048" Y="4.912877961519" Z="1.5" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.5" />
                  <Point X="0.588843457614" Y="5.034893262422" Z="1.5" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.5" />
                  <Point X="0.079027133492" Y="4.65005776599" Z="1.5" />
                  <Point X="0" Y="4.355124473572" Z="1.5" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>