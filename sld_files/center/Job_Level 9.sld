<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#133" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="806" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="0" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="0.004716064453" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.62658605957" Y="-3.748703369141" />
                  <Point X="0.563302062988" Y="-3.512524414063" />
                  <Point X="0.557721191406" Y="-3.497141845703" />
                  <Point X="0.542363342285" Y="-3.467377441406" />
                  <Point X="0.516497497559" Y="-3.430109619141" />
                  <Point X="0.378635681152" Y="-3.231477050781" />
                  <Point X="0.356751586914" Y="-3.209021484375" />
                  <Point X="0.330496398926" Y="-3.189777587891" />
                  <Point X="0.302495330811" Y="-3.175669433594" />
                  <Point X="0.262469573975" Y="-3.163246826172" />
                  <Point X="0.049136341095" Y="-3.097036132812" />
                  <Point X="0.020976791382" Y="-3.092766601562" />
                  <Point X="-0.00866468811" Y="-3.092766601562" />
                  <Point X="-0.036823932648" Y="-3.097035888672" />
                  <Point X="-0.076849700928" Y="-3.109458251953" />
                  <Point X="-0.290183074951" Y="-3.175669189453" />
                  <Point X="-0.31818460083" Y="-3.189777587891" />
                  <Point X="-0.344439788818" Y="-3.209021484375" />
                  <Point X="-0.366323577881" Y="-3.231476806641" />
                  <Point X="-0.39218927002" Y="-3.268744384766" />
                  <Point X="-0.530051208496" Y="-3.467377197266" />
                  <Point X="-0.538189025879" Y="-3.481573730469" />
                  <Point X="-0.550990112305" Y="-3.512524414063" />
                  <Point X="-0.888287231445" Y="-4.771335449219" />
                  <Point X="-0.916584411621" Y="-4.87694140625" />
                  <Point X="-1.079339477539" Y="-4.845350585938" />
                  <Point X="-1.121828857422" Y="-4.83441796875" />
                  <Point X="-1.24641796875" Y="-4.802362304688" />
                  <Point X="-1.222580810547" Y="-4.621302246094" />
                  <Point X="-1.214963012695" Y="-4.563438476562" />
                  <Point X="-1.214201171875" Y="-4.5479296875" />
                  <Point X="-1.216508666992" Y="-4.516224121094" />
                  <Point X="-1.226070800781" Y="-4.468151855469" />
                  <Point X="-1.277036132812" Y="-4.211932617188" />
                  <Point X="-1.287938354492" Y="-4.182966308594" />
                  <Point X="-1.304010375977" Y="-4.15512890625" />
                  <Point X="-1.32364440918" Y="-4.131204589844" />
                  <Point X="-1.360494873047" Y="-4.098887207031" />
                  <Point X="-1.556905151367" Y="-3.926639648438" />
                  <Point X="-1.583188476562" Y="-3.910295410156" />
                  <Point X="-1.612885742188" Y="-3.897994384766" />
                  <Point X="-1.643027099609" Y="-3.890966308594" />
                  <Point X="-1.691936035156" Y="-3.887760498047" />
                  <Point X="-1.952616210938" Y="-3.870674804688" />
                  <Point X="-1.98341796875" Y="-3.873708496094" />
                  <Point X="-2.014466674805" Y="-3.882028076172" />
                  <Point X="-2.042657714844" Y="-3.894801269531" />
                  <Point X="-2.083411376953" Y="-3.922031982422" />
                  <Point X="-2.300624023438" Y="-4.067169433594" />
                  <Point X="-2.312785644531" Y="-4.076821533203" />
                  <Point X="-2.335102783203" Y="-4.099461914062" />
                  <Point X="-2.480149169922" Y="-4.288489746094" />
                  <Point X="-2.801705078125" Y="-4.089390380859" />
                  <Point X="-2.860543212891" Y="-4.044086914063" />
                  <Point X="-3.104721923828" Y="-3.856077636719" />
                  <Point X="-2.541367431641" Y="-2.880319335938" />
                  <Point X="-2.423761230469" Y="-2.676619628906" />
                  <Point X="-2.411279541016" Y="-2.646973144531" />
                  <Point X="-2.405864257812" Y="-2.624442871094" />
                  <Point X="-2.406064208984" Y="-2.601271728516" />
                  <Point X="-2.410208740234" Y="-2.569790039062" />
                  <Point X="-2.416570556641" Y="-2.545972167969" />
                  <Point X="-2.428846923828" Y="-2.524593505859" />
                  <Point X="-2.441171386719" Y="-2.508427490234" />
                  <Point X="-2.449545410156" Y="-2.498848632812" />
                  <Point X="-2.464153320312" Y="-2.484240722656" />
                  <Point X="-2.489310302734" Y="-2.466213134766" />
                  <Point X="-2.518139160156" Y="-2.45199609375" />
                  <Point X="-2.547756835938" Y="-2.443011474609" />
                  <Point X="-2.578690917969" Y="-2.444024169922" />
                  <Point X="-2.610217285156" Y="-2.450295166016" />
                  <Point X="-2.63918359375" Y="-2.461197265625" />
                  <Point X="-3.724883300781" Y="-3.088026367188" />
                  <Point X="-3.818024414062" Y="-3.141801269531" />
                  <Point X="-4.082860107422" Y="-2.793861083984" />
                  <Point X="-4.125037109375" Y="-2.723136474609" />
                  <Point X="-4.306142578125" Y="-2.419450439453" />
                  <Point X="-3.311727050781" Y="-1.656408203125" />
                  <Point X="-3.105954589844" Y="-1.498513549805" />
                  <Point X="-3.083062744141" Y="-1.475877807617" />
                  <Point X="-3.064366455078" Y="-1.446454345703" />
                  <Point X="-3.056435791016" Y="-1.426686767578" />
                  <Point X="-3.052639160156" Y="-1.415132446289" />
                  <Point X="-3.046151855469" Y="-1.390084472656" />
                  <Point X="-3.042037597656" Y="-1.358602783203" />
                  <Point X="-3.042736816406" Y="-1.33573425293" />
                  <Point X="-3.048883300781" Y="-1.313696166992" />
                  <Point X="-3.060888183594" Y="-1.284713623047" />
                  <Point X="-3.073192382812" Y="-1.263361083984" />
                  <Point X="-3.090574951172" Y="-1.245891845703" />
                  <Point X="-3.107260986328" Y="-1.233000244141" />
                  <Point X="-3.117156494141" Y="-1.226304321289" />
                  <Point X="-3.139455322266" Y="-1.213180297852" />
                  <Point X="-3.168718261719" Y="-1.201956420898" />
                  <Point X="-3.200605957031" Y="-1.195474853516" />
                  <Point X="-3.231929199219" Y="-1.194383789062" />
                  <Point X="-4.602521972656" Y="-1.374825927734" />
                  <Point X="-4.7321015625" Y="-1.391885375977" />
                  <Point X="-4.834077636719" Y="-0.992654846191" />
                  <Point X="-4.845236328125" Y="-0.914633789062" />
                  <Point X="-4.892424316406" Y="-0.584698181152" />
                  <Point X="-3.767281738281" Y="-0.283217254639" />
                  <Point X="-3.532875976562" Y="-0.220408340454" />
                  <Point X="-3.514008789063" Y="-0.213124237061" />
                  <Point X="-3.494056152344" Y="-0.20286076355" />
                  <Point X="-3.48334375" Y="-0.196426223755" />
                  <Point X="-3.459975341797" Y="-0.180207107544" />
                  <Point X="-3.436020996094" Y="-0.158681106567" />
                  <Point X="-3.415918945313" Y="-0.130131149292" />
                  <Point X="-3.407032226562" Y="-0.110684677124" />
                  <Point X="-3.40270703125" Y="-0.099358535767" />
                  <Point X="-3.394917480469" Y="-0.074260597229" />
                  <Point X="-3.390733886719" Y="-0.042062812805" />
                  <Point X="-3.3921953125" Y="-0.007712188721" />
                  <Point X="-3.39637890625" Y="0.016409433365" />
                  <Point X="-3.404168457031" Y="0.041507369995" />
                  <Point X="-3.417484130859" Y="0.070830696106" />
                  <Point X="-3.438825927734" Y="0.098746017456" />
                  <Point X="-3.455029052734" Y="0.113306816101" />
                  <Point X="-3.464360351562" Y="0.120690353394" />
                  <Point X="-3.487728759766" Y="0.136909317017" />
                  <Point X="-3.501924560547" Y="0.145046646118" />
                  <Point X="-3.532875976562" Y="0.157848342896" />
                  <Point X="-4.782236328125" Y="0.49261328125" />
                  <Point X="-4.89181640625" Y="0.521975341797" />
                  <Point X="-4.824488769531" Y="0.976969970703" />
                  <Point X="-4.802023925781" Y="1.059872924805" />
                  <Point X="-4.703551269531" Y="1.423267822266" />
                  <Point X="-3.933694091797" Y="1.321914306641" />
                  <Point X="-3.765666259766" Y="1.29979284668" />
                  <Point X="-3.744985839844" Y="1.299341674805" />
                  <Point X="-3.723424072266" Y="1.301228271484" />
                  <Point X="-3.703137451172" Y="1.305263671875" />
                  <Point X="-3.693433349609" Y="1.308323242188" />
                  <Point X="-3.641711425781" Y="1.324631225586" />
                  <Point X="-3.622778564453" Y="1.332961669922" />
                  <Point X="-3.604034423828" Y="1.343783569336" />
                  <Point X="-3.587353515625" Y="1.356014648438" />
                  <Point X="-3.573715087891" Y="1.37156628418" />
                  <Point X="-3.561300537109" Y="1.389296020508" />
                  <Point X="-3.551351318359" Y="1.407431152344" />
                  <Point X="-3.547457519531" Y="1.416831665039" />
                  <Point X="-3.526703857422" Y="1.466935424805" />
                  <Point X="-3.520915527344" Y="1.486794067383" />
                  <Point X="-3.517157226562" Y="1.508109130859" />
                  <Point X="-3.5158046875" Y="1.528749145508" />
                  <Point X="-3.518951171875" Y="1.549192749023" />
                  <Point X="-3.524552978516" Y="1.570099121094" />
                  <Point X="-3.532049560547" Y="1.589377197266" />
                  <Point X="-3.536747802734" Y="1.598402587891" />
                  <Point X="-3.561789306641" Y="1.646506958008" />
                  <Point X="-3.57328125" Y="1.663706054688" />
                  <Point X="-3.587193603516" Y="1.680286254883" />
                  <Point X="-3.602135742188" Y="1.694590209961" />
                  <Point X="-4.318771972656" Y="2.244484619141" />
                  <Point X="-4.351859863281" Y="2.269874023438" />
                  <Point X="-4.081152832031" Y="2.733660888672" />
                  <Point X="-4.021651367188" Y="2.810141357422" />
                  <Point X="-3.750504882813" Y="3.158661865234" />
                  <Point X="-3.309854980469" Y="2.904252441406" />
                  <Point X="-3.206656982422" Y="2.844670898438" />
                  <Point X="-3.187724609375" Y="2.836340332031" />
                  <Point X="-3.167082519531" Y="2.829831787109" />
                  <Point X="-3.146794433594" Y="2.825796386719" />
                  <Point X="-3.133279296875" Y="2.824614013672" />
                  <Point X="-3.061245117188" Y="2.818311767578" />
                  <Point X="-3.040565185547" Y="2.818762939453" />
                  <Point X="-3.019106445312" Y="2.821588134766" />
                  <Point X="-2.999014648438" Y="2.826504638672" />
                  <Point X="-2.980463134766" Y="2.835653320313" />
                  <Point X="-2.962208740234" Y="2.847282714844" />
                  <Point X="-2.946078125" Y="2.860228759766" />
                  <Point X="-2.936484863281" Y="2.869821777344" />
                  <Point X="-2.885354492188" Y="2.920952148438" />
                  <Point X="-2.872406738281" Y="2.937084472656" />
                  <Point X="-2.86077734375" Y="2.955338867188" />
                  <Point X="-2.851628662109" Y="2.973890380859" />
                  <Point X="-2.846712158203" Y="2.993982177734" />
                  <Point X="-2.843886962891" Y="3.015440917969" />
                  <Point X="-2.843435791016" Y="3.036120849609" />
                  <Point X="-2.844618164062" Y="3.049635986328" />
                  <Point X="-2.850920410156" Y="3.121670410156" />
                  <Point X="-2.854955810547" Y="3.141958496094" />
                  <Point X="-2.861464355469" Y="3.162600585938" />
                  <Point X="-2.869794921875" Y="3.181532958984" />
                  <Point X="-3.183333007812" Y="3.724596679688" />
                  <Point X="-2.700627197266" Y="4.094682617188" />
                  <Point X="-2.606913085938" Y="4.146748535156" />
                  <Point X="-2.167036621094" Y="4.391134277344" />
                  <Point X="-2.074787353516" Y="4.270912597656" />
                  <Point X="-2.0431953125" Y="4.229741210938" />
                  <Point X="-2.028892700195" Y="4.214799804688" />
                  <Point X="-2.012312866211" Y="4.200887207031" />
                  <Point X="-1.99511328125" Y="4.189395019531" />
                  <Point X="-1.980071044922" Y="4.181564453125" />
                  <Point X="-1.899897094727" Y="4.139828125" />
                  <Point X="-1.88061706543" Y="4.132330566406" />
                  <Point X="-1.859710327148" Y="4.126729003906" />
                  <Point X="-1.839267456055" Y="4.123582519531" />
                  <Point X="-1.818628051758" Y="4.124935546875" />
                  <Point X="-1.797312744141" Y="4.128693847656" />
                  <Point X="-1.777453491211" Y="4.134481933594" />
                  <Point X="-1.761785888672" Y="4.140971679688" />
                  <Point X="-1.678279541016" Y="4.175561523437" />
                  <Point X="-1.660145751953" Y="4.185510253906" />
                  <Point X="-1.642416259766" Y="4.197924316406" />
                  <Point X="-1.626864013672" Y="4.211562988281" />
                  <Point X="-1.614632568359" Y="4.228244628906" />
                  <Point X="-1.603810791016" Y="4.246988769531" />
                  <Point X="-1.59548046875" Y="4.265920898438" />
                  <Point X="-1.590380859375" Y="4.282094726562" />
                  <Point X="-1.563201171875" Y="4.368297363281" />
                  <Point X="-1.559165649414" Y="4.388584960938" />
                  <Point X="-1.557279174805" Y="4.410146972656" />
                  <Point X="-1.55773034668" Y="4.430826660156" />
                  <Point X="-1.584201904297" Y="4.631897949219" />
                  <Point X="-1.584122192383" Y="4.631920410156" />
                  <Point X="-0.94963494873" Y="4.80980859375" />
                  <Point X="-0.83603503418" Y="4.823103515625" />
                  <Point X="-0.294710784912" Y="4.886458007812" />
                  <Point X="-0.164432159424" Y="4.400250976562" />
                  <Point X="-0.133903305054" Y="4.286315429688" />
                  <Point X="-0.121129844666" Y="4.258124023438" />
                  <Point X="-0.10327155304" Y="4.231397460938" />
                  <Point X="-0.082114562988" Y="4.20880859375" />
                  <Point X="-0.054819446564" Y="4.19421875" />
                  <Point X="-0.024381370544" Y="4.183886230469" />
                  <Point X="0.006155920029" Y="4.178844238281" />
                  <Point X="0.03669316864" Y="4.183886230469" />
                  <Point X="0.067131248474" Y="4.19421875" />
                  <Point X="0.094426452637" Y="4.20880859375" />
                  <Point X="0.115583503723" Y="4.231397460938" />
                  <Point X="0.133441650391" Y="4.258124023438" />
                  <Point X="0.146215255737" Y="4.286315429688" />
                  <Point X="0.307419281006" Y="4.8879375" />
                  <Point X="0.844041564941" Y="4.831738769531" />
                  <Point X="0.938032348633" Y="4.809046386719" />
                  <Point X="1.481028808594" Y="4.677949707031" />
                  <Point X="1.540957397461" Y="4.656213867188" />
                  <Point X="1.894643798828" Y="4.527929199219" />
                  <Point X="1.953806396484" Y="4.500260742188" />
                  <Point X="2.294574707031" Y="4.34089453125" />
                  <Point X="2.351759765625" Y="4.307578125" />
                  <Point X="2.680980224609" Y="4.115773925781" />
                  <Point X="2.734883300781" Y="4.077440917969" />
                  <Point X="2.943260009766" Y="3.929254882812" />
                  <Point X="2.283701171875" Y="2.786864990234" />
                  <Point X="2.147581054688" Y="2.551097900391" />
                  <Point X="2.139879882812" Y="2.534089355469" />
                  <Point X="2.131486572266" Y="2.509322509766" />
                  <Point X="2.129685058594" Y="2.503373046875" />
                  <Point X="2.111607177734" Y="2.435770263672" />
                  <Point X="2.108382324219" Y="2.411276367188" />
                  <Point X="2.1083671875" Y="2.38140625" />
                  <Point X="2.109050292969" Y="2.369985351562" />
                  <Point X="2.116099121094" Y="2.311528320312" />
                  <Point X="2.121442138672" Y="2.289604980469" />
                  <Point X="2.129708251953" Y="2.267516357422" />
                  <Point X="2.140071533203" Y="2.247470214844" />
                  <Point X="2.146858154297" Y="2.23746875" />
                  <Point X="2.183029296875" Y="2.184161621094" />
                  <Point X="2.199611572266" Y="2.165549072266" />
                  <Point X="2.222913085938" Y="2.145462158203" />
                  <Point X="2.231600585938" Y="2.138805664063" />
                  <Point X="2.284907714844" Y="2.102634765625" />
                  <Point X="2.304952880859" Y="2.092271972656" />
                  <Point X="2.327041259766" Y="2.084006103516" />
                  <Point X="2.348964111328" Y="2.078663330078" />
                  <Point X="2.359931884766" Y="2.077340820312" />
                  <Point X="2.418388916016" Y="2.070291748047" />
                  <Point X="2.443829589844" Y="2.070656005859" />
                  <Point X="2.475415771484" Y="2.075385498047" />
                  <Point X="2.485889892578" Y="2.077562988281" />
                  <Point X="2.553492675781" Y="2.095640869141" />
                  <Point X="2.565283447266" Y="2.099638183594" />
                  <Point X="2.588533935547" Y="2.110145019531" />
                  <Point X="3.845149902344" Y="2.835652587891" />
                  <Point X="3.967326416016" Y="2.906191162109" />
                  <Point X="4.123272949219" Y="2.689460449219" />
                  <Point X="4.153323730469" Y="2.639801269531" />
                  <Point X="4.262198730469" Y="2.459884277344" />
                  <Point X="3.409999023438" Y="1.805968017578" />
                  <Point X="3.230783935547" Y="1.668451293945" />
                  <Point X="3.216886230469" Y="1.65537097168" />
                  <Point X="3.198512939453" Y="1.634212768555" />
                  <Point X="3.194845458984" Y="1.629719116211" />
                  <Point X="3.146191650391" Y="1.566246582031" />
                  <Point X="3.133620849609" Y="1.544320922852" />
                  <Point X="3.121750976562" Y="1.515210205078" />
                  <Point X="3.118229492188" Y="1.504927001953" />
                  <Point X="3.100105957031" Y="1.440121459961" />
                  <Point X="3.096652587891" Y="1.417823242188" />
                  <Point X="3.095836425781" Y="1.394252929688" />
                  <Point X="3.097739257812" Y="1.371768554688" />
                  <Point X="3.100530517578" Y="1.358240234375" />
                  <Point X="3.115408203125" Y="1.286135742188" />
                  <Point X="3.123869140625" Y="1.262073608398" />
                  <Point X="3.138659179688" Y="1.23315625" />
                  <Point X="3.143874755859" Y="1.224200195313" />
                  <Point X="3.184340332031" Y="1.162694824219" />
                  <Point X="3.198893554688" Y="1.145450439453" />
                  <Point X="3.216137451172" Y="1.129360595703" />
                  <Point X="3.234346679688" Y="1.11603503418" />
                  <Point X="3.245348632812" Y="1.109841796875" />
                  <Point X="3.303988769531" Y="1.076832763672" />
                  <Point X="3.328208007812" Y="1.067291748047" />
                  <Point X="3.361058837891" Y="1.059327880859" />
                  <Point X="3.370993652344" Y="1.05747265625" />
                  <Point X="3.450278808594" Y="1.046994018555" />
                  <Point X="3.462702148438" Y="1.046175048828" />
                  <Point X="3.488203613281" Y="1.04698449707" />
                  <Point X="4.681904785156" Y="1.204138427734" />
                  <Point X="4.77683984375" Y="1.21663671875" />
                  <Point X="4.845936035156" Y="0.932811950684" />
                  <Point X="4.85540625" Y="0.871983947754" />
                  <Point X="4.890864746094" Y="0.644238464355" />
                  <Point X="3.921964111328" Y="0.384622314453" />
                  <Point X="3.716579833984" Y="0.329589935303" />
                  <Point X="3.698409667969" Y="0.322660552979" />
                  <Point X="3.671714111328" Y="0.309205383301" />
                  <Point X="3.666930908203" Y="0.306620391846" />
                  <Point X="3.589035644531" Y="0.261595428467" />
                  <Point X="3.568667480469" Y="0.24577961731" />
                  <Point X="3.545586669922" Y="0.222185775757" />
                  <Point X="3.538761962891" Y="0.214403015137" />
                  <Point X="3.492024902344" Y="0.154848968506" />
                  <Point X="3.480301025391" Y="0.135569152832" />
                  <Point X="3.470527099609" Y="0.114105384827" />
                  <Point X="3.463680908203" Y="0.092604728699" />
                  <Point X="3.4607578125" Y="0.077342201233" />
                  <Point X="3.445178710938" Y="-0.004005618572" />
                  <Point X="3.443830566406" Y="-0.029995779037" />
                  <Point X="3.446753662109" Y="-0.064067565918" />
                  <Point X="3.4481015625" Y="-0.07381615448" />
                  <Point X="3.463680664062" Y="-0.155164123535" />
                  <Point X="3.470527099609" Y="-0.176665542603" />
                  <Point X="3.480301025391" Y="-0.198129302979" />
                  <Point X="3.492024902344" Y="-0.217408966064" />
                  <Point X="3.500793701172" Y="-0.228582565308" />
                  <Point X="3.547530761719" Y="-0.288136627197" />
                  <Point X="3.566684570312" Y="-0.306531036377" />
                  <Point X="3.595611328125" Y="-0.327399017334" />
                  <Point X="3.603650390625" Y="-0.332603179932" />
                  <Point X="3.681545654297" Y="-0.377628143311" />
                  <Point X="3.692710205078" Y="-0.383139038086" />
                  <Point X="3.716579833984" Y="-0.392149932861" />
                  <Point X="4.8112578125" Y="-0.685468078613" />
                  <Point X="4.891472167969" Y="-0.706961486816" />
                  <Point X="4.855022460938" Y="-0.948726928711" />
                  <Point X="4.842889648438" Y="-1.001894897461" />
                  <Point X="4.801173828125" Y="-1.18469909668" />
                  <Point X="3.662570068359" Y="-1.034798950195" />
                  <Point X="3.424382080078" Y="-1.003441040039" />
                  <Point X="3.408035644531" Y="-1.002710327148" />
                  <Point X="3.374658447266" Y="-1.005508728027" />
                  <Point X="3.345974853516" Y="-1.011743225098" />
                  <Point X="3.193094238281" Y="-1.044972412109" />
                  <Point X="3.163973632812" Y="-1.056597167969" />
                  <Point X="3.136147216797" Y="-1.073489624023" />
                  <Point X="3.112397216797" Y="-1.093960449219" />
                  <Point X="3.095059814453" Y="-1.114812133789" />
                  <Point X="3.002653076172" Y="-1.225948608398" />
                  <Point X="2.987932617188" Y="-1.250330566406" />
                  <Point X="2.976589355469" Y="-1.277715698242" />
                  <Point X="2.969757568359" Y="-1.305365478516" />
                  <Point X="2.967272705078" Y="-1.332369140625" />
                  <Point X="2.954028564453" Y="-1.476295776367" />
                  <Point X="2.956347167969" Y="-1.507564208984" />
                  <Point X="2.964078613281" Y="-1.539185180664" />
                  <Point X="2.976450195312" Y="-1.567996826172" />
                  <Point X="2.99232421875" Y="-1.59268762207" />
                  <Point X="3.076930419922" Y="-1.724287109375" />
                  <Point X="3.086932861328" Y="-1.737239013672" />
                  <Point X="3.110628417969" Y="-1.760909179688" />
                  <Point X="4.12649609375" Y="-2.540412353516" />
                  <Point X="4.213122558594" Y="-2.606883300781" />
                  <Point X="4.124810058594" Y="-2.749786132812" />
                  <Point X="4.099717285156" Y="-2.785439453125" />
                  <Point X="4.028981445312" Y="-2.8859453125" />
                  <Point X="3.013093261719" Y="-2.299421630859" />
                  <Point X="2.800954589844" Y="-2.176943115234" />
                  <Point X="2.786131591797" Y="-2.170012451172" />
                  <Point X="2.754224609375" Y="-2.159825195312" />
                  <Point X="2.720086669922" Y="-2.153659912109" />
                  <Point X="2.538134277344" Y="-2.120799560547" />
                  <Point X="2.506781982422" Y="-2.120395263672" />
                  <Point X="2.474609130859" Y="-2.125353027344" />
                  <Point X="2.444833496094" Y="-2.135176757812" />
                  <Point X="2.416473144531" Y="-2.150102539062" />
                  <Point X="2.265315429688" Y="-2.229655761719" />
                  <Point X="2.242384521484" Y="-2.246548828125" />
                  <Point X="2.221424804688" Y="-2.267508544922" />
                  <Point X="2.204531738281" Y="-2.290439453125" />
                  <Point X="2.189605957031" Y="-2.318799804688" />
                  <Point X="2.110052734375" Y="-2.469957519531" />
                  <Point X="2.100229003906" Y="-2.499733154297" />
                  <Point X="2.095271240234" Y="-2.531906005859" />
                  <Point X="2.095675537109" Y="-2.563258300781" />
                  <Point X="2.101840820312" Y="-2.597396240234" />
                  <Point X="2.134701171875" Y="-2.779348632812" />
                  <Point X="2.138985595703" Y="-2.795141845703" />
                  <Point X="2.151819091797" Y="-2.826078613281" />
                  <Point X="2.804616455078" Y="-3.956756347656" />
                  <Point X="2.861283203125" Y="-4.054906494141" />
                  <Point X="2.781850097656" Y="-4.111643554688" />
                  <Point X="2.753796630859" Y="-4.129802246094" />
                  <Point X="2.701764892578" Y="-4.163481445312" />
                  <Point X="1.920427368164" Y="-3.145222900391" />
                  <Point X="1.758546142578" Y="-2.934255126953" />
                  <Point X="1.747503540039" Y="-2.922178955078" />
                  <Point X="1.721923828125" Y="-2.900557373047" />
                  <Point X="1.688254516602" Y="-2.878911132812" />
                  <Point X="1.508800537109" Y="-2.763538574219" />
                  <Point X="1.479989746094" Y="-2.751167236328" />
                  <Point X="1.448368408203" Y="-2.743435546875" />
                  <Point X="1.417099609375" Y="-2.741116699219" />
                  <Point X="1.380276489258" Y="-2.744505126953" />
                  <Point X="1.184012695312" Y="-2.762565673828" />
                  <Point X="1.156362670898" Y="-2.769397460938" />
                  <Point X="1.128977661133" Y="-2.780740722656" />
                  <Point X="1.104595825195" Y="-2.795461425781" />
                  <Point X="1.076161987305" Y="-2.819103271484" />
                  <Point X="0.924611938477" Y="-2.945111816406" />
                  <Point X="0.904141052246" Y="-2.968861816406" />
                  <Point X="0.887248779297" Y="-2.996688476562" />
                  <Point X="0.875624206543" Y="-3.025809082031" />
                  <Point X="0.867122741699" Y="-3.064923095703" />
                  <Point X="0.821809936523" Y="-3.273396972656" />
                  <Point X="0.819724487305" Y="-3.289627441406" />
                  <Point X="0.819742248535" Y="-3.323120117188" />
                  <Point X="1.004739868164" Y="-4.728317382812" />
                  <Point X="1.022065185547" Y="-4.859915039062" />
                  <Point X="0.975682617188" Y="-4.87008203125" />
                  <Point X="0.949757385254" Y="-4.874791992188" />
                  <Point X="0.929315551758" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058428344727" Y="-4.75263671875" />
                  <Point X="-1.09815612793" Y="-4.742414550781" />
                  <Point X="-1.141246337891" Y="-4.731328125" />
                  <Point X="-1.128393554688" Y="-4.633702148438" />
                  <Point X="-1.120775634766" Y="-4.575838378906" />
                  <Point X="-1.120077514648" Y="-4.568099609375" />
                  <Point X="-1.119451782227" Y="-4.541033691406" />
                  <Point X="-1.121759277344" Y="-4.509328125" />
                  <Point X="-1.123334106445" Y="-4.497690429688" />
                  <Point X="-1.132896240234" Y="-4.449618164062" />
                  <Point X="-1.183861450195" Y="-4.193398925781" />
                  <Point X="-1.188125" Y="-4.17846875" />
                  <Point X="-1.19902734375" Y="-4.149502441406" />
                  <Point X="-1.205666015625" Y="-4.135466308594" />
                  <Point X="-1.221738037109" Y="-4.10762890625" />
                  <Point X="-1.23057434082" Y="-4.094861816406" />
                  <Point X="-1.250208251953" Y="-4.0709375" />
                  <Point X="-1.261006103516" Y="-4.059780273438" />
                  <Point X="-1.297856567383" Y="-4.027462890625" />
                  <Point X="-1.494266845703" Y="-3.855215332031" />
                  <Point X="-1.50673828125" Y="-3.845965820312" />
                  <Point X="-1.533021606445" Y="-3.829621582031" />
                  <Point X="-1.546833618164" Y="-3.822526855469" />
                  <Point X="-1.576530761719" Y="-3.810225830078" />
                  <Point X="-1.591313232422" Y="-3.805476074219" />
                  <Point X="-1.621454589844" Y="-3.798447998047" />
                  <Point X="-1.636813476562" Y="-3.796169677734" />
                  <Point X="-1.685722412109" Y="-3.792963867188" />
                  <Point X="-1.94640246582" Y="-3.775878173828" />
                  <Point X="-1.961927612305" Y="-3.776132324219" />
                  <Point X="-1.992729492188" Y="-3.779166015625" />
                  <Point X="-2.008006103516" Y="-3.781945556641" />
                  <Point X="-2.03905480957" Y="-3.790265136719" />
                  <Point X="-2.053673828125" Y="-3.79549609375" />
                  <Point X="-2.081864990234" Y="-3.808269287109" />
                  <Point X="-2.095436767578" Y="-3.815811523438" />
                  <Point X="-2.136190429687" Y="-3.843042236328" />
                  <Point X="-2.353403076172" Y="-3.9881796875" />
                  <Point X="-2.359681640625" Y="-3.992757080078" />
                  <Point X="-2.380442138672" Y="-4.010131103516" />
                  <Point X="-2.402759277344" Y="-4.032771484375" />
                  <Point X="-2.410471191406" Y="-4.041629638672" />
                  <Point X="-2.503202636719" Y="-4.162479492188" />
                  <Point X="-2.747581542969" Y="-4.011166015625" />
                  <Point X="-2.8025859375" Y="-3.968814453125" />
                  <Point X="-2.980862792969" Y="-3.831547363281" />
                  <Point X="-2.459094970703" Y="-2.927819335938" />
                  <Point X="-2.341488769531" Y="-2.724119628906" />
                  <Point X="-2.336204833984" Y="-2.713482421875" />
                  <Point X="-2.323723144531" Y="-2.6838359375" />
                  <Point X="-2.31891015625" Y="-2.669174560547" />
                  <Point X="-2.313494873047" Y="-2.646644287109" />
                  <Point X="-2.310867675781" Y="-2.623623046875" />
                  <Point X="-2.311067626953" Y="-2.600451904297" />
                  <Point X="-2.311876953125" Y="-2.588872070312" />
                  <Point X="-2.316021484375" Y="-2.557390380859" />
                  <Point X="-2.318426269531" Y="-2.545274658203" />
                  <Point X="-2.324788085938" Y="-2.521456787109" />
                  <Point X="-2.334187255859" Y="-2.498664794922" />
                  <Point X="-2.346463623047" Y="-2.477286132812" />
                  <Point X="-2.353297851562" Y="-2.466997314453" />
                  <Point X="-2.365622314453" Y="-2.450831298828" />
                  <Point X="-2.382370361328" Y="-2.431673583984" />
                  <Point X="-2.396978271484" Y="-2.417065673828" />
                  <Point X="-2.408817138672" Y="-2.407020751953" />
                  <Point X="-2.433974121094" Y="-2.388993164062" />
                  <Point X="-2.447292236328" Y="-2.381010498047" />
                  <Point X="-2.47612109375" Y="-2.366793457031" />
                  <Point X="-2.490561523438" Y="-2.361086914062" />
                  <Point X="-2.520179199219" Y="-2.352102294922" />
                  <Point X="-2.550865234375" Y="-2.348062255859" />
                  <Point X="-2.581799316406" Y="-2.349074951172" />
                  <Point X="-2.597224609375" Y="-2.350849609375" />
                  <Point X="-2.628750976562" Y="-2.357120605469" />
                  <Point X="-2.643680908203" Y="-2.361384033203" />
                  <Point X="-2.672647216797" Y="-2.372286132812" />
                  <Point X="-2.68668359375" Y="-2.378924804688" />
                  <Point X="-3.772383300781" Y="-3.00575390625" />
                  <Point X="-3.793089355469" Y="-3.017708496094" />
                  <Point X="-4.004015869141" Y="-2.74059375" />
                  <Point X="-4.043444335938" Y="-2.674478271484" />
                  <Point X="-4.181265136719" Y="-2.443373535156" />
                  <Point X="-3.253894775391" Y="-1.731776733398" />
                  <Point X="-3.048122314453" Y="-1.573882080078" />
                  <Point X="-3.039158447266" Y="-1.566065551758" />
                  <Point X="-3.016266601562" Y="-1.5434296875" />
                  <Point X="-3.002880615234" Y="-1.526827270508" />
                  <Point X="-2.984184326172" Y="-1.497403808594" />
                  <Point X="-2.976197509766" Y="-1.481827270508" />
                  <Point X="-2.968266845703" Y="-1.462059692383" />
                  <Point X="-2.960673583984" Y="-1.438951049805" />
                  <Point X="-2.954186279297" Y="-1.413903076172" />
                  <Point X="-2.951952880859" Y="-1.402395019531" />
                  <Point X="-2.947838623047" Y="-1.370913452148" />
                  <Point X="-2.94708203125" Y="-1.35569934082" />
                  <Point X="-2.94778125" Y="-1.332830932617" />
                  <Point X="-2.951229248047" Y="-1.310212402344" />
                  <Point X="-2.957375732422" Y="-1.288174560547" />
                  <Point X="-2.961114746094" Y="-1.277341430664" />
                  <Point X="-2.973119628906" Y="-1.248358886719" />
                  <Point X="-2.978576171875" Y="-1.237282104492" />
                  <Point X="-2.990880371094" Y="-1.21592956543" />
                  <Point X="-3.005850341797" Y="-1.196353149414" />
                  <Point X="-3.023232910156" Y="-1.178883911133" />
                  <Point X="-3.032493408203" Y="-1.170715209961" />
                  <Point X="-3.049179443359" Y="-1.157823486328" />
                  <Point X="-3.068970214844" Y="-1.144431884766" />
                  <Point X="-3.091269042969" Y="-1.131307739258" />
                  <Point X="-3.105434326172" Y="-1.124480957031" />
                  <Point X="-3.134697265625" Y="-1.113257080078" />
                  <Point X="-3.149795410156" Y="-1.108860107422" />
                  <Point X="-3.181683105469" Y="-1.102378540039" />
                  <Point X="-3.197298828125" Y="-1.100532470703" />
                  <Point X="-3.228622070312" Y="-1.09944140625" />
                  <Point X="-3.244329101562" Y="-1.100196533203" />
                  <Point X="-4.614921875" Y="-1.280638793945" />
                  <Point X="-4.660920410156" Y="-1.286694458008" />
                  <Point X="-4.740762207031" Y="-0.974117980957" />
                  <Point X="-4.751193359375" Y="-0.901183654785" />
                  <Point X="-4.786452636719" Y="-0.65465435791" />
                  <Point X="-3.742693847656" Y="-0.374980255127" />
                  <Point X="-3.508288085938" Y="-0.312171325684" />
                  <Point X="-3.498660400391" Y="-0.309032867432" />
                  <Point X="-3.479793212891" Y="-0.301748718262" />
                  <Point X="-3.470553710938" Y="-0.297602874756" />
                  <Point X="-3.450601074219" Y="-0.287339416504" />
                  <Point X="-3.429176269531" Y="-0.274470306396" />
                  <Point X="-3.405807861328" Y="-0.258251251221" />
                  <Point X="-3.396477294922" Y="-0.250868255615" />
                  <Point X="-3.372522949219" Y="-0.229342300415" />
                  <Point X="-3.358343994141" Y="-0.213373672485" />
                  <Point X="-3.338241943359" Y="-0.184823654175" />
                  <Point X="-3.329513671875" Y="-0.169617004395" />
                  <Point X="-3.320626953125" Y="-0.150170440674" />
                  <Point X="-3.3119765625" Y="-0.127518234253" />
                  <Point X="-3.304187011719" Y="-0.102420280457" />
                  <Point X="-3.300709472656" Y="-0.086501434326" />
                  <Point X="-3.296525878906" Y="-0.054303619385" />
                  <Point X="-3.295819824219" Y="-0.038024803162" />
                  <Point X="-3.29728125" Y="-0.00367418623" />
                  <Point X="-3.298592773438" Y="0.008521957397" />
                  <Point X="-3.302776367188" Y="0.032643604279" />
                  <Point X="-3.3056484375" Y="0.044569103241" />
                  <Point X="-3.313437988281" Y="0.069667053223" />
                  <Point X="-3.317669189453" Y="0.080786575317" />
                  <Point X="-3.330984863281" Y="0.110109870911" />
                  <Point X="-3.342013427734" Y="0.128529602051" />
                  <Point X="-3.363355224609" Y="0.156444839478" />
                  <Point X="-3.375327392578" Y="0.169406692505" />
                  <Point X="-3.391530517578" Y="0.183967422485" />
                  <Point X="-3.410193359375" Y="0.198734741211" />
                  <Point X="-3.433561767578" Y="0.214953674316" />
                  <Point X="-3.440484130859" Y="0.219328674316" />
                  <Point X="-3.465615234375" Y="0.23283404541" />
                  <Point X="-3.496566650391" Y="0.245635681152" />
                  <Point X="-3.508288085938" Y="0.249611343384" />
                  <Point X="-4.7576484375" Y="0.584376220703" />
                  <Point X="-4.785446289062" Y="0.591824707031" />
                  <Point X="-4.73133203125" Y="0.957522949219" />
                  <Point X="-4.710330566406" Y="1.035026123047" />
                  <Point X="-4.6335859375" Y="1.318237060547" />
                  <Point X="-3.946093994141" Y="1.227727050781" />
                  <Point X="-3.778066162109" Y="1.20560559082" />
                  <Point X="-3.76773828125" Y="1.204815429688" />
                  <Point X="-3.747057861328" Y="1.204364257812" />
                  <Point X="-3.736705322266" Y="1.20470324707" />
                  <Point X="-3.715143554688" Y="1.20658984375" />
                  <Point X="-3.704889892578" Y="1.208053833008" />
                  <Point X="-3.684603271484" Y="1.212089233398" />
                  <Point X="-3.664867431641" Y="1.217719848633" />
                  <Point X="-3.613145507812" Y="1.234027709961" />
                  <Point X="-3.603451416016" Y="1.237676269531" />
                  <Point X="-3.584518554688" Y="1.246006713867" />
                  <Point X="-3.575278564453" Y="1.250689331055" />
                  <Point X="-3.556534423828" Y="1.261511108398" />
                  <Point X="-3.547859619141" Y="1.267171508789" />
                  <Point X="-3.531178710938" Y="1.279402832031" />
                  <Point X="-3.515928710938" Y="1.293376953125" />
                  <Point X="-3.502290283203" Y="1.308928466797" />
                  <Point X="-3.495895751953" Y="1.317076416016" />
                  <Point X="-3.483481201172" Y="1.334806152344" />
                  <Point X="-3.478011474609" Y="1.343602172852" />
                  <Point X="-3.468062255859" Y="1.361737426758" />
                  <Point X="-3.459688964844" Y="1.380476928711" />
                  <Point X="-3.438935302734" Y="1.430580688477" />
                  <Point X="-3.435499267578" Y="1.44035144043" />
                  <Point X="-3.4297109375" Y="1.460210083008" />
                  <Point X="-3.427358642578" Y="1.470298095703" />
                  <Point X="-3.423600341797" Y="1.491613037109" />
                  <Point X="-3.422360595703" Y="1.501897094727" />
                  <Point X="-3.421008056641" Y="1.522537109375" />
                  <Point X="-3.421910400391" Y="1.543200439453" />
                  <Point X="-3.425056884766" Y="1.563644042969" />
                  <Point X="-3.427188232422" Y="1.573780395508" />
                  <Point X="-3.432790039062" Y="1.594686767578" />
                  <Point X="-3.43601171875" Y="1.604529785156" />
                  <Point X="-3.443508300781" Y="1.623807861328" />
                  <Point X="-3.452481445312" Y="1.642268066406" />
                  <Point X="-3.477522949219" Y="1.690372436523" />
                  <Point X="-3.482799316406" Y="1.699285766602" />
                  <Point X="-3.494291259766" Y="1.716484863281" />
                  <Point X="-3.500506835938" Y="1.724770629883" />
                  <Point X="-3.514419189453" Y="1.741350830078" />
                  <Point X="-3.5215" Y="1.748910888672" />
                  <Point X="-3.536442138672" Y="1.76321496582" />
                  <Point X="-3.544303466797" Y="1.769958618164" />
                  <Point X="-4.227614257812" Y="2.294281982422" />
                  <Point X="-4.002291259766" Y="2.680314941406" />
                  <Point X="-3.946670898438" Y="2.751806884766" />
                  <Point X="-3.726338623047" Y="3.035012939453" />
                  <Point X="-3.357354980469" Y="2.821979980469" />
                  <Point X="-3.254156982422" Y="2.7623984375" />
                  <Point X="-3.244918457031" Y="2.757716552734" />
                  <Point X="-3.225986083984" Y="2.749385986328" />
                  <Point X="-3.216292236328" Y="2.745737304688" />
                  <Point X="-3.195650146484" Y="2.739228759766" />
                  <Point X="-3.185615478516" Y="2.736656982422" />
                  <Point X="-3.165327392578" Y="2.732621582031" />
                  <Point X="-3.141558837891" Y="2.729975585938" />
                  <Point X="-3.069524658203" Y="2.723673339844" />
                  <Point X="-3.059173095703" Y="2.723334472656" />
                  <Point X="-3.038493164062" Y="2.723785644531" />
                  <Point X="-3.028164794922" Y="2.724575683594" />
                  <Point X="-3.006706054688" Y="2.727400878906" />
                  <Point X="-2.996525878906" Y="2.729310791016" />
                  <Point X="-2.976434082031" Y="2.734227294922" />
                  <Point X="-2.956996826172" Y="2.741302001953" />
                  <Point X="-2.9384453125" Y="2.750450683594" />
                  <Point X="-2.929419433594" Y="2.75553125" />
                  <Point X="-2.911165039062" Y="2.767160644531" />
                  <Point X="-2.902746337891" Y="2.773193359375" />
                  <Point X="-2.886615722656" Y="2.786139404297" />
                  <Point X="-2.869310546875" Y="2.802645751953" />
                  <Point X="-2.818180175781" Y="2.853776123047" />
                  <Point X="-2.811265869141" Y="2.861488769531" />
                  <Point X="-2.798318115234" Y="2.87762109375" />
                  <Point X="-2.792284667969" Y="2.886040771484" />
                  <Point X="-2.780655273438" Y="2.904295166016" />
                  <Point X="-2.775574707031" Y="2.913321044922" />
                  <Point X="-2.766426025391" Y="2.931872558594" />
                  <Point X="-2.759351318359" Y="2.951309814453" />
                  <Point X="-2.754434814453" Y="2.971401611328" />
                  <Point X="-2.752524902344" Y="2.981581787109" />
                  <Point X="-2.749699707031" Y="3.003040527344" />
                  <Point X="-2.748909667969" Y="3.013368896484" />
                  <Point X="-2.748458496094" Y="3.034048828125" />
                  <Point X="-2.749979736328" Y="3.057915527344" />
                  <Point X="-2.756281982422" Y="3.129949951172" />
                  <Point X="-2.757745605469" Y="3.140203369141" />
                  <Point X="-2.761781005859" Y="3.160491455078" />
                  <Point X="-2.764352783203" Y="3.170526123047" />
                  <Point X="-2.770861328125" Y="3.191168212891" />
                  <Point X="-2.774510009766" Y="3.200862060547" />
                  <Point X="-2.782840576172" Y="3.219794433594" />
                  <Point X="-2.787522460938" Y="3.229032958984" />
                  <Point X="-3.059387207031" Y="3.699916503906" />
                  <Point X="-2.648374023438" Y="4.015036376953" />
                  <Point X="-2.560775390625" Y="4.063704589844" />
                  <Point X="-2.192524902344" Y="4.268296386719" />
                  <Point X="-2.150156005859" Y="4.213080078125" />
                  <Point X="-2.118563964844" Y="4.171908691406" />
                  <Point X="-2.111821533203" Y="4.164049316406" />
                  <Point X="-2.097518798828" Y="4.149107910156" />
                  <Point X="-2.089958740234" Y="4.142026367188" />
                  <Point X="-2.07337890625" Y="4.128113769531" />
                  <Point X="-2.065091308594" Y="4.121896972656" />
                  <Point X="-2.047891845703" Y="4.110404785156" />
                  <Point X="-2.0239375" Y="4.097298828125" />
                  <Point X="-1.943763549805" Y="4.055562255859" />
                  <Point X="-1.934328735352" Y="4.051287353516" />
                  <Point X="-1.915048583984" Y="4.043789794922" />
                  <Point X="-1.905203369141" Y="4.040567138672" />
                  <Point X="-1.884296630859" Y="4.034965576172" />
                  <Point X="-1.874162109375" Y="4.032834716797" />
                  <Point X="-1.853719238281" Y="4.029688232422" />
                  <Point X="-1.833052978516" Y="4.028785888672" />
                  <Point X="-1.812413574219" Y="4.030138916016" />
                  <Point X="-1.802132202148" Y="4.031378662109" />
                  <Point X="-1.780816894531" Y="4.035136962891" />
                  <Point X="-1.77073059082" Y="4.037488769531" />
                  <Point X="-1.75087121582" Y="4.043276855469" />
                  <Point X="-1.725430908203" Y="4.053203125" />
                  <Point X="-1.641924560547" Y="4.08779296875" />
                  <Point X="-1.632584960938" Y="4.092272949219" />
                  <Point X="-1.614451171875" Y="4.102221679688" />
                  <Point X="-1.605656738281" Y="4.107690429687" />
                  <Point X="-1.587927246094" Y="4.120104492188" />
                  <Point X="-1.579779296875" Y="4.126499023438" />
                  <Point X="-1.564226928711" Y="4.140137695312" />
                  <Point X="-1.550251708984" Y="4.155388671875" />
                  <Point X="-1.538020263672" Y="4.1720703125" />
                  <Point X="-1.532359863281" Y="4.180745117188" />
                  <Point X="-1.521538085938" Y="4.199489257812" />
                  <Point X="-1.516856201172" Y="4.208728027344" />
                  <Point X="-1.508525878906" Y="4.22766015625" />
                  <Point X="-1.499777709961" Y="4.25352734375" />
                  <Point X="-1.472598144531" Y="4.339729980469" />
                  <Point X="-1.470026611328" Y="4.349763671875" />
                  <Point X="-1.465990966797" Y="4.370051269531" />
                  <Point X="-1.46452722168" Y="4.380305175781" />
                  <Point X="-1.46264074707" Y="4.4018671875" />
                  <Point X="-1.462301757812" Y="4.412219238281" />
                  <Point X="-1.462752929688" Y="4.432898925781" />
                  <Point X="-1.46354309082" Y="4.4432265625" />
                  <Point X="-1.479266113281" Y="4.562655273438" />
                  <Point X="-0.931175720215" Y="4.716320800781" />
                  <Point X="-0.824992370605" Y="4.728747558594" />
                  <Point X="-0.365221984863" Y="4.782557128906" />
                  <Point X="-0.256195098877" Y="4.375663085938" />
                  <Point X="-0.22566633606" Y="4.261727539062" />
                  <Point X="-0.220435256958" Y="4.247107910156" />
                  <Point X="-0.207661849976" Y="4.218916503906" />
                  <Point X="-0.200119247437" Y="4.205344726562" />
                  <Point X="-0.182260864258" Y="4.178618164062" />
                  <Point X="-0.17260824585" Y="4.166456054688" />
                  <Point X="-0.151451339722" Y="4.1438671875" />
                  <Point X="-0.126897941589" Y="4.125026367188" />
                  <Point X="-0.099602897644" Y="4.110436523438" />
                  <Point X="-0.085356651306" Y="4.104260742188" />
                  <Point X="-0.054918682098" Y="4.093927978516" />
                  <Point X="-0.039857234955" Y="4.090155273438" />
                  <Point X="-0.009319986343" Y="4.08511328125" />
                  <Point X="0.021631772995" Y="4.08511328125" />
                  <Point X="0.052169025421" Y="4.090155273438" />
                  <Point X="0.06723046875" Y="4.093927978516" />
                  <Point X="0.097668441772" Y="4.104260742188" />
                  <Point X="0.111914535522" Y="4.110436523438" />
                  <Point X="0.139209732056" Y="4.125026367188" />
                  <Point X="0.163763122559" Y="4.143866699219" />
                  <Point X="0.184920181274" Y="4.166455566406" />
                  <Point X="0.1945730896" Y="4.178618164062" />
                  <Point X="0.212431182861" Y="4.205344726562" />
                  <Point X="0.219973495483" Y="4.218916015625" />
                  <Point X="0.232747039795" Y="4.247107421875" />
                  <Point X="0.237978271484" Y="4.261727539062" />
                  <Point X="0.378190216064" Y="4.785006347656" />
                  <Point X="0.82787677002" Y="4.737912109375" />
                  <Point X="0.915736877441" Y="4.716699707031" />
                  <Point X="1.453601806641" Y="4.586841796875" />
                  <Point X="1.508566040039" Y="4.566906738281" />
                  <Point X="1.858251464844" Y="4.440073242188" />
                  <Point X="1.913561523438" Y="4.414206542969" />
                  <Point X="2.250448486328" Y="4.256655761719" />
                  <Point X="2.303936523438" Y="4.225493164062" />
                  <Point X="2.629434570312" Y="4.035857666016" />
                  <Point X="2.679826660156" Y="4.000021484375" />
                  <Point X="2.817779785156" Y="3.901916748047" />
                  <Point X="2.201428710938" Y="2.834364990234" />
                  <Point X="2.06530859375" Y="2.598597900391" />
                  <Point X="2.061038818359" Y="2.590282714844" />
                  <Point X="2.04990625" Y="2.564580810547" />
                  <Point X="2.041512817383" Y="2.539813964844" />
                  <Point X="2.037909790039" Y="2.527915039062" />
                  <Point X="2.01983190918" Y="2.460312255859" />
                  <Point X="2.017420043945" Y="2.448170898438" />
                  <Point X="2.01419519043" Y="2.423677001953" />
                  <Point X="2.013382202148" Y="2.411324462891" />
                  <Point X="2.01336730957" Y="2.381454345703" />
                  <Point X="2.014733520508" Y="2.358612548828" />
                  <Point X="2.021782226562" Y="2.300155517578" />
                  <Point X="2.02380078125" Y="2.289033935547" />
                  <Point X="2.029143676758" Y="2.267110595703" />
                  <Point X="2.032468261719" Y="2.256308837891" />
                  <Point X="2.040734375" Y="2.234220214844" />
                  <Point X="2.045318237305" Y="2.223889160156" />
                  <Point X="2.055681640625" Y="2.203843017578" />
                  <Point X="2.068247558594" Y="2.184126464844" />
                  <Point X="2.104418701172" Y="2.130819335937" />
                  <Point X="2.112096923828" Y="2.120966552734" />
                  <Point X="2.128679199219" Y="2.102354003906" />
                  <Point X="2.137583251953" Y="2.093594238281" />
                  <Point X="2.160884765625" Y="2.073507324219" />
                  <Point X="2.178259765625" Y="2.060194335938" />
                  <Point X="2.231566894531" Y="2.02402331543" />
                  <Point X="2.241280517578" Y="2.018244873047" />
                  <Point X="2.261325683594" Y="2.007882080078" />
                  <Point X="2.271657226562" Y="2.003297851562" />
                  <Point X="2.293745605469" Y="1.995031982422" />
                  <Point X="2.304547363281" Y="1.991707397461" />
                  <Point X="2.326470214844" Y="1.986364746094" />
                  <Point X="2.348559082031" Y="1.983024047852" />
                  <Point X="2.407016113281" Y="1.975974975586" />
                  <Point X="2.419749023438" Y="1.975301513672" />
                  <Point X="2.445189697266" Y="1.975665893555" />
                  <Point X="2.457897460938" Y="1.976703369141" />
                  <Point X="2.489483642578" Y="1.981432861328" />
                  <Point X="2.510431884766" Y="1.985787719727" />
                  <Point X="2.578034667969" Y="2.003865600586" />
                  <Point X="2.583994384766" Y="2.005670776367" />
                  <Point X="2.604404541016" Y="2.013067260742" />
                  <Point X="2.627655029297" Y="2.02357409668" />
                  <Point X="2.636033935547" Y="2.027872558594" />
                  <Point X="3.892649902344" Y="2.753380126953" />
                  <Point X="3.940404785156" Y="2.780951416016" />
                  <Point X="4.043953857422" Y="2.637041259766" />
                  <Point X="4.072046875" Y="2.590617431641" />
                  <Point X="4.136884765625" Y="2.483472412109" />
                  <Point X="3.352166748047" Y="1.881336547852" />
                  <Point X="3.172951660156" Y="1.743819824219" />
                  <Point X="3.165673828125" Y="1.737630126953" />
                  <Point X="3.145156494141" Y="1.717659423828" />
                  <Point X="3.126783203125" Y="1.696501220703" />
                  <Point X="3.119447998047" Y="1.687513793945" />
                  <Point X="3.070794189453" Y="1.624041259766" />
                  <Point X="3.063776367188" Y="1.613498291016" />
                  <Point X="3.051205566406" Y="1.591572753906" />
                  <Point X="3.045652587891" Y="1.580189941406" />
                  <Point X="3.033782714844" Y="1.551079223633" />
                  <Point X="3.026739746094" Y="1.530513061523" />
                  <Point X="3.008616210938" Y="1.465707519531" />
                  <Point X="3.006225097656" Y="1.454661010742" />
                  <Point X="3.002771728516" Y="1.432362792969" />
                  <Point X="3.001709472656" Y="1.421110839844" />
                  <Point X="3.000893310547" Y="1.397540649414" />
                  <Point X="3.001174804688" Y="1.386241821289" />
                  <Point X="3.003077636719" Y="1.363757446289" />
                  <Point X="3.007490234375" Y="1.339043457031" />
                  <Point X="3.022367919922" Y="1.266938964844" />
                  <Point X="3.025787353516" Y="1.254622436523" />
                  <Point X="3.034248291016" Y="1.230560180664" />
                  <Point X="3.039289794922" Y="1.218814697266" />
                  <Point X="3.054079833984" Y="1.189897338867" />
                  <Point X="3.064510986328" Y="1.171985229492" />
                  <Point X="3.1049765625" Y="1.110479858398" />
                  <Point X="3.111739501953" Y="1.101424072266" />
                  <Point X="3.126292724609" Y="1.08417980957" />
                  <Point X="3.134083007812" Y="1.075991210938" />
                  <Point X="3.151326904297" Y="1.059901367188" />
                  <Point X="3.160034179688" Y="1.052696166992" />
                  <Point X="3.178243408203" Y="1.039370605469" />
                  <Point X="3.198747314453" Y="1.027057006836" />
                  <Point X="3.257387451172" Y="0.994047973633" />
                  <Point X="3.269168701172" Y="0.988444030762" />
                  <Point X="3.293387939453" Y="0.978903015137" />
                  <Point X="3.305825927734" Y="0.974966003418" />
                  <Point X="3.338676757813" Y="0.967002197266" />
                  <Point X="3.358546386719" Y="0.963291687012" />
                  <Point X="3.437831542969" Y="0.952812988281" />
                  <Point X="3.444029785156" Y="0.952199829102" />
                  <Point X="3.465716064453" Y="0.951222900391" />
                  <Point X="3.491217529297" Y="0.952032287598" />
                  <Point X="3.500603515625" Y="0.952797241211" />
                  <Point X="4.6943046875" Y="1.109951171875" />
                  <Point X="4.704704101562" Y="1.11132019043" />
                  <Point X="4.75268359375" Y="0.914235107422" />
                  <Point X="4.761537109375" Y="0.857369628906" />
                  <Point X="4.78387109375" Y="0.713920776367" />
                  <Point X="3.897376220703" Y="0.476385223389" />
                  <Point X="3.691991943359" Y="0.421352844238" />
                  <Point X="3.682728759766" Y="0.418354217529" />
                  <Point X="3.655651611328" Y="0.40749420166" />
                  <Point X="3.628956054688" Y="0.3940390625" />
                  <Point X="3.619389648438" Y="0.388868927002" />
                  <Point X="3.541494384766" Y="0.343844055176" />
                  <Point X="3.530771240234" Y="0.336630493164" />
                  <Point X="3.510403076172" Y="0.32081463623" />
                  <Point X="3.500758056641" Y="0.312212341309" />
                  <Point X="3.477677246094" Y="0.288618469238" />
                  <Point X="3.464028076172" Y="0.273053039551" />
                  <Point X="3.417291015625" Y="0.213499099731" />
                  <Point X="3.410854248047" Y="0.204208084106" />
                  <Point X="3.399130371094" Y="0.184928268433" />
                  <Point X="3.393843017578" Y="0.174939483643" />
                  <Point X="3.384069091797" Y="0.153475662231" />
                  <Point X="3.380005371094" Y="0.142929077148" />
                  <Point X="3.373159179688" Y="0.121428543091" />
                  <Point X="3.367453613281" Y="0.095211914062" />
                  <Point X="3.351874511719" Y="0.013864059448" />
                  <Point X="3.350306152344" Y="0.000915586174" />
                  <Point X="3.348958007812" Y="-0.025074546814" />
                  <Point X="3.349178222656" Y="-0.038116207123" />
                  <Point X="3.352101318359" Y="-0.07218800354" />
                  <Point X="3.354797119141" Y="-0.09168510437" />
                  <Point X="3.370376220703" Y="-0.173032958984" />
                  <Point X="3.373158935547" Y="-0.183987792969" />
                  <Point X="3.380005371094" Y="-0.205489227295" />
                  <Point X="3.384069091797" Y="-0.216035797119" />
                  <Point X="3.393843017578" Y="-0.23749961853" />
                  <Point X="3.399130615234" Y="-0.247488723755" />
                  <Point X="3.410854492188" Y="-0.266768371582" />
                  <Point X="3.426059570312" Y="-0.287232421875" />
                  <Point X="3.472796630859" Y="-0.346786346436" />
                  <Point X="3.481727783203" Y="-0.356656402588" />
                  <Point X="3.500881591797" Y="-0.375050720215" />
                  <Point X="3.511104248047" Y="-0.383575286865" />
                  <Point X="3.540031005859" Y="-0.404443267822" />
                  <Point X="3.556109130859" Y="-0.414851776123" />
                  <Point X="3.634004394531" Y="-0.459876647949" />
                  <Point X="3.639496582031" Y="-0.46281552124" />
                  <Point X="3.659158447266" Y="-0.472016906738" />
                  <Point X="3.683028076172" Y="-0.481027770996" />
                  <Point X="3.691991943359" Y="-0.483912841797" />
                  <Point X="4.784876953125" Y="-0.776750488281" />
                  <Point X="4.76161328125" Y="-0.931053161621" />
                  <Point X="4.750270507812" Y="-0.980759460449" />
                  <Point X="4.727801757812" Y="-1.079219726563" />
                  <Point X="3.674969970703" Y="-0.940611694336" />
                  <Point X="3.436781982422" Y="-0.909253723145" />
                  <Point X="3.428624511719" Y="-0.908535888672" />
                  <Point X="3.400098632812" Y="-0.908042480469" />
                  <Point X="3.366721435547" Y="-0.910840881348" />
                  <Point X="3.354480957031" Y="-0.912676208496" />
                  <Point X="3.325797363281" Y="-0.918910827637" />
                  <Point X="3.172916748047" Y="-0.952139892578" />
                  <Point X="3.157873535156" Y="-0.956742614746" />
                  <Point X="3.128752929688" Y="-0.96836730957" />
                  <Point X="3.114675292969" Y="-0.975389587402" />
                  <Point X="3.086848876953" Y="-0.992282043457" />
                  <Point X="3.074123779297" Y="-1.001530700684" />
                  <Point X="3.050373779297" Y="-1.022001586914" />
                  <Point X="3.039349121094" Y="-1.033223632812" />
                  <Point X="3.02201171875" Y="-1.054075317383" />
                  <Point X="2.929604980469" Y="-1.165211791992" />
                  <Point X="2.921325927734" Y="-1.17684777832" />
                  <Point X="2.90660546875" Y="-1.201229614258" />
                  <Point X="2.9001640625" Y="-1.213975708008" />
                  <Point X="2.888820800781" Y="-1.241360839844" />
                  <Point X="2.884362792969" Y="-1.254928222656" />
                  <Point X="2.877531005859" Y="-1.282577758789" />
                  <Point X="2.875157226562" Y="-1.296660400391" />
                  <Point X="2.872672363281" Y="-1.3236640625" />
                  <Point X="2.859428222656" Y="-1.467590698242" />
                  <Point X="2.859288574219" Y="-1.483321044922" />
                  <Point X="2.861607177734" Y="-1.514589355469" />
                  <Point X="2.864065429688" Y="-1.530127441406" />
                  <Point X="2.871796875" Y="-1.561748413086" />
                  <Point X="2.876785888672" Y="-1.576668212891" />
                  <Point X="2.889157470703" Y="-1.605479736328" />
                  <Point X="2.896540283203" Y="-1.619371826172" />
                  <Point X="2.912414306641" Y="-1.644062744141" />
                  <Point X="2.997020507812" Y="-1.775662231445" />
                  <Point X="3.001741943359" Y="-1.782353393555" />
                  <Point X="3.019793701172" Y="-1.804450195312" />
                  <Point X="3.043489257812" Y="-1.828120361328" />
                  <Point X="3.052796142578" Y="-1.836277587891" />
                  <Point X="4.068663818359" Y="-2.615781005859" />
                  <Point X="4.087170898438" Y="-2.629981933594" />
                  <Point X="4.04548828125" Y="-2.697430419922" />
                  <Point X="4.022029052734" Y="-2.730762695312" />
                  <Point X="4.001274414063" Y="-2.760252197266" />
                  <Point X="3.060593261719" Y="-2.217149169922" />
                  <Point X="2.848454589844" Y="-2.094670654297" />
                  <Point X="2.841191894531" Y="-2.090885253906" />
                  <Point X="2.815026123047" Y="-2.079513183594" />
                  <Point X="2.783119140625" Y="-2.069325927734" />
                  <Point X="2.771108398438" Y="-2.066337646484" />
                  <Point X="2.736970458984" Y="-2.060172363281" />
                  <Point X="2.555018066406" Y="-2.027311889648" />
                  <Point X="2.539359130859" Y="-2.025807495117" />
                  <Point X="2.508006835938" Y="-2.025403198242" />
                  <Point X="2.492313476562" Y="-2.026503540039" />
                  <Point X="2.460140625" Y="-2.031461303711" />
                  <Point X="2.444844482422" Y="-2.035136474609" />
                  <Point X="2.415068847656" Y="-2.044960083008" />
                  <Point X="2.400589355469" Y="-2.051108642578" />
                  <Point X="2.372229003906" Y="-2.066034423828" />
                  <Point X="2.221071289062" Y="-2.145587646484" />
                  <Point X="2.20896875" Y="-2.153170166016" />
                  <Point X="2.186037841797" Y="-2.170063232422" />
                  <Point X="2.175209472656" Y="-2.179373779297" />
                  <Point X="2.154249755859" Y="-2.200333496094" />
                  <Point X="2.144939208984" Y="-2.211161865234" />
                  <Point X="2.128046142578" Y="-2.234092773438" />
                  <Point X="2.120463623047" Y="-2.2461953125" />
                  <Point X="2.105537841797" Y="-2.274555664062" />
                  <Point X="2.025984619141" Y="-2.425713378906" />
                  <Point X="2.01983605957" Y="-2.440192871094" />
                  <Point X="2.010012329102" Y="-2.469968505859" />
                  <Point X="2.006337280273" Y="-2.485264648438" />
                  <Point X="2.001379516602" Y="-2.5174375" />
                  <Point X="2.000279174805" Y="-2.533130859375" />
                  <Point X="2.00068347168" Y="-2.564483154297" />
                  <Point X="2.002187866211" Y="-2.580142089844" />
                  <Point X="2.008353149414" Y="-2.614280029297" />
                  <Point X="2.041213500977" Y="-2.796232421875" />
                  <Point X="2.043015014648" Y="-2.804221435547" />
                  <Point X="2.051236083984" Y="-2.83154296875" />
                  <Point X="2.064069580078" Y="-2.862479736328" />
                  <Point X="2.069546630859" Y="-2.873578613281" />
                  <Point X="2.722343994141" Y="-4.004256347656" />
                  <Point X="2.735893066406" Y="-4.027724365234" />
                  <Point X="2.723754150391" Y="-4.036083496094" />
                  <Point X="1.995795898438" Y="-3.087390625" />
                  <Point X="1.833914550781" Y="-2.876422851562" />
                  <Point X="1.828654418945" Y="-2.870147216797" />
                  <Point X="1.808830322266" Y="-2.849625488281" />
                  <Point X="1.783250610352" Y="-2.82800390625" />
                  <Point X="1.773298583984" Y="-2.820647216797" />
                  <Point X="1.739629272461" Y="-2.799000976562" />
                  <Point X="1.560175292969" Y="-2.683628417969" />
                  <Point X="1.546283935547" Y="-2.67624609375" />
                  <Point X="1.517473144531" Y="-2.663874755859" />
                  <Point X="1.502553344727" Y="-2.658885742188" />
                  <Point X="1.470932006836" Y="-2.651154052734" />
                  <Point X="1.455394165039" Y="-2.648695800781" />
                  <Point X="1.424125244141" Y="-2.646376953125" />
                  <Point X="1.40839440918" Y="-2.646516357422" />
                  <Point X="1.371571411133" Y="-2.649904785156" />
                  <Point X="1.175307739258" Y="-2.667965332031" />
                  <Point X="1.161225341797" Y="-2.670339111328" />
                  <Point X="1.133575317383" Y="-2.677170898438" />
                  <Point X="1.120007568359" Y="-2.68162890625" />
                  <Point X="1.092622680664" Y="-2.692972167969" />
                  <Point X="1.079876220703" Y="-2.6994140625" />
                  <Point X="1.055494262695" Y="-2.714134765625" />
                  <Point X="1.043858764648" Y="-2.722413574219" />
                  <Point X="1.015424804688" Y="-2.746055419922" />
                  <Point X="0.863874755859" Y="-2.872063964844" />
                  <Point X="0.852653137207" Y="-2.883088134766" />
                  <Point X="0.832182312012" Y="-2.906838134766" />
                  <Point X="0.822933044434" Y="-2.919564208984" />
                  <Point X="0.806040710449" Y="-2.947390869141" />
                  <Point X="0.799018859863" Y="-2.961468261719" />
                  <Point X="0.787394165039" Y="-2.990588867188" />
                  <Point X="0.782791625977" Y="-3.005631835938" />
                  <Point X="0.774290222168" Y="-3.044745849609" />
                  <Point X="0.728977355957" Y="-3.253219726562" />
                  <Point X="0.727584594727" Y="-3.261290039062" />
                  <Point X="0.724724487305" Y="-3.289677734375" />
                  <Point X="0.7247421875" Y="-3.323170410156" />
                  <Point X="0.725554992676" Y="-3.335520019531" />
                  <Point X="0.833091247559" Y="-4.152339355469" />
                  <Point X="0.718349060059" Y="-3.724115478516" />
                  <Point X="0.655064941406" Y="-3.487936523438" />
                  <Point X="0.652606262207" Y="-3.480124511719" />
                  <Point X="0.642145324707" Y="-3.453580566406" />
                  <Point X="0.626787353516" Y="-3.423816162109" />
                  <Point X="0.620407714844" Y="-3.413210449219" />
                  <Point X="0.594541992188" Y="-3.375942626953" />
                  <Point X="0.456680145264" Y="-3.177310058594" />
                  <Point X="0.44667098999" Y="-3.165173095703" />
                  <Point X="0.424786834717" Y="-3.142717529297" />
                  <Point X="0.412912322998" Y="-3.132399169922" />
                  <Point X="0.386657196045" Y="-3.113155273438" />
                  <Point X="0.373242340088" Y="-3.104937744141" />
                  <Point X="0.345241210938" Y="-3.090829589844" />
                  <Point X="0.330654907227" Y="-3.084938964844" />
                  <Point X="0.290629272461" Y="-3.072516357422" />
                  <Point X="0.077295951843" Y="-3.006305664062" />
                  <Point X="0.063377418518" Y="-3.003109619141" />
                  <Point X="0.035217838287" Y="-2.998840087891" />
                  <Point X="0.020976791382" Y="-2.997766601562" />
                  <Point X="-0.008664708138" Y="-2.997766601562" />
                  <Point X="-0.022905158997" Y="-2.998840087891" />
                  <Point X="-0.051064296722" Y="-3.003109375" />
                  <Point X="-0.064982978821" Y="-3.006305175781" />
                  <Point X="-0.105008758545" Y="-3.018727539062" />
                  <Point X="-0.318342224121" Y="-3.084938476562" />
                  <Point X="-0.332929107666" Y="-3.090829345703" />
                  <Point X="-0.36093057251" Y="-3.104937744141" />
                  <Point X="-0.374345275879" Y="-3.113155273438" />
                  <Point X="-0.400600524902" Y="-3.132399169922" />
                  <Point X="-0.412475219727" Y="-3.142717773438" />
                  <Point X="-0.434358886719" Y="-3.165173095703" />
                  <Point X="-0.444368041992" Y="-3.177309814453" />
                  <Point X="-0.470233642578" Y="-3.214577392578" />
                  <Point X="-0.60809564209" Y="-3.413210205078" />
                  <Point X="-0.612470336914" Y="-3.420132324219" />
                  <Point X="-0.625976745605" Y="-3.445265136719" />
                  <Point X="-0.638777832031" Y="-3.476215820312" />
                  <Point X="-0.642752990723" Y="-3.487936523438" />
                  <Point X="-0.980050170898" Y="-4.746747558594" />
                  <Point X="-0.985424987793" Y="-4.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.669035270248" Y="-1.071482966235" />
                  <Point X="4.691396500364" Y="-0.751702477252" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.574672002508" Y="-1.059059797581" />
                  <Point X="4.597916047603" Y="-0.726654466222" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.697023635602" Y="0.690650073261" />
                  <Point X="4.721589599707" Y="1.041959727203" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.480308734769" Y="-1.046636628926" />
                  <Point X="4.504435594842" Y="-0.701606455192" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.599973238469" Y="0.664645501265" />
                  <Point X="4.630524884261" Y="1.10155439133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.385945467029" Y="-1.034213460272" />
                  <Point X="4.410955142081" Y="-0.676558444162" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.502922841336" Y="0.63864092927" />
                  <Point X="4.534408047483" Y="1.088900354409" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.29158219929" Y="-1.021790291618" />
                  <Point X="4.317474689321" Y="-0.651510433133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.405872444202" Y="0.612636357274" />
                  <Point X="4.438291210705" Y="1.076246317487" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.083476474193" Y="-2.635960044854" />
                  <Point X="4.084061351915" Y="-2.627595903746" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.19721893155" Y="-1.009367122963" />
                  <Point X="4.22399423656" Y="-0.626462422103" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.308822047069" Y="0.586631785279" />
                  <Point X="4.342174373927" Y="1.063592280565" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.980396047616" Y="-2.74819805544" />
                  <Point X="3.993678994003" Y="-2.558243072253" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.102855663811" Y="-0.996943954309" />
                  <Point X="4.130513783799" Y="-0.601414411073" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.211771649936" Y="0.560627213283" />
                  <Point X="4.24605753715" Y="1.050938243643" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.888859604205" Y="-2.695349415489" />
                  <Point X="3.903296639826" Y="-2.488890187341" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.008492396071" Y="-0.984520785655" />
                  <Point X="4.037033331038" Y="-0.576366400044" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.114721252802" Y="0.534622641288" />
                  <Point X="4.149940700372" Y="1.038284206721" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.797323160795" Y="-2.642500775539" />
                  <Point X="3.812914285649" Y="-2.419537302428" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.914129128332" Y="-0.972097617001" />
                  <Point X="3.943552878277" Y="-0.551318389014" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.017670855669" Y="0.508618069292" />
                  <Point X="4.053823863594" Y="1.025630169799" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.705786717385" Y="-2.589652135588" />
                  <Point X="3.722531931473" Y="-2.350184417516" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.819765860592" Y="-0.959674448346" />
                  <Point X="3.850072425516" Y="-0.526270377984" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.920620458536" Y="0.482613497296" />
                  <Point X="3.957707026816" Y="1.012976132877" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.056205134447" Y="2.421564697034" />
                  <Point X="4.068442907356" Y="2.596573003127" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.614250273974" Y="-2.536803495638" />
                  <Point X="3.632149577296" Y="-2.280831532603" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.725402592853" Y="-0.947251279692" />
                  <Point X="3.756591972755" Y="-0.501222366955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.823570065618" Y="0.456608985583" />
                  <Point X="3.861590190038" Y="1.000322095955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.955573589552" Y="2.34434732608" />
                  <Point X="3.982056131357" Y="2.723065318065" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.522713830564" Y="-2.483954855687" />
                  <Point X="3.541767223119" Y="-2.211478647691" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.631039323906" Y="-0.93482812831" />
                  <Point X="3.663293092436" Y="-0.473577749034" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.726519674027" Y="0.430604492855" />
                  <Point X="3.76547335326" Y="0.987668059033" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.854942044657" Y="2.267129955127" />
                  <Point X="3.888788057564" Y="2.751150489825" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.431177387153" Y="-2.431106215737" />
                  <Point X="3.451384868943" Y="-2.142125762778" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.536676053572" Y="-0.922404996758" />
                  <Point X="3.571543779689" Y="-0.423773282419" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.628721942221" Y="0.393912537121" />
                  <Point X="3.669356516482" Y="0.975014022111" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.754310499762" Y="2.189912584174" />
                  <Point X="3.789549595893" Y="2.693855137134" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.339640943743" Y="-2.378257575786" />
                  <Point X="3.361002514766" Y="-2.072772877866" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.442312783238" Y="-0.909981865205" />
                  <Point X="3.481056912361" Y="-0.355915005215" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.529410527058" Y="0.335573900883" />
                  <Point X="3.573239679704" Y="0.962359985189" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.653678954867" Y="2.112695213221" />
                  <Point X="3.690311134223" Y="2.636559784443" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.248104500332" Y="-2.325408935835" />
                  <Point X="3.270620160589" Y="-2.003419992953" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.346775276069" Y="-0.914351102738" />
                  <Point X="3.394074732355" Y="-0.23793736426" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.426459154726" Y="0.225181451978" />
                  <Point X="3.477254526978" Y="0.95158911794" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.553047409972" Y="2.035477842267" />
                  <Point X="3.591072672552" Y="2.579264431753" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.156568056922" Y="-2.272560295885" />
                  <Point X="3.180237806413" Y="-1.934067108041" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.250073537099" Y="-0.935369630697" />
                  <Point X="3.382618399928" Y="0.960110216656" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.452415865077" Y="1.958260471314" />
                  <Point X="3.491834210882" Y="2.521969079062" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.065031613512" Y="-2.219711655934" />
                  <Point X="3.089855452236" Y="-1.864714223128" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.153217029607" Y="-0.958601451645" />
                  <Point X="3.288826203268" Y="0.980700082233" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.351784320178" Y="1.881043100306" />
                  <Point X="3.392595749211" Y="2.464673726372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.97349517234" Y="-2.166862983973" />
                  <Point X="3.000505257081" Y="-1.78060077652" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.053755503795" Y="-1.019086770179" />
                  <Point X="3.196912852173" Y="1.028158691166" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.251152774282" Y="1.803825715049" />
                  <Point X="3.293357287541" Y="2.407378373681" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.881958731282" Y="-2.11401431038" />
                  <Point X="2.914584862048" Y="-1.647438903038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.950022590347" Y="-1.140655777745" />
                  <Point X="3.107226644101" Y="1.107466929198" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.150241569442" Y="1.722609020559" />
                  <Point X="3.19411882587" Y="2.35008302099" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.658288487261" Y="-3.950767054188" />
                  <Point X="2.661872287534" Y="-3.89951632255" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.789704637831" Y="-2.071428544149" />
                  <Point X="3.022989481582" Y="1.264700149092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.044926085728" Y="1.57840820378" />
                  <Point X="3.094880364199" Y="2.2927876683" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.571010204707" Y="-3.837023876968" />
                  <Point X="2.57692843038" Y="-3.752389306788" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.695779950287" Y="-2.052733386493" />
                  <Point X="2.995641902529" Y="2.235492315609" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.483731922152" Y="-3.723280699748" />
                  <Point X="2.491984573225" Y="-3.605262291026" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.60173563056" Y="-2.03574904875" />
                  <Point X="2.896403440858" Y="2.178196962918" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.396453639598" Y="-3.609537522528" />
                  <Point X="2.40704071607" Y="-3.458135275264" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.507223260774" Y="-2.02545813871" />
                  <Point X="2.797164979188" Y="2.120901610228" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.309175357043" Y="-3.495794345308" />
                  <Point X="2.322096858915" Y="-3.311008259502" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.410491658139" Y="-2.046903736973" />
                  <Point X="2.697926517517" Y="2.063606257537" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.221897074489" Y="-3.382051168088" />
                  <Point X="2.237153001761" Y="-3.163881243741" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.311694126516" Y="-2.097893496218" />
                  <Point X="2.599024162416" Y="2.011117452727" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.719316121972" Y="3.731372619692" />
                  <Point X="2.735341243964" Y="3.960542541025" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.134618791935" Y="-3.268307990868" />
                  <Point X="2.152209144606" Y="-3.016754227979" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.212763056343" Y="-2.150792945681" />
                  <Point X="2.501896882613" Y="1.98401340733" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.610960445075" Y="3.543695014869" />
                  <Point X="2.644620634009" Y="4.025058142941" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.04734050938" Y="-3.154564813648" />
                  <Point X="2.067306420827" Y="-2.869038977537" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.109388350582" Y="-2.267239344667" />
                  <Point X="2.406110437198" Y="1.976084187019" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.502604768179" Y="3.356017410046" />
                  <Point X="2.553247642151" Y="4.080244248993" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.960062227819" Y="-3.040821622231" />
                  <Point X="2.311846550314" Y="1.989928568121" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.394249091283" Y="3.168339805223" />
                  <Point X="2.461743489127" Y="4.133554662984" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.872783947689" Y="-2.927078410336" />
                  <Point X="2.219568030756" Y="2.03216502474" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.285893414387" Y="2.9806622004" />
                  <Point X="2.370239336103" Y="4.186865076974" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.784411325716" Y="-2.82898501591" />
                  <Point X="2.129207779038" Y="2.101833989541" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.17753773684" Y="2.792984586273" />
                  <Point X="2.278735196123" Y="4.240175677498" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.693356307424" Y="-2.769251676017" />
                  <Point X="2.042892953024" Y="2.229355237204" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.069182056993" Y="2.605306939256" />
                  <Point X="2.186739068617" Y="4.286450528626" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.602221438144" Y="-2.710660258453" />
                  <Point X="2.09452278527" Y="4.329577004524" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.510425804986" Y="-2.661518204578" />
                  <Point X="2.002306501922" Y="4.372703480421" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.416247722561" Y="-2.646446762538" />
                  <Point X="1.91009021871" Y="4.415829958267" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.320444945428" Y="-2.654609537297" />
                  <Point X="1.817584918471" Y="4.454823300053" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.224596198085" Y="-2.66342971669" />
                  <Point X="1.724708558767" Y="4.488510244289" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.128281714257" Y="-2.678910238106" />
                  <Point X="1.631832199064" Y="4.522197188525" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.029152645502" Y="-2.734641199222" />
                  <Point X="1.538955839361" Y="4.555884132762" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.928041916395" Y="-2.818711223664" />
                  <Point X="1.44601668623" Y="4.588673089141" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.758912135386" Y="-3.875499008463" />
                  <Point X="0.772001314512" Y="-3.688315026203" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.826058296162" Y="-2.91526417278" />
                  <Point X="1.352365775251" Y="4.611283434087" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.683389349927" Y="-3.593644390594" />
                  <Point X="1.258714864273" Y="4.633893779034" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.60257156536" Y="-3.387511787801" />
                  <Point X="1.165063953294" Y="4.656504123981" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.516056122688" Y="-3.262859492015" />
                  <Point X="1.071413042315" Y="4.679114468927" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.428928137358" Y="-3.146966964348" />
                  <Point X="0.977762131337" Y="4.701724813874" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.337830928709" Y="-3.087836974674" />
                  <Point X="0.884111223918" Y="4.724335209725" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.244667757906" Y="-3.05825162026" />
                  <Point X="0.790105241815" Y="4.741867801031" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.151458663818" Y="-3.029322999409" />
                  <Point X="0.695565595366" Y="4.75176863662" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.058115489858" Y="-3.002311810059" />
                  <Point X="0.601025948916" Y="4.761669472208" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.037023402839" Y="-3.000980595069" />
                  <Point X="0.506486302467" Y="4.771570307796" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.134128345056" Y="-3.027765198097" />
                  <Point X="0.411946656017" Y="4.781471143384" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.231472970021" Y="-3.057977424116" />
                  <Point X="0.295340536865" Y="4.475806717383" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.328888062443" Y="-3.089197381718" />
                  <Point X="0.177956731747" Y="4.159020863944" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.429051994263" Y="-3.159727574147" />
                  <Point X="0.078439083803" Y="4.097732961533" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.534604930864" Y="-3.307324125411" />
                  <Point X="-0.017579984008" Y="4.086477085967" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.64239216619" Y="-3.486872637048" />
                  <Point X="-0.110720990427" Y="4.116379405846" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.771226293786" Y="-3.967405730784" />
                  <Point X="-0.199768604716" Y="4.204819960435" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.900087173235" Y="-4.448321393836" />
                  <Point X="-0.277495335959" Y="4.455156685192" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.017159091949" Y="-4.760647063921" />
                  <Point X="-0.353018100958" Y="4.737011595645" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.110887087337" Y="-4.73913907738" />
                  <Point X="-0.445724051146" Y="4.773135509484" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.172093995452" Y="-4.252557875448" />
                  <Point X="-0.541741834922" Y="4.761897996483" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.254328161485" Y="-4.066680471294" />
                  <Point X="-0.637759618698" Y="4.750660483482" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.343988757211" Y="-3.987005959662" />
                  <Point X="-0.733777402474" Y="4.739422970481" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.43371814568" Y="-3.908315230083" />
                  <Point X="-0.829795184689" Y="4.728185479815" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.523846307499" Y="-3.835327225102" />
                  <Point X="-0.92581293724" Y="4.716948413354" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.61657893792" Y="-3.799584856372" />
                  <Point X="-1.022886805053" Y="4.69060819499" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.711231022583" Y="-3.791291962144" />
                  <Point X="-1.120023148394" Y="4.663374534972" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.806028527252" Y="-3.785078670897" />
                  <Point X="-1.217159491734" Y="4.636140874955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.900826031921" Y="-3.77886537965" />
                  <Point X="-1.314295835074" Y="4.608907214937" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.996122200204" Y="-3.779783310317" />
                  <Point X="-1.411432178415" Y="4.58167355492" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.093810313986" Y="-3.814907655281" />
                  <Point X="-1.534980666747" Y="4.176728624251" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.193696560555" Y="-3.881466763611" />
                  <Point X="-1.636240994595" Y="4.090519238132" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.293596232716" Y="-3.94821786685" />
                  <Point X="-1.73432503886" Y="4.049732823482" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.394128514808" Y="-4.024015713585" />
                  <Point X="-1.831012417433" Y="4.02891965876" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.498625730907" Y="-4.156514758271" />
                  <Point X="-2.406314366283" Y="-2.836400741082" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.378335801669" Y="-2.436288626206" />
                  <Point X="-1.924935718151" Y="4.047634648955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.590495297326" Y="-4.108429999301" />
                  <Point X="-2.514670027414" Y="-3.024078120453" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.46895531554" Y="-2.370327282921" />
                  <Point X="-2.016949228954" Y="4.093660907344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.681775136138" Y="-4.051911742621" />
                  <Point X="-2.623025693175" Y="-3.211755566035" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.562657368147" Y="-2.348448297332" />
                  <Point X="-2.107569761244" Y="4.159607686543" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.772800097173" Y="-3.991748563917" />
                  <Point X="-2.731381358936" Y="-3.399433011617" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.659202405369" Y="-2.367225885907" />
                  <Point X="-2.195309675085" Y="4.2667492288" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.863166627731" Y="-3.92216939072" />
                  <Point X="-2.839737024697" Y="-3.587110457199" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.758137197914" Y="-2.420178567778" />
                  <Point X="-2.294390941239" Y="4.211701876728" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.953533160349" Y="-3.852590246982" />
                  <Point X="-2.948092690458" Y="-3.774787902781" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.857375660131" Y="-2.477473928281" />
                  <Point X="-2.393472207393" Y="4.156654524656" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.956614122348" Y="-2.534769288783" />
                  <Point X="-2.492553473547" Y="4.101607172584" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.055852584565" Y="-2.592064649286" />
                  <Point X="-2.978536162488" Y="-1.486388301004" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.963513264844" Y="-1.271550855597" />
                  <Point X="-2.591634748852" Y="4.04655968966" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.155091046781" Y="-2.649360009788" />
                  <Point X="-3.081687289391" Y="-1.599637373364" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.05071976979" Y="-1.156781210748" />
                  <Point X="-2.764429718081" Y="2.937357271357" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.753325403394" Y="3.096156369708" />
                  <Point X="-2.691376512728" Y="3.982066780096" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.254329508998" Y="-2.706655370291" />
                  <Point X="-3.18231882511" Y="-1.676854613091" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.142744371067" Y="-1.110913553526" />
                  <Point X="-2.869064435714" Y="2.802891863114" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.833675802865" Y="3.308972890767" />
                  <Point X="-2.792003319261" Y="3.904917170873" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.353567971215" Y="-2.763950730793" />
                  <Point X="-3.282950363117" Y="-1.754071885542" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.237202987877" Y="-1.099853940104" />
                  <Point X="-2.968906315735" Y="2.736967225987" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.918619656397" Y="3.456099958344" />
                  <Point X="-2.892630125795" Y="3.82776756165" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.452806433432" Y="-2.821246091296" />
                  <Point X="-3.383581906762" Y="-1.831289238608" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.333277791139" Y="-1.111906869744" />
                  <Point X="-3.065078073865" Y="2.72352777713" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.003563509928" Y="3.60322702592" />
                  <Point X="-2.993256932328" Y="3.750617952426" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.552044895649" Y="-2.878541451798" />
                  <Point X="-3.484213450406" Y="-1.908506591674" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.429394628316" Y="-1.124560912383" />
                  <Point X="-3.36630517654" Y="-0.222339718214" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.34179546014" Y="0.128165556077" />
                  <Point X="-3.15971782105" Y="2.73199710551" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.651283357865" Y="-2.935836812301" />
                  <Point X="-3.58484499405" Y="-1.985723944739" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.525511465494" Y="-1.137214955022" />
                  <Point X="-3.466660014241" Y="-0.295599991926" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.431079114562" Y="0.213230579494" />
                  <Point X="-3.252869556175" Y="2.761745997736" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.750521820082" Y="-2.993132172803" />
                  <Point X="-3.685476537695" Y="-2.062941297805" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.621628302672" Y="-1.149868997661" />
                  <Point X="-3.564096448638" Y="-0.327125153992" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.523482410709" Y="0.253682647761" />
                  <Point X="-3.441642748981" Y="1.424044336693" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.43034766072" Y="1.58557162426" />
                  <Point X="-3.344412100786" Y="2.81450738646" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.842896571477" Y="-2.952271895557" />
                  <Point X="-3.786108081339" Y="-2.14015865087" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.717745139849" Y="-1.1625230403" />
                  <Point X="-3.661146848823" Y="-0.353129769633" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.616962864545" Y="0.278730643418" />
                  <Point X="-3.547843520598" Y="1.267183313091" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.51466792714" Y="1.741616402992" />
                  <Point X="-3.435948544065" Y="2.867356028288" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.930115753675" Y="-2.837683543867" />
                  <Point X="-3.886739624983" Y="-2.217376003936" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.813861977027" Y="-1.175177082939" />
                  <Point X="-3.758197248326" Y="-0.379134375521" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.710443318381" Y="0.303778639075" />
                  <Point X="-3.64612100506" Y="1.223630574732" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.604678496617" Y="1.816286056818" />
                  <Point X="-3.527484988255" Y="2.92020465709" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.017032293652" Y="-2.718767206768" />
                  <Point X="-3.987371168628" Y="-2.294593357001" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.909978814205" Y="-1.187831125578" />
                  <Point X="-3.855247644242" Y="-0.405138930105" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.803923772217" Y="0.328826634732" />
                  <Point X="-3.742690216806" Y="1.204507274377" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.695060850273" Y="1.885638949179" />
                  <Point X="-3.619021432445" Y="2.973053285892" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.10226962809" Y="-2.575837111694" />
                  <Point X="-4.088002712272" Y="-2.371810710067" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.006095651382" Y="-1.200485168217" />
                  <Point X="-3.952298040158" Y="-0.431143484689" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.897404226053" Y="0.35387463039" />
                  <Point X="-3.837300081217" Y="1.213403946331" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.785443203929" Y="1.95499184154" />
                  <Point X="-3.710557876635" Y="3.025901914694" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.10221248856" Y="-1.213139210855" />
                  <Point X="-4.049348436074" Y="-0.457148039273" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.990884679888" Y="0.378922626047" />
                  <Point X="-3.931663342708" Y="1.225827204345" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.875825557585" Y="2.024344733901" />
                  <Point X="-3.812936236574" Y="2.92370392479" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.198329325738" Y="-1.225793253494" />
                  <Point X="-4.146398831989" Y="-0.483152593857" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.084365133724" Y="0.403970621704" />
                  <Point X="-4.026026610181" Y="1.238250376805" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.966207911241" Y="2.093697626262" />
                  <Point X="-3.917573087003" Y="2.789208016133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.294446162915" Y="-1.238447296133" />
                  <Point X="-4.243449227905" Y="-0.50915714844" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.17784558756" Y="0.429018617361" />
                  <Point X="-4.120389878734" Y="1.250673533819" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.056590264896" Y="2.163050518623" />
                  <Point X="-4.022887017462" Y="2.645029411943" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.390563000093" Y="-1.251101338772" />
                  <Point X="-4.340499623821" Y="-0.535161703024" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.271326041396" Y="0.454066613018" />
                  <Point X="-4.214753147288" Y="1.263096690833" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.146972618552" Y="2.232403410984" />
                  <Point X="-4.131080783289" Y="2.459667243289" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.48667983727" Y="-1.263755381411" />
                  <Point X="-4.437550019737" Y="-0.561166257608" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.364806495232" Y="0.479114608675" />
                  <Point X="-4.309116415841" Y="1.275519847847" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.582796674448" Y="-1.27640942405" />
                  <Point X="-4.534600415652" Y="-0.587170812192" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.458286949068" Y="0.504162604332" />
                  <Point X="-4.403479684395" Y="1.287943004861" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.674916332984" Y="-1.231901148966" />
                  <Point X="-4.631650811568" Y="-0.613175366776" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.551767402903" Y="0.529210599989" />
                  <Point X="-4.497842952948" Y="1.300366161875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.748391967497" Y="-0.920770908643" />
                  <Point X="-4.728701207484" Y="-0.639179921359" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.645247856739" Y="0.554258595646" />
                  <Point X="-4.592206221502" Y="1.312789318889" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.738728310575" Y="0.579306591304" />
                  <Point X="-4.705654678415" Y="1.052281566725" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="0.001626220703" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.534823059082" Y="-3.773291259766" />
                  <Point X="0.471539031982" Y="-3.537112304688" />
                  <Point X="0.464318664551" Y="-3.521544189453" />
                  <Point X="0.438452972412" Y="-3.484276367188" />
                  <Point X="0.300591033936" Y="-3.285643798828" />
                  <Point X="0.274335754395" Y="-3.266399902344" />
                  <Point X="0.234309997559" Y="-3.253977294922" />
                  <Point X="0.02097677803" Y="-3.187766601562" />
                  <Point X="-0.008664756775" Y="-3.187766601562" />
                  <Point X="-0.048690509796" Y="-3.200188964844" />
                  <Point X="-0.262023895264" Y="-3.266399902344" />
                  <Point X="-0.288279144287" Y="-3.285643798828" />
                  <Point X="-0.314144866943" Y="-3.322911376953" />
                  <Point X="-0.45200680542" Y="-3.521544189453" />
                  <Point X="-0.459227142334" Y="-3.537112304688" />
                  <Point X="-0.796524353027" Y="-4.795923339844" />
                  <Point X="-0.847743896484" Y="-4.987076660156" />
                  <Point X="-0.849336853027" Y="-4.986767578125" />
                  <Point X="-1.100246826172" Y="-4.938065429688" />
                  <Point X="-1.145501464844" Y="-4.926421386719" />
                  <Point X="-1.351589599609" Y="-4.873396972656" />
                  <Point X="-1.316768188477" Y="-4.60890234375" />
                  <Point X="-1.309150146484" Y="-4.551038574219" />
                  <Point X="-1.309683227539" Y="-4.5347578125" />
                  <Point X="-1.319245361328" Y="-4.486685546875" />
                  <Point X="-1.370210693359" Y="-4.230466308594" />
                  <Point X="-1.386282714844" Y="-4.20262890625" />
                  <Point X="-1.423133178711" Y="-4.170311523438" />
                  <Point X="-1.619543457031" Y="-3.998063964844" />
                  <Point X="-1.649240722656" Y="-3.985762939453" />
                  <Point X="-1.698149780273" Y="-3.982557128906" />
                  <Point X="-1.958829833984" Y="-3.965471435547" />
                  <Point X="-1.989878540039" Y="-3.973791015625" />
                  <Point X="-2.030632202148" Y="-4.001021728516" />
                  <Point X="-2.247844970703" Y="-4.146159179688" />
                  <Point X="-2.259734130859" Y="-4.157294433594" />
                  <Point X="-2.449100341797" Y="-4.404081054688" />
                  <Point X="-2.457095214844" Y="-4.414500488281" />
                  <Point X="-2.488050048828" Y="-4.395333984375" />
                  <Point X="-2.855830810547" Y="-4.16761328125" />
                  <Point X="-2.918500732422" Y="-4.119359375" />
                  <Point X="-3.228581054688" Y="-3.880608154297" />
                  <Point X="-2.623639892578" Y="-2.832819335938" />
                  <Point X="-2.506033691406" Y="-2.629119628906" />
                  <Point X="-2.500251464844" Y="-2.613671386719" />
                  <Point X="-2.504395996094" Y="-2.582189697266" />
                  <Point X="-2.516720458984" Y="-2.566023681641" />
                  <Point X="-2.531328369141" Y="-2.551415771484" />
                  <Point X="-2.560157226562" Y="-2.537198730469" />
                  <Point X="-2.59168359375" Y="-2.543469726562" />
                  <Point X="-3.677383300781" Y="-3.170298828125" />
                  <Point X="-3.842959472656" Y="-3.265894042969" />
                  <Point X="-3.871134521484" Y="-3.228877685547" />
                  <Point X="-4.161703613281" Y="-2.847129394531" />
                  <Point X="-4.206629882812" Y="-2.771794677734" />
                  <Point X="-4.431020019531" Y="-2.395527099609" />
                  <Point X="-3.369559326172" Y="-1.581039672852" />
                  <Point X="-3.163786865234" Y="-1.423144897461" />
                  <Point X="-3.152535400391" Y="-1.411081420898" />
                  <Point X="-3.144604736328" Y="-1.391313842773" />
                  <Point X="-3.138117431641" Y="-1.366265869141" />
                  <Point X="-3.136651855469" Y="-1.350051025391" />
                  <Point X="-3.148656738281" Y="-1.321068481445" />
                  <Point X="-3.165342773438" Y="-1.308176757812" />
                  <Point X="-3.187641601562" Y="-1.295052734375" />
                  <Point X="-3.219529296875" Y="-1.288571044922" />
                  <Point X="-4.590122070312" Y="-1.469013061523" />
                  <Point X="-4.803283203125" Y="-1.497076293945" />
                  <Point X="-4.813749511719" Y="-1.456100830078" />
                  <Point X="-4.927393066406" Y="-1.011191650391" />
                  <Point X="-4.939279296875" Y="-0.928083984375" />
                  <Point X="-4.998396484375" Y="-0.514742126465" />
                  <Point X="-3.791869628906" Y="-0.19145425415" />
                  <Point X="-3.557463867188" Y="-0.128645431519" />
                  <Point X="-3.537511230469" Y="-0.118381988525" />
                  <Point X="-3.514142822266" Y="-0.102162895203" />
                  <Point X="-3.50232421875" Y="-0.090645278931" />
                  <Point X="-3.4934375" Y="-0.071198860168" />
                  <Point X="-3.485647949219" Y="-0.046100891113" />
                  <Point X="-3.487109375" Y="-0.011750290871" />
                  <Point X="-3.494898925781" Y="0.013347681046" />
                  <Point X="-3.50232421875" Y="0.028085197449" />
                  <Point X="-3.51852734375" Y="0.042645889282" />
                  <Point X="-3.541895751953" Y="0.058864833832" />
                  <Point X="-3.557463867188" Y="0.066085334778" />
                  <Point X="-4.80682421875" Y="0.400850402832" />
                  <Point X="-4.998186523438" Y="0.452125823975" />
                  <Point X="-4.990887207031" Y="0.501456329346" />
                  <Point X="-4.917645019531" Y="0.996418212891" />
                  <Point X="-4.893717285156" Y="1.084719604492" />
                  <Point X="-4.773516113281" Y="1.528298583984" />
                  <Point X="-3.921294189453" Y="1.4161015625" />
                  <Point X="-3.753266357422" Y="1.393980102539" />
                  <Point X="-3.731704589844" Y="1.395866699219" />
                  <Point X="-3.722000488281" Y="1.398926391602" />
                  <Point X="-3.670278564453" Y="1.41523425293" />
                  <Point X="-3.651534423828" Y="1.426056152344" />
                  <Point X="-3.639119873047" Y="1.443785888672" />
                  <Point X="-3.635226074219" Y="1.453186401367" />
                  <Point X="-3.614472412109" Y="1.503290161133" />
                  <Point X="-3.610714111328" Y="1.524605224609" />
                  <Point X="-3.616315917969" Y="1.54551171875" />
                  <Point X="-3.621014160156" Y="1.554537109375" />
                  <Point X="-3.646055664062" Y="1.602641479492" />
                  <Point X="-3.659968017578" Y="1.619221679688" />
                  <Point X="-4.376604492188" Y="2.169115966797" />
                  <Point X="-4.47610546875" Y="2.245466064453" />
                  <Point X="-4.444605957031" Y="2.299432617188" />
                  <Point X="-4.160014648438" Y="2.787006591797" />
                  <Point X="-4.096631835938" Y="2.868475830078" />
                  <Point X="-3.774671142578" Y="3.282310791016" />
                  <Point X="-3.262354980469" Y="2.986524902344" />
                  <Point X="-3.159156982422" Y="2.926943359375" />
                  <Point X="-3.138514892578" Y="2.920434814453" />
                  <Point X="-3.124999755859" Y="2.919252441406" />
                  <Point X="-3.052965576172" Y="2.912950195312" />
                  <Point X="-3.031506835938" Y="2.915775390625" />
                  <Point X="-3.013252441406" Y="2.927404785156" />
                  <Point X="-3.003659179688" Y="2.936997802734" />
                  <Point X="-2.952528808594" Y="2.988128173828" />
                  <Point X="-2.940899414062" Y="3.006382568359" />
                  <Point X="-2.93807421875" Y="3.027841308594" />
                  <Point X="-2.939256591797" Y="3.041356445312" />
                  <Point X="-2.945558837891" Y="3.113390869141" />
                  <Point X="-2.952067382812" Y="3.134032958984" />
                  <Point X="-3.269630615234" Y="3.684068359375" />
                  <Point X="-3.307278564453" Y="3.749276855469" />
                  <Point X="-3.248544921875" Y="3.794307373047" />
                  <Point X="-2.752876220703" Y="4.174331542969" />
                  <Point X="-2.65305078125" Y="4.229792480469" />
                  <Point X="-2.141548583984" Y="4.513972167969" />
                  <Point X="-1.999418823242" Y="4.328744628906" />
                  <Point X="-1.967826660156" Y="4.287573242188" />
                  <Point X="-1.951246826172" Y="4.273660644531" />
                  <Point X="-1.936204589844" Y="4.265830078125" />
                  <Point X="-1.856030761719" Y="4.22409375" />
                  <Point X="-1.835124023438" Y="4.2184921875" />
                  <Point X="-1.81380859375" Y="4.222250488281" />
                  <Point X="-1.798141113281" Y="4.228740234375" />
                  <Point X="-1.714634765625" Y="4.263330078125" />
                  <Point X="-1.696905273438" Y="4.275744140625" />
                  <Point X="-1.686083496094" Y="4.29448828125" />
                  <Point X="-1.680983886719" Y="4.310662109375" />
                  <Point X="-1.653804077148" Y="4.396864746094" />
                  <Point X="-1.651917602539" Y="4.418426757812" />
                  <Point X="-1.688020996094" Y="4.692659179688" />
                  <Point X="-1.689137573242" Y="4.701141113281" />
                  <Point X="-1.609767944336" Y="4.723393554688" />
                  <Point X="-0.968094238281" Y="4.903296386719" />
                  <Point X="-0.847077941895" Y="4.917459472656" />
                  <Point X="-0.224199615479" Y="4.990358398438" />
                  <Point X="-0.072669212341" Y="4.424838867188" />
                  <Point X="-0.042140380859" Y="4.310903320312" />
                  <Point X="-0.024282136917" Y="4.284176757813" />
                  <Point X="0.006155934334" Y="4.273844238281" />
                  <Point X="0.036594020844" Y="4.284176757813" />
                  <Point X="0.054452251434" Y="4.310903320312" />
                  <Point X="0.217168014526" Y="4.918166992188" />
                  <Point X="0.236648391724" Y="4.990868652344" />
                  <Point X="0.299931152344" Y="4.984241210938" />
                  <Point X="0.860205810547" Y="4.925565429688" />
                  <Point X="0.960327636719" Y="4.901393066406" />
                  <Point X="1.508456054688" Y="4.769057617188" />
                  <Point X="1.573348999023" Y="4.745520996094" />
                  <Point X="1.931033691406" Y="4.615786132813" />
                  <Point X="1.994051147461" Y="4.586314941406" />
                  <Point X="2.33869921875" Y="4.425134277344" />
                  <Point X="2.399583007812" Y="4.389663085938" />
                  <Point X="2.732523681641" Y="4.19569140625" />
                  <Point X="2.789939697266" Y="4.154860351562" />
                  <Point X="3.068740234375" Y="3.956592773438" />
                  <Point X="2.365973632812" Y="2.739364990234" />
                  <Point X="2.229853515625" Y="2.503597900391" />
                  <Point X="2.221460205078" Y="2.478831054688" />
                  <Point X="2.203382324219" Y="2.411228271484" />
                  <Point X="2.2033671875" Y="2.381358154297" />
                  <Point X="2.210416015625" Y="2.322901123047" />
                  <Point X="2.218682128906" Y="2.3008125" />
                  <Point X="2.22546875" Y="2.290811035156" />
                  <Point X="2.261639892578" Y="2.23750390625" />
                  <Point X="2.28494140625" Y="2.217416992188" />
                  <Point X="2.338248535156" Y="2.18124609375" />
                  <Point X="2.360336914062" Y="2.172980224609" />
                  <Point X="2.3713046875" Y="2.171657714844" />
                  <Point X="2.42976171875" Y="2.164608642578" />
                  <Point X="2.461347900391" Y="2.169338134766" />
                  <Point X="2.528950683594" Y="2.187416015625" />
                  <Point X="2.541033935547" Y="2.192417480469" />
                  <Point X="3.797649902344" Y="2.917925048828" />
                  <Point X="3.994248046875" Y="3.031430908203" />
                  <Point X="4.005102783203" Y="3.016345214844" />
                  <Point X="4.202591796875" Y="2.741880126953" />
                  <Point X="4.234600585938" Y="2.688985351562" />
                  <Point X="4.387512695312" Y="2.436296142578" />
                  <Point X="3.467831298828" Y="1.730599365234" />
                  <Point X="3.288616210938" Y="1.593082763672" />
                  <Point X="3.270242919922" Y="1.571924438477" />
                  <Point X="3.221589111328" Y="1.508451904297" />
                  <Point X="3.209719238281" Y="1.479341186523" />
                  <Point X="3.191595703125" Y="1.414535522461" />
                  <Point X="3.190779541016" Y="1.390965332031" />
                  <Point X="3.193570800781" Y="1.377437011719" />
                  <Point X="3.208448486328" Y="1.305332519531" />
                  <Point X="3.223238525391" Y="1.276415161133" />
                  <Point X="3.263704101562" Y="1.214909667969" />
                  <Point X="3.280947998047" Y="1.198819824219" />
                  <Point X="3.291949951172" Y="1.192626586914" />
                  <Point X="3.350590087891" Y="1.159617431641" />
                  <Point X="3.383440917969" Y="1.151653564453" />
                  <Point X="3.462726074219" Y="1.141175048828" />
                  <Point X="3.475803710938" Y="1.14117175293" />
                  <Point X="4.669504882812" Y="1.298325683594" />
                  <Point X="4.848975097656" Y="1.32195324707" />
                  <Point X="4.854368164063" Y="1.299801269531" />
                  <Point X="4.939188476562" Y="0.951386047363" />
                  <Point X="4.949275390625" Y="0.886598693848" />
                  <Point X="4.997858398438" Y="0.574556152344" />
                  <Point X="3.946552001953" Y="0.292859405518" />
                  <Point X="3.741167724609" Y="0.237826919556" />
                  <Point X="3.714472167969" Y="0.224371780396" />
                  <Point X="3.636576904297" Y="0.179346878052" />
                  <Point X="3.61349609375" Y="0.15575302124" />
                  <Point X="3.566759033203" Y="0.096198890686" />
                  <Point X="3.556985107422" Y="0.07473513031" />
                  <Point X="3.554062011719" Y="0.059472537994" />
                  <Point X="3.538482910156" Y="-0.021875303268" />
                  <Point X="3.541406005859" Y="-0.055947227478" />
                  <Point X="3.556985107422" Y="-0.137295227051" />
                  <Point X="3.566759033203" Y="-0.158758987427" />
                  <Point X="3.575527832031" Y="-0.169932525635" />
                  <Point X="3.622264892578" Y="-0.229486679077" />
                  <Point X="3.651191650391" Y="-0.250354598999" />
                  <Point X="3.729086914062" Y="-0.295379486084" />
                  <Point X="3.741167724609" Y="-0.300387023926" />
                  <Point X="4.835845703125" Y="-0.59370513916" />
                  <Point X="4.998067871094" Y="-0.637172363281" />
                  <Point X="4.995791503906" Y="-0.652271240234" />
                  <Point X="4.948431640625" Y="-0.966399230957" />
                  <Point X="4.935508789062" Y="-1.023030090332" />
                  <Point X="4.874545898438" Y="-1.290178344727" />
                  <Point X="3.650170166016" Y="-1.128986206055" />
                  <Point X="3.411982177734" Y="-1.097628295898" />
                  <Point X="3.3948359375" Y="-1.098341186523" />
                  <Point X="3.36615234375" Y="-1.104575683594" />
                  <Point X="3.213271728516" Y="-1.137804931641" />
                  <Point X="3.1854453125" Y="-1.154697265625" />
                  <Point X="3.168107910156" Y="-1.175548950195" />
                  <Point X="3.075701171875" Y="-1.286685424805" />
                  <Point X="3.064357910156" Y="-1.314070556641" />
                  <Point X="3.061873046875" Y="-1.34107421875" />
                  <Point X="3.04862890625" Y="-1.485000854492" />
                  <Point X="3.056360351562" Y="-1.516621826172" />
                  <Point X="3.072234375" Y="-1.541312744141" />
                  <Point X="3.156840576172" Y="-1.672912353516" />
                  <Point X="3.168460693359" Y="-1.685540649414" />
                  <Point X="4.184328613281" Y="-2.465043701172" />
                  <Point X="4.33907421875" Y="-2.583784179688" />
                  <Point X="4.337635742188" Y="-2.586112060547" />
                  <Point X="4.204133300781" Y="-2.802139404297" />
                  <Point X="4.177405273438" Y="-2.840116210938" />
                  <Point X="4.056688476562" Y="-3.011638427734" />
                  <Point X="2.965593261719" Y="-2.381694091797" />
                  <Point X="2.753454589844" Y="-2.259215576172" />
                  <Point X="2.737340820313" Y="-2.253312744141" />
                  <Point X="2.703202880859" Y="-2.247147460938" />
                  <Point X="2.521250488281" Y="-2.214287109375" />
                  <Point X="2.489077636719" Y="-2.219244873047" />
                  <Point X="2.460717285156" Y="-2.234170654297" />
                  <Point X="2.309559570312" Y="-2.313723876953" />
                  <Point X="2.288599853516" Y="-2.33468359375" />
                  <Point X="2.273674072266" Y="-2.363043945312" />
                  <Point X="2.194120849609" Y="-2.514201660156" />
                  <Point X="2.189163085938" Y="-2.546374511719" />
                  <Point X="2.195328369141" Y="-2.580512451172" />
                  <Point X="2.228188720703" Y="-2.76246484375" />
                  <Point X="2.234091552734" Y="-2.778578613281" />
                  <Point X="2.886888916016" Y="-3.909256347656" />
                  <Point X="2.986673583984" Y="-4.082088623047" />
                  <Point X="2.835296630859" Y="-4.190213378906" />
                  <Point X="2.805418457031" Y="-4.209553222656" />
                  <Point X="2.679775878906" Y="-4.290879394531" />
                  <Point X="1.845058837891" Y="-3.203055175781" />
                  <Point X="1.683177612305" Y="-2.992087402344" />
                  <Point X="1.670549316406" Y="-2.980467529297" />
                  <Point X="1.636879882812" Y="-2.958821289062" />
                  <Point X="1.45742590332" Y="-2.843448730469" />
                  <Point X="1.4258046875" Y="-2.835717041016" />
                  <Point X="1.388981567383" Y="-2.83910546875" />
                  <Point X="1.192717773438" Y="-2.857166015625" />
                  <Point X="1.165332885742" Y="-2.868509277344" />
                  <Point X="1.136899047852" Y="-2.892151123047" />
                  <Point X="0.985349060059" Y="-3.018159667969" />
                  <Point X="0.968456726074" Y="-3.045986328125" />
                  <Point X="0.95995526123" Y="-3.085100341797" />
                  <Point X="0.91464251709" Y="-3.29357421875" />
                  <Point X="0.91392956543" Y="-3.310720214844" />
                  <Point X="1.098927124023" Y="-4.715917480469" />
                  <Point X="1.127642211914" Y="-4.934028320312" />
                  <Point X="0.994346313477" Y="-4.963246582031" />
                  <Point X="0.966738586426" Y="-4.968262207031" />
                  <Point X="0.860200378418" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#132" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.030528837156" Y="4.469059650747" Z="0.4" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.4" />
                  <Point X="-0.859045964258" Y="4.998400702517" Z="0.4" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.4" />
                  <Point X="-1.629421755161" Y="4.802817165748" Z="0.4" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.4" />
                  <Point X="-1.743749236756" Y="4.717412958834" Z="0.4" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.4" />
                  <Point X="-1.73482565947" Y="4.356977565837" Z="0.4" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.4" />
                  <Point X="-1.823431524828" Y="4.306214334649" Z="0.4" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.4" />
                  <Point X="-1.919272926231" Y="4.341460754392" Z="0.4" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.4" />
                  <Point X="-1.965907245416" Y="4.390462892805" Z="0.4" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.4" />
                  <Point X="-2.683489695037" Y="4.304779848199" Z="0.4" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.4" />
                  <Point X="-3.285123066913" Y="3.865267279419" Z="0.4" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.4" />
                  <Point X="-3.319087842277" Y="3.690348398154" Z="0.4" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.4" />
                  <Point X="-2.995222443605" Y="3.068278669706" Z="0.4" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.4" />
                  <Point X="-3.045170430908" Y="3.003633136978" Z="0.4" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.4" />
                  <Point X="-3.126797795901" Y="3.000342255568" Z="0.4" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.4" />
                  <Point X="-3.243510950143" Y="3.061106106468" Z="0.4" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.4" />
                  <Point X="-4.1422512232" Y="2.93045837439" Z="0.4" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.4" />
                  <Point X="-4.49394420981" Y="2.355917798171" Z="0.4" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.4" />
                  <Point X="-4.413198389528" Y="2.16072827957" Z="0.4" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.4" />
                  <Point X="-3.671520745474" Y="1.562729755556" Z="0.4" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.4" />
                  <Point X="-3.687576091951" Y="1.50360061398" Z="0.4" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.4" />
                  <Point X="-3.743191945802" Y="1.477892230536" Z="0.4" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.4" />
                  <Point X="-3.920923908327" Y="1.496953825991" Z="0.4" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.4" />
                  <Point X="-4.94813293892" Y="1.12907711649" Z="0.4" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.4" />
                  <Point X="-5.046504362885" Y="0.540055446174" Z="0.4" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.4" />
                  <Point X="-4.825921179415" Y="0.38383415917" Z="0.4" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.4" />
                  <Point X="-3.553192267622" Y="0.032849980732" Z="0.4" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.4" />
                  <Point X="-3.541018236225" Y="0.004708911587" Z="0.4" />
                  <Point X="-3.539556741714" Y="0" Z="0.4" />
                  <Point X="-3.54734636491" Y="-0.025098039474" Z="0.4" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.4" />
                  <Point X="-3.572176327501" Y="-0.046026002402" Z="0.4" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.4" />
                  <Point X="-3.810966609357" Y="-0.111877897586" Z="0.4" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.4" />
                  <Point X="-4.994931317408" Y="-0.903883110755" Z="0.4" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.4" />
                  <Point X="-4.868350479233" Y="-1.437195074712" Z="0.4" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.4" />
                  <Point X="-4.589751741561" Y="-1.487305245705" Z="0.4" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.4" />
                  <Point X="-3.196860032493" Y="-1.319987443863" Z="0.4" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.4" />
                  <Point X="-3.199163643371" Y="-1.347497671794" Z="0.4" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.4" />
                  <Point X="-3.40615329321" Y="-1.510091949363" Z="0.4" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.4" />
                  <Point X="-4.255729323935" Y="-2.766124222808" Z="0.4" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.4" />
                  <Point X="-3.917076378774" Y="-3.227880864227" Z="0.4" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.4" />
                  <Point X="-3.658539159899" Y="-3.182319953662" Z="0.4" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.4" />
                  <Point X="-2.558231696556" Y="-2.570098772576" Z="0.4" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.4" />
                  <Point X="-2.673097059506" Y="-2.776539222593" Z="0.4" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.4" />
                  <Point X="-2.955160636368" Y="-4.127696763658" Z="0.4" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.4" />
                  <Point X="-2.520527631943" Y="-4.406564695894" Z="0.4" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.4" />
                  <Point X="-2.415588695114" Y="-4.403239213951" Z="0.4" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.4" />
                  <Point X="-2.009009591475" Y="-4.011315089263" Z="0.4" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.4" />
                  <Point X="-1.70757584091" Y="-4.001170266431" Z="0.4" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.4" />
                  <Point X="-1.462256605416" Y="-4.176623330081" Z="0.4" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.4" />
                  <Point X="-1.374440680063" Y="-4.46516012784" Z="0.4" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.4" />
                  <Point X="-1.372496427572" Y="-4.571095842946" Z="0.4" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.4" />
                  <Point X="-1.164116177721" Y="-4.943564378934" Z="0.4" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.4" />
                  <Point X="-0.865020114528" Y="-5.004571754218" Z="0.4" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.4" />
                  <Point X="-0.7543841194" Y="-4.777583976523" Z="0.4" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.4" />
                  <Point X="-0.279224833474" Y="-3.320140297226" Z="0.4" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.4" />
                  <Point X="-0.040025747755" Y="-3.216661883454" Z="0.4" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.4" />
                  <Point X="0.213333331606" Y="-3.270450161834" Z="0.4" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.4" />
                  <Point X="0.391221025958" Y="-3.481505458542" Z="0.4" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.4" />
                  <Point X="0.480370741001" Y="-3.754952053292" Z="0.4" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.4" />
                  <Point X="0.969519659796" Y="-4.986177060929" Z="0.4" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.4" />
                  <Point X="1.148767733789" Y="-4.947955403336" Z="0.4" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.4" />
                  <Point X="1.14234356295" Y="-4.678111169199" Z="0.4" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.4" />
                  <Point X="1.002658426557" Y="-3.064439993946" Z="0.4" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.4" />
                  <Point X="1.162709863562" Y="-2.899317284961" Z="0.4" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.4" />
                  <Point X="1.387407344699" Y="-2.857615085432" Z="0.4" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.4" />
                  <Point X="1.603684588297" Y="-2.969599015139" Z="0.4" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.4" />
                  <Point X="1.799235186664" Y="-3.202213030684" Z="0.4" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.4" />
                  <Point X="2.82643156981" Y="-4.220247442195" Z="0.4" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.4" />
                  <Point X="3.016616562522" Y="-4.086469021107" Z="0.4" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.4" />
                  <Point X="2.924034301514" Y="-3.852976421443" Z="0.4" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.4" />
                  <Point X="2.238376418643" Y="-2.540346217782" Z="0.4" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.4" />
                  <Point X="2.311764361318" Y="-2.355050505131" Z="0.4" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.4" />
                  <Point X="2.477847873242" Y="-2.247136948649" Z="0.4" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.4" />
                  <Point X="2.688160585731" Y="-2.265071590848" Z="0.4" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.4" />
                  <Point X="2.934437175664" Y="-2.39371519876" Z="0.4" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.4" />
                  <Point X="4.212137738877" Y="-2.837613507429" Z="0.4" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.4" />
                  <Point X="4.374012850382" Y="-2.58111729211" Z="0.4" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.4" />
                  <Point X="4.208610708218" Y="-2.394095976762" Z="0.4" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.4" />
                  <Point X="3.108136419019" Y="-1.482993325571" Z="0.4" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.4" />
                  <Point X="3.105506592394" Y="-1.314375849207" Z="0.4" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.4" />
                  <Point X="3.200398125633" Y="-1.176235696724" Z="0.4" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.4" />
                  <Point X="3.370616269366" Y="-1.122154921547" Z="0.4" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.4" />
                  <Point X="3.637487871427" Y="-1.147278468694" Z="0.4" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.4" />
                  <Point X="4.978099850241" Y="-1.002874077761" Z="0.4" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.4" />
                  <Point X="5.039077108073" Y="-0.628445224462" Z="0.4" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.4" />
                  <Point X="4.842630844309" Y="-0.514128748239" Z="0.4" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.4" />
                  <Point X="3.670057471473" Y="-0.175785798747" Z="0.4" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.4" />
                  <Point X="3.608705577778" Y="-0.107784111933" Z="0.4" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.4" />
                  <Point X="3.584357678071" Y="-0.015262562411" Z="0.4" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.4" />
                  <Point X="3.597013772353" Y="0.081347968827" Z="0.4" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.4" />
                  <Point X="3.646673860622" Y="0.156164626642" Z="0.4" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.4" />
                  <Point X="3.73333794288" Y="0.212363045495" Z="0.4" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.4" />
                  <Point X="3.953336972998" Y="0.275843184529" Z="0.4" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.4" />
                  <Point X="4.992524140223" Y="0.925570785492" Z="0.4" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.4" />
                  <Point X="4.896793203092" Y="1.342908179594" Z="0.4" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.4" />
                  <Point X="4.656822448593" Y="1.379177854064" Z="0.4" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.4" />
                  <Point X="3.38383617128" Y="1.232502593841" Z="0.4" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.4" />
                  <Point X="3.310320627854" Y="1.267477815238" Z="0.4" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.4" />
                  <Point X="3.258853143836" Y="1.335176672761" Z="0.4" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.4" />
                  <Point X="3.236383381685" Y="1.418820907933" Z="0.4" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.4" />
                  <Point X="3.251715625027" Y="1.497154826835" Z="0.4" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.4" />
                  <Point X="3.303769712976" Y="1.572786323576" Z="0.4" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.4" />
                  <Point X="3.492113198361" Y="1.722211632384" Z="0.4" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.4" />
                  <Point X="4.271222067695" Y="2.746151598906" Z="0.4" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.4" />
                  <Point X="4.039532322732" Y="3.076828126456" Z="0.4" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.4" />
                  <Point X="3.766493983716" Y="2.992506349036" Z="0.4" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.4" />
                  <Point X="2.442275305261" Y="2.248920947927" Z="0.4" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.4" />
                  <Point X="2.37113455072" Y="2.252578018927" Z="0.4" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.4" />
                  <Point X="2.306859603296" Y="2.290071625797" Z="0.4" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.4" />
                  <Point X="2.260686974438" Y="2.350165257086" Z="0.4" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.4" />
                  <Point X="2.246851683832" Y="2.418623893113" Z="0.4" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.4" />
                  <Point X="2.26360699473" Y="2.497194270621" Z="0.4" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.4" />
                  <Point X="2.403119003823" Y="2.745645098239" Z="0.4" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.4" />
                  <Point X="2.81276079731" Y="4.226887459588" Z="0.4" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.4" />
                  <Point X="2.418597040398" Y="4.464145982215" Z="0.4" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.4" />
                  <Point X="2.009076708665" Y="4.662886855183" Z="0.4" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.4" />
                  <Point X="1.584241568022" Y="4.823804817821" Z="0.4" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.4" />
                  <Point X="0.965906055851" Y="4.981276723544" Z="0.4" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.4" />
                  <Point X="0.298983015673" Y="5.065249392201" Z="0.4" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.4" />
                  <Point X="0.162715724397" Y="4.962387855204" Z="0.4" />
                  <Point X="0" Y="4.355124473572" Z="0.4" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>