<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#207" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="3285" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="0" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="0.004716064453" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.918348083496" Y="-4.837573730469" />
                  <Point X="0.563302001953" Y="-3.512524658203" />
                  <Point X="0.557721130371" Y="-3.497141845703" />
                  <Point X="0.542363098145" Y="-3.467377197266" />
                  <Point X="0.397246917725" Y="-3.258292236328" />
                  <Point X="0.378635437012" Y="-3.231476806641" />
                  <Point X="0.356751678467" Y="-3.209021484375" />
                  <Point X="0.330496337891" Y="-3.189777587891" />
                  <Point X="0.302495269775" Y="-3.175669189453" />
                  <Point X="0.077936233521" Y="-3.105974365234" />
                  <Point X="0.049136127472" Y="-3.097035888672" />
                  <Point X="0.020976577759" Y="-3.092766601562" />
                  <Point X="-0.00866505146" Y="-3.092766601562" />
                  <Point X="-0.036824447632" Y="-3.097035888672" />
                  <Point X="-0.261383331299" Y="-3.16673046875" />
                  <Point X="-0.290183563232" Y="-3.175669189453" />
                  <Point X="-0.31818572998" Y="-3.189778320312" />
                  <Point X="-0.344440765381" Y="-3.209022460938" />
                  <Point X="-0.366323608398" Y="-3.231477050781" />
                  <Point X="-0.511439788818" Y="-3.440561767578" />
                  <Point X="-0.530051269531" Y="-3.467377441406" />
                  <Point X="-0.538188781738" Y="-3.481573486328" />
                  <Point X="-0.55099017334" Y="-3.512524414063" />
                  <Point X="-0.59652545166" Y="-3.682464599609" />
                  <Point X="-0.916584655762" Y="-4.876941894531" />
                  <Point X="-1.049545166016" Y="-4.851133789062" />
                  <Point X="-1.079340698242" Y="-4.845350585938" />
                  <Point X="-1.24641796875" Y="-4.802362792969" />
                  <Point X="-1.214963012695" Y="-4.563438476562" />
                  <Point X="-1.214201171875" Y="-4.5479296875" />
                  <Point X="-1.216508666992" Y="-4.516224121094" />
                  <Point X="-1.270155761719" Y="-4.246522460938" />
                  <Point X="-1.277036132812" Y="-4.211932617188" />
                  <Point X="-1.287938354492" Y="-4.182966308594" />
                  <Point X="-1.304010375977" Y="-4.15512890625" />
                  <Point X="-1.323644775391" Y="-4.131204101562" />
                  <Point X="-1.530390136719" Y="-3.949892822266" />
                  <Point X="-1.556905395508" Y="-3.926639404297" />
                  <Point X="-1.583188720703" Y="-3.910295410156" />
                  <Point X="-1.612886230469" Y="-3.897994384766" />
                  <Point X="-1.64302746582" Y="-3.890966308594" />
                  <Point X="-1.917424804688" Y="-3.872981201172" />
                  <Point X="-1.952616699219" Y="-3.870674804688" />
                  <Point X="-1.983418212891" Y="-3.873708496094" />
                  <Point X="-2.014466796875" Y="-3.882028076172" />
                  <Point X="-2.042657836914" Y="-3.894801513672" />
                  <Point X="-2.271300292969" Y="-4.047575927734" />
                  <Point X="-2.300624267578" Y="-4.067169677734" />
                  <Point X="-2.312785644531" Y="-4.076821533203" />
                  <Point X="-2.335102783203" Y="-4.099462402344" />
                  <Point X="-2.360667236328" Y="-4.132778808594" />
                  <Point X="-2.480148681641" Y="-4.288490234375" />
                  <Point X="-2.758043457031" Y="-4.116424316406" />
                  <Point X="-2.80170703125" Y="-4.089389160156" />
                  <Point X="-3.104721923828" Y="-3.856077880859" />
                  <Point X="-3.083573974609" Y="-3.819448730469" />
                  <Point X="-2.423761230469" Y="-2.676619628906" />
                  <Point X="-2.412859130859" Y="-2.647653320312" />
                  <Point X="-2.406588134766" Y="-2.616126953125" />
                  <Point X="-2.405575439453" Y="-2.585192871094" />
                  <Point X="-2.414560058594" Y="-2.555575195312" />
                  <Point X="-2.428777099609" Y="-2.526746337891" />
                  <Point X="-2.4468046875" Y="-2.501589355469" />
                  <Point X="-2.462181152344" Y="-2.486212890625" />
                  <Point X="-2.464153320312" Y="-2.484240722656" />
                  <Point X="-2.489310302734" Y="-2.466213134766" />
                  <Point X="-2.518139160156" Y="-2.45199609375" />
                  <Point X="-2.547756835938" Y="-2.443011474609" />
                  <Point X="-2.578690917969" Y="-2.444024169922" />
                  <Point X="-2.610217285156" Y="-2.450295166016" />
                  <Point X="-2.63918359375" Y="-2.461197265625" />
                  <Point X="-2.785753662109" Y="-2.545819580078" />
                  <Point X="-3.818024414062" Y="-3.141801269531" />
                  <Point X="-4.048361816406" Y="-2.839185302734" />
                  <Point X="-4.082859863281" Y="-2.793861572266" />
                  <Point X="-4.306142578125" Y="-2.419450195312" />
                  <Point X="-4.260412597656" Y="-2.384360107422" />
                  <Point X="-3.105954345703" Y="-1.498513427734" />
                  <Point X="-3.084578613281" Y="-1.475595214844" />
                  <Point X="-3.066614257812" Y="-1.448466186523" />
                  <Point X="-3.053856933594" Y="-1.419834594727" />
                  <Point X="-3.047028320312" Y="-1.39346875" />
                  <Point X="-3.04615234375" Y="-1.390087158203" />
                  <Point X="-3.043347900391" Y="-1.359662963867" />
                  <Point X="-3.045555419922" Y="-1.327991943359" />
                  <Point X="-3.052555664062" Y="-1.29824597168" />
                  <Point X="-3.068637207031" Y="-1.272261352539" />
                  <Point X="-3.089469238281" Y="-1.248303955078" />
                  <Point X="-3.112971435547" Y="-1.228767822266" />
                  <Point X="-3.136443847656" Y="-1.21495300293" />
                  <Point X="-3.139454101562" Y="-1.213181152344" />
                  <Point X="-3.168715332031" Y="-1.201957641602" />
                  <Point X="-3.200604492188" Y="-1.195475097656" />
                  <Point X="-3.231929199219" Y="-1.194383789062" />
                  <Point X="-3.416960205078" Y="-1.218743652344" />
                  <Point X="-4.732102050781" Y="-1.391885253906" />
                  <Point X="-4.820583984375" Y="-1.045481811523" />
                  <Point X="-4.834076660156" Y="-0.992658508301" />
                  <Point X="-4.892424316406" Y="-0.584698181152" />
                  <Point X="-4.847978027344" Y="-0.572788818359" />
                  <Point X="-3.532875976562" Y="-0.220408340454" />
                  <Point X="-3.517492919922" Y="-0.214827301025" />
                  <Point X="-3.487729003906" Y="-0.199469451904" />
                  <Point X="-3.463130859375" Y="-0.182396987915" />
                  <Point X="-3.459976074219" Y="-0.18020741272" />
                  <Point X="-3.437520263672" Y="-0.158323181152" />
                  <Point X="-3.418275878906" Y="-0.132066772461" />
                  <Point X="-3.404168212891" Y="-0.104066459656" />
                  <Point X="-3.39596875" Y="-0.077647796631" />
                  <Point X="-3.394917236328" Y="-0.074259529114" />
                  <Point X="-3.390647705078" Y="-0.046099830627" />
                  <Point X="-3.390647705078" Y="-0.016460325241" />
                  <Point X="-3.394917236328" Y="0.011699376106" />
                  <Point X="-3.403116699219" Y="0.038118038177" />
                  <Point X="-3.404168212891" Y="0.041506305695" />
                  <Point X="-3.418276123047" Y="0.069507080078" />
                  <Point X="-3.437520507812" Y="0.095763175964" />
                  <Point X="-3.459976074219" Y="0.117647407532" />
                  <Point X="-3.48457421875" Y="0.134719726562" />
                  <Point X="-3.496079833984" Y="0.141557739258" />
                  <Point X="-3.514802490234" Y="0.150967681885" />
                  <Point X="-3.532875976562" Y="0.157848342896" />
                  <Point X="-3.701540527344" Y="0.203041809082" />
                  <Point X="-4.89181640625" Y="0.521975158691" />
                  <Point X="-4.833182617188" Y="0.918215332031" />
                  <Point X="-4.824487792969" Y="0.976975708008" />
                  <Point X="-4.703551269531" Y="1.423267944336" />
                  <Point X="-3.765666259766" Y="1.29979284668" />
                  <Point X="-3.744986083984" Y="1.299341552734" />
                  <Point X="-3.723424560547" Y="1.301228149414" />
                  <Point X="-3.703137695312" Y="1.305263427734" />
                  <Point X="-3.648694091797" Y="1.322429321289" />
                  <Point X="-3.641711669922" Y="1.324630981445" />
                  <Point X="-3.622779052734" Y="1.332961669922" />
                  <Point X="-3.604034667969" Y="1.343783691406" />
                  <Point X="-3.587353515625" Y="1.356014648438" />
                  <Point X="-3.573715087891" Y="1.371566162109" />
                  <Point X="-3.561300537109" Y="1.389296142578" />
                  <Point X="-3.551351318359" Y="1.407431152344" />
                  <Point X="-3.529505615234" Y="1.460171264648" />
                  <Point X="-3.526703857422" Y="1.466935302734" />
                  <Point X="-3.520915527344" Y="1.486794067383" />
                  <Point X="-3.517157226562" Y="1.508109130859" />
                  <Point X="-3.5158046875" Y="1.528749267578" />
                  <Point X="-3.518951171875" Y="1.549192871094" />
                  <Point X="-3.524552978516" Y="1.570099243164" />
                  <Point X="-3.532049804688" Y="1.589377441406" />
                  <Point X="-3.558408935547" Y="1.640013183594" />
                  <Point X="-3.561789550781" Y="1.646507080078" />
                  <Point X="-3.573281005859" Y="1.663705688477" />
                  <Point X="-3.587193603516" Y="1.680286132812" />
                  <Point X="-3.602135986328" Y="1.694590332031" />
                  <Point X="-3.698882324219" Y="1.768826416016" />
                  <Point X="-4.351859863281" Y="2.269874267578" />
                  <Point X="-4.1149375" Y="2.675779541016" />
                  <Point X="-4.081153808594" Y="2.733659423828" />
                  <Point X="-3.750504882813" Y="3.158661865234" />
                  <Point X="-3.206656982422" Y="2.844670898438" />
                  <Point X="-3.187724609375" Y="2.836340332031" />
                  <Point X="-3.167082519531" Y="2.829831787109" />
                  <Point X="-3.146794677734" Y="2.825796386719" />
                  <Point X="-3.070969970703" Y="2.819162597656" />
                  <Point X="-3.061245361328" Y="2.818311767578" />
                  <Point X="-3.040565185547" Y="2.818762939453" />
                  <Point X="-3.019106445312" Y="2.821588134766" />
                  <Point X="-2.999014648438" Y="2.826504638672" />
                  <Point X="-2.980463134766" Y="2.835653320313" />
                  <Point X="-2.962208740234" Y="2.847282714844" />
                  <Point X="-2.946077392578" Y="2.860229492188" />
                  <Point X="-2.892256347656" Y="2.914050292969" />
                  <Point X="-2.885353759766" Y="2.920952880859" />
                  <Point X="-2.872406738281" Y="2.937084472656" />
                  <Point X="-2.86077734375" Y="2.955338867188" />
                  <Point X="-2.851628662109" Y="2.973890380859" />
                  <Point X="-2.846712158203" Y="2.993982177734" />
                  <Point X="-2.843886962891" Y="3.015440917969" />
                  <Point X="-2.843435791016" Y="3.03612109375" />
                  <Point X="-2.850069580078" Y="3.111945800781" />
                  <Point X="-2.850920410156" Y="3.121670654297" />
                  <Point X="-2.854955810547" Y="3.141958496094" />
                  <Point X="-2.861464355469" Y="3.162600585938" />
                  <Point X="-2.869794921875" Y="3.181532958984" />
                  <Point X="-2.912666259766" Y="3.255788085938" />
                  <Point X="-3.183332763672" Y="3.724596435547" />
                  <Point X="-2.7594609375" Y="4.049574951172" />
                  <Point X="-2.700624755859" Y="4.094684082031" />
                  <Point X="-2.167037109375" Y="4.391134277344" />
                  <Point X="-2.04319519043" Y="4.229740722656" />
                  <Point X="-2.028892333984" Y="4.214799804688" />
                  <Point X="-2.012313476562" Y="4.200888183594" />
                  <Point X="-1.995114135742" Y="4.189395507812" />
                  <Point X="-1.910721679688" Y="4.145463378906" />
                  <Point X="-1.899898193359" Y="4.139828613281" />
                  <Point X="-1.880621948242" Y="4.132332519531" />
                  <Point X="-1.859715332031" Y="4.126729980469" />
                  <Point X="-1.839271728516" Y="4.123582519531" />
                  <Point X="-1.818631469727" Y="4.124935058594" />
                  <Point X="-1.797315917969" Y="4.128692871094" />
                  <Point X="-1.777454223633" Y="4.134481445312" />
                  <Point X="-1.689553588867" Y="4.170891601563" />
                  <Point X="-1.678280029297" Y="4.175561035156" />
                  <Point X="-1.660146240234" Y="4.185509765625" />
                  <Point X="-1.642416259766" Y="4.197924316406" />
                  <Point X="-1.626864746094" Y="4.2115625" />
                  <Point X="-1.614633666992" Y="4.228243652344" />
                  <Point X="-1.603811523438" Y="4.246987792969" />
                  <Point X="-1.59548034668" Y="4.265921386719" />
                  <Point X="-1.566870361328" Y="4.356660644531" />
                  <Point X="-1.563201049805" Y="4.368297851562" />
                  <Point X="-1.559165771484" Y="4.388584472656" />
                  <Point X="-1.557279174805" Y="4.410146484375" />
                  <Point X="-1.557730224609" Y="4.430826660156" />
                  <Point X="-1.562604370117" Y="4.467848144531" />
                  <Point X="-1.584201904297" Y="4.631897949219" />
                  <Point X="-1.025801635742" Y="4.788454101562" />
                  <Point X="-0.94963482666" Y="4.80980859375" />
                  <Point X="-0.294710968018" Y="4.886457519531" />
                  <Point X="-0.133903366089" Y="4.286315429688" />
                  <Point X="-0.121129745483" Y="4.258124023438" />
                  <Point X="-0.103271453857" Y="4.231397460938" />
                  <Point X="-0.082114509583" Y="4.20880859375" />
                  <Point X="-0.054819351196" Y="4.19421875" />
                  <Point X="-0.024381277084" Y="4.183886230469" />
                  <Point X="0.006155910492" Y="4.178844238281" />
                  <Point X="0.036693107605" Y="4.183886230469" />
                  <Point X="0.06713117981" Y="4.19421875" />
                  <Point X="0.094426467896" Y="4.20880859375" />
                  <Point X="0.115583587646" Y="4.231397460938" />
                  <Point X="0.133441726685" Y="4.258124023438" />
                  <Point X="0.146215194702" Y="4.286315429688" />
                  <Point X="0.168182006836" Y="4.368296386719" />
                  <Point X="0.307419311523" Y="4.8879375" />
                  <Point X="0.777528503418" Y="4.838704589844" />
                  <Point X="0.84404095459" Y="4.831738769531" />
                  <Point X="1.414501098633" Y="4.694012207031" />
                  <Point X="1.481025512695" Y="4.677951171875" />
                  <Point X="1.852174682617" Y="4.543332519531" />
                  <Point X="1.894649414062" Y="4.527926757812" />
                  <Point X="2.253689697266" Y="4.360015625" />
                  <Point X="2.294571044922" Y="4.340896484375" />
                  <Point X="2.641448974609" Y="4.1388046875" />
                  <Point X="2.680978759766" Y="4.115774902344" />
                  <Point X="2.943260009766" Y="3.929254882812" />
                  <Point X="2.911264892578" Y="3.873837158203" />
                  <Point X="2.147581054688" Y="2.551097900391" />
                  <Point X="2.142075927734" Y="2.539930908203" />
                  <Point X="2.133077148438" Y="2.516057128906" />
                  <Point X="2.114048095703" Y="2.444897216797" />
                  <Point X="2.111675048828" Y="2.433048583984" />
                  <Point X="2.107896240234" Y="2.405020019531" />
                  <Point X="2.107727783203" Y="2.380953857422" />
                  <Point X="2.115147705078" Y="2.319420898438" />
                  <Point X="2.116099121094" Y="2.311529052734" />
                  <Point X="2.121441650391" Y="2.289606445312" />
                  <Point X="2.129708251953" Y="2.267516113281" />
                  <Point X="2.140071044922" Y="2.247470703125" />
                  <Point X="2.178145507812" Y="2.191358642578" />
                  <Point X="2.185692138672" Y="2.181653808594" />
                  <Point X="2.203875488281" Y="2.161157714844" />
                  <Point X="2.221599121094" Y="2.145592285156" />
                  <Point X="2.277711181641" Y="2.107517822266" />
                  <Point X="2.284907714844" Y="2.102634765625" />
                  <Point X="2.304952392578" Y="2.092272216797" />
                  <Point X="2.327040283203" Y="2.084006347656" />
                  <Point X="2.348963623047" Y="2.078663574219" />
                  <Point X="2.410496826172" Y="2.071243652344" />
                  <Point X="2.423237304688" Y="2.070570068359" />
                  <Point X="2.45003125" Y="2.070955810547" />
                  <Point X="2.473206054688" Y="2.074170898438" />
                  <Point X="2.544365966797" Y="2.093200195312" />
                  <Point X="2.551276855469" Y="2.095333251953" />
                  <Point X="2.572487060547" Y="2.102775390625" />
                  <Point X="2.588533935547" Y="2.110145019531" />
                  <Point X="2.758177978516" Y="2.208089111328" />
                  <Point X="3.967326660156" Y="2.906191162109" />
                  <Point X="4.099823730469" Y="2.722050292969" />
                  <Point X="4.123272949219" Y="2.6894609375" />
                  <Point X="4.26219921875" Y="2.459884033203" />
                  <Point X="4.236245117188" Y="2.43996875" />
                  <Point X="3.230783691406" Y="1.668451293945" />
                  <Point X="3.221425048828" Y="1.660241943359" />
                  <Point X="3.203973876953" Y="1.641628051758" />
                  <Point X="3.152760009766" Y="1.574815551758" />
                  <Point X="3.146309326172" Y="1.565248535156" />
                  <Point X="3.131271240234" Y="1.539726806641" />
                  <Point X="3.121629882812" Y="1.517085571289" />
                  <Point X="3.102552490234" Y="1.448869750977" />
                  <Point X="3.100105957031" Y="1.440120849609" />
                  <Point X="3.09665234375" Y="1.417821411133" />
                  <Point X="3.095836425781" Y="1.394251953125" />
                  <Point X="3.097739501953" Y="1.371767822266" />
                  <Point X="3.113399902344" Y="1.295868896484" />
                  <Point X="3.116485107422" Y="1.284519165039" />
                  <Point X="3.125691650391" Y="1.257407836914" />
                  <Point X="3.136282470703" Y="1.235740356445" />
                  <Point X="3.178877197266" Y="1.170998291016" />
                  <Point X="3.184340087891" Y="1.162695068359" />
                  <Point X="3.198893554688" Y="1.145450317383" />
                  <Point X="3.216137939453" Y="1.129359985352" />
                  <Point X="3.23434765625" Y="1.116034301758" />
                  <Point X="3.296073242188" Y="1.081288330078" />
                  <Point X="3.307106689453" Y="1.075982666016" />
                  <Point X="3.332997802734" Y="1.065528930664" />
                  <Point X="3.356117919922" Y="1.059438598633" />
                  <Point X="3.439574951172" Y="1.048408691406" />
                  <Point X="3.446367919922" Y="1.047758178711" />
                  <Point X="3.470149414062" Y="1.046340209961" />
                  <Point X="3.488203857422" Y="1.04698449707" />
                  <Point X="3.649354248047" Y="1.068200439453" />
                  <Point X="4.776839355469" Y="1.21663671875" />
                  <Point X="4.835865234375" Y="0.97417578125" />
                  <Point X="4.845936035156" Y="0.932809082031" />
                  <Point X="4.890864746094" Y="0.644238464355" />
                  <Point X="4.868859863281" Y="0.638342224121" />
                  <Point X="3.716579833984" Y="0.329589904785" />
                  <Point X="3.704790283203" Y="0.325586151123" />
                  <Point X="3.681545898438" Y="0.315067962646" />
                  <Point X="3.599551757813" Y="0.26767401123" />
                  <Point X="3.590333007812" Y="0.261604827881" />
                  <Point X="3.565505371094" Y="0.243106277466" />
                  <Point X="3.547531005859" Y="0.225576599121" />
                  <Point X="3.498334716797" Y="0.162888839722" />
                  <Point X="3.492025146484" Y="0.154848953247" />
                  <Point X="3.480301513672" Y="0.135569747925" />
                  <Point X="3.470527587891" Y="0.114106285095" />
                  <Point X="3.463680908203" Y="0.092604568481" />
                  <Point X="3.447281982422" Y="0.006975962162" />
                  <Point X="3.445822265625" Y="-0.004202794075" />
                  <Point X="3.443718994141" Y="-0.033994846344" />
                  <Point X="3.445178710938" Y="-0.058554080963" />
                  <Point X="3.461577636719" Y="-0.144182540894" />
                  <Point X="3.463680908203" Y="-0.15516456604" />
                  <Point X="3.470527099609" Y="-0.176665527344" />
                  <Point X="3.48030078125" Y="-0.198128997803" />
                  <Point X="3.492024902344" Y="-0.217409103394" />
                  <Point X="3.541221191406" Y="-0.280096862793" />
                  <Point X="3.549044677734" Y="-0.288885528564" />
                  <Point X="3.569666259766" Y="-0.309345672607" />
                  <Point X="3.589035888672" Y="-0.324155700684" />
                  <Point X="3.671030029297" Y="-0.371549682617" />
                  <Point X="3.676727783203" Y="-0.374589599609" />
                  <Point X="3.699324462891" Y="-0.385675598145" />
                  <Point X="3.716579833984" Y="-0.392149932861" />
                  <Point X="3.864362060547" Y="-0.431748077393" />
                  <Point X="4.891472167969" Y="-0.706961425781" />
                  <Point X="4.860645507812" Y="-0.911428955078" />
                  <Point X="4.855022460938" Y="-0.948725769043" />
                  <Point X="4.801173828125" Y="-1.184698974609" />
                  <Point X="4.760703125" Y="-1.17937097168" />
                  <Point X="3.424382080078" Y="-1.003440979004" />
                  <Point X="3.40803515625" Y="-1.002710266113" />
                  <Point X="3.374658447266" Y="-1.005508850098" />
                  <Point X="3.213733154297" Y="-1.040486694336" />
                  <Point X="3.193094238281" Y="-1.04497253418" />
                  <Point X="3.163973876953" Y="-1.056597167969" />
                  <Point X="3.136147460938" Y="-1.073489379883" />
                  <Point X="3.112397460938" Y="-1.093960083008" />
                  <Point X="3.015128173828" Y="-1.210944702148" />
                  <Point X="3.002653320312" Y="-1.225948242188" />
                  <Point X="2.987932617188" Y="-1.250330322266" />
                  <Point X="2.976589355469" Y="-1.277715209961" />
                  <Point X="2.969757568359" Y="-1.305365356445" />
                  <Point X="2.955816650391" Y="-1.456865356445" />
                  <Point X="2.954028564453" Y="-1.476295532227" />
                  <Point X="2.956347412109" Y="-1.507564331055" />
                  <Point X="2.964079101562" Y="-1.539185546875" />
                  <Point X="2.976450439453" Y="-1.567996582031" />
                  <Point X="3.065508789062" Y="-1.706521118164" />
                  <Point X="3.076930664062" Y="-1.724287109375" />
                  <Point X="3.086932373047" Y="-1.73723828125" />
                  <Point X="3.110628417969" Y="-1.760909301758" />
                  <Point X="3.247771484375" Y="-1.866142700195" />
                  <Point X="4.213122070312" Y="-2.6068828125" />
                  <Point X="4.14066015625" Y="-2.724137207031" />
                  <Point X="4.124806640625" Y="-2.749790771484" />
                  <Point X="4.028981445312" Y="-2.885945068359" />
                  <Point X="3.991128662109" Y="-2.864090820312" />
                  <Point X="2.800954589844" Y="-2.176943115234" />
                  <Point X="2.786131347656" Y="-2.170012207031" />
                  <Point X="2.754224365234" Y="-2.159825195312" />
                  <Point X="2.562697509766" Y="-2.125235595703" />
                  <Point X="2.538134033203" Y="-2.120799560547" />
                  <Point X="2.506781738281" Y="-2.120395263672" />
                  <Point X="2.474609130859" Y="-2.125353027344" />
                  <Point X="2.444833251953" Y="-2.135176757812" />
                  <Point X="2.285721435547" Y="-2.218916015625" />
                  <Point X="2.265315185547" Y="-2.229655761719" />
                  <Point X="2.242384033203" Y="-2.246549072266" />
                  <Point X="2.221424316406" Y="-2.267509033203" />
                  <Point X="2.204531738281" Y="-2.290439453125" />
                  <Point X="2.120792480469" Y="-2.449551025391" />
                  <Point X="2.110052734375" Y="-2.469957519531" />
                  <Point X="2.100229003906" Y="-2.499733154297" />
                  <Point X="2.095271240234" Y="-2.531905517578" />
                  <Point X="2.095675537109" Y="-2.563258056641" />
                  <Point X="2.130265136719" Y="-2.754784667969" />
                  <Point X="2.134701171875" Y="-2.779348388672" />
                  <Point X="2.138985351562" Y="-2.795141357422" />
                  <Point X="2.151819091797" Y="-2.826078613281" />
                  <Point X="2.239947265625" Y="-2.978720947266" />
                  <Point X="2.861283203125" Y="-4.054906494141" />
                  <Point X="2.800661865234" Y="-4.09820703125" />
                  <Point X="2.781852294922" Y="-4.111642578125" />
                  <Point X="2.701764892578" Y="-4.163481445312" />
                  <Point X="2.666758544922" Y="-4.117860351563" />
                  <Point X="1.758546264648" Y="-2.934255126953" />
                  <Point X="1.747504150391" Y="-2.9221796875" />
                  <Point X="1.721924316406" Y="-2.900557373047" />
                  <Point X="1.533027099609" Y="-2.779114013672" />
                  <Point X="1.50880078125" Y="-2.763538574219" />
                  <Point X="1.479989624023" Y="-2.751167236328" />
                  <Point X="1.448368286133" Y="-2.743435546875" />
                  <Point X="1.417099487305" Y="-2.741116699219" />
                  <Point X="1.210508422852" Y="-2.760127441406" />
                  <Point X="1.184012573242" Y="-2.762565673828" />
                  <Point X="1.156362915039" Y="-2.769397460938" />
                  <Point X="1.128977783203" Y="-2.780740722656" />
                  <Point X="1.104595703125" Y="-2.795461181641" />
                  <Point X="0.945071411133" Y="-2.928100341797" />
                  <Point X="0.924611999512" Y="-2.945111572266" />
                  <Point X="0.904141113281" Y="-2.968861816406" />
                  <Point X="0.887248779297" Y="-2.996688476562" />
                  <Point X="0.875624267578" Y="-3.025808837891" />
                  <Point X="0.827927429199" Y="-3.245252441406" />
                  <Point X="0.821809997559" Y="-3.273396728516" />
                  <Point X="0.81972454834" Y="-3.289627441406" />
                  <Point X="0.81974230957" Y="-3.323120117188" />
                  <Point X="0.844717163086" Y="-3.512822509766" />
                  <Point X="1.022065307617" Y="-4.859915527344" />
                  <Point X="0.993472473145" Y="-4.866183105469" />
                  <Point X="0.975682373047" Y="-4.870082519531" />
                  <Point X="0.929315673828" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.031443237305" Y="-4.757874511719" />
                  <Point X="-1.058434082031" Y="-4.752635742188" />
                  <Point X="-1.141246337891" Y="-4.731328613281" />
                  <Point X="-1.120775634766" Y="-4.575838378906" />
                  <Point X="-1.120077514648" Y="-4.568099609375" />
                  <Point X="-1.119451782227" Y="-4.541033691406" />
                  <Point X="-1.121759277344" Y="-4.509328125" />
                  <Point X="-1.123334106445" Y="-4.497690429688" />
                  <Point X="-1.176981201172" Y="-4.227988769531" />
                  <Point X="-1.183861450195" Y="-4.193398925781" />
                  <Point X="-1.188125" Y="-4.17846875" />
                  <Point X="-1.19902734375" Y="-4.149502441406" />
                  <Point X="-1.205666015625" Y="-4.135466308594" />
                  <Point X="-1.221738037109" Y="-4.10762890625" />
                  <Point X="-1.23057409668" Y="-4.094861816406" />
                  <Point X="-1.250208496094" Y="-4.070937011719" />
                  <Point X="-1.261006958008" Y="-4.059779296875" />
                  <Point X="-1.467752197266" Y="-3.878468017578" />
                  <Point X="-1.494267456055" Y="-3.855214599609" />
                  <Point X="-1.506739013672" Y="-3.845965087891" />
                  <Point X="-1.533022338867" Y="-3.82962109375" />
                  <Point X="-1.546833984375" Y="-3.822526855469" />
                  <Point X="-1.576531494141" Y="-3.810225830078" />
                  <Point X="-1.591313598633" Y="-3.805476074219" />
                  <Point X="-1.621454833984" Y="-3.798447998047" />
                  <Point X="-1.636813964844" Y="-3.796169677734" />
                  <Point X="-1.911211425781" Y="-3.778184570312" />
                  <Point X="-1.946403198242" Y="-3.775878173828" />
                  <Point X="-1.961928344727" Y="-3.776132324219" />
                  <Point X="-1.992729858398" Y="-3.779166015625" />
                  <Point X="-2.008006469727" Y="-3.781945556641" />
                  <Point X="-2.039054931641" Y="-3.790265136719" />
                  <Point X="-2.053674560547" Y="-3.795496337891" />
                  <Point X="-2.081865722656" Y="-3.808269775391" />
                  <Point X="-2.095437011719" Y="-3.815812011719" />
                  <Point X="-2.324079589844" Y="-3.968586425781" />
                  <Point X="-2.353403564453" Y="-3.988180175781" />
                  <Point X="-2.359681640625" Y="-3.992757080078" />
                  <Point X="-2.380442626953" Y="-4.010131835938" />
                  <Point X="-2.402759765625" Y="-4.032772705078" />
                  <Point X="-2.410471435547" Y="-4.041630126953" />
                  <Point X="-2.436035888672" Y="-4.074946533203" />
                  <Point X="-2.503202148438" Y="-4.162479492188" />
                  <Point X="-2.708032226562" Y="-4.035653808594" />
                  <Point X="-2.747583496094" Y="-4.011164794922" />
                  <Point X="-2.980862792969" Y="-3.831547607422" />
                  <Point X="-2.341489013672" Y="-2.724119873047" />
                  <Point X="-2.334850097656" Y="-2.710083251953" />
                  <Point X="-2.323947998047" Y="-2.681116943359" />
                  <Point X="-2.319684570312" Y="-2.666187011719" />
                  <Point X="-2.313413574219" Y="-2.634660644531" />
                  <Point X="-2.311638916016" Y="-2.619235351562" />
                  <Point X="-2.310626220703" Y="-2.588301269531" />
                  <Point X="-2.314666259766" Y="-2.557615234375" />
                  <Point X="-2.323650878906" Y="-2.527997558594" />
                  <Point X="-2.329357421875" Y="-2.513557128906" />
                  <Point X="-2.343574462891" Y="-2.484728271484" />
                  <Point X="-2.351557128906" Y="-2.47141015625" />
                  <Point X="-2.369584716797" Y="-2.446253173828" />
                  <Point X="-2.379629638672" Y="-2.434414306641" />
                  <Point X="-2.395006103516" Y="-2.419037841797" />
                  <Point X="-2.408817138672" Y="-2.407020751953" />
                  <Point X="-2.433974121094" Y="-2.388993164062" />
                  <Point X="-2.447292236328" Y="-2.381010498047" />
                  <Point X="-2.47612109375" Y="-2.366793457031" />
                  <Point X="-2.490561523438" Y="-2.361086914062" />
                  <Point X="-2.520179199219" Y="-2.352102294922" />
                  <Point X="-2.550865234375" Y="-2.348062255859" />
                  <Point X="-2.581799316406" Y="-2.349074951172" />
                  <Point X="-2.597224609375" Y="-2.350849609375" />
                  <Point X="-2.628750976562" Y="-2.357120605469" />
                  <Point X="-2.643680908203" Y="-2.361384033203" />
                  <Point X="-2.672647216797" Y="-2.372286132812" />
                  <Point X="-2.68668359375" Y="-2.378924804688" />
                  <Point X="-2.833253662109" Y="-2.463547119141" />
                  <Point X="-3.793089599609" Y="-3.017708496094" />
                  <Point X="-3.972768554688" Y="-2.781647216797" />
                  <Point X="-4.004014160156" Y="-2.740596679688" />
                  <Point X="-4.181265136719" Y="-2.443373291016" />
                  <Point X="-3.048121826172" Y="-1.573881835938" />
                  <Point X="-3.036481933594" Y="-1.563310180664" />
                  <Point X="-3.015106201172" Y="-1.540391845703" />
                  <Point X="-3.005370361328" Y="-1.528045532227" />
                  <Point X="-2.987406005859" Y="-1.500916503906" />
                  <Point X="-2.979838378906" Y="-1.487130737305" />
                  <Point X="-2.967081054688" Y="-1.458499145508" />
                  <Point X="-2.961891357422" Y="-1.443653198242" />
                  <Point X="-2.955062744141" Y="-1.417287353516" />
                  <Point X="-2.951553466797" Y="-1.398807128906" />
                  <Point X="-2.948749023438" Y="-1.3683828125" />
                  <Point X="-2.948577880859" Y="-1.353057373047" />
                  <Point X="-2.950785400391" Y="-1.321386352539" />
                  <Point X="-2.953081542969" Y="-1.306229614258" />
                  <Point X="-2.960081787109" Y="-1.276483886719" />
                  <Point X="-2.971774658203" Y="-1.248251708984" />
                  <Point X="-2.987856201172" Y="-1.222267089844" />
                  <Point X="-2.996948974609" Y="-1.209925170898" />
                  <Point X="-3.017781005859" Y="-1.185967773438" />
                  <Point X="-3.028741699219" Y="-1.175248046875" />
                  <Point X="-3.052243896484" Y="-1.155711914062" />
                  <Point X="-3.06478515625" Y="-1.146895507812" />
                  <Point X="-3.088257568359" Y="-1.133080688477" />
                  <Point X="-3.105432373047" Y="-1.124481933594" />
                  <Point X="-3.134693603516" Y="-1.113258544922" />
                  <Point X="-3.149790527344" Y="-1.108861694336" />
                  <Point X="-3.1816796875" Y="-1.102379150391" />
                  <Point X="-3.197296875" Y="-1.100532714844" />
                  <Point X="-3.228621582031" Y="-1.09944140625" />
                  <Point X="-3.244329101562" Y="-1.100196533203" />
                  <Point X="-3.429360107422" Y="-1.124556396484" />
                  <Point X="-4.660920898438" Y="-1.286694335938" />
                  <Point X="-4.7285390625" Y="-1.021970825195" />
                  <Point X="-4.740761230469" Y="-0.974121032715" />
                  <Point X="-4.786452148438" Y="-0.654654296875" />
                  <Point X="-3.508288085938" Y="-0.312171325684" />
                  <Point X="-3.500476074219" Y="-0.309712524414" />
                  <Point X="-3.473931152344" Y="-0.299251098633" />
                  <Point X="-3.444167236328" Y="-0.283893310547" />
                  <Point X="-3.433562011719" Y="-0.277513824463" />
                  <Point X="-3.408963867188" Y="-0.260441345215" />
                  <Point X="-3.393672363281" Y="-0.248242828369" />
                  <Point X="-3.371216552734" Y="-0.226358551025" />
                  <Point X="-3.360897460938" Y="-0.214483139038" />
                  <Point X="-3.341653076172" Y="-0.188226669312" />
                  <Point X="-3.333436035156" Y="-0.174812423706" />
                  <Point X="-3.319328369141" Y="-0.146812164307" />
                  <Point X="-3.313437744141" Y="-0.132226165771" />
                  <Point X="-3.30523828125" Y="-0.105807548523" />
                  <Point X="-3.300990722656" Y="-0.088500556946" />
                  <Point X="-3.296721191406" Y="-0.060340827942" />
                  <Point X="-3.295647705078" Y="-0.046099781036" />
                  <Point X="-3.295647705078" Y="-0.016460363388" />
                  <Point X="-3.296721191406" Y="-0.002219316721" />
                  <Point X="-3.300990722656" Y="0.025940412521" />
                  <Point X="-3.304186767578" Y="0.039859096527" />
                  <Point X="-3.312386230469" Y="0.066277709961" />
                  <Point X="-3.319328369141" Y="0.08425201416" />
                  <Point X="-3.333436279297" Y="0.112252868652" />
                  <Point X="-3.341653564453" Y="0.125667419434" />
                  <Point X="-3.360897949219" Y="0.151923583984" />
                  <Point X="-3.371216552734" Y="0.16379826355" />
                  <Point X="-3.393672119141" Y="0.185682540894" />
                  <Point X="-3.405809326172" Y="0.195691986084" />
                  <Point X="-3.430407470703" Y="0.212764297485" />
                  <Point X="-3.436038574219" Y="0.216385498047" />
                  <Point X="-3.453418212891" Y="0.226439987183" />
                  <Point X="-3.472140869141" Y="0.235849899292" />
                  <Point X="-3.481002197266" Y="0.239751403809" />
                  <Point X="-3.508288085938" Y="0.249611343384" />
                  <Point X="-3.676952636719" Y="0.294804748535" />
                  <Point X="-4.785446289062" Y="0.591824523926" />
                  <Point X="-4.739206054688" Y="0.904309020996" />
                  <Point X="-4.731330566406" Y="0.957533081055" />
                  <Point X="-4.633586425781" Y="1.318237182617" />
                  <Point X="-3.778066162109" Y="1.20560559082" />
                  <Point X="-3.767739013672" Y="1.204815429688" />
                  <Point X="-3.747058837891" Y="1.204364135742" />
                  <Point X="-3.736705322266" Y="1.204703125" />
                  <Point X="-3.715143798828" Y="1.20658972168" />
                  <Point X="-3.704891113281" Y="1.208053588867" />
                  <Point X="-3.684604248047" Y="1.212088745117" />
                  <Point X="-3.674570800781" Y="1.21466027832" />
                  <Point X="-3.620127197266" Y="1.231826171875" />
                  <Point X="-3.603450195312" Y="1.237676635742" />
                  <Point X="-3.584517578125" Y="1.246007324219" />
                  <Point X="-3.575279296875" Y="1.250689331055" />
                  <Point X="-3.556534912109" Y="1.261511230469" />
                  <Point X="-3.547860839844" Y="1.267171142578" />
                  <Point X="-3.5311796875" Y="1.279401977539" />
                  <Point X="-3.515928955078" Y="1.293376464844" />
                  <Point X="-3.502290527344" Y="1.308927856445" />
                  <Point X="-3.495895507812" Y="1.317076782227" />
                  <Point X="-3.483480957031" Y="1.334806762695" />
                  <Point X="-3.478011474609" Y="1.343602172852" />
                  <Point X="-3.468062255859" Y="1.361737182617" />
                  <Point X="-3.463582763672" Y="1.371076171875" />
                  <Point X="-3.441737060547" Y="1.42381628418" />
                  <Point X="-3.435499267578" Y="1.44035144043" />
                  <Point X="-3.4297109375" Y="1.460210205078" />
                  <Point X="-3.427358642578" Y="1.470298095703" />
                  <Point X="-3.423600341797" Y="1.491613037109" />
                  <Point X="-3.422360595703" Y="1.501897094727" />
                  <Point X="-3.421008056641" Y="1.522537231445" />
                  <Point X="-3.421910400391" Y="1.543200561523" />
                  <Point X="-3.425056884766" Y="1.563644165039" />
                  <Point X="-3.427188232422" Y="1.573780395508" />
                  <Point X="-3.432790039062" Y="1.594686889648" />
                  <Point X="-3.436012207031" Y="1.604530639648" />
                  <Point X="-3.443509033203" Y="1.623808837891" />
                  <Point X="-3.447783691406" Y="1.633243286133" />
                  <Point X="-3.474142822266" Y="1.68387890625" />
                  <Point X="-3.482799316406" Y="1.69928515625" />
                  <Point X="-3.494290771484" Y="1.716483886719" />
                  <Point X="-3.500506591797" Y="1.724770263672" />
                  <Point X="-3.514419189453" Y="1.741350830078" />
                  <Point X="-3.5215" Y="1.748910644531" />
                  <Point X="-3.536442382812" Y="1.76321496582" />
                  <Point X="-3.544303710938" Y="1.769958862305" />
                  <Point X="-3.641050048828" Y="1.844194946289" />
                  <Point X="-4.227614257812" Y="2.294282226563" />
                  <Point X="-4.032891113281" Y="2.627890136719" />
                  <Point X="-4.002293212891" Y="2.680312011719" />
                  <Point X="-3.726338623047" Y="3.035012695312" />
                  <Point X="-3.254156982422" Y="2.7623984375" />
                  <Point X="-3.244918457031" Y="2.757716552734" />
                  <Point X="-3.225986083984" Y="2.749385986328" />
                  <Point X="-3.216292236328" Y="2.745737304688" />
                  <Point X="-3.195650146484" Y="2.739228759766" />
                  <Point X="-3.185615722656" Y="2.736656982422" />
                  <Point X="-3.165327880859" Y="2.732621582031" />
                  <Point X="-3.155074462891" Y="2.731157958984" />
                  <Point X="-3.079249755859" Y="2.724524169922" />
                  <Point X="-3.059173339844" Y="2.723334472656" />
                  <Point X="-3.038493164062" Y="2.723785644531" />
                  <Point X="-3.028164794922" Y="2.724575683594" />
                  <Point X="-3.006706054688" Y="2.727400878906" />
                  <Point X="-2.996525878906" Y="2.729310791016" />
                  <Point X="-2.976434082031" Y="2.734227294922" />
                  <Point X="-2.956996826172" Y="2.741302001953" />
                  <Point X="-2.9384453125" Y="2.750450683594" />
                  <Point X="-2.929419433594" Y="2.75553125" />
                  <Point X="-2.911165039062" Y="2.767160644531" />
                  <Point X="-2.90274609375" Y="2.773193603516" />
                  <Point X="-2.886614746094" Y="2.786140380859" />
                  <Point X="-2.87890234375" Y="2.793054199219" />
                  <Point X="-2.825081298828" Y="2.846875" />
                  <Point X="-2.811264892578" Y="2.861489990234" />
                  <Point X="-2.798317871094" Y="2.877621582031" />
                  <Point X="-2.792284667969" Y="2.886040771484" />
                  <Point X="-2.780655273438" Y="2.904295166016" />
                  <Point X="-2.775574707031" Y="2.913321044922" />
                  <Point X="-2.766426025391" Y="2.931872558594" />
                  <Point X="-2.759351318359" Y="2.951309814453" />
                  <Point X="-2.754434814453" Y="2.971401611328" />
                  <Point X="-2.752524902344" Y="2.981581787109" />
                  <Point X="-2.749699707031" Y="3.003040527344" />
                  <Point X="-2.748909667969" Y="3.013368896484" />
                  <Point X="-2.748458496094" Y="3.034049072266" />
                  <Point X="-2.748797363281" Y="3.044400878906" />
                  <Point X="-2.755431152344" Y="3.120225585938" />
                  <Point X="-2.757745605469" Y="3.140203857422" />
                  <Point X="-2.761781005859" Y="3.160491699219" />
                  <Point X="-2.764352783203" Y="3.170526123047" />
                  <Point X="-2.770861328125" Y="3.191168212891" />
                  <Point X="-2.774510009766" Y="3.200862060547" />
                  <Point X="-2.782840576172" Y="3.219794433594" />
                  <Point X="-2.787522460938" Y="3.229032958984" />
                  <Point X="-2.830393798828" Y="3.303288085938" />
                  <Point X="-3.059387207031" Y="3.699916259766" />
                  <Point X="-2.701658691406" Y="3.974183105469" />
                  <Point X="-2.648373535156" Y="4.015036376953" />
                  <Point X="-2.192525146484" Y="4.268296386719" />
                  <Point X="-2.118563720703" Y="4.171908203125" />
                  <Point X="-2.111819580078" Y="4.164046875" />
                  <Point X="-2.097516845703" Y="4.149105957031" />
                  <Point X="-2.089958007813" Y="4.142026367188" />
                  <Point X="-2.073379150391" Y="4.128114746094" />
                  <Point X="-2.065093994141" Y="4.121899414062" />
                  <Point X="-2.047894775391" Y="4.110406738281" />
                  <Point X="-2.03898046875" Y="4.105129394531" />
                  <Point X="-1.954588012695" Y="4.061197509766" />
                  <Point X="-1.934329711914" Y="4.051287841797" />
                  <Point X="-1.915053466797" Y="4.043791748047" />
                  <Point X="-1.905212280273" Y="4.0405703125" />
                  <Point X="-1.884305664062" Y="4.034967773438" />
                  <Point X="-1.874171020508" Y="4.032836181641" />
                  <Point X="-1.853727416992" Y="4.029688720703" />
                  <Point X="-1.833059814453" Y="4.028785888672" />
                  <Point X="-1.812419555664" Y="4.030138427734" />
                  <Point X="-1.802137817383" Y="4.031377685547" />
                  <Point X="-1.780822387695" Y="4.035135498047" />
                  <Point X="-1.77073449707" Y="4.037487304687" />
                  <Point X="-1.750872924805" Y="4.043275878906" />
                  <Point X="-1.741098876953" Y="4.046713134766" />
                  <Point X="-1.653198242188" Y="4.083123291016" />
                  <Point X="-1.632585449219" Y="4.092272460938" />
                  <Point X="-1.614451660156" Y="4.102221191406" />
                  <Point X="-1.605656860352" Y="4.107689941406" />
                  <Point X="-1.587926879883" Y="4.120104492188" />
                  <Point X="-1.579778686523" Y="4.126499023438" />
                  <Point X="-1.564227172852" Y="4.140137207031" />
                  <Point X="-1.550252441406" Y="4.155388183594" />
                  <Point X="-1.538021362305" Y="4.172069335938" />
                  <Point X="-1.532361694336" Y="4.180743164062" />
                  <Point X="-1.521539550781" Y="4.199487304687" />
                  <Point X="-1.516857299805" Y="4.208726074219" />
                  <Point X="-1.508526123047" Y="4.227659667969" />
                  <Point X="-1.504877197266" Y="4.237354492188" />
                  <Point X="-1.476267211914" Y="4.32809375" />
                  <Point X="-1.470026489258" Y="4.349764160156" />
                  <Point X="-1.465991210938" Y="4.37005078125" />
                  <Point X="-1.46452734375" Y="4.380304199219" />
                  <Point X="-1.46264074707" Y="4.401866210938" />
                  <Point X="-1.462301757812" Y="4.412217773438" />
                  <Point X="-1.462752807617" Y="4.432897949219" />
                  <Point X="-1.46354284668" Y="4.443227050781" />
                  <Point X="-1.468417114258" Y="4.480248535156" />
                  <Point X="-1.479266113281" Y="4.562655273438" />
                  <Point X="-1.000155883789" Y="4.696981445312" />
                  <Point X="-0.931178466797" Y="4.7163203125" />
                  <Point X="-0.365222137451" Y="4.782557128906" />
                  <Point X="-0.22566633606" Y="4.261727539062" />
                  <Point X="-0.22043510437" Y="4.247107421875" />
                  <Point X="-0.207661560059" Y="4.218916015625" />
                  <Point X="-0.200119094849" Y="4.205344726562" />
                  <Point X="-0.182260864258" Y="4.178618164062" />
                  <Point X="-0.17260824585" Y="4.166456054688" />
                  <Point X="-0.151451339722" Y="4.1438671875" />
                  <Point X="-0.126897789001" Y="4.125026367188" />
                  <Point X="-0.099602752686" Y="4.110436523438" />
                  <Point X="-0.085356651306" Y="4.104260742188" />
                  <Point X="-0.054918533325" Y="4.093927978516" />
                  <Point X="-0.039857234955" Y="4.090155273438" />
                  <Point X="-0.009319986343" Y="4.08511328125" />
                  <Point X="0.021631772995" Y="4.08511328125" />
                  <Point X="0.052169025421" Y="4.090155273438" />
                  <Point X="0.067230316162" Y="4.093927978516" />
                  <Point X="0.097668441772" Y="4.104260742188" />
                  <Point X="0.111914390564" Y="4.110436523438" />
                  <Point X="0.139209732056" Y="4.125026367188" />
                  <Point X="0.163762969971" Y="4.143866699219" />
                  <Point X="0.184920181274" Y="4.166455566406" />
                  <Point X="0.1945730896" Y="4.178618164062" />
                  <Point X="0.21243132019" Y="4.205344726562" />
                  <Point X="0.219973648071" Y="4.218916503906" />
                  <Point X="0.232747192383" Y="4.247107910156" />
                  <Point X="0.237978118896" Y="4.261727539062" />
                  <Point X="0.259944885254" Y="4.343708496094" />
                  <Point X="0.378190185547" Y="4.785006347656" />
                  <Point X="0.767633605957" Y="4.744221191406" />
                  <Point X="0.827876098633" Y="4.737912109375" />
                  <Point X="1.392205810547" Y="4.601665527344" />
                  <Point X="1.453595214844" Y="4.586844238281" />
                  <Point X="1.819782348633" Y="4.454025390625" />
                  <Point X="1.858261108398" Y="4.440068847656" />
                  <Point X="2.213444824219" Y="4.273961425781" />
                  <Point X="2.250447509766" Y="4.25665625" />
                  <Point X="2.593625976562" Y="4.056719482422" />
                  <Point X="2.629436035156" Y="4.035856933594" />
                  <Point X="2.817780029297" Y="3.901916748047" />
                  <Point X="2.065308349609" Y="2.59859765625" />
                  <Point X="2.062372558594" Y="2.593104248047" />
                  <Point X="2.053181152344" Y="2.573438232422" />
                  <Point X="2.044182373047" Y="2.549564453125" />
                  <Point X="2.041301757812" Y="2.540598876953" />
                  <Point X="2.022272827148" Y="2.469438964844" />
                  <Point X="2.017526855469" Y="2.445741699219" />
                  <Point X="2.013748046875" Y="2.417713134766" />
                  <Point X="2.0128984375" Y="2.405685058594" />
                  <Point X="2.012730102539" Y="2.381618896484" />
                  <Point X="2.013410888672" Y="2.369580810547" />
                  <Point X="2.020830932617" Y="2.308047851563" />
                  <Point X="2.023800415039" Y="2.289035888672" />
                  <Point X="2.029142944336" Y="2.26711328125" />
                  <Point X="2.032467529297" Y="2.256310791016" />
                  <Point X="2.040734130859" Y="2.234220458984" />
                  <Point X="2.045318115234" Y="2.223889160156" />
                  <Point X="2.055680908203" Y="2.20384375" />
                  <Point X="2.061459716797" Y="2.194129638672" />
                  <Point X="2.099534179688" Y="2.138017578125" />
                  <Point X="2.114627441406" Y="2.118607910156" />
                  <Point X="2.132810791016" Y="2.098111816406" />
                  <Point X="2.141187011719" Y="2.08977734375" />
                  <Point X="2.158910644531" Y="2.074211914062" />
                  <Point X="2.168258056641" Y="2.066980957031" />
                  <Point X="2.224370117188" Y="2.028906616211" />
                  <Point X="2.241280517578" Y="2.018244873047" />
                  <Point X="2.261325195312" Y="2.007882324219" />
                  <Point X="2.271656005859" Y="2.003298461914" />
                  <Point X="2.293743896484" Y="1.995032470703" />
                  <Point X="2.304546875" Y="1.991707641602" />
                  <Point X="2.326470214844" Y="1.986364868164" />
                  <Point X="2.337590576172" Y="1.984346801758" />
                  <Point X="2.399123779297" Y="1.976926879883" />
                  <Point X="2.424604736328" Y="1.975579956055" />
                  <Point X="2.451398681641" Y="1.975965820312" />
                  <Point X="2.463085693359" Y="1.976857055664" />
                  <Point X="2.486260498047" Y="1.980072265625" />
                  <Point X="2.497748291016" Y="1.982395751953" />
                  <Point X="2.568908203125" Y="2.001425048828" />
                  <Point X="2.582729980469" Y="2.005691040039" />
                  <Point X="2.603940185547" Y="2.013133178711" />
                  <Point X="2.612135009766" Y="2.016444458008" />
                  <Point X="2.636033935547" Y="2.027872558594" />
                  <Point X="2.805677978516" Y="2.125816650391" />
                  <Point X="3.940405029297" Y="2.780951416016" />
                  <Point X="4.022711181641" Y="2.666564697266" />
                  <Point X="4.043953857422" Y="2.637041992188" />
                  <Point X="4.136885253906" Y="2.483472167969" />
                  <Point X="3.172951416016" Y="1.743819824219" />
                  <Point X="3.168136962891" Y="1.739868530273" />
                  <Point X="3.152120117188" Y="1.725217651367" />
                  <Point X="3.134668945312" Y="1.706603759766" />
                  <Point X="3.128576416016" Y="1.699422485352" />
                  <Point X="3.077362548828" Y="1.632610107422" />
                  <Point X="3.064461181641" Y="1.613475708008" />
                  <Point X="3.049423095703" Y="1.587953979492" />
                  <Point X="3.043865966797" Y="1.576946777344" />
                  <Point X="3.034224609375" Y="1.554305541992" />
                  <Point X="3.030140380859" Y="1.54267175293" />
                  <Point X="3.011062988281" Y="1.474455932617" />
                  <Point X="3.006225097656" Y="1.454660522461" />
                  <Point X="3.002771484375" Y="1.432361206055" />
                  <Point X="3.001709228516" Y="1.421108154297" />
                  <Point X="3.000893310547" Y="1.397538696289" />
                  <Point X="3.001174804688" Y="1.386239746094" />
                  <Point X="3.003077880859" Y="1.363755615234" />
                  <Point X="3.004699462891" Y="1.352570678711" />
                  <Point X="3.020359863281" Y="1.276671630859" />
                  <Point X="3.026530273438" Y="1.253972045898" />
                  <Point X="3.035736816406" Y="1.226860717773" />
                  <Point X="3.040341796875" Y="1.215689819336" />
                  <Point X="3.050932617188" Y="1.194022338867" />
                  <Point X="3.056918457031" Y="1.183525756836" />
                  <Point X="3.099513183594" Y="1.118783569336" />
                  <Point X="3.111739013672" Y="1.101424438477" />
                  <Point X="3.126292480469" Y="1.08417980957" />
                  <Point X="3.134082763672" Y="1.075991210938" />
                  <Point X="3.151327148438" Y="1.059900878906" />
                  <Point X="3.160035400391" Y="1.052695068359" />
                  <Point X="3.178245117188" Y="1.039369384766" />
                  <Point X="3.187747070312" Y="1.033249267578" />
                  <Point X="3.24947265625" Y="0.998503112793" />
                  <Point X="3.271539306641" Y="0.987892028809" />
                  <Point X="3.297430419922" Y="0.977438171387" />
                  <Point X="3.308798339844" Y="0.973662719727" />
                  <Point X="3.331918457031" Y="0.96757244873" />
                  <Point X="3.343670654297" Y="0.965257507324" />
                  <Point X="3.427127685547" Y="0.954227600098" />
                  <Point X="3.440713623047" Y="0.952926574707" />
                  <Point X="3.464495117188" Y="0.951508544922" />
                  <Point X="3.473537353516" Y="0.951400634766" />
                  <Point X="3.500604003906" Y="0.952797241211" />
                  <Point X="3.661754394531" Y="0.974013183594" />
                  <Point X="4.704703613281" Y="1.111320068359" />
                  <Point X="4.743561035156" Y="0.95170489502" />
                  <Point X="4.75268359375" Y="0.914233276367" />
                  <Point X="4.78387109375" Y="0.713920776367" />
                  <Point X="3.691991943359" Y="0.421352844238" />
                  <Point X="3.68603125" Y="0.419544250488" />
                  <Point X="3.665625488281" Y="0.412137329102" />
                  <Point X="3.642381103516" Y="0.401619140625" />
                  <Point X="3.634004882812" Y="0.397316650391" />
                  <Point X="3.552010742188" Y="0.349922729492" />
                  <Point X="3.533573242188" Y="0.337784393311" />
                  <Point X="3.508745605469" Y="0.319285766602" />
                  <Point X="3.499176757812" Y="0.311117431641" />
                  <Point X="3.481202392578" Y="0.29358782959" />
                  <Point X="3.472796875" Y="0.284226654053" />
                  <Point X="3.423600585938" Y="0.221538848877" />
                  <Point X="3.410854736328" Y="0.204208526611" />
                  <Point X="3.399131103516" Y="0.184929321289" />
                  <Point X="3.39384375" Y="0.17494052124" />
                  <Point X="3.384069824219" Y="0.153476989746" />
                  <Point X="3.380006103516" Y="0.142930557251" />
                  <Point X="3.373159423828" Y="0.121428840637" />
                  <Point X="3.370376464844" Y="0.110473548889" />
                  <Point X="3.353977539062" Y="0.024844913483" />
                  <Point X="3.351058105469" Y="0.002487421989" />
                  <Point X="3.348954833984" Y="-0.027304632187" />
                  <Point X="3.348886474609" Y="-0.039631416321" />
                  <Point X="3.350346191406" Y="-0.064190605164" />
                  <Point X="3.351874267578" Y="-0.076423019409" />
                  <Point X="3.368273193359" Y="-0.162051498413" />
                  <Point X="3.373158935547" Y="-0.183987945557" />
                  <Point X="3.380005126953" Y="-0.205488922119" />
                  <Point X="3.384068847656" Y="-0.216035354614" />
                  <Point X="3.393842529297" Y="-0.237498886108" />
                  <Point X="3.399130126953" Y="-0.247488265991" />
                  <Point X="3.410854248047" Y="-0.266768371582" />
                  <Point X="3.417290771484" Y="-0.276059082031" />
                  <Point X="3.466487060547" Y="-0.338746887207" />
                  <Point X="3.482134033203" Y="-0.356324066162" />
                  <Point X="3.502755615234" Y="-0.376784240723" />
                  <Point X="3.511963623047" Y="-0.384813446045" />
                  <Point X="3.531333251953" Y="-0.399623565674" />
                  <Point X="3.541494873047" Y="-0.40640435791" />
                  <Point X="3.623489013672" Y="-0.453798400879" />
                  <Point X="3.634884765625" Y="-0.459878417969" />
                  <Point X="3.657481445312" Y="-0.470964355469" />
                  <Point X="3.665951660156" Y="-0.474620788574" />
                  <Point X="3.691991943359" Y="-0.483912841797" />
                  <Point X="3.839774169922" Y="-0.523511047363" />
                  <Point X="4.784876464844" Y="-0.776750488281" />
                  <Point X="4.76670703125" Y="-0.897266235352" />
                  <Point X="4.76161328125" Y="-0.931052001953" />
                  <Point X="4.727801757812" Y="-1.079219726563" />
                  <Point X="3.436781982422" Y="-0.909253723145" />
                  <Point X="3.428624267578" Y="-0.908535766602" />
                  <Point X="3.400097412109" Y="-0.908042480469" />
                  <Point X="3.366720703125" Y="-0.910841003418" />
                  <Point X="3.354480957031" Y="-0.912676391602" />
                  <Point X="3.193555664062" Y="-0.947654174805" />
                  <Point X="3.172916748047" Y="-0.952140075684" />
                  <Point X="3.157873535156" Y="-0.956742736816" />
                  <Point X="3.128753173828" Y="-0.96836730957" />
                  <Point X="3.114676025391" Y="-0.975389282227" />
                  <Point X="3.086849609375" Y="-0.992281494141" />
                  <Point X="3.074124267578" Y="-1.001530273438" />
                  <Point X="3.050374267578" Y="-1.022000976562" />
                  <Point X="3.039349609375" Y="-1.033222900391" />
                  <Point X="2.942080322266" Y="-1.150207519531" />
                  <Point X="2.921326416016" Y="-1.176847045898" />
                  <Point X="2.906605712891" Y="-1.201229125977" />
                  <Point X="2.9001640625" Y="-1.213975219727" />
                  <Point X="2.888820800781" Y="-1.241360107422" />
                  <Point X="2.884362792969" Y="-1.254927856445" />
                  <Point X="2.877531005859" Y="-1.28257800293" />
                  <Point X="2.875157226562" Y="-1.29666027832" />
                  <Point X="2.861216308594" Y="-1.44816027832" />
                  <Point X="2.859288818359" Y="-1.483321289062" />
                  <Point X="2.861607666016" Y="-1.514590087891" />
                  <Point X="2.864065917969" Y="-1.530127929688" />
                  <Point X="2.871797607422" Y="-1.561749267578" />
                  <Point X="2.876786376953" Y="-1.576668701172" />
                  <Point X="2.889157714844" Y="-1.605479736328" />
                  <Point X="2.896540283203" Y="-1.619371337891" />
                  <Point X="2.985598632812" Y="-1.757895874023" />
                  <Point X="2.997020507812" Y="-1.775661987305" />
                  <Point X="3.001741699219" Y="-1.782352783203" />
                  <Point X="3.019792724609" Y="-1.804448852539" />
                  <Point X="3.043488769531" Y="-1.828119873047" />
                  <Point X="3.052796142578" Y="-1.836277954102" />
                  <Point X="3.189939208984" Y="-1.941511474609" />
                  <Point X="4.087170410156" Y="-2.629981445312" />
                  <Point X="4.059846679688" Y="-2.6741953125" />
                  <Point X="4.045485351562" Y="-2.697434082031" />
                  <Point X="4.001274414063" Y="-2.760251953125" />
                  <Point X="2.848454589844" Y="-2.094670654297" />
                  <Point X="2.841192626953" Y="-2.090885498047" />
                  <Point X="2.815025390625" Y="-2.079512695312" />
                  <Point X="2.783118408203" Y="-2.069325683594" />
                  <Point X="2.771108154297" Y="-2.066337646484" />
                  <Point X="2.579581298828" Y="-2.031747924805" />
                  <Point X="2.555017822266" Y="-2.027311889648" />
                  <Point X="2.539358886719" Y="-2.025807495117" />
                  <Point X="2.508006591797" Y="-2.025403198242" />
                  <Point X="2.492313232422" Y="-2.026503540039" />
                  <Point X="2.460140625" Y="-2.031461303711" />
                  <Point X="2.444844482422" Y="-2.035136230469" />
                  <Point X="2.415068603516" Y="-2.044959960938" />
                  <Point X="2.400588867188" Y="-2.051108642578" />
                  <Point X="2.241477050781" Y="-2.134847900391" />
                  <Point X="2.221070800781" Y="-2.145587646484" />
                  <Point X="2.208968505859" Y="-2.153170166016" />
                  <Point X="2.186037353516" Y="-2.170063476562" />
                  <Point X="2.175208496094" Y="-2.179374267578" />
                  <Point X="2.154248779297" Y="-2.200334228516" />
                  <Point X="2.144938476562" Y="-2.211162841797" />
                  <Point X="2.128045898438" Y="-2.234093261719" />
                  <Point X="2.120463623047" Y="-2.246195068359" />
                  <Point X="2.036724487305" Y="-2.405306640625" />
                  <Point X="2.025984741211" Y="-2.425713134766" />
                  <Point X="2.01983605957" Y="-2.440192871094" />
                  <Point X="2.010012329102" Y="-2.469968505859" />
                  <Point X="2.006337280273" Y="-2.485264404297" />
                  <Point X="2.001379516602" Y="-2.517436767578" />
                  <Point X="2.000279174805" Y="-2.533130371094" />
                  <Point X="2.00068347168" Y="-2.564482910156" />
                  <Point X="2.002187866211" Y="-2.580141845703" />
                  <Point X="2.03677746582" Y="-2.771668457031" />
                  <Point X="2.041213500977" Y="-2.796232177734" />
                  <Point X="2.043014648438" Y="-2.804220214844" />
                  <Point X="2.051236083984" Y="-2.831542480469" />
                  <Point X="2.064069824219" Y="-2.862479736328" />
                  <Point X="2.069546630859" Y="-2.873578613281" />
                  <Point X="2.157674804687" Y="-3.026220947266" />
                  <Point X="2.735893066406" Y="-4.027724609375" />
                  <Point X="2.72375390625" Y="-4.036083740234" />
                  <Point X="1.833914794922" Y="-2.876422851562" />
                  <Point X="1.828654052734" Y="-2.870146728516" />
                  <Point X="1.80883190918" Y="-2.849626953125" />
                  <Point X="1.783252197266" Y="-2.828004638672" />
                  <Point X="1.773299072266" Y="-2.820647216797" />
                  <Point X="1.584401855469" Y="-2.699203857422" />
                  <Point X="1.56017565918" Y="-2.683628417969" />
                  <Point X="1.546283813477" Y="-2.676245849609" />
                  <Point X="1.51747265625" Y="-2.663874511719" />
                  <Point X="1.502553222656" Y="-2.658885742188" />
                  <Point X="1.470931884766" Y="-2.651154052734" />
                  <Point X="1.455394042969" Y="-2.648695800781" />
                  <Point X="1.424125244141" Y="-2.646376953125" />
                  <Point X="1.408394287109" Y="-2.646516357422" />
                  <Point X="1.201803222656" Y="-2.665527099609" />
                  <Point X="1.175307373047" Y="-2.667965332031" />
                  <Point X="1.161224975586" Y="-2.670339111328" />
                  <Point X="1.133575317383" Y="-2.677170898438" />
                  <Point X="1.120008056641" Y="-2.68162890625" />
                  <Point X="1.092622924805" Y="-2.692972167969" />
                  <Point X="1.079877197266" Y="-2.699413330078" />
                  <Point X="1.055495239258" Y="-2.714133789062" />
                  <Point X="1.043858642578" Y="-2.722413085938" />
                  <Point X="0.884334350586" Y="-2.855052246094" />
                  <Point X="0.863874938965" Y="-2.872063476562" />
                  <Point X="0.852652832031" Y="-2.883088378906" />
                  <Point X="0.832182006836" Y="-2.906838623047" />
                  <Point X="0.822933166504" Y="-2.919563964844" />
                  <Point X="0.80604083252" Y="-2.947390625" />
                  <Point X="0.799018859863" Y="-2.961468017578" />
                  <Point X="0.787394348145" Y="-2.990588378906" />
                  <Point X="0.782791748047" Y="-3.005631347656" />
                  <Point X="0.735094970703" Y="-3.225074951172" />
                  <Point X="0.728977478027" Y="-3.253219238281" />
                  <Point X="0.727584594727" Y="-3.261289794922" />
                  <Point X="0.724724487305" Y="-3.289677734375" />
                  <Point X="0.724742370605" Y="-3.323170410156" />
                  <Point X="0.725554992676" Y="-3.335520019531" />
                  <Point X="0.750529907227" Y="-3.525222412109" />
                  <Point X="0.833091552734" Y="-4.152340332031" />
                  <Point X="0.655064941406" Y="-3.487936767578" />
                  <Point X="0.652606445312" Y="-3.480125" />
                  <Point X="0.642145141602" Y="-3.453580566406" />
                  <Point X="0.62678704834" Y="-3.423815917969" />
                  <Point X="0.620407592773" Y="-3.413210205078" />
                  <Point X="0.475291290283" Y="-3.204125244141" />
                  <Point X="0.456679840088" Y="-3.177309814453" />
                  <Point X="0.446670837402" Y="-3.165173095703" />
                  <Point X="0.424787139893" Y="-3.142717773438" />
                  <Point X="0.41291217041" Y="-3.132399169922" />
                  <Point X="0.386656890869" Y="-3.113155273438" />
                  <Point X="0.373242950439" Y="-3.104938232422" />
                  <Point X="0.345241790771" Y="-3.090829833984" />
                  <Point X="0.330654754639" Y="-3.084938476562" />
                  <Point X="0.106095657349" Y="-3.015243652344" />
                  <Point X="0.077295509338" Y="-3.006305175781" />
                  <Point X="0.06337638092" Y="-3.003109130859" />
                  <Point X="0.035216796875" Y="-2.99883984375" />
                  <Point X="0.020976644516" Y="-2.997766601562" />
                  <Point X="-0.00866500473" Y="-2.997766601562" />
                  <Point X="-0.022905456543" Y="-2.998840087891" />
                  <Point X="-0.051064739227" Y="-3.003109375" />
                  <Point X="-0.06498387146" Y="-3.006305175781" />
                  <Point X="-0.289542663574" Y="-3.075999755859" />
                  <Point X="-0.318342956543" Y="-3.084938476562" />
                  <Point X="-0.332930603027" Y="-3.090829833984" />
                  <Point X="-0.360932647705" Y="-3.104938964844" />
                  <Point X="-0.374347198486" Y="-3.113156494141" />
                  <Point X="-0.400602172852" Y="-3.132400634766" />
                  <Point X="-0.4124765625" Y="-3.142718994141" />
                  <Point X="-0.434359344482" Y="-3.165173583984" />
                  <Point X="-0.444368041992" Y="-3.177310058594" />
                  <Point X="-0.589484191895" Y="-3.386394775391" />
                  <Point X="-0.60809564209" Y="-3.413210449219" />
                  <Point X="-0.61247052002" Y="-3.4201328125" />
                  <Point X="-0.625976318359" Y="-3.445264404297" />
                  <Point X="-0.638777648926" Y="-3.476215332031" />
                  <Point X="-0.642753173828" Y="-3.487936523438" />
                  <Point X="-0.688288391113" Y="-3.657876708984" />
                  <Point X="-0.985425231934" Y="-4.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.667063726524" Y="-3.962203660832" />
                  <Point X="2.686717455773" Y="-3.942549931583" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.917425602077" Y="-2.711841785279" />
                  <Point X="4.037443079252" Y="-2.591824308104" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.608732357283" Y="-3.886184741647" />
                  <Point X="2.63754184514" Y="-3.85737525379" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.832250953907" Y="-2.662666145023" />
                  <Point X="3.961424171781" Y="-2.53349292715" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.550400988042" Y="-3.810165822463" />
                  <Point X="2.588366234507" Y="-3.772200575998" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.747076305738" Y="-2.613490504767" />
                  <Point X="3.885405264309" Y="-2.475161546195" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.492069618801" Y="-3.734146903278" />
                  <Point X="2.539190623874" Y="-3.687025898206" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.661901657569" Y="-2.564314864511" />
                  <Point X="3.809386356838" Y="-2.416830165241" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.43373824956" Y="-3.658127984094" />
                  <Point X="2.490015013241" Y="-3.601851220413" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.576727009399" Y="-2.515139224255" />
                  <Point X="3.733367449367" Y="-2.358498784287" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.375406880319" Y="-3.582109064909" />
                  <Point X="2.440839402608" Y="-3.516676542621" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.49155236123" Y="-2.465963583999" />
                  <Point X="3.657348541896" Y="-2.300167403333" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.317075511079" Y="-3.506090145725" />
                  <Point X="2.391663791974" Y="-3.431501864829" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.40637771306" Y="-2.416787943743" />
                  <Point X="3.581329634425" Y="-2.241836022379" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.258744141838" Y="-3.43007122654" />
                  <Point X="2.342488181341" Y="-3.346327187036" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.321203064891" Y="-2.367612303487" />
                  <Point X="3.505310726953" Y="-2.183504641424" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.623347330166" Y="-1.065468038211" />
                  <Point X="4.762296764182" Y="-0.926518604195" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.200412772597" Y="-3.354052307355" />
                  <Point X="2.293312570708" Y="-3.261152509244" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.236028416722" Y="-2.318436663231" />
                  <Point X="3.429291819482" Y="-2.12517326047" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.504626886954" Y="-1.049838192998" />
                  <Point X="4.779228073764" Y="-0.775237006188" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.142081403356" Y="-3.278033388171" />
                  <Point X="2.244136960075" Y="-3.175977831452" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.150853768552" Y="-2.269261022975" />
                  <Point X="3.353272912011" Y="-2.066841879516" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.385906443742" Y="-1.034208347785" />
                  <Point X="4.673269345868" Y="-0.746845445659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.083750034115" Y="-3.202014468986" />
                  <Point X="2.194961349442" Y="-3.090803153659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.065679120383" Y="-2.220085382719" />
                  <Point X="3.27725400454" Y="-2.008510498562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.26718600053" Y="-1.018578502571" />
                  <Point X="4.567310617972" Y="-0.71845388513" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.025418664874" Y="-3.125995549802" />
                  <Point X="2.145785729944" Y="-3.005628484732" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.980504472213" Y="-2.170909742463" />
                  <Point X="3.201235097069" Y="-1.950179117607" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.148465557318" Y="-1.002948657358" />
                  <Point X="4.461351890076" Y="-0.6900623246" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.967087295633" Y="-3.049976630617" />
                  <Point X="2.096610082645" Y="-2.920453843605" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.895329824044" Y="-2.121734102206" />
                  <Point X="3.125216164329" Y="-1.891847761922" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.029745114106" Y="-0.987318812144" />
                  <Point X="4.35539316218" Y="-0.661670764071" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.811384612563" Y="-4.071329025262" />
                  <Point X="0.821141670134" Y="-4.061571967691" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.908755926392" Y="-2.973957711433" />
                  <Point X="2.051221066131" Y="-2.831492571694" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.806062533434" Y="-2.076651104391" />
                  <Point X="3.049406642198" Y="-1.833306995627" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.911024670894" Y="-0.971688966931" />
                  <Point X="4.249434434284" Y="-0.633279203541" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.782993029181" Y="-3.965370320219" />
                  <Point X="0.805511826149" Y="-3.942851523251" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.850424557152" Y="-2.897938792248" />
                  <Point X="2.02758647438" Y="-2.720776875019" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.695652909017" Y="-2.052710440383" />
                  <Point X="2.987503913527" Y="-1.760859435873" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.792304227682" Y="-0.956059121718" />
                  <Point X="4.143475706388" Y="-0.604887643012" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.754601445799" Y="-3.859411615176" />
                  <Point X="0.789881982164" Y="-3.82413107881" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.784745850141" Y="-2.829267210833" />
                  <Point X="2.007034552584" Y="-2.60697850839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.581854581632" Y="-2.032158479342" />
                  <Point X="2.934929603655" Y="-1.679083457319" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.67358378447" Y="-0.940429276504" />
                  <Point X="4.037516978492" Y="-0.576496082483" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.726209862416" Y="-3.753452910132" />
                  <Point X="0.774252138179" Y="-3.70541063437" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.703737307377" Y="-2.775925465172" />
                  <Point X="2.010168933672" Y="-2.469493838876" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.444369999528" Y="-2.03529277302" />
                  <Point X="2.884659265742" Y="-1.595003506807" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.554863341258" Y="-0.924799431291" />
                  <Point X="3.931558250596" Y="-0.548104521953" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.697818279034" Y="-3.647494205089" />
                  <Point X="0.758622294194" Y="-3.586690189929" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.621961356992" Y="-2.723351127132" />
                  <Point X="2.85947538695" Y="-1.485837097173" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.436117262645" Y="-0.909195221478" />
                  <Point X="3.825599528024" Y="-0.519712956099" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.669426695652" Y="-3.541535500046" />
                  <Point X="0.74299241841" Y="-3.467969777288" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.538191245975" Y="-2.672770949723" />
                  <Point X="2.871190109921" Y="-1.339772085777" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.282679446495" Y="-0.928282749203" />
                  <Point X="3.71964083993" Y="-0.491321355768" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.635639518866" Y="-3.440972388406" />
                  <Point X="0.727362508486" Y="-3.349249398787" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.429813147673" Y="-2.646798759599" />
                  <Point X="3.019420484407" Y="-1.057191422865" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.077628537339" Y="-0.998983369933" />
                  <Point X="3.623060940279" Y="-0.453550966994" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.582978954664" Y="-3.359282664183" />
                  <Point X="0.740068386289" Y="-3.202193232558" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.284328597038" Y="-2.657933021809" />
                  <Point X="3.538113579189" Y="-0.404148039658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.609445132389" Y="0.667183513542" />
                  <Point X="4.766668971566" Y="0.824407352719" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.527935624479" Y="-3.279975705943" />
                  <Point X="0.777379560742" Y="-3.030531769679" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.12935305792" Y="-2.678558272501" />
                  <Point X="3.467747978625" Y="-0.340163351797" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.425919272337" Y="0.618007941916" />
                  <Point X="4.746704441109" Y="0.938793110687" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.074850776732" Y="-4.748411818729" />
                  <Point X="-0.945960245722" Y="-4.619521287718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.472892287623" Y="-3.200668754373" />
                  <Point X="3.409318379132" Y="-0.264242662864" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.242393412285" Y="0.568832370289" />
                  <Point X="4.720400948348" Y="1.046839906352" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.133533558776" Y="-4.672744312347" />
                  <Point X="-0.89678463593" Y="-4.435995389501" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.409391829499" Y="-3.129818924072" />
                  <Point X="3.369891816152" Y="-0.169318937418" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.058867552233" Y="0.519656798662" />
                  <Point X="4.642317538451" Y="1.10310678488" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.120586189091" Y="-4.525446654236" />
                  <Point X="-0.847609026138" Y="-4.252469491283" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.322464072779" Y="-3.082396392366" />
                  <Point X="3.349803328316" Y="-0.055057136829" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.875341692181" Y="0.470481227036" />
                  <Point X="4.487598039269" Y="1.082737574124" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.140563317863" Y="-4.411073494583" />
                  <Point X="-0.798433416346" Y="-4.068943593066" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.219935007628" Y="-3.050575169092" />
                  <Point X="3.367863848758" Y="0.097353672038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.691806864249" Y="0.421296687529" />
                  <Point X="4.332878540087" Y="1.062368363367" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.16285350003" Y="-4.299013388324" />
                  <Point X="-0.749257806554" Y="-3.885417694848" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.117405942476" Y="-3.018753945818" />
                  <Point X="4.178159040905" Y="1.041999152611" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.185578062369" Y="-4.187387662238" />
                  <Point X="-0.700082196762" Y="-3.701891796631" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.004042998306" Y="-2.997766601563" />
                  <Point X="4.023439541723" Y="1.021629941854" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.229276861133" Y="-4.096736172576" />
                  <Point X="-0.650906712843" Y="-3.518366024286" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.172086472414" Y="-3.039545783858" />
                  <Point X="3.868720042541" Y="1.001260731097" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.29599046504" Y="-4.029099488058" />
                  <Point X="-0.44474690507" Y="-3.177855928088" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.395694233302" Y="-3.12880325632" />
                  <Point X="3.714000543359" Y="0.980891520341" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.367568395565" Y="-3.966327130157" />
                  <Point X="3.559280979286" Y="0.960522244694" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.43914632609" Y="-3.903554772257" />
                  <Point X="3.419627315481" Y="0.955218869314" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.512391804743" Y="-3.842449962485" />
                  <Point X="3.304986758956" Y="0.974928601215" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.606278756934" Y="-3.80198662625" />
                  <Point X="3.214114538" Y="1.018406668684" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.728784011992" Y="-3.790141592883" />
                  <Point X="3.135772312934" Y="1.074414732044" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.854870112908" Y="-3.781877405373" />
                  <Point X="3.078194445049" Y="1.151187152584" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.985829371341" Y="-3.77848637538" />
                  <Point X="3.031625292609" Y="1.238968288569" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.503801606851" Y="-4.162108322466" />
                  <Point X="-2.500001165264" Y="-4.158307880879" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.282491130361" Y="-3.940797845975" />
                  <Point X="3.005756141166" Y="1.347449425552" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.58677613421" Y="-4.110732561399" />
                  <Point X="3.015974439805" Y="1.492018012616" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.580496710187" Y="2.056540282998" />
                  <Point X="4.088079864434" Y="2.564123437245" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.669750661568" Y="-4.059356800332" />
                  <Point X="4.036712309533" Y="2.64710617077" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.752287228975" Y="-4.007543079313" />
                  <Point X="3.980493485346" Y="2.725237635008" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.828192793698" Y="-3.949098355611" />
                  <Point X="3.849199399021" Y="2.728293837109" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.904098358422" Y="-3.890653631909" />
                  <Point X="3.531323337886" Y="2.544768064398" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.980003923145" Y="-3.832208908207" />
                  <Point X="3.21344727675" Y="2.361242291688" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.799413424768" Y="-3.517268121404" />
                  <Point X="2.895571215614" Y="2.177716518978" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.615887464292" Y="-3.199391872503" />
                  <Point X="2.592690267812" Y="2.009185859601" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.432361503816" Y="-2.881515623601" />
                  <Point X="2.424735965697" Y="1.975581845912" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.312575917431" Y="-2.627379748791" />
                  <Point X="2.306126511537" Y="1.991322680177" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.330594691175" Y="-2.511048234109" />
                  <Point X="2.2154278531" Y="2.034974310166" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.383970345402" Y="-2.430073599911" />
                  <Point X="2.138527176527" Y="2.092423922018" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.462002914908" Y="-2.373755880992" />
                  <Point X="2.079417462102" Y="2.167664496018" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.571329521723" Y="-2.348732199382" />
                  <Point X="2.032806815357" Y="2.255404137699" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.803065681964" Y="-2.446118071197" />
                  <Point X="2.013369262354" Y="2.370316873121" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.120941786393" Y="-2.6296438872" />
                  <Point X="2.0383818703" Y="2.529679769492" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.438817856488" Y="-2.81316966887" />
                  <Point X="2.191472947601" Y="2.817121135219" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.756693926583" Y="-2.996695450539" />
                  <Point X="2.37499888414" Y="3.134997360184" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.844506285416" Y="-2.950157520947" />
                  <Point X="2.55852482068" Y="3.45287358515" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.902571195768" Y="-2.873872142874" />
                  <Point X="2.74205075722" Y="3.770749810115" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.960636106121" Y="-2.797586764801" />
                  <Point X="2.771663218545" Y="3.934712559865" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.016708985391" Y="-2.719309355646" />
                  <Point X="2.693148443291" Y="3.990548073037" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.066898672273" Y="-2.635148754103" />
                  <Point X="2.61343123633" Y="4.0451811545" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.117088359156" Y="-2.55098815256" />
                  <Point X="-3.442925759908" Y="-1.876825553312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.950197552946" Y="-1.38409734635" />
                  <Point X="2.528539086357" Y="4.094639292953" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.167278046038" Y="-2.466827551017" />
                  <Point X="-4.020346807557" Y="-2.319896312535" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.965017415804" Y="-1.264566920782" />
                  <Point X="2.443647109337" Y="4.144097604359" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.019291383924" Y="-1.184490600477" />
                  <Point X="2.358755132318" Y="4.193555915765" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.097569592856" Y="-1.128418520984" />
                  <Point X="2.273863155298" Y="4.243014227171" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.20380726243" Y="-1.100305902132" />
                  <Point X="2.184156994008" Y="4.287658354306" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.352257229973" Y="-1.114405581249" />
                  <Point X="2.092616909756" Y="4.330468558479" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.506976751451" Y="-1.134774814302" />
                  <Point X="2.001076825504" Y="4.373278762653" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.661696231444" Y="-1.155144005869" />
                  <Point X="1.909536741252" Y="4.416088966826" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.816415711437" Y="-1.175513197437" />
                  <Point X="1.814895407278" Y="4.455797921278" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.97113519143" Y="-1.195882389005" />
                  <Point X="1.716304711231" Y="4.491557513656" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.125854671423" Y="-1.216251580572" />
                  <Point X="1.617714015184" Y="4.527317106035" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.280574151416" Y="-1.23662077214" />
                  <Point X="1.519123319137" Y="4.563076698414" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.435293631409" Y="-1.256989963707" />
                  <Point X="-3.479911727072" Y="-0.301608059371" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.315407689561" Y="-0.13710402186" />
                  <Point X="1.417302699315" Y="4.595606367017" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.590013111402" Y="-1.277359155275" />
                  <Point X="-3.667480846263" Y="-0.354826890136" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.299111035571" Y="0.013542920556" />
                  <Point X="1.309080566579" Y="4.621734522706" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.675728229622" Y="-1.228723985069" />
                  <Point X="-3.851006742048" Y="-0.404002497496" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.333935845022" Y="0.113068399531" />
                  <Point X="1.200858435431" Y="4.647862679983" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.703063109756" Y="-1.121708576779" />
                  <Point X="-4.034532637834" Y="-0.453178104856" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.394768125513" Y="0.186586407465" />
                  <Point X="1.092636304282" Y="4.67399083726" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.730397980782" Y="-1.014693159379" />
                  <Point X="-4.218058533619" Y="-0.502353712216" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.477496792584" Y="0.238208028819" />
                  <Point X="0.984414173134" Y="4.700118994538" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.751198650759" Y="-0.90114354093" />
                  <Point X="-4.401584429405" Y="-0.551529319576" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.580969022155" Y="0.269086087674" />
                  <Point X="0.876192041986" Y="4.726247151815" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.76800943904" Y="-0.783604040786" />
                  <Point X="-4.58511032519" Y="-0.600704926936" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.686927811519" Y="0.297477586735" />
                  <Point X="0.145275022735" Y="4.129680420989" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.195298676488" Y="4.179704074742" />
                  <Point X="0.760556912487" Y="4.744962310742" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.784820227322" Y="-0.666064540642" />
                  <Point X="-4.768636220975" Y="-0.649880534296" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.792886558672" Y="0.325869128007" />
                  <Point X="-0.030195635789" Y="4.08856005089" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.272752858753" Y="4.391508545433" />
                  <Point X="0.638942871264" Y="4.757698557944" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.898845305826" Y="0.354260669279" />
                  <Point X="-0.127566489803" Y="4.125539485302" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.321928419005" Y="4.57503439411" />
                  <Point X="0.51732883004" Y="4.770434805145" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.004804052979" Y="0.382652210551" />
                  <Point X="-0.192906251663" Y="4.194550011867" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.371103979256" Y="4.758560242787" />
                  <Point X="0.395714788817" Y="4.783171052347" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.110762800133" Y="0.411043751823" />
                  <Point X="-0.232938587623" Y="4.288867964333" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.216721547286" Y="0.439435293095" />
                  <Point X="-0.261330133555" Y="4.394826706826" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.32268029444" Y="0.467826834367" />
                  <Point X="-0.289721679488" Y="4.500785449319" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.428639041593" Y="0.496218375639" />
                  <Point X="-3.718567240921" Y="1.206290176311" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.422318539977" Y="1.502538877256" />
                  <Point X="-0.31811322542" Y="4.606744191812" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.534597788747" Y="0.524609916911" />
                  <Point X="-3.844814511379" Y="1.214393194279" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.441238241604" Y="1.617969464054" />
                  <Point X="-0.346504771353" Y="4.712702934305" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.6405565359" Y="0.553001458183" />
                  <Point X="-3.963534930628" Y="1.230023063455" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.487394875952" Y="1.706163118131" />
                  <Point X="-0.417068743467" Y="4.776489250616" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.746515283054" Y="0.581392999455" />
                  <Point X="-4.082255349877" Y="1.245652932632" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.552024811066" Y="1.775883471443" />
                  <Point X="-0.569226892419" Y="4.75868139009" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.770685956552" Y="0.691572614382" />
                  <Point X="-4.200975769126" Y="1.261282801808" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.628043727263" Y="1.834214843671" />
                  <Point X="-0.72138504137" Y="4.740873529564" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.747352537975" Y="0.849256321384" />
                  <Point X="-4.319696188376" Y="1.276912670984" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.704062601856" Y="1.892546257503" />
                  <Point X="-0.873543190322" Y="4.723065669038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.715683361072" Y="1.015275786713" />
                  <Point X="-4.438416607625" Y="1.29254254016" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.780081467863" Y="1.950877679922" />
                  <Point X="-3.00283132782" Y="2.728127819965" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.753251787022" Y="2.977707360763" />
                  <Point X="-1.643556112471" Y="4.087403035314" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.512633119464" Y="4.218326028321" />
                  <Point X="-1.047154516566" Y="4.683804631219" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.665744376301" Y="1.19956505991" />
                  <Point X="-4.557137026874" Y="1.308172409337" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.856100333869" Y="2.009209102341" />
                  <Point X="-3.135834732197" Y="2.729474704013" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.754598711509" Y="3.110710724701" />
                  <Point X="-1.836378572751" Y="4.02893086346" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.462613578082" Y="4.402695858129" />
                  <Point X="-1.233847016111" Y="4.631462420099" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.932119199875" Y="2.067540524761" />
                  <Point X="-3.242852311226" Y="2.75680741341" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.781931436848" Y="3.217728287788" />
                  <Point X="-1.943759266969" Y="4.055900457667" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.474349560757" Y="4.525310163879" />
                  <Point X="-1.420539515656" Y="4.57912020898" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.008138065882" Y="2.12587194718" />
                  <Point X="-3.328620204633" Y="2.805389808428" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.830513902128" Y="3.303496110933" />
                  <Point X="-2.032338305927" Y="4.101671707134" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.084156931888" Y="2.184203369599" />
                  <Point X="-3.413794867572" Y="2.854565433915" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.879689524139" Y="3.388670777348" />
                  <Point X="-2.108148406596" Y="4.16021189489" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.160175797894" Y="2.242534792018" />
                  <Point X="-3.49896953051" Y="2.903741059403" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.92886514615" Y="3.473845443763" />
                  <Point X="-2.167294867207" Y="4.235415722705" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.206353045583" Y="2.330707832754" />
                  <Point X="-3.584144193448" Y="2.95291668489" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.97804076816" Y="3.559020110178" />
                  <Point X="-2.36407290663" Y="4.172987971707" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.017987634877" Y="2.653423531886" />
                  <Point X="-3.669318856386" Y="3.002092310377" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.027216390171" Y="3.644194776592" />
                  <Point X="-2.682668262984" Y="3.988742903779" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.0001640625" Y="0.001626220703" />
                  <Width Value="9.996463867188" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.826585021973" Y="-4.862161621094" />
                  <Point X="0.471539031982" Y="-3.537112304688" />
                  <Point X="0.464318664551" Y="-3.521544189453" />
                  <Point X="0.319202453613" Y="-3.312459228516" />
                  <Point X="0.300591033936" Y="-3.285643798828" />
                  <Point X="0.274335754395" Y="-3.266399902344" />
                  <Point X="0.049776805878" Y="-3.196705078125" />
                  <Point X="0.020976625443" Y="-3.187766601562" />
                  <Point X="-0.008665060997" Y="-3.187766601562" />
                  <Point X="-0.233224014282" Y="-3.257461181641" />
                  <Point X="-0.262024200439" Y="-3.266399902344" />
                  <Point X="-0.288279144287" Y="-3.285644042969" />
                  <Point X="-0.433395355225" Y="-3.494728759766" />
                  <Point X="-0.45200680542" Y="-3.521544433594" />
                  <Point X="-0.459227142334" Y="-3.537112304688" />
                  <Point X="-0.504762542725" Y="-3.707052490234" />
                  <Point X="-0.84774395752" Y="-4.987077148438" />
                  <Point X="-1.067647216797" Y="-4.944393066406" />
                  <Point X="-1.100245361328" Y="-4.938065917969" />
                  <Point X="-1.351589599609" Y="-4.873396972656" />
                  <Point X="-1.309150146484" Y="-4.551038574219" />
                  <Point X="-1.309683227539" Y="-4.5347578125" />
                  <Point X="-1.363330322266" Y="-4.265056152344" />
                  <Point X="-1.370210693359" Y="-4.230466308594" />
                  <Point X="-1.386282714844" Y="-4.20262890625" />
                  <Point X="-1.593028076172" Y="-4.021317382812" />
                  <Point X="-1.619543457031" Y="-3.998063964844" />
                  <Point X="-1.649240966797" Y="-3.985762939453" />
                  <Point X="-1.923638183594" Y="-3.967777832031" />
                  <Point X="-1.958830078125" Y="-3.965471435547" />
                  <Point X="-1.989878540039" Y="-3.973791015625" />
                  <Point X="-2.218520996094" Y="-4.126565429688" />
                  <Point X="-2.247844970703" Y="-4.146159179688" />
                  <Point X="-2.259734130859" Y="-4.157294433594" />
                  <Point X="-2.285298583984" Y="-4.190610839844" />
                  <Point X="-2.457095214844" Y="-4.414500488281" />
                  <Point X="-2.8080546875" Y="-4.197194824219" />
                  <Point X="-2.855831054688" Y="-4.16761328125" />
                  <Point X="-3.21672265625" Y="-3.889738769531" />
                  <Point X="-3.228581054688" Y="-3.880608154297" />
                  <Point X="-3.165846435547" Y="-3.771948730469" />
                  <Point X="-2.506033691406" Y="-2.629119628906" />
                  <Point X="-2.499762695312" Y="-2.597593261719" />
                  <Point X="-2.513979736328" Y="-2.568764404297" />
                  <Point X="-2.529356201172" Y="-2.553387939453" />
                  <Point X="-2.531328369141" Y="-2.551415771484" />
                  <Point X="-2.560157226562" Y="-2.537198730469" />
                  <Point X="-2.59168359375" Y="-2.543469726562" />
                  <Point X="-2.738253662109" Y="-2.628092041016" />
                  <Point X="-3.842959228516" Y="-3.265894042969" />
                  <Point X="-4.123955078125" Y="-2.896723388672" />
                  <Point X="-4.161703613281" Y="-2.847129394531" />
                  <Point X="-4.420433105469" Y="-2.413279785156" />
                  <Point X="-4.431020019531" Y="-2.395527099609" />
                  <Point X="-4.318245117188" Y="-2.308991699219" />
                  <Point X="-3.163786865234" Y="-1.423144897461" />
                  <Point X="-3.145822509766" Y="-1.396015869141" />
                  <Point X="-3.138993896484" Y="-1.369650024414" />
                  <Point X="-3.138117919922" Y="-1.366268554688" />
                  <Point X="-3.140325439453" Y="-1.33459753418" />
                  <Point X="-3.161157470703" Y="-1.310640136719" />
                  <Point X="-3.184629882812" Y="-1.296825439453" />
                  <Point X="-3.187640136719" Y="-1.295053588867" />
                  <Point X="-3.219529296875" Y="-1.288571044922" />
                  <Point X="-3.404560302734" Y="-1.312930908203" />
                  <Point X="-4.803283691406" Y="-1.497076293945" />
                  <Point X="-4.91262890625" Y="-1.068992675781" />
                  <Point X="-4.927392089844" Y="-1.011195495605" />
                  <Point X="-4.995846191406" Y="-0.532571838379" />
                  <Point X="-4.998395996094" Y="-0.51474206543" />
                  <Point X="-4.872565917969" Y="-0.481025878906" />
                  <Point X="-3.557463867188" Y="-0.128645431519" />
                  <Point X="-3.541895996094" Y="-0.121425079346" />
                  <Point X="-3.517297851562" Y="-0.104352607727" />
                  <Point X="-3.514143066406" Y="-0.102163047791" />
                  <Point X="-3.494898681641" Y="-0.075906707764" />
                  <Point X="-3.48669921875" Y="-0.049488063812" />
                  <Point X="-3.485647705078" Y="-0.046099822998" />
                  <Point X="-3.485647705078" Y="-0.016460268021" />
                  <Point X="-3.493847167969" Y="0.009958530426" />
                  <Point X="-3.494898681641" Y="0.013346768379" />
                  <Point X="-3.514143066406" Y="0.039602954865" />
                  <Point X="-3.538741210938" Y="0.056675422668" />
                  <Point X="-3.557463867188" Y="0.066085334778" />
                  <Point X="-3.726128417969" Y="0.111278785706" />
                  <Point X="-4.998186523438" Y="0.452125793457" />
                  <Point X="-4.927159179688" Y="0.932121582031" />
                  <Point X="-4.917645019531" Y="0.996418457031" />
                  <Point X="-4.779851074219" Y="1.504920043945" />
                  <Point X="-4.773516113281" Y="1.528298583984" />
                  <Point X="-4.695963378906" Y="1.518088623047" />
                  <Point X="-3.753266357422" Y="1.393980102539" />
                  <Point X="-3.731704833984" Y="1.395866577148" />
                  <Point X="-3.677261230469" Y="1.413032592773" />
                  <Point X="-3.670278808594" Y="1.415234130859" />
                  <Point X="-3.651534423828" Y="1.426056152344" />
                  <Point X="-3.639119873047" Y="1.443785888672" />
                  <Point X="-3.617274169922" Y="1.496526123047" />
                  <Point X="-3.614472412109" Y="1.503290161133" />
                  <Point X="-3.610714111328" Y="1.524605224609" />
                  <Point X="-3.616315917969" Y="1.54551159668" />
                  <Point X="-3.642675048828" Y="1.596147338867" />
                  <Point X="-3.646055664062" Y="1.602641235352" />
                  <Point X="-3.659968261719" Y="1.619221801758" />
                  <Point X="-3.756714599609" Y="1.693457885742" />
                  <Point X="-4.47610546875" Y="2.245466308594" />
                  <Point X="-4.196983886719" Y="2.723668945313" />
                  <Point X="-4.160014648438" Y="2.787006591797" />
                  <Point X="-3.795002441406" Y="3.256177978516" />
                  <Point X="-3.774671142578" Y="3.282310791016" />
                  <Point X="-3.738135253906" Y="3.261216796875" />
                  <Point X="-3.159156982422" Y="2.926943359375" />
                  <Point X="-3.138514892578" Y="2.920434814453" />
                  <Point X="-3.062690185547" Y="2.913801025391" />
                  <Point X="-3.052965576172" Y="2.912950195312" />
                  <Point X="-3.031506835938" Y="2.915775390625" />
                  <Point X="-3.013252441406" Y="2.927404785156" />
                  <Point X="-2.959431396484" Y="2.981225585938" />
                  <Point X="-2.952528808594" Y="2.988128173828" />
                  <Point X="-2.940899414062" Y="3.006382568359" />
                  <Point X="-2.93807421875" Y="3.027841308594" />
                  <Point X="-2.944708007812" Y="3.103666015625" />
                  <Point X="-2.945558837891" Y="3.113390869141" />
                  <Point X="-2.952067382812" Y="3.134032958984" />
                  <Point X="-2.994938720703" Y="3.208288085938" />
                  <Point X="-3.307278564453" Y="3.749276611328" />
                  <Point X="-2.817263183594" Y="4.124966796875" />
                  <Point X="-2.752876220703" Y="4.174331542969" />
                  <Point X="-2.177993164062" Y="4.493724121094" />
                  <Point X="-2.141548828125" Y="4.513971679688" />
                  <Point X="-1.967826660156" Y="4.287573242188" />
                  <Point X="-1.951247802734" Y="4.273661621094" />
                  <Point X="-1.86685534668" Y="4.229729492188" />
                  <Point X="-1.856031860352" Y="4.224094726562" />
                  <Point X="-1.83512512207" Y="4.2184921875" />
                  <Point X="-1.813809570312" Y="4.22225" />
                  <Point X="-1.725909057617" Y="4.25866015625" />
                  <Point X="-1.714635620117" Y="4.263329589844" />
                  <Point X="-1.696905639648" Y="4.275744140625" />
                  <Point X="-1.686083496094" Y="4.29448828125" />
                  <Point X="-1.657473510742" Y="4.385227539062" />
                  <Point X="-1.653804199219" Y="4.396864746094" />
                  <Point X="-1.651917602539" Y="4.418426757812" />
                  <Point X="-1.656791625977" Y="4.455448242188" />
                  <Point X="-1.689137573242" Y="4.701141113281" />
                  <Point X="-1.051447509766" Y="4.879927246094" />
                  <Point X="-0.968094177246" Y="4.903296386719" />
                  <Point X="-0.27116015625" Y="4.984862304688" />
                  <Point X="-0.224199661255" Y="4.990358398438" />
                  <Point X="-0.213418167114" Y="4.95012109375" />
                  <Point X="-0.042140380859" Y="4.310903320312" />
                  <Point X="-0.024282115936" Y="4.284176757813" />
                  <Point X="0.006155934334" Y="4.273844238281" />
                  <Point X="0.036594036102" Y="4.284176757813" />
                  <Point X="0.054452251434" Y="4.310903320312" />
                  <Point X="0.076419059753" Y="4.392884277344" />
                  <Point X="0.236648422241" Y="4.990868652344" />
                  <Point X="0.787423400879" Y="4.933187988281" />
                  <Point X="0.860205688477" Y="4.925565429688" />
                  <Point X="1.436796386719" Y="4.786358886719" />
                  <Point X="1.508456665039" Y="4.769057617188" />
                  <Point X="1.884566894531" Y="4.632639648438" />
                  <Point X="1.931038085938" Y="4.615784179688" />
                  <Point X="2.293934326172" Y="4.446069824219" />
                  <Point X="2.338695800781" Y="4.425136230469" />
                  <Point X="2.689271972656" Y="4.220889648438" />
                  <Point X="2.732523681641" Y="4.19569140625" />
                  <Point X="3.063163330078" Y="3.960558837891" />
                  <Point X="3.068740234375" Y="3.956592773438" />
                  <Point X="2.993537353516" Y="3.826337158203" />
                  <Point X="2.229853515625" Y="2.503597900391" />
                  <Point X="2.224852294922" Y="2.491515380859" />
                  <Point X="2.205823242188" Y="2.42035546875" />
                  <Point X="2.202044433594" Y="2.392326904297" />
                  <Point X="2.209464355469" Y="2.330793945312" />
                  <Point X="2.210415771484" Y="2.322902099609" />
                  <Point X="2.218682373047" Y="2.300811767578" />
                  <Point X="2.256756835938" Y="2.244699707031" />
                  <Point X="2.274940185547" Y="2.224203613281" />
                  <Point X="2.331052246094" Y="2.186129150391" />
                  <Point X="2.338248779297" Y="2.18124609375" />
                  <Point X="2.360336669922" Y="2.172980224609" />
                  <Point X="2.421869873047" Y="2.165560302734" />
                  <Point X="2.448663818359" Y="2.165946044922" />
                  <Point X="2.519823730469" Y="2.184975341797" />
                  <Point X="2.541033935547" Y="2.192417480469" />
                  <Point X="2.710677978516" Y="2.290361572266" />
                  <Point X="3.994248046875" Y="3.031430908203" />
                  <Point X="4.176936035156" Y="2.777535888672" />
                  <Point X="4.202594726563" Y="2.741875732422" />
                  <Point X="4.386918457031" Y="2.437279296875" />
                  <Point X="4.387513183594" Y="2.436296142578" />
                  <Point X="4.294077636719" Y="2.364600341797" />
                  <Point X="3.288616210938" Y="1.593082763672" />
                  <Point X="3.279371337891" Y="1.583833496094" />
                  <Point X="3.228157470703" Y="1.517020996094" />
                  <Point X="3.213119384766" Y="1.491499389648" />
                  <Point X="3.194041992188" Y="1.423283569336" />
                  <Point X="3.191595458984" Y="1.414534667969" />
                  <Point X="3.190779541016" Y="1.390965087891" />
                  <Point X="3.206439941406" Y="1.31506628418" />
                  <Point X="3.215646484375" Y="1.287954956055" />
                  <Point X="3.258241210938" Y="1.223212890625" />
                  <Point X="3.263704101562" Y="1.214909667969" />
                  <Point X="3.280948486328" Y="1.198819458008" />
                  <Point X="3.342674072266" Y="1.164073364258" />
                  <Point X="3.368565185547" Y="1.153619628906" />
                  <Point X="3.452022216797" Y="1.14258972168" />
                  <Point X="3.475803710938" Y="1.14117175293" />
                  <Point X="3.636954101562" Y="1.162387695312" />
                  <Point X="4.848975097656" Y="1.32195324707" />
                  <Point X="4.928169433594" Y="0.996646789551" />
                  <Point X="4.939188476562" Y="0.951385070801" />
                  <Point X="4.997274902344" Y="0.578304870605" />
                  <Point X="4.997858398438" Y="0.574556152344" />
                  <Point X="4.893447753906" Y="0.546579345703" />
                  <Point X="3.741167724609" Y="0.237826919556" />
                  <Point X="3.729086914062" Y="0.232819259644" />
                  <Point X="3.647092773438" Y="0.185425292969" />
                  <Point X="3.622265136719" Y="0.166926727295" />
                  <Point X="3.573068847656" Y="0.104238845825" />
                  <Point X="3.566759277344" Y="0.096199043274" />
                  <Point X="3.556985351562" Y="0.074735580444" />
                  <Point X="3.540586425781" Y="-0.010892963409" />
                  <Point X="3.538483154297" Y="-0.040685092926" />
                  <Point X="3.554882080078" Y="-0.12631363678" />
                  <Point X="3.556985351562" Y="-0.137295669556" />
                  <Point X="3.566759033203" Y="-0.158759140015" />
                  <Point X="3.615955322266" Y="-0.221446868896" />
                  <Point X="3.636576904297" Y="-0.241906967163" />
                  <Point X="3.718571044922" Y="-0.289300933838" />
                  <Point X="3.741167724609" Y="-0.300387023926" />
                  <Point X="3.888949951172" Y="-0.339985168457" />
                  <Point X="4.998067871094" Y="-0.637172363281" />
                  <Point X="4.954583984375" Y="-0.925591674805" />
                  <Point X="4.948431640625" Y="-0.966398864746" />
                  <Point X="4.874545898438" Y="-1.290178344727" />
                  <Point X="4.748303222656" Y="-1.273558227539" />
                  <Point X="3.411982177734" Y="-1.097628295898" />
                  <Point X="3.3948359375" Y="-1.098341308594" />
                  <Point X="3.233910644531" Y="-1.133319091797" />
                  <Point X="3.213271728516" Y="-1.137805053711" />
                  <Point X="3.1854453125" Y="-1.154697265625" />
                  <Point X="3.088176025391" Y="-1.271681884766" />
                  <Point X="3.075701171875" Y="-1.286685424805" />
                  <Point X="3.064357910156" Y="-1.3140703125" />
                  <Point X="3.050416992188" Y="-1.46557043457" />
                  <Point X="3.04862890625" Y="-1.485000610352" />
                  <Point X="3.056360595703" Y="-1.516621826172" />
                  <Point X="3.145418945312" Y="-1.655146362305" />
                  <Point X="3.156840820312" Y="-1.672912353516" />
                  <Point X="3.168460693359" Y="-1.685540649414" />
                  <Point X="3.305603759766" Y="-1.790774169922" />
                  <Point X="4.33907421875" Y="-2.583784179688" />
                  <Point X="4.221473632812" Y="-2.774079101562" />
                  <Point X="4.204130859375" Y="-2.802142822266" />
                  <Point X="4.056688232422" Y="-3.011638427734" />
                  <Point X="3.943628662109" Y="-2.94636328125" />
                  <Point X="2.753454589844" Y="-2.259215576172" />
                  <Point X="2.737340576172" Y="-2.253312744141" />
                  <Point X="2.545813720703" Y="-2.218723144531" />
                  <Point X="2.521250244141" Y="-2.214287109375" />
                  <Point X="2.489077636719" Y="-2.219244873047" />
                  <Point X="2.329965820312" Y="-2.302984130859" />
                  <Point X="2.309559570312" Y="-2.313723876953" />
                  <Point X="2.288599853516" Y="-2.334683837891" />
                  <Point X="2.204860595703" Y="-2.493795410156" />
                  <Point X="2.194120849609" Y="-2.514201904297" />
                  <Point X="2.189163085938" Y="-2.546374267578" />
                  <Point X="2.223752685547" Y="-2.737900878906" />
                  <Point X="2.228188720703" Y="-2.762464599609" />
                  <Point X="2.234091552734" Y="-2.778578613281" />
                  <Point X="2.322219726562" Y="-2.931220947266" />
                  <Point X="2.986673583984" Y="-4.082088623047" />
                  <Point X="2.855879150391" Y="-4.175512207031" />
                  <Point X="2.835304931641" Y="-4.190208007812" />
                  <Point X="2.679775634766" Y="-4.29087890625" />
                  <Point X="2.591390136719" Y="-4.175692871094" />
                  <Point X="1.683177612305" Y="-2.992087402344" />
                  <Point X="1.670549560547" Y="-2.980467529297" />
                  <Point X="1.48165234375" Y="-2.859024169922" />
                  <Point X="1.457426025391" Y="-2.843448730469" />
                  <Point X="1.4258046875" Y="-2.835717041016" />
                  <Point X="1.219213623047" Y="-2.854727783203" />
                  <Point X="1.192717773438" Y="-2.857166015625" />
                  <Point X="1.165332763672" Y="-2.868509277344" />
                  <Point X="1.005808410645" Y="-3.0011484375" />
                  <Point X="0.985349060059" Y="-3.018159667969" />
                  <Point X="0.968456726074" Y="-3.045986328125" />
                  <Point X="0.920759887695" Y="-3.265429931641" />
                  <Point X="0.91464251709" Y="-3.29357421875" />
                  <Point X="0.91392956543" Y="-3.310720214844" />
                  <Point X="0.93890435791" Y="-3.500422607422" />
                  <Point X="1.127642211914" Y="-4.934028808594" />
                  <Point X="1.013813537598" Y="-4.958979980469" />
                  <Point X="0.994350219727" Y="-4.96324609375" />
                  <Point X="0.860200378418" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#206" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.171277830952" Y="4.994342073515" Z="2.25" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="2.25" />
                  <Point X="-0.283128192046" Y="5.06580346622" Z="2.25" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="2.25" />
                  <Point X="-1.071101298622" Y="4.959350642157" Z="2.25" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="2.25" />
                  <Point X="-1.712519868411" Y="4.480202116458" Z="2.25" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="2.25" />
                  <Point X="-1.711315179565" Y="4.431543099596" Z="2.25" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="2.25" />
                  <Point X="-1.751198562558" Y="4.336134458792" Z="2.25" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="2.25" />
                  <Point X="-1.849922576037" Y="4.305358990207" Z="2.25" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="2.25" />
                  <Point X="-2.111557992981" Y="4.580278730407" Z="2.25" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="2.25" />
                  <Point X="-2.208432099115" Y="4.568711462616" Z="2.25" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="2.25" />
                  <Point X="-2.853841234747" Y="4.195926327517" Z="2.25" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="2.25" />
                  <Point X="-3.044395923203" Y="3.214568065397" Z="2.25" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="2.25" />
                  <Point X="-3.000673879805" Y="3.130588239903" Z="2.25" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="2.25" />
                  <Point X="-3.00094265274" Y="3.047860915146" Z="2.25" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="2.25" />
                  <Point X="-3.064488225704" Y="2.994890819368" Z="2.25" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="2.25" />
                  <Point X="-3.7192912829" Y="3.335798025543" Z="2.25" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="2.25" />
                  <Point X="-3.840621815225" Y="3.318160495151" Z="2.25" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="2.25" />
                  <Point X="-4.246321928298" Y="2.780154074928" Z="2.25" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="2.25" />
                  <Point X="-3.793308638411" Y="1.685070197349" Z="2.25" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="2.25" />
                  <Point X="-3.693181665063" Y="1.604340000402" Z="2.25" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="2.25" />
                  <Point X="-3.669624191117" Y="1.546940339246" Z="2.25" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="2.25" />
                  <Point X="-3.698452417165" Y="1.491998535844" Z="2.25" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="2.25" />
                  <Point X="-4.69559308377" Y="1.598940988558" Z="2.25" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="2.25" />
                  <Point X="-4.834266983479" Y="1.549277389038" Z="2.25" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="2.25" />
                  <Point X="-4.982776755928" Y="0.970720533935" Z="2.25" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="2.25" />
                  <Point X="-3.74522522337" Y="0.09426254603" Z="2.25" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="2.25" />
                  <Point X="-3.573405977029" Y="0.046879449396" Z="2.25" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="2.25" />
                  <Point X="-3.547756255128" Y="0.026418699103" Z="2.25" />
                  <Point X="-3.539556741714" Y="0" Z="2.25" />
                  <Point X="-3.540608346007" Y="-0.003388251958" Z="2.25" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="2.25" />
                  <Point X="-3.551962618094" Y="-0.031996533738" Z="2.25" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="2.25" />
                  <Point X="-4.891662565403" Y="-0.401449510726" Z="2.25" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="2.25" />
                  <Point X="-5.051498585428" Y="-0.508370739248" Z="2.25" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="2.25" />
                  <Point X="-4.967229630298" Y="-1.050086884911" Z="2.25" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="2.25" />
                  <Point X="-3.404189831201" Y="-1.331223089101" Z="2.25" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="2.25" />
                  <Point X="-3.216148527614" Y="-1.308635074996" Z="2.25" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="2.25" />
                  <Point X="-3.193552100645" Y="-1.325831282188" Z="2.25" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="2.25" />
                  <Point X="-4.354838987306" Y="-2.238044054824" Z="2.25" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="2.25" />
                  <Point X="-4.469532314342" Y="-2.407609243926" Z="2.25" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="2.25" />
                  <Point X="-4.169897086498" Y="-2.895726504527" Z="2.25" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="2.25" />
                  <Point X="-2.719409672138" Y="-2.64011329863" Z="2.25" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="2.25" />
                  <Point X="-2.570867435576" Y="-2.557463033555" Z="2.25" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="2.25" />
                  <Point X="-3.215303714538" Y="-3.715668710354" Z="2.25" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="2.25" />
                  <Point X="-3.253382484296" Y="-3.898075873609" Z="2.25" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="2.25" />
                  <Point X="-2.840532302694" Y="-4.208425750049" Z="2.25" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="2.25" />
                  <Point X="-2.25178686864" Y="-4.189768592189" Z="2.25" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="2.25" />
                  <Point X="-2.196898420269" Y="-4.136858575686" Z="2.25" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="2.25" />
                  <Point X="-1.933064002958" Y="-3.986390975394" Z="2.25" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="2.25" />
                  <Point X="-1.632151336102" Y="-4.027629447113" Z="2.25" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="2.25" />
                  <Point X="-1.418525630012" Y="-4.243530324691" Z="2.25" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="2.25" />
                  <Point X="-1.407617669522" Y="-4.837868045817" Z="2.25" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="2.25" />
                  <Point X="-1.37948619773" Y="-4.888151544955" Z="2.25" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="2.25" />
                  <Point X="-1.083330138377" Y="-4.962197009392" Z="2.25" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="2.25" />
                  <Point X="-0.462622144358" Y="-3.688713384123" Z="2.25" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="2.25" />
                  <Point X="-0.39847532594" Y="-3.491957521785" Z="2.25" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="2.25" />
                  <Point X="-0.22455893825" Y="-3.273934175873" Z="2.25" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="2.25" />
                  <Point X="0.028800141111" Y="-3.213177869415" Z="2.25" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="2.25" />
                  <Point X="0.271970533492" Y="-3.309688233982" Z="2.25" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="2.25" />
                  <Point X="0.772132716044" Y="-4.843822645693" Z="2.25" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="2.25" />
                  <Point X="0.838168144167" Y="-5.010038837474" Z="2.25" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="2.25" />
                  <Point X="1.018362531082" Y="-4.976540148752" Z="2.25" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="2.25" />
                  <Point X="0.982320606478" Y="-3.462616284498" Z="2.25" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="2.25" />
                  <Point X="0.963463020517" Y="-3.244769606698" Z="2.25" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="2.25" />
                  <Point X="1.031619345882" Y="-3.008314751716" Z="2.25" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="2.25" />
                  <Point X="1.217639466309" Y="-2.873237280024" Z="2.25" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="2.25" />
                  <Point X="1.448456883062" Y="-2.869801943144" Z="2.25" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="2.25" />
                  <Point X="2.545566548883" Y="-4.174850800018" Z="2.25" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="2.25" />
                  <Point X="2.684238741178" Y="-4.312286120072" Z="2.25" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="2.25" />
                  <Point X="2.87878498704" Y="-4.184918906084" Z="2.25" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="2.25" />
                  <Point X="2.359364978702" Y="-2.874940874171" Z="2.25" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="2.25" />
                  <Point X="2.26680071023" Y="-2.697734926991" Z="2.25" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="2.25" />
                  <Point X="2.24295085421" Y="-2.485801794938" Z="2.25" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="2.25" />
                  <Point X="2.347096583435" Y="-2.315950455757" Z="2.25" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="2.25" />
                  <Point X="2.530771876521" Y="-2.23664729926" Z="2.25" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="2.25" />
                  <Point X="3.912472722936" Y="-2.958384521573" Z="2.25" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="2.25" />
                  <Point X="4.084963145513" Y="-3.018311087349" Z="2.25" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="2.25" />
                  <Point X="4.257851571663" Y="-2.769083743048" Z="2.25" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="2.25" />
                  <Point X="3.329885661914" Y="-1.719826443428" Z="2.25" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="2.25" />
                  <Point X="3.181320903751" Y="-1.596826981865" Z="2.25" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="2.25" />
                  <Point X="3.094050466858" Y="-1.438872304936" Z="2.25" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="2.25" />
                  <Point X="3.120466418477" Y="-1.272368725657" Z="2.25" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="2.25" />
                  <Point X="3.238374530194" Y="-1.150898210627" Z="2.25" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="2.25" />
                  <Point X="4.735620852114" Y="-1.291850407371" Z="2.25" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="2.25" />
                  <Point X="4.916604357478" Y="-1.27235571892" Z="2.25" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="2.25" />
                  <Point X="4.997869647486" Y="-0.90176560192" Z="2.25" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="2.25" />
                  <Point X="3.89573497447" Y="-0.26040877625" Z="2.25" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="2.25" />
                  <Point X="3.737436792246" Y="-0.214732253899" Z="2.25" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="2.25" />
                  <Point X="3.649133170242" Y="-0.159298365023" Z="2.25" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="2.25" />
                  <Point X="3.597833542226" Y="-0.085628501526" Z="2.25" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="2.25" />
                  <Point X="3.583537908198" Y="0.010982029655" Z="2.25" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="2.25" />
                  <Point X="3.606246268158" Y="0.104650373552" Z="2.25" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="2.25" />
                  <Point X="3.665958622106" Y="0.173416590343" Z="2.25" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="2.25" />
                  <Point X="4.900232842837" Y="0.529563156519" Z="2.25" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="2.25" />
                  <Point X="5.040523798928" Y="0.617276813127" Z="2.25" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="2.25" />
                  <Point X="4.970594671" Y="1.039753939291" Z="2.25" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="2.25" />
                  <Point X="3.624271844719" Y="1.243239946335" Z="2.25" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="2.25" />
                  <Point X="3.452417853862" Y="1.223438689025" Z="2.25" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="2.25" />
                  <Point X="3.36104418132" Y="1.238924848015" Z="2.25" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="2.25" />
                  <Point X="3.293855695102" Y="1.281974333387" Z="2.25" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="2.25" />
                  <Point X="3.249252508093" Y="1.35645055977" Z="2.25" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="2.25" />
                  <Point X="3.236038730266" Y="1.4410980069" Z="2.25" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="2.25" />
                  <Point X="3.261684271265" Y="1.517882548471" Z="2.25" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="2.25" />
                  <Point X="4.318359423557" Y="2.356212637873" Z="2.25" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="2.25" />
                  <Point X="4.423539637117" Y="2.494445211766" Z="2.25" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="2.25" />
                  <Point X="4.211365870517" Y="2.838018476285" Z="2.25" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="2.25" />
                  <Point X="2.679521921305" Y="2.364942771999" Z="2.25" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="2.25" />
                  <Point X="2.50075152235" Y="2.264558250185" Z="2.25" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="2.25" />
                  <Point X="2.421699907824" Y="2.246480773431" Z="2.25" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="2.25" />
                  <Point X="2.352970282834" Y="2.258783650128" Z="2.25" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="2.25" />
                  <Point X="2.291974950107" Y="2.304054577548" Z="2.25" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="2.25" />
                  <Point X="2.252948929327" Y="2.368058536009" Z="2.25" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="2.25" />
                  <Point X="2.247969692472" Y="2.438718053532" Z="2.25" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="2.25" />
                  <Point X="3.030682580861" Y="3.83261716065" Z="2.25" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="2.25" />
                  <Point X="3.08598449439" Y="4.032585860832" Z="2.25" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="2.25" />
                  <Point X="2.708286009277" Y="4.295372518166" Z="2.25" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="2.25" />
                  <Point X="2.308959878866" Y="4.522641526216" Z="2.25" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="2.25" />
                  <Point X="1.895459394655" Y="4.710923812723" Z="2.25" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="2.25" />
                  <Point X="1.442374959544" Y="4.866242441956" Z="2.25" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="2.25" />
                  <Point X="0.786475577119" Y="5.014195901209" Z="2.25" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="2.25" />
                  <Point X="0.021966730601" Y="4.437105432436" Z="2.25" />
                  <Point X="0" Y="4.355124473572" Z="2.25" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>