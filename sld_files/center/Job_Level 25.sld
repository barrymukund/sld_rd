<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#165" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1878" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="0" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="0.004715820312" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.752753173828" Y="-4.21956640625" />
                  <Point X="0.563302062988" Y="-3.512524414063" />
                  <Point X="0.557721191406" Y="-3.497141845703" />
                  <Point X="0.54236315918" Y="-3.467377197266" />
                  <Point X="0.464929748535" Y="-3.355810058594" />
                  <Point X="0.378635528564" Y="-3.231476806641" />
                  <Point X="0.356751586914" Y="-3.209021484375" />
                  <Point X="0.330496398926" Y="-3.189777587891" />
                  <Point X="0.302495178223" Y="-3.175669189453" />
                  <Point X="0.18267137146" Y="-3.138480224609" />
                  <Point X="0.049136188507" Y="-3.097035888672" />
                  <Point X="0.020976791382" Y="-3.092766601562" />
                  <Point X="-0.008664840698" Y="-3.092766601562" />
                  <Point X="-0.036824237823" Y="-3.097035888672" />
                  <Point X="-0.156648040771" Y="-3.134224609375" />
                  <Point X="-0.290183227539" Y="-3.175669189453" />
                  <Point X="-0.31818460083" Y="-3.189777587891" />
                  <Point X="-0.344439788818" Y="-3.209021484375" />
                  <Point X="-0.366323577881" Y="-3.231476806641" />
                  <Point X="-0.443757171631" Y="-3.343043701172" />
                  <Point X="-0.530051208496" Y="-3.467377197266" />
                  <Point X="-0.538189025879" Y="-3.481573730469" />
                  <Point X="-0.550990112305" Y="-3.512524414063" />
                  <Point X="-0.762120117188" Y="-4.300472167969" />
                  <Point X="-0.916584533691" Y="-4.876941894531" />
                  <Point X="-0.925638916016" Y="-4.875184082031" />
                  <Point X="-1.079342651367" Y="-4.845350097656" />
                  <Point X="-1.214962524414" Y="-4.810456054688" />
                  <Point X="-1.24641809082" Y="-4.802362792969" />
                  <Point X="-1.237768554688" Y="-4.736663085938" />
                  <Point X="-1.214963012695" Y="-4.563438476562" />
                  <Point X="-1.214201171875" Y="-4.5479296875" />
                  <Point X="-1.216508666992" Y="-4.516224121094" />
                  <Point X="-1.245134643555" Y="-4.372312011719" />
                  <Point X="-1.277036132812" Y="-4.211932617188" />
                  <Point X="-1.287938354492" Y="-4.182966308594" />
                  <Point X="-1.304010375977" Y="-4.15512890625" />
                  <Point X="-1.323644775391" Y="-4.131204101562" />
                  <Point X="-1.433963378906" Y="-4.03445703125" />
                  <Point X="-1.556905517578" Y="-3.926639160156" />
                  <Point X="-1.583188476562" Y="-3.910295410156" />
                  <Point X="-1.612885742188" Y="-3.897994384766" />
                  <Point X="-1.64302734375" Y="-3.890966308594" />
                  <Point X="-1.789444824219" Y="-3.881369628906" />
                  <Point X="-1.952616577148" Y="-3.870674804688" />
                  <Point X="-1.98341796875" Y="-3.873708496094" />
                  <Point X="-2.014466674805" Y="-3.882028076172" />
                  <Point X="-2.042657714844" Y="-3.894801513672" />
                  <Point X="-2.164660644531" Y="-3.976321289062" />
                  <Point X="-2.300624267578" Y="-4.067169677734" />
                  <Point X="-2.312785644531" Y="-4.076821533203" />
                  <Point X="-2.335102539062" Y="-4.099461914062" />
                  <Point X="-2.453635742188" Y="-4.253937011719" />
                  <Point X="-2.480148925781" Y="-4.288489746094" />
                  <Point X="-2.576419189453" Y="-4.228881835938" />
                  <Point X="-2.801707763672" Y="-4.089388916016" />
                  <Point X="-2.989504394531" Y="-3.944791748047" />
                  <Point X="-3.104721923828" Y="-3.856077880859" />
                  <Point X="-2.775835205078" Y="-3.286429199219" />
                  <Point X="-2.423761230469" Y="-2.676619628906" />
                  <Point X="-2.412859130859" Y="-2.647653320312" />
                  <Point X="-2.406588134766" Y="-2.616126953125" />
                  <Point X="-2.405575439453" Y="-2.585192871094" />
                  <Point X="-2.414560058594" Y="-2.555575195312" />
                  <Point X="-2.428777099609" Y="-2.526746337891" />
                  <Point X="-2.4468046875" Y="-2.501589355469" />
                  <Point X="-2.464153320312" Y="-2.484240722656" />
                  <Point X="-2.489310302734" Y="-2.466213134766" />
                  <Point X="-2.518139160156" Y="-2.45199609375" />
                  <Point X="-2.547756835938" Y="-2.443011474609" />
                  <Point X="-2.578690917969" Y="-2.444024169922" />
                  <Point X="-2.610217285156" Y="-2.450295166016" />
                  <Point X="-2.63918359375" Y="-2.461197265625" />
                  <Point X="-3.318773193359" Y="-2.85355859375" />
                  <Point X="-3.818024414062" Y="-3.141801269531" />
                  <Point X="-3.904869140625" Y="-3.027705322266" />
                  <Point X="-4.082858886719" Y="-2.793863037109" />
                  <Point X="-4.217492675781" Y="-2.568102783203" />
                  <Point X="-4.306142578125" Y="-2.419450439453" />
                  <Point X="-3.721969482422" Y="-1.971198242188" />
                  <Point X="-3.105954589844" Y="-1.498513427734" />
                  <Point X="-3.084577148438" Y="-1.475593261719" />
                  <Point X="-3.066612304688" Y="-1.448462036133" />
                  <Point X="-3.053856445312" Y="-1.419832885742" />
                  <Point X="-3.046151855469" Y="-1.390085571289" />
                  <Point X="-3.04334765625" Y="-1.35965612793" />
                  <Point X="-3.045556640625" Y="-1.327985595703" />
                  <Point X="-3.052557861328" Y="-1.298240844727" />
                  <Point X="-3.068639892578" Y="-1.272257446289" />
                  <Point X="-3.089472412109" Y="-1.24830090332" />
                  <Point X="-3.11297265625" Y="-1.228766723633" />
                  <Point X="-3.139455322266" Y="-1.213180297852" />
                  <Point X="-3.168718261719" Y="-1.201956542969" />
                  <Point X="-3.200605957031" Y="-1.195474853516" />
                  <Point X="-3.231929199219" Y="-1.194383789062" />
                  <Point X="-4.089846679687" Y="-1.307330810547" />
                  <Point X="-4.732102050781" Y="-1.391885253906" />
                  <Point X="-4.764463378906" Y="-1.265191650391" />
                  <Point X="-4.834077636719" Y="-0.992654052734" />
                  <Point X="-4.869697753906" Y="-0.74360144043" />
                  <Point X="-4.892424316406" Y="-0.584698242188" />
                  <Point X="-4.234609863281" Y="-0.408437286377" />
                  <Point X="-3.532875976562" Y="-0.220408447266" />
                  <Point X="-3.510474365234" Y="-0.211210403442" />
                  <Point X="-3.481780761719" Y="-0.194880325317" />
                  <Point X="-3.468558837891" Y="-0.185797042847" />
                  <Point X="-3.442112792969" Y="-0.164126983643" />
                  <Point X="-3.425920898438" Y="-0.147103591919" />
                  <Point X="-3.414402099609" Y="-0.126627166748" />
                  <Point X="-3.4026015625" Y="-0.097792449951" />
                  <Point X="-3.398008056641" Y="-0.083394523621" />
                  <Point X="-3.390885253906" Y="-0.052864006042" />
                  <Point X="-3.388401123047" Y="-0.031473653793" />
                  <Point X="-3.390798095703" Y="-0.01007338047" />
                  <Point X="-3.397420410156" Y="0.018844209671" />
                  <Point X="-3.401940185547" Y="0.033223468781" />
                  <Point X="-3.414241210938" Y="0.063671104431" />
                  <Point X="-3.425667724609" Y="0.084199211121" />
                  <Point X="-3.441782714844" Y="0.101295394897" />
                  <Point X="-3.466726806641" Y="0.121922950745" />
                  <Point X="-3.479892822266" Y="0.131056945801" />
                  <Point X="-3.510088378906" Y="0.148429504395" />
                  <Point X="-3.532875976562" Y="0.157848251343" />
                  <Point X="-4.314908203125" Y="0.367393249512" />
                  <Point X="-4.89181640625" Y="0.521975280762" />
                  <Point X="-4.869352539063" Y="0.673783691406" />
                  <Point X="-4.824487792969" Y="0.976975463867" />
                  <Point X="-4.75278515625" Y="1.241580932617" />
                  <Point X="-4.703551757812" Y="1.423267822266" />
                  <Point X="-4.268686523438" Y="1.366016845703" />
                  <Point X="-3.765666259766" Y="1.29979284668" />
                  <Point X="-3.744985839844" Y="1.299341674805" />
                  <Point X="-3.723424072266" Y="1.301228271484" />
                  <Point X="-3.703137451172" Y="1.305263549805" />
                  <Point X="-3.674086669922" Y="1.314423217773" />
                  <Point X="-3.641711425781" Y="1.324631103516" />
                  <Point X="-3.622778564453" Y="1.332961791992" />
                  <Point X="-3.604034423828" Y="1.343783691406" />
                  <Point X="-3.587353271484" Y="1.356014892578" />
                  <Point X="-3.57371484375" Y="1.371566650391" />
                  <Point X="-3.561300537109" Y="1.389296264648" />
                  <Point X="-3.5513515625" Y="1.407431274414" />
                  <Point X="-3.539694824219" Y="1.435573364258" />
                  <Point X="-3.526704101563" Y="1.466935546875" />
                  <Point X="-3.520916015625" Y="1.48679296875" />
                  <Point X="-3.517157470703" Y="1.508108154297" />
                  <Point X="-3.515804443359" Y="1.528748779297" />
                  <Point X="-3.518951171875" Y="1.549192871094" />
                  <Point X="-3.524552978516" Y="1.570099243164" />
                  <Point X="-3.532049804688" Y="1.589377563477" />
                  <Point X="-3.546114990234" Y="1.616396728516" />
                  <Point X="-3.561789550781" Y="1.646507202148" />
                  <Point X="-3.57328125" Y="1.663705932617" />
                  <Point X="-3.587193847656" Y="1.680286376953" />
                  <Point X="-3.602135986328" Y="1.694590087891" />
                  <Point X="-4.050711669922" Y="2.038794433594" />
                  <Point X="-4.351859863281" Y="2.269874267578" />
                  <Point X="-4.255479980469" Y="2.434996826172" />
                  <Point X="-4.081153808594" Y="2.733659423828" />
                  <Point X="-3.891216796875" Y="2.977796386719" />
                  <Point X="-3.750504882813" Y="3.158661621094" />
                  <Point X="-3.515597900391" Y="3.023038085938" />
                  <Point X="-3.206656982422" Y="2.844670898438" />
                  <Point X="-3.187724121094" Y="2.836340332031" />
                  <Point X="-3.167081787109" Y="2.829831787109" />
                  <Point X="-3.146794433594" Y="2.825796386719" />
                  <Point X="-3.106334716797" Y="2.822256591797" />
                  <Point X="-3.061245117188" Y="2.818311767578" />
                  <Point X="-3.040564941406" Y="2.818762939453" />
                  <Point X="-3.019106201172" Y="2.821588134766" />
                  <Point X="-2.999014160156" Y="2.826504882812" />
                  <Point X="-2.980462402344" Y="2.835653564453" />
                  <Point X="-2.962208251953" Y="2.847282958984" />
                  <Point X="-2.946077636719" Y="2.860229248047" />
                  <Point X="-2.917358886719" Y="2.888947753906" />
                  <Point X="-2.885354003906" Y="2.920952636719" />
                  <Point X="-2.872406982422" Y="2.937083984375" />
                  <Point X="-2.860777587891" Y="2.955338134766" />
                  <Point X="-2.85162890625" Y="2.973889892578" />
                  <Point X="-2.846712158203" Y="2.993981933594" />
                  <Point X="-2.843886962891" Y="3.015440673828" />
                  <Point X="-2.843435791016" Y="3.036120849609" />
                  <Point X="-2.846975585938" Y="3.076580810547" />
                  <Point X="-2.850920410156" Y="3.121670410156" />
                  <Point X="-2.854955810547" Y="3.141957763672" />
                  <Point X="-2.861464355469" Y="3.162600097656" />
                  <Point X="-2.869794921875" Y="3.181532958984" />
                  <Point X="-3.068572509766" Y="3.525825439453" />
                  <Point X="-3.183333007812" Y="3.724596435547" />
                  <Point X="-3.004242675781" Y="3.861903076172" />
                  <Point X="-2.700625732422" Y="4.09468359375" />
                  <Point X="-2.401482910156" Y="4.260880859375" />
                  <Point X="-2.167036865234" Y="4.391133789062" />
                  <Point X="-2.137771728516" Y="4.352994628906" />
                  <Point X="-2.0431953125" Y="4.229740722656" />
                  <Point X="-2.028891967773" Y="4.214799316406" />
                  <Point X="-2.01231237793" Y="4.200887207031" />
                  <Point X="-1.995113037109" Y="4.189395019531" />
                  <Point X="-1.950081420898" Y="4.165953125" />
                  <Point X="-1.899896972656" Y="4.139828125" />
                  <Point X="-1.880619262695" Y="4.132331542969" />
                  <Point X="-1.859712402344" Y="4.126729492187" />
                  <Point X="-1.839268798828" Y="4.123582519531" />
                  <Point X="-1.818628417969" Y="4.124935546875" />
                  <Point X="-1.797312988281" Y="4.128693847656" />
                  <Point X="-1.777453613281" Y="4.134481933594" />
                  <Point X="-1.730550170898" Y="4.15391015625" />
                  <Point X="-1.678279663086" Y="4.175561523437" />
                  <Point X="-1.66014465332" Y="4.185510742187" />
                  <Point X="-1.642415039062" Y="4.197925292969" />
                  <Point X="-1.626863525391" Y="4.211563964844" />
                  <Point X="-1.614632568359" Y="4.228245117188" />
                  <Point X="-1.603810791016" Y="4.246989257813" />
                  <Point X="-1.59548046875" Y="4.265921386719" />
                  <Point X="-1.580214111328" Y="4.314339355469" />
                  <Point X="-1.563201171875" Y="4.368297851562" />
                  <Point X="-1.559165649414" Y="4.388584960938" />
                  <Point X="-1.557279174805" Y="4.410146484375" />
                  <Point X="-1.55773034668" Y="4.430826660156" />
                  <Point X="-1.580329101562" Y="4.602481445312" />
                  <Point X="-1.584201904297" Y="4.631897949219" />
                  <Point X="-1.342686401367" Y="4.699610351562" />
                  <Point X="-0.949634887695" Y="4.80980859375" />
                  <Point X="-0.586989501953" Y="4.852250488281" />
                  <Point X="-0.294710754395" Y="4.886457519531" />
                  <Point X="-0.225296463013" Y="4.627399902344" />
                  <Point X="-0.133903305054" Y="4.286315429688" />
                  <Point X="-0.121129844666" Y="4.258124023438" />
                  <Point X="-0.10327155304" Y="4.231397460938" />
                  <Point X="-0.0821145401" Y="4.20880859375" />
                  <Point X="-0.054819446564" Y="4.19421875" />
                  <Point X="-0.024381370544" Y="4.183886230469" />
                  <Point X="0.006155907631" Y="4.178844238281" />
                  <Point X="0.03669316864" Y="4.183886230469" />
                  <Point X="0.067131248474" Y="4.19421875" />
                  <Point X="0.094426467896" Y="4.20880859375" />
                  <Point X="0.115583503723" Y="4.231397460938" />
                  <Point X="0.133441802979" Y="4.258124023438" />
                  <Point X="0.146215255737" Y="4.286315429688" />
                  <Point X="0.248066482544" Y="4.6664296875" />
                  <Point X="0.307419342041" Y="4.8879375" />
                  <Point X="0.500843811035" Y="4.867680664062" />
                  <Point X="0.844041015625" Y="4.831738769531" />
                  <Point X="1.144072998047" Y="4.759301757813" />
                  <Point X="1.481026611328" Y="4.677950683594" />
                  <Point X="1.675537353516" Y="4.607400390625" />
                  <Point X="1.894645507812" Y="4.527928222656" />
                  <Point X="2.083485595703" Y="4.439613769531" />
                  <Point X="2.294576416016" Y="4.340893554688" />
                  <Point X="2.477031005859" Y="4.234595214844" />
                  <Point X="2.680977539062" Y="4.115775390625" />
                  <Point X="2.853034179688" Y="3.993418457031" />
                  <Point X="2.943260009766" Y="3.929254882812" />
                  <Point X="2.555080078125" Y="3.256906982422" />
                  <Point X="2.147581054688" Y="2.551097900391" />
                  <Point X="2.142075927734" Y="2.539931152344" />
                  <Point X="2.133076660156" Y="2.516056640625" />
                  <Point X="2.122922851562" Y="2.4780859375" />
                  <Point X="2.111606933594" Y="2.435770263672" />
                  <Point X="2.108619384766" Y="2.417934326172" />
                  <Point X="2.107728027344" Y="2.380952880859" />
                  <Point X="2.111687255859" Y="2.348119140625" />
                  <Point X="2.116099365234" Y="2.311528076172" />
                  <Point X="2.121441894531" Y="2.289605224609" />
                  <Point X="2.129708007812" Y="2.267516357422" />
                  <Point X="2.140071044922" Y="2.247471191406" />
                  <Point X="2.160387451172" Y="2.217530029297" />
                  <Point X="2.183028808594" Y="2.184162597656" />
                  <Point X="2.19446484375" Y="2.170328613281" />
                  <Point X="2.221598876953" Y="2.145592529297" />
                  <Point X="2.251540039062" Y="2.125276123047" />
                  <Point X="2.284907470703" Y="2.102635009766" />
                  <Point X="2.304952880859" Y="2.092271972656" />
                  <Point X="2.327041259766" Y="2.084006103516" />
                  <Point X="2.348963867188" Y="2.078663330078" />
                  <Point X="2.381797851562" Y="2.074704101562" />
                  <Point X="2.418388671875" Y="2.070291748047" />
                  <Point X="2.436467529297" Y="2.069845703125" />
                  <Point X="2.473206542969" Y="2.074171142578" />
                  <Point X="2.511177246094" Y="2.084325195313" />
                  <Point X="2.553492919922" Y="2.095640869141" />
                  <Point X="2.565283447266" Y="2.099638183594" />
                  <Point X="2.588533935547" Y="2.110145019531" />
                  <Point X="3.375107910156" Y="2.564273681641" />
                  <Point X="3.967326416016" Y="2.906191162109" />
                  <Point X="4.002296630859" Y="2.857590576172" />
                  <Point X="4.123270019531" Y="2.689465087891" />
                  <Point X="4.219190917969" Y="2.530955322266" />
                  <Point X="4.262198730469" Y="2.459884277344" />
                  <Point X="3.767294677734" Y="2.080130615234" />
                  <Point X="3.230783935547" Y="1.668451171875" />
                  <Point X="3.221424560547" Y="1.660241455078" />
                  <Point X="3.203973876953" Y="1.641627929688" />
                  <Point X="3.176646240234" Y="1.605976928711" />
                  <Point X="3.146191650391" Y="1.566246459961" />
                  <Point X="3.136605957031" Y="1.550912109375" />
                  <Point X="3.121629882812" Y="1.517086303711" />
                  <Point X="3.111450195312" Y="1.480686523438" />
                  <Point X="3.100105957031" Y="1.440121582031" />
                  <Point X="3.09665234375" Y="1.417821899414" />
                  <Point X="3.095836425781" Y="1.394252197266" />
                  <Point X="3.097739501953" Y="1.371768188477" />
                  <Point X="3.106095947266" Y="1.331268920898" />
                  <Point X="3.115408447266" Y="1.286135375977" />
                  <Point X="3.1206796875" Y="1.268978393555" />
                  <Point X="3.136282470703" Y="1.235740356445" />
                  <Point X="3.159010742188" Y="1.201194335938" />
                  <Point X="3.184340087891" Y="1.16269519043" />
                  <Point X="3.198893554688" Y="1.145450439453" />
                  <Point X="3.216137451172" Y="1.129360595703" />
                  <Point X="3.234347167969" Y="1.116034790039" />
                  <Point X="3.267283691406" Y="1.097494384766" />
                  <Point X="3.303989257812" Y="1.076832519531" />
                  <Point X="3.320521728516" Y="1.069501464844" />
                  <Point X="3.356118164062" Y="1.059438598633" />
                  <Point X="3.400650634766" Y="1.053552978516" />
                  <Point X="3.450278808594" Y="1.046994018555" />
                  <Point X="3.462702392578" Y="1.046175048828" />
                  <Point X="3.488203613281" Y="1.04698449707" />
                  <Point X="4.235396484375" Y="1.145354248047" />
                  <Point X="4.776839355469" Y="1.21663671875" />
                  <Point X="4.793978027344" Y="1.146236450195" />
                  <Point X="4.845936035156" Y="0.932809631348" />
                  <Point X="4.876162597656" Y="0.738667602539" />
                  <Point X="4.890864746094" Y="0.644238525391" />
                  <Point X="4.331432617187" Y="0.494339050293" />
                  <Point X="3.716579833984" Y="0.32958984375" />
                  <Point X="3.704791259766" Y="0.325586517334" />
                  <Point X="3.681545654297" Y="0.315068054199" />
                  <Point X="3.637793945312" Y="0.289778717041" />
                  <Point X="3.589035644531" Y="0.261595489502" />
                  <Point X="3.574311035156" Y="0.251096282959" />
                  <Point X="3.547530761719" Y="0.225576522827" />
                  <Point X="3.521279785156" Y="0.192126464844" />
                  <Point X="3.492024902344" Y="0.154848876953" />
                  <Point X="3.480301025391" Y="0.135569213867" />
                  <Point X="3.470527099609" Y="0.114105445862" />
                  <Point X="3.463680908203" Y="0.092604179382" />
                  <Point X="3.454930419922" Y="0.046913291931" />
                  <Point X="3.445178710938" Y="-0.004006166458" />
                  <Point X="3.443482910156" Y="-0.021875236511" />
                  <Point X="3.445178710938" Y="-0.058553871155" />
                  <Point X="3.453929199219" Y="-0.104244911194" />
                  <Point X="3.463680908203" Y="-0.155164367676" />
                  <Point X="3.470527099609" Y="-0.176665481567" />
                  <Point X="3.480301025391" Y="-0.198129257202" />
                  <Point X="3.492024902344" Y="-0.217408905029" />
                  <Point X="3.518275878906" Y="-0.250858963013" />
                  <Point X="3.547530761719" Y="-0.288136566162" />
                  <Point X="3.559999023438" Y="-0.301236083984" />
                  <Point X="3.589035644531" Y="-0.324155517578" />
                  <Point X="3.632787353516" Y="-0.349444854736" />
                  <Point X="3.681545654297" Y="-0.377628082275" />
                  <Point X="3.692710205078" Y="-0.383138977051" />
                  <Point X="3.716579833984" Y="-0.392150024414" />
                  <Point X="4.401789550781" Y="-0.575751281738" />
                  <Point X="4.891472167969" Y="-0.706961425781" />
                  <Point X="4.884033691406" Y="-0.756301147461" />
                  <Point X="4.855022460938" Y="-0.948726013184" />
                  <Point X="4.816296875" Y="-1.118427246094" />
                  <Point X="4.801173828125" Y="-1.184698974609" />
                  <Point X="4.137438476562" Y="-1.09731652832" />
                  <Point X="3.424382080078" Y="-1.003440979004" />
                  <Point X="3.408035644531" Y="-1.002710266113" />
                  <Point X="3.374658691406" Y="-1.005508666992" />
                  <Point X="3.288789550781" Y="-1.024172607422" />
                  <Point X="3.193094482422" Y="-1.044972290039" />
                  <Point X="3.163974121094" Y="-1.056596923828" />
                  <Point X="3.136147460938" Y="-1.073489257812" />
                  <Point X="3.112397460938" Y="-1.093960083008" />
                  <Point X="3.060494873047" Y="-1.15638269043" />
                  <Point X="3.002653320312" Y="-1.225948242188" />
                  <Point X="2.987932617188" Y="-1.250330688477" />
                  <Point X="2.976589355469" Y="-1.277715942383" />
                  <Point X="2.969757568359" Y="-1.305365478516" />
                  <Point X="2.962318603516" Y="-1.386205322266" />
                  <Point X="2.954028564453" Y="-1.476295776367" />
                  <Point X="2.956347412109" Y="-1.507564697266" />
                  <Point X="2.964079101562" Y="-1.539185668945" />
                  <Point X="2.976450439453" Y="-1.567996582031" />
                  <Point X="3.023971679688" Y="-1.641912841797" />
                  <Point X="3.076930664062" Y="-1.724287109375" />
                  <Point X="3.086932373047" Y="-1.73723828125" />
                  <Point X="3.110628417969" Y="-1.760909057617" />
                  <Point X="3.746507324219" Y="-2.248836425781" />
                  <Point X="4.213122558594" Y="-2.6068828125" />
                  <Point X="4.206590332031" Y="-2.617452880859" />
                  <Point X="4.124810058594" Y="-2.749786376953" />
                  <Point X="4.04472265625" Y="-2.863578857422" />
                  <Point X="4.028981445312" Y="-2.8859453125" />
                  <Point X="3.43602734375" Y="-2.543602783203" />
                  <Point X="2.800954589844" Y="-2.176943115234" />
                  <Point X="2.786131591797" Y="-2.170012451172" />
                  <Point X="2.754224609375" Y="-2.159825195312" />
                  <Point X="2.652026611328" Y="-2.141368408203" />
                  <Point X="2.538134277344" Y="-2.120799560547" />
                  <Point X="2.506781982422" Y="-2.120395263672" />
                  <Point X="2.474609130859" Y="-2.125353027344" />
                  <Point X="2.444833496094" Y="-2.135176757812" />
                  <Point X="2.359931884766" Y="-2.179859619141" />
                  <Point X="2.265315429688" Y="-2.229655761719" />
                  <Point X="2.242384033203" Y="-2.246549072266" />
                  <Point X="2.221424316406" Y="-2.267509033203" />
                  <Point X="2.204531982422" Y="-2.290439453125" />
                  <Point X="2.159848876953" Y="-2.375340820312" />
                  <Point X="2.110052978516" Y="-2.469957519531" />
                  <Point X="2.100229003906" Y="-2.499733398438" />
                  <Point X="2.095271240234" Y="-2.531906005859" />
                  <Point X="2.095675537109" Y="-2.563258300781" />
                  <Point X="2.114132324219" Y="-2.665456298828" />
                  <Point X="2.134701171875" Y="-2.779348632812" />
                  <Point X="2.138985595703" Y="-2.795141845703" />
                  <Point X="2.151819091797" Y="-2.826078613281" />
                  <Point X="2.560435302734" Y="-3.533822265625" />
                  <Point X="2.861283203125" Y="-4.054906494141" />
                  <Point X="2.781851806641" Y="-4.111642578125" />
                  <Point X="2.701765136719" Y="-4.163481445312" />
                  <Point X="2.243165527344" Y="-3.565822998047" />
                  <Point X="1.758546020508" Y="-2.934255126953" />
                  <Point X="1.747503540039" Y="-2.922179443359" />
                  <Point X="1.721924194336" Y="-2.900557373047" />
                  <Point X="1.621129394531" Y="-2.835755615234" />
                  <Point X="1.50880078125" Y="-2.763538574219" />
                  <Point X="1.479989746094" Y="-2.751167236328" />
                  <Point X="1.448368408203" Y="-2.743435546875" />
                  <Point X="1.417099487305" Y="-2.741116699219" />
                  <Point X="1.30686315918" Y="-2.751260742188" />
                  <Point X="1.184012573242" Y="-2.762565673828" />
                  <Point X="1.156362670898" Y="-2.769397460938" />
                  <Point X="1.128977661133" Y="-2.780740722656" />
                  <Point X="1.104595825195" Y="-2.795461181641" />
                  <Point X="1.019474304199" Y="-2.866237060547" />
                  <Point X="0.924612121582" Y="-2.945111572266" />
                  <Point X="0.904141479492" Y="-2.968861572266" />
                  <Point X="0.887249084473" Y="-2.996688232422" />
                  <Point X="0.875624328613" Y="-3.025808837891" />
                  <Point X="0.850173339844" Y="-3.142903320312" />
                  <Point X="0.821810058594" Y="-3.273396728516" />
                  <Point X="0.819724487305" Y="-3.289627441406" />
                  <Point X="0.819742248535" Y="-3.323120117188" />
                  <Point X="0.935540893555" Y="-4.202698242188" />
                  <Point X="1.022065307617" Y="-4.859915039062" />
                  <Point X="0.975679138184" Y="-4.870083007812" />
                  <Point X="0.929315551758" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058439453125" Y="-4.752634277344" />
                  <Point X="-1.141246459961" Y="-4.731328613281" />
                  <Point X="-1.120775634766" Y="-4.575838378906" />
                  <Point X="-1.120077514648" Y="-4.568099609375" />
                  <Point X="-1.119451782227" Y="-4.541033691406" />
                  <Point X="-1.121759277344" Y="-4.509328125" />
                  <Point X="-1.123334106445" Y="-4.497690429688" />
                  <Point X="-1.151960083008" Y="-4.353778320312" />
                  <Point X="-1.183861450195" Y="-4.193398925781" />
                  <Point X="-1.188125" Y="-4.17846875" />
                  <Point X="-1.19902734375" Y="-4.149502441406" />
                  <Point X="-1.205666015625" Y="-4.135466308594" />
                  <Point X="-1.221738037109" Y="-4.10762890625" />
                  <Point X="-1.23057409668" Y="-4.094861816406" />
                  <Point X="-1.250208496094" Y="-4.070937011719" />
                  <Point X="-1.261006958008" Y="-4.059779296875" />
                  <Point X="-1.371325317383" Y="-3.963032226562" />
                  <Point X="-1.494267578125" Y="-3.855214355469" />
                  <Point X="-1.506739135742" Y="-3.84596484375" />
                  <Point X="-1.533021972656" Y="-3.82962109375" />
                  <Point X="-1.546833618164" Y="-3.822526855469" />
                  <Point X="-1.576530761719" Y="-3.810225830078" />
                  <Point X="-1.591313476562" Y="-3.805476074219" />
                  <Point X="-1.621454833984" Y="-3.798447998047" />
                  <Point X="-1.636813964844" Y="-3.796169677734" />
                  <Point X="-1.783231567383" Y="-3.786572998047" />
                  <Point X="-1.946403198242" Y="-3.775878173828" />
                  <Point X="-1.961928222656" Y="-3.776132324219" />
                  <Point X="-1.992729614258" Y="-3.779166015625" />
                  <Point X="-2.008006103516" Y="-3.781945556641" />
                  <Point X="-2.03905480957" Y="-3.790265136719" />
                  <Point X="-2.053674560547" Y="-3.795496337891" />
                  <Point X="-2.081865478516" Y="-3.808269775391" />
                  <Point X="-2.095437011719" Y="-3.815812011719" />
                  <Point X="-2.217439941406" Y="-3.897331787109" />
                  <Point X="-2.353403564453" Y="-3.988180175781" />
                  <Point X="-2.359681640625" Y="-3.992757080078" />
                  <Point X="-2.380442382812" Y="-4.010131347656" />
                  <Point X="-2.402759277344" Y="-4.032771728516" />
                  <Point X="-2.410470947266" Y="-4.041629394531" />
                  <Point X="-2.503202636719" Y="-4.162479492188" />
                  <Point X="-2.526408203125" Y="-4.148111328125" />
                  <Point X="-2.747584716797" Y="-4.011164306641" />
                  <Point X="-2.931547119141" Y="-3.869519287109" />
                  <Point X="-2.980862792969" Y="-3.831547851562" />
                  <Point X="-2.693562744141" Y="-3.333929199219" />
                  <Point X="-2.341488769531" Y="-2.724119628906" />
                  <Point X="-2.334850097656" Y="-2.710083251953" />
                  <Point X="-2.323947998047" Y="-2.681116943359" />
                  <Point X="-2.319684570312" Y="-2.666187011719" />
                  <Point X="-2.313413574219" Y="-2.634660644531" />
                  <Point X="-2.311638916016" Y="-2.619235351562" />
                  <Point X="-2.310626220703" Y="-2.588301269531" />
                  <Point X="-2.314666259766" Y="-2.557615234375" />
                  <Point X="-2.323650878906" Y="-2.527997558594" />
                  <Point X="-2.329357421875" Y="-2.513557128906" />
                  <Point X="-2.343574462891" Y="-2.484728271484" />
                  <Point X="-2.351557128906" Y="-2.47141015625" />
                  <Point X="-2.369584716797" Y="-2.446253173828" />
                  <Point X="-2.379629638672" Y="-2.434414306641" />
                  <Point X="-2.396978271484" Y="-2.417065673828" />
                  <Point X="-2.408817138672" Y="-2.407020751953" />
                  <Point X="-2.433974121094" Y="-2.388993164062" />
                  <Point X="-2.447292236328" Y="-2.381010498047" />
                  <Point X="-2.47612109375" Y="-2.366793457031" />
                  <Point X="-2.490561523438" Y="-2.361086914062" />
                  <Point X="-2.520179199219" Y="-2.352102294922" />
                  <Point X="-2.550865234375" Y="-2.348062255859" />
                  <Point X="-2.581799316406" Y="-2.349074951172" />
                  <Point X="-2.597224609375" Y="-2.350849609375" />
                  <Point X="-2.628750976562" Y="-2.357120605469" />
                  <Point X="-2.643680908203" Y="-2.361384033203" />
                  <Point X="-2.672647216797" Y="-2.372286132812" />
                  <Point X="-2.68668359375" Y="-2.378924804688" />
                  <Point X="-3.366273193359" Y="-2.771286132812" />
                  <Point X="-3.793089355469" Y="-3.017708496094" />
                  <Point X="-3.829275878906" Y="-2.970166992188" />
                  <Point X="-4.004014648438" Y="-2.740595703125" />
                  <Point X="-4.135899902344" Y="-2.519444335938" />
                  <Point X="-4.181265136719" Y="-2.443373535156" />
                  <Point X="-3.664137207031" Y="-2.046566772461" />
                  <Point X="-3.048122314453" Y="-1.573881958008" />
                  <Point X="-3.036482177734" Y="-1.563309814453" />
                  <Point X="-3.015104736328" Y="-1.540389648438" />
                  <Point X="-3.005367431641" Y="-1.528041503906" />
                  <Point X="-2.987402587891" Y="-1.500910400391" />
                  <Point X="-2.9798359375" Y="-1.487125732422" />
                  <Point X="-2.967080078125" Y="-1.458496459961" />
                  <Point X="-2.961890869141" Y="-1.443652099609" />
                  <Point X="-2.954186279297" Y="-1.413904785156" />
                  <Point X="-2.951552734375" Y="-1.398803222656" />
                  <Point X="-2.948748535156" Y="-1.368373779297" />
                  <Point X="-2.948577880859" Y="-1.353046020508" />
                  <Point X="-2.950786865234" Y="-1.321375488281" />
                  <Point X="-2.953083740234" Y="-1.306219604492" />
                  <Point X="-2.960084960938" Y="-1.276474853516" />
                  <Point X="-2.971778564453" Y="-1.248243774414" />
                  <Point X="-2.987860595703" Y="-1.222260375977" />
                  <Point X="-2.996953369141" Y="-1.209919189453" />
                  <Point X="-3.017785888672" Y="-1.185962646484" />
                  <Point X="-3.028745605469" Y="-1.175244506836" />
                  <Point X="-3.052245849609" Y="-1.155710327148" />
                  <Point X="-3.064786376953" Y="-1.146894287109" />
                  <Point X="-3.091269042969" Y="-1.131307861328" />
                  <Point X="-3.105434814453" Y="-1.124480957031" />
                  <Point X="-3.134697753906" Y="-1.113257080078" />
                  <Point X="-3.149794921875" Y="-1.108860351562" />
                  <Point X="-3.181682617188" Y="-1.102378540039" />
                  <Point X="-3.197298828125" Y="-1.100532348633" />
                  <Point X="-3.228622070312" Y="-1.09944140625" />
                  <Point X="-3.244329101562" Y="-1.100196533203" />
                  <Point X="-4.102246582031" Y="-1.213143554688" />
                  <Point X="-4.660920410156" Y="-1.286694335938" />
                  <Point X="-4.672418457031" Y="-1.241680664062" />
                  <Point X="-4.740762207031" Y="-0.974117614746" />
                  <Point X="-4.775654785156" Y="-0.730151184082" />
                  <Point X="-4.786452636719" Y="-0.654654418945" />
                  <Point X="-4.210021972656" Y="-0.500200195313" />
                  <Point X="-3.508288085938" Y="-0.312171417236" />
                  <Point X="-3.496792480469" Y="-0.308288970947" />
                  <Point X="-3.474390869141" Y="-0.299090820312" />
                  <Point X="-3.463484863281" Y="-0.293775482178" />
                  <Point X="-3.434791259766" Y="-0.277445404053" />
                  <Point X="-3.427987792969" Y="-0.273183044434" />
                  <Point X="-3.408347412109" Y="-0.259278930664" />
                  <Point X="-3.381901367188" Y="-0.237608810425" />
                  <Point X="-3.373277587891" Y="-0.229599960327" />
                  <Point X="-3.357085693359" Y="-0.212576553345" />
                  <Point X="-3.343122558594" Y="-0.193680938721" />
                  <Point X="-3.331603759766" Y="-0.173204406738" />
                  <Point X="-3.326479980469" Y="-0.162609085083" />
                  <Point X="-3.314679443359" Y="-0.133774307251" />
                  <Point X="-3.312095947266" Y="-0.126667312622" />
                  <Point X="-3.305492431641" Y="-0.104978477478" />
                  <Point X="-3.298369628906" Y="-0.074448059082" />
                  <Point X="-3.29651953125" Y="-0.063823009491" />
                  <Point X="-3.294035400391" Y="-0.042432609558" />
                  <Point X="-3.293991455078" Y="-0.020899082184" />
                  <Point X="-3.296388427734" Y="0.000501130044" />
                  <Point X="-3.2981953125" Y="0.011133315086" />
                  <Point X="-3.304817626953" Y="0.040050872803" />
                  <Point X="-3.306791992188" Y="0.047331016541" />
                  <Point X="-3.313857177734" Y="0.068809555054" />
                  <Point X="-3.326158203125" Y="0.09925718689" />
                  <Point X="-3.331234130859" Y="0.10987525177" />
                  <Point X="-3.342660644531" Y="0.130403335571" />
                  <Point X="-3.356538085938" Y="0.149361236572" />
                  <Point X="-3.372653076172" Y="0.166457473755" />
                  <Point X="-3.381241455078" Y="0.174505706787" />
                  <Point X="-3.406185546875" Y="0.195133224487" />
                  <Point X="-3.412575439453" Y="0.19997833252" />
                  <Point X="-3.432517333984" Y="0.213401062012" />
                  <Point X="-3.462712890625" Y="0.230773742676" />
                  <Point X="-3.473799804688" Y="0.236225524902" />
                  <Point X="-3.496587402344" Y="0.245644195557" />
                  <Point X="-3.508288085938" Y="0.249611251831" />
                  <Point X="-4.2903203125" Y="0.459156158447" />
                  <Point X="-4.785446289062" Y="0.591824707031" />
                  <Point X="-4.775375976562" Y="0.659877502441" />
                  <Point X="-4.731331054688" Y="0.95752947998" />
                  <Point X="-4.661091796875" Y="1.216734008789" />
                  <Point X="-4.633586425781" Y="1.318237060547" />
                  <Point X="-4.281086425781" Y="1.271829589844" />
                  <Point X="-3.778066162109" Y="1.20560559082" />
                  <Point X="-3.76773828125" Y="1.204815429688" />
                  <Point X="-3.747057861328" Y="1.204364257812" />
                  <Point X="-3.736705322266" Y="1.204703125" />
                  <Point X="-3.715143554688" Y="1.20658984375" />
                  <Point X="-3.704890380859" Y="1.208053710938" />
                  <Point X="-3.684603759766" Y="1.212088989258" />
                  <Point X="-3.6745703125" Y="1.214660522461" />
                  <Point X="-3.64551953125" Y="1.223820068359" />
                  <Point X="-3.613144287109" Y="1.234027954102" />
                  <Point X="-3.603450439453" Y="1.237676513672" />
                  <Point X="-3.584517578125" Y="1.246007324219" />
                  <Point X="-3.575278564453" Y="1.250689331055" />
                  <Point X="-3.556534423828" Y="1.261511108398" />
                  <Point X="-3.547859863281" Y="1.267171630859" />
                  <Point X="-3.531178710938" Y="1.279402832031" />
                  <Point X="-3.515928222656" Y="1.293377197266" />
                  <Point X="-3.502289794922" Y="1.308929199219" />
                  <Point X="-3.495895263672" Y="1.317077148438" />
                  <Point X="-3.483480957031" Y="1.334806762695" />
                  <Point X="-3.478010986328" Y="1.343603149414" />
                  <Point X="-3.468062011719" Y="1.36173815918" />
                  <Point X="-3.463583007813" Y="1.371076660156" />
                  <Point X="-3.451926269531" Y="1.39921875" />
                  <Point X="-3.438935546875" Y="1.430581054688" />
                  <Point X="-3.435499511719" Y="1.440351074219" />
                  <Point X="-3.429711425781" Y="1.460208496094" />
                  <Point X="-3.427359375" Y="1.470295898438" />
                  <Point X="-3.423600830078" Y="1.491611083984" />
                  <Point X="-3.422360839844" Y="1.501894042969" />
                  <Point X="-3.4210078125" Y="1.522534790039" />
                  <Point X="-3.42191015625" Y="1.543200927734" />
                  <Point X="-3.425056884766" Y="1.563645019531" />
                  <Point X="-3.427188232422" Y="1.573780517578" />
                  <Point X="-3.432790039062" Y="1.594686889648" />
                  <Point X="-3.436011962891" Y="1.604530395508" />
                  <Point X="-3.443508789062" Y="1.62380871582" />
                  <Point X="-3.447783691406" Y="1.633243286133" />
                  <Point X="-3.461848876953" Y="1.660262573242" />
                  <Point X="-3.4775234375" Y="1.690373046875" />
                  <Point X="-3.482799560547" Y="1.699286010742" />
                  <Point X="-3.494291259766" Y="1.716484741211" />
                  <Point X="-3.500506835938" Y="1.724770629883" />
                  <Point X="-3.514419433594" Y="1.741351074219" />
                  <Point X="-3.521500732422" Y="1.748911621094" />
                  <Point X="-3.536442871094" Y="1.763215454102" />
                  <Point X="-3.544303710938" Y="1.769958618164" />
                  <Point X="-3.992879394531" Y="2.114163085938" />
                  <Point X="-4.227614257812" Y="2.294282226563" />
                  <Point X="-4.17343359375" Y="2.387107421875" />
                  <Point X="-4.002293212891" Y="2.680312011719" />
                  <Point X="-3.816236083984" Y="2.919461914062" />
                  <Point X="-3.726338623047" Y="3.035012451172" />
                  <Point X="-3.563097900391" Y="2.940765625" />
                  <Point X="-3.254156982422" Y="2.7623984375" />
                  <Point X="-3.244917480469" Y="2.757716064453" />
                  <Point X="-3.225984619141" Y="2.749385498047" />
                  <Point X="-3.216291259766" Y="2.745737304688" />
                  <Point X="-3.195648925781" Y="2.739228759766" />
                  <Point X="-3.185615234375" Y="2.736657226562" />
                  <Point X="-3.165327880859" Y="2.732621826172" />
                  <Point X="-3.15507421875" Y="2.731157958984" />
                  <Point X="-3.114614501953" Y="2.727618164062" />
                  <Point X="-3.069524902344" Y="2.723673339844" />
                  <Point X="-3.059173095703" Y="2.723334472656" />
                  <Point X="-3.038492919922" Y="2.723785644531" />
                  <Point X="-3.028164550781" Y="2.724575683594" />
                  <Point X="-3.006705810547" Y="2.727400878906" />
                  <Point X="-2.996524902344" Y="2.729310791016" />
                  <Point X="-2.976432861328" Y="2.734227539062" />
                  <Point X="-2.956996826172" Y="2.741302001953" />
                  <Point X="-2.938445068359" Y="2.750450683594" />
                  <Point X="-2.929418212891" Y="2.755531738281" />
                  <Point X="-2.9111640625" Y="2.767161132812" />
                  <Point X="-2.902745117188" Y="2.773194091797" />
                  <Point X="-2.886614501953" Y="2.786140380859" />
                  <Point X="-2.878902832031" Y="2.793053710938" />
                  <Point X="-2.850184082031" Y="2.821772216797" />
                  <Point X="-2.818179199219" Y="2.853777099609" />
                  <Point X="-2.811265625" Y="2.861489257812" />
                  <Point X="-2.798318603516" Y="2.877620605469" />
                  <Point X="-2.79228515625" Y="2.886039794922" />
                  <Point X="-2.780655761719" Y="2.904293945312" />
                  <Point X="-2.775574707031" Y="2.913320800781" />
                  <Point X="-2.766426025391" Y="2.931872558594" />
                  <Point X="-2.7593515625" Y="2.95130859375" />
                  <Point X="-2.754434814453" Y="2.971400634766" />
                  <Point X="-2.752524902344" Y="2.981581542969" />
                  <Point X="-2.749699707031" Y="3.003040283203" />
                  <Point X="-2.748909667969" Y="3.013368652344" />
                  <Point X="-2.748458496094" Y="3.034048828125" />
                  <Point X="-2.748797363281" Y="3.044400634766" />
                  <Point X="-2.752337158203" Y="3.084860595703" />
                  <Point X="-2.756281982422" Y="3.129950195312" />
                  <Point X="-2.757745849609" Y="3.140203857422" />
                  <Point X="-2.76178125" Y="3.160491210938" />
                  <Point X="-2.764352783203" Y="3.170524902344" />
                  <Point X="-2.770861328125" Y="3.191167236328" />
                  <Point X="-2.774509521484" Y="3.200860595703" />
                  <Point X="-2.782840087891" Y="3.219793457031" />
                  <Point X="-2.787522460938" Y="3.229032958984" />
                  <Point X="-2.986300048828" Y="3.573325439453" />
                  <Point X="-3.059387207031" Y="3.699916259766" />
                  <Point X="-2.946440673828" Y="3.786511230469" />
                  <Point X="-2.648374023438" Y="4.015036376953" />
                  <Point X="-2.355345458984" Y="4.177836914063" />
                  <Point X="-2.192525146484" Y="4.268296386719" />
                  <Point X="-2.118563964844" Y="4.171908691406" />
                  <Point X="-2.111819824219" Y="4.164046875" />
                  <Point X="-2.097516357422" Y="4.14910546875" />
                  <Point X="-2.089957275391" Y="4.142025390625" />
                  <Point X="-2.073377685547" Y="4.12811328125" />
                  <Point X="-2.065091308594" Y="4.121897460938" />
                  <Point X="-2.047892089844" Y="4.110405273438" />
                  <Point X="-2.038979003906" Y="4.10512890625" />
                  <Point X="-1.993947387695" Y="4.081687011719" />
                  <Point X="-1.943762939453" Y="4.055562011719" />
                  <Point X="-1.934328125" Y="4.051287109375" />
                  <Point X="-1.915050415039" Y="4.043790527344" />
                  <Point X="-1.905207397461" Y="4.040568603516" />
                  <Point X="-1.884300537109" Y="4.034966552734" />
                  <Point X="-1.874165893555" Y="4.032835449219" />
                  <Point X="-1.853722290039" Y="4.029688476562" />
                  <Point X="-1.8330546875" Y="4.028785888672" />
                  <Point X="-1.812414306641" Y="4.030138916016" />
                  <Point X="-1.802132568359" Y="4.031378662109" />
                  <Point X="-1.780817138672" Y="4.035136962891" />
                  <Point X="-1.770730957031" Y="4.037488525391" />
                  <Point X="-1.750871582031" Y="4.043276611328" />
                  <Point X="-1.741098388672" Y="4.046713378906" />
                  <Point X="-1.694194946289" Y="4.066141601563" />
                  <Point X="-1.641924438477" Y="4.08779296875" />
                  <Point X="-1.632585693359" Y="4.092272460938" />
                  <Point X="-1.614450683594" Y="4.102221679688" />
                  <Point X="-1.605654541016" Y="4.10769140625" />
                  <Point X="-1.587924804688" Y="4.120105957031" />
                  <Point X="-1.579776245117" Y="4.126501464844" />
                  <Point X="-1.564224609375" Y="4.140140136719" />
                  <Point X="-1.550250976562" Y="4.155390136719" />
                  <Point X="-1.538020019531" Y="4.172071289062" />
                  <Point X="-1.532359863281" Y="4.180745605469" />
                  <Point X="-1.521538085938" Y="4.199489746094" />
                  <Point X="-1.516856201172" Y="4.208728515625" />
                  <Point X="-1.508525878906" Y="4.227660644531" />
                  <Point X="-1.504877441406" Y="4.237354003906" />
                  <Point X="-1.489611083984" Y="4.285771972656" />
                  <Point X="-1.472598144531" Y="4.33973046875" />
                  <Point X="-1.470026733398" Y="4.349763671875" />
                  <Point X="-1.465991210938" Y="4.37005078125" />
                  <Point X="-1.46452722168" Y="4.3803046875" />
                  <Point X="-1.46264074707" Y="4.401866210938" />
                  <Point X="-1.462301757812" Y="4.41221875" />
                  <Point X="-1.462752929688" Y="4.432898925781" />
                  <Point X="-1.46354309082" Y="4.4432265625" />
                  <Point X="-1.479266113281" Y="4.562654785156" />
                  <Point X="-1.317040649414" Y="4.608137207031" />
                  <Point X="-0.931174865723" Y="4.716320800781" />
                  <Point X="-0.575946716309" Y="4.75789453125" />
                  <Point X="-0.365221862793" Y="4.782556640625" />
                  <Point X="-0.317059448242" Y="4.602812011719" />
                  <Point X="-0.22566633606" Y="4.261727539062" />
                  <Point X="-0.220435256958" Y="4.247107910156" />
                  <Point X="-0.207661849976" Y="4.218916503906" />
                  <Point X="-0.200119247437" Y="4.205344726562" />
                  <Point X="-0.182260864258" Y="4.178618164062" />
                  <Point X="-0.17260824585" Y="4.166456054688" />
                  <Point X="-0.151451339722" Y="4.1438671875" />
                  <Point X="-0.126897941589" Y="4.125026367188" />
                  <Point X="-0.099602897644" Y="4.110436523438" />
                  <Point X="-0.085356651306" Y="4.104260742188" />
                  <Point X="-0.054918682098" Y="4.093927978516" />
                  <Point X="-0.039857234955" Y="4.090155273438" />
                  <Point X="-0.009319986343" Y="4.08511328125" />
                  <Point X="0.021631772995" Y="4.08511328125" />
                  <Point X="0.052169025421" Y="4.090155273438" />
                  <Point X="0.06723046875" Y="4.093927978516" />
                  <Point X="0.097668441772" Y="4.104260742188" />
                  <Point X="0.111914535522" Y="4.110436523438" />
                  <Point X="0.139209732056" Y="4.125026367188" />
                  <Point X="0.163763122559" Y="4.143866699219" />
                  <Point X="0.184920181274" Y="4.166455566406" />
                  <Point X="0.194572952271" Y="4.178617675781" />
                  <Point X="0.212431182861" Y="4.205344238281" />
                  <Point X="0.2199737854" Y="4.218916503906" />
                  <Point X="0.232747192383" Y="4.247107910156" />
                  <Point X="0.237978271484" Y="4.261727539062" />
                  <Point X="0.339829406738" Y="4.641841796875" />
                  <Point X="0.378190185547" Y="4.785006347656" />
                  <Point X="0.490948852539" Y="4.773197265625" />
                  <Point X="0.827876464844" Y="4.737912109375" />
                  <Point X="1.121777587891" Y="4.666955078125" />
                  <Point X="1.45359777832" Y="4.586843261719" />
                  <Point X="1.643145019531" Y="4.518093261719" />
                  <Point X="1.858256347656" Y="4.440070800781" />
                  <Point X="2.043240722656" Y="4.353559570312" />
                  <Point X="2.250451904297" Y="4.256653808594" />
                  <Point X="2.429208007812" Y="4.152510253906" />
                  <Point X="2.629432861328" Y="4.035858398438" />
                  <Point X="2.797977783203" Y="3.915998779297" />
                  <Point X="2.817779785156" Y="3.901916748047" />
                  <Point X="2.472807617188" Y="3.304406982422" />
                  <Point X="2.06530859375" Y="2.598597900391" />
                  <Point X="2.062373046875" Y="2.593104980469" />
                  <Point X="2.053181396484" Y="2.573438964844" />
                  <Point X="2.044182250977" Y="2.549564453125" />
                  <Point X="2.041301391602" Y="2.540598388672" />
                  <Point X="2.031147583008" Y="2.502627685547" />
                  <Point X="2.019831665039" Y="2.460312011719" />
                  <Point X="2.017912231445" Y="2.451464355469" />
                  <Point X="2.013646972656" Y="2.420223388672" />
                  <Point X="2.012755615234" Y="2.383241943359" />
                  <Point X="2.013411254883" Y="2.369579833984" />
                  <Point X="2.017370361328" Y="2.33674609375" />
                  <Point X="2.021782592773" Y="2.300155029297" />
                  <Point X="2.023800537109" Y="2.28903515625" />
                  <Point X="2.029143066406" Y="2.267112304688" />
                  <Point X="2.032468017578" Y="2.256309326172" />
                  <Point X="2.040734130859" Y="2.234220458984" />
                  <Point X="2.045318481445" Y="2.223888183594" />
                  <Point X="2.055681640625" Y="2.203843017578" />
                  <Point X="2.061459960938" Y="2.194130126953" />
                  <Point X="2.081776367188" Y="2.164188964844" />
                  <Point X="2.104417724609" Y="2.130821533203" />
                  <Point X="2.109808105469" Y="2.123633789062" />
                  <Point X="2.130463623047" Y="2.100123046875" />
                  <Point X="2.15759765625" Y="2.075386962891" />
                  <Point X="2.1682578125" Y="2.066981445312" />
                  <Point X="2.198198974609" Y="2.046665039062" />
                  <Point X="2.23156640625" Y="2.024023803711" />
                  <Point X="2.241279785156" Y="2.018245117188" />
                  <Point X="2.261325195312" Y="2.007882080078" />
                  <Point X="2.271657226562" Y="2.003297851562" />
                  <Point X="2.293745605469" Y="1.995032104492" />
                  <Point X="2.304547119141" Y="1.991707641602" />
                  <Point X="2.326469726562" Y="1.986364868164" />
                  <Point X="2.337590820312" Y="1.984346557617" />
                  <Point X="2.370424804688" Y="1.980387329102" />
                  <Point X="2.407015625" Y="1.975974975586" />
                  <Point X="2.416045410156" Y="1.975320678711" />
                  <Point X="2.447575439453" Y="1.975497314453" />
                  <Point X="2.484314453125" Y="1.979822753906" />
                  <Point X="2.497748779297" Y="1.982395996094" />
                  <Point X="2.535719482422" Y="1.992550048828" />
                  <Point X="2.57803515625" Y="2.003865722656" />
                  <Point X="2.583995361328" Y="2.005670776367" />
                  <Point X="2.604404541016" Y="2.013067260742" />
                  <Point X="2.627655029297" Y="2.02357409668" />
                  <Point X="2.636033935547" Y="2.027872558594" />
                  <Point X="3.422607910156" Y="2.482001220703" />
                  <Point X="3.940404785156" Y="2.780951416016" />
                  <Point X="4.043950927734" Y="2.637045898438" />
                  <Point X="4.136884765625" Y="2.483472412109" />
                  <Point X="3.709462402344" Y="2.155499267578" />
                  <Point X="3.172951660156" Y="1.743819702148" />
                  <Point X="3.168138427734" Y="1.739869262695" />
                  <Point X="3.152119384766" Y="1.725217041016" />
                  <Point X="3.134668701172" Y="1.706603515625" />
                  <Point X="3.128576416016" Y="1.699422485352" />
                  <Point X="3.101248779297" Y="1.663771484375" />
                  <Point X="3.070794189453" Y="1.624041015625" />
                  <Point X="3.065635986328" Y="1.616602905273" />
                  <Point X="3.049739013672" Y="1.589371704102" />
                  <Point X="3.034762939453" Y="1.555546020508" />
                  <Point X="3.030140380859" Y="1.542672485352" />
                  <Point X="3.019960693359" Y="1.506272827148" />
                  <Point X="3.008616455078" Y="1.465707885742" />
                  <Point X="3.006225097656" Y="1.454661132812" />
                  <Point X="3.002771484375" Y="1.432361450195" />
                  <Point X="3.001709228516" Y="1.421108520508" />
                  <Point X="3.000893310547" Y="1.397538818359" />
                  <Point X="3.001174804688" Y="1.386239868164" />
                  <Point X="3.003077880859" Y="1.363755859375" />
                  <Point X="3.004699462891" Y="1.352570678711" />
                  <Point X="3.013055908203" Y="1.312071533203" />
                  <Point X="3.022368408203" Y="1.266937866211" />
                  <Point X="3.024597900391" Y="1.258235107422" />
                  <Point X="3.034683349609" Y="1.22860949707" />
                  <Point X="3.050286132812" Y="1.195371459961" />
                  <Point X="3.056918457031" Y="1.183525756836" />
                  <Point X="3.079646728516" Y="1.148979736328" />
                  <Point X="3.104976074219" Y="1.11048059082" />
                  <Point X="3.111739013672" Y="1.101424560547" />
                  <Point X="3.126292480469" Y="1.08417980957" />
                  <Point X="3.134083007812" Y="1.075991333008" />
                  <Point X="3.151326904297" Y="1.059901367188" />
                  <Point X="3.160034667969" Y="1.052695922852" />
                  <Point X="3.178244384766" Y="1.039370117188" />
                  <Point X="3.187746337891" Y="1.033249755859" />
                  <Point X="3.220682861328" Y="1.014709289551" />
                  <Point X="3.257388427734" Y="0.994047424316" />
                  <Point X="3.265479492188" Y="0.989987792969" />
                  <Point X="3.294678710938" Y="0.97808416748" />
                  <Point X="3.330275146484" Y="0.968021179199" />
                  <Point X="3.343670654297" Y="0.965257568359" />
                  <Point X="3.388203125" Y="0.959371948242" />
                  <Point X="3.437831298828" Y="0.952812927246" />
                  <Point X="3.444030029297" Y="0.952199707031" />
                  <Point X="3.465716308594" Y="0.951222961426" />
                  <Point X="3.491217529297" Y="0.952032348633" />
                  <Point X="3.500603515625" Y="0.952797180176" />
                  <Point X="4.247796386719" Y="1.051166992188" />
                  <Point X="4.704703613281" Y="1.111320068359" />
                  <Point X="4.75268359375" Y="0.914233764648" />
                  <Point X="4.782293457031" Y="0.724052856445" />
                  <Point X="4.78387109375" Y="0.713920898438" />
                  <Point X="4.306844726562" Y="0.586101989746" />
                  <Point X="3.691991943359" Y="0.421352722168" />
                  <Point X="3.686031738281" Y="0.419544433594" />
                  <Point X="3.665627197266" Y="0.412138122559" />
                  <Point X="3.642381591797" Y="0.401619628906" />
                  <Point X="3.634004394531" Y="0.397316558838" />
                  <Point X="3.590252685547" Y="0.372027313232" />
                  <Point X="3.541494384766" Y="0.343844116211" />
                  <Point X="3.533881835938" Y="0.338945800781" />
                  <Point X="3.508773925781" Y="0.319870483398" />
                  <Point X="3.481993652344" Y="0.294350738525" />
                  <Point X="3.472796630859" Y="0.284226409912" />
                  <Point X="3.446545654297" Y="0.250776443481" />
                  <Point X="3.417290771484" Y="0.213498855591" />
                  <Point X="3.410854492188" Y="0.204208282471" />
                  <Point X="3.399130615234" Y="0.184928619385" />
                  <Point X="3.393843017578" Y="0.174939529419" />
                  <Point X="3.384069091797" Y="0.153475708008" />
                  <Point X="3.380005126953" Y="0.142928390503" />
                  <Point X="3.373158935547" Y="0.121427116394" />
                  <Point X="3.370376708984" Y="0.110473304749" />
                  <Point X="3.361626220703" Y="0.064782463074" />
                  <Point X="3.351874511719" Y="0.013863072395" />
                  <Point X="3.350603759766" Y="0.004969180107" />
                  <Point X="3.348584228516" Y="-0.026262733459" />
                  <Point X="3.350280029297" Y="-0.062941379547" />
                  <Point X="3.351874511719" Y="-0.076422966003" />
                  <Point X="3.360625" Y="-0.122113952637" />
                  <Point X="3.370376708984" Y="-0.173033493042" />
                  <Point X="3.373158935547" Y="-0.183987594604" />
                  <Point X="3.380005126953" Y="-0.205488723755" />
                  <Point X="3.384069091797" Y="-0.216035751343" />
                  <Point X="3.393843017578" Y="-0.237499420166" />
                  <Point X="3.399130615234" Y="-0.24748866272" />
                  <Point X="3.410854492188" Y="-0.266768310547" />
                  <Point X="3.417290771484" Y="-0.276058898926" />
                  <Point X="3.443541748047" Y="-0.309508850098" />
                  <Point X="3.472796630859" Y="-0.346786437988" />
                  <Point X="3.478718017578" Y="-0.353633209229" />
                  <Point X="3.501139404297" Y="-0.375805206299" />
                  <Point X="3.530176025391" Y="-0.398724639893" />
                  <Point X="3.541494384766" Y="-0.406404144287" />
                  <Point X="3.58524609375" Y="-0.431693389893" />
                  <Point X="3.634004394531" Y="-0.459876586914" />
                  <Point X="3.639496582031" Y="-0.462815460205" />
                  <Point X="3.659157958984" Y="-0.472016693115" />
                  <Point X="3.683027587891" Y="-0.481027709961" />
                  <Point X="3.691991943359" Y="-0.48391293335" />
                  <Point X="4.377201660156" Y="-0.667514221191" />
                  <Point X="4.784876953125" Y="-0.776750488281" />
                  <Point X="4.761613769531" Y="-0.931051208496" />
                  <Point X="4.727801757812" Y="-1.079219604492" />
                  <Point X="4.149838378906" Y="-1.003129272461" />
                  <Point X="3.436781982422" Y="-0.909253662109" />
                  <Point X="3.428624511719" Y="-0.908535827637" />
                  <Point X="3.400098388672" Y="-0.908042419434" />
                  <Point X="3.366721435547" Y="-0.910840820312" />
                  <Point X="3.354481201172" Y="-0.912676147461" />
                  <Point X="3.268612060547" Y="-0.931340087891" />
                  <Point X="3.172916992188" Y="-0.952139831543" />
                  <Point X="3.157873779297" Y="-0.956742553711" />
                  <Point X="3.128753417969" Y="-0.968367126465" />
                  <Point X="3.114676269531" Y="-0.975388916016" />
                  <Point X="3.086849609375" Y="-0.99228125" />
                  <Point X="3.074124023438" Y="-1.001530395508" />
                  <Point X="3.050374023438" Y="-1.022001098633" />
                  <Point X="3.039349609375" Y="-1.033222900391" />
                  <Point X="2.987447021484" Y="-1.095645507812" />
                  <Point X="2.92960546875" Y="-1.16521105957" />
                  <Point X="2.921326171875" Y="-1.17684753418" />
                  <Point X="2.90660546875" Y="-1.201229858398" />
                  <Point X="2.9001640625" Y="-1.213975952148" />
                  <Point X="2.888820800781" Y="-1.241361206055" />
                  <Point X="2.884362792969" Y="-1.254928222656" />
                  <Point X="2.877531005859" Y="-1.282577758789" />
                  <Point X="2.875157226562" Y="-1.29666027832" />
                  <Point X="2.867718261719" Y="-1.37750012207" />
                  <Point X="2.859428222656" Y="-1.467590576172" />
                  <Point X="2.859288818359" Y="-1.483321533203" />
                  <Point X="2.861607666016" Y="-1.514590454102" />
                  <Point X="2.864065917969" Y="-1.530128540039" />
                  <Point X="2.871797607422" Y="-1.561749511719" />
                  <Point X="2.876786376953" Y="-1.576668945312" />
                  <Point X="2.889157714844" Y="-1.605479858398" />
                  <Point X="2.896540283203" Y="-1.619371337891" />
                  <Point X="2.944061523438" Y="-1.693287597656" />
                  <Point X="2.997020507812" Y="-1.775661865234" />
                  <Point X="3.001741699219" Y="-1.782352783203" />
                  <Point X="3.01979296875" Y="-1.80444921875" />
                  <Point X="3.043489013672" Y="-1.828119995117" />
                  <Point X="3.052796142578" Y="-1.836277587891" />
                  <Point X="3.688675048828" Y="-2.324205078125" />
                  <Point X="4.087170898438" Y="-2.629981689453" />
                  <Point X="4.045488525391" Y="-2.697430419922" />
                  <Point X="4.001274414063" Y="-2.760251953125" />
                  <Point X="3.48352734375" Y="-2.461330322266" />
                  <Point X="2.848454589844" Y="-2.094670654297" />
                  <Point X="2.841191894531" Y="-2.090885253906" />
                  <Point X="2.815026123047" Y="-2.079513183594" />
                  <Point X="2.783119140625" Y="-2.069325927734" />
                  <Point X="2.771108398438" Y="-2.066337646484" />
                  <Point X="2.668910400391" Y="-2.047880737305" />
                  <Point X="2.555018066406" Y="-2.027312011719" />
                  <Point X="2.539359130859" Y="-2.025807495117" />
                  <Point X="2.508006835938" Y="-2.025403198242" />
                  <Point X="2.492313476562" Y="-2.026503662109" />
                  <Point X="2.460140625" Y="-2.031461425781" />
                  <Point X="2.444844482422" Y="-2.035136352539" />
                  <Point X="2.415068847656" Y="-2.044960083008" />
                  <Point X="2.400589355469" Y="-2.051108642578" />
                  <Point X="2.315687744141" Y="-2.095791503906" />
                  <Point X="2.221071289062" Y="-2.145587646484" />
                  <Point X="2.208968994141" Y="-2.153169921875" />
                  <Point X="2.186037597656" Y="-2.170063232422" />
                  <Point X="2.175208496094" Y="-2.179374267578" />
                  <Point X="2.154248779297" Y="-2.200334228516" />
                  <Point X="2.144937988281" Y="-2.211163330078" />
                  <Point X="2.128045654297" Y="-2.23409375" />
                  <Point X="2.120464111328" Y="-2.246195068359" />
                  <Point X="2.075781005859" Y="-2.331096435547" />
                  <Point X="2.025984985352" Y="-2.425713134766" />
                  <Point X="2.019836303711" Y="-2.440192382812" />
                  <Point X="2.010012329102" Y="-2.469968261719" />
                  <Point X="2.006337280273" Y="-2.485264892578" />
                  <Point X="2.001379516602" Y="-2.5174375" />
                  <Point X="2.000279174805" Y="-2.533130859375" />
                  <Point X="2.00068347168" Y="-2.564483154297" />
                  <Point X="2.002187866211" Y="-2.580142089844" />
                  <Point X="2.020644775391" Y="-2.682340087891" />
                  <Point X="2.041213500977" Y="-2.796232421875" />
                  <Point X="2.043015014648" Y="-2.804221435547" />
                  <Point X="2.051236083984" Y="-2.83154296875" />
                  <Point X="2.064069580078" Y="-2.862479736328" />
                  <Point X="2.069546630859" Y="-2.873578613281" />
                  <Point X="2.478162841797" Y="-3.581322265625" />
                  <Point X="2.735893066406" Y="-4.027724609375" />
                  <Point X="2.723754150391" Y="-4.036083496094" />
                  <Point X="2.318534179688" Y="-3.507990722656" />
                  <Point X="1.833914550781" Y="-2.876422851562" />
                  <Point X="1.828653564453" Y="-2.870146240234" />
                  <Point X="1.808831665039" Y="-2.849626953125" />
                  <Point X="1.783252319336" Y="-2.828004882812" />
                  <Point X="1.773299072266" Y="-2.820647216797" />
                  <Point X="1.672504394531" Y="-2.755845458984" />
                  <Point X="1.56017565918" Y="-2.683628417969" />
                  <Point X="1.546283935547" Y="-2.676245849609" />
                  <Point X="1.517473022461" Y="-2.663874511719" />
                  <Point X="1.502553344727" Y="-2.658885742188" />
                  <Point X="1.470932006836" Y="-2.651154052734" />
                  <Point X="1.455394165039" Y="-2.648695800781" />
                  <Point X="1.424125244141" Y="-2.646376953125" />
                  <Point X="1.408394287109" Y="-2.646516357422" />
                  <Point X="1.298158081055" Y="-2.656660400391" />
                  <Point X="1.175307373047" Y="-2.667965332031" />
                  <Point X="1.161225097656" Y="-2.670339111328" />
                  <Point X="1.133575317383" Y="-2.677170898438" />
                  <Point X="1.120007568359" Y="-2.68162890625" />
                  <Point X="1.092622680664" Y="-2.692972167969" />
                  <Point X="1.079876708984" Y="-2.699413574219" />
                  <Point X="1.055494873047" Y="-2.714134033203" />
                  <Point X="1.043858764648" Y="-2.722413330078" />
                  <Point X="0.958737182617" Y="-2.793189208984" />
                  <Point X="0.863874938965" Y="-2.872063720703" />
                  <Point X="0.852653015137" Y="-2.883088378906" />
                  <Point X="0.832182312012" Y="-2.906838378906" />
                  <Point X="0.822933654785" Y="-2.919563720703" />
                  <Point X="0.806041137695" Y="-2.947390380859" />
                  <Point X="0.799019287109" Y="-2.961467529297" />
                  <Point X="0.787394470215" Y="-2.990588134766" />
                  <Point X="0.782791931152" Y="-3.005631347656" />
                  <Point X="0.757340820312" Y="-3.122725830078" />
                  <Point X="0.728977661133" Y="-3.253219238281" />
                  <Point X="0.727584777832" Y="-3.261289306641" />
                  <Point X="0.724724487305" Y="-3.289677734375" />
                  <Point X="0.7247421875" Y="-3.323170410156" />
                  <Point X="0.725554992676" Y="-3.335520019531" />
                  <Point X="0.833091674805" Y="-4.152341796875" />
                  <Point X="0.655064941406" Y="-3.487936523438" />
                  <Point X="0.652606262207" Y="-3.480124511719" />
                  <Point X="0.642145141602" Y="-3.453580566406" />
                  <Point X="0.626787231445" Y="-3.423815917969" />
                  <Point X="0.620407592773" Y="-3.413210205078" />
                  <Point X="0.542974304199" Y="-3.301643066406" />
                  <Point X="0.456679992676" Y="-3.177309814453" />
                  <Point X="0.446670684814" Y="-3.165172851562" />
                  <Point X="0.424786712646" Y="-3.142717529297" />
                  <Point X="0.412912322998" Y="-3.132399169922" />
                  <Point X="0.386657196045" Y="-3.113155273438" />
                  <Point X="0.373242797852" Y="-3.104937988281" />
                  <Point X="0.345241485596" Y="-3.090829589844" />
                  <Point X="0.330654602051" Y="-3.084938476562" />
                  <Point X="0.210830810547" Y="-3.047749511719" />
                  <Point X="0.077295654297" Y="-3.006305175781" />
                  <Point X="0.063376529694" Y="-3.003109375" />
                  <Point X="0.035217094421" Y="-2.998840087891" />
                  <Point X="0.020976791382" Y="-2.997766601562" />
                  <Point X="-0.008664855957" Y="-2.997766601562" />
                  <Point X="-0.022905158997" Y="-2.998840087891" />
                  <Point X="-0.051064590454" Y="-3.003109375" />
                  <Point X="-0.064983573914" Y="-3.006305175781" />
                  <Point X="-0.184807373047" Y="-3.043493896484" />
                  <Point X="-0.318342529297" Y="-3.084938476562" />
                  <Point X="-0.332929412842" Y="-3.090829589844" />
                  <Point X="-0.360930847168" Y="-3.104937988281" />
                  <Point X="-0.374345275879" Y="-3.113155273438" />
                  <Point X="-0.400600524902" Y="-3.132399169922" />
                  <Point X="-0.412475219727" Y="-3.142717773438" />
                  <Point X="-0.434358886719" Y="-3.165173095703" />
                  <Point X="-0.444367889404" Y="-3.177309814453" />
                  <Point X="-0.521801635742" Y="-3.288876708984" />
                  <Point X="-0.60809564209" Y="-3.413210205078" />
                  <Point X="-0.612470336914" Y="-3.420132324219" />
                  <Point X="-0.625976745605" Y="-3.445265136719" />
                  <Point X="-0.638777832031" Y="-3.476215820312" />
                  <Point X="-0.642752990723" Y="-3.487936523438" />
                  <Point X="-0.853882995605" Y="-4.275884277344" />
                  <Point X="-0.985425109863" Y="-4.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.63557800146" Y="-1.067078115086" />
                  <Point X="4.702008749085" Y="-0.754546019612" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.541099494248" Y="-1.05463978206" />
                  <Point X="4.610119865137" Y="-0.729924467012" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.446620987037" Y="-1.042201449035" />
                  <Point X="4.518230981188" Y="-0.705302914412" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.986169404517" Y="-2.751531065366" />
                  <Point X="4.022545886107" Y="-2.5803931748" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.352142479826" Y="-1.029763116009" />
                  <Point X="4.42634209724" Y="-0.680681361812" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.719084051854" Y="0.696561252175" />
                  <Point X="4.758039602365" Y="0.879832708042" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.899663076745" Y="-2.701586576909" />
                  <Point X="3.939042898706" Y="-2.516319080808" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.257663972614" Y="-1.017324782983" />
                  <Point X="4.334453211973" Y="-0.656059815413" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.616096077665" Y="0.66896569064" />
                  <Point X="4.707596010801" Y="1.09943903109" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.813156748974" Y="-2.651642088452" />
                  <Point X="3.855539911305" Y="-2.452244986816" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.163185465403" Y="-1.004886449957" />
                  <Point X="4.242564325191" Y="-0.631438276141" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.513108103475" Y="0.641370129106" />
                  <Point X="4.610358938542" Y="1.09889933593" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.726650421203" Y="-2.601697599994" />
                  <Point X="3.772036923904" Y="-2.388170892824" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.068706956848" Y="-0.992448123253" />
                  <Point X="4.15067543841" Y="-0.606816736869" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.410120129286" Y="0.613774567572" />
                  <Point X="4.510440499595" Y="1.085744802319" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.640144093431" Y="-2.551753111537" />
                  <Point X="3.688533936521" Y="-2.324096798745" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.974228448072" Y="-0.980009797589" />
                  <Point X="4.058786551628" Y="-0.582195197598" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.307132155097" Y="0.586179006037" />
                  <Point X="4.410522060648" Y="1.072590268708" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.55363776566" Y="-2.501808623079" />
                  <Point X="3.605030960117" Y="-2.260022653017" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.879749939296" Y="-0.967571471925" />
                  <Point X="3.966897664847" Y="-0.557573658326" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.204144186207" Y="0.558583469434" />
                  <Point X="4.310603621701" Y="1.059435735097" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.467131437753" Y="-2.451864135263" />
                  <Point X="3.521527983712" Y="-2.19594850729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.78527143052" Y="-0.95513314626" />
                  <Point X="3.875008778065" Y="-0.532952119054" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.101156217332" Y="0.530987932901" />
                  <Point X="4.210685184459" Y="1.046281209505" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.380625109263" Y="-2.401919650184" />
                  <Point X="3.438025007308" Y="-2.131874361562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.690792921744" Y="-0.942694820596" />
                  <Point X="3.783119891284" Y="-0.508330579783" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.998168248457" Y="0.503392396367" />
                  <Point X="4.110766750101" Y="1.033126697485" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.294118780774" Y="-2.351975165106" />
                  <Point X="3.354522030903" Y="-2.067800215835" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.596314412968" Y="-0.930256494932" />
                  <Point X="3.691239165115" Y="-0.483670647844" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.895180279581" Y="0.475796859834" />
                  <Point X="4.010848315743" Y="1.019972185465" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.207612452284" Y="-2.302030680027" />
                  <Point X="3.271019054499" Y="-2.003726070107" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.501835904192" Y="-0.917818169268" />
                  <Point X="3.602985432122" Y="-0.441947054609" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.792192310706" Y="0.448201323301" />
                  <Point X="3.910929881386" Y="1.006817673444" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.121106123795" Y="-2.252086194949" />
                  <Point X="3.187516078095" Y="-1.93965192438" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.406766930358" Y="-0.908157763295" />
                  <Point X="3.517223430463" Y="-0.388500787112" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.689181892391" Y="0.420500170453" />
                  <Point X="3.811011447028" Y="0.993663161424" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.12588632695" Y="2.475033002226" />
                  <Point X="4.130072862814" Y="2.494729104903" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.661913776001" Y="-3.955491577671" />
                  <Point X="2.67059883984" Y="-3.91463156483" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.034599795305" Y="-2.202141709871" />
                  <Point X="3.10401310169" Y="-1.875577778652" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.306465859156" Y="-0.923112440133" />
                  <Point X="3.4383086886" Y="-0.302840695008" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.58056621685" Y="0.366428355694" />
                  <Point X="3.71109301267" Y="0.980508649404" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.0098361105" Y="2.385984422455" />
                  <Point X="4.058196952144" Y="2.613504294168" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.58585921764" Y="-3.856375380147" />
                  <Point X="2.599611225121" Y="-3.791677271688" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.948093466815" Y="-2.152197224792" />
                  <Point X="3.02162136427" Y="-1.806275664539" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.204639105004" Y="-0.945244890915" />
                  <Point X="3.369618845959" Y="-0.169076234165" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.463435992396" Y="0.272298737751" />
                  <Point X="3.611174578313" Y="0.967354137384" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.893785894051" Y="2.296935842684" />
                  <Point X="3.983836599283" Y="2.720591101902" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.50980465928" Y="-3.757259182622" />
                  <Point X="2.528623610401" Y="-3.668722978547" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.861587138326" Y="-2.102252739714" />
                  <Point X="2.947408719612" Y="-1.698493944352" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.099099511744" Y="-0.98484487636" />
                  <Point X="3.511256143955" Y="0.954199625363" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.777735677601" Y="2.207887262913" />
                  <Point X="3.893828386575" Y="2.754060517045" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.433750100919" Y="-3.658142985097" />
                  <Point X="2.457635986483" Y="-3.545768728682" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.772049026174" Y="-2.066571675338" />
                  <Point X="2.87641630841" Y="-1.575562216886" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.97532752622" Y="-1.110221523415" />
                  <Point X="3.414494613716" Y="0.955897179513" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.661685464454" Y="2.118838698681" />
                  <Point X="3.783119932853" Y="2.690142955042" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.357695542559" Y="-3.559026787573" />
                  <Point X="2.386648339952" Y="-3.422814585203" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.678530267455" Y="-2.049618080659" />
                  <Point X="3.320534593002" Y="0.970774799724" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.545635256027" Y="2.029790156654" />
                  <Point X="3.672411479131" Y="2.626225393039" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.281640977642" Y="-3.459910620894" />
                  <Point X="2.31566069342" Y="-3.299860441725" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.584998350523" Y="-2.032726390504" />
                  <Point X="3.231461176888" Y="1.00864208707" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.4295850476" Y="1.940741614628" />
                  <Point X="3.561703025409" Y="2.562307831036" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.205586405765" Y="-3.360794486957" />
                  <Point X="2.244673046889" Y="-3.176906298246" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.489093197168" Y="-2.026999899882" />
                  <Point X="3.146242683694" Y="1.064645360855" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.313534839173" Y="1.851693072602" />
                  <Point X="3.450994571688" Y="2.498390269032" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.129531833888" Y="-3.26167835302" />
                  <Point X="2.173685400358" Y="-3.053952154767" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.385115348115" Y="-2.059252456506" />
                  <Point X="3.070123038062" Y="1.163455346845" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.197484630746" Y="1.762644530575" />
                  <Point X="3.340286119986" Y="2.434472716533" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.053477262012" Y="-3.162562219084" />
                  <Point X="2.102697753827" Y="-2.930998011289" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.275759734708" Y="-2.11680540523" />
                  <Point X="3.008885823456" Y="1.332281665939" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.070942242296" Y="1.624234162499" />
                  <Point X="3.22957766898" Y="2.37055516731" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.977422690135" Y="-3.063446085147" />
                  <Point X="2.038001317921" Y="-2.778446048876" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.162673537135" Y="-2.191909372545" />
                  <Point X="3.118869217975" Y="2.306637618088" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.901368118259" Y="-2.96432995121" />
                  <Point X="3.00816076697" Y="2.242720068865" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.825056587814" Y="-2.86642271229" />
                  <Point X="2.897452315964" Y="2.178802519643" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.741948330811" Y="-2.800491557782" />
                  <Point X="2.786743864959" Y="2.11488497042" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.656502520607" Y="-2.745557726447" />
                  <Point X="2.676035413954" Y="2.050967421198" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.571056703374" Y="-2.690623928178" />
                  <Point X="2.568350825489" Y="2.00127602673" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.481761118271" Y="-2.653801863746" />
                  <Point X="2.466215505596" Y="1.977691888271" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.385744318205" Y="-2.648600629605" />
                  <Point X="2.369685043365" Y="1.980476531925" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.754979146455" Y="3.793142770331" />
                  <Point X="2.783310133032" Y="3.92642958281" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.286684381051" Y="-2.65771622983" />
                  <Point X="2.276989381458" Y="2.00130249265" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.601265851044" Y="3.526903335261" />
                  <Point X="2.69894088148" Y="3.986428224393" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.187624429998" Y="-2.666831895447" />
                  <Point X="2.190604478781" Y="2.051818241264" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.447552565108" Y="3.260663944771" />
                  <Point X="2.614210335068" Y="4.044727107302" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.800556474284" Y="-4.030918691561" />
                  <Point X="0.810776170579" Y="-3.982838800661" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.084021897006" Y="-2.697318728827" />
                  <Point X="2.108981323889" Y="2.124736251878" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.29383932737" Y="2.994424781033" />
                  <Point X="2.527789962981" Y="4.095075985461" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.746397187295" Y="-3.828793341089" />
                  <Point X="0.773628964528" Y="-3.700677901984" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.968192763469" Y="-2.785327195269" />
                  <Point X="2.037160415974" Y="2.243770208762" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.140126089631" Y="2.728185617295" />
                  <Point X="2.441369590894" Y="4.145424863621" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.692237900305" Y="-3.626667990618" />
                  <Point X="0.736481758476" Y="-3.418517003306" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.849517348645" Y="-2.886726362356" />
                  <Point X="2.354949178323" Y="4.195773551315" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.634893652966" Y="-3.439526700503" />
                  <Point X="2.268528759121" Y="4.246122207815" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.561427871441" Y="-3.328231265531" />
                  <Point X="2.180589662739" Y="4.289326049927" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.487076004942" Y="-3.221104532607" />
                  <Point X="2.092248876642" Y="4.33064009051" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.409361721193" Y="-3.129796729122" />
                  <Point X="2.003908098982" Y="4.371954170784" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.322323858571" Y="-3.082352915527" />
                  <Point X="1.915567331833" Y="4.413268300513" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.231212129855" Y="-3.054075135017" />
                  <Point X="1.826583840776" Y="4.45155865199" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.140100386364" Y="-3.02579742402" />
                  <Point X="1.73641325989" Y="4.484264184917" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.048286804506" Y="-3.00082160294" />
                  <Point X="1.646242679004" Y="4.516969717843" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.049263800775" Y="-3.002836354993" />
                  <Point X="1.55607208193" Y="4.549675174607" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.152924917385" Y="-3.033598802625" />
                  <Point X="1.465901484279" Y="4.582380628661" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.256906951965" Y="-3.065871050606" />
                  <Point X="1.373821628623" Y="4.606103730016" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.362543177339" Y="-3.105925654403" />
                  <Point X="1.28144009213" Y="4.62840753462" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.48828994234" Y="-3.240592908442" />
                  <Point X="1.189058555636" Y="4.650711339223" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.632025123707" Y="-3.459889007744" />
                  <Point X="1.09667702033" Y="4.673015149411" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.006088853074" Y="-4.762795729036" />
                  <Point X="1.004295488205" Y="4.695318974568" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.098841752187" Y="-4.742239048195" />
                  <Point X="0.91191395608" Y="4.717622799725" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.133316547617" Y="-4.44750544604" />
                  <Point X="0.819295245518" Y="4.738810788063" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.180267441439" Y="-4.211467272034" />
                  <Point X="0.724287778579" Y="4.74876056123" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.248072419812" Y="-4.073539852109" />
                  <Point X="0.629280311641" Y="4.758710334396" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.32950187888" Y="-3.999710574287" />
                  <Point X="0.534272844702" Y="4.768660107562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.411364412418" Y="-3.927918751659" />
                  <Point X="0.439265405662" Y="4.778610011981" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.49322695308" Y="-3.856126962544" />
                  <Point X="0.226006673661" Y="4.23223132305" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.580333044205" Y="-3.809004138819" />
                  <Point X="0.102090830406" Y="4.106177878582" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.674206413685" Y="-3.793718856603" />
                  <Point X="0.000491055512" Y="4.08511328125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.769994288074" Y="-3.787440611826" />
                  <Point X="-0.09195352201" Y="4.107120501133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.865782159098" Y="-3.781162351215" />
                  <Point X="-0.175649277132" Y="4.170287694301" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.961835026077" Y="-3.776130798556" />
                  <Point X="-0.241095748863" Y="4.319311015584" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.064075359964" Y="-3.800208989017" />
                  <Point X="-0.295255000821" Y="4.521436530868" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.175949023931" Y="-3.869608434219" />
                  <Point X="-0.349414279556" Y="4.723561920172" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.289148695775" Y="-3.94524625601" />
                  <Point X="-0.435751449168" Y="4.774302235203" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.405558603382" Y="-4.035987049628" />
                  <Point X="-0.535351504739" Y="4.762645577608" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.526501440113" Y="-4.14805359809" />
                  <Point X="-0.63495155048" Y="4.75098896626" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.612328162268" Y="-4.094911816588" />
                  <Point X="-0.734551589458" Y="4.739332386729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.698154884423" Y="-4.041770035086" />
                  <Point X="-2.462707263064" Y="-2.934076066434" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.361434728163" Y="-2.457626249475" />
                  <Point X="-0.834151628436" Y="4.727675807198" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.782979092849" Y="-3.983911797306" />
                  <Point X="-2.616420427116" Y="-3.200314883506" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.442839230369" Y="-2.38367955885" />
                  <Point X="-0.933846796026" Y="4.715571682734" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.866441808962" Y="-3.9196482418" />
                  <Point X="-2.770133656729" Y="-3.466554009018" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.532893818535" Y="-2.350428323083" />
                  <Point X="-1.037123801507" Y="4.686616335883" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.949904526213" Y="-3.855384691648" />
                  <Point X="-2.923846952392" Y="-3.732793445269" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.631612343308" Y="-2.357937704343" />
                  <Point X="-1.140400806988" Y="4.657660989032" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.739702050355" Y="-2.40953503189" />
                  <Point X="-1.243677812469" Y="4.628705642181" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.850410506062" Y="-2.473452603232" />
                  <Point X="-1.346954815431" Y="4.59975030718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.961118961769" Y="-2.537370174573" />
                  <Point X="-1.533515969651" Y="4.178973846527" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.46882290908" Y="4.483330767163" />
                  <Point X="-1.450231812216" Y="4.570795001238" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.071827417476" Y="-2.601287745914" />
                  <Point X="-1.650800949622" Y="4.084116161116" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.182535873182" Y="-2.665205317255" />
                  <Point X="-1.756982600263" Y="4.041495533188" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.293244328889" Y="-2.729122888596" />
                  <Point X="-3.047586930632" Y="-1.573395696141" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.976762244894" Y="-1.24019174712" />
                  <Point X="-1.856522986049" Y="4.03011959986" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.403952782045" Y="-2.793040447939" />
                  <Point X="-3.163656277849" Y="-1.662534279094" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.055448896104" Y="-1.153458572867" />
                  <Point X="-1.947791608258" Y="4.057659254514" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.514661230259" Y="-2.856957984025" />
                  <Point X="-3.279706462852" Y="-1.75158271092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.143482371151" Y="-1.110698747468" />
                  <Point X="-2.035237927293" Y="4.103181431769" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.625369678472" Y="-2.920875520111" />
                  <Point X="-3.395756647856" Y="-1.840631142746" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.23831091439" Y="-1.099907204477" />
                  <Point X="-2.117913121465" Y="4.171149986712" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.736078126685" Y="-2.984793056198" />
                  <Point X="-3.511806832859" Y="-1.929679574572" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.33811935989" Y="-1.112544259603" />
                  <Point X="-2.194635600688" Y="4.267123863639" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.829913545089" Y="-2.969329228205" />
                  <Point X="-3.627857017862" Y="-2.018728006398" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.438037795201" Y="-1.125698776111" />
                  <Point X="-2.304763121612" Y="4.205939375569" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.905834503617" Y="-2.869584492889" />
                  <Point X="-3.743907225494" Y="-2.107776544684" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.537956230513" Y="-1.13885329262" />
                  <Point X="-3.333367756203" Y="-0.17634019633" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.296178104366" Y="-0.001376640533" />
                  <Point X="-2.41489065003" Y="4.144754852242" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.981755462146" Y="-2.769839757573" />
                  <Point X="-3.859957443418" Y="-2.196825131389" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.637874665825" Y="-1.152007809129" />
                  <Point X="-3.454346255952" Y="-0.288574526099" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.360391250487" Y="0.153449021552" />
                  <Point X="-2.77238949119" Y="2.919779802765" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.748499402136" Y="3.032173835047" />
                  <Point X="-2.525018184814" Y="4.083570298965" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.054623539511" Y="-2.655732345613" />
                  <Point X="-3.976007661342" Y="-2.285873718094" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.737793101136" Y="-1.165162325637" />
                  <Point X="-3.559395046839" Y="-0.325865467918" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.443435234685" Y="0.219682555831" />
                  <Point X="-2.900242700017" Y="2.775202509677" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.79940383371" Y="3.249612076311" />
                  <Point X="-2.635145719597" Y="4.022385745689" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.126225265659" Y="-2.535667219591" />
                  <Point X="-4.092057879265" Y="-2.374922304799" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.837711536448" Y="-1.178316842146" />
                  <Point X="-3.662383008917" Y="-0.353460972471" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.532799990639" Y="0.256179196996" />
                  <Point X="-3.007549208957" Y="2.727289839525" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.870391492086" Y="3.372566164064" />
                  <Point X="-2.75046800907" Y="3.936761793093" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.93762997176" Y="-1.191471358654" />
                  <Point X="-3.765370970994" Y="-0.381056477024" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.624688874586" Y="0.280800749602" />
                  <Point X="-3.104784577836" Y="2.72675815814" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.941379150462" Y="3.495520251818" />
                  <Point X="-2.866499466125" Y="3.847801469335" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.037548407071" Y="-1.204625875163" />
                  <Point X="-3.868358933072" Y="-0.408651981577" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.716577758533" Y="0.305422302208" />
                  <Point X="-3.503591222781" Y="1.30744517122" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.438877511974" Y="1.611899243582" />
                  <Point X="-3.199029652354" Y="2.740294705641" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.012366792413" Y="3.618474416844" />
                  <Point X="-2.982530909518" Y="3.758841209853" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.137466841677" Y="-1.21778038835" />
                  <Point X="-3.971346895149" Y="-0.43624748613" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.808466642481" Y="0.330043854814" />
                  <Point X="-3.616546932036" Y="1.232955103176" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.509683675342" Y="1.735707198214" />
                  <Point X="-3.287376970834" Y="2.781578013779" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.237385274986" Y="-1.230934895437" />
                  <Point X="-4.074334857227" Y="-0.463842990683" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.900355526428" Y="0.35466540742" />
                  <Point X="-3.719351665484" Y="1.206221621548" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.59178194422" Y="1.806389973263" />
                  <Point X="-3.373883302115" Y="2.831522485728" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.337303708295" Y="-1.244089402523" />
                  <Point X="-4.177322819304" Y="-0.491438495236" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.992244410375" Y="0.379286960026" />
                  <Point X="-3.815555864472" Y="1.210541213084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.675284923138" Y="1.870464107166" />
                  <Point X="-3.460389633395" Y="2.881466957677" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.437222141604" Y="-1.25724390961" />
                  <Point X="-4.280310789578" Y="-0.519034038348" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.084133294323" Y="0.403908512632" />
                  <Point X="-3.910034367157" Y="1.222979567404" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.758787902056" Y="1.934538241068" />
                  <Point X="-3.546895964675" Y="2.931411429626" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.537140574913" Y="-1.270398416697" />
                  <Point X="-4.383298763664" Y="-0.546629599397" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.17602217827" Y="0.428530065238" />
                  <Point X="-4.004512869842" Y="1.235417921724" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.842290880974" Y="1.998612374971" />
                  <Point X="-3.633402319073" Y="2.981355792814" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.637059008222" Y="-1.283552923783" />
                  <Point X="-4.48628673775" Y="-0.574225160447" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.267911062217" Y="0.453151617843" />
                  <Point X="-4.098991372527" Y="1.247856276044" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.925793859892" Y="2.062686508873" />
                  <Point X="-3.719908678799" Y="3.031300130937" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.701271264599" Y="-1.128723075781" />
                  <Point X="-4.589274711836" Y="-0.601820721497" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.359799947827" Y="0.477773162627" />
                  <Point X="-4.193469875212" Y="1.260294630363" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.00929683089" Y="2.126760680035" />
                  <Point X="-3.85003805818" Y="2.876014297325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.750724882661" Y="-0.904459293586" />
                  <Point X="-4.692262685923" Y="-0.629416282546" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.451688833974" Y="0.502394704887" />
                  <Point X="-4.287948378389" Y="1.272732982368" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.092799769526" Y="2.190835003451" />
                  <Point X="-3.983670206855" Y="2.704249229824" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.54357772012" Y="0.527016247147" />
                  <Point X="-4.382426887851" Y="1.285171304807" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.176302708161" Y="2.254909326866" />
                  <Point X="-4.133753051006" Y="2.455089725067" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.635466606266" Y="0.551637789407" />
                  <Point X="-4.476905397312" Y="1.297609627246" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.727355492413" Y="0.576259331667" />
                  <Point X="-4.571383906774" Y="1.310047949686" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="0.001625976562" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978485351562" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.66099029541" Y="-4.244154296875" />
                  <Point X="0.471539031982" Y="-3.537112304688" />
                  <Point X="0.464318664551" Y="-3.521544189453" />
                  <Point X="0.386885223389" Y="-3.409977050781" />
                  <Point X="0.300591033936" Y="-3.285643798828" />
                  <Point X="0.274335754395" Y="-3.266399902344" />
                  <Point X="0.154511917114" Y="-3.2292109375" />
                  <Point X="0.02097677803" Y="-3.187766601562" />
                  <Point X="-0.008664908409" Y="-3.187766601562" />
                  <Point X="-0.128488739014" Y="-3.224955322266" />
                  <Point X="-0.262023895264" Y="-3.266399902344" />
                  <Point X="-0.288279144287" Y="-3.285643798828" />
                  <Point X="-0.365712738037" Y="-3.397210693359" />
                  <Point X="-0.45200680542" Y="-3.521544189453" />
                  <Point X="-0.459227142334" Y="-3.537112304688" />
                  <Point X="-0.670357116699" Y="-4.325060058594" />
                  <Point X="-0.84774395752" Y="-4.987077148438" />
                  <Point X="-0.943741271973" Y="-4.968443359375" />
                  <Point X="-1.100246582031" Y="-4.938065429688" />
                  <Point X="-1.238634277344" Y="-4.902459472656" />
                  <Point X="-1.35158972168" Y="-4.873396972656" />
                  <Point X="-1.331955810547" Y="-4.724263183594" />
                  <Point X="-1.309150146484" Y="-4.551038574219" />
                  <Point X="-1.309683227539" Y="-4.5347578125" />
                  <Point X="-1.338309204102" Y="-4.390845703125" />
                  <Point X="-1.370210693359" Y="-4.230466308594" />
                  <Point X="-1.386282714844" Y="-4.20262890625" />
                  <Point X="-1.496601318359" Y="-4.105881835938" />
                  <Point X="-1.619543457031" Y="-3.998063964844" />
                  <Point X="-1.649240722656" Y="-3.985762939453" />
                  <Point X="-1.795658081055" Y="-3.976166259766" />
                  <Point X="-1.958829833984" Y="-3.965471435547" />
                  <Point X="-1.989878540039" Y="-3.973791015625" />
                  <Point X="-2.111881347656" Y="-4.055310791016" />
                  <Point X="-2.247844970703" Y="-4.146159179688" />
                  <Point X="-2.259734130859" Y="-4.157294433594" />
                  <Point X="-2.378267333984" Y="-4.31176953125" />
                  <Point X="-2.457095214844" Y="-4.414500488281" />
                  <Point X="-2.626430419922" Y="-4.30965234375" />
                  <Point X="-2.855831054688" Y="-4.16761328125" />
                  <Point X="-3.047461669922" Y="-4.020064208984" />
                  <Point X="-3.228581054688" Y="-3.880608154297" />
                  <Point X="-2.858107666016" Y="-3.238929199219" />
                  <Point X="-2.506033691406" Y="-2.629119628906" />
                  <Point X="-2.499762695312" Y="-2.597593261719" />
                  <Point X="-2.513979736328" Y="-2.568764404297" />
                  <Point X="-2.531328369141" Y="-2.551415771484" />
                  <Point X="-2.560157226562" Y="-2.537198730469" />
                  <Point X="-2.59168359375" Y="-2.543469726562" />
                  <Point X="-3.271273193359" Y="-2.935831054688" />
                  <Point X="-3.842959472656" Y="-3.265894042969" />
                  <Point X="-3.980462402344" Y="-3.085243408203" />
                  <Point X="-4.161703613281" Y="-2.847129394531" />
                  <Point X="-4.299085449219" Y="-2.616761230469" />
                  <Point X="-4.431020019531" Y="-2.395527099609" />
                  <Point X="-3.779801757813" Y="-1.895829711914" />
                  <Point X="-3.163786865234" Y="-1.423144897461" />
                  <Point X="-3.145822021484" Y="-1.396013671875" />
                  <Point X="-3.138117431641" Y="-1.366266357422" />
                  <Point X="-3.140326416016" Y="-1.334595703125" />
                  <Point X="-3.161158935547" Y="-1.310639160156" />
                  <Point X="-3.187641601562" Y="-1.295052734375" />
                  <Point X="-3.219529296875" Y="-1.288571044922" />
                  <Point X="-4.077446777344" Y="-1.401518066406" />
                  <Point X="-4.803283203125" Y="-1.497076293945" />
                  <Point X="-4.856508300781" Y="-1.288702758789" />
                  <Point X="-4.927393066406" Y="-1.011191589355" />
                  <Point X="-4.963740722656" Y="-0.757051696777" />
                  <Point X="-4.998396484375" Y="-0.514742126465" />
                  <Point X="-4.259197753906" Y="-0.316674407959" />
                  <Point X="-3.557463867188" Y="-0.128645431519" />
                  <Point X="-3.528770263672" Y="-0.112315292358" />
                  <Point X="-3.50232421875" Y="-0.090645217896" />
                  <Point X="-3.490523681641" Y="-0.061810546875" />
                  <Point X="-3.483400878906" Y="-0.031280044556" />
                  <Point X="-3.490023193359" Y="-0.002362585783" />
                  <Point X="-3.50232421875" Y="0.028085128784" />
                  <Point X="-3.527268310547" Y="0.048712741852" />
                  <Point X="-3.557463867188" Y="0.066085334778" />
                  <Point X="-4.33949609375" Y="0.275630249023" />
                  <Point X="-4.998186523438" Y="0.452125793457" />
                  <Point X="-4.963329101563" Y="0.687689880371" />
                  <Point X="-4.91764453125" Y="0.996421936035" />
                  <Point X="-4.844478515625" Y="1.266427856445" />
                  <Point X="-4.773516601562" Y="1.528298706055" />
                  <Point X="-4.256286621094" Y="1.460204101562" />
                  <Point X="-3.753266357422" Y="1.393980102539" />
                  <Point X="-3.731704589844" Y="1.395866699219" />
                  <Point X="-3.702653808594" Y="1.405026367188" />
                  <Point X="-3.670278564453" Y="1.41523425293" />
                  <Point X="-3.651534423828" Y="1.426056152344" />
                  <Point X="-3.639120117188" Y="1.443785766602" />
                  <Point X="-3.627463378906" Y="1.471927856445" />
                  <Point X="-3.61447265625" Y="1.503290039063" />
                  <Point X="-3.610714111328" Y="1.524605224609" />
                  <Point X="-3.616315917969" Y="1.54551159668" />
                  <Point X="-3.630381103516" Y="1.572530639648" />
                  <Point X="-3.646055664062" Y="1.602641235352" />
                  <Point X="-3.659968261719" Y="1.619221679688" />
                  <Point X="-4.108543945313" Y="1.96342590332" />
                  <Point X="-4.47610546875" Y="2.245466308594" />
                  <Point X="-4.337526367188" Y="2.482886230469" />
                  <Point X="-4.160014648438" Y="2.787006591797" />
                  <Point X="-3.966197509766" Y="3.036130859375" />
                  <Point X="-3.774671142578" Y="3.282310791016" />
                  <Point X="-3.468097900391" Y="3.105310546875" />
                  <Point X="-3.159156982422" Y="2.926943359375" />
                  <Point X="-3.138514648438" Y="2.920434814453" />
                  <Point X="-3.098054931641" Y="2.916895019531" />
                  <Point X="-3.052965332031" Y="2.912950195312" />
                  <Point X="-3.031506591797" Y="2.915775390625" />
                  <Point X="-3.013252441406" Y="2.927404785156" />
                  <Point X="-2.984533691406" Y="2.956123291016" />
                  <Point X="-2.952528808594" Y="2.988128173828" />
                  <Point X="-2.940899414062" Y="3.006382324219" />
                  <Point X="-2.93807421875" Y="3.027841064453" />
                  <Point X="-2.941614013672" Y="3.068301025391" />
                  <Point X="-2.945558837891" Y="3.113390625" />
                  <Point X="-2.952067382812" Y="3.134032958984" />
                  <Point X="-3.150844970703" Y="3.478325439453" />
                  <Point X="-3.307278564453" Y="3.749276611328" />
                  <Point X="-3.062044677734" Y="3.937294921875" />
                  <Point X="-2.752876220703" Y="4.174331542969" />
                  <Point X="-2.447620361328" Y="4.343924804688" />
                  <Point X="-2.141548583984" Y="4.513971679688" />
                  <Point X="-2.062403076172" Y="4.410827148438" />
                  <Point X="-1.967826660156" Y="4.287573242188" />
                  <Point X="-1.951247070312" Y="4.273661132813" />
                  <Point X="-1.906215454102" Y="4.250219238281" />
                  <Point X="-1.856031005859" Y="4.224094238281" />
                  <Point X="-1.835124267578" Y="4.2184921875" />
                  <Point X="-1.813808837891" Y="4.222250488281" />
                  <Point X="-1.766905395508" Y="4.241678710938" />
                  <Point X="-1.714634887695" Y="4.263330078125" />
                  <Point X="-1.696905273438" Y="4.275744628906" />
                  <Point X="-1.686083496094" Y="4.294488769531" />
                  <Point X="-1.670817138672" Y="4.342906738281" />
                  <Point X="-1.653804077148" Y="4.396865234375" />
                  <Point X="-1.651917602539" Y="4.418426757812" />
                  <Point X="-1.674516357422" Y="4.590081542969" />
                  <Point X="-1.689137573242" Y="4.701141113281" />
                  <Point X="-1.36833215332" Y="4.791083496094" />
                  <Point X="-0.968094177246" Y="4.903296386719" />
                  <Point X="-0.598032348633" Y="4.946606445312" />
                  <Point X="-0.22419960022" Y="4.990358398438" />
                  <Point X="-0.13353352356" Y="4.651987792969" />
                  <Point X="-0.042140380859" Y="4.310903320312" />
                  <Point X="-0.024282117844" Y="4.284176757813" />
                  <Point X="0.006155934334" Y="4.273844238281" />
                  <Point X="0.036594036102" Y="4.284176757813" />
                  <Point X="0.054452251434" Y="4.310903320312" />
                  <Point X="0.156303543091" Y="4.691017578125" />
                  <Point X="0.236648406982" Y="4.990868652344" />
                  <Point X="0.510738769531" Y="4.9621640625" />
                  <Point X="0.860205749512" Y="4.925565429688" />
                  <Point X="1.166368286133" Y="4.8516484375" />
                  <Point X="1.508455932617" Y="4.769058105469" />
                  <Point X="1.7079296875" Y="4.696707519531" />
                  <Point X="1.931034301758" Y="4.615785644531" />
                  <Point X="2.12373046875" Y="4.52566796875" />
                  <Point X="2.33869921875" Y="4.425134277344" />
                  <Point X="2.524854003906" Y="4.316680175781" />
                  <Point X="2.732523681641" Y="4.19569140625" />
                  <Point X="2.908090576172" Y="4.070837890625" />
                  <Point X="3.068740234375" Y="3.956592773438" />
                  <Point X="2.637352539062" Y="3.209406982422" />
                  <Point X="2.229853515625" Y="2.503597900391" />
                  <Point X="2.224852050781" Y="2.491514892578" />
                  <Point X="2.214698242188" Y="2.453544189453" />
                  <Point X="2.203382324219" Y="2.411228515625" />
                  <Point X="2.202044677734" Y="2.392325927734" />
                  <Point X="2.20600390625" Y="2.3594921875" />
                  <Point X="2.210416015625" Y="2.322901123047" />
                  <Point X="2.218682128906" Y="2.300812255859" />
                  <Point X="2.238998535156" Y="2.27087109375" />
                  <Point X="2.261639892578" Y="2.237503662109" />
                  <Point X="2.274939941406" Y="2.224203613281" />
                  <Point X="2.304881103516" Y="2.203887207031" />
                  <Point X="2.338248535156" Y="2.18124609375" />
                  <Point X="2.360336914062" Y="2.172980224609" />
                  <Point X="2.393170898438" Y="2.169020996094" />
                  <Point X="2.42976171875" Y="2.164608642578" />
                  <Point X="2.448664306641" Y="2.165946289062" />
                  <Point X="2.486635009766" Y="2.176100341797" />
                  <Point X="2.528950683594" Y="2.187416015625" />
                  <Point X="2.541033935547" Y="2.192417480469" />
                  <Point X="3.327607910156" Y="2.646546142578" />
                  <Point X="3.994248046875" Y="3.031430908203" />
                  <Point X="4.079409179688" Y="2.913076171875" />
                  <Point X="4.202591796875" Y="2.741880126953" />
                  <Point X="4.300467773438" Y="2.580139404297" />
                  <Point X="4.387513183594" Y="2.436296142578" />
                  <Point X="3.825126953125" Y="2.004761962891" />
                  <Point X="3.288616210938" Y="1.593082763672" />
                  <Point X="3.279371337891" Y="1.583833374023" />
                  <Point X="3.252043701172" Y="1.548182373047" />
                  <Point X="3.221589111328" Y="1.508451904297" />
                  <Point X="3.213119384766" Y="1.4915" />
                  <Point X="3.202939697266" Y="1.455100219727" />
                  <Point X="3.191595458984" Y="1.41453527832" />
                  <Point X="3.190779541016" Y="1.390965576172" />
                  <Point X="3.199135986328" Y="1.350466186523" />
                  <Point X="3.208448486328" Y="1.305332763672" />
                  <Point X="3.215646484375" Y="1.287954956055" />
                  <Point X="3.238374755859" Y="1.253408935547" />
                  <Point X="3.263704101562" Y="1.214909667969" />
                  <Point X="3.280947998047" Y="1.198819824219" />
                  <Point X="3.313884521484" Y="1.180279418945" />
                  <Point X="3.350590087891" Y="1.159617431641" />
                  <Point X="3.368565673828" Y="1.153619628906" />
                  <Point X="3.413098144531" Y="1.147734008789" />
                  <Point X="3.462726318359" Y="1.141175048828" />
                  <Point X="3.475803710938" Y="1.14117175293" />
                  <Point X="4.222996582031" Y="1.239541625977" />
                  <Point X="4.848975097656" Y="1.32195324707" />
                  <Point X="4.886282226562" Y="1.168707519531" />
                  <Point X="4.939188476562" Y="0.951385925293" />
                  <Point X="4.970031738281" Y="0.753282287598" />
                  <Point X="4.997858398438" Y="0.574556152344" />
                  <Point X="4.356020507813" Y="0.402576171875" />
                  <Point X="3.741167724609" Y="0.237826919556" />
                  <Point X="3.729086914062" Y="0.232819412231" />
                  <Point X="3.685335205078" Y="0.207530075073" />
                  <Point X="3.636576904297" Y="0.179346878052" />
                  <Point X="3.622264892578" Y="0.166926574707" />
                  <Point X="3.596013916016" Y="0.133476501465" />
                  <Point X="3.566759033203" Y="0.096198890686" />
                  <Point X="3.556985107422" Y="0.07473513031" />
                  <Point X="3.548234619141" Y="0.02904410553" />
                  <Point X="3.538482910156" Y="-0.021875303268" />
                  <Point X="3.538482910156" Y="-0.04068478775" />
                  <Point X="3.547233398438" Y="-0.086375808716" />
                  <Point X="3.556985107422" Y="-0.137295227051" />
                  <Point X="3.566759033203" Y="-0.158758987427" />
                  <Point X="3.593010009766" Y="-0.19220904541" />
                  <Point X="3.622264892578" Y="-0.229486679077" />
                  <Point X="3.636576904297" Y="-0.241906967163" />
                  <Point X="3.680328613281" Y="-0.267196289062" />
                  <Point X="3.729086914062" Y="-0.295379486084" />
                  <Point X="3.741167724609" Y="-0.300387023926" />
                  <Point X="4.426377441406" Y="-0.483988342285" />
                  <Point X="4.998067871094" Y="-0.637172363281" />
                  <Point X="4.977972167969" Y="-0.770463806152" />
                  <Point X="4.948431640625" Y="-0.966399108887" />
                  <Point X="4.908916015625" Y="-1.139562744141" />
                  <Point X="4.874545410156" Y="-1.290178344727" />
                  <Point X="4.125038574219" Y="-1.19150378418" />
                  <Point X="3.411982177734" Y="-1.097628295898" />
                  <Point X="3.394836181641" Y="-1.098341186523" />
                  <Point X="3.308967041016" Y="-1.117005249023" />
                  <Point X="3.213271972656" Y="-1.137804931641" />
                  <Point X="3.1854453125" Y="-1.154697265625" />
                  <Point X="3.133542724609" Y="-1.217119873047" />
                  <Point X="3.075701171875" Y="-1.286685424805" />
                  <Point X="3.064357910156" Y="-1.314070678711" />
                  <Point X="3.056918945312" Y="-1.394910644531" />
                  <Point X="3.04862890625" Y="-1.485000854492" />
                  <Point X="3.056360595703" Y="-1.516621826172" />
                  <Point X="3.103881835938" Y="-1.590538085938" />
                  <Point X="3.156840820312" Y="-1.672912353516" />
                  <Point X="3.168460693359" Y="-1.685540649414" />
                  <Point X="3.804339599609" Y="-2.173467773437" />
                  <Point X="4.339074707031" Y="-2.583784179688" />
                  <Point X="4.287403808594" Y="-2.667395019531" />
                  <Point X="4.204131835938" Y="-2.802141845703" />
                  <Point X="4.122410644531" Y="-2.918255859375" />
                  <Point X="4.056688476562" Y="-3.011638427734" />
                  <Point X="3.38852734375" Y="-2.625875244141" />
                  <Point X="2.753454589844" Y="-2.259215576172" />
                  <Point X="2.737340820313" Y="-2.253312744141" />
                  <Point X="2.635142822266" Y="-2.234855957031" />
                  <Point X="2.521250488281" Y="-2.214287109375" />
                  <Point X="2.489077636719" Y="-2.219244873047" />
                  <Point X="2.404176025391" Y="-2.263927734375" />
                  <Point X="2.309559570312" Y="-2.313723876953" />
                  <Point X="2.288599853516" Y="-2.334683837891" />
                  <Point X="2.243916748047" Y="-2.419585205078" />
                  <Point X="2.194120849609" Y="-2.514201904297" />
                  <Point X="2.189163085938" Y="-2.546374511719" />
                  <Point X="2.207619873047" Y="-2.648572509766" />
                  <Point X="2.228188720703" Y="-2.76246484375" />
                  <Point X="2.234091552734" Y="-2.778578613281" />
                  <Point X="2.642707763672" Y="-3.486322265625" />
                  <Point X="2.986673583984" Y="-4.082088623047" />
                  <Point X="2.934107910156" Y="-4.119634765625" />
                  <Point X="2.835296630859" Y="-4.190213378906" />
                  <Point X="2.743929443359" Y="-4.249354003906" />
                  <Point X="2.679775878906" Y="-4.290879394531" />
                  <Point X="2.167796875" Y="-3.623655273438" />
                  <Point X="1.683177490234" Y="-2.992087402344" />
                  <Point X="1.670549316406" Y="-2.980467529297" />
                  <Point X="1.569754516602" Y="-2.915665771484" />
                  <Point X="1.45742590332" Y="-2.843448730469" />
                  <Point X="1.4258046875" Y="-2.835717041016" />
                  <Point X="1.315568359375" Y="-2.845861083984" />
                  <Point X="1.192717773438" Y="-2.857166015625" />
                  <Point X="1.165332885742" Y="-2.868509277344" />
                  <Point X="1.080211303711" Y="-2.93928515625" />
                  <Point X="0.985349243164" Y="-3.018159667969" />
                  <Point X="0.968456726074" Y="-3.045986328125" />
                  <Point X="0.943005859375" Y="-3.163080810547" />
                  <Point X="0.91464251709" Y="-3.29357421875" />
                  <Point X="0.91392956543" Y="-3.310720214844" />
                  <Point X="1.029728149414" Y="-4.190298339844" />
                  <Point X="1.127642211914" Y="-4.934028320312" />
                  <Point X="1.087826293945" Y="-4.942755859375" />
                  <Point X="0.994345458984" Y="-4.963247070312" />
                  <Point X="0.909938171387" Y="-4.978581054688" />
                  <Point X="0.860200378418" Y="-4.987616699219" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#164" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.091393266905" Y="4.696208806539" Z="1.2" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.2" />
                  <Point X="-0.610000441139" Y="5.027547843578" Z="1.2" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.2" />
                  <Point X="-1.387985882063" Y="4.870507317709" Z="1.2" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.2" />
                  <Point X="-1.730244645039" Y="4.614835297266" Z="1.2" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.2" />
                  <Point X="-1.724658965457" Y="4.389222120976" Z="1.2" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.2" />
                  <Point X="-1.792195649252" Y="4.319152766711" Z="1.2" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.2" />
                  <Point X="-1.889283585606" Y="4.32584918069" Z="1.2" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.2" />
                  <Point X="-2.028891352471" Y="4.472545417174" Z="1.2" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.2" />
                  <Point X="-2.478059383287" Y="4.418912438217" Z="1.2" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.2" />
                  <Point X="-3.098622815166" Y="4.008254975894" Z="1.2" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.2" />
                  <Point X="-3.200302147543" Y="3.484605551556" Z="1.2" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.2" />
                  <Point X="-2.997579821421" Y="3.09522334871" Z="1.2" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.2" />
                  <Point X="-3.026044905214" Y="3.022758662673" Z="1.2" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.2" />
                  <Point X="-3.099853116897" Y="2.997984877752" Z="1.2" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.2" />
                  <Point X="-3.449253796741" Y="3.179891801203" Z="1.2" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.2" />
                  <Point X="-4.011816884616" Y="3.09811334553" Z="1.2" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.2" />
                  <Point X="-4.386864304291" Y="2.539371323255" Z="1.2" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.2" />
                  <Point X="-4.145137956612" Y="1.955038298069" Z="1.2" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.2" />
                  <Point X="-3.68088762962" Y="1.580723374949" Z="1.2" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.2" />
                  <Point X="-3.679813107807" Y="1.522342116798" Z="1.2" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.2" />
                  <Point X="-3.723845122607" Y="1.483992254453" Z="1.2" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.2" />
                  <Point X="-4.255915984194" Y="1.541056382777" Z="1.2" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.2" />
                  <Point X="-4.898893606838" Y="1.310785342457" Z="1.2" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.2" />
                  <Point X="-5.018946478795" Y="0.726288997638" Z="1.2" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.2" />
                  <Point X="-4.358593198422" Y="0.258614002137" Z="1.2" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.2" />
                  <Point X="-3.56193333115" Y="0.038916777992" Z="1.2" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.2" />
                  <Point X="-3.543931974129" Y="0.01409692781" Z="1.2" />
                  <Point X="-3.539556741714" Y="0" Z="1.2" />
                  <Point X="-3.544432627006" Y="-0.015710023251" Z="1.2" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.2" />
                  <Point X="-3.563435263974" Y="-0.039959205142" Z="1.2" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.2" />
                  <Point X="-4.27829459035" Y="-0.237098054619" Z="1.2" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.2" />
                  <Point X="-5.019392838714" Y="-0.732850733887" Z="1.2" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.2" />
                  <Point X="-4.911109031045" Y="-1.269796938582" Z="1.2" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.2" />
                  <Point X="-4.077076320865" Y="-1.419810259065" Z="1.2" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.2" />
                  <Point X="-3.205201003356" Y="-1.31507831138" Z="1.2" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.2" />
                  <Point X="-3.1967370303" Y="-1.338128422235" Z="1.2" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.2" />
                  <Point X="-3.816395755522" Y="-1.824882049022" Z="1.2" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.2" />
                  <Point X="-4.348184671138" Y="-2.611090718426" Z="1.2" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.2" />
                  <Point X="-4.026404252384" Y="-3.084246546519" Z="1.2" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.2" />
                  <Point X="-3.252429111138" Y="-2.947852210946" Z="1.2" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.2" />
                  <Point X="-2.563695799916" Y="-2.564634669216" Z="1.2" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.2" />
                  <Point X="-2.907564802223" Y="-3.182649271355" Z="1.2" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.2" />
                  <Point X="-3.084121435472" Y="-4.028401243637" Z="1.2" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.2" />
                  <Point X="-2.658908030106" Y="-4.320882989583" Z="1.2" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.2" />
                  <Point X="-2.344755472855" Y="-4.310927593729" Z="1.2" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.2" />
                  <Point X="-2.090258814738" Y="-4.065604164473" Z="1.2" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.2" />
                  <Point X="-1.805084235309" Y="-3.994779221658" Z="1.2" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.2" />
                  <Point X="-1.535724597064" Y="-4.112193542852" Z="1.2" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.2" />
                  <Point X="-1.393504442203" Y="-4.369320212965" Z="1.2" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.2" />
                  <Point X="-1.387683991658" Y="-4.686456795539" Z="1.2" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.2" />
                  <Point X="-1.257249159347" Y="-4.919602072349" Z="1.2" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.2" />
                  <Point X="-0.959424449165" Y="-4.986247540239" Z="1.2" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.2" />
                  <Point X="-0.628216778841" Y="-4.306721017647" Z="1.2" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.2" />
                  <Point X="-0.330792614" Y="-3.394439637576" Z="1.2" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.2" />
                  <Point X="-0.119823884185" Y="-3.241428280176" Z="1.2" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.2" />
                  <Point X="0.133535195176" Y="-3.245683765113" Z="1.2" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.2" />
                  <Point X="0.339653245432" Y="-3.407206118192" Z="1.2" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.2" />
                  <Point X="0.60653808156" Y="-4.225815012168" Z="1.2" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.2" />
                  <Point X="0.912719004389" Y="-4.996495667003" Z="1.2" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.2" />
                  <Point X="1.092376294781" Y="-4.960316374327" Z="1.2" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.2" />
                  <Point X="1.073144446638" Y="-4.152491759599" Z="1.2" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.2" />
                  <Point X="0.985709061783" Y="-3.142420367028" Z="1.2" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.2" />
                  <Point X="1.106022072133" Y="-2.946451324639" Z="1.2" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.2" />
                  <Point X="1.313994208098" Y="-2.864370629039" Z="1.2" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.2" />
                  <Point X="1.536559094141" Y="-2.926443524547" Z="1.2" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.2" />
                  <Point X="2.121973073029" Y="-3.622813147153" Z="1.2" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.2" />
                  <Point X="2.76494277905" Y="-4.260047951547" Z="1.2" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.2" />
                  <Point X="2.95701371907" Y="-4.12904194434" Z="1.2" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.2" />
                  <Point X="2.679852972731" Y="-3.430042130731" Z="1.2" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.2" />
                  <Point X="2.250668004195" Y="-2.608406200143" Z="1.2" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.2" />
                  <Point X="2.282007169055" Y="-2.411591603426" Z="1.2" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.2" />
                  <Point X="2.421306774947" Y="-2.276894140912" Z="1.2" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.2" />
                  <Point X="2.62010060337" Y="-2.252780005296" Z="1.2" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.2" />
                  <Point X="3.357371466376" Y="-2.637896527544" Z="1.2" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.2" />
                  <Point X="4.157143320125" Y="-2.915753001448" Z="1.2" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.2" />
                  <Point X="4.323780946071" Y="-2.662400081705" Z="1.2" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.2" />
                  <Point X="3.828621499006" Y="-2.102519962348" Z="1.2" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.2" />
                  <Point X="3.139783763768" Y="-1.532218690455" Z="1.2" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.2" />
                  <Point X="3.100552592162" Y="-1.368212154387" Z="1.2" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.2" />
                  <Point X="3.165833063079" Y="-1.217806736262" Z="1.2" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.2" />
                  <Point X="3.313430652426" Y="-1.13458445196" Z="1.2" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.2" />
                  <Point X="4.1123561874" Y="-1.209796063798" Z="1.2" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.2" />
                  <Point X="4.951507204722" Y="-1.119406679343" Z="1.2" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.2" />
                  <Point X="5.021257665657" Y="-0.74663782012" Z="1.2" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.2" />
                  <Point X="4.433162360054" Y="-0.404412003595" Z="1.2" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.2" />
                  <Point X="3.69919447505" Y="-0.192627509083" Z="1.2" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.2" />
                  <Point X="3.626187779925" Y="-0.130060545702" Z="1.2" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.2" />
                  <Point X="3.590185078787" Y="-0.045691076622" Z="1.2" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.2" />
                  <Point X="3.591186371637" Y="0.050919454591" Z="1.2" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.2" />
                  <Point X="3.629191658476" Y="0.133888192873" Z="1.2" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.2" />
                  <Point X="3.704200939302" Y="0.195521335159" Z="1.2" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.2" />
                  <Point X="4.362805457252" Y="0.385559929173" Z="1.2" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.2" />
                  <Point X="5.013280749392" Y="0.792254473118" Z="1.2" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.2" />
                  <Point X="4.928707351377" Y="1.211814454058" Z="1.2" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.2" />
                  <Point X="4.21031407935" Y="1.320393893965" Z="1.2" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.2" />
                  <Point X="3.413493115099" Y="1.228583067434" Z="1.2" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.2" />
                  <Point X="3.332255137461" Y="1.255130586169" Z="1.2" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.2" />
                  <Point X="3.273989382221" Y="1.312170255735" Z="1.2" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.2" />
                  <Point X="3.241948409321" Y="1.391849946565" Z="1.2" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.2" />
                  <Point X="3.244936427292" Y="1.472914039836" Z="1.2" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.2" />
                  <Point X="3.285570603047" Y="1.549044150558" Z="1.2" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.2" />
                  <Point X="3.849408863311" Y="1.996374229352" Z="1.2" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.2" />
                  <Point X="4.337089124742" Y="2.637305593656" Z="1.2" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.2" />
                  <Point X="4.113838721774" Y="2.973559088544" Z="1.2" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.2" />
                  <Point X="3.296452010782" Y="2.721127504912" Z="1.2" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.2" />
                  <Point X="2.467562318056" Y="2.255683024579" Z="1.2" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.2" />
                  <Point X="2.393000651089" Y="2.249941372226" Z="1.2" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.2" />
                  <Point X="2.326799356609" Y="2.276541690372" Z="1.2" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.2" />
                  <Point X="2.274216909863" Y="2.330225503773" Z="1.2" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.2" />
                  <Point X="2.249488330533" Y="2.396757792743" Z="1.2" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.2" />
                  <Point X="2.256844918078" Y="2.471907257825" Z="1.2" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.2" />
                  <Point X="2.674497847948" Y="3.215687071173" Z="1.2" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.2" />
                  <Point X="2.930911585237" Y="4.142865146612" Z="1.2" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.2" />
                  <Point X="2.543867945859" Y="4.391162862626" Z="1.2" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.2" />
                  <Point X="2.1387559174" Y="4.60224022644" Z="1.2" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.2" />
                  <Point X="1.718822249809" Y="4.774991410211" Z="1.2" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.2" />
                  <Point X="1.171946662854" Y="4.931532169344" Z="1.2" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.2" />
                  <Point X="0.509790609812" Y="5.043172206907" Z="1.2" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.2" />
                  <Point X="0.101851294648" Y="4.735238699412" Z="1.2" />
                  <Point X="0" Y="4.355124473572" Z="1.2" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>