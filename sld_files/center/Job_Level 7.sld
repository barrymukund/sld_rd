<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#129" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="672" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="0" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000475830078" Y="0.004715576172" />
                  <Width Value="9.783896972656" />
                  <Height Value="9.766443847656" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.610815063477" Y="-3.689845458984" />
                  <Point X="0.563302062988" Y="-3.512524414063" />
                  <Point X="0.557721191406" Y="-3.497141845703" />
                  <Point X="0.542363037109" Y="-3.467377197266" />
                  <Point X="0.52294342041" Y="-3.439396972656" />
                  <Point X="0.378635528564" Y="-3.231476806641" />
                  <Point X="0.356751586914" Y="-3.209021484375" />
                  <Point X="0.330496398926" Y="-3.189777587891" />
                  <Point X="0.302495635986" Y="-3.175669433594" />
                  <Point X="0.272444610596" Y="-3.166342529297" />
                  <Point X="0.049136493683" Y="-3.097036132812" />
                  <Point X="0.020976638794" Y="-3.092766601562" />
                  <Point X="-0.00866468811" Y="-3.092766601562" />
                  <Point X="-0.036823932648" Y="-3.097035888672" />
                  <Point X="-0.066875099182" Y="-3.106362548828" />
                  <Point X="-0.290183074951" Y="-3.175669189453" />
                  <Point X="-0.31818460083" Y="-3.189777587891" />
                  <Point X="-0.344439788818" Y="-3.209021484375" />
                  <Point X="-0.366323272705" Y="-3.231476318359" />
                  <Point X="-0.38574319458" Y="-3.259456298828" />
                  <Point X="-0.53005090332" Y="-3.467376708984" />
                  <Point X="-0.538189025879" Y="-3.481573730469" />
                  <Point X="-0.550990112305" Y="-3.512524414063" />
                  <Point X="-0.904058410645" Y="-4.830192871094" />
                  <Point X="-0.916584655762" Y="-4.876941894531" />
                  <Point X="-1.079339477539" Y="-4.845350585938" />
                  <Point X="-1.11018762207" Y="-4.837413574219" />
                  <Point X="-1.246418212891" Y="-4.802362792969" />
                  <Point X="-1.220682617188" Y="-4.606881835938" />
                  <Point X="-1.214963012695" Y="-4.563438476562" />
                  <Point X="-1.214201171875" Y="-4.5479296875" />
                  <Point X="-1.216508666992" Y="-4.516224121094" />
                  <Point X="-1.223687988281" Y="-4.480131835938" />
                  <Point X="-1.277036132812" Y="-4.211932617188" />
                  <Point X="-1.287938354492" Y="-4.182966308594" />
                  <Point X="-1.304010375977" Y="-4.15512890625" />
                  <Point X="-1.32364453125" Y="-4.131204589844" />
                  <Point X="-1.351311523438" Y="-4.106940917969" />
                  <Point X="-1.556905273438" Y="-3.926639404297" />
                  <Point X="-1.583188476562" Y="-3.910295410156" />
                  <Point X="-1.612885742188" Y="-3.897994384766" />
                  <Point X="-1.643027587891" Y="-3.890966308594" />
                  <Point X="-1.679748046875" Y="-3.888559570312" />
                  <Point X="-1.952616699219" Y="-3.870674804688" />
                  <Point X="-1.98341796875" Y="-3.873708496094" />
                  <Point X="-2.014466674805" Y="-3.882028076172" />
                  <Point X="-2.042657592773" Y="-3.894801269531" />
                  <Point X="-2.073255126953" Y="-3.915245849609" />
                  <Point X="-2.300624023438" Y="-4.067169433594" />
                  <Point X="-2.312785644531" Y="-4.076821533203" />
                  <Point X="-2.335102783203" Y="-4.099461914062" />
                  <Point X="-2.480149169922" Y="-4.288490234375" />
                  <Point X="-2.801707275391" Y="-4.089388916016" />
                  <Point X="-2.844423339844" Y="-4.056499023438" />
                  <Point X="-3.104721923828" Y="-3.856077880859" />
                  <Point X="-2.512058837891" Y="-2.829555419922" />
                  <Point X="-2.423761230469" Y="-2.676619628906" />
                  <Point X="-2.412859130859" Y="-2.647653320312" />
                  <Point X="-2.406588134766" Y="-2.616126953125" />
                  <Point X="-2.40575" Y="-2.583932128906" />
                  <Point X="-2.415716552734" Y="-2.553307373047" />
                  <Point X="-2.431991455078" Y="-2.522420654297" />
                  <Point X="-2.448862548828" Y="-2.499531494141" />
                  <Point X="-2.464153320312" Y="-2.484240722656" />
                  <Point X="-2.489310302734" Y="-2.466213134766" />
                  <Point X="-2.518139160156" Y="-2.45199609375" />
                  <Point X="-2.547756835938" Y="-2.443011474609" />
                  <Point X="-2.578690917969" Y="-2.444024169922" />
                  <Point X="-2.610217285156" Y="-2.450295166016" />
                  <Point X="-2.63918359375" Y="-2.461197265625" />
                  <Point X="-3.775646972656" Y="-3.117334716797" />
                  <Point X="-3.818024414062" Y="-3.141801269531" />
                  <Point X="-4.082859375" Y="-2.793861816406" />
                  <Point X="-4.113480957031" Y="-2.742515136719" />
                  <Point X="-4.306143066406" Y="-2.419450439453" />
                  <Point X="-3.260446777344" Y="-1.617059326172" />
                  <Point X="-3.105954589844" Y="-1.498513427734" />
                  <Point X="-3.083062744141" Y="-1.475877441406" />
                  <Point X="-3.064641601562" Y="-1.447132202148" />
                  <Point X="-3.057014160156" Y="-1.428536132813" />
                  <Point X="-3.052942871094" Y="-1.41630456543" />
                  <Point X="-3.046152099609" Y="-1.390085571289" />
                  <Point X="-3.04334765625" Y="-1.35965637207" />
                  <Point X="-3.045556640625" Y="-1.327985595703" />
                  <Point X="-3.053166992188" Y="-1.296803710938" />
                  <Point X="-3.070726806641" Y="-1.269935791016" />
                  <Point X="-3.094700439453" Y="-1.244130615234" />
                  <Point X="-3.116114013672" Y="-1.22691784668" />
                  <Point X="-3.139455566406" Y="-1.213180297852" />
                  <Point X="-3.168718261719" Y="-1.201956542969" />
                  <Point X="-3.200605957031" Y="-1.195474853516" />
                  <Point X="-3.231929199219" Y="-1.194383789062" />
                  <Point X="-4.666606445312" Y="-1.383262573242" />
                  <Point X="-4.732102050781" Y="-1.391885253906" />
                  <Point X="-4.834077636719" Y="-0.992654174805" />
                  <Point X="-4.842178710938" Y="-0.936012817383" />
                  <Point X="-4.892424316406" Y="-0.584698303223" />
                  <Point X="-3.708865722656" Y="-0.267564697266" />
                  <Point X="-3.532875976562" Y="-0.220408447266" />
                  <Point X="-3.514708740234" Y="-0.213480575562" />
                  <Point X="-3.495848876953" Y="-0.203975616455" />
                  <Point X="-3.484437011719" Y="-0.197184814453" />
                  <Point X="-3.459975830078" Y="-0.180207504272" />
                  <Point X="-3.436020507812" Y="-0.158680435181" />
                  <Point X="-3.416228515625" Y="-0.13080140686" />
                  <Point X="-3.407705810547" Y="-0.112528419495" />
                  <Point X="-3.403071289062" Y="-0.100532119751" />
                  <Point X="-3.394917480469" Y="-0.074260688782" />
                  <Point X="-3.390699951172" Y="-0.042960899353" />
                  <Point X="-3.391797119141" Y="-0.009783764839" />
                  <Point X="-3.396014648438" Y="0.015236000061" />
                  <Point X="-3.404168457031" Y="0.041507579803" />
                  <Point X="-3.417485107422" Y="0.070831970215" />
                  <Point X="-3.438253417969" Y="0.098227256775" />
                  <Point X="-3.453363525391" Y="0.112029556274" />
                  <Point X="-3.463267578125" Y="0.119931915283" />
                  <Point X="-3.487728759766" Y="0.136909378052" />
                  <Point X="-3.501925292969" Y="0.145047164917" />
                  <Point X="-3.532875976562" Y="0.157848251343" />
                  <Point X="-4.84065234375" Y="0.508265808105" />
                  <Point X="-4.89181640625" Y="0.521975158691" />
                  <Point X="-4.82448828125" Y="0.97697076416" />
                  <Point X="-4.808178710938" Y="1.037159179687" />
                  <Point X="-4.703551269531" Y="1.423267700195" />
                  <Point X="-3.8918203125" Y="1.316401245117" />
                  <Point X="-3.765666259766" Y="1.29979284668" />
                  <Point X="-3.744986083984" Y="1.299341674805" />
                  <Point X="-3.723424560547" Y="1.301228149414" />
                  <Point X="-3.70313671875" Y="1.305263793945" />
                  <Point X="-3.695851074219" Y="1.307561035156" />
                  <Point X="-3.641710693359" Y="1.324631347656" />
                  <Point X="-3.622779052734" Y="1.332961669922" />
                  <Point X="-3.604034667969" Y="1.343783691406" />
                  <Point X="-3.587353271484" Y="1.356014892578" />
                  <Point X="-3.57371484375" Y="1.371566772461" />
                  <Point X="-3.561300292969" Y="1.389296875" />
                  <Point X="-3.551352050781" Y="1.407429199219" />
                  <Point X="-3.548428466797" Y="1.414487060547" />
                  <Point X="-3.526704589844" Y="1.46693347168" />
                  <Point X="-3.520915527344" Y="1.486794189453" />
                  <Point X="-3.517157226562" Y="1.50810925293" />
                  <Point X="-3.5158046875" Y="1.528749389648" />
                  <Point X="-3.518951171875" Y="1.549192993164" />
                  <Point X="-3.524552978516" Y="1.570099365234" />
                  <Point X="-3.532049560547" Y="1.589377075195" />
                  <Point X="-3.535576904297" Y="1.596153198242" />
                  <Point X="-3.561789306641" Y="1.646506835938" />
                  <Point X="-3.573281738281" Y="1.663706665039" />
                  <Point X="-3.587194335938" Y="1.680286987305" />
                  <Point X="-3.602135986328" Y="1.694590087891" />
                  <Point X="-4.351860351563" Y="2.269874267578" />
                  <Point X="-4.081153808594" Y="2.733659423828" />
                  <Point X="-4.037955566406" Y="2.789184326172" />
                  <Point X="-3.750504882813" Y="3.158661621094" />
                  <Point X="-3.284137207031" Y="2.889404052734" />
                  <Point X="-3.206656982422" Y="2.844670898438" />
                  <Point X="-3.187724609375" Y="2.836340332031" />
                  <Point X="-3.167082519531" Y="2.829831787109" />
                  <Point X="-3.146794189453" Y="2.825796386719" />
                  <Point X="-3.136647216797" Y="2.824908691406" />
                  <Point X="-3.061244873047" Y="2.818311767578" />
                  <Point X="-3.040565185547" Y="2.818762939453" />
                  <Point X="-3.019106445312" Y="2.821588134766" />
                  <Point X="-2.999015136719" Y="2.826504394531" />
                  <Point X="-2.980463867188" Y="2.835652832031" />
                  <Point X="-2.962209472656" Y="2.847281982422" />
                  <Point X="-2.946077392578" Y="2.860229492188" />
                  <Point X="-2.938875" Y="2.867431884766" />
                  <Point X="-2.885353759766" Y="2.920952880859" />
                  <Point X="-2.872406738281" Y="2.937084228516" />
                  <Point X="-2.86077734375" Y="2.955338623047" />
                  <Point X="-2.851628662109" Y="2.973890136719" />
                  <Point X="-2.846712158203" Y="2.993981933594" />
                  <Point X="-2.843886962891" Y="3.015440917969" />
                  <Point X="-2.843435546875" Y="3.036120361328" />
                  <Point X="-2.844323242188" Y="3.046267578125" />
                  <Point X="-2.850920166016" Y="3.121669921875" />
                  <Point X="-2.854955810547" Y="3.141958496094" />
                  <Point X="-2.861464355469" Y="3.162600585938" />
                  <Point X="-2.869794921875" Y="3.181532958984" />
                  <Point X="-3.183333007812" Y="3.724596679688" />
                  <Point X="-2.700624511719" Y="4.094684326172" />
                  <Point X="-2.632592041016" Y="4.132481933594" />
                  <Point X="-2.167036621094" Y="4.391134765625" />
                  <Point X="-2.066914550781" Y="4.26065234375" />
                  <Point X="-2.0431953125" Y="4.229741210938" />
                  <Point X="-2.028892822266" Y="4.214800292969" />
                  <Point X="-2.012313232422" Y="4.200887695312" />
                  <Point X="-1.995112182617" Y="4.189394042969" />
                  <Point X="-1.983818603516" Y="4.183515136719" />
                  <Point X="-1.899895874023" Y="4.139827148438" />
                  <Point X="-1.8806171875" Y="4.132330566406" />
                  <Point X="-1.859710571289" Y="4.126729003906" />
                  <Point X="-1.839267578125" Y="4.123582519531" />
                  <Point X="-1.818628295898" Y="4.124935546875" />
                  <Point X="-1.797312988281" Y="4.128693847656" />
                  <Point X="-1.77745300293" Y="4.134482421875" />
                  <Point X="-1.765689941406" Y="4.139354980469" />
                  <Point X="-1.678279052734" Y="4.175562011719" />
                  <Point X="-1.66014465332" Y="4.185510742187" />
                  <Point X="-1.642415039062" Y="4.197925292969" />
                  <Point X="-1.626863525391" Y="4.211563964844" />
                  <Point X="-1.614632568359" Y="4.228245117188" />
                  <Point X="-1.603810791016" Y="4.246989257813" />
                  <Point X="-1.595480224609" Y="4.265922363281" />
                  <Point X="-1.591651611328" Y="4.278065429688" />
                  <Point X="-1.563200805664" Y="4.368298828125" />
                  <Point X="-1.559165649414" Y="4.388584960938" />
                  <Point X="-1.557279174805" Y="4.410146484375" />
                  <Point X="-1.55773034668" Y="4.430826660156" />
                  <Point X="-1.584201904297" Y="4.631897949219" />
                  <Point X="-0.949635437012" Y="4.80980859375" />
                  <Point X="-0.867165893555" Y="4.819459960938" />
                  <Point X="-0.294710784912" Y="4.886457519531" />
                  <Point X="-0.156824111938" Y="4.371856933594" />
                  <Point X="-0.133903305054" Y="4.286315429688" />
                  <Point X="-0.121129844666" Y="4.258124023438" />
                  <Point X="-0.10327155304" Y="4.231397460938" />
                  <Point X="-0.082114532471" Y="4.20880859375" />
                  <Point X="-0.054819446564" Y="4.19421875" />
                  <Point X="-0.024381370544" Y="4.183886230469" />
                  <Point X="0.0061559062" Y="4.178844238281" />
                  <Point X="0.03669316864" Y="4.183886230469" />
                  <Point X="0.067131248474" Y="4.19421875" />
                  <Point X="0.094426467896" Y="4.20880859375" />
                  <Point X="0.115583503723" Y="4.231397460938" />
                  <Point X="0.133441802979" Y="4.258124023438" />
                  <Point X="0.146215255737" Y="4.286315429688" />
                  <Point X="0.307419403076" Y="4.8879375" />
                  <Point X="0.844041870117" Y="4.83173828125" />
                  <Point X="0.91227746582" Y="4.815264648438" />
                  <Point X="1.481025634766" Y="4.677951171875" />
                  <Point X="1.524134033203" Y="4.662315429688" />
                  <Point X="1.894642822266" Y="4.527929199219" />
                  <Point X="1.937596191406" Y="4.507841308594" />
                  <Point X="2.294579101562" Y="4.340892089844" />
                  <Point X="2.3361015625" Y="4.316701171875" />
                  <Point X="2.680974609375" Y="4.11577734375" />
                  <Point X="2.720114746094" Y="4.087943603516" />
                  <Point X="2.943260009766" Y="3.929254882812" />
                  <Point X="2.249778808594" Y="2.728109863281" />
                  <Point X="2.147581054688" Y="2.551097900391" />
                  <Point X="2.140168945312" Y="2.534929443359" />
                  <Point X="2.132620849609" Y="2.513323486328" />
                  <Point X="2.130530273438" Y="2.506533691406" />
                  <Point X="2.111607177734" Y="2.435770019531" />
                  <Point X="2.108389892578" Y="2.412435058594" />
                  <Point X="2.108045166016" Y="2.385298339844" />
                  <Point X="2.108720703125" Y="2.37271875" />
                  <Point X="2.116099121094" Y="2.311528320312" />
                  <Point X="2.121442138672" Y="2.289604980469" />
                  <Point X="2.129708251953" Y="2.267516357422" />
                  <Point X="2.140071044922" Y="2.247471191406" />
                  <Point X="2.145166259766" Y="2.239962158203" />
                  <Point X="2.183028808594" Y="2.184162597656" />
                  <Point X="2.198719970703" Y="2.166327636719" />
                  <Point X="2.219529052734" Y="2.147932373047" />
                  <Point X="2.229107910156" Y="2.140497314453" />
                  <Point X="2.284907470703" Y="2.102635009766" />
                  <Point X="2.304951904297" Y="2.092272460938" />
                  <Point X="2.327040283203" Y="2.084006347656" />
                  <Point X="2.348961669922" Y="2.078663818359" />
                  <Point X="2.357196289062" Y="2.077670654297" />
                  <Point X="2.418386474609" Y="2.070292236328" />
                  <Point X="2.442623779297" Y="2.070483398438" />
                  <Point X="2.471049316406" Y="2.074367675781" />
                  <Point X="2.482729248047" Y="2.076717773438" />
                  <Point X="2.553492675781" Y="2.095640869141" />
                  <Point X="2.565283447266" Y="2.099638183594" />
                  <Point X="2.588533935547" Y="2.110145019531" />
                  <Point X="3.903905273438" Y="2.869574951172" />
                  <Point X="3.967326416016" Y="2.906191162109" />
                  <Point X="4.123273925781" Y="2.689459472656" />
                  <Point X="4.145090332031" Y="2.653407226562" />
                  <Point X="4.262198730469" Y="2.459884277344" />
                  <Point X="3.365336914062" Y="1.771697387695" />
                  <Point X="3.230783935547" Y="1.668451171875" />
                  <Point X="3.217474853516" Y="1.656042236328" />
                  <Point X="3.201376464844" Y="1.637851806641" />
                  <Point X="3.197120361328" Y="1.632686767578" />
                  <Point X="3.146191650391" Y="1.566246704102" />
                  <Point X="3.134083007812" Y="1.545434326172" />
                  <Point X="3.123060546875" Y="1.519353637695" />
                  <Point X="3.119076904297" Y="1.50795715332" />
                  <Point X="3.100105957031" Y="1.440121582031" />
                  <Point X="3.096652587891" Y="1.417823120117" />
                  <Point X="3.095836425781" Y="1.394252929688" />
                  <Point X="3.097739501953" Y="1.371768066406" />
                  <Point X="3.099835205078" Y="1.361611206055" />
                  <Point X="3.115408447266" Y="1.286135253906" />
                  <Point X="3.123318359375" Y="1.26316784668" />
                  <Point X="3.136216552734" Y="1.237126464844" />
                  <Point X="3.141982666016" Y="1.227076538086" />
                  <Point X="3.184340087891" Y="1.16269519043" />
                  <Point X="3.198893798828" Y="1.145450195312" />
                  <Point X="3.216137695313" Y="1.129360473633" />
                  <Point X="3.234348388672" Y="1.116034301758" />
                  <Point X="3.242608642578" Y="1.111384643555" />
                  <Point X="3.303990478516" Y="1.07683203125" />
                  <Point X="3.326990234375" Y="1.067595703125" />
                  <Point X="3.356133789062" Y="1.060121582031" />
                  <Point X="3.367286376953" Y="1.057962524414" />
                  <Point X="3.450278808594" Y="1.046994018555" />
                  <Point X="3.462702148438" Y="1.046175048828" />
                  <Point X="3.488203613281" Y="1.04698449707" />
                  <Point X="4.737718261719" Y="1.211486328125" />
                  <Point X="4.77683984375" Y="1.21663671875" />
                  <Point X="4.845936035156" Y="0.932811462402" />
                  <Point X="4.852811523438" Y="0.888648803711" />
                  <Point X="4.890864746094" Y="0.644238525391" />
                  <Point X="3.870780517578" Y="0.370907806396" />
                  <Point X="3.716579833984" Y="0.32958984375" />
                  <Point X="3.699206054688" Y="0.323057250977" />
                  <Point X="3.676152587891" Y="0.311707244873" />
                  <Point X="3.670572998047" Y="0.308725494385" />
                  <Point X="3.589035644531" Y="0.261595489502" />
                  <Point X="3.569540771484" Y="0.246660675049" />
                  <Point X="3.548645263672" Y="0.225851257324" />
                  <Point X="3.540947509766" Y="0.217187530518" />
                  <Point X="3.492025146484" Y="0.154848876953" />
                  <Point X="3.480301269531" Y="0.135569366455" />
                  <Point X="3.47052734375" Y="0.114105743408" />
                  <Point X="3.463680908203" Y="0.092604484558" />
                  <Point X="3.461486328125" Y="0.081145370483" />
                  <Point X="3.445178710938" Y="-0.004006166458" />
                  <Point X="3.443731689453" Y="-0.028745119095" />
                  <Point X="3.445926269531" Y="-0.059013492584" />
                  <Point X="3.447373046875" Y="-0.070012680054" />
                  <Point X="3.463680664062" Y="-0.155164367676" />
                  <Point X="3.47052734375" Y="-0.176665786743" />
                  <Point X="3.480301269531" Y="-0.19812940979" />
                  <Point X="3.492024658203" Y="-0.217408447266" />
                  <Point X="3.498608154297" Y="-0.225797607422" />
                  <Point X="3.547530517578" Y="-0.288136260986" />
                  <Point X="3.565653320313" Y="-0.305776428223" />
                  <Point X="3.590937988281" Y="-0.324539245605" />
                  <Point X="3.600008300781" Y="-0.330498077393" />
                  <Point X="3.681545654297" Y="-0.377628082275" />
                  <Point X="3.692710205078" Y="-0.383138977051" />
                  <Point X="3.716579833984" Y="-0.392150024414" />
                  <Point X="4.86244140625" Y="-0.699182556152" />
                  <Point X="4.89147265625" Y="-0.706961425781" />
                  <Point X="4.855021972656" Y="-0.948728637695" />
                  <Point X="4.846213867188" Y="-0.987328552246" />
                  <Point X="4.801173828125" Y="-1.184698974609" />
                  <Point X="3.603211669922" Y="-1.026984375" />
                  <Point X="3.424382080078" Y="-1.003440979004" />
                  <Point X="3.408035644531" Y="-1.002710266113" />
                  <Point X="3.374658447266" Y="-1.005508666992" />
                  <Point X="3.353123046875" Y="-1.01018939209" />
                  <Point X="3.193094238281" Y="-1.044972290039" />
                  <Point X="3.163973632812" Y="-1.056597167969" />
                  <Point X="3.136147216797" Y="-1.073489501953" />
                  <Point X="3.112397460938" Y="-1.093959960937" />
                  <Point X="3.099380615234" Y="-1.109615234375" />
                  <Point X="3.002653320312" Y="-1.225948120117" />
                  <Point X="2.987932617188" Y="-1.250330444336" />
                  <Point X="2.976589355469" Y="-1.277715820312" />
                  <Point X="2.969757568359" Y="-1.305364990234" />
                  <Point X="2.967891845703" Y="-1.325639160156" />
                  <Point X="2.954028564453" Y="-1.476295288086" />
                  <Point X="2.956347412109" Y="-1.507564697266" />
                  <Point X="2.964079101562" Y="-1.539185668945" />
                  <Point X="2.976450439453" Y="-1.567996459961" />
                  <Point X="2.988368408203" Y="-1.586534057617" />
                  <Point X="3.076930664062" Y="-1.724286987305" />
                  <Point X="3.086932373047" Y="-1.73723828125" />
                  <Point X="3.110628417969" Y="-1.760909057617" />
                  <Point X="4.173994628906" Y="-2.576859375" />
                  <Point X="4.213122070312" Y="-2.606883056641" />
                  <Point X="4.124811035156" Y="-2.749784423828" />
                  <Point X="4.106591308594" Y="-2.775671875" />
                  <Point X="4.028981445312" Y="-2.8859453125" />
                  <Point X="2.960226318359" Y="-2.268898925781" />
                  <Point X="2.800954589844" Y="-2.176943115234" />
                  <Point X="2.786131591797" Y="-2.170012451172" />
                  <Point X="2.754223876953" Y="-2.159824951172" />
                  <Point X="2.728593261719" Y="-2.155196289062" />
                  <Point X="2.538133544922" Y="-2.120799316406" />
                  <Point X="2.506781982422" Y="-2.120395263672" />
                  <Point X="2.474609130859" Y="-2.125353027344" />
                  <Point X="2.444833740234" Y="-2.135176513672" />
                  <Point X="2.423541015625" Y="-2.146382568359" />
                  <Point X="2.265315673828" Y="-2.229655517578" />
                  <Point X="2.242384033203" Y="-2.246549072266" />
                  <Point X="2.221424316406" Y="-2.267509033203" />
                  <Point X="2.204531982422" Y="-2.290439208984" />
                  <Point X="2.193325683594" Y="-2.311731933594" />
                  <Point X="2.110052978516" Y="-2.469957275391" />
                  <Point X="2.100229003906" Y="-2.499733398438" />
                  <Point X="2.095271240234" Y="-2.531906005859" />
                  <Point X="2.095675292969" Y="-2.5632578125" />
                  <Point X="2.100303955078" Y="-2.588888183594" />
                  <Point X="2.134700927734" Y="-2.779348144531" />
                  <Point X="2.138985595703" Y="-2.795141845703" />
                  <Point X="2.151819091797" Y="-2.826078613281" />
                  <Point X="2.835139160156" Y="-4.009623291016" />
                  <Point X="2.861283447266" Y="-4.054906494141" />
                  <Point X="2.78184765625" Y="-4.111645507812" />
                  <Point X="2.761483154297" Y="-4.124827148438" />
                  <Point X="2.701765136719" Y="-4.163481445312" />
                  <Point X="1.880085083008" Y="-3.092647705078" />
                  <Point X="1.758546020508" Y="-2.934255126953" />
                  <Point X="1.747503540039" Y="-2.922179443359" />
                  <Point X="1.721924316406" Y="-2.900557617188" />
                  <Point X="1.696645629883" Y="-2.884305664062" />
                  <Point X="1.50880090332" Y="-2.763538818359" />
                  <Point X="1.479989746094" Y="-2.751167236328" />
                  <Point X="1.448368774414" Y="-2.743435546875" />
                  <Point X="1.417099487305" Y="-2.741116699219" />
                  <Point X="1.389452758789" Y="-2.743660888672" />
                  <Point X="1.184012573242" Y="-2.762565673828" />
                  <Point X="1.15636315918" Y="-2.769397460938" />
                  <Point X="1.128978027344" Y="-2.780740722656" />
                  <Point X="1.104596069336" Y="-2.795461181641" />
                  <Point X="1.083248168945" Y="-2.813211181641" />
                  <Point X="0.924612121582" Y="-2.945111572266" />
                  <Point X="0.904141052246" Y="-2.968861816406" />
                  <Point X="0.887248779297" Y="-2.996688476562" />
                  <Point X="0.875624206543" Y="-3.025809082031" />
                  <Point X="0.869241394043" Y="-3.055175537109" />
                  <Point X="0.821809936523" Y="-3.273396972656" />
                  <Point X="0.819724487305" Y="-3.289627441406" />
                  <Point X="0.819742248535" Y="-3.323120117188" />
                  <Point X="1.013389953613" Y="-4.794020019531" />
                  <Point X="1.022065246582" Y="-4.859915039062" />
                  <Point X="0.975671203613" Y="-4.870084960938" />
                  <Point X="0.95685723877" Y="-4.873502929688" />
                  <Point X="0.929315673828" Y="-4.878506347656" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058432983398" Y="-4.752635742188" />
                  <Point X="-1.08651574707" Y="-4.74541015625" />
                  <Point X="-1.141246582031" Y="-4.731328613281" />
                  <Point X="-1.126495361328" Y="-4.619281738281" />
                  <Point X="-1.120775756836" Y="-4.575838378906" />
                  <Point X="-1.120077392578" Y="-4.568099609375" />
                  <Point X="-1.119451660156" Y="-4.541033691406" />
                  <Point X="-1.121759277344" Y="-4.509328125" />
                  <Point X="-1.123334106445" Y="-4.497690429688" />
                  <Point X="-1.130513427734" Y="-4.461598144531" />
                  <Point X="-1.183861572266" Y="-4.193398925781" />
                  <Point X="-1.18812512207" Y="-4.17846875" />
                  <Point X="-1.19902734375" Y="-4.149502441406" />
                  <Point X="-1.205666137695" Y="-4.135466308594" />
                  <Point X="-1.221738037109" Y="-4.10762890625" />
                  <Point X="-1.23057434082" Y="-4.094861572266" />
                  <Point X="-1.250208496094" Y="-4.070937255859" />
                  <Point X="-1.261006103516" Y="-4.059780273438" />
                  <Point X="-1.288673217773" Y="-4.035516601562" />
                  <Point X="-1.494266845703" Y="-3.855215087891" />
                  <Point X="-1.506738769531" Y="-3.845965332031" />
                  <Point X="-1.533021972656" Y="-3.829621337891" />
                  <Point X="-1.546833496094" Y="-3.822526855469" />
                  <Point X="-1.576530761719" Y="-3.810225830078" />
                  <Point X="-1.591313598633" Y="-3.805476074219" />
                  <Point X="-1.621455444336" Y="-3.798447998047" />
                  <Point X="-1.636814453125" Y="-3.796169677734" />
                  <Point X="-1.673534912109" Y="-3.793762939453" />
                  <Point X="-1.946403564453" Y="-3.775878173828" />
                  <Point X="-1.961928344727" Y="-3.776132324219" />
                  <Point X="-1.992729736328" Y="-3.779166015625" />
                  <Point X="-2.008006103516" Y="-3.781945556641" />
                  <Point X="-2.03905480957" Y="-3.790265136719" />
                  <Point X="-2.053674072266" Y="-3.79549609375" />
                  <Point X="-2.081864990234" Y="-3.808269287109" />
                  <Point X="-2.095436767578" Y="-3.815811523438" />
                  <Point X="-2.126034179688" Y="-3.836256103516" />
                  <Point X="-2.353403076172" Y="-3.9881796875" />
                  <Point X="-2.359681640625" Y="-3.992757080078" />
                  <Point X="-2.380442138672" Y="-4.010131103516" />
                  <Point X="-2.402759277344" Y="-4.032771484375" />
                  <Point X="-2.410471435547" Y="-4.041629638672" />
                  <Point X="-2.503202880859" Y="-4.162479492188" />
                  <Point X="-2.747583496094" Y="-4.011164794922" />
                  <Point X="-2.786466064453" Y="-3.9812265625" />
                  <Point X="-2.980862792969" Y="-3.831547607422" />
                  <Point X="-2.429786376953" Y="-2.877055419922" />
                  <Point X="-2.341488769531" Y="-2.724119628906" />
                  <Point X="-2.334850097656" Y="-2.710083251953" />
                  <Point X="-2.323947998047" Y="-2.681116943359" />
                  <Point X="-2.319684570312" Y="-2.666187011719" />
                  <Point X="-2.313413574219" Y="-2.634660644531" />
                  <Point X="-2.311620361328" Y="-2.618599365234" />
                  <Point X="-2.310782226562" Y="-2.586404541016" />
                  <Point X="-2.315413574219" Y="-2.554532958984" />
                  <Point X="-2.325380126953" Y="-2.523908203125" />
                  <Point X="-2.331670410156" Y="-2.509021484375" />
                  <Point X="-2.3479453125" Y="-2.478134765625" />
                  <Point X="-2.355519775391" Y="-2.466055175781" />
                  <Point X="-2.372390869141" Y="-2.443166015625" />
                  <Point X="-2.3816875" Y="-2.432356445312" />
                  <Point X="-2.396978271484" Y="-2.417065673828" />
                  <Point X="-2.408817138672" Y="-2.407020751953" />
                  <Point X="-2.433974121094" Y="-2.388993164062" />
                  <Point X="-2.447292236328" Y="-2.381010498047" />
                  <Point X="-2.47612109375" Y="-2.366793457031" />
                  <Point X="-2.490561523438" Y="-2.361086914062" />
                  <Point X="-2.520179199219" Y="-2.352102294922" />
                  <Point X="-2.550865234375" Y="-2.348062255859" />
                  <Point X="-2.581799316406" Y="-2.349074951172" />
                  <Point X="-2.597224609375" Y="-2.350849609375" />
                  <Point X="-2.628750976562" Y="-2.357120605469" />
                  <Point X="-2.643680908203" Y="-2.361384033203" />
                  <Point X="-2.672647216797" Y="-2.372286132812" />
                  <Point X="-2.68668359375" Y="-2.378924804688" />
                  <Point X="-3.793089111328" Y="-3.017708496094" />
                  <Point X="-4.004015869141" Y="-2.74059375" />
                  <Point X="-4.031888671875" Y="-2.693855957031" />
                  <Point X="-4.181265625" Y="-2.443373779297" />
                  <Point X="-3.202614501953" Y="-1.692427856445" />
                  <Point X="-3.048122314453" Y="-1.573881958008" />
                  <Point X="-3.039157958984" Y="-1.566065063477" />
                  <Point X="-3.016266113281" Y="-1.543429077148" />
                  <Point X="-3.003077636719" Y="-1.527135253906" />
                  <Point X="-2.984656494141" Y="-1.498390014648" />
                  <Point X="-2.976747802734" Y="-1.483182983398" />
                  <Point X="-2.969120361328" Y="-1.464587036133" />
                  <Point X="-2.960977539062" Y="-1.440123657227" />
                  <Point X="-2.954186767578" Y="-1.413904785156" />
                  <Point X="-2.951552978516" Y="-1.398803955078" />
                  <Point X="-2.948748535156" Y="-1.368374755859" />
                  <Point X="-2.948577880859" Y="-1.353046386719" />
                  <Point X="-2.950786865234" Y="-1.321375610352" />
                  <Point X="-2.953265625" Y="-1.305460693359" />
                  <Point X="-2.960875976562" Y="-1.274278808594" />
                  <Point X="-2.97364453125" Y="-1.244830932617" />
                  <Point X="-2.991204345703" Y="-1.217963012695" />
                  <Point X="-3.001127197266" Y="-1.205276000977" />
                  <Point X="-3.025100830078" Y="-1.179470825195" />
                  <Point X="-3.035181884766" Y="-1.170086425781" />
                  <Point X="-3.056595458984" Y="-1.152873657227" />
                  <Point X="-3.067928222656" Y="-1.145045166016" />
                  <Point X="-3.091269775391" Y="-1.131307617188" />
                  <Point X="-3.105434814453" Y="-1.124480957031" />
                  <Point X="-3.134697509766" Y="-1.113257080078" />
                  <Point X="-3.149794921875" Y="-1.108860351562" />
                  <Point X="-3.181682617188" Y="-1.102378540039" />
                  <Point X="-3.197298828125" Y="-1.100532348633" />
                  <Point X="-3.228622070312" Y="-1.09944140625" />
                  <Point X="-3.244329101562" Y="-1.100196533203" />
                  <Point X="-4.660920898438" Y="-1.286694213867" />
                  <Point X="-4.740762207031" Y="-0.974117797852" />
                  <Point X="-4.748135742188" Y="-0.922562438965" />
                  <Point X="-4.786452148438" Y="-0.654654541016" />
                  <Point X="-3.684277832031" Y="-0.359327667236" />
                  <Point X="-3.508288085938" Y="-0.312171386719" />
                  <Point X="-3.499026367188" Y="-0.309173370361" />
                  <Point X="-3.471953613281" Y="-0.298315734863" />
                  <Point X="-3.45309375" Y="-0.288810699463" />
                  <Point X="-3.430270019531" Y="-0.275229400635" />
                  <Point X="-3.405808837891" Y="-0.258252044678" />
                  <Point X="-3.396477539062" Y="-0.250868453979" />
                  <Point X="-3.372522216797" Y="-0.229341323853" />
                  <Point X="-3.358556396484" Y="-0.213674087524" />
                  <Point X="-3.338764404297" Y="-0.18579510498" />
                  <Point X="-3.330132568359" Y="-0.170957336426" />
                  <Point X="-3.321609863281" Y="-0.152684448242" />
                  <Point X="-3.312340820312" Y="-0.128691955566" />
                  <Point X="-3.304187011719" Y="-0.102420478821" />
                  <Point X="-3.300768310547" Y="-0.086946899414" />
                  <Point X="-3.29655078125" Y="-0.055647212982" />
                  <Point X="-3.295751953125" Y="-0.039820953369" />
                  <Point X="-3.296849121094" Y="-0.006643863201" />
                  <Point X="-3.298118652344" Y="0.006007364273" />
                  <Point X="-3.302336181641" Y="0.031027139664" />
                  <Point X="-3.305284179688" Y="0.043395687103" />
                  <Point X="-3.313437988281" Y="0.069667312622" />
                  <Point X="-3.317669677734" Y="0.080788024902" />
                  <Point X="-3.330986328125" Y="0.110112358093" />
                  <Point X="-3.341780273438" Y="0.128223556519" />
                  <Point X="-3.362548583984" Y="0.155618911743" />
                  <Point X="-3.374182373047" Y="0.168369277954" />
                  <Point X="-3.389292480469" Y="0.182171585083" />
                  <Point X="-3.409100341797" Y="0.197976150513" />
                  <Point X="-3.433561523438" Y="0.214953643799" />
                  <Point X="-3.440484130859" Y="0.219328643799" />
                  <Point X="-3.465616699219" Y="0.232834899902" />
                  <Point X="-3.496567382812" Y="0.245635940552" />
                  <Point X="-3.508288085938" Y="0.249611160278" />
                  <Point X="-4.785446289062" Y="0.591824584961" />
                  <Point X="-4.731331542969" Y="0.957524414062" />
                  <Point X="-4.716485351562" Y="1.012312561035" />
                  <Point X="-4.633586425781" Y="1.318236938477" />
                  <Point X="-3.904220214844" Y="1.222213989258" />
                  <Point X="-3.778066162109" Y="1.20560559082" />
                  <Point X="-3.76773828125" Y="1.204815551758" />
                  <Point X="-3.747058105469" Y="1.204364257812" />
                  <Point X="-3.736705810547" Y="1.20470324707" />
                  <Point X="-3.715144287109" Y="1.20658972168" />
                  <Point X="-3.704890380859" Y="1.208053710938" />
                  <Point X="-3.684602539062" Y="1.212089355469" />
                  <Point X="-3.667282958984" Y="1.216958251953" />
                  <Point X="-3.613142578125" Y="1.234028564453" />
                  <Point X="-3.603448974609" Y="1.237677124023" />
                  <Point X="-3.584517333984" Y="1.246007324219" />
                  <Point X="-3.575279296875" Y="1.250689086914" />
                  <Point X="-3.556534912109" Y="1.261511230469" />
                  <Point X="-3.547860595703" Y="1.267171264648" />
                  <Point X="-3.531179199219" Y="1.27940246582" />
                  <Point X="-3.515927978516" Y="1.293377563477" />
                  <Point X="-3.502289550781" Y="1.308929443359" />
                  <Point X="-3.495895019531" Y="1.317077636719" />
                  <Point X="-3.48348046875" Y="1.334807739258" />
                  <Point X="-3.478012207031" Y="1.343601196289" />
                  <Point X="-3.468063964844" Y="1.361733520508" />
                  <Point X="-3.460660400391" Y="1.378130859375" />
                  <Point X="-3.438936523438" Y="1.430577270508" />
                  <Point X="-3.4355" Y="1.440348876953" />
                  <Point X="-3.4297109375" Y="1.460209594727" />
                  <Point X="-3.427358642578" Y="1.470298095703" />
                  <Point X="-3.423600341797" Y="1.49161315918" />
                  <Point X="-3.422360595703" Y="1.501897216797" />
                  <Point X="-3.421008056641" Y="1.522537353516" />
                  <Point X="-3.421910400391" Y="1.543200683594" />
                  <Point X="-3.425056884766" Y="1.563644287109" />
                  <Point X="-3.427188232422" Y="1.573780639648" />
                  <Point X="-3.432790039062" Y="1.594687011719" />
                  <Point X="-3.436011962891" Y="1.604530517578" />
                  <Point X="-3.443508544922" Y="1.623808349609" />
                  <Point X="-3.451310546875" Y="1.640018554688" />
                  <Point X="-3.477522949219" Y="1.690372192383" />
                  <Point X="-3.482799316406" Y="1.699285644531" />
                  <Point X="-3.494291748047" Y="1.716485473633" />
                  <Point X="-3.500507568359" Y="1.724771606445" />
                  <Point X="-3.514420166016" Y="1.741351928711" />
                  <Point X="-3.521501708984" Y="1.748912475586" />
                  <Point X="-3.536443359375" Y="1.763215576172" />
                  <Point X="-3.544303710938" Y="1.769958618164" />
                  <Point X="-4.227614746094" Y="2.294282226563" />
                  <Point X="-4.002292236328" Y="2.680313232422" />
                  <Point X="-3.962975097656" Y="2.730849609375" />
                  <Point X="-3.726338623047" Y="3.035012451172" />
                  <Point X="-3.331637207031" Y="2.807131591797" />
                  <Point X="-3.254156982422" Y="2.7623984375" />
                  <Point X="-3.244918457031" Y="2.757716552734" />
                  <Point X="-3.225986083984" Y="2.749385986328" />
                  <Point X="-3.216292236328" Y="2.745737304688" />
                  <Point X="-3.195650146484" Y="2.739228759766" />
                  <Point X="-3.185615234375" Y="2.736656982422" />
                  <Point X="-3.165326904297" Y="2.732621582031" />
                  <Point X="-3.144926513672" Y="2.730270263672" />
                  <Point X="-3.069524169922" Y="2.723673339844" />
                  <Point X="-3.059172851562" Y="2.723334472656" />
                  <Point X="-3.038493164062" Y="2.723785644531" />
                  <Point X="-3.028164794922" Y="2.724575683594" />
                  <Point X="-3.006706054688" Y="2.727400878906" />
                  <Point X="-2.996526611328" Y="2.729310546875" />
                  <Point X="-2.976435302734" Y="2.734226806641" />
                  <Point X="-2.956997802734" Y="2.741301513672" />
                  <Point X="-2.938446533203" Y="2.750449951172" />
                  <Point X="-2.929420898438" Y="2.755530273438" />
                  <Point X="-2.911166503906" Y="2.767159423828" />
                  <Point X="-2.902746337891" Y="2.773193359375" />
                  <Point X="-2.886614257812" Y="2.786140869141" />
                  <Point X="-2.871699951172" Y="2.800256835938" />
                  <Point X="-2.818178710938" Y="2.853777832031" />
                  <Point X="-2.811265380859" Y="2.861489501953" />
                  <Point X="-2.798318359375" Y="2.877620849609" />
                  <Point X="-2.792284667969" Y="2.886040527344" />
                  <Point X="-2.780655273438" Y="2.904294921875" />
                  <Point X="-2.775574707031" Y="2.913320800781" />
                  <Point X="-2.766426025391" Y="2.931872314453" />
                  <Point X="-2.759351318359" Y="2.951309570312" />
                  <Point X="-2.754434814453" Y="2.971401367188" />
                  <Point X="-2.752524902344" Y="2.981581542969" />
                  <Point X="-2.749699707031" Y="3.003040527344" />
                  <Point X="-2.748909667969" Y="3.013367675781" />
                  <Point X="-2.748458251953" Y="3.034047119141" />
                  <Point X="-2.749684570312" Y="3.054546630859" />
                  <Point X="-2.756281494141" Y="3.129948974609" />
                  <Point X="-2.757745605469" Y="3.140203369141" />
                  <Point X="-2.76178125" Y="3.160491943359" />
                  <Point X="-2.764352783203" Y="3.170526123047" />
                  <Point X="-2.770861328125" Y="3.191168212891" />
                  <Point X="-2.774510009766" Y="3.200862060547" />
                  <Point X="-2.782840576172" Y="3.219794433594" />
                  <Point X="-2.787522460938" Y="3.229032958984" />
                  <Point X="-3.059387451172" Y="3.699916503906" />
                  <Point X="-2.648372070312" Y="4.015037597656" />
                  <Point X="-2.586454345703" Y="4.049437988281" />
                  <Point X="-2.192524902344" Y="4.268296875" />
                  <Point X="-2.142283203125" Y="4.2028203125" />
                  <Point X="-2.118563964844" Y="4.171909179688" />
                  <Point X="-2.111820556641" Y="4.164048339844" />
                  <Point X="-2.097518066406" Y="4.149107421875" />
                  <Point X="-2.089959472656" Y="4.14202734375" />
                  <Point X="-2.073379882812" Y="4.128114746094" />
                  <Point X="-2.065093261719" Y="4.1218984375" />
                  <Point X="-2.047892211914" Y="4.110404785156" />
                  <Point X="-2.027683837891" Y="4.099248535156" />
                  <Point X="-1.943760986328" Y="4.055560546875" />
                  <Point X="-1.934325561523" Y="4.051285644531" />
                  <Point X="-1.91504699707" Y="4.0437890625" />
                  <Point X="-1.905203613281" Y="4.040567138672" />
                  <Point X="-1.88429699707" Y="4.034965576172" />
                  <Point X="-1.874162475586" Y="4.032834716797" />
                  <Point X="-1.853719360352" Y="4.029688232422" />
                  <Point X="-1.833053222656" Y="4.028785888672" />
                  <Point X="-1.812413818359" Y="4.030138916016" />
                  <Point X="-1.802132446289" Y="4.031378662109" />
                  <Point X="-1.780817138672" Y="4.035136962891" />
                  <Point X="-1.770729614258" Y="4.037489013672" />
                  <Point X="-1.750869628906" Y="4.043277587891" />
                  <Point X="-1.729334106445" Y="4.051586669922" />
                  <Point X="-1.641923217773" Y="4.087793701172" />
                  <Point X="-1.632585571289" Y="4.092272705078" />
                  <Point X="-1.614451171875" Y="4.102221191406" />
                  <Point X="-1.605654418945" Y="4.10769140625" />
                  <Point X="-1.587924804688" Y="4.120105957031" />
                  <Point X="-1.579776245117" Y="4.126501464844" />
                  <Point X="-1.564224853516" Y="4.140140136719" />
                  <Point X="-1.550250976562" Y="4.155390136719" />
                  <Point X="-1.538020019531" Y="4.172071289062" />
                  <Point X="-1.532359863281" Y="4.180745605469" />
                  <Point X="-1.521538085938" Y="4.199489746094" />
                  <Point X="-1.516855834961" Y="4.208729003906" />
                  <Point X="-1.508525268555" Y="4.227662109375" />
                  <Point X="-1.501048339844" Y="4.249499023438" />
                  <Point X="-1.47259753418" Y="4.339732421875" />
                  <Point X="-1.470026245117" Y="4.349765136719" />
                  <Point X="-1.465991088867" Y="4.370051269531" />
                  <Point X="-1.46452722168" Y="4.3803046875" />
                  <Point X="-1.46264074707" Y="4.401866210938" />
                  <Point X="-1.462301757812" Y="4.41221875" />
                  <Point X="-1.462752929688" Y="4.432898925781" />
                  <Point X="-1.46354309082" Y="4.4432265625" />
                  <Point X="-1.479266235352" Y="4.562655273438" />
                  <Point X="-0.931178833008" Y="4.7163203125" />
                  <Point X="-0.856123413086" Y="4.725104003906" />
                  <Point X="-0.365221954346" Y="4.782556640625" />
                  <Point X="-0.248587020874" Y="4.347269042969" />
                  <Point X="-0.225666244507" Y="4.261727539062" />
                  <Point X="-0.220435317993" Y="4.247107910156" />
                  <Point X="-0.207661773682" Y="4.218916503906" />
                  <Point X="-0.200119155884" Y="4.205344726562" />
                  <Point X="-0.182260925293" Y="4.178618164062" />
                  <Point X="-0.172608306885" Y="4.166455566406" />
                  <Point X="-0.151451248169" Y="4.143866699219" />
                  <Point X="-0.126898002625" Y="4.125026367188" />
                  <Point X="-0.099602958679" Y="4.110436523438" />
                  <Point X="-0.085356712341" Y="4.104260742188" />
                  <Point X="-0.054918586731" Y="4.093927978516" />
                  <Point X="-0.039857292175" Y="4.090155273438" />
                  <Point X="-0.009319890976" Y="4.08511328125" />
                  <Point X="0.021631721497" Y="4.08511328125" />
                  <Point X="0.052168972015" Y="4.090155273438" />
                  <Point X="0.067230415344" Y="4.093927978516" />
                  <Point X="0.097668540955" Y="4.104260742188" />
                  <Point X="0.111914489746" Y="4.110436523438" />
                  <Point X="0.139209686279" Y="4.125026367188" />
                  <Point X="0.16376322937" Y="4.143866699219" />
                  <Point X="0.184920150757" Y="4.166455566406" />
                  <Point X="0.194572906494" Y="4.178617675781" />
                  <Point X="0.212431137085" Y="4.205344238281" />
                  <Point X="0.219973754883" Y="4.218916503906" />
                  <Point X="0.232747146606" Y="4.247107910156" />
                  <Point X="0.237978225708" Y="4.261727539062" />
                  <Point X="0.378190307617" Y="4.785006347656" />
                  <Point X="0.827878723145" Y="4.737911132812" />
                  <Point X="0.889982788086" Y="4.72291796875" />
                  <Point X="1.45359375" Y="4.586844726562" />
                  <Point X="1.491741699219" Y="4.573008300781" />
                  <Point X="1.858255615234" Y="4.440070800781" />
                  <Point X="1.897351318359" Y="4.421787109375" />
                  <Point X="2.250453369141" Y="4.256652832031" />
                  <Point X="2.288278808594" Y="4.234615722656" />
                  <Point X="2.629428466797" Y="4.035861328125" />
                  <Point X="2.665059082031" Y="4.0105234375" />
                  <Point X="2.817779785156" Y="3.901916748047" />
                  <Point X="2.167506347656" Y="2.775609863281" />
                  <Point X="2.06530859375" Y="2.598597900391" />
                  <Point X="2.061223144531" Y="2.590687011719" />
                  <Point X="2.050484375" Y="2.566260986328" />
                  <Point X="2.042936157227" Y="2.544655029297" />
                  <Point X="2.038755004883" Y="2.531075439453" />
                  <Point X="2.01983190918" Y="2.460311767578" />
                  <Point X="2.017497436523" Y="2.448745361328" />
                  <Point X="2.014280151367" Y="2.425410400391" />
                  <Point X="2.013397583008" Y="2.413641845703" />
                  <Point X="2.013052856445" Y="2.386505126953" />
                  <Point X="2.014403930664" Y="2.361345947266" />
                  <Point X="2.021782348633" Y="2.300155517578" />
                  <Point X="2.02380065918" Y="2.289033935547" />
                  <Point X="2.029143676758" Y="2.267110595703" />
                  <Point X="2.032468261719" Y="2.256308837891" />
                  <Point X="2.040734375" Y="2.234220214844" />
                  <Point X="2.045318359375" Y="2.223889160156" />
                  <Point X="2.055681152344" Y="2.203843994141" />
                  <Point X="2.066555175781" Y="2.186620849609" />
                  <Point X="2.104417724609" Y="2.130821289063" />
                  <Point X="2.111703857422" Y="2.121411132812" />
                  <Point X="2.127395019531" Y="2.103576171875" />
                  <Point X="2.135800048828" Y="2.095151367188" />
                  <Point X="2.156609130859" Y="2.076756103516" />
                  <Point X="2.175766845703" Y="2.061885986328" />
                  <Point X="2.23156640625" Y="2.024023681641" />
                  <Point X="2.241279785156" Y="2.018245117188" />
                  <Point X="2.26132421875" Y="2.00788269043" />
                  <Point X="2.271655273438" Y="2.003298706055" />
                  <Point X="2.293743652344" Y="1.995032592773" />
                  <Point X="2.304545898438" Y="1.991708007812" />
                  <Point X="2.326467285156" Y="1.986365356445" />
                  <Point X="2.345821044922" Y="1.983354125977" />
                  <Point X="2.407011230469" Y="1.975975830078" />
                  <Point X="2.419135742188" Y="1.975295288086" />
                  <Point X="2.443373046875" Y="1.975486328125" />
                  <Point X="2.455485839844" Y="1.976358154297" />
                  <Point X="2.483911376953" Y="1.980242431641" />
                  <Point X="2.507271240234" Y="1.984942504883" />
                  <Point X="2.578034667969" Y="2.003865600586" />
                  <Point X="2.583994384766" Y="2.005670654297" />
                  <Point X="2.604404541016" Y="2.013067260742" />
                  <Point X="2.627655029297" Y="2.02357409668" />
                  <Point X="2.636033935547" Y="2.027872558594" />
                  <Point X="3.940404785156" Y="2.780951416016" />
                  <Point X="4.043954101562" Y="2.637041503906" />
                  <Point X="4.063813232422" Y="2.604223632812" />
                  <Point X="4.136884277344" Y="2.483472412109" />
                  <Point X="3.307504638672" Y="1.847065917969" />
                  <Point X="3.172951660156" Y="1.743819702148" />
                  <Point X="3.165999511719" Y="1.737934936523" />
                  <Point X="3.146333496094" Y="1.719001831055" />
                  <Point X="3.130235107422" Y="1.700811401367" />
                  <Point X="3.121722900391" Y="1.690481567383" />
                  <Point X="3.070794189453" Y="1.624041503906" />
                  <Point X="3.064077880859" Y="1.614020385742" />
                  <Point X="3.051969238281" Y="1.593208129883" />
                  <Point X="3.046577148438" Y="1.582416870117" />
                  <Point X="3.0355546875" Y="1.556336181641" />
                  <Point X="3.027587158203" Y="1.533543212891" />
                  <Point X="3.008616210938" Y="1.465707763672" />
                  <Point X="3.006225097656" Y="1.454660888672" />
                  <Point X="3.002771728516" Y="1.432362426758" />
                  <Point X="3.001709472656" Y="1.421110717773" />
                  <Point X="3.000893310547" Y="1.397540527344" />
                  <Point X="3.001174804688" Y="1.386240966797" />
                  <Point X="3.003077880859" Y="1.363756103516" />
                  <Point X="3.006795166016" Y="1.3424140625" />
                  <Point X="3.022368408203" Y="1.266937988281" />
                  <Point X="3.025586181641" Y="1.255200805664" />
                  <Point X="3.03349609375" Y="1.232233276367" />
                  <Point X="3.038188232422" Y="1.221003173828" />
                  <Point X="3.051086425781" Y="1.194961791992" />
                  <Point X="3.062618652344" Y="1.174861938477" />
                  <Point X="3.104976074219" Y="1.11048046875" />
                  <Point X="3.111739013672" Y="1.101424438477" />
                  <Point X="3.126292724609" Y="1.084179443359" />
                  <Point X="3.134083496094" Y="1.075990844727" />
                  <Point X="3.151327392578" Y="1.059901000977" />
                  <Point X="3.160035644531" Y="1.05269519043" />
                  <Point X="3.178246337891" Y="1.039369018555" />
                  <Point X="3.196009033203" Y="1.028598999023" />
                  <Point X="3.257390869141" Y="0.994046386719" />
                  <Point X="3.268588134766" Y="0.988674987793" />
                  <Point X="3.291587890625" Y="0.979438659668" />
                  <Point X="3.303390380859" Y="0.975573730469" />
                  <Point X="3.332533935547" Y="0.968099609375" />
                  <Point X="3.354839111328" Y="0.963781555176" />
                  <Point X="3.437831542969" Y="0.952813049316" />
                  <Point X="3.444029785156" Y="0.952199829102" />
                  <Point X="3.465716064453" Y="0.951222900391" />
                  <Point X="3.491217529297" Y="0.952032287598" />
                  <Point X="3.500603515625" Y="0.952797241211" />
                  <Point X="4.704704101562" Y="1.11132019043" />
                  <Point X="4.75268359375" Y="0.914234985352" />
                  <Point X="4.758942382812" Y="0.874034729004" />
                  <Point X="4.78387109375" Y="0.713920898438" />
                  <Point X="3.846192626953" Y="0.462670715332" />
                  <Point X="3.691991943359" Y="0.421352813721" />
                  <Point X="3.683145019531" Y="0.41851171875" />
                  <Point X="3.657244384766" Y="0.408287506104" />
                  <Point X="3.634190917969" Y="0.39693762207" />
                  <Point X="3.623031738281" Y="0.390974151611" />
                  <Point X="3.541494384766" Y="0.343844177246" />
                  <Point X="3.531261962891" Y="0.337009155273" />
                  <Point X="3.511767089844" Y="0.32207434082" />
                  <Point X="3.502504394531" Y="0.313974395752" />
                  <Point X="3.481608886719" Y="0.29316494751" />
                  <Point X="3.466213378906" Y="0.275837615967" />
                  <Point X="3.417291015625" Y="0.213498916626" />
                  <Point X="3.410854736328" Y="0.204208648682" />
                  <Point X="3.399130859375" Y="0.184929138184" />
                  <Point X="3.393843505859" Y="0.17493989563" />
                  <Point X="3.384069580078" Y="0.153476211548" />
                  <Point X="3.380005615234" Y="0.142929641724" />
                  <Point X="3.373159179688" Y="0.121428359985" />
                  <Point X="3.368182128906" Y="0.099014389038" />
                  <Point X="3.351874511719" Y="0.013862977028" />
                  <Point X="3.350340820313" Y="0.001541097403" />
                  <Point X="3.348893798828" Y="-0.023197929382" />
                  <Point X="3.34898046875" Y="-0.035614925385" />
                  <Point X="3.351175048828" Y="-0.065883323669" />
                  <Point X="3.354068603516" Y="-0.087881591797" />
                  <Point X="3.370376220703" Y="-0.173033309937" />
                  <Point X="3.373159179688" Y="-0.183989044189" />
                  <Point X="3.380005859375" Y="-0.205490463257" />
                  <Point X="3.384069580078" Y="-0.216036300659" />
                  <Point X="3.393843505859" Y="-0.237499832153" />
                  <Point X="3.399130615234" Y="-0.247488479614" />
                  <Point X="3.410854003906" Y="-0.266767547607" />
                  <Point X="3.423873291016" Y="-0.284446685791" />
                  <Point X="3.472795654297" Y="-0.346785369873" />
                  <Point X="3.481268066406" Y="-0.356211791992" />
                  <Point X="3.499390869141" Y="-0.373851989746" />
                  <Point X="3.509041503906" Y="-0.382066070557" />
                  <Point X="3.534326171875" Y="-0.400828826904" />
                  <Point X="3.552467041016" Y="-0.412746734619" />
                  <Point X="3.634004394531" Y="-0.459876708984" />
                  <Point X="3.639496582031" Y="-0.462815429688" />
                  <Point X="3.659157958984" Y="-0.472016662598" />
                  <Point X="3.683027587891" Y="-0.481027679443" />
                  <Point X="3.691991943359" Y="-0.48391305542" />
                  <Point X="4.784876953125" Y="-0.776750488281" />
                  <Point X="4.761612304688" Y="-0.931057800293" />
                  <Point X="4.753594726562" Y="-0.966193786621" />
                  <Point X="4.727802246094" Y="-1.079219604492" />
                  <Point X="3.615611572266" Y="-0.932797180176" />
                  <Point X="3.436781982422" Y="-0.909253662109" />
                  <Point X="3.428624511719" Y="-0.908535827637" />
                  <Point X="3.400098632812" Y="-0.908042419434" />
                  <Point X="3.366721435547" Y="-0.910840820312" />
                  <Point X="3.354481201172" Y="-0.912676147461" />
                  <Point X="3.332945800781" Y="-0.917356872559" />
                  <Point X="3.172916992188" Y="-0.952139709473" />
                  <Point X="3.157873046875" Y="-0.956742553711" />
                  <Point X="3.128752441406" Y="-0.968367553711" />
                  <Point X="3.114675537109" Y="-0.975389404297" />
                  <Point X="3.086849121094" Y="-0.992281738281" />
                  <Point X="3.074124023438" Y="-1.001530395508" />
                  <Point X="3.050374267578" Y="-1.022000793457" />
                  <Point X="3.039349365234" Y="-1.033222900391" />
                  <Point X="3.026332519531" Y="-1.048878173828" />
                  <Point X="2.929605224609" Y="-1.16521105957" />
                  <Point X="2.921326171875" Y="-1.176847290039" />
                  <Point X="2.90660546875" Y="-1.201229614258" />
                  <Point X="2.900163818359" Y="-1.213975830078" />
                  <Point X="2.888820556641" Y="-1.241361206055" />
                  <Point X="2.884363037109" Y="-1.254927612305" />
                  <Point X="2.87753125" Y="-1.282576904297" />
                  <Point X="2.875157226562" Y="-1.296659545898" />
                  <Point X="2.873291503906" Y="-1.31693359375" />
                  <Point X="2.859428222656" Y="-1.46758972168" />
                  <Point X="2.859288818359" Y="-1.483320922852" />
                  <Point X="2.861607666016" Y="-1.514590332031" />
                  <Point X="2.864065917969" Y="-1.530128540039" />
                  <Point X="2.871797607422" Y="-1.561749511719" />
                  <Point X="2.876786621094" Y="-1.576669067383" />
                  <Point X="2.889157958984" Y="-1.605479858398" />
                  <Point X="2.896540283203" Y="-1.619371459961" />
                  <Point X="2.908458251953" Y="-1.637908935547" />
                  <Point X="2.997020507812" Y="-1.775661865234" />
                  <Point X="3.001741455078" Y="-1.782352294922" />
                  <Point X="3.01979296875" Y="-1.80444934082" />
                  <Point X="3.043489013672" Y="-1.828119995117" />
                  <Point X="3.052796142578" Y="-1.836277587891" />
                  <Point X="4.087170410156" Y="-2.629981933594" />
                  <Point X="4.045490722656" Y="-2.697426269531" />
                  <Point X="4.028903320312" Y="-2.720994628906" />
                  <Point X="4.001274169922" Y="-2.760251953125" />
                  <Point X="3.007726318359" Y="-2.186626464844" />
                  <Point X="2.848454589844" Y="-2.094670654297" />
                  <Point X="2.841191894531" Y="-2.090885253906" />
                  <Point X="2.815026123047" Y="-2.079513183594" />
                  <Point X="2.783118408203" Y="-2.069325683594" />
                  <Point X="2.771106933594" Y="-2.066337158203" />
                  <Point X="2.745476318359" Y="-2.061708496094" />
                  <Point X="2.555016601562" Y="-2.027311523438" />
                  <Point X="2.539357666016" Y="-2.025807250977" />
                  <Point X="2.508006103516" Y="-2.025403198242" />
                  <Point X="2.492313476562" Y="-2.026503540039" />
                  <Point X="2.460140625" Y="-2.031461303711" />
                  <Point X="2.444844726562" Y="-2.035136108398" />
                  <Point X="2.415069335938" Y="-2.044959594727" />
                  <Point X="2.40058984375" Y="-2.051108154297" />
                  <Point X="2.379297119141" Y="-2.062314208984" />
                  <Point X="2.221071777344" Y="-2.145587158203" />
                  <Point X="2.208969238281" Y="-2.153169677734" />
                  <Point X="2.186037597656" Y="-2.170063232422" />
                  <Point X="2.175208496094" Y="-2.179374267578" />
                  <Point X="2.154248779297" Y="-2.200334228516" />
                  <Point X="2.144938232422" Y="-2.211162841797" />
                  <Point X="2.128045898438" Y="-2.234093017578" />
                  <Point X="2.120464111328" Y="-2.246194580078" />
                  <Point X="2.1092578125" Y="-2.267487304688" />
                  <Point X="2.025985107422" Y="-2.425712646484" />
                  <Point X="2.019836303711" Y="-2.440192382812" />
                  <Point X="2.010012329102" Y="-2.469968505859" />
                  <Point X="2.006337280273" Y="-2.485264892578" />
                  <Point X="2.001379516602" Y="-2.5174375" />
                  <Point X="2.000279174805" Y="-2.533130126953" />
                  <Point X="2.000683227539" Y="-2.564481933594" />
                  <Point X="2.00218737793" Y="-2.580141113281" />
                  <Point X="2.006816162109" Y="-2.605771484375" />
                  <Point X="2.041213134766" Y="-2.796231445312" />
                  <Point X="2.043015014648" Y="-2.804221679688" />
                  <Point X="2.051236083984" Y="-2.83154296875" />
                  <Point X="2.064069580078" Y="-2.862479736328" />
                  <Point X="2.069546630859" Y="-2.873578613281" />
                  <Point X="2.735893310547" Y="-4.027724365234" />
                  <Point X="2.723754394531" Y="-4.036083740234" />
                  <Point X="1.955453613281" Y="-3.034815429688" />
                  <Point X="1.833914550781" Y="-2.876422851562" />
                  <Point X="1.828653442383" Y="-2.870146240234" />
                  <Point X="1.808831420898" Y="-2.849626708984" />
                  <Point X="1.783252197266" Y="-2.828004882812" />
                  <Point X="1.773299438477" Y="-2.820647705078" />
                  <Point X="1.748020507812" Y="-2.804395751953" />
                  <Point X="1.56017590332" Y="-2.68362890625" />
                  <Point X="1.546284545898" Y="-2.676246337891" />
                  <Point X="1.517473388672" Y="-2.663874755859" />
                  <Point X="1.502553588867" Y="-2.658885742188" />
                  <Point X="1.470932617188" Y="-2.651154052734" />
                  <Point X="1.45539440918" Y="-2.648695800781" />
                  <Point X="1.42412512207" Y="-2.646376953125" />
                  <Point X="1.408393920898" Y="-2.646516357422" />
                  <Point X="1.380747192383" Y="-2.649060546875" />
                  <Point X="1.175306884766" Y="-2.667965332031" />
                  <Point X="1.161224731445" Y="-2.670339355469" />
                  <Point X="1.133575317383" Y="-2.677171142578" />
                  <Point X="1.120008422852" Y="-2.68162890625" />
                  <Point X="1.092623168945" Y="-2.692972167969" />
                  <Point X="1.079877197266" Y="-2.699413574219" />
                  <Point X="1.055495239258" Y="-2.714134033203" />
                  <Point X="1.04385925293" Y="-2.722413085938" />
                  <Point X="1.022511291504" Y="-2.740163085938" />
                  <Point X="0.863875244141" Y="-2.872063476562" />
                  <Point X="0.852653320312" Y="-2.883087890625" />
                  <Point X="0.832182128906" Y="-2.906838134766" />
                  <Point X="0.822933044434" Y="-2.919564208984" />
                  <Point X="0.80604083252" Y="-2.947390869141" />
                  <Point X="0.799018859863" Y="-2.961468261719" />
                  <Point X="0.787394165039" Y="-2.990588867188" />
                  <Point X="0.782791748047" Y="-3.005631835938" />
                  <Point X="0.776408874512" Y="-3.034998291016" />
                  <Point X="0.728977478027" Y="-3.253219726562" />
                  <Point X="0.727584594727" Y="-3.261290039062" />
                  <Point X="0.724724487305" Y="-3.289677734375" />
                  <Point X="0.7247421875" Y="-3.323170410156" />
                  <Point X="0.725554992676" Y="-3.335520019531" />
                  <Point X="0.83309161377" Y="-4.152341308594" />
                  <Point X="0.702578063965" Y="-3.665257568359" />
                  <Point X="0.655064941406" Y="-3.487936523438" />
                  <Point X="0.652606262207" Y="-3.480124511719" />
                  <Point X="0.642145141602" Y="-3.453580322266" />
                  <Point X="0.62678692627" Y="-3.423815673828" />
                  <Point X="0.620407714844" Y="-3.413210449219" />
                  <Point X="0.600988037109" Y="-3.385230224609" />
                  <Point X="0.456680114746" Y="-3.177310058594" />
                  <Point X="0.446670654297" Y="-3.165172851562" />
                  <Point X="0.424786804199" Y="-3.142717529297" />
                  <Point X="0.41291229248" Y="-3.132399169922" />
                  <Point X="0.386657165527" Y="-3.113155273438" />
                  <Point X="0.373242767334" Y="-3.104937988281" />
                  <Point X="0.345241912842" Y="-3.090829833984" />
                  <Point X="0.330655609131" Y="-3.084938964844" />
                  <Point X="0.300604492188" Y="-3.075612060547" />
                  <Point X="0.077296348572" Y="-3.006305664062" />
                  <Point X="0.063377368927" Y="-3.003109619141" />
                  <Point X="0.03521749115" Y="-2.998840087891" />
                  <Point X="0.02097659111" Y="-2.997766601562" />
                  <Point X="-0.008664761543" Y="-2.997766601562" />
                  <Point X="-0.022905065536" Y="-2.998840087891" />
                  <Point X="-0.051064350128" Y="-3.003109375" />
                  <Point X="-0.064983032227" Y="-3.006305175781" />
                  <Point X="-0.095034286499" Y="-3.015631835938" />
                  <Point X="-0.318342132568" Y="-3.084938476562" />
                  <Point X="-0.332929046631" Y="-3.090829345703" />
                  <Point X="-0.360930633545" Y="-3.104937744141" />
                  <Point X="-0.374345336914" Y="-3.113155273438" />
                  <Point X="-0.400600463867" Y="-3.132399169922" />
                  <Point X="-0.412474975586" Y="-3.142717529297" />
                  <Point X="-0.43435836792" Y="-3.165172363281" />
                  <Point X="-0.444367370605" Y="-3.17730859375" />
                  <Point X="-0.463787200928" Y="-3.205288574219" />
                  <Point X="-0.608094848633" Y="-3.413208984375" />
                  <Point X="-0.612469970703" Y="-3.420131835938" />
                  <Point X="-0.62597668457" Y="-3.445265136719" />
                  <Point X="-0.638777893066" Y="-3.476215820312" />
                  <Point X="-0.642753112793" Y="-3.487936523438" />
                  <Point X="-0.985425415039" Y="-4.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.179065560745" Y="-2.447062948705" />
                  <Point X="-3.708978690879" Y="-2.969147309434" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.96195599007" Y="-3.798800069815" />
                  <Point X="-2.823087107497" Y="-3.953029588721" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.106327383967" Y="-2.385871605903" />
                  <Point X="-3.624868270431" Y="-2.920586122775" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.912011529193" Y="-3.712293740874" />
                  <Point X="-2.511021291405" Y="-4.157638517286" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.030725782816" Y="-2.327860418046" />
                  <Point X="-3.540757849982" Y="-2.872024936116" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.862067068317" Y="-3.625787411933" />
                  <Point X="-2.445977374289" Y="-4.087901833411" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.955124181664" Y="-2.269849230189" />
                  <Point X="-3.456647429533" Y="-2.823463749457" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.81212260744" Y="-3.539281082992" />
                  <Point X="-2.38447946606" Y="-4.014226907689" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.879522580513" Y="-2.211838042332" />
                  <Point X="-3.372537009085" Y="-2.774902562797" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.762178146563" Y="-3.45277475405" />
                  <Point X="-2.307633726125" Y="-3.957597475935" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.6911104024" Y="-1.168503178224" />
                  <Point X="-4.592769415512" Y="-1.277721908982" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.803920979361" Y="-2.153826854474" />
                  <Point X="-3.288426588636" Y="-2.726341376138" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.712233685687" Y="-3.366268425109" />
                  <Point X="-2.227818239691" Y="-3.904266281809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.741227034868" Y="-0.970867746766" />
                  <Point X="-4.478481977382" Y="-1.262675695819" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.72831937821" Y="-2.095815666617" />
                  <Point X="-3.204316168187" Y="-2.677780189479" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.66228922481" Y="-3.279762096168" />
                  <Point X="-2.148002753257" Y="-3.850935087683" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.765366888397" Y="-0.802082451094" />
                  <Point X="-4.364194539253" Y="-1.247629482657" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.652717777058" Y="-2.03780447876" />
                  <Point X="-3.120205747739" Y="-2.62921900282" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.612344763934" Y="-3.193255767227" />
                  <Point X="-2.065329911347" Y="-3.800777308307" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.77342051271" Y="-0.651162722905" />
                  <Point X="-4.249907101123" Y="-1.232583269494" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.577116175907" Y="-1.979793290903" />
                  <Point X="-3.03609532729" Y="-2.58065781616" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.562400303057" Y="-3.106749438285" />
                  <Point X="-1.95971782878" Y="-3.776096136686" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.135936906223" Y="-4.690997538755" />
                  <Point X="-1.087072557405" Y="-4.745266896081" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.670432552916" Y="-0.623567167691" />
                  <Point X="-4.135619662993" Y="-1.217537056331" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.501514574755" Y="-1.921782103046" />
                  <Point X="-2.951984906841" Y="-2.532096629501" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.512455842181" Y="-3.020243109344" />
                  <Point X="-1.82490888937" Y="-3.783841359668" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.12004440361" Y="-4.566672678813" />
                  <Point X="-0.97497157282" Y="-4.727792380249" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.567444593122" Y="-0.595971612478" />
                  <Point X="-4.021332224864" Y="-1.202490843169" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.425912973604" Y="-1.863770915188" />
                  <Point X="-2.867874486392" Y="-2.483535442842" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.462511381304" Y="-2.933736780403" />
                  <Point X="-1.689056357356" Y="-3.792745609657" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.142903306057" Y="-4.399310023442" />
                  <Point X="-0.945653965834" Y="-4.618377609236" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.464456633328" Y="-0.568376057265" />
                  <Point X="-3.907044786734" Y="-1.187444630006" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.350311372452" Y="-1.805759727331" />
                  <Point X="-2.783764065944" Y="-2.434974256183" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.412566911854" Y="-2.847230460983" />
                  <Point X="-1.521652101623" Y="-3.836691598873" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.179151756181" Y="-4.217076768854" />
                  <Point X="-0.916336358847" Y="-4.508962838223" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.361468673534" Y="-0.540780502052" />
                  <Point X="-3.792757348605" Y="-1.172398416844" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.274709771301" Y="-1.747748539474" />
                  <Point X="-2.699653645495" Y="-2.386413069523" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.362622426112" Y="-2.760724159658" />
                  <Point X="-0.88701875186" Y="-4.39954806721" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.25848071374" Y="-0.513184946839" />
                  <Point X="-3.678469910475" Y="-1.157352203681" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.199108167659" Y="-1.689737354383" />
                  <Point X="-2.60283515967" Y="-2.35196561931" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.319738596793" Y="-2.666376204947" />
                  <Point X="-0.857701144873" Y="-4.290133296197" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.155492753946" Y="-0.485589391626" />
                  <Point X="-3.564182472345" Y="-1.142305990518" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.123506512808" Y="-1.631726226165" />
                  <Point X="-2.450090422412" Y="-2.379630563846" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.329976292274" Y="-2.513030819986" />
                  <Point X="-0.828383537887" Y="-4.180718525184" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.052504794152" Y="-0.457993836413" />
                  <Point X="-3.449895034216" Y="-1.127259777356" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.047916338499" Y="-1.573702347514" />
                  <Point X="-0.7990659309" Y="-4.071303754172" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.949516834358" Y="-0.430398281199" />
                  <Point X="-3.335607596086" Y="-1.112213564193" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.986002115153" Y="-1.50048978657" />
                  <Point X="-0.769748323913" Y="-3.961888983159" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.779327423498" Y="0.633175016274" />
                  <Point X="-4.728310639594" Y="0.576515137605" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.846528874564" Y="-0.402802725986" />
                  <Point X="-3.21896989982" Y="-1.09977757713" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.951383276618" Y="-1.396962629659" />
                  <Point X="-0.740430716927" Y="-3.852474212146" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.761283859351" Y="0.755110880358" />
                  <Point X="-4.559826670585" Y="0.531370005312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.74354091477" Y="-0.375207170773" />
                  <Point X="-0.71111310994" Y="-3.743059441133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.743240295205" Y="0.877046744443" />
                  <Point X="-4.391342701576" Y="0.48622487302" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.640552945118" Y="-0.347611626509" />
                  <Point X="-0.681795502953" Y="-3.63364467012" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.721277048372" Y="0.99462935988" />
                  <Point X="-4.222858732566" Y="0.441079740727" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.537564962104" Y="-0.320016097083" />
                  <Point X="-0.652477895967" Y="-3.524229899107" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.69170472633" Y="1.103761241165" />
                  <Point X="-4.054374763557" Y="0.395934608434" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.443153288386" Y="-0.282895611224" />
                  <Point X="-0.614676359801" Y="-3.424237486015" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.662132350225" Y="1.212893062407" />
                  <Point X="-3.885890794548" Y="0.350789476141" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.368053010751" Y="-0.224327647194" />
                  <Point X="-0.55963674711" Y="-3.343389896445" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.628552578023" Y="1.317574219392" />
                  <Point X="-3.717406825539" Y="0.305644343849" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.316540548719" Y="-0.13956275996" />
                  <Point X="-0.503991288381" Y="-3.263215167065" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.483525934607" Y="1.298481086467" />
                  <Point X="-3.548922856529" Y="0.260499211556" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.296409232704" Y="-0.019945579229" />
                  <Point X="-0.448345713204" Y="-3.183040567014" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.338499291191" Y="1.279387953543" />
                  <Point X="-0.379821540355" Y="-3.117169098712" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.193472647775" Y="1.260294820618" />
                  <Point X="-0.289161593793" Y="-3.07588189772" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.048446004359" Y="1.241201687694" />
                  <Point X="-0.189247693295" Y="-3.044872253781" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.90341936165" Y="1.222108555555" />
                  <Point X="-0.08933377182" Y="-3.01386263314" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.789595904507" Y="-3.990012931324" />
                  <Point X="0.815509642904" Y="-4.018793053493" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.759858735784" Y="1.204643600069" />
                  <Point X="0.024229167405" Y="-2.998011782427" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.735436686641" Y="-3.787887753931" />
                  <Point X="0.793617235429" Y="-3.852503799534" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.64845651921" Y="1.222894176398" />
                  <Point X="0.191425729608" Y="-3.041727104609" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.681277376503" Y="-3.58576247406" />
                  <Point X="0.771724827953" Y="-3.686214545575" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.555815203422" Y="1.261980843932" />
                  <Point X="0.379784160078" Y="-3.108945062525" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.508275482198" Y="-3.251649132919" />
                  <Point X="0.749832420477" Y="-3.519925291615" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.487887100589" Y="1.328514315054" />
                  <Point X="0.727940013001" Y="-3.353636037656" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.443036794964" Y="1.42067827657" />
                  <Point X="0.736052239064" Y="-3.220670305207" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.178131449973" Y="2.379058872245" />
                  <Point X="-3.8205664844" Y="1.981942746614" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.421726345528" Y="1.538985896967" />
                  <Point X="0.760910496291" Y="-3.106302924543" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.127854252448" Y="2.4651956597" />
                  <Point X="0.786674410994" Y="-2.992941378406" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.077577054923" Y="2.551332447154" />
                  <Point X="0.834529586955" Y="-2.90411466349" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.027299857397" Y="2.637469234609" />
                  <Point X="0.903694671093" Y="-2.838954999286" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.972509872543" Y="2.718594063979" />
                  <Point X="0.976799497817" Y="-2.778170862503" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.91325402138" Y="2.794759046339" />
                  <Point X="1.050302359748" Y="-2.717828788602" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.853998196971" Y="2.870924058411" />
                  <Point X="1.140081648709" Y="-2.675563518258" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.794742372562" Y="2.947089070483" />
                  <Point X="1.254512653813" Y="-2.660676752374" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.735486548153" Y="3.023254082554" />
                  <Point X="1.372566318519" Y="-2.64981335758" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.5012016626" Y="2.905029628861" />
                  <Point X="1.511162147633" Y="-2.661764347661" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.580464844188" Y="-3.849345304596" />
                  <Point X="2.726627276423" Y="-4.011675131033" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.237973863803" Y="2.754660813504" />
                  <Point X="1.805976093619" Y="-2.847213133382" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.843358875913" Y="-2.888730919236" />
                  <Point X="2.498164590102" Y="-3.615966340196" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.083324655258" Y="2.724880739323" />
                  <Point X="2.269701903781" Y="-3.220257549359" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.96699769401" Y="2.737661832585" />
                  <Point X="2.054396384139" Y="-2.839161272497" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.884562136781" Y="2.788083143297" />
                  <Point X="2.015529889065" Y="-2.654020384423" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.817031264514" Y="2.855057783657" />
                  <Point X="2.004156898732" Y="-2.49941412679" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.764104140226" Y="2.938251529286" />
                  <Point X="2.040609986937" Y="-2.397924110518" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.750621223408" Y="3.065252505368" />
                  <Point X="2.087766900355" Y="-2.308321896484" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.818382876169" Y="3.282484717187" />
                  <Point X="2.13736989737" Y="-2.221436333504" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.046845840324" Y="3.678193816591" />
                  <Point X="2.205823840345" Y="-2.155486867024" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.987911754521" Y="3.754716155584" />
                  <Point X="2.291412420897" Y="-2.108567343475" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.912284451932" Y="3.812698799103" />
                  <Point X="2.378146345601" Y="-2.062919853475" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.836657149343" Y="3.870681442622" />
                  <Point X="2.475521894801" Y="-2.029091084817" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.761029846754" Y="3.928664086141" />
                  <Point X="2.610830774535" Y="-2.03739154778" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.685402544165" Y="3.98664672966" />
                  <Point X="2.76349009205" Y="-2.06496162408" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.604885020199" Y="4.039198232119" />
                  <Point X="2.993452445316" Y="-2.17838541932" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.519675596666" Y="4.086538852198" />
                  <Point X="3.259691782505" Y="-2.332098886905" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.434466135739" Y="4.133879430748" />
                  <Point X="3.525931090099" Y="-2.485812321621" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.349256674812" Y="4.181220009297" />
                  <Point X="2.953768467027" Y="-1.708386079682" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.104968996412" Y="-1.876311279866" />
                  <Point X="3.792170397692" Y="-2.639525756336" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.264047213885" Y="4.228560587847" />
                  <Point X="2.8599253361" Y="-1.462187451808" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.518548022517" Y="-2.193662049893" />
                  <Point X="4.013309903525" Y="-2.7431507868" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.012804826545" Y="4.09150292045" />
                  <Point X="2.871778447879" Y="-1.333376393852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.932127048623" Y="-2.511012819919" />
                  <Point X="4.067665663924" Y="-2.661543702315" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.828752882119" Y="4.029067799828" />
                  <Point X="2.89762458168" Y="-1.220106161273" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.723405136273" Y="4.05404254712" />
                  <Point X="2.951894606485" Y="-1.138403857765" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.63084994483" Y="4.093224865428" />
                  <Point X="3.013268037892" Y="-1.064590686527" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.554658111117" Y="4.150580533616" />
                  <Point X="3.080288961755" Y="-0.997049691087" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.505137187043" Y="4.23755724783" />
                  <Point X="3.168816559993" Y="-0.953394277361" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.471867120298" Y="4.342582367571" />
                  <Point X="3.275454784372" Y="-0.929852751678" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.468490776302" Y="4.480807829912" />
                  <Point X="3.384806227468" Y="-0.909324560658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.427435678091" Y="4.577186796279" />
                  <Point X="3.522770708478" Y="-0.920574367632" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.32536704297" Y="4.605803364979" />
                  <Point X="3.667797408943" Y="-0.939667563916" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.223298407849" Y="4.634419933679" />
                  <Point X="3.812824004918" Y="-0.958760644152" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.121229772728" Y="4.663036502379" />
                  <Point X="3.957850600893" Y="-0.977853724388" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.019161137607" Y="4.69165307108" />
                  <Point X="3.374402550727" Y="-0.187893745883" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.603335543113" Y="-0.442149592285" />
                  <Point X="4.102877196868" Y="-0.996946804623" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.915218330299" Y="4.718188160693" />
                  <Point X="3.349201908624" Y="-0.017930325146" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.793189900871" Y="-0.511028945769" />
                  <Point X="4.247903792843" Y="-1.016039884859" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.799569821479" Y="4.731722751714" />
                  <Point X="3.368885875567" Y="0.102183687063" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.961673842318" Y="-0.55617404745" />
                  <Point X="4.392930388818" Y="-1.035132965095" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.68392158029" Y="4.745257639969" />
                  <Point X="-0.203888229294" Y="4.212126592817" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.124131214048" Y="4.123547453539" />
                  <Point X="3.408437080102" Y="0.200232896567" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.130157783764" Y="-0.601319149131" />
                  <Point X="4.537956984793" Y="-1.054226045331" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.568273339101" Y="4.758792528223" />
                  <Point X="-0.270130858506" Y="4.427671758072" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.036151580004" Y="4.08751064879" />
                  <Point X="3.467198791006" Y="0.276946677283" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.29864172521" Y="-0.646464250812" />
                  <Point X="4.682983580768" Y="-1.073319125567" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.452625097912" Y="4.772327416478" />
                  <Point X="-0.324290144343" Y="4.629797010954" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.133132324579" Y="4.121777892405" />
                  <Point X="3.537313631369" Y="0.341051530337" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.467125666657" Y="-0.691609352493" />
                  <Point X="4.745662123576" Y="-1.000955427383" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.201054477639" Y="4.18831797142" />
                  <Point X="3.621168125605" Y="0.389896951851" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.635609608103" Y="-0.736754454174" />
                  <Point X="4.768632970715" Y="-0.884491865455" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.243668838113" Y="4.282965201603" />
                  <Point X="2.020666510263" Y="2.309409348092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.307339019076" Y="1.991027272146" />
                  <Point X="3.715104147117" Y="0.427545703004" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.272986430933" Y="4.39237998835" />
                  <Point X="2.018512876584" Y="2.453776472844" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.448814605248" Y="1.975877987837" />
                  <Point X="3.003822877741" Y="1.359478854573" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.360822328376" Y="0.962990796911" />
                  <Point X="3.818092140255" Y="0.455141221185" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.302304023752" Y="4.501794775097" />
                  <Point X="2.048969856778" Y="2.561925841714" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.556608541704" Y="1.998135965224" />
                  <Point X="3.01510798324" Y="1.488920747411" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.498024976188" Y="0.952587091421" />
                  <Point X="3.921080117105" Y="0.482736757455" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.331621616572" Y="4.611209561844" />
                  <Point X="2.095968970905" Y="2.651703309616" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.650268753244" Y="2.036091034383" />
                  <Point X="3.050924649865" Y="1.591117581455" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.612416497258" Y="0.967517708767" />
                  <Point X="4.024068087843" Y="0.510332300514" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.360939209392" Y="4.720624348591" />
                  <Point X="2.145913381736" Y="2.738209694138" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.734379181402" Y="2.084652212481" />
                  <Point X="3.106809684524" Y="1.671026234809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.726703916892" Y="0.982563942471" />
                  <Point X="4.127056058581" Y="0.537927843573" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.436282482778" Y="4.778922438497" />
                  <Point X="2.195857814142" Y="2.824716054699" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.818489609559" Y="2.133213390579" />
                  <Point X="3.170766618351" Y="1.741970135928" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.840991336526" Y="0.997610176175" />
                  <Point X="4.230044029319" Y="0.565523386631" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.577427263816" Y="4.764140550511" />
                  <Point X="2.245802262979" Y="2.911222397012" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.902600037716" Y="2.181774568677" />
                  <Point X="3.246276133714" Y="1.800083595414" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.95527875616" Y="1.012656409879" />
                  <Point X="4.333032000057" Y="0.59311892969" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.718572044853" Y="4.749358662525" />
                  <Point X="2.295746711816" Y="2.997728739324" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.986710465873" Y="2.230335746775" />
                  <Point X="3.321877724953" Y="1.85809479428" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.069566175794" Y="1.027702643582" />
                  <Point X="4.436019970795" Y="0.620714472748" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.864723768352" Y="4.729016001581" />
                  <Point X="2.345691160653" Y="3.084235081637" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.07082089403" Y="2.278896924873" />
                  <Point X="3.397479292772" Y="1.916106019157" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.183853595429" Y="1.042748877286" />
                  <Point X="4.539007941533" Y="0.648310015807" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.028067112455" Y="4.689580111643" />
                  <Point X="2.39563560949" Y="3.170741423949" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.154931322188" Y="2.327458102971" />
                  <Point X="3.473080860591" Y="1.974117244034" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.298141015063" Y="1.05779511099" />
                  <Point X="4.641995912271" Y="0.675905558865" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.191410780229" Y="4.650143862232" />
                  <Point X="2.445580058327" Y="3.257247766262" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.239041750345" Y="2.37601928107" />
                  <Point X="3.54868242841" Y="2.03212846891" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.412428434697" Y="1.072841344694" />
                  <Point X="4.744983883009" Y="0.703501101924" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.354754448003" Y="4.610707612821" />
                  <Point X="2.495524507165" Y="3.343754108574" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.323152178502" Y="2.424580459168" />
                  <Point X="3.624283996229" Y="2.090139693787" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.526715854331" Y="1.087887578398" />
                  <Point X="4.767236568953" Y="0.820762262664" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.52855766329" Y="4.559654859043" />
                  <Point X="2.545468956002" Y="3.430260450886" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.407262606659" Y="2.473141637266" />
                  <Point X="3.699885564048" Y="2.148150918664" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.641003273965" Y="1.102933812102" />
                  <Point X="4.731107535009" Y="1.002862892147" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.71838838923" Y="4.490801751352" />
                  <Point X="2.595413404839" Y="3.516766793199" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.491373034816" Y="2.521702815364" />
                  <Point X="3.775487131867" Y="2.206162143541" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.916375404489" Y="4.412890166669" />
                  <Point X="2.645357853676" Y="3.603273135511" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.575483462974" Y="2.570263993462" />
                  <Point X="3.851088699685" Y="2.264173368417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.137195570196" Y="4.309619799345" />
                  <Point X="2.695302302513" Y="3.689779477824" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.659593891131" Y="2.61882517156" />
                  <Point X="3.926690267504" Y="2.322184593294" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.381429211337" Y="4.180346133189" />
                  <Point X="2.74524675135" Y="3.776285820136" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.743704319288" Y="2.667386349658" />
                  <Point X="4.002291835323" Y="2.380195818171" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.6570364742" Y="4.016228530113" />
                  <Point X="2.795191200187" Y="3.862792162448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.827814747445" Y="2.715947527756" />
                  <Point X="4.077893403142" Y="2.438207043048" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.911925175602" Y="2.764508705855" />
                  <Point X="4.079320923637" Y="2.578596893158" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="0.001625976562" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978485351562" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.519052062988" Y="-3.714433349609" />
                  <Point X="0.471539031982" Y="-3.537112304688" />
                  <Point X="0.464318664551" Y="-3.521544189453" />
                  <Point X="0.444898895264" Y="-3.493563964844" />
                  <Point X="0.300591033936" Y="-3.285643798828" />
                  <Point X="0.274335754395" Y="-3.266399902344" />
                  <Point X="0.244284729004" Y="-3.257072998047" />
                  <Point X="0.020976625443" Y="-3.187766601562" />
                  <Point X="-0.008664756775" Y="-3.187766601562" />
                  <Point X="-0.038715789795" Y="-3.197093261719" />
                  <Point X="-0.262023895264" Y="-3.266399902344" />
                  <Point X="-0.288279144287" Y="-3.285643798828" />
                  <Point X="-0.307698944092" Y="-3.313623779297" />
                  <Point X="-0.45200680542" Y="-3.521544189453" />
                  <Point X="-0.459227142334" Y="-3.537112304688" />
                  <Point X="-0.812295471191" Y="-4.854780761719" />
                  <Point X="-0.84774395752" Y="-4.987076660156" />
                  <Point X="-1.100246948242" Y="-4.938065429688" />
                  <Point X="-1.133859741211" Y="-4.929416992188" />
                  <Point X="-1.35158972168" Y="-4.873396972656" />
                  <Point X="-1.314869750977" Y="-4.594481933594" />
                  <Point X="-1.309150146484" Y="-4.551038574219" />
                  <Point X="-1.309683227539" Y="-4.5347578125" />
                  <Point X="-1.316862548828" Y="-4.498665527344" />
                  <Point X="-1.370210693359" Y="-4.230466308594" />
                  <Point X="-1.386282714844" Y="-4.20262890625" />
                  <Point X="-1.413949829102" Y="-4.178365234375" />
                  <Point X="-1.619543457031" Y="-3.998063964844" />
                  <Point X="-1.649240722656" Y="-3.985762939453" />
                  <Point X="-1.685961181641" Y="-3.983356201172" />
                  <Point X="-1.958829833984" Y="-3.965471435547" />
                  <Point X="-1.989878540039" Y="-3.973791015625" />
                  <Point X="-2.020476196289" Y="-3.994235595703" />
                  <Point X="-2.247844970703" Y="-4.146159179688" />
                  <Point X="-2.259734130859" Y="-4.157294433594" />
                  <Point X="-2.457095458984" Y="-4.414500488281" />
                  <Point X="-2.470752441406" Y="-4.406044433594" />
                  <Point X="-2.85583203125" Y="-4.167612304688" />
                  <Point X="-2.902380615234" Y="-4.131771484375" />
                  <Point X="-3.228581054688" Y="-3.880608154297" />
                  <Point X="-2.594331298828" Y="-2.782055419922" />
                  <Point X="-2.506033691406" Y="-2.629119628906" />
                  <Point X="-2.499762695312" Y="-2.597593261719" />
                  <Point X="-2.516037597656" Y="-2.566706542969" />
                  <Point X="-2.531328369141" Y="-2.551415771484" />
                  <Point X="-2.560157226562" Y="-2.537198730469" />
                  <Point X="-2.59168359375" Y="-2.543469726562" />
                  <Point X="-3.728146972656" Y="-3.199607177734" />
                  <Point X="-3.842959228516" Y="-3.265894042969" />
                  <Point X="-3.857468505859" Y="-3.24683203125" />
                  <Point X="-4.161703613281" Y="-2.847129150391" />
                  <Point X="-4.195073242188" Y="-2.791173828125" />
                  <Point X="-4.431020019531" Y="-2.395527099609" />
                  <Point X="-3.318279052734" Y="-1.541690795898" />
                  <Point X="-3.163786865234" Y="-1.423144897461" />
                  <Point X="-3.152535644531" Y="-1.411081542969" />
                  <Point X="-3.144908203125" Y="-1.392485351562" />
                  <Point X="-3.138117431641" Y="-1.366266357422" />
                  <Point X="-3.140326416016" Y="-1.334595581055" />
                  <Point X="-3.164300048828" Y="-1.308790405273" />
                  <Point X="-3.187641601562" Y="-1.295052734375" />
                  <Point X="-3.219529296875" Y="-1.288571044922" />
                  <Point X="-4.654206542969" Y="-1.477449951172" />
                  <Point X="-4.803283203125" Y="-1.497076293945" />
                  <Point X="-4.808404785156" Y="-1.477025634766" />
                  <Point X="-4.927393066406" Y="-1.011191589355" />
                  <Point X="-4.936221679688" Y="-0.949463134766" />
                  <Point X="-4.998396484375" Y="-0.514742126465" />
                  <Point X="-3.733453613281" Y="-0.17580178833" />
                  <Point X="-3.557463867188" Y="-0.128645431519" />
                  <Point X="-3.538604003906" Y="-0.119140441895" />
                  <Point X="-3.514142822266" Y="-0.102163047791" />
                  <Point X="-3.502324462891" Y="-0.090645385742" />
                  <Point X="-3.493801757812" Y="-0.072372436523" />
                  <Point X="-3.485647949219" Y="-0.046100891113" />
                  <Point X="-3.486745117188" Y="-0.012923716545" />
                  <Point X="-3.494898925781" Y="0.013347833633" />
                  <Point X="-3.502324462891" Y="0.028085302353" />
                  <Point X="-3.517434570312" Y="0.041887588501" />
                  <Point X="-3.541895751953" Y="0.05886498642" />
                  <Point X="-3.557463867188" Y="0.066085334778" />
                  <Point X="-4.865240234375" Y="0.416502868652" />
                  <Point X="-4.998186523438" Y="0.452125793457" />
                  <Point X="-4.994331542969" Y="0.478177215576" />
                  <Point X="-4.917645019531" Y="0.996418151855" />
                  <Point X="-4.899872070312" Y="1.062006103516" />
                  <Point X="-4.773516113281" Y="1.528298583984" />
                  <Point X="-3.879420410156" Y="1.410588623047" />
                  <Point X="-3.753266357422" Y="1.393980102539" />
                  <Point X="-3.731704833984" Y="1.395866577148" />
                  <Point X="-3.724419189453" Y="1.398163818359" />
                  <Point X="-3.670278808594" Y="1.415234130859" />
                  <Point X="-3.651534423828" Y="1.426056152344" />
                  <Point X="-3.639119873047" Y="1.443786132813" />
                  <Point X="-3.636196289062" Y="1.45084387207" />
                  <Point X="-3.614472412109" Y="1.503290283203" />
                  <Point X="-3.610714111328" Y="1.52460534668" />
                  <Point X="-3.616315917969" Y="1.54551171875" />
                  <Point X="-3.619843261719" Y="1.552287841797" />
                  <Point X="-3.646055664062" Y="1.602641479492" />
                  <Point X="-3.659968261719" Y="1.619221679688" />
                  <Point X="-4.410111816406" Y="2.194827148438" />
                  <Point X="-4.476105957031" Y="2.245466308594" />
                  <Point X="-4.457991210938" Y="2.276500976562" />
                  <Point X="-4.160014648438" Y="2.787006591797" />
                  <Point X="-4.112936035156" Y="2.847519042969" />
                  <Point X="-3.774671142578" Y="3.282310791016" />
                  <Point X="-3.236637207031" Y="2.971676513672" />
                  <Point X="-3.159156982422" Y="2.926943359375" />
                  <Point X="-3.138514892578" Y="2.920434814453" />
                  <Point X="-3.128367919922" Y="2.919547119141" />
                  <Point X="-3.052965576172" Y="2.912950195312" />
                  <Point X="-3.031506835938" Y="2.915775390625" />
                  <Point X="-3.013252441406" Y="2.927404541016" />
                  <Point X="-3.006050048828" Y="2.934606933594" />
                  <Point X="-2.952528808594" Y="2.988127929688" />
                  <Point X="-2.940899414062" Y="3.006382324219" />
                  <Point X="-2.93807421875" Y="3.027841308594" />
                  <Point X="-2.938961914062" Y="3.037988525391" />
                  <Point X="-2.945558837891" Y="3.113390869141" />
                  <Point X="-2.952067382812" Y="3.134032958984" />
                  <Point X="-3.284478759766" Y="3.709786132812" />
                  <Point X="-3.307278564453" Y="3.749276611328" />
                  <Point X="-3.271857421875" Y="3.776433837891" />
                  <Point X="-2.752876220703" Y="4.174331542969" />
                  <Point X="-2.678729492188" Y="4.215525878906" />
                  <Point X="-2.141548339844" Y="4.513972167969" />
                  <Point X="-1.991545898438" Y="4.318484375" />
                  <Point X="-1.967826660156" Y="4.287573242188" />
                  <Point X="-1.951246948242" Y="4.273660644531" />
                  <Point X="-1.939953369141" Y="4.267781738281" />
                  <Point X="-1.856030761719" Y="4.22409375" />
                  <Point X="-1.835124145508" Y="4.2184921875" />
                  <Point X="-1.813808837891" Y="4.222250488281" />
                  <Point X="-1.802045776367" Y="4.227123046875" />
                  <Point X="-1.714634887695" Y="4.263330078125" />
                  <Point X="-1.696905273438" Y="4.275744628906" />
                  <Point X="-1.686083496094" Y="4.294488769531" />
                  <Point X="-1.682254882813" Y="4.306631835938" />
                  <Point X="-1.653804077148" Y="4.396865234375" />
                  <Point X="-1.651917602539" Y="4.418426757812" />
                  <Point X="-1.689137695313" Y="4.701141113281" />
                  <Point X="-1.639947509766" Y="4.714932128906" />
                  <Point X="-0.968094238281" Y="4.903296386719" />
                  <Point X="-0.878208557129" Y="4.913815917969" />
                  <Point X="-0.224199615479" Y="4.990358398438" />
                  <Point X="-0.06506111145" Y="4.396444824219" />
                  <Point X="-0.042140380859" Y="4.310903320312" />
                  <Point X="-0.024282114029" Y="4.284176757813" />
                  <Point X="0.006155934334" Y="4.273844238281" />
                  <Point X="0.036594036102" Y="4.284176757813" />
                  <Point X="0.054452251434" Y="4.310903320312" />
                  <Point X="0.224776107788" Y="4.946560546875" />
                  <Point X="0.236648422241" Y="4.990868652344" />
                  <Point X="0.273580200195" Y="4.987000976562" />
                  <Point X="0.860205810547" Y="4.925565429688" />
                  <Point X="0.934572509766" Y="4.907611328125" />
                  <Point X="1.508455932617" Y="4.769058105469" />
                  <Point X="1.556526367188" Y="4.751622558594" />
                  <Point X="1.931033569336" Y="4.615786132813" />
                  <Point X="1.977841308594" Y="4.593895507812" />
                  <Point X="2.33869921875" Y="4.425134277344" />
                  <Point X="2.383924316406" Y="4.398786132812" />
                  <Point X="2.732523681641" Y="4.19569140625" />
                  <Point X="2.775170898438" Y="4.16536328125" />
                  <Point X="3.068740234375" Y="3.956592773438" />
                  <Point X="2.332051269531" Y="2.680609863281" />
                  <Point X="2.229853515625" Y="2.503597900391" />
                  <Point X="2.222305419922" Y="2.481991943359" />
                  <Point X="2.203382324219" Y="2.411228271484" />
                  <Point X="2.203037597656" Y="2.384091552734" />
                  <Point X="2.210416015625" Y="2.322901123047" />
                  <Point X="2.218682128906" Y="2.3008125" />
                  <Point X="2.22377734375" Y="2.293303466797" />
                  <Point X="2.261639892578" Y="2.23750390625" />
                  <Point X="2.282448974609" Y="2.219108642578" />
                  <Point X="2.338248535156" Y="2.181246337891" />
                  <Point X="2.360336914062" Y="2.172980224609" />
                  <Point X="2.368571533203" Y="2.171987060547" />
                  <Point X="2.42976171875" Y="2.164608642578" />
                  <Point X="2.458187255859" Y="2.168492919922" />
                  <Point X="2.528950683594" Y="2.187416015625" />
                  <Point X="2.541033935547" Y="2.192417480469" />
                  <Point X="3.856405273438" Y="2.951847412109" />
                  <Point X="3.994248046875" Y="3.031430908203" />
                  <Point X="3.995814453125" Y="3.02925390625" />
                  <Point X="4.202591796875" Y="2.741880126953" />
                  <Point X="4.2263671875" Y="2.702591064453" />
                  <Point X="4.387512695312" Y="2.436296142578" />
                  <Point X="3.423169189453" Y="1.696328857422" />
                  <Point X="3.288616210938" Y="1.593082641602" />
                  <Point X="3.272517822266" Y="1.574892211914" />
                  <Point X="3.221589111328" Y="1.508451904297" />
                  <Point X="3.210566650391" Y="1.48237109375" />
                  <Point X="3.191595703125" Y="1.414535522461" />
                  <Point X="3.190779541016" Y="1.390965332031" />
                  <Point X="3.192875244141" Y="1.38080847168" />
                  <Point X="3.208448486328" Y="1.305332519531" />
                  <Point X="3.221346679688" Y="1.279291137695" />
                  <Point X="3.263704101562" Y="1.214909667969" />
                  <Point X="3.280947998047" Y="1.198819946289" />
                  <Point X="3.289208251953" Y="1.194170288086" />
                  <Point X="3.350590087891" Y="1.159617675781" />
                  <Point X="3.379733642578" Y="1.152143554688" />
                  <Point X="3.462726074219" Y="1.141175048828" />
                  <Point X="3.475803710938" Y="1.14117175293" />
                  <Point X="4.725318359375" Y="1.305673583984" />
                  <Point X="4.848975585938" Y="1.32195324707" />
                  <Point X="4.85037890625" Y="1.316187988281" />
                  <Point X="4.939188476562" Y="0.951386108398" />
                  <Point X="4.946680664062" Y="0.903263183594" />
                  <Point X="4.997858398438" Y="0.574556152344" />
                  <Point X="3.895368408203" Y="0.279144897461" />
                  <Point X="3.741167724609" Y="0.237826919556" />
                  <Point X="3.718114257812" Y="0.226476913452" />
                  <Point X="3.636576904297" Y="0.179346878052" />
                  <Point X="3.615681396484" Y="0.158537521362" />
                  <Point X="3.566759033203" Y="0.096198890686" />
                  <Point X="3.556985107422" Y="0.074735275269" />
                  <Point X="3.554790527344" Y="0.06327620697" />
                  <Point X="3.538482910156" Y="-0.021875303268" />
                  <Point X="3.540677490234" Y="-0.05214371109" />
                  <Point X="3.556985107422" Y="-0.13729536438" />
                  <Point X="3.566759033203" Y="-0.158758987427" />
                  <Point X="3.573342529297" Y="-0.167148040771" />
                  <Point X="3.622264892578" Y="-0.229486679077" />
                  <Point X="3.647549560547" Y="-0.248249450684" />
                  <Point X="3.729086914062" Y="-0.295379486084" />
                  <Point X="3.741167724609" Y="-0.300387023926" />
                  <Point X="4.887029296875" Y="-0.607419616699" />
                  <Point X="4.998067871094" Y="-0.637172363281" />
                  <Point X="4.998019042969" Y="-0.637497131348" />
                  <Point X="4.948431640625" Y="-0.966399291992" />
                  <Point X="4.938833007812" Y="-1.008463439941" />
                  <Point X="4.874545898438" Y="-1.290178344727" />
                  <Point X="3.590811767578" Y="-1.121171630859" />
                  <Point X="3.411982177734" Y="-1.097628295898" />
                  <Point X="3.3948359375" Y="-1.098341186523" />
                  <Point X="3.373300537109" Y="-1.103021972656" />
                  <Point X="3.213271728516" Y="-1.137804931641" />
                  <Point X="3.1854453125" Y="-1.154697265625" />
                  <Point X="3.172428466797" Y="-1.170352416992" />
                  <Point X="3.075701171875" Y="-1.286685424805" />
                  <Point X="3.064357910156" Y="-1.314070556641" />
                  <Point X="3.0624921875" Y="-1.334344726563" />
                  <Point X="3.04862890625" Y="-1.485000854492" />
                  <Point X="3.056360595703" Y="-1.516621826172" />
                  <Point X="3.068278564453" Y="-1.535159545898" />
                  <Point X="3.156840820312" Y="-1.672912353516" />
                  <Point X="3.168460693359" Y="-1.685540649414" />
                  <Point X="4.231827148438" Y="-2.501490722656" />
                  <Point X="4.33907421875" Y="-2.583784179688" />
                  <Point X="4.204133300781" Y="-2.802139160156" />
                  <Point X="4.184279296875" Y="-2.830348876953" />
                  <Point X="4.056688476562" Y="-3.011638427734" />
                  <Point X="2.912726318359" Y="-2.351171386719" />
                  <Point X="2.753454589844" Y="-2.259215576172" />
                  <Point X="2.737340820313" Y="-2.253312744141" />
                  <Point X="2.711710205078" Y="-2.248684082031" />
                  <Point X="2.521250488281" Y="-2.214287109375" />
                  <Point X="2.489077636719" Y="-2.219244873047" />
                  <Point X="2.467784912109" Y="-2.230450927734" />
                  <Point X="2.309559570312" Y="-2.313723876953" />
                  <Point X="2.288599853516" Y="-2.334683837891" />
                  <Point X="2.277393554688" Y="-2.3559765625" />
                  <Point X="2.194120849609" Y="-2.514201904297" />
                  <Point X="2.189163085938" Y="-2.546374511719" />
                  <Point X="2.193791748047" Y="-2.572004882812" />
                  <Point X="2.228188720703" Y="-2.76246484375" />
                  <Point X="2.234091552734" Y="-2.778578613281" />
                  <Point X="2.917411621094" Y="-3.962123291016" />
                  <Point X="2.986673583984" Y="-4.082088623047" />
                  <Point X="2.835296875" Y="-4.190213378906" />
                  <Point X="2.813104492188" Y="-4.204578125" />
                  <Point X="2.679775878906" Y="-4.290879394531" />
                  <Point X="1.804716552734" Y="-3.150479980469" />
                  <Point X="1.683177490234" Y="-2.992087402344" />
                  <Point X="1.670549316406" Y="-2.980467529297" />
                  <Point X="1.645270629883" Y="-2.964215576172" />
                  <Point X="1.45742590332" Y="-2.843448730469" />
                  <Point X="1.425804931641" Y="-2.835717041016" />
                  <Point X="1.398158325195" Y="-2.838261230469" />
                  <Point X="1.192718017578" Y="-2.857166015625" />
                  <Point X="1.165332885742" Y="-2.868509277344" />
                  <Point X="1.143984985352" Y="-2.886259277344" />
                  <Point X="0.985349060059" Y="-3.018159667969" />
                  <Point X="0.968456726074" Y="-3.045986328125" />
                  <Point X="0.962073913574" Y="-3.075352783203" />
                  <Point X="0.91464251709" Y="-3.29357421875" />
                  <Point X="0.91392956543" Y="-3.310720214844" />
                  <Point X="1.107577148438" Y="-4.781620117188" />
                  <Point X="1.127642211914" Y="-4.934028320312" />
                  <Point X="0.994345214844" Y="-4.963247070312" />
                  <Point X="0.973838928223" Y="-4.96697265625" />
                  <Point X="0.860200378418" Y="-4.987616699219" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#128" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.022920783437" Y="4.440666006273" Z="0.3" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.3" />
                  <Point X="-0.890176654647" Y="4.994757309884" Z="0.3" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.3" />
                  <Point X="-1.659601239299" Y="4.794355896753" Z="0.3" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.3" />
                  <Point X="-1.74543731072" Y="4.73023516653" Z="0.3" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.3" />
                  <Point X="-1.736096496222" Y="4.352946996444" Z="0.3" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.3" />
                  <Point X="-1.827336009275" Y="4.304597030641" Z="0.3" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.3" />
                  <Point X="-1.923021593809" Y="4.343412201105" Z="0.3" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.3" />
                  <Point X="-1.958034232034" Y="4.380202577259" Z="0.3" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.3" />
                  <Point X="-2.709168484006" Y="4.290513274447" Z="0.3" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.3" />
                  <Point X="-3.308435598382" Y="3.84739381736" Z="0.3" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.3" />
                  <Point X="-3.333936054119" Y="3.716066253979" Z="0.3" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.3" />
                  <Point X="-2.994927771378" Y="3.064910584831" Z="0.3" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.3" />
                  <Point X="-3.04756112162" Y="3.001242446267" Z="0.3" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.3" />
                  <Point X="-3.130165880776" Y="3.000636927795" Z="0.3" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.3" />
                  <Point X="-3.217793094318" Y="3.046257894626" Z="0.3" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.3" />
                  <Point X="-4.158555515522" Y="2.909501502997" Z="0.3" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.3" />
                  <Point X="-4.507329198" Y="2.332986107535" Z="0.3" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.3" />
                  <Point X="-4.446705943642" Y="2.186439527258" Z="0.3" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.3" />
                  <Point X="-3.670349884955" Y="1.560480553132" Z="0.3" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.3" />
                  <Point X="-3.688546464969" Y="1.501257926128" Z="0.3" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.3" />
                  <Point X="-3.745610298701" Y="1.477129727547" Z="0.3" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.3" />
                  <Point X="-3.879049898843" Y="1.491441006393" Z="0.3" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.3" />
                  <Point X="-4.95428785543" Y="1.106363588244" Z="0.3" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.3" />
                  <Point X="-5.049949098396" Y="0.516776252241" Z="0.3" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.3" />
                  <Point X="-4.884337177039" Y="0.399486678799" Z="0.3" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.3" />
                  <Point X="-3.552099634682" Y="0.032091631074" Z="0.3" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.3" />
                  <Point X="-3.540654018987" Y="0.003535409559" Z="0.3" />
                  <Point X="-3.539556741714" Y="0" Z="0.3" />
                  <Point X="-3.547710582148" Y="-0.026271541502" Z="0.3" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.3" />
                  <Point X="-3.573268960442" Y="-0.04678435206" Z="0.3" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.3" />
                  <Point X="-3.752550611733" Y="-0.096225377957" Z="0.3" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.3" />
                  <Point X="-4.991873627245" Y="-0.925262157864" Z="0.3" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.3" />
                  <Point X="-4.863005660257" Y="-1.458119841728" Z="0.3" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.3" />
                  <Point X="-4.653836169148" Y="-1.495742119034" Z="0.3" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.3" />
                  <Point X="-3.195817411136" Y="-1.320601085424" Z="0.3" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.3" />
                  <Point X="-3.199466970005" Y="-1.348668827989" Z="0.3" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.3" />
                  <Point X="-3.354872985421" Y="-1.470743186905" Z="0.3" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.3" />
                  <Point X="-4.244172405535" Y="-2.785503410855" Z="0.3" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.3" />
                  <Point X="-3.903410394573" Y="-3.24583515394" Z="0.3" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.3" />
                  <Point X="-3.709302915994" Y="-3.211628421502" Z="0.3" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.3" />
                  <Point X="-2.557548683636" Y="-2.570781785496" Z="0.3" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.3" />
                  <Point X="-2.643788591666" Y="-2.725775466498" Z="0.3" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.3" />
                  <Point X="-2.93904053648" Y="-4.14010870366" Z="0.3" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.3" />
                  <Point X="-2.503230082172" Y="-4.417274909183" Z="0.3" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.3" />
                  <Point X="-2.424442847897" Y="-4.414778166478" Z="0.3" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.3" />
                  <Point X="-1.998853438567" Y="-4.004528954861" Z="0.3" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.3" />
                  <Point X="-1.69538729161" Y="-4.001969147028" Z="0.3" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.3" />
                  <Point X="-1.45307310646" Y="-4.184677053485" Z="0.3" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.3" />
                  <Point X="-1.372057709795" Y="-4.477140117199" Z="0.3" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.3" />
                  <Point X="-1.370597982061" Y="-4.556675723872" Z="0.3" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.3" />
                  <Point X="-1.152474555018" Y="-4.946559667257" Z="0.3" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.3" />
                  <Point X="-0.853219572698" Y="-5.006862280966" Z="0.3" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.3" />
                  <Point X="-0.77015503697" Y="-4.836441846383" Z="0.3" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.3" />
                  <Point X="-0.272778860908" Y="-3.310852879682" Z="0.3" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.3" />
                  <Point X="-0.030050980702" Y="-3.213566083864" Z="0.3" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.3" />
                  <Point X="0.22330809866" Y="-3.273545961424" Z="0.3" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.3" />
                  <Point X="0.397666998524" Y="-3.490792876086" Z="0.3" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.3" />
                  <Point X="0.464599823431" Y="-3.696094183433" Z="0.3" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.3" />
                  <Point X="0.976619741722" Y="-4.98488723517" Z="0.3" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.3" />
                  <Point X="1.155816663665" Y="-4.946410281962" Z="0.3" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.3" />
                  <Point X="1.150993452488" Y="-4.743813595399" Z="0.3" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.3" />
                  <Point X="1.004777097153" Y="-3.054692447311" Z="0.3" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.3" />
                  <Point X="1.169795837491" Y="-2.893425530001" Z="0.3" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.3" />
                  <Point X="1.396583986774" Y="-2.856770642481" Z="0.3" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.3" />
                  <Point X="1.612075275066" Y="-2.974993451463" Z="0.3" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.3" />
                  <Point X="1.758892950868" Y="-3.149638016126" Z="0.3" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.3" />
                  <Point X="2.834117668655" Y="-4.215272378526" Z="0.3" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.3" />
                  <Point X="3.024066917953" Y="-4.081147405703" Z="0.3" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.3" />
                  <Point X="2.954556967612" Y="-3.905843207782" Z="0.3" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.3" />
                  <Point X="2.23683997045" Y="-2.531838719987" Z="0.3" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.3" />
                  <Point X="2.31548401035" Y="-2.347982867844" Z="0.3" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.3" />
                  <Point X="2.484915510528" Y="-2.243417299617" Z="0.3" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.3" />
                  <Point X="2.696668083526" Y="-2.266608039041" Z="0.3" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.3" />
                  <Point X="2.881570389325" Y="-2.363192532662" Z="0.3" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.3" />
                  <Point X="4.219012041221" Y="-2.827846070676" Z="0.3" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.3" />
                  <Point X="4.380291838421" Y="-2.57095694341" Z="0.3" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.3" />
                  <Point X="4.25610935937" Y="-2.430542978564" Z="0.3" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.3" />
                  <Point X="3.104180500925" Y="-1.476840154961" Z="0.3" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.3" />
                  <Point X="3.106125842423" Y="-1.307646311059" Z="0.3" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.3" />
                  <Point X="3.204718758452" Y="-1.171039316781" Z="0.3" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.3" />
                  <Point X="3.377764471483" Y="-1.120601230245" Z="0.3" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.3" />
                  <Point X="3.578129331931" Y="-1.139463769306" Z="0.3" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.3" />
                  <Point X="4.981423930931" Y="-0.988307502563" Z="0.3" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.3" />
                  <Point X="5.041304538375" Y="-0.613671150005" Z="0.3" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.3" />
                  <Point X="4.893814404841" Y="-0.52784334132" Z="0.3" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.3" />
                  <Point X="3.666415346025" Y="-0.173680584955" Z="0.3" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.3" />
                  <Point X="3.60652030251" Y="-0.104999557712" Z="0.3" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.3" />
                  <Point X="3.583629252982" Y="-0.011458998134" Z="0.3" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.3" />
                  <Point X="3.597742197442" Y="0.085151533107" Z="0.3" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.3" />
                  <Point X="3.648859135891" Y="0.158949180863" Z="0.3" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.3" />
                  <Point X="3.736980068327" Y="0.214468259287" Z="0.3" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.3" />
                  <Point X="3.902153412466" Y="0.262128591449" Z="0.3" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.3" />
                  <Point X="4.989929564077" Y="0.942235324539" Z="0.3" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.3" />
                  <Point X="4.892803934557" Y="1.359294895286" Z="0.3" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.3" />
                  <Point X="4.712635994749" Y="1.386525849076" Z="0.3" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.3" />
                  <Point X="3.380129053303" Y="1.232992534642" Z="0.3" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.3" />
                  <Point X="3.307578814153" Y="1.269021218871" Z="0.3" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.3" />
                  <Point X="3.256961114038" Y="1.33805247489" Z="0.3" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.3" />
                  <Point X="3.235687753231" Y="1.422192278104" Z="0.3" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.3" />
                  <Point X="3.252563024744" Y="1.50018492521" Z="0.3" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.3" />
                  <Point X="3.306044601717" Y="1.575754095203" Z="0.3" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.3" />
                  <Point X="3.447451240243" Y="1.687941307763" Z="0.3" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.3" />
                  <Point X="4.262988685564" Y="2.759757349562" Z="0.3" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.3" />
                  <Point X="4.030244022852" Y="3.089736756195" Z="0.3" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.3" />
                  <Point X="3.825249230333" Y="3.026428704552" Z="0.3" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.3" />
                  <Point X="2.439114428662" Y="2.248075688346" Z="0.3" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.3" />
                  <Point X="2.368401288174" Y="2.252907599765" Z="0.3" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.3" />
                  <Point X="2.304367134131" Y="2.291762867725" Z="0.3" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.3" />
                  <Point X="2.25899573251" Y="2.352657726251" Z="0.3" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.3" />
                  <Point X="2.246522102994" Y="2.421357155659" Z="0.3" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.3" />
                  <Point X="2.264452254312" Y="2.50035514722" Z="0.3" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.3" />
                  <Point X="2.369196648308" Y="2.686889851622" Z="0.3" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.3" />
                  <Point X="2.797991948819" Y="4.23739024871" Z="0.3" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.3" />
                  <Point X="2.402938177215" Y="4.473268872164" Z="0.3" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.3" />
                  <Point X="1.992866807573" Y="4.670467683775" Z="0.3" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.3" />
                  <Point X="1.567418982799" Y="4.829906493772" Z="0.3" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.3" />
                  <Point X="0.940150979976" Y="4.98749479282" Z="0.3" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.3" />
                  <Point X="0.272632066406" Y="5.068009040363" Z="0.3" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.3" />
                  <Point X="0.170323778116" Y="4.990781499678" Z="0.3" />
                  <Point X="0" Y="4.355124473572" Z="0.3" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>