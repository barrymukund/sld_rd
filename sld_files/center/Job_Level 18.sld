<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#151" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1409" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="0" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="0.004716064453" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.697555053711" Y="-4.013563720703" />
                  <Point X="0.563302062988" Y="-3.512524414063" />
                  <Point X="0.557721191406" Y="-3.497141845703" />
                  <Point X="0.542363037109" Y="-3.467377197266" />
                  <Point X="0.487490509033" Y="-3.388316162109" />
                  <Point X="0.378635528564" Y="-3.231476806641" />
                  <Point X="0.356751586914" Y="-3.209021484375" />
                  <Point X="0.330496398926" Y="-3.189777587891" />
                  <Point X="0.302495330811" Y="-3.175669433594" />
                  <Point X="0.2175831604" Y="-3.149315673828" />
                  <Point X="0.049136188507" Y="-3.097036132812" />
                  <Point X="0.020976638794" Y="-3.092766601562" />
                  <Point X="-0.00866468811" Y="-3.092766601562" />
                  <Point X="-0.036824085236" Y="-3.097035888672" />
                  <Point X="-0.121736251831" Y="-3.123389404297" />
                  <Point X="-0.290183227539" Y="-3.175669189453" />
                  <Point X="-0.31818460083" Y="-3.189777587891" />
                  <Point X="-0.344439788818" Y="-3.209021484375" />
                  <Point X="-0.366323425293" Y="-3.2314765625" />
                  <Point X="-0.421196105957" Y="-3.310537353516" />
                  <Point X="-0.530051086426" Y="-3.467376953125" />
                  <Point X="-0.538189025879" Y="-3.481573730469" />
                  <Point X="-0.550990112305" Y="-3.512524414063" />
                  <Point X="-0.817318237305" Y="-4.506474609375" />
                  <Point X="-0.916584594727" Y="-4.876941894531" />
                  <Point X="-1.079341308594" Y="-4.845350097656" />
                  <Point X="-1.174216796875" Y="-4.820939453125" />
                  <Point X="-1.24641796875" Y="-4.802362792969" />
                  <Point X="-1.231123901367" Y="-4.686192871094" />
                  <Point X="-1.214963012695" Y="-4.563438476562" />
                  <Point X="-1.214201171875" Y="-4.5479296875" />
                  <Point X="-1.216508666992" Y="-4.516224121094" />
                  <Point X="-1.236794067383" Y="-4.4142421875" />
                  <Point X="-1.277036132812" Y="-4.211932617188" />
                  <Point X="-1.287938354492" Y="-4.182966308594" />
                  <Point X="-1.304010375977" Y="-4.15512890625" />
                  <Point X="-1.323644775391" Y="-4.131204101562" />
                  <Point X="-1.401821166992" Y="-4.062645263672" />
                  <Point X="-1.556905517578" Y="-3.926639404297" />
                  <Point X="-1.583188476562" Y="-3.910295410156" />
                  <Point X="-1.612885742188" Y="-3.897994384766" />
                  <Point X="-1.64302722168" Y="-3.890966308594" />
                  <Point X="-1.746784667969" Y="-3.884165527344" />
                  <Point X="-1.952616333008" Y="-3.870674804688" />
                  <Point X="-1.98341796875" Y="-3.873708496094" />
                  <Point X="-2.014466674805" Y="-3.882028076172" />
                  <Point X="-2.042657714844" Y="-3.894801513672" />
                  <Point X="-2.129114013672" Y="-3.952569824219" />
                  <Point X="-2.300624023438" Y="-4.067169677734" />
                  <Point X="-2.312785644531" Y="-4.076821533203" />
                  <Point X="-2.335102539062" Y="-4.099461914062" />
                  <Point X="-2.480149169922" Y="-4.288490234375" />
                  <Point X="-2.515877929688" Y="-4.266367675781" />
                  <Point X="-2.801707763672" Y="-4.089388916016" />
                  <Point X="-2.933083984375" Y="-3.988233398438" />
                  <Point X="-3.104721923828" Y="-3.856077880859" />
                  <Point X="-2.673255371094" Y="-3.108756103516" />
                  <Point X="-2.423761230469" Y="-2.676619628906" />
                  <Point X="-2.412859130859" Y="-2.647653076172" />
                  <Point X="-2.406588134766" Y="-2.616126464844" />
                  <Point X="-2.405575439453" Y="-2.585192382812" />
                  <Point X="-2.414560302734" Y="-2.555574707031" />
                  <Point X="-2.428777587891" Y="-2.526745605469" />
                  <Point X="-2.446804931641" Y="-2.501588867188" />
                  <Point X="-2.464153564453" Y="-2.484240234375" />
                  <Point X="-2.489311767578" Y="-2.466212158203" />
                  <Point X="-2.518140625" Y="-2.451995605469" />
                  <Point X="-2.5477578125" Y="-2.443011474609" />
                  <Point X="-2.578691162109" Y="-2.444024169922" />
                  <Point X="-2.610217285156" Y="-2.450295166016" />
                  <Point X="-2.63918359375" Y="-2.461197265625" />
                  <Point X="-3.496446289062" Y="-2.956138183594" />
                  <Point X="-3.818024414062" Y="-3.141801269531" />
                  <Point X="-3.857037841797" Y="-3.090545410156" />
                  <Point X="-4.082860595703" Y="-2.793860595703" />
                  <Point X="-4.177043457031" Y="-2.635929931641" />
                  <Point X="-4.306142578125" Y="-2.419450439453" />
                  <Point X="-3.542488525391" Y="-1.833477539062" />
                  <Point X="-3.105954589844" Y="-1.498513549805" />
                  <Point X="-3.083062988281" Y="-1.475877929688" />
                  <Point X="-3.063475585938" Y="-1.444146972656" />
                  <Point X="-3.054179931641" Y="-1.41910949707" />
                  <Point X="-3.051274658203" Y="-1.409863769531" />
                  <Point X="-3.046152099609" Y="-1.39008605957" />
                  <Point X="-3.042037597656" Y="-1.358602905273" />
                  <Point X="-3.042736816406" Y="-1.335734741211" />
                  <Point X="-3.048883056641" Y="-1.313696777344" />
                  <Point X="-3.060887939453" Y="-1.284714111328" />
                  <Point X="-3.073942626953" Y="-1.262393188477" />
                  <Point X="-3.092532714844" Y="-1.244419555664" />
                  <Point X="-3.113910400391" Y="-1.228766357422" />
                  <Point X="-3.121848388672" Y="-1.22354284668" />
                  <Point X="-3.139455566406" Y="-1.213180175781" />
                  <Point X="-3.168718261719" Y="-1.201956420898" />
                  <Point X="-3.200605957031" Y="-1.195474853516" />
                  <Point X="-3.231929199219" Y="-1.194383789062" />
                  <Point X="-4.314142089844" Y="-1.336859863281" />
                  <Point X="-4.732102050781" Y="-1.391885375977" />
                  <Point X="-4.745756347656" Y="-1.338428466797" />
                  <Point X="-4.834077636719" Y="-0.992654418945" />
                  <Point X="-4.85899609375" Y="-0.818428100586" />
                  <Point X="-4.892424316406" Y="-0.584698242188" />
                  <Point X="-4.030153808594" Y="-0.353653625488" />
                  <Point X="-3.532875976562" Y="-0.220408340454" />
                  <Point X="-3.511687011719" Y="-0.211888961792" />
                  <Point X="-3.486817626953" Y="-0.19821295166" />
                  <Point X="-3.478427246094" Y="-0.193013671875" />
                  <Point X="-3.459975585938" Y="-0.180207107544" />
                  <Point X="-3.436020751953" Y="-0.158680801392" />
                  <Point X="-3.414914550781" Y="-0.127854293823" />
                  <Point X="-3.404388671875" Y="-0.103127182007" />
                  <Point X="-3.401068115234" Y="-0.094078048706" />
                  <Point X="-3.394917480469" Y="-0.074260902405" />
                  <Point X="-3.389474365234" Y="-0.045520599365" />
                  <Point X="-3.390533447266" Y="-0.011264388084" />
                  <Point X="-3.395880859375" Y="0.013546210289" />
                  <Point X="-3.398017822266" Y="0.021690372467" />
                  <Point X="-3.404168457031" Y="0.041507671356" />
                  <Point X="-3.417485107422" Y="0.070832061768" />
                  <Point X="-3.440755126953" Y="0.100433166504" />
                  <Point X="-3.461874755859" Y="0.118406364441" />
                  <Point X="-3.469276855469" Y="0.124102752686" />
                  <Point X="-3.487728515625" Y="0.136909317017" />
                  <Point X="-3.501925292969" Y="0.145047103882" />
                  <Point X="-3.532875976562" Y="0.157848342896" />
                  <Point X="-4.519364257812" Y="0.4221769104" />
                  <Point X="-4.89181640625" Y="0.521975219727" />
                  <Point X="-4.881409179688" Y="0.592306640625" />
                  <Point X="-4.824488769531" Y="0.976970214844" />
                  <Point X="-4.774326660156" Y="1.162083618164" />
                  <Point X="-4.70355078125" Y="1.423267822266" />
                  <Point X="-4.122127441406" Y="1.346721801758" />
                  <Point X="-3.765666259766" Y="1.29979284668" />
                  <Point X="-3.744985595703" Y="1.299341674805" />
                  <Point X="-3.723424072266" Y="1.301228271484" />
                  <Point X="-3.703137695312" Y="1.305263671875" />
                  <Point X="-3.682551025391" Y="1.311754516602" />
                  <Point X="-3.641711669922" Y="1.324631225586" />
                  <Point X="-3.622779296875" Y="1.332961547852" />
                  <Point X="-3.604034912109" Y="1.343783447266" />
                  <Point X="-3.587353759766" Y="1.356014526367" />
                  <Point X="-3.573715087891" Y="1.37156628418" />
                  <Point X="-3.561300537109" Y="1.389296020508" />
                  <Point X="-3.551351318359" Y="1.407431030273" />
                  <Point X="-3.543090820312" Y="1.427373657227" />
                  <Point X="-3.526703857422" Y="1.466935302734" />
                  <Point X="-3.520915527344" Y="1.486794189453" />
                  <Point X="-3.517157226562" Y="1.508109375" />
                  <Point X="-3.5158046875" Y="1.528749389648" />
                  <Point X="-3.518951171875" Y="1.549192871094" />
                  <Point X="-3.524552978516" Y="1.570099365234" />
                  <Point X="-3.532049560547" Y="1.589377075195" />
                  <Point X="-3.542016601562" Y="1.608524047852" />
                  <Point X="-3.561789306641" Y="1.646506835938" />
                  <Point X="-3.573281738281" Y="1.663706787109" />
                  <Point X="-3.587194335938" Y="1.680286987305" />
                  <Point X="-3.602135986328" Y="1.694590332031" />
                  <Point X="-4.167987792969" Y="2.128783935547" />
                  <Point X="-4.351859863281" Y="2.269874023438" />
                  <Point X="-4.302327148438" Y="2.354735839844" />
                  <Point X="-4.081153808594" Y="2.733659423828" />
                  <Point X="-3.948281738281" Y="2.904447509766" />
                  <Point X="-3.750504882813" Y="3.158661865234" />
                  <Point X="-3.425585449219" Y="2.971069335938" />
                  <Point X="-3.206656982422" Y="2.844670898438" />
                  <Point X="-3.187724121094" Y="2.836340332031" />
                  <Point X="-3.167081787109" Y="2.829831787109" />
                  <Point X="-3.146794677734" Y="2.825796386719" />
                  <Point X="-3.118123046875" Y="2.823287841797" />
                  <Point X="-3.061245361328" Y="2.818311767578" />
                  <Point X="-3.040564941406" Y="2.818762939453" />
                  <Point X="-3.019106201172" Y="2.821588134766" />
                  <Point X="-2.999014160156" Y="2.826504882812" />
                  <Point X="-2.980462402344" Y="2.835653564453" />
                  <Point X="-2.962208251953" Y="2.847282958984" />
                  <Point X="-2.946077636719" Y="2.860229248047" />
                  <Point X="-2.925726318359" Y="2.880580322266" />
                  <Point X="-2.885354003906" Y="2.920952636719" />
                  <Point X="-2.872406982422" Y="2.937083984375" />
                  <Point X="-2.860777587891" Y="2.955338134766" />
                  <Point X="-2.85162890625" Y="2.973889892578" />
                  <Point X="-2.846712158203" Y="2.993981933594" />
                  <Point X="-2.843886962891" Y="3.015440673828" />
                  <Point X="-2.843435791016" Y="3.03612109375" />
                  <Point X="-2.845944335938" Y="3.064792724609" />
                  <Point X="-2.850920410156" Y="3.121670654297" />
                  <Point X="-2.854955810547" Y="3.141957763672" />
                  <Point X="-2.861464355469" Y="3.162600097656" />
                  <Point X="-2.869794921875" Y="3.181532958984" />
                  <Point X="-3.120541259766" Y="3.615837890625" />
                  <Point X="-3.183333007812" Y="3.724596191406" />
                  <Point X="-3.085836425781" Y="3.799345947266" />
                  <Point X="-2.700625244141" Y="4.094683837891" />
                  <Point X="-2.491358886719" Y="4.210947753906" />
                  <Point X="-2.167036865234" Y="4.391134277344" />
                  <Point X="-2.110216064453" Y="4.317083984375" />
                  <Point X="-2.04319543457" Y="4.229740722656" />
                  <Point X="-2.028892700195" Y="4.214799804688" />
                  <Point X="-2.012312866211" Y="4.200887207031" />
                  <Point X="-1.995112792969" Y="4.18939453125" />
                  <Point X="-1.963201538086" Y="4.172782714844" />
                  <Point X="-1.899896484375" Y="4.139827636719" />
                  <Point X="-1.88061730957" Y="4.132330566406" />
                  <Point X="-1.85971081543" Y="4.126729003906" />
                  <Point X="-1.839267822266" Y="4.123582519531" />
                  <Point X="-1.818628417969" Y="4.124935546875" />
                  <Point X="-1.797313110352" Y="4.128693847656" />
                  <Point X="-1.777452880859" Y="4.134482421875" />
                  <Point X="-1.744215087891" Y="4.148250488281" />
                  <Point X="-1.678278686523" Y="4.175562011719" />
                  <Point X="-1.660144775391" Y="4.185510742187" />
                  <Point X="-1.642415161133" Y="4.197925292969" />
                  <Point X="-1.626863769531" Y="4.211563476563" />
                  <Point X="-1.6146328125" Y="4.228244628906" />
                  <Point X="-1.603810913086" Y="4.246988769531" />
                  <Point X="-1.59548046875" Y="4.265921386719" />
                  <Point X="-1.584662109375" Y="4.300232421875" />
                  <Point X="-1.563201171875" Y="4.368297851562" />
                  <Point X="-1.559165649414" Y="4.388584960938" />
                  <Point X="-1.557279174805" Y="4.410146484375" />
                  <Point X="-1.55773034668" Y="4.430826660156" />
                  <Point X="-1.584201904297" Y="4.631897949219" />
                  <Point X="-1.448314575195" Y="4.66999609375" />
                  <Point X="-0.94963470459" Y="4.80980859375" />
                  <Point X="-0.695946777344" Y="4.839499023438" />
                  <Point X="-0.294710784912" Y="4.886458007812" />
                  <Point X="-0.198668334961" Y="4.528021972656" />
                  <Point X="-0.133903305054" Y="4.286315429688" />
                  <Point X="-0.121129844666" Y="4.258124023438" />
                  <Point X="-0.10327155304" Y="4.231397460938" />
                  <Point X="-0.082114532471" Y="4.20880859375" />
                  <Point X="-0.054819446564" Y="4.19421875" />
                  <Point X="-0.024381370544" Y="4.183886230469" />
                  <Point X="0.0061559062" Y="4.178844238281" />
                  <Point X="0.03669316864" Y="4.183886230469" />
                  <Point X="0.067131248474" Y="4.19421875" />
                  <Point X="0.094426467896" Y="4.20880859375" />
                  <Point X="0.115583503723" Y="4.231397460938" />
                  <Point X="0.133441802979" Y="4.258124023438" />
                  <Point X="0.146215255737" Y="4.286315429688" />
                  <Point X="0.274694763184" Y="4.765807617188" />
                  <Point X="0.307419372559" Y="4.8879375" />
                  <Point X="0.408615356445" Y="4.877339355469" />
                  <Point X="0.844041015625" Y="4.831738769531" />
                  <Point X="1.053930053711" Y="4.781064941406" />
                  <Point X="1.481027099609" Y="4.677950683594" />
                  <Point X="1.616658447266" Y="4.628756347656" />
                  <Point X="1.894645874023" Y="4.527928222656" />
                  <Point X="2.026751098633" Y="4.466146972656" />
                  <Point X="2.294576416016" Y="4.340893554688" />
                  <Point X="2.422224853516" Y="4.266525390625" />
                  <Point X="2.680978515625" Y="4.115774902344" />
                  <Point X="2.801343017578" Y="4.030178222656" />
                  <Point X="2.943260009766" Y="3.929254638672" />
                  <Point X="2.436351806641" Y="3.051263671875" />
                  <Point X="2.147581054688" Y="2.551097900391" />
                  <Point X="2.142075683594" Y="2.539930664062" />
                  <Point X="2.133076904297" Y="2.516056884766" />
                  <Point X="2.125881347656" Y="2.489149169922" />
                  <Point X="2.111607177734" Y="2.435770507813" />
                  <Point X="2.108619384766" Y="2.417934326172" />
                  <Point X="2.107728027344" Y="2.380952880859" />
                  <Point X="2.110533691406" Y="2.357685546875" />
                  <Point X="2.116099365234" Y="2.311528076172" />
                  <Point X="2.121442138672" Y="2.289604980469" />
                  <Point X="2.129708251953" Y="2.267516357422" />
                  <Point X="2.140071289062" Y="2.247470947266" />
                  <Point X="2.154468505859" Y="2.226253417969" />
                  <Point X="2.183029052734" Y="2.184162353516" />
                  <Point X="2.19446484375" Y="2.170328857422" />
                  <Point X="2.221598632812" Y="2.145592773438" />
                  <Point X="2.242816162109" Y="2.131195556641" />
                  <Point X="2.284907226562" Y="2.102635253906" />
                  <Point X="2.304952636719" Y="2.092271972656" />
                  <Point X="2.327040771484" Y="2.084006103516" />
                  <Point X="2.348963867188" Y="2.078663574219" />
                  <Point X="2.372231201172" Y="2.075857910156" />
                  <Point X="2.418388671875" Y="2.070291992188" />
                  <Point X="2.436467529297" Y="2.069845703125" />
                  <Point X="2.473206542969" Y="2.074171142578" />
                  <Point X="2.500114257812" Y="2.081366699219" />
                  <Point X="2.553492919922" Y="2.095640869141" />
                  <Point X="2.565283447266" Y="2.099638183594" />
                  <Point X="2.588533935547" Y="2.110145019531" />
                  <Point X="3.580751220703" Y="2.683001953125" />
                  <Point X="3.967326416016" Y="2.906191162109" />
                  <Point X="3.969787597656" Y="2.902770751953" />
                  <Point X="4.123270507813" Y="2.689464355469" />
                  <Point X="4.190374023438" Y="2.578575439453" />
                  <Point X="4.262198730469" Y="2.459884277344" />
                  <Point X="3.610977783203" Y="1.960184326172" />
                  <Point X="3.230783935547" Y="1.668451293945" />
                  <Point X="3.221424560547" Y="1.660241455078" />
                  <Point X="3.203973876953" Y="1.641627929688" />
                  <Point X="3.184608398438" Y="1.616364135742" />
                  <Point X="3.146191650391" Y="1.566246459961" />
                  <Point X="3.13660546875" Y="1.550911254883" />
                  <Point X="3.121629882812" Y="1.5170859375" />
                  <Point X="3.114416259766" Y="1.491291625977" />
                  <Point X="3.100105957031" Y="1.440121459961" />
                  <Point X="3.096652587891" Y="1.417823242188" />
                  <Point X="3.095836425781" Y="1.394252929688" />
                  <Point X="3.097739501953" Y="1.371768188477" />
                  <Point X="3.103661132812" Y="1.343068725586" />
                  <Point X="3.115408447266" Y="1.286135498047" />
                  <Point X="3.120679931641" Y="1.268977661133" />
                  <Point X="3.136282470703" Y="1.235740478516" />
                  <Point X="3.152388671875" Y="1.211259643555" />
                  <Point X="3.184340087891" Y="1.16269519043" />
                  <Point X="3.198893554688" Y="1.145450439453" />
                  <Point X="3.216137451172" Y="1.129360595703" />
                  <Point X="3.234346923828" Y="1.116034912109" />
                  <Point X="3.257687011719" Y="1.102896362305" />
                  <Point X="3.303989013672" Y="1.076832519531" />
                  <Point X="3.320521240234" Y="1.069501586914" />
                  <Point X="3.356117919922" Y="1.059438598633" />
                  <Point X="3.387675292969" Y="1.055267822266" />
                  <Point X="3.450278564453" Y="1.046994018555" />
                  <Point X="3.462702148438" Y="1.046175048828" />
                  <Point X="3.488203613281" Y="1.04698449707" />
                  <Point X="4.430743652344" Y="1.171072265625" />
                  <Point X="4.776839355469" Y="1.21663659668" />
                  <Point X="4.780015625" Y="1.20358996582" />
                  <Point X="4.845936523438" Y="0.932808898926" />
                  <Point X="4.867081542969" Y="0.796993591309" />
                  <Point X="4.890864746094" Y="0.644238464355" />
                  <Point X="4.152290039062" Y="0.446338012695" />
                  <Point X="3.716579833984" Y="0.329589935303" />
                  <Point X="3.704791259766" Y="0.325586608887" />
                  <Point X="3.681545654297" Y="0.315067993164" />
                  <Point X="3.650541259766" Y="0.297146850586" />
                  <Point X="3.589035644531" Y="0.261595428467" />
                  <Point X="3.574311035156" Y="0.251096221924" />
                  <Point X="3.547530761719" Y="0.225576461792" />
                  <Point X="3.528928222656" Y="0.201872268677" />
                  <Point X="3.492024902344" Y="0.154848815918" />
                  <Point X="3.48030078125" Y="0.13556854248" />
                  <Point X="3.470527099609" Y="0.114104927063" />
                  <Point X="3.463680908203" Y="0.092604270935" />
                  <Point X="3.457479980469" Y="0.06022555542" />
                  <Point X="3.445178710938" Y="-0.004006377697" />
                  <Point X="3.443483154297" Y="-0.021875295639" />
                  <Point X="3.445178710938" Y="-0.058553779602" />
                  <Point X="3.451379638672" Y="-0.090932342529" />
                  <Point X="3.463680908203" Y="-0.155164428711" />
                  <Point X="3.470527099609" Y="-0.176665084839" />
                  <Point X="3.48030078125" Y="-0.198128707886" />
                  <Point X="3.492024902344" Y="-0.217408813477" />
                  <Point X="3.510627441406" Y="-0.241113021851" />
                  <Point X="3.547530761719" Y="-0.288136627197" />
                  <Point X="3.559999023438" Y="-0.30123614502" />
                  <Point X="3.589035644531" Y="-0.324155578613" />
                  <Point X="3.620040039063" Y="-0.342076721191" />
                  <Point X="3.681545654297" Y="-0.377628143311" />
                  <Point X="3.692710205078" Y="-0.383139038086" />
                  <Point X="3.716579833984" Y="-0.392149932861" />
                  <Point X="4.580932128906" Y="-0.623752380371" />
                  <Point X="4.891472167969" Y="-0.706961486816" />
                  <Point X="4.855022460938" Y="-0.948726135254" />
                  <Point X="4.827931152344" Y="-1.067444213867" />
                  <Point X="4.801173828125" Y="-1.184698974609" />
                  <Point X="3.92968359375" Y="-1.069965209961" />
                  <Point X="3.424382080078" Y="-1.003441040039" />
                  <Point X="3.40803515625" Y="-1.002710327148" />
                  <Point X="3.374658447266" Y="-1.005508850098" />
                  <Point X="3.313807861328" Y="-1.018734985352" />
                  <Point X="3.193094238281" Y="-1.04497253418" />
                  <Point X="3.163973876953" Y="-1.056597167969" />
                  <Point X="3.136147460938" Y="-1.073489379883" />
                  <Point X="3.112397460938" Y="-1.093960083008" />
                  <Point X="3.0756171875" Y="-1.13819519043" />
                  <Point X="3.002653320312" Y="-1.225948364258" />
                  <Point X="2.987932617188" Y="-1.250330566406" />
                  <Point X="2.976589355469" Y="-1.277715698242" />
                  <Point X="2.969757568359" Y="-1.305365478516" />
                  <Point X="2.964486083984" Y="-1.362651977539" />
                  <Point X="2.954028564453" Y="-1.476295776367" />
                  <Point X="2.956347412109" Y="-1.507564819336" />
                  <Point X="2.964079101562" Y="-1.539185791016" />
                  <Point X="2.976450439453" Y="-1.567996582031" />
                  <Point X="3.010125976562" Y="-1.620376708984" />
                  <Point X="3.076930664062" Y="-1.724287109375" />
                  <Point X="3.086932373047" Y="-1.73723815918" />
                  <Point X="3.110628417969" Y="-1.760909179688" />
                  <Point X="3.912752441406" Y="-2.376400878906" />
                  <Point X="4.213122558594" Y="-2.606883300781" />
                  <Point X="4.124813476562" Y="-2.74978125" />
                  <Point X="4.068782714844" Y="-2.829392578125" />
                  <Point X="4.028981445312" Y="-2.885945068359" />
                  <Point X="3.250993652344" Y="-2.4367734375" />
                  <Point X="2.800954589844" Y="-2.176943115234" />
                  <Point X="2.786131591797" Y="-2.170012451172" />
                  <Point X="2.754224609375" Y="-2.159825195312" />
                  <Point X="2.681802734375" Y="-2.146745849609" />
                  <Point X="2.538134277344" Y="-2.120799560547" />
                  <Point X="2.506781982422" Y="-2.120395263672" />
                  <Point X="2.474609130859" Y="-2.125353027344" />
                  <Point X="2.444833251953" Y="-2.135176757812" />
                  <Point X="2.384668457031" Y="-2.166841064453" />
                  <Point X="2.265315185547" Y="-2.229655761719" />
                  <Point X="2.242384521484" Y="-2.246548828125" />
                  <Point X="2.221424804688" Y="-2.267508544922" />
                  <Point X="2.204531738281" Y="-2.290439208984" />
                  <Point X="2.172867431641" Y="-2.350604003906" />
                  <Point X="2.110052734375" Y="-2.469957275391" />
                  <Point X="2.100229003906" Y="-2.499733154297" />
                  <Point X="2.095271240234" Y="-2.531906005859" />
                  <Point X="2.095675292969" Y="-2.563258056641" />
                  <Point X="2.108754394531" Y="-2.6356796875" />
                  <Point X="2.134700927734" Y="-2.779348388672" />
                  <Point X="2.138985595703" Y="-2.795141845703" />
                  <Point X="2.151819091797" Y="-2.826078613281" />
                  <Point X="2.667264648438" Y="-3.718855957031" />
                  <Point X="2.861283203125" Y="-4.054906494141" />
                  <Point X="2.781849121094" Y="-4.11164453125" />
                  <Point X="2.719209472656" Y="-4.152189941406" />
                  <Point X="2.701765136719" Y="-4.163481445312" />
                  <Point X="2.101967529297" Y="-3.381810302734" />
                  <Point X="1.758546020508" Y="-2.934255126953" />
                  <Point X="1.747503540039" Y="-2.922179443359" />
                  <Point X="1.721924316406" Y="-2.900557617188" />
                  <Point X="1.650496948242" Y="-2.854636230469" />
                  <Point X="1.50880090332" Y="-2.763538818359" />
                  <Point X="1.479989746094" Y="-2.751167236328" />
                  <Point X="1.448368408203" Y="-2.743435546875" />
                  <Point X="1.417099487305" Y="-2.741116699219" />
                  <Point X="1.338981445312" Y="-2.748305175781" />
                  <Point X="1.184012573242" Y="-2.762565673828" />
                  <Point X="1.156362670898" Y="-2.769397460938" />
                  <Point X="1.128977661133" Y="-2.780740722656" />
                  <Point X="1.104596069336" Y="-2.795461181641" />
                  <Point X="1.044275268555" Y="-2.845615722656" />
                  <Point X="0.924612121582" Y="-2.945111572266" />
                  <Point X="0.904141052246" Y="-2.968861816406" />
                  <Point X="0.887248779297" Y="-2.996688476562" />
                  <Point X="0.875624206543" Y="-3.025808837891" />
                  <Point X="0.857588745117" Y="-3.108786865234" />
                  <Point X="0.821809936523" Y="-3.273396728516" />
                  <Point X="0.819724487305" Y="-3.289627441406" />
                  <Point X="0.819742248535" Y="-3.323120117188" />
                  <Point X="0.965815612793" Y="-4.432656738281" />
                  <Point X="1.022065307617" Y="-4.859915527344" />
                  <Point X="0.975686706543" Y="-4.870081542969" />
                  <Point X="0.929315551758" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058434936523" Y="-4.752635253906" />
                  <Point X="-1.141246337891" Y="-4.731328613281" />
                  <Point X="-1.136936645508" Y="-4.698592773438" />
                  <Point X="-1.120775634766" Y="-4.575838378906" />
                  <Point X="-1.120077514648" Y="-4.568099609375" />
                  <Point X="-1.119451782227" Y="-4.541033691406" />
                  <Point X="-1.121759277344" Y="-4.509328125" />
                  <Point X="-1.123334106445" Y="-4.497690429688" />
                  <Point X="-1.143619506836" Y="-4.395708496094" />
                  <Point X="-1.183861450195" Y="-4.193398925781" />
                  <Point X="-1.188125" Y="-4.17846875" />
                  <Point X="-1.19902734375" Y="-4.149502441406" />
                  <Point X="-1.205666015625" Y="-4.135466308594" />
                  <Point X="-1.221738037109" Y="-4.10762890625" />
                  <Point X="-1.23057409668" Y="-4.094861816406" />
                  <Point X="-1.250208496094" Y="-4.070937011719" />
                  <Point X="-1.261006958008" Y="-4.059779296875" />
                  <Point X="-1.339183349609" Y="-3.991220458984" />
                  <Point X="-1.494267700195" Y="-3.855214599609" />
                  <Point X="-1.506738647461" Y="-3.845965576172" />
                  <Point X="-1.533021606445" Y="-3.829621582031" />
                  <Point X="-1.546833618164" Y="-3.822526855469" />
                  <Point X="-1.576530761719" Y="-3.810225830078" />
                  <Point X="-1.591313476562" Y="-3.805476074219" />
                  <Point X="-1.621454833984" Y="-3.798447998047" />
                  <Point X="-1.636813720703" Y="-3.796169677734" />
                  <Point X="-1.740571289062" Y="-3.789368896484" />
                  <Point X="-1.946402832031" Y="-3.775878173828" />
                  <Point X="-1.961927978516" Y="-3.776132324219" />
                  <Point X="-1.992729614258" Y="-3.779166015625" />
                  <Point X="-2.008006103516" Y="-3.781945556641" />
                  <Point X="-2.03905480957" Y="-3.790265136719" />
                  <Point X="-2.053674560547" Y="-3.795496337891" />
                  <Point X="-2.081865478516" Y="-3.808269775391" />
                  <Point X="-2.095437011719" Y="-3.815812011719" />
                  <Point X="-2.181893310547" Y="-3.873580322266" />
                  <Point X="-2.353403320312" Y="-3.988180175781" />
                  <Point X="-2.359680664062" Y="-3.992756591797" />
                  <Point X="-2.380442382812" Y="-4.010131347656" />
                  <Point X="-2.402759277344" Y="-4.032771728516" />
                  <Point X="-2.410471191406" Y="-4.041629638672" />
                  <Point X="-2.503202636719" Y="-4.162479492188" />
                  <Point X="-2.747584228516" Y="-4.011164794922" />
                  <Point X="-2.875126708984" Y="-3.9129609375" />
                  <Point X="-2.980862792969" Y="-3.831547607422" />
                  <Point X="-2.590982910156" Y="-3.156256103516" />
                  <Point X="-2.341488769531" Y="-2.724119628906" />
                  <Point X="-2.334850097656" Y="-2.710083007812" />
                  <Point X="-2.323947998047" Y="-2.681116455078" />
                  <Point X="-2.319684570312" Y="-2.666186523438" />
                  <Point X="-2.313413574219" Y="-2.634659912109" />
                  <Point X="-2.311638916016" Y="-2.619234863281" />
                  <Point X="-2.310626220703" Y="-2.58830078125" />
                  <Point X="-2.314666503906" Y="-2.557614013672" />
                  <Point X="-2.323651367188" Y="-2.527996337891" />
                  <Point X="-2.329357910156" Y="-2.513556396484" />
                  <Point X="-2.343575195312" Y="-2.484727294922" />
                  <Point X="-2.351557617188" Y="-2.471409667969" />
                  <Point X="-2.369584960938" Y="-2.446252929688" />
                  <Point X="-2.379629882812" Y="-2.434413818359" />
                  <Point X="-2.396978515625" Y="-2.417065185547" />
                  <Point X="-2.408818359375" Y="-2.407019775391" />
                  <Point X="-2.4339765625" Y="-2.388991699219" />
                  <Point X="-2.447294921875" Y="-2.381009033203" />
                  <Point X="-2.476123779297" Y="-2.366792480469" />
                  <Point X="-2.490563964844" Y="-2.361086181641" />
                  <Point X="-2.520181152344" Y="-2.352102050781" />
                  <Point X="-2.550866210938" Y="-2.348062255859" />
                  <Point X="-2.581799560547" Y="-2.349074951172" />
                  <Point X="-2.597224853516" Y="-2.350849609375" />
                  <Point X="-2.628750976562" Y="-2.357120605469" />
                  <Point X="-2.643680908203" Y="-2.361384033203" />
                  <Point X="-2.672647216797" Y="-2.372286132812" />
                  <Point X="-2.68668359375" Y="-2.378924804688" />
                  <Point X="-3.543946289062" Y="-2.873865722656" />
                  <Point X="-3.793089111328" Y="-3.017708251953" />
                  <Point X="-4.004016845703" Y="-2.740592773438" />
                  <Point X="-4.095450683594" Y="-2.587271728516" />
                  <Point X="-4.181265136719" Y="-2.443373779297" />
                  <Point X="-3.48465625" Y="-1.908846069336" />
                  <Point X="-3.048122314453" Y="-1.573882080078" />
                  <Point X="-3.039158203125" Y="-1.566065307617" />
                  <Point X="-3.016266601562" Y="-1.5434296875" />
                  <Point X="-3.002224609375" Y="-1.525779174805" />
                  <Point X="-2.982637207031" Y="-1.494048339844" />
                  <Point X="-2.974415527344" Y="-1.477212280273" />
                  <Point X="-2.965119873047" Y="-1.452174804688" />
                  <Point X="-2.959309326172" Y="-1.433683349609" />
                  <Point X="-2.954186767578" Y="-1.413905639648" />
                  <Point X="-2.951953125" Y="-1.402396850586" />
                  <Point X="-2.947838623047" Y="-1.370913574219" />
                  <Point X="-2.94708203125" Y="-1.355699584961" />
                  <Point X="-2.94778125" Y="-1.332831420898" />
                  <Point X="-2.951229003906" Y="-1.310213745117" />
                  <Point X="-2.957375244141" Y="-1.28817590332" />
                  <Point X="-2.961114257812" Y="-1.277342163086" />
                  <Point X="-2.973119140625" Y="-1.24835949707" />
                  <Point X="-2.978883544922" Y="-1.236752807617" />
                  <Point X="-2.991938232422" Y="-1.214431884766" />
                  <Point X="-3.007909423828" Y="-1.194095214844" />
                  <Point X="-3.026499511719" Y="-1.176121582031" />
                  <Point X="-3.036408691406" Y="-1.167770507812" />
                  <Point X="-3.057786376953" Y="-1.152117431641" />
                  <Point X="-3.073662353516" Y="-1.141670288086" />
                  <Point X="-3.09126953125" Y="-1.131307617188" />
                  <Point X="-3.105434814453" Y="-1.124480712891" />
                  <Point X="-3.134697509766" Y="-1.113256958008" />
                  <Point X="-3.149795410156" Y="-1.108860107422" />
                  <Point X="-3.181683105469" Y="-1.102378540039" />
                  <Point X="-3.197298828125" Y="-1.100532470703" />
                  <Point X="-3.228622070312" Y="-1.09944140625" />
                  <Point X="-3.244329101562" Y="-1.100196533203" />
                  <Point X="-4.326541992188" Y="-1.242672607422" />
                  <Point X="-4.660920410156" Y="-1.286694458008" />
                  <Point X="-4.74076171875" Y="-0.974118774414" />
                  <Point X="-4.764953125" Y="-0.804977661133" />
                  <Point X="-4.786452636719" Y="-0.65465447998" />
                  <Point X="-4.005565917969" Y="-0.445416534424" />
                  <Point X="-3.508288085938" Y="-0.312171325684" />
                  <Point X="-3.497436767578" Y="-0.308550598145" />
                  <Point X="-3.476247802734" Y="-0.300031219482" />
                  <Point X="-3.46591015625" Y="-0.295132476807" />
                  <Point X="-3.441040771484" Y="-0.281456481934" />
                  <Point X="-3.424259765625" Y="-0.271057922363" />
                  <Point X="-3.405808105469" Y="-0.258251251221" />
                  <Point X="-3.396477783203" Y="-0.250868545532" />
                  <Point X="-3.372522949219" Y="-0.229342163086" />
                  <Point X="-3.357633544922" Y="-0.212350692749" />
                  <Point X="-3.33652734375" Y="-0.181524230957" />
                  <Point X="-3.327504638672" Y="-0.165063049316" />
                  <Point X="-3.316978759766" Y="-0.14033605957" />
                  <Point X="-3.310337646484" Y="-0.122237960815" />
                  <Point X="-3.304187011719" Y="-0.102420875549" />
                  <Point X="-3.301576660156" Y="-0.091938652039" />
                  <Point X="-3.296133544922" Y="-0.063198402405" />
                  <Point X="-3.294519775391" Y="-0.042584999084" />
                  <Point X="-3.295578857422" Y="-0.00832875824" />
                  <Point X="-3.297666015625" Y="0.008751282692" />
                  <Point X="-3.303013427734" Y="0.033561943054" />
                  <Point X="-3.307287353516" Y="0.049850120544" />
                  <Point X="-3.313437988281" Y="0.069667503357" />
                  <Point X="-3.317669677734" Y="0.080788063049" />
                  <Point X="-3.330986328125" Y="0.110112548828" />
                  <Point X="-3.342799560547" Y="0.129543807983" />
                  <Point X="-3.366069580078" Y="0.159144882202" />
                  <Point X="-3.379185791016" Y="0.172781021118" />
                  <Point X="-3.400305419922" Y="0.19075428772" />
                  <Point X="-3.415109375" Y="0.202146987915" />
                  <Point X="-3.433561035156" Y="0.214953521729" />
                  <Point X="-3.440484375" Y="0.219328979492" />
                  <Point X="-3.465616210938" Y="0.232834640503" />
                  <Point X="-3.496566894531" Y="0.245635818481" />
                  <Point X="-3.508288085938" Y="0.249611343384" />
                  <Point X="-4.494776367188" Y="0.513939819336" />
                  <Point X="-4.785446289062" Y="0.591824645996" />
                  <Point X="-4.731331542969" Y="0.957527282715" />
                  <Point X="-4.682633789062" Y="1.137236450195" />
                  <Point X="-4.6335859375" Y="1.318237060547" />
                  <Point X="-4.13452734375" Y="1.252534545898" />
                  <Point X="-3.778066162109" Y="1.20560559082" />
                  <Point X="-3.76773828125" Y="1.204815429688" />
                  <Point X="-3.747057617188" Y="1.204364257812" />
                  <Point X="-3.736704833984" Y="1.20470324707" />
                  <Point X="-3.715143310547" Y="1.20658984375" />
                  <Point X="-3.704889648438" Y="1.208053833008" />
                  <Point X="-3.684603271484" Y="1.212089233398" />
                  <Point X="-3.674571044922" Y="1.214660400391" />
                  <Point X="-3.653984375" Y="1.221151245117" />
                  <Point X="-3.613145019531" Y="1.234027954102" />
                  <Point X="-3.603451171875" Y="1.237676391602" />
                  <Point X="-3.584518798828" Y="1.246006713867" />
                  <Point X="-3.575279785156" Y="1.250688842773" />
                  <Point X="-3.556535400391" Y="1.261510864258" />
                  <Point X="-3.547860595703" Y="1.267171142578" />
                  <Point X="-3.531179443359" Y="1.27940222168" />
                  <Point X="-3.515929199219" Y="1.293376342773" />
                  <Point X="-3.502290527344" Y="1.308928100586" />
                  <Point X="-3.495895751953" Y="1.317076416016" />
                  <Point X="-3.483481201172" Y="1.334806152344" />
                  <Point X="-3.478011474609" Y="1.343602050781" />
                  <Point X="-3.468062255859" Y="1.361737060547" />
                  <Point X="-3.463582763672" Y="1.371076171875" />
                  <Point X="-3.455322265625" Y="1.391018920898" />
                  <Point X="-3.438935302734" Y="1.430580444336" />
                  <Point X="-3.435499023438" Y="1.4403515625" />
                  <Point X="-3.429710693359" Y="1.460210449219" />
                  <Point X="-3.427358642578" Y="1.470298217773" />
                  <Point X="-3.423600341797" Y="1.49161340332" />
                  <Point X="-3.422360595703" Y="1.501897338867" />
                  <Point X="-3.421008056641" Y="1.522537353516" />
                  <Point X="-3.421910400391" Y="1.543200805664" />
                  <Point X="-3.425056884766" Y="1.563644165039" />
                  <Point X="-3.427188232422" Y="1.573780395508" />
                  <Point X="-3.432790039062" Y="1.594686889648" />
                  <Point X="-3.436011962891" Y="1.604530517578" />
                  <Point X="-3.443508544922" Y="1.623808227539" />
                  <Point X="-3.447783203125" Y="1.6332421875" />
                  <Point X="-3.457750244141" Y="1.652389282227" />
                  <Point X="-3.477522949219" Y="1.690371948242" />
                  <Point X="-3.482799316406" Y="1.699285400391" />
                  <Point X="-3.494291748047" Y="1.716485351562" />
                  <Point X="-3.5005078125" Y="1.724771972656" />
                  <Point X="-3.514420410156" Y="1.741352172852" />
                  <Point X="-3.521500976563" Y="1.748911987305" />
                  <Point X="-3.536442626953" Y="1.763215332031" />
                  <Point X="-3.544303710938" Y="1.769958862305" />
                  <Point X="-4.110155273438" Y="2.204152587891" />
                  <Point X="-4.227614257812" Y="2.294282226563" />
                  <Point X="-4.220280761719" Y="2.306846435547" />
                  <Point X="-4.002293212891" Y="2.680312011719" />
                  <Point X="-3.873301025391" Y="2.846113037109" />
                  <Point X="-3.726338378906" Y="3.035012695312" />
                  <Point X="-3.473085449219" Y="2.888796875" />
                  <Point X="-3.254156982422" Y="2.7623984375" />
                  <Point X="-3.244917480469" Y="2.757716064453" />
                  <Point X="-3.225984619141" Y="2.749385498047" />
                  <Point X="-3.216291259766" Y="2.745737304688" />
                  <Point X="-3.195648925781" Y="2.739228759766" />
                  <Point X="-3.185615478516" Y="2.736657226562" />
                  <Point X="-3.165328369141" Y="2.732621826172" />
                  <Point X="-3.155074707031" Y="2.731157958984" />
                  <Point X="-3.126403076172" Y="2.728649414062" />
                  <Point X="-3.069525390625" Y="2.723673339844" />
                  <Point X="-3.059173339844" Y="2.723334472656" />
                  <Point X="-3.038492919922" Y="2.723785644531" />
                  <Point X="-3.028164550781" Y="2.724575683594" />
                  <Point X="-3.006705810547" Y="2.727400878906" />
                  <Point X="-2.996524902344" Y="2.729310791016" />
                  <Point X="-2.976432861328" Y="2.734227539062" />
                  <Point X="-2.956996826172" Y="2.741302001953" />
                  <Point X="-2.938445068359" Y="2.750450683594" />
                  <Point X="-2.929418212891" Y="2.755531738281" />
                  <Point X="-2.9111640625" Y="2.767161132812" />
                  <Point X="-2.902745117188" Y="2.773194091797" />
                  <Point X="-2.886614501953" Y="2.786140380859" />
                  <Point X="-2.878902832031" Y="2.793053710938" />
                  <Point X="-2.858551513672" Y="2.813404785156" />
                  <Point X="-2.818179199219" Y="2.853777099609" />
                  <Point X="-2.811265625" Y="2.861489257812" />
                  <Point X="-2.798318603516" Y="2.877620605469" />
                  <Point X="-2.79228515625" Y="2.886039794922" />
                  <Point X="-2.780655761719" Y="2.904293945312" />
                  <Point X="-2.775574707031" Y="2.913320800781" />
                  <Point X="-2.766426025391" Y="2.931872558594" />
                  <Point X="-2.7593515625" Y="2.95130859375" />
                  <Point X="-2.754434814453" Y="2.971400634766" />
                  <Point X="-2.752524902344" Y="2.981581542969" />
                  <Point X="-2.749699707031" Y="3.003040283203" />
                  <Point X="-2.748909667969" Y="3.013368652344" />
                  <Point X="-2.748458496094" Y="3.034049072266" />
                  <Point X="-2.748797363281" Y="3.044401123047" />
                  <Point X="-2.751305908203" Y="3.073072753906" />
                  <Point X="-2.756281982422" Y="3.129950683594" />
                  <Point X="-2.757745849609" Y="3.140204345703" />
                  <Point X="-2.76178125" Y="3.160491455078" />
                  <Point X="-2.764352783203" Y="3.170524902344" />
                  <Point X="-2.770861328125" Y="3.191167236328" />
                  <Point X="-2.774509521484" Y="3.200860595703" />
                  <Point X="-2.782840087891" Y="3.219793457031" />
                  <Point X="-2.787522460938" Y="3.229032958984" />
                  <Point X="-3.038268798828" Y="3.663337890625" />
                  <Point X="-3.059387451172" Y="3.699916259766" />
                  <Point X="-3.028034179688" Y="3.723954345703" />
                  <Point X="-2.648373779297" Y="4.015036376953" />
                  <Point X="-2.445221435547" Y="4.127903808594" />
                  <Point X="-2.192525146484" Y="4.268296386719" />
                  <Point X="-2.185584716797" Y="4.259251464844" />
                  <Point X="-2.118563964844" Y="4.171908203125" />
                  <Point X="-2.1118203125" Y="4.164047363281" />
                  <Point X="-2.097517578125" Y="4.149106445313" />
                  <Point X="-2.089958740234" Y="4.142026367188" />
                  <Point X="-2.07337890625" Y="4.128113769531" />
                  <Point X="-2.065091796875" Y="4.121897460938" />
                  <Point X="-2.047891845703" Y="4.110404785156" />
                  <Point X="-2.038978637695" Y="4.105128417969" />
                  <Point X="-2.007067382812" Y="4.088516601563" />
                  <Point X="-1.943762329102" Y="4.055561523438" />
                  <Point X="-1.934327392578" Y="4.051286621094" />
                  <Point X="-1.915048339844" Y="4.043789550781" />
                  <Point X="-1.905203857422" Y="4.040567382812" />
                  <Point X="-1.884297363281" Y="4.034965820313" />
                  <Point X="-1.874162597656" Y="4.032834716797" />
                  <Point X="-1.853719604492" Y="4.029688232422" />
                  <Point X="-1.833053344727" Y="4.028785888672" />
                  <Point X="-1.81241394043" Y="4.030138916016" />
                  <Point X="-1.802132568359" Y="4.031378662109" />
                  <Point X="-1.780817260742" Y="4.035136962891" />
                  <Point X="-1.770729980469" Y="4.037489013672" />
                  <Point X="-1.750869750977" Y="4.043277587891" />
                  <Point X="-1.741096801758" Y="4.046714355469" />
                  <Point X="-1.707859008789" Y="4.060482421875" />
                  <Point X="-1.641922607422" Y="4.087793945312" />
                  <Point X="-1.632584350586" Y="4.092273193359" />
                  <Point X="-1.614450439453" Y="4.102222167969" />
                  <Point X="-1.605654541016" Y="4.10769140625" />
                  <Point X="-1.587924926758" Y="4.120105957031" />
                  <Point X="-1.57977734375" Y="4.126500488281" />
                  <Point X="-1.564225952148" Y="4.140138671875" />
                  <Point X="-1.550251342773" Y="4.155389648437" />
                  <Point X="-1.538020263672" Y="4.172070800781" />
                  <Point X="-1.532360351562" Y="4.180744628906" />
                  <Point X="-1.521538330078" Y="4.199488769531" />
                  <Point X="-1.516856201172" Y="4.208728027344" />
                  <Point X="-1.508525756836" Y="4.227660644531" />
                  <Point X="-1.504877441406" Y="4.237354003906" />
                  <Point X="-1.494059082031" Y="4.271665039063" />
                  <Point X="-1.472598144531" Y="4.33973046875" />
                  <Point X="-1.470026733398" Y="4.349763671875" />
                  <Point X="-1.465991210938" Y="4.37005078125" />
                  <Point X="-1.46452722168" Y="4.3803046875" />
                  <Point X="-1.46264074707" Y="4.401866210938" />
                  <Point X="-1.462301757812" Y="4.41221875" />
                  <Point X="-1.462752929688" Y="4.432898925781" />
                  <Point X="-1.46354309082" Y="4.4432265625" />
                  <Point X="-1.479266113281" Y="4.562655273438" />
                  <Point X="-1.422668579102" Y="4.5785234375" />
                  <Point X="-0.931178161621" Y="4.7163203125" />
                  <Point X="-0.684903808594" Y="4.745143066406" />
                  <Point X="-0.365222015381" Y="4.782557128906" />
                  <Point X="-0.290431274414" Y="4.503434082031" />
                  <Point X="-0.22566633606" Y="4.261727539062" />
                  <Point X="-0.220435256958" Y="4.247107910156" />
                  <Point X="-0.207661849976" Y="4.218916503906" />
                  <Point X="-0.200119247437" Y="4.205344726562" />
                  <Point X="-0.182260864258" Y="4.178618164062" />
                  <Point X="-0.17260824585" Y="4.166455566406" />
                  <Point X="-0.151451187134" Y="4.143866699219" />
                  <Point X="-0.126897941589" Y="4.125026367188" />
                  <Point X="-0.099602897644" Y="4.110436523438" />
                  <Point X="-0.085356651306" Y="4.104260742188" />
                  <Point X="-0.054918682098" Y="4.093927978516" />
                  <Point X="-0.039857234955" Y="4.090155273438" />
                  <Point X="-0.009319986343" Y="4.08511328125" />
                  <Point X="0.021631772995" Y="4.08511328125" />
                  <Point X="0.052169025421" Y="4.090155273438" />
                  <Point X="0.06723046875" Y="4.093927978516" />
                  <Point X="0.097668441772" Y="4.104260742188" />
                  <Point X="0.111914535522" Y="4.110436523438" />
                  <Point X="0.139209732056" Y="4.125026367188" />
                  <Point X="0.163763122559" Y="4.143866699219" />
                  <Point X="0.184920181274" Y="4.166455566406" />
                  <Point X="0.194572952271" Y="4.178617675781" />
                  <Point X="0.212431182861" Y="4.205344238281" />
                  <Point X="0.2199737854" Y="4.218916503906" />
                  <Point X="0.232747192383" Y="4.247107910156" />
                  <Point X="0.237978271484" Y="4.261727539062" />
                  <Point X="0.366457702637" Y="4.741219726562" />
                  <Point X="0.378190246582" Y="4.785005859375" />
                  <Point X="0.398720184326" Y="4.782855957031" />
                  <Point X="0.827876281738" Y="4.737912109375" />
                  <Point X="1.031634643555" Y="4.688718261719" />
                  <Point X="1.453597167969" Y="4.58684375" />
                  <Point X="1.584266235352" Y="4.53944921875" />
                  <Point X="1.858256835938" Y="4.440070800781" />
                  <Point X="1.986506347656" Y="4.380092773438" />
                  <Point X="2.250453125" Y="4.256653320312" />
                  <Point X="2.374401855469" Y="4.184440429688" />
                  <Point X="2.629434570312" Y="4.035857421875" />
                  <Point X="2.746286376953" Y="3.952758789062" />
                  <Point X="2.817779785156" Y="3.901916503906" />
                  <Point X="2.354079345703" Y="3.098763671875" />
                  <Point X="2.06530859375" Y="2.598597900391" />
                  <Point X="2.062373046875" Y="2.593104980469" />
                  <Point X="2.053180908203" Y="2.573437988281" />
                  <Point X="2.044182250977" Y="2.549564208984" />
                  <Point X="2.041301757812" Y="2.540599121094" />
                  <Point X="2.034106201172" Y="2.51369140625" />
                  <Point X="2.01983203125" Y="2.460312744141" />
                  <Point X="2.017912597656" Y="2.451465576172" />
                  <Point X="2.013646972656" Y="2.420223388672" />
                  <Point X="2.012755615234" Y="2.383241943359" />
                  <Point X="2.013411254883" Y="2.369579833984" />
                  <Point X="2.016216918945" Y="2.3463125" />
                  <Point X="2.021782592773" Y="2.300155029297" />
                  <Point X="2.02380078125" Y="2.289034423828" />
                  <Point X="2.029143554687" Y="2.267111328125" />
                  <Point X="2.032468261719" Y="2.256308837891" />
                  <Point X="2.040734375" Y="2.234220214844" />
                  <Point X="2.045318481445" Y="2.223888671875" />
                  <Point X="2.055681640625" Y="2.203843261719" />
                  <Point X="2.061460449219" Y="2.194129394531" />
                  <Point X="2.075857666016" Y="2.172911865234" />
                  <Point X="2.104418212891" Y="2.130820800781" />
                  <Point X="2.109808837891" Y="2.123633056641" />
                  <Point X="2.130463134766" Y="2.100123535156" />
                  <Point X="2.157596923828" Y="2.075387451172" />
                  <Point X="2.168257080078" Y="2.066981933594" />
                  <Point X="2.189474609375" Y="2.052584716797" />
                  <Point X="2.231565673828" Y="2.024024414062" />
                  <Point X="2.241278808594" Y="2.01824597168" />
                  <Point X="2.26132421875" Y="2.00788269043" />
                  <Point X="2.271656494141" Y="2.003297973633" />
                  <Point X="2.293744628906" Y="1.995032104492" />
                  <Point X="2.304548095703" Y="1.991707275391" />
                  <Point X="2.326471191406" Y="1.986364746094" />
                  <Point X="2.337590820312" Y="1.984346801758" />
                  <Point X="2.360858154297" Y="1.981541259766" />
                  <Point X="2.407015625" Y="1.975975219727" />
                  <Point X="2.416044189453" Y="1.975320922852" />
                  <Point X="2.447575439453" Y="1.975497314453" />
                  <Point X="2.484314453125" Y="1.979822875977" />
                  <Point X="2.497748779297" Y="1.982395996094" />
                  <Point X="2.524656494141" Y="1.989591552734" />
                  <Point X="2.57803515625" Y="2.003865722656" />
                  <Point X="2.583995361328" Y="2.005670776367" />
                  <Point X="2.604404541016" Y="2.013067260742" />
                  <Point X="2.627655029297" Y="2.02357409668" />
                  <Point X="2.636033935547" Y="2.027872558594" />
                  <Point X="3.628251220703" Y="2.600729492188" />
                  <Point X="3.940404785156" Y="2.780951416016" />
                  <Point X="4.043951171875" Y="2.637045410156" />
                  <Point X="4.109097167969" Y="2.529391357422" />
                  <Point X="4.136884765625" Y="2.483472412109" />
                  <Point X="3.553145507813" Y="2.035552856445" />
                  <Point X="3.172951660156" Y="1.743819824219" />
                  <Point X="3.168137939453" Y="1.739868896484" />
                  <Point X="3.152119384766" Y="1.725216918945" />
                  <Point X="3.134668701172" Y="1.706603393555" />
                  <Point X="3.128576416016" Y="1.699422485352" />
                  <Point X="3.1092109375" Y="1.674158691406" />
                  <Point X="3.070794189453" Y="1.624041015625" />
                  <Point X="3.065635742188" Y="1.616602783203" />
                  <Point X="3.04973828125" Y="1.589370239258" />
                  <Point X="3.034762695312" Y="1.555544921875" />
                  <Point X="3.030140136719" Y="1.542671875" />
                  <Point X="3.022926513672" Y="1.516877441406" />
                  <Point X="3.008616210938" Y="1.465707397461" />
                  <Point X="3.006225097656" Y="1.454661010742" />
                  <Point X="3.002771728516" Y="1.432362792969" />
                  <Point X="3.001709472656" Y="1.421110839844" />
                  <Point X="3.000893310547" Y="1.397540649414" />
                  <Point X="3.001174804688" Y="1.386240966797" />
                  <Point X="3.003077880859" Y="1.363756225586" />
                  <Point X="3.004699462891" Y="1.352571044922" />
                  <Point X="3.01062109375" Y="1.323871582031" />
                  <Point X="3.022368408203" Y="1.266938354492" />
                  <Point X="3.02459765625" Y="1.258235351562" />
                  <Point X="3.034683837891" Y="1.228608520508" />
                  <Point X="3.050286376953" Y="1.195371337891" />
                  <Point X="3.056918457031" Y="1.183526000977" />
                  <Point X="3.073024658203" Y="1.159045166016" />
                  <Point X="3.104976074219" Y="1.110480712891" />
                  <Point X="3.111739013672" Y="1.101424682617" />
                  <Point X="3.126292480469" Y="1.08417980957" />
                  <Point X="3.134083007812" Y="1.075991210938" />
                  <Point X="3.151326904297" Y="1.059901367188" />
                  <Point X="3.160034423828" Y="1.052696044922" />
                  <Point X="3.178243896484" Y="1.039370361328" />
                  <Point X="3.187745849609" Y="1.03325" />
                  <Point X="3.2110859375" Y="1.02011138916" />
                  <Point X="3.257387939453" Y="0.994047546387" />
                  <Point X="3.265479248047" Y="0.989987915039" />
                  <Point X="3.294677978516" Y="0.978084289551" />
                  <Point X="3.330274658203" Y="0.96802130127" />
                  <Point X="3.343670410156" Y="0.965257507324" />
                  <Point X="3.375227783203" Y="0.961086853027" />
                  <Point X="3.437831054688" Y="0.952812988281" />
                  <Point X="3.444029785156" Y="0.952199829102" />
                  <Point X="3.465716064453" Y="0.951222900391" />
                  <Point X="3.491217529297" Y="0.952032287598" />
                  <Point X="3.500603515625" Y="0.952797241211" />
                  <Point X="4.443143554688" Y="1.076885009766" />
                  <Point X="4.704704101562" Y="1.11132019043" />
                  <Point X="4.75268359375" Y="0.914235656738" />
                  <Point X="4.773212402344" Y="0.782379089355" />
                  <Point X="4.78387109375" Y="0.713920776367" />
                  <Point X="4.127702148438" Y="0.538101013184" />
                  <Point X="3.691991943359" Y="0.421352966309" />
                  <Point X="3.686031738281" Y="0.419544525146" />
                  <Point X="3.665626708984" Y="0.412138061523" />
                  <Point X="3.642381103516" Y="0.401619415283" />
                  <Point X="3.634004394531" Y="0.397316497803" />
                  <Point X="3.603" Y="0.379395385742" />
                  <Point X="3.541494384766" Y="0.343844055176" />
                  <Point X="3.533881835938" Y="0.338945739746" />
                  <Point X="3.508773925781" Y="0.319870452881" />
                  <Point X="3.481993652344" Y="0.294350708008" />
                  <Point X="3.472796630859" Y="0.284226196289" />
                  <Point X="3.454194091797" Y="0.260522033691" />
                  <Point X="3.417290771484" Y="0.213498657227" />
                  <Point X="3.410854248047" Y="0.204207794189" />
                  <Point X="3.399130126953" Y="0.184927536011" />
                  <Point X="3.393842529297" Y="0.174938293457" />
                  <Point X="3.384068847656" Y="0.153474609375" />
                  <Point X="3.380005371094" Y="0.142928634644" />
                  <Point X="3.373159179688" Y="0.121427955627" />
                  <Point X="3.370376464844" Y="0.110473251343" />
                  <Point X="3.364175537109" Y="0.078094566345" />
                  <Point X="3.351874267578" Y="0.01386257267" />
                  <Point X="3.350603515625" Y="0.00496778965" />
                  <Point X="3.348584472656" Y="-0.026262191772" />
                  <Point X="3.350280029297" Y="-0.062940689087" />
                  <Point X="3.351874267578" Y="-0.076422866821" />
                  <Point X="3.358075195312" Y="-0.10880140686" />
                  <Point X="3.370376464844" Y="-0.173033401489" />
                  <Point X="3.373159179688" Y="-0.183988250732" />
                  <Point X="3.380005371094" Y="-0.20548878479" />
                  <Point X="3.384068847656" Y="-0.216034759521" />
                  <Point X="3.393842529297" Y="-0.237498428345" />
                  <Point X="3.399130126953" Y="-0.247487976074" />
                  <Point X="3.410854248047" Y="-0.266768218994" />
                  <Point X="3.417290771484" Y="-0.276058654785" />
                  <Point X="3.435893310547" Y="-0.299762817383" />
                  <Point X="3.472796630859" Y="-0.346786346436" />
                  <Point X="3.478718017578" Y="-0.353633270264" />
                  <Point X="3.501139404297" Y="-0.375805267334" />
                  <Point X="3.530176025391" Y="-0.398724700928" />
                  <Point X="3.541494384766" Y="-0.406404205322" />
                  <Point X="3.572498779297" Y="-0.424325286865" />
                  <Point X="3.634004394531" Y="-0.459876647949" />
                  <Point X="3.639496582031" Y="-0.46281552124" />
                  <Point X="3.659158447266" Y="-0.472016906738" />
                  <Point X="3.683028076172" Y="-0.481027770996" />
                  <Point X="3.691991943359" Y="-0.483912963867" />
                  <Point X="4.556344238281" Y="-0.715515319824" />
                  <Point X="4.784876953125" Y="-0.776750488281" />
                  <Point X="4.76161328125" Y="-0.931052185059" />
                  <Point X="4.735312011719" Y="-1.04630871582" />
                  <Point X="4.727801757812" Y="-1.079219604492" />
                  <Point X="3.942083496094" Y="-0.975777954102" />
                  <Point X="3.436781982422" Y="-0.909253723145" />
                  <Point X="3.428624267578" Y="-0.908535888672" />
                  <Point X="3.40009765625" Y="-0.908042602539" />
                  <Point X="3.366720947266" Y="-0.910841003418" />
                  <Point X="3.354480957031" Y="-0.912676391602" />
                  <Point X="3.293630371094" Y="-0.92590246582" />
                  <Point X="3.172916748047" Y="-0.952140075684" />
                  <Point X="3.157873535156" Y="-0.956742736816" />
                  <Point X="3.128753173828" Y="-0.96836730957" />
                  <Point X="3.114676025391" Y="-0.975389282227" />
                  <Point X="3.086849609375" Y="-0.992281494141" />
                  <Point X="3.074124267578" Y="-1.001530273438" />
                  <Point X="3.050374267578" Y="-1.022000976562" />
                  <Point X="3.039349609375" Y="-1.03322277832" />
                  <Point X="3.002569335938" Y="-1.077457885742" />
                  <Point X="2.92960546875" Y="-1.16521105957" />
                  <Point X="2.921326171875" Y="-1.176847290039" />
                  <Point X="2.90660546875" Y="-1.201229492188" />
                  <Point X="2.9001640625" Y="-1.213975708008" />
                  <Point X="2.888820800781" Y="-1.241360839844" />
                  <Point X="2.884362792969" Y="-1.254928222656" />
                  <Point X="2.877531005859" Y="-1.282577758789" />
                  <Point X="2.875157226562" Y="-1.296660400391" />
                  <Point X="2.869885742188" Y="-1.353946777344" />
                  <Point X="2.859428222656" Y="-1.467590698242" />
                  <Point X="2.859288818359" Y="-1.483321533203" />
                  <Point X="2.861607666016" Y="-1.514590454102" />
                  <Point X="2.864065917969" Y="-1.53012878418" />
                  <Point X="2.871797607422" Y="-1.561749633789" />
                  <Point X="2.876786621094" Y="-1.576669189453" />
                  <Point X="2.889157958984" Y="-1.605479980469" />
                  <Point X="2.896540283203" Y="-1.619371337891" />
                  <Point X="2.930215820312" Y="-1.671751464844" />
                  <Point X="2.997020507812" Y="-1.775661987305" />
                  <Point X="3.001741943359" Y="-1.782353027344" />
                  <Point X="3.019792724609" Y="-1.804448730469" />
                  <Point X="3.043488769531" Y="-1.828119750977" />
                  <Point X="3.052796142578" Y="-1.836277587891" />
                  <Point X="3.854920166016" Y="-2.45176953125" />
                  <Point X="4.087170898438" Y="-2.629982177734" />
                  <Point X="4.045491943359" Y="-2.697425537109" />
                  <Point X="4.001274414063" Y="-2.760251708984" />
                  <Point X="3.298493652344" Y="-2.354500976562" />
                  <Point X="2.848454589844" Y="-2.094670654297" />
                  <Point X="2.841191894531" Y="-2.090885253906" />
                  <Point X="2.815026123047" Y="-2.079513183594" />
                  <Point X="2.783119140625" Y="-2.069325927734" />
                  <Point X="2.771108398438" Y="-2.066337646484" />
                  <Point X="2.698686523438" Y="-2.053258300781" />
                  <Point X="2.555018066406" Y="-2.027311889648" />
                  <Point X="2.539359130859" Y="-2.025807495117" />
                  <Point X="2.508006835938" Y="-2.025403198242" />
                  <Point X="2.492313476562" Y="-2.026503540039" />
                  <Point X="2.460140625" Y="-2.031461303711" />
                  <Point X="2.444844482422" Y="-2.035136230469" />
                  <Point X="2.415068603516" Y="-2.044959960938" />
                  <Point X="2.400588867188" Y="-2.051108642578" />
                  <Point X="2.340424072266" Y="-2.082772949219" />
                  <Point X="2.221070800781" Y="-2.145587646484" />
                  <Point X="2.208968261719" Y="-2.153170410156" />
                  <Point X="2.186037597656" Y="-2.170063476562" />
                  <Point X="2.175209472656" Y="-2.179373779297" />
                  <Point X="2.154249755859" Y="-2.200333496094" />
                  <Point X="2.144939453125" Y="-2.211161621094" />
                  <Point X="2.128046386719" Y="-2.234092285156" />
                  <Point X="2.120463623047" Y="-2.246194824219" />
                  <Point X="2.088799316406" Y="-2.306359619141" />
                  <Point X="2.025984741211" Y="-2.425712890625" />
                  <Point X="2.0198359375" Y="-2.440192626953" />
                  <Point X="2.010012207031" Y="-2.469968505859" />
                  <Point X="2.006337280273" Y="-2.485264648438" />
                  <Point X="2.001379516602" Y="-2.5174375" />
                  <Point X="2.000279174805" Y="-2.533130126953" />
                  <Point X="2.000683227539" Y="-2.564482177734" />
                  <Point X="2.002187744141" Y="-2.580141601562" />
                  <Point X="2.015266601563" Y="-2.652563232422" />
                  <Point X="2.041213256836" Y="-2.796231933594" />
                  <Point X="2.043015136719" Y="-2.804222167969" />
                  <Point X="2.051236083984" Y="-2.83154296875" />
                  <Point X="2.064069580078" Y="-2.862479736328" />
                  <Point X="2.069546630859" Y="-2.873578613281" />
                  <Point X="2.5849921875" Y="-3.766355957031" />
                  <Point X="2.735893066406" Y="-4.027724609375" />
                  <Point X="2.723754394531" Y="-4.036083496094" />
                  <Point X="2.177336181641" Y="-3.323978027344" />
                  <Point X="1.833914550781" Y="-2.876422851562" />
                  <Point X="1.828653564453" Y="-2.870146240234" />
                  <Point X="1.808831420898" Y="-2.849626708984" />
                  <Point X="1.783252197266" Y="-2.828004882812" />
                  <Point X="1.773299316406" Y="-2.820647705078" />
                  <Point X="1.701871948242" Y="-2.774726318359" />
                  <Point X="1.56017590332" Y="-2.68362890625" />
                  <Point X="1.546284545898" Y="-2.676246337891" />
                  <Point X="1.517473388672" Y="-2.663874755859" />
                  <Point X="1.502553344727" Y="-2.658885742188" />
                  <Point X="1.470932006836" Y="-2.651154052734" />
                  <Point X="1.455394165039" Y="-2.648695800781" />
                  <Point X="1.424125244141" Y="-2.646376953125" />
                  <Point X="1.408394287109" Y="-2.646516357422" />
                  <Point X="1.330276245117" Y="-2.653704833984" />
                  <Point X="1.175307373047" Y="-2.667965332031" />
                  <Point X="1.161225097656" Y="-2.670339111328" />
                  <Point X="1.133575317383" Y="-2.677170898438" />
                  <Point X="1.120007568359" Y="-2.68162890625" />
                  <Point X="1.092622680664" Y="-2.692972167969" />
                  <Point X="1.079876342773" Y="-2.699413818359" />
                  <Point X="1.055494750977" Y="-2.714134277344" />
                  <Point X="1.04385925293" Y="-2.722413085938" />
                  <Point X="0.983538391113" Y="-2.772567626953" />
                  <Point X="0.863875244141" Y="-2.872063476562" />
                  <Point X="0.852653320312" Y="-2.883087890625" />
                  <Point X="0.832182128906" Y="-2.906838134766" />
                  <Point X="0.822933044434" Y="-2.919564208984" />
                  <Point X="0.806040710449" Y="-2.947390869141" />
                  <Point X="0.799018859863" Y="-2.961468017578" />
                  <Point X="0.787394348145" Y="-2.990588378906" />
                  <Point X="0.782791625977" Y="-3.005631591797" />
                  <Point X="0.764756225586" Y="-3.088609619141" />
                  <Point X="0.728977355957" Y="-3.253219482422" />
                  <Point X="0.727584472656" Y="-3.261289794922" />
                  <Point X="0.724724487305" Y="-3.289677734375" />
                  <Point X="0.7247421875" Y="-3.323170410156" />
                  <Point X="0.725554992676" Y="-3.335520019531" />
                  <Point X="0.833091552734" Y="-4.152341308594" />
                  <Point X="0.789317932129" Y="-3.988975830078" />
                  <Point X="0.655064941406" Y="-3.487936523438" />
                  <Point X="0.652606262207" Y="-3.480124511719" />
                  <Point X="0.642145019531" Y="-3.453580322266" />
                  <Point X="0.62678692627" Y="-3.423815673828" />
                  <Point X="0.620407592773" Y="-3.413210205078" />
                  <Point X="0.565534973145" Y="-3.334149169922" />
                  <Point X="0.456679992676" Y="-3.177309814453" />
                  <Point X="0.446670684814" Y="-3.165172851562" />
                  <Point X="0.424786712646" Y="-3.142717529297" />
                  <Point X="0.412912322998" Y="-3.132399169922" />
                  <Point X="0.386657196045" Y="-3.113155273438" />
                  <Point X="0.373242340088" Y="-3.104937744141" />
                  <Point X="0.345241210938" Y="-3.090829589844" />
                  <Point X="0.330654907227" Y="-3.084938964844" />
                  <Point X="0.245742767334" Y="-3.058585205078" />
                  <Point X="0.077295806885" Y="-3.006305664062" />
                  <Point X="0.063377269745" Y="-3.003109619141" />
                  <Point X="0.035217689514" Y="-2.998840087891" />
                  <Point X="0.020976644516" Y="-2.997766601562" />
                  <Point X="-0.008664708138" Y="-2.997766601562" />
                  <Point X="-0.022905010223" Y="-2.998840087891" />
                  <Point X="-0.051064441681" Y="-3.003109375" />
                  <Point X="-0.064983421326" Y="-3.006305175781" />
                  <Point X="-0.149895553589" Y="-3.032658691406" />
                  <Point X="-0.318342529297" Y="-3.084938476562" />
                  <Point X="-0.332929412842" Y="-3.090829589844" />
                  <Point X="-0.360930847168" Y="-3.104937988281" />
                  <Point X="-0.374345275879" Y="-3.113155273438" />
                  <Point X="-0.400600524902" Y="-3.132399169922" />
                  <Point X="-0.412475067139" Y="-3.142717529297" />
                  <Point X="-0.434358734131" Y="-3.165172607422" />
                  <Point X="-0.444367767334" Y="-3.177309326172" />
                  <Point X="-0.499240356445" Y="-3.256370117188" />
                  <Point X="-0.608095336914" Y="-3.413209716797" />
                  <Point X="-0.612470336914" Y="-3.420132324219" />
                  <Point X="-0.625976745605" Y="-3.445265136719" />
                  <Point X="-0.638777832031" Y="-3.476215820312" />
                  <Point X="-0.642752990723" Y="-3.487936523438" />
                  <Point X="-0.909081237793" Y="-4.48188671875" />
                  <Point X="-0.985425231934" Y="-4.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.952183828287" Y="-3.781874202755" />
                  <Point X="-2.444996102021" Y="-4.086623333922" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.904676579022" Y="-3.699589265221" />
                  <Point X="-2.38319694707" Y="-4.012925839686" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.857169329756" Y="-3.617304327688" />
                  <Point X="-2.29967564736" Y="-3.952280326796" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.131120999339" Y="-4.654418795974" />
                  <Point X="-0.979691217568" Y="-4.745406988388" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.896312629467" Y="-2.882093868629" />
                  <Point X="-3.730628754098" Y="-2.981646784546" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.809662080491" Y="-3.535019390154" />
                  <Point X="-2.212341939706" Y="-3.893925539703" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.119669918476" Y="-4.550469126774" />
                  <Point X="-0.954112553304" Y="-4.649946027696" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.035656772809" Y="-2.687537287668" />
                  <Point X="-3.636562208758" Y="-2.927337494474" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.762154831226" Y="-3.45273445262" />
                  <Point X="-2.125008124207" Y="-3.835570817411" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.136946035043" Y="-4.429258415944" />
                  <Point X="-0.92853388904" Y="-4.554485067004" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.138659686363" Y="-2.514816720533" />
                  <Point X="-3.542495664241" Y="-2.873028203907" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.71464758196" Y="-3.370449515087" />
                  <Point X="-2.023080684594" Y="-3.785984829137" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.161984043779" Y="-4.30338388978" />
                  <Point X="-0.902955218139" Y="-4.4590241103" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.133766455751" Y="-2.406926697372" />
                  <Point X="-3.448429172278" Y="-2.818718881762" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.667140332695" Y="-3.288164577553" />
                  <Point X="-1.844312471425" Y="-3.782569435629" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.188913950546" Y="-4.176372596594" />
                  <Point X="-0.877376526159" Y="-4.363563166262" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.052761434057" Y="-2.344769252116" />
                  <Point X="-3.354362680315" Y="-2.764409559617" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.61963308343" Y="-3.20587964002" />
                  <Point X="-1.637275902112" Y="-3.796139384092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.397017046695" Y="-3.940501468685" />
                  <Point X="-0.851797834178" Y="-4.268102222223" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.971756412363" Y="-2.282611806859" />
                  <Point X="-3.260296188352" Y="-2.710100237473" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.572125837874" Y="-3.123594700257" />
                  <Point X="-0.826219142198" Y="-4.172641278185" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.89075139067" Y="-2.220454361603" />
                  <Point X="-3.166229696388" Y="-2.655790915328" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.524618597954" Y="-3.041309757108" />
                  <Point X="-0.800640450218" Y="-4.077180334146" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.809746368976" Y="-2.158296916347" />
                  <Point X="-3.072163204425" Y="-2.601481593183" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.477111358034" Y="-2.959024813959" />
                  <Point X="-0.775061758238" Y="-3.981719390108" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.728741347282" Y="-2.09613947109" />
                  <Point X="-2.978096712462" Y="-2.547172271038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.429604118115" Y="-2.87673987081" />
                  <Point X="-0.749483066258" Y="-3.88625844607" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.647736325589" Y="-2.033982025834" />
                  <Point X="-2.884030220499" Y="-2.492862948894" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.382096878195" Y="-2.794454927661" />
                  <Point X="-0.723904374278" Y="-3.790797502031" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.566731303895" Y="-1.971824580578" />
                  <Point X="-2.789963728536" Y="-2.438553626749" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.335561077994" Y="-2.711586284641" />
                  <Point X="-0.698325682298" Y="-3.695336557993" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.6860117354" Y="-1.188462874903" />
                  <Point X="-4.547400038761" Y="-1.27174918475" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.485726282201" Y="-1.909667135322" />
                  <Point X="-2.695897236572" Y="-2.384244304604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.311507126539" Y="-2.615209184067" />
                  <Point X="-0.672746990318" Y="-3.599875613954" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.719453734401" Y="-1.057538721946" />
                  <Point X="-4.396098817722" Y="-1.251829957348" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.404721146531" Y="-1.847509758549" />
                  <Point X="-2.570587384427" Y="-2.348707887199" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.343306585334" Y="-2.485271968835" />
                  <Point X="-0.647168298338" Y="-3.504414669916" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.747053978049" Y="-0.930124649727" />
                  <Point X="-4.244797568967" Y="-1.231910746599" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.323716009336" Y="-1.785352382694" />
                  <Point X="-0.60985982736" Y="-3.416001688147" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.764395784139" Y="-0.80887446865" />
                  <Point X="-4.093496296627" Y="-1.211991550021" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.24271087214" Y="-1.723195006838" />
                  <Point X="-0.55569811636" Y="-3.337715154611" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.781737197397" Y="-0.687624523609" />
                  <Point X="-3.942195024287" Y="-1.192072353443" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.161705734944" Y="-1.661037630983" />
                  <Point X="-0.501414117542" Y="-3.259502099009" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.804033196965" Y="-4.043893980511" />
                  <Point X="0.820083877346" Y="-4.053538202261" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.69357448062" Y="-0.629767855451" />
                  <Point X="-3.790893751947" Y="-1.172153156866" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.080700597748" Y="-1.598880255127" />
                  <Point X="-0.447129973253" Y="-3.181289130814" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.768637682255" Y="-3.911796036896" />
                  <Point X="0.80423943743" Y="-3.933187729549" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.566008959882" Y="-0.595586780473" />
                  <Point X="-3.639592479608" Y="-1.152233960288" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.007297183989" Y="-1.532155303022" />
                  <Point X="-0.375122852274" Y="-3.113725201365" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.733242167901" Y="-3.779698093496" />
                  <Point X="0.788394997514" Y="-3.812837256838" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.438443439144" Y="-0.561405705495" />
                  <Point X="-3.488291207268" Y="-1.13231476371" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.963664251261" Y="-1.447542441255" />
                  <Point X="-0.265746917139" Y="-3.068614720721" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.697846653548" Y="-3.647600150095" />
                  <Point X="0.772550557598" Y="-3.692486784126" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.310877918406" Y="-0.527224630517" />
                  <Point X="-3.336989934928" Y="-1.112395567133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.947363104883" Y="-1.346506985423" />
                  <Point X="-0.144119116333" Y="-3.030865903669" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.662451139194" Y="-3.515502206694" />
                  <Point X="0.756706117682" Y="-3.572136311414" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.183312397668" Y="-0.493043555539" />
                  <Point X="-3.162831444784" Y="-1.106210372594" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.000174255407" Y="-1.203944672092" />
                  <Point X="-0.014074511891" Y="-2.998174412459" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.580166512263" Y="-3.355230442084" />
                  <Point X="0.740861677765" Y="-3.451785838703" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.05574687693" Y="-0.45886248056" />
                  <Point X="0.297815828891" Y="-3.074746862955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.444354462725" Y="-3.162796157192" />
                  <Point X="0.725297228661" Y="-3.331603601444" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.928181361214" Y="-0.424681402564" />
                  <Point X="0.734789935019" Y="-3.226477222127" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.800615848756" Y="-0.390500322611" />
                  <Point X="0.756096804098" Y="-3.128449507936" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.673050336297" Y="-0.356319242658" />
                  <Point X="0.777403465074" Y="-3.030421668703" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.545484823839" Y="-0.322138162705" />
                  <Point X="0.81075229683" Y="-2.939629495661" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.434984209287" Y="-0.277703457632" />
                  <Point X="0.871795690304" Y="-2.865477894115" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.641408803829" Y="-3.928768724948" />
                  <Point X="2.698601505299" Y="-3.963133566957" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.778279121331" Y="0.640259727491" />
                  <Point X="-4.627021024712" Y="0.549374693924" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.358207332342" Y="-0.213005486705" />
                  <Point X="0.949173408388" Y="-2.801140944967" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.483612803351" Y="-3.723125149685" />
                  <Point X="2.600625282311" Y="-3.793433340227" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.763218160236" Y="0.74204036182" />
                  <Point X="-4.2941092172" Y="0.460171271916" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.312953493543" Y="-0.129366563564" />
                  <Point X="1.026551231242" Y="-2.73680405877" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.325816802872" Y="-3.517481574421" />
                  <Point X="2.502648661898" Y="-3.623732874699" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.748157199141" Y="0.843820996148" />
                  <Point X="-3.96119759163" Y="0.370967959229" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.294928431082" Y="-0.029366941016" />
                  <Point X="1.119515770703" Y="-2.681832616763" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.168020793763" Y="-3.311837993973" />
                  <Point X="2.404671966032" Y="-3.454032363836" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.733096238046" Y="0.945601630477" />
                  <Point X="-3.628285966059" Y="0.281764646543" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.326624967235" Y="0.100508432053" />
                  <Point X="1.266866805487" Y="-2.659539878003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.0102246471" Y="-3.106194330873" />
                  <Point X="2.306695270167" Y="-3.284331852972" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.708530192611" Y="1.041671033946" />
                  <Point X="1.430156819266" Y="-2.646824244027" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.852428500436" Y="-2.900550667773" />
                  <Point X="2.208718574301" Y="-3.114631342109" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.682702621909" Y="1.136982436561" />
                  <Point X="2.110741878435" Y="-2.944930831245" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.656874987393" Y="1.232293800833" />
                  <Point X="2.040421499918" Y="-2.791847912344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.610369268618" Y="1.315180518597" />
                  <Point X="2.017969058415" Y="-2.667526951708" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.374162211694" Y="1.28408317289" />
                  <Point X="2.000447209318" Y="-2.546168589878" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.13795515477" Y="1.252985827184" />
                  <Point X="2.017964807191" Y="-2.445864051844" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.901748370257" Y="1.221888645159" />
                  <Point X="2.060315762851" Y="-2.360480900543" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.696912063338" Y="1.209640747719" />
                  <Point X="2.10463104221" Y="-2.276278033996" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.578260873223" Y="1.249178092914" />
                  <Point X="2.157417866671" Y="-2.197165385282" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.499393269522" Y="1.312619828468" />
                  <Point X="2.239707042439" Y="-2.135779537638" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.453348661705" Y="1.395783609648" />
                  <Point X="2.338034589227" Y="-2.084030515533" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.424058577083" Y="1.489014524006" />
                  <Point X="2.442435789158" Y="-2.035930912415" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.437278788196" Y="1.607788200975" />
                  <Point X="2.637264242634" Y="-2.04216548484" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.622196302929" Y="1.829728026082" />
                  <Point X="4.005892499679" Y="-2.753690133851" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.223272321001" Y="2.301721107118" />
                  <Point X="4.059359224462" Y="-2.674986010466" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.175378992034" Y="2.383774064563" />
                  <Point X="3.792126515184" Y="-2.403586226609" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.127485636562" Y="2.465827006083" />
                  <Point X="3.126348007047" Y="-1.892715967339" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.079592281091" Y="2.547879947602" />
                  <Point X="2.922134969586" Y="-1.659182222502" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.031698925619" Y="2.629932889122" />
                  <Point X="2.861405328716" Y="-1.511862000159" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.979611676863" Y="2.709465885326" />
                  <Point X="2.865335660558" Y="-1.403393409048" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.920853787461" Y="2.784990756263" />
                  <Point X="2.874999900475" Y="-1.298370097491" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.862095905156" Y="2.860515631463" />
                  <Point X="2.904550552593" Y="-1.20529574788" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.80333805297" Y="2.936040524762" />
                  <Point X="2.960483926612" Y="-1.128073736882" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.744580200785" Y="3.01156541806" />
                  <Point X="3.021934991762" Y="-1.054167089192" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.082949539651" Y="2.72484778218" />
                  <Point X="3.095078600145" Y="-0.987286030268" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.939907379605" Y="2.749729554083" />
                  <Point X="3.208255073209" Y="-0.944459143197" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.859631312398" Y="2.812324999383" />
                  <Point X="3.343708768999" Y="-0.915017761963" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.793963415722" Y="2.883697919072" />
                  <Point X="3.54151612522" Y="-0.923042239735" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.754567880714" Y="2.970856866255" />
                  <Point X="3.777723000576" Y="-0.954139476345" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.751920372657" Y="3.080096255661" />
                  <Point X="4.01392984485" Y="-0.985236694279" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.776686026661" Y="3.205807134592" />
                  <Point X="4.25013661802" Y="-1.016333869488" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.870723049422" Y="3.373140451035" />
                  <Point X="4.486343391189" Y="-1.047431044698" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.968699813814" Y="3.542841003073" />
                  <Point X="3.444304623693" Y="-0.310480813074" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.765905243839" Y="-0.503717960774" />
                  <Point X="4.722550164359" Y="-1.078528219908" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.053358003032" Y="3.704538947788" />
                  <Point X="3.366523536247" Y="-0.152915047987" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.0988169694" Y="-0.592921333541" />
                  <Point X="4.749548856445" Y="-0.983920498012" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.972315103258" Y="3.766673633597" />
                  <Point X="3.348824296851" Y="-0.031450099312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.431728694961" Y="-0.682124706308" />
                  <Point X="4.768624019335" Y="-0.884551839458" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.891272304007" Y="3.828808379807" />
                  <Point X="3.36281211293" Y="0.070975345596" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.764640557404" Y="-0.771328161321" />
                  <Point X="4.783945614187" Y="-0.782927809691" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.810229504756" Y="3.890943126017" />
                  <Point X="3.389631580205" Y="0.165690756622" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.729186705505" Y="3.953077872227" />
                  <Point X="3.44203779627" Y="0.245032097932" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.648101942536" Y="4.015187404091" />
                  <Point X="3.506130524292" Y="0.317351474433" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.552264641299" Y="4.068432716679" />
                  <Point X="3.595010377967" Y="0.37477724327" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.456427340062" Y="4.121678029267" />
                  <Point X="3.69887728073" Y="0.423197884515" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.360589896496" Y="4.174923256335" />
                  <Point X="3.826442848473" Y="0.45737893125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.264752434084" Y="4.22816847208" />
                  <Point X="3.954008416217" Y="0.491559977984" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.765495354817" Y="4.039014727114" />
                  <Point X="3.102735221927" Y="1.113886689202" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.351907488787" Y="0.964168886692" />
                  <Point X="4.08157398396" Y="0.525741024719" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.653940531615" Y="4.082815999724" />
                  <Point X="3.020939229604" Y="1.273864852519" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.545457634775" Y="0.958702398897" />
                  <Point X="4.209139544396" Y="0.559922075844" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.564495574243" Y="4.139902220005" />
                  <Point X="3.000913563499" Y="1.396727659387" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.696758911541" Y="0.978621592815" />
                  <Point X="4.336705100693" Y="0.594103129457" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.512200049853" Y="4.219310071582" />
                  <Point X="3.017528277123" Y="1.497574705009" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.848060188308" Y="0.998540786732" />
                  <Point X="4.464270656989" Y="0.628284183069" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.481447934101" Y="4.311662509011" />
                  <Point X="2.074361672152" Y="2.175116547854" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.40548038861" Y="1.976160350911" />
                  <Point X="3.049626695988" Y="1.589118201915" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.999361465074" Y="1.01845998065" />
                  <Point X="4.591836213286" Y="0.662465236681" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.462341264558" Y="4.411012236457" />
                  <Point X="2.019522643303" Y="2.31889733341" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.554359953627" Y="1.99753465605" />
                  <Point X="3.10395643253" Y="1.66730377552" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.150662741841" Y="1.038379174568" />
                  <Point X="4.719401769583" Y="0.696646290294" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.474885310297" Y="4.52937963228" />
                  <Point X="2.015291706617" Y="2.432269709382" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.662699442305" Y="2.043267896553" />
                  <Point X="3.16794059294" Y="1.739688386024" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.301964018607" Y="1.058298368486" />
                  <Point X="4.774455176351" Y="0.774397038959" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.388271736855" Y="4.588167119661" />
                  <Point X="2.038277197255" Y="2.529288805983" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.756765939432" Y="2.097577215595" />
                  <Point X="3.248736424344" Y="1.801971525487" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.453265291342" Y="1.078217564826" />
                  <Point X="4.755419143938" Y="0.896665213914" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.262503569941" Y="4.623428153771" />
                  <Point X="2.076144397675" Y="2.617366069234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.850832436559" Y="2.151886534637" />
                  <Point X="3.329741518452" Y="1.864128927233" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.604566507849" Y="1.098136794952" />
                  <Point X="4.725621082444" Y="1.025399868325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.136735403027" Y="4.65868918788" />
                  <Point X="2.12365162216" Y="2.699651021657" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.944898933687" Y="2.206195853678" />
                  <Point X="3.41074661256" Y="1.926286328978" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.010967236112" Y="4.69395022199" />
                  <Point X="-0.20128644319" Y="4.207444919539" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.002307592457" Y="4.08511328125" />
                  <Point X="2.171158846645" Y="2.78193597408" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.038965430814" Y="2.26050517272" />
                  <Point X="3.491751706668" Y="1.988443730724" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.874738255262" Y="4.722925764962" />
                  <Point X="-0.248404109888" Y="4.346586242654" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.129220119975" Y="4.119686714139" />
                  <Point X="2.21866607113" Y="2.864220926503" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.133031927942" Y="2.314814491762" />
                  <Point X="3.572756785559" Y="2.050601141612" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.720356270113" Y="4.740993882534" />
                  <Point X="-0.283799563836" Y="4.478684149759" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.200595134411" Y="4.187630451517" />
                  <Point X="2.266173295615" Y="2.946505878926" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.227098425069" Y="2.369123810804" />
                  <Point X="3.653761816816" Y="2.112758581122" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.565974315228" Y="4.75906201829" />
                  <Point X="-0.319195064287" Y="4.610782084807" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.241271904348" Y="4.274019555088" />
                  <Point X="2.3136805201" Y="3.028790831349" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.321164922196" Y="2.423433129845" />
                  <Point X="3.734766848073" Y="2.174916020632" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.411592369363" Y="4.777130159467" />
                  <Point X="-0.354590575459" Y="4.742880026296" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.266850569054" Y="4.369480515515" />
                  <Point X="2.361187745513" Y="3.111075783215" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.415231419324" Y="2.477742448887" />
                  <Point X="3.81577187933" Y="2.237073460142" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.292429233759" Y="4.464941475942" />
                  <Point X="2.408694976203" Y="3.19336073191" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.509297916451" Y="2.532051767929" />
                  <Point X="3.896776910587" Y="2.299230899652" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.318007898464" Y="4.560402436368" />
                  <Point X="2.456202206893" Y="3.275645680605" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.603364413578" Y="2.586361086971" />
                  <Point X="3.977781941844" Y="2.361388339162" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.343586563169" Y="4.655863396795" />
                  <Point X="2.503709437582" Y="3.3579306293" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.697430915589" Y="2.640670403078" />
                  <Point X="4.058786973101" Y="2.423545778672" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.36916524683" Y="4.751324345832" />
                  <Point X="2.551216668272" Y="3.440215577995" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.791497419355" Y="2.694979718131" />
                  <Point X="4.133102423929" Y="2.489722723619" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.522759456108" Y="4.769865806902" />
                  <Point X="2.598723898961" Y="3.52250052669" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.885563923122" Y="2.749289033183" />
                  <Point X="4.022307506322" Y="2.667125199134" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.746146804083" Y="4.74647131945" />
                  <Point X="2.646231129651" Y="3.604785475385" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.023412783355" Y="4.690703284245" />
                  <Point X="2.69373836034" Y="3.68707042408" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.331762312921" Y="4.616258367769" />
                  <Point X="2.74124559103" Y="3.769355372775" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.735092071531" Y="4.484743572074" />
                  <Point X="2.788752821719" Y="3.85164032147" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="0.001626220703" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.605792175293" Y="-4.038151611328" />
                  <Point X="0.471539031982" Y="-3.537112304688" />
                  <Point X="0.464318664551" Y="-3.521544189453" />
                  <Point X="0.409446044922" Y="-3.442483154297" />
                  <Point X="0.300591033936" Y="-3.285643798828" />
                  <Point X="0.274335754395" Y="-3.266399902344" />
                  <Point X="0.189423522949" Y="-3.240046142578" />
                  <Point X="0.020976625443" Y="-3.187766601562" />
                  <Point X="-0.008664756775" Y="-3.187766601562" />
                  <Point X="-0.09357698822" Y="-3.214120117188" />
                  <Point X="-0.262023895264" Y="-3.266399902344" />
                  <Point X="-0.288279144287" Y="-3.285643798828" />
                  <Point X="-0.343151763916" Y="-3.364704589844" />
                  <Point X="-0.45200680542" Y="-3.521544189453" />
                  <Point X="-0.459227142334" Y="-3.537112304688" />
                  <Point X="-0.725555236816" Y="-4.5310625" />
                  <Point X="-0.84774395752" Y="-4.987077148438" />
                  <Point X="-0.90243927002" Y="-4.976460449219" />
                  <Point X="-1.100246948242" Y="-4.938065429688" />
                  <Point X="-1.197888671875" Y="-4.912942871094" />
                  <Point X="-1.351589599609" Y="-4.873396972656" />
                  <Point X="-1.325311157227" Y="-4.67379296875" />
                  <Point X="-1.309150146484" Y="-4.551038574219" />
                  <Point X="-1.309683227539" Y="-4.5347578125" />
                  <Point X="-1.32996862793" Y="-4.432775878906" />
                  <Point X="-1.370210693359" Y="-4.230466308594" />
                  <Point X="-1.386282714844" Y="-4.20262890625" />
                  <Point X="-1.464459106445" Y="-4.134069824219" />
                  <Point X="-1.619543457031" Y="-3.998063964844" />
                  <Point X="-1.649240722656" Y="-3.985762939453" />
                  <Point X="-1.752998168945" Y="-3.978962158203" />
                  <Point X="-1.958829833984" Y="-3.965471435547" />
                  <Point X="-1.989878540039" Y="-3.973791015625" />
                  <Point X="-2.076334960938" Y="-4.031559326172" />
                  <Point X="-2.247844970703" Y="-4.146159179688" />
                  <Point X="-2.259734130859" Y="-4.157294433594" />
                  <Point X="-2.409256835938" Y="-4.352155761719" />
                  <Point X="-2.457095214844" Y="-4.414500488281" />
                  <Point X="-2.565888916016" Y="-4.347138183594" />
                  <Point X="-2.855832275391" Y="-4.167612304688" />
                  <Point X="-2.991041259766" Y="-4.063505859375" />
                  <Point X="-3.228581054688" Y="-3.880608154297" />
                  <Point X="-2.755527832031" Y="-3.061256103516" />
                  <Point X="-2.506033691406" Y="-2.629119628906" />
                  <Point X="-2.499762695312" Y="-2.597593017578" />
                  <Point X="-2.513979980469" Y="-2.568763916016" />
                  <Point X="-2.531328613281" Y="-2.551415283203" />
                  <Point X="-2.560157470703" Y="-2.537198730469" />
                  <Point X="-2.59168359375" Y="-2.543469726562" />
                  <Point X="-3.448946289062" Y="-3.038410644531" />
                  <Point X="-3.842959472656" Y="-3.265894042969" />
                  <Point X="-3.932631347656" Y="-3.148083496094" />
                  <Point X="-4.161703613281" Y="-2.847129394531" />
                  <Point X="-4.258636230469" Y="-2.684588378906" />
                  <Point X="-4.431020019531" Y="-2.395527099609" />
                  <Point X="-3.600320800781" Y="-1.758109130859" />
                  <Point X="-3.163786865234" Y="-1.423144897461" />
                  <Point X="-3.152535644531" Y="-1.411081665039" />
                  <Point X="-3.143239990234" Y="-1.386044189453" />
                  <Point X="-3.138117431641" Y="-1.366266479492" />
                  <Point X="-3.136651855469" Y="-1.350051391602" />
                  <Point X="-3.148656738281" Y="-1.321068603516" />
                  <Point X="-3.170034423828" Y="-1.305415405273" />
                  <Point X="-3.187641601562" Y="-1.295052734375" />
                  <Point X="-3.219529296875" Y="-1.288571044922" />
                  <Point X="-4.3017421875" Y="-1.431047119141" />
                  <Point X="-4.803283203125" Y="-1.497076293945" />
                  <Point X="-4.837801269531" Y="-1.361939453125" />
                  <Point X="-4.927393066406" Y="-1.011191589355" />
                  <Point X="-4.9530390625" Y="-0.831878479004" />
                  <Point X="-4.998396484375" Y="-0.514742126465" />
                  <Point X="-4.054741699219" Y="-0.261890625" />
                  <Point X="-3.557463867188" Y="-0.128645431519" />
                  <Point X="-3.532594482422" Y="-0.114969413757" />
                  <Point X="-3.514142822266" Y="-0.102162895203" />
                  <Point X="-3.502324462891" Y="-0.090645370483" />
                  <Point X="-3.491798583984" Y="-0.065918296814" />
                  <Point X="-3.485647949219" Y="-0.046101043701" />
                  <Point X="-3.483400878906" Y="-0.031280042648" />
                  <Point X="-3.488748291016" Y="-0.006469421864" />
                  <Point X="-3.494898925781" Y="0.013347833633" />
                  <Point X="-3.502324462891" Y="0.028085302353" />
                  <Point X="-3.523444091797" Y="0.046058467865" />
                  <Point X="-3.541895751953" Y="0.05886498642" />
                  <Point X="-3.557463867188" Y="0.066085334778" />
                  <Point X="-4.543952148438" Y="0.330414031982" />
                  <Point X="-4.998186523438" Y="0.452125793457" />
                  <Point X="-4.975385742188" Y="0.606212768555" />
                  <Point X="-4.917645019531" Y="0.996418151855" />
                  <Point X="-4.866020019531" Y="1.186930541992" />
                  <Point X="-4.773516113281" Y="1.528298583984" />
                  <Point X="-4.109727539062" Y="1.440909057617" />
                  <Point X="-3.753266357422" Y="1.393980102539" />
                  <Point X="-3.731704833984" Y="1.395866699219" />
                  <Point X="-3.711118164062" Y="1.402357666016" />
                  <Point X="-3.670278808594" Y="1.41523425293" />
                  <Point X="-3.651534423828" Y="1.426056152344" />
                  <Point X="-3.639119873047" Y="1.443785888672" />
                  <Point X="-3.630859375" Y="1.463728515625" />
                  <Point X="-3.614472412109" Y="1.503290161133" />
                  <Point X="-3.610714111328" Y="1.52460534668" />
                  <Point X="-3.616315917969" Y="1.54551184082" />
                  <Point X="-3.626282958984" Y="1.564658813477" />
                  <Point X="-3.646055664062" Y="1.602641601562" />
                  <Point X="-3.659968261719" Y="1.619221801758" />
                  <Point X="-4.2258203125" Y="2.053415283203" />
                  <Point X="-4.47610546875" Y="2.245466064453" />
                  <Point X="-4.384373535156" Y="2.402625244141" />
                  <Point X="-4.160014648438" Y="2.787006591797" />
                  <Point X="-4.023262451172" Y="2.962781982422" />
                  <Point X="-3.774671142578" Y="3.282310791016" />
                  <Point X="-3.378085449219" Y="3.053341796875" />
                  <Point X="-3.159156982422" Y="2.926943359375" />
                  <Point X="-3.138514648438" Y="2.920434814453" />
                  <Point X="-3.109843017578" Y="2.917926269531" />
                  <Point X="-3.052965332031" Y="2.912950195312" />
                  <Point X="-3.031506591797" Y="2.915775390625" />
                  <Point X="-3.013252441406" Y="2.927404785156" />
                  <Point X="-2.992901123047" Y="2.947755859375" />
                  <Point X="-2.952528808594" Y="2.988128173828" />
                  <Point X="-2.940899414062" Y="3.006382324219" />
                  <Point X="-2.93807421875" Y="3.027841064453" />
                  <Point X="-2.940582763672" Y="3.056512695312" />
                  <Point X="-2.945558837891" Y="3.113390625" />
                  <Point X="-2.952067382812" Y="3.134032958984" />
                  <Point X="-3.202813720703" Y="3.568337890625" />
                  <Point X="-3.307278564453" Y="3.749276611328" />
                  <Point X="-3.143638671875" Y="3.874737792969" />
                  <Point X="-2.752876220703" Y="4.174331542969" />
                  <Point X="-2.537496337891" Y="4.293991699219" />
                  <Point X="-2.141548583984" Y="4.513972167969" />
                  <Point X="-2.03484753418" Y="4.374916503906" />
                  <Point X="-1.967826904297" Y="4.287573242188" />
                  <Point X="-1.951246948242" Y="4.273660644531" />
                  <Point X="-1.919335693359" Y="4.257048828125" />
                  <Point X="-1.856030761719" Y="4.22409375" />
                  <Point X="-1.835124267578" Y="4.2184921875" />
                  <Point X="-1.813808959961" Y="4.222250488281" />
                  <Point X="-1.780571289062" Y="4.236018554688" />
                  <Point X="-1.714635009766" Y="4.263330078125" />
                  <Point X="-1.696905395508" Y="4.275744628906" />
                  <Point X="-1.686083496094" Y="4.294488769531" />
                  <Point X="-1.675265136719" Y="4.328799804688" />
                  <Point X="-1.653804077148" Y="4.396865234375" />
                  <Point X="-1.651917602539" Y="4.418426757812" />
                  <Point X="-1.680424682617" Y="4.634959472656" />
                  <Point X="-1.689137573242" Y="4.701141113281" />
                  <Point X="-1.473960327148" Y="4.761469238281" />
                  <Point X="-0.968094177246" Y="4.903296386719" />
                  <Point X="-0.706989746094" Y="4.933854980469" />
                  <Point X="-0.224199630737" Y="4.990358398438" />
                  <Point X="-0.106905418396" Y="4.552609863281" />
                  <Point X="-0.042140380859" Y="4.310903320312" />
                  <Point X="-0.024282114029" Y="4.284176757813" />
                  <Point X="0.006155934334" Y="4.273844238281" />
                  <Point X="0.036594036102" Y="4.284176757813" />
                  <Point X="0.054452251434" Y="4.310903320312" />
                  <Point X="0.182931808472" Y="4.790395507812" />
                  <Point X="0.236648406982" Y="4.990868652344" />
                  <Point X="0.418510375977" Y="4.971822753906" />
                  <Point X="0.860205749512" Y="4.925565429688" />
                  <Point X="1.076225463867" Y="4.873411621094" />
                  <Point X="1.508455932617" Y="4.769058105469" />
                  <Point X="1.64905065918" Y="4.718063476562" />
                  <Point X="1.931033691406" Y="4.615786132813" />
                  <Point X="2.066995849609" Y="4.552201171875" />
                  <Point X="2.33869921875" Y="4.425134277344" />
                  <Point X="2.470047851562" Y="4.348610351562" />
                  <Point X="2.7325234375" Y="4.19569140625" />
                  <Point X="2.856399414062" Y="4.10759765625" />
                  <Point X="3.068740234375" Y="3.956592773438" />
                  <Point X="2.518624267578" Y="3.003763671875" />
                  <Point X="2.229853515625" Y="2.503597900391" />
                  <Point X="2.224852050781" Y="2.491514648438" />
                  <Point X="2.217656494141" Y="2.464606933594" />
                  <Point X="2.203382324219" Y="2.411228271484" />
                  <Point X="2.202044677734" Y="2.392325927734" />
                  <Point X="2.204850341797" Y="2.36905859375" />
                  <Point X="2.210416015625" Y="2.322901123047" />
                  <Point X="2.218682128906" Y="2.3008125" />
                  <Point X="2.233079345703" Y="2.279594970703" />
                  <Point X="2.261639892578" Y="2.23750390625" />
                  <Point X="2.274940185547" Y="2.224203613281" />
                  <Point X="2.296157714844" Y="2.209806396484" />
                  <Point X="2.338248779297" Y="2.18124609375" />
                  <Point X="2.360336914062" Y="2.172980224609" />
                  <Point X="2.383604248047" Y="2.170174560547" />
                  <Point X="2.42976171875" Y="2.164608642578" />
                  <Point X="2.448664306641" Y="2.165946289062" />
                  <Point X="2.475572021484" Y="2.173141845703" />
                  <Point X="2.528950683594" Y="2.187416015625" />
                  <Point X="2.541033935547" Y="2.192417480469" />
                  <Point X="3.533251220703" Y="2.765274414062" />
                  <Point X="3.994248046875" Y="3.031430908203" />
                  <Point X="4.046900146484" Y="2.958256347656" />
                  <Point X="4.202591796875" Y="2.741880126953" />
                  <Point X="4.271650878906" Y="2.627759521484" />
                  <Point X="4.387512695312" Y="2.436296142578" />
                  <Point X="3.668810058594" Y="1.884815795898" />
                  <Point X="3.288616210938" Y="1.593082763672" />
                  <Point X="3.279371337891" Y="1.583833374023" />
                  <Point X="3.260005859375" Y="1.558569580078" />
                  <Point X="3.221589111328" Y="1.508451904297" />
                  <Point X="3.213119628906" Y="1.4915" />
                  <Point X="3.205906005859" Y="1.465705688477" />
                  <Point X="3.191595703125" Y="1.414535522461" />
                  <Point X="3.190779541016" Y="1.390965332031" />
                  <Point X="3.196701171875" Y="1.362265869141" />
                  <Point X="3.208448486328" Y="1.305332519531" />
                  <Point X="3.215646484375" Y="1.287954956055" />
                  <Point X="3.231752685547" Y="1.263474121094" />
                  <Point X="3.263704101562" Y="1.214909667969" />
                  <Point X="3.280947998047" Y="1.198819824219" />
                  <Point X="3.304288085938" Y="1.185681274414" />
                  <Point X="3.350590087891" Y="1.159617431641" />
                  <Point X="3.368565429688" Y="1.153619628906" />
                  <Point X="3.400122802734" Y="1.149448852539" />
                  <Point X="3.462726074219" Y="1.141175048828" />
                  <Point X="3.475803710938" Y="1.14117175293" />
                  <Point X="4.41834375" Y="1.265259521484" />
                  <Point X="4.848975097656" Y="1.32195324707" />
                  <Point X="4.872319824219" Y="1.226061157227" />
                  <Point X="4.939188476562" Y="0.951385925293" />
                  <Point X="4.960950683594" Y="0.811608215332" />
                  <Point X="4.997858398438" Y="0.574556152344" />
                  <Point X="4.176877929688" Y="0.35457510376" />
                  <Point X="3.741167724609" Y="0.237826919556" />
                  <Point X="3.729086914062" Y="0.232819412231" />
                  <Point X="3.698082519531" Y="0.214898284912" />
                  <Point X="3.636576904297" Y="0.179346878052" />
                  <Point X="3.622264892578" Y="0.166926574707" />
                  <Point X="3.603662353516" Y="0.143222442627" />
                  <Point X="3.566759033203" Y="0.096198890686" />
                  <Point X="3.556985351562" Y="0.074735275269" />
                  <Point X="3.550784423828" Y="0.042356563568" />
                  <Point X="3.538483154297" Y="-0.021875303268" />
                  <Point X="3.538483154297" Y="-0.04068478775" />
                  <Point X="3.544684082031" Y="-0.073063354492" />
                  <Point X="3.556985351562" Y="-0.13729536438" />
                  <Point X="3.566759033203" Y="-0.158758987427" />
                  <Point X="3.585361572266" Y="-0.182463119507" />
                  <Point X="3.622264892578" Y="-0.229486679077" />
                  <Point X="3.636576904297" Y="-0.241906967163" />
                  <Point X="3.667581298828" Y="-0.259828094482" />
                  <Point X="3.729086914062" Y="-0.295379486084" />
                  <Point X="3.741167724609" Y="-0.300387023926" />
                  <Point X="4.605520019531" Y="-0.531989379883" />
                  <Point X="4.998067871094" Y="-0.637172363281" />
                  <Point X="4.985768066406" Y="-0.718754577637" />
                  <Point X="4.948431640625" Y="-0.966399169922" />
                  <Point X="4.920550292969" Y="-1.088579711914" />
                  <Point X="4.874545898438" Y="-1.290178344727" />
                  <Point X="3.917283691406" Y="-1.16415234375" />
                  <Point X="3.411982177734" Y="-1.097628295898" />
                  <Point X="3.3948359375" Y="-1.098341308594" />
                  <Point X="3.333985351562" Y="-1.111567382812" />
                  <Point X="3.213271728516" Y="-1.137805053711" />
                  <Point X="3.1854453125" Y="-1.154697265625" />
                  <Point X="3.148665039062" Y="-1.198932495117" />
                  <Point X="3.075701171875" Y="-1.286685424805" />
                  <Point X="3.064357910156" Y="-1.314070556641" />
                  <Point X="3.059086425781" Y="-1.371357055664" />
                  <Point X="3.04862890625" Y="-1.485000854492" />
                  <Point X="3.056360595703" Y="-1.516621826172" />
                  <Point X="3.090036132812" Y="-1.569001953125" />
                  <Point X="3.156840820312" Y="-1.672912353516" />
                  <Point X="3.168460693359" Y="-1.685540649414" />
                  <Point X="3.970584716797" Y="-2.301032226562" />
                  <Point X="4.33907421875" Y="-2.583784179688" />
                  <Point X="4.309380371094" Y="-2.631833740234" />
                  <Point X="4.204133300781" Y="-2.802139404297" />
                  <Point X="4.146470703125" Y="-2.884069580078" />
                  <Point X="4.056688476562" Y="-3.011638427734" />
                  <Point X="3.203493652344" Y="-2.519045898438" />
                  <Point X="2.753454589844" Y="-2.259215576172" />
                  <Point X="2.737340820313" Y="-2.253312744141" />
                  <Point X="2.664918945312" Y="-2.240233398438" />
                  <Point X="2.521250488281" Y="-2.214287109375" />
                  <Point X="2.489077636719" Y="-2.219244873047" />
                  <Point X="2.428912841797" Y="-2.250909179688" />
                  <Point X="2.309559570312" Y="-2.313723876953" />
                  <Point X="2.288599853516" Y="-2.33468359375" />
                  <Point X="2.256935546875" Y="-2.394848388672" />
                  <Point X="2.194120849609" Y="-2.514201660156" />
                  <Point X="2.189163085938" Y="-2.546374511719" />
                  <Point X="2.2022421875" Y="-2.618796142578" />
                  <Point X="2.228188720703" Y="-2.76246484375" />
                  <Point X="2.234091552734" Y="-2.778578613281" />
                  <Point X="2.749537109375" Y="-3.671355957031" />
                  <Point X="2.986673583984" Y="-4.082088623047" />
                  <Point X="2.960184082031" Y="-4.101009277344" />
                  <Point X="2.835296875" Y="-4.190213378906" />
                  <Point X="2.770830810547" Y="-4.231940917969" />
                  <Point X="2.679775878906" Y="-4.290879394531" />
                  <Point X="2.026598876953" Y="-3.439642578125" />
                  <Point X="1.683177490234" Y="-2.992087402344" />
                  <Point X="1.670549316406" Y="-2.980467529297" />
                  <Point X="1.599121948242" Y="-2.934546142578" />
                  <Point X="1.45742590332" Y="-2.843448730469" />
                  <Point X="1.4258046875" Y="-2.835717041016" />
                  <Point X="1.347686645508" Y="-2.842905517578" />
                  <Point X="1.192717773438" Y="-2.857166015625" />
                  <Point X="1.165332885742" Y="-2.868509277344" />
                  <Point X="1.105012084961" Y="-2.918663818359" />
                  <Point X="0.985349060059" Y="-3.018159667969" />
                  <Point X="0.968456726074" Y="-3.045986328125" />
                  <Point X="0.950421203613" Y="-3.128964355469" />
                  <Point X="0.91464251709" Y="-3.29357421875" />
                  <Point X="0.91392956543" Y="-3.310720214844" />
                  <Point X="1.060002807617" Y="-4.420256835938" />
                  <Point X="1.127642211914" Y="-4.934028320312" />
                  <Point X="1.112497802734" Y="-4.937348144531" />
                  <Point X="0.994346313477" Y="-4.963246582031" />
                  <Point X="0.934788391113" Y="-4.97406640625" />
                  <Point X="0.860200378418" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#150" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.06476507889" Y="4.59683105088" Z="0.85" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.85" />
                  <Point X="-0.718957857503" Y="5.014795969364" Z="0.85" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.85" />
                  <Point X="-1.493614076544" Y="4.840892876226" Z="0.85" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.85" />
                  <Point X="-1.736152903915" Y="4.659713024202" Z="0.85" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.85" />
                  <Point X="-1.729106894088" Y="4.375115128102" Z="0.85" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.85" />
                  <Point X="-1.805861344817" Y="4.313492202684" Z="0.85" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.85" />
                  <Point X="-1.90240392213" Y="4.332679244185" Z="0.85" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.85" />
                  <Point X="-2.001335805635" Y="4.436634312762" Z="0.85" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.85" />
                  <Point X="-2.567935144678" Y="4.368979430084" Z="0.85" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.85" />
                  <Point X="-3.180216675305" Y="3.945697858686" Z="0.85" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.85" />
                  <Point X="-3.252270888989" Y="3.574618046943" Z="0.85" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.85" />
                  <Point X="-2.996548468626" Y="3.083435051646" Z="0.85" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.85" />
                  <Point X="-3.034412322705" Y="3.014391245181" Z="0.85" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.85" />
                  <Point X="-3.111641413961" Y="2.999016230547" Z="0.85" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.85" />
                  <Point X="-3.359241301354" Y="3.127923059757" Z="0.85" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.85" />
                  <Point X="-4.068881907746" Y="3.024764295656" Z="0.85" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.85" />
                  <Point X="-4.433711762955" Y="2.459110406031" Z="0.85" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.85" />
                  <Point X="-4.262414396013" Y="2.045027664976" Z="0.85" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.85" />
                  <Point X="-3.676789617806" Y="1.572851166465" Z="0.85" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.85" />
                  <Point X="-3.68320941337" Y="1.514142709315" Z="0.85" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.85" />
                  <Point X="-3.732309357755" Y="1.48132349399" Z="0.85" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.85" />
                  <Point X="-4.109356951002" Y="1.521761514183" Z="0.85" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.85" />
                  <Point X="-4.920435814624" Y="1.231287993596" Z="0.85" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.85" />
                  <Point X="-5.031003053085" Y="0.644811818872" Z="0.85" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.85" />
                  <Point X="-4.563049190107" Y="0.313397820839" Z="0.85" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.85" />
                  <Point X="-3.558109115856" Y="0.03626255419" Z="0.85" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.85" />
                  <Point X="-3.542657213796" Y="0.009989670713" Z="0.85" />
                  <Point X="-3.539556741714" Y="0" Z="0.85" />
                  <Point X="-3.545707387339" Y="-0.019817280348" Z="0.85" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.85" />
                  <Point X="-3.567259479267" Y="-0.042613428943" Z="0.85" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.85" />
                  <Point X="-4.073838598666" Y="-0.182314235917" Z="0.85" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.85" />
                  <Point X="-5.008690923142" Y="-0.807677398767" Z="0.85" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.85" />
                  <Point X="-4.892402164627" Y="-1.343033623139" Z="0.85" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.85" />
                  <Point X="-4.301371817419" Y="-1.44933931572" Z="0.85" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.85" />
                  <Point X="-3.201551828604" Y="-1.317226056841" Z="0.85" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.85" />
                  <Point X="-3.197798673519" Y="-1.342227468917" Z="0.85" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.85" />
                  <Point X="-3.63691467826" Y="-1.687161380421" Z="0.85" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.85" />
                  <Point X="-4.307735456737" Y="-2.678917876593" Z="0.85" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.85" />
                  <Point X="-3.97857330768" Y="-3.147086560516" Z="0.85" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.85" />
                  <Point X="-3.430102257471" Y="-3.050431848384" Z="0.85" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.85" />
                  <Point X="-2.561305254696" Y="-2.567025214436" Z="0.85" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.85" />
                  <Point X="-2.804985164784" Y="-3.004976125021" Z="0.85" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.85" />
                  <Point X="-3.027701085864" Y="-4.071843033646" Z="0.85" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.85" />
                  <Point X="-2.598366605909" Y="-4.358368736094" Z="0.85" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.85" />
                  <Point X="-2.375745007594" Y="-4.351313927576" Z="0.85" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.85" />
                  <Point X="-2.05471227956" Y="-4.041852694068" Z="0.85" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.85" />
                  <Point X="-1.762424312759" Y="-3.997575303746" Z="0.85" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.85" />
                  <Point X="-1.503582350718" Y="-4.140381574765" Z="0.85" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.85" />
                  <Point X="-1.385164046266" Y="-4.411250175723" Z="0.85" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.85" />
                  <Point X="-1.38103943237" Y="-4.63598637878" Z="0.85" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.85" />
                  <Point X="-1.216503479886" Y="-4.93008558148" Z="0.85" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.85" />
                  <Point X="-0.918122552761" Y="-4.994264383855" Z="0.85" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.85" />
                  <Point X="-0.683414990336" Y="-4.512723562155" Z="0.85" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.85" />
                  <Point X="-0.30823171002" Y="-3.361933676173" Z="0.85" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.85" />
                  <Point X="-0.084912199497" Y="-3.23059298161" Z="0.85" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.85" />
                  <Point X="0.168446879864" Y="-3.256519063678" Z="0.85" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.85" />
                  <Point X="0.362214149412" Y="-3.439712079595" Z="0.85" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.85" />
                  <Point X="0.551339870065" Y="-4.01981246766" Z="0.85" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.85" />
                  <Point X="0.93756929113" Y="-4.991981276846" Z="0.85" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.85" />
                  <Point X="1.117047549347" Y="-4.954908449519" Z="0.85" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.85" />
                  <Point X="1.103419060024" Y="-4.382450251299" Z="0.85" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.85" />
                  <Point X="0.993124408871" Y="-3.108303953805" Z="0.85" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.85" />
                  <Point X="1.130822980883" Y="-2.925830182279" Z="0.85" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.85" />
                  <Point X="1.346112455361" Y="-2.861415078711" Z="0.85" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.85" />
                  <Point X="1.565926497834" Y="-2.945324051681" Z="0.85" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.85" />
                  <Point X="1.980775247744" Y="-3.438800596198" Z="0.85" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.85" />
                  <Point X="2.791844125008" Y="-4.242635228706" Z="0.85" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.85" />
                  <Point X="2.98308996308" Y="-4.110416290426" Z="0.85" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.85" />
                  <Point X="2.786682304073" Y="-3.615075882917" Z="0.85" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.85" />
                  <Point X="2.245290435516" Y="-2.57862995786" Z="0.85" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.85" />
                  <Point X="2.29502594067" Y="-2.386854872922" Z="0.85" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.85" />
                  <Point X="2.446043505451" Y="-2.263875369297" Z="0.85" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.85" />
                  <Point X="2.649876845653" Y="-2.258157573975" Z="0.85" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.85" />
                  <Point X="3.17233771419" Y="-2.531067196201" Z="0.85" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.85" />
                  <Point X="4.181203378329" Y="-2.881566972815" Z="0.85" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.85" />
                  <Point X="4.345757404207" Y="-2.626838861257" Z="0.85" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.85" />
                  <Point X="3.994866778036" Y="-2.230084468654" Z="0.85" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.85" />
                  <Point X="3.12593805044" Y="-1.510682593318" Z="0.85" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.85" />
                  <Point X="3.102719967264" Y="-1.344658770871" Z="0.85" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.85" />
                  <Point X="3.180955277946" Y="-1.199619406464" Z="0.85" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.85" />
                  <Point X="3.338449359837" Y="-1.129146532404" Z="0.85" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.85" />
                  <Point X="3.904601299162" Y="-1.18244461594" Z="0.85" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.85" />
                  <Point X="4.963141487137" Y="-1.068423666151" Z="0.85" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.85" />
                  <Point X="5.029053671714" Y="-0.69492855952" Z="0.85" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.85" />
                  <Point X="4.612304821916" Y="-0.452413079377" Z="0.85" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.85" />
                  <Point X="3.686447035985" Y="-0.185259260811" Z="0.85" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.85" />
                  <Point X="3.618539316485" Y="-0.120314605928" Z="0.85" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.85" />
                  <Point X="3.587635590974" Y="-0.032378601655" Z="0.85" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.85" />
                  <Point X="3.59373585945" Y="0.064231929569" Z="0.85" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.85" />
                  <Point X="3.636840121915" Y="0.143634132647" Z="0.85" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.85" />
                  <Point X="3.716948378367" Y="0.202889583431" Z="0.85" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.85" />
                  <Point X="4.183662995391" Y="0.337558853392" Z="0.85" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.85" />
                  <Point X="5.004199732881" Y="0.850580359781" Z="0.85" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.85" />
                  <Point X="4.914744911502" Y="1.26916795898" Z="0.85" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.85" />
                  <Point X="4.405661490894" Y="1.346111876508" Z="0.85" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.85" />
                  <Point X="3.400518202178" Y="1.230297860237" Z="0.85" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.85" />
                  <Point X="3.322658789508" Y="1.260532498886" Z="0.85" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.85" />
                  <Point X="3.267367277928" Y="1.322235563184" Z="0.85" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.85" />
                  <Point X="3.239513709731" Y="1.403649742164" Z="0.85" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.85" />
                  <Point X="3.247902326301" Y="1.483519384148" Z="0.85" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.85" />
                  <Point X="3.293532713641" Y="1.559431351253" Z="0.85" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.85" />
                  <Point X="3.693092009895" Y="1.876428093179" Z="0.85" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.85" />
                  <Point X="4.308272287284" Y="2.684925720953" Z="0.85" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.85" />
                  <Point X="4.081329672193" Y="3.01873929263" Z="0.85" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.85" />
                  <Point X="3.502095373941" Y="2.839855749216" Z="0.85" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.85" />
                  <Point X="2.456499249959" Y="2.252724616044" Z="0.85" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.85" />
                  <Point X="2.383434232177" Y="2.251094905158" Z="0.85" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.85" />
                  <Point X="2.318075714535" Y="2.28246103712" Z="0.85" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.85" />
                  <Point X="2.268297563115" Y="2.338949145847" Z="0.85" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.85" />
                  <Point X="2.248334797601" Y="2.406324211655" Z="0.85" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.85" />
                  <Point X="2.259803326613" Y="2.482970325923" Z="0.85" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.85" />
                  <Point X="2.555769603643" Y="3.010043708014" Z="0.85" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.85" />
                  <Point X="2.879220615519" Y="4.179624908539" Z="0.85" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.85" />
                  <Point X="2.48906192472" Y="4.423092977446" Z="0.85" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.85" />
                  <Point X="2.082021263578" Y="4.628773126515" Z="0.85" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.85" />
                  <Point X="1.659943201527" Y="4.79634727604" Z="0.85" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.85" />
                  <Point X="1.08180389729" Y="4.953295411807" Z="0.85" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.85" />
                  <Point X="0.417562287376" Y="5.052830975473" Z="0.85" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.85" />
                  <Point X="0.128479482663" Y="4.834616455071" Z="0.85" />
                  <Point X="0" Y="4.355124473572" Z="0.85" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>