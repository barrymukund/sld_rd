<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#123" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="471" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="0" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000475830078" Y="0.004715820312" />
                  <Width Value="9.783896972656" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.587158630371" Y="-3.60155859375" />
                  <Point X="0.563302062988" Y="-3.512524414063" />
                  <Point X="0.553965698242" Y="-3.489880371094" />
                  <Point X="0.536994506836" Y="-3.460263183594" />
                  <Point X="0.532612304688" Y="-3.453328125" />
                  <Point X="0.378635467529" Y="-3.231476806641" />
                  <Point X="0.357109161377" Y="-3.207521728516" />
                  <Point X="0.324807403564" Y="-3.185802001953" />
                  <Point X="0.294980895996" Y="-3.173693603516" />
                  <Point X="0.28740625" Y="-3.170986083984" />
                  <Point X="0.049136131287" Y="-3.097035888672" />
                  <Point X="0.020396741867" Y="-3.091593017578" />
                  <Point X="-0.015287060738" Y="-3.092971191406" />
                  <Point X="-0.045196754456" Y="-3.099901123047" />
                  <Point X="-0.05191317749" Y="-3.10171875" />
                  <Point X="-0.290183441162" Y="-3.175669189453" />
                  <Point X="-0.319507995605" Y="-3.188986083984" />
                  <Point X="-0.350142181396" Y="-3.213490966797" />
                  <Point X="-0.371410766602" Y="-3.239358398438" />
                  <Point X="-0.376074371338" Y="-3.245525878906" />
                  <Point X="-0.530051147461" Y="-3.467377197266" />
                  <Point X="-0.538188964844" Y="-3.481573730469" />
                  <Point X="-0.55099005127" Y="-3.512524414063" />
                  <Point X="-0.916584533691" Y="-4.876941894531" />
                  <Point X="-1.079342041016" Y="-4.845350097656" />
                  <Point X="-1.092724243164" Y="-4.841906738281" />
                  <Point X="-1.24641809082" Y="-4.802362792969" />
                  <Point X="-1.217834960938" Y="-4.585251953125" />
                  <Point X="-1.214962890625" Y="-4.563438476562" />
                  <Point X="-1.214829956055" Y="-4.539694335938" />
                  <Point X="-1.218967773438" Y="-4.505291503906" />
                  <Point X="-1.220113525391" Y="-4.498102050781" />
                  <Point X="-1.277036132812" Y="-4.211932617188" />
                  <Point X="-1.287037719727" Y="-4.181756835937" />
                  <Point X="-1.308025146484" Y="-4.148802246094" />
                  <Point X="-1.332250976563" Y="-4.12402734375" />
                  <Point X="-1.337536621094" Y="-4.119020996094" />
                  <Point X="-1.556905395508" Y="-3.926639160156" />
                  <Point X="-1.583210327148" Y="-3.908787597656" />
                  <Point X="-1.619922241211" Y="-3.895419433594" />
                  <Point X="-1.654223754883" Y="-3.890511962891" />
                  <Point X="-1.66146496582" Y="-3.8897578125" />
                  <Point X="-1.952616577148" Y="-3.870674804688" />
                  <Point X="-1.984352661133" Y="-3.872525634766" />
                  <Point X="-2.021616455078" Y="-3.884268798828" />
                  <Point X="-2.051817382813" Y="-3.901257080078" />
                  <Point X="-2.058020996094" Y="-3.905066894531" />
                  <Point X="-2.300624267578" Y="-4.067169677734" />
                  <Point X="-2.312784912109" Y="-4.076821044922" />
                  <Point X="-2.335102783203" Y="-4.099461914062" />
                  <Point X="-2.480149169922" Y="-4.288489746094" />
                  <Point X="-2.801705566406" Y="-4.089390380859" />
                  <Point X="-2.820243164062" Y="-4.0751171875" />
                  <Point X="-3.104721923828" Y="-3.856077880859" />
                  <Point X="-2.468096191406" Y="-2.753409912109" />
                  <Point X="-2.423761230469" Y="-2.676619628906" />
                  <Point X="-2.412859130859" Y="-2.647653320312" />
                  <Point X="-2.406588134766" Y="-2.616126953125" />
                  <Point X="-2.405664550781" Y="-2.584533691406" />
                  <Point X="-2.415156982422" Y="-2.554385986328" />
                  <Point X="-2.430407226562" Y="-2.524523925781" />
                  <Point X="-2.447837890625" Y="-2.500556152344" />
                  <Point X="-2.464153320312" Y="-2.484240722656" />
                  <Point X="-2.489310302734" Y="-2.466213134766" />
                  <Point X="-2.518139160156" Y="-2.45199609375" />
                  <Point X="-2.547756835938" Y="-2.443011474609" />
                  <Point X="-2.578690917969" Y="-2.444024169922" />
                  <Point X="-2.610217285156" Y="-2.450295166016" />
                  <Point X="-2.63918359375" Y="-2.461197265625" />
                  <Point X="-3.818024169922" Y="-3.141801269531" />
                  <Point X="-4.082859375" Y="-2.793862304688" />
                  <Point X="-4.096145507813" Y="-2.771583740234" />
                  <Point X="-4.306142578125" Y="-2.419450439453" />
                  <Point X="-3.183526367188" Y="-1.558036254883" />
                  <Point X="-3.105954589844" Y="-1.498513305664" />
                  <Point X="-3.083677978516" Y="-1.474208984375" />
                  <Point X="-3.065254394531" Y="-1.445306518555" />
                  <Point X="-3.053397949219" Y="-1.418061645508" />
                  <Point X="-3.046152099609" Y="-1.3900859375" />
                  <Point X="-3.04334765625" Y="-1.359657226562" />
                  <Point X="-3.045556396484" Y="-1.327986450195" />
                  <Point X="-3.052876220703" Y="-1.297480957031" />
                  <Point X="-3.069732421875" Y="-1.271022705078" />
                  <Point X="-3.092142333984" Y="-1.246137817383" />
                  <Point X="-3.114549804688" Y="-1.227838500977" />
                  <Point X="-3.139455322266" Y="-1.213180419922" />
                  <Point X="-3.168718261719" Y="-1.201956542969" />
                  <Point X="-3.200605957031" Y="-1.195474731445" />
                  <Point X="-3.231929199219" Y="-1.194383911133" />
                  <Point X="-4.732102050781" Y="-1.391885375977" />
                  <Point X="-4.834077636719" Y="-0.992654785156" />
                  <Point X="-4.837592285156" Y="-0.968080871582" />
                  <Point X="-4.892424316406" Y="-0.584698181152" />
                  <Point X="-3.621241943359" Y="-0.244085845947" />
                  <Point X="-3.532875976562" Y="-0.220408355713" />
                  <Point X="-3.5159453125" Y="-0.214092514038" />
                  <Point X="-3.486075683594" Y="-0.198322250366" />
                  <Point X="-3.459975585938" Y="-0.180207275391" />
                  <Point X="-3.436542236328" Y="-0.156963912964" />
                  <Point X="-3.416747314453" Y="-0.128933380127" />
                  <Point X="-3.4036171875" Y="-0.102291732788" />
                  <Point X="-3.394917236328" Y="-0.074259994507" />
                  <Point X="-3.390662597656" Y="-0.044435451508" />
                  <Point X="-3.391213378906" Y="-0.013018630028" />
                  <Point X="-3.395468261719" Y="0.013476083755" />
                  <Point X="-3.404168457031" Y="0.041507827759" />
                  <Point X="-3.419358154297" Y="0.070955169678" />
                  <Point X="-3.440254882812" Y="0.098357284546" />
                  <Point X="-3.461628417969" Y="0.118794494629" />
                  <Point X="-3.487728515625" Y="0.136909317017" />
                  <Point X="-3.501925292969" Y="0.145047103882" />
                  <Point X="-3.532875976562" Y="0.157848358154" />
                  <Point X="-4.891816894531" Y="0.521975402832" />
                  <Point X="-4.82448828125" Y="0.976971496582" />
                  <Point X="-4.817411132812" Y="1.003089355469" />
                  <Point X="-4.703551269531" Y="1.423267700195" />
                  <Point X="-3.829009277344" Y="1.308132202148" />
                  <Point X="-3.765666259766" Y="1.29979284668" />
                  <Point X="-3.744985839844" Y="1.299341674805" />
                  <Point X="-3.723424072266" Y="1.301228393555" />
                  <Point X="-3.703134521484" Y="1.305264648438" />
                  <Point X="-3.699476318359" Y="1.30641809082" />
                  <Point X="-3.641708496094" Y="1.324632080078" />
                  <Point X="-3.622778320312" Y="1.332962036133" />
                  <Point X="-3.604034179688" Y="1.343783935547" />
                  <Point X="-3.587353271484" Y="1.356015136719" />
                  <Point X="-3.57371484375" Y="1.371566650391" />
                  <Point X="-3.561300292969" Y="1.389296508789" />
                  <Point X="-3.551352783203" Y="1.407427612305" />
                  <Point X="-3.549884765625" Y="1.410971313477" />
                  <Point X="-3.526705322266" Y="1.466931762695" />
                  <Point X="-3.520915527344" Y="1.486794067383" />
                  <Point X="-3.517157226562" Y="1.508109008789" />
                  <Point X="-3.5158046875" Y="1.528749145508" />
                  <Point X="-3.518951171875" Y="1.549192871094" />
                  <Point X="-3.524552978516" Y="1.570099243164" />
                  <Point X="-3.53205078125" Y="1.589379516602" />
                  <Point X="-3.533822021484" Y="1.592781860352" />
                  <Point X="-3.561790527344" Y="1.646509155273" />
                  <Point X="-3.573281005859" Y="1.663705566406" />
                  <Point X="-3.587193359375" Y="1.680285888672" />
                  <Point X="-3.602135742188" Y="1.694590209961" />
                  <Point X="-4.351859863281" Y="2.269874023438" />
                  <Point X="-4.081153320312" Y="2.733660400391" />
                  <Point X="-4.062412353516" Y="2.757749023438" />
                  <Point X="-3.750505126953" Y="3.158661865234" />
                  <Point X="-3.245560302734" Y="2.867131835938" />
                  <Point X="-3.206656982422" Y="2.844670898438" />
                  <Point X="-3.187724121094" Y="2.836340332031" />
                  <Point X="-3.167081787109" Y="2.829831787109" />
                  <Point X="-3.146795410156" Y="2.825796386719" />
                  <Point X="-3.141700439453" Y="2.825350585938" />
                  <Point X="-3.06124609375" Y="2.818311767578" />
                  <Point X="-3.040564697266" Y="2.818762939453" />
                  <Point X="-3.019106201172" Y="2.821588134766" />
                  <Point X="-2.999014892578" Y="2.826504638672" />
                  <Point X="-2.980463867188" Y="2.835652832031" />
                  <Point X="-2.962209472656" Y="2.847281982422" />
                  <Point X="-2.946077392578" Y="2.860229492188" />
                  <Point X="-2.9424609375" Y="2.863845947266" />
                  <Point X="-2.885353759766" Y="2.920952880859" />
                  <Point X="-2.872406738281" Y="2.937084228516" />
                  <Point X="-2.86077734375" Y="2.955338623047" />
                  <Point X="-2.851628662109" Y="2.973890136719" />
                  <Point X="-2.846712158203" Y="2.993981933594" />
                  <Point X="-2.843886962891" Y="3.015440673828" />
                  <Point X="-2.843435791016" Y="3.036121826172" />
                  <Point X="-2.843881591797" Y="3.041216796875" />
                  <Point X="-2.850920410156" Y="3.121671386719" />
                  <Point X="-2.854955810547" Y="3.141958251953" />
                  <Point X="-2.861464355469" Y="3.162600341797" />
                  <Point X="-2.869794921875" Y="3.181532714844" />
                  <Point X="-3.183333007812" Y="3.724596679688" />
                  <Point X="-2.700625732422" Y="4.094683349609" />
                  <Point X="-2.671109863281" Y="4.11108203125" />
                  <Point X="-2.167036865234" Y="4.391134277344" />
                  <Point X="-2.055104980469" Y="4.24526171875" />
                  <Point X="-2.043195678711" Y="4.229740722656" />
                  <Point X="-2.028892578125" Y="4.214799804688" />
                  <Point X="-2.0123125" Y="4.200887207031" />
                  <Point X="-1.995109619141" Y="4.189392578125" />
                  <Point X="-1.989438964844" Y="4.186440917969" />
                  <Point X="-1.899893554688" Y="4.139825683594" />
                  <Point X="-1.8806171875" Y="4.132330566406" />
                  <Point X="-1.859710571289" Y="4.126729003906" />
                  <Point X="-1.839267578125" Y="4.123582519531" />
                  <Point X="-1.818628295898" Y="4.124935546875" />
                  <Point X="-1.797312866211" Y="4.128693847656" />
                  <Point X="-1.777450439453" Y="4.134483398438" />
                  <Point X="-1.771544067383" Y="4.136930175781" />
                  <Point X="-1.678276489258" Y="4.175562988281" />
                  <Point X="-1.66014465332" Y="4.185510742187" />
                  <Point X="-1.642415039062" Y="4.197925292969" />
                  <Point X="-1.626863525391" Y="4.211563964844" />
                  <Point X="-1.614632568359" Y="4.228245117188" />
                  <Point X="-1.603810791016" Y="4.246989257813" />
                  <Point X="-1.59548059082" Y="4.265920898438" />
                  <Point X="-1.593558105469" Y="4.272018066406" />
                  <Point X="-1.563201049805" Y="4.368297363281" />
                  <Point X="-1.559165649414" Y="4.388584960938" />
                  <Point X="-1.557279174805" Y="4.410146484375" />
                  <Point X="-1.557730224609" Y="4.430826660156" />
                  <Point X="-1.584201904297" Y="4.631897949219" />
                  <Point X="-0.949635681152" Y="4.809808105469" />
                  <Point X="-0.913861755371" Y="4.813995117188" />
                  <Point X="-0.294710845947" Y="4.886457519531" />
                  <Point X="-0.145411972046" Y="4.329266601562" />
                  <Point X="-0.133903381348" Y="4.286315429688" />
                  <Point X="-0.121129760742" Y="4.258124023438" />
                  <Point X="-0.103271614075" Y="4.231397460938" />
                  <Point X="-0.082114547729" Y="4.20880859375" />
                  <Point X="-0.054819355011" Y="4.19421875" />
                  <Point X="-0.024381278992" Y="4.183886230469" />
                  <Point X="0.006155910969" Y="4.178844238281" />
                  <Point X="0.03669311142" Y="4.183886230469" />
                  <Point X="0.067131187439" Y="4.19421875" />
                  <Point X="0.094426490784" Y="4.20880859375" />
                  <Point X="0.115583602905" Y="4.231397460938" />
                  <Point X="0.133441741943" Y="4.258124023438" />
                  <Point X="0.146215209961" Y="4.286315429688" />
                  <Point X="0.307419372559" Y="4.8879375" />
                  <Point X="0.844043029785" Y="4.83173828125" />
                  <Point X="0.873645141602" Y="4.824591796875" />
                  <Point X="1.481026245117" Y="4.677951171875" />
                  <Point X="1.498899414062" Y="4.671468261719" />
                  <Point X="1.894642333984" Y="4.527929199219" />
                  <Point X="1.913283325195" Y="4.519211914062" />
                  <Point X="2.294575195312" Y="4.340894042969" />
                  <Point X="2.312613525391" Y="4.330385253906" />
                  <Point X="2.680979003906" Y="4.115774414062" />
                  <Point X="2.697961181641" Y="4.103697753906" />
                  <Point X="2.943260009766" Y="3.929254638672" />
                  <Point X="2.198895263672" Y="2.639977050781" />
                  <Point X="2.147581054688" Y="2.551097900391" />
                  <Point X="2.140825927734" Y="2.536750244141" />
                  <Point X="2.131798339844" Y="2.511275390625" />
                  <Point X="2.111607177734" Y="2.435770507813" />
                  <Point X="2.1084453125" Y="2.414686523438" />
                  <Point X="2.107606201172" Y="2.391649169922" />
                  <Point X="2.108226318359" Y="2.376818359375" />
                  <Point X="2.116099121094" Y="2.311528076172" />
                  <Point X="2.121442138672" Y="2.289604248047" />
                  <Point X="2.129708251953" Y="2.267515869141" />
                  <Point X="2.140071533203" Y="2.247470458984" />
                  <Point X="2.142629882812" Y="2.243700195312" />
                  <Point X="2.183029296875" Y="2.184161865234" />
                  <Point X="2.196980957031" Y="2.167903564453" />
                  <Point X="2.214051269531" Y="2.152045166016" />
                  <Point X="2.225369140625" Y="2.143034179688" />
                  <Point X="2.284907470703" Y="2.102635009766" />
                  <Point X="2.304951904297" Y="2.092272460938" />
                  <Point X="2.327040283203" Y="2.084006347656" />
                  <Point X="2.348958740234" Y="2.0786640625" />
                  <Point X="2.353093261719" Y="2.078165283203" />
                  <Point X="2.418383544922" Y="2.070292480469" />
                  <Point X="2.440193115234" Y="2.070183105469" />
                  <Point X="2.463877197266" Y="2.072799560547" />
                  <Point X="2.477987548828" Y="2.075449951172" />
                  <Point X="2.553492431641" Y="2.095640869141" />
                  <Point X="2.565283447266" Y="2.099638183594" />
                  <Point X="2.588533935547" Y="2.110145019531" />
                  <Point X="3.967326660156" Y="2.90619140625" />
                  <Point X="4.123269042969" Y="2.689466552734" />
                  <Point X="4.132739746094" Y="2.673816162109" />
                  <Point X="4.262198242188" Y="2.459884033203" />
                  <Point X="3.298343994141" Y="1.720291870117" />
                  <Point X="3.230783935547" Y="1.668451293945" />
                  <Point X="3.218820800781" Y="1.65753112793" />
                  <Point X="3.200532470703" Y="1.637138427734" />
                  <Point X="3.146191650391" Y="1.566246459961" />
                  <Point X="3.135047851562" Y="1.547638549805" />
                  <Point X="3.125296386719" Y="1.526102905273" />
                  <Point X="3.120347900391" Y="1.512502319336" />
                  <Point X="3.100105957031" Y="1.440121459961" />
                  <Point X="3.096652587891" Y="1.417823120117" />
                  <Point X="3.095836425781" Y="1.394252929688" />
                  <Point X="3.097739257812" Y="1.371769042969" />
                  <Point X="3.098791503906" Y="1.366668945312" />
                  <Point X="3.115408203125" Y="1.286136108398" />
                  <Point X="3.122240478516" Y="1.26541784668" />
                  <Point X="3.132300537109" Y="1.243690063477" />
                  <Point X="3.139144775391" Y="1.231390014648" />
                  <Point X="3.184340332031" Y="1.162694946289" />
                  <Point X="3.198893554688" Y="1.145450439453" />
                  <Point X="3.216137451172" Y="1.129360595703" />
                  <Point X="3.234346435547" Y="1.11603527832" />
                  <Point X="3.238493896484" Y="1.113700561523" />
                  <Point X="3.303988525391" Y="1.076832885742" />
                  <Point X="3.324488037109" Y="1.06827355957" />
                  <Point X="3.348071044922" Y="1.061534667969" />
                  <Point X="3.361725830078" Y="1.058697509766" />
                  <Point X="3.450278808594" Y="1.046994018555" />
                  <Point X="3.462702148438" Y="1.046175048828" />
                  <Point X="3.488203613281" Y="1.04698449707" />
                  <Point X="4.776839355469" Y="1.21663671875" />
                  <Point X="4.845936523438" Y="0.93280847168" />
                  <Point X="4.848919921875" Y="0.913645141602" />
                  <Point X="4.890864746094" Y="0.644238525391" />
                  <Point X="3.794005126953" Y="0.350335906982" />
                  <Point X="3.716579833984" Y="0.329589935303" />
                  <Point X="3.701061035156" Y="0.323945770264" />
                  <Point X="3.676036132812" Y="0.311883453369" />
                  <Point X="3.589035644531" Y="0.261595428467" />
                  <Point X="3.571360839844" Y="0.248425643921" />
                  <Point X="3.553743164062" Y="0.231793045044" />
                  <Point X="3.544225097656" Y="0.22136428833" />
                  <Point X="3.492024902344" Y="0.154848968506" />
                  <Point X="3.480301025391" Y="0.135569152832" />
                  <Point X="3.470527099609" Y="0.114105384827" />
                  <Point X="3.463681152344" Y="0.092606552124" />
                  <Point X="3.462579101562" Y="0.086852935791" />
                  <Point X="3.445178955078" Y="-0.004003949165" />
                  <Point X="3.443578369141" Y="-0.026133321762" />
                  <Point X="3.444680419922" Y="-0.050696350098" />
                  <Point X="3.446280517578" Y="-0.064307090759" />
                  <Point X="3.463680664062" Y="-0.155164138794" />
                  <Point X="3.470527099609" Y="-0.176665542603" />
                  <Point X="3.480301025391" Y="-0.198129318237" />
                  <Point X="3.492024658203" Y="-0.21740852356" />
                  <Point X="3.495330322266" Y="-0.221620864868" />
                  <Point X="3.547530517578" Y="-0.288136322021" />
                  <Point X="3.563500244141" Y="-0.304130645752" />
                  <Point X="3.583321777344" Y="-0.319735443115" />
                  <Point X="3.594545166016" Y="-0.327340148926" />
                  <Point X="3.681545654297" Y="-0.37762802124" />
                  <Point X="3.692710205078" Y="-0.383139068604" />
                  <Point X="3.716579833984" Y="-0.392149963379" />
                  <Point X="4.89147265625" Y="-0.706961486816" />
                  <Point X="4.855021972656" Y="-0.948727600098" />
                  <Point X="4.851199707031" Y="-0.96547845459" />
                  <Point X="4.801173828125" Y="-1.184698974609" />
                  <Point X="3.514173828125" Y="-1.015262207031" />
                  <Point X="3.424382080078" Y="-1.003441101074" />
                  <Point X="3.401635742188" Y="-1.003193359375" />
                  <Point X="3.373676757812" Y="-1.006256652832" />
                  <Point X="3.363845703125" Y="-1.007859069824" />
                  <Point X="3.193094482422" Y="-1.044972412109" />
                  <Point X="3.162724609375" Y="-1.055693359375" />
                  <Point X="3.131489501953" Y="-1.076036376953" />
                  <Point X="3.112453613281" Y="-1.094671020508" />
                  <Point X="3.105861572266" Y="-1.101820800781" />
                  <Point X="3.002653320312" Y="-1.225948364258" />
                  <Point X="2.986626220703" Y="-1.250417602539" />
                  <Point X="2.974509765625" Y="-1.282393432617" />
                  <Point X="2.969977294922" Y="-1.307125732422" />
                  <Point X="2.968820800781" Y="-1.315545288086" />
                  <Point X="2.954028564453" Y="-1.476295654297" />
                  <Point X="2.955109130859" Y="-1.508482543945" />
                  <Point X="2.965240966797" Y="-1.544660766602" />
                  <Point X="2.977736572266" Y="-1.569132202148" />
                  <Point X="2.982434570312" Y="-1.577304321289" />
                  <Point X="3.076930664062" Y="-1.724287109375" />
                  <Point X="3.086932373047" Y="-1.73723828125" />
                  <Point X="3.110628417969" Y="-1.760909057617" />
                  <Point X="4.213122558594" Y="-2.606883056641" />
                  <Point X="4.124812011719" Y="-2.749782714844" />
                  <Point X="4.116902832031" Y="-2.761020263672" />
                  <Point X="4.028981445312" Y="-2.885945068359" />
                  <Point X="2.880926269531" Y="-2.223114746094" />
                  <Point X="2.800954589844" Y="-2.176943115234" />
                  <Point X="2.779395263672" Y="-2.167825927734" />
                  <Point X="2.750412109375" Y="-2.159599121094" />
                  <Point X="2.741355224609" Y="-2.157501220703" />
                  <Point X="2.538134277344" Y="-2.120799560547" />
                  <Point X="2.505973388672" Y="-2.119082275391" />
                  <Point X="2.468744140625" Y="-2.126161865234" />
                  <Point X="2.442381103516" Y="-2.136958984375" />
                  <Point X="2.434141845703" Y="-2.140803466797" />
                  <Point X="2.265315185547" Y="-2.229655761719" />
                  <Point X="2.241143798828" Y="-2.246129394531" />
                  <Point X="2.217397216797" Y="-2.271526367188" />
                  <Point X="2.202879638672" Y="-2.294286132812" />
                  <Point X="2.198905029297" Y="-2.301130371094" />
                  <Point X="2.110052734375" Y="-2.469957275391" />
                  <Point X="2.098733642578" Y="-2.500108886719" />
                  <Point X="2.094302734375" Y="-2.538047607422" />
                  <Point X="2.096839355469" Y="-2.56741796875" />
                  <Point X="2.097999511719" Y="-2.576127441406" />
                  <Point X="2.134701171875" Y="-2.779348632812" />
                  <Point X="2.138985595703" Y="-2.795141845703" />
                  <Point X="2.151819091797" Y="-2.826078613281" />
                  <Point X="2.861283447266" Y="-4.05490625" />
                  <Point X="2.781846923828" Y="-4.111645996094" />
                  <Point X="2.773010498047" Y="-4.117365722656" />
                  <Point X="2.701765136719" Y="-4.163481445312" />
                  <Point X="1.819571533203" Y="-3.01378515625" />
                  <Point X="1.758546020508" Y="-2.934255126953" />
                  <Point X="1.74166015625" Y="-2.917222412109" />
                  <Point X="1.716339233398" Y="-2.897442382812" />
                  <Point X="1.709231445312" Y="-2.892397216797" />
                  <Point X="1.50880078125" Y="-2.763538574219" />
                  <Point X="1.479745361328" Y="-2.749644775391" />
                  <Point X="1.442084106445" Y="-2.741937744141" />
                  <Point X="1.411745239258" Y="-2.741994628906" />
                  <Point X="1.403217895508" Y="-2.742394042969" />
                  <Point X="1.184012695312" Y="-2.762565673828" />
                  <Point X="1.155377807617" Y="-2.76853515625" />
                  <Point X="1.123302978516" Y="-2.783204589844" />
                  <Point X="1.099751586914" Y="-2.799864501953" />
                  <Point X="1.093877075195" Y="-2.804373535156" />
                  <Point X="0.924612121582" Y="-2.945111572266" />
                  <Point X="0.902616027832" Y="-2.968636230469" />
                  <Point X="0.883835571289" Y="-3.002528320312" />
                  <Point X="0.874512634277" Y="-3.032600097656" />
                  <Point X="0.872419433594" Y="-3.040553955078" />
                  <Point X="0.821810058594" Y="-3.273396728516" />
                  <Point X="0.819724609375" Y="-3.289627441406" />
                  <Point X="0.819742370605" Y="-3.323120117188" />
                  <Point X="1.022065185547" Y="-4.859915039062" />
                  <Point X="0.975675231934" Y="-4.870083496094" />
                  <Point X="0.96750592041" Y="-4.871567871094" />
                  <Point X="0.929315612793" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058438110352" Y="-4.752634765625" />
                  <Point X="-1.069051025391" Y="-4.749903808594" />
                  <Point X="-1.141246459961" Y="-4.731328613281" />
                  <Point X="-1.123647583008" Y="-4.597651855469" />
                  <Point X="-1.120775634766" Y="-4.575838378906" />
                  <Point X="-1.119964355469" Y="-4.563970214844" />
                  <Point X="-1.119831420898" Y="-4.540226074219" />
                  <Point X="-1.120509765625" Y="-4.528350097656" />
                  <Point X="-1.124647583008" Y="-4.493947265625" />
                  <Point X="-1.126938842773" Y="-4.479568359375" />
                  <Point X="-1.183861572266" Y="-4.193398925781" />
                  <Point X="-1.186860229492" Y="-4.182044433594" />
                  <Point X="-1.196861816406" Y="-4.151868652344" />
                  <Point X="-1.206907836914" Y="-4.130725585938" />
                  <Point X="-1.227895263672" Y="-4.097770996094" />
                  <Point X="-1.2401015625" Y="-4.082384033203" />
                  <Point X="-1.264327392578" Y="-4.057609130859" />
                  <Point X="-1.274898681641" Y="-4.047596191406" />
                  <Point X="-1.494267456055" Y="-3.855214355469" />
                  <Point X="-1.503559204102" Y="-3.848031494141" />
                  <Point X="-1.529864013672" Y="-3.830179931641" />
                  <Point X="-1.550705322266" Y="-3.819521484375" />
                  <Point X="-1.587417236328" Y="-3.806153320312" />
                  <Point X="-1.606467773438" Y="-3.801376953125" />
                  <Point X="-1.640769287109" Y="-3.796469482422" />
                  <Point X="-1.655251708984" Y="-3.794961181641" />
                  <Point X="-1.946403198242" Y="-3.775878173828" />
                  <Point X="-1.958147583008" Y="-3.7758359375" />
                  <Point X="-1.989883666992" Y="-3.777686767578" />
                  <Point X="-2.01290625" Y="-3.781918212891" />
                  <Point X="-2.050170166016" Y="-3.793661376953" />
                  <Point X="-2.068191894531" Y="-3.801469482422" />
                  <Point X="-2.098392822266" Y="-3.818457763672" />
                  <Point X="-2.110800292969" Y="-3.826077392578" />
                  <Point X="-2.353403564453" Y="-3.988180175781" />
                  <Point X="-2.359681884766" Y="-3.992757324219" />
                  <Point X="-2.380440917969" Y="-4.010130126953" />
                  <Point X="-2.402758789062" Y="-4.032770996094" />
                  <Point X="-2.410471191406" Y="-4.041629638672" />
                  <Point X="-2.503202880859" Y="-4.162479492188" />
                  <Point X="-2.747580566406" Y="-4.011166992188" />
                  <Point X="-2.762286132812" Y="-3.999844482422" />
                  <Point X="-2.980862792969" Y="-3.831547607422" />
                  <Point X="-2.385823730469" Y="-2.800909912109" />
                  <Point X="-2.341488769531" Y="-2.724119628906" />
                  <Point X="-2.334850097656" Y="-2.710083251953" />
                  <Point X="-2.323947998047" Y="-2.681116943359" />
                  <Point X="-2.319684570312" Y="-2.666187011719" />
                  <Point X="-2.313413574219" Y="-2.634660644531" />
                  <Point X="-2.311628662109" Y="-2.618903076172" />
                  <Point X="-2.310705078125" Y="-2.587309814453" />
                  <Point X="-2.315050048828" Y="-2.556002441406" />
                  <Point X="-2.324542480469" Y="-2.525854736328" />
                  <Point X="-2.330551269531" Y="-2.511178710938" />
                  <Point X="-2.345801513672" Y="-2.481316650391" />
                  <Point X="-2.353576660156" Y="-2.4686484375" />
                  <Point X="-2.371007324219" Y="-2.444680664062" />
                  <Point X="-2.380662841797" Y="-2.433381103516" />
                  <Point X="-2.396978271484" Y="-2.417065673828" />
                  <Point X="-2.408817138672" Y="-2.407020751953" />
                  <Point X="-2.433974121094" Y="-2.388993164062" />
                  <Point X="-2.447292236328" Y="-2.381010498047" />
                  <Point X="-2.47612109375" Y="-2.366793457031" />
                  <Point X="-2.490561523438" Y="-2.361086914062" />
                  <Point X="-2.520179199219" Y="-2.352102294922" />
                  <Point X="-2.550865234375" Y="-2.348062255859" />
                  <Point X="-2.581799316406" Y="-2.349074951172" />
                  <Point X="-2.597224609375" Y="-2.350849609375" />
                  <Point X="-2.628750976562" Y="-2.357120605469" />
                  <Point X="-2.643680908203" Y="-2.361384033203" />
                  <Point X="-2.672647216797" Y="-2.372286132812" />
                  <Point X="-2.68668359375" Y="-2.378924804688" />
                  <Point X="-3.793089355469" Y="-3.017708496094" />
                  <Point X="-4.004014892578" Y="-2.740595703125" />
                  <Point X="-4.014552978516" Y="-2.722925048828" />
                  <Point X="-4.181265625" Y="-2.443373779297" />
                  <Point X="-3.125694091797" Y="-1.633404785156" />
                  <Point X="-3.048122314453" Y="-1.573881835938" />
                  <Point X="-3.035921630859" Y="-1.562703491211" />
                  <Point X="-3.013645019531" Y="-1.538399169922" />
                  <Point X="-3.003569091797" Y="-1.52527355957" />
                  <Point X="-2.985145507812" Y="-1.49637109375" />
                  <Point X="-2.978145507812" Y="-1.48321472168" />
                  <Point X="-2.9662890625" Y="-1.455969726562" />
                  <Point X="-2.961432617188" Y="-1.441881103516" />
                  <Point X="-2.954186767578" Y="-1.413905395508" />
                  <Point X="-2.951552978516" Y="-1.39880456543" />
                  <Point X="-2.948748535156" Y="-1.368375854492" />
                  <Point X="-2.948577880859" Y="-1.353047973633" />
                  <Point X="-2.950786621094" Y="-1.321377197266" />
                  <Point X="-2.953178466797" Y="-1.3058203125" />
                  <Point X="-2.960498291016" Y="-1.275314819336" />
                  <Point X="-2.972754638672" Y="-1.246436401367" />
                  <Point X="-2.989610839844" Y="-1.219978271484" />
                  <Point X="-2.999138427734" Y="-1.207449829102" />
                  <Point X="-3.021548339844" Y="-1.182565063477" />
                  <Point X="-3.032051757812" Y="-1.172557250977" />
                  <Point X="-3.054459228516" Y="-1.1542578125" />
                  <Point X="-3.066364013672" Y="-1.145965820312" />
                  <Point X="-3.09126953125" Y="-1.131307739258" />
                  <Point X="-3.105434326172" Y="-1.124481079102" />
                  <Point X="-3.134697265625" Y="-1.113257202148" />
                  <Point X="-3.149794677734" Y="-1.108860351562" />
                  <Point X="-3.181682373047" Y="-1.102378540039" />
                  <Point X="-3.197299560547" Y="-1.100532104492" />
                  <Point X="-3.228622802734" Y="-1.09944140625" />
                  <Point X="-3.244329101562" Y="-1.100196655273" />
                  <Point X="-4.660920898438" Y="-1.286694458008" />
                  <Point X="-4.740762207031" Y="-0.974118530273" />
                  <Point X="-4.743549316406" Y="-0.954630554199" />
                  <Point X="-4.786452148438" Y="-0.654654296875" />
                  <Point X="-3.596654052734" Y="-0.335848724365" />
                  <Point X="-3.508288085938" Y="-0.312171295166" />
                  <Point X="-3.499672119141" Y="-0.309416870117" />
                  <Point X="-3.471590576172" Y="-0.298102355957" />
                  <Point X="-3.441720947266" Y="-0.282332122803" />
                  <Point X="-3.431908447266" Y="-0.276366546631" />
                  <Point X="-3.405808349609" Y="-0.258251495361" />
                  <Point X="-3.393074462891" Y="-0.247655288696" />
                  <Point X="-3.369641113281" Y="-0.224412002563" />
                  <Point X="-3.358941650391" Y="-0.211764785767" />
                  <Point X="-3.339146728516" Y="-0.183734359741" />
                  <Point X="-3.331534179688" Y="-0.170930038452" />
                  <Point X="-3.318404052734" Y="-0.144288345337" />
                  <Point X="-3.312886474609" Y="-0.130450958252" />
                  <Point X="-3.304186523438" Y="-0.102419197083" />
                  <Point X="-3.300869384766" Y="-0.087676399231" />
                  <Point X="-3.296614746094" Y="-0.057851940155" />
                  <Point X="-3.295677246094" Y="-0.042770282745" />
                  <Point X="-3.296228027344" Y="-0.011353330612" />
                  <Point X="-3.297415283203" Y="0.002044727325" />
                  <Point X="-3.301670166016" Y="0.02853943634" />
                  <Point X="-3.304737792969" Y="0.04163608551" />
                  <Point X="-3.313437988281" Y="0.069667854309" />
                  <Point X="-3.319739013672" Y="0.085058647156" />
                  <Point X="-3.334928710938" Y="0.11450605011" />
                  <Point X="-3.343817382812" Y="0.128562362671" />
                  <Point X="-3.364714111328" Y="0.155964401245" />
                  <Point X="-3.374600585938" Y="0.167019577026" />
                  <Point X="-3.395974121094" Y="0.187456710815" />
                  <Point X="-3.407461425781" Y="0.196838973999" />
                  <Point X="-3.433561523438" Y="0.214953887939" />
                  <Point X="-3.440484375" Y="0.219328887939" />
                  <Point X="-3.465616210938" Y="0.232834701538" />
                  <Point X="-3.496566894531" Y="0.245635894775" />
                  <Point X="-3.508288085938" Y="0.249611251831" />
                  <Point X="-4.785446289062" Y="0.591824707031" />
                  <Point X="-4.731331542969" Y="0.957526794434" />
                  <Point X="-4.725717773438" Y="0.978243225098" />
                  <Point X="-4.633586425781" Y="1.318236816406" />
                  <Point X="-3.841409179688" Y="1.213944946289" />
                  <Point X="-3.778066162109" Y="1.20560546875" />
                  <Point X="-3.76773828125" Y="1.204815429688" />
                  <Point X="-3.747057861328" Y="1.204364257812" />
                  <Point X="-3.736704589844" Y="1.20470324707" />
                  <Point X="-3.715142822266" Y="1.20658996582" />
                  <Point X="-3.704888671875" Y="1.208054199219" />
                  <Point X="-3.684599121094" Y="1.212090332031" />
                  <Point X="-3.670908935547" Y="1.215815063477" />
                  <Point X="-3.613141113281" Y="1.234029052734" />
                  <Point X="-3.603445800781" Y="1.237678344727" />
                  <Point X="-3.584515625" Y="1.246008422852" />
                  <Point X="-3.575278320312" Y="1.250689575195" />
                  <Point X="-3.556534179688" Y="1.261511474609" />
                  <Point X="-3.547858886719" Y="1.267172241211" />
                  <Point X="-3.531177978516" Y="1.279403442383" />
                  <Point X="-3.515928710938" Y="1.293376953125" />
                  <Point X="-3.502290283203" Y="1.308928466797" />
                  <Point X="-3.495895263672" Y="1.317077026367" />
                  <Point X="-3.483480712891" Y="1.334806884766" />
                  <Point X="-3.478012207031" Y="1.343600952148" />
                  <Point X="-3.468064697266" Y="1.361732055664" />
                  <Point X="-3.462117675781" Y="1.374612792969" />
                  <Point X="-3.438938232422" Y="1.430573242188" />
                  <Point X="-3.435501220703" Y="1.440346069336" />
                  <Point X="-3.429711425781" Y="1.460208251953" />
                  <Point X="-3.427358642578" Y="1.470297851562" />
                  <Point X="-3.423600341797" Y="1.491612792969" />
                  <Point X="-3.422360595703" Y="1.501896972656" />
                  <Point X="-3.421008056641" Y="1.522536987305" />
                  <Point X="-3.42191015625" Y="1.543200439453" />
                  <Point X="-3.425056640625" Y="1.563644165039" />
                  <Point X="-3.427188232422" Y="1.573780639648" />
                  <Point X="-3.432790039062" Y="1.594686889648" />
                  <Point X="-3.436012451172" Y="1.60453137207" />
                  <Point X="-3.443510253906" Y="1.623811645508" />
                  <Point X="-3.449556884766" Y="1.636649658203" />
                  <Point X="-3.477525390625" Y="1.690377075195" />
                  <Point X="-3.482801269531" Y="1.69928894043" />
                  <Point X="-3.494291748047" Y="1.716485351562" />
                  <Point X="-3.500506347656" Y="1.724769897461" />
                  <Point X="-3.514418701172" Y="1.741350219727" />
                  <Point X="-3.521499267578" Y="1.74891027832" />
                  <Point X="-3.536441650391" Y="1.763214599609" />
                  <Point X="-3.544303466797" Y="1.769958862305" />
                  <Point X="-4.227614257812" Y="2.294282226563" />
                  <Point X="-4.002292236328" Y="2.680313720703" />
                  <Point X="-3.987431884766" Y="2.699414306641" />
                  <Point X="-3.726338867188" Y="3.035012695312" />
                  <Point X="-3.293060302734" Y="2.784859375" />
                  <Point X="-3.254156982422" Y="2.7623984375" />
                  <Point X="-3.244917480469" Y="2.757716064453" />
                  <Point X="-3.225984619141" Y="2.749385498047" />
                  <Point X="-3.216291259766" Y="2.745737304688" />
                  <Point X="-3.195648925781" Y="2.739228759766" />
                  <Point X="-3.185616210938" Y="2.736657470703" />
                  <Point X="-3.165329833984" Y="2.732622070312" />
                  <Point X="-3.149981201172" Y="2.730712158203" />
                  <Point X="-3.069526855469" Y="2.723673339844" />
                  <Point X="-3.059174072266" Y="2.723334472656" />
                  <Point X="-3.038492675781" Y="2.723785644531" />
                  <Point X="-3.0281640625" Y="2.724575683594" />
                  <Point X="-3.006705566406" Y="2.727400878906" />
                  <Point X="-2.996525146484" Y="2.729310791016" />
                  <Point X="-2.976433837891" Y="2.734227294922" />
                  <Point X="-2.956998046875" Y="2.741301513672" />
                  <Point X="-2.938447021484" Y="2.750449707031" />
                  <Point X="-2.929420898438" Y="2.755530273438" />
                  <Point X="-2.911166503906" Y="2.767159423828" />
                  <Point X="-2.902746337891" Y="2.773193359375" />
                  <Point X="-2.886614257812" Y="2.786140869141" />
                  <Point X="-2.875285888672" Y="2.796670898438" />
                  <Point X="-2.818178710938" Y="2.853777832031" />
                  <Point X="-2.811265380859" Y="2.861489501953" />
                  <Point X="-2.798318359375" Y="2.877620849609" />
                  <Point X="-2.792284667969" Y="2.886040527344" />
                  <Point X="-2.780655273438" Y="2.904294921875" />
                  <Point X="-2.775574707031" Y="2.913320800781" />
                  <Point X="-2.766426025391" Y="2.931872314453" />
                  <Point X="-2.759351318359" Y="2.951309570312" />
                  <Point X="-2.754434814453" Y="2.971401367188" />
                  <Point X="-2.752524902344" Y="2.981581542969" />
                  <Point X="-2.749699707031" Y="3.003040283203" />
                  <Point X="-2.748909667969" Y="3.013368652344" />
                  <Point X="-2.748458496094" Y="3.034049804688" />
                  <Point X="-2.749243164062" Y="3.049497558594" />
                  <Point X="-2.756281982422" Y="3.129952148438" />
                  <Point X="-2.757745849609" Y="3.140205322266" />
                  <Point X="-2.76178125" Y="3.1604921875" />
                  <Point X="-2.764352783203" Y="3.170525878906" />
                  <Point X="-2.770861328125" Y="3.19116796875" />
                  <Point X="-2.774510009766" Y="3.200861816406" />
                  <Point X="-2.782840576172" Y="3.219794189453" />
                  <Point X="-2.787522460938" Y="3.229032714844" />
                  <Point X="-3.059387451172" Y="3.699916503906" />
                  <Point X="-2.648373535156" Y="4.015036376953" />
                  <Point X="-2.624971679688" Y="4.028038330078" />
                  <Point X="-2.192525146484" Y="4.268296386719" />
                  <Point X="-2.130473632812" Y="4.187429199219" />
                  <Point X="-2.111819580078" Y="4.164046386719" />
                  <Point X="-2.097516601562" Y="4.14910546875" />
                  <Point X="-2.089958007813" Y="4.142026367188" />
                  <Point X="-2.073377929688" Y="4.128113769531" />
                  <Point X="-2.065091796875" Y="4.121897460938" />
                  <Point X="-2.047888916016" Y="4.110402832031" />
                  <Point X="-2.033301635742" Y="4.102172851562" />
                  <Point X="-1.943756225586" Y="4.055557861328" />
                  <Point X="-1.934320922852" Y="4.051283203125" />
                  <Point X="-1.915044555664" Y="4.043788085938" />
                  <Point X="-1.905203613281" Y="4.040567138672" />
                  <Point X="-1.88429699707" Y="4.034965576172" />
                  <Point X="-1.874162475586" Y="4.032834716797" />
                  <Point X="-1.853719360352" Y="4.029688232422" />
                  <Point X="-1.833053222656" Y="4.028785888672" />
                  <Point X="-1.812413818359" Y="4.030138916016" />
                  <Point X="-1.802132446289" Y="4.031378662109" />
                  <Point X="-1.780817016602" Y="4.035136962891" />
                  <Point X="-1.770728393555" Y="4.037489257812" />
                  <Point X="-1.750866088867" Y="4.043278808594" />
                  <Point X="-1.735185668945" Y="4.049163085938" />
                  <Point X="-1.64191796875" Y="4.087795898438" />
                  <Point X="-1.632581542969" Y="4.092274414063" />
                  <Point X="-1.614449707031" Y="4.102222167969" />
                  <Point X="-1.605654418945" Y="4.10769140625" />
                  <Point X="-1.587924804688" Y="4.120105957031" />
                  <Point X="-1.579776245117" Y="4.126501464844" />
                  <Point X="-1.564224853516" Y="4.140140136719" />
                  <Point X="-1.550250976562" Y="4.155390136719" />
                  <Point X="-1.538020019531" Y="4.172071289062" />
                  <Point X="-1.532359863281" Y="4.180745605469" />
                  <Point X="-1.521538085938" Y="4.199489746094" />
                  <Point X="-1.516856323242" Y="4.208728027344" />
                  <Point X="-1.508526123047" Y="4.227659667969" />
                  <Point X="-1.502955200195" Y="4.243450195312" />
                  <Point X="-1.472598144531" Y="4.339729492188" />
                  <Point X="-1.470026367188" Y="4.349764160156" />
                  <Point X="-1.465990966797" Y="4.370051757812" />
                  <Point X="-1.46452722168" Y="4.3803046875" />
                  <Point X="-1.46264074707" Y="4.401866210938" />
                  <Point X="-1.462301757812" Y="4.412217773438" />
                  <Point X="-1.462752807617" Y="4.432897949219" />
                  <Point X="-1.46354309082" Y="4.4432265625" />
                  <Point X="-1.479266113281" Y="4.562654785156" />
                  <Point X="-0.931176574707" Y="4.7163203125" />
                  <Point X="-0.902818237305" Y="4.719639160156" />
                  <Point X="-0.365222045898" Y="4.782556640625" />
                  <Point X="-0.237174865723" Y="4.304678710938" />
                  <Point X="-0.225666397095" Y="4.261727539062" />
                  <Point X="-0.220435165405" Y="4.247107421875" />
                  <Point X="-0.207661468506" Y="4.218916015625" />
                  <Point X="-0.200119308472" Y="4.205344726562" />
                  <Point X="-0.182261230469" Y="4.178618164062" />
                  <Point X="-0.172608306885" Y="4.166455566406" />
                  <Point X="-0.151451248169" Y="4.143866699219" />
                  <Point X="-0.126897850037" Y="4.125026367188" />
                  <Point X="-0.099602661133" Y="4.110436523438" />
                  <Point X="-0.085356559753" Y="4.104260742188" />
                  <Point X="-0.054918586731" Y="4.093927978516" />
                  <Point X="-0.039857143402" Y="4.090155273438" />
                  <Point X="-0.009320039749" Y="4.08511328125" />
                  <Point X="0.02163187027" Y="4.08511328125" />
                  <Point X="0.052168972015" Y="4.090155273438" />
                  <Point X="0.067230415344" Y="4.093927978516" />
                  <Point X="0.097668388367" Y="4.104260742188" />
                  <Point X="0.111914344788" Y="4.110436523438" />
                  <Point X="0.139209686279" Y="4.125026367188" />
                  <Point X="0.163763076782" Y="4.143866699219" />
                  <Point X="0.184920150757" Y="4.166455566406" />
                  <Point X="0.194573196411" Y="4.178618164062" />
                  <Point X="0.212431289673" Y="4.205344726562" />
                  <Point X="0.219973754883" Y="4.218916503906" />
                  <Point X="0.232747146606" Y="4.247107910156" />
                  <Point X="0.237978225708" Y="4.261727539062" />
                  <Point X="0.3781902771" Y="4.785006347656" />
                  <Point X="0.827877685547" Y="4.737911621094" />
                  <Point X="0.851350891113" Y="4.732244628906" />
                  <Point X="1.453599609375" Y="4.586843261719" />
                  <Point X="1.466506347656" Y="4.582161621094" />
                  <Point X="1.858251342773" Y="4.440072753906" />
                  <Point X="1.873040405273" Y="4.433156738281" />
                  <Point X="2.250450683594" Y="4.256654296875" />
                  <Point X="2.264791748047" Y="4.248299316406" />
                  <Point X="2.629435791016" Y="4.035856445312" />
                  <Point X="2.642905029297" Y="4.026278076172" />
                  <Point X="2.817779785156" Y="3.901916748047" />
                  <Point X="2.116622802734" Y="2.687477050781" />
                  <Point X="2.06530859375" Y="2.598597900391" />
                  <Point X="2.061630859375" Y="2.591564697266" />
                  <Point X="2.051282226562" Y="2.568482177734" />
                  <Point X="2.042254516602" Y="2.543007324219" />
                  <Point X="2.04002331543" Y="2.535817382812" />
                  <Point X="2.01983215332" Y="2.4603125" />
                  <Point X="2.017657714844" Y="2.449859619141" />
                  <Point X="2.014495849609" Y="2.428775634766" />
                  <Point X="2.013508300781" Y="2.41814453125" />
                  <Point X="2.012669189453" Y="2.395107177734" />
                  <Point X="2.012689086914" Y="2.387680419922" />
                  <Point X="2.013909423828" Y="2.365445556641" />
                  <Point X="2.021782348633" Y="2.300155273438" />
                  <Point X="2.023800537109" Y="2.289034179688" />
                  <Point X="2.029143676758" Y="2.267110351562" />
                  <Point X="2.032468261719" Y="2.256307617188" />
                  <Point X="2.040734619141" Y="2.234219238281" />
                  <Point X="2.045318969727" Y="2.223887451172" />
                  <Point X="2.055682128906" Y="2.203842041016" />
                  <Point X="2.064019287109" Y="2.190358154297" />
                  <Point X="2.104418701172" Y="2.130819824219" />
                  <Point X="2.110934814453" Y="2.122295898438" />
                  <Point X="2.124886474609" Y="2.106037597656" />
                  <Point X="2.132322021484" Y="2.098303222656" />
                  <Point X="2.149392333984" Y="2.082444824219" />
                  <Point X="2.15487890625" Y="2.077724121094" />
                  <Point X="2.172028076172" Y="2.064422851562" />
                  <Point X="2.23156640625" Y="2.024023681641" />
                  <Point X="2.241279785156" Y="2.018245239258" />
                  <Point X="2.26132421875" Y="2.00788269043" />
                  <Point X="2.271655273438" Y="2.003298706055" />
                  <Point X="2.293743652344" Y="1.995032592773" />
                  <Point X="2.304544189453" Y="1.991708374023" />
                  <Point X="2.326462646484" Y="1.986366088867" />
                  <Point X="2.341715087891" Y="1.983849121094" />
                  <Point X="2.407005371094" Y="1.975976318359" />
                  <Point X="2.417907226562" Y="1.975293701172" />
                  <Point X="2.439716796875" Y="1.975184326172" />
                  <Point X="2.450624511719" Y="1.975757568359" />
                  <Point X="2.47430859375" Y="1.978374023438" />
                  <Point X="2.481414550781" Y="1.979432373047" />
                  <Point X="2.502529296875" Y="1.983674682617" />
                  <Point X="2.578034179688" Y="2.003865600586" />
                  <Point X="2.583993652344" Y="2.005670410156" />
                  <Point X="2.604404541016" Y="2.013067260742" />
                  <Point X="2.627655029297" Y="2.02357409668" />
                  <Point X="2.636033935547" Y="2.027872558594" />
                  <Point X="3.940405029297" Y="2.780951660156" />
                  <Point X="4.043948486328" Y="2.637049560547" />
                  <Point X="4.051462890625" Y="2.624632080078" />
                  <Point X="4.136884277344" Y="2.483472167969" />
                  <Point X="3.24051171875" Y="1.795660522461" />
                  <Point X="3.172951660156" Y="1.743819702148" />
                  <Point X="3.166737060547" Y="1.738615234375" />
                  <Point X="3.148095703125" Y="1.720957885742" />
                  <Point X="3.129807373047" Y="1.700565185547" />
                  <Point X="3.125135009766" Y="1.694932983398" />
                  <Point X="3.070794189453" Y="1.624041015625" />
                  <Point X="3.064689453125" Y="1.615056030273" />
                  <Point X="3.053545654297" Y="1.596448120117" />
                  <Point X="3.048506347656" Y="1.586825073242" />
                  <Point X="3.038754882812" Y="1.565289428711" />
                  <Point X="3.036021972656" Y="1.558584838867" />
                  <Point X="3.028858154297" Y="1.538088256836" />
                  <Point X="3.008616210938" Y="1.465707397461" />
                  <Point X="3.006225097656" Y="1.454661010742" />
                  <Point X="3.002771728516" Y="1.432362670898" />
                  <Point X="3.001709472656" Y="1.421110717773" />
                  <Point X="3.000893310547" Y="1.397540527344" />
                  <Point X="3.001174804688" Y="1.386241455078" />
                  <Point X="3.003077636719" Y="1.36375769043" />
                  <Point X="3.005751220703" Y="1.347473022461" />
                  <Point X="3.022367919922" Y="1.266940063477" />
                  <Point X="3.025187255859" Y="1.256383911133" />
                  <Point X="3.03201953125" Y="1.235665649414" />
                  <Point X="3.036032470703" Y="1.225503173828" />
                  <Point X="3.046092529297" Y="1.203775390625" />
                  <Point X="3.049286865234" Y="1.197498046875" />
                  <Point X="3.059781005859" Y="1.179175292969" />
                  <Point X="3.1049765625" Y="1.110480224609" />
                  <Point X="3.111739257813" Y="1.101424438477" />
                  <Point X="3.126292480469" Y="1.08417980957" />
                  <Point X="3.134083007812" Y="1.075991210938" />
                  <Point X="3.151326904297" Y="1.059901367188" />
                  <Point X="3.160034423828" Y="1.052696044922" />
                  <Point X="3.178243408203" Y="1.039370849609" />
                  <Point X="3.191892333984" Y="1.030916137695" />
                  <Point X="3.257386962891" Y="0.994048400879" />
                  <Point X="3.267385009766" Y="0.989167785645" />
                  <Point X="3.287884521484" Y="0.980608398438" />
                  <Point X="3.298386230469" Y="0.9769296875" />
                  <Point X="3.321969238281" Y="0.970190856934" />
                  <Point X="3.328744873047" Y="0.968521240234" />
                  <Point X="3.349278564453" Y="0.964516418457" />
                  <Point X="3.437831542969" Y="0.952812988281" />
                  <Point X="3.444029785156" Y="0.952199768066" />
                  <Point X="3.465716064453" Y="0.951222839355" />
                  <Point X="3.491217529297" Y="0.952032226562" />
                  <Point X="3.500603515625" Y="0.952797241211" />
                  <Point X="4.704703613281" Y="1.11132019043" />
                  <Point X="4.752684570312" Y="0.914231018066" />
                  <Point X="4.75505078125" Y="0.89903125" />
                  <Point X="4.78387109375" Y="0.713920898438" />
                  <Point X="3.769417236328" Y="0.442098876953" />
                  <Point X="3.691991943359" Y="0.421352905273" />
                  <Point X="3.684109375" Y="0.418868530273" />
                  <Point X="3.659811767578" Y="0.409523101807" />
                  <Point X="3.634786865234" Y="0.397460876465" />
                  <Point X="3.628494873047" Y="0.394132019043" />
                  <Point X="3.541494384766" Y="0.343843963623" />
                  <Point X="3.532273925781" Y="0.337773620605" />
                  <Point X="3.514599121094" Y="0.324603851318" />
                  <Point X="3.506144775391" Y="0.317504272461" />
                  <Point X="3.488527099609" Y="0.300871582031" />
                  <Point X="3.48357421875" Y="0.295834472656" />
                  <Point X="3.469491210938" Y="0.280014312744" />
                  <Point X="3.417291015625" Y="0.213499008179" />
                  <Point X="3.410854248047" Y="0.204208145142" />
                  <Point X="3.399130371094" Y="0.184928344727" />
                  <Point X="3.393843017578" Y="0.17493939209" />
                  <Point X="3.384069091797" Y="0.153475570679" />
                  <Point X="3.380005615234" Y="0.142930480957" />
                  <Point X="3.373159667969" Y="0.12143157959" />
                  <Point X="3.369275146484" Y="0.104724433899" />
                  <Point X="3.351875" Y="0.013867530823" />
                  <Point X="3.350426513672" Y="0.002849371672" />
                  <Point X="3.348825927734" Y="-0.019279985428" />
                  <Point X="3.348673828125" Y="-0.030391330719" />
                  <Point X="3.349775878906" Y="-0.054954387665" />
                  <Point X="3.350330078125" Y="-0.06178836441" />
                  <Point X="3.352976074219" Y="-0.082175865173" />
                  <Point X="3.370376220703" Y="-0.173032913208" />
                  <Point X="3.373158935547" Y="-0.18398789978" />
                  <Point X="3.380005371094" Y="-0.205489334106" />
                  <Point X="3.384069091797" Y="-0.216035766602" />
                  <Point X="3.393843017578" Y="-0.237499588013" />
                  <Point X="3.399130615234" Y="-0.247488830566" />
                  <Point X="3.410854248047" Y="-0.266768035889" />
                  <Point X="3.420595214844" Y="-0.280269683838" />
                  <Point X="3.472795410156" Y="-0.346785125732" />
                  <Point X="3.480303710938" Y="-0.355259765625" />
                  <Point X="3.4962734375" Y="-0.371254119873" />
                  <Point X="3.504735595703" Y="-0.37877456665" />
                  <Point X="3.524557128906" Y="-0.394379394531" />
                  <Point X="3.530032714844" Y="-0.398381958008" />
                  <Point X="3.54700390625" Y="-0.4095887146" />
                  <Point X="3.634004394531" Y="-0.459876617432" />
                  <Point X="3.639495605469" Y="-0.462814880371" />
                  <Point X="3.659158447266" Y="-0.472016876221" />
                  <Point X="3.683028076172" Y="-0.481027893066" />
                  <Point X="3.691991943359" Y="-0.48391293335" />
                  <Point X="4.784876953125" Y="-0.776750427246" />
                  <Point X="4.761612304688" Y="-0.93105871582" />
                  <Point X="4.758580566406" Y="-0.944344177246" />
                  <Point X="4.727802246094" Y="-1.079219604492" />
                  <Point X="3.526573730469" Y="-0.921074890137" />
                  <Point X="3.436781982422" Y="-0.90925390625" />
                  <Point X="3.425416748047" Y="-0.908446716309" />
                  <Point X="3.402670410156" Y="-0.908198974609" />
                  <Point X="3.3912890625" Y="-0.908758544922" />
                  <Point X="3.363330078125" Y="-0.911821777344" />
                  <Point X="3.343668212891" Y="-0.915026550293" />
                  <Point X="3.172916992188" Y="-0.952139892578" />
                  <Point X="3.161470947266" Y="-0.955390319824" />
                  <Point X="3.131101074219" Y="-0.966111206055" />
                  <Point X="3.110878662109" Y="-0.976088134766" />
                  <Point X="3.079643554688" Y="-0.996431030273" />
                  <Point X="3.065033691406" Y="-1.008149658203" />
                  <Point X="3.045997802734" Y="-1.026784179688" />
                  <Point X="3.032813720703" Y="-1.041083740234" />
                  <Point X="2.92960546875" Y="-1.165211303711" />
                  <Point X="2.923182861328" Y="-1.173895996094" />
                  <Point X="2.907155761719" Y="-1.198365234375" />
                  <Point X="2.897790039062" Y="-1.216755371094" />
                  <Point X="2.885673583984" Y="-1.248731201172" />
                  <Point X="2.881065917969" Y="-1.265268798828" />
                  <Point X="2.876533447266" Y="-1.290001098633" />
                  <Point X="2.874220458984" Y="-1.306840209961" />
                  <Point X="2.859428222656" Y="-1.467590576172" />
                  <Point X="2.85908203125" Y="-1.479483154297" />
                  <Point X="2.860162597656" Y="-1.511670043945" />
                  <Point X="2.86362890625" Y="-1.534101928711" />
                  <Point X="2.873760742188" Y="-1.570280151367" />
                  <Point X="2.8806328125" Y="-1.58786340332" />
                  <Point X="2.893128417969" Y="-1.612334960938" />
                  <Point X="2.902524414062" Y="-1.628679077148" />
                  <Point X="2.997020507812" Y="-1.775661865234" />
                  <Point X="3.001741699219" Y="-1.782352783203" />
                  <Point X="3.01979296875" Y="-1.80444921875" />
                  <Point X="3.043489013672" Y="-1.828120117188" />
                  <Point X="3.052796142578" Y="-1.83627746582" />
                  <Point X="4.087170654297" Y="-2.629981933594" />
                  <Point X="4.045490478516" Y="-2.697426757812" />
                  <Point X="4.039215332031" Y="-2.706342529297" />
                  <Point X="4.001274414063" Y="-2.760251708984" />
                  <Point X="2.928426269531" Y="-2.140842285156" />
                  <Point X="2.848454589844" Y="-2.094670654297" />
                  <Point X="2.837956298828" Y="-2.0894453125" />
                  <Point X="2.816396972656" Y="-2.080328125" />
                  <Point X="2.8053359375" Y="-2.076436279297" />
                  <Point X="2.776352783203" Y="-2.068209472656" />
                  <Point X="2.758239013672" Y="-2.064013671875" />
                  <Point X="2.555018066406" Y="-2.027311889648" />
                  <Point X="2.543199707031" Y="-2.025934570313" />
                  <Point X="2.511038818359" Y="-2.024217407227" />
                  <Point X="2.488226074219" Y="-2.025754760742" />
                  <Point X="2.450996826172" Y="-2.032834350586" />
                  <Point X="2.432739013672" Y="-2.038249267578" />
                  <Point X="2.406375976562" Y="-2.049046386719" />
                  <Point X="2.389897460938" Y="-2.056735351563" />
                  <Point X="2.221070800781" Y="-2.145587646484" />
                  <Point X="2.211813476562" Y="-2.151153808594" />
                  <Point X="2.187642089844" Y="-2.167627441406" />
                  <Point X="2.171751708984" Y="-2.181246826172" />
                  <Point X="2.148005126953" Y="-2.206643798828" />
                  <Point X="2.137303710938" Y="-2.220437744141" />
                  <Point X="2.122786132813" Y="-2.243197509766" />
                  <Point X="2.114836914062" Y="-2.256885986328" />
                  <Point X="2.025984741211" Y="-2.425712890625" />
                  <Point X="2.02111328125" Y="-2.436568847656" />
                  <Point X="2.009794311523" Y="-2.466720458984" />
                  <Point X="2.004375" Y="-2.489088623047" />
                  <Point X="1.999944091797" Y="-2.52702734375" />
                  <Point X="1.999655029297" Y="-2.546221923828" />
                  <Point X="2.002191650391" Y="-2.575592285156" />
                  <Point X="2.00451184082" Y="-2.593011230469" />
                  <Point X="2.041213378906" Y="-2.796232421875" />
                  <Point X="2.043015014648" Y="-2.804221435547" />
                  <Point X="2.051236083984" Y="-2.83154296875" />
                  <Point X="2.064069580078" Y="-2.862479736328" />
                  <Point X="2.069546630859" Y="-2.873578613281" />
                  <Point X="2.735893310547" Y="-4.027724121094" />
                  <Point X="2.723754394531" Y="-4.036083740234" />
                  <Point X="1.894940063477" Y="-2.955952880859" />
                  <Point X="1.833914550781" Y="-2.876422851562" />
                  <Point X="1.826011352539" Y="-2.867371337891" />
                  <Point X="1.809125488281" Y="-2.850338623047" />
                  <Point X="1.800142822266" Y="-2.842357421875" />
                  <Point X="1.774821899414" Y="-2.822577392578" />
                  <Point X="1.760606445312" Y="-2.812487060547" />
                  <Point X="1.56017565918" Y="-2.683628417969" />
                  <Point X="1.549783569336" Y="-2.677833251953" />
                  <Point X="1.520728149414" Y="-2.663939453125" />
                  <Point X="1.498791503906" Y="-2.656573486328" />
                  <Point X="1.461130249023" Y="-2.648866455078" />
                  <Point X="1.441906005859" Y="-2.646937988281" />
                  <Point X="1.411567138672" Y="-2.646994873047" />
                  <Point X="1.394512451172" Y="-2.647793701172" />
                  <Point X="1.175307373047" Y="-2.667965332031" />
                  <Point X="1.164625" Y="-2.669564941406" />
                  <Point X="1.135990112305" Y="-2.675534423828" />
                  <Point X="1.115865844727" Y="-2.682141845703" />
                  <Point X="1.083791015625" Y="-2.696811279297" />
                  <Point X="1.068440185547" Y="-2.705647705078" />
                  <Point X="1.044888793945" Y="-2.722307617188" />
                  <Point X="1.033140014648" Y="-2.731325439453" />
                  <Point X="0.863875061035" Y="-2.872063476562" />
                  <Point X="0.855220458984" Y="-2.880228759766" />
                  <Point X="0.833224243164" Y="-2.903753417969" />
                  <Point X="0.819520629883" Y="-2.922591064453" />
                  <Point X="0.800740234375" Y="-2.956483154297" />
                  <Point X="0.793096252441" Y="-2.974396972656" />
                  <Point X="0.783773254395" Y="-3.00446875" />
                  <Point X="0.779586975098" Y="-3.020376464844" />
                  <Point X="0.728977661133" Y="-3.253219238281" />
                  <Point X="0.727584594727" Y="-3.261289794922" />
                  <Point X="0.72472467041" Y="-3.289677734375" />
                  <Point X="0.72474230957" Y="-3.323170410156" />
                  <Point X="0.725555175781" Y="-3.335520019531" />
                  <Point X="0.83309161377" Y="-4.152340820313" />
                  <Point X="0.678921569824" Y="-3.576970703125" />
                  <Point X="0.655064941406" Y="-3.487936523438" />
                  <Point X="0.651129699707" Y="-3.476312255859" />
                  <Point X="0.641793212891" Y="-3.453668212891" />
                  <Point X="0.636392272949" Y="-3.4426484375" />
                  <Point X="0.619421142578" Y="-3.41303125" />
                  <Point X="0.610656677246" Y="-3.399161132812" />
                  <Point X="0.45667980957" Y="-3.177309814453" />
                  <Point X="0.449297119141" Y="-3.167979492188" />
                  <Point X="0.427770874023" Y="-3.144024414062" />
                  <Point X="0.41011819458" Y="-3.128686279297" />
                  <Point X="0.377816467285" Y="-3.106966552734" />
                  <Point X="0.360541442871" Y="-3.097778808594" />
                  <Point X="0.330714904785" Y="-3.085670410156" />
                  <Point X="0.315565612793" Y="-3.080255371094" />
                  <Point X="0.077295608521" Y="-3.006305175781" />
                  <Point X="0.066813682556" Y="-3.003695068359" />
                  <Point X="0.038074317932" Y="-2.998252197266" />
                  <Point X="0.016730434418" Y="-2.996663818359" />
                  <Point X="-0.018953330994" Y="-2.998041992188" />
                  <Point X="-0.036730117798" Y="-3.000422851562" />
                  <Point X="-0.066639732361" Y="-3.007352783203" />
                  <Point X="-0.080072715759" Y="-3.010988037109" />
                  <Point X="-0.318342895508" Y="-3.084938476562" />
                  <Point X="-0.329464324951" Y="-3.089170654297" />
                  <Point X="-0.358788818359" Y="-3.102487548828" />
                  <Point X="-0.378850402832" Y="-3.114800537109" />
                  <Point X="-0.409484558105" Y="-3.139305419922" />
                  <Point X="-0.423522857666" Y="-3.15315625" />
                  <Point X="-0.444791412354" Y="-3.179023681641" />
                  <Point X="-0.454118835449" Y="-3.191358886719" />
                  <Point X="-0.608095581055" Y="-3.413210205078" />
                  <Point X="-0.612470275879" Y="-3.420132324219" />
                  <Point X="-0.62597668457" Y="-3.445265136719" />
                  <Point X="-0.638777709961" Y="-3.476215820312" />
                  <Point X="-0.642752929688" Y="-3.487936523438" />
                  <Point X="-0.985425170898" Y="-4.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.758138851002" Y="0.707025966559" />
                  <Point X="4.607840006986" Y="1.098567841541" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.090026872618" Y="2.447517175438" />
                  <Point X="3.986766493415" Y="2.716519660116" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.665870439276" Y="0.68230272655" />
                  <Point X="4.510976400691" Y="1.085815492653" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.011421327811" Y="2.387200950225" />
                  <Point X="3.874811658818" Y="2.743081305051" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.573602027549" Y="0.657579486542" />
                  <Point X="4.414112794396" Y="1.073063143765" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.932815783004" Y="2.326884725011" />
                  <Point X="3.791513711473" Y="2.694989206377" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.481333615822" Y="0.632856246533" />
                  <Point X="4.317249188101" Y="1.060310794876" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.854210238198" Y="2.266568499797" />
                  <Point X="3.708215764128" Y="2.646897107703" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.389065204096" Y="0.608133006524" />
                  <Point X="4.220385581806" Y="1.047558445988" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.775604693391" Y="2.206252274583" />
                  <Point X="3.624917816783" Y="2.598805009028" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.296796792369" Y="0.583409766516" />
                  <Point X="4.123521975511" Y="1.034806097099" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.696999148584" Y="2.145936049369" />
                  <Point X="3.541619869437" Y="2.550712910354" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.765779086403" Y="-0.903421549622" />
                  <Point X="4.709390347876" Y="-0.756523863512" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.204528380642" Y="0.558686526507" />
                  <Point X="4.026658369216" Y="1.022053748211" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.618393603777" Y="2.085619824156" />
                  <Point X="3.458321922092" Y="2.502620811679" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.729181965081" Y="-1.07317345948" />
                  <Point X="4.595965106726" Y="-0.726131678546" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.112259968916" Y="0.533963286499" />
                  <Point X="3.929794762921" Y="1.009301399323" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.53978805897" Y="2.025303598942" />
                  <Point X="3.375023974747" Y="2.454528713005" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.624524791461" Y="-1.065622871356" />
                  <Point X="4.482539865576" Y="-0.695739493581" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.019991557189" Y="0.50924004649" />
                  <Point X="3.832931156626" Y="0.996549050434" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.461182514163" Y="1.964987373728" />
                  <Point X="3.291726027402" Y="2.406436614331" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.757646159526" Y="3.797762237807" />
                  <Point X="2.680073513204" Y="3.99984589046" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.517349749188" Y="-1.051513011137" />
                  <Point X="4.369114624426" Y="-0.665347308615" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.927723145462" Y="0.484516806481" />
                  <Point X="3.736067550331" Y="0.983796701546" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.382576969357" Y="1.904671148514" />
                  <Point X="3.208428080057" Y="2.358344515656" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.696525083757" Y="3.691897413502" />
                  <Point X="2.545783553273" Y="4.084592526162" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.410174706915" Y="-1.037403150919" />
                  <Point X="4.255689383276" Y="-0.63495512365" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.835454733736" Y="0.459793566473" />
                  <Point X="3.639203944036" Y="0.971044352657" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.30397142455" Y="1.8443549233" />
                  <Point X="3.125130132712" Y="2.310252416982" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.635404007987" Y="3.586032589198" />
                  <Point X="2.414711833305" Y="4.160955360126" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.302999664642" Y="-1.0232932907" />
                  <Point X="4.142264142126" Y="-0.604562938684" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.743186314029" Y="0.435070347253" />
                  <Point X="3.542340337741" Y="0.958292003769" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.22536588669" Y="1.78403867999" />
                  <Point X="3.041832185367" Y="2.262160318308" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.574282932218" Y="3.480167764893" />
                  <Point X="2.283640113337" Y="4.23731819409" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.19582462237" Y="-1.009183430481" />
                  <Point X="4.028838900976" Y="-0.574170753718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.652573479036" Y="0.406034182398" />
                  <Point X="3.442876349007" Y="0.952313882737" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.147903731051" Y="1.720743824156" />
                  <Point X="2.958534238022" Y="2.214068219633" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.513161856448" Y="3.374302940588" />
                  <Point X="2.157831924447" Y="4.299969060803" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.088649580097" Y="-0.995073570262" />
                  <Point X="3.915413659826" Y="-0.543778568753" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.568659797588" Y="0.359546125904" />
                  <Point X="3.335393953272" Y="0.967224426098" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.07910419167" Y="1.634882081439" />
                  <Point X="2.875236290677" Y="2.165976120959" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.452040780679" Y="3.268438116283" />
                  <Point X="2.033808358611" Y="4.357970825512" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.981474537824" Y="-0.980963710044" />
                  <Point X="3.801988418676" Y="-0.513386383787" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.489185473824" Y="0.30149314725" />
                  <Point X="3.213953166081" Y="1.018497822404" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.02280699772" Y="1.516450615357" />
                  <Point X="2.791938343332" Y="2.117884022285" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.390919704909" Y="3.162573291979" />
                  <Point X="1.909784792775" Y="4.415972590221" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.874299495551" Y="-0.966853849825" />
                  <Point X="3.688482230487" Y="-0.482783324576" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.419919034301" Y="0.216847720987" />
                  <Point X="2.708640395987" Y="2.06979192361" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.329798629139" Y="3.056708467674" />
                  <Point X="1.789154444685" Y="4.465134720486" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.767124453278" Y="-0.952743989606" />
                  <Point X="3.56194090815" Y="-0.418222579937" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.366587483956" Y="0.090690489181" />
                  <Point X="2.625071193768" Y="2.022406468041" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.26867755337" Y="2.950843643369" />
                  <Point X="1.670936082372" Y="4.508013412981" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.659949411005" Y="-0.938634129387" />
                  <Point X="2.534861284159" Y="1.992320646676" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.2075564776" Y="2.844978819064" />
                  <Point X="1.552717720058" Y="4.550892105475" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.552774368732" Y="-0.924524269168" />
                  <Point X="2.439680456941" Y="1.975184508417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.146435401831" Y="2.739113994759" />
                  <Point X="1.435479257393" Y="4.591218072112" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.445599444048" Y="-0.910414715278" />
                  <Point X="2.334114165172" Y="1.985103430291" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.085314371096" Y="2.633249053134" />
                  <Point X="1.323326553191" Y="4.61829518499" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.035344177937" Y="-2.711842942909" />
                  <Point X="3.969160977337" Y="-2.539429810758" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.345496589959" Y="-0.914728535161" />
                  <Point X="2.212431214125" Y="2.037007685007" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.032194320787" Y="2.506540844897" />
                  <Point X="1.211173848989" Y="4.645372297867" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.938185839472" Y="-2.723827488244" />
                  <Point X="3.82491444416" Y="-2.428745414975" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.251538937588" Y="-0.935051152839" />
                  <Point X="1.099021144787" Y="4.672449410745" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.807453665025" Y="-2.648349200601" />
                  <Point X="3.680667910984" Y="-2.318061019192" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.158051060174" Y="-0.95659757612" />
                  <Point X="0.986868440585" Y="4.699526523622" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.676721490577" Y="-2.572870912958" />
                  <Point X="3.536421377808" Y="-2.207376623409" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.073480510354" Y="-1.001374432006" />
                  <Point X="0.874715736384" Y="4.7266036365" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.545989316129" Y="-2.497392625314" />
                  <Point X="3.392174844632" Y="-2.096692227626" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.001446159262" Y="-1.078809202106" />
                  <Point X="0.766134066027" Y="4.744377888158" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.415257141681" Y="-2.421914337671" />
                  <Point X="3.247928311456" Y="-1.986007831843" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.931827978941" Y="-1.162538312263" />
                  <Point X="0.660113122888" Y="4.755481217344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.284524967233" Y="-2.346436050028" />
                  <Point X="3.10368177828" Y="-1.87532343606" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.877329709922" Y="-1.28565613801" />
                  <Point X="0.554092179748" Y="4.766584546531" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.153792792785" Y="-2.270957762384" />
                  <Point X="2.914240514342" Y="-1.646902741389" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.859954919807" Y="-1.505483932697" />
                  <Point X="0.448071236609" Y="4.777687875717" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.023060618337" Y="-2.195479474741" />
                  <Point X="0.363931007255" Y="4.731789996694" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.892328457073" Y="-2.120001221443" />
                  <Point X="0.322099742113" Y="4.575673497663" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.770135908922" Y="-2.066769420881" />
                  <Point X="0.280268476971" Y="4.419556998631" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.660546683688" Y="-2.046370399031" />
                  <Point X="0.23843721183" Y="4.2634404996" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.551305969481" Y="-2.026879279443" />
                  <Point X="0.177108859183" Y="4.15811565002" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.451776238456" Y="-2.032686135954" />
                  <Point X="0.096212733585" Y="4.103766591778" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.364400180286" Y="-2.070154392714" />
                  <Point X="0.001614294235" Y="4.08511328125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.279744056506" Y="-2.114708320809" />
                  <Point X="-0.112514444001" Y="4.117338138782" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.359105846494" Y="4.759730704864" />
                  <Point X="-0.367754146506" Y="4.782260296653" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.196089140815" Y="-2.161870485148" />
                  <Point X="-0.465137910206" Y="4.770863004134" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.12451091504" Y="-2.240493502326" />
                  <Point X="-0.562521673907" Y="4.759465711615" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.700262657501" Y="-4.005468741005" />
                  <Point X="2.655065755404" Y="-3.887726785593" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.065228081132" Y="-2.351147110403" />
                  <Point X="-0.659905437608" Y="4.748068419096" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.496638864271" Y="-3.740101294365" />
                  <Point X="2.351424643303" Y="-3.361805315281" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.009035699798" Y="-2.469851622686" />
                  <Point X="-0.757289201308" Y="4.736671126577" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.293015071042" Y="-3.474733847726" />
                  <Point X="-0.854672965009" Y="4.725273834058" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.089391277812" Y="-3.209366401086" />
                  <Point X="-0.950874832255" Y="4.710797596008" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.885767472246" Y="-2.943998922309" />
                  <Point X="-1.04274619696" Y="4.685040013147" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.724653124586" Y="-2.789372367468" />
                  <Point X="-1.134617561666" Y="4.659282430285" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.589553090423" Y="-2.702515416247" />
                  <Point X="-1.226488926371" Y="4.633524847424" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.467717897161" Y="-2.650214556999" />
                  <Point X="-1.318360291077" Y="4.607767264562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.366035749538" Y="-2.650414176566" />
                  <Point X="-1.410231655783" Y="4.582009681701" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.267748845351" Y="-2.65945870768" />
                  <Point X="-1.466062491237" Y="4.462363310202" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.169584418719" Y="-2.668822303732" />
                  <Point X="-1.494311570353" Y="4.270864006879" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.079514551498" Y="-2.699272947993" />
                  <Point X="-1.551303258014" Y="4.154241758771" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.000482780512" Y="-2.758478816048" />
                  <Point X="-1.629850217609" Y="4.093772913862" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.923344287724" Y="-2.822616842431" />
                  <Point X="-1.717324797114" Y="4.056561313953" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.847053499184" Y="-2.888963213884" />
                  <Point X="-1.809094599908" Y="4.030539153268" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.786391414185" Y="-2.996023750026" />
                  <Point X="-1.916096114045" Y="4.044196957237" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.748638382201" Y="-3.16276440966" />
                  <Point X="-2.041991791915" Y="4.107075740534" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.732007467015" Y="-3.384530064788" />
                  <Point X="-2.20333252136" Y="4.262292040087" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.785124725099" Y="-3.787995923383" />
                  <Point X="-2.287204252023" Y="4.21569469806" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.442430827297" Y="-3.160338468095" />
                  <Point X="-2.371075982686" Y="4.169097356032" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.309168951901" Y="-3.078270084176" />
                  <Point X="-2.454947713349" Y="4.122500014005" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.193647245214" Y="-3.042416419766" />
                  <Point X="-2.538819444012" Y="4.075902671978" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.078125538527" Y="-3.006562755355" />
                  <Point X="-2.622691174675" Y="4.029305329951" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.026515281313" Y="-2.999054770284" />
                  <Point X="-2.702919497006" Y="3.973216584719" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.119049684993" Y="-3.023085077563" />
                  <Point X="-2.781539934231" Y="3.912939155581" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.209975754568" Y="-3.051305238432" />
                  <Point X="-2.860160371456" Y="3.852661726443" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.300901824143" Y="-3.079525399301" />
                  <Point X="-2.938780808681" Y="3.792384297305" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.386707221934" Y="-3.121085366238" />
                  <Point X="-2.748511397627" Y="3.031624864808" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.897231834417" Y="3.419054848386" />
                  <Point X="-3.017401245906" Y="3.732106868167" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.458865342925" Y="-3.19819770473" />
                  <Point X="-2.793652751956" Y="2.88413144292" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.524386165525" Y="-3.292600796681" />
                  <Point X="-2.865568650968" Y="2.806388094599" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.589906988124" Y="-3.387003888632" />
                  <Point X="-2.944675630237" Y="2.747378150822" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.646932994015" Y="-3.503536734697" />
                  <Point X="-3.037409882817" Y="2.723868467725" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.688764275577" Y="-3.659653190951" />
                  <Point X="-3.141511252196" Y="2.7299711363" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.73059555714" Y="-3.815769647204" />
                  <Point X="-3.256162081169" Y="2.7635560867" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.772426838703" Y="-3.971886103457" />
                  <Point X="-3.386894265777" Y="2.839034400812" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.814258120265" Y="-4.12800255971" />
                  <Point X="-3.517626398558" Y="2.914512579909" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.856089401828" Y="-4.284119015964" />
                  <Point X="-3.648358531339" Y="2.989990759006" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.897920683391" Y="-4.440235472217" />
                  <Point X="-3.753833611034" Y="2.999672065304" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.939751964954" Y="-4.59635192847" />
                  <Point X="-3.821972455821" Y="2.912089154324" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.981583246516" Y="-4.752468384723" />
                  <Point X="-1.170602617074" Y="-4.260056089469" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.237588470108" Y="-4.085551976239" />
                  <Point X="-3.890111300607" Y="2.824506243343" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.086000474083" Y="-4.745542877438" />
                  <Point X="-1.128531193601" Y="-4.634746565108" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.394017278501" Y="-3.943131668506" />
                  <Point X="-3.448203178161" Y="1.408205555547" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.604922600363" Y="1.816473608548" />
                  <Point X="-3.958250145393" Y="2.736923332362" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.541398522422" Y="-3.824281072042" />
                  <Point X="-3.50894167331" Y="1.301344074651" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.74916912201" Y="1.927157974296" />
                  <Point X="-4.02400142323" Y="2.64312059683" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.654377186336" Y="-3.79505226055" />
                  <Point X="-3.588744717917" Y="1.24414744307" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.893415643657" Y="2.037842340045" />
                  <Point X="-4.085388698287" Y="2.537949245378" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.758775550103" Y="-3.788175895143" />
                  <Point X="-3.678803237131" Y="1.213667236243" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.037662165303" Y="2.148526705793" />
                  <Point X="-4.146775973344" Y="2.432777893926" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.863160620204" Y="-3.78133416092" />
                  <Point X="-2.311469256096" Y="-2.61345023595" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.380532985009" Y="-2.433533070992" />
                  <Point X="-3.296037315244" Y="-0.048562751617" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.381221975449" Y="0.173350875164" />
                  <Point X="-3.777449274663" Y="1.205558279478" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.18190868695" Y="2.259211071541" />
                  <Point X="-4.208163248401" Y="2.327606542474" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.966835471426" Y="-3.776342610132" />
                  <Point X="-2.35906206017" Y="-2.754557412913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.51267643578" Y="-2.354378282832" />
                  <Point X="-3.343527387631" Y="-0.189937553775" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.512709093038" Y="0.250795856927" />
                  <Point X="-3.884610641267" Y="1.219632513361" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.060266963646" Y="-3.798035921866" />
                  <Point X="-2.420183159062" Y="-2.860422176983" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.614472751409" Y="-2.354280484575" />
                  <Point X="-2.962983264974" Y="-1.446379556755" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.08178530706" Y="-1.136889656055" />
                  <Point X="-3.416274463188" Y="-0.265515613166" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.626134339327" Y="0.281188055281" />
                  <Point X="-3.991785670973" Y="1.233742340843" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.143002155034" Y="-3.847594049931" />
                  <Point X="-2.481304243023" Y="-2.966286979948" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.703127166957" Y="-2.388418506507" />
                  <Point X="-3.024762826777" Y="-1.550528966296" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.19750315929" Y="-1.100525015035" />
                  <Point X="-3.501016184426" Y="-0.309846552261" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.739559585617" Y="0.311580253636" />
                  <Point X="-4.09896070068" Y="1.247852168326" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.223988661803" Y="-3.901707657174" />
                  <Point X="-2.542425326984" Y="-3.072151782914" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.786425115659" Y="-2.436510601649" />
                  <Point X="-3.101758707662" Y="-1.61503850939" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.296739341416" Y="-1.10709659257" />
                  <Point X="-3.593153691083" Y="-0.334910811635" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.852984831906" Y="0.341972451991" />
                  <Point X="-4.206135730386" Y="1.261961995808" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.304975168571" Y="-3.955821264418" />
                  <Point X="-2.603546410944" Y="-3.17801658588" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.86972306436" Y="-2.484602696791" />
                  <Point X="-3.180364262977" Y="-1.675354707229" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.393602953666" Y="-1.119848925945" />
                  <Point X="-3.685422099322" Y="-0.35963406073" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.966410078196" Y="0.372364650345" />
                  <Point X="-4.313310760093" Y="1.276071823291" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.384360400659" Y="-4.014106334823" />
                  <Point X="-2.664667494905" Y="-3.283881388845" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.953021013061" Y="-2.532694791932" />
                  <Point X="-3.258969817971" Y="-1.735670905904" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.490466565917" Y="-1.13260125932" />
                  <Point X="-3.777690505214" Y="-0.384357315937" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.079835324485" Y="0.4027568487" />
                  <Point X="-4.4204857898" Y="1.290181650773" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.453852203422" Y="-4.098164669774" />
                  <Point X="-2.725788578866" Y="-3.389746191811" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.036318961762" Y="-2.580786887074" />
                  <Point X="-3.337575372965" Y="-1.795987104579" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.587330178167" Y="-1.145353592694" />
                  <Point X="-3.869958911106" Y="-0.409080571145" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.193260570775" Y="0.433149047055" />
                  <Point X="-4.527660819506" Y="1.304291478256" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.539565446191" Y="-4.139964708751" />
                  <Point X="-2.786909662827" Y="-3.495610994777" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.119616910463" Y="-2.628878982216" />
                  <Point X="-3.416180927959" Y="-1.856303303254" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.684193790417" Y="-1.158105926069" />
                  <Point X="-3.962227316999" Y="-0.433803826352" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.306685817064" Y="0.463541245409" />
                  <Point X="-4.6340773189" Y="1.316425266715" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.673050900537" Y="-4.057313881751" />
                  <Point X="-2.848030746788" Y="-3.601475797742" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.202914859164" Y="-2.676971077357" />
                  <Point X="-3.494786482953" Y="-1.91661950193" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.781057402667" Y="-1.170858259444" />
                  <Point X="-4.054495722891" Y="-0.458527081559" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.420111063354" Y="0.493933443764" />
                  <Point X="-4.676185929361" Y="1.161031276941" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.811380608381" Y="-3.962043342941" />
                  <Point X="-2.909151830749" Y="-3.707340600708" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.286212807865" Y="-2.725063172499" />
                  <Point X="-3.573392037948" Y="-1.976935700605" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.877921014917" Y="-1.183610592818" />
                  <Point X="-4.146764128784" Y="-0.483250336766" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.533536309643" Y="0.524325642119" />
                  <Point X="-4.718294539821" Y="1.005637287167" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.955834601374" Y="-3.850818495856" />
                  <Point X="-2.97027291471" Y="-3.813205403674" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.369510756567" Y="-2.773155267641" />
                  <Point X="-3.651997592942" Y="-2.03725189928" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.974784627168" Y="-1.196362926193" />
                  <Point X="-4.239032534676" Y="-0.507973591974" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.646961555933" Y="0.554717840473" />
                  <Point X="-4.750878463926" Y="0.825430641123" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.452808705268" Y="-2.821247362782" />
                  <Point X="-3.730603147936" Y="-2.097568097955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.071648239418" Y="-1.209115259568" />
                  <Point X="-4.331300940569" Y="-0.532696847181" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.760386802223" Y="0.585110038828" />
                  <Point X="-4.779191072769" Y="0.634096838398" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.536106653969" Y="-2.869339457924" />
                  <Point X="-3.80920870293" Y="-2.15788429663" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.168511851668" Y="-1.221867592942" />
                  <Point X="-4.423569346461" Y="-0.557420102388" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.61940460267" Y="-2.917431553066" />
                  <Point X="-3.887814257924" Y="-2.218200495305" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.265375463918" Y="-1.234619926317" />
                  <Point X="-4.515837752354" Y="-0.582143357595" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.702702551371" Y="-2.965523648207" />
                  <Point X="-3.966419812918" Y="-2.27851669398" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.362239076168" Y="-1.247372259692" />
                  <Point X="-4.608106158246" Y="-0.606866612802" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.786000500072" Y="-3.013615743349" />
                  <Point X="-4.045025367912" Y="-2.338832892655" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.459102688419" Y="-1.260124593066" />
                  <Point X="-4.700374564139" Y="-0.63158986801" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.980909244871" Y="-2.770951774074" />
                  <Point X="-4.123630922906" Y="-2.39914909133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.555966300669" Y="-1.272876926441" />
                  <Point X="-4.782397700839" Y="-0.683002961955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.652829912919" Y="-1.285629259816" />
                  <Point X="-4.677825504207" Y="-1.220513518286" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.0001640625" Y="0.001626220703" />
                  <Width Value="9.99646484375" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.495395721436" Y="-3.626146484375" />
                  <Point X="0.471539031982" Y="-3.537112304688" />
                  <Point X="0.454567871094" Y="-3.507495117188" />
                  <Point X="0.300591033936" Y="-3.285643798828" />
                  <Point X="0.289073364258" Y="-3.273825195312" />
                  <Point X="0.259246887207" Y="-3.261716796875" />
                  <Point X="0.02097677803" Y="-3.187766601562" />
                  <Point X="0.00615596056" Y="-3.18551953125" />
                  <Point X="-0.023753627777" Y="-3.192449462891" />
                  <Point X="-0.262023895264" Y="-3.266399902344" />
                  <Point X="-0.276761444092" Y="-3.273825439453" />
                  <Point X="-0.298029968262" Y="-3.299692871094" />
                  <Point X="-0.45200680542" Y="-3.521544189453" />
                  <Point X="-0.459227142334" Y="-3.537112304688" />
                  <Point X="-0.835951660156" Y="-4.943067871094" />
                  <Point X="-0.84774395752" Y="-4.987077148438" />
                  <Point X="-1.100246582031" Y="-4.938065429688" />
                  <Point X="-1.116397460938" Y="-4.933909667969" />
                  <Point X="-1.35158972168" Y="-4.873396972656" />
                  <Point X="-1.312022216797" Y="-4.572852050781" />
                  <Point X="-1.309150146484" Y="-4.551038574219" />
                  <Point X="-1.313287963867" Y="-4.516635742188" />
                  <Point X="-1.370210693359" Y="-4.230466308594" />
                  <Point X="-1.375948730469" Y="-4.215220703125" />
                  <Point X="-1.400174560547" Y="-4.190445800781" />
                  <Point X="-1.619543457031" Y="-3.998063964844" />
                  <Point X="-1.633376708984" Y="-3.989461914063" />
                  <Point X="-1.667678222656" Y="-3.984554443359" />
                  <Point X="-1.958829833984" Y="-3.965471435547" />
                  <Point X="-1.975041015625" Y="-3.967068115234" />
                  <Point X="-2.005241821289" Y="-3.984056396484" />
                  <Point X="-2.247844970703" Y="-4.146159179688" />
                  <Point X="-2.259734375" Y="-4.157294433594" />
                  <Point X="-2.457095458984" Y="-4.4145" />
                  <Point X="-2.855831054688" Y="-4.16761328125" />
                  <Point X="-2.878200439453" Y="-4.150389648438" />
                  <Point X="-3.228581054688" Y="-3.880608154297" />
                  <Point X="-2.550368652344" Y="-2.705909912109" />
                  <Point X="-2.506033691406" Y="-2.629119628906" />
                  <Point X="-2.499762695312" Y="-2.597593261719" />
                  <Point X="-2.515012939453" Y="-2.567731201172" />
                  <Point X="-2.531328369141" Y="-2.551415771484" />
                  <Point X="-2.560157226562" Y="-2.537198730469" />
                  <Point X="-2.59168359375" Y="-2.543469726562" />
                  <Point X="-3.804292480469" Y="-3.243569824219" />
                  <Point X="-3.842959228516" Y="-3.265894042969" />
                  <Point X="-4.161703613281" Y="-2.847129150391" />
                  <Point X="-4.177737792969" Y="-2.820242675781" />
                  <Point X="-4.431020019531" Y="-2.395527099609" />
                  <Point X="-3.241358642578" Y="-1.482667724609" />
                  <Point X="-3.163786865234" Y="-1.423144775391" />
                  <Point X="-3.14536328125" Y="-1.3942421875" />
                  <Point X="-3.138117431641" Y="-1.366266479492" />
                  <Point X="-3.140326171875" Y="-1.334595703125" />
                  <Point X="-3.162736083984" Y="-1.3097109375" />
                  <Point X="-3.187641601562" Y="-1.295052734375" />
                  <Point X="-3.219529296875" Y="-1.288571044922" />
                  <Point X="-4.750333007812" Y="-1.490105224609" />
                  <Point X="-4.803283203125" Y="-1.497076293945" />
                  <Point X="-4.927393066406" Y="-1.011191467285" />
                  <Point X="-4.931635253906" Y="-0.981531677246" />
                  <Point X="-4.998396484375" Y="-0.514742126465" />
                  <Point X="-3.645829833984" Y="-0.152322937012" />
                  <Point X="-3.557463867188" Y="-0.128645431519" />
                  <Point X="-3.540242919922" Y="-0.120277816772" />
                  <Point X="-3.514142822266" Y="-0.102162895203" />
                  <Point X="-3.494347900391" Y="-0.074132423401" />
                  <Point X="-3.485647949219" Y="-0.046100738525" />
                  <Point X="-3.486198730469" Y="-0.014683854103" />
                  <Point X="-3.494898925781" Y="0.013347985268" />
                  <Point X="-3.515795654297" Y="0.04075006485" />
                  <Point X="-3.541895751953" Y="0.05886498642" />
                  <Point X="-3.557463867188" Y="0.066085334778" />
                  <Point X="-4.952864257812" Y="0.439981719971" />
                  <Point X="-4.998187011719" Y="0.45212588501" />
                  <Point X="-4.917645019531" Y="0.996418151855" />
                  <Point X="-4.909104492188" Y="1.027935913086" />
                  <Point X="-4.773516113281" Y="1.528298583984" />
                  <Point X="-3.816609375" Y="1.402319458008" />
                  <Point X="-3.753266357422" Y="1.393980102539" />
                  <Point X="-3.731704589844" Y="1.395866699219" />
                  <Point X="-3.728046386719" Y="1.397020263672" />
                  <Point X="-3.670278564453" Y="1.41523425293" />
                  <Point X="-3.651534423828" Y="1.426056274414" />
                  <Point X="-3.639119873047" Y="1.443786132813" />
                  <Point X="-3.637651855469" Y="1.447329833984" />
                  <Point X="-3.614472412109" Y="1.503290283203" />
                  <Point X="-3.610714111328" Y="1.524605224609" />
                  <Point X="-3.616315917969" Y="1.54551159668" />
                  <Point X="-3.618087158203" Y="1.54891394043" />
                  <Point X="-3.646055664062" Y="1.602641235352" />
                  <Point X="-3.659968017578" Y="1.619221557617" />
                  <Point X="-4.460373046875" Y="2.233393798828" />
                  <Point X="-4.47610546875" Y="2.245466064453" />
                  <Point X="-4.160014648438" Y="2.787006591797" />
                  <Point X="-4.137392578125" Y="2.816083740234" />
                  <Point X="-3.774671142578" Y="3.282310791016" />
                  <Point X="-3.198060302734" Y="2.949404296875" />
                  <Point X="-3.159156982422" Y="2.926943359375" />
                  <Point X="-3.138514648438" Y="2.920434814453" />
                  <Point X="-3.133419677734" Y="2.919989013672" />
                  <Point X="-3.052965332031" Y="2.912950195312" />
                  <Point X="-3.031506835938" Y="2.915775390625" />
                  <Point X="-3.013252441406" Y="2.927404541016" />
                  <Point X="-3.009635986328" Y="2.931020996094" />
                  <Point X="-2.952528808594" Y="2.988127929688" />
                  <Point X="-2.940899414062" Y="3.006382324219" />
                  <Point X="-2.93807421875" Y="3.027841064453" />
                  <Point X="-2.938520019531" Y="3.032936035156" />
                  <Point X="-2.945558837891" Y="3.113390625" />
                  <Point X="-2.952067382812" Y="3.134032714844" />
                  <Point X="-3.306751220703" Y="3.748362792969" />
                  <Point X="-3.306826416016" Y="3.749623535156" />
                  <Point X="-2.752875976562" Y="4.174331542969" />
                  <Point X="-2.717247802734" Y="4.194125976562" />
                  <Point X="-2.141548583984" Y="4.513972167969" />
                  <Point X="-1.979736450195" Y="4.303094238281" />
                  <Point X="-1.967827026367" Y="4.287573242188" />
                  <Point X="-1.951246948242" Y="4.273660644531" />
                  <Point X="-1.945576293945" Y="4.270708984375" />
                  <Point X="-1.856030761719" Y="4.22409375" />
                  <Point X="-1.835124145508" Y="4.2184921875" />
                  <Point X="-1.813808837891" Y="4.222250488281" />
                  <Point X="-1.80790246582" Y="4.224697265625" />
                  <Point X="-1.714634887695" Y="4.263330078125" />
                  <Point X="-1.696905273438" Y="4.275744628906" />
                  <Point X="-1.686083496094" Y="4.294488769531" />
                  <Point X="-1.684161010742" Y="4.3005859375" />
                  <Point X="-1.653804077148" Y="4.396865234375" />
                  <Point X="-1.651917602539" Y="4.418426757812" />
                  <Point X="-1.689137695313" Y="4.701141113281" />
                  <Point X="-1.685216674805" Y="4.702240234375" />
                  <Point X="-0.968094238281" Y="4.903296386719" />
                  <Point X="-0.924904724121" Y="4.908351074219" />
                  <Point X="-0.224199615479" Y="4.990358398438" />
                  <Point X="-0.053649044037" Y="4.353854492188" />
                  <Point X="-0.042140380859" Y="4.310903320312" />
                  <Point X="-0.02428212738" Y="4.284176757813" />
                  <Point X="0.006155934334" Y="4.273844238281" />
                  <Point X="0.036594047546" Y="4.284176757813" />
                  <Point X="0.054452251434" Y="4.310903320312" />
                  <Point X="0.236188186646" Y="4.989150878906" />
                  <Point X="0.236648498535" Y="4.990868652344" />
                  <Point X="0.860205932617" Y="4.925565429688" />
                  <Point X="0.895939819336" Y="4.916938476562" />
                  <Point X="1.508455932617" Y="4.769058105469" />
                  <Point X="1.531292480469" Y="4.760774902344" />
                  <Point X="1.931033813477" Y="4.615785644531" />
                  <Point X="1.953526489258" Y="4.605267089844" />
                  <Point X="2.338699462891" Y="4.425134277344" />
                  <Point X="2.360436035156" Y="4.412470703125" />
                  <Point X="2.7325234375" Y="4.19569140625" />
                  <Point X="2.753017578125" Y="4.1811171875" />
                  <Point X="3.068740234375" Y="3.956592773438" />
                  <Point X="2.281167724609" Y="2.592477050781" />
                  <Point X="2.229853515625" Y="2.503597900391" />
                  <Point X="2.223573486328" Y="2.486733398438" />
                  <Point X="2.203382324219" Y="2.411228515625" />
                  <Point X="2.202543212891" Y="2.388191162109" />
                  <Point X="2.210416015625" Y="2.322900878906" />
                  <Point X="2.218682128906" Y="2.3008125" />
                  <Point X="2.221240478516" Y="2.297042236328" />
                  <Point X="2.261639892578" Y="2.23750390625" />
                  <Point X="2.278710205078" Y="2.221645507812" />
                  <Point X="2.338248535156" Y="2.181246337891" />
                  <Point X="2.360336914062" Y="2.172980224609" />
                  <Point X="2.364471435547" Y="2.172481445312" />
                  <Point X="2.42976171875" Y="2.164608642578" />
                  <Point X="2.453445800781" Y="2.167225097656" />
                  <Point X="2.528950683594" Y="2.187416015625" />
                  <Point X="2.541033935547" Y="2.192417480469" />
                  <Point X="3.944538085938" Y="3.002730957031" />
                  <Point X="3.994248046875" Y="3.031430908203" />
                  <Point X="4.202591796875" Y="2.741880371094" />
                  <Point X="4.214017089844" Y="2.722999755859" />
                  <Point X="4.387512695312" Y="2.436296142578" />
                  <Point X="3.356176269531" Y="1.644923339844" />
                  <Point X="3.288616210938" Y="1.593082641602" />
                  <Point X="3.275929931641" Y="1.57934387207" />
                  <Point X="3.221589111328" Y="1.508451904297" />
                  <Point X="3.211837646484" Y="1.486916381836" />
                  <Point X="3.191595703125" Y="1.414535522461" />
                  <Point X="3.190779541016" Y="1.390965332031" />
                  <Point X="3.191831787109" Y="1.385865356445" />
                  <Point X="3.208448486328" Y="1.305332519531" />
                  <Point X="3.218508544922" Y="1.283604736328" />
                  <Point X="3.263704101562" Y="1.214909667969" />
                  <Point X="3.280947998047" Y="1.198819824219" />
                  <Point X="3.285095458984" Y="1.196485107422" />
                  <Point X="3.350590087891" Y="1.159617431641" />
                  <Point X="3.374173095703" Y="1.152878417969" />
                  <Point X="3.462726074219" Y="1.141175048828" />
                  <Point X="3.475803710938" Y="1.14117175293" />
                  <Point X="4.809038574219" Y="1.316695556641" />
                  <Point X="4.848975097656" Y="1.32195324707" />
                  <Point X="4.939188476562" Y="0.951385925293" />
                  <Point X="4.9427890625" Y="0.928260131836" />
                  <Point X="4.997858398438" Y="0.574556152344" />
                  <Point X="3.818593017578" Y="0.258572998047" />
                  <Point X="3.741167724609" Y="0.237826919556" />
                  <Point X="3.723577392578" Y="0.229634857178" />
                  <Point X="3.636576904297" Y="0.179346878052" />
                  <Point X="3.618959228516" Y="0.162714324951" />
                  <Point X="3.566759033203" Y="0.096198890686" />
                  <Point X="3.556985107422" Y="0.07473513031" />
                  <Point X="3.555883056641" Y="0.068981483459" />
                  <Point X="3.538482910156" Y="-0.021875303268" />
                  <Point X="3.539584960938" Y="-0.046438282013" />
                  <Point X="3.556985107422" Y="-0.137295227051" />
                  <Point X="3.566759033203" Y="-0.158758987427" />
                  <Point X="3.570064697266" Y="-0.162971237183" />
                  <Point X="3.622264892578" Y="-0.229486679077" />
                  <Point X="3.642086425781" Y="-0.245091522217" />
                  <Point X="3.729086914062" Y="-0.295379486084" />
                  <Point X="3.741167724609" Y="-0.300387023926" />
                  <Point X="4.9638046875" Y="-0.627991516113" />
                  <Point X="4.998068359375" Y="-0.637172424316" />
                  <Point X="4.948431640625" Y="-0.966399230957" />
                  <Point X="4.943818847656" Y="-0.986613586426" />
                  <Point X="4.874545898438" Y="-1.290178344727" />
                  <Point X="3.501773925781" Y="-1.109449462891" />
                  <Point X="3.411982177734" Y="-1.097628295898" />
                  <Point X="3.384023193359" Y="-1.10069152832" />
                  <Point X="3.213271972656" Y="-1.137804931641" />
                  <Point X="3.1979453125" Y="-1.143923217773" />
                  <Point X="3.178909423828" Y="-1.162557861328" />
                  <Point X="3.075701171875" Y="-1.286685424805" />
                  <Point X="3.067953613281" Y="-1.299518066406" />
                  <Point X="3.063421142578" Y="-1.324250366211" />
                  <Point X="3.04862890625" Y="-1.485000854492" />
                  <Point X="3.049849121094" Y="-1.501458129883" />
                  <Point X="3.062344726562" Y="-1.52592956543" />
                  <Point X="3.156840820312" Y="-1.672912353516" />
                  <Point X="3.168460693359" Y="-1.685540649414" />
                  <Point X="4.303075195312" Y="-2.556161132812" />
                  <Point X="4.33907421875" Y="-2.583784179688" />
                  <Point X="4.204133300781" Y="-2.802139160156" />
                  <Point X="4.194590820313" Y="-2.815697509766" />
                  <Point X="4.056688476562" Y="-3.011638427734" />
                  <Point X="2.833426269531" Y="-2.305387207031" />
                  <Point X="2.753454589844" Y="-2.259215576172" />
                  <Point X="2.724471435547" Y="-2.250988769531" />
                  <Point X="2.521250488281" Y="-2.214287109375" />
                  <Point X="2.504749267578" Y="-2.214074462891" />
                  <Point X="2.478386230469" Y="-2.224871582031" />
                  <Point X="2.309559570312" Y="-2.313723876953" />
                  <Point X="2.297490722656" Y="-2.322614990234" />
                  <Point X="2.282973144531" Y="-2.345374755859" />
                  <Point X="2.194120849609" Y="-2.514201660156" />
                  <Point X="2.188950439453" Y="-2.529873291016" />
                  <Point X="2.191487060547" Y="-2.559243652344" />
                  <Point X="2.228188720703" Y="-2.76246484375" />
                  <Point X="2.234091552734" Y="-2.778578613281" />
                  <Point X="2.963195556641" Y="-4.041423339844" />
                  <Point X="2.986673583984" Y="-4.082088623047" />
                  <Point X="2.835296630859" Y="-4.190213378906" />
                  <Point X="2.824633544922" Y="-4.197115722656" />
                  <Point X="2.679775878906" Y="-4.290879394531" />
                  <Point X="1.744203125" Y="-3.071617431641" />
                  <Point X="1.683177490234" Y="-2.992087402344" />
                  <Point X="1.657856567383" Y="-2.972307373047" />
                  <Point X="1.45742590332" Y="-2.843448730469" />
                  <Point X="1.442262207031" Y="-2.8369375" />
                  <Point X="1.411923217773" Y="-2.836994384766" />
                  <Point X="1.192718017578" Y="-2.857166015625" />
                  <Point X="1.178165527344" Y="-2.86076171875" />
                  <Point X="1.154614013672" Y="-2.877421630859" />
                  <Point X="0.985349060059" Y="-3.018159667969" />
                  <Point X="0.974574951172" Y="-3.030659667969" />
                  <Point X="0.965251953125" Y="-3.060731445312" />
                  <Point X="0.91464251709" Y="-3.29357421875" />
                  <Point X="0.91392956543" Y="-3.310720214844" />
                  <Point X="1.120552001953" Y="-4.880173828125" />
                  <Point X="1.127642211914" Y="-4.934028320312" />
                  <Point X="0.994346191406" Y="-4.963246582031" />
                  <Point X="0.984488891602" Y="-4.965037597656" />
                  <Point X="0.860200378418" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#122" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.011508702859" Y="4.398075539562" Z="0.15" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.15" />
                  <Point X="-0.936872690232" Y="4.989292220936" Z="0.15" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.15" />
                  <Point X="-1.704870465505" Y="4.78166399326" Z="0.15" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.15" />
                  <Point X="-1.747969421667" Y="4.749468478074" Z="0.15" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.15" />
                  <Point X="-1.738002751349" Y="4.346901142356" Z="0.15" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.15" />
                  <Point X="-1.833192735946" Y="4.30217107463" Z="0.15" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.15" />
                  <Point X="-1.928644595176" Y="4.346339371174" Z="0.15" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.15" />
                  <Point X="-1.946224711962" Y="4.36481210394" Z="0.15" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.15" />
                  <Point X="-2.747686667459" Y="4.269113413819" Z="0.15" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.15" />
                  <Point X="-3.343404395585" Y="3.820583624271" Z="0.15" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.15" />
                  <Point X="-3.356208371882" Y="3.754643037716" Z="0.15" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.15" />
                  <Point X="-2.994485763037" Y="3.059858457517" Z="0.15" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.15" />
                  <Point X="-3.051147157687" Y="2.997656410199" Z="0.15" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.15" />
                  <Point X="-3.135218008089" Y="3.001078936136" Z="0.15" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.15" />
                  <Point X="-3.179216310581" Y="3.023985576864" Z="0.15" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.15" />
                  <Point X="-4.183011954007" Y="2.878066195908" Z="0.15" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.15" />
                  <Point X="-4.527406680284" Y="2.298588571582" Z="0.15" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.15" />
                  <Point X="-4.496967274814" Y="2.225006398789" Z="0.15" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.15" />
                  <Point X="-3.668593594178" Y="1.557106749496" Z="0.15" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.15" />
                  <Point X="-3.690002024496" Y="1.497743894349" Z="0.15" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.15" />
                  <Point X="-3.74923782805" Y="1.475985973062" Z="0.15" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.15" />
                  <Point X="-3.816238884618" Y="1.483171776995" Z="0.15" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.15" />
                  <Point X="-4.963520230196" Y="1.072293295875" Z="0.15" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.15" />
                  <Point X="-5.055116201663" Y="0.481857461341" Z="0.15" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.15" />
                  <Point X="-4.971961173475" Y="0.422965458243" Z="0.15" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.15" />
                  <Point X="-3.55046068527" Y="0.030954106588" Z="0.15" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.15" />
                  <Point X="-3.54010769313" Y="0.001775156517" Z="0.15" />
                  <Point X="-3.539556741714" Y="0" Z="0.15" />
                  <Point X="-3.548256908005" Y="-0.028031794544" Z="0.15" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.15" />
                  <Point X="-3.574907909853" Y="-0.047921876546" Z="0.15" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.15" />
                  <Point X="-3.664926615297" Y="-0.072746598513" Z="0.15" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.15" />
                  <Point X="-4.987287092" Y="-0.957330728527" Z="0.15" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.15" />
                  <Point X="-4.854988431792" Y="-1.489506992252" Z="0.15" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.15" />
                  <Point X="-4.749962810529" Y="-1.508397429029" Z="0.15" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.15" />
                  <Point X="-3.194253479099" Y="-1.321521547764" Z="0.15" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.15" />
                  <Point X="-3.199921959956" Y="-1.350425562282" Z="0.15" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.15" />
                  <Point X="-3.277952523737" Y="-1.411720043219" Z="0.15" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.15" />
                  <Point X="-4.226837027934" Y="-2.814572192927" Z="0.15" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.15" />
                  <Point X="-3.882911418271" Y="-3.272766588511" Z="0.15" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.15" />
                  <Point X="-3.785448550137" Y="-3.255591123261" Z="0.15" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.15" />
                  <Point X="-2.556524164256" Y="-2.571806304876" Z="0.15" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.15" />
                  <Point X="-2.599825889907" Y="-2.649629832355" Z="0.15" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.15" />
                  <Point X="-2.914860386648" Y="-4.158726613664" Z="0.15" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.15" />
                  <Point X="-2.477283757517" Y="-4.433340229116" Z="0.15" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.15" />
                  <Point X="-2.43772407707" Y="-4.43208659527" Z="0.15" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.15" />
                  <Point X="-1.983619209206" Y="-3.99434975326" Z="0.15" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.15" />
                  <Point X="-1.67710446766" Y="-4.003167467923" Z="0.15" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.15" />
                  <Point X="-1.439297858026" Y="-4.19675763859" Z="0.15" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.15" />
                  <Point X="-1.368483254394" Y="-4.495110101238" Z="0.15" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.15" />
                  <Point X="-1.367750313795" Y="-4.535045545261" Z="0.15" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.15" />
                  <Point X="-1.135012120963" Y="-4.951052599742" Z="0.15" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.15" />
                  <Point X="-0.835518759953" Y="-5.010298071087" Z="0.15" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.15" />
                  <Point X="-0.793811413325" Y="-4.924728651172" Z="0.15" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.15" />
                  <Point X="-0.26310990206" Y="-3.296921753366" Z="0.15" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.15" />
                  <Point X="-0.015088830121" Y="-3.208922384478" Z="0.15" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.15" />
                  <Point X="0.238270249241" Y="-3.27818966081" Z="0.15" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.15" />
                  <Point X="0.407335957372" Y="-3.504724002402" Z="0.15" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.15" />
                  <Point X="0.440943447076" Y="-3.607807378644" Z="0.15" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.15" />
                  <Point X="0.987269864611" Y="-4.982952496531" Z="0.15" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.15" />
                  <Point X="1.166390058479" Y="-4.944092599902" Z="0.15" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.15" />
                  <Point X="1.163968286797" Y="-4.8423672347" Z="0.15" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.15" />
                  <Point X="1.007955103049" Y="-3.040071127358" Z="0.15" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.15" />
                  <Point X="1.180424798384" Y="-2.884587897561" Z="0.15" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.15" />
                  <Point X="1.410348949887" Y="-2.855503978055" Z="0.15" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.15" />
                  <Point X="1.62466130522" Y="-2.983085105949" Z="0.15" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.15" />
                  <Point X="1.698379597175" Y="-3.070775494288" Z="0.15" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.15" />
                  <Point X="2.845646816922" Y="-4.207809783023" Z="0.15" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.15" />
                  <Point X="3.0352424511" Y="-4.073164982596" Z="0.15" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.15" />
                  <Point X="3.000340966759" Y="-3.985143387291" Z="0.15" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.15" />
                  <Point X="2.234535298159" Y="-2.519077473294" Z="0.15" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.15" />
                  <Point X="2.3210634839" Y="-2.337381411913" Z="0.15" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.15" />
                  <Point X="2.495516966459" Y="-2.237837826067" Z="0.15" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.15" />
                  <Point X="2.709429330218" Y="-2.268912711332" Z="0.15" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.15" />
                  <Point X="2.802270209816" Y="-2.317408533515" Z="0.15" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.15" />
                  <Point X="4.229323494738" Y="-2.813194915548" Z="0.15" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.15" />
                  <Point X="4.38971032048" Y="-2.555716420361" Z="0.15" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.15" />
                  <Point X="4.327357336097" Y="-2.485213481267" Z="0.15" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.15" />
                  <Point X="3.098246623784" Y="-1.467610399045" Z="0.15" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.15" />
                  <Point X="3.107054717467" Y="-1.297552003838" Z="0.15" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.15" />
                  <Point X="3.211199707681" Y="-1.163244746868" Z="0.15" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.15" />
                  <Point X="3.388486774659" Y="-1.118270693293" Z="0.15" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.15" />
                  <Point X="3.489091522686" Y="-1.127741720224" Z="0.15" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.15" />
                  <Point X="4.986410051966" Y="-0.966457639766" Z="0.15" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.15" />
                  <Point X="5.044645683828" Y="-0.591510038319" Z="0.15" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.15" />
                  <Point X="4.970589745638" Y="-0.548415230941" Z="0.15" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.15" />
                  <Point X="3.660952157854" Y="-0.170522764267" Z="0.15" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.15" />
                  <Point X="3.603242389607" Y="-0.100822726381" Z="0.15" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.15" />
                  <Point X="3.582536615348" Y="-0.005753651719" Z="0.15" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.15" />
                  <Point X="3.598834835076" Y="0.090856879526" Z="0.15" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.15" />
                  <Point X="3.652137048793" Y="0.163126012194" Z="0.15" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.15" />
                  <Point X="3.742443256498" Y="0.217626079975" Z="0.15" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.15" />
                  <Point X="3.825378071668" Y="0.241556701828" Z="0.15" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.15" />
                  <Point X="4.986037699857" Y="0.967232133109" Z="0.15" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.15" />
                  <Point X="4.886820031753" Y="1.383874968824" Z="0.15" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.15" />
                  <Point X="4.796356313982" Y="1.397547841595" Z="0.15" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.15" />
                  <Point X="3.374568376336" Y="1.233727445843" Z="0.15" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.15" />
                  <Point X="3.303466093602" Y="1.271336324322" Z="0.15" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.15" />
                  <Point X="3.25412306934" Y="1.342366178082" Z="0.15" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.15" />
                  <Point X="3.234644310549" Y="1.42724933336" Z="0.15" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.15" />
                  <Point X="3.253834124319" Y="1.504730072772" Z="0.15" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.15" />
                  <Point X="3.309456934829" Y="1.580205752644" Z="0.15" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.15" />
                  <Point X="3.380458303065" Y="1.636535820832" Z="0.15" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.15" />
                  <Point X="4.250638612368" Y="2.780165975547" Z="0.15" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.15" />
                  <Point X="4.016311573031" Y="3.109099700803" Z="0.15" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.15" />
                  <Point X="3.913382100258" Y="3.077312237825" Z="0.15" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.15" />
                  <Point X="2.434373113763" Y="2.246807798973" Z="0.15" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.15" />
                  <Point X="2.364301394354" Y="2.253401971021" Z="0.15" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.15" />
                  <Point X="2.300628430385" Y="2.294299730617" Z="0.15" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.15" />
                  <Point X="2.256458869618" Y="2.356396429997" Z="0.15" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.15" />
                  <Point X="2.246027731738" Y="2.425457049478" Z="0.15" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.15" />
                  <Point X="2.265720143684" Y="2.505096462119" Z="0.15" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.15" />
                  <Point X="2.318313115035" Y="2.598756981697" Z="0.15" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.15" />
                  <Point X="2.775838676083" Y="4.253144432393" Z="0.15" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.15" />
                  <Point X="2.379449882441" Y="4.486953207086" Z="0.15" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.15" />
                  <Point X="1.968551955935" Y="4.681838926665" Z="0.15" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.15" />
                  <Point X="1.542185104964" Y="4.839059007699" Z="0.15" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.15" />
                  <Point X="0.901518366163" Y="4.996821896732" Z="0.15" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.15" />
                  <Point X="0.233105642505" Y="5.072148512605" Z="0.15" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.15" />
                  <Point X="0.181735858694" Y="5.033371966389" Z="0.15" />
                  <Point X="0" Y="4.355124473572" Z="0.15" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>