<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#191" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2749" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="0" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000475830078" Y="0.004715820312" />
                  <Width Value="9.783896972656" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.855264221191" Y="-4.602142578125" />
                  <Point X="0.563302062988" Y="-3.512524414063" />
                  <Point X="0.557721191406" Y="-3.497141845703" />
                  <Point X="0.542363037109" Y="-3.467377197266" />
                  <Point X="0.423030731201" Y="-3.295441894531" />
                  <Point X="0.378635528564" Y="-3.231476806641" />
                  <Point X="0.356751586914" Y="-3.209021484375" />
                  <Point X="0.330496398926" Y="-3.189777587891" />
                  <Point X="0.302495330811" Y="-3.175669189453" />
                  <Point X="0.11783531189" Y="-3.118357421875" />
                  <Point X="0.049136188507" Y="-3.097035888672" />
                  <Point X="0.020976638794" Y="-3.092766601562" />
                  <Point X="-0.008664840698" Y="-3.092766601562" />
                  <Point X="-0.036824390411" Y="-3.097035888672" />
                  <Point X="-0.221484100342" Y="-3.154347412109" />
                  <Point X="-0.290183380127" Y="-3.175669189453" />
                  <Point X="-0.31818460083" Y="-3.189777587891" />
                  <Point X="-0.344439788818" Y="-3.209021484375" />
                  <Point X="-0.366323577881" Y="-3.231476806641" />
                  <Point X="-0.485655883789" Y="-3.403411865234" />
                  <Point X="-0.530051208496" Y="-3.467377197266" />
                  <Point X="-0.538189025879" Y="-3.481573730469" />
                  <Point X="-0.550990112305" Y="-3.512524414063" />
                  <Point X="-0.659609069824" Y="-3.917895996094" />
                  <Point X="-0.916584594727" Y="-4.876941894531" />
                  <Point X="-1.002342712402" Y="-4.860295898438" />
                  <Point X="-1.079341430664" Y="-4.845350097656" />
                  <Point X="-1.24641796875" Y="-4.802362792969" />
                  <Point X="-1.214963012695" Y="-4.563438476562" />
                  <Point X="-1.214201171875" Y="-4.5479296875" />
                  <Point X="-1.216508666992" Y="-4.516224121094" />
                  <Point X="-1.260623901367" Y="-4.294442382812" />
                  <Point X="-1.277036132812" Y="-4.211932617188" />
                  <Point X="-1.287938354492" Y="-4.182966308594" />
                  <Point X="-1.304010375977" Y="-4.15512890625" />
                  <Point X="-1.32364453125" Y="-4.131204101562" />
                  <Point X="-1.49365612793" Y="-3.982107666016" />
                  <Point X="-1.556905517578" Y="-3.926639404297" />
                  <Point X="-1.583188598633" Y="-3.910295410156" />
                  <Point X="-1.612885864258" Y="-3.897994384766" />
                  <Point X="-1.64302734375" Y="-3.890966308594" />
                  <Point X="-1.868670410156" Y="-3.876176757812" />
                  <Point X="-1.952616577148" Y="-3.870674804688" />
                  <Point X="-1.98341809082" Y="-3.873708496094" />
                  <Point X="-2.014466674805" Y="-3.882028076172" />
                  <Point X="-2.042657836914" Y="-3.894801513672" />
                  <Point X="-2.23067578125" Y="-4.020431396484" />
                  <Point X="-2.300624267578" Y="-4.067169677734" />
                  <Point X="-2.312785644531" Y="-4.076821533203" />
                  <Point X="-2.335102783203" Y="-4.099461914062" />
                  <Point X="-2.396083984375" Y="-4.178934082031" />
                  <Point X="-2.480149169922" Y="-4.288489746094" />
                  <Point X="-2.688853027344" Y="-4.159265625" />
                  <Point X="-2.801708007812" Y="-4.089388427734" />
                  <Point X="-3.094284912109" Y="-3.864114013672" />
                  <Point X="-3.104721923828" Y="-3.856077880859" />
                  <Point X="-2.966340087891" Y="-3.616393798828" />
                  <Point X="-2.423761230469" Y="-2.676619628906" />
                  <Point X="-2.412859130859" Y="-2.647653320312" />
                  <Point X="-2.406588134766" Y="-2.616126953125" />
                  <Point X="-2.405575439453" Y="-2.585192871094" />
                  <Point X="-2.414560058594" Y="-2.555575195312" />
                  <Point X="-2.428777099609" Y="-2.526746337891" />
                  <Point X="-2.4468046875" Y="-2.501589355469" />
                  <Point X="-2.464153320312" Y="-2.484240722656" />
                  <Point X="-2.489310302734" Y="-2.466213134766" />
                  <Point X="-2.518139160156" Y="-2.45199609375" />
                  <Point X="-2.547756835938" Y="-2.443011474609" />
                  <Point X="-2.578690917969" Y="-2.444024169922" />
                  <Point X="-2.610217285156" Y="-2.450295166016" />
                  <Point X="-2.63918359375" Y="-2.461197265625" />
                  <Point X="-2.988808837891" Y="-2.663053466797" />
                  <Point X="-3.818024414062" Y="-3.141801269531" />
                  <Point X="-3.993697998047" Y="-2.911002441406" />
                  <Point X="-4.082859130859" Y="-2.793862792969" />
                  <Point X="-4.292612304688" Y="-2.442138183594" />
                  <Point X="-4.306142578125" Y="-2.419450195312" />
                  <Point X="-4.055291748047" Y="-2.226965332031" />
                  <Point X="-3.105954589844" Y="-1.498513427734" />
                  <Point X="-3.084576904297" Y="-1.475592895508" />
                  <Point X="-3.066612060547" Y="-1.448461425781" />
                  <Point X="-3.053856689453" Y="-1.419832885742" />
                  <Point X="-3.048241210938" Y="-1.398151855469" />
                  <Point X="-3.046152099609" Y="-1.390085693359" />
                  <Point X="-3.04334765625" Y="-1.35965612793" />
                  <Point X="-3.045556640625" Y="-1.327985473633" />
                  <Point X="-3.052557861328" Y="-1.298240356445" />
                  <Point X="-3.068640380859" Y="-1.272256835938" />
                  <Point X="-3.089473144531" Y="-1.248300292969" />
                  <Point X="-3.112972900391" Y="-1.228766601562" />
                  <Point X="-3.132274658203" Y="-1.21740637207" />
                  <Point X="-3.139455566406" Y="-1.213180053711" />
                  <Point X="-3.168718994141" Y="-1.201956054688" />
                  <Point X="-3.200606445313" Y="-1.195474609375" />
                  <Point X="-3.231929199219" Y="-1.194383789062" />
                  <Point X="-3.673297851562" Y="-1.252491088867" />
                  <Point X="-4.732102050781" Y="-1.391885253906" />
                  <Point X="-4.799204589844" Y="-1.129180786133" />
                  <Point X="-4.834077636719" Y="-0.99265435791" />
                  <Point X="-4.889572753906" Y="-0.604637756348" />
                  <Point X="-4.892424316406" Y="-0.584698242188" />
                  <Point X="-4.614313964844" Y="-0.510178619385" />
                  <Point X="-3.532875976562" Y="-0.220408447266" />
                  <Point X="-3.517493652344" Y="-0.214827560425" />
                  <Point X="-3.487728759766" Y="-0.199469406128" />
                  <Point X="-3.467501220703" Y="-0.18543031311" />
                  <Point X="-3.459975830078" Y="-0.180207199097" />
                  <Point X="-3.437520507812" Y="-0.158323577881" />
                  <Point X="-3.418276123047" Y="-0.132067489624" />
                  <Point X="-3.404168212891" Y="-0.104067008972" />
                  <Point X="-3.397425537109" Y="-0.082342315674" />
                  <Point X="-3.394917236328" Y="-0.07426007843" />
                  <Point X="-3.390647705078" Y="-0.046098556519" />
                  <Point X="-3.390648193359" Y="-0.016457229614" />
                  <Point X="-3.394917480469" Y="0.011700194359" />
                  <Point X="-3.401659912109" Y="0.033424892426" />
                  <Point X="-3.404168457031" Y="0.041507125854" />
                  <Point X="-3.41827734375" Y="0.06950957489" />
                  <Point X="-3.437521240234" Y="0.095764305115" />
                  <Point X="-3.459976074219" Y="0.117647621155" />
                  <Point X="-3.480203613281" Y="0.131686569214" />
                  <Point X="-3.489307373047" Y="0.137274124146" />
                  <Point X="-3.512400634766" Y="0.149717300415" />
                  <Point X="-3.532875976562" Y="0.157848251343" />
                  <Point X="-3.935204345703" Y="0.265651916504" />
                  <Point X="-4.89181640625" Y="0.521975280762" />
                  <Point X="-4.846961914062" Y="0.825098571777" />
                  <Point X="-4.824487792969" Y="0.976975280762" />
                  <Point X="-4.712777832031" Y="1.38921875" />
                  <Point X="-4.703551269531" Y="1.423267822266" />
                  <Point X="-4.5408671875" Y="1.401850219727" />
                  <Point X="-3.765666259766" Y="1.29979284668" />
                  <Point X="-3.744986083984" Y="1.299341674805" />
                  <Point X="-3.723424560547" Y="1.301228149414" />
                  <Point X="-3.703137695312" Y="1.305263427734" />
                  <Point X="-3.658367675781" Y="1.319379394531" />
                  <Point X="-3.641711669922" Y="1.324630981445" />
                  <Point X="-3.622778564453" Y="1.332961791992" />
                  <Point X="-3.604034179688" Y="1.343783935547" />
                  <Point X="-3.587353271484" Y="1.356015136719" />
                  <Point X="-3.57371484375" Y="1.371566894531" />
                  <Point X="-3.561300292969" Y="1.389296875" />
                  <Point X="-3.551351318359" Y="1.407431274414" />
                  <Point X="-3.533386962891" Y="1.45080065918" />
                  <Point X="-3.526703857422" Y="1.466935546875" />
                  <Point X="-3.520915527344" Y="1.486794433594" />
                  <Point X="-3.517157226562" Y="1.508109130859" />
                  <Point X="-3.5158046875" Y="1.528749389648" />
                  <Point X="-3.518951171875" Y="1.549193237305" />
                  <Point X="-3.524552978516" Y="1.570099365234" />
                  <Point X="-3.532049804688" Y="1.589377563477" />
                  <Point X="-3.553725585938" Y="1.631016357422" />
                  <Point X="-3.561789550781" Y="1.646507324219" />
                  <Point X="-3.57328125" Y="1.663705932617" />
                  <Point X="-3.587193847656" Y="1.680286376953" />
                  <Point X="-3.602135986328" Y="1.694590087891" />
                  <Point X="-3.832912597656" Y="1.871671264648" />
                  <Point X="-4.351859863281" Y="2.269874267578" />
                  <Point X="-4.168477539062" Y="2.584052734375" />
                  <Point X="-4.081153808594" Y="2.733659423828" />
                  <Point X="-3.785239013672" Y="3.114016113281" />
                  <Point X="-3.750505126953" Y="3.158661865234" />
                  <Point X="-3.682763916016" Y="3.119551513672" />
                  <Point X="-3.206656982422" Y="2.844670898438" />
                  <Point X="-3.187724609375" Y="2.836340332031" />
                  <Point X="-3.167082519531" Y="2.829831787109" />
                  <Point X="-3.146794677734" Y="2.825796386719" />
                  <Point X="-3.084442382812" Y="2.820341308594" />
                  <Point X="-3.061245361328" Y="2.818311767578" />
                  <Point X="-3.040565185547" Y="2.818762939453" />
                  <Point X="-3.019106445312" Y="2.821588134766" />
                  <Point X="-2.999014648438" Y="2.826504638672" />
                  <Point X="-2.980463134766" Y="2.835653320313" />
                  <Point X="-2.962208740234" Y="2.847282714844" />
                  <Point X="-2.946077392578" Y="2.860229492188" />
                  <Point X="-2.901819091797" Y="2.904487548828" />
                  <Point X="-2.885353759766" Y="2.920952880859" />
                  <Point X="-2.872406738281" Y="2.937084472656" />
                  <Point X="-2.86077734375" Y="2.955338867188" />
                  <Point X="-2.851628662109" Y="2.973890380859" />
                  <Point X="-2.846712158203" Y="2.993982177734" />
                  <Point X="-2.843886962891" Y="3.015440917969" />
                  <Point X="-2.843435791016" Y="3.03612109375" />
                  <Point X="-2.848890869141" Y="3.098473632812" />
                  <Point X="-2.850920410156" Y="3.121670654297" />
                  <Point X="-2.854955810547" Y="3.141958496094" />
                  <Point X="-2.861464355469" Y="3.162600585938" />
                  <Point X="-2.869794921875" Y="3.181532958984" />
                  <Point X="-2.972059082031" Y="3.358659423828" />
                  <Point X="-3.183332763672" Y="3.724596435547" />
                  <Point X="-2.852711181641" Y="3.978080566406" />
                  <Point X="-2.700625732422" Y="4.094683837891" />
                  <Point X="-2.234570800781" Y="4.353613769531" />
                  <Point X="-2.167036621094" Y="4.391134277344" />
                  <Point X="-2.04319519043" Y="4.229740722656" />
                  <Point X="-2.028891723633" Y="4.214798828125" />
                  <Point X="-2.012311889648" Y="4.20088671875" />
                  <Point X="-1.99511328125" Y="4.189395019531" />
                  <Point X="-1.925715209961" Y="4.153268554688" />
                  <Point X="-1.899897094727" Y="4.139828125" />
                  <Point X="-1.880619506836" Y="4.132331542969" />
                  <Point X="-1.859712768555" Y="4.126729492187" />
                  <Point X="-1.839268920898" Y="4.123582519531" />
                  <Point X="-1.818628540039" Y="4.124935546875" />
                  <Point X="-1.797313232422" Y="4.128693847656" />
                  <Point X="-1.777453613281" Y="4.134481933594" />
                  <Point X="-1.705171020508" Y="4.164422851562" />
                  <Point X="-1.678279541016" Y="4.175561523437" />
                  <Point X="-1.660144775391" Y="4.185510742187" />
                  <Point X="-1.642415161133" Y="4.197925292969" />
                  <Point X="-1.626863769531" Y="4.211563476563" />
                  <Point X="-1.6146328125" Y="4.228244628906" />
                  <Point X="-1.603810913086" Y="4.246988769531" />
                  <Point X="-1.59548046875" Y="4.265921875" />
                  <Point X="-1.571953857422" Y="4.340538574219" />
                  <Point X="-1.563200927734" Y="4.368298339844" />
                  <Point X="-1.559165649414" Y="4.388584960938" />
                  <Point X="-1.557279174805" Y="4.410146484375" />
                  <Point X="-1.55773034668" Y="4.430826660156" />
                  <Point X="-1.569356689453" Y="4.519137207031" />
                  <Point X="-1.584201904297" Y="4.631897949219" />
                  <Point X="-1.14651965332" Y="4.754608886719" />
                  <Point X="-0.949634765625" Y="4.80980859375" />
                  <Point X="-0.384639831543" Y="4.875932617188" />
                  <Point X="-0.294710784912" Y="4.886457519531" />
                  <Point X="-0.27474887085" Y="4.811958496094" />
                  <Point X="-0.133903305054" Y="4.286315429688" />
                  <Point X="-0.121129844666" Y="4.258124023438" />
                  <Point X="-0.10327155304" Y="4.231397460938" />
                  <Point X="-0.082114532471" Y="4.20880859375" />
                  <Point X="-0.054819446564" Y="4.19421875" />
                  <Point X="-0.024381370544" Y="4.183886230469" />
                  <Point X="0.006155906677" Y="4.178844238281" />
                  <Point X="0.03669316864" Y="4.183886230469" />
                  <Point X="0.067131248474" Y="4.19421875" />
                  <Point X="0.094426467896" Y="4.20880859375" />
                  <Point X="0.115583503723" Y="4.231397460938" />
                  <Point X="0.133441802979" Y="4.258124023438" />
                  <Point X="0.146215255737" Y="4.286315429688" />
                  <Point X="0.198614074707" Y="4.48187109375" />
                  <Point X="0.307419342041" Y="4.8879375" />
                  <Point X="0.67212487793" Y="4.849743164062" />
                  <Point X="0.84404107666" Y="4.831738769531" />
                  <Point X="1.311480957031" Y="4.718884277344" />
                  <Point X="1.481026611328" Y="4.677950683594" />
                  <Point X="1.784884155273" Y="4.567739257812" />
                  <Point X="1.894645629883" Y="4.527928222656" />
                  <Point X="2.188849853516" Y="4.390338378906" />
                  <Point X="2.294576416016" Y="4.340893554688" />
                  <Point X="2.578813720703" Y="4.175296386719" />
                  <Point X="2.680977539062" Y="4.115775390625" />
                  <Point X="2.943260009766" Y="3.929254882812" />
                  <Point X="2.775575439453" Y="3.638816162109" />
                  <Point X="2.147581054688" Y="2.551097900391" />
                  <Point X="2.142075683594" Y="2.539930664062" />
                  <Point X="2.133076904297" Y="2.516056884766" />
                  <Point X="2.117428710937" Y="2.457540527344" />
                  <Point X="2.111607177734" Y="2.435770507813" />
                  <Point X="2.108619384766" Y="2.417934326172" />
                  <Point X="2.107728027344" Y="2.380953125" />
                  <Point X="2.113829589844" Y="2.330353027344" />
                  <Point X="2.116099365234" Y="2.311528320312" />
                  <Point X="2.121441894531" Y="2.289605712891" />
                  <Point X="2.129708007812" Y="2.267516601562" />
                  <Point X="2.140070800781" Y="2.247471191406" />
                  <Point X="2.171380371094" Y="2.201328857422" />
                  <Point X="2.183028564453" Y="2.184162597656" />
                  <Point X="2.19446484375" Y="2.170328613281" />
                  <Point X="2.221598876953" Y="2.145592285156" />
                  <Point X="2.267741210938" Y="2.114282714844" />
                  <Point X="2.284907470703" Y="2.102634765625" />
                  <Point X="2.304952392578" Y="2.092271972656" />
                  <Point X="2.327040527344" Y="2.084006103516" />
                  <Point X="2.348963623047" Y="2.078663574219" />
                  <Point X="2.399563720703" Y="2.072562011719" />
                  <Point X="2.408589599609" Y="2.071907714844" />
                  <Point X="2.446317138672" Y="2.070975341797" />
                  <Point X="2.473206542969" Y="2.074171142578" />
                  <Point X="2.531722900391" Y="2.089819335938" />
                  <Point X="2.553492919922" Y="2.095640869141" />
                  <Point X="2.565283447266" Y="2.099638183594" />
                  <Point X="2.588533935547" Y="2.110145019531" />
                  <Point X="2.993198974609" Y="2.343778320312" />
                  <Point X="3.967326416016" Y="2.906191162109" />
                  <Point X="4.062670654297" Y="2.773684570312" />
                  <Point X="4.123270019531" Y="2.689465087891" />
                  <Point X="4.262198730469" Y="2.459884033203" />
                  <Point X="4.057597412109" Y="2.302887695312" />
                  <Point X="3.230783935547" Y="1.668451171875" />
                  <Point X="3.221424560547" Y="1.660241455078" />
                  <Point X="3.203973632812" Y="1.641627685547" />
                  <Point X="3.161859130859" Y="1.586686279297" />
                  <Point X="3.14619140625" Y="1.566246459961" />
                  <Point X="3.13660546875" Y="1.550911376953" />
                  <Point X="3.121629638672" Y="1.517086181641" />
                  <Point X="3.105941894531" Y="1.460990478516" />
                  <Point X="3.100105712891" Y="1.440121459961" />
                  <Point X="3.09665234375" Y="1.417821899414" />
                  <Point X="3.095836425781" Y="1.394252075195" />
                  <Point X="3.097739501953" Y="1.371768066406" />
                  <Point X="3.110617431641" Y="1.309354736328" />
                  <Point X="3.115408447266" Y="1.286135253906" />
                  <Point X="3.120679931641" Y="1.268977783203" />
                  <Point X="3.136282470703" Y="1.235740356445" />
                  <Point X="3.171309082031" Y="1.182501464844" />
                  <Point X="3.184340087891" Y="1.16269519043" />
                  <Point X="3.198893554688" Y="1.145450439453" />
                  <Point X="3.216137451172" Y="1.129360595703" />
                  <Point X="3.234347167969" Y="1.116034790039" />
                  <Point X="3.28510546875" Y="1.087462402344" />
                  <Point X="3.303989257812" Y="1.076832519531" />
                  <Point X="3.320521240234" Y="1.069501586914" />
                  <Point X="3.356118164062" Y="1.059438598633" />
                  <Point X="3.424746826172" Y="1.050368408203" />
                  <Point X="3.450278808594" Y="1.046994018555" />
                  <Point X="3.462702148438" Y="1.046175048828" />
                  <Point X="3.488203613281" Y="1.04698449707" />
                  <Point X="3.872608154297" Y="1.097592407227" />
                  <Point X="4.776839355469" Y="1.21663671875" />
                  <Point X="4.819908203125" Y="1.03972265625" />
                  <Point X="4.845936035156" Y="0.932809692383" />
                  <Point X="4.890864746094" Y="0.644238525391" />
                  <Point X="4.664125976563" Y="0.583484069824" />
                  <Point X="3.716579833984" Y="0.32958984375" />
                  <Point X="3.704791259766" Y="0.325586517334" />
                  <Point X="3.681545654297" Y="0.315068054199" />
                  <Point X="3.614120117188" Y="0.276094787598" />
                  <Point X="3.589035644531" Y="0.261595489502" />
                  <Point X="3.574310791016" Y="0.251095825195" />
                  <Point X="3.547531005859" Y="0.225576522827" />
                  <Point X="3.507075683594" Y="0.174027130127" />
                  <Point X="3.492025146484" Y="0.154849029541" />
                  <Point X="3.480301025391" Y="0.135569061279" />
                  <Point X="3.47052734375" Y="0.114105743408" />
                  <Point X="3.463680908203" Y="0.09260433197" />
                  <Point X="3.450195800781" Y="0.022190126419" />
                  <Point X="3.445178710938" Y="-0.004006166458" />
                  <Point X="3.443483154297" Y="-0.021873718262" />
                  <Point X="3.445178710938" Y="-0.058553718567" />
                  <Point X="3.4586640625" Y="-0.128967926025" />
                  <Point X="3.463680908203" Y="-0.155164367676" />
                  <Point X="3.47052734375" Y="-0.176665786743" />
                  <Point X="3.480301269531" Y="-0.19812940979" />
                  <Point X="3.492025146484" Y="-0.217408905029" />
                  <Point X="3.53248046875" Y="-0.268958465576" />
                  <Point X="3.547531005859" Y="-0.288136566162" />
                  <Point X="3.559998779297" Y="-0.301235473633" />
                  <Point X="3.589035644531" Y="-0.324155517578" />
                  <Point X="3.656461181641" Y="-0.363128753662" />
                  <Point X="3.681545654297" Y="-0.377628082275" />
                  <Point X="3.692710205078" Y="-0.383138977051" />
                  <Point X="3.716579833984" Y="-0.392150024414" />
                  <Point X="4.069096191406" Y="-0.48660647583" />
                  <Point X="4.89147265625" Y="-0.706961425781" />
                  <Point X="4.869555664062" Y="-0.852332580566" />
                  <Point X="4.855022460938" Y="-0.948725769043" />
                  <Point X="4.801173828125" Y="-1.184698974609" />
                  <Point X="4.523269042969" Y="-1.148112304688" />
                  <Point X="3.424382080078" Y="-1.003440979004" />
                  <Point X="3.408035644531" Y="-1.002710266113" />
                  <Point X="3.374658691406" Y="-1.005508666992" />
                  <Point X="3.242326171875" Y="-1.034271728516" />
                  <Point X="3.193094482422" Y="-1.044972290039" />
                  <Point X="3.163974365234" Y="-1.056596801758" />
                  <Point X="3.136147705078" Y="-1.073489013672" />
                  <Point X="3.112397460938" Y="-1.093960083008" />
                  <Point X="3.032410888672" Y="-1.190159057617" />
                  <Point X="3.002653320312" Y="-1.225948120117" />
                  <Point X="2.987932373047" Y="-1.250330810547" />
                  <Point X="2.976589111328" Y="-1.277716430664" />
                  <Point X="2.969757568359" Y="-1.305365478516" />
                  <Point X="2.958293457031" Y="-1.429947631836" />
                  <Point X="2.954028564453" Y="-1.476295776367" />
                  <Point X="2.956347412109" Y="-1.507564208984" />
                  <Point X="2.964078857422" Y="-1.539184936523" />
                  <Point X="2.976450195312" Y="-1.567996459961" />
                  <Point X="3.049685058594" Y="-1.681908325195" />
                  <Point X="3.076930419922" Y="-1.724287109375" />
                  <Point X="3.086932861328" Y="-1.737239013672" />
                  <Point X="3.110628417969" Y="-1.760909057617" />
                  <Point X="3.437765869141" Y="-2.011930786133" />
                  <Point X="4.213122558594" Y="-2.6068828125" />
                  <Point X="4.165776855469" Y="-2.683495361328" />
                  <Point X="4.124809082031" Y="-2.749787841797" />
                  <Point X="4.028981445312" Y="-2.8859453125" />
                  <Point X="3.779661621094" Y="-2.742000244141" />
                  <Point X="2.800954589844" Y="-2.176943115234" />
                  <Point X="2.786131591797" Y="-2.170012451172" />
                  <Point X="2.754224609375" Y="-2.159825195312" />
                  <Point X="2.596727783203" Y="-2.131381591797" />
                  <Point X="2.538134277344" Y="-2.120799560547" />
                  <Point X="2.506781982422" Y="-2.120395263672" />
                  <Point X="2.474609130859" Y="-2.125353027344" />
                  <Point X="2.444833251953" Y="-2.135176757812" />
                  <Point X="2.3139921875" Y="-2.204037353516" />
                  <Point X="2.265315185547" Y="-2.229655761719" />
                  <Point X="2.242384033203" Y="-2.246549072266" />
                  <Point X="2.221424316406" Y="-2.267509033203" />
                  <Point X="2.204531738281" Y="-2.290439453125" />
                  <Point X="2.135670898438" Y="-2.421280517578" />
                  <Point X="2.110052734375" Y="-2.469957519531" />
                  <Point X="2.100229003906" Y="-2.499733398438" />
                  <Point X="2.095271240234" Y="-2.531906005859" />
                  <Point X="2.095675537109" Y="-2.563258300781" />
                  <Point X="2.124119140625" Y="-2.720754882812" />
                  <Point X="2.134701171875" Y="-2.779348632812" />
                  <Point X="2.138985595703" Y="-2.795141845703" />
                  <Point X="2.151819091797" Y="-2.826078613281" />
                  <Point X="2.362037841797" Y="-3.190187988281" />
                  <Point X="2.861283447266" Y="-4.054906738281" />
                  <Point X="2.830463623047" Y="-4.076920654297" />
                  <Point X="2.781851318359" Y="-4.111643066406" />
                  <Point X="2.701765380859" Y="-4.163481445312" />
                  <Point X="2.505389892578" Y="-3.907560546875" />
                  <Point X="1.758546142578" Y="-2.934255126953" />
                  <Point X="1.747503540039" Y="-2.922178955078" />
                  <Point X="1.721924194336" Y="-2.900557373047" />
                  <Point X="1.56658996582" Y="-2.800691650391" />
                  <Point X="1.50880078125" Y="-2.763538574219" />
                  <Point X="1.479989746094" Y="-2.751167236328" />
                  <Point X="1.448368774414" Y="-2.743435546875" />
                  <Point X="1.417099609375" Y="-2.741116699219" />
                  <Point X="1.24721496582" Y="-2.756749755859" />
                  <Point X="1.184012695312" Y="-2.762565673828" />
                  <Point X="1.15636315918" Y="-2.769397460938" />
                  <Point X="1.128978027344" Y="-2.780740722656" />
                  <Point X="1.104595825195" Y="-2.795461181641" />
                  <Point X="0.973415466309" Y="-2.904533447266" />
                  <Point X="0.924612121582" Y="-2.945111572266" />
                  <Point X="0.904141662598" Y="-2.968861328125" />
                  <Point X="0.887249206543" Y="-2.996687744141" />
                  <Point X="0.875624328613" Y="-3.02580859375" />
                  <Point X="0.836401977539" Y="-3.206262207031" />
                  <Point X="0.821810058594" Y="-3.273396484375" />
                  <Point X="0.819724487305" Y="-3.289627197266" />
                  <Point X="0.819742248535" Y="-3.323120117188" />
                  <Point X="0.879316467285" Y="-3.775632324219" />
                  <Point X="1.022065246582" Y="-4.859915039062" />
                  <Point X="1.021667480469" Y="-4.860002441406" />
                  <Point X="0.975676818848" Y="-4.870083496094" />
                  <Point X="0.929315551758" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058435668945" Y="-4.752634765625" />
                  <Point X="-1.141246337891" Y="-4.731328613281" />
                  <Point X="-1.120775756836" Y="-4.575838378906" />
                  <Point X="-1.120077392578" Y="-4.568099609375" />
                  <Point X="-1.119451660156" Y="-4.541033691406" />
                  <Point X="-1.121759277344" Y="-4.509328125" />
                  <Point X="-1.123334106445" Y="-4.497690429688" />
                  <Point X="-1.16744934082" Y="-4.275908691406" />
                  <Point X="-1.183861572266" Y="-4.193398925781" />
                  <Point X="-1.18812512207" Y="-4.17846875" />
                  <Point X="-1.19902734375" Y="-4.149502441406" />
                  <Point X="-1.205666137695" Y="-4.135466308594" />
                  <Point X="-1.221738037109" Y="-4.10762890625" />
                  <Point X="-1.230573852539" Y="-4.094862304688" />
                  <Point X="-1.250207885742" Y="-4.0709375" />
                  <Point X="-1.261006591797" Y="-4.059779296875" />
                  <Point X="-1.431018188477" Y="-3.910682861328" />
                  <Point X="-1.494267578125" Y="-3.855214599609" />
                  <Point X="-1.506738769531" Y="-3.845965332031" />
                  <Point X="-1.533021850586" Y="-3.829621337891" />
                  <Point X="-1.546833740234" Y="-3.822526855469" />
                  <Point X="-1.576530883789" Y="-3.810225830078" />
                  <Point X="-1.591313354492" Y="-3.805476074219" />
                  <Point X="-1.621455078125" Y="-3.798447998047" />
                  <Point X="-1.636813964844" Y="-3.796169677734" />
                  <Point X="-1.86245703125" Y="-3.781380126953" />
                  <Point X="-1.946403198242" Y="-3.775878173828" />
                  <Point X="-1.961928222656" Y="-3.776132324219" />
                  <Point X="-1.992729736328" Y="-3.779166015625" />
                  <Point X="-2.008006225586" Y="-3.781945556641" />
                  <Point X="-2.03905480957" Y="-3.790265136719" />
                  <Point X="-2.053674316406" Y="-3.795496337891" />
                  <Point X="-2.081865478516" Y="-3.808269775391" />
                  <Point X="-2.095437011719" Y="-3.815812011719" />
                  <Point X="-2.283455078125" Y="-3.941441894531" />
                  <Point X="-2.353403564453" Y="-3.988180175781" />
                  <Point X="-2.359681640625" Y="-3.992757080078" />
                  <Point X="-2.380442138672" Y="-4.010131103516" />
                  <Point X="-2.402759277344" Y="-4.032771484375" />
                  <Point X="-2.410471435547" Y="-4.041629638672" />
                  <Point X="-2.471452636719" Y="-4.1211015625" />
                  <Point X="-2.503202880859" Y="-4.162479492188" />
                  <Point X="-2.638842041016" Y="-4.078495117188" />
                  <Point X="-2.747583984375" Y="-4.011164550781" />
                  <Point X="-2.980862792969" Y="-3.831547607422" />
                  <Point X="-2.884067626953" Y="-3.663893798828" />
                  <Point X="-2.341488769531" Y="-2.724119628906" />
                  <Point X="-2.334850097656" Y="-2.710083251953" />
                  <Point X="-2.323947998047" Y="-2.681116943359" />
                  <Point X="-2.319684570312" Y="-2.666187011719" />
                  <Point X="-2.313413574219" Y="-2.634660644531" />
                  <Point X="-2.311638916016" Y="-2.619235351562" />
                  <Point X="-2.310626220703" Y="-2.588301269531" />
                  <Point X="-2.314666259766" Y="-2.557615234375" />
                  <Point X="-2.323650878906" Y="-2.527997558594" />
                  <Point X="-2.329357421875" Y="-2.513557128906" />
                  <Point X="-2.343574462891" Y="-2.484728271484" />
                  <Point X="-2.351557128906" Y="-2.47141015625" />
                  <Point X="-2.369584716797" Y="-2.446253173828" />
                  <Point X="-2.379629638672" Y="-2.434414306641" />
                  <Point X="-2.396978271484" Y="-2.417065673828" />
                  <Point X="-2.408817138672" Y="-2.407020751953" />
                  <Point X="-2.433974121094" Y="-2.388993164062" />
                  <Point X="-2.447292236328" Y="-2.381010498047" />
                  <Point X="-2.47612109375" Y="-2.366793457031" />
                  <Point X="-2.490561523438" Y="-2.361086914062" />
                  <Point X="-2.520179199219" Y="-2.352102294922" />
                  <Point X="-2.550865234375" Y="-2.348062255859" />
                  <Point X="-2.581799316406" Y="-2.349074951172" />
                  <Point X="-2.597224609375" Y="-2.350849609375" />
                  <Point X="-2.628750976562" Y="-2.357120605469" />
                  <Point X="-2.643680908203" Y="-2.361384033203" />
                  <Point X="-2.672647216797" Y="-2.372286132812" />
                  <Point X="-2.68668359375" Y="-2.378924804688" />
                  <Point X="-3.036308837891" Y="-2.580781005859" />
                  <Point X="-3.793089355469" Y="-3.017708496094" />
                  <Point X="-3.918104736328" Y="-2.853464111328" />
                  <Point X="-4.004015380859" Y="-2.740594970703" />
                  <Point X="-4.181265136719" Y="-2.443373535156" />
                  <Point X="-3.997459472656" Y="-2.302333984375" />
                  <Point X="-3.048122314453" Y="-1.573881958008" />
                  <Point X="-3.036481933594" Y="-1.563309692383" />
                  <Point X="-3.015104248047" Y="-1.540389282227" />
                  <Point X="-3.005366943359" Y="-1.528040893555" />
                  <Point X="-2.987402099609" Y="-1.500909545898" />
                  <Point X="-2.979835449219" Y="-1.487124511719" />
                  <Point X="-2.967080078125" Y="-1.45849597168" />
                  <Point X="-2.961891357422" Y="-1.44365234375" />
                  <Point X="-2.956275878906" Y="-1.421971313477" />
                  <Point X="-2.951552978516" Y="-1.398804077148" />
                  <Point X="-2.948748535156" Y="-1.368374511719" />
                  <Point X="-2.948577880859" Y="-1.353046020508" />
                  <Point X="-2.950786865234" Y="-1.321375366211" />
                  <Point X="-2.953083740234" Y="-1.306219726562" />
                  <Point X="-2.960084960938" Y="-1.276474609375" />
                  <Point X="-2.971779052734" Y="-1.24824230957" />
                  <Point X="-2.987861572266" Y="-1.222258789062" />
                  <Point X="-2.996954345703" Y="-1.209918212891" />
                  <Point X="-3.017787109375" Y="-1.185961669922" />
                  <Point X="-3.028746337891" Y="-1.175243652344" />
                  <Point X="-3.05224609375" Y="-1.155710083008" />
                  <Point X="-3.064786376953" Y="-1.14689453125" />
                  <Point X="-3.084088134766" Y="-1.135534179688" />
                  <Point X="-3.105434814453" Y="-1.124480712891" />
                  <Point X="-3.134698242188" Y="-1.113256591797" />
                  <Point X="-3.149796142578" Y="-1.108859741211" />
                  <Point X="-3.18168359375" Y="-1.102378295898" />
                  <Point X="-3.197300048828" Y="-1.100532104492" />
                  <Point X="-3.228622802734" Y="-1.09944128418" />
                  <Point X="-3.244329101562" Y="-1.100196533203" />
                  <Point X="-3.685697753906" Y="-1.158303833008" />
                  <Point X="-4.660920898438" Y="-1.286694335938" />
                  <Point X="-4.707159667969" Y="-1.105669799805" />
                  <Point X="-4.740762207031" Y="-0.974117919922" />
                  <Point X="-4.786452636719" Y="-0.654654418945" />
                  <Point X="-4.589726074219" Y="-0.60194152832" />
                  <Point X="-3.508288085938" Y="-0.312171447754" />
                  <Point X="-3.500475341797" Y="-0.309712493896" />
                  <Point X="-3.473932373047" Y="-0.299251647949" />
                  <Point X="-3.444167480469" Y="-0.2838934021" />
                  <Point X="-3.433561523438" Y="-0.27751361084" />
                  <Point X="-3.413333984375" Y="-0.26347454834" />
                  <Point X="-3.393672363281" Y="-0.248242782593" />
                  <Point X="-3.371217041016" Y="-0.22635925293" />
                  <Point X="-3.360897949219" Y="-0.21448399353" />
                  <Point X="-3.341653564453" Y="-0.188227966309" />
                  <Point X="-3.333436523438" Y="-0.174813568115" />
                  <Point X="-3.319328613281" Y="-0.146813156128" />
                  <Point X="-3.313437744141" Y="-0.132227005005" />
                  <Point X="-3.306695068359" Y="-0.110502197266" />
                  <Point X="-3.300990478516" Y="-0.088500213623" />
                  <Point X="-3.296720947266" Y="-0.060338699341" />
                  <Point X="-3.295647705078" Y="-0.046097057343" />
                  <Point X="-3.295648193359" Y="-0.016455703735" />
                  <Point X="-3.296721679688" Y="-0.002215845585" />
                  <Point X="-3.300990966797" Y="0.025941507339" />
                  <Point X="-3.304186767578" Y="0.039859149933" />
                  <Point X="-3.310929199219" Y="0.061583957672" />
                  <Point X="-3.319328857422" Y="0.084253112793" />
                  <Point X="-3.333437744141" Y="0.112255599976" />
                  <Point X="-3.341655517578" Y="0.125670898438" />
                  <Point X="-3.360899414062" Y="0.151925735474" />
                  <Point X="-3.371217529297" Y="0.1637996521" />
                  <Point X="-3.393672363281" Y="0.185683044434" />
                  <Point X="-3.405809082031" Y="0.195692199707" />
                  <Point X="-3.426036621094" Y="0.209731124878" />
                  <Point X="-3.444244384766" Y="0.220906234741" />
                  <Point X="-3.467337646484" Y="0.233349380493" />
                  <Point X="-3.477338623047" Y="0.238010345459" />
                  <Point X="-3.497813964844" Y="0.246141204834" />
                  <Point X="-3.508288085938" Y="0.249611251831" />
                  <Point X="-3.910616455078" Y="0.357414886475" />
                  <Point X="-4.785446289062" Y="0.591824768066" />
                  <Point X="-4.752985351562" Y="0.811192443848" />
                  <Point X="-4.731330566406" Y="0.957532531738" />
                  <Point X="-4.633586425781" Y="1.318237182617" />
                  <Point X="-4.553267089844" Y="1.307662963867" />
                  <Point X="-3.778066162109" Y="1.20560546875" />
                  <Point X="-3.76773828125" Y="1.204815429688" />
                  <Point X="-3.747058105469" Y="1.204364257812" />
                  <Point X="-3.736705810547" Y="1.20470324707" />
                  <Point X="-3.715144287109" Y="1.20658972168" />
                  <Point X="-3.704891113281" Y="1.208053466797" />
                  <Point X="-3.684604248047" Y="1.212088745117" />
                  <Point X="-3.674570556641" Y="1.21466027832" />
                  <Point X="-3.629800537109" Y="1.228776245117" />
                  <Point X="-3.603450683594" Y="1.237676513672" />
                  <Point X="-3.584517578125" Y="1.246007324219" />
                  <Point X="-3.575278320312" Y="1.250689331055" />
                  <Point X="-3.556533935547" Y="1.26151171875" />
                  <Point X="-3.547858886719" Y="1.267172241211" />
                  <Point X="-3.531177978516" Y="1.279403442383" />
                  <Point X="-3.515928222656" Y="1.293377563477" />
                  <Point X="-3.502289794922" Y="1.308929321289" />
                  <Point X="-3.495895263672" Y="1.317077514648" />
                  <Point X="-3.483480712891" Y="1.334807373047" />
                  <Point X="-3.478011474609" Y="1.343602539062" />
                  <Point X="-3.4680625" Y="1.361737060547" />
                  <Point X="-3.463582763672" Y="1.371076049805" />
                  <Point X="-3.445618408203" Y="1.41444543457" />
                  <Point X="-3.435499023438" Y="1.440351806641" />
                  <Point X="-3.429710693359" Y="1.460210693359" />
                  <Point X="-3.427358642578" Y="1.470298217773" />
                  <Point X="-3.423600341797" Y="1.491612792969" />
                  <Point X="-3.422360595703" Y="1.501897216797" />
                  <Point X="-3.421008056641" Y="1.522537475586" />
                  <Point X="-3.42191015625" Y="1.543200561523" />
                  <Point X="-3.425056640625" Y="1.56364440918" />
                  <Point X="-3.427188232422" Y="1.57378112793" />
                  <Point X="-3.432790039062" Y="1.594687255859" />
                  <Point X="-3.436012207031" Y="1.604530761719" />
                  <Point X="-3.443509033203" Y="1.623808959961" />
                  <Point X="-3.447783691406" Y="1.633243774414" />
                  <Point X="-3.469459472656" Y="1.674882446289" />
                  <Point X="-3.482799804688" Y="1.699286376953" />
                  <Point X="-3.494291503906" Y="1.716484985352" />
                  <Point X="-3.500506835938" Y="1.724770629883" />
                  <Point X="-3.514419433594" Y="1.741351074219" />
                  <Point X="-3.521500732422" Y="1.748911621094" />
                  <Point X="-3.536442871094" Y="1.763215332031" />
                  <Point X="-3.544303710938" Y="1.769958618164" />
                  <Point X="-3.775080322266" Y="1.947039794922" />
                  <Point X="-4.227614257812" Y="2.294282226563" />
                  <Point X="-4.086431152344" Y="2.536163330078" />
                  <Point X="-4.002293212891" Y="2.680312011719" />
                  <Point X="-3.726338623047" Y="3.035012695312" />
                  <Point X="-3.254156982422" Y="2.7623984375" />
                  <Point X="-3.244918457031" Y="2.757716552734" />
                  <Point X="-3.225986083984" Y="2.749385986328" />
                  <Point X="-3.216292236328" Y="2.745737304688" />
                  <Point X="-3.195650146484" Y="2.739228759766" />
                  <Point X="-3.185615722656" Y="2.736656982422" />
                  <Point X="-3.165327880859" Y="2.732621582031" />
                  <Point X="-3.155074462891" Y="2.731157958984" />
                  <Point X="-3.092722167969" Y="2.725702880859" />
                  <Point X="-3.069525146484" Y="2.723673339844" />
                  <Point X="-3.059173339844" Y="2.723334472656" />
                  <Point X="-3.038493164062" Y="2.723785644531" />
                  <Point X="-3.028164794922" Y="2.724575683594" />
                  <Point X="-3.006706054688" Y="2.727400878906" />
                  <Point X="-2.996525878906" Y="2.729310791016" />
                  <Point X="-2.976434082031" Y="2.734227294922" />
                  <Point X="-2.956996826172" Y="2.741302001953" />
                  <Point X="-2.9384453125" Y="2.750450683594" />
                  <Point X="-2.929419433594" Y="2.75553125" />
                  <Point X="-2.911165039062" Y="2.767160644531" />
                  <Point X="-2.90274609375" Y="2.773193603516" />
                  <Point X="-2.886614746094" Y="2.786140380859" />
                  <Point X="-2.87890234375" Y="2.793054199219" />
                  <Point X="-2.834644042969" Y="2.837312255859" />
                  <Point X="-2.818178710938" Y="2.853777587891" />
                  <Point X="-2.811264892578" Y="2.861489990234" />
                  <Point X="-2.798317871094" Y="2.877621582031" />
                  <Point X="-2.792284667969" Y="2.886040771484" />
                  <Point X="-2.780655273438" Y="2.904295166016" />
                  <Point X="-2.775574707031" Y="2.913321044922" />
                  <Point X="-2.766426025391" Y="2.931872558594" />
                  <Point X="-2.759351318359" Y="2.951309814453" />
                  <Point X="-2.754434814453" Y="2.971401611328" />
                  <Point X="-2.752524902344" Y="2.981581787109" />
                  <Point X="-2.749699707031" Y="3.003040527344" />
                  <Point X="-2.748909667969" Y="3.013368896484" />
                  <Point X="-2.748458496094" Y="3.034049072266" />
                  <Point X="-2.748797363281" Y="3.044400878906" />
                  <Point X="-2.754252441406" Y="3.106753417969" />
                  <Point X="-2.756281982422" Y="3.129950439453" />
                  <Point X="-2.757745605469" Y="3.140203857422" />
                  <Point X="-2.761781005859" Y="3.160491699219" />
                  <Point X="-2.764352783203" Y="3.170526123047" />
                  <Point X="-2.770861328125" Y="3.191168212891" />
                  <Point X="-2.774510009766" Y="3.200862060547" />
                  <Point X="-2.782840576172" Y="3.219794433594" />
                  <Point X="-2.787522460938" Y="3.229032958984" />
                  <Point X="-2.889786621094" Y="3.406159423828" />
                  <Point X="-3.059386962891" Y="3.699916259766" />
                  <Point X="-2.794909179688" Y="3.902688720703" />
                  <Point X="-2.648374023438" Y="4.015036621094" />
                  <Point X="-2.192525146484" Y="4.268296386719" />
                  <Point X="-2.118563720703" Y="4.171908203125" />
                  <Point X="-2.1118203125" Y="4.164047851562" />
                  <Point X="-2.097516845703" Y="4.149105957031" />
                  <Point X="-2.089956542969" Y="4.142024414062" />
                  <Point X="-2.073376708984" Y="4.128112304688" />
                  <Point X="-2.065090820313" Y="4.121896972656" />
                  <Point X="-2.047892333984" Y="4.110405273438" />
                  <Point X="-2.038979370117" Y="4.10512890625" />
                  <Point X="-1.969581420898" Y="4.069002685547" />
                  <Point X="-1.943763305664" Y="4.055562255859" />
                  <Point X="-1.934328491211" Y="4.051287353516" />
                  <Point X="-1.91505090332" Y="4.043790771484" />
                  <Point X="-1.905207763672" Y="4.040568603516" />
                  <Point X="-1.884301025391" Y="4.034966552734" />
                  <Point X="-1.874166137695" Y="4.032835449219" />
                  <Point X="-1.853722290039" Y="4.029688476562" />
                  <Point X="-1.83305480957" Y="4.028785888672" />
                  <Point X="-1.812414550781" Y="4.030138916016" />
                  <Point X="-1.802132568359" Y="4.031378662109" />
                  <Point X="-1.780817382812" Y="4.035136962891" />
                  <Point X="-1.770731445312" Y="4.037488525391" />
                  <Point X="-1.750871826172" Y="4.043276611328" />
                  <Point X="-1.741098144531" Y="4.046713623047" />
                  <Point X="-1.668815551758" Y="4.076654541016" />
                  <Point X="-1.641924072266" Y="4.087793212891" />
                  <Point X="-1.632585083008" Y="4.092272705078" />
                  <Point X="-1.614450317383" Y="4.102222167969" />
                  <Point X="-1.605654418945" Y="4.10769140625" />
                  <Point X="-1.587924926758" Y="4.120105957031" />
                  <Point X="-1.57977734375" Y="4.126500488281" />
                  <Point X="-1.564225952148" Y="4.140138671875" />
                  <Point X="-1.550251220703" Y="4.155389648437" />
                  <Point X="-1.538020141602" Y="4.172070800781" />
                  <Point X="-1.532360351562" Y="4.180744628906" />
                  <Point X="-1.521538574219" Y="4.199488769531" />
                  <Point X="-1.516855834961" Y="4.208729003906" />
                  <Point X="-1.508525390625" Y="4.227662109375" />
                  <Point X="-1.504877319336" Y="4.237354980469" />
                  <Point X="-1.481350708008" Y="4.311971679688" />
                  <Point X="-1.47259777832" Y="4.339731445312" />
                  <Point X="-1.470026367188" Y="4.349764648437" />
                  <Point X="-1.465991088867" Y="4.370051269531" />
                  <Point X="-1.46452722168" Y="4.3803046875" />
                  <Point X="-1.46264074707" Y="4.401866210938" />
                  <Point X="-1.462301757812" Y="4.41221875" />
                  <Point X="-1.462752929688" Y="4.432898925781" />
                  <Point X="-1.46354309082" Y="4.4432265625" />
                  <Point X="-1.475169555664" Y="4.531537109375" />
                  <Point X="-1.479266235352" Y="4.562655273438" />
                  <Point X="-1.120873779297" Y="4.663136230469" />
                  <Point X="-0.931178283691" Y="4.7163203125" />
                  <Point X="-0.373596862793" Y="4.781576660156" />
                  <Point X="-0.365221984863" Y="4.782556640625" />
                  <Point X="-0.225666244507" Y="4.261727539062" />
                  <Point X="-0.220435317993" Y="4.247107910156" />
                  <Point X="-0.207661773682" Y="4.218916503906" />
                  <Point X="-0.200119155884" Y="4.205344726562" />
                  <Point X="-0.182260925293" Y="4.178618164062" />
                  <Point X="-0.172608306885" Y="4.166455566406" />
                  <Point X="-0.151451248169" Y="4.143866699219" />
                  <Point X="-0.126898002625" Y="4.125026367188" />
                  <Point X="-0.099602958679" Y="4.110436523438" />
                  <Point X="-0.085356712341" Y="4.104260742188" />
                  <Point X="-0.054918586731" Y="4.093927978516" />
                  <Point X="-0.039857292175" Y="4.090155273438" />
                  <Point X="-0.009319890976" Y="4.08511328125" />
                  <Point X="0.021631721497" Y="4.08511328125" />
                  <Point X="0.052168972015" Y="4.090155273438" />
                  <Point X="0.067230415344" Y="4.093927978516" />
                  <Point X="0.097668540955" Y="4.104260742188" />
                  <Point X="0.111914489746" Y="4.110436523438" />
                  <Point X="0.139209686279" Y="4.125026367188" />
                  <Point X="0.16376322937" Y="4.143866699219" />
                  <Point X="0.184920150757" Y="4.166455566406" />
                  <Point X="0.194572906494" Y="4.178617675781" />
                  <Point X="0.212431137085" Y="4.205344238281" />
                  <Point X="0.219973754883" Y="4.218916503906" />
                  <Point X="0.232747146606" Y="4.247107910156" />
                  <Point X="0.237978225708" Y="4.261727539062" />
                  <Point X="0.29037701416" Y="4.457283203125" />
                  <Point X="0.3781902771" Y="4.785006347656" />
                  <Point X="0.662230041504" Y="4.755259765625" />
                  <Point X="0.827876281738" Y="4.737912109375" />
                  <Point X="1.289185546875" Y="4.626537597656" />
                  <Point X="1.453596191406" Y="4.58684375" />
                  <Point X="1.752491943359" Y="4.478432128906" />
                  <Point X="1.858257202148" Y="4.4400703125" />
                  <Point X="2.148604980469" Y="4.304284179688" />
                  <Point X="2.250453125" Y="4.256653320312" />
                  <Point X="2.530990722656" Y="4.093211181641" />
                  <Point X="2.629432128906" Y="4.035858886719" />
                  <Point X="2.817779785156" Y="3.901916748047" />
                  <Point X="2.693302978516" Y="3.686316162109" />
                  <Point X="2.06530859375" Y="2.598597900391" />
                  <Point X="2.062373046875" Y="2.593104980469" />
                  <Point X="2.053180908203" Y="2.573437988281" />
                  <Point X="2.044182373047" Y="2.549564208984" />
                  <Point X="2.041301757812" Y="2.540599121094" />
                  <Point X="2.025653564453" Y="2.482082763672" />
                  <Point X="2.01983215332" Y="2.460312744141" />
                  <Point X="2.017912597656" Y="2.451465576172" />
                  <Point X="2.013646972656" Y="2.420223388672" />
                  <Point X="2.012755615234" Y="2.3832421875" />
                  <Point X="2.013411254883" Y="2.369580078125" />
                  <Point X="2.019512817383" Y="2.318979980469" />
                  <Point X="2.02380065918" Y="2.28903515625" />
                  <Point X="2.029143188477" Y="2.267112548828" />
                  <Point X="2.032467773437" Y="2.256310058594" />
                  <Point X="2.040733886719" Y="2.234220947266" />
                  <Point X="2.045317749023" Y="2.223889648438" />
                  <Point X="2.055680664062" Y="2.203844238281" />
                  <Point X="2.061459472656" Y="2.194130126953" />
                  <Point X="2.092769042969" Y="2.147987792969" />
                  <Point X="2.104417236328" Y="2.130821533203" />
                  <Point X="2.10980859375" Y="2.123633056641" />
                  <Point X="2.130463134766" Y="2.100123291016" />
                  <Point X="2.157597167969" Y="2.075386962891" />
                  <Point X="2.1682578125" Y="2.066980957031" />
                  <Point X="2.214400146484" Y="2.035671508789" />
                  <Point X="2.23156640625" Y="2.02402355957" />
                  <Point X="2.241279785156" Y="2.018244995117" />
                  <Point X="2.261324707031" Y="2.007882202148" />
                  <Point X="2.27165625" Y="2.003297973633" />
                  <Point X="2.293744384766" Y="1.995032104492" />
                  <Point X="2.304547851562" Y="1.991707275391" />
                  <Point X="2.326470947266" Y="1.986364746094" />
                  <Point X="2.337590576172" Y="1.984346801758" />
                  <Point X="2.388190673828" Y="1.978245239258" />
                  <Point X="2.406242431641" Y="1.976936767578" />
                  <Point X="2.443969970703" Y="1.976004394531" />
                  <Point X="2.457529052734" Y="1.976639282227" />
                  <Point X="2.484418457031" Y="1.979834960938" />
                  <Point X="2.497748779297" Y="1.982395996094" />
                  <Point X="2.556265136719" Y="1.998044189453" />
                  <Point X="2.57803515625" Y="2.003865722656" />
                  <Point X="2.583995361328" Y="2.005670776367" />
                  <Point X="2.604404541016" Y="2.013067260742" />
                  <Point X="2.627655029297" Y="2.02357409668" />
                  <Point X="2.636033935547" Y="2.027872558594" />
                  <Point X="3.040698974609" Y="2.261505859375" />
                  <Point X="3.940404785156" Y="2.780951416016" />
                  <Point X="3.985558105469" Y="2.718198730469" />
                  <Point X="4.043951416016" Y="2.637044921875" />
                  <Point X="4.136884765625" Y="2.483471923828" />
                  <Point X="3.999765136719" Y="2.378256103516" />
                  <Point X="3.172951660156" Y="1.743819702148" />
                  <Point X="3.168138427734" Y="1.739869262695" />
                  <Point X="3.152119384766" Y="1.725216918945" />
                  <Point X="3.134668457031" Y="1.706603149414" />
                  <Point X="3.128576171875" Y="1.699422363281" />
                  <Point X="3.086461669922" Y="1.644480957031" />
                  <Point X="3.070793945312" Y="1.624041137695" />
                  <Point X="3.065635253906" Y="1.616602172852" />
                  <Point X="3.049738525391" Y="1.58937097168" />
                  <Point X="3.034762695312" Y="1.555545776367" />
                  <Point X="3.030139892578" Y="1.542672119141" />
                  <Point X="3.014452148438" Y="1.486576538086" />
                  <Point X="3.008615966797" Y="1.465707519531" />
                  <Point X="3.006224853516" Y="1.45466015625" />
                  <Point X="3.002771484375" Y="1.432360595703" />
                  <Point X="3.001709228516" Y="1.421108642578" />
                  <Point X="3.000893310547" Y="1.397538574219" />
                  <Point X="3.001174804688" Y="1.386239746094" />
                  <Point X="3.003077880859" Y="1.363755737305" />
                  <Point X="3.004699462891" Y="1.352570800781" />
                  <Point X="3.017577392578" Y="1.290157592773" />
                  <Point X="3.022368408203" Y="1.266937988281" />
                  <Point X="3.024597900391" Y="1.25823449707" />
                  <Point X="3.03468359375" Y="1.228608886719" />
                  <Point X="3.050286132812" Y="1.195371459961" />
                  <Point X="3.056918457031" Y="1.183525756836" />
                  <Point X="3.091945068359" Y="1.130286865234" />
                  <Point X="3.104976074219" Y="1.11048059082" />
                  <Point X="3.111739013672" Y="1.101424560547" />
                  <Point X="3.126292480469" Y="1.08417980957" />
                  <Point X="3.134083007812" Y="1.075991210938" />
                  <Point X="3.151326904297" Y="1.059901367188" />
                  <Point X="3.160034667969" Y="1.052695922852" />
                  <Point X="3.178244384766" Y="1.039370117188" />
                  <Point X="3.187746582031" Y="1.033249633789" />
                  <Point X="3.238504882813" Y="1.004677307129" />
                  <Point X="3.257388671875" Y="0.994047363281" />
                  <Point X="3.265479003906" Y="0.989988037109" />
                  <Point X="3.294678222656" Y="0.978084228516" />
                  <Point X="3.330275146484" Y="0.968021240234" />
                  <Point X="3.343670898438" Y="0.965257629395" />
                  <Point X="3.412299560547" Y="0.956187438965" />
                  <Point X="3.437831542969" Y="0.952812988281" />
                  <Point X="3.444029785156" Y="0.952199768066" />
                  <Point X="3.465716064453" Y="0.951222839355" />
                  <Point X="3.491217529297" Y="0.952032226562" />
                  <Point X="3.500603515625" Y="0.952797241211" />
                  <Point X="3.885008056641" Y="1.003405090332" />
                  <Point X="4.704703613281" Y="1.11132019043" />
                  <Point X="4.727604003906" Y="1.017251708984" />
                  <Point X="4.75268359375" Y="0.914233947754" />
                  <Point X="4.78387109375" Y="0.713920898438" />
                  <Point X="4.639538085938" Y="0.675247070312" />
                  <Point X="3.691991943359" Y="0.421352752686" />
                  <Point X="3.686031738281" Y="0.419544311523" />
                  <Point X="3.665627197266" Y="0.412138122559" />
                  <Point X="3.642381591797" Y="0.401619659424" />
                  <Point X="3.634004394531" Y="0.397316558838" />
                  <Point X="3.566578857422" Y="0.358343322754" />
                  <Point X="3.541494384766" Y="0.343844116211" />
                  <Point X="3.533880859375" Y="0.338945068359" />
                  <Point X="3.508773681641" Y="0.319870056152" />
                  <Point X="3.481993896484" Y="0.294350769043" />
                  <Point X="3.472797119141" Y="0.284226715088" />
                  <Point X="3.432341796875" Y="0.232677307129" />
                  <Point X="3.417291259766" Y="0.213499313354" />
                  <Point X="3.410854736328" Y="0.204208587646" />
                  <Point X="3.399130615234" Y="0.184928634644" />
                  <Point X="3.393843017578" Y="0.174939239502" />
                  <Point X="3.384069335938" Y="0.153475860596" />
                  <Point X="3.380005615234" Y="0.142929428101" />
                  <Point X="3.373159179688" Y="0.121428161621" />
                  <Point X="3.370376464844" Y="0.110473167419" />
                  <Point X="3.356891357422" Y="0.040058898926" />
                  <Point X="3.351874267578" Y="0.013862626076" />
                  <Point X="3.350603515625" Y="0.004968585968" />
                  <Point X="3.348584472656" Y="-0.026260505676" />
                  <Point X="3.350280029297" Y="-0.062940490723" />
                  <Point X="3.351874511719" Y="-0.076422821045" />
                  <Point X="3.365359863281" Y="-0.146837081909" />
                  <Point X="3.370376708984" Y="-0.173033508301" />
                  <Point X="3.373159179688" Y="-0.183988052368" />
                  <Point X="3.380005615234" Y="-0.205489471436" />
                  <Point X="3.384069580078" Y="-0.216036209106" />
                  <Point X="3.393843505859" Y="-0.23749987793" />
                  <Point X="3.399130859375" Y="-0.247489120483" />
                  <Point X="3.410854736328" Y="-0.26676864624" />
                  <Point X="3.417291259766" Y="-0.276059051514" />
                  <Point X="3.457746582031" Y="-0.327608612061" />
                  <Point X="3.472797119141" Y="-0.346786621094" />
                  <Point X="3.478718505859" Y="-0.353633514404" />
                  <Point X="3.501138671875" Y="-0.375804046631" />
                  <Point X="3.530175537109" Y="-0.398724090576" />
                  <Point X="3.541494384766" Y="-0.406404174805" />
                  <Point X="3.608919921875" Y="-0.445377410889" />
                  <Point X="3.634004394531" Y="-0.459876617432" />
                  <Point X="3.639496582031" Y="-0.462815338135" />
                  <Point X="3.659157958984" Y="-0.472016723633" />
                  <Point X="3.683027587891" Y="-0.481027740479" />
                  <Point X="3.691991943359" Y="-0.48391293335" />
                  <Point X="4.044508300781" Y="-0.578369506836" />
                  <Point X="4.784876953125" Y="-0.776750488281" />
                  <Point X="4.7756171875" Y="-0.838169921875" />
                  <Point X="4.76161328125" Y="-0.931051940918" />
                  <Point X="4.727801757812" Y="-1.079219726563" />
                  <Point X="4.535668945313" Y="-1.053925048828" />
                  <Point X="3.436781982422" Y="-0.909253723145" />
                  <Point X="3.428624511719" Y="-0.908535705566" />
                  <Point X="3.400098388672" Y="-0.908042480469" />
                  <Point X="3.366721435547" Y="-0.910840881348" />
                  <Point X="3.354481201172" Y="-0.912676208496" />
                  <Point X="3.222148681641" Y="-0.941439208984" />
                  <Point X="3.172916992188" Y="-0.952139770508" />
                  <Point X="3.157873779297" Y="-0.956742492676" />
                  <Point X="3.128753662109" Y="-0.968367004395" />
                  <Point X="3.114676757812" Y="-0.975388671875" />
                  <Point X="3.086850097656" Y="-0.992280883789" />
                  <Point X="3.074124023438" Y="-1.001530151367" />
                  <Point X="3.050373779297" Y="-1.022001159668" />
                  <Point X="3.039349609375" Y="-1.033222900391" />
                  <Point X="2.959363037109" Y="-1.129421875" />
                  <Point X="2.92960546875" Y="-1.1652109375" />
                  <Point X="2.921326171875" Y="-1.176847167969" />
                  <Point X="2.906605224609" Y="-1.201229858398" />
                  <Point X="2.900163574219" Y="-1.2139765625" />
                  <Point X="2.8888203125" Y="-1.241362182617" />
                  <Point X="2.884362548828" Y="-1.254929077148" />
                  <Point X="2.877531005859" Y="-1.282578125" />
                  <Point X="2.875157226562" Y="-1.29666027832" />
                  <Point X="2.863693115234" Y="-1.421242431641" />
                  <Point X="2.859428222656" Y="-1.467590576172" />
                  <Point X="2.859288818359" Y="-1.483321655273" />
                  <Point X="2.861607666016" Y="-1.514590087891" />
                  <Point X="2.864065917969" Y="-1.530127563477" />
                  <Point X="2.871797363281" Y="-1.561748291016" />
                  <Point X="2.876785888672" Y="-1.576667480469" />
                  <Point X="2.889157226562" Y="-1.605478881836" />
                  <Point X="2.896540039062" Y="-1.619371337891" />
                  <Point X="2.969774902344" Y="-1.733283203125" />
                  <Point X="2.997020263672" Y="-1.775661987305" />
                  <Point X="3.001741943359" Y="-1.782353393555" />
                  <Point X="3.019793945312" Y="-1.804450317383" />
                  <Point X="3.043489501953" Y="-1.828120361328" />
                  <Point X="3.052796142578" Y="-1.83627746582" />
                  <Point X="3.37993359375" Y="-2.087299316406" />
                  <Point X="4.087170898438" Y="-2.629981445312" />
                  <Point X="4.084963378906" Y="-2.633553466797" />
                  <Point X="4.045485839844" Y="-2.697434326172" />
                  <Point X="4.001274658203" Y="-2.760252197266" />
                  <Point X="3.827161621094" Y="-2.659727783203" />
                  <Point X="2.848454589844" Y="-2.094670654297" />
                  <Point X="2.841191894531" Y="-2.090885253906" />
                  <Point X="2.815026123047" Y="-2.079513183594" />
                  <Point X="2.783119140625" Y="-2.069325927734" />
                  <Point X="2.771108398438" Y="-2.066337646484" />
                  <Point X="2.613611572266" Y="-2.037893920898" />
                  <Point X="2.555018066406" Y="-2.027311889648" />
                  <Point X="2.539359130859" Y="-2.025807495117" />
                  <Point X="2.508006835938" Y="-2.025403198242" />
                  <Point X="2.492313476562" Y="-2.026503540039" />
                  <Point X="2.460140625" Y="-2.031461303711" />
                  <Point X="2.444844482422" Y="-2.035136108398" />
                  <Point X="2.415068603516" Y="-2.044959960938" />
                  <Point X="2.400588867188" Y="-2.051108642578" />
                  <Point X="2.269747802734" Y="-2.119969238281" />
                  <Point X="2.221070800781" Y="-2.145587646484" />
                  <Point X="2.208968505859" Y="-2.153170166016" />
                  <Point X="2.186037353516" Y="-2.170063476562" />
                  <Point X="2.175208496094" Y="-2.179374267578" />
                  <Point X="2.154248779297" Y="-2.200334228516" />
                  <Point X="2.144938476562" Y="-2.211162841797" />
                  <Point X="2.128045898438" Y="-2.234093261719" />
                  <Point X="2.120463623047" Y="-2.246195068359" />
                  <Point X="2.051602783203" Y="-2.377036132812" />
                  <Point X="2.025984741211" Y="-2.425713134766" />
                  <Point X="2.0198359375" Y="-2.440192871094" />
                  <Point X="2.010012084961" Y="-2.46996875" />
                  <Point X="2.006337280273" Y="-2.485264892578" />
                  <Point X="2.001379516602" Y="-2.5174375" />
                  <Point X="2.000279174805" Y="-2.533130859375" />
                  <Point X="2.00068347168" Y="-2.564483154297" />
                  <Point X="2.002187866211" Y="-2.580142089844" />
                  <Point X="2.030631469727" Y="-2.737638671875" />
                  <Point X="2.041213378906" Y="-2.796232421875" />
                  <Point X="2.043015014648" Y="-2.804221435547" />
                  <Point X="2.051236083984" Y="-2.83154296875" />
                  <Point X="2.064069580078" Y="-2.862479736328" />
                  <Point X="2.069546630859" Y="-2.873578613281" />
                  <Point X="2.279765380859" Y="-3.237687988281" />
                  <Point X="2.735893310547" Y="-4.027724609375" />
                  <Point X="2.723754150391" Y="-4.036083496094" />
                  <Point X="2.580758300781" Y="-3.849728027344" />
                  <Point X="1.833914672852" Y="-2.876422607422" />
                  <Point X="1.828654418945" Y="-2.870147216797" />
                  <Point X="1.808830932617" Y="-2.849625732422" />
                  <Point X="1.783251586914" Y="-2.828004150391" />
                  <Point X="1.773299072266" Y="-2.820647216797" />
                  <Point X="1.61796484375" Y="-2.720781494141" />
                  <Point X="1.56017565918" Y="-2.683628417969" />
                  <Point X="1.546283935547" Y="-2.676245849609" />
                  <Point X="1.517472900391" Y="-2.663874511719" />
                  <Point X="1.502553588867" Y="-2.658885742188" />
                  <Point X="1.470932617188" Y="-2.651154052734" />
                  <Point X="1.45539440918" Y="-2.648695800781" />
                  <Point X="1.42412512207" Y="-2.646376953125" />
                  <Point X="1.408394287109" Y="-2.646516357422" />
                  <Point X="1.238509521484" Y="-2.662149414062" />
                  <Point X="1.175307373047" Y="-2.667965332031" />
                  <Point X="1.161224975586" Y="-2.670339111328" />
                  <Point X="1.133575439453" Y="-2.677170898438" />
                  <Point X="1.120008422852" Y="-2.68162890625" />
                  <Point X="1.092623168945" Y="-2.692972167969" />
                  <Point X="1.079877563477" Y="-2.699413330078" />
                  <Point X="1.055495361328" Y="-2.714133789062" />
                  <Point X="1.043858764648" Y="-2.722413085938" />
                  <Point X="0.912678344727" Y="-2.831485351563" />
                  <Point X="0.863875061035" Y="-2.872063476562" />
                  <Point X="0.852653015137" Y="-2.883088378906" />
                  <Point X="0.832182434082" Y="-2.906838134766" />
                  <Point X="0.822934082031" Y="-2.919562988281" />
                  <Point X="0.806041564941" Y="-2.947389404297" />
                  <Point X="0.799019470215" Y="-2.961466796875" />
                  <Point X="0.787394470215" Y="-2.990587646484" />
                  <Point X="0.782791931152" Y="-3.005631103516" />
                  <Point X="0.743569580078" Y="-3.186084716797" />
                  <Point X="0.728977661133" Y="-3.253218994141" />
                  <Point X="0.727584716797" Y="-3.2612890625" />
                  <Point X="0.724724487305" Y="-3.289677490234" />
                  <Point X="0.7247421875" Y="-3.323170410156" />
                  <Point X="0.725554992676" Y="-3.335520019531" />
                  <Point X="0.785129150391" Y="-3.788032226562" />
                  <Point X="0.833091308594" Y="-4.152340332031" />
                  <Point X="0.655064941406" Y="-3.487936523438" />
                  <Point X="0.652606262207" Y="-3.480124511719" />
                  <Point X="0.642145141602" Y="-3.453580322266" />
                  <Point X="0.62678692627" Y="-3.423815673828" />
                  <Point X="0.620407409668" Y="-3.413210205078" />
                  <Point X="0.50107510376" Y="-3.241274902344" />
                  <Point X="0.456679962158" Y="-3.177309814453" />
                  <Point X="0.446670654297" Y="-3.165172851562" />
                  <Point X="0.424786804199" Y="-3.142717529297" />
                  <Point X="0.41291229248" Y="-3.132399169922" />
                  <Point X="0.386657165527" Y="-3.113155273438" />
                  <Point X="0.373242919922" Y="-3.104938232422" />
                  <Point X="0.345241912842" Y="-3.090829833984" />
                  <Point X="0.330654876709" Y="-3.084938476562" />
                  <Point X="0.145994766235" Y="-3.027626708984" />
                  <Point X="0.077295608521" Y="-3.006305175781" />
                  <Point X="0.063376476288" Y="-3.003109130859" />
                  <Point X="0.035216896057" Y="-2.99883984375" />
                  <Point X="0.02097659111" Y="-2.997766601562" />
                  <Point X="-0.008664910316" Y="-2.997766601562" />
                  <Point X="-0.022905065536" Y="-2.99883984375" />
                  <Point X="-0.051064647675" Y="-3.003109130859" />
                  <Point X="-0.064983779907" Y="-3.006305175781" />
                  <Point X="-0.249643585205" Y="-3.063616699219" />
                  <Point X="-0.31834274292" Y="-3.084938476562" />
                  <Point X="-0.332929779053" Y="-3.090829589844" />
                  <Point X="-0.360930938721" Y="-3.104937988281" />
                  <Point X="-0.374345336914" Y="-3.113155273438" />
                  <Point X="-0.400600463867" Y="-3.132399169922" />
                  <Point X="-0.412475128174" Y="-3.142717773438" />
                  <Point X="-0.434358978271" Y="-3.165173095703" />
                  <Point X="-0.444367980957" Y="-3.177309814453" />
                  <Point X="-0.563700256348" Y="-3.349244873047" />
                  <Point X="-0.608095581055" Y="-3.413210205078" />
                  <Point X="-0.612470458984" Y="-3.420132324219" />
                  <Point X="-0.62597668457" Y="-3.445265136719" />
                  <Point X="-0.638777893066" Y="-3.476215820312" />
                  <Point X="-0.642753112793" Y="-3.487936523438" />
                  <Point X="-0.751372070312" Y="-3.893308105469" />
                  <Point X="-0.985425170898" Y="-4.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.948701859326" Y="3.784777417354" />
                  <Point X="-3.04465369599" Y="3.674397455841" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.642526348238" Y="2.986623645153" />
                  <Point X="-4.193370859471" Y="2.35294952214" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.598649600066" Y="4.042662434045" />
                  <Point X="-2.99441675751" Y="3.587384399507" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.558714073429" Y="2.938234594993" />
                  <Point X="-4.16215592847" Y="2.244054149359" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.35519555978" Y="4.177920227363" />
                  <Point X="-2.94417981903" Y="3.500371343173" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.47490179862" Y="2.889845544834" />
                  <Point X="-4.086646601789" Y="2.186113649987" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.172941889546" Y="4.242775048464" />
                  <Point X="-2.89394288055" Y="3.413358286838" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.391089523811" Y="2.841456494674" />
                  <Point X="-4.011137275107" Y="2.128173150615" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.113649103067" Y="4.166179553568" />
                  <Point X="-2.84370586175" Y="3.326345322901" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.307277249002" Y="2.793067444515" />
                  <Point X="-3.935627948426" Y="2.070232651243" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.593872623786" Y="1.313008772487" />
                  <Point X="-4.653631857901" Y="1.244263637522" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.040210063159" Y="4.105857461698" />
                  <Point X="-2.793468835706" Y="3.239332367298" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.221003115081" Y="2.747510439301" />
                  <Point X="-3.860118621744" Y="2.01229215187" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.480922810931" Y="1.29813862556" />
                  <Point X="-4.710642985754" Y="1.033875793942" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.953602300031" Y="4.060684252984" />
                  <Point X="-2.75718396628" Y="3.136269291509" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.112574118238" Y="2.727439688459" />
                  <Point X="-3.784609295063" Y="1.954351652498" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.367973021501" Y="1.283268451685" />
                  <Point X="-4.747783387923" Y="0.846346605418" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.363043789916" Y="4.595240062399" />
                  <Point X="-1.467705245685" Y="4.474840830228" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.854558401693" Y="4.029817181323" />
                  <Point X="-2.750159841731" Y="2.999545579241" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.981976461823" Y="2.732871063219" />
                  <Point X="-3.709099905834" Y="1.896411225079" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.255023232071" Y="1.26839827781" />
                  <Point X="-4.773606566786" Y="0.671836393043" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.196602876046" Y="4.641904388146" />
                  <Point X="-1.480179176798" Y="4.315687170725" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.698743365982" Y="4.064257832537" />
                  <Point X="-3.633590507571" Y="1.838470808051" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.142073442641" Y="1.253528103936" />
                  <Point X="-4.730160741662" Y="0.577011054454" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.030161979293" Y="4.688568694203" />
                  <Point X="-3.558081109309" Y="1.780530391024" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.029123653212" Y="1.238657930061" />
                  <Point X="-4.628065096111" Y="0.549654616374" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.874383521112" Y="4.722967267784" />
                  <Point X="-3.491504519775" Y="1.712313953047" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.916173863782" Y="1.223787756186" />
                  <Point X="-4.525969450561" Y="0.522298178293" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.734250633232" Y="4.739367671576" />
                  <Point X="-3.443217095306" Y="1.623058237385" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.803224074352" Y="1.208917582312" />
                  <Point X="-4.42387380501" Y="0.494941740212" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.594117745353" Y="4.755768075368" />
                  <Point X="-3.422335816446" Y="1.502275357651" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.671519554372" Y="1.215622257948" />
                  <Point X="-4.321778159459" Y="0.467585302131" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.453984857474" Y="4.772168479159" />
                  <Point X="-4.219682513909" Y="0.440228864051" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.354349874356" Y="4.741981372756" />
                  <Point X="-4.117586868358" Y="0.41287242597" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.324691616963" Y="4.631295251837" />
                  <Point X="-4.015491222807" Y="0.385515987889" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.29503335957" Y="4.520609130918" />
                  <Point X="-3.913395577257" Y="0.358159549809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.265375102176" Y="4.409923009999" />
                  <Point X="-3.811299949697" Y="0.330803091032" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.631959780817" Y="-0.613258051764" />
                  <Point X="-4.769709499307" Y="-0.771720976019" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.235716844783" Y="4.299236889081" />
                  <Point X="-3.70920432264" Y="0.303446631677" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.467860861996" Y="-0.569287883131" />
                  <Point X="-4.751925288809" Y="-0.896066625352" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.196355817495" Y="4.199712528112" />
                  <Point X="-3.607108695583" Y="0.276090172321" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.303761999536" Y="-0.525317779333" />
                  <Point X="-4.730119003935" Y="-1.01578540739" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.433292221097" Y="4.77923569614" />
                  <Point X="0.351402197569" Y="4.685032000206" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.132000064733" Y="4.128941309675" />
                  <Point X="-3.505153090157" Y="0.248572636505" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.139663137076" Y="-0.481347675534" />
                  <Point X="-4.701531649669" Y="-1.127703461434" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.548665174313" Y="4.76715305333" />
                  <Point X="0.295313072172" Y="4.475704799124" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.039842185493" Y="4.090152779192" />
                  <Point X="-3.4179313862" Y="0.204105685925" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.975564274615" Y="-0.437377571736" />
                  <Point X="-4.672944549177" Y="-1.239621807411" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.664038127078" Y="4.755070410002" />
                  <Point X="0.23922431377" Y="4.266378020221" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.098678520447" Y="4.104698579814" />
                  <Point X="-3.350156010854" Y="0.137268293272" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.811465412155" Y="-0.393407467938" />
                  <Point X="-4.578562430369" Y="-1.275851642965" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.779411051533" Y="4.742987734107" />
                  <Point X="-3.305441587801" Y="0.043902309663" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.647366549695" Y="-0.34943736414" />
                  <Point X="-4.436418744226" Y="-1.257138080377" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.88821222396" Y="4.723345122297" />
                  <Point X="-3.303680684514" Y="-0.098876046064" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.479913711299" Y="-0.301608952386" />
                  <Point X="-4.294275058083" Y="-1.23842451779" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.992253084758" Y="4.698226398383" />
                  <Point X="-4.152131371941" Y="-1.219710955202" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.096293945556" Y="4.673107674468" />
                  <Point X="-4.009987685798" Y="-1.200997392614" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.200334806355" Y="4.647988950554" />
                  <Point X="-3.867843999655" Y="-1.182283830026" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.304375669392" Y="4.622870229215" />
                  <Point X="-3.725700313512" Y="-1.163570267439" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.40841654553" Y="4.597751522947" />
                  <Point X="-3.583556617962" Y="-1.14485669403" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.507739577428" Y="4.567205557715" />
                  <Point X="-3.441412918728" Y="-1.126143116382" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.603441371185" Y="4.53249383453" />
                  <Point X="-3.299269219494" Y="-1.107429538735" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.699143164943" Y="4.497782111346" />
                  <Point X="-3.170906266438" Y="-1.104568896119" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.794844957991" Y="4.463070387346" />
                  <Point X="-3.07605682168" Y="-1.140261134665" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.888452165617" Y="4.42594911845" />
                  <Point X="-3.003844720183" Y="-1.20199465772" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.886025847241" Y="-2.216827955734" />
                  <Point X="-4.141265280215" Y="-2.510447335704" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.977945896471" Y="4.384095835831" />
                  <Point X="-2.956383440981" Y="-1.292200744796" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.507988283599" Y="-1.926749529015" />
                  <Point X="-4.090047450651" Y="-2.596332005925" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.067439627325" Y="4.342242553213" />
                  <Point X="-2.961776465582" Y="-1.443208753154" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.129950719957" Y="-1.636671102295" />
                  <Point X="-4.038829621087" Y="-2.682216676145" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.156933367139" Y="4.300389280903" />
                  <Point X="-3.985194993286" Y="-2.765321138027" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.246427194283" Y="4.258536109054" />
                  <Point X="-3.926431061608" Y="-2.842525010777" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.330252436203" Y="4.210161975849" />
                  <Point X="-3.867667058183" Y="-2.919728800992" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.413810661707" Y="4.161480675395" />
                  <Point X="-3.808903042914" Y="-2.996932577583" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.497368887212" Y="4.112799374942" />
                  <Point X="-3.608389143362" Y="-2.911071765566" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.580927111603" Y="4.064118073209" />
                  <Point X="-3.355684993372" Y="-2.765172938281" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.662064866645" Y="4.012652340004" />
                  <Point X="-3.102980843382" Y="-2.619274110995" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.739853288382" Y="3.957333639581" />
                  <Point X="-2.850276742528" Y="-2.473375340235" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.81764171012" Y="3.902014939158" />
                  <Point X="-2.622210960386" Y="-2.355819712929" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.569282380865" Y="3.471506169908" />
                  <Point X="-2.498753264046" Y="-2.358601922667" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.320342390133" Y="3.040329426039" />
                  <Point X="-2.412606409619" Y="-2.40430534619" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.071402399401" Y="2.60915268217" />
                  <Point X="-2.348906991337" Y="-2.475831591077" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.013093781583" Y="2.397272247127" />
                  <Point X="-2.31196429839" Y="-2.57813792747" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.02841640048" Y="2.270094860584" />
                  <Point X="-2.39785347708" Y="-2.821746168395" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.073207287738" Y="2.176816838981" />
                  <Point X="-2.646793505469" Y="-3.252922955583" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.131551968065" Y="2.099130672721" />
                  <Point X="-2.895733604461" Y="-3.68409982399" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.206766607866" Y="2.040851174871" />
                  <Point X="-2.931243233192" Y="-3.869753022272" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.293041564053" Y="1.995295115566" />
                  <Point X="-2.855837626461" Y="-3.927812837798" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.403153747273" Y="1.977160649156" />
                  <Point X="-2.78043201973" Y="-3.985872653325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.544433334493" Y="1.994880179642" />
                  <Point X="-2.701399930837" Y="-4.039760678343" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.762420741191" Y="2.100841962243" />
                  <Point X="-2.619568716347" Y="-4.090428677706" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.015124675729" Y="2.246740541678" />
                  <Point X="-2.537737412381" Y="-4.141096574139" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.267828847842" Y="2.392639394413" />
                  <Point X="-2.175737136911" Y="-3.86946693707" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.520533046705" Y="2.53853827792" />
                  <Point X="-1.969362899655" Y="-3.776864577703" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.773237245569" Y="2.684437161428" />
                  <Point X="-1.848222979338" Y="-3.782313083755" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.959700607941" Y="2.754134679368" />
                  <Point X="-1.729132129115" Y="-3.790118775306" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.016706864609" Y="2.674908832816" />
                  <Point X="3.276178651568" Y="1.823028571877" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.029035425794" Y="1.538722812888" />
                  <Point X="-1.612343250216" Y="-3.800572581942" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.070923312299" Y="2.592473678153" />
                  <Point X="3.654216338312" Y="2.11310714021" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.003054564671" Y="1.364031207823" />
                  <Point X="-1.519193685503" Y="-3.838220308788" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.122585990538" Y="2.507100747794" />
                  <Point X="4.032254155429" Y="2.403185858519" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.028077695381" Y="1.248012983604" />
                  <Point X="-1.445368389916" Y="-3.898098064327" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.074571489594" Y="1.156693932362" />
                  <Point X="-1.373942976465" Y="-3.960736568458" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.132103031691" Y="1.078072357572" />
                  <Point X="-1.302517570129" Y="-4.023375080773" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.208741855588" Y="1.021431196112" />
                  <Point X="-0.406844017112" Y="-3.137824565436" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.457042273433" Y="-3.195571053605" />
                  <Point X="-1.23456090862" Y="-4.09000392755" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.296491539642" Y="0.977571617155" />
                  <Point X="-0.204199250065" Y="-3.049512470774" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.668346667227" Y="-3.58345299577" />
                  <Point X="-1.187497254081" Y="-4.180667429477" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.404656846177" Y="0.957197525314" />
                  <Point X="-0.035999761831" Y="-3.000825136636" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.72443566733" Y="-3.792780052718" />
                  <Point X="-1.163159667948" Y="-4.297474282518" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.530081551585" Y="0.956678100663" />
                  <Point X="0.083451841054" Y="-3.008215829721" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.780524656498" Y="-4.002107097087" />
                  <Point X="-1.139719854543" Y="-4.415313904943" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.672225257607" Y="0.975391686118" />
                  <Point X="0.182583107072" Y="-3.038982396364" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.836613635561" Y="-4.211434129832" />
                  <Point X="-1.119735855168" Y="-4.537128986649" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.814368963629" Y="0.994105271574" />
                  <Point X="0.281714260081" Y="-3.069749093008" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.892702614625" Y="-4.420761162577" />
                  <Point X="-1.137421824105" Y="-4.702278409802" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.956512675323" Y="1.012818863556" />
                  <Point X="0.375695216458" Y="-3.106440413149" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.948791593688" Y="-4.630088195322" />
                  <Point X="-1.055769566662" Y="-4.7531522757" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.098656392622" Y="1.031532461984" />
                  <Point X="0.448542775819" Y="-3.167442925553" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.240800109921" Y="1.050246060413" />
                  <Point X="3.694755107333" Y="0.422093140515" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.355261910651" Y="0.031550892585" />
                  <Point X="0.505142956364" Y="-3.247135909249" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.38294382722" Y="1.068959658841" />
                  <Point X="3.858854049285" Y="0.466063335757" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.358266177057" Y="-0.109797137492" />
                  <Point X="1.123645954239" Y="-2.680433643959" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.763400001532" Y="-3.094849206782" />
                  <Point X="0.56102640507" Y="-3.327653398608" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.525087544519" Y="1.087673257269" />
                  <Point X="4.022952991236" Y="0.510033530999" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.386723656438" Y="-0.221864595499" />
                  <Point X="1.267755938704" Y="-2.659458113902" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.725355055911" Y="-3.283418953517" />
                  <Point X="0.616909853776" Y="-3.408170887967" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.667231261817" Y="1.106386855698" />
                  <Point X="4.187051933187" Y="0.554003726241" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.440159163514" Y="-0.305198119573" />
                  <Point X="1.404576906218" Y="-2.646867638665" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.736127986634" Y="-3.415830157596" />
                  <Point X="0.659395989328" Y="-3.504100223119" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.724982901446" Y="1.028018474155" />
                  <Point X="4.351150875138" Y="0.597973921483" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.503226207386" Y="-0.377451828003" />
                  <Point X="1.516075485107" Y="-2.663407239299" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.752684307582" Y="-3.541588332275" />
                  <Point X="0.689054233879" Y="-3.614786358812" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.752522603991" Y="0.914895234669" />
                  <Point X="4.51524981709" Y="0.641944116725" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.583052890246" Y="-0.430425777224" />
                  <Point X="3.103343063714" Y="-0.9822688063" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.898562563271" Y="-1.217841824425" />
                  <Point X="1.601358075322" Y="-2.710104885066" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.769240628529" Y="-3.667346506954" />
                  <Point X="0.718712478429" Y="-3.725472494505" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.771692329289" Y="0.792143437791" />
                  <Point X="4.679348728075" Y="0.685914276344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.669410236229" Y="-0.475887057911" />
                  <Point X="3.274627410729" Y="-0.93003274808" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.865595377114" Y="-1.400570277092" />
                  <Point X="2.214242935913" Y="-2.149865547416" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.105056035762" Y="-2.275470707832" />
                  <Point X="1.682106371969" Y="-2.762018638903" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.785796952511" Y="-3.793104678143" />
                  <Point X="0.748370722979" Y="-3.836158630198" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.770113277507" Y="-0.504845503951" />
                  <Point X="3.419330431174" Y="-0.908375008167" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.867311218296" Y="-1.543400470841" />
                  <Point X="2.437844289359" Y="-2.037445657836" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.00037671064" Y="-2.540694539579" />
                  <Point X="1.762854653911" Y="-2.813932409657" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.802353348673" Y="-3.918862766298" />
                  <Point X="0.77802896753" Y="-3.946844765891" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.872208897172" Y="-0.53220197181" />
                  <Point X="3.53338696343" Y="-0.92197202006" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.909552551505" Y="-1.639611418876" />
                  <Point X="2.570153503624" Y="-2.030045360999" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.017630508095" Y="-2.665650359319" />
                  <Point X="1.834146432777" Y="-2.876724642792" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.818909744836" Y="-4.044620854452" />
                  <Point X="0.80768721208" Y="-4.057530901583" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.974304516837" Y="-0.559558439668" />
                  <Point X="3.646336784401" Y="-0.936842157651" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.963068705771" Y="-1.72285216897" />
                  <Point X="2.678949545082" Y="-2.049693875312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.039283367792" Y="-2.785545636834" />
                  <Point X="1.893163559375" Y="-2.953637248106" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.076400148999" Y="-0.586914893151" />
                  <Point X="3.759286605371" Y="-0.951712295242" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.018930992556" Y="-1.803394002334" />
                  <Point X="2.786751696349" Y="-2.070485729501" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.077164249939" Y="-2.886772710012" />
                  <Point X="1.952180685973" Y="-3.03054985342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.178495808672" Y="-0.614271314986" />
                  <Point X="3.872236426342" Y="-0.966582432834" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.090843440529" Y="-1.865472237337" />
                  <Point X="2.877184989771" Y="-2.111258169025" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.127401247277" Y="-2.973785698637" />
                  <Point X="2.011197812571" Y="-3.107462458734" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.280591468344" Y="-0.641627736821" />
                  <Point X="3.985186247312" Y="-0.981452570425" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.166352793295" Y="-1.923412706702" />
                  <Point X="2.960997252903" Y="-2.159647232616" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.177638244615" Y="-3.060798687263" />
                  <Point X="2.070214939169" Y="-3.184375064047" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.382687128017" Y="-0.668984158657" />
                  <Point X="4.098136068283" Y="-0.996322708016" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.241862146061" Y="-1.981353176067" />
                  <Point X="3.044809516036" Y="-2.208036296208" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.227875241953" Y="-3.147811675889" />
                  <Point X="2.129232065767" Y="-3.261287669361" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.48478278769" Y="-0.696340580492" />
                  <Point X="4.211085889253" Y="-1.011192845608" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.317371498827" Y="-2.039293645433" />
                  <Point X="3.128621779168" Y="-2.2564253598" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.278112239291" Y="-3.234824664514" />
                  <Point X="2.188249192365" Y="-3.338200274675" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.586878447362" Y="-0.723697002327" />
                  <Point X="4.324035710224" Y="-1.026062983199" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.392880862349" Y="-2.097234102425" />
                  <Point X="3.212434042301" Y="-2.304814423392" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.328349226866" Y="-3.321837664371" />
                  <Point X="2.247266318963" Y="-3.415112879989" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.688974107035" Y="-0.751053424163" />
                  <Point X="4.436985531194" Y="-1.04093312079" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.468390277842" Y="-2.155174499631" />
                  <Point X="3.296246305433" Y="-2.353203486983" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.378586214109" Y="-3.408850664611" />
                  <Point X="2.306283445561" Y="-3.492025485302" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.783274898153" Y="-0.787376816521" />
                  <Point X="4.549935358336" Y="-1.055803251282" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.543899693335" Y="-2.213114896837" />
                  <Point X="3.380058568566" Y="-2.401592550575" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.428823201351" Y="-3.49586366485" />
                  <Point X="2.365300572159" Y="-3.568938090616" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.753553450949" Y="-0.966371473639" />
                  <Point X="4.662885228171" Y="-1.070673332662" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.619409108829" Y="-2.271055294043" />
                  <Point X="3.463870831698" Y="-2.449981614167" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.479060188594" Y="-3.582876665089" />
                  <Point X="2.424317698757" Y="-3.64585069593" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.694918524322" Y="-2.328995691248" />
                  <Point X="3.547683094831" Y="-2.498370677758" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.529297175836" Y="-3.669889665329" />
                  <Point X="2.483334825355" Y="-3.722763301244" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.770427939815" Y="-2.386936088454" />
                  <Point X="3.631495357964" Y="-2.54675974135" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.579534163079" Y="-3.756902665568" />
                  <Point X="2.542351951953" Y="-3.799675906557" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.845937355308" Y="-2.44487648566" />
                  <Point X="3.715307621096" Y="-2.595148804942" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.629771150321" Y="-3.843915665807" />
                  <Point X="2.601369097514" Y="-3.876588490057" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.921446770802" Y="-2.502816882866" />
                  <Point X="3.799119884229" Y="-2.643537868533" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.680008137564" Y="-3.930928666046" />
                  <Point X="2.660386278412" Y="-3.953501032906" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.996956186295" Y="-2.560757280072" />
                  <Point X="3.88293212122" Y="-2.691926962197" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.730245124807" Y="-4.017941666286" />
                  <Point X="2.719403459309" Y="-4.030413575756" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.072465601788" Y="-2.618697677278" />
                  <Point X="3.966744345067" Y="-2.740316070982" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="0.001625976562" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978485351562" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.763501281738" Y="-4.62673046875" />
                  <Point X="0.471539031982" Y="-3.537112304688" />
                  <Point X="0.464318664551" Y="-3.521544189453" />
                  <Point X="0.344986328125" Y="-3.349608886719" />
                  <Point X="0.300591033936" Y="-3.285643798828" />
                  <Point X="0.274335754395" Y="-3.266399902344" />
                  <Point X="0.089675842285" Y="-3.209088134766" />
                  <Point X="0.020976625443" Y="-3.187766601562" />
                  <Point X="-0.008664908409" Y="-3.187766601562" />
                  <Point X="-0.193324661255" Y="-3.245078125" />
                  <Point X="-0.262023895264" Y="-3.266399902344" />
                  <Point X="-0.288279144287" Y="-3.285643798828" />
                  <Point X="-0.40761151123" Y="-3.457578857422" />
                  <Point X="-0.45200680542" Y="-3.521544189453" />
                  <Point X="-0.459227142334" Y="-3.537112304688" />
                  <Point X="-0.567846069336" Y="-3.942483886719" />
                  <Point X="-0.84774395752" Y="-4.987077148438" />
                  <Point X="-1.020444702148" Y="-4.953555175781" />
                  <Point X="-1.100246459961" Y="-4.938065429688" />
                  <Point X="-1.314305175781" Y="-4.882989746094" />
                  <Point X="-1.351589599609" Y="-4.873396972656" />
                  <Point X="-1.344295654297" Y="-4.817994140625" />
                  <Point X="-1.309150146484" Y="-4.551038574219" />
                  <Point X="-1.309683227539" Y="-4.5347578125" />
                  <Point X="-1.353798461914" Y="-4.312976074219" />
                  <Point X="-1.370210693359" Y="-4.230466308594" />
                  <Point X="-1.386282714844" Y="-4.20262890625" />
                  <Point X="-1.556294067383" Y="-4.053532226562" />
                  <Point X="-1.619543457031" Y="-3.998063964844" />
                  <Point X="-1.649240844727" Y="-3.985762939453" />
                  <Point X="-1.874883789062" Y="-3.970973388672" />
                  <Point X="-1.958829956055" Y="-3.965471435547" />
                  <Point X="-1.989878540039" Y="-3.973791015625" />
                  <Point X="-2.177896484375" Y="-4.099420898438" />
                  <Point X="-2.247844970703" Y="-4.146159179688" />
                  <Point X="-2.259734130859" Y="-4.157294433594" />
                  <Point X="-2.320715332031" Y="-4.236766601563" />
                  <Point X="-2.457095214844" Y="-4.414500488281" />
                  <Point X="-2.738864257812" Y="-4.240036132813" />
                  <Point X="-2.855832275391" Y="-4.167612304688" />
                  <Point X="-3.1522421875" Y="-3.939386474609" />
                  <Point X="-3.228581054688" Y="-3.880608154297" />
                  <Point X="-3.048612548828" Y="-3.568893798828" />
                  <Point X="-2.506033691406" Y="-2.629119628906" />
                  <Point X="-2.499762695312" Y="-2.597593261719" />
                  <Point X="-2.513979736328" Y="-2.568764404297" />
                  <Point X="-2.531328369141" Y="-2.551415771484" />
                  <Point X="-2.560157226562" Y="-2.537198730469" />
                  <Point X="-2.59168359375" Y="-2.543469726562" />
                  <Point X="-2.941308837891" Y="-2.745325927734" />
                  <Point X="-3.842959472656" Y="-3.265894042969" />
                  <Point X="-4.069291259766" Y="-2.968540527344" />
                  <Point X="-4.161703613281" Y="-2.847129394531" />
                  <Point X="-4.374205078125" Y="-2.490796386719" />
                  <Point X="-4.431020019531" Y="-2.395526855469" />
                  <Point X="-4.113124023438" Y="-2.151596679688" />
                  <Point X="-3.163786865234" Y="-1.423144897461" />
                  <Point X="-3.145822021484" Y="-1.396013427734" />
                  <Point X="-3.140206542969" Y="-1.374332397461" />
                  <Point X="-3.138117431641" Y="-1.366266235352" />
                  <Point X="-3.140326416016" Y="-1.334595458984" />
                  <Point X="-3.161159179688" Y="-1.310638916016" />
                  <Point X="-3.1804609375" Y="-1.299278808594" />
                  <Point X="-3.187641845703" Y="-1.295052490234" />
                  <Point X="-3.219529296875" Y="-1.288571044922" />
                  <Point X="-3.660897949219" Y="-1.346678344727" />
                  <Point X="-4.803283203125" Y="-1.497076293945" />
                  <Point X="-4.891249511719" Y="-1.152691772461" />
                  <Point X="-4.927393066406" Y="-1.011191589355" />
                  <Point X="-4.983615722656" Y="-0.618088012695" />
                  <Point X="-4.998396484375" Y="-0.514742126465" />
                  <Point X="-4.638901855469" Y="-0.418415710449" />
                  <Point X="-3.557463867188" Y="-0.128645431519" />
                  <Point X="-3.541895996094" Y="-0.121425231934" />
                  <Point X="-3.521668457031" Y="-0.107386116028" />
                  <Point X="-3.514143066406" Y="-0.102163047791" />
                  <Point X="-3.494898681641" Y="-0.075907012939" />
                  <Point X="-3.488156005859" Y="-0.054182373047" />
                  <Point X="-3.485647705078" Y="-0.046100128174" />
                  <Point X="-3.485648193359" Y="-0.016458747864" />
                  <Point X="-3.492390625" Y="0.005266046524" />
                  <Point X="-3.494899169922" Y="0.01334828949" />
                  <Point X="-3.514143066406" Y="0.039603107452" />
                  <Point X="-3.534370605469" Y="0.05364207077" />
                  <Point X="-3.557463867188" Y="0.066085334778" />
                  <Point X="-3.959792236328" Y="0.173888946533" />
                  <Point X="-4.998186523438" Y="0.452125793457" />
                  <Point X="-4.940938476562" Y="0.839004699707" />
                  <Point X="-4.917645019531" Y="0.996418518066" />
                  <Point X="-4.804470703125" Y="1.414065795898" />
                  <Point X="-4.773516113281" Y="1.528298583984" />
                  <Point X="-4.528467285156" Y="1.496037475586" />
                  <Point X="-3.753266357422" Y="1.393980102539" />
                  <Point X="-3.731704833984" Y="1.395866577148" />
                  <Point X="-3.686934814453" Y="1.409982543945" />
                  <Point X="-3.670278808594" Y="1.415234130859" />
                  <Point X="-3.651534423828" Y="1.426056274414" />
                  <Point X="-3.639119873047" Y="1.443786254883" />
                  <Point X="-3.621155517578" Y="1.487155761719" />
                  <Point X="-3.614472412109" Y="1.503290527344" />
                  <Point X="-3.610714111328" Y="1.52460534668" />
                  <Point X="-3.616315917969" Y="1.545511474609" />
                  <Point X="-3.637991699219" Y="1.587150268555" />
                  <Point X="-3.646055664062" Y="1.602641235352" />
                  <Point X="-3.659968261719" Y="1.619221679688" />
                  <Point X="-3.890744873047" Y="1.796302734375" />
                  <Point X="-4.47610546875" Y="2.245466308594" />
                  <Point X="-4.250523925781" Y="2.631942138672" />
                  <Point X="-4.160014648438" Y="2.787006591797" />
                  <Point X="-3.860219726562" Y="3.172350585938" />
                  <Point X="-3.774671386719" Y="3.282310791016" />
                  <Point X="-3.635263916016" Y="3.201823974609" />
                  <Point X="-3.159156982422" Y="2.926943359375" />
                  <Point X="-3.138514892578" Y="2.920434814453" />
                  <Point X="-3.076162597656" Y="2.914979736328" />
                  <Point X="-3.052965576172" Y="2.912950195312" />
                  <Point X="-3.031506835938" Y="2.915775390625" />
                  <Point X="-3.013252441406" Y="2.927404785156" />
                  <Point X="-2.968994140625" Y="2.971662841797" />
                  <Point X="-2.952528808594" Y="2.988128173828" />
                  <Point X="-2.940899414062" Y="3.006382568359" />
                  <Point X="-2.93807421875" Y="3.027841308594" />
                  <Point X="-2.943529296875" Y="3.090193847656" />
                  <Point X="-2.945558837891" Y="3.113390869141" />
                  <Point X="-2.952067382812" Y="3.134032958984" />
                  <Point X="-3.054331542969" Y="3.311159423828" />
                  <Point X="-3.307278564453" Y="3.749276611328" />
                  <Point X="-2.910513183594" Y="4.053472412109" />
                  <Point X="-2.752876220703" Y="4.174331542969" />
                  <Point X="-2.280708251953" Y="4.436657714844" />
                  <Point X="-2.141548583984" Y="4.513971679688" />
                  <Point X="-2.113577636719" Y="4.47751953125" />
                  <Point X="-1.967826904297" Y="4.287573242188" />
                  <Point X="-1.951247070312" Y="4.273661132813" />
                  <Point X="-1.881848999023" Y="4.237534667969" />
                  <Point X="-1.856031005859" Y="4.224094238281" />
                  <Point X="-1.835124389648" Y="4.2184921875" />
                  <Point X="-1.813809082031" Y="4.222250488281" />
                  <Point X="-1.741526489258" Y="4.25219140625" />
                  <Point X="-1.714635009766" Y="4.263330078125" />
                  <Point X="-1.696905395508" Y="4.275744628906" />
                  <Point X="-1.686083496094" Y="4.294488769531" />
                  <Point X="-1.662556884766" Y="4.36910546875" />
                  <Point X="-1.653804077148" Y="4.396865234375" />
                  <Point X="-1.651917602539" Y="4.418426757812" />
                  <Point X="-1.663543945312" Y="4.506737304688" />
                  <Point X="-1.689137573242" Y="4.701141113281" />
                  <Point X="-1.172165405273" Y="4.84608203125" />
                  <Point X="-0.968094177246" Y="4.903296386719" />
                  <Point X="-0.395682861328" Y="4.970288574219" />
                  <Point X="-0.224199615479" Y="4.990358398438" />
                  <Point X="-0.182985931396" Y="4.836546386719" />
                  <Point X="-0.042140380859" Y="4.310903320312" />
                  <Point X="-0.024282115936" Y="4.284176757813" />
                  <Point X="0.006155934334" Y="4.273844238281" />
                  <Point X="0.036594036102" Y="4.284176757813" />
                  <Point X="0.054452251434" Y="4.310903320312" />
                  <Point X="0.106851142883" Y="4.506458984375" />
                  <Point X="0.236648406982" Y="4.990868652344" />
                  <Point X="0.682019775391" Y="4.9442265625" />
                  <Point X="0.860205871582" Y="4.925565429688" />
                  <Point X="1.333776245117" Y="4.811230957031" />
                  <Point X="1.508455932617" Y="4.769058105469" />
                  <Point X="1.817276489258" Y="4.657046386719" />
                  <Point X="1.931034301758" Y="4.615785644531" />
                  <Point X="2.229094726562" Y="4.476392578125" />
                  <Point X="2.33869921875" Y="4.425134277344" />
                  <Point X="2.62663671875" Y="4.257381347656" />
                  <Point X="2.732523681641" Y="4.19569140625" />
                  <Point X="3.004088134766" Y="4.002569824219" />
                  <Point X="3.068740234375" Y="3.956592773438" />
                  <Point X="2.857847900391" Y="3.591316162109" />
                  <Point X="2.229853515625" Y="2.503597900391" />
                  <Point X="2.224852050781" Y="2.491514648438" />
                  <Point X="2.209203857422" Y="2.432998291016" />
                  <Point X="2.203382324219" Y="2.411228271484" />
                  <Point X="2.202044677734" Y="2.392326171875" />
                  <Point X="2.208146240234" Y="2.341726074219" />
                  <Point X="2.210416015625" Y="2.322901367188" />
                  <Point X="2.218682128906" Y="2.300812255859" />
                  <Point X="2.249991699219" Y="2.254669921875" />
                  <Point X="2.261639892578" Y="2.237503662109" />
                  <Point X="2.274939941406" Y="2.224203613281" />
                  <Point X="2.321082275391" Y="2.192894042969" />
                  <Point X="2.338248535156" Y="2.18124609375" />
                  <Point X="2.360336669922" Y="2.172980224609" />
                  <Point X="2.410936767578" Y="2.166878662109" />
                  <Point X="2.448664306641" Y="2.165946289062" />
                  <Point X="2.507180664062" Y="2.181594482422" />
                  <Point X="2.528950683594" Y="2.187416015625" />
                  <Point X="2.541033935547" Y="2.192417480469" />
                  <Point X="2.945698974609" Y="2.42605078125" />
                  <Point X="3.994248046875" Y="3.031430908203" />
                  <Point X="4.139783203125" Y="2.829170166016" />
                  <Point X="4.202591796875" Y="2.741880126953" />
                  <Point X="4.353984863281" Y="2.491701904297" />
                  <Point X="4.387513183594" Y="2.436296142578" />
                  <Point X="4.1154296875" Y="2.227519042969" />
                  <Point X="3.288616210938" Y="1.593082763672" />
                  <Point X="3.27937109375" Y="1.583833129883" />
                  <Point X="3.237256591797" Y="1.528891601562" />
                  <Point X="3.221588867188" Y="1.508451782227" />
                  <Point X="3.213119384766" Y="1.4915" />
                  <Point X="3.197431640625" Y="1.435404418945" />
                  <Point X="3.191595458984" Y="1.41453527832" />
                  <Point X="3.190779541016" Y="1.390965332031" />
                  <Point X="3.203657470703" Y="1.328552001953" />
                  <Point X="3.208448486328" Y="1.305332519531" />
                  <Point X="3.215646484375" Y="1.287954956055" />
                  <Point X="3.250673095703" Y="1.234716064453" />
                  <Point X="3.263704101562" Y="1.214909667969" />
                  <Point X="3.280947998047" Y="1.198819824219" />
                  <Point X="3.331706298828" Y="1.170247314453" />
                  <Point X="3.350590087891" Y="1.159617431641" />
                  <Point X="3.368565429688" Y="1.153619628906" />
                  <Point X="3.437194091797" Y="1.144549438477" />
                  <Point X="3.462726074219" Y="1.141175048828" />
                  <Point X="3.475803710938" Y="1.14117175293" />
                  <Point X="3.860208251953" Y="1.191779663086" />
                  <Point X="4.848975097656" Y="1.32195324707" />
                  <Point X="4.912212402344" Y="1.062193725586" />
                  <Point X="4.939188476562" Y="0.951385803223" />
                  <Point X="4.986896484375" Y="0.64496282959" />
                  <Point X="4.997858398438" Y="0.574556152344" />
                  <Point X="4.688713867188" Y="0.491721038818" />
                  <Point X="3.741167724609" Y="0.237826919556" />
                  <Point X="3.729086914062" Y="0.232819412231" />
                  <Point X="3.661661376953" Y="0.19384614563" />
                  <Point X="3.636576904297" Y="0.179346878052" />
                  <Point X="3.622264892578" Y="0.166926422119" />
                  <Point X="3.581809570312" Y="0.115376953125" />
                  <Point X="3.566759033203" Y="0.096198890686" />
                  <Point X="3.556985351562" Y="0.074735580444" />
                  <Point X="3.543500244141" Y="0.004321252346" />
                  <Point X="3.538483154297" Y="-0.021875" />
                  <Point X="3.538482910156" Y="-0.040684635162" />
                  <Point X="3.551968261719" Y="-0.111098815918" />
                  <Point X="3.556985107422" Y="-0.137295227051" />
                  <Point X="3.566759033203" Y="-0.158758834839" />
                  <Point X="3.607214355469" Y="-0.210308456421" />
                  <Point X="3.622264892578" Y="-0.229486526489" />
                  <Point X="3.636576904297" Y="-0.241906967163" />
                  <Point X="3.704002441406" Y="-0.280880218506" />
                  <Point X="3.729086914062" Y="-0.295379486084" />
                  <Point X="3.741167724609" Y="-0.300387023926" />
                  <Point X="4.093684082031" Y="-0.394843475342" />
                  <Point X="4.998067871094" Y="-0.637172363281" />
                  <Point X="4.963494140625" Y="-0.866495117188" />
                  <Point X="4.948431640625" Y="-0.966399353027" />
                  <Point X="4.887309570312" Y="-1.234245605469" />
                  <Point X="4.874545898438" Y="-1.290178344727" />
                  <Point X="4.510869140625" Y="-1.242299560547" />
                  <Point X="3.411982177734" Y="-1.097628295898" />
                  <Point X="3.394836181641" Y="-1.098341186523" />
                  <Point X="3.262503662109" Y="-1.127104248047" />
                  <Point X="3.213271972656" Y="-1.137804931641" />
                  <Point X="3.1854453125" Y="-1.154697143555" />
                  <Point X="3.105458740234" Y="-1.250896240234" />
                  <Point X="3.075701171875" Y="-1.286685180664" />
                  <Point X="3.064357910156" Y="-1.314070678711" />
                  <Point X="3.052893798828" Y="-1.438652709961" />
                  <Point X="3.04862890625" Y="-1.485000854492" />
                  <Point X="3.056360351562" Y="-1.516621582031" />
                  <Point X="3.129595214844" Y="-1.630533569336" />
                  <Point X="3.156840576172" Y="-1.672912353516" />
                  <Point X="3.168460693359" Y="-1.685540649414" />
                  <Point X="3.495598144531" Y="-1.936562255859" />
                  <Point X="4.339074707031" Y="-2.583784179688" />
                  <Point X="4.246590332031" Y="-2.733437255859" />
                  <Point X="4.204131835938" Y="-2.802141845703" />
                  <Point X="4.077727783203" Y="-2.981744384766" />
                  <Point X="4.056688476562" Y="-3.011638427734" />
                  <Point X="3.732161621094" Y="-2.824272705078" />
                  <Point X="2.753454589844" Y="-2.259215576172" />
                  <Point X="2.737340820313" Y="-2.253312744141" />
                  <Point X="2.579843994141" Y="-2.224869140625" />
                  <Point X="2.521250488281" Y="-2.214287109375" />
                  <Point X="2.489077636719" Y="-2.219244873047" />
                  <Point X="2.358236572266" Y="-2.28810546875" />
                  <Point X="2.309559570312" Y="-2.313723876953" />
                  <Point X="2.288599853516" Y="-2.334683837891" />
                  <Point X="2.219739013672" Y="-2.465524902344" />
                  <Point X="2.194120849609" Y="-2.514201904297" />
                  <Point X="2.189163085938" Y="-2.546374511719" />
                  <Point X="2.217606689453" Y="-2.70387109375" />
                  <Point X="2.228188720703" Y="-2.76246484375" />
                  <Point X="2.234091552734" Y="-2.778578613281" />
                  <Point X="2.444310302734" Y="-3.142687988281" />
                  <Point X="2.986673583984" Y="-4.082088623047" />
                  <Point X="2.885680908203" Y="-4.154225585937" />
                  <Point X="2.835295654297" Y="-4.190214355469" />
                  <Point X="2.693970214844" Y="-4.281691894531" />
                  <Point X="2.679776123047" Y="-4.290879394531" />
                  <Point X="2.430021240234" Y="-3.965392822266" />
                  <Point X="1.683177612305" Y="-2.992087402344" />
                  <Point X="1.670549316406" Y="-2.980467529297" />
                  <Point X="1.515215087891" Y="-2.880601806641" />
                  <Point X="1.45742590332" Y="-2.843448730469" />
                  <Point X="1.425804931641" Y="-2.835717041016" />
                  <Point X="1.255920288086" Y="-2.851350097656" />
                  <Point X="1.192718017578" Y="-2.857166015625" />
                  <Point X="1.165332885742" Y="-2.868509277344" />
                  <Point X="1.03415246582" Y="-2.977581542969" />
                  <Point X="0.985349243164" Y="-3.018159667969" />
                  <Point X="0.968456726074" Y="-3.045986083984" />
                  <Point X="0.929234436035" Y="-3.226439697266" />
                  <Point X="0.91464251709" Y="-3.293573974609" />
                  <Point X="0.91392956543" Y="-3.310720214844" />
                  <Point X="0.97350378418" Y="-3.763232421875" />
                  <Point X="1.127642211914" Y="-4.934028320312" />
                  <Point X="1.042008666992" Y="-4.952799316406" />
                  <Point X="0.99434564209" Y="-4.963247070312" />
                  <Point X="0.863787658691" Y="-4.98696484375" />
                  <Point X="0.860200378418" Y="-4.987616699219" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#190" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.140845616077" Y="4.88076749562" Z="1.85" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.85" />
                  <Point X="-0.407650953605" Y="5.05122989569" Z="1.85" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.85" />
                  <Point X="-1.191819235171" Y="4.925505566177" Z="1.85" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.85" />
                  <Point X="-1.71927216427" Y="4.531490947242" Z="1.85" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.85" />
                  <Point X="-1.716398526572" Y="4.415420822026" Z="1.85" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.85" />
                  <Point X="-1.766816500346" Y="4.329665242761" Z="1.85" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.85" />
                  <Point X="-1.864917246349" Y="4.313164777058" Z="1.85" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.85" />
                  <Point X="-2.080065939453" Y="4.539237468223" Z="1.85" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.85" />
                  <Point X="-2.31114725499" Y="4.511645167607" Z="1.85" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.85" />
                  <Point X="-2.947091360621" Y="4.12443247928" Z="1.85" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.85" />
                  <Point X="-3.10378877057" Y="3.317439488696" Z="1.85" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.85" />
                  <Point X="-2.999495190897" Y="3.117115900401" Z="1.85" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.85" />
                  <Point X="-3.010505415587" Y="3.038298152299" Z="1.85" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.85" />
                  <Point X="-3.077960565206" Y="2.996069508276" Z="1.85" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.85" />
                  <Point X="-3.616419859601" Y="3.276405178175" Z="1.85" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.85" />
                  <Point X="-3.905838984517" Y="3.234333009581" Z="1.85" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.85" />
                  <Point X="-4.299861881057" Y="2.688427312386" Z="1.85" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.85" />
                  <Point X="-3.927338854868" Y="1.7879151881" Z="1.85" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.85" />
                  <Point X="-3.68849822299" Y="1.595343190706" Z="1.85" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.85" />
                  <Point X="-3.673505683189" Y="1.537569587837" Z="1.85" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.85" />
                  <Point X="-3.708125828762" Y="1.488948523886" Z="1.85" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.85" />
                  <Point X="-4.528097045836" Y="1.576889710165" Z="1.85" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.85" />
                  <Point X="-4.858886649521" Y="1.458423276055" Z="1.85" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.85" />
                  <Point X="-4.996555697973" Y="0.877603758203" Z="1.85" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.85" />
                  <Point X="-3.978889213866" Y="0.156872624547" Z="1.85" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.85" />
                  <Point X="-3.569035445265" Y="0.043846050766" Z="1.85" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.85" />
                  <Point X="-3.546299386176" Y="0.021724690992" Z="1.85" />
                  <Point X="-3.539556741714" Y="0" Z="1.85" />
                  <Point X="-3.542065214959" Y="-0.008082260069" Z="1.85" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.85" />
                  <Point X="-3.556333149858" Y="-0.035029932368" Z="1.85" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.85" />
                  <Point X="-4.657998574907" Y="-0.338839432209" Z="1.85" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.85" />
                  <Point X="-5.039267824775" Y="-0.593886927682" Z="1.85" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.85" />
                  <Point X="-4.945850354392" Y="-1.133785952976" Z="1.85" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.85" />
                  <Point X="-3.660527541549" Y="-1.364970582421" Z="1.85" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.85" />
                  <Point X="-3.211978042183" Y="-1.311089641238" Z="1.85" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.85" />
                  <Point X="-3.19476540718" Y="-1.330515906967" Z="1.85" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.85" />
                  <Point X="-4.14971775615" Y="-2.080649004995" Z="1.85" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.85" />
                  <Point X="-4.423304640741" Y="-2.485125996116" Z="1.85" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.85" />
                  <Point X="-4.115233149693" Y="-2.967543663381" Z="1.85" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.85" />
                  <Point X="-2.922464696519" Y="-2.757347169988" Z="1.85" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.85" />
                  <Point X="-2.568135383896" Y="-2.560195085235" Z="1.85" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.85" />
                  <Point X="-3.09806984318" Y="-3.512613685973" Z="1.85" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.85" />
                  <Point X="-3.188902084744" Y="-3.94772363362" Z="1.85" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.85" />
                  <Point X="-2.771342103613" Y="-4.251266603205" Z="1.85" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.85" />
                  <Point X="-2.28720347977" Y="-4.235924402299" Z="1.85" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.85" />
                  <Point X="-2.156273808638" Y="-4.109714038081" Z="1.85" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.85" />
                  <Point X="-1.884309805758" Y="-3.98958649778" Z="1.85" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.85" />
                  <Point X="-1.595417340278" Y="-4.059844340728" Z="1.85" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.85" />
                  <Point X="-1.408993748942" Y="-4.291450282129" Z="1.85" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.85" />
                  <Point X="-1.400023887479" Y="-4.780187569521" Z="1.85" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.85" />
                  <Point X="-1.332919706917" Y="-4.900132698248" Z="1.85" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.85" />
                  <Point X="-1.036127971058" Y="-4.971359116382" Z="1.85" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.85" />
                  <Point X="-0.525705814637" Y="-3.924144863561" Z="1.85" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.85" />
                  <Point X="-0.372691435677" Y="-3.45480785161" Z="1.85" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.85" />
                  <Point X="-0.184659870035" Y="-3.261550977512" Z="1.85" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.85" />
                  <Point X="0.068699209326" Y="-3.225561067776" Z="1.85" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.85" />
                  <Point X="0.297754423755" Y="-3.346837904158" Z="1.85" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.85" />
                  <Point X="0.709049045764" Y="-4.608391166255" Z="1.85" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.85" />
                  <Point X="0.866568471871" Y="-5.004879534438" Z="1.85" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.85" />
                  <Point X="1.046558250586" Y="-4.970359663257" Z="1.85" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.85" />
                  <Point X="1.016920164634" Y="-3.725425989298" Z="1.85" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.85" />
                  <Point X="0.971937702904" Y="-3.205779420157" Z="1.85" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.85" />
                  <Point X="1.059963241597" Y="-2.984747731877" Z="1.85" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.85" />
                  <Point X="1.25434603461" Y="-2.869859508221" Z="1.85" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.85" />
                  <Point X="1.48201963014" Y="-2.891379688441" Z="1.85" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.85" />
                  <Point X="2.3841976057" Y="-3.964550741783" Z="1.85" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.85" />
                  <Point X="2.714983136558" Y="-4.292385865396" Z="1.85" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.85" />
                  <Point X="2.908586408766" Y="-4.163632444467" Z="1.85" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.85" />
                  <Point X="2.481455643094" Y="-3.086408019527" Z="1.85" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.85" />
                  <Point X="2.260654917455" Y="-2.663704935811" Z="1.85" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.85" />
                  <Point X="2.257829450342" Y="-2.45753124579" Z="1.85" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.85" />
                  <Point X="2.375367132582" Y="-2.301071859625" Z="1.85" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.85" />
                  <Point X="2.564801867702" Y="-2.242793092036" Z="1.85" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.85" />
                  <Point X="3.70100557758" Y="-2.836293857181" Z="1.85" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.85" />
                  <Point X="4.112460354889" Y="-2.979241340339" Z="1.85" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.85" />
                  <Point X="4.282967523818" Y="-2.728442348251" Z="1.85" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.85" />
                  <Point X="3.51988026652" Y="-1.865614450636" Z="1.85" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.85" />
                  <Point X="3.165497231376" Y="-1.572214299423" Z="1.85" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.85" />
                  <Point X="3.096527466974" Y="-1.411954152346" Z="1.85" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.85" />
                  <Point X="3.137748949754" Y="-1.251583205888" Z="1.85" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.85" />
                  <Point X="3.266967338664" Y="-1.144683445421" Z="1.85" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.85" />
                  <Point X="4.498186694127" Y="-1.260591609819" Z="1.85" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.85" />
                  <Point X="4.929900680238" Y="-1.214089418129" Z="1.85" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.85" />
                  <Point X="5.006779368694" Y="-0.842669304092" Z="1.85" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.85" />
                  <Point X="4.100469216597" Y="-0.315267148572" Z="1.85" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.85" />
                  <Point X="3.722868290457" Y="-0.206311398731" Z="1.85" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.85" />
                  <Point X="3.640392069169" Y="-0.148160148139" Z="1.85" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.85" />
                  <Point X="3.594919841868" Y="-0.07041424442" Z="1.85" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.85" />
                  <Point X="3.586451608556" Y="0.026196286773" Z="1.85" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.85" />
                  <Point X="3.614987369232" Y="0.115788590436" Z="1.85" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.85" />
                  <Point X="3.680527123895" Y="0.181837445511" Z="1.85" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.85" />
                  <Point X="4.695498600709" Y="0.474704784197" Z="1.85" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.85" />
                  <Point X="5.030145494343" Y="0.683934969314" Z="1.85" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.85" />
                  <Point X="4.954637596858" Y="1.10530080206" Z="1.85" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.85" />
                  <Point X="3.84752602934" Y="1.272631926384" Z="1.85" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.85" />
                  <Point X="3.437589381952" Y="1.225398452228" Z="1.85" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.85" />
                  <Point X="3.350076926516" Y="1.24509846255" Z="1.85" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.85" />
                  <Point X="3.28628757591" Y="1.2934775419" Z="1.85" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.85" />
                  <Point X="3.246469994276" Y="1.369936040454" Z="1.85" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.85" />
                  <Point X="3.239428329133" Y="1.453218400399" Z="1.85" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.85" />
                  <Point X="3.27078382623" Y="1.52975363498" Z="1.85" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.85" />
                  <Point X="4.139711591082" Y="2.219131339389" Z="1.85" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.85" />
                  <Point X="4.390606108593" Y="2.548868214391" Z="1.85" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.85" />
                  <Point X="4.174212670996" Y="2.889652995241" Z="1.85" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.85" />
                  <Point X="2.914542907773" Y="2.500632194061" Z="1.85" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.85" />
                  <Point X="2.488108015953" Y="2.261177211859" Z="1.85" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.85" />
                  <Point X="2.410766857639" Y="2.247799096782" Z="1.85" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.85" />
                  <Point X="2.343000406177" Y="2.26554861784" Z="1.85" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.85" />
                  <Point X="2.285209982395" Y="2.314024454205" Z="1.85" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.85" />
                  <Point X="2.251630605977" Y="2.378991586193" Z="1.85" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.85" />
                  <Point X="2.251350730798" Y="2.451361559929" Z="1.85" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.85" />
                  <Point X="2.894993158799" Y="3.597596174182" Z="1.85" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.85" />
                  <Point X="3.026909100427" Y="4.074597017319" Z="1.85" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.85" />
                  <Point X="2.645650556547" Y="4.33186407796" Z="1.85" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.85" />
                  <Point X="2.244120274498" Y="4.552964840587" Z="1.85" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.85" />
                  <Point X="1.828169053761" Y="4.735330516528" Z="1.85" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.85" />
                  <Point X="1.339354656043" Y="4.891114719056" Z="1.85" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.85" />
                  <Point X="0.681071780049" Y="5.025234493856" Z="1.85" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.85" />
                  <Point X="0.052398945476" Y="4.550680010331" Z="1.85" />
                  <Point X="0" Y="4.355124473572" Z="1.85" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>