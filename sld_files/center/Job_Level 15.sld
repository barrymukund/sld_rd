<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#145" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1208" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="0" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000475830078" Y="0.004715820312" />
                  <Width Value="9.783896972656" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.673898681641" Y="-3.925276855469" />
                  <Point X="0.563302062988" Y="-3.512524414063" />
                  <Point X="0.557721191406" Y="-3.497141845703" />
                  <Point X="0.542363037109" Y="-3.467377197266" />
                  <Point X="0.497159393311" Y="-3.402247314453" />
                  <Point X="0.378635528564" Y="-3.231476806641" />
                  <Point X="0.356751586914" Y="-3.209021484375" />
                  <Point X="0.330496398926" Y="-3.189777587891" />
                  <Point X="0.302495178223" Y="-3.175669189453" />
                  <Point X="0.232545150757" Y="-3.153959228516" />
                  <Point X="0.049136188507" Y="-3.097035888672" />
                  <Point X="0.020976791382" Y="-3.092766601562" />
                  <Point X="-0.008664840698" Y="-3.092766601562" />
                  <Point X="-0.036824085236" Y="-3.097035888672" />
                  <Point X="-0.106774116516" Y="-3.118745605469" />
                  <Point X="-0.290183074951" Y="-3.175669189453" />
                  <Point X="-0.31818460083" Y="-3.189777587891" />
                  <Point X="-0.344439788818" Y="-3.209021484375" />
                  <Point X="-0.366323425293" Y="-3.2314765625" />
                  <Point X="-0.411527069092" Y="-3.296606201172" />
                  <Point X="-0.530051086426" Y="-3.467376953125" />
                  <Point X="-0.538189025879" Y="-3.481573730469" />
                  <Point X="-0.550990112305" Y="-3.512524414063" />
                  <Point X="-0.840974609375" Y="-4.59476171875" />
                  <Point X="-0.916584655762" Y="-4.876941894531" />
                  <Point X="-1.079339233398" Y="-4.845350585938" />
                  <Point X="-1.156754394531" Y="-4.825432128906" />
                  <Point X="-1.24641809082" Y="-4.802362792969" />
                  <Point X="-1.228276367188" Y="-4.6645625" />
                  <Point X="-1.214963012695" Y="-4.563438476562" />
                  <Point X="-1.214201171875" Y="-4.5479296875" />
                  <Point X="-1.216508666992" Y="-4.516224121094" />
                  <Point X="-1.233219726562" Y="-4.432211914062" />
                  <Point X="-1.277036132812" Y="-4.211932617188" />
                  <Point X="-1.287938354492" Y="-4.182966308594" />
                  <Point X="-1.304010375977" Y="-4.15512890625" />
                  <Point X="-1.32364453125" Y="-4.131204101562" />
                  <Point X="-1.388045654297" Y="-4.074725830078" />
                  <Point X="-1.556905517578" Y="-3.926639404297" />
                  <Point X="-1.583188476562" Y="-3.910295410156" />
                  <Point X="-1.612885742188" Y="-3.897994384766" />
                  <Point X="-1.64302734375" Y="-3.890966308594" />
                  <Point X="-1.728502075195" Y="-3.885364013672" />
                  <Point X="-1.952616577148" Y="-3.870674804688" />
                  <Point X="-1.98341796875" Y="-3.873708496094" />
                  <Point X="-2.014466674805" Y="-3.882028076172" />
                  <Point X="-2.042657714844" Y="-3.894801513672" />
                  <Point X="-2.113879882812" Y="-3.942390625" />
                  <Point X="-2.300624267578" Y="-4.067169677734" />
                  <Point X="-2.312785644531" Y="-4.076821533203" />
                  <Point X="-2.335102539062" Y="-4.099461914062" />
                  <Point X="-2.480148925781" Y="-4.288489746094" />
                  <Point X="-2.489931640625" Y="-4.282432617188" />
                  <Point X="-2.801706787109" Y="-4.089389404297" />
                  <Point X="-2.908903808594" Y="-4.0068515625" />
                  <Point X="-3.104721923828" Y="-3.856077880859" />
                  <Point X="-2.629292724609" Y="-3.032610595703" />
                  <Point X="-2.423761230469" Y="-2.676619628906" />
                  <Point X="-2.412859130859" Y="-2.647653320312" />
                  <Point X="-2.406588134766" Y="-2.616126953125" />
                  <Point X="-2.405575439453" Y="-2.585192871094" />
                  <Point X="-2.414560058594" Y="-2.555575195312" />
                  <Point X="-2.428777099609" Y="-2.526746337891" />
                  <Point X="-2.4468046875" Y="-2.501589355469" />
                  <Point X="-2.464153320312" Y="-2.484240722656" />
                  <Point X="-2.489310302734" Y="-2.466213134766" />
                  <Point X="-2.518139160156" Y="-2.45199609375" />
                  <Point X="-2.547756835938" Y="-2.443011474609" />
                  <Point X="-2.578690917969" Y="-2.444024169922" />
                  <Point X="-2.610217285156" Y="-2.450295166016" />
                  <Point X="-2.63918359375" Y="-2.461197265625" />
                  <Point X="-3.572592041016" Y="-3.000100830078" />
                  <Point X="-3.818024414062" Y="-3.141801269531" />
                  <Point X="-3.836539306641" Y="-3.1174765625" />
                  <Point X="-4.082859130859" Y="-2.793862792969" />
                  <Point X="-4.159708007812" Y="-2.664998535156" />
                  <Point X="-4.306142578125" Y="-2.419450439453" />
                  <Point X="-3.465567871094" Y="-1.774454467773" />
                  <Point X="-3.105954589844" Y="-1.498513305664" />
                  <Point X="-3.083062744141" Y="-1.475877319336" />
                  <Point X="-3.063723632812" Y="-1.444807373047" />
                  <Point X="-3.0548828125" Y="-1.421526489258" />
                  <Point X="-3.051729248047" Y="-1.411619506836" />
                  <Point X="-3.046151855469" Y="-1.390085083008" />
                  <Point X="-3.042037597656" Y="-1.358602783203" />
                  <Point X="-3.042736816406" Y="-1.335734375" />
                  <Point X="-3.048883300781" Y="-1.313696411133" />
                  <Point X="-3.060888183594" Y="-1.284713867188" />
                  <Point X="-3.073728027344" Y="-1.262667602539" />
                  <Point X="-3.091971923828" Y="-1.244833374023" />
                  <Point X="-3.111785888672" Y="-1.230100585938" />
                  <Point X="-3.120284179688" Y="-1.224463745117" />
                  <Point X="-3.139455078125" Y="-1.213180664062" />
                  <Point X="-3.168718261719" Y="-1.201956420898" />
                  <Point X="-3.200605957031" Y="-1.195474853516" />
                  <Point X="-3.231929199219" Y="-1.194383789062" />
                  <Point X="-4.410268554688" Y="-1.349515136719" />
                  <Point X="-4.732102050781" Y="-1.391885375977" />
                  <Point X="-4.737739257812" Y="-1.369815429688" />
                  <Point X="-4.834077636719" Y="-0.992654174805" />
                  <Point X="-4.854409179688" Y="-0.850496704102" />
                  <Point X="-4.892424316406" Y="-0.584698181152" />
                  <Point X="-3.942529785156" Y="-0.330174682617" />
                  <Point X="-3.532875976562" Y="-0.220408340454" />
                  <Point X="-3.512341308594" Y="-0.212245361328" />
                  <Point X="-3.489110839844" Y="-0.199706863403" />
                  <Point X="-3.480066162109" Y="-0.194151184082" />
                  <Point X="-3.459975585938" Y="-0.180207107544" />
                  <Point X="-3.436020751953" Y="-0.158680801392" />
                  <Point X="-3.415194091797" Y="-0.12850440979" />
                  <Point X="-3.405214599609" Y="-0.105537315369" />
                  <Point X="-3.401614013672" Y="-0.095837142944" />
                  <Point X="-3.394917236328" Y="-0.074259681702" />
                  <Point X="-3.389474365234" Y="-0.045520599365" />
                  <Point X="-3.390396728516" Y="-0.011908434868" />
                  <Point X="-3.395197753906" Y="0.011141849518" />
                  <Point X="-3.397471435547" Y="0.019930515289" />
                  <Point X="-3.404168457031" Y="0.041507976532" />
                  <Point X="-3.417485351562" Y="0.070832366943" />
                  <Point X="-3.440206542969" Y="0.099962768555" />
                  <Point X="-3.459687255859" Y="0.116798446655" />
                  <Point X="-3.467637939453" Y="0.122965240479" />
                  <Point X="-3.487728515625" Y="0.136909164429" />
                  <Point X="-3.501925292969" Y="0.145047103882" />
                  <Point X="-3.532875976562" Y="0.157848342896" />
                  <Point X="-4.60698828125" Y="0.445655853271" />
                  <Point X="-4.89181640625" Y="0.521975219727" />
                  <Point X="-4.886576171875" Y="0.557387878418" />
                  <Point X="-4.824488769531" Y="0.976970275879" />
                  <Point X="-4.783559082031" Y="1.128013305664" />
                  <Point X="-4.703551269531" Y="1.423267822266" />
                  <Point X="-4.059316162109" Y="1.338452758789" />
                  <Point X="-3.765666259766" Y="1.29979284668" />
                  <Point X="-3.744986083984" Y="1.299341674805" />
                  <Point X="-3.723424560547" Y="1.301228149414" />
                  <Point X="-3.703137695312" Y="1.305263671875" />
                  <Point X="-3.686178710938" Y="1.310610717773" />
                  <Point X="-3.641711669922" Y="1.324631225586" />
                  <Point X="-3.622779052734" Y="1.332961669922" />
                  <Point X="-3.604034667969" Y="1.343783569336" />
                  <Point X="-3.587353515625" Y="1.356014648438" />
                  <Point X="-3.573715087891" Y="1.37156628418" />
                  <Point X="-3.561300537109" Y="1.389296020508" />
                  <Point X="-3.551351318359" Y="1.407431030273" />
                  <Point X="-3.544546386719" Y="1.423859619141" />
                  <Point X="-3.526703857422" Y="1.466935302734" />
                  <Point X="-3.520915527344" Y="1.486794067383" />
                  <Point X="-3.517157226562" Y="1.508109130859" />
                  <Point X="-3.5158046875" Y="1.528749267578" />
                  <Point X="-3.518951171875" Y="1.549192871094" />
                  <Point X="-3.524552978516" Y="1.570099121094" />
                  <Point X="-3.532050048828" Y="1.589377929688" />
                  <Point X="-3.540260986328" Y="1.605150878906" />
                  <Point X="-3.561789794922" Y="1.646507568359" />
                  <Point X="-3.57328125" Y="1.663705932617" />
                  <Point X="-3.587193847656" Y="1.680286376953" />
                  <Point X="-3.602135986328" Y="1.694590209961" />
                  <Point X="-4.218249023438" Y="2.167350830078" />
                  <Point X="-4.351859863281" Y="2.269874511719" />
                  <Point X="-4.322404785156" Y="2.320338378906" />
                  <Point X="-4.081152832031" Y="2.733660888672" />
                  <Point X="-3.972738525391" Y="2.873011962891" />
                  <Point X="-3.750504882813" Y="3.158661865234" />
                  <Point X="-3.387008544922" Y="2.948796875" />
                  <Point X="-3.206656982422" Y="2.844670898438" />
                  <Point X="-3.187724609375" Y="2.836340332031" />
                  <Point X="-3.167082519531" Y="2.829831787109" />
                  <Point X="-3.146794677734" Y="2.825796386719" />
                  <Point X="-3.123175292969" Y="2.823729980469" />
                  <Point X="-3.061245361328" Y="2.818311767578" />
                  <Point X="-3.040565185547" Y="2.818762939453" />
                  <Point X="-3.019106445312" Y="2.821588134766" />
                  <Point X="-2.999014648438" Y="2.826504638672" />
                  <Point X="-2.980463134766" Y="2.835653320313" />
                  <Point X="-2.962208740234" Y="2.847282714844" />
                  <Point X="-2.946077880859" Y="2.860229248047" />
                  <Point X="-2.9293125" Y="2.876994384766" />
                  <Point X="-2.885354248047" Y="2.920952636719" />
                  <Point X="-2.872406982422" Y="2.937083984375" />
                  <Point X="-2.860777587891" Y="2.955338134766" />
                  <Point X="-2.85162890625" Y="2.973889892578" />
                  <Point X="-2.846712158203" Y="2.993981933594" />
                  <Point X="-2.843886962891" Y="3.015440917969" />
                  <Point X="-2.843435791016" Y="3.03612109375" />
                  <Point X="-2.845502197266" Y="3.059740478516" />
                  <Point X="-2.850920410156" Y="3.121670654297" />
                  <Point X="-2.854955810547" Y="3.141958496094" />
                  <Point X="-2.861464355469" Y="3.162600585938" />
                  <Point X="-2.869794921875" Y="3.181532958984" />
                  <Point X="-3.142813476562" Y="3.654414794922" />
                  <Point X="-3.183333007812" Y="3.724596679688" />
                  <Point X="-3.120805419922" Y="3.772535888672" />
                  <Point X="-2.700626464844" Y="4.094683105469" />
                  <Point X="-2.529876953125" Y="4.189547363281" />
                  <Point X="-2.167036621094" Y="4.391133789062" />
                  <Point X="-2.098406738281" Y="4.301693359375" />
                  <Point X="-2.04319543457" Y="4.229740722656" />
                  <Point X="-2.028891845703" Y="4.214799316406" />
                  <Point X="-2.012311401367" Y="4.20088671875" />
                  <Point X="-1.995113647461" Y="4.189395019531" />
                  <Point X="-1.968825439453" Y="4.175709960937" />
                  <Point X="-1.899897583008" Y="4.139828125" />
                  <Point X="-1.88061706543" Y="4.132330566406" />
                  <Point X="-1.859710449219" Y="4.126729003906" />
                  <Point X="-1.839267333984" Y="4.123582519531" />
                  <Point X="-1.818628051758" Y="4.124935546875" />
                  <Point X="-1.797312988281" Y="4.128693847656" />
                  <Point X="-1.77745324707" Y="4.134482421875" />
                  <Point X="-1.750072265625" Y="4.14582421875" />
                  <Point X="-1.678279296875" Y="4.175562011719" />
                  <Point X="-1.66014465332" Y="4.185510742187" />
                  <Point X="-1.642415039062" Y="4.197925292969" />
                  <Point X="-1.626863525391" Y="4.211563964844" />
                  <Point X="-1.614632568359" Y="4.228245117188" />
                  <Point X="-1.603810791016" Y="4.246989257813" />
                  <Point X="-1.59548046875" Y="4.265921386719" />
                  <Point X="-1.586568359375" Y="4.294186523438" />
                  <Point X="-1.563201171875" Y="4.368297851562" />
                  <Point X="-1.559165649414" Y="4.388584960938" />
                  <Point X="-1.557279174805" Y="4.410146484375" />
                  <Point X="-1.55773034668" Y="4.430826660156" />
                  <Point X="-1.584201904297" Y="4.631897949219" />
                  <Point X="-1.493583862305" Y="4.657304199219" />
                  <Point X="-0.949635009766" Y="4.809808105469" />
                  <Point X="-0.742642822266" Y="4.834033691406" />
                  <Point X="-0.294710784912" Y="4.886457519531" />
                  <Point X="-0.187256271362" Y="4.485431640625" />
                  <Point X="-0.133903305054" Y="4.286315429688" />
                  <Point X="-0.121129844666" Y="4.258124023438" />
                  <Point X="-0.10327155304" Y="4.231397460938" />
                  <Point X="-0.0821145401" Y="4.20880859375" />
                  <Point X="-0.054819446564" Y="4.19421875" />
                  <Point X="-0.024381370544" Y="4.183886230469" />
                  <Point X="0.006155907631" Y="4.178844238281" />
                  <Point X="0.03669316864" Y="4.183886230469" />
                  <Point X="0.067131248474" Y="4.19421875" />
                  <Point X="0.094426467896" Y="4.20880859375" />
                  <Point X="0.115583503723" Y="4.231397460938" />
                  <Point X="0.133441802979" Y="4.258124023438" />
                  <Point X="0.146215255737" Y="4.286315429688" />
                  <Point X="0.286106811523" Y="4.808397949219" />
                  <Point X="0.307419403076" Y="4.8879375" />
                  <Point X="0.369089111328" Y="4.881479003906" />
                  <Point X="0.844041015625" Y="4.831738769531" />
                  <Point X="1.015297485352" Y="4.790392089844" />
                  <Point X="1.481026611328" Y="4.677950683594" />
                  <Point X="1.591424316406" Y="4.637908691406" />
                  <Point X="1.894646362305" Y="4.527927734375" />
                  <Point X="2.002435913086" Y="4.477518066406" />
                  <Point X="2.294576171875" Y="4.340893554688" />
                  <Point X="2.398736572266" Y="4.280209472656" />
                  <Point X="2.680978759766" Y="4.115774902344" />
                  <Point X="2.779189941406" Y="4.045932373047" />
                  <Point X="2.943260009766" Y="3.929254638672" />
                  <Point X="2.385468261719" Y="2.963130859375" />
                  <Point X="2.147581054688" Y="2.551097900391" />
                  <Point X="2.142075927734" Y="2.539931152344" />
                  <Point X="2.133076660156" Y="2.516056640625" />
                  <Point X="2.127149169922" Y="2.493890380859" />
                  <Point X="2.111606933594" Y="2.435770263672" />
                  <Point X="2.108413330078" Y="2.408799804688" />
                  <Point X="2.109386962891" Y="2.370729736328" />
                  <Point X="2.1100390625" Y="2.361785644531" />
                  <Point X="2.116099121094" Y="2.311528320312" />
                  <Point X="2.121442138672" Y="2.289604980469" />
                  <Point X="2.129708251953" Y="2.267516357422" />
                  <Point X="2.140071044922" Y="2.247471191406" />
                  <Point X="2.151931396484" Y="2.2299921875" />
                  <Point X="2.183028808594" Y="2.184162597656" />
                  <Point X="2.19446484375" Y="2.170328857422" />
                  <Point X="2.221598388672" Y="2.145593017578" />
                  <Point X="2.239077148438" Y="2.133732666016" />
                  <Point X="2.284906982422" Y="2.102635498047" />
                  <Point X="2.304952636719" Y="2.092271972656" />
                  <Point X="2.327040771484" Y="2.084006103516" />
                  <Point X="2.348963867188" Y="2.078663330078" />
                  <Point X="2.368131347656" Y="2.076352050781" />
                  <Point X="2.418388671875" Y="2.070291748047" />
                  <Point X="2.436467529297" Y="2.069845703125" />
                  <Point X="2.47320703125" Y="2.074171142578" />
                  <Point X="2.495373291016" Y="2.080098876953" />
                  <Point X="2.553493408203" Y="2.095640869141" />
                  <Point X="2.565283447266" Y="2.099638183594" />
                  <Point X="2.588533935547" Y="2.110145019531" />
                  <Point X="3.668884277344" Y="2.733885498047" />
                  <Point X="3.967326416016" Y="2.906191162109" />
                  <Point X="4.123272460938" Y="2.689461425781" />
                  <Point X="4.178023925781" Y="2.598984130859" />
                  <Point X="4.262198730469" Y="2.459884277344" />
                  <Point X="3.543984863281" Y="1.908778808594" />
                  <Point X="3.230783935547" Y="1.668451293945" />
                  <Point X="3.221424560547" Y="1.660241455078" />
                  <Point X="3.203973876953" Y="1.641627929688" />
                  <Point X="3.188020751953" Y="1.620815795898" />
                  <Point X="3.146191650391" Y="1.566246459961" />
                  <Point X="3.13660546875" Y="1.550911254883" />
                  <Point X="3.121630126953" Y="1.517086425781" />
                  <Point X="3.1156875" Y="1.495837036133" />
                  <Point X="3.100106201172" Y="1.440121826172" />
                  <Point X="3.096652587891" Y="1.417823242188" />
                  <Point X="3.095836425781" Y="1.394252929688" />
                  <Point X="3.097739257812" Y="1.371768310547" />
                  <Point X="3.102617431641" Y="1.348125854492" />
                  <Point X="3.115408203125" Y="1.286135498047" />
                  <Point X="3.120679931641" Y="1.268977661133" />
                  <Point X="3.136282470703" Y="1.235740478516" />
                  <Point X="3.14955078125" Y="1.215573120117" />
                  <Point X="3.184340087891" Y="1.162695068359" />
                  <Point X="3.198893554688" Y="1.145450439453" />
                  <Point X="3.216137451172" Y="1.129360595703" />
                  <Point X="3.234346923828" Y="1.116034912109" />
                  <Point X="3.253574462891" Y="1.105211425781" />
                  <Point X="3.303989013672" Y="1.076832519531" />
                  <Point X="3.320521728516" Y="1.069501342773" />
                  <Point X="3.356118164062" Y="1.059438598633" />
                  <Point X="3.382114990234" Y="1.056002685547" />
                  <Point X="3.450278808594" Y="1.046994018555" />
                  <Point X="3.462702392578" Y="1.046175048828" />
                  <Point X="3.488203613281" Y="1.04698449707" />
                  <Point X="4.514463867188" Y="1.182094238281" />
                  <Point X="4.776839355469" Y="1.21663659668" />
                  <Point X="4.845936035156" Y="0.932809265137" />
                  <Point X="4.863189941406" Y="0.82199029541" />
                  <Point X="4.890864746094" Y="0.644238525391" />
                  <Point X="4.075514648438" Y="0.425766143799" />
                  <Point X="3.716579833984" Y="0.329589935303" />
                  <Point X="3.704791259766" Y="0.325586608887" />
                  <Point X="3.681545166016" Y="0.315067840576" />
                  <Point X="3.656004150391" Y="0.300304412842" />
                  <Point X="3.58903515625" Y="0.261595275879" />
                  <Point X="3.574311035156" Y="0.251096221924" />
                  <Point X="3.547530761719" Y="0.22557661438" />
                  <Point X="3.532206054688" Y="0.206049240112" />
                  <Point X="3.492024902344" Y="0.154848968506" />
                  <Point X="3.480301025391" Y="0.135569152832" />
                  <Point X="3.470527099609" Y="0.114105384827" />
                  <Point X="3.463680908203" Y="0.092604576111" />
                  <Point X="3.458572509766" Y="0.06593120575" />
                  <Point X="3.445178710938" Y="-0.004005922318" />
                  <Point X="3.443482910156" Y="-0.021875295639" />
                  <Point X="3.445178710938" Y="-0.058554080963" />
                  <Point X="3.450287109375" Y="-0.085227302551" />
                  <Point X="3.463680908203" Y="-0.155164581299" />
                  <Point X="3.470527099609" Y="-0.176665542603" />
                  <Point X="3.480301025391" Y="-0.198129302979" />
                  <Point X="3.492025146484" Y="-0.21740927124" />
                  <Point X="3.507349853516" Y="-0.236936508179" />
                  <Point X="3.547531005859" Y="-0.288136932373" />
                  <Point X="3.559999023438" Y="-0.30123614502" />
                  <Point X="3.589035888672" Y="-0.324155731201" />
                  <Point X="3.614577148438" Y="-0.338919006348" />
                  <Point X="3.681545898438" Y="-0.377628295898" />
                  <Point X="3.692710205078" Y="-0.383139038086" />
                  <Point X="3.716579833984" Y="-0.392149932861" />
                  <Point X="4.657707519531" Y="-0.64432421875" />
                  <Point X="4.89147265625" Y="-0.706961486816" />
                  <Point X="4.855022460938" Y="-0.948726074219" />
                  <Point X="4.832917480469" Y="-1.045594604492" />
                  <Point X="4.801173828125" Y="-1.184698974609" />
                  <Point X="3.840645751953" Y="-1.058243041992" />
                  <Point X="3.424382080078" Y="-1.003441040039" />
                  <Point X="3.408035644531" Y="-1.002710327148" />
                  <Point X="3.374658447266" Y="-1.005508728027" />
                  <Point X="3.324530273438" Y="-1.01640435791" />
                  <Point X="3.193094238281" Y="-1.04497253418" />
                  <Point X="3.163974121094" Y="-1.056596923828" />
                  <Point X="3.136147460938" Y="-1.073489257812" />
                  <Point X="3.112397460938" Y="-1.093960083008" />
                  <Point X="3.082098144531" Y="-1.130400756836" />
                  <Point X="3.002653320312" Y="-1.225948364258" />
                  <Point X="2.987932617188" Y="-1.250330566406" />
                  <Point X="2.976589355469" Y="-1.277715698242" />
                  <Point X="2.969757568359" Y="-1.305365722656" />
                  <Point X="2.965415039062" Y="-1.352557861328" />
                  <Point X="2.954028564453" Y="-1.476295898438" />
                  <Point X="2.956347412109" Y="-1.507564819336" />
                  <Point X="2.964079101562" Y="-1.539185791016" />
                  <Point X="2.976450439453" Y="-1.567996582031" />
                  <Point X="3.004192138672" Y="-1.611146972656" />
                  <Point X="3.076930664062" Y="-1.724287109375" />
                  <Point X="3.086932373047" Y="-1.73723815918" />
                  <Point X="3.110628417969" Y="-1.760909179688" />
                  <Point X="3.984000488281" Y="-2.431071289062" />
                  <Point X="4.213123046875" Y="-2.606883056641" />
                  <Point X="4.124812988281" Y="-2.749781494141" />
                  <Point X="4.079094238281" Y="-2.814741455078" />
                  <Point X="4.028981445312" Y="-2.885945068359" />
                  <Point X="3.171693603516" Y="-2.390989501953" />
                  <Point X="2.800954589844" Y="-2.176943115234" />
                  <Point X="2.786131591797" Y="-2.170012451172" />
                  <Point X="2.754224609375" Y="-2.159825195312" />
                  <Point X="2.694563964844" Y="-2.149050537109" />
                  <Point X="2.538134277344" Y="-2.120799560547" />
                  <Point X="2.506781982422" Y="-2.120395263672" />
                  <Point X="2.474609130859" Y="-2.125353027344" />
                  <Point X="2.444833251953" Y="-2.135176757812" />
                  <Point X="2.395270019531" Y="-2.161261474609" />
                  <Point X="2.265315185547" Y="-2.229655761719" />
                  <Point X="2.242384521484" Y="-2.246548828125" />
                  <Point X="2.221424804688" Y="-2.267508544922" />
                  <Point X="2.204531738281" Y="-2.290439208984" />
                  <Point X="2.178447021484" Y="-2.340002441406" />
                  <Point X="2.110052734375" Y="-2.469957275391" />
                  <Point X="2.100229003906" Y="-2.499733154297" />
                  <Point X="2.095271240234" Y="-2.531906005859" />
                  <Point X="2.095675537109" Y="-2.563258300781" />
                  <Point X="2.106450195312" Y="-2.622918701172" />
                  <Point X="2.134701171875" Y="-2.779348632812" />
                  <Point X="2.138985595703" Y="-2.795141845703" />
                  <Point X="2.151819091797" Y="-2.826078613281" />
                  <Point X="2.713048583984" Y="-3.798156005859" />
                  <Point X="2.861283447266" Y="-4.054906738281" />
                  <Point X="2.781855224609" Y="-4.111640136719" />
                  <Point X="2.73073828125" Y="-4.144727539062" />
                  <Point X="2.701765136719" Y="-4.163481445312" />
                  <Point X="2.041453979492" Y="-3.302947753906" />
                  <Point X="1.758546020508" Y="-2.934255126953" />
                  <Point X="1.747503540039" Y="-2.922179443359" />
                  <Point X="1.721924072266" Y="-2.900557373047" />
                  <Point X="1.663082641602" Y="-2.862727783203" />
                  <Point X="1.50880078125" Y="-2.763538574219" />
                  <Point X="1.479989746094" Y="-2.751167236328" />
                  <Point X="1.448368774414" Y="-2.743435546875" />
                  <Point X="1.417099731445" Y="-2.741116699219" />
                  <Point X="1.352746582031" Y="-2.747038574219" />
                  <Point X="1.184012817383" Y="-2.762565673828" />
                  <Point X="1.15636315918" Y="-2.769397460938" />
                  <Point X="1.128978027344" Y="-2.780740722656" />
                  <Point X="1.104595825195" Y="-2.795461181641" />
                  <Point X="1.054904052734" Y="-2.836778320312" />
                  <Point X="0.924612121582" Y="-2.945111572266" />
                  <Point X="0.904141479492" Y="-2.968861572266" />
                  <Point X="0.887249084473" Y="-2.996688232422" />
                  <Point X="0.875624206543" Y="-3.025809082031" />
                  <Point X="0.860766784668" Y="-3.094165771484" />
                  <Point X="0.821809936523" Y="-3.273396972656" />
                  <Point X="0.819724487305" Y="-3.289627441406" />
                  <Point X="0.819742248535" Y="-3.323120117188" />
                  <Point X="0.978790222168" Y="-4.531209960938" />
                  <Point X="1.022065246582" Y="-4.859915039062" />
                  <Point X="0.975687805176" Y="-4.870081054688" />
                  <Point X="0.929315612793" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058432128906" Y="-4.752635742188" />
                  <Point X="-1.133082397461" Y="-4.733428710938" />
                  <Point X="-1.141246459961" Y="-4.731328125" />
                  <Point X="-1.134089111328" Y="-4.676962402344" />
                  <Point X="-1.120775756836" Y="-4.575838378906" />
                  <Point X="-1.120077392578" Y="-4.568099609375" />
                  <Point X="-1.119451660156" Y="-4.541033691406" />
                  <Point X="-1.121759277344" Y="-4.509328125" />
                  <Point X="-1.123334106445" Y="-4.497690429688" />
                  <Point X="-1.140045166016" Y="-4.413678222656" />
                  <Point X="-1.183861572266" Y="-4.193398925781" />
                  <Point X="-1.18812512207" Y="-4.17846875" />
                  <Point X="-1.19902734375" Y="-4.149502441406" />
                  <Point X="-1.205666137695" Y="-4.135466308594" />
                  <Point X="-1.221738037109" Y="-4.10762890625" />
                  <Point X="-1.230573852539" Y="-4.094862304688" />
                  <Point X="-1.250207885742" Y="-4.0709375" />
                  <Point X="-1.261006713867" Y="-4.059779296875" />
                  <Point X="-1.325407836914" Y="-4.003301025391" />
                  <Point X="-1.494267700195" Y="-3.855214599609" />
                  <Point X="-1.506738647461" Y="-3.845965576172" />
                  <Point X="-1.533021606445" Y="-3.829621582031" />
                  <Point X="-1.546833496094" Y="-3.822526855469" />
                  <Point X="-1.576530761719" Y="-3.810225830078" />
                  <Point X="-1.591313354492" Y="-3.805476074219" />
                  <Point X="-1.621455078125" Y="-3.798447998047" />
                  <Point X="-1.636814208984" Y="-3.796169677734" />
                  <Point X="-1.722288818359" Y="-3.790567382812" />
                  <Point X="-1.946403198242" Y="-3.775878173828" />
                  <Point X="-1.961928222656" Y="-3.776132324219" />
                  <Point X="-1.992729614258" Y="-3.779166015625" />
                  <Point X="-2.008006103516" Y="-3.781945556641" />
                  <Point X="-2.03905480957" Y="-3.790265136719" />
                  <Point X="-2.053674560547" Y="-3.795496337891" />
                  <Point X="-2.081865478516" Y="-3.808269775391" />
                  <Point X="-2.095436767578" Y="-3.815812011719" />
                  <Point X="-2.166658935547" Y="-3.863401123047" />
                  <Point X="-2.353403320312" Y="-3.988180175781" />
                  <Point X="-2.359681640625" Y="-3.992757080078" />
                  <Point X="-2.380442382812" Y="-4.010131347656" />
                  <Point X="-2.402759277344" Y="-4.032771728516" />
                  <Point X="-2.410470947266" Y="-4.041629638672" />
                  <Point X="-2.503202392578" Y="-4.162479492188" />
                  <Point X="-2.747582275391" Y="-4.011165527344" />
                  <Point X="-2.850946533203" Y="-3.931578857422" />
                  <Point X="-2.980862792969" Y="-3.831547607422" />
                  <Point X="-2.547020263672" Y="-3.080110595703" />
                  <Point X="-2.341488769531" Y="-2.724119628906" />
                  <Point X="-2.334850097656" Y="-2.710083251953" />
                  <Point X="-2.323947998047" Y="-2.681116943359" />
                  <Point X="-2.319684570312" Y="-2.666187011719" />
                  <Point X="-2.313413574219" Y="-2.634660644531" />
                  <Point X="-2.311638916016" Y="-2.619235351562" />
                  <Point X="-2.310626220703" Y="-2.588301269531" />
                  <Point X="-2.314666259766" Y="-2.557615234375" />
                  <Point X="-2.323650878906" Y="-2.527997558594" />
                  <Point X="-2.329357421875" Y="-2.513557128906" />
                  <Point X="-2.343574462891" Y="-2.484728271484" />
                  <Point X="-2.351557128906" Y="-2.47141015625" />
                  <Point X="-2.369584716797" Y="-2.446253173828" />
                  <Point X="-2.379629638672" Y="-2.434414306641" />
                  <Point X="-2.396978271484" Y="-2.417065673828" />
                  <Point X="-2.408817138672" Y="-2.407020751953" />
                  <Point X="-2.433974121094" Y="-2.388993164062" />
                  <Point X="-2.447292236328" Y="-2.381010498047" />
                  <Point X="-2.47612109375" Y="-2.366793457031" />
                  <Point X="-2.490561523438" Y="-2.361086914062" />
                  <Point X="-2.520179199219" Y="-2.352102294922" />
                  <Point X="-2.550865234375" Y="-2.348062255859" />
                  <Point X="-2.581799316406" Y="-2.349074951172" />
                  <Point X="-2.597224609375" Y="-2.350849609375" />
                  <Point X="-2.628750976562" Y="-2.357120605469" />
                  <Point X="-2.643680908203" Y="-2.361384033203" />
                  <Point X="-2.672647216797" Y="-2.372286132812" />
                  <Point X="-2.68668359375" Y="-2.378924804688" />
                  <Point X="-3.620092041016" Y="-2.917828369141" />
                  <Point X="-3.793089355469" Y="-3.017708496094" />
                  <Point X="-4.004015380859" Y="-2.740594970703" />
                  <Point X="-4.078115234375" Y="-2.616340332031" />
                  <Point X="-4.181265136719" Y="-2.443373779297" />
                  <Point X="-3.407735595703" Y="-1.849822998047" />
                  <Point X="-3.048122314453" Y="-1.573881835938" />
                  <Point X="-3.039157958984" Y="-1.566064819336" />
                  <Point X="-3.016266113281" Y="-1.543428955078" />
                  <Point X="-3.00241015625" Y="-1.526078491211" />
                  <Point X="-2.983071044922" Y="-1.495008544922" />
                  <Point X="-2.974911621094" Y="-1.478533325195" />
                  <Point X="-2.966070800781" Y="-1.455252441406" />
                  <Point X="-2.959763671875" Y="-1.435438476562" />
                  <Point X="-2.954186279297" Y="-1.413904052734" />
                  <Point X="-2.951952880859" Y="-1.402395507812" />
                  <Point X="-2.947838623047" Y="-1.370913208008" />
                  <Point X="-2.94708203125" Y="-1.355699462891" />
                  <Point X="-2.94778125" Y="-1.332831054688" />
                  <Point X="-2.951229248047" Y="-1.310212524414" />
                  <Point X="-2.957375732422" Y="-1.288174560547" />
                  <Point X="-2.961114746094" Y="-1.277341674805" />
                  <Point X="-2.973119628906" Y="-1.248359130859" />
                  <Point X="-2.978796142578" Y="-1.236903076172" />
                  <Point X="-2.991635986328" Y="-1.214856811523" />
                  <Point X="-3.007319824219" Y="-1.194734008789" />
                  <Point X="-3.025563720703" Y="-1.176899780273" />
                  <Point X="-3.035286865234" Y="-1.168598266602" />
                  <Point X="-3.055100830078" Y="-1.153865478516" />
                  <Point X="-3.072097900391" Y="-1.142591308594" />
                  <Point X="-3.091268798828" Y="-1.131308227539" />
                  <Point X="-3.10543359375" Y="-1.124481567383" />
                  <Point X="-3.134696777344" Y="-1.113257446289" />
                  <Point X="-3.149795410156" Y="-1.108860107422" />
                  <Point X="-3.181683105469" Y="-1.102378540039" />
                  <Point X="-3.197298828125" Y="-1.100532470703" />
                  <Point X="-3.228622070312" Y="-1.09944140625" />
                  <Point X="-3.244329101562" Y="-1.100196533203" />
                  <Point X="-4.422668457031" Y="-1.255327758789" />
                  <Point X="-4.660920410156" Y="-1.286694458008" />
                  <Point X="-4.74076171875" Y="-0.974118835449" />
                  <Point X="-4.760366210938" Y="-0.837046630859" />
                  <Point X="-4.786452148438" Y="-0.654654296875" />
                  <Point X="-3.917941894531" Y="-0.421937652588" />
                  <Point X="-3.508288085938" Y="-0.312171295166" />
                  <Point X="-3.497782470703" Y="-0.308688903809" />
                  <Point X="-3.477247802734" Y="-0.300525939941" />
                  <Point X="-3.46721875" Y="-0.295845367432" />
                  <Point X="-3.44398828125" Y="-0.283306793213" />
                  <Point X="-3.425898681641" Y="-0.2721953125" />
                  <Point X="-3.405808105469" Y="-0.258251342773" />
                  <Point X="-3.396477783203" Y="-0.250868515015" />
                  <Point X="-3.372522949219" Y="-0.229342269897" />
                  <Point X="-3.357834228516" Y="-0.21264239502" />
                  <Point X="-3.337007568359" Y="-0.182466003418" />
                  <Point X="-3.328063720703" Y="-0.166363601685" />
                  <Point X="-3.318084228516" Y="-0.143396469116" />
                  <Point X="-3.310883300781" Y="-0.123996269226" />
                  <Point X="-3.304186523438" Y="-0.102418899536" />
                  <Point X="-3.301576416016" Y="-0.091937416077" />
                  <Point X="-3.296133544922" Y="-0.063198352814" />
                  <Point X="-3.294510009766" Y="-0.042914592743" />
                  <Point X="-3.295432373047" Y="-0.009302483559" />
                  <Point X="-3.297392578125" Y="0.007462924004" />
                  <Point X="-3.302193603516" Y="0.030513147354" />
                  <Point X="-3.306740966797" Y="0.048090629578" />
                  <Point X="-3.313437988281" Y="0.069668151855" />
                  <Point X="-3.317669921875" Y="0.080789009094" />
                  <Point X="-3.330986816406" Y="0.110113349915" />
                  <Point X="-3.342576904297" Y="0.129259552002" />
                  <Point X="-3.365298095703" Y="0.158389938354" />
                  <Point X="-3.378088623047" Y="0.171840164185" />
                  <Point X="-3.397569335938" Y="0.188675720215" />
                  <Point X="-3.413470947266" Y="0.201009628296" />
                  <Point X="-3.433561523438" Y="0.214953582764" />
                  <Point X="-3.440483886719" Y="0.219328445435" />
                  <Point X="-3.465616210938" Y="0.232834701538" />
                  <Point X="-3.496566894531" Y="0.245635894775" />
                  <Point X="-3.508288085938" Y="0.249611251831" />
                  <Point X="-4.582400390625" Y="0.537418762207" />
                  <Point X="-4.785446289062" Y="0.591824645996" />
                  <Point X="-4.731331542969" Y="0.957527770996" />
                  <Point X="-4.691866210938" Y="1.103166259766" />
                  <Point X="-4.633586425781" Y="1.318237060547" />
                  <Point X="-4.071716064453" Y="1.24426550293" />
                  <Point X="-3.778066162109" Y="1.20560546875" />
                  <Point X="-3.76773828125" Y="1.204815429688" />
                  <Point X="-3.747058105469" Y="1.204364257812" />
                  <Point X="-3.736705810547" Y="1.20470324707" />
                  <Point X="-3.715144287109" Y="1.20658972168" />
                  <Point X="-3.704890136719" Y="1.208053588867" />
                  <Point X="-3.684603271484" Y="1.212089233398" />
                  <Point X="-3.657612060547" Y="1.220007324219" />
                  <Point X="-3.613145019531" Y="1.234028076172" />
                  <Point X="-3.603451171875" Y="1.237676513672" />
                  <Point X="-3.584518554688" Y="1.246007080078" />
                  <Point X="-3.575279541016" Y="1.250688964844" />
                  <Point X="-3.55653515625" Y="1.261510864258" />
                  <Point X="-3.547860351562" Y="1.267171264648" />
                  <Point X="-3.531179199219" Y="1.27940234375" />
                  <Point X="-3.515928710938" Y="1.293376831055" />
                  <Point X="-3.502290283203" Y="1.308928466797" />
                  <Point X="-3.495895751953" Y="1.317076416016" />
                  <Point X="-3.483481201172" Y="1.334806152344" />
                  <Point X="-3.478011474609" Y="1.343602050781" />
                  <Point X="-3.468062255859" Y="1.361737060547" />
                  <Point X="-3.456777832031" Y="1.387504760742" />
                  <Point X="-3.438935302734" Y="1.430580444336" />
                  <Point X="-3.435499267578" Y="1.44035144043" />
                  <Point X="-3.4297109375" Y="1.460210205078" />
                  <Point X="-3.427358642578" Y="1.470297973633" />
                  <Point X="-3.423600341797" Y="1.491613037109" />
                  <Point X="-3.422360595703" Y="1.501896972656" />
                  <Point X="-3.421008056641" Y="1.522537231445" />
                  <Point X="-3.421910400391" Y="1.543200561523" />
                  <Point X="-3.425056884766" Y="1.563644165039" />
                  <Point X="-3.427188232422" Y="1.573780639648" />
                  <Point X="-3.432790039062" Y="1.594686889648" />
                  <Point X="-3.436012207031" Y="1.604530517578" />
                  <Point X="-3.443509277344" Y="1.623809326172" />
                  <Point X="-3.455995117188" Y="1.649017211914" />
                  <Point X="-3.477523925781" Y="1.690373901367" />
                  <Point X="-3.482799804688" Y="1.699286376953" />
                  <Point X="-3.494291259766" Y="1.716484741211" />
                  <Point X="-3.500506835938" Y="1.724770629883" />
                  <Point X="-3.514419433594" Y="1.741351074219" />
                  <Point X="-3.521500488281" Y="1.748911254883" />
                  <Point X="-3.536442626953" Y="1.763215087891" />
                  <Point X="-3.544303710938" Y="1.769958862305" />
                  <Point X="-4.160416503906" Y="2.242719482422" />
                  <Point X="-4.227614257812" Y="2.294282470703" />
                  <Point X="-4.002292236328" Y="2.680313476562" />
                  <Point X="-3.8977578125" Y="2.814677490234" />
                  <Point X="-3.726338623047" Y="3.035012695312" />
                  <Point X="-3.434508544922" Y="2.866524414062" />
                  <Point X="-3.254156982422" Y="2.7623984375" />
                  <Point X="-3.244918457031" Y="2.757716552734" />
                  <Point X="-3.225986083984" Y="2.749385986328" />
                  <Point X="-3.216292236328" Y="2.745737304688" />
                  <Point X="-3.195650146484" Y="2.739228759766" />
                  <Point X="-3.185615722656" Y="2.736656982422" />
                  <Point X="-3.165327880859" Y="2.732621582031" />
                  <Point X="-3.155074462891" Y="2.731157958984" />
                  <Point X="-3.131455078125" Y="2.729091552734" />
                  <Point X="-3.069525146484" Y="2.723673339844" />
                  <Point X="-3.059173339844" Y="2.723334472656" />
                  <Point X="-3.038493164062" Y="2.723785644531" />
                  <Point X="-3.028164794922" Y="2.724575683594" />
                  <Point X="-3.006706054688" Y="2.727400878906" />
                  <Point X="-2.996525878906" Y="2.729310791016" />
                  <Point X="-2.976434082031" Y="2.734227294922" />
                  <Point X="-2.956996826172" Y="2.741302001953" />
                  <Point X="-2.9384453125" Y="2.750450683594" />
                  <Point X="-2.929419433594" Y="2.75553125" />
                  <Point X="-2.911165039062" Y="2.767160644531" />
                  <Point X="-2.902745605469" Y="2.773194091797" />
                  <Point X="-2.886614746094" Y="2.786140625" />
                  <Point X="-2.878903320312" Y="2.793053710938" />
                  <Point X="-2.862137939453" Y="2.809818847656" />
                  <Point X="-2.8181796875" Y="2.853777099609" />
                  <Point X="-2.811266357422" Y="2.861488525391" />
                  <Point X="-2.798319091797" Y="2.877619873047" />
                  <Point X="-2.79228515625" Y="2.886039794922" />
                  <Point X="-2.780655761719" Y="2.904293945312" />
                  <Point X="-2.775574707031" Y="2.913320800781" />
                  <Point X="-2.766426025391" Y="2.931872558594" />
                  <Point X="-2.7593515625" Y="2.95130859375" />
                  <Point X="-2.754434814453" Y="2.971400634766" />
                  <Point X="-2.752524902344" Y="2.981581542969" />
                  <Point X="-2.749699707031" Y="3.003040527344" />
                  <Point X="-2.748909667969" Y="3.013368896484" />
                  <Point X="-2.748458496094" Y="3.034049072266" />
                  <Point X="-2.748797363281" Y="3.044400878906" />
                  <Point X="-2.750863769531" Y="3.068020263672" />
                  <Point X="-2.756281982422" Y="3.129950439453" />
                  <Point X="-2.757745605469" Y="3.140203857422" />
                  <Point X="-2.761781005859" Y="3.160491699219" />
                  <Point X="-2.764352783203" Y="3.170526123047" />
                  <Point X="-2.770861328125" Y="3.191168212891" />
                  <Point X="-2.774510009766" Y="3.200862060547" />
                  <Point X="-2.782840576172" Y="3.219794433594" />
                  <Point X="-2.787522460938" Y="3.229032958984" />
                  <Point X="-3.059387207031" Y="3.699916503906" />
                  <Point X="-2.648375732422" Y="4.015035400391" />
                  <Point X="-2.483739501953" Y="4.106502929688" />
                  <Point X="-2.192524658203" Y="4.268295898437" />
                  <Point X="-2.173775390625" Y="4.243861328125" />
                  <Point X="-2.118563964844" Y="4.171908691406" />
                  <Point X="-2.111819335938" Y="4.164046386719" />
                  <Point X="-2.097515625" Y="4.149104980469" />
                  <Point X="-2.089956542969" Y="4.142024902344" />
                  <Point X="-2.073376220703" Y="4.128112304688" />
                  <Point X="-2.065092285156" Y="4.1218984375" />
                  <Point X="-2.04789453125" Y="4.110406738281" />
                  <Point X="-2.03898046875" Y="4.105129394531" />
                  <Point X="-2.012692138672" Y="4.091444335938" />
                  <Point X="-1.943764404297" Y="4.0555625" />
                  <Point X="-1.93432824707" Y="4.051287109375" />
                  <Point X="-1.915047729492" Y="4.043789550781" />
                  <Point X="-1.905203491211" Y="4.040567138672" />
                  <Point X="-1.884296875" Y="4.034965576172" />
                  <Point X="-1.874162109375" Y="4.032834716797" />
                  <Point X="-1.853718994141" Y="4.029688232422" />
                  <Point X="-1.833052856445" Y="4.028785888672" />
                  <Point X="-1.812413574219" Y="4.030138916016" />
                  <Point X="-1.802132080078" Y="4.031378662109" />
                  <Point X="-1.780816772461" Y="4.035136962891" />
                  <Point X="-1.770729248047" Y="4.037489013672" />
                  <Point X="-1.750869506836" Y="4.043277587891" />
                  <Point X="-1.74109765625" Y="4.046714111328" />
                  <Point X="-1.713716674805" Y="4.058055908203" />
                  <Point X="-1.641923706055" Y="4.087793701172" />
                  <Point X="-1.632586303711" Y="4.092272460938" />
                  <Point X="-1.614451660156" Y="4.102221191406" />
                  <Point X="-1.605654418945" Y="4.10769140625" />
                  <Point X="-1.587924804688" Y="4.120105957031" />
                  <Point X="-1.579776245117" Y="4.126501464844" />
                  <Point X="-1.564224853516" Y="4.140140136719" />
                  <Point X="-1.550250976562" Y="4.155390136719" />
                  <Point X="-1.538020019531" Y="4.172071289062" />
                  <Point X="-1.532359863281" Y="4.180745605469" />
                  <Point X="-1.521538085938" Y="4.199489746094" />
                  <Point X="-1.516856201172" Y="4.208728515625" />
                  <Point X="-1.508525878906" Y="4.227660644531" />
                  <Point X="-1.504877441406" Y="4.237354003906" />
                  <Point X="-1.495965209961" Y="4.265619140625" />
                  <Point X="-1.472598144531" Y="4.33973046875" />
                  <Point X="-1.470026733398" Y="4.349763671875" />
                  <Point X="-1.465991088867" Y="4.37005078125" />
                  <Point X="-1.46452722168" Y="4.3803046875" />
                  <Point X="-1.46264074707" Y="4.401866210938" />
                  <Point X="-1.462301757812" Y="4.41221875" />
                  <Point X="-1.462752929688" Y="4.432898925781" />
                  <Point X="-1.46354309082" Y="4.4432265625" />
                  <Point X="-1.479266235352" Y="4.562655273438" />
                  <Point X="-1.467937988281" Y="4.565831542969" />
                  <Point X="-0.931178527832" Y="4.716319824219" />
                  <Point X="-0.731599731445" Y="4.739677734375" />
                  <Point X="-0.365221923828" Y="4.782556640625" />
                  <Point X="-0.279019195557" Y="4.46084375" />
                  <Point X="-0.225666244507" Y="4.261727539062" />
                  <Point X="-0.220435317993" Y="4.247107910156" />
                  <Point X="-0.207661773682" Y="4.218916503906" />
                  <Point X="-0.200119155884" Y="4.205344726562" />
                  <Point X="-0.182260925293" Y="4.178618164062" />
                  <Point X="-0.172608306885" Y="4.166456054688" />
                  <Point X="-0.151451248169" Y="4.1438671875" />
                  <Point X="-0.126898002625" Y="4.125026367188" />
                  <Point X="-0.099602806091" Y="4.110436523438" />
                  <Point X="-0.085356712341" Y="4.104260742188" />
                  <Point X="-0.054918586731" Y="4.093927978516" />
                  <Point X="-0.039857292175" Y="4.090155273438" />
                  <Point X="-0.009319890976" Y="4.08511328125" />
                  <Point X="0.021631721497" Y="4.08511328125" />
                  <Point X="0.052168972015" Y="4.090155273438" />
                  <Point X="0.067230415344" Y="4.093927978516" />
                  <Point X="0.097668540955" Y="4.104260742188" />
                  <Point X="0.111914489746" Y="4.110436523438" />
                  <Point X="0.139209686279" Y="4.125026367188" />
                  <Point X="0.16376322937" Y="4.143866699219" />
                  <Point X="0.184920150757" Y="4.166455566406" />
                  <Point X="0.194572906494" Y="4.178617675781" />
                  <Point X="0.212431137085" Y="4.205344238281" />
                  <Point X="0.219973754883" Y="4.218916503906" />
                  <Point X="0.232747146606" Y="4.247107910156" />
                  <Point X="0.237978225708" Y="4.261727539062" />
                  <Point X="0.377869842529" Y="4.783810058594" />
                  <Point X="0.378190338135" Y="4.785006347656" />
                  <Point X="0.827876403809" Y="4.737912109375" />
                  <Point X="0.993002075195" Y="4.698045410156" />
                  <Point X="1.453596679688" Y="4.58684375" />
                  <Point X="1.559032104492" Y="4.5486015625" />
                  <Point X="1.858256713867" Y="4.4400703125" />
                  <Point X="1.962191040039" Y="4.391463867188" />
                  <Point X="2.250452880859" Y="4.256653320312" />
                  <Point X="2.350913574219" Y="4.198124511719" />
                  <Point X="2.629433837891" Y="4.035858154297" />
                  <Point X="2.724133300781" Y="3.968512939453" />
                  <Point X="2.817779785156" Y="3.901916748047" />
                  <Point X="2.303195800781" Y="3.010630859375" />
                  <Point X="2.06530859375" Y="2.598597900391" />
                  <Point X="2.062373046875" Y="2.593104980469" />
                  <Point X="2.053181396484" Y="2.573438964844" />
                  <Point X="2.044182373047" Y="2.549564453125" />
                  <Point X="2.041301391602" Y="2.540598388672" />
                  <Point X="2.035373901367" Y="2.518432128906" />
                  <Point X="2.019831665039" Y="2.460312011719" />
                  <Point X="2.017265991211" Y="2.446941162109" />
                  <Point X="2.014072387695" Y="2.419970703125" />
                  <Point X="2.013444335937" Y="2.40637109375" />
                  <Point X="2.01441796875" Y="2.368301025391" />
                  <Point X="2.015722167969" Y="2.350412841797" />
                  <Point X="2.021782348633" Y="2.300155517578" />
                  <Point X="2.02380065918" Y="2.289033935547" />
                  <Point X="2.029143676758" Y="2.267110595703" />
                  <Point X="2.032468261719" Y="2.256308837891" />
                  <Point X="2.040734375" Y="2.234220214844" />
                  <Point X="2.045318359375" Y="2.223889160156" />
                  <Point X="2.055681152344" Y="2.203843994141" />
                  <Point X="2.061459960938" Y="2.194129882813" />
                  <Point X="2.0733203125" Y="2.176650878906" />
                  <Point X="2.104417724609" Y="2.130821289063" />
                  <Point X="2.10980859375" Y="2.123633300781" />
                  <Point X="2.130463378906" Y="2.100123535156" />
                  <Point X="2.157596923828" Y="2.075387695312" />
                  <Point X="2.168256591797" Y="2.066982421875" />
                  <Point X="2.185735351563" Y="2.055122070313" />
                  <Point X="2.231565185547" Y="2.024024780273" />
                  <Point X="2.241278076172" Y="2.018246337891" />
                  <Point X="2.261323730469" Y="2.0078828125" />
                  <Point X="2.271656494141" Y="2.003297973633" />
                  <Point X="2.293744628906" Y="1.995032104492" />
                  <Point X="2.304547119141" Y="1.991707519531" />
                  <Point X="2.326470214844" Y="1.986364746094" />
                  <Point X="2.356758300781" Y="1.98203527832" />
                  <Point X="2.407015625" Y="1.975974975586" />
                  <Point X="2.416045410156" Y="1.975320800781" />
                  <Point X="2.447575439453" Y="1.975497314453" />
                  <Point X="2.484314941406" Y="1.979822631836" />
                  <Point X="2.497749755859" Y="1.982396118164" />
                  <Point X="2.519916015625" Y="1.988323852539" />
                  <Point X="2.578036132812" Y="2.003865844727" />
                  <Point X="2.583996826172" Y="2.005671264648" />
                  <Point X="2.604404541016" Y="2.013067260742" />
                  <Point X="2.627655029297" Y="2.02357409668" />
                  <Point X="2.636033935547" Y="2.027872558594" />
                  <Point X="3.716384277344" Y="2.651613037109" />
                  <Point X="3.940404785156" Y="2.780951416016" />
                  <Point X="4.043952636719" Y="2.637043701172" />
                  <Point X="4.096747070313" Y="2.549800292969" />
                  <Point X="4.136884765625" Y="2.483472412109" />
                  <Point X="3.486152587891" Y="1.984147338867" />
                  <Point X="3.172951660156" Y="1.743819702148" />
                  <Point X="3.168137939453" Y="1.739868896484" />
                  <Point X="3.152119384766" Y="1.725216918945" />
                  <Point X="3.134668701172" Y="1.706603393555" />
                  <Point X="3.128576416016" Y="1.699422485352" />
                  <Point X="3.112623291016" Y="1.678610473633" />
                  <Point X="3.070794189453" Y="1.624041015625" />
                  <Point X="3.065635742188" Y="1.616602661133" />
                  <Point X="3.04973828125" Y="1.589370117188" />
                  <Point X="3.034762939453" Y="1.555545288086" />
                  <Point X="3.030140380859" Y="1.542672485352" />
                  <Point X="3.024197753906" Y="1.521423095703" />
                  <Point X="3.008616455078" Y="1.465708007812" />
                  <Point X="3.006225585938" Y="1.454662109375" />
                  <Point X="3.002771972656" Y="1.432363525391" />
                  <Point X="3.001709472656" Y="1.421110839844" />
                  <Point X="3.000893310547" Y="1.397540527344" />
                  <Point X="3.001174804688" Y="1.386241943359" />
                  <Point X="3.003077636719" Y="1.363757446289" />
                  <Point X="3.004698974609" Y="1.352571289063" />
                  <Point X="3.009577148438" Y="1.328928833008" />
                  <Point X="3.022367919922" Y="1.266938476562" />
                  <Point X="3.024597900391" Y="1.258234130859" />
                  <Point X="3.034683837891" Y="1.228608520508" />
                  <Point X="3.050286376953" Y="1.195371337891" />
                  <Point X="3.056918457031" Y="1.183526000977" />
                  <Point X="3.070186767578" Y="1.163358520508" />
                  <Point X="3.104976074219" Y="1.11048059082" />
                  <Point X="3.111739257813" Y="1.101424194336" />
                  <Point X="3.126292724609" Y="1.08417956543" />
                  <Point X="3.134083007812" Y="1.075991210938" />
                  <Point X="3.151326904297" Y="1.059901367188" />
                  <Point X="3.160034423828" Y="1.052696044922" />
                  <Point X="3.178243896484" Y="1.039370361328" />
                  <Point X="3.187745849609" Y="1.03325012207" />
                  <Point X="3.206973388672" Y="1.022426452637" />
                  <Point X="3.257387939453" Y="0.994047485352" />
                  <Point X="3.265479003906" Y="0.989987854004" />
                  <Point X="3.294678955078" Y="0.97808392334" />
                  <Point X="3.330275390625" Y="0.968021118164" />
                  <Point X="3.343670654297" Y="0.965257629395" />
                  <Point X="3.369667480469" Y="0.961821716309" />
                  <Point X="3.437831298828" Y="0.952813110352" />
                  <Point X="3.444030029297" Y="0.952199768066" />
                  <Point X="3.465716308594" Y="0.951222839355" />
                  <Point X="3.491217529297" Y="0.952032409668" />
                  <Point X="3.500603515625" Y="0.952797241211" />
                  <Point X="4.526863769531" Y="1.087906982422" />
                  <Point X="4.704703613281" Y="1.111320068359" />
                  <Point X="4.75268359375" Y="0.914233581543" />
                  <Point X="4.769320800781" Y="0.807375366211" />
                  <Point X="4.78387109375" Y="0.713920837402" />
                  <Point X="4.050926757813" Y="0.517529174805" />
                  <Point X="3.691991943359" Y="0.421352905273" />
                  <Point X="3.686031738281" Y="0.419544464111" />
                  <Point X="3.665626953125" Y="0.412138122559" />
                  <Point X="3.642380859375" Y="0.401619354248" />
                  <Point X="3.634003417969" Y="0.397316131592" />
                  <Point X="3.608462402344" Y="0.38255267334" />
                  <Point X="3.541493408203" Y="0.343843536377" />
                  <Point X="3.533880859375" Y="0.338945068359" />
                  <Point X="3.508774169922" Y="0.319870666504" />
                  <Point X="3.481993896484" Y="0.294351074219" />
                  <Point X="3.472796630859" Y="0.2842265625" />
                  <Point X="3.457471923828" Y="0.264699157715" />
                  <Point X="3.417290771484" Y="0.213498855591" />
                  <Point X="3.410854248047" Y="0.204208145142" />
                  <Point X="3.399130371094" Y="0.184928344727" />
                  <Point X="3.393843017578" Y="0.17493939209" />
                  <Point X="3.384069091797" Y="0.153475570679" />
                  <Point X="3.380005371094" Y="0.142928985596" />
                  <Point X="3.373159179688" Y="0.121428161621" />
                  <Point X="3.370376708984" Y="0.110473907471" />
                  <Point X="3.365268310547" Y="0.083800552368" />
                  <Point X="3.351874511719" Y="0.013863369942" />
                  <Point X="3.350603515625" Y="0.004969180107" />
                  <Point X="3.348584228516" Y="-0.02626288414" />
                  <Point X="3.350280029297" Y="-0.062941680908" />
                  <Point X="3.351874511719" Y="-0.076423561096" />
                  <Point X="3.356982910156" Y="-0.103096763611" />
                  <Point X="3.370376708984" Y="-0.173033950806" />
                  <Point X="3.373158935547" Y="-0.18398789978" />
                  <Point X="3.380005126953" Y="-0.205488876343" />
                  <Point X="3.384069091797" Y="-0.216035766602" />
                  <Point X="3.393843017578" Y="-0.237499588013" />
                  <Point X="3.399130615234" Y="-0.247488830566" />
                  <Point X="3.410854736328" Y="-0.266768920898" />
                  <Point X="3.417291259766" Y="-0.276059356689" />
                  <Point X="3.432615966797" Y="-0.295586608887" />
                  <Point X="3.472797119141" Y="-0.34678704834" />
                  <Point X="3.478718505859" Y="-0.353633666992" />
                  <Point X="3.501139648438" Y="-0.375805389404" />
                  <Point X="3.530176513672" Y="-0.398724975586" />
                  <Point X="3.541494873047" Y="-0.406404327393" />
                  <Point X="3.567036132812" Y="-0.421167633057" />
                  <Point X="3.634004882812" Y="-0.45987689209" />
                  <Point X="3.639497070313" Y="-0.462815765381" />
                  <Point X="3.659158447266" Y="-0.472016876221" />
                  <Point X="3.683028076172" Y="-0.481027740479" />
                  <Point X="3.691991943359" Y="-0.48391293335" />
                  <Point X="4.633119628906" Y="-0.736087219238" />
                  <Point X="4.784876953125" Y="-0.776750427246" />
                  <Point X="4.76161328125" Y="-0.931053649902" />
                  <Point X="4.740298339844" Y="-1.024459350586" />
                  <Point X="4.727802246094" Y="-1.079219604492" />
                  <Point X="3.853045654297" Y="-0.964055725098" />
                  <Point X="3.436781982422" Y="-0.909253723145" />
                  <Point X="3.428624511719" Y="-0.908535888672" />
                  <Point X="3.400098632812" Y="-0.908042480469" />
                  <Point X="3.366721435547" Y="-0.910840881348" />
                  <Point X="3.354480712891" Y="-0.912676208496" />
                  <Point X="3.304352539062" Y="-0.923571899414" />
                  <Point X="3.172916503906" Y="-0.952140075684" />
                  <Point X="3.157874023438" Y="-0.956742614746" />
                  <Point X="3.12875390625" Y="-0.968367004395" />
                  <Point X="3.114676269531" Y="-0.975388977051" />
                  <Point X="3.086849609375" Y="-0.992281311035" />
                  <Point X="3.074124023438" Y="-1.001530273438" />
                  <Point X="3.050374023438" Y="-1.022001159668" />
                  <Point X="3.039349609375" Y="-1.033222900391" />
                  <Point X="3.009050292969" Y="-1.069663574219" />
                  <Point X="2.92960546875" Y="-1.165211181641" />
                  <Point X="2.921326171875" Y="-1.176847290039" />
                  <Point X="2.90660546875" Y="-1.201229492188" />
                  <Point X="2.9001640625" Y="-1.213975708008" />
                  <Point X="2.888820800781" Y="-1.241360839844" />
                  <Point X="2.884362792969" Y="-1.254928344727" />
                  <Point X="2.877531005859" Y="-1.282578369141" />
                  <Point X="2.875157226562" Y="-1.296660766602" />
                  <Point X="2.870814697266" Y="-1.343853027344" />
                  <Point X="2.859428222656" Y="-1.467590942383" />
                  <Point X="2.859288818359" Y="-1.483321655273" />
                  <Point X="2.861607666016" Y="-1.514590576172" />
                  <Point X="2.864065917969" Y="-1.530128662109" />
                  <Point X="2.871797607422" Y="-1.561749633789" />
                  <Point X="2.876786621094" Y="-1.576669067383" />
                  <Point X="2.889157958984" Y="-1.605479980469" />
                  <Point X="2.896540283203" Y="-1.619371337891" />
                  <Point X="2.924281982422" Y="-1.662521728516" />
                  <Point X="2.997020507812" Y="-1.775661865234" />
                  <Point X="3.001741943359" Y="-1.782353149414" />
                  <Point X="3.019792724609" Y="-1.804448730469" />
                  <Point X="3.043488769531" Y="-1.828119750977" />
                  <Point X="3.052796142578" Y="-1.836277709961" />
                  <Point X="3.926168212891" Y="-2.506439941406" />
                  <Point X="4.087171142578" Y="-2.629981933594" />
                  <Point X="4.045493164062" Y="-2.697422851562" />
                  <Point X="4.001406005859" Y="-2.760064697266" />
                  <Point X="4.001274414063" Y="-2.760251953125" />
                  <Point X="3.219193603516" Y="-2.308717041016" />
                  <Point X="2.848454589844" Y="-2.094670654297" />
                  <Point X="2.841191894531" Y="-2.090885253906" />
                  <Point X="2.815026123047" Y="-2.079513183594" />
                  <Point X="2.783119140625" Y="-2.069325927734" />
                  <Point X="2.771108398438" Y="-2.066337646484" />
                  <Point X="2.711447753906" Y="-2.055562988281" />
                  <Point X="2.555018066406" Y="-2.027311889648" />
                  <Point X="2.539359130859" Y="-2.025807495117" />
                  <Point X="2.508006835938" Y="-2.025403198242" />
                  <Point X="2.492313476562" Y="-2.026503540039" />
                  <Point X="2.460140625" Y="-2.031461303711" />
                  <Point X="2.444844482422" Y="-2.035136108398" />
                  <Point X="2.415068603516" Y="-2.044959960938" />
                  <Point X="2.400588867188" Y="-2.051108642578" />
                  <Point X="2.351025634766" Y="-2.077193359375" />
                  <Point X="2.221070800781" Y="-2.145587646484" />
                  <Point X="2.208968261719" Y="-2.153170410156" />
                  <Point X="2.186037597656" Y="-2.170063476562" />
                  <Point X="2.175209472656" Y="-2.179373779297" />
                  <Point X="2.154249755859" Y="-2.200333496094" />
                  <Point X="2.144939453125" Y="-2.211161621094" />
                  <Point X="2.128046386719" Y="-2.234092285156" />
                  <Point X="2.120463623047" Y="-2.246194824219" />
                  <Point X="2.09437890625" Y="-2.295758056641" />
                  <Point X="2.025984619141" Y="-2.425712890625" />
                  <Point X="2.0198359375" Y="-2.440192626953" />
                  <Point X="2.010012084961" Y="-2.469968505859" />
                  <Point X="2.006337280273" Y="-2.485264648438" />
                  <Point X="2.001379516602" Y="-2.5174375" />
                  <Point X="2.000279174805" Y="-2.533130859375" />
                  <Point X="2.00068347168" Y="-2.564483154297" />
                  <Point X="2.002187866211" Y="-2.580142089844" />
                  <Point X="2.012962646484" Y="-2.639802490234" />
                  <Point X="2.041213378906" Y="-2.796232421875" />
                  <Point X="2.043015014648" Y="-2.804221435547" />
                  <Point X="2.051236083984" Y="-2.83154296875" />
                  <Point X="2.064069580078" Y="-2.862479736328" />
                  <Point X="2.069546630859" Y="-2.873578613281" />
                  <Point X="2.630776123047" Y="-3.845656005859" />
                  <Point X="2.735893066406" Y="-4.027724365234" />
                  <Point X="2.723754150391" Y="-4.036083251953" />
                  <Point X="2.116822509766" Y="-3.245115478516" />
                  <Point X="1.833914550781" Y="-2.876422851562" />
                  <Point X="1.828653442383" Y="-2.870146240234" />
                  <Point X="1.808831542969" Y="-2.849626953125" />
                  <Point X="1.783252075195" Y="-2.828004882812" />
                  <Point X="1.773298828125" Y="-2.820647216797" />
                  <Point X="1.714457397461" Y="-2.782817626953" />
                  <Point X="1.560175415039" Y="-2.683628417969" />
                  <Point X="1.546283935547" Y="-2.676245849609" />
                  <Point X="1.517472900391" Y="-2.663874511719" />
                  <Point X="1.502553588867" Y="-2.658885742188" />
                  <Point X="1.470932617188" Y="-2.651154052734" />
                  <Point X="1.45539453125" Y="-2.648695800781" />
                  <Point X="1.424125488281" Y="-2.646376953125" />
                  <Point X="1.40839453125" Y="-2.646516357422" />
                  <Point X="1.344041259766" Y="-2.652438232422" />
                  <Point X="1.175307495117" Y="-2.667965332031" />
                  <Point X="1.161225097656" Y="-2.670339111328" />
                  <Point X="1.133575439453" Y="-2.677170898438" />
                  <Point X="1.120008422852" Y="-2.68162890625" />
                  <Point X="1.092623168945" Y="-2.692972167969" />
                  <Point X="1.079877563477" Y="-2.699413330078" />
                  <Point X="1.055495361328" Y="-2.714133789062" />
                  <Point X="1.043858764648" Y="-2.722413085938" />
                  <Point X="0.994166992188" Y="-2.763730224609" />
                  <Point X="0.863875061035" Y="-2.872063476562" />
                  <Point X="0.852653015137" Y="-2.883088378906" />
                  <Point X="0.832182250977" Y="-2.906838378906" />
                  <Point X="0.82293359375" Y="-2.919563720703" />
                  <Point X="0.806041137695" Y="-2.947390380859" />
                  <Point X="0.799019287109" Y="-2.961467285156" />
                  <Point X="0.787394470215" Y="-2.990588134766" />
                  <Point X="0.782791748047" Y="-3.005631835938" />
                  <Point X="0.767934204102" Y="-3.073988525391" />
                  <Point X="0.728977478027" Y="-3.253219726562" />
                  <Point X="0.727584594727" Y="-3.261290039062" />
                  <Point X="0.724724487305" Y="-3.289677734375" />
                  <Point X="0.7247421875" Y="-3.323170410156" />
                  <Point X="0.725554992676" Y="-3.335520019531" />
                  <Point X="0.833091308594" Y="-4.15233984375" />
                  <Point X="0.765661621094" Y="-3.900688964844" />
                  <Point X="0.655064941406" Y="-3.487936523438" />
                  <Point X="0.652606262207" Y="-3.480124511719" />
                  <Point X="0.642145141602" Y="-3.453580322266" />
                  <Point X="0.62678692627" Y="-3.423815673828" />
                  <Point X="0.620407409668" Y="-3.413210205078" />
                  <Point X="0.575203735352" Y="-3.348080322266" />
                  <Point X="0.456679962158" Y="-3.177309814453" />
                  <Point X="0.446670654297" Y="-3.165172851562" />
                  <Point X="0.424786804199" Y="-3.142717529297" />
                  <Point X="0.41291229248" Y="-3.132399169922" />
                  <Point X="0.386657165527" Y="-3.113155273438" />
                  <Point X="0.373242767334" Y="-3.104937988281" />
                  <Point X="0.345241607666" Y="-3.090829589844" />
                  <Point X="0.330654724121" Y="-3.084938476562" />
                  <Point X="0.260704589844" Y="-3.063228515625" />
                  <Point X="0.077295753479" Y="-3.006305175781" />
                  <Point X="0.063376476288" Y="-3.003109375" />
                  <Point X="0.035217193604" Y="-2.998840087891" />
                  <Point X="0.020976739883" Y="-2.997766601562" />
                  <Point X="-0.008664910316" Y="-2.997766601562" />
                  <Point X="-0.02290521431" Y="-2.998840087891" />
                  <Point X="-0.051064498901" Y="-3.003109375" />
                  <Point X="-0.064983329773" Y="-3.006305175781" />
                  <Point X="-0.134933303833" Y="-3.028014892578" />
                  <Point X="-0.318342285156" Y="-3.084938476562" />
                  <Point X="-0.332929046631" Y="-3.090829345703" />
                  <Point X="-0.360930633545" Y="-3.104937744141" />
                  <Point X="-0.374345336914" Y="-3.113155273438" />
                  <Point X="-0.400600463867" Y="-3.132399169922" />
                  <Point X="-0.412474975586" Y="-3.142717529297" />
                  <Point X="-0.434358673096" Y="-3.165172607422" />
                  <Point X="-0.444367828369" Y="-3.177309326172" />
                  <Point X="-0.489571350098" Y="-3.242438964844" />
                  <Point X="-0.608095458984" Y="-3.413209716797" />
                  <Point X="-0.612470275879" Y="-3.420132324219" />
                  <Point X="-0.62597668457" Y="-3.445265136719" />
                  <Point X="-0.638777893066" Y="-3.476215820312" />
                  <Point X="-0.642753112793" Y="-3.487936523438" />
                  <Point X="-0.932737487793" Y="-4.570173828125" />
                  <Point X="-0.985425231934" Y="-4.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.083980411789" Y="2.442877415799" />
                  <Point X="3.857323849346" Y="2.732984586279" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.008609014791" Y="2.385042826372" />
                  <Point X="3.774242913536" Y="2.685017756543" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.933237617794" Y="2.327208236945" />
                  <Point X="3.69116197194" Y="2.637050934213" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.768979010169" Y="3.817391299484" />
                  <Point X="2.572277818345" Y="4.069157344001" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.857866220796" Y="2.269373647518" />
                  <Point X="3.608081017071" Y="2.589084128871" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.717748619245" Y="3.72865763134" />
                  <Point X="2.351000426968" Y="4.198073911166" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.733049370714" Y="0.994884719823" />
                  <Point X="4.647920701394" Y="1.10384444778" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.782494823798" Y="2.211539058091" />
                  <Point X="3.525000062202" Y="2.54111732353" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.666518228321" Y="3.639923963196" />
                  <Point X="2.14680567508" Y="4.305125696862" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.771891623637" Y="0.790863324899" />
                  <Point X="4.538607720504" Y="1.089453104638" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.707123426801" Y="2.153704468664" />
                  <Point X="3.441919107333" Y="2.493150518188" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.615287837397" Y="3.551190295052" />
                  <Point X="1.956838440809" Y="4.393967090438" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.723985650531" Y="0.697874595986" />
                  <Point X="4.42929473043" Y="1.075061773253" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.631752029803" Y="2.095869879237" />
                  <Point X="3.358838152464" Y="2.445183712847" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.564057446473" Y="3.462456626909" />
                  <Point X="1.777328621245" Y="4.469423603564" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.624297997894" Y="0.671163394491" />
                  <Point X="4.31998173925" Y="1.060670443283" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.556380632805" Y="2.03803528981" />
                  <Point X="3.275757197595" Y="2.397216907505" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.512827055549" Y="3.373722958765" />
                  <Point X="1.609099258824" Y="4.530441789964" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.524610345257" Y="0.644452192996" />
                  <Point X="4.21066874807" Y="1.046279113313" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.481009233962" Y="1.980200702746" />
                  <Point X="3.192676242726" Y="2.349250102164" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.461596664625" Y="3.284989290621" />
                  <Point X="1.442356246279" Y="4.589557535276" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.42492269262" Y="0.617740991501" />
                  <Point X="4.101355756891" Y="1.031887783342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.40563780991" Y="1.922366147946" />
                  <Point X="3.109595287857" Y="2.301283296822" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.410366273701" Y="3.196255622477" />
                  <Point X="1.293772762126" Y="4.625430144179" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.325235039983" Y="0.591029790006" />
                  <Point X="3.992042765711" Y="1.017496453372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.330266385859" Y="1.864531593147" />
                  <Point X="3.026514332987" Y="2.253316491481" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.359135882777" Y="3.107521954333" />
                  <Point X="1.145189277973" Y="4.661302753081" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.225547387346" Y="0.564318588511" />
                  <Point X="3.882729774531" Y="1.003105123402" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.254894961807" Y="1.806697038347" />
                  <Point X="2.943433378118" Y="2.205349686139" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.307905491853" Y="3.01878828619" />
                  <Point X="0.996605793821" Y="4.697175361984" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.125859734709" Y="0.537607387016" />
                  <Point X="3.773416783351" Y="0.988713793432" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.179523537756" Y="1.748862483547" />
                  <Point X="2.860352423249" Y="2.157382880798" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.256675107365" Y="2.930054609809" />
                  <Point X="0.848022056792" Y="4.733048294553" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.026172084931" Y="0.510896181862" />
                  <Point X="3.664103792171" Y="0.974322463461" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.113232628935" Y="1.679405399262" />
                  <Point X="2.77727146838" Y="2.109416075456" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.205444723528" Y="2.841320932594" />
                  <Point X="0.714378732093" Y="4.749798371379" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.926484443806" Y="0.484184965631" />
                  <Point X="3.554790800991" Y="0.959931133491" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.055417875366" Y="1.599099330989" />
                  <Point X="2.694190513511" Y="2.061449270115" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.154214339691" Y="2.752587255379" />
                  <Point X="0.583078828166" Y="4.763549006397" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.826796802682" Y="0.4574737494" />
                  <Point X="3.439959826468" Y="0.952602500129" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.016645938994" Y="1.494419568192" />
                  <Point X="2.609599732315" Y="2.015414954347" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.102983955854" Y="2.663853578164" />
                  <Point X="0.451778924239" Y="4.777299641415" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.766379177279" Y="-0.899442427043" />
                  <Point X="4.640243431096" Y="-0.737996034195" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.727109161558" Y="0.430762533169" />
                  <Point X="3.300859985419" Y="0.976336599397" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.004194628948" Y="1.356050939975" />
                  <Point X="2.511886514627" Y="1.98617659136" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.053144122743" Y="2.573340077184" />
                  <Point X="0.364658191624" Y="4.734503515796" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.741021267082" Y="-1.021291360397" />
                  <Point X="4.48776644408" Y="-0.697139968883" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.633094222246" Y="0.396790589722" />
                  <Point X="2.398497668935" Y="1.977002117266" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.020102709121" Y="2.461325579745" />
                  <Point X="0.333870914384" Y="4.619603855356" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.658605598939" Y="-1.070109693916" />
                  <Point X="4.335289455881" Y="-0.656283902056" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.550043211419" Y="0.348785457753" />
                  <Point X="2.248725663319" Y="2.014395964271" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.021110086151" Y="2.305730617623" />
                  <Point X="0.303083637143" Y="4.504704194917" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.524226920235" Y="-1.052418406885" />
                  <Point X="4.182812467681" Y="-0.615427835229" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.476629202955" Y="0.288445525252" />
                  <Point X="0.272296359903" Y="4.389804534478" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.389848241532" Y="-1.034727119854" />
                  <Point X="4.030335479482" Y="-0.574571768401" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.416038951085" Y="0.211691932805" />
                  <Point X="0.241509082663" Y="4.274904874039" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.255469562829" Y="-1.017035832824" />
                  <Point X="3.877858491282" Y="-0.533715701574" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.371403665961" Y="0.11451691418" />
                  <Point X="0.195313824102" Y="4.17972653036" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.121090884125" Y="-0.999344545793" />
                  <Point X="3.725381503082" Y="-0.492859634747" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.349520631355" Y="-0.011779657111" />
                  <Point X="0.123890880783" Y="4.116838150687" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.349038850818" Y="4.722160603265" />
                  <Point X="-0.393628023761" Y="4.779232142059" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.986712205422" Y="-0.981653258763" />
                  <Point X="3.532522083172" Y="-0.400316412364" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.379798065263" Y="-0.204838583607" />
                  <Point X="0.027378954791" Y="4.086062204446" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.28611113466" Y="4.487311221215" />
                  <Point X="-0.504084856812" Y="4.766304862921" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.852333526935" Y="-0.963961972009" />
                  <Point X="-0.121269291844" Y="4.122017705546" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.221647620226" Y="4.250496107012" />
                  <Point X="-0.614541689864" Y="4.753377583783" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.717954889" Y="-0.946270737159" />
                  <Point X="-0.724998522915" Y="4.740450304645" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.583576251064" Y="-0.928579502309" />
                  <Point X="-0.835455254891" Y="4.727522896137" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.449197613129" Y="-0.910888267459" />
                  <Point X="-0.944369724212" Y="4.712621481447" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.333586056033" Y="-0.91721780069" />
                  <Point X="-1.043264151515" Y="4.684894997823" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.230529931217" Y="-0.939617554406" />
                  <Point X="-1.142158578818" Y="4.657168514199" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.131559671938" Y="-0.967246977527" />
                  <Point X="-1.241053006121" Y="4.629442030576" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.052410322452" Y="-1.02024600828" />
                  <Point X="-1.339947433424" Y="4.601715546952" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.989156210882" Y="-1.093590015795" />
                  <Point X="-1.438841860727" Y="4.573989063328" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.079128352428" Y="-2.642996316011" />
                  <Point X="4.041700899738" Y="-2.595091361127" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.927198763203" Y="-1.168593677407" />
                  <Point X="-1.464821325435" Y="4.452935683469" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.02445204615" Y="-2.727319413632" />
                  <Point X="3.740683929586" Y="-2.364112787352" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.881895575059" Y="-1.264913819151" />
                  <Point X="-1.479594908534" Y="4.317539429213" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.870747503124" Y="-2.684892148276" />
                  <Point X="3.439667028127" Y="-2.133134301503" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.865777790809" Y="-1.398589574392" />
                  <Point X="-1.516237610935" Y="4.210134371212" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.651123859138" Y="-2.558092281246" />
                  <Point X="3.138650126668" Y="-1.902155815653" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.871388522022" Y="-1.560076561179" />
                  <Point X="-1.574837289508" Y="4.130832961129" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.431500215153" Y="-2.431292414216" />
                  <Point X="-1.656916184377" Y="4.081583577476" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.211876572472" Y="-2.304492548856" />
                  <Point X="-1.748261554714" Y="4.044194741557" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.992252967659" Y="-2.177692731964" />
                  <Point X="-1.857999264423" Y="4.030347026515" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.788330870026" Y="-2.070989927801" />
                  <Point X="-2.03561261843" Y="4.103376174421" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.646566581461" Y="-2.043845491229" />
                  <Point X="-2.257022448113" Y="4.232462254888" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.511637733541" Y="-2.025450019713" />
                  <Point X="-2.341088811529" Y="4.185756714971" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.408502309432" Y="-2.047748274962" />
                  <Point X="-2.425155174946" Y="4.139051175054" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.322627490209" Y="-2.092139097003" />
                  <Point X="-2.509221620087" Y="4.09234573974" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.237198057717" Y="-2.137099988063" />
                  <Point X="-2.593288253118" Y="4.045640544913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.160871379134" Y="-2.193711872819" />
                  <Point X="-2.674365579642" Y="3.995109212238" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.10470808662" Y="-2.276131714849" />
                  <Point X="-2.749760429659" Y="3.937304641306" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.056184710911" Y="-2.368330204466" />
                  <Point X="-2.825155279676" Y="3.879500070375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.011503875611" Y="-2.465446921525" />
                  <Point X="-2.900550129692" Y="3.821695499443" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.008691463891" Y="-2.616152776999" />
                  <Point X="-2.975944979709" Y="3.763890928511" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.047991521464" Y="-2.820760135156" />
                  <Point X="-3.051339829725" Y="3.706086357579" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.35504556711" Y="-3.368076969831" />
                  <Point X="-2.772035338126" Y="3.194287332402" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.696347155063" Y="-3.959228659706" />
                  <Point X="-2.749117750803" Y="3.010648579957" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.598364471203" Y="-2.708180489528" />
                  <Point X="-2.783317235775" Y="2.900116346251" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.429853392452" Y="-2.64680172267" />
                  <Point X="-2.84596304275" Y="2.82599374436" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.315735431223" Y="-2.655042971433" />
                  <Point X="-2.917434018112" Y="2.763166842899" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.203264769508" Y="-2.665392667425" />
                  <Point X="-3.009735682056" Y="2.727002006981" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.101411466117" Y="-2.689331962359" />
                  <Point X="-3.13195941762" Y="2.72913567625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.021333364417" Y="-2.741142244487" />
                  <Point X="-3.298510755055" Y="2.78800608861" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.948251534341" Y="-2.801907345936" />
                  <Point X="-3.518134254569" Y="2.914805770725" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.875169667537" Y="-2.862672400377" />
                  <Point X="-3.729466156506" Y="3.030992691903" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.812495334467" Y="-2.936758490531" />
                  <Point X="-3.789617225693" Y="2.953676971257" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.774729274971" Y="-3.042725717019" />
                  <Point X="-3.849768294881" Y="2.876361250611" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.748490070303" Y="-3.16344664489" />
                  <Point X="-3.90991937555" Y="2.799045544659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.724933679619" Y="-3.28760141807" />
                  <Point X="-3.970070501525" Y="2.721729896696" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.742525438978" Y="-3.464423421578" />
                  <Point X="-4.026229034916" Y="2.639303963266" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.766957051787" Y="-3.650000038275" />
                  <Point X="0.662751443984" Y="-3.51662294254" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.323826963313" Y="-3.082819389559" />
                  <Point X="-4.077781371969" Y="2.550982367376" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.791388664596" Y="-3.835576654972" />
                  <Point X="0.725679135824" Y="-3.751472293465" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.16467987605" Y="-3.033425985251" />
                  <Point X="-3.424593694658" Y="1.56063468723" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.653747436184" Y="1.853938101181" />
                  <Point X="-4.129333709021" Y="2.462660771485" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.815820277405" Y="-4.021153271668" />
                  <Point X="0.788606848357" Y="-3.986321670875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.016262981693" Y="-2.997766601562" />
                  <Point X="-3.440531043359" Y="1.426727985018" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.954764771318" Y="2.08491714211" />
                  <Point X="-4.180886046074" Y="2.374339175595" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.091252844095" Y="-3.014458198338" />
                  <Point X="-3.486217472526" Y="1.330898369414" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.188281812519" Y="-3.044572360444" />
                  <Point X="-3.553904064977" Y="1.263227678712" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.285310682074" Y="-3.074686649097" />
                  <Point X="-3.644037397543" Y="1.224287505191" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.375278216441" Y="-3.113839034635" />
                  <Point X="-3.749062546918" Y="1.204407987983" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.44525163453" Y="-3.178582721998" />
                  <Point X="-3.881158861638" Y="1.21917798233" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.501965970316" Y="-3.260297260804" />
                  <Point X="-4.015537657789" Y="1.236869419687" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.558680284787" Y="-3.342011826892" />
                  <Point X="-4.149916376878" Y="1.254560758409" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.614806353715" Y="-3.42447931294" />
                  <Point X="-3.297850254696" Y="0.009660276926" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.4753627006" Y="0.236865846672" />
                  <Point X="-4.284295040605" Y="1.272252026272" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.653742377954" Y="-3.528949052846" />
                  <Point X="-3.311881646857" Y="-0.12668593841" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.631716291017" Y="0.282683738068" />
                  <Point X="-4.418673704333" Y="1.289943294135" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.684529631994" Y="-3.64384874298" />
                  <Point X="-3.361778395633" Y="-0.217126590661" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.784193296018" Y="0.3235398264" />
                  <Point X="-4.553052368061" Y="1.307634561998" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.715316886034" Y="-3.758748433114" />
                  <Point X="-3.434961471614" Y="-0.277762103262" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.936670301019" Y="0.364395914733" />
                  <Point X="-4.646026261508" Y="1.272330140607" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.746104140074" Y="-3.873648123248" />
                  <Point X="-3.52511265885" Y="-0.316679423847" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.08914730602" Y="0.405252003065" />
                  <Point X="-4.677072054687" Y="1.15776136548" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.776891394114" Y="-3.988547813382" />
                  <Point X="-3.624800303468" Y="-0.343390635607" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.241624311021" Y="0.446108091397" />
                  <Point X="-4.708117934198" Y="1.043192700853" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.807678648154" Y="-4.103447503516" />
                  <Point X="-2.947523236284" Y="-1.364571328746" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.146573651711" Y="-1.109798415135" />
                  <Point X="-3.724487948085" Y="-0.370101847367" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.394101316022" Y="0.486964179729" />
                  <Point X="-4.736174694784" Y="0.924798138471" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.838465902195" Y="-4.21834719365" />
                  <Point X="-2.976514493977" Y="-1.481769789376" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.271805974396" Y="-1.103813929956" />
                  <Point X="-3.824175592702" Y="-0.396813059127" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.546578321023" Y="0.527820268061" />
                  <Point X="-4.755372036733" Y="0.795064037339" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.869253156235" Y="-4.333246883784" />
                  <Point X="-3.034675583995" Y="-1.561632567209" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.381118972903" Y="-1.118205250548" />
                  <Point X="-3.923863236753" Y="-0.423524271612" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.699055270371" Y="0.56867628516" />
                  <Point X="-4.774569378683" Y="0.665329936206" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.900040410275" Y="-4.448146573918" />
                  <Point X="-2.314119231949" Y="-2.638208218854" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.539652766465" Y="-2.349538458572" />
                  <Point X="-3.10910368576" Y="-1.620674519475" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.49043197141" Y="-1.13259657114" />
                  <Point X="-4.023550871831" Y="-0.450235495582" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.930827664315" Y="-4.563046264052" />
                  <Point X="-1.171645392184" Y="-4.254813628382" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.4611661952" Y="-3.884243899216" />
                  <Point X="-2.352565410527" Y="-2.743304932615" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.649301857949" Y="-2.36349959977" />
                  <Point X="-3.184475103942" Y="-1.678509081788" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.599744969917" Y="-1.146987891732" />
                  <Point X="-4.123238506909" Y="-0.476946719552" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.961614993389" Y="-4.677945858147" />
                  <Point X="-1.130468545365" Y="-4.461823167229" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.651274534777" Y="-3.795221899086" />
                  <Point X="-2.403795812758" Y="-2.832038586287" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.735697933514" Y="-2.407223244117" />
                  <Point X="-3.259846522124" Y="-1.736343644101" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.709057968423" Y="-1.161379212324" />
                  <Point X="-4.222926141986" Y="-0.503657943522" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.017629923478" Y="-4.760555595422" />
                  <Point X="-1.126712980529" Y="-4.620935649336" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.778337939962" Y="-3.786893735182" />
                  <Point X="-2.455026214989" Y="-2.920772239958" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.818778887562" Y="-2.455190050509" />
                  <Point X="-3.335217940306" Y="-1.794178206413" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.81837096693" Y="-1.175770532916" />
                  <Point X="-4.322613777064" Y="-0.530369167492" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.905401343766" Y="-3.778565573046" />
                  <Point X="-2.50625661722" Y="-3.009505893629" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.901859841611" Y="-2.503156856902" />
                  <Point X="-3.410589357968" Y="-1.85201276939" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.927683965437" Y="-1.190161853508" />
                  <Point X="-4.422301412142" Y="-0.557080391461" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.020666846891" Y="-3.785338035161" />
                  <Point X="-2.557487021387" Y="-3.098239544823" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.984940795659" Y="-2.551123663294" />
                  <Point X="-3.485960762444" Y="-1.909847349246" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.036996963944" Y="-1.2045531741" />
                  <Point X="-4.52198904722" Y="-0.583791615431" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.109876569831" Y="-3.825460375095" />
                  <Point X="-2.608717433092" Y="-3.186973186367" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.068021749707" Y="-2.599090469686" />
                  <Point X="-3.56133216692" Y="-1.967681929101" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.146309962451" Y="-1.218944494693" />
                  <Point X="-4.621676682298" Y="-0.610502839401" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.189083969492" Y="-3.878385105011" />
                  <Point X="-2.659947844798" Y="-3.275706827911" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.151102703756" Y="-2.647057276078" />
                  <Point X="-3.636703571395" Y="-2.025516508957" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.255622960958" Y="-1.233335815285" />
                  <Point X="-4.721364317376" Y="-0.637214063371" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.268291291715" Y="-3.931309934044" />
                  <Point X="-2.711178256504" Y="-3.364440469456" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.234183657804" Y="-2.69502408247" />
                  <Point X="-3.712074975871" Y="-2.083351088812" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.364935959464" Y="-1.247727135877" />
                  <Point X="-4.777076033716" Y="-0.720211896546" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.347498613938" Y="-3.984234763077" />
                  <Point X="-2.76240866821" Y="-3.453174111" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.317264611852" Y="-2.742990888862" />
                  <Point X="-3.787446380347" Y="-2.141185668668" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.474248919099" Y="-1.262118506223" />
                  <Point X="-4.750061745882" Y="-0.909094186531" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.416784824644" Y="-4.049858035779" />
                  <Point X="-2.813639079915" Y="-3.541907752544" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.4003455659" Y="-2.790957695254" />
                  <Point X="-3.862817784823" Y="-2.199020248523" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.583561835226" Y="-1.276509932257" />
                  <Point X="-4.702361843522" Y="-1.124452855734" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.476519891946" Y="-4.127706214558" />
                  <Point X="-2.864869491621" Y="-3.630641394089" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.483426519949" Y="-2.838924501647" />
                  <Point X="-3.938189189298" Y="-2.256854828379" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.632416369248" Y="-4.082473401268" />
                  <Point X="-2.916099903327" Y="-3.719375035633" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.566507473997" Y="-2.886891308039" />
                  <Point X="-4.013560593774" Y="-2.314689408234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.900937792467" Y="-3.893087230875" />
                  <Point X="-2.967330315033" Y="-3.808108677177" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.649588419704" Y="-2.934858125107" />
                  <Point X="-4.08893199825" Y="-2.37252398809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.732669350257" Y="-2.982824961571" />
                  <Point X="-4.164303402726" Y="-2.430358567945" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="0.001626220703" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.582135803223" Y="-3.949864746094" />
                  <Point X="0.471539031982" Y="-3.537112304688" />
                  <Point X="0.464318664551" Y="-3.521544189453" />
                  <Point X="0.419115020752" Y="-3.456414306641" />
                  <Point X="0.300591033936" Y="-3.285643798828" />
                  <Point X="0.274335754395" Y="-3.266399902344" />
                  <Point X="0.204385681152" Y="-3.244689941406" />
                  <Point X="0.02097677803" Y="-3.187766601562" />
                  <Point X="-0.008664908409" Y="-3.187766601562" />
                  <Point X="-0.078614830017" Y="-3.209476318359" />
                  <Point X="-0.262023895264" Y="-3.266399902344" />
                  <Point X="-0.288279144287" Y="-3.285643798828" />
                  <Point X="-0.333482788086" Y="-3.3507734375" />
                  <Point X="-0.45200680542" Y="-3.521544189453" />
                  <Point X="-0.459227142334" Y="-3.537112304688" />
                  <Point X="-0.749211608887" Y="-4.619349609375" />
                  <Point X="-0.84774395752" Y="-4.987076660156" />
                  <Point X="-0.88473840332" Y="-4.979895996094" />
                  <Point X="-1.100246459961" Y="-4.938065429688" />
                  <Point X="-1.180426391602" Y="-4.917435546875" />
                  <Point X="-1.35158972168" Y="-4.873396972656" />
                  <Point X="-1.322463623047" Y="-4.652162597656" />
                  <Point X="-1.309150146484" Y="-4.551038574219" />
                  <Point X="-1.309683227539" Y="-4.5347578125" />
                  <Point X="-1.326394287109" Y="-4.450745605469" />
                  <Point X="-1.370210693359" Y="-4.230466308594" />
                  <Point X="-1.386282714844" Y="-4.20262890625" />
                  <Point X="-1.45068371582" Y="-4.146150390625" />
                  <Point X="-1.619543457031" Y="-3.998063964844" />
                  <Point X="-1.649240722656" Y="-3.985762939453" />
                  <Point X="-1.734715332031" Y="-3.980160644531" />
                  <Point X="-1.958829833984" Y="-3.965471435547" />
                  <Point X="-1.989878540039" Y="-3.973791015625" />
                  <Point X="-2.061100585938" Y="-4.021380126953" />
                  <Point X="-2.247844970703" Y="-4.146159179688" />
                  <Point X="-2.259734130859" Y="-4.157294433594" />
                  <Point X="-2.422538085938" Y="-4.369464355469" />
                  <Point X="-2.457095214844" Y="-4.414500488281" />
                  <Point X="-2.539942871094" Y="-4.363203125" />
                  <Point X="-2.855831054688" Y="-4.16761328125" />
                  <Point X="-2.966861083984" Y="-4.082124023438" />
                  <Point X="-3.228581054688" Y="-3.880608154297" />
                  <Point X="-2.711565185547" Y="-2.985110595703" />
                  <Point X="-2.506033691406" Y="-2.629119628906" />
                  <Point X="-2.499762695312" Y="-2.597593261719" />
                  <Point X="-2.513979736328" Y="-2.568764404297" />
                  <Point X="-2.531328369141" Y="-2.551415771484" />
                  <Point X="-2.560157226562" Y="-2.537198730469" />
                  <Point X="-2.59168359375" Y="-2.543469726562" />
                  <Point X="-3.525092041016" Y="-3.082373291016" />
                  <Point X="-3.842959472656" Y="-3.265894042969" />
                  <Point X="-3.912132568359" Y="-3.175014892578" />
                  <Point X="-4.161703613281" Y="-2.847129394531" />
                  <Point X="-4.24130078125" Y="-2.713656982422" />
                  <Point X="-4.431020019531" Y="-2.395527099609" />
                  <Point X="-3.523400146484" Y="-1.6990859375" />
                  <Point X="-3.163786865234" Y="-1.423144775391" />
                  <Point X="-3.152535644531" Y="-1.411081420898" />
                  <Point X="-3.143694824219" Y="-1.387800537109" />
                  <Point X="-3.138117431641" Y="-1.366266113281" />
                  <Point X="-3.136651855469" Y="-1.350051147461" />
                  <Point X="-3.148656738281" Y="-1.321068603516" />
                  <Point X="-3.168470703125" Y="-1.3063359375" />
                  <Point X="-3.187641601562" Y="-1.295052734375" />
                  <Point X="-3.219529296875" Y="-1.288571044922" />
                  <Point X="-4.397868652344" Y="-1.443702392578" />
                  <Point X="-4.803283203125" Y="-1.497076293945" />
                  <Point X="-4.829784179688" Y="-1.393326538086" />
                  <Point X="-4.927393066406" Y="-1.011191650391" />
                  <Point X="-4.948452148438" Y="-0.863946899414" />
                  <Point X="-4.998396484375" Y="-0.514742126465" />
                  <Point X="-3.967117675781" Y="-0.23841178894" />
                  <Point X="-3.557463867188" Y="-0.128645431519" />
                  <Point X="-3.534233398438" Y="-0.116106941223" />
                  <Point X="-3.514142822266" Y="-0.102162895203" />
                  <Point X="-3.50232421875" Y="-0.090645202637" />
                  <Point X="-3.492344726562" Y="-0.067678131104" />
                  <Point X="-3.485647949219" Y="-0.046100585938" />
                  <Point X="-3.483400878906" Y="-0.031279787064" />
                  <Point X="-3.488201904297" Y="-0.008229559898" />
                  <Point X="-3.494898925781" Y="0.013347985268" />
                  <Point X="-3.502324462891" Y="0.028085325241" />
                  <Point X="-3.521805175781" Y="0.044920940399" />
                  <Point X="-3.541895751953" Y="0.05886498642" />
                  <Point X="-3.557463867188" Y="0.066085334778" />
                  <Point X="-4.631576171875" Y="0.353892883301" />
                  <Point X="-4.998186523438" Y="0.452125793457" />
                  <Point X="-4.980552734375" Y="0.571294006348" />
                  <Point X="-4.917645019531" Y="0.996418212891" />
                  <Point X="-4.875252441406" Y="1.152860107422" />
                  <Point X="-4.773516113281" Y="1.528298583984" />
                  <Point X="-4.046916259766" Y="1.432640014648" />
                  <Point X="-3.753266357422" Y="1.393980102539" />
                  <Point X="-3.731704833984" Y="1.395866577148" />
                  <Point X="-3.714745849609" Y="1.401213745117" />
                  <Point X="-3.670278808594" Y="1.415234130859" />
                  <Point X="-3.651534423828" Y="1.426056152344" />
                  <Point X="-3.639119873047" Y="1.443785888672" />
                  <Point X="-3.632314941406" Y="1.460214477539" />
                  <Point X="-3.614472412109" Y="1.503290161133" />
                  <Point X="-3.610714111328" Y="1.524605224609" />
                  <Point X="-3.616315917969" Y="1.54551159668" />
                  <Point X="-3.624526855469" Y="1.561284545898" />
                  <Point X="-3.646055664062" Y="1.602641235352" />
                  <Point X="-3.659968261719" Y="1.619221679688" />
                  <Point X="-4.276081542969" Y="2.091982177734" />
                  <Point X="-4.47610546875" Y="2.245466308594" />
                  <Point X="-4.404451171875" Y="2.368227783203" />
                  <Point X="-4.160014648438" Y="2.787006591797" />
                  <Point X="-4.047718994141" Y="2.931346435547" />
                  <Point X="-3.774671142578" Y="3.282310791016" />
                  <Point X="-3.339508544922" Y="3.031069335938" />
                  <Point X="-3.159156982422" Y="2.926943359375" />
                  <Point X="-3.138514892578" Y="2.920434814453" />
                  <Point X="-3.114895507812" Y="2.918368408203" />
                  <Point X="-3.052965576172" Y="2.912950195312" />
                  <Point X="-3.031506835938" Y="2.915775390625" />
                  <Point X="-3.013252441406" Y="2.927404785156" />
                  <Point X="-2.996487060547" Y="2.944169921875" />
                  <Point X="-2.952528808594" Y="2.988128173828" />
                  <Point X="-2.940899414062" Y="3.006382324219" />
                  <Point X="-2.93807421875" Y="3.027841308594" />
                  <Point X="-2.940140625" Y="3.051460693359" />
                  <Point X="-2.945558837891" Y="3.113390869141" />
                  <Point X="-2.952067382812" Y="3.134032958984" />
                  <Point X="-3.2250859375" Y="3.606914794922" />
                  <Point X="-3.307278564453" Y="3.749276855469" />
                  <Point X="-3.178607421875" Y="3.847927734375" />
                  <Point X="-2.752876220703" Y="4.174331542969" />
                  <Point X="-2.576014404297" Y="4.272591796875" />
                  <Point X="-2.141548583984" Y="4.513971679688" />
                  <Point X="-2.023038085938" Y="4.359525878906" />
                  <Point X="-1.967826904297" Y="4.287573242188" />
                  <Point X="-1.951246582031" Y="4.273660644531" />
                  <Point X="-1.924958496094" Y="4.259975585938" />
                  <Point X="-1.856030639648" Y="4.22409375" />
                  <Point X="-1.835124023438" Y="4.2184921875" />
                  <Point X="-1.813808837891" Y="4.222250488281" />
                  <Point X="-1.786427856445" Y="4.233592285156" />
                  <Point X="-1.714634887695" Y="4.263330078125" />
                  <Point X="-1.696905273438" Y="4.275744628906" />
                  <Point X="-1.686083496094" Y="4.294488769531" />
                  <Point X="-1.677171386719" Y="4.32275390625" />
                  <Point X="-1.653804077148" Y="4.396865234375" />
                  <Point X="-1.651917602539" Y="4.418426757812" />
                  <Point X="-1.682956787109" Y="4.654192871094" />
                  <Point X="-1.689137695313" Y="4.701141113281" />
                  <Point X="-1.519229492188" Y="4.74877734375" />
                  <Point X="-0.968094177246" Y="4.903296386719" />
                  <Point X="-0.753685791016" Y="4.928389648438" />
                  <Point X="-0.224199615479" Y="4.990358398438" />
                  <Point X="-0.095493347168" Y="4.51001953125" />
                  <Point X="-0.042140380859" Y="4.310903320312" />
                  <Point X="-0.024282117844" Y="4.284176757813" />
                  <Point X="0.006155934334" Y="4.273844238281" />
                  <Point X="0.036594036102" Y="4.284176757813" />
                  <Point X="0.054452251434" Y="4.310903320312" />
                  <Point X="0.19434387207" Y="4.832985839844" />
                  <Point X="0.236648422241" Y="4.990868652344" />
                  <Point X="0.378984008789" Y="4.975962402344" />
                  <Point X="0.860205749512" Y="4.925565429688" />
                  <Point X="1.037592773438" Y="4.882738769531" />
                  <Point X="1.508455932617" Y="4.769058105469" />
                  <Point X="1.623816650391" Y="4.727215820312" />
                  <Point X="1.931033691406" Y="4.615786132813" />
                  <Point X="2.042680786133" Y="4.563572265625" />
                  <Point X="2.33869921875" Y="4.425134277344" />
                  <Point X="2.446559570312" Y="4.362294433594" />
                  <Point X="2.732523681641" Y="4.19569140625" />
                  <Point X="2.834246337891" Y="4.123352050781" />
                  <Point X="3.068740234375" Y="3.956592773438" />
                  <Point X="2.467740722656" Y="2.915630859375" />
                  <Point X="2.229853515625" Y="2.503597900391" />
                  <Point X="2.224852050781" Y="2.491514892578" />
                  <Point X="2.218924560547" Y="2.469348632812" />
                  <Point X="2.203382324219" Y="2.411228515625" />
                  <Point X="2.204355957031" Y="2.373158447266" />
                  <Point X="2.210416015625" Y="2.322901123047" />
                  <Point X="2.218682128906" Y="2.3008125" />
                  <Point X="2.230542480469" Y="2.283333496094" />
                  <Point X="2.261639892578" Y="2.23750390625" />
                  <Point X="2.274940185547" Y="2.224203613281" />
                  <Point X="2.292418945312" Y="2.212343261719" />
                  <Point X="2.338248779297" Y="2.18124609375" />
                  <Point X="2.360336914062" Y="2.172980224609" />
                  <Point X="2.379504394531" Y="2.170668945312" />
                  <Point X="2.42976171875" Y="2.164608642578" />
                  <Point X="2.448664306641" Y="2.165946289062" />
                  <Point X="2.470830566406" Y="2.171874023438" />
                  <Point X="2.528950683594" Y="2.187416015625" />
                  <Point X="2.541033935547" Y="2.192417480469" />
                  <Point X="3.621384277344" Y="2.816157958984" />
                  <Point X="3.994248046875" Y="3.031430908203" />
                  <Point X="4.032967773438" Y="2.977619384766" />
                  <Point X="4.202591796875" Y="2.741880126953" />
                  <Point X="4.25930078125" Y="2.64816796875" />
                  <Point X="4.387512695312" Y="2.436296142578" />
                  <Point X="3.601817138672" Y="1.83341027832" />
                  <Point X="3.288616210938" Y="1.593082763672" />
                  <Point X="3.279371337891" Y="1.583833374023" />
                  <Point X="3.263418212891" Y="1.563021240234" />
                  <Point X="3.221589111328" Y="1.508451904297" />
                  <Point X="3.213119628906" Y="1.4915" />
                  <Point X="3.207177001953" Y="1.470250854492" />
                  <Point X="3.191595703125" Y="1.414535522461" />
                  <Point X="3.190779541016" Y="1.390965332031" />
                  <Point X="3.195657714844" Y="1.367322875977" />
                  <Point X="3.208448486328" Y="1.305332519531" />
                  <Point X="3.215646484375" Y="1.287954956055" />
                  <Point X="3.228914794922" Y="1.267787719727" />
                  <Point X="3.263704101562" Y="1.214909667969" />
                  <Point X="3.280947998047" Y="1.198819824219" />
                  <Point X="3.300175537109" Y="1.187996337891" />
                  <Point X="3.350590087891" Y="1.159617431641" />
                  <Point X="3.368565673828" Y="1.153619628906" />
                  <Point X="3.3945625" Y="1.15018371582" />
                  <Point X="3.462726318359" Y="1.141175048828" />
                  <Point X="3.475803710938" Y="1.14117175293" />
                  <Point X="4.502063964844" Y="1.276281494141" />
                  <Point X="4.848975097656" Y="1.32195324707" />
                  <Point X="4.8663359375" Y="1.250641113281" />
                  <Point X="4.939188476562" Y="0.951386047363" />
                  <Point X="4.957059082031" Y="0.836605041504" />
                  <Point X="4.997858398438" Y="0.574556152344" />
                  <Point X="4.100102539062" Y="0.334003204346" />
                  <Point X="3.741167724609" Y="0.237826919556" />
                  <Point X="3.729086914062" Y="0.232819412231" />
                  <Point X="3.703545898438" Y="0.218056060791" />
                  <Point X="3.636576904297" Y="0.179346878052" />
                  <Point X="3.622264892578" Y="0.166926574707" />
                  <Point X="3.606940185547" Y="0.147399261475" />
                  <Point X="3.566759033203" Y="0.096198890686" />
                  <Point X="3.556985107422" Y="0.07473513031" />
                  <Point X="3.551876708984" Y="0.048061836243" />
                  <Point X="3.538482910156" Y="-0.021875303268" />
                  <Point X="3.538482910156" Y="-0.040684635162" />
                  <Point X="3.543591308594" Y="-0.067357925415" />
                  <Point X="3.556985107422" Y="-0.137295227051" />
                  <Point X="3.566759033203" Y="-0.158758987427" />
                  <Point X="3.582083740234" Y="-0.178286148071" />
                  <Point X="3.622264892578" Y="-0.229486679077" />
                  <Point X="3.636576904297" Y="-0.241906967163" />
                  <Point X="3.662118164062" Y="-0.256670166016" />
                  <Point X="3.729086914062" Y="-0.295379486084" />
                  <Point X="3.741167724609" Y="-0.300387023926" />
                  <Point X="4.682295410156" Y="-0.552561279297" />
                  <Point X="4.998067871094" Y="-0.637172363281" />
                  <Point X="4.989109375" Y="-0.696593383789" />
                  <Point X="4.948431640625" Y="-0.966399169922" />
                  <Point X="4.925536621094" Y="-1.066729858398" />
                  <Point X="4.874545898438" Y="-1.290178344727" />
                  <Point X="3.828245849609" Y="-1.152430297852" />
                  <Point X="3.411982177734" Y="-1.097628295898" />
                  <Point X="3.394836181641" Y="-1.098341186523" />
                  <Point X="3.344708007812" Y="-1.109236816406" />
                  <Point X="3.213271972656" Y="-1.137804931641" />
                  <Point X="3.1854453125" Y="-1.154697265625" />
                  <Point X="3.155145996094" Y="-1.191137939453" />
                  <Point X="3.075701171875" Y="-1.286685424805" />
                  <Point X="3.064357910156" Y="-1.314070556641" />
                  <Point X="3.060015380859" Y="-1.361262817383" />
                  <Point X="3.04862890625" Y="-1.485000854492" />
                  <Point X="3.056360595703" Y="-1.516621826172" />
                  <Point X="3.084102294922" Y="-1.559772216797" />
                  <Point X="3.156840820312" Y="-1.672912353516" />
                  <Point X="3.168460693359" Y="-1.685540649414" />
                  <Point X="4.041832763672" Y="-2.355702636719" />
                  <Point X="4.339074707031" Y="-2.583784179688" />
                  <Point X="4.318798828125" Y="-2.616593261719" />
                  <Point X="4.204133300781" Y="-2.802139404297" />
                  <Point X="4.156782226562" Y="-2.869418457031" />
                  <Point X="4.056688476562" Y="-3.011638427734" />
                  <Point X="3.124193603516" Y="-2.473261962891" />
                  <Point X="2.753454589844" Y="-2.259215576172" />
                  <Point X="2.737340820313" Y="-2.253312744141" />
                  <Point X="2.677680175781" Y="-2.242538085938" />
                  <Point X="2.521250488281" Y="-2.214287109375" />
                  <Point X="2.489077636719" Y="-2.219244873047" />
                  <Point X="2.439514404297" Y="-2.245329589844" />
                  <Point X="2.309559570312" Y="-2.313723876953" />
                  <Point X="2.288599853516" Y="-2.33468359375" />
                  <Point X="2.262515136719" Y="-2.384246826172" />
                  <Point X="2.194120849609" Y="-2.514201660156" />
                  <Point X="2.189163085938" Y="-2.546374511719" />
                  <Point X="2.199937744141" Y="-2.606034912109" />
                  <Point X="2.228188720703" Y="-2.76246484375" />
                  <Point X="2.234091552734" Y="-2.778578613281" />
                  <Point X="2.795321044922" Y="-3.750656005859" />
                  <Point X="2.986673583984" Y="-4.082088623047" />
                  <Point X="2.971359619141" Y="-4.093027099609" />
                  <Point X="2.835296630859" Y="-4.190213378906" />
                  <Point X="2.782360107422" Y="-4.224478515625" />
                  <Point X="2.679775878906" Y="-4.290879394531" />
                  <Point X="1.966085449219" Y="-3.360780029297" />
                  <Point X="1.683177490234" Y="-2.992087402344" />
                  <Point X="1.670549316406" Y="-2.980467529297" />
                  <Point X="1.611707885742" Y="-2.942637939453" />
                  <Point X="1.45742590332" Y="-2.843448730469" />
                  <Point X="1.425804931641" Y="-2.835717041016" />
                  <Point X="1.361451660156" Y="-2.841638916016" />
                  <Point X="1.192718017578" Y="-2.857166015625" />
                  <Point X="1.165332885742" Y="-2.868509277344" />
                  <Point X="1.115641113281" Y="-2.909826416016" />
                  <Point X="0.985349243164" Y="-3.018159667969" />
                  <Point X="0.968456726074" Y="-3.045986328125" />
                  <Point X="0.953599243164" Y="-3.114343017578" />
                  <Point X="0.91464251709" Y="-3.29357421875" />
                  <Point X="0.91392956543" Y="-3.310720214844" />
                  <Point X="1.072977539062" Y="-4.518810058594" />
                  <Point X="1.127642211914" Y="-4.934028320312" />
                  <Point X="1.123071044922" Y="-4.935030273438" />
                  <Point X="0.994346191406" Y="-4.963246582031" />
                  <Point X="0.945438476562" Y="-4.972131835938" />
                  <Point X="0.860200378418" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#144" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.053352998312" Y="4.554240584169" Z="0.7" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.7" />
                  <Point X="-0.765653893088" Y="5.009330880415" Z="0.7" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.7" />
                  <Point X="-1.53888330275" Y="4.828200972733" Z="0.7" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.7" />
                  <Point X="-1.738685014862" Y="4.678946335746" Z="0.7" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.7" />
                  <Point X="-1.731013149215" Y="4.369069274014" Z="0.7" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.7" />
                  <Point X="-1.811718071487" Y="4.311066246672" Z="0.7" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.7" />
                  <Point X="-1.908026923497" Y="4.335606414254" Z="0.7" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.7" />
                  <Point X="-1.989526285562" Y="4.421243839443" Z="0.7" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.7" />
                  <Point X="-2.606453328131" Y="4.347579569456" Z="0.7" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.7" />
                  <Point X="-3.215185472508" Y="3.918887665597" Z="0.7" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.7" />
                  <Point X="-3.274543206752" Y="3.61319483068" Z="0.7" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.7" />
                  <Point X="-2.996106460286" Y="3.078382924333" Z="0.7" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.7" />
                  <Point X="-3.037998358773" Y="3.010805209114" Z="0.7" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.7" />
                  <Point X="-3.116693541274" Y="2.999458238887" Z="0.7" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.7" />
                  <Point X="-3.320664517617" Y="3.105650741994" Z="0.7" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.7" />
                  <Point X="-4.093338346231" Y="2.993328988567" Z="0.7" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.7" />
                  <Point X="-4.45378924524" Y="2.424712870077" Z="0.7" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.7" />
                  <Point X="-4.312675727185" Y="2.083594536508" Z="0.7" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.7" />
                  <Point X="-3.675033327029" Y="1.569477362828" Z="0.7" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.7" />
                  <Point X="-3.684664972897" Y="1.510628677537" Z="0.7" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.7" />
                  <Point X="-3.735936887104" Y="1.480179739505" Z="0.7" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.7" />
                  <Point X="-4.046545936777" Y="1.513492284786" Z="0.7" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.7" />
                  <Point X="-4.929668189389" Y="1.197217701228" Z="0.7" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.7" />
                  <Point X="-5.036170156351" Y="0.609893027973" Z="0.7" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.7" />
                  <Point X="-4.650673186543" Y="0.336876600282" Z="0.7" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.7" />
                  <Point X="-3.556470166445" Y="0.035125029704" Z="0.7" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.7" />
                  <Point X="-3.542110887939" Y="0.008229417671" Z="0.7" />
                  <Point X="-3.539556741714" Y="0" Z="0.7" />
                  <Point X="-3.546253713196" Y="-0.02157753339" Z="0.7" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.7" />
                  <Point X="-3.568898428678" Y="-0.04375095343" Z="0.7" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.7" />
                  <Point X="-3.98621460223" Y="-0.158835456474" Z="0.7" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.7" />
                  <Point X="-5.004104387898" Y="-0.83974596943" Z="0.7" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.7" />
                  <Point X="-4.884384936162" Y="-1.374420773663" Z="0.7" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.7" />
                  <Point X="-4.3974984588" Y="-1.461994625715" Z="0.7" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.7" />
                  <Point X="-3.199987896567" Y="-1.318146519182" Z="0.7" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.7" />
                  <Point X="-3.19825366347" Y="-1.343984203209" Z="0.7" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.7" />
                  <Point X="-3.559994216577" Y="-1.628138236735" Z="0.7" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.7" />
                  <Point X="-4.290400079136" Y="-2.707986658665" Z="0.7" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.7" />
                  <Point X="-3.958074331378" Y="-3.174017995086" Z="0.7" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.7" />
                  <Point X="-3.506247891613" Y="-3.094394550144" Z="0.7" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.7" />
                  <Point X="-2.560280735316" Y="-2.568049733816" Z="0.7" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.7" />
                  <Point X="-2.761022463025" Y="-2.928830490879" Z="0.7" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.7" />
                  <Point X="-3.003520936032" Y="-4.09046094365" Z="0.7" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.7" />
                  <Point X="-2.572420281254" Y="-4.374434056027" Z="0.7" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.7" />
                  <Point X="-2.389026236767" Y="-4.368622356368" Z="0.7" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.7" />
                  <Point X="-2.039478050199" Y="-4.031673492466" Z="0.7" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.7" />
                  <Point X="-1.744141488809" Y="-3.998773624641" Z="0.7" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.7" />
                  <Point X="-1.489807102284" Y="-4.15246215987" Z="0.7" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.7" />
                  <Point X="-1.381589590865" Y="-4.429220159762" Z="0.7" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.7" />
                  <Point X="-1.378191764104" Y="-4.614356200169" Z="0.7" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.7" />
                  <Point X="-1.199041045831" Y="-4.934578513964" Z="0.7" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.7" />
                  <Point X="-0.900421740017" Y="-4.997700173976" Z="0.7" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.7" />
                  <Point X="-0.707071366691" Y="-4.601010366945" Z="0.7" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.7" />
                  <Point X="-0.298562751171" Y="-3.348002549857" Z="0.7" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.7" />
                  <Point X="-0.069950048917" Y="-3.225949282224" Z="0.7" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.7" />
                  <Point X="0.183409030445" Y="-3.261162763064" Z="0.7" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.7" />
                  <Point X="0.371883108261" Y="-3.453643205911" Z="0.7" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.7" />
                  <Point X="0.52768349371" Y="-3.931525662871" Z="0.7" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.7" />
                  <Point X="0.948219414019" Y="-4.990046538207" Z="0.7" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.7" />
                  <Point X="1.127620944161" Y="-4.952590767458" Z="0.7" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.7" />
                  <Point X="1.116393894333" Y="-4.481003890599" Z="0.7" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.7" />
                  <Point X="0.996302414766" Y="-3.093682633852" Z="0.7" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.7" />
                  <Point X="1.141451941776" Y="-2.91699254984" Z="0.7" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.7" />
                  <Point X="1.359877418474" Y="-2.860148414285" Z="0.7" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.7" />
                  <Point X="1.578512527988" Y="-2.953415706167" Z="0.7" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.7" />
                  <Point X="1.920261894051" Y="-3.35993807436" Z="0.7" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.7" />
                  <Point X="2.803373273275" Y="-4.235172633202" Z="0.7" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.7" />
                  <Point X="2.994265496227" Y="-4.102433867319" Z="0.7" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.7" />
                  <Point X="2.83246630322" Y="-3.694376062426" Z="0.7" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.7" />
                  <Point X="2.242985763225" Y="-2.565868711167" Z="0.7" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.7" />
                  <Point X="2.300605414219" Y="-2.376253416991" Z="0.7" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.7" />
                  <Point X="2.456644961381" Y="-2.258295895748" Z="0.7" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.7" />
                  <Point X="2.662638092345" Y="-2.260462246266" Z="0.7" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.7" />
                  <Point X="3.093037534681" Y="-2.485283197054" Z="0.7" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.7" />
                  <Point X="4.191514831845" Y="-2.866915817686" Z="0.7" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.7" />
                  <Point X="4.355175886266" Y="-2.611598338208" Z="0.7" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.7" />
                  <Point X="4.066114754763" Y="-2.284754971357" Z="0.7" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.7" />
                  <Point X="3.1200041733" Y="-1.501452837403" Z="0.7" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.7" />
                  <Point X="3.103648842307" Y="-1.334564463649" Z="0.7" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.7" />
                  <Point X="3.187436227175" Y="-1.191824836551" Z="0.7" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.7" />
                  <Point X="3.349171663013" Y="-1.126815995452" Z="0.7" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.7" />
                  <Point X="3.815563489917" Y="-1.170722566858" Z="0.7" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.7" />
                  <Point X="4.968127608171" Y="-1.046573803354" Z="0.7" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.7" />
                  <Point X="5.032394817167" Y="-0.672767447834" Z="0.7" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.7" />
                  <Point X="4.689080162713" Y="-0.472984968998" Z="0.7" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.7" />
                  <Point X="3.680983847814" Y="-0.182101440123" Z="0.7" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.7" />
                  <Point X="3.615261403583" Y="-0.116137774597" Z="0.7" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.7" />
                  <Point X="3.58654295334" Y="-0.02667325524" Z="0.7" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.7" />
                  <Point X="3.594828497084" Y="0.069937275988" Z="0.7" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.7" />
                  <Point X="3.640118034817" Y="0.147810963978" Z="0.7" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.7" />
                  <Point X="3.722411566538" Y="0.206047404119" Z="0.7" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.7" />
                  <Point X="4.106887654593" Y="0.316986963771" Z="0.7" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.7" />
                  <Point X="5.000307868661" Y="0.875577168352" Z="0.7" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.7" />
                  <Point X="4.908761008699" Y="1.293748032518" Z="0.7" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.7" />
                  <Point X="4.489381810127" Y="1.357133869027" Z="0.7" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.7" />
                  <Point X="3.394957525212" Y="1.231032771438" Z="0.7" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.7" />
                  <Point X="3.318546068957" Y="1.262847604337" Z="0.7" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.7" />
                  <Point X="3.26452923323" Y="1.326549266376" Z="0.7" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.7" />
                  <Point X="3.238470267049" Y="1.40870679742" Z="0.7" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.7" />
                  <Point X="3.249173425876" Y="1.48806453171" Z="0.7" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.7" />
                  <Point X="3.296945046753" Y="1.563883008694" Z="0.7" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.7" />
                  <Point X="3.626099072717" Y="1.825022606247" Z="0.7" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.7" />
                  <Point X="4.295922214088" Y="2.705334346938" Z="0.7" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.7" />
                  <Point X="4.067397222373" Y="3.038102237239" Z="0.7" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.7" />
                  <Point X="3.590228243866" Y="2.890739282489" Z="0.7" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.7" />
                  <Point X="2.451757935059" Y="2.251456726672" Z="0.7" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.7" />
                  <Point X="2.379334338358" Y="2.251589276414" Z="0.7" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.7" />
                  <Point X="2.314337010788" Y="2.284997900013" Z="0.7" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.7" />
                  <Point X="2.265760700222" Y="2.342687849594" Z="0.7" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.7" />
                  <Point X="2.247840426344" Y="2.410424105474" Z="0.7" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.7" />
                  <Point X="2.261071215986" Y="2.487711640823" Z="0.7" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.7" />
                  <Point X="2.50488607037" Y="2.921910838089" Z="0.7" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.7" />
                  <Point X="2.857067342783" Y="4.195379092222" Z="0.7" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.7" />
                  <Point X="2.465573629946" Y="4.436777312369" Z="0.7" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.7" />
                  <Point X="2.05770641194" Y="4.640144369404" Z="0.7" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.7" />
                  <Point X="1.634709323692" Y="4.805499789967" Z="0.7" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.7" />
                  <Point X="1.043171283477" Y="4.962622515719" Z="0.7" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.7" />
                  <Point X="0.378035863475" Y="5.056970447716" Z="0.7" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.7" />
                  <Point X="0.139891563241" Y="4.877206921782" Z="0.7" />
                  <Point X="0" Y="4.355124473572" Z="0.7" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>