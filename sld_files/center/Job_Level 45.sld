<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#205" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="3218" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="0" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="0.004716064453" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.910462402344" Y="-4.808145019531" />
                  <Point X="0.563302001953" Y="-3.512524414063" />
                  <Point X="0.557721130371" Y="-3.497141845703" />
                  <Point X="0.542363098145" Y="-3.467377197266" />
                  <Point X="0.40046987915" Y="-3.262936035156" />
                  <Point X="0.378635437012" Y="-3.231476806641" />
                  <Point X="0.356751678467" Y="-3.209021484375" />
                  <Point X="0.330496337891" Y="-3.189777587891" />
                  <Point X="0.302495269775" Y="-3.175669189453" />
                  <Point X="0.082923606873" Y="-3.107522216797" />
                  <Point X="0.049136127472" Y="-3.097035888672" />
                  <Point X="0.020976577759" Y="-3.092766601562" />
                  <Point X="-0.00866505146" Y="-3.092766601562" />
                  <Point X="-0.036824447632" Y="-3.097035888672" />
                  <Point X="-0.256395935059" Y="-3.165182617188" />
                  <Point X="-0.290183410645" Y="-3.175669189453" />
                  <Point X="-0.318185424805" Y="-3.189778076172" />
                  <Point X="-0.344440460205" Y="-3.209022216797" />
                  <Point X="-0.366323455811" Y="-3.231476806641" />
                  <Point X="-0.508216827393" Y="-3.435917724609" />
                  <Point X="-0.530051269531" Y="-3.467377197266" />
                  <Point X="-0.538189208984" Y="-3.48157421875" />
                  <Point X="-0.55099017334" Y="-3.512524414063" />
                  <Point X="-0.604410949707" Y="-3.711893554688" />
                  <Point X="-0.916584594727" Y="-4.876941894531" />
                  <Point X="-1.043645019531" Y="-4.852278808594" />
                  <Point X="-1.079341430664" Y="-4.845350097656" />
                  <Point X="-1.24641796875" Y="-4.802362792969" />
                  <Point X="-1.214963012695" Y="-4.563438964844" />
                  <Point X="-1.214201171875" Y="-4.5479296875" />
                  <Point X="-1.216508666992" Y="-4.516224121094" />
                  <Point X="-1.268964233398" Y="-4.252512207031" />
                  <Point X="-1.277036132812" Y="-4.211932617188" />
                  <Point X="-1.287937988281" Y="-4.182966796875" />
                  <Point X="-1.304010009766" Y="-4.15512890625" />
                  <Point X="-1.323644775391" Y="-4.131203613281" />
                  <Point X="-1.525798339844" Y="-3.953919433594" />
                  <Point X="-1.556905395508" Y="-3.926639160156" />
                  <Point X="-1.583188354492" Y="-3.910295410156" />
                  <Point X="-1.612885742188" Y="-3.897994384766" />
                  <Point X="-1.64302746582" Y="-3.890966308594" />
                  <Point X="-1.911330322266" Y="-3.873380859375" />
                  <Point X="-1.952616699219" Y="-3.870674804688" />
                  <Point X="-1.98341796875" Y="-3.873708496094" />
                  <Point X="-2.014466674805" Y="-3.882028076172" />
                  <Point X="-2.042657836914" Y="-3.894801513672" />
                  <Point X="-2.266222167969" Y="-4.044182861328" />
                  <Point X="-2.300624267578" Y="-4.067169677734" />
                  <Point X="-2.312785644531" Y="-4.076821533203" />
                  <Point X="-2.335102539062" Y="-4.099461914062" />
                  <Point X="-2.365093994141" Y="-4.138547363281" />
                  <Point X="-2.480148925781" Y="-4.288490234375" />
                  <Point X="-2.74939453125" Y="-4.121779785156" />
                  <Point X="-2.801711181641" Y="-4.089386474609" />
                  <Point X="-3.104721923828" Y="-3.856078125" />
                  <Point X="-3.068919677734" Y="-3.794066894531" />
                  <Point X="-2.423761230469" Y="-2.676619628906" />
                  <Point X="-2.412858886719" Y="-2.647651855469" />
                  <Point X="-2.406588134766" Y="-2.616124267578" />
                  <Point X="-2.405575927734" Y="-2.585189697266" />
                  <Point X="-2.414561523438" Y="-2.555571533203" />
                  <Point X="-2.428780029297" Y="-2.526741943359" />
                  <Point X="-2.446805664062" Y="-2.501587890625" />
                  <Point X="-2.461840576172" Y="-2.486552734375" />
                  <Point X="-2.464154296875" Y="-2.484239257812" />
                  <Point X="-2.489315673828" Y="-2.466209472656" />
                  <Point X="-2.518144042969" Y="-2.451994140625" />
                  <Point X="-2.547760742188" Y="-2.443010986328" />
                  <Point X="-2.578693359375" Y="-2.444024169922" />
                  <Point X="-2.610218505859" Y="-2.450295410156" />
                  <Point X="-2.63918359375" Y="-2.461197265625" />
                  <Point X="-2.811135498047" Y="-2.560473876953" />
                  <Point X="-3.818024414062" Y="-3.141801269531" />
                  <Point X="-4.041528808594" Y="-2.848162353516" />
                  <Point X="-4.082858154297" Y="-2.793864013672" />
                  <Point X="-4.306142089844" Y="-2.419449951172" />
                  <Point X="-4.234772460938" Y="-2.364686035156" />
                  <Point X="-3.105954589844" Y="-1.498513549805" />
                  <Point X="-3.084577880859" Y="-1.475594482422" />
                  <Point X="-3.06661328125" Y="-1.448464477539" />
                  <Point X="-3.053856689453" Y="-1.419833618164" />
                  <Point X="-3.0471796875" Y="-1.394053344727" />
                  <Point X="-3.046152099609" Y="-1.390086303711" />
                  <Point X="-3.043347900391" Y="-1.359659179688" />
                  <Point X="-3.045556396484" Y="-1.327987792969" />
                  <Point X="-3.052557373047" Y="-1.29824206543" />
                  <Point X="-3.068639892578" Y="-1.2722578125" />
                  <Point X="-3.089472900391" Y="-1.24830078125" />
                  <Point X="-3.11297265625" Y="-1.228766967773" />
                  <Point X="-3.135923583984" Y="-1.215259033203" />
                  <Point X="-3.139455322266" Y="-1.213180541992" />
                  <Point X="-3.168718505859" Y="-1.201956542969" />
                  <Point X="-3.200605957031" Y="-1.195474853516" />
                  <Point X="-3.231929199219" Y="-1.194383789062" />
                  <Point X="-3.449002441406" Y="-1.222962036133" />
                  <Point X="-4.732102050781" Y="-1.391885253906" />
                  <Point X="-4.817911621094" Y="-1.055944213867" />
                  <Point X="-4.834076660156" Y="-0.992658508301" />
                  <Point X="-4.892424316406" Y="-0.584698181152" />
                  <Point X="-4.818770019531" Y="-0.564962524414" />
                  <Point X="-3.532875976562" Y="-0.220408340454" />
                  <Point X="-3.517493652344" Y="-0.214827758789" />
                  <Point X="-3.487728759766" Y="-0.199469451904" />
                  <Point X="-3.463677001953" Y="-0.182776153564" />
                  <Point X="-3.459975830078" Y="-0.180207565308" />
                  <Point X="-3.437520996094" Y="-0.15832409668" />
                  <Point X="-3.418276367188" Y="-0.132067840576" />
                  <Point X="-3.404168212891" Y="-0.104066764832" />
                  <Point X="-3.396150878906" Y="-0.078234916687" />
                  <Point X="-3.394917236328" Y="-0.07425983429" />
                  <Point X="-3.390647705078" Y="-0.046100284576" />
                  <Point X="-3.390647705078" Y="-0.016459869385" />
                  <Point X="-3.394917236328" Y="0.011699831009" />
                  <Point X="-3.402934570312" Y="0.037531673431" />
                  <Point X="-3.404168212891" Y="0.041506759644" />
                  <Point X="-3.418276367188" Y="0.069507843018" />
                  <Point X="-3.437520996094" Y="0.095763938904" />
                  <Point X="-3.459976318359" Y="0.117647712708" />
                  <Point X="-3.484028076172" Y="0.134340866089" />
                  <Point X="-3.495167480469" Y="0.140993835449" />
                  <Point X="-3.514436279297" Y="0.15078263855" />
                  <Point X="-3.532876220703" Y="0.157848342896" />
                  <Point X="-3.730748779297" Y="0.210867965698" />
                  <Point X="-4.89181640625" Y="0.521975158691" />
                  <Point X="-4.834905273438" Y="0.906575744629" />
                  <Point X="-4.824487304688" Y="0.976979858398" />
                  <Point X="-4.703551757812" Y="1.423268066406" />
                  <Point X="-4.687426269531" Y="1.421145019531" />
                  <Point X="-3.765666259766" Y="1.29979284668" />
                  <Point X="-3.744986083984" Y="1.299341552734" />
                  <Point X="-3.723424804688" Y="1.301228149414" />
                  <Point X="-3.703137939453" Y="1.305263549805" />
                  <Point X="-3.649903564453" Y="1.322048339844" />
                  <Point X="-3.641711914063" Y="1.324631103516" />
                  <Point X="-3.622779785156" Y="1.332961303711" />
                  <Point X="-3.60403515625" Y="1.343783203125" />
                  <Point X="-3.587353515625" Y="1.356014648438" />
                  <Point X="-3.57371484375" Y="1.371566772461" />
                  <Point X="-3.561300292969" Y="1.38929675293" />
                  <Point X="-3.551351318359" Y="1.407431152344" />
                  <Point X="-3.529990722656" Y="1.45899987793" />
                  <Point X="-3.526703857422" Y="1.466935302734" />
                  <Point X="-3.520915527344" Y="1.486794311523" />
                  <Point X="-3.517157226562" Y="1.50810925293" />
                  <Point X="-3.5158046875" Y="1.528749511719" />
                  <Point X="-3.518951171875" Y="1.549193115234" />
                  <Point X="-3.524552978516" Y="1.570099609375" />
                  <Point X="-3.532049804688" Y="1.589377685547" />
                  <Point X="-3.557823486328" Y="1.638888671875" />
                  <Point X="-3.561789550781" Y="1.646507446289" />
                  <Point X="-3.573281738281" Y="1.663706665039" />
                  <Point X="-3.587194335938" Y="1.680286743164" />
                  <Point X="-3.602135986328" Y="1.694590332031" />
                  <Point X="-3.715636230469" Y="1.781682006836" />
                  <Point X="-4.351859863281" Y="2.269874267578" />
                  <Point X="-4.121629882812" Y="2.664313720703" />
                  <Point X="-4.081153808594" Y="2.733659667969" />
                  <Point X="-3.750504882813" Y="3.158661865234" />
                  <Point X="-3.206656738281" Y="2.844670898438" />
                  <Point X="-3.187723876953" Y="2.836340087891" />
                  <Point X="-3.167081298828" Y="2.829831542969" />
                  <Point X="-3.146794433594" Y="2.825796386719" />
                  <Point X="-3.072653808594" Y="2.819309814453" />
                  <Point X="-3.061245117188" Y="2.818311767578" />
                  <Point X="-3.040564453125" Y="2.818762939453" />
                  <Point X="-3.019105957031" Y="2.821588134766" />
                  <Point X="-2.999013916016" Y="2.826504882812" />
                  <Point X="-2.980462402344" Y="2.835653564453" />
                  <Point X="-2.962208251953" Y="2.847282958984" />
                  <Point X="-2.946077392578" Y="2.860229492188" />
                  <Point X="-2.893451660156" Y="2.912854980469" />
                  <Point X="-2.885353759766" Y="2.920952880859" />
                  <Point X="-2.872406982422" Y="2.937083984375" />
                  <Point X="-2.860777587891" Y="2.955338134766" />
                  <Point X="-2.85162890625" Y="2.973889648438" />
                  <Point X="-2.846712158203" Y="2.993981689453" />
                  <Point X="-2.843886962891" Y="3.015440185547" />
                  <Point X="-2.843435791016" Y="3.036120605469" />
                  <Point X="-2.849922363281" Y="3.110261474609" />
                  <Point X="-2.850920410156" Y="3.121670166016" />
                  <Point X="-2.854955566406" Y="3.141957275391" />
                  <Point X="-2.861464111328" Y="3.162599853516" />
                  <Point X="-2.869794921875" Y="3.181532958984" />
                  <Point X="-2.920090332031" Y="3.268646972656" />
                  <Point X="-3.183332763672" Y="3.724596435547" />
                  <Point X="-2.771117431641" Y="4.040637939453" />
                  <Point X="-2.700625732422" Y="4.09468359375" />
                  <Point X="-2.167036865234" Y="4.391134277344" />
                  <Point X="-2.04319543457" Y="4.229740722656" />
                  <Point X="-2.028891967773" Y="4.214799316406" />
                  <Point X="-2.01231237793" Y="4.200887207031" />
                  <Point X="-1.995113769531" Y="4.189395019531" />
                  <Point X="-1.912595581055" Y="4.146438476563" />
                  <Point X="-1.899897705078" Y="4.139828125" />
                  <Point X="-1.880619873047" Y="4.132331542969" />
                  <Point X="-1.859713500977" Y="4.126729492187" />
                  <Point X="-1.839270507812" Y="4.123582519531" />
                  <Point X="-1.818630981445" Y="4.124935058594" />
                  <Point X="-1.797315795898" Y="4.128692871094" />
                  <Point X="-1.777454223633" Y="4.134481445312" />
                  <Point X="-1.691505981445" Y="4.170083007812" />
                  <Point X="-1.678280029297" Y="4.175561035156" />
                  <Point X="-1.660146240234" Y="4.185509765625" />
                  <Point X="-1.642416259766" Y="4.197924316406" />
                  <Point X="-1.626864746094" Y="4.2115625" />
                  <Point X="-1.614633666992" Y="4.228243652344" />
                  <Point X="-1.603811523438" Y="4.246987792969" />
                  <Point X="-1.59548034668" Y="4.265920898438" />
                  <Point X="-1.567505859375" Y="4.35464453125" />
                  <Point X="-1.563201049805" Y="4.368297363281" />
                  <Point X="-1.559165649414" Y="4.388584960938" />
                  <Point X="-1.557279174805" Y="4.410146972656" />
                  <Point X="-1.557730224609" Y="4.430826660156" />
                  <Point X="-1.563448364258" Y="4.474259277344" />
                  <Point X="-1.584201904297" Y="4.631897949219" />
                  <Point X="-1.040891601562" Y="4.784223144531" />
                  <Point X="-0.949634643555" Y="4.80980859375" />
                  <Point X="-0.29471081543" Y="4.886457519531" />
                  <Point X="-0.133903366089" Y="4.286315917969" />
                  <Point X="-0.121129745483" Y="4.258124023438" />
                  <Point X="-0.103271453857" Y="4.231397460938" />
                  <Point X="-0.082114509583" Y="4.20880859375" />
                  <Point X="-0.054819351196" Y="4.19421875" />
                  <Point X="-0.024381277084" Y="4.183886230469" />
                  <Point X="0.006155910492" Y="4.178844238281" />
                  <Point X="0.036693107605" Y="4.183886230469" />
                  <Point X="0.06713117981" Y="4.19421875" />
                  <Point X="0.094426467896" Y="4.20880859375" />
                  <Point X="0.115583587646" Y="4.231397460938" />
                  <Point X="0.133441726685" Y="4.258124023438" />
                  <Point X="0.146215194702" Y="4.286315429688" />
                  <Point X="0.171986022949" Y="4.382493164062" />
                  <Point X="0.307419311523" Y="4.8879375" />
                  <Point X="0.764353210449" Y="4.840084472656" />
                  <Point X="0.84404095459" Y="4.831738769531" />
                  <Point X="1.401623779297" Y="4.69712109375" />
                  <Point X="1.481029907227" Y="4.677949707031" />
                  <Point X="1.843763183594" Y="4.546384277344" />
                  <Point X="1.894645751953" Y="4.527928710938" />
                  <Point X="2.245584472656" Y="4.363806152344" />
                  <Point X="2.294571533203" Y="4.340896484375" />
                  <Point X="2.633619628906" Y="4.143366210938" />
                  <Point X="2.680978759766" Y="4.115774902344" />
                  <Point X="2.943260253906" Y="3.929254638672" />
                  <Point X="2.894303710938" Y="3.844459472656" />
                  <Point X="2.147581054688" Y="2.551097900391" />
                  <Point X="2.142076171875" Y="2.539931884766" />
                  <Point X="2.133076904297" Y="2.516057128906" />
                  <Point X="2.114470458984" Y="2.446477783203" />
                  <Point X="2.112187744141" Y="2.435281494141" />
                  <Point X="2.107986572266" Y="2.405672363281" />
                  <Point X="2.107727783203" Y="2.380953613281" />
                  <Point X="2.114982910156" Y="2.320787109375" />
                  <Point X="2.116099121094" Y="2.311528808594" />
                  <Point X="2.121441650391" Y="2.289605957031" />
                  <Point X="2.129708007812" Y="2.267516357422" />
                  <Point X="2.140071044922" Y="2.247470947266" />
                  <Point X="2.177300048828" Y="2.192604980469" />
                  <Point X="2.184423339844" Y="2.183380371094" />
                  <Point X="2.203452636719" Y="2.161637451172" />
                  <Point X="2.221599121094" Y="2.145592285156" />
                  <Point X="2.276465087891" Y="2.10836328125" />
                  <Point X="2.284907714844" Y="2.102634765625" />
                  <Point X="2.304953125" Y="2.092271728516" />
                  <Point X="2.327040771484" Y="2.084006103516" />
                  <Point X="2.348963623047" Y="2.078663574219" />
                  <Point X="2.409130126953" Y="2.071408447266" />
                  <Point X="2.421250244141" Y="2.070728027344" />
                  <Point X="2.449412109375" Y="2.070949462891" />
                  <Point X="2.473206787109" Y="2.074171386719" />
                  <Point X="2.542786376953" Y="2.092777832031" />
                  <Point X="2.549235351562" Y="2.09475" />
                  <Point X="2.572024658203" Y="2.102614501953" />
                  <Point X="2.588533935547" Y="2.110145019531" />
                  <Point X="2.787555419922" Y="2.225050292969" />
                  <Point X="3.967326416016" Y="2.906191162109" />
                  <Point X="4.095179443359" Y="2.728504638672" />
                  <Point X="4.1232734375" Y="2.689460693359" />
                  <Point X="4.26219921875" Y="2.459884033203" />
                  <Point X="4.2139140625" Y="2.422833496094" />
                  <Point X="3.230783691406" Y="1.668451293945" />
                  <Point X="3.221424560547" Y="1.660241455078" />
                  <Point X="3.203973632812" Y="1.641627807617" />
                  <Point X="3.153897216797" Y="1.576299072266" />
                  <Point X="3.147796630859" Y="1.567320678711" />
                  <Point X="3.131621337891" Y="1.540316040039" />
                  <Point X="3.121630126953" Y="1.51708605957" />
                  <Point X="3.1029765625" Y="1.450385498047" />
                  <Point X="3.100106201172" Y="1.440121582031" />
                  <Point X="3.096652587891" Y="1.417822021484" />
                  <Point X="3.095836669922" Y="1.394251342773" />
                  <Point X="3.097739746094" Y="1.371767578125" />
                  <Point X="3.113052246094" Y="1.2975546875" />
                  <Point X="3.115925537109" Y="1.286835693359" />
                  <Point X="3.125479980469" Y="1.258038818359" />
                  <Point X="3.136282714844" Y="1.235740234375" />
                  <Point X="3.177931396484" Y="1.172436035156" />
                  <Point X="3.184340332031" Y="1.162694824219" />
                  <Point X="3.198893798828" Y="1.145450073242" />
                  <Point X="3.216137451172" Y="1.129360595703" />
                  <Point X="3.234347412109" Y="1.116034667969" />
                  <Point X="3.294702148438" Y="1.082060302734" />
                  <Point X="3.305131835938" Y="1.077000976562" />
                  <Point X="3.332394775391" Y="1.065775146484" />
                  <Point X="3.356118408203" Y="1.059438598633" />
                  <Point X="3.437721923828" Y="1.048653564453" />
                  <Point X="3.444019775391" Y="1.048033813477" />
                  <Point X="3.469654296875" Y="1.046370849609" />
                  <Point X="3.488203613281" Y="1.04698449707" />
                  <Point X="3.677260742188" Y="1.071874267578" />
                  <Point X="4.776839355469" Y="1.21663671875" />
                  <Point X="4.833870605469" Y="0.98236907959" />
                  <Point X="4.845936035156" Y="0.93280871582" />
                  <Point X="4.890864746094" Y="0.644238586426" />
                  <Point X="4.843268066406" Y="0.631485107422" />
                  <Point X="3.716579833984" Y="0.329589904785" />
                  <Point X="3.704790283203" Y="0.325586151123" />
                  <Point X="3.681545898438" Y="0.315067962646" />
                  <Point X="3.601372802734" Y="0.26872668457" />
                  <Point X="3.592717529297" Y="0.263074157715" />
                  <Point X="3.566068603516" Y="0.243522628784" />
                  <Point X="3.547530761719" Y="0.225576446533" />
                  <Point X="3.499427001953" Y="0.164280914307" />
                  <Point X="3.492024902344" Y="0.154848800659" />
                  <Point X="3.480301025391" Y="0.135569152832" />
                  <Point X="3.47052734375" Y="0.114105987549" />
                  <Point X="3.463680908203" Y="0.092604568481" />
                  <Point X="3.447646240234" Y="0.008877895355" />
                  <Point X="3.446237304688" Y="-0.001617196321" />
                  <Point X="3.443769775391" Y="-0.033311336517" />
                  <Point X="3.445178710938" Y="-0.058554229736" />
                  <Point X="3.461213378906" Y="-0.142280761719" />
                  <Point X="3.463680908203" Y="-0.15516456604" />
                  <Point X="3.47052734375" Y="-0.176665985107" />
                  <Point X="3.480301025391" Y="-0.198129150391" />
                  <Point X="3.492024902344" Y="-0.217408813477" />
                  <Point X="3.540128662109" Y="-0.278704498291" />
                  <Point X="3.547474609375" Y="-0.287015777588" />
                  <Point X="3.569188720703" Y="-0.308868286133" />
                  <Point X="3.589035644531" Y="-0.324155548096" />
                  <Point X="3.669208740234" Y="-0.370497009277" />
                  <Point X="3.674460693359" Y="-0.373316680908" />
                  <Point X="3.698878417969" Y="-0.385455352783" />
                  <Point X="3.716579833984" Y="-0.392150054932" />
                  <Point X="3.889953857422" Y="-0.438605377197" />
                  <Point X="4.891472167969" Y="-0.706961425781" />
                  <Point X="4.861759277344" Y="-0.904041931152" />
                  <Point X="4.855022460938" Y="-0.948725952148" />
                  <Point X="4.801173828125" Y="-1.18469909668" />
                  <Point X="4.731023925781" Y="-1.175463623047" />
                  <Point X="3.424382080078" Y="-1.003440979004" />
                  <Point X="3.40803515625" Y="-1.002710266113" />
                  <Point X="3.374658447266" Y="-1.005508850098" />
                  <Point X="3.217307128906" Y="-1.039709838867" />
                  <Point X="3.193094238281" Y="-1.04497253418" />
                  <Point X="3.163973876953" Y="-1.056597167969" />
                  <Point X="3.136147460938" Y="-1.073489379883" />
                  <Point X="3.112397460938" Y="-1.093960083008" />
                  <Point X="3.017288574219" Y="-1.208346435547" />
                  <Point X="3.002653320312" Y="-1.225948242188" />
                  <Point X="2.987932617188" Y="-1.250330444336" />
                  <Point X="2.976589355469" Y="-1.277715454102" />
                  <Point X="2.969757568359" Y="-1.305365356445" />
                  <Point X="2.956126220703" Y="-1.453500732422" />
                  <Point X="2.954028564453" Y="-1.476295654297" />
                  <Point X="2.956347167969" Y="-1.507563964844" />
                  <Point X="2.964078613281" Y="-1.539184692383" />
                  <Point X="2.976450195312" Y="-1.567996337891" />
                  <Point X="3.063530761719" Y="-1.703444335938" />
                  <Point X="3.076930419922" Y="-1.724286865234" />
                  <Point X="3.086932373047" Y="-1.737238037109" />
                  <Point X="3.110628417969" Y="-1.760909301758" />
                  <Point X="3.271520751953" Y="-1.884366088867" />
                  <Point X="4.213122070312" Y="-2.6068828125" />
                  <Point X="4.143799804688" Y="-2.719056884766" />
                  <Point X="4.124806152344" Y="-2.749791748047" />
                  <Point X="4.028981445312" Y="-2.885945068359" />
                  <Point X="3.9646953125" Y="-2.848829589844" />
                  <Point X="2.800954589844" Y="-2.176943115234" />
                  <Point X="2.786131591797" Y="-2.170012451172" />
                  <Point X="2.754224609375" Y="-2.159825195312" />
                  <Point X="2.566951660156" Y="-2.12600390625" />
                  <Point X="2.538134277344" Y="-2.120799560547" />
                  <Point X="2.506781982422" Y="-2.120395263672" />
                  <Point X="2.474609130859" Y="-2.125353027344" />
                  <Point X="2.444833251953" Y="-2.135176757812" />
                  <Point X="2.289255371094" Y="-2.217056152344" />
                  <Point X="2.265315185547" Y="-2.229655761719" />
                  <Point X="2.242384521484" Y="-2.246548828125" />
                  <Point X="2.221424560547" Y="-2.267508789062" />
                  <Point X="2.204531494141" Y="-2.290439453125" />
                  <Point X="2.122651855469" Y="-2.446017333984" />
                  <Point X="2.110052490234" Y="-2.469957519531" />
                  <Point X="2.100228759766" Y="-2.499733886719" />
                  <Point X="2.095271240234" Y="-2.531906494141" />
                  <Point X="2.095675537109" Y="-2.563258300781" />
                  <Point X="2.129496826172" Y="-2.75053125" />
                  <Point X="2.134701171875" Y="-2.779348632812" />
                  <Point X="2.138985595703" Y="-2.795141845703" />
                  <Point X="2.151819091797" Y="-2.826078613281" />
                  <Point X="2.255208496094" Y="-3.005154296875" />
                  <Point X="2.861283203125" Y="-4.054906494141" />
                  <Point X="2.804387207031" Y="-4.095545898438" />
                  <Point X="2.781848876953" Y="-4.11164453125" />
                  <Point X="2.701764892578" Y="-4.163481445312" />
                  <Point X="2.646587890625" Y="-4.091572998047" />
                  <Point X="1.758546264648" Y="-2.934255126953" />
                  <Point X="1.747504394531" Y="-2.922179931641" />
                  <Point X="1.721924316406" Y="-2.900557373047" />
                  <Point X="1.53722253418" Y="-2.781811279297" />
                  <Point X="1.50880078125" Y="-2.763538574219" />
                  <Point X="1.479989868164" Y="-2.751167236328" />
                  <Point X="1.448368774414" Y="-2.743435546875" />
                  <Point X="1.417099609375" Y="-2.741116699219" />
                  <Point X="1.215096923828" Y="-2.759705322266" />
                  <Point X="1.184012695312" Y="-2.762565673828" />
                  <Point X="1.156363891602" Y="-2.769397216797" />
                  <Point X="1.128978759766" Y="-2.780740234375" />
                  <Point X="1.104595825195" Y="-2.7954609375" />
                  <Point X="0.948614624023" Y="-2.925154296875" />
                  <Point X="0.924612182617" Y="-2.945111328125" />
                  <Point X="0.904141418457" Y="-2.968861572266" />
                  <Point X="0.887248779297" Y="-2.996688476562" />
                  <Point X="0.875624267578" Y="-3.025808837891" />
                  <Point X="0.828986633301" Y="-3.240378662109" />
                  <Point X="0.821809997559" Y="-3.273396728516" />
                  <Point X="0.81972454834" Y="-3.289627441406" />
                  <Point X="0.81974230957" Y="-3.323120117188" />
                  <Point X="0.849042114258" Y="-3.545673828125" />
                  <Point X="1.022065307617" Y="-4.859915039062" />
                  <Point X="0.996996887207" Y="-4.86541015625" />
                  <Point X="0.975669128418" Y="-4.870084472656" />
                  <Point X="0.929315429688" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.02554284668" Y="-4.75901953125" />
                  <Point X="-1.058436401367" Y="-4.752634765625" />
                  <Point X="-1.141246337891" Y="-4.731328613281" />
                  <Point X="-1.120775634766" Y="-4.575838867188" />
                  <Point X="-1.120077514648" Y="-4.568100097656" />
                  <Point X="-1.119451782227" Y="-4.541033691406" />
                  <Point X="-1.121759277344" Y="-4.509328125" />
                  <Point X="-1.123334106445" Y="-4.497690429688" />
                  <Point X="-1.175789672852" Y="-4.233978515625" />
                  <Point X="-1.183861450195" Y="-4.193398925781" />
                  <Point X="-1.188125" Y="-4.178469238281" />
                  <Point X="-1.199026855469" Y="-4.149503417969" />
                  <Point X="-1.205665283203" Y="-4.135467285156" />
                  <Point X="-1.221737304688" Y="-4.107629394531" />
                  <Point X="-1.230573730469" Y="-4.094861816406" />
                  <Point X="-1.250208496094" Y="-4.070936523438" />
                  <Point X="-1.261006958008" Y="-4.059778808594" />
                  <Point X="-1.463160400391" Y="-3.882494628906" />
                  <Point X="-1.494267456055" Y="-3.855214355469" />
                  <Point X="-1.506739013672" Y="-3.84596484375" />
                  <Point X="-1.533021972656" Y="-3.82962109375" />
                  <Point X="-1.546833618164" Y="-3.822526855469" />
                  <Point X="-1.576530883789" Y="-3.810225830078" />
                  <Point X="-1.591313476562" Y="-3.805476074219" />
                  <Point X="-1.621455200195" Y="-3.798447998047" />
                  <Point X="-1.636814208984" Y="-3.796169677734" />
                  <Point X="-1.905116943359" Y="-3.778584228516" />
                  <Point X="-1.946403442383" Y="-3.775878173828" />
                  <Point X="-1.961928344727" Y="-3.776132324219" />
                  <Point X="-1.992729736328" Y="-3.779166015625" />
                  <Point X="-2.008006103516" Y="-3.781945556641" />
                  <Point X="-2.03905480957" Y="-3.790265136719" />
                  <Point X="-2.053674316406" Y="-3.795496337891" />
                  <Point X="-2.081865478516" Y="-3.808269775391" />
                  <Point X="-2.095437011719" Y="-3.815812011719" />
                  <Point X="-2.319001464844" Y="-3.965193359375" />
                  <Point X="-2.353403564453" Y="-3.988180175781" />
                  <Point X="-2.359681640625" Y="-3.992757080078" />
                  <Point X="-2.380442382812" Y="-4.010131347656" />
                  <Point X="-2.402759277344" Y="-4.032771728516" />
                  <Point X="-2.410470947266" Y="-4.041629394531" />
                  <Point X="-2.440462402344" Y="-4.08071484375" />
                  <Point X="-2.503202636719" Y="-4.162479492188" />
                  <Point X="-2.699383300781" Y="-4.041009277344" />
                  <Point X="-2.747587402344" Y="-4.011162353516" />
                  <Point X="-2.980862792969" Y="-3.831548095703" />
                  <Point X="-2.341488769531" Y="-2.724119628906" />
                  <Point X="-2.334849853516" Y="-2.710082519531" />
                  <Point X="-2.323947509766" Y="-2.681114746094" />
                  <Point X="-2.319684082031" Y="-2.666184082031" />
                  <Point X="-2.313413330078" Y="-2.634656494141" />
                  <Point X="-2.311638916016" Y="-2.619231201172" />
                  <Point X="-2.310626708984" Y="-2.588296630859" />
                  <Point X="-2.314667480469" Y="-2.557609863281" />
                  <Point X="-2.323653076172" Y="-2.527991699219" />
                  <Point X="-2.329360107422" Y="-2.513551025391" />
                  <Point X="-2.343578613281" Y="-2.484721435547" />
                  <Point X="-2.351560302734" Y="-2.471405517578" />
                  <Point X="-2.3695859375" Y="-2.446251464844" />
                  <Point X="-2.379629882812" Y="-2.434413330078" />
                  <Point X="-2.394664794922" Y="-2.419378173828" />
                  <Point X="-2.408820068359" Y="-2.407018066406" />
                  <Point X="-2.433981445312" Y="-2.38898828125" />
                  <Point X="-2.447301269531" Y="-2.381005126953" />
                  <Point X="-2.476129638672" Y="-2.366789794922" />
                  <Point X="-2.490569824219" Y="-2.361083984375" />
                  <Point X="-2.520186523438" Y="-2.352100830078" />
                  <Point X="-2.550870849609" Y="-2.348062011719" />
                  <Point X="-2.581803466797" Y="-2.349075195312" />
                  <Point X="-2.597228271484" Y="-2.350849853516" />
                  <Point X="-2.628753417969" Y="-2.35712109375" />
                  <Point X="-2.643682617188" Y="-2.361384521484" />
                  <Point X="-2.672647705078" Y="-2.372286376953" />
                  <Point X="-2.68668359375" Y="-2.378924804688" />
                  <Point X="-2.858635498047" Y="-2.478201416016" />
                  <Point X="-3.793089599609" Y="-3.017708496094" />
                  <Point X="-3.965935546875" Y="-2.790624267578" />
                  <Point X="-4.004013916016" Y="-2.740596923828" />
                  <Point X="-4.181265136719" Y="-2.443373291016" />
                  <Point X="-4.176939941406" Y="-2.440054443359" />
                  <Point X="-3.048122314453" Y="-1.573882080078" />
                  <Point X="-3.036482421875" Y="-1.563310424805" />
                  <Point X="-3.015105712891" Y="-1.540391235352" />
                  <Point X="-3.005368896484" Y="-1.528043945312" />
                  <Point X="-2.987404296875" Y="-1.5009140625" />
                  <Point X="-2.979836914062" Y="-1.487128051758" />
                  <Point X="-2.967080322266" Y="-1.458497192383" />
                  <Point X="-2.961891113281" Y="-1.44365234375" />
                  <Point X="-2.955214111328" Y="-1.417872070312" />
                  <Point X="-2.951552978516" Y="-1.3988046875" />
                  <Point X="-2.948748779297" Y="-1.368377563477" />
                  <Point X="-2.948578125" Y="-1.35305078125" />
                  <Point X="-2.950786621094" Y="-1.321379394531" />
                  <Point X="-2.953083251953" Y="-1.306223266602" />
                  <Point X="-2.960084228516" Y="-1.276477539062" />
                  <Point X="-2.971778076172" Y="-1.248244995117" />
                  <Point X="-2.987860595703" Y="-1.222260742188" />
                  <Point X="-2.996953613281" Y="-1.209919555664" />
                  <Point X="-3.017786621094" Y="-1.185962402344" />
                  <Point X="-3.028745849609" Y="-1.175244384766" />
                  <Point X="-3.052245605469" Y="-1.155710693359" />
                  <Point X="-3.064786132812" Y="-1.146894775391" />
                  <Point X="-3.087737060547" Y="-1.13338684082" />
                  <Point X="-3.105434326172" Y="-1.124481201172" />
                  <Point X="-3.134697509766" Y="-1.113257202148" />
                  <Point X="-3.149795166016" Y="-1.108860351562" />
                  <Point X="-3.181682617188" Y="-1.102378662109" />
                  <Point X="-3.197298828125" Y="-1.100532470703" />
                  <Point X="-3.228622070312" Y="-1.09944140625" />
                  <Point X="-3.244329101562" Y="-1.100196533203" />
                  <Point X="-3.46140234375" Y="-1.128774780273" />
                  <Point X="-4.660920898438" Y="-1.286694335938" />
                  <Point X="-4.725866699219" Y="-1.032433227539" />
                  <Point X="-4.740761230469" Y="-0.974120727539" />
                  <Point X="-4.786452148438" Y="-0.65465423584" />
                  <Point X="-3.508288085938" Y="-0.312171325684" />
                  <Point X="-3.500477050781" Y="-0.30971295166" />
                  <Point X="-3.473932128906" Y="-0.299251556396" />
                  <Point X="-3.444167236328" Y="-0.283893310547" />
                  <Point X="-3.433561523438" Y="-0.277513824463" />
                  <Point X="-3.409509765625" Y="-0.260820495605" />
                  <Point X="-3.393671875" Y="-0.248242675781" />
                  <Point X="-3.371217041016" Y="-0.226359298706" />
                  <Point X="-3.360898681641" Y="-0.214484771729" />
                  <Point X="-3.341654052734" Y="-0.18822845459" />
                  <Point X="-3.333436523438" Y="-0.174813751221" />
                  <Point X="-3.319328369141" Y="-0.146812759399" />
                  <Point X="-3.313437744141" Y="-0.132226455688" />
                  <Point X="-3.305420410156" Y="-0.106394607544" />
                  <Point X="-3.300990722656" Y="-0.088500854492" />
                  <Point X="-3.296721191406" Y="-0.060341423035" />
                  <Point X="-3.295647705078" Y="-0.046100227356" />
                  <Point X="-3.295647705078" Y="-0.016459917068" />
                  <Point X="-3.296721191406" Y="-0.002218870878" />
                  <Point X="-3.300990722656" Y="0.025940858841" />
                  <Point X="-3.304186767578" Y="0.039859542847" />
                  <Point X="-3.312204101562" Y="0.065691390991" />
                  <Point X="-3.319328369141" Y="0.084252754211" />
                  <Point X="-3.333436523438" Y="0.112253753662" />
                  <Point X="-3.341654296875" Y="0.125668754578" />
                  <Point X="-3.360898925781" Y="0.15192477417" />
                  <Point X="-3.371217285156" Y="0.163799301147" />
                  <Point X="-3.393672607422" Y="0.185683135986" />
                  <Point X="-3.405809326172" Y="0.19569229126" />
                  <Point X="-3.429861083984" Y="0.212385467529" />
                  <Point X="-3.452140136719" Y="0.225691223145" />
                  <Point X="-3.471408935547" Y="0.235479980469" />
                  <Point X="-3.480444580078" Y="0.239493240356" />
                  <Point X="-3.508288574219" Y="0.249611343384" />
                  <Point X="-3.706161132812" Y="0.30263092041" />
                  <Point X="-4.785446289062" Y="0.591824645996" />
                  <Point X="-4.740928710938" Y="0.892669494629" />
                  <Point X="-4.731330078125" Y="0.957537109375" />
                  <Point X="-4.633586914062" Y="1.318237182617" />
                  <Point X="-3.778066650391" Y="1.20560559082" />
                  <Point X="-3.767739013672" Y="1.204815429688" />
                  <Point X="-3.747058837891" Y="1.204364135742" />
                  <Point X="-3.736705322266" Y="1.204703125" />
                  <Point X="-3.715144042969" Y="1.20658972168" />
                  <Point X="-3.704890869141" Y="1.208053588867" />
                  <Point X="-3.684604003906" Y="1.212088989258" />
                  <Point X="-3.674570800781" Y="1.214660522461" />
                  <Point X="-3.621336425781" Y="1.2314453125" />
                  <Point X="-3.603451416016" Y="1.237676269531" />
                  <Point X="-3.584519287109" Y="1.246006469727" />
                  <Point X="-3.575280761719" Y="1.250688232422" />
                  <Point X="-3.556536132812" Y="1.261510009766" />
                  <Point X="-3.547860839844" Y="1.267171020508" />
                  <Point X="-3.531179199219" Y="1.27940234375" />
                  <Point X="-3.515928222656" Y="1.293377197266" />
                  <Point X="-3.502289550781" Y="1.308929443359" />
                  <Point X="-3.495895263672" Y="1.317077392578" />
                  <Point X="-3.483480712891" Y="1.334807373047" />
                  <Point X="-3.478011474609" Y="1.343602416992" />
                  <Point X="-3.4680625" Y="1.361736694336" />
                  <Point X="-3.463582763672" Y="1.371076049805" />
                  <Point X="-3.442222167969" Y="1.422644775391" />
                  <Point X="-3.435499023438" Y="1.44035168457" />
                  <Point X="-3.429710693359" Y="1.460210693359" />
                  <Point X="-3.427358642578" Y="1.470298095703" />
                  <Point X="-3.423600341797" Y="1.491613037109" />
                  <Point X="-3.422360595703" Y="1.501897338867" />
                  <Point X="-3.421008056641" Y="1.522537597656" />
                  <Point X="-3.421910400391" Y="1.543200805664" />
                  <Point X="-3.425056884766" Y="1.56364440918" />
                  <Point X="-3.427188232422" Y="1.573780639648" />
                  <Point X="-3.432790039062" Y="1.594687133789" />
                  <Point X="-3.436012207031" Y="1.60453125" />
                  <Point X="-3.443509033203" Y="1.623809326172" />
                  <Point X="-3.447783691406" Y="1.633243530273" />
                  <Point X="-3.473557373047" Y="1.682754638672" />
                  <Point X="-3.482800048828" Y="1.699286743164" />
                  <Point X="-3.494292236328" Y="1.716485961914" />
                  <Point X="-3.500508056641" Y="1.724772216797" />
                  <Point X="-3.514420654297" Y="1.741352294922" />
                  <Point X="-3.521500488281" Y="1.748911132812" />
                  <Point X="-3.536442138672" Y="1.76321472168" />
                  <Point X="-3.544303710938" Y="1.769958984375" />
                  <Point X="-3.657803955078" Y="1.85705065918" />
                  <Point X="-4.227614257812" Y="2.294282226563" />
                  <Point X="-4.039583496094" Y="2.616424316406" />
                  <Point X="-4.00229296875" Y="2.6803125" />
                  <Point X="-3.726338623047" Y="3.035012695312" />
                  <Point X="-3.254156738281" Y="2.7623984375" />
                  <Point X="-3.244918212891" Y="2.757716552734" />
                  <Point X="-3.225985351562" Y="2.749385742188" />
                  <Point X="-3.216290771484" Y="2.745736816406" />
                  <Point X="-3.195648193359" Y="2.739228271484" />
                  <Point X="-3.185614257812" Y="2.736656738281" />
                  <Point X="-3.165327392578" Y="2.732621582031" />
                  <Point X="-3.155074462891" Y="2.731157958984" />
                  <Point X="-3.080933837891" Y="2.724671386719" />
                  <Point X="-3.059173095703" Y="2.723334472656" />
                  <Point X="-3.038492431641" Y="2.723785644531" />
                  <Point X="-3.028163818359" Y="2.724575683594" />
                  <Point X="-3.006705322266" Y="2.727400878906" />
                  <Point X="-2.996524658203" Y="2.729310791016" />
                  <Point X="-2.976432617188" Y="2.734227539062" />
                  <Point X="-2.95699609375" Y="2.741302246094" />
                  <Point X="-2.938444580078" Y="2.750450927734" />
                  <Point X="-2.929418212891" Y="2.755531738281" />
                  <Point X="-2.9111640625" Y="2.767161132812" />
                  <Point X="-2.902745117188" Y="2.773194335938" />
                  <Point X="-2.886614257812" Y="2.786140869141" />
                  <Point X="-2.87890234375" Y="2.793054199219" />
                  <Point X="-2.826276611328" Y="2.8456796875" />
                  <Point X="-2.811265136719" Y="2.861489501953" />
                  <Point X="-2.798318359375" Y="2.877620605469" />
                  <Point X="-2.79228515625" Y="2.886039794922" />
                  <Point X="-2.780655761719" Y="2.904293945312" />
                  <Point X="-2.775574951172" Y="2.9133203125" />
                  <Point X="-2.766426269531" Y="2.931871826172" />
                  <Point X="-2.7593515625" Y="2.951308349609" />
                  <Point X="-2.754434814453" Y="2.971400390625" />
                  <Point X="-2.752524902344" Y="2.981581054688" />
                  <Point X="-2.749699707031" Y="3.003039550781" />
                  <Point X="-2.748909667969" Y="3.013368164062" />
                  <Point X="-2.748458496094" Y="3.034048583984" />
                  <Point X="-2.748797363281" Y="3.044400390625" />
                  <Point X="-2.755283935547" Y="3.118541259766" />
                  <Point X="-2.757745605469" Y="3.140202880859" />
                  <Point X="-2.761780761719" Y="3.160489990234" />
                  <Point X="-2.764352294922" Y="3.170524169922" />
                  <Point X="-2.770860839844" Y="3.191166748047" />
                  <Point X="-2.774509521484" Y="3.200860839844" />
                  <Point X="-2.782840332031" Y="3.219793945312" />
                  <Point X="-2.787522460938" Y="3.229032958984" />
                  <Point X="-2.837817871094" Y="3.316146972656" />
                  <Point X="-3.059387207031" Y="3.699916259766" />
                  <Point X="-2.713315429688" Y="3.96524609375" />
                  <Point X="-2.648374511719" Y="4.015035888672" />
                  <Point X="-2.192525390625" Y="4.268296386719" />
                  <Point X="-2.118563964844" Y="4.171908203125" />
                  <Point X="-2.111819580078" Y="4.164046386719" />
                  <Point X="-2.097516113281" Y="4.149104980469" />
                  <Point X="-2.089957275391" Y="4.142025390625" />
                  <Point X="-2.073377685547" Y="4.12811328125" />
                  <Point X="-2.065093017578" Y="4.1218984375" />
                  <Point X="-2.04789440918" Y="4.11040625" />
                  <Point X="-2.038980224609" Y="4.10512890625" />
                  <Point X="-1.956461914063" Y="4.062172607422" />
                  <Point X="-1.934328735352" Y="4.051287109375" />
                  <Point X="-1.91505090332" Y="4.043790527344" />
                  <Point X="-1.905208496094" Y="4.040568847656" />
                  <Point X="-1.884302124023" Y="4.034966796875" />
                  <Point X="-1.874167480469" Y="4.032835449219" />
                  <Point X="-1.853724487305" Y="4.029688476562" />
                  <Point X="-1.833058349609" Y="4.028785888672" />
                  <Point X="-1.812418823242" Y="4.030138427734" />
                  <Point X="-1.802137084961" Y="4.031377929688" />
                  <Point X="-1.780821899414" Y="4.035135742188" />
                  <Point X="-1.770734375" Y="4.037487548828" />
                  <Point X="-1.750872680664" Y="4.043276123047" />
                  <Point X="-1.741098754883" Y="4.046713134766" />
                  <Point X="-1.655150512695" Y="4.082314697266" />
                  <Point X="-1.632585449219" Y="4.092272460938" />
                  <Point X="-1.614451660156" Y="4.102221191406" />
                  <Point X="-1.605656860352" Y="4.107689941406" />
                  <Point X="-1.587926879883" Y="4.120104492188" />
                  <Point X="-1.579778686523" Y="4.126499023438" />
                  <Point X="-1.564227172852" Y="4.140137207031" />
                  <Point X="-1.550252441406" Y="4.155388183594" />
                  <Point X="-1.538021362305" Y="4.172069335938" />
                  <Point X="-1.532361694336" Y="4.180743164062" />
                  <Point X="-1.521539550781" Y="4.199487304687" />
                  <Point X="-1.516857666016" Y="4.208725097656" />
                  <Point X="-1.508526489258" Y="4.227658203125" />
                  <Point X="-1.504877197266" Y="4.237354003906" />
                  <Point X="-1.476902709961" Y="4.326077636719" />
                  <Point X="-1.470026489258" Y="4.349764160156" />
                  <Point X="-1.465990966797" Y="4.370051757812" />
                  <Point X="-1.46452722168" Y="4.380305175781" />
                  <Point X="-1.46264074707" Y="4.4018671875" />
                  <Point X="-1.462301757812" Y="4.41221875" />
                  <Point X="-1.462752807617" Y="4.4328984375" />
                  <Point X="-1.46354284668" Y="4.443227050781" />
                  <Point X="-1.469261230469" Y="4.486659667969" />
                  <Point X="-1.479266113281" Y="4.562654785156" />
                  <Point X="-1.015245849609" Y="4.69275" />
                  <Point X="-0.931175476074" Y="4.716320800781" />
                  <Point X="-0.365222015381" Y="4.782557128906" />
                  <Point X="-0.22566633606" Y="4.261728027344" />
                  <Point X="-0.220435394287" Y="4.247108886719" />
                  <Point X="-0.207661712646" Y="4.218916992188" />
                  <Point X="-0.200119094849" Y="4.205344726562" />
                  <Point X="-0.182260864258" Y="4.178618164062" />
                  <Point X="-0.17260824585" Y="4.166456054688" />
                  <Point X="-0.151451339722" Y="4.1438671875" />
                  <Point X="-0.126897789001" Y="4.125026367188" />
                  <Point X="-0.099602752686" Y="4.110436523438" />
                  <Point X="-0.085356651306" Y="4.104260742188" />
                  <Point X="-0.054918533325" Y="4.093927978516" />
                  <Point X="-0.039857234955" Y="4.090155273438" />
                  <Point X="-0.009319986343" Y="4.08511328125" />
                  <Point X="0.021631772995" Y="4.08511328125" />
                  <Point X="0.052169025421" Y="4.090155273438" />
                  <Point X="0.067230316162" Y="4.093927978516" />
                  <Point X="0.097668441772" Y="4.104260742188" />
                  <Point X="0.111914390564" Y="4.110436523438" />
                  <Point X="0.139209732056" Y="4.125026367188" />
                  <Point X="0.163762969971" Y="4.143866699219" />
                  <Point X="0.184920181274" Y="4.166455566406" />
                  <Point X="0.1945730896" Y="4.178618164062" />
                  <Point X="0.21243132019" Y="4.205344726562" />
                  <Point X="0.219973648071" Y="4.218916503906" />
                  <Point X="0.232747192383" Y="4.247107910156" />
                  <Point X="0.237978118896" Y="4.261727539062" />
                  <Point X="0.263749023438" Y="4.357905273438" />
                  <Point X="0.378190216064" Y="4.785006347656" />
                  <Point X="0.754458312988" Y="4.745601074219" />
                  <Point X="0.827876037598" Y="4.737912109375" />
                  <Point X="1.379328491211" Y="4.604774414062" />
                  <Point X="1.453599609375" Y="4.586842773438" />
                  <Point X="1.81137109375" Y="4.457077148438" />
                  <Point X="1.85825793457" Y="4.440070800781" />
                  <Point X="2.205339599609" Y="4.277751953125" />
                  <Point X="2.250448974609" Y="4.256655761719" />
                  <Point X="2.585796630859" Y="4.061281005859" />
                  <Point X="2.629435058594" Y="4.035857421875" />
                  <Point X="2.817780029297" Y="3.901916748047" />
                  <Point X="2.81203125" Y="3.891959472656" />
                  <Point X="2.06530859375" Y="2.598597900391" />
                  <Point X="2.062373291016" Y="2.59310546875" />
                  <Point X="2.053181640625" Y="2.573439453125" />
                  <Point X="2.044182373047" Y="2.549564697266" />
                  <Point X="2.041301635742" Y="2.540599121094" />
                  <Point X="2.022695068359" Y="2.471019775391" />
                  <Point X="2.018129760742" Y="2.448627197266" />
                  <Point X="2.013928588867" Y="2.419018066406" />
                  <Point X="2.012991821289" Y="2.406666992188" />
                  <Point X="2.012733032227" Y="2.381948242188" />
                  <Point X="2.013410888672" Y="2.369580566406" />
                  <Point X="2.020666137695" Y="2.3094140625" />
                  <Point X="2.023800292969" Y="2.289035888672" />
                  <Point X="2.029142822266" Y="2.267113037109" />
                  <Point X="2.032467529297" Y="2.256310058594" />
                  <Point X="2.040733886719" Y="2.234220458984" />
                  <Point X="2.045318237305" Y="2.223888671875" />
                  <Point X="2.055681396484" Y="2.203843261719" />
                  <Point X="2.061459960938" Y="2.194129638672" />
                  <Point X="2.098688964844" Y="2.139263671875" />
                  <Point X="2.112935546875" Y="2.120814453125" />
                  <Point X="2.13196484375" Y="2.099071533203" />
                  <Point X="2.140524658203" Y="2.090468261719" />
                  <Point X="2.158671142578" Y="2.074423095703" />
                  <Point X="2.1682578125" Y="2.066981201172" />
                  <Point X="2.223123779297" Y="2.029752075195" />
                  <Point X="2.241280029297" Y="2.018244995117" />
                  <Point X="2.261325439453" Y="2.007881958008" />
                  <Point X="2.271657226562" Y="2.003297607422" />
                  <Point X="2.293744873047" Y="1.995031982422" />
                  <Point X="2.304547851562" Y="1.991707275391" />
                  <Point X="2.326470703125" Y="1.986364746094" />
                  <Point X="2.337590576172" Y="1.984346801758" />
                  <Point X="2.397757080078" Y="1.977091674805" />
                  <Point X="2.421997314453" Y="1.975730957031" />
                  <Point X="2.450159179688" Y="1.975952392578" />
                  <Point X="2.462159179688" Y="1.97680859375" />
                  <Point X="2.485953857422" Y="1.980030395508" />
                  <Point X="2.497748535156" Y="1.982396118164" />
                  <Point X="2.567328125" Y="2.001002685547" />
                  <Point X="2.580226074219" Y="2.004947143555" />
                  <Point X="2.603015380859" Y="2.012811523438" />
                  <Point X="2.611449951172" Y="2.016181640625" />
                  <Point X="2.636033935547" Y="2.027872558594" />
                  <Point X="2.835055419922" Y="2.142777832031" />
                  <Point X="3.940404785156" Y="2.780951416016" />
                  <Point X="4.018066894531" Y="2.673018798828" />
                  <Point X="4.043953613281" Y="2.637042236328" />
                  <Point X="4.136885253906" Y="2.483472167969" />
                  <Point X="3.172951416016" Y="1.743819824219" />
                  <Point X="3.16813671875" Y="1.739868164062" />
                  <Point X="3.152119628906" Y="1.725217285156" />
                  <Point X="3.134668701172" Y="1.706603515625" />
                  <Point X="3.128575927734" Y="1.699422241211" />
                  <Point X="3.078499511719" Y="1.634093505859" />
                  <Point X="3.066298339844" Y="1.61613684082" />
                  <Point X="3.050123046875" Y="1.589131958008" />
                  <Point X="3.044350830078" Y="1.577851074219" />
                  <Point X="3.034359619141" Y="1.55462097168" />
                  <Point X="3.030140625" Y="1.54267199707" />
                  <Point X="3.011487060547" Y="1.475971557617" />
                  <Point X="3.006225341797" Y="1.454661254883" />
                  <Point X="3.002771728516" Y="1.432361694336" />
                  <Point X="3.001709472656" Y="1.421108520508" />
                  <Point X="3.000893554688" Y="1.397537719727" />
                  <Point X="3.001175048828" Y="1.386239013672" />
                  <Point X="3.003078125" Y="1.363755249023" />
                  <Point X="3.004699707031" Y="1.35257043457" />
                  <Point X="3.020012207031" Y="1.278357543945" />
                  <Point X="3.025758789062" Y="1.256919555664" />
                  <Point X="3.035313232422" Y="1.228122680664" />
                  <Point X="3.039984619141" Y="1.216619873047" />
                  <Point X="3.050787353516" Y="1.194321166992" />
                  <Point X="3.056918701172" Y="1.183525756836" />
                  <Point X="3.098567382812" Y="1.120221435547" />
                  <Point X="3.111739257813" Y="1.101424194336" />
                  <Point X="3.126292724609" Y="1.084179443359" />
                  <Point X="3.134083496094" Y="1.075990600586" />
                  <Point X="3.151327148438" Y="1.059901123047" />
                  <Point X="3.160034667969" Y="1.052695800781" />
                  <Point X="3.178244628906" Y="1.039369750977" />
                  <Point X="3.187746826172" Y="1.033249511719" />
                  <Point X="3.2481015625" Y="0.99927520752" />
                  <Point X="3.2689609375" Y="0.98915637207" />
                  <Point X="3.296223876953" Y="0.977930603027" />
                  <Point X="3.307879882812" Y="0.973992675781" />
                  <Point X="3.331603515625" Y="0.96765612793" />
                  <Point X="3.343671142578" Y="0.965257507324" />
                  <Point X="3.425274658203" Y="0.95447253418" />
                  <Point X="3.437869873047" Y="0.953233032227" />
                  <Point X="3.463504394531" Y="0.951570068359" />
                  <Point X="3.472795410156" Y="0.951422790527" />
                  <Point X="3.500603515625" Y="0.952797241211" />
                  <Point X="3.689660644531" Y="0.977687011719" />
                  <Point X="4.704703613281" Y="1.111320068359" />
                  <Point X="4.74156640625" Y="0.959898193359" />
                  <Point X="4.75268359375" Y="0.914233276367" />
                  <Point X="4.78387109375" Y="0.713920898438" />
                  <Point X="3.691991943359" Y="0.421352844238" />
                  <Point X="3.68603125" Y="0.419544250488" />
                  <Point X="3.665625488281" Y="0.412137329102" />
                  <Point X="3.642381103516" Y="0.401619140625" />
                  <Point X="3.634004882812" Y="0.397316650391" />
                  <Point X="3.553831787109" Y="0.35097543335" />
                  <Point X="3.536521240234" Y="0.339670288086" />
                  <Point X="3.509872314453" Y="0.320118774414" />
                  <Point X="3.499991455078" Y="0.31177822876" />
                  <Point X="3.481453613281" Y="0.293832000732" />
                  <Point X="3.472796875" Y="0.284226501465" />
                  <Point X="3.424693115234" Y="0.222930999756" />
                  <Point X="3.410854492188" Y="0.204208236694" />
                  <Point X="3.399130615234" Y="0.184928573608" />
                  <Point X="3.393843017578" Y="0.174939483643" />
                  <Point X="3.384069335938" Y="0.153476394653" />
                  <Point X="3.380005615234" Y="0.142929672241" />
                  <Point X="3.373159179688" Y="0.121428245544" />
                  <Point X="3.370376464844" Y="0.110473548889" />
                  <Point X="3.354341796875" Y="0.026746841431" />
                  <Point X="3.351523925781" Y="0.005756680012" />
                  <Point X="3.349056396484" Y="-0.025937450409" />
                  <Point X="3.348917480469" Y="-0.038605472565" />
                  <Point X="3.350326416016" Y="-0.06384847641" />
                  <Point X="3.351874267578" Y="-0.076423164368" />
                  <Point X="3.367908935547" Y="-0.160149734497" />
                  <Point X="3.373159179688" Y="-0.183988250732" />
                  <Point X="3.380005615234" Y="-0.2054896698" />
                  <Point X="3.384069335938" Y="-0.216036392212" />
                  <Point X="3.393843017578" Y="-0.237499481201" />
                  <Point X="3.399130615234" Y="-0.247488571167" />
                  <Point X="3.410854492188" Y="-0.266768218994" />
                  <Point X="3.417290771484" Y="-0.276058807373" />
                  <Point X="3.46539453125" Y="-0.33735446167" />
                  <Point X="3.480086425781" Y="-0.353977172852" />
                  <Point X="3.501800537109" Y="-0.375829650879" />
                  <Point X="3.511217529297" Y="-0.384130096436" />
                  <Point X="3.531064453125" Y="-0.399417449951" />
                  <Point X="3.541494384766" Y="-0.406404205322" />
                  <Point X="3.621667480469" Y="-0.452745574951" />
                  <Point X="3.632171142578" Y="-0.458384918213" />
                  <Point X="3.656588867188" Y="-0.470523529053" />
                  <Point X="3.665272460938" Y="-0.474312683105" />
                  <Point X="3.6919921875" Y="-0.483912963867" />
                  <Point X="3.865366210938" Y="-0.530368347168" />
                  <Point X="4.784876464844" Y="-0.776750488281" />
                  <Point X="4.767820800781" Y="-0.889879272461" />
                  <Point X="4.76161328125" Y="-0.931052124023" />
                  <Point X="4.727801757812" Y="-1.079219726563" />
                  <Point X="3.436782226562" Y="-0.909253723145" />
                  <Point X="3.428624267578" Y="-0.908535766602" />
                  <Point X="3.400097412109" Y="-0.908042480469" />
                  <Point X="3.366720703125" Y="-0.910841003418" />
                  <Point X="3.354480957031" Y="-0.912676391602" />
                  <Point X="3.197129638672" Y="-0.946877319336" />
                  <Point X="3.172916748047" Y="-0.952140075684" />
                  <Point X="3.157873535156" Y="-0.956742736816" />
                  <Point X="3.128753173828" Y="-0.96836730957" />
                  <Point X="3.114676025391" Y="-0.975389282227" />
                  <Point X="3.086849609375" Y="-0.992281494141" />
                  <Point X="3.074124267578" Y="-1.001530273438" />
                  <Point X="3.050374267578" Y="-1.022000976562" />
                  <Point X="3.039349609375" Y="-1.033222900391" />
                  <Point X="2.944240722656" Y="-1.14760925293" />
                  <Point X="2.92960546875" Y="-1.16521105957" />
                  <Point X="2.921326171875" Y="-1.176847167969" />
                  <Point X="2.90660546875" Y="-1.201229370117" />
                  <Point X="2.9001640625" Y="-1.213975463867" />
                  <Point X="2.888820800781" Y="-1.241360473633" />
                  <Point X="2.884362792969" Y="-1.254927978516" />
                  <Point X="2.877531005859" Y="-1.282577758789" />
                  <Point X="2.875157226562" Y="-1.29666027832" />
                  <Point X="2.861525878906" Y="-1.444795654297" />
                  <Point X="2.859428222656" Y="-1.467590576172" />
                  <Point X="2.859288574219" Y="-1.483320800781" />
                  <Point X="2.861607177734" Y="-1.514589111328" />
                  <Point X="2.864065673828" Y="-1.530127441406" />
                  <Point X="2.871797119141" Y="-1.561748046875" />
                  <Point X="2.876785888672" Y="-1.576667724609" />
                  <Point X="2.889157470703" Y="-1.605479370117" />
                  <Point X="2.896540039062" Y="-1.619371337891" />
                  <Point X="2.983620605469" Y="-1.754819213867" />
                  <Point X="2.997020263672" Y="-1.775661743164" />
                  <Point X="3.0017421875" Y="-1.782353393555" />
                  <Point X="3.019792480469" Y="-1.804448364258" />
                  <Point X="3.043488525391" Y="-1.828119628906" />
                  <Point X="3.052796142578" Y="-1.836277954102" />
                  <Point X="3.213688476562" Y="-1.959734619141" />
                  <Point X="4.087170410156" Y="-2.629981445312" />
                  <Point X="4.062986328125" Y="-2.669114990234" />
                  <Point X="4.045483154297" Y="-2.697437988281" />
                  <Point X="4.001274658203" Y="-2.760252197266" />
                  <Point X="2.848454345703" Y="-2.094670654297" />
                  <Point X="2.841191894531" Y="-2.090885253906" />
                  <Point X="2.815026123047" Y="-2.079513183594" />
                  <Point X="2.783119140625" Y="-2.069325927734" />
                  <Point X="2.771108398438" Y="-2.066337646484" />
                  <Point X="2.583835449219" Y="-2.032516235352" />
                  <Point X="2.555018066406" Y="-2.027311889648" />
                  <Point X="2.539359130859" Y="-2.025807495117" />
                  <Point X="2.508006835938" Y="-2.025403198242" />
                  <Point X="2.492313476562" Y="-2.026503540039" />
                  <Point X="2.460140625" Y="-2.031461303711" />
                  <Point X="2.444844482422" Y="-2.035136230469" />
                  <Point X="2.415068603516" Y="-2.044959960938" />
                  <Point X="2.400588867188" Y="-2.051108642578" />
                  <Point X="2.245010986328" Y="-2.132988037109" />
                  <Point X="2.221070800781" Y="-2.145587646484" />
                  <Point X="2.208968261719" Y="-2.153170410156" />
                  <Point X="2.186037597656" Y="-2.170063476562" />
                  <Point X="2.175209472656" Y="-2.179373779297" />
                  <Point X="2.154249511719" Y="-2.200333740234" />
                  <Point X="2.144939208984" Y="-2.211161865234" />
                  <Point X="2.128046142578" Y="-2.234092529297" />
                  <Point X="2.120463378906" Y="-2.246195068359" />
                  <Point X="2.038583862305" Y="-2.401772949219" />
                  <Point X="2.02598449707" Y="-2.425713134766" />
                  <Point X="2.019835571289" Y="-2.440193359375" />
                  <Point X="2.01001184082" Y="-2.469969726563" />
                  <Point X="2.006336914062" Y="-2.485265869141" />
                  <Point X="2.001379272461" Y="-2.517438476562" />
                  <Point X="2.000279174805" Y="-2.533131347656" />
                  <Point X="2.00068347168" Y="-2.564483154297" />
                  <Point X="2.002187866211" Y="-2.580142089844" />
                  <Point X="2.036009155273" Y="-2.767415039062" />
                  <Point X="2.041213500977" Y="-2.796232421875" />
                  <Point X="2.043015014648" Y="-2.804221435547" />
                  <Point X="2.051236083984" Y="-2.83154296875" />
                  <Point X="2.064069580078" Y="-2.862479736328" />
                  <Point X="2.069546630859" Y="-2.873578613281" />
                  <Point X="2.172936035156" Y="-3.052654296875" />
                  <Point X="2.735892822266" Y="-4.027724365234" />
                  <Point X="2.721956542969" Y="-4.033740966797" />
                  <Point X="1.833915039062" Y="-2.876423095703" />
                  <Point X="1.828654052734" Y="-2.870146728516" />
                  <Point X="1.808832275391" Y="-2.849627197266" />
                  <Point X="1.783252197266" Y="-2.828004638672" />
                  <Point X="1.773299072266" Y="-2.820647216797" />
                  <Point X="1.588597412109" Y="-2.701901123047" />
                  <Point X="1.56017565918" Y="-2.683628417969" />
                  <Point X="1.546284057617" Y="-2.676245849609" />
                  <Point X="1.517473144531" Y="-2.663874511719" />
                  <Point X="1.502553710938" Y="-2.658885742188" />
                  <Point X="1.470932617188" Y="-2.651154052734" />
                  <Point X="1.45539440918" Y="-2.648695800781" />
                  <Point X="1.424125244141" Y="-2.646376953125" />
                  <Point X="1.408394287109" Y="-2.646516357422" />
                  <Point X="1.206391601562" Y="-2.665104980469" />
                  <Point X="1.175307373047" Y="-2.667965332031" />
                  <Point X="1.161225097656" Y="-2.670339111328" />
                  <Point X="1.133576293945" Y="-2.677170654297" />
                  <Point X="1.120009765625" Y="-2.681628417969" />
                  <Point X="1.092624633789" Y="-2.692971435547" />
                  <Point X="1.07987890625" Y="-2.699412597656" />
                  <Point X="1.05549597168" Y="-2.714133300781" />
                  <Point X="1.043858764648" Y="-2.722412841797" />
                  <Point X="0.887877502441" Y="-2.852106201172" />
                  <Point X="0.863875061035" Y="-2.872063232422" />
                  <Point X="0.852652832031" Y="-2.883088378906" />
                  <Point X="0.832182128906" Y="-2.906838623047" />
                  <Point X="0.822933654785" Y="-2.919563476563" />
                  <Point X="0.806041015625" Y="-2.947390380859" />
                  <Point X="0.799018859863" Y="-2.961468017578" />
                  <Point X="0.787394348145" Y="-2.990588378906" />
                  <Point X="0.782791748047" Y="-3.005631347656" />
                  <Point X="0.73615423584" Y="-3.220201171875" />
                  <Point X="0.728977478027" Y="-3.253219238281" />
                  <Point X="0.727584594727" Y="-3.261289794922" />
                  <Point X="0.724724487305" Y="-3.289677734375" />
                  <Point X="0.724742370605" Y="-3.323170410156" />
                  <Point X="0.725554992676" Y="-3.335520019531" />
                  <Point X="0.754854858398" Y="-3.558073730469" />
                  <Point X="0.83309173584" Y="-4.152341796875" />
                  <Point X="0.655064941406" Y="-3.487936523438" />
                  <Point X="0.652606140137" Y="-3.480124511719" />
                  <Point X="0.642145141602" Y="-3.453580566406" />
                  <Point X="0.62678704834" Y="-3.423815917969" />
                  <Point X="0.620407592773" Y="-3.413210205078" />
                  <Point X="0.478514312744" Y="-3.208769042969" />
                  <Point X="0.456679840088" Y="-3.177309814453" />
                  <Point X="0.446670837402" Y="-3.165173095703" />
                  <Point X="0.424787139893" Y="-3.142717773438" />
                  <Point X="0.41291217041" Y="-3.132399169922" />
                  <Point X="0.386656890869" Y="-3.113155273438" />
                  <Point X="0.373242950439" Y="-3.104938232422" />
                  <Point X="0.345241790771" Y="-3.090829833984" />
                  <Point X="0.330654754639" Y="-3.084938476562" />
                  <Point X="0.111082992554" Y="-3.016791503906" />
                  <Point X="0.077295509338" Y="-3.006305175781" />
                  <Point X="0.06337638092" Y="-3.003109130859" />
                  <Point X="0.035216796875" Y="-2.99883984375" />
                  <Point X="0.020976644516" Y="-2.997766601562" />
                  <Point X="-0.00866500473" Y="-2.997766601562" />
                  <Point X="-0.022905456543" Y="-2.998840087891" />
                  <Point X="-0.051064739227" Y="-3.003109375" />
                  <Point X="-0.06498387146" Y="-3.006305175781" />
                  <Point X="-0.284555328369" Y="-3.074451904297" />
                  <Point X="-0.318342803955" Y="-3.084938476562" />
                  <Point X="-0.332929992676" Y="-3.090829833984" />
                  <Point X="-0.360932037354" Y="-3.104938720703" />
                  <Point X="-0.374346893311" Y="-3.11315625" />
                  <Point X="-0.400601867676" Y="-3.132400390625" />
                  <Point X="-0.412475952148" Y="-3.142718505859" />
                  <Point X="-0.434358886719" Y="-3.165173095703" />
                  <Point X="-0.444367889404" Y="-3.177309570312" />
                  <Point X="-0.586261169434" Y="-3.381750488281" />
                  <Point X="-0.60809564209" Y="-3.413209960938" />
                  <Point X="-0.612470947266" Y="-3.420133056641" />
                  <Point X="-0.625976745605" Y="-3.445265380859" />
                  <Point X="-0.638777832031" Y="-3.476215576172" />
                  <Point X="-0.642753173828" Y="-3.487936523438" />
                  <Point X="-0.69617388916" Y="-3.687305664062" />
                  <Point X="-0.985425170898" Y="-4.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.833116251439" Y="-3.945307996952" />
                  <Point X="-2.454072352743" Y="-4.098451672768" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.128438332855" Y="-4.634042582662" />
                  <Point X="-0.967296166496" Y="-4.699148243959" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.958207069431" Y="-3.792307225318" />
                  <Point X="-2.391445363721" Y="-4.021293818223" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.119888270547" Y="-4.535036231513" />
                  <Point X="-0.942523717346" Y="-4.606696162539" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.910240265818" Y="-3.709226271392" />
                  <Point X="-2.303061338231" Y="-3.954542481912" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.137719290888" Y="-4.425371231107" />
                  <Point X="-0.917751268195" Y="-4.51424408112" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.862273462205" Y="-3.626145317467" />
                  <Point X="-2.207500637193" Y="-3.890690710736" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.159881062296" Y="-4.313956493693" />
                  <Point X="-0.892978819045" Y="-4.421791999701" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.814306658592" Y="-3.543064363542" />
                  <Point X="-2.111939936155" Y="-3.82683893956" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.18204282886" Y="-4.202541758236" />
                  <Point X="-0.868206369894" Y="-4.329339918281" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.766339854979" Y="-3.459983409617" />
                  <Point X="-1.979548464799" Y="-3.777867765511" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.248239514608" Y="-4.073335760576" />
                  <Point X="-0.843433920743" Y="-4.236887836862" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.875549749648" Y="-2.90937272166" />
                  <Point X="-3.716646224597" Y="-2.973573913159" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.718373051366" Y="-3.376902455691" />
                  <Point X="-1.689138607706" Y="-3.792740163463" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.459890181472" Y="-3.885362539893" />
                  <Point X="-0.818661471593" Y="-4.144435755443" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.988172491776" Y="-2.76140937966" />
                  <Point X="-3.612241005324" Y="-2.913295559304" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.670406247753" Y="-3.293821501766" />
                  <Point X="-0.793889022442" Y="-4.051983674023" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.073189812365" Y="-2.624599351938" />
                  <Point X="-3.507835786052" Y="-2.85301720545" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.62243944414" Y="-3.210740547841" />
                  <Point X="-0.769116573292" Y="-3.959531592604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.153688650801" Y="-2.489614909506" />
                  <Point X="-3.403430566779" Y="-2.792738851596" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.574472640528" Y="-3.127659593915" />
                  <Point X="-0.744344124141" Y="-3.867079511184" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.123758323285" Y="-2.399246746216" />
                  <Point X="-3.299025347507" Y="-2.732460497742" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.526505836915" Y="-3.04457863999" />
                  <Point X="-0.71957167499" Y="-3.774627429765" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.03628616801" Y="-2.332126990423" />
                  <Point X="-3.194620128235" Y="-2.672182143887" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.478539033302" Y="-2.961497686065" />
                  <Point X="-0.69479922724" Y="-3.68217534778" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.948814012735" Y="-2.26500723463" />
                  <Point X="-3.090214908962" Y="-2.611903790033" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.430572229689" Y="-2.87841673214" />
                  <Point X="-0.670026803321" Y="-3.589723256166" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.86134185746" Y="-2.197887478837" />
                  <Point X="-2.98580968969" Y="-2.551625436179" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.382605426076" Y="-2.795335778214" />
                  <Point X="-0.645254379402" Y="-3.497271164553" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.815730082731" Y="-4.087547202792" />
                  <Point X="0.825057500733" Y="-4.091315724284" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.773869702185" Y="-2.130767723044" />
                  <Point X="-2.881404470418" Y="-2.491347082324" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.335678541148" Y="-2.711834669868" />
                  <Point X="-0.606259988488" Y="-3.410565120588" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.784942821999" Y="-3.972647541481" />
                  <Point X="0.81081046507" Y="-3.983098747682" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.68639754691" Y="-2.063647967251" />
                  <Point X="-2.776999337521" Y="-2.431068693572" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.311634243309" Y="-2.619088396222" />
                  <Point X="-0.550720775759" Y="-3.330543618539" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.754155561268" Y="-3.857747880169" />
                  <Point X="0.796563429407" Y="-3.874881771079" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.598925391635" Y="-1.996528211458" />
                  <Point X="-2.670702962325" Y="-2.371554416308" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.331876234307" Y="-2.508449300441" />
                  <Point X="-0.495181464603" Y="-3.250522156256" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.723368300536" Y="-3.742848218857" />
                  <Point X="0.782316393744" Y="-3.766664794476" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.51145323636" Y="-1.929408455665" />
                  <Point X="-0.438974907623" Y="-3.170770278785" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.692581039804" Y="-3.627948557546" />
                  <Point X="0.768069358081" Y="-3.658447817874" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.423981081085" Y="-1.862288699872" />
                  <Point X="-0.355316883363" Y="-3.102109514034" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.661793779073" Y="-3.513048896234" />
                  <Point X="0.753822318212" Y="-3.550230839572" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.668503627047" Y="-1.257008152106" />
                  <Point X="-4.611222144601" Y="-1.280151373269" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.33650892581" Y="-1.79516894408" />
                  <Point X="-0.219865516806" Y="-3.054374617894" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.601750639431" Y="-3.386329092583" />
                  <Point X="0.739575224524" Y="-3.442013839526" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.6976868599" Y="-1.142756560125" />
                  <Point X="-4.419949269056" Y="-1.254969830726" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.249036770535" Y="-1.728049188287" />
                  <Point X="-0.076441063896" Y="-3.009861057741" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.5029249546" Y="-3.243940123571" />
                  <Point X="0.725444704108" Y="-3.333843938138" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.726870077395" Y="-1.028504974348" />
                  <Point X="-4.228676393512" Y="-1.229788288182" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.16156461526" Y="-1.660929432494" />
                  <Point X="0.733055381802" Y="-3.234458050969" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.748910887972" Y="-0.917139108282" />
                  <Point X="-4.037403517967" Y="-1.204606745639" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.074092459985" Y="-1.593809676701" />
                  <Point X="0.7535278498" Y="-3.140268664393" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.612775467893" Y="-3.891453462425" />
                  <Point X="2.670737231001" Y="-3.914871534815" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.764463818827" Y="-0.808394515774" />
                  <Point X="-3.846130642423" Y="-1.179425203095" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.000704341478" Y="-1.520999600688" />
                  <Point X="0.774000252705" Y="-3.046079251518" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.498828690177" Y="-3.742955175323" />
                  <Point X="2.593584473895" Y="-3.781238996995" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.780016749682" Y="-0.699649923265" />
                  <Point X="-3.654857766878" Y="-1.154243660552" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.959679670998" Y="-1.435113842914" />
                  <Point X="0.802232693866" Y="-2.955025097612" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.38488191246" Y="-3.594456888222" />
                  <Point X="2.51643171679" Y="-3.647606459175" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.697066153605" Y="-0.630703338975" />
                  <Point X="-3.463584891334" Y="-1.129062118008" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.949719890956" Y="-1.3366770547" />
                  <Point X="0.859975792156" Y="-2.875894023128" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.270935134744" Y="-3.445958601121" />
                  <Point X="2.439278959685" Y="-3.513973921354" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.544589125624" Y="-0.589847256562" />
                  <Point X="-3.272312053298" Y="-1.10388056031" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.991484568694" Y="-1.217342229026" />
                  <Point X="0.942430553789" Y="-2.806747108719" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.156988357027" Y="-3.297460314019" />
                  <Point X="2.36212620258" Y="-3.380341383534" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.392112097644" Y="-0.548991174149" />
                  <Point X="1.025361606775" Y="-2.737792628507" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.043041579311" Y="-3.148962026918" />
                  <Point X="2.284973445475" Y="-3.246708845714" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.239635069664" Y="-0.508135091736" />
                  <Point X="1.131006384972" Y="-2.678015088966" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.929094801594" Y="-3.000463739817" />
                  <Point X="2.20782068837" Y="-3.113076307893" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.087158041683" Y="-0.467279009323" />
                  <Point X="1.325519389118" Y="-2.654142643353" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.808348498856" Y="-2.849218266284" />
                  <Point X="2.130667908281" Y="-2.979443760787" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.934681013703" Y="-0.42642292691" />
                  <Point X="2.057887857402" Y="-2.84757791096" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.782203985722" Y="-0.385566844497" />
                  <Point X="2.029943140239" Y="-2.733826711798" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.629726957742" Y="-0.344710762085" />
                  <Point X="2.009982360007" Y="-2.623301232542" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.481379058957" Y="-0.302186403187" />
                  <Point X="2.001389995215" Y="-2.51736889127" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.384183054198" Y="-0.238995337602" />
                  <Point X="2.02636130644" Y="-2.424997155344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.325994899985" Y="-0.160044077382" />
                  <Point X="2.070829885702" Y="-2.340502827037" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.298016890426" Y="-0.068887126436" />
                  <Point X="2.115298610364" Y="-2.256008557475" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.779482868205" Y="0.632124781823" />
                  <Point X="-4.471583223653" Y="0.507725250499" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.303226724741" Y="0.035678583813" />
                  <Point X="2.176491634323" Y="-2.178271343439" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.765176527199" Y="0.728805445415" />
                  <Point X="-3.718620762911" Y="0.305969469844" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.374389900822" Y="0.166891173818" />
                  <Point X="2.276713986576" Y="-2.116303001609" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.750870186192" Y="0.825486109007" />
                  <Point X="2.386849204166" Y="-2.058339717349" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.714862281565" Y="-2.594891828871" />
                  <Point X="4.028477548483" Y="-2.721600621528" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.736563916314" Y="0.922166801337" />
                  <Point X="2.570630836274" Y="-2.030131515994" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.123711853011" Y="-2.253590751767" />
                  <Point X="4.080805083644" Y="-2.640281517512" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.715460279613" Y="1.016101179204" />
                  <Point X="3.840573597455" Y="-2.440760896266" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.690435114208" Y="1.108451156629" />
                  <Point X="3.558546613751" Y="-2.224353797902" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.665409948803" Y="1.200801134053" />
                  <Point X="3.276519630048" Y="-2.007946699538" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.640384783397" Y="1.293151111478" />
                  <Point X="3.016515899754" Y="-1.80043757313" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.35959470339" Y="1.282165355755" />
                  <Point X="2.9227098293" Y="-1.66007665997" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.983417108788" Y="1.232640542519" />
                  <Point X="2.865095191502" Y="-1.534338034753" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.68114508509" Y="1.212975518163" />
                  <Point X="2.862799958711" Y="-1.430949899957" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.553146841664" Y="1.263721671512" />
                  <Point X="2.87189038672" Y="-1.332161870722" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.481876356718" Y="1.337387327021" />
                  <Point X="2.890530761446" Y="-1.237232270416" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.441833057687" Y="1.423669584598" />
                  <Point X="2.9387248764" Y="-1.154243156234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.421315828241" Y="1.517840886374" />
                  <Point X="3.002495219491" Y="-1.077547246719" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.44653679433" Y="1.630491618669" />
                  <Point X="3.072152479146" Y="-1.003229805885" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.551168108256" Y="1.775226214093" />
                  <Point X="3.190071577486" Y="-0.948411413587" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.833195288677" Y="1.991633391936" />
                  <Point X="3.355022633284" Y="-0.912595165554" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.115221760837" Y="2.208040283623" />
                  <Point X="3.679413369862" Y="-0.941196729995" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.198506755035" Y="2.344150406052" />
                  <Point X="4.055590683381" Y="-0.990721429667" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.150113817754" Y="2.4270591908" />
                  <Point X="4.431767996901" Y="-1.040246129339" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.101720880473" Y="2.509967975547" />
                  <Point X="4.732362587777" Y="-1.059233426843" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.053327943193" Y="2.592876760295" />
                  <Point X="4.753770183368" Y="-0.965421856339" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.004935228601" Y="2.675785635015" />
                  <Point X="3.472363298894" Y="-0.345239068492" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.058975801954" Y="-0.582245904131" />
                  <Point X="4.770839356082" Y="-0.869857449215" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.944954692834" Y="2.75401272608" />
                  <Point X="3.37999599411" Y="-0.205459454395" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.884304910666" Y="2.831969424047" />
                  <Point X="3.355032202551" Y="-0.092912627354" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.823655128498" Y="2.909926122015" />
                  <Point X="3.352187232716" Y="0.010697615625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.76300534633" Y="2.987882819982" />
                  <Point X="-3.368948771901" Y="2.82867362945" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.119992080724" Y="2.728088597117" />
                  <Point X="3.36954739179" Y="0.106144456629" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.931465122934" Y="2.754379562448" />
                  <Point X="3.404853953607" Y="0.194340480265" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.848596953224" Y="2.823359449152" />
                  <Point X="3.463932973983" Y="0.272931807191" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.783657115395" Y="2.899582852122" />
                  <Point X="3.542570552571" Y="0.34362096366" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.751539286055" Y="2.989067207306" />
                  <Point X="3.647210519806" Y="0.403804473181" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.75297116497" Y="3.092106524495" />
                  <Point X="3.791429460919" Y="0.447997039263" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.775798455287" Y="3.203790149002" />
                  <Point X="3.943906485038" Y="0.488853123236" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.84923441466" Y="3.335921003062" />
                  <Point X="4.096383509157" Y="0.529709207209" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.926387219588" Y="3.469553560204" />
                  <Point X="4.248860533276" Y="0.570565291182" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.003540024516" Y="3.603186117346" />
                  <Point X="3.082837722032" Y="1.144129887401" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.54268905033" Y="0.958337890784" />
                  <Point X="4.401337557396" Y="0.611421375155" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.03521877109" Y="3.718445962318" />
                  <Point X="3.021931229742" Y="1.271198508164" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.733962004783" Y="0.983519401446" />
                  <Point X="4.553814581515" Y="0.652277459128" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.947699026888" Y="3.785546490936" />
                  <Point X="3.001542578562" Y="1.381896858505" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.925234822744" Y="1.008700967255" />
                  <Point X="4.706291605634" Y="0.693133543101" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.860179282685" Y="3.852647019554" />
                  <Point X="3.012584678161" Y="1.479896361233" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.116507640704" Y="1.033882533064" />
                  <Point X="4.775509100098" Y="0.767628660605" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.772659538483" Y="3.919747548172" />
                  <Point X="3.041299152975" Y="1.570755760901" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.307780458665" Y="1.059064098873" />
                  <Point X="4.758485684166" Y="0.876967367649" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.685139879777" Y="3.986848111334" />
                  <Point X="3.092600550416" Y="1.652489451467" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.499053276626" Y="1.084245664681" />
                  <Point X="4.734447611134" Y="0.989140180127" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.586454455349" Y="4.049437412311" />
                  <Point X="2.082560458021" Y="2.163032938495" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.516945993095" Y="1.987529790201" />
                  <Point X="3.156431127003" Y="1.72916102507" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.690326094587" Y="1.10942723049" />
                  <Point X="4.706783089017" Y="1.102778173142" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.479680619907" Y="4.108758783114" />
                  <Point X="2.023767741309" Y="2.289247538489" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.650303044711" Y="2.036110844502" />
                  <Point X="3.242210951015" Y="1.796964527077" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.372906784464" Y="4.168080153917" />
                  <Point X="2.012881262621" Y="2.39610676194" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.754708147203" Y="2.096389245539" />
                  <Point X="3.329683123239" Y="1.864084276022" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.266132949022" Y="4.227401524719" />
                  <Point X="-2.113143653654" Y="4.165589837119" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.787391697384" Y="4.033977503669" />
                  <Point X="2.028386531215" Y="2.492303027344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.859113273885" Y="2.156667636802" />
                  <Point X="3.417155295464" Y="1.931204024966" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.65432869098" Y="4.082677359947" />
                  <Point X="2.057626756323" Y="2.582950010105" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.963518481358" Y="2.216945995424" />
                  <Point X="3.504627467688" Y="1.998323773911" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.558477636641" Y="4.146411820775" />
                  <Point X="2.104496902772" Y="2.666474042285" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.067923688831" Y="2.277224354045" />
                  <Point X="3.592099639913" Y="2.065443522856" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.508189060368" Y="4.228554717655" />
                  <Point X="2.152463712693" Y="2.749554993662" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.172328896304" Y="2.337502712667" />
                  <Point X="3.679571812137" Y="2.132563271801" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.479056918325" Y="4.319245368809" />
                  <Point X="2.200430522614" Y="2.832635945039" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.276734103777" Y="2.397781071288" />
                  <Point X="3.767043984362" Y="2.199683020746" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.462361565543" Y="4.41496080899" />
                  <Point X="2.248397332535" Y="2.915716896415" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.38113931125" Y="2.45805942991" />
                  <Point X="3.854516156586" Y="2.266802769691" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.473926223418" Y="4.522094034619" />
                  <Point X="2.296364142456" Y="2.998797847792" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.485544518723" Y="2.518337788531" />
                  <Point X="3.941988328811" Y="2.333922518636" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.38566837543" Y="4.58889634995" />
                  <Point X="2.344330952377" Y="3.081878799169" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.589949726196" Y="2.578616147153" />
                  <Point X="4.029460501035" Y="2.401042267581" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.235957595001" Y="4.630870068921" />
                  <Point X="-0.205149553284" Y="4.214396586265" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.081245219685" Y="4.098685587044" />
                  <Point X="2.392297762297" Y="3.164959750545" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.694354933669" Y="2.638894505774" />
                  <Point X="4.11693267326" Y="2.468162016526" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.086246814573" Y="4.672843787892" />
                  <Point X="-0.244722279535" Y="4.332845806053" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.180030181365" Y="4.161234672361" />
                  <Point X="2.440264572218" Y="3.248040701922" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.798760141142" Y="2.699172864396" />
                  <Point X="4.073536770268" Y="2.588155899983" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.936536552674" Y="4.714817716363" />
                  <Point X="-0.275509537988" Y="4.447745466444" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.230942742932" Y="4.243125462818" />
                  <Point X="2.488231382139" Y="3.331121653299" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.903165348615" Y="2.759451223017" />
                  <Point X="3.97747915739" Y="2.729426495331" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.74157834724" Y="4.73851028898" />
                  <Point X="-0.306296796441" Y="4.562645126835" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.257565930307" Y="4.334829797458" />
                  <Point X="2.53619819206" Y="3.414202604676" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.54493962671" Y="4.761523889425" />
                  <Point X="-0.337084054894" Y="4.677544787226" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.282338395391" Y="4.42728187244" />
                  <Point X="2.584165001981" Y="3.497283556052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.307110816686" Y="4.519733965113" />
                  <Point X="2.632131811902" Y="3.580364507429" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.331883237982" Y="4.612186057787" />
                  <Point X="2.680098621822" Y="3.663445458806" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.356655659278" Y="4.70463815046" />
                  <Point X="2.728065431743" Y="3.746526410182" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.422934933061" Y="4.780320386177" />
                  <Point X="2.776032241664" Y="3.829607361559" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.765270215871" Y="4.744468754447" />
                  <Point X="2.774525778746" Y="3.932676812641" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.342792947192" Y="4.613595225532" />
                  <Point X="2.305200449598" Y="4.224757354619" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.0001640625" Y="0.001626220703" />
                  <Width Value="9.996463867188" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.818699462891" Y="-4.832732910156" />
                  <Point X="0.471539031982" Y="-3.537112304688" />
                  <Point X="0.464318664551" Y="-3.521544189453" />
                  <Point X="0.322425506592" Y="-3.317103027344" />
                  <Point X="0.300591033936" Y="-3.285643798828" />
                  <Point X="0.274335754395" Y="-3.266399902344" />
                  <Point X="0.054764087677" Y="-3.198252929688" />
                  <Point X="0.020976625443" Y="-3.187766601562" />
                  <Point X="-0.008665060997" Y="-3.187766601562" />
                  <Point X="-0.228236572266" Y="-3.255913330078" />
                  <Point X="-0.262024047852" Y="-3.266399902344" />
                  <Point X="-0.288279144287" Y="-3.285644042969" />
                  <Point X="-0.430172485352" Y="-3.490084960938" />
                  <Point X="-0.452006958008" Y="-3.521544433594" />
                  <Point X="-0.459227142334" Y="-3.537112304688" />
                  <Point X="-0.512647949219" Y="-3.736481445312" />
                  <Point X="-0.84774395752" Y="-4.987077148438" />
                  <Point X="-1.061747070312" Y="-4.945538085938" />
                  <Point X="-1.100245727539" Y="-4.938065429688" />
                  <Point X="-1.351589599609" Y="-4.873396972656" />
                  <Point X="-1.350940185547" Y="-4.868464355469" />
                  <Point X="-1.309150146484" Y="-4.551038574219" />
                  <Point X="-1.309683227539" Y="-4.5347578125" />
                  <Point X="-1.362138793945" Y="-4.271045898438" />
                  <Point X="-1.370210693359" Y="-4.230466308594" />
                  <Point X="-1.386282714844" Y="-4.202628417969" />
                  <Point X="-1.588436279297" Y="-4.025344238281" />
                  <Point X="-1.619543457031" Y="-3.998063964844" />
                  <Point X="-1.649240722656" Y="-3.985762939453" />
                  <Point X="-1.917543579102" Y="-3.968177490234" />
                  <Point X="-1.958829833984" Y="-3.965471435547" />
                  <Point X="-1.989878540039" Y="-3.973791015625" />
                  <Point X="-2.213442871094" Y="-4.123172363281" />
                  <Point X="-2.247844970703" Y="-4.146159179688" />
                  <Point X="-2.259734130859" Y="-4.157294433594" />
                  <Point X="-2.289725585938" Y="-4.196379882812" />
                  <Point X="-2.457095214844" Y="-4.414500488281" />
                  <Point X="-2.799405761719" Y="-4.202550292969" />
                  <Point X="-2.855833740234" Y="-4.167611328125" />
                  <Point X="-3.208662841797" Y="-3.895944580078" />
                  <Point X="-3.228581054688" Y="-3.880608398438" />
                  <Point X="-3.151192138672" Y="-3.746566894531" />
                  <Point X="-2.506033691406" Y="-2.629119628906" />
                  <Point X="-2.499762939453" Y="-2.597592041016" />
                  <Point X="-2.513981445312" Y="-2.568762451172" />
                  <Point X="-2.529016357422" Y="-2.553727294922" />
                  <Point X="-2.531330078125" Y="-2.551413818359" />
                  <Point X="-2.560158447266" Y="-2.537198486328" />
                  <Point X="-2.59168359375" Y="-2.543469726562" />
                  <Point X="-2.763635498047" Y="-2.642746337891" />
                  <Point X="-3.842959228516" Y="-3.265894042969" />
                  <Point X="-4.117122070312" Y="-2.905700439453" />
                  <Point X="-4.161703125" Y="-2.847129882812" />
                  <Point X="-4.414654296875" Y="-2.422968994141" />
                  <Point X="-4.431020019531" Y="-2.395526855469" />
                  <Point X="-4.292604980469" Y="-2.289317382812" />
                  <Point X="-3.163786865234" Y="-1.423144897461" />
                  <Point X="-3.145822265625" Y="-1.396014892578" />
                  <Point X="-3.139145263672" Y="-1.370234619141" />
                  <Point X="-3.138117675781" Y="-1.366267578125" />
                  <Point X="-3.140326171875" Y="-1.334596191406" />
                  <Point X="-3.161159179688" Y="-1.310639160156" />
                  <Point X="-3.184110107422" Y="-1.297131225586" />
                  <Point X="-3.187641845703" Y="-1.295052734375" />
                  <Point X="-3.219529296875" Y="-1.288571044922" />
                  <Point X="-3.436602539062" Y="-1.317149291992" />
                  <Point X="-4.803283691406" Y="-1.497076293945" />
                  <Point X="-4.909956542969" Y="-1.079455200195" />
                  <Point X="-4.927392089844" Y="-1.011195495605" />
                  <Point X="-4.994317382812" Y="-0.543261535645" />
                  <Point X="-4.998395996094" Y="-0.51474206543" />
                  <Point X="-4.843357910156" Y="-0.473199645996" />
                  <Point X="-3.557463867188" Y="-0.128645431519" />
                  <Point X="-3.541896240234" Y="-0.121425384521" />
                  <Point X="-3.517844482422" Y="-0.104731987" />
                  <Point X="-3.514143310547" Y="-0.102163352966" />
                  <Point X="-3.494898681641" Y="-0.075907165527" />
                  <Point X="-3.486881347656" Y="-0.050075233459" />
                  <Point X="-3.485647705078" Y="-0.046100280762" />
                  <Point X="-3.485647705078" Y="-0.016459812164" />
                  <Point X="-3.493665039062" Y="0.009372121811" />
                  <Point X="-3.494898681641" Y="0.013347072601" />
                  <Point X="-3.514143310547" Y="0.03960326004" />
                  <Point X="-3.538195068359" Y="0.05629650116" />
                  <Point X="-3.557463867188" Y="0.066085334778" />
                  <Point X="-3.755336425781" Y="0.119105018616" />
                  <Point X="-4.998186523438" Y="0.452125793457" />
                  <Point X="-4.928881835938" Y="0.920481811523" />
                  <Point X="-4.91764453125" Y="0.996421875" />
                  <Point X="-4.782929199219" Y="1.493563232422" />
                  <Point X="-4.773516601562" Y="1.528298706055" />
                  <Point X="-4.675026367188" Y="1.515332275391" />
                  <Point X="-3.753266357422" Y="1.393980102539" />
                  <Point X="-3.731705078125" Y="1.395866577148" />
                  <Point X="-3.678470703125" Y="1.412651367188" />
                  <Point X="-3.670279052734" Y="1.415234130859" />
                  <Point X="-3.651534423828" Y="1.426056152344" />
                  <Point X="-3.639119873047" Y="1.443786132813" />
                  <Point X="-3.617759277344" Y="1.495354980469" />
                  <Point X="-3.614472412109" Y="1.503290527344" />
                  <Point X="-3.610714111328" Y="1.52460546875" />
                  <Point X="-3.616315917969" Y="1.54551171875" />
                  <Point X="-3.642089599609" Y="1.595022705078" />
                  <Point X="-3.646055664062" Y="1.602641479492" />
                  <Point X="-3.659968261719" Y="1.619221679688" />
                  <Point X="-3.773468505859" Y="1.706313354492" />
                  <Point X="-4.47610546875" Y="2.245466308594" />
                  <Point X="-4.203676269531" Y="2.712203125" />
                  <Point X="-4.160014648438" Y="2.787006591797" />
                  <Point X="-3.803154541016" Y="3.245699707031" />
                  <Point X="-3.774671386719" Y="3.282310791016" />
                  <Point X="-3.725276367188" Y="3.253792724609" />
                  <Point X="-3.159156982422" Y="2.926943359375" />
                  <Point X="-3.138514404297" Y="2.920434814453" />
                  <Point X="-3.064373779297" Y="2.913948242188" />
                  <Point X="-3.052965087891" Y="2.912950195312" />
                  <Point X="-3.031506591797" Y="2.915775390625" />
                  <Point X="-3.013252441406" Y="2.927404785156" />
                  <Point X="-2.960626708984" Y="2.980030273438" />
                  <Point X="-2.952528808594" Y="2.988128173828" />
                  <Point X="-2.940899414062" Y="3.006382324219" />
                  <Point X="-2.93807421875" Y="3.027840820312" />
                  <Point X="-2.944560791016" Y="3.101981689453" />
                  <Point X="-2.945558837891" Y="3.113390380859" />
                  <Point X="-2.952067382812" Y="3.134032958984" />
                  <Point X="-3.002362792969" Y="3.221146972656" />
                  <Point X="-3.307278564453" Y="3.749276611328" />
                  <Point X="-2.828919433594" Y="4.116029785156" />
                  <Point X="-2.752876220703" Y="4.174331542969" />
                  <Point X="-2.190832519531" Y="4.486590820312" />
                  <Point X="-2.141548583984" Y="4.513971679688" />
                  <Point X="-2.141133300781" Y="4.513430664062" />
                  <Point X="-1.967826904297" Y="4.287573242188" />
                  <Point X="-1.951247192383" Y="4.273661132813" />
                  <Point X="-1.868729125977" Y="4.230704589844" />
                  <Point X="-1.85603125" Y="4.224094238281" />
                  <Point X="-1.83512487793" Y="4.2184921875" />
                  <Point X="-1.813809692383" Y="4.22225" />
                  <Point X="-1.727861450195" Y="4.2578515625" />
                  <Point X="-1.714635620117" Y="4.263329589844" />
                  <Point X="-1.696905639648" Y="4.275744140625" />
                  <Point X="-1.686083496094" Y="4.29448828125" />
                  <Point X="-1.658108886719" Y="4.383211914062" />
                  <Point X="-1.653804077148" Y="4.396864746094" />
                  <Point X="-1.651917602539" Y="4.418426757812" />
                  <Point X="-1.657635620117" Y="4.461859375" />
                  <Point X="-1.689137573242" Y="4.701141113281" />
                  <Point X="-1.066537231445" Y="4.875696289062" />
                  <Point X="-0.96809387207" Y="4.903296386719" />
                  <Point X="-0.28672543335" Y="4.983040527344" />
                  <Point X="-0.224199645996" Y="4.990358398438" />
                  <Point X="-0.209614196777" Y="4.935924316406" />
                  <Point X="-0.042140380859" Y="4.310903320312" />
                  <Point X="-0.024282115936" Y="4.284176757813" />
                  <Point X="0.006155934334" Y="4.273844238281" />
                  <Point X="0.036594036102" Y="4.284176757813" />
                  <Point X="0.054452251434" Y="4.310903320312" />
                  <Point X="0.08022303009" Y="4.407081054688" />
                  <Point X="0.236648422241" Y="4.990868652344" />
                  <Point X="0.774248046875" Y="4.934567871094" />
                  <Point X="0.860205932617" Y="4.925565429688" />
                  <Point X="1.423919067383" Y="4.789467773438" />
                  <Point X="1.508459228516" Y="4.769057128906" />
                  <Point X="1.876155395508" Y="4.63569140625" />
                  <Point X="1.931033447266" Y="4.615786621094" />
                  <Point X="2.285829345703" Y="4.449860351562" />
                  <Point X="2.338695800781" Y="4.425136230469" />
                  <Point X="2.681442626953" Y="4.225451171875" />
                  <Point X="2.732523681641" Y="4.19569140625" />
                  <Point X="3.055779052734" Y="3.965810058594" />
                  <Point X="3.068740478516" Y="3.956592773438" />
                  <Point X="2.976576171875" Y="3.796959472656" />
                  <Point X="2.229853515625" Y="2.503597900391" />
                  <Point X="2.224852050781" Y="2.491515136719" />
                  <Point X="2.206245605469" Y="2.421935791016" />
                  <Point X="2.202044433594" Y="2.392326660156" />
                  <Point X="2.209299560547" Y="2.33216015625" />
                  <Point X="2.210415771484" Y="2.322901855469" />
                  <Point X="2.218682128906" Y="2.300812255859" />
                  <Point X="2.255911132812" Y="2.245946289062" />
                  <Point X="2.274940429688" Y="2.224203369141" />
                  <Point X="2.329806396484" Y="2.186974365234" />
                  <Point X="2.338249023438" Y="2.181245849609" />
                  <Point X="2.360336669922" Y="2.172980224609" />
                  <Point X="2.420503173828" Y="2.165725097656" />
                  <Point X="2.448665039062" Y="2.165946533203" />
                  <Point X="2.518244628906" Y="2.184552978516" />
                  <Point X="2.541033935547" Y="2.192417480469" />
                  <Point X="2.740055419922" Y="2.307322753906" />
                  <Point X="3.994248046875" Y="3.031430908203" />
                  <Point X="4.172291992187" Y="2.783990234375" />
                  <Point X="4.20259375" Y="2.741877441406" />
                  <Point X="4.382801757813" Y="2.444081787109" />
                  <Point X="4.387513183594" Y="2.436296142578" />
                  <Point X="4.271746582031" Y="2.347465087891" />
                  <Point X="3.288616210938" Y="1.593082763672" />
                  <Point X="3.279371337891" Y="1.583833374023" />
                  <Point X="3.229294921875" Y="1.518504638672" />
                  <Point X="3.213119628906" Y="1.4915" />
                  <Point X="3.194466064453" Y="1.424799438477" />
                  <Point X="3.191595703125" Y="1.414535522461" />
                  <Point X="3.190779785156" Y="1.39096472168" />
                  <Point X="3.206092285156" Y="1.316751831055" />
                  <Point X="3.215646728516" Y="1.287954833984" />
                  <Point X="3.257295410156" Y="1.224650634766" />
                  <Point X="3.263704345703" Y="1.214909423828" />
                  <Point X="3.280947998047" Y="1.198819824219" />
                  <Point X="3.341302734375" Y="1.164845458984" />
                  <Point X="3.368565673828" Y="1.153619628906" />
                  <Point X="3.450169189453" Y="1.142834594727" />
                  <Point X="3.475803710938" Y="1.14117175293" />
                  <Point X="3.664860839844" Y="1.166061523438" />
                  <Point X="4.848975097656" Y="1.32195324707" />
                  <Point X="4.926174804688" Y="1.004840026855" />
                  <Point X="4.939188476562" Y="0.951385009766" />
                  <Point X="4.995977539062" Y="0.586637023926" />
                  <Point X="4.997858398438" Y="0.574556152344" />
                  <Point X="4.867855957031" Y="0.539722106934" />
                  <Point X="3.741167724609" Y="0.237826919556" />
                  <Point X="3.729086914062" Y="0.232819259644" />
                  <Point X="3.648913818359" Y="0.186477935791" />
                  <Point X="3.622264892578" Y="0.166926422119" />
                  <Point X="3.574161132813" Y="0.105630859375" />
                  <Point X="3.566759033203" Y="0.096198738098" />
                  <Point X="3.556985351562" Y="0.074735580444" />
                  <Point X="3.540950683594" Y="-0.008991053581" />
                  <Point X="3.538483154297" Y="-0.040685245514" />
                  <Point X="3.554517822266" Y="-0.124411880493" />
                  <Point X="3.556985351562" Y="-0.137295669556" />
                  <Point X="3.566759033203" Y="-0.158758834839" />
                  <Point X="3.614862792969" Y="-0.220054397583" />
                  <Point X="3.636576904297" Y="-0.241906967163" />
                  <Point X="3.71675" Y="-0.288248443604" />
                  <Point X="3.741167724609" Y="-0.300387023926" />
                  <Point X="3.914541748047" Y="-0.346842407227" />
                  <Point X="4.998067871094" Y="-0.637172363281" />
                  <Point X="4.955697753906" Y="-0.918204650879" />
                  <Point X="4.948431640625" Y="-0.966399230957" />
                  <Point X="4.875675292969" Y="-1.285228393555" />
                  <Point X="4.874545410156" Y="-1.290178344727" />
                  <Point X="4.718624023438" Y="-1.269650878906" />
                  <Point X="3.411982177734" Y="-1.097628295898" />
                  <Point X="3.3948359375" Y="-1.098341308594" />
                  <Point X="3.237484619141" Y="-1.132542236328" />
                  <Point X="3.213271728516" Y="-1.137805053711" />
                  <Point X="3.1854453125" Y="-1.154697265625" />
                  <Point X="3.090336425781" Y="-1.269083740234" />
                  <Point X="3.075701171875" Y="-1.286685424805" />
                  <Point X="3.064357910156" Y="-1.31407043457" />
                  <Point X="3.0507265625" Y="-1.462205810547" />
                  <Point X="3.04862890625" Y="-1.485000732422" />
                  <Point X="3.056360351562" Y="-1.516621582031" />
                  <Point X="3.143440917969" Y="-1.652069458008" />
                  <Point X="3.156840576172" Y="-1.672912109375" />
                  <Point X="3.168460693359" Y="-1.685540649414" />
                  <Point X="3.329353027344" Y="-1.808997680664" />
                  <Point X="4.33907421875" Y="-2.583783935547" />
                  <Point X="4.22461328125" Y="-2.768998779297" />
                  <Point X="4.204129394531" Y="-2.802145019531" />
                  <Point X="4.056688232422" Y="-3.011638427734" />
                  <Point X="3.9171953125" Y="-2.931102050781" />
                  <Point X="2.753454589844" Y="-2.259215576172" />
                  <Point X="2.737340820313" Y="-2.253312744141" />
                  <Point X="2.550067871094" Y="-2.219491455078" />
                  <Point X="2.521250488281" Y="-2.214287109375" />
                  <Point X="2.489077636719" Y="-2.219244873047" />
                  <Point X="2.333499755859" Y="-2.301124267578" />
                  <Point X="2.309559570312" Y="-2.313723876953" />
                  <Point X="2.288599609375" Y="-2.334683837891" />
                  <Point X="2.206719970703" Y="-2.49026171875" />
                  <Point X="2.194120605469" Y="-2.514201904297" />
                  <Point X="2.189163085938" Y="-2.546374511719" />
                  <Point X="2.222984375" Y="-2.733647460938" />
                  <Point X="2.228188720703" Y="-2.76246484375" />
                  <Point X="2.234091552734" Y="-2.778578613281" />
                  <Point X="2.337480957031" Y="-2.957654296875" />
                  <Point X="2.986673583984" Y="-4.082088623047" />
                  <Point X="2.859604248047" Y="-4.172851074219" />
                  <Point X="2.835296630859" Y="-4.190213378906" />
                  <Point X="2.679775878906" Y="-4.290879394531" />
                  <Point X="2.571219238281" Y="-4.149405273438" />
                  <Point X="1.683177490234" Y="-2.992087402344" />
                  <Point X="1.670549560547" Y="-2.980467529297" />
                  <Point X="1.48584777832" Y="-2.861721435547" />
                  <Point X="1.457426025391" Y="-2.843448730469" />
                  <Point X="1.425804931641" Y="-2.835717041016" />
                  <Point X="1.223802246094" Y="-2.854305664062" />
                  <Point X="1.192718017578" Y="-2.857166015625" />
                  <Point X="1.165332885742" Y="-2.868509033203" />
                  <Point X="1.00935168457" Y="-2.998202392578" />
                  <Point X="0.985349243164" Y="-3.018159423828" />
                  <Point X="0.968456726074" Y="-3.045986328125" />
                  <Point X="0.921819091797" Y="-3.260556152344" />
                  <Point X="0.91464251709" Y="-3.29357421875" />
                  <Point X="0.91392956543" Y="-3.310720214844" />
                  <Point X="0.943229309082" Y="-3.533273925781" />
                  <Point X="1.127642211914" Y="-4.934028320312" />
                  <Point X="1.01733795166" Y="-4.95820703125" />
                  <Point X="0.994335632324" Y="-4.963248535156" />
                  <Point X="0.860200317383" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#204" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.167473804092" Y="4.980145251278" Z="2.2" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="2.2" />
                  <Point X="-0.298693537241" Y="5.063981769904" Z="2.2" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="2.2" />
                  <Point X="-1.08619104069" Y="4.95512000766" Z="2.2" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="2.2" />
                  <Point X="-1.713363905394" Y="4.486613220306" Z="2.2" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="2.2" />
                  <Point X="-1.711950597941" Y="4.4295278149" Z="2.2" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="2.2" />
                  <Point X="-1.753150804782" Y="4.335325806788" Z="2.2" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="2.2" />
                  <Point X="-1.851796909826" Y="4.306334713563" Z="2.2" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="2.2" />
                  <Point X="-2.10762148629" Y="4.575148572634" Z="2.2" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="2.2" />
                  <Point X="-2.221271493599" Y="4.56157817574" Z="2.2" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="2.2" />
                  <Point X="-2.865497500482" Y="4.186989596487" Z="2.2" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="2.2" />
                  <Point X="-3.051820029124" Y="3.227426993309" Z="2.2" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="2.2" />
                  <Point X="-3.000526543691" Y="3.128904197465" Z="2.2" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="2.2" />
                  <Point X="-3.002137998096" Y="3.04666556979" Z="2.2" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="2.2" />
                  <Point X="-3.066172268141" Y="2.995038155482" Z="2.2" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="2.2" />
                  <Point X="-3.706432354988" Y="3.328373919622" Z="2.2" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="2.2" />
                  <Point X="-3.848773961386" Y="3.307682059455" Z="2.2" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="2.2" />
                  <Point X="-4.253014422393" Y="2.768688229611" Z="2.2" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="2.2" />
                  <Point X="-3.810062415468" Y="1.697925821193" Z="2.2" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="2.2" />
                  <Point X="-3.692596234804" Y="1.60321539919" Z="2.2" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="2.2" />
                  <Point X="-3.670109377626" Y="1.54576899532" Z="2.2" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="2.2" />
                  <Point X="-3.699661593614" Y="1.491617284349" Z="2.2" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="2.2" />
                  <Point X="-4.674656079028" Y="1.596184578759" Z="2.2" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="2.2" />
                  <Point X="-4.837344441734" Y="1.537920624915" Z="2.2" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="2.2" />
                  <Point X="-4.984499123684" Y="0.959080936969" Z="2.2" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="2.2" />
                  <Point X="-3.774433222182" Y="0.102088805845" Z="2.2" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="2.2" />
                  <Point X="-3.572859660558" Y="0.046500274567" Z="2.2" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="2.2" />
                  <Point X="-3.547574146509" Y="0.025831948089" Z="2.2" />
                  <Point X="-3.539556741714" Y="0" Z="2.2" />
                  <Point X="-3.540790454626" Y="-0.003975002972" Z="2.2" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="2.2" />
                  <Point X="-3.552508934565" Y="-0.032375708567" Z="2.2" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="2.2" />
                  <Point X="-4.862454566591" Y="-0.393623250911" Z="2.2" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="2.2" />
                  <Point X="-5.049969740346" Y="-0.519060262802" Z="2.2" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="2.2" />
                  <Point X="-4.964557220809" Y="-1.06054926842" Z="2.2" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="2.2" />
                  <Point X="-3.436232044995" Y="-1.335441525766" Z="2.2" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="2.2" />
                  <Point X="-3.215627216935" Y="-1.308941895776" Z="2.2" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="2.2" />
                  <Point X="-3.193703763962" Y="-1.326416860285" Z="2.2" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="2.2" />
                  <Point X="-4.329198833412" Y="-2.218369673595" Z="2.2" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="2.2" />
                  <Point X="-4.463753855142" Y="-2.417298837949" Z="2.2" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="2.2" />
                  <Point X="-4.163064094397" Y="-2.904703649384" Z="2.2" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="2.2" />
                  <Point X="-2.744791550186" Y="-2.65476753255" Z="2.2" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="2.2" />
                  <Point X="-2.570525929116" Y="-2.557804540015" Z="2.2" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="2.2" />
                  <Point X="-3.200649480618" Y="-3.690286832306" Z="2.2" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="2.2" />
                  <Point X="-3.245322434352" Y="-3.904281843611" Z="2.2" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="2.2" />
                  <Point X="-2.831883527809" Y="-4.213780856694" Z="2.2" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="2.2" />
                  <Point X="-2.256213945031" Y="-4.195538068452" Z="2.2" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="2.2" />
                  <Point X="-2.191820343815" Y="-4.133465508485" Z="2.2" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="2.2" />
                  <Point X="-1.926969728308" Y="-3.986790415692" Z="2.2" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="2.2" />
                  <Point X="-1.627559586624" Y="-4.031656308815" Z="2.2" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="2.2" />
                  <Point X="-1.417334144878" Y="-4.249520319371" Z="2.2" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="2.2" />
                  <Point X="-1.406668446767" Y="-4.83065798628" Z="2.2" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="2.2" />
                  <Point X="-1.373665386378" Y="-4.889649189117" Z="2.2" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="2.2" />
                  <Point X="-1.077429867462" Y="-4.963342272766" Z="2.2" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="2.2" />
                  <Point X="-0.470507603143" Y="-3.718142319052" Z="2.2" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="2.2" />
                  <Point X="-0.395252339657" Y="-3.487313813014" Z="2.2" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="2.2" />
                  <Point X="-0.219571554723" Y="-3.272386276078" Z="2.2" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="2.2" />
                  <Point X="0.033787524638" Y="-3.21472576921" Z="2.2" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="2.2" />
                  <Point X="0.275193519775" Y="-3.314331942754" Z="2.2" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="2.2" />
                  <Point X="0.764247257259" Y="-4.814393710763" Z="2.2" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="2.2" />
                  <Point X="0.84171818513" Y="-5.009393924595" Z="2.2" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="2.2" />
                  <Point X="1.02188699602" Y="-4.975767588065" Z="2.2" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="2.2" />
                  <Point X="0.986645551248" Y="-3.495467497598" Z="2.2" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="2.2" />
                  <Point X="0.964522355815" Y="-3.239895833381" Z="2.2" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="2.2" />
                  <Point X="1.035162332847" Y="-3.005368874236" Z="2.2" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="2.2" />
                  <Point X="1.222227787347" Y="-2.872815058549" Z="2.2" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="2.2" />
                  <Point X="1.452652226447" Y="-2.872499161306" Z="2.2" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="2.2" />
                  <Point X="2.525395430985" Y="-4.148563292738" Z="2.2" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="2.2" />
                  <Point X="2.688081790601" Y="-4.309798588238" Z="2.2" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="2.2" />
                  <Point X="2.882510164756" Y="-4.182258098382" Z="2.2" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="2.2" />
                  <Point X="2.374626311751" Y="-2.901374267341" Z="2.2" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="2.2" />
                  <Point X="2.266032486134" Y="-2.693481178094" Z="2.2" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="2.2" />
                  <Point X="2.244810678727" Y="-2.482267976294" Z="2.2" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="2.2" />
                  <Point X="2.350630402078" Y="-2.31409063124" Z="2.2" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="2.2" />
                  <Point X="2.535025625419" Y="-2.237415523357" Z="2.2" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="2.2" />
                  <Point X="3.886039329766" Y="-2.943123188524" Z="2.2" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="2.2" />
                  <Point X="4.088400296685" Y="-3.013427368972" Z="2.2" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="2.2" />
                  <Point X="4.260991065682" Y="-2.764003568699" Z="2.2" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="2.2" />
                  <Point X="3.35363498749" Y="-1.738049944329" Z="2.2" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="2.2" />
                  <Point X="3.179342944704" Y="-1.593750396559" Z="2.2" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="2.2" />
                  <Point X="3.094360091872" Y="-1.435507535862" Z="2.2" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="2.2" />
                  <Point X="3.122626734887" Y="-1.269770535686" Z="2.2" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="2.2" />
                  <Point X="3.241948631253" Y="-1.150121364976" Z="2.2" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="2.2" />
                  <Point X="4.705941582365" Y="-1.287943057677" Z="2.2" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="2.2" />
                  <Point X="4.918266397823" Y="-1.265072431321" Z="2.2" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="2.2" />
                  <Point X="4.998983362637" Y="-0.894378564692" Z="2.2" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="2.2" />
                  <Point X="3.921326754736" Y="-0.26726607279" Z="2.2" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="2.2" />
                  <Point X="3.735615729523" Y="-0.213679647003" Z="2.2" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="2.2" />
                  <Point X="3.648040532608" Y="-0.157906087913" Z="2.2" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="2.2" />
                  <Point X="3.597469329681" Y="-0.083726719387" Z="2.2" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="2.2" />
                  <Point X="3.583902120743" Y="0.012883811795" Z="2.2" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="2.2" />
                  <Point X="3.607338905792" Y="0.106042650662" Z="2.2" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="2.2" />
                  <Point X="3.66777968483" Y="0.174469197239" Z="2.2" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="2.2" />
                  <Point X="4.874641062571" Y="0.522705859978" Z="2.2" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="2.2" />
                  <Point X="5.039226510854" Y="0.62560908265" Z="2.2" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="2.2" />
                  <Point X="4.968600036732" Y="1.047947297137" Z="2.2" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="2.2" />
                  <Point X="3.652178617796" Y="1.246913943841" Z="2.2" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="2.2" />
                  <Point X="3.450564294873" Y="1.223683659425" Z="2.2" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="2.2" />
                  <Point X="3.359673274469" Y="1.239696549832" Z="2.2" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="2.2" />
                  <Point X="3.292909680203" Y="1.283412234451" Z="2.2" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="2.2" />
                  <Point X="3.248904693866" Y="1.358136244856" Z="2.2" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="2.2" />
                  <Point X="3.236462430124" Y="1.442613056087" Z="2.2" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="2.2" />
                  <Point X="3.262821715636" Y="1.519366434285" Z="2.2" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="2.2" />
                  <Point X="4.296028444498" Y="2.339077475562" Z="2.2" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="2.2" />
                  <Point X="4.419422946051" Y="2.501248087094" Z="2.2" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="2.2" />
                  <Point X="4.206721720577" Y="2.844472791155" Z="2.2" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="2.2" />
                  <Point X="2.708899544614" Y="2.381903949757" Z="2.2" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="2.2" />
                  <Point X="2.499171084051" Y="2.264135620395" Z="2.2" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="2.2" />
                  <Point X="2.420333276551" Y="2.24664556385" Z="2.2" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="2.2" />
                  <Point X="2.351724048252" Y="2.259629271092" Z="2.2" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="2.2" />
                  <Point X="2.291129329143" Y="2.305300812131" Z="2.2" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="2.2" />
                  <Point X="2.252784138909" Y="2.369425167282" Z="2.2" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="2.2" />
                  <Point X="2.248392322263" Y="2.440298491831" Z="2.2" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="2.2" />
                  <Point X="3.013721403103" Y="3.803239537341" Z="2.2" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="2.2" />
                  <Point X="3.078600070145" Y="4.037837255393" Z="2.2" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="2.2" />
                  <Point X="2.700456577686" Y="4.29993396314" Z="2.2" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="2.2" />
                  <Point X="2.30085492832" Y="4.526431940512" Z="2.2" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="2.2" />
                  <Point X="1.887048102043" Y="4.713974650699" Z="2.2" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="2.2" />
                  <Point X="1.429497421607" Y="4.869351476594" Z="2.2" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="2.2" />
                  <Point X="0.773300102485" Y="5.015575725289" Z="2.2" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="2.2" />
                  <Point X="0.025770757461" Y="4.451302254673" Z="2.2" />
                  <Point X="0" Y="4.355124473572" Z="2.2" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>