<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#159" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1677" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="0" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000475830078" Y="0.004716064453" />
                  <Width Value="9.783896972656" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.729096801758" Y="-4.131279296875" />
                  <Point X="0.563302062988" Y="-3.512524414063" />
                  <Point X="0.557721191406" Y="-3.497141845703" />
                  <Point X="0.54236315918" Y="-3.467377197266" />
                  <Point X="0.474598632813" Y="-3.369741210938" />
                  <Point X="0.378635528564" Y="-3.231476806641" />
                  <Point X="0.356751586914" Y="-3.209021484375" />
                  <Point X="0.330496398926" Y="-3.189777587891" />
                  <Point X="0.302495330811" Y="-3.175669433594" />
                  <Point X="0.197633651733" Y="-3.143124023438" />
                  <Point X="0.049136188507" Y="-3.097036132812" />
                  <Point X="0.020976638794" Y="-3.092766601562" />
                  <Point X="-0.00866468811" Y="-3.092766601562" />
                  <Point X="-0.036824085236" Y="-3.097035888672" />
                  <Point X="-0.141685913086" Y="-3.129581054688" />
                  <Point X="-0.290183227539" Y="-3.175669189453" />
                  <Point X="-0.31818460083" Y="-3.189777587891" />
                  <Point X="-0.344439788818" Y="-3.209021484375" />
                  <Point X="-0.366323577881" Y="-3.2314765625" />
                  <Point X="-0.434088134766" Y="-3.329112304688" />
                  <Point X="-0.530051086426" Y="-3.467376953125" />
                  <Point X="-0.538189025879" Y="-3.481573730469" />
                  <Point X="-0.550990112305" Y="-3.512524414063" />
                  <Point X="-0.785776489258" Y="-4.388759277344" />
                  <Point X="-0.916584594727" Y="-4.876941894531" />
                  <Point X="-1.079340698242" Y="-4.845350585938" />
                  <Point X="-1.19749987793" Y="-4.814948730469" />
                  <Point X="-1.24641796875" Y="-4.802362792969" />
                  <Point X="-1.234920776367" Y="-4.715032714844" />
                  <Point X="-1.214963012695" Y="-4.563438476562" />
                  <Point X="-1.214201171875" Y="-4.5479296875" />
                  <Point X="-1.216508666992" Y="-4.516224121094" />
                  <Point X="-1.241560180664" Y="-4.390282226563" />
                  <Point X="-1.277036132812" Y="-4.211932617188" />
                  <Point X="-1.287938354492" Y="-4.182966308594" />
                  <Point X="-1.304010375977" Y="-4.15512890625" />
                  <Point X="-1.323644775391" Y="-4.131204101562" />
                  <Point X="-1.420188110352" Y="-4.046537841797" />
                  <Point X="-1.556905517578" Y="-3.926639404297" />
                  <Point X="-1.583188476562" Y="-3.910295410156" />
                  <Point X="-1.612885742188" Y="-3.897994384766" />
                  <Point X="-1.64302734375" Y="-3.890966308594" />
                  <Point X="-1.771161865234" Y="-3.882567871094" />
                  <Point X="-1.952616577148" Y="-3.870674804688" />
                  <Point X="-1.98341796875" Y="-3.873708496094" />
                  <Point X="-2.014466674805" Y="-3.882028076172" />
                  <Point X="-2.042657714844" Y="-3.894801513672" />
                  <Point X="-2.149426513672" Y="-3.966142089844" />
                  <Point X="-2.300624267578" Y="-4.067169677734" />
                  <Point X="-2.312785644531" Y="-4.076821533203" />
                  <Point X="-2.335102783203" Y="-4.099461914062" />
                  <Point X="-2.466916992188" Y="-4.271245605469" />
                  <Point X="-2.480148925781" Y="-4.288489746094" />
                  <Point X="-2.55047265625" Y="-4.244947265625" />
                  <Point X="-2.801708984375" Y="-4.089387695313" />
                  <Point X="-2.96532421875" Y="-3.963409667969" />
                  <Point X="-3.104721923828" Y="-3.856077880859" />
                  <Point X="-2.731872314453" Y="-3.210283691406" />
                  <Point X="-2.423761230469" Y="-2.676619628906" />
                  <Point X="-2.412859130859" Y="-2.647653320312" />
                  <Point X="-2.406588134766" Y="-2.616126953125" />
                  <Point X="-2.405575439453" Y="-2.585192871094" />
                  <Point X="-2.414560058594" Y="-2.555575195312" />
                  <Point X="-2.428777099609" Y="-2.526746337891" />
                  <Point X="-2.4468046875" Y="-2.501589355469" />
                  <Point X="-2.464153320312" Y="-2.484240722656" />
                  <Point X="-2.489310302734" Y="-2.466213134766" />
                  <Point X="-2.518139160156" Y="-2.45199609375" />
                  <Point X="-2.547756835938" Y="-2.443011474609" />
                  <Point X="-2.578690917969" Y="-2.444024169922" />
                  <Point X="-2.610217285156" Y="-2.450295166016" />
                  <Point X="-2.63918359375" Y="-2.461197265625" />
                  <Point X="-3.394918701172" Y="-2.897521240234" />
                  <Point X="-3.818024414062" Y="-3.141801269531" />
                  <Point X="-3.884369873047" Y="-3.05463671875" />
                  <Point X="-4.082859863281" Y="-2.793861572266" />
                  <Point X="-4.200157226562" Y="-2.597171386719" />
                  <Point X="-4.306142578125" Y="-2.419450439453" />
                  <Point X="-3.645049072266" Y="-1.912175170898" />
                  <Point X="-3.105954589844" Y="-1.498513549805" />
                  <Point X="-3.084577392578" Y="-1.475593505859" />
                  <Point X="-3.066612548828" Y="-1.448462280273" />
                  <Point X="-3.053856445312" Y="-1.419832885742" />
                  <Point X="-3.046151855469" Y="-1.390085693359" />
                  <Point X="-3.04334765625" Y="-1.359657104492" />
                  <Point X="-3.045556396484" Y="-1.327986328125" />
                  <Point X="-3.052557373047" Y="-1.298241210938" />
                  <Point X="-3.068639648438" Y="-1.272257568359" />
                  <Point X="-3.089472167969" Y="-1.248301025391" />
                  <Point X="-3.112972167969" Y="-1.228767089844" />
                  <Point X="-3.139454833984" Y="-1.213180664062" />
                  <Point X="-3.168718261719" Y="-1.201956420898" />
                  <Point X="-3.200606201172" Y="-1.195474731445" />
                  <Point X="-3.231929199219" Y="-1.194383789062" />
                  <Point X="-4.185973144531" Y="-1.319986083984" />
                  <Point X="-4.732102050781" Y="-1.391885375977" />
                  <Point X="-4.756446289062" Y="-1.296578979492" />
                  <Point X="-4.834077636719" Y="-0.992654785156" />
                  <Point X="-4.865111328125" Y="-0.77566998291" />
                  <Point X="-4.892424316406" Y="-0.584698181152" />
                  <Point X="-4.146985839844" Y="-0.384958557129" />
                  <Point X="-3.532875976562" Y="-0.220408340454" />
                  <Point X="-3.510948242188" Y="-0.211478363037" />
                  <Point X="-3.483893554688" Y="-0.19628565979" />
                  <Point X="-3.470521240234" Y="-0.187198425293" />
                  <Point X="-3.442436279297" Y="-0.16439100647" />
                  <Point X="-3.426201171875" Y="-0.147480773926" />
                  <Point X="-3.414601074219" Y="-0.127109764099" />
                  <Point X="-3.403346923828" Y="-0.100035514832" />
                  <Point X="-3.398641113281" Y="-0.085523139954" />
                  <Point X="-3.390971923828" Y="-0.053232158661" />
                  <Point X="-3.388402832031" Y="-0.031901224136" />
                  <Point X="-3.390692871094" Y="-0.010538525581" />
                  <Point X="-3.396768798828" Y="0.016618904114" />
                  <Point X="-3.401237792969" Y="0.031075269699" />
                  <Point X="-3.414085205078" Y="0.063282917023" />
                  <Point X="-3.425390136719" Y="0.083818107605" />
                  <Point X="-3.441379394531" Y="0.100959877014" />
                  <Point X="-3.464684570312" Y="0.120449920654" />
                  <Point X="-3.477877197266" Y="0.129701538086" />
                  <Point X="-3.509711669922" Y="0.148211624146" />
                  <Point X="-3.532875976562" Y="0.157848342896" />
                  <Point X="-4.402532226562" Y="0.39087197876" />
                  <Point X="-4.89181640625" Y="0.521975219727" />
                  <Point X="-4.87451953125" Y="0.638864868164" />
                  <Point X="-4.824487792969" Y="0.976975463867" />
                  <Point X="-4.762017578125" Y="1.207510742188" />
                  <Point X="-4.703551757812" Y="1.423267944336" />
                  <Point X="-4.205875488281" Y="1.357747558594" />
                  <Point X="-3.765666259766" Y="1.29979284668" />
                  <Point X="-3.744986083984" Y="1.299341674805" />
                  <Point X="-3.723424560547" Y="1.301228149414" />
                  <Point X="-3.703137695312" Y="1.305263671875" />
                  <Point X="-3.677714355469" Y="1.313279541016" />
                  <Point X="-3.641711669922" Y="1.324631225586" />
                  <Point X="-3.622779052734" Y="1.332961669922" />
                  <Point X="-3.604034667969" Y="1.343783569336" />
                  <Point X="-3.587353271484" Y="1.356014892578" />
                  <Point X="-3.57371484375" Y="1.37156652832" />
                  <Point X="-3.561300537109" Y="1.389296264648" />
                  <Point X="-3.551351318359" Y="1.407431152344" />
                  <Point X="-3.541150146484" Y="1.432059204102" />
                  <Point X="-3.526703857422" Y="1.466935424805" />
                  <Point X="-3.520916015625" Y="1.48679296875" />
                  <Point X="-3.517157470703" Y="1.508108154297" />
                  <Point X="-3.515804443359" Y="1.528748657227" />
                  <Point X="-3.518951171875" Y="1.549192749023" />
                  <Point X="-3.524552978516" Y="1.570099121094" />
                  <Point X="-3.532049804688" Y="1.589377563477" />
                  <Point X="-3.544358642578" Y="1.613022827148" />
                  <Point X="-3.561789550781" Y="1.646507324219" />
                  <Point X="-3.573281738281" Y="1.663706665039" />
                  <Point X="-3.587194335938" Y="1.680286865234" />
                  <Point X="-3.602135986328" Y="1.694590209961" />
                  <Point X="-4.10097265625" Y="2.077361328125" />
                  <Point X="-4.351859863281" Y="2.269874267578" />
                  <Point X="-4.275557128906" Y="2.400599365234" />
                  <Point X="-4.081153808594" Y="2.733659423828" />
                  <Point X="-3.915673339844" Y="2.946361083984" />
                  <Point X="-3.750504882813" Y="3.158661865234" />
                  <Point X="-3.477020996094" Y="3.000765625" />
                  <Point X="-3.206656982422" Y="2.844670898438" />
                  <Point X="-3.187724121094" Y="2.836340332031" />
                  <Point X="-3.167081787109" Y="2.829831787109" />
                  <Point X="-3.146794677734" Y="2.825796386719" />
                  <Point X="-3.111386962891" Y="2.822698486328" />
                  <Point X="-3.061245361328" Y="2.818311767578" />
                  <Point X="-3.040564941406" Y="2.818762939453" />
                  <Point X="-3.019106201172" Y="2.821588134766" />
                  <Point X="-2.999014160156" Y="2.826504882812" />
                  <Point X="-2.980462402344" Y="2.835653564453" />
                  <Point X="-2.962208251953" Y="2.847282958984" />
                  <Point X="-2.946077636719" Y="2.860229248047" />
                  <Point X="-2.920944824219" Y="2.885361816406" />
                  <Point X="-2.885354003906" Y="2.920952636719" />
                  <Point X="-2.872406982422" Y="2.937083984375" />
                  <Point X="-2.860777587891" Y="2.955338134766" />
                  <Point X="-2.85162890625" Y="2.973889892578" />
                  <Point X="-2.846712158203" Y="2.993981933594" />
                  <Point X="-2.843886962891" Y="3.015440673828" />
                  <Point X="-2.843435791016" Y="3.03612109375" />
                  <Point X="-2.846533691406" Y="3.071528808594" />
                  <Point X="-2.850920410156" Y="3.121670654297" />
                  <Point X="-2.854955810547" Y="3.141957763672" />
                  <Point X="-2.861464355469" Y="3.162600097656" />
                  <Point X="-2.869794921875" Y="3.181532958984" />
                  <Point X="-3.090844726562" Y="3.56440234375" />
                  <Point X="-3.183333007812" Y="3.724596435547" />
                  <Point X="-3.039211425781" Y="3.835093017578" />
                  <Point X="-2.700626464844" Y="4.094683105469" />
                  <Point X="-2.440001220703" Y="4.239480957031" />
                  <Point X="-2.167036865234" Y="4.391134277344" />
                  <Point X="-2.125962158203" Y="4.337604492188" />
                  <Point X="-2.04319543457" Y="4.229740722656" />
                  <Point X="-2.028891845703" Y="4.214799316406" />
                  <Point X="-2.012311401367" Y="4.20088671875" />
                  <Point X="-1.99511340332" Y="4.189395019531" />
                  <Point X="-1.955704956055" Y="4.168879882812" />
                  <Point X="-1.899897460938" Y="4.139828125" />
                  <Point X="-1.8806171875" Y="4.132330566406" />
                  <Point X="-1.859710571289" Y="4.126729003906" />
                  <Point X="-1.839267700195" Y="4.123582519531" />
                  <Point X="-1.818628295898" Y="4.124935546875" />
                  <Point X="-1.797312988281" Y="4.128693847656" />
                  <Point X="-1.777453491211" Y="4.134481933594" />
                  <Point X="-1.736406616211" Y="4.151484375" />
                  <Point X="-1.678279296875" Y="4.175561523437" />
                  <Point X="-1.66014465332" Y="4.185510742187" />
                  <Point X="-1.642415039062" Y="4.197925292969" />
                  <Point X="-1.626863525391" Y="4.211563964844" />
                  <Point X="-1.614632568359" Y="4.228245117188" />
                  <Point X="-1.603810791016" Y="4.246989257813" />
                  <Point X="-1.59548046875" Y="4.265921386719" />
                  <Point X="-1.582120483398" Y="4.308293457031" />
                  <Point X="-1.563201171875" Y="4.368297851562" />
                  <Point X="-1.559165649414" Y="4.388584960938" />
                  <Point X="-1.557279174805" Y="4.410146484375" />
                  <Point X="-1.55773034668" Y="4.430826660156" />
                  <Point X="-1.582861206055" Y="4.62171484375" />
                  <Point X="-1.584201782227" Y="4.631897949219" />
                  <Point X="-1.387955566406" Y="4.686918457031" />
                  <Point X="-0.94963494873" Y="4.80980859375" />
                  <Point X="-0.63368536377" Y="4.846785644531" />
                  <Point X="-0.294710754395" Y="4.886458007812" />
                  <Point X="-0.213884414673" Y="4.584809570312" />
                  <Point X="-0.133903305054" Y="4.286315429688" />
                  <Point X="-0.121129844666" Y="4.258124023438" />
                  <Point X="-0.10327155304" Y="4.231397460938" />
                  <Point X="-0.0821145401" Y="4.20880859375" />
                  <Point X="-0.054819446564" Y="4.19421875" />
                  <Point X="-0.024381370544" Y="4.183886230469" />
                  <Point X="0.006155911446" Y="4.178844238281" />
                  <Point X="0.03669316864" Y="4.183886230469" />
                  <Point X="0.067131248474" Y="4.19421875" />
                  <Point X="0.094426460266" Y="4.20880859375" />
                  <Point X="0.115583503723" Y="4.231397460938" />
                  <Point X="0.133441802979" Y="4.258124023438" />
                  <Point X="0.146215255737" Y="4.286315429688" />
                  <Point X="0.259478546143" Y="4.709020507812" />
                  <Point X="0.307419342041" Y="4.8879375" />
                  <Point X="0.461317443848" Y="4.8718203125" />
                  <Point X="0.844041259766" Y="4.831738769531" />
                  <Point X="1.105440307617" Y="4.76862890625" />
                  <Point X="1.481026489258" Y="4.677950683594" />
                  <Point X="1.650303344727" Y="4.616552734375" />
                  <Point X="1.894646484375" Y="4.527927734375" />
                  <Point X="2.059170654297" Y="4.450985351562" />
                  <Point X="2.294577880859" Y="4.340893066406" />
                  <Point X="2.45354296875" Y="4.248279785156" />
                  <Point X="2.680977294922" Y="4.115775878906" />
                  <Point X="2.830880859375" Y="4.009172607422" />
                  <Point X="2.943260009766" Y="3.929254638672" />
                  <Point X="2.504196533203" Y="3.168774169922" />
                  <Point X="2.147581054688" Y="2.551097900391" />
                  <Point X="2.142075683594" Y="2.539930664062" />
                  <Point X="2.133076904297" Y="2.516056640625" />
                  <Point X="2.124190917969" Y="2.482827392578" />
                  <Point X="2.111607177734" Y="2.435770263672" />
                  <Point X="2.108619384766" Y="2.417934326172" />
                  <Point X="2.107728027344" Y="2.380952880859" />
                  <Point X="2.111192871094" Y="2.352218994141" />
                  <Point X="2.116099365234" Y="2.311528076172" />
                  <Point X="2.121441894531" Y="2.289605224609" />
                  <Point X="2.129708007812" Y="2.267516357422" />
                  <Point X="2.140070800781" Y="2.247471191406" />
                  <Point X="2.157850341797" Y="2.221268554688" />
                  <Point X="2.183028564453" Y="2.184162597656" />
                  <Point X="2.19446484375" Y="2.170328613281" />
                  <Point X="2.221598632812" Y="2.145592529297" />
                  <Point X="2.247801025391" Y="2.127812988281" />
                  <Point X="2.284907226562" Y="2.102635009766" />
                  <Point X="2.304952880859" Y="2.092271972656" />
                  <Point X="2.327041259766" Y="2.084006103516" />
                  <Point X="2.348963867188" Y="2.078663574219" />
                  <Point X="2.377697753906" Y="2.075198730469" />
                  <Point X="2.418388671875" Y="2.070291992188" />
                  <Point X="2.436467529297" Y="2.069845703125" />
                  <Point X="2.473206298828" Y="2.074171142578" />
                  <Point X="2.506435791016" Y="2.083057128906" />
                  <Point X="2.553492675781" Y="2.095640869141" />
                  <Point X="2.565284179688" Y="2.099638427734" />
                  <Point X="2.588533935547" Y="2.110145019531" />
                  <Point X="3.463240966797" Y="2.615157226563" />
                  <Point X="3.967326416016" Y="2.906191162109" />
                  <Point X="3.988364013672" Y="2.876953613281" />
                  <Point X="4.1232734375" Y="2.689459472656" />
                  <Point X="4.206840820313" Y="2.551364013672" />
                  <Point X="4.262198730469" Y="2.459884277344" />
                  <Point X="3.700301513672" Y="2.028724731445" />
                  <Point X="3.230783935547" Y="1.668451171875" />
                  <Point X="3.221425048828" Y="1.660241943359" />
                  <Point X="3.203973632812" Y="1.641627807617" />
                  <Point X="3.180058349609" Y="1.610428344727" />
                  <Point X="3.14619140625" Y="1.566246337891" />
                  <Point X="3.13660546875" Y="1.550911254883" />
                  <Point X="3.121630126953" Y="1.517086181641" />
                  <Point X="3.112721679688" Y="1.485231811523" />
                  <Point X="3.100106201172" Y="1.440121704102" />
                  <Point X="3.096652587891" Y="1.417823242188" />
                  <Point X="3.095836425781" Y="1.394253173828" />
                  <Point X="3.097739501953" Y="1.371768188477" />
                  <Point X="3.105052490234" Y="1.336325805664" />
                  <Point X="3.115408447266" Y="1.286135498047" />
                  <Point X="3.1206796875" Y="1.268978393555" />
                  <Point X="3.136282470703" Y="1.235740478516" />
                  <Point X="3.156172851563" Y="1.2055078125" />
                  <Point X="3.184340087891" Y="1.162695068359" />
                  <Point X="3.198893554688" Y="1.145450439453" />
                  <Point X="3.216137451172" Y="1.129360595703" />
                  <Point X="3.234346923828" Y="1.116034912109" />
                  <Point X="3.263170654297" Y="1.099809570312" />
                  <Point X="3.303989013672" Y="1.076832519531" />
                  <Point X="3.320521240234" Y="1.069501586914" />
                  <Point X="3.356118164062" Y="1.059438598633" />
                  <Point X="3.39508984375" Y="1.054287841797" />
                  <Point X="3.450278808594" Y="1.046994018555" />
                  <Point X="3.462702148438" Y="1.046175048828" />
                  <Point X="3.488203613281" Y="1.04698449707" />
                  <Point X="4.319116699219" Y="1.156376342773" />
                  <Point X="4.776839355469" Y="1.21663659668" />
                  <Point X="4.787994140625" Y="1.17081652832" />
                  <Point X="4.845936035156" Y="0.932809448242" />
                  <Point X="4.872270996094" Y="0.763664367676" />
                  <Point X="4.890864746094" Y="0.644238464355" />
                  <Point X="4.254657226562" Y="0.473767150879" />
                  <Point X="3.716579833984" Y="0.329589935303" />
                  <Point X="3.704791259766" Y="0.325586608887" />
                  <Point X="3.681545654297" Y="0.315067993164" />
                  <Point X="3.643257080078" Y="0.292936340332" />
                  <Point X="3.589035644531" Y="0.261595428467" />
                  <Point X="3.574311035156" Y="0.251096221924" />
                  <Point X="3.547530761719" Y="0.22557661438" />
                  <Point X="3.524557617188" Y="0.196303375244" />
                  <Point X="3.492024902344" Y="0.154848815918" />
                  <Point X="3.48030078125" Y="0.13556854248" />
                  <Point X="3.470527099609" Y="0.114104927063" />
                  <Point X="3.463680908203" Y="0.092604118347" />
                  <Point X="3.456023193359" Y="0.052618427277" />
                  <Point X="3.445178710938" Y="-0.004006377697" />
                  <Point X="3.443483154297" Y="-0.021875144958" />
                  <Point X="3.445178710938" Y="-0.058553627014" />
                  <Point X="3.452836425781" Y="-0.098539321899" />
                  <Point X="3.463680908203" Y="-0.155164123535" />
                  <Point X="3.470527099609" Y="-0.176665084839" />
                  <Point X="3.48030078125" Y="-0.198128707886" />
                  <Point X="3.492025146484" Y="-0.217409118652" />
                  <Point X="3.514998291016" Y="-0.2466822052" />
                  <Point X="3.547531005859" Y="-0.288136932373" />
                  <Point X="3.559999023438" Y="-0.30123614502" />
                  <Point X="3.589035644531" Y="-0.324155578613" />
                  <Point X="3.62732421875" Y="-0.346287078857" />
                  <Point X="3.681545654297" Y="-0.377627990723" />
                  <Point X="3.692710205078" Y="-0.383139038086" />
                  <Point X="3.716579833984" Y="-0.392149932861" />
                  <Point X="4.478564941406" Y="-0.596323242188" />
                  <Point X="4.89147265625" Y="-0.706961547852" />
                  <Point X="4.887375" Y="-0.734140258789" />
                  <Point X="4.855022460938" Y="-0.948726257324" />
                  <Point X="4.821283203125" Y="-1.096577514648" />
                  <Point X="4.801173828125" Y="-1.18469909668" />
                  <Point X="4.048400634766" Y="-1.085594604492" />
                  <Point X="3.424382080078" Y="-1.003441040039" />
                  <Point X="3.408035644531" Y="-1.002710327148" />
                  <Point X="3.374658447266" Y="-1.005508728027" />
                  <Point X="3.299511474609" Y="-1.021842285156" />
                  <Point X="3.193094238281" Y="-1.044972412109" />
                  <Point X="3.163973632812" Y="-1.056597167969" />
                  <Point X="3.136147216797" Y="-1.073489624023" />
                  <Point X="3.112397460938" Y="-1.093960083008" />
                  <Point X="3.066975830078" Y="-1.148588134766" />
                  <Point X="3.002653320312" Y="-1.225948364258" />
                  <Point X="2.987932617188" Y="-1.250330566406" />
                  <Point X="2.976589355469" Y="-1.277715698242" />
                  <Point X="2.969757568359" Y="-1.305365478516" />
                  <Point X="2.963247558594" Y="-1.376111083984" />
                  <Point X="2.954028564453" Y="-1.476295776367" />
                  <Point X="2.956347167969" Y="-1.507564208984" />
                  <Point X="2.964078613281" Y="-1.539185180664" />
                  <Point X="2.976450195312" Y="-1.567996826172" />
                  <Point X="3.018037597656" Y="-1.632683105469" />
                  <Point X="3.076930419922" Y="-1.724287109375" />
                  <Point X="3.086932861328" Y="-1.737239013672" />
                  <Point X="3.110628417969" Y="-1.760909179688" />
                  <Point X="3.817755126953" Y="-2.303506835938" />
                  <Point X="4.213122558594" Y="-2.6068828125" />
                  <Point X="4.12480859375" Y="-2.749788085938" />
                  <Point X="4.055034179688" Y="-2.848927734375" />
                  <Point X="4.028981445312" Y="-2.8859453125" />
                  <Point X="3.356727294922" Y="-2.497818847656" />
                  <Point X="2.800954589844" Y="-2.176943115234" />
                  <Point X="2.786131591797" Y="-2.170012451172" />
                  <Point X="2.754224609375" Y="-2.159825195312" />
                  <Point X="2.664787841797" Y="-2.143673095703" />
                  <Point X="2.538134277344" Y="-2.120799560547" />
                  <Point X="2.506781982422" Y="-2.120395263672" />
                  <Point X="2.474609130859" Y="-2.125353027344" />
                  <Point X="2.444833251953" Y="-2.135176757812" />
                  <Point X="2.370533203125" Y="-2.174280273438" />
                  <Point X="2.265315185547" Y="-2.229655761719" />
                  <Point X="2.242384521484" Y="-2.246548828125" />
                  <Point X="2.221424804688" Y="-2.267508544922" />
                  <Point X="2.204531738281" Y="-2.290439208984" />
                  <Point X="2.165428222656" Y="-2.364739257812" />
                  <Point X="2.110052734375" Y="-2.469957275391" />
                  <Point X="2.100229003906" Y="-2.499733154297" />
                  <Point X="2.095271240234" Y="-2.531906005859" />
                  <Point X="2.095675537109" Y="-2.563258300781" />
                  <Point X="2.111827636719" Y="-2.652695068359" />
                  <Point X="2.134701171875" Y="-2.779348632812" />
                  <Point X="2.138985595703" Y="-2.795141845703" />
                  <Point X="2.151819091797" Y="-2.826078613281" />
                  <Point X="2.606219238281" Y="-3.613122314453" />
                  <Point X="2.861283203125" Y="-4.054906494141" />
                  <Point X="2.781849121094" Y="-4.11164453125" />
                  <Point X="2.703837402344" Y="-4.162140136719" />
                  <Point X="2.701765136719" Y="-4.163481445312" />
                  <Point X="2.182652099609" Y="-3.486960449219" />
                  <Point X="1.758546020508" Y="-2.934255126953" />
                  <Point X="1.747503540039" Y="-2.922179443359" />
                  <Point X="1.721924194336" Y="-2.900557373047" />
                  <Point X="1.633715454102" Y="-2.843847167969" />
                  <Point X="1.50880078125" Y="-2.763538574219" />
                  <Point X="1.479989746094" Y="-2.751167236328" />
                  <Point X="1.448368774414" Y="-2.743435546875" />
                  <Point X="1.417099731445" Y="-2.741116699219" />
                  <Point X="1.320628295898" Y="-2.749994140625" />
                  <Point X="1.184012817383" Y="-2.762565673828" />
                  <Point X="1.15636315918" Y="-2.769397460938" />
                  <Point X="1.128978027344" Y="-2.780740722656" />
                  <Point X="1.104595825195" Y="-2.795461181641" />
                  <Point X="1.030103149414" Y="-2.857399414062" />
                  <Point X="0.924612121582" Y="-2.945111572266" />
                  <Point X="0.904141479492" Y="-2.968861572266" />
                  <Point X="0.887249084473" Y="-2.996688232422" />
                  <Point X="0.875624328613" Y="-3.025808837891" />
                  <Point X="0.853351379395" Y="-3.128281982422" />
                  <Point X="0.821810058594" Y="-3.273396728516" />
                  <Point X="0.819724487305" Y="-3.289627441406" />
                  <Point X="0.819742248535" Y="-3.323120117188" />
                  <Point X="0.94851574707" Y="-4.301251953125" />
                  <Point X="1.022065307617" Y="-4.859915527344" />
                  <Point X="0.975686218262" Y="-4.870081542969" />
                  <Point X="0.929315551758" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058432128906" Y="-4.752636230469" />
                  <Point X="-1.141246337891" Y="-4.731328125" />
                  <Point X="-1.140733520508" Y="-4.727432617188" />
                  <Point X="-1.120775756836" Y="-4.575838378906" />
                  <Point X="-1.120077392578" Y="-4.568099609375" />
                  <Point X="-1.119451660156" Y="-4.541033691406" />
                  <Point X="-1.121759277344" Y="-4.509328125" />
                  <Point X="-1.123334106445" Y="-4.497690429688" />
                  <Point X="-1.148385620117" Y="-4.371748535156" />
                  <Point X="-1.183861572266" Y="-4.193398925781" />
                  <Point X="-1.18812512207" Y="-4.17846875" />
                  <Point X="-1.19902734375" Y="-4.149502441406" />
                  <Point X="-1.205666137695" Y="-4.135466308594" />
                  <Point X="-1.221738037109" Y="-4.10762890625" />
                  <Point X="-1.23057409668" Y="-4.094861816406" />
                  <Point X="-1.250208496094" Y="-4.070937011719" />
                  <Point X="-1.261006958008" Y="-4.059779296875" />
                  <Point X="-1.357550170898" Y="-3.975113037109" />
                  <Point X="-1.494267700195" Y="-3.855214599609" />
                  <Point X="-1.506738647461" Y="-3.845965576172" />
                  <Point X="-1.533021606445" Y="-3.829621582031" />
                  <Point X="-1.546833496094" Y="-3.822526855469" />
                  <Point X="-1.576530761719" Y="-3.810225830078" />
                  <Point X="-1.591313354492" Y="-3.805476074219" />
                  <Point X="-1.621455078125" Y="-3.798447998047" />
                  <Point X="-1.636813964844" Y="-3.796169677734" />
                  <Point X="-1.764948486328" Y="-3.787771240234" />
                  <Point X="-1.946403198242" Y="-3.775878173828" />
                  <Point X="-1.961928222656" Y="-3.776132324219" />
                  <Point X="-1.992729614258" Y="-3.779166015625" />
                  <Point X="-2.008006103516" Y="-3.781945556641" />
                  <Point X="-2.03905480957" Y="-3.790265136719" />
                  <Point X="-2.053674560547" Y="-3.795496337891" />
                  <Point X="-2.081865478516" Y="-3.808269775391" />
                  <Point X="-2.095436767578" Y="-3.815811767578" />
                  <Point X="-2.202205566406" Y="-3.88715234375" />
                  <Point X="-2.353403320312" Y="-3.988179931641" />
                  <Point X="-2.359681640625" Y="-3.992757080078" />
                  <Point X="-2.380442138672" Y="-4.010131103516" />
                  <Point X="-2.402759277344" Y="-4.032771484375" />
                  <Point X="-2.410471435547" Y="-4.041629638672" />
                  <Point X="-2.503202880859" Y="-4.162479492188" />
                  <Point X="-2.747585205078" Y="-4.011163330078" />
                  <Point X="-2.907366943359" Y="-3.888137207031" />
                  <Point X="-2.980862792969" Y="-3.831547851562" />
                  <Point X="-2.649599853516" Y="-3.257783691406" />
                  <Point X="-2.341488769531" Y="-2.724119628906" />
                  <Point X="-2.334850097656" Y="-2.710083251953" />
                  <Point X="-2.323947998047" Y="-2.681116943359" />
                  <Point X="-2.319684570312" Y="-2.666187011719" />
                  <Point X="-2.313413574219" Y="-2.634660644531" />
                  <Point X="-2.311638916016" Y="-2.619235351562" />
                  <Point X="-2.310626220703" Y="-2.588301269531" />
                  <Point X="-2.314666259766" Y="-2.557615234375" />
                  <Point X="-2.323650878906" Y="-2.527997558594" />
                  <Point X="-2.329357421875" Y="-2.513557128906" />
                  <Point X="-2.343574462891" Y="-2.484728271484" />
                  <Point X="-2.351557128906" Y="-2.47141015625" />
                  <Point X="-2.369584716797" Y="-2.446253173828" />
                  <Point X="-2.379629638672" Y="-2.434414306641" />
                  <Point X="-2.396978271484" Y="-2.417065673828" />
                  <Point X="-2.408817138672" Y="-2.407020751953" />
                  <Point X="-2.433974121094" Y="-2.388993164062" />
                  <Point X="-2.447292236328" Y="-2.381010498047" />
                  <Point X="-2.47612109375" Y="-2.366793457031" />
                  <Point X="-2.490561523438" Y="-2.361086914062" />
                  <Point X="-2.520179199219" Y="-2.352102294922" />
                  <Point X="-2.550865234375" Y="-2.348062255859" />
                  <Point X="-2.581799316406" Y="-2.349074951172" />
                  <Point X="-2.597224609375" Y="-2.350849609375" />
                  <Point X="-2.628750976562" Y="-2.357120605469" />
                  <Point X="-2.643680908203" Y="-2.361384033203" />
                  <Point X="-2.672647216797" Y="-2.372286132812" />
                  <Point X="-2.68668359375" Y="-2.378924804688" />
                  <Point X="-3.442418701172" Y="-2.815248779297" />
                  <Point X="-3.793089111328" Y="-3.017708496094" />
                  <Point X="-3.808776367188" Y="-2.997098632812" />
                  <Point X="-4.004016845703" Y="-2.740592773438" />
                  <Point X="-4.118564453125" Y="-2.548513183594" />
                  <Point X="-4.181265136719" Y="-2.443373779297" />
                  <Point X="-3.587216796875" Y="-1.987543701172" />
                  <Point X="-3.048122314453" Y="-1.573882080078" />
                  <Point X="-3.036481933594" Y="-1.563309814453" />
                  <Point X="-3.015104736328" Y="-1.540389526367" />
                  <Point X="-3.005367675781" Y="-1.528041870117" />
                  <Point X="-2.987402832031" Y="-1.500910522461" />
                  <Point X="-2.979836425781" Y="-1.487126220703" />
                  <Point X="-2.967080322266" Y="-1.458496826172" />
                  <Point X="-2.961891113281" Y="-1.443652099609" />
                  <Point X="-2.954186523438" Y="-1.413904907227" />
                  <Point X="-2.951552734375" Y="-1.398803466797" />
                  <Point X="-2.948748535156" Y="-1.36837512207" />
                  <Point X="-2.948577880859" Y="-1.353047851562" />
                  <Point X="-2.950786621094" Y="-1.321377075195" />
                  <Point X="-2.953083251953" Y="-1.306221313477" />
                  <Point X="-2.960084228516" Y="-1.276476196289" />
                  <Point X="-2.971778320312" Y="-1.248243896484" />
                  <Point X="-2.987860595703" Y="-1.222260253906" />
                  <Point X="-2.996953125" Y="-1.209919311523" />
                  <Point X="-3.017785644531" Y="-1.185962768555" />
                  <Point X="-3.028745361328" Y="-1.175244506836" />
                  <Point X="-3.052245361328" Y="-1.155710571289" />
                  <Point X="-3.064785888672" Y="-1.14689465332" />
                  <Point X="-3.091268554688" Y="-1.131308349609" />
                  <Point X="-3.10543359375" Y="-1.124481445312" />
                  <Point X="-3.134697021484" Y="-1.113257202148" />
                  <Point X="-3.149795166016" Y="-1.108860107422" />
                  <Point X="-3.181683105469" Y="-1.102378417969" />
                  <Point X="-3.197299560547" Y="-1.100532348633" />
                  <Point X="-3.228622558594" Y="-1.09944140625" />
                  <Point X="-3.244329101562" Y="-1.100196411133" />
                  <Point X="-4.198373046875" Y="-1.225798828125" />
                  <Point X="-4.660920898438" Y="-1.286694458008" />
                  <Point X="-4.664401367188" Y="-1.273067749023" />
                  <Point X="-4.740762207031" Y="-0.974117919922" />
                  <Point X="-4.771068359375" Y="-0.762219787598" />
                  <Point X="-4.786452636719" Y="-0.65465435791" />
                  <Point X="-4.122397949219" Y="-0.476721435547" />
                  <Point X="-3.508288085938" Y="-0.312171356201" />
                  <Point X="-3.497044921875" Y="-0.308392028809" />
                  <Point X="-3.4751171875" Y="-0.299462158203" />
                  <Point X="-3.464432861328" Y="-0.29431149292" />
                  <Point X="-3.437378173828" Y="-0.279118804932" />
                  <Point X="-3.430498046875" Y="-0.274859863281" />
                  <Point X="-3.410633300781" Y="-0.260944152832" />
                  <Point X="-3.382548339844" Y="-0.238136779785" />
                  <Point X="-3.373906982422" Y="-0.230184265137" />
                  <Point X="-3.357671875" Y="-0.213273956299" />
                  <Point X="-3.343647460938" Y="-0.194490249634" />
                  <Point X="-3.332047363281" Y="-0.174119247437" />
                  <Point X="-3.326877929688" Y="-0.163574310303" />
                  <Point X="-3.315623779297" Y="-0.136499969482" />
                  <Point X="-3.312979003906" Y="-0.129338424683" />
                  <Point X="-3.306212158203" Y="-0.107475250244" />
                  <Point X="-3.29854296875" Y="-0.075184249878" />
                  <Point X="-3.296653564453" Y="-0.064591896057" />
                  <Point X="-3.294084472656" Y="-0.043260940552" />
                  <Point X="-3.293944091797" Y="-0.021775417328" />
                  <Point X="-3.296234130859" Y="-0.000412656933" />
                  <Point X="-3.297984863281" Y="0.01020303154" />
                  <Point X="-3.304060791016" Y="0.037360450745" />
                  <Point X="-3.306006835938" Y="0.044676856995" />
                  <Point X="-3.312998779297" Y="0.066273101807" />
                  <Point X="-3.325846191406" Y="0.098480880737" />
                  <Point X="-3.330862792969" Y="0.109098052979" />
                  <Point X="-3.342167724609" Y="0.129633285522" />
                  <Point X="-3.355920166016" Y="0.148617324829" />
                  <Point X="-3.371909423828" Y="0.165759048462" />
                  <Point X="-3.380434570312" Y="0.173834625244" />
                  <Point X="-3.403739746094" Y="0.193324584961" />
                  <Point X="-3.410139160156" Y="0.198230484009" />
                  <Point X="-3.430125" Y="0.211827835083" />
                  <Point X="-3.461959472656" Y="0.230337936401" />
                  <Point X="-3.473221923828" Y="0.235924224854" />
                  <Point X="-3.496386230469" Y="0.245560928345" />
                  <Point X="-3.508288085938" Y="0.249611358643" />
                  <Point X="-4.377944335938" Y="0.482634979248" />
                  <Point X="-4.785445800781" Y="0.591824707031" />
                  <Point X="-4.78054296875" Y="0.624958496094" />
                  <Point X="-4.731331054688" Y="0.957529663086" />
                  <Point X="-4.67032421875" Y="1.18266394043" />
                  <Point X="-4.633586425781" Y="1.318237060547" />
                  <Point X="-4.218275390625" Y="1.263560180664" />
                  <Point X="-3.778066162109" Y="1.20560559082" />
                  <Point X="-3.76773828125" Y="1.204815429688" />
                  <Point X="-3.747058105469" Y="1.204364379883" />
                  <Point X="-3.736705810547" Y="1.204703125" />
                  <Point X="-3.715144287109" Y="1.206589599609" />
                  <Point X="-3.704890136719" Y="1.208053710938" />
                  <Point X="-3.684603271484" Y="1.212089233398" />
                  <Point X="-3.674570800781" Y="1.214660522461" />
                  <Point X="-3.649147460938" Y="1.222676391602" />
                  <Point X="-3.613144775391" Y="1.234028076172" />
                  <Point X="-3.603451171875" Y="1.237676513672" />
                  <Point X="-3.584518554688" Y="1.246006958008" />
                  <Point X="-3.575279541016" Y="1.250688842773" />
                  <Point X="-3.55653515625" Y="1.261510864258" />
                  <Point X="-3.547860107422" Y="1.267171264648" />
                  <Point X="-3.531178710938" Y="1.279402709961" />
                  <Point X="-3.515928466797" Y="1.293377075195" />
                  <Point X="-3.502290039063" Y="1.308928710938" />
                  <Point X="-3.495895019531" Y="1.317077270508" />
                  <Point X="-3.483480712891" Y="1.334807128906" />
                  <Point X="-3.47801171875" Y="1.343602050781" />
                  <Point X="-3.4680625" Y="1.361736938477" />
                  <Point X="-3.463582519531" Y="1.371076538086" />
                  <Point X="-3.453381347656" Y="1.395704589844" />
                  <Point X="-3.438935058594" Y="1.430580932617" />
                  <Point X="-3.435499023438" Y="1.440352050781" />
                  <Point X="-3.429711181641" Y="1.460209594727" />
                  <Point X="-3.427359375" Y="1.470295898438" />
                  <Point X="-3.423600830078" Y="1.491611083984" />
                  <Point X="-3.422360839844" Y="1.501894042969" />
                  <Point X="-3.4210078125" Y="1.522534667969" />
                  <Point X="-3.42191015625" Y="1.543200805664" />
                  <Point X="-3.425056884766" Y="1.563644897461" />
                  <Point X="-3.427188232422" Y="1.573780395508" />
                  <Point X="-3.432790039062" Y="1.594686767578" />
                  <Point X="-3.436011962891" Y="1.604530151367" />
                  <Point X="-3.443508789062" Y="1.62380859375" />
                  <Point X="-3.447783691406" Y="1.633243408203" />
                  <Point X="-3.460092529297" Y="1.656888671875" />
                  <Point X="-3.4775234375" Y="1.690373168945" />
                  <Point X="-3.482799804688" Y="1.699286376953" />
                  <Point X="-3.494291992188" Y="1.716485717773" />
                  <Point X="-3.5005078125" Y="1.724771850586" />
                  <Point X="-3.514420410156" Y="1.741352172852" />
                  <Point X="-3.521500976563" Y="1.748911865234" />
                  <Point X="-3.536442626953" Y="1.763215087891" />
                  <Point X="-3.544303710938" Y="1.769958740234" />
                  <Point X="-4.043140380859" Y="2.152729980469" />
                  <Point X="-4.227614257812" Y="2.294282226563" />
                  <Point X="-4.193510742187" Y="2.352709960938" />
                  <Point X="-4.002293212891" Y="2.680312011719" />
                  <Point X="-3.840692626953" Y="2.888026611328" />
                  <Point X="-3.726338623047" Y="3.035012695312" />
                  <Point X="-3.524520996094" Y="2.918493164063" />
                  <Point X="-3.254156982422" Y="2.7623984375" />
                  <Point X="-3.244917480469" Y="2.757716064453" />
                  <Point X="-3.225984619141" Y="2.749385498047" />
                  <Point X="-3.216291259766" Y="2.745737304688" />
                  <Point X="-3.195648925781" Y="2.739228759766" />
                  <Point X="-3.185615478516" Y="2.736657226562" />
                  <Point X="-3.165328369141" Y="2.732621826172" />
                  <Point X="-3.155074707031" Y="2.731157958984" />
                  <Point X="-3.119666992188" Y="2.728060058594" />
                  <Point X="-3.069525390625" Y="2.723673339844" />
                  <Point X="-3.059173339844" Y="2.723334472656" />
                  <Point X="-3.038492919922" Y="2.723785644531" />
                  <Point X="-3.028164550781" Y="2.724575683594" />
                  <Point X="-3.006705810547" Y="2.727400878906" />
                  <Point X="-2.996524902344" Y="2.729310791016" />
                  <Point X="-2.976432861328" Y="2.734227539062" />
                  <Point X="-2.956996826172" Y="2.741302001953" />
                  <Point X="-2.938445068359" Y="2.750450683594" />
                  <Point X="-2.929418212891" Y="2.755531738281" />
                  <Point X="-2.9111640625" Y="2.767161132812" />
                  <Point X="-2.902745117188" Y="2.773194091797" />
                  <Point X="-2.886614501953" Y="2.786140380859" />
                  <Point X="-2.878902832031" Y="2.793053710938" />
                  <Point X="-2.853770019531" Y="2.818186279297" />
                  <Point X="-2.818179199219" Y="2.853777099609" />
                  <Point X="-2.811265625" Y="2.861489257812" />
                  <Point X="-2.798318603516" Y="2.877620605469" />
                  <Point X="-2.79228515625" Y="2.886039794922" />
                  <Point X="-2.780655761719" Y="2.904293945312" />
                  <Point X="-2.775574707031" Y="2.913320800781" />
                  <Point X="-2.766426025391" Y="2.931872558594" />
                  <Point X="-2.7593515625" Y="2.95130859375" />
                  <Point X="-2.754434814453" Y="2.971400634766" />
                  <Point X="-2.752524902344" Y="2.981581542969" />
                  <Point X="-2.749699707031" Y="3.003040283203" />
                  <Point X="-2.748909667969" Y="3.013368652344" />
                  <Point X="-2.748458496094" Y="3.034049072266" />
                  <Point X="-2.748797363281" Y="3.044401123047" />
                  <Point X="-2.751895263672" Y="3.079808837891" />
                  <Point X="-2.756281982422" Y="3.129950683594" />
                  <Point X="-2.757745849609" Y="3.140204345703" />
                  <Point X="-2.76178125" Y="3.160491455078" />
                  <Point X="-2.764352783203" Y="3.170524902344" />
                  <Point X="-2.770861328125" Y="3.191167236328" />
                  <Point X="-2.774509521484" Y="3.200860595703" />
                  <Point X="-2.782840087891" Y="3.219793457031" />
                  <Point X="-2.787522460938" Y="3.229032958984" />
                  <Point X="-3.008572265625" Y="3.61190234375" />
                  <Point X="-3.059387207031" Y="3.699916259766" />
                  <Point X="-2.981409423828" Y="3.759701171875" />
                  <Point X="-2.648374755859" Y="4.015035888672" />
                  <Point X="-2.393863769531" Y="4.156437011719" />
                  <Point X="-2.192525146484" Y="4.268296875" />
                  <Point X="-2.118564208984" Y="4.171908691406" />
                  <Point X="-2.111819335938" Y="4.164046386719" />
                  <Point X="-2.097515625" Y="4.149104980469" />
                  <Point X="-2.089956542969" Y="4.142024902344" />
                  <Point X="-2.073376220703" Y="4.128112304688" />
                  <Point X="-2.065091796875" Y="4.121897949219" />
                  <Point X="-2.047893676758" Y="4.11040625" />
                  <Point X="-2.038980224609" Y="4.105129394531" />
                  <Point X="-1.999571777344" Y="4.084614257813" />
                  <Point X="-1.943764282227" Y="4.0555625" />
                  <Point X="-1.934328735352" Y="4.051287109375" />
                  <Point X="-1.915048339844" Y="4.043789550781" />
                  <Point X="-1.905203613281" Y="4.040567138672" />
                  <Point X="-1.88429699707" Y="4.034965576172" />
                  <Point X="-1.874162475586" Y="4.032834716797" />
                  <Point X="-1.853719482422" Y="4.029688232422" />
                  <Point X="-1.833053222656" Y="4.028785888672" />
                  <Point X="-1.812413818359" Y="4.030138916016" />
                  <Point X="-1.802132446289" Y="4.031378662109" />
                  <Point X="-1.780817138672" Y="4.035136962891" />
                  <Point X="-1.770731079102" Y="4.037488525391" />
                  <Point X="-1.750871582031" Y="4.043276611328" />
                  <Point X="-1.741097900391" Y="4.046713623047" />
                  <Point X="-1.700051147461" Y="4.063716064453" />
                  <Point X="-1.641923828125" Y="4.087793212891" />
                  <Point X="-1.632584594727" Y="4.092272949219" />
                  <Point X="-1.614449951172" Y="4.102222167969" />
                  <Point X="-1.605654418945" Y="4.10769140625" />
                  <Point X="-1.587924804688" Y="4.120105957031" />
                  <Point X="-1.579776245117" Y="4.126501464844" />
                  <Point X="-1.564224853516" Y="4.140140136719" />
                  <Point X="-1.550250976562" Y="4.155390136719" />
                  <Point X="-1.538020019531" Y="4.172071289062" />
                  <Point X="-1.532359863281" Y="4.180745605469" />
                  <Point X="-1.521538085938" Y="4.199489746094" />
                  <Point X="-1.516856201172" Y="4.208728515625" />
                  <Point X="-1.508525878906" Y="4.227660644531" />
                  <Point X="-1.504877441406" Y="4.237354003906" />
                  <Point X="-1.491517456055" Y="4.279726074219" />
                  <Point X="-1.472598144531" Y="4.33973046875" />
                  <Point X="-1.470026733398" Y="4.349763671875" />
                  <Point X="-1.465991088867" Y="4.37005078125" />
                  <Point X="-1.46452722168" Y="4.3803046875" />
                  <Point X="-1.46264074707" Y="4.401866210938" />
                  <Point X="-1.462301757812" Y="4.41221875" />
                  <Point X="-1.462752929688" Y="4.432898925781" />
                  <Point X="-1.46354309082" Y="4.4432265625" />
                  <Point X="-1.479266113281" Y="4.562654785156" />
                  <Point X="-1.362309936523" Y="4.5954453125" />
                  <Point X="-0.93117590332" Y="4.7163203125" />
                  <Point X="-0.62264251709" Y="4.7524296875" />
                  <Point X="-0.365221923828" Y="4.782557128906" />
                  <Point X="-0.305647369385" Y="4.560221679688" />
                  <Point X="-0.225666244507" Y="4.261727539062" />
                  <Point X="-0.220435317993" Y="4.247107910156" />
                  <Point X="-0.207661773682" Y="4.218916503906" />
                  <Point X="-0.200119155884" Y="4.205344726562" />
                  <Point X="-0.182260925293" Y="4.178618164062" />
                  <Point X="-0.172608306885" Y="4.166456054688" />
                  <Point X="-0.151451248169" Y="4.1438671875" />
                  <Point X="-0.126898002625" Y="4.125026367188" />
                  <Point X="-0.099602806091" Y="4.110436523438" />
                  <Point X="-0.085356712341" Y="4.104260742188" />
                  <Point X="-0.054918586731" Y="4.093927978516" />
                  <Point X="-0.039857292175" Y="4.090155273438" />
                  <Point X="-0.009319890976" Y="4.08511328125" />
                  <Point X="0.021631721497" Y="4.08511328125" />
                  <Point X="0.052169120789" Y="4.090155273438" />
                  <Point X="0.067230415344" Y="4.093927978516" />
                  <Point X="0.097668540955" Y="4.104260742188" />
                  <Point X="0.111914489746" Y="4.110436523438" />
                  <Point X="0.139209686279" Y="4.125026367188" />
                  <Point X="0.16376322937" Y="4.143866699219" />
                  <Point X="0.184920150757" Y="4.166455566406" />
                  <Point X="0.194572906494" Y="4.178617675781" />
                  <Point X="0.212431137085" Y="4.205344238281" />
                  <Point X="0.219973754883" Y="4.218916503906" />
                  <Point X="0.232747146606" Y="4.247107910156" />
                  <Point X="0.237978225708" Y="4.261727539062" />
                  <Point X="0.351241516113" Y="4.684432617188" />
                  <Point X="0.378190246582" Y="4.785006347656" />
                  <Point X="0.451422576904" Y="4.777336914062" />
                  <Point X="0.827876708984" Y="4.737912109375" />
                  <Point X="1.083144775391" Y="4.676282226562" />
                  <Point X="1.453596069336" Y="4.58684375" />
                  <Point X="1.617911010742" Y="4.527245605469" />
                  <Point X="1.858258056641" Y="4.440069824219" />
                  <Point X="2.01892590332" Y="4.364931152344" />
                  <Point X="2.250454101562" Y="4.256652832031" />
                  <Point X="2.405719970703" Y="4.166194824219" />
                  <Point X="2.629433349609" Y="4.035858398438" />
                  <Point X="2.77582421875" Y="3.931753173828" />
                  <Point X="2.817779785156" Y="3.901916503906" />
                  <Point X="2.421924072266" Y="3.216274169922" />
                  <Point X="2.06530859375" Y="2.598597900391" />
                  <Point X="2.062373046875" Y="2.593104980469" />
                  <Point X="2.053180908203" Y="2.5734375" />
                  <Point X="2.044182128906" Y="2.549563476562" />
                  <Point X="2.041301757812" Y="2.540598632812" />
                  <Point X="2.032415649414" Y="2.507369384766" />
                  <Point X="2.01983215332" Y="2.460312255859" />
                  <Point X="2.017912719727" Y="2.451465576172" />
                  <Point X="2.013646972656" Y="2.420223388672" />
                  <Point X="2.012755615234" Y="2.383241943359" />
                  <Point X="2.013411254883" Y="2.369579833984" />
                  <Point X="2.016876098633" Y="2.340845947266" />
                  <Point X="2.021782592773" Y="2.300155029297" />
                  <Point X="2.023800537109" Y="2.28903515625" />
                  <Point X="2.029143066406" Y="2.267112304688" />
                  <Point X="2.032467895508" Y="2.256309326172" />
                  <Point X="2.040734008789" Y="2.234220458984" />
                  <Point X="2.045318115234" Y="2.223889160156" />
                  <Point X="2.055680908203" Y="2.203843994141" />
                  <Point X="2.061459472656" Y="2.194130126953" />
                  <Point X="2.079239013672" Y="2.167927490234" />
                  <Point X="2.104417236328" Y="2.130821533203" />
                  <Point X="2.10980859375" Y="2.123633056641" />
                  <Point X="2.130463134766" Y="2.100123291016" />
                  <Point X="2.157596923828" Y="2.075387207031" />
                  <Point X="2.168257324219" Y="2.066981445312" />
                  <Point X="2.194459716797" Y="2.049201904297" />
                  <Point X="2.231565917969" Y="2.024023925781" />
                  <Point X="2.241280029297" Y="2.018245117188" />
                  <Point X="2.261325683594" Y="2.007881835938" />
                  <Point X="2.271657226562" Y="2.003297851562" />
                  <Point X="2.293745605469" Y="1.995031982422" />
                  <Point X="2.304548095703" Y="1.991707397461" />
                  <Point X="2.326470703125" Y="1.986364868164" />
                  <Point X="2.337590820312" Y="1.984346801758" />
                  <Point X="2.366324707031" Y="1.980881958008" />
                  <Point X="2.407015625" Y="1.975975219727" />
                  <Point X="2.416044189453" Y="1.975321044922" />
                  <Point X="2.447575683594" Y="1.975497314453" />
                  <Point X="2.484314453125" Y="1.979822753906" />
                  <Point X="2.497748046875" Y="1.982395874023" />
                  <Point X="2.530977539062" Y="1.991281860352" />
                  <Point X="2.578034423828" Y="2.003865600586" />
                  <Point X="2.583994384766" Y="2.005670654297" />
                  <Point X="2.604405761719" Y="2.013067626953" />
                  <Point X="2.627655517578" Y="2.02357421875" />
                  <Point X="2.636033935547" Y="2.027872558594" />
                  <Point X="3.510740966797" Y="2.532884765625" />
                  <Point X="3.940404785156" Y="2.780951416016" />
                  <Point X="4.043953125" Y="2.637041992188" />
                  <Point X="4.125563964844" Y="2.502179931641" />
                  <Point X="4.136884765625" Y="2.483472167969" />
                  <Point X="3.642469238281" Y="2.104093261719" />
                  <Point X="3.172951660156" Y="1.743819702148" />
                  <Point X="3.168138671875" Y="1.739869506836" />
                  <Point X="3.152120361328" Y="1.725217651367" />
                  <Point X="3.134668945312" Y="1.706603515625" />
                  <Point X="3.128575927734" Y="1.699422119141" />
                  <Point X="3.104660644531" Y="1.66822277832" />
                  <Point X="3.070793701172" Y="1.624040771484" />
                  <Point X="3.065635253906" Y="1.616601928711" />
                  <Point X="3.049738037109" Y="1.589369995117" />
                  <Point X="3.034762695312" Y="1.555544921875" />
                  <Point X="3.030140625" Y="1.542672363281" />
                  <Point X="3.021232177734" Y="1.510817871094" />
                  <Point X="3.008616699219" Y="1.465707885742" />
                  <Point X="3.006225585938" Y="1.454662109375" />
                  <Point X="3.002771972656" Y="1.432363647461" />
                  <Point X="3.001709472656" Y="1.421110717773" />
                  <Point X="3.000893310547" Y="1.397540771484" />
                  <Point X="3.001174804688" Y="1.386241210938" />
                  <Point X="3.003077880859" Y="1.363756347656" />
                  <Point X="3.004699462891" Y="1.352570800781" />
                  <Point X="3.012012451172" Y="1.317128417969" />
                  <Point X="3.022368408203" Y="1.266938110352" />
                  <Point X="3.02459765625" Y="1.258235351562" />
                  <Point X="3.03468359375" Y="1.228609375" />
                  <Point X="3.050286376953" Y="1.195371459961" />
                  <Point X="3.056918457031" Y="1.183526000977" />
                  <Point X="3.076808837891" Y="1.153293334961" />
                  <Point X="3.104976074219" Y="1.11048059082" />
                  <Point X="3.111739257813" Y="1.101424194336" />
                  <Point X="3.126292724609" Y="1.08417956543" />
                  <Point X="3.134083007812" Y="1.075991333008" />
                  <Point X="3.151326904297" Y="1.059901367188" />
                  <Point X="3.160034423828" Y="1.052695922852" />
                  <Point X="3.178243896484" Y="1.039370361328" />
                  <Point X="3.187745849609" Y="1.03325" />
                  <Point X="3.216569580078" Y="1.017024536133" />
                  <Point X="3.257387939453" Y="0.994047607422" />
                  <Point X="3.265479248047" Y="0.989987792969" />
                  <Point X="3.294678222656" Y="0.97808416748" />
                  <Point X="3.330275146484" Y="0.968021179199" />
                  <Point X="3.343670654297" Y="0.965257568359" />
                  <Point X="3.382642333984" Y="0.960106872559" />
                  <Point X="3.437831298828" Y="0.952813049316" />
                  <Point X="3.444029785156" Y="0.952199707031" />
                  <Point X="3.465716064453" Y="0.95122277832" />
                  <Point X="3.491217529297" Y="0.952032348633" />
                  <Point X="3.500603515625" Y="0.952797302246" />
                  <Point X="4.331516601562" Y="1.062189086914" />
                  <Point X="4.704703613281" Y="1.111320068359" />
                  <Point X="4.75268359375" Y="0.914233703613" />
                  <Point X="4.778401855469" Y="0.749049438477" />
                  <Point X="4.78387109375" Y="0.713920837402" />
                  <Point X="4.230069335938" Y="0.565530151367" />
                  <Point X="3.691991943359" Y="0.421352844238" />
                  <Point X="3.686031738281" Y="0.419544403076" />
                  <Point X="3.665626708984" Y="0.412138092041" />
                  <Point X="3.642381103516" Y="0.401619445801" />
                  <Point X="3.634004150391" Y="0.39731652832" />
                  <Point X="3.595715576172" Y="0.375184783936" />
                  <Point X="3.541494140625" Y="0.343843933105" />
                  <Point X="3.533881835938" Y="0.338945770264" />
                  <Point X="3.508774169922" Y="0.319870605469" />
                  <Point X="3.481993896484" Y="0.294351013184" />
                  <Point X="3.472796630859" Y="0.284226654053" />
                  <Point X="3.449823486328" Y="0.254953460693" />
                  <Point X="3.417290771484" Y="0.213498809814" />
                  <Point X="3.410854248047" Y="0.204207794189" />
                  <Point X="3.399130126953" Y="0.184927536011" />
                  <Point X="3.393842529297" Y="0.174938156128" />
                  <Point X="3.384068847656" Y="0.153474624634" />
                  <Point X="3.380005371094" Y="0.142928497314" />
                  <Point X="3.373159179688" Y="0.121427658081" />
                  <Point X="3.370376464844" Y="0.110472961426" />
                  <Point X="3.36271875" Y="0.070487304688" />
                  <Point X="3.351874267578" Y="0.01386242485" />
                  <Point X="3.350603515625" Y="0.00496778965" />
                  <Point X="3.348584472656" Y="-0.026262044907" />
                  <Point X="3.350280029297" Y="-0.062940544128" />
                  <Point X="3.351874267578" Y="-0.076422424316" />
                  <Point X="3.359531982422" Y="-0.116408226013" />
                  <Point X="3.370376464844" Y="-0.173032958984" />
                  <Point X="3.373158935547" Y="-0.183987518311" />
                  <Point X="3.380005126953" Y="-0.205488494873" />
                  <Point X="3.384068847656" Y="-0.21603477478" />
                  <Point X="3.393842529297" Y="-0.237498291016" />
                  <Point X="3.399130371094" Y="-0.24748828125" />
                  <Point X="3.410854736328" Y="-0.266768676758" />
                  <Point X="3.417291259766" Y="-0.276059265137" />
                  <Point X="3.440264404297" Y="-0.305332458496" />
                  <Point X="3.472797119141" Y="-0.346787109375" />
                  <Point X="3.478718505859" Y="-0.353633728027" />
                  <Point X="3.501139404297" Y="-0.375805297852" />
                  <Point X="3.530176025391" Y="-0.398724731445" />
                  <Point X="3.541494384766" Y="-0.40640423584" />
                  <Point X="3.579782958984" Y="-0.428535644531" />
                  <Point X="3.634004394531" Y="-0.459876525879" />
                  <Point X="3.639495605469" Y="-0.462814788818" />
                  <Point X="3.659158447266" Y="-0.472016937256" />
                  <Point X="3.683028076172" Y="-0.481027770996" />
                  <Point X="3.691991943359" Y="-0.483912841797" />
                  <Point X="4.453977050781" Y="-0.688086242676" />
                  <Point X="4.784876953125" Y="-0.776750610352" />
                  <Point X="4.76161328125" Y="-0.931053344727" />
                  <Point X="4.7286640625" Y="-1.075442138672" />
                  <Point X="4.727801757812" Y="-1.079219848633" />
                  <Point X="4.060800537109" Y="-0.991407348633" />
                  <Point X="3.436781982422" Y="-0.90925378418" />
                  <Point X="3.428624511719" Y="-0.908535766602" />
                  <Point X="3.400098632812" Y="-0.908042480469" />
                  <Point X="3.366721435547" Y="-0.910840942383" />
                  <Point X="3.354480957031" Y="-0.912676269531" />
                  <Point X="3.279333984375" Y="-0.929009765625" />
                  <Point X="3.172916748047" Y="-0.952139953613" />
                  <Point X="3.157873535156" Y="-0.956742675781" />
                  <Point X="3.128752929688" Y="-0.968367370605" />
                  <Point X="3.114675292969" Y="-0.975389648438" />
                  <Point X="3.086848876953" Y="-0.992282104492" />
                  <Point X="3.074124023438" Y="-1.001530456543" />
                  <Point X="3.050374267578" Y="-1.022000915527" />
                  <Point X="3.039349609375" Y="-1.033223022461" />
                  <Point X="2.993927978516" Y="-1.087850952148" />
                  <Point X="2.92960546875" Y="-1.16521105957" />
                  <Point X="2.921326171875" Y="-1.176847290039" />
                  <Point X="2.90660546875" Y="-1.201229370117" />
                  <Point X="2.9001640625" Y="-1.213975830078" />
                  <Point X="2.888820800781" Y="-1.241360839844" />
                  <Point X="2.884362792969" Y="-1.254928100586" />
                  <Point X="2.877531005859" Y="-1.282577880859" />
                  <Point X="2.875157226562" Y="-1.296660400391" />
                  <Point X="2.868647216797" Y="-1.367406005859" />
                  <Point X="2.859428222656" Y="-1.467590698242" />
                  <Point X="2.859288574219" Y="-1.483320922852" />
                  <Point X="2.861607177734" Y="-1.514589355469" />
                  <Point X="2.864065429688" Y="-1.530127441406" />
                  <Point X="2.871796875" Y="-1.561748535156" />
                  <Point X="2.876785888672" Y="-1.576668334961" />
                  <Point X="2.889157470703" Y="-1.605479858398" />
                  <Point X="2.896540039062" Y="-1.619371704102" />
                  <Point X="2.938127441406" Y="-1.684057983398" />
                  <Point X="2.997020263672" Y="-1.775661987305" />
                  <Point X="3.001741943359" Y="-1.782353271484" />
                  <Point X="3.019793701172" Y="-1.804450073242" />
                  <Point X="3.043489257812" Y="-1.828120361328" />
                  <Point X="3.052796142578" Y="-1.836277709961" />
                  <Point X="3.759922851562" Y="-2.378875488281" />
                  <Point X="4.087170654297" Y="-2.629981689453" />
                  <Point X="4.045487548828" Y="-2.697431152344" />
                  <Point X="4.001274414063" Y="-2.760251953125" />
                  <Point X="3.404227294922" Y="-2.415546386719" />
                  <Point X="2.848454589844" Y="-2.094670654297" />
                  <Point X="2.841191894531" Y="-2.090885253906" />
                  <Point X="2.815026123047" Y="-2.079513183594" />
                  <Point X="2.783119140625" Y="-2.069325927734" />
                  <Point X="2.771108398438" Y="-2.066337646484" />
                  <Point X="2.681671630859" Y="-2.050185546875" />
                  <Point X="2.555018066406" Y="-2.027311767578" />
                  <Point X="2.539359130859" Y="-2.025807495117" />
                  <Point X="2.508006835938" Y="-2.025403198242" />
                  <Point X="2.492313476562" Y="-2.026503662109" />
                  <Point X="2.460140625" Y="-2.031461425781" />
                  <Point X="2.444844482422" Y="-2.035136230469" />
                  <Point X="2.415068603516" Y="-2.044959838867" />
                  <Point X="2.400588867188" Y="-2.051108642578" />
                  <Point X="2.326288818359" Y="-2.090212158203" />
                  <Point X="2.221070800781" Y="-2.145587646484" />
                  <Point X="2.208968261719" Y="-2.153170410156" />
                  <Point X="2.186037597656" Y="-2.170063476562" />
                  <Point X="2.175209472656" Y="-2.179373779297" />
                  <Point X="2.154249755859" Y="-2.200333496094" />
                  <Point X="2.144939453125" Y="-2.211161621094" />
                  <Point X="2.128046386719" Y="-2.234092285156" />
                  <Point X="2.120463623047" Y="-2.246194824219" />
                  <Point X="2.081360107422" Y="-2.320494873047" />
                  <Point X="2.025984619141" Y="-2.425712890625" />
                  <Point X="2.0198359375" Y="-2.440192626953" />
                  <Point X="2.010012084961" Y="-2.469968505859" />
                  <Point X="2.006337280273" Y="-2.485264648438" />
                  <Point X="2.001379516602" Y="-2.5174375" />
                  <Point X="2.000279174805" Y="-2.533130859375" />
                  <Point X="2.00068347168" Y="-2.564483154297" />
                  <Point X="2.002187866211" Y="-2.580142089844" />
                  <Point X="2.01833996582" Y="-2.669578857422" />
                  <Point X="2.041213378906" Y="-2.796232421875" />
                  <Point X="2.043015014648" Y="-2.804221435547" />
                  <Point X="2.051236083984" Y="-2.83154296875" />
                  <Point X="2.064069580078" Y="-2.862479736328" />
                  <Point X="2.069546630859" Y="-2.873578613281" />
                  <Point X="2.523946777344" Y="-3.660622314453" />
                  <Point X="2.735893066406" Y="-4.027724609375" />
                  <Point X="2.723754394531" Y="-4.036083740234" />
                  <Point X="2.258020751953" Y="-3.429128173828" />
                  <Point X="1.833914550781" Y="-2.876422851562" />
                  <Point X="1.828653442383" Y="-2.870146240234" />
                  <Point X="1.808831665039" Y="-2.849626953125" />
                  <Point X="1.783252319336" Y="-2.828004882812" />
                  <Point X="1.773299072266" Y="-2.820647216797" />
                  <Point X="1.685090332031" Y="-2.763937011719" />
                  <Point X="1.56017565918" Y="-2.683628417969" />
                  <Point X="1.546283935547" Y="-2.676245849609" />
                  <Point X="1.517472900391" Y="-2.663874511719" />
                  <Point X="1.502553588867" Y="-2.658885742188" />
                  <Point X="1.470932617188" Y="-2.651154052734" />
                  <Point X="1.45539453125" Y="-2.648695800781" />
                  <Point X="1.424125488281" Y="-2.646376953125" />
                  <Point X="1.40839453125" Y="-2.646516357422" />
                  <Point X="1.311923095703" Y="-2.655393798828" />
                  <Point X="1.175307739258" Y="-2.667965332031" />
                  <Point X="1.161225097656" Y="-2.670339111328" />
                  <Point X="1.133575439453" Y="-2.677170898438" />
                  <Point X="1.120008422852" Y="-2.68162890625" />
                  <Point X="1.092623168945" Y="-2.692972167969" />
                  <Point X="1.079877563477" Y="-2.699413330078" />
                  <Point X="1.055495361328" Y="-2.714133789062" />
                  <Point X="1.043858764648" Y="-2.722413085938" />
                  <Point X="0.969366149902" Y="-2.784351318359" />
                  <Point X="0.863875061035" Y="-2.872063476562" />
                  <Point X="0.852653015137" Y="-2.883088378906" />
                  <Point X="0.832182250977" Y="-2.906838378906" />
                  <Point X="0.82293359375" Y="-2.919563720703" />
                  <Point X="0.806041137695" Y="-2.947390380859" />
                  <Point X="0.799019287109" Y="-2.961467529297" />
                  <Point X="0.787394470215" Y="-2.990588134766" />
                  <Point X="0.782791931152" Y="-3.005631347656" />
                  <Point X="0.760518981934" Y="-3.108104492188" />
                  <Point X="0.728977661133" Y="-3.253219238281" />
                  <Point X="0.727584716797" Y="-3.261289306641" />
                  <Point X="0.724724487305" Y="-3.289677734375" />
                  <Point X="0.7247421875" Y="-3.323170410156" />
                  <Point X="0.725554992676" Y="-3.335520019531" />
                  <Point X="0.833091369629" Y="-4.152340332031" />
                  <Point X="0.820859741211" Y="-4.10669140625" />
                  <Point X="0.655064941406" Y="-3.487936523438" />
                  <Point X="0.652606262207" Y="-3.480124511719" />
                  <Point X="0.642145141602" Y="-3.453580566406" />
                  <Point X="0.62678717041" Y="-3.423815917969" />
                  <Point X="0.620407714844" Y="-3.413210205078" />
                  <Point X="0.552643066406" Y="-3.31557421875" />
                  <Point X="0.456679962158" Y="-3.177309814453" />
                  <Point X="0.446670654297" Y="-3.165172851562" />
                  <Point X="0.424786804199" Y="-3.142717529297" />
                  <Point X="0.41291229248" Y="-3.132399169922" />
                  <Point X="0.386657165527" Y="-3.113155273438" />
                  <Point X="0.37324230957" Y="-3.104937744141" />
                  <Point X="0.34524130249" Y="-3.090829589844" />
                  <Point X="0.330654998779" Y="-3.084938964844" />
                  <Point X="0.225793228149" Y="-3.052393554688" />
                  <Point X="0.077295753479" Y="-3.006305664062" />
                  <Point X="0.063377220154" Y="-3.003109619141" />
                  <Point X="0.035217639923" Y="-2.998840087891" />
                  <Point X="0.02097659111" Y="-2.997766601562" />
                  <Point X="-0.008664761543" Y="-2.997766601562" />
                  <Point X="-0.022905065536" Y="-2.998840087891" />
                  <Point X="-0.051064498901" Y="-3.003109375" />
                  <Point X="-0.064983482361" Y="-3.006305175781" />
                  <Point X="-0.16984526062" Y="-3.038850341797" />
                  <Point X="-0.318342590332" Y="-3.084938476562" />
                  <Point X="-0.332929473877" Y="-3.090829589844" />
                  <Point X="-0.360930786133" Y="-3.104937988281" />
                  <Point X="-0.374345336914" Y="-3.113155273438" />
                  <Point X="-0.400600463867" Y="-3.132399169922" />
                  <Point X="-0.412474853516" Y="-3.142717285156" />
                  <Point X="-0.434358673096" Y="-3.165172363281" />
                  <Point X="-0.444367980957" Y="-3.177309570312" />
                  <Point X="-0.512132446289" Y="-3.2749453125" />
                  <Point X="-0.608095458984" Y="-3.413209960938" />
                  <Point X="-0.612470275879" Y="-3.420132324219" />
                  <Point X="-0.62597668457" Y="-3.445265136719" />
                  <Point X="-0.638777893066" Y="-3.476215820312" />
                  <Point X="-0.642753112793" Y="-3.487936523438" />
                  <Point X="-0.877539428711" Y="-4.364171386719" />
                  <Point X="-0.985425231934" Y="-4.766807128906" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.677125244086" Y="3.006599320652" />
                  <Point X="-4.168944885218" Y="2.249263486756" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.594739938006" Y="2.959034113434" />
                  <Point X="-4.093343345338" Y="2.191252195645" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.941769772849" Y="3.790092539592" />
                  <Point X="-3.031593470433" Y="3.651776174762" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.512354629731" Y="2.911468909596" />
                  <Point X="-4.017741786748" Y="2.133240933343" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.553830344694" Y="1.307736945461" />
                  <Point X="-4.69558028017" Y="1.089461186198" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.716170509424" Y="3.963057487618" />
                  <Point X="-2.978282949646" Y="3.559439724341" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.429969308786" Y="2.863903725269" />
                  <Point X="-3.94214019118" Y="2.075229727984" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.449477610594" Y="1.293998610996" />
                  <Point X="-4.75002516951" Y="0.83119595506" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.524417254404" Y="4.083904153138" />
                  <Point X="-2.924972445401" Y="3.467103248447" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.34758398784" Y="2.816338540941" />
                  <Point X="-3.866538595611" Y="2.017218522625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.345124876493" Y="1.280260276532" />
                  <Point X="-4.78345263229" Y="0.605294722712" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.347204914009" Y="4.182359773684" />
                  <Point X="-2.871661941156" Y="3.374766772553" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.265198666895" Y="2.768773356614" />
                  <Point X="-3.790937000042" Y="1.959207317266" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.240772142393" Y="1.266521942067" />
                  <Point X="-4.694713775646" Y="0.567513125404" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.184724284019" Y="4.258130549519" />
                  <Point X="-2.818351436911" Y="3.28243029666" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.174248907681" Y="2.734396250711" />
                  <Point X="-3.715335404474" Y="1.901196111907" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.136419385147" Y="1.252783643244" />
                  <Point X="-4.598228501615" Y="0.541659964826" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.123373046408" Y="4.178175717219" />
                  <Point X="-2.768731185299" Y="3.184411330028" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.067970978154" Y="2.723622457232" />
                  <Point X="-3.639733808905" Y="1.843184906547" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.03206662154" Y="1.239045354217" />
                  <Point X="-4.501743227583" Y="0.515806804247" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.052227820879" Y="4.113302303771" />
                  <Point X="-2.748681377456" Y="3.040857873074" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.936599314995" Y="2.751489624986" />
                  <Point X="-3.564132213336" Y="1.785173701188" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.927713857932" Y="1.225307065189" />
                  <Point X="-4.405257953551" Y="0.489953643669" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.968191906574" Y="4.068278810327" />
                  <Point X="-3.494920282954" Y="1.717323274278" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.823361094325" Y="1.211568776162" />
                  <Point X="-4.308772672776" Y="0.464100493475" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.877481823997" Y="4.033532634769" />
                  <Point X="-3.443084113841" Y="1.622716521371" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.713133663682" Y="1.206876681076" />
                  <Point X="-4.212287389337" Y="0.438247347382" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.407530303549" Y="4.582767062214" />
                  <Point X="-1.469376123794" Y="4.48753285046" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.759515551618" Y="4.040757310933" />
                  <Point X="-3.426825120881" Y="1.473325721393" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.569084452441" Y="1.254265560948" />
                  <Point X="-4.115802105899" Y="0.412394201289" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.269040804716" Y="4.62159473574" />
                  <Point X="-1.491955857018" Y="4.278335656793" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.600371045803" Y="4.111390906037" />
                  <Point X="-4.01931682246" Y="0.386541055196" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.676311577256" Y="-0.625142149125" />
                  <Point X="-4.770032818454" Y="-0.76946020481" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.130551330404" Y="4.660422371505" />
                  <Point X="-3.922831539022" Y="0.360687909103" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.539173994277" Y="-0.588396243457" />
                  <Point X="-4.749588386832" Y="-0.912405994434" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.992061856093" Y="4.699250007271" />
                  <Point X="-3.826346255583" Y="0.33483476301" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.402036411298" Y="-0.551650337789" />
                  <Point X="-4.722590339523" Y="-1.045260100876" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.862480659533" Y="4.724360098239" />
                  <Point X="-3.729860972145" Y="0.308981616916" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.264898828319" Y="-0.514904432121" />
                  <Point X="-4.69061362664" Y="-1.170447734633" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.739888694489" Y="4.738707716472" />
                  <Point X="-3.633375688706" Y="0.283128470823" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.12776124534" Y="-0.478158526453" />
                  <Point X="-4.652074265117" Y="-1.28552977568" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.617296728272" Y="4.75305533651" />
                  <Point X="-3.536890405268" Y="0.25727532473" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.990623690588" Y="-0.44141266425" />
                  <Point X="-4.528209812026" Y="-1.269222697687" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.494704736345" Y="4.767402996139" />
                  <Point X="-3.446824250836" Y="0.221537586781" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.853486136984" Y="-0.404666803817" />
                  <Point X="-4.404345358935" Y="-1.252915619694" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.372112744418" Y="4.781750655769" />
                  <Point X="-3.370649749114" Y="0.164408579536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.716348583381" Y="-0.367920943384" />
                  <Point X="-4.280480905844" Y="-1.2366085417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.333995534108" Y="4.66601855886" />
                  <Point X="-3.316083363746" Y="0.074005990982" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.579211029778" Y="-0.331175082951" />
                  <Point X="-4.156616451332" Y="-1.220301461519" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.300909395732" Y="4.542539290549" />
                  <Point X="-3.298107039927" Y="-0.072740351375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.428419321898" Y="-0.273403668737" />
                  <Point X="-4.032751994026" Y="-1.203994377036" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.267823275528" Y="4.419059994255" />
                  <Point X="-3.90888753672" Y="-1.187687292552" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.234737155325" Y="4.295580697961" />
                  <Point X="-3.785023079415" Y="-1.171380208069" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.189947961634" Y="4.190122554499" />
                  <Point X="-3.661158622109" Y="-1.155073123586" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.120996089369" Y="4.121871673206" />
                  <Point X="-3.537294164803" Y="-1.138766039102" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.420074533069" Y="4.780619914063" />
                  <Point X="0.346768252255" Y="4.66773814061" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.029435863006" Y="4.088434604278" />
                  <Point X="-3.413429707497" Y="-1.122458954619" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.526135772138" Y="4.769512446541" />
                  <Point X="0.267200661906" Y="4.370787342394" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.093111693357" Y="4.102713839139" />
                  <Point X="-3.289565250191" Y="-1.106151870136" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.134826560925" Y="-2.407740147802" />
                  <Point X="-4.17011228454" Y="-2.462075397319" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.632197024792" Y="4.758404999938" />
                  <Point X="-3.174754778924" Y="-1.103786701535" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.909041232665" Y="-2.234488685055" />
                  <Point X="-4.115886889929" Y="-2.553003065592" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.738258277446" Y="4.747297553334" />
                  <Point X="-3.08264790658" Y="-1.136382009471" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.683255904406" Y="-2.061237222309" />
                  <Point X="-4.061661616649" Y="-2.6439309207" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.843057628931" Y="4.734246949332" />
                  <Point X="-3.008503412611" Y="-1.196636954532" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.457470828277" Y="-1.887986147811" />
                  <Point X="-4.00743634337" Y="-2.734858775808" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.940979230985" Y="4.710605539952" />
                  <Point X="-2.95662410098" Y="-1.291177273789" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.231685938778" Y="-1.714735360696" />
                  <Point X="-3.946747260608" Y="-2.81583323716" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.03890083304" Y="4.686964130573" />
                  <Point X="-3.885623129247" Y="-2.896137782418" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.136822461498" Y="4.663322761852" />
                  <Point X="-3.824498997887" Y="-2.976442327676" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.23474411172" Y="4.639681426644" />
                  <Point X="-3.70499246495" Y="-2.966845858244" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.332665761942" Y="4.616040091436" />
                  <Point X="-3.523771870468" Y="-2.862218067663" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.430587412165" Y="4.592398756228" />
                  <Point X="-3.342551279387" Y="-2.757590282319" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.523733882652" Y="4.561404289052" />
                  <Point X="-3.161330691075" Y="-2.65296250124" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.615413712055" Y="4.528151392653" />
                  <Point X="-2.980110102763" Y="-2.548334720161" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.70709348756" Y="4.49489841326" />
                  <Point X="-2.798889514452" Y="-2.443706939081" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.798773261556" Y="4.461645431544" />
                  <Point X="-2.62952956568" Y="-2.357342941278" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.888769832948" Y="4.425800545109" />
                  <Point X="-2.514057676781" Y="-2.353959278841" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.97565644852" Y="4.385166746669" />
                  <Point X="-2.426849844698" Y="-2.394098447229" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.062542992106" Y="4.344532837381" />
                  <Point X="-2.357970319252" Y="-2.462460732856" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.14942946428" Y="4.303898818128" />
                  <Point X="-2.313288036133" Y="-2.568083504161" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.236315936455" Y="4.263264798875" />
                  <Point X="-2.663139113894" Y="-3.281234374942" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.319263045075" Y="4.216564691706" />
                  <Point X="-2.93178322531" Y="-3.86933748343" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.401444563752" Y="4.168685679405" />
                  <Point X="-2.856267933455" Y="-3.927481584854" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.483625943096" Y="4.120806452551" />
                  <Point X="-2.780752609062" Y="-3.985625636174" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.565807314794" Y="4.072927213924" />
                  <Point X="-2.702279744576" Y="-4.039215475125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.646929107593" Y="4.023416366873" />
                  <Point X="-2.62149049888" Y="-4.089238399809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.724417616646" Y="3.968310753477" />
                  <Point X="-2.540701253184" Y="-4.139261324492" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.801906083482" Y="3.913205075075" />
                  <Point X="-2.31084735101" Y="-3.959744807322" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.096107126309" Y="2.651942535843" />
                  <Point X="-2.110743545645" Y="-3.826039421898" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.015384765396" Y="2.353213546894" />
                  <Point X="-1.965272916144" Y="-3.776461749851" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.045057082607" Y="2.224477454977" />
                  <Point X="-1.855489097073" Y="-3.781836946852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.100888201545" Y="2.136022385337" />
                  <Point X="-1.746839176984" Y="-3.78895819517" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.168999820351" Y="2.066477627182" />
                  <Point X="-1.638189314344" Y="-3.796079531951" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.248520805351" Y="2.014501752286" />
                  <Point X="-1.54327645259" Y="-3.824353995105" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.341876834715" Y="1.983829977481" />
                  <Point X="-1.466090464894" Y="-3.879925450538" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.449919236267" Y="1.975773232654" />
                  <Point X="-1.393918890096" Y="-3.943218424706" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.5822702573" Y="2.005148479284" />
                  <Point X="-1.321747294504" Y="-4.006511366856" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.754850521951" Y="2.096471328683" />
                  <Point X="-1.25027000883" Y="-4.070873452522" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.936071055019" Y="2.201099024693" />
                  <Point X="-1.195002432194" Y="-4.160196301209" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.117291588086" Y="2.305726720703" />
                  <Point X="-0.439972590198" Y="-3.171979754469" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.640124823467" Y="-3.480187165909" />
                  <Point X="-1.164969297112" Y="-4.288376782326" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.298512121153" Y="2.410354416714" />
                  <Point X="-0.258013300991" Y="-3.066214473761" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.720632081584" Y="-3.7785849256" />
                  <Point X="-1.138408878139" Y="-4.421904777309" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.47973265422" Y="2.514982112724" />
                  <Point X="-0.116144912385" Y="-3.022183766259" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.800199841492" Y="-4.075535984915" />
                  <Point X="-1.120077875648" Y="-4.568104962406" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.660953257931" Y="2.619609917516" />
                  <Point X="0.012986292592" Y="-2.997766601562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.879767598971" Y="-4.37248704049" />
                  <Point X="-1.116874139221" Y="-4.737599094513" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.842173876225" Y="2.724237744763" />
                  <Point X="3.237794825752" Y="1.793575620077" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.002695575042" Y="1.431554520889" />
                  <Point X="0.113432144761" Y="-3.017521006631" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.959335272147" Y="-4.66943796625" />
                  <Point X="-1.018409768253" Y="-4.760404713058" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.96767047353" Y="2.743058104447" />
                  <Point X="3.463579814123" Y="1.96682655944" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.019161198239" Y="1.282481903573" />
                  <Point X="0.207705717376" Y="-3.046779888731" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.02720923976" Y="2.66031231097" />
                  <Point X="3.689364860889" Y="2.140077588725" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.062582325943" Y="1.17491712323" />
                  <Point X="0.301979148772" Y="-3.076038988289" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.083226122575" Y="2.572143292616" />
                  <Point X="3.915150130412" Y="2.313328961026" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.121002201223" Y="1.090448388782" />
                  <Point X="0.389701158996" Y="-3.115386391775" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.194619047333" Y="1.029380937269" />
                  <Point X="0.459822111211" Y="-3.181837047814" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.278787934729" Y="0.984562204429" />
                  <Point X="0.518341599308" Y="-3.266152391976" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.376691705897" Y="0.960893337893" />
                  <Point X="0.915317048888" Y="-2.829291259257" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.773732348177" Y="-3.047312579294" />
                  <Point X="0.576861070431" Y="-3.350467762277" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.484064354337" Y="0.951805263713" />
                  <Point X="1.125698906673" Y="-2.679759061016" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.724728462351" Y="-3.297199399752" />
                  <Point X="0.633745300794" Y="-3.437301182531" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.607087033505" Y="0.966816123535" />
                  <Point X="1.251165573879" Y="-2.660984789642" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.740314148885" Y="-3.447627000706" />
                  <Point X="0.672319146763" Y="-3.552330122188" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.730951477266" Y="0.983123187161" />
                  <Point X="1.371639539119" Y="-2.649898605102" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.759407258491" Y="-3.592653643756" />
                  <Point X="0.705405297605" Y="-3.675809371302" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.854815921027" Y="0.999430250787" />
                  <Point X="1.482294611099" Y="-2.653932190275" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.778500368097" Y="-3.737680286807" />
                  <Point X="0.738491448447" Y="-3.799288620417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.978680364788" Y="1.015737314413" />
                  <Point X="3.542860641843" Y="0.344633792511" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.361836623944" Y="0.065881249739" />
                  <Point X="1.57153961386" Y="-2.69093439091" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.797593477704" Y="-3.882706929858" />
                  <Point X="0.771577599289" Y="-3.922767869531" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.102544808548" Y="1.032044378039" />
                  <Point X="3.708899066123" Y="0.425883091123" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.358894078904" Y="-0.113077325856" />
                  <Point X="1.651450574669" Y="-2.74230975572" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.81668658731" Y="-4.027733572908" />
                  <Point X="0.804663750131" Y="-4.046247118646" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.226409252309" Y="1.048351441664" />
                  <Point X="3.846036629099" Y="0.462628965989" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.392822858467" Y="-0.235259040525" />
                  <Point X="1.7313615031" Y="-2.793685170388" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.350273695304" Y="1.064658504112" />
                  <Point X="3.983174192076" Y="0.499374840856" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.451387090048" Y="-0.319505485766" />
                  <Point X="2.288593165769" Y="-2.110051109899" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.000543247449" Y="-2.55360908695" />
                  <Point X="1.808492938254" Y="-2.849340629368" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.474138134011" Y="1.080965559954" />
                  <Point X="4.120311755052" Y="0.536120715722" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.518963512056" Y="-0.389874374719" />
                  <Point X="3.148371696024" Y="-0.960535728103" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.866088325502" Y="-1.395214000238" />
                  <Point X="2.451566753305" Y="-2.033521246009" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.022727851322" Y="-2.693875246294" />
                  <Point X="1.871971532988" Y="-2.926019618969" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.598002572717" Y="1.097272615797" />
                  <Point X="4.257449320001" Y="0.572866593626" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.599667192435" Y="-0.440029058436" />
                  <Point X="3.282577191282" Y="-0.928304841588" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.871637459565" Y="-1.561096536698" />
                  <Point X="2.567419286836" Y="-2.02955144224" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.049768106523" Y="-2.82666435828" />
                  <Point X="1.933322923727" Y="-3.005974215473" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.708983298707" Y="1.093740493824" />
                  <Point X="4.394586892855" Y="0.609612483703" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.685748267869" Y="-0.481903279911" />
                  <Point X="3.408911269928" Y="-0.908194873729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.92167679851" Y="-1.658470165428" />
                  <Point X="2.668803122448" Y="-2.047861479467" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.099210282797" Y="-2.924957536884" />
                  <Point X="1.994674314465" Y="-3.085928811977" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.739868839481" Y="0.966872602366" />
                  <Point X="4.531724465709" Y="0.64635837378" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.782047362714" Y="-0.508043131297" />
                  <Point X="3.514825686459" Y="-0.919528428133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.978029175212" Y="-1.746122568501" />
                  <Point X="2.770187121087" Y="-2.066171265655" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.152520794844" Y="-3.017294000764" />
                  <Point X="2.056025705204" Y="-3.165883408481" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.76550029004" Y="0.83191412147" />
                  <Point X="4.668862038563" Y="0.683104263857" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.878532634427" Y="-0.533896295445" />
                  <Point X="3.619178446013" Y="-0.933266723403" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.040192412197" Y="-1.824827031415" />
                  <Point X="2.860454658157" Y="-2.101598901535" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.205831306891" Y="-3.109630464644" />
                  <Point X="2.117377095943" Y="-3.245838004986" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.975017906141" Y="-0.559749459593" />
                  <Point X="3.723531205567" Y="-0.947005018672" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.115022737763" Y="-1.884025888428" />
                  <Point X="2.942839962923" Y="-2.149164110776" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.259141818938" Y="-3.201966928524" />
                  <Point X="2.178728486681" Y="-3.32579260149" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.071503177854" Y="-0.585602623741" />
                  <Point X="3.827883965121" Y="-0.960743313941" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.190624342487" Y="-1.942037079689" />
                  <Point X="3.025225267689" Y="-2.196729320018" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.312452330985" Y="-3.294303392403" />
                  <Point X="2.24007987742" Y="-3.405747197994" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.167988449568" Y="-0.611455787889" />
                  <Point X="3.932236724675" Y="-0.974481609211" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.266225947211" Y="-2.00004827095" />
                  <Point X="3.107610572455" Y="-2.244294529259" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.365762843032" Y="-3.386639856283" />
                  <Point X="2.301431256058" Y="-3.485701813132" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.264473721281" Y="-0.637308952037" />
                  <Point X="4.036589484229" Y="-0.98821990448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.341827551935" Y="-2.058059462211" />
                  <Point X="3.189995877221" Y="-2.2918597385" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.419073355079" Y="-3.478976320163" />
                  <Point X="2.362782629694" Y="-3.56565643597" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.360958992995" Y="-0.663162116185" />
                  <Point X="4.140942232084" Y="-1.001958217764" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.417429156659" Y="-2.116070653473" />
                  <Point X="3.272381181987" Y="-2.339424947741" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.472383867126" Y="-3.571312784042" />
                  <Point X="2.424134003331" Y="-3.645611058809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.457444264942" Y="-0.689015279974" />
                  <Point X="4.245294976405" Y="-1.01569653649" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.493030761383" Y="-2.174081844734" />
                  <Point X="3.354766486753" Y="-2.386990156983" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.525694377261" Y="-3.663649250867" />
                  <Point X="2.485485376968" Y="-3.725565681647" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.55392954315" Y="-0.71486843412" />
                  <Point X="4.349647720726" Y="-1.029434855216" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.568632366107" Y="-2.232093035995" />
                  <Point X="3.437151790889" Y="-2.434555367195" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.579004830976" Y="-3.755985804569" />
                  <Point X="2.546836750605" Y="-3.805520304486" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.650414821359" Y="-0.740721588267" />
                  <Point X="4.454000465047" Y="-1.043173173942" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.644233970831" Y="-2.290104227256" />
                  <Point X="3.519537094078" Y="-2.482120578865" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.632315284691" Y="-3.848322358272" />
                  <Point X="2.608188124242" Y="-3.885474927324" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.746900099567" Y="-0.766574742413" />
                  <Point X="4.558353209368" Y="-1.056911492668" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.719835575555" Y="-2.348115418517" />
                  <Point X="3.601922397266" Y="-2.529685790536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.685625738406" Y="-3.940658911975" />
                  <Point X="2.669539497879" Y="-3.965429550163" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.76410840841" Y="-0.914503724124" />
                  <Point X="4.662705953689" Y="-1.070649811394" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.795437182214" Y="-2.406126606799" />
                  <Point X="3.684307700455" Y="-2.577251002206" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.871038791056" Y="-2.464137791718" />
                  <Point X="3.766693003643" Y="-2.624816213877" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.946640399898" Y="-2.522148976638" />
                  <Point X="3.849078306832" Y="-2.672381425547" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.022242008741" Y="-2.580160161557" />
                  <Point X="3.931463610021" Y="-2.719946637218" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="0.001626220703" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.63733392334" Y="-4.1558671875" />
                  <Point X="0.471539031982" Y="-3.537112304688" />
                  <Point X="0.464318664551" Y="-3.521544189453" />
                  <Point X="0.396554199219" Y="-3.423908203125" />
                  <Point X="0.300591033936" Y="-3.285643798828" />
                  <Point X="0.274335754395" Y="-3.266399902344" />
                  <Point X="0.169474075317" Y="-3.233854492188" />
                  <Point X="0.020976625443" Y="-3.187766601562" />
                  <Point X="-0.008664756775" Y="-3.187766601562" />
                  <Point X="-0.113526580811" Y="-3.220311767578" />
                  <Point X="-0.262023895264" Y="-3.266399902344" />
                  <Point X="-0.288279144287" Y="-3.285643798828" />
                  <Point X="-0.356043762207" Y="-3.383279541016" />
                  <Point X="-0.45200680542" Y="-3.521544189453" />
                  <Point X="-0.459227142334" Y="-3.537112304688" />
                  <Point X="-0.69401348877" Y="-4.413347167969" />
                  <Point X="-0.84774395752" Y="-4.987077148438" />
                  <Point X="-0.926040283203" Y="-4.971879394531" />
                  <Point X="-1.100246948242" Y="-4.938065429688" />
                  <Point X="-1.221171875" Y="-4.906952148438" />
                  <Point X="-1.351589599609" Y="-4.873396972656" />
                  <Point X="-1.329108032227" Y="-4.7026328125" />
                  <Point X="-1.309150146484" Y="-4.551038574219" />
                  <Point X="-1.309683227539" Y="-4.5347578125" />
                  <Point X="-1.334734741211" Y="-4.408815917969" />
                  <Point X="-1.370210693359" Y="-4.230466308594" />
                  <Point X="-1.386282714844" Y="-4.20262890625" />
                  <Point X="-1.482826049805" Y="-4.117962402344" />
                  <Point X="-1.619543457031" Y="-3.998063964844" />
                  <Point X="-1.649240722656" Y="-3.985762939453" />
                  <Point X="-1.777375244141" Y="-3.977364501953" />
                  <Point X="-1.958829833984" Y="-3.965471435547" />
                  <Point X="-1.989878540039" Y="-3.973791015625" />
                  <Point X="-2.096647216797" Y="-4.045131591797" />
                  <Point X="-2.247844970703" Y="-4.146159179688" />
                  <Point X="-2.259734130859" Y="-4.157294433594" />
                  <Point X="-2.391548339844" Y="-4.329078125" />
                  <Point X="-2.457095214844" Y="-4.414500488281" />
                  <Point X="-2.600483886719" Y="-4.325717773438" />
                  <Point X="-2.855832275391" Y="-4.167612304688" />
                  <Point X="-3.023281494141" Y="-4.038682128906" />
                  <Point X="-3.228581054688" Y="-3.880608154297" />
                  <Point X="-2.814144775391" Y="-3.162783691406" />
                  <Point X="-2.506033691406" Y="-2.629119628906" />
                  <Point X="-2.499762695312" Y="-2.597593261719" />
                  <Point X="-2.513979736328" Y="-2.568764404297" />
                  <Point X="-2.531328369141" Y="-2.551415771484" />
                  <Point X="-2.560157226562" Y="-2.537198730469" />
                  <Point X="-2.59168359375" Y="-2.543469726562" />
                  <Point X="-3.347418701172" Y="-2.979793701172" />
                  <Point X="-3.842959472656" Y="-3.265894042969" />
                  <Point X="-3.959963378906" Y="-3.112174804688" />
                  <Point X="-4.161703613281" Y="-2.847129394531" />
                  <Point X="-4.28175" Y="-2.645829833984" />
                  <Point X="-4.431020019531" Y="-2.395527099609" />
                  <Point X="-3.702881347656" Y="-1.836806640625" />
                  <Point X="-3.163786865234" Y="-1.423144897461" />
                  <Point X="-3.145822021484" Y="-1.396013793945" />
                  <Point X="-3.138117431641" Y="-1.366266479492" />
                  <Point X="-3.140326171875" Y="-1.334595825195" />
                  <Point X="-3.161158691406" Y="-1.310639282227" />
                  <Point X="-3.187641357422" Y="-1.295052734375" />
                  <Point X="-3.219529296875" Y="-1.288571044922" />
                  <Point X="-4.173573242188" Y="-1.414173339844" />
                  <Point X="-4.803283691406" Y="-1.497076293945" />
                  <Point X="-4.848491210938" Y="-1.32008996582" />
                  <Point X="-4.927393066406" Y="-1.011191650391" />
                  <Point X="-4.959154296875" Y="-0.789120239258" />
                  <Point X="-4.998396484375" Y="-0.514742126465" />
                  <Point X="-4.171573730469" Y="-0.293195556641" />
                  <Point X="-3.557463867188" Y="-0.128645431519" />
                  <Point X="-3.530409179688" Y="-0.113452659607" />
                  <Point X="-3.50232421875" Y="-0.090645271301" />
                  <Point X="-3.491070068359" Y="-0.063570991516" />
                  <Point X="-3.483400878906" Y="-0.031280042648" />
                  <Point X="-3.489476806641" Y="-0.004122571468" />
                  <Point X="-3.50232421875" Y="0.028085187912" />
                  <Point X="-3.525629394531" Y="0.047575218201" />
                  <Point X="-3.557463867188" Y="0.066085334778" />
                  <Point X="-4.427120117188" Y="0.299109100342" />
                  <Point X="-4.998186523438" Y="0.452125793457" />
                  <Point X="-4.96849609375" Y="0.652771118164" />
                  <Point X="-4.91764453125" Y="0.996421936035" />
                  <Point X="-4.8537109375" Y="1.232357666016" />
                  <Point X="-4.773516601562" Y="1.528298706055" />
                  <Point X="-4.193475585937" Y="1.451934814453" />
                  <Point X="-3.753266357422" Y="1.393980102539" />
                  <Point X="-3.731704833984" Y="1.395866577148" />
                  <Point X="-3.706281494141" Y="1.403882568359" />
                  <Point X="-3.670278808594" Y="1.415234130859" />
                  <Point X="-3.651534423828" Y="1.426056152344" />
                  <Point X="-3.639120117188" Y="1.443785766602" />
                  <Point X="-3.628918945312" Y="1.468413818359" />
                  <Point X="-3.61447265625" Y="1.503290039063" />
                  <Point X="-3.610714111328" Y="1.524605224609" />
                  <Point X="-3.616315917969" Y="1.54551171875" />
                  <Point X="-3.628624755859" Y="1.569156982422" />
                  <Point X="-3.646055664062" Y="1.602641479492" />
                  <Point X="-3.659968261719" Y="1.619221679688" />
                  <Point X="-4.158805175781" Y="2.001992675781" />
                  <Point X="-4.47610546875" Y="2.245466308594" />
                  <Point X="-4.357603515625" Y="2.448488769531" />
                  <Point X="-4.160014648438" Y="2.787006591797" />
                  <Point X="-3.990654052734" Y="3.004695556641" />
                  <Point X="-3.774671142578" Y="3.282310791016" />
                  <Point X="-3.429520996094" Y="3.083038085938" />
                  <Point X="-3.159156982422" Y="2.926943359375" />
                  <Point X="-3.138514648438" Y="2.920434814453" />
                  <Point X="-3.103106933594" Y="2.917336914062" />
                  <Point X="-3.052965332031" Y="2.912950195312" />
                  <Point X="-3.031506591797" Y="2.915775390625" />
                  <Point X="-3.013252441406" Y="2.927404785156" />
                  <Point X="-2.988119628906" Y="2.952537353516" />
                  <Point X="-2.952528808594" Y="2.988128173828" />
                  <Point X="-2.940899414062" Y="3.006382324219" />
                  <Point X="-2.93807421875" Y="3.027841064453" />
                  <Point X="-2.941172119141" Y="3.063248779297" />
                  <Point X="-2.945558837891" Y="3.113390625" />
                  <Point X="-2.952067382812" Y="3.134032958984" />
                  <Point X="-3.1731171875" Y="3.51690234375" />
                  <Point X="-3.307278564453" Y="3.749276611328" />
                  <Point X="-3.097013427734" Y="3.910484863281" />
                  <Point X="-2.752876220703" Y="4.174331542969" />
                  <Point X="-2.486138671875" Y="4.322524902344" />
                  <Point X="-2.141548583984" Y="4.513972167969" />
                  <Point X="-2.050593505859" Y="4.395437011719" />
                  <Point X="-1.967826904297" Y="4.287573242188" />
                  <Point X="-1.951246582031" Y="4.273660644531" />
                  <Point X="-1.911838134766" Y="4.253145507813" />
                  <Point X="-1.856030639648" Y="4.22409375" />
                  <Point X="-1.835124145508" Y="4.2184921875" />
                  <Point X="-1.813808837891" Y="4.222250488281" />
                  <Point X="-1.772762084961" Y="4.239252929688" />
                  <Point X="-1.714634887695" Y="4.263330078125" />
                  <Point X="-1.696905273438" Y="4.275744628906" />
                  <Point X="-1.686083496094" Y="4.294488769531" />
                  <Point X="-1.672723510742" Y="4.336860839844" />
                  <Point X="-1.653804077148" Y="4.396865234375" />
                  <Point X="-1.651917602539" Y="4.418426757812" />
                  <Point X="-1.677048461914" Y="4.609314941406" />
                  <Point X="-1.689137573242" Y="4.701141113281" />
                  <Point X="-1.413601318359" Y="4.778391601562" />
                  <Point X="-0.968094177246" Y="4.903296386719" />
                  <Point X="-0.644728393555" Y="4.941141601562" />
                  <Point X="-0.224199615479" Y="4.990358398438" />
                  <Point X="-0.122121459961" Y="4.609397460938" />
                  <Point X="-0.042140380859" Y="4.310903320312" />
                  <Point X="-0.024282121658" Y="4.284176757813" />
                  <Point X="0.006155934334" Y="4.273844238281" />
                  <Point X="0.036594028473" Y="4.284176757813" />
                  <Point X="0.054452251434" Y="4.310903320312" />
                  <Point X="0.167715606689" Y="4.733608398438" />
                  <Point X="0.236648406982" Y="4.990868652344" />
                  <Point X="0.471212280273" Y="4.966303710938" />
                  <Point X="0.860205749512" Y="4.925565429688" />
                  <Point X="1.127735595703" Y="4.860975585938" />
                  <Point X="1.508455932617" Y="4.769058105469" />
                  <Point X="1.682695800781" Y="4.705859863281" />
                  <Point X="1.931033691406" Y="4.615786132813" />
                  <Point X="2.099415527344" Y="4.537039550781" />
                  <Point X="2.33869921875" Y="4.425134277344" />
                  <Point X="2.501365722656" Y="4.330364746094" />
                  <Point X="2.732523681641" Y="4.19569140625" />
                  <Point X="2.885937255859" Y="4.086592285156" />
                  <Point X="3.068740234375" Y="3.956592773438" />
                  <Point X="2.586468994141" Y="3.121274169922" />
                  <Point X="2.229853515625" Y="2.503597900391" />
                  <Point X="2.224852050781" Y="2.491514648438" />
                  <Point X="2.215966064453" Y="2.458285400391" />
                  <Point X="2.203382324219" Y="2.411228271484" />
                  <Point X="2.202044677734" Y="2.392325927734" />
                  <Point X="2.205509521484" Y="2.363592041016" />
                  <Point X="2.210416015625" Y="2.322901123047" />
                  <Point X="2.218682128906" Y="2.300812255859" />
                  <Point X="2.236461669922" Y="2.274609619141" />
                  <Point X="2.261639892578" Y="2.237503662109" />
                  <Point X="2.274939941406" Y="2.224203613281" />
                  <Point X="2.301142333984" Y="2.206424072266" />
                  <Point X="2.338248535156" Y="2.18124609375" />
                  <Point X="2.360336914062" Y="2.172980224609" />
                  <Point X="2.389070800781" Y="2.169515380859" />
                  <Point X="2.42976171875" Y="2.164608642578" />
                  <Point X="2.448664550781" Y="2.165946289062" />
                  <Point X="2.481894042969" Y="2.174832275391" />
                  <Point X="2.528950927734" Y="2.187416015625" />
                  <Point X="2.541033935547" Y="2.192417480469" />
                  <Point X="3.415740966797" Y="2.6974296875" />
                  <Point X="3.994248046875" Y="3.031430908203" />
                  <Point X="4.0654765625" Y="2.932439208984" />
                  <Point X="4.202591796875" Y="2.741880126953" />
                  <Point X="4.288117675781" Y="2.600547851562" />
                  <Point X="4.387512695312" Y="2.436296142578" />
                  <Point X="3.758133789062" Y="1.953356323242" />
                  <Point X="3.288616210938" Y="1.593082641602" />
                  <Point X="3.279371337891" Y="1.583833374023" />
                  <Point X="3.255456054688" Y="1.552633911133" />
                  <Point X="3.221589111328" Y="1.508451904297" />
                  <Point X="3.213119628906" Y="1.4915" />
                  <Point X="3.204211181641" Y="1.459645629883" />
                  <Point X="3.191595703125" Y="1.414535522461" />
                  <Point X="3.190779541016" Y="1.390965576172" />
                  <Point X="3.198092529297" Y="1.355523193359" />
                  <Point X="3.208448486328" Y="1.305332763672" />
                  <Point X="3.215646484375" Y="1.287954956055" />
                  <Point X="3.235536865234" Y="1.257722412109" />
                  <Point X="3.263704101562" Y="1.214909667969" />
                  <Point X="3.280947998047" Y="1.198819824219" />
                  <Point X="3.309771728516" Y="1.182594482422" />
                  <Point X="3.350590087891" Y="1.159617431641" />
                  <Point X="3.368565429688" Y="1.153619628906" />
                  <Point X="3.407537109375" Y="1.148468994141" />
                  <Point X="3.462726074219" Y="1.141175048828" />
                  <Point X="3.475803710938" Y="1.14117175293" />
                  <Point X="4.306716796875" Y="1.250563598633" />
                  <Point X="4.848975097656" Y="1.32195324707" />
                  <Point X="4.880298339844" Y="1.193287597656" />
                  <Point X="4.939188476562" Y="0.951385620117" />
                  <Point X="4.966140136719" Y="0.77827923584" />
                  <Point X="4.997858398438" Y="0.574556152344" />
                  <Point X="4.279245117188" Y="0.382004272461" />
                  <Point X="3.741167724609" Y="0.237826919556" />
                  <Point X="3.729086914062" Y="0.232819412231" />
                  <Point X="3.690798339844" Y="0.210687850952" />
                  <Point X="3.636576904297" Y="0.179346878052" />
                  <Point X="3.622264892578" Y="0.166926574707" />
                  <Point X="3.599291748047" Y="0.137653320312" />
                  <Point X="3.566759033203" Y="0.096198890686" />
                  <Point X="3.556985351562" Y="0.074735275269" />
                  <Point X="3.549327636719" Y="0.034749530792" />
                  <Point X="3.538483154297" Y="-0.021875152588" />
                  <Point X="3.538483154297" Y="-0.04068478775" />
                  <Point X="3.546140869141" Y="-0.080670532227" />
                  <Point X="3.556985351562" Y="-0.13729536438" />
                  <Point X="3.566759033203" Y="-0.158758987427" />
                  <Point X="3.589732177734" Y="-0.188032089233" />
                  <Point X="3.622264892578" Y="-0.229486679077" />
                  <Point X="3.636576904297" Y="-0.241906967163" />
                  <Point X="3.674865478516" Y="-0.264038513184" />
                  <Point X="3.729086914062" Y="-0.295379486084" />
                  <Point X="3.741167724609" Y="-0.300387023926" />
                  <Point X="4.503152832031" Y="-0.504560241699" />
                  <Point X="4.998067871094" Y="-0.637172363281" />
                  <Point X="4.981313476562" Y="-0.748302734375" />
                  <Point X="4.948431640625" Y="-0.966399169922" />
                  <Point X="4.91390234375" Y="-1.117712890625" />
                  <Point X="4.874545898438" Y="-1.290178344727" />
                  <Point X="4.036000732422" Y="-1.179781738281" />
                  <Point X="3.411982177734" Y="-1.097628295898" />
                  <Point X="3.3948359375" Y="-1.098341186523" />
                  <Point X="3.319688964844" Y="-1.114674682617" />
                  <Point X="3.213271728516" Y="-1.137804931641" />
                  <Point X="3.1854453125" Y="-1.154697265625" />
                  <Point X="3.140023681641" Y="-1.209325317383" />
                  <Point X="3.075701171875" Y="-1.286685424805" />
                  <Point X="3.064357910156" Y="-1.314070556641" />
                  <Point X="3.057847900391" Y="-1.384816162109" />
                  <Point X="3.04862890625" Y="-1.485000854492" />
                  <Point X="3.056360351562" Y="-1.516621826172" />
                  <Point X="3.097947753906" Y="-1.581308227539" />
                  <Point X="3.156840576172" Y="-1.672912353516" />
                  <Point X="3.168460693359" Y="-1.685540649414" />
                  <Point X="3.875587402344" Y="-2.228138183594" />
                  <Point X="4.339074707031" Y="-2.583784179688" />
                  <Point X="4.296822265625" Y="-2.652154541016" />
                  <Point X="4.204133300781" Y="-2.802139404297" />
                  <Point X="4.132722167969" Y="-2.903604492188" />
                  <Point X="4.056688476562" Y="-3.011638427734" />
                  <Point X="3.309227294922" Y="-2.580091308594" />
                  <Point X="2.753454589844" Y="-2.259215576172" />
                  <Point X="2.737340820313" Y="-2.253312744141" />
                  <Point X="2.647904052734" Y="-2.237160644531" />
                  <Point X="2.521250488281" Y="-2.214287109375" />
                  <Point X="2.489077636719" Y="-2.219244873047" />
                  <Point X="2.414777587891" Y="-2.258348388672" />
                  <Point X="2.309559570312" Y="-2.313723876953" />
                  <Point X="2.288599853516" Y="-2.33468359375" />
                  <Point X="2.249496337891" Y="-2.408983642578" />
                  <Point X="2.194120849609" Y="-2.514201660156" />
                  <Point X="2.189163085938" Y="-2.546374511719" />
                  <Point X="2.205315185547" Y="-2.635811279297" />
                  <Point X="2.228188720703" Y="-2.76246484375" />
                  <Point X="2.234091552734" Y="-2.778578613281" />
                  <Point X="2.688491699219" Y="-3.565622314453" />
                  <Point X="2.986673583984" Y="-4.082088623047" />
                  <Point X="2.945283447266" Y="-4.11165234375" />
                  <Point X="2.835296875" Y="-4.190213378906" />
                  <Point X="2.755458740234" Y="-4.241891113281" />
                  <Point X="2.679775878906" Y="-4.290879394531" />
                  <Point X="2.107283447266" Y="-3.544792724609" />
                  <Point X="1.683177490234" Y="-2.992087402344" />
                  <Point X="1.670549316406" Y="-2.980467529297" />
                  <Point X="1.582340576172" Y="-2.923757324219" />
                  <Point X="1.45742590332" Y="-2.843448730469" />
                  <Point X="1.425804931641" Y="-2.835717041016" />
                  <Point X="1.329333496094" Y="-2.844594482422" />
                  <Point X="1.192718017578" Y="-2.857166015625" />
                  <Point X="1.165332885742" Y="-2.868509277344" />
                  <Point X="1.090840209961" Y="-2.930447509766" />
                  <Point X="0.985349243164" Y="-3.018159667969" />
                  <Point X="0.968456726074" Y="-3.045986328125" />
                  <Point X="0.946183837891" Y="-3.148459472656" />
                  <Point X="0.91464251709" Y="-3.29357421875" />
                  <Point X="0.91392956543" Y="-3.310720214844" />
                  <Point X="1.04270300293" Y="-4.288852050781" />
                  <Point X="1.127642211914" Y="-4.934028320312" />
                  <Point X="1.098400146484" Y="-4.940438476562" />
                  <Point X="0.994346374512" Y="-4.963246582031" />
                  <Point X="0.920588134766" Y="-4.976645996094" />
                  <Point X="0.860200378418" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#158" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.079981186327" Y="4.653618339828" Z="1.05" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.05" />
                  <Point X="-0.656696476724" Y="5.022082754629" Z="1.05" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.05" />
                  <Point X="-1.433255108269" Y="4.857815414216" Z="1.05" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.05" />
                  <Point X="-1.732776755986" Y="4.63406860881" Z="1.05" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.05" />
                  <Point X="-1.726565220585" Y="4.383176266887" Z="1.05" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.05" />
                  <Point X="-1.798052375923" Y="4.316726810699" Z="1.05" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.05" />
                  <Point X="-1.894906586973" Y="4.328776350759" Z="1.05" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.05" />
                  <Point X="-2.017081832398" Y="4.457154943855" Z="1.05" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.05" />
                  <Point X="-2.51657756674" Y="4.397512577589" Z="1.05" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.05" />
                  <Point X="-3.133591612369" Y="3.981444782805" Z="1.05" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.05" />
                  <Point X="-3.222574465305" Y="3.523182335294" Z="1.05" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.05" />
                  <Point X="-2.99713781308" Y="3.090171221397" Z="1.05" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.05" />
                  <Point X="-3.029630941282" Y="3.019172626605" Z="1.05" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.05" />
                  <Point X="-3.10490524421" Y="2.998426886092" Z="1.05" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.05" />
                  <Point X="-3.410677013004" Y="3.15761948344" Z="1.05" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.05" />
                  <Point X="-4.0362733231" Y="3.066678038441" Z="1.05" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.05" />
                  <Point X="-4.406941786576" Y="2.504973787302" Z="1.05" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.05" />
                  <Point X="-4.195399287784" Y="1.993605169601" Z="1.05" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.05" />
                  <Point X="-3.679131338843" Y="1.577349571313" Z="1.05" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.05" />
                  <Point X="-3.681268667334" Y="1.518828085019" Z="1.05" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.05" />
                  <Point X="-3.727472651956" Y="1.482848499969" Z="1.05" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.05" />
                  <Point X="-4.193104969969" Y="1.532787153379" Z="1.05" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.05" />
                  <Point X="-4.908125981603" Y="1.276715050088" Z="1.05" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.05" />
                  <Point X="-5.024113582062" Y="0.691370206739" Z="1.05" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.05" />
                  <Point X="-4.446217194859" Y="0.28209278158" Z="1.05" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.05" />
                  <Point X="-3.560294381738" Y="0.037779253506" Z="1.05" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.05" />
                  <Point X="-3.543385648272" Y="0.012336674768" Z="1.05" />
                  <Point X="-3.539556741714" Y="0" Z="1.05" />
                  <Point X="-3.544978952863" Y="-0.017470276292" Z="1.05" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.05" />
                  <Point X="-3.565074213385" Y="-0.041096729628" Z="1.05" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.05" />
                  <Point X="-4.190670593914" Y="-0.213619275176" Z="1.05" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.05" />
                  <Point X="-5.014806303469" Y="-0.76491930455" Z="1.05" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.05" />
                  <Point X="-4.90309180258" Y="-1.301184089106" Z="1.05" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.05" />
                  <Point X="-4.173202962245" Y="-1.43246556906" Z="1.05" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.05" />
                  <Point X="-3.20363707132" Y="-1.315998773721" Z="1.05" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.05" />
                  <Point X="-3.197192020251" Y="-1.339885156527" Z="1.05" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.05" />
                  <Point X="-3.739475293838" Y="-1.765858905336" Z="1.05" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.05" />
                  <Point X="-4.330849293538" Y="-2.640159500498" Z="1.05" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.05" />
                  <Point X="-4.005905276083" Y="-3.111177981089" Z="1.05" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.05" />
                  <Point X="-3.32857474528" Y="-2.991814912705" Z="1.05" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.05" />
                  <Point X="-2.562671280536" Y="-2.565659188596" Z="1.05" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.05" />
                  <Point X="-2.863602100463" Y="-3.106503637212" Z="1.05" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.05" />
                  <Point X="-3.05994128564" Y="-4.047019153641" Z="1.05" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.05" />
                  <Point X="-2.63296170545" Y="-4.336948309516" Z="1.05" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.05" />
                  <Point X="-2.358036702029" Y="-4.328236022521" Z="1.05" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.05" />
                  <Point X="-2.075024585376" Y="-4.055424962871" Z="1.05" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.05" />
                  <Point X="-1.786801411359" Y="-3.995977542553" Z="1.05" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.05" />
                  <Point X="-1.52194934863" Y="-4.124274127957" Z="1.05" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.05" />
                  <Point X="-1.389929986802" Y="-4.387290197004" Z="1.05" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.05" />
                  <Point X="-1.384836323392" Y="-4.664826616928" Z="1.05" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.05" />
                  <Point X="-1.239786725292" Y="-4.924095004833" Z="1.05" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.05" />
                  <Point X="-0.941723636421" Y="-4.989683330361" Z="1.05" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.05" />
                  <Point X="-0.651873155196" Y="-4.395007822436" Z="1.05" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.05" />
                  <Point X="-0.321123655151" Y="-3.38050851126" Z="1.05" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.05" />
                  <Point X="-0.104861733605" Y="-3.23678458079" Z="1.05" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.05" />
                  <Point X="0.148497345756" Y="-3.250327464498" Z="1.05" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.05" />
                  <Point X="0.349322204281" Y="-3.421137244508" Z="1.05" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.05" />
                  <Point X="0.582881705205" Y="-4.137528207379" Z="1.05" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.05" />
                  <Point X="0.923369127278" Y="-4.994560928364" Z="1.05" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.05" />
                  <Point X="1.102949689595" Y="-4.957998692266" Z="1.05" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.05" />
                  <Point X="1.086119280946" Y="-4.251045398899" Z="1.05" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.05" />
                  <Point X="0.988887067678" Y="-3.127799047075" Z="1.05" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.05" />
                  <Point X="1.116651033026" Y="-2.937613692199" Z="1.05" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.05" />
                  <Point X="1.327759171211" Y="-2.863103964613" Z="1.05" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.05" />
                  <Point X="1.549145124295" Y="-2.934535179033" Z="1.05" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.05" />
                  <Point X="2.061459719335" Y="-3.543950625315" Z="1.05" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.05" />
                  <Point X="2.776471927318" Y="-4.252585356044" Z="1.05" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.05" />
                  <Point X="2.968189252217" Y="-4.121059521234" Z="1.05" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.05" />
                  <Point X="2.725636971877" Y="-3.509342310239" Z="1.05" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.05" />
                  <Point X="2.248363331904" Y="-2.59564495345" Z="1.05" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.05" />
                  <Point X="2.287586642604" Y="-2.400990147495" Z="1.05" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.05" />
                  <Point X="2.431908230877" Y="-2.271314667363" Z="1.05" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.05" />
                  <Point X="2.632861850062" Y="-2.255084677587" Z="1.05" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.05" />
                  <Point X="3.278071286868" Y="-2.592112528397" Z="1.05" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.05" />
                  <Point X="4.167454773641" Y="-2.901101846319" Z="1.05" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.05" />
                  <Point X="4.33319942813" Y="-2.647159558656" Z="1.05" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.05" />
                  <Point X="3.899869475733" Y="-2.15719046505" Z="1.05" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.05" />
                  <Point X="3.133849886627" Y="-1.522988934539" Z="1.05" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.05" />
                  <Point X="3.101481467206" Y="-1.358117847166" Z="1.05" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.05" />
                  <Point X="3.172314012308" Y="-1.210012166349" Z="1.05" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.05" />
                  <Point X="3.324152955603" Y="-1.132253915008" Z="1.05" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.05" />
                  <Point X="4.023318378155" Y="-1.198074014716" Z="1.05" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.05" />
                  <Point X="4.956493325757" Y="-1.097556816546" Z="1.05" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.05" />
                  <Point X="5.02459881111" Y="-0.724476708434" Z="1.05" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.05" />
                  <Point X="4.509937700852" Y="-0.424983893216" Z="1.05" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.05" />
                  <Point X="3.693731286879" Y="-0.189469688395" Z="1.05" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.05" />
                  <Point X="3.622909867022" Y="-0.12588371437" Z="1.05" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.05" />
                  <Point X="3.589092441153" Y="-0.039985730208" Z="1.05" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.05" />
                  <Point X="3.592279009271" Y="0.05662480101" Z="1.05" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.05" />
                  <Point X="3.632469571378" Y="0.138065024205" Z="1.05" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.05" />
                  <Point X="3.709664127473" Y="0.198679155847" Z="1.05" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.05" />
                  <Point X="4.286030116455" Y="0.364988039553" Z="1.05" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.05" />
                  <Point X="5.009388885173" Y="0.817251281688" Z="1.05" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.05" />
                  <Point X="4.922723448573" Y="1.236394527596" Z="1.05" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.05" />
                  <Point X="4.294034398583" Y="1.331415886483" Z="1.05" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.05" />
                  <Point X="3.407932438133" Y="1.229317978635" Z="1.05" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.05" />
                  <Point X="3.32814241691" Y="1.257445691619" Z="1.05" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.05" />
                  <Point X="3.271151337524" Y="1.316483958927" Z="1.05" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.05" />
                  <Point X="3.24090496664" Y="1.396907001822" Z="1.05" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.05" />
                  <Point X="3.246207526867" Y="1.477459187398" Z="1.05" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.05" />
                  <Point X="3.288982936159" Y="1.553495807999" Z="1.05" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.05" />
                  <Point X="3.782415926133" Y="1.944968742421" Z="1.05" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.05" />
                  <Point X="4.324739051546" Y="2.657714219641" Z="1.05" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.05" />
                  <Point X="4.099906271954" Y="2.992922033153" Z="1.05" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.05" />
                  <Point X="3.384584880707" Y="2.772011038185" Z="1.05" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.05" />
                  <Point X="2.462821003157" Y="2.254415135207" Z="1.05" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.05" />
                  <Point X="2.38890075727" Y="2.250435743483" Z="1.05" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.05" />
                  <Point X="2.323060652863" Y="2.279078553264" Z="1.05" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.05" />
                  <Point X="2.271680046971" Y="2.333964207519" Z="1.05" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.05" />
                  <Point X="2.248993959276" Y="2.400857686563" Z="1.05" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.05" />
                  <Point X="2.25811280745" Y="2.476648572725" Z="1.05" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.05" />
                  <Point X="2.623614314674" Y="3.127554201248" Z="1.05" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.05" />
                  <Point X="2.908758312501" Y="4.158619330295" Z="1.05" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.05" />
                  <Point X="2.520379651085" Y="4.404847197549" Z="1.05" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.05" />
                  <Point X="2.114441065762" Y="4.613611469329" Z="1.05" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.05" />
                  <Point X="1.693588371974" Y="4.784143924138" Z="1.05" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.05" />
                  <Point X="1.133314049041" Y="4.940859273257" Z="1.05" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.05" />
                  <Point X="0.470264185911" Y="5.04731167915" Z="1.05" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.05" />
                  <Point X="0.113263375226" Y="4.777829166123" Z="1.05" />
                  <Point X="0" Y="4.355124473572" Z="1.05" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>