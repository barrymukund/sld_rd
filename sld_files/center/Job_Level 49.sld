<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#213" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="3486" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="0" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000475830078" Y="0.004716064453" />
                  <Width Value="9.783895996094" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.563302124023" Y="-3.512524658203" />
                  <Point X="0.557721130371" Y="-3.497141601562" />
                  <Point X="0.542362976074" Y="-3.467376953125" />
                  <Point X="0.387577880859" Y="-3.244360839844" />
                  <Point X="0.378635284424" Y="-3.2314765625" />
                  <Point X="0.356751220703" Y="-3.209020996094" />
                  <Point X="0.330495574951" Y="-3.189777099609" />
                  <Point X="0.302494812012" Y="-3.175668945312" />
                  <Point X="0.062973945618" Y="-3.101330322266" />
                  <Point X="0.049135822296" Y="-3.097035644531" />
                  <Point X="0.020975667953" Y="-3.092766357422" />
                  <Point X="-0.008665962219" Y="-3.092766601562" />
                  <Point X="-0.03682460022" Y="-3.097035888672" />
                  <Point X="-0.276345611572" Y="-3.171374267578" />
                  <Point X="-0.29018371582" Y="-3.175669189453" />
                  <Point X="-0.31818572998" Y="-3.189778320312" />
                  <Point X="-0.344440765381" Y="-3.209022460938" />
                  <Point X="-0.366323608398" Y="-3.231477050781" />
                  <Point X="-0.521108825684" Y="-3.454492919922" />
                  <Point X="-0.525667114258" Y="-3.461736572266" />
                  <Point X="-0.541829772949" Y="-3.490188964844" />
                  <Point X="-0.550989990234" Y="-3.512524414063" />
                  <Point X="-0.57286907959" Y="-3.594177734375" />
                  <Point X="-0.916584472656" Y="-4.876941894531" />
                  <Point X="-1.067245605469" Y="-4.847698242188" />
                  <Point X="-1.07933996582" Y="-4.845350585938" />
                  <Point X="-1.24641784668" Y="-4.802362792969" />
                  <Point X="-1.214963012695" Y="-4.563438476562" />
                  <Point X="-1.214201171875" Y="-4.5479296875" />
                  <Point X="-1.216508789062" Y="-4.516223632812" />
                  <Point X="-1.273730224609" Y="-4.228551757813" />
                  <Point X="-1.277036254883" Y="-4.211932128906" />
                  <Point X="-1.287938720703" Y="-4.182965332031" />
                  <Point X="-1.304010620117" Y="-4.155128417969" />
                  <Point X="-1.323644775391" Y="-4.131204101562" />
                  <Point X="-1.544165405273" Y="-3.937812255859" />
                  <Point X="-1.556905639648" Y="-3.926639404297" />
                  <Point X="-1.583188842773" Y="-3.910295410156" />
                  <Point X="-1.612886352539" Y="-3.897994384766" />
                  <Point X="-1.643027709961" Y="-3.890966308594" />
                  <Point X="-1.935707763672" Y="-3.871782958984" />
                  <Point X="-1.952616821289" Y="-3.870674804688" />
                  <Point X="-1.983418579102" Y="-3.873708740234" />
                  <Point X="-2.014467041016" Y="-3.882028320312" />
                  <Point X="-2.042657836914" Y="-3.894801513672" />
                  <Point X="-2.286534423828" Y="-4.057755126953" />
                  <Point X="-2.292692138672" Y="-4.062236572266" />
                  <Point X="-2.318671142578" Y="-4.082786376953" />
                  <Point X="-2.335102783203" Y="-4.099462402344" />
                  <Point X="-2.347385986328" Y="-4.115470214844" />
                  <Point X="-2.480148681641" Y="-4.288490234375" />
                  <Point X="-2.783989746094" Y="-4.100359375" />
                  <Point X="-2.801708007812" Y="-4.089388671875" />
                  <Point X="-3.104721923828" Y="-3.856078125" />
                  <Point X="-2.423761230469" Y="-2.676619628906" />
                  <Point X="-2.412859130859" Y="-2.647653320312" />
                  <Point X="-2.406588134766" Y="-2.616126953125" />
                  <Point X="-2.405575439453" Y="-2.585192871094" />
                  <Point X="-2.414560058594" Y="-2.555575195312" />
                  <Point X="-2.428777099609" Y="-2.526746337891" />
                  <Point X="-2.4468046875" Y="-2.501589355469" />
                  <Point X="-2.463205810547" Y="-2.485188232422" />
                  <Point X="-2.464153320312" Y="-2.484240722656" />
                  <Point X="-2.489310302734" Y="-2.466213134766" />
                  <Point X="-2.518139160156" Y="-2.45199609375" />
                  <Point X="-2.547756835938" Y="-2.443011474609" />
                  <Point X="-2.578690917969" Y="-2.444024169922" />
                  <Point X="-2.610217285156" Y="-2.450295166016" />
                  <Point X="-2.63918359375" Y="-2.461197265625" />
                  <Point X="-2.709608154297" Y="-2.501856933594" />
                  <Point X="-3.818024414062" Y="-3.141801269531" />
                  <Point X="-4.068860839844" Y="-2.81225390625" />
                  <Point X="-4.082857666016" Y="-2.793864257812" />
                  <Point X="-4.306142578125" Y="-2.419449951172" />
                  <Point X="-3.105954833984" Y="-1.498513671875" />
                  <Point X="-3.084578613281" Y="-1.475595458984" />
                  <Point X="-3.066614257812" Y="-1.448466552734" />
                  <Point X="-3.053856933594" Y="-1.419834960938" />
                  <Point X="-3.046573242188" Y="-1.391712524414" />
                  <Point X="-3.04615234375" Y="-1.390087768555" />
                  <Point X="-3.043347900391" Y="-1.359661376953" />
                  <Point X="-3.045556152344" Y="-1.327988769531" />
                  <Point X="-3.052557373047" Y="-1.298241943359" />
                  <Point X="-3.068640625" Y="-1.272257080078" />
                  <Point X="-3.089474609375" Y="-1.248299438477" />
                  <Point X="-3.112973388672" Y="-1.228766601562" />
                  <Point X="-3.138009521484" Y="-1.21403125" />
                  <Point X="-3.139456054688" Y="-1.213180053711" />
                  <Point X="-3.168720947266" Y="-1.201955566406" />
                  <Point X="-3.200607421875" Y="-1.195474487305" />
                  <Point X="-3.231929199219" Y="-1.194383789062" />
                  <Point X="-3.320833496094" Y="-1.206088256836" />
                  <Point X="-4.732102050781" Y="-1.391885253906" />
                  <Point X="-4.8286015625" Y="-1.014094238281" />
                  <Point X="-4.834076171875" Y="-0.992661315918" />
                  <Point X="-4.892423828125" Y="-0.584698242188" />
                  <Point X="-3.532875976562" Y="-0.220408340454" />
                  <Point X="-3.517493896484" Y="-0.214827758789" />
                  <Point X="-3.487729248047" Y="-0.199469909668" />
                  <Point X="-3.4614921875" Y="-0.181259918213" />
                  <Point X="-3.459976318359" Y="-0.180207870483" />
                  <Point X="-3.43751953125" Y="-0.158321960449" />
                  <Point X="-3.418273681641" Y="-0.13206237793" />
                  <Point X="-3.404167236328" Y="-0.104063728333" />
                  <Point X="-3.395421386719" Y="-0.075884597778" />
                  <Point X="-3.394916259766" Y="-0.074256645203" />
                  <Point X="-3.390646728516" Y="-0.046096946716" />
                  <Point X="-3.390646728516" Y="-0.016463209152" />
                  <Point X="-3.394916259766" Y="0.011696643829" />
                  <Point X="-3.403659179688" Y="0.039866313934" />
                  <Point X="-3.404164306641" Y="0.041494464874" />
                  <Point X="-3.418273681641" Y="0.069502220154" />
                  <Point X="-3.43751953125" Y="0.095761810303" />
                  <Point X="-3.459976318359" Y="0.117647712708" />
                  <Point X="-3.486213378906" Y="0.135857696533" />
                  <Point X="-3.498978759766" Y="0.143317138672" />
                  <Point X="-3.532875976562" Y="0.157848342896" />
                  <Point X="-3.613916503906" Y="0.179563018799" />
                  <Point X="-4.89181640625" Y="0.521975158691" />
                  <Point X="-4.828015625" Y="0.953134094238" />
                  <Point X="-4.824487792969" Y="0.976976074219" />
                  <Point X="-4.703551269531" Y="1.423267944336" />
                  <Point X="-3.765666503906" Y="1.29979309082" />
                  <Point X="-3.744987304688" Y="1.299341552734" />
                  <Point X="-3.723426757812" Y="1.301227783203" />
                  <Point X="-3.703138671875" Y="1.305263305664" />
                  <Point X="-3.645067626953" Y="1.323573120117" />
                  <Point X="-3.641712646484" Y="1.324630737305" />
                  <Point X="-3.62278125" Y="1.332960571289" />
                  <Point X="-3.604036376953" Y="1.343782470703" />
                  <Point X="-3.587354980469" Y="1.356013427734" />
                  <Point X="-3.573716064453" Y="1.371565185547" />
                  <Point X="-3.561301025391" Y="1.389295288086" />
                  <Point X="-3.551351318359" Y="1.407431152344" />
                  <Point X="-3.528050048828" Y="1.463685424805" />
                  <Point X="-3.526703857422" Y="1.466935302734" />
                  <Point X="-3.520915771484" Y="1.486793701172" />
                  <Point X="-3.517157470703" Y="1.508108154297" />
                  <Point X="-3.515804443359" Y="1.528747680664" />
                  <Point X="-3.518950683594" Y="1.549190673828" />
                  <Point X="-3.524552001953" Y="1.570096801758" />
                  <Point X="-3.532049316406" Y="1.589376831055" />
                  <Point X="-3.560164794922" Y="1.643386230469" />
                  <Point X="-3.5617890625" Y="1.646506591797" />
                  <Point X="-3.573280517578" Y="1.663704956055" />
                  <Point X="-3.587193603516" Y="1.680286010742" />
                  <Point X="-3.602135986328" Y="1.694590332031" />
                  <Point X="-3.64862109375" Y="1.730259521484" />
                  <Point X="-4.351860351563" Y="2.269874023438" />
                  <Point X="-4.094860107422" Y="2.710177001953" />
                  <Point X="-4.081153564453" Y="2.733659667969" />
                  <Point X="-3.750504882813" Y="3.158661865234" />
                  <Point X="-3.206656982422" Y="2.844670898438" />
                  <Point X="-3.187723388672" Y="2.836340087891" />
                  <Point X="-3.167080322266" Y="2.829831542969" />
                  <Point X="-3.146793945312" Y="2.825796386719" />
                  <Point X="-3.065916992188" Y="2.818720458984" />
                  <Point X="-3.061244628906" Y="2.818311767578" />
                  <Point X="-3.040564453125" Y="2.818762939453" />
                  <Point X="-3.019106933594" Y="2.821587890625" />
                  <Point X="-2.999016113281" Y="2.82650390625" />
                  <Point X="-2.980465332031" Y="2.835651855469" />
                  <Point X="-2.962211914062" Y="2.847280029297" />
                  <Point X="-2.946078613281" Y="2.860228271484" />
                  <Point X="-2.888671630859" Y="2.917635253906" />
                  <Point X="-2.885354980469" Y="2.920951660156" />
                  <Point X="-2.872409423828" Y="2.937080810547" />
                  <Point X="-2.860779296875" Y="2.955335205078" />
                  <Point X="-2.851629882812" Y="2.97388671875" />
                  <Point X="-2.846712890625" Y="2.993978759766" />
                  <Point X="-2.843887207031" Y="3.015437744141" />
                  <Point X="-2.843435791016" Y="3.036120117188" />
                  <Point X="-2.85051171875" Y="3.116997070313" />
                  <Point X="-2.850920410156" Y="3.121669677734" />
                  <Point X="-2.854955322266" Y="3.141955566406" />
                  <Point X="-2.861463867188" Y="3.162598876953" />
                  <Point X="-2.869794921875" Y="3.181532958984" />
                  <Point X="-2.890393798828" Y="3.217211181641" />
                  <Point X="-3.183332763672" Y="3.724596679688" />
                  <Point X="-2.724492431641" Y="4.076385009766" />
                  <Point X="-2.700623535156" Y="4.094685302734" />
                  <Point X="-2.167036865234" Y="4.391134765625" />
                  <Point X="-2.043195556641" Y="4.229741210938" />
                  <Point X="-2.028892944336" Y="4.214800292969" />
                  <Point X="-2.012315063477" Y="4.200889160156" />
                  <Point X="-1.995115234375" Y="4.189395996094" />
                  <Point X="-1.905099731445" Y="4.142536621094" />
                  <Point X="-1.899899291992" Y="4.139829101563" />
                  <Point X="-1.880624389648" Y="4.132333496094" />
                  <Point X="-1.859716918945" Y="4.12673046875" />
                  <Point X="-1.839271972656" Y="4.123583007812" />
                  <Point X="-1.818630371094" Y="4.124935058594" />
                  <Point X="-1.797314086914" Y="4.128693359375" />
                  <Point X="-1.777453979492" Y="4.134481933594" />
                  <Point X="-1.683696777344" Y="4.173317871094" />
                  <Point X="-1.678280029297" Y="4.175561523437" />
                  <Point X="-1.660146240234" Y="4.185510253906" />
                  <Point X="-1.642416748047" Y="4.197924316406" />
                  <Point X="-1.626865356445" Y="4.211562011719" />
                  <Point X="-1.614634277344" Y="4.228242675781" />
                  <Point X="-1.603812133789" Y="4.246986328125" />
                  <Point X="-1.59548034668" Y="4.265920898438" />
                  <Point X="-1.564964355469" Y="4.362706054688" />
                  <Point X="-1.563201049805" Y="4.368297363281" />
                  <Point X="-1.559165893555" Y="4.388583984375" />
                  <Point X="-1.557279296875" Y="4.410146484375" />
                  <Point X="-1.557730224609" Y="4.430826171875" />
                  <Point X="-1.560072143555" Y="4.448614746094" />
                  <Point X="-1.584201782227" Y="4.631897949219" />
                  <Point X="-0.980532531738" Y="4.801145996094" />
                  <Point X="-0.949635253906" Y="4.80980859375" />
                  <Point X="-0.294710968018" Y="4.886457519531" />
                  <Point X="-0.133903213501" Y="4.286315429688" />
                  <Point X="-0.121129745483" Y="4.258124023438" />
                  <Point X="-0.103271606445" Y="4.231397460938" />
                  <Point X="-0.0821145401" Y="4.20880859375" />
                  <Point X="-0.054819351196" Y="4.19421875" />
                  <Point X="-0.024381277084" Y="4.183886230469" />
                  <Point X="0.006156007767" Y="4.178844238281" />
                  <Point X="0.036693260193" Y="4.183886230469" />
                  <Point X="0.067131332397" Y="4.19421875" />
                  <Point X="0.094426460266" Y="4.20880859375" />
                  <Point X="0.115583435059" Y="4.231397460938" />
                  <Point X="0.133441726685" Y="4.258124023438" />
                  <Point X="0.14621534729" Y="4.286315917969" />
                  <Point X="0.156769943237" Y="4.325706542969" />
                  <Point X="0.307419189453" Y="4.8879375" />
                  <Point X="0.817054992676" Y="4.834564941406" />
                  <Point X="0.844040649414" Y="4.831738769531" />
                  <Point X="1.453133544922" Y="4.684685058594" />
                  <Point X="1.481025634766" Y="4.677951171875" />
                  <Point X="1.877408569336" Y="4.534180175781" />
                  <Point X="1.894649658203" Y="4.527926757812" />
                  <Point X="2.27800390625" Y="4.34864453125" />
                  <Point X="2.294571533203" Y="4.340896484375" />
                  <Point X="2.664937255859" Y="4.125120605469" />
                  <Point X="2.680977539062" Y="4.115775390625" />
                  <Point X="2.943260253906" Y="3.929254394531" />
                  <Point X="2.147581054688" Y="2.55109765625" />
                  <Point X="2.142075683594" Y="2.539930175781" />
                  <Point X="2.133076660156" Y="2.516055908203" />
                  <Point X="2.112779541016" Y="2.440154785156" />
                  <Point X="2.110102050781" Y="2.425796630859" />
                  <Point X="2.107591552734" Y="2.402512451172" />
                  <Point X="2.107727539062" Y="2.380955566406" />
                  <Point X="2.115641845703" Y="2.315322753906" />
                  <Point X="2.116098876953" Y="2.311530761719" />
                  <Point X="2.12144140625" Y="2.289608154297" />
                  <Point X="2.129708984375" Y="2.267515136719" />
                  <Point X="2.140071777344" Y="2.247469726562" />
                  <Point X="2.180683105469" Y="2.187618896484" />
                  <Point X="2.189857666016" Y="2.176125244141" />
                  <Point X="2.205503417969" Y="2.159368896484" />
                  <Point X="2.221599121094" Y="2.145592285156" />
                  <Point X="2.281449951172" Y="2.104980957031" />
                  <Point X="2.284907714844" Y="2.102634765625" />
                  <Point X="2.304951660156" Y="2.092272460938" />
                  <Point X="2.3270390625" Y="2.084006591797" />
                  <Point X="2.348963134766" Y="2.078663574219" />
                  <Point X="2.414596191406" Y="2.070749267578" />
                  <Point X="2.429651611328" Y="2.070137207031" />
                  <Point X="2.452346679688" Y="2.071017578125" />
                  <Point X="2.473206298828" Y="2.074171142578" />
                  <Point X="2.549107666016" Y="2.094468261719" />
                  <Point X="2.557915283203" Y="2.097289306641" />
                  <Point X="2.588533935547" Y="2.110145019531" />
                  <Point X="2.670044921875" Y="2.157205566406" />
                  <Point X="3.967326416016" Y="2.906190917969" />
                  <Point X="4.113756347656" Y="2.702686767578" />
                  <Point X="4.12326953125" Y="2.689465332031" />
                  <Point X="4.262198730469" Y="2.459884033203" />
                  <Point X="3.230783935547" Y="1.668451538086" />
                  <Point X="3.221423828125" Y="1.660240722656" />
                  <Point X="3.203974121094" Y="1.641628051758" />
                  <Point X="3.149347900391" Y="1.570363769531" />
                  <Point X="3.141567626953" Y="1.558465454102" />
                  <Point X="3.129941650391" Y="1.537395507812" />
                  <Point X="3.121629638672" Y="1.517085571289" />
                  <Point X="3.10128125" Y="1.444324707031" />
                  <Point X="3.100105712891" Y="1.44012097168" />
                  <Point X="3.09665234375" Y="1.417820556641" />
                  <Point X="3.095836669922" Y="1.394250366211" />
                  <Point X="3.097739746094" Y="1.371767456055" />
                  <Point X="3.114443603516" Y="1.290811645508" />
                  <Point X="3.118390869141" Y="1.277031860352" />
                  <Point X="3.126554443359" Y="1.2549765625" />
                  <Point X="3.136283203125" Y="1.235739013672" />
                  <Point X="3.181715820312" Y="1.166683349609" />
                  <Point X="3.184340820312" Y="1.162693603516" />
                  <Point X="3.198895751953" Y="1.145447631836" />
                  <Point X="3.216139160156" Y="1.129359008789" />
                  <Point X="3.234347412109" Y="1.116034667969" />
                  <Point X="3.300185791016" Y="1.078973388672" />
                  <Point X="3.31353125" Y="1.072769287109" />
                  <Point X="3.335310058594" Y="1.064630493164" />
                  <Point X="3.356118164062" Y="1.059438598633" />
                  <Point X="3.445135986328" Y="1.047673706055" />
                  <Point X="3.454024658203" Y="1.046921508789" />
                  <Point X="3.488203613281" Y="1.04698449707" />
                  <Point X="3.565633789063" Y="1.057178344727" />
                  <Point X="4.776839355469" Y="1.21663659668" />
                  <Point X="4.841849121094" Y="0.949595275879" />
                  <Point X="4.845937011719" Y="0.932804016113" />
                  <Point X="4.890865234375" Y="0.644238586426" />
                  <Point X="3.716579833984" Y="0.329589904785" />
                  <Point X="3.704791259766" Y="0.325586608887" />
                  <Point X="3.681545654297" Y="0.315067962646" />
                  <Point X="3.594088378906" Y="0.264516143799" />
                  <Point X="3.582637695312" Y="0.256731903076" />
                  <Point X="3.563272216797" Y="0.241390136719" />
                  <Point X="3.547530273438" Y="0.22557585144" />
                  <Point X="3.495055908203" Y="0.158711105347" />
                  <Point X="3.492024414062" Y="0.154848205566" />
                  <Point X="3.480300292969" Y="0.135568237305" />
                  <Point X="3.470527587891" Y="0.114107048035" />
                  <Point X="3.463681152344" Y="0.092605781555" />
                  <Point X="3.446189697266" Y="0.001271982193" />
                  <Point X="3.444577636719" Y="-0.012614411354" />
                  <Point X="3.443566894531" Y="-0.03670400238" />
                  <Point X="3.445178955078" Y="-0.058555294037" />
                  <Point X="3.462670410156" Y="-0.149889099121" />
                  <Point X="3.463681152344" Y="-0.155165939331" />
                  <Point X="3.470527587891" Y="-0.176667358398" />
                  <Point X="3.480300292969" Y="-0.198128387451" />
                  <Point X="3.492024414062" Y="-0.217408203125" />
                  <Point X="3.544498779297" Y="-0.284272796631" />
                  <Point X="3.55420703125" Y="-0.294880645752" />
                  <Point X="3.571551269531" Y="-0.311165039062" />
                  <Point X="3.589035888672" Y="-0.324155700684" />
                  <Point X="3.676493164063" Y="-0.374707519531" />
                  <Point X="3.684139404297" Y="-0.378676086426" />
                  <Point X="3.716580078125" Y="-0.392149932861" />
                  <Point X="3.787587158203" Y="-0.411176239014" />
                  <Point X="4.891472167969" Y="-0.706961303711" />
                  <Point X="4.8573046875" Y="-0.933589477539" />
                  <Point X="4.855021484375" Y="-0.948731872559" />
                  <Point X="4.801174316406" Y="-1.18469909668" />
                  <Point X="3.424382080078" Y="-1.003440979004" />
                  <Point X="3.40803515625" Y="-1.002710266113" />
                  <Point X="3.374658447266" Y="-1.005508666992" />
                  <Point X="3.203010742188" Y="-1.042817138672" />
                  <Point X="3.193094238281" Y="-1.044972412109" />
                  <Point X="3.163973632812" Y="-1.056597167969" />
                  <Point X="3.136146972656" Y="-1.073489624023" />
                  <Point X="3.112397216797" Y="-1.093960327148" />
                  <Point X="3.008646972656" Y="-1.218739379883" />
                  <Point X="3.002653076172" Y="-1.225948242188" />
                  <Point X="2.987932128906" Y="-1.250330932617" />
                  <Point X="2.976589111328" Y="-1.277715820312" />
                  <Point X="2.969757568359" Y="-1.305365356445" />
                  <Point X="2.954887695312" Y="-1.466959716797" />
                  <Point X="2.954028564453" Y="-1.476295410156" />
                  <Point X="2.956347412109" Y="-1.507563964844" />
                  <Point X="2.964079101562" Y="-1.539185546875" />
                  <Point X="2.976450439453" Y="-1.567996826172" />
                  <Point X="3.071442626953" Y="-1.715750976562" />
                  <Point X="3.077471435547" Y="-1.724096679688" />
                  <Point X="3.094579345703" Y="-1.745261230469" />
                  <Point X="3.110628417969" Y="-1.760909301758" />
                  <Point X="3.1765234375" Y="-1.811472290039" />
                  <Point X="4.213122070312" Y="-2.6068828125" />
                  <Point X="4.131241699219" Y="-2.739377929688" />
                  <Point X="4.124807617188" Y="-2.749789550781" />
                  <Point X="4.028981445312" Y="-2.885945068359" />
                  <Point X="2.800954589844" Y="-2.176943115234" />
                  <Point X="2.786130859375" Y="-2.170011962891" />
                  <Point X="2.754224121094" Y="-2.159825195312" />
                  <Point X="2.549936035156" Y="-2.122930908203" />
                  <Point X="2.538133789062" Y="-2.120799560547" />
                  <Point X="2.506781738281" Y="-2.120395263672" />
                  <Point X="2.474609863281" Y="-2.125352783203" />
                  <Point X="2.444833740234" Y="-2.135176513672" />
                  <Point X="2.275120605469" Y="-2.224495361328" />
                  <Point X="2.265315673828" Y="-2.229655517578" />
                  <Point X="2.242385009766" Y="-2.246548583984" />
                  <Point X="2.221424560547" Y="-2.267509033203" />
                  <Point X="2.204531494141" Y="-2.290439697266" />
                  <Point X="2.115212646484" Y="-2.460152832031" />
                  <Point X="2.110052490234" Y="-2.469957763672" />
                  <Point X="2.100228759766" Y="-2.499734863281" />
                  <Point X="2.095271484375" Y="-2.531907470703" />
                  <Point X="2.09567578125" Y="-2.563258544922" />
                  <Point X="2.132569824219" Y="-2.767546386719" />
                  <Point X="2.134762939453" Y="-2.776937011719" />
                  <Point X="2.142797119141" Y="-2.804853027344" />
                  <Point X="2.151819335938" Y="-2.826078857422" />
                  <Point X="2.194163574219" Y="-2.899420898438" />
                  <Point X="2.861283447266" Y="-4.05490625" />
                  <Point X="2.789486083984" Y="-4.106189453125" />
                  <Point X="2.781843994141" Y="-4.111647949219" />
                  <Point X="2.701765136719" Y="-4.163481445312" />
                  <Point X="1.758546264648" Y="-2.934255371094" />
                  <Point X="1.747504150391" Y="-2.922179931641" />
                  <Point X="1.721924072266" Y="-2.900557128906" />
                  <Point X="1.520441040039" Y="-2.771021972656" />
                  <Point X="1.50880065918" Y="-2.763538330078" />
                  <Point X="1.479988647461" Y="-2.751166748047" />
                  <Point X="1.4483671875" Y="-2.743435302734" />
                  <Point X="1.417099121094" Y="-2.741116699219" />
                  <Point X="1.196743041992" Y="-2.761394042969" />
                  <Point X="1.184012451172" Y="-2.762565673828" />
                  <Point X="1.156362670898" Y="-2.769397460938" />
                  <Point X="1.128978149414" Y="-2.780740478516" />
                  <Point X="1.104596069336" Y="-2.7954609375" />
                  <Point X="0.934442565918" Y="-2.936937744141" />
                  <Point X="0.924612304688" Y="-2.945111328125" />
                  <Point X="0.904141418457" Y="-2.968861572266" />
                  <Point X="0.887248779297" Y="-2.996688476562" />
                  <Point X="0.875624267578" Y="-3.025808837891" />
                  <Point X="0.824749267578" Y="-3.259873779297" />
                  <Point X="0.823248352051" Y="-3.268817382812" />
                  <Point X="0.819595947266" Y="-3.299486328125" />
                  <Point X="0.819742126465" Y="-3.323120117188" />
                  <Point X="0.831742248535" Y="-3.414269042969" />
                  <Point X="1.022065307617" Y="-4.859915039062" />
                  <Point X="0.98289831543" Y="-4.868500488281" />
                  <Point X="0.97567956543" Y="-4.870083007812" />
                  <Point X="0.929315490723" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.049143798828" Y="-4.754438964844" />
                  <Point X="-1.058432861328" Y="-4.752635742188" />
                  <Point X="-1.141246337891" Y="-4.731328613281" />
                  <Point X="-1.120775634766" Y="-4.575838378906" />
                  <Point X="-1.120077514648" Y="-4.568099609375" />
                  <Point X="-1.119451782227" Y="-4.541033691406" />
                  <Point X="-1.121759399414" Y="-4.509327636719" />
                  <Point X="-1.123334228516" Y="-4.497689941406" />
                  <Point X="-1.180555664062" Y="-4.210018066406" />
                  <Point X="-1.188125366211" Y="-4.178467773438" />
                  <Point X="-1.199027832031" Y="-4.149500976562" />
                  <Point X="-1.205666503906" Y="-4.13546484375" />
                  <Point X="-1.221738525391" Y="-4.107627929688" />
                  <Point X="-1.230574584961" Y="-4.094861083984" />
                  <Point X="-1.250208740234" Y="-4.070936767578" />
                  <Point X="-1.261006835938" Y="-4.059779296875" />
                  <Point X="-1.48152746582" Y="-3.866387451172" />
                  <Point X="-1.506739135742" Y="-3.845965332031" />
                  <Point X="-1.533022338867" Y="-3.829621337891" />
                  <Point X="-1.546834106445" Y="-3.822526855469" />
                  <Point X="-1.576531616211" Y="-3.810225830078" />
                  <Point X="-1.591313842773" Y="-3.805476074219" />
                  <Point X="-1.621455200195" Y="-3.798447998047" />
                  <Point X="-1.636814331055" Y="-3.796169677734" />
                  <Point X="-1.929494384766" Y="-3.776986328125" />
                  <Point X="-1.961929199219" Y="-3.776132324219" />
                  <Point X="-1.992730957031" Y="-3.779166259766" />
                  <Point X="-2.008006835938" Y="-3.781945800781" />
                  <Point X="-2.039055297852" Y="-3.790265380859" />
                  <Point X="-2.053674560547" Y="-3.795496337891" />
                  <Point X="-2.081865234375" Y="-3.80826953125" />
                  <Point X="-2.095437011719" Y="-3.815812011719" />
                  <Point X="-2.339313720703" Y="-3.978765625" />
                  <Point X="-2.351629150391" Y="-3.987728515625" />
                  <Point X="-2.377608154297" Y="-4.008278320312" />
                  <Point X="-2.386340332031" Y="-4.016108886719" />
                  <Point X="-2.402771972656" Y="-4.032784912109" />
                  <Point X="-2.410471435547" Y="-4.041630126953" />
                  <Point X="-2.422754638672" Y="-4.057637939453" />
                  <Point X="-2.503202392578" Y="-4.162479980469" />
                  <Point X="-2.733978515625" Y="-4.019588867188" />
                  <Point X="-2.747584228516" Y="-4.011164550781" />
                  <Point X="-2.980862792969" Y="-3.831547851562" />
                  <Point X="-2.341488769531" Y="-2.724119628906" />
                  <Point X="-2.334850097656" Y="-2.710083251953" />
                  <Point X="-2.323947998047" Y="-2.681116943359" />
                  <Point X="-2.319684570312" Y="-2.666187011719" />
                  <Point X="-2.313413574219" Y="-2.634660644531" />
                  <Point X="-2.311638916016" Y="-2.619235351562" />
                  <Point X="-2.310626220703" Y="-2.588301269531" />
                  <Point X="-2.314666259766" Y="-2.557615234375" />
                  <Point X="-2.323650878906" Y="-2.527997558594" />
                  <Point X="-2.329357421875" Y="-2.513557128906" />
                  <Point X="-2.343574462891" Y="-2.484728271484" />
                  <Point X="-2.351557128906" Y="-2.47141015625" />
                  <Point X="-2.369584716797" Y="-2.446253173828" />
                  <Point X="-2.379629638672" Y="-2.434414306641" />
                  <Point X="-2.396030761719" Y="-2.418013183594" />
                  <Point X="-2.408817138672" Y="-2.407020751953" />
                  <Point X="-2.433974121094" Y="-2.388993164062" />
                  <Point X="-2.447292236328" Y="-2.381010498047" />
                  <Point X="-2.47612109375" Y="-2.366793457031" />
                  <Point X="-2.490561523438" Y="-2.361086914062" />
                  <Point X="-2.520179199219" Y="-2.352102294922" />
                  <Point X="-2.550865234375" Y="-2.348062255859" />
                  <Point X="-2.581799316406" Y="-2.349074951172" />
                  <Point X="-2.597224609375" Y="-2.350849609375" />
                  <Point X="-2.628750976562" Y="-2.357120605469" />
                  <Point X="-2.643680908203" Y="-2.361384033203" />
                  <Point X="-2.672647216797" Y="-2.372286132812" />
                  <Point X="-2.68668359375" Y="-2.378924804688" />
                  <Point X="-2.757108154297" Y="-2.419584472656" />
                  <Point X="-3.793089355469" Y="-3.017708496094" />
                  <Point X="-3.993267578125" Y="-2.754715576172" />
                  <Point X="-4.004013671875" Y="-2.740596923828" />
                  <Point X="-4.181265136719" Y="-2.443373291016" />
                  <Point X="-3.048122558594" Y="-1.573882202148" />
                  <Point X="-3.036483154297" Y="-1.563311035156" />
                  <Point X="-3.015106933594" Y="-1.540392822266" />
                  <Point X="-3.005370361328" Y="-1.528046020508" />
                  <Point X="-2.987406005859" Y="-1.500917114258" />
                  <Point X="-2.979838378906" Y="-1.487131103516" />
                  <Point X="-2.967081054688" Y="-1.458499511719" />
                  <Point X="-2.961891357422" Y="-1.443653930664" />
                  <Point X="-2.954607666016" Y="-1.415531494141" />
                  <Point X="-2.951553222656" Y="-1.398807128906" />
                  <Point X="-2.948748779297" Y="-1.368380737305" />
                  <Point X="-2.948577880859" Y="-1.353053955078" />
                  <Point X="-2.950786132812" Y="-1.321381469727" />
                  <Point X="-2.953082763672" Y="-1.306224243164" />
                  <Point X="-2.960083984375" Y="-1.276477416992" />
                  <Point X="-2.971778564453" Y="-1.248244140625" />
                  <Point X="-2.987861816406" Y="-1.222259277344" />
                  <Point X="-2.996955078125" Y="-1.20991796875" />
                  <Point X="-3.0177890625" Y="-1.185960449219" />
                  <Point X="-3.028747802734" Y="-1.175242797852" />
                  <Point X="-3.052246582031" Y="-1.155709960938" />
                  <Point X="-3.064786621094" Y="-1.14689465332" />
                  <Point X="-3.089822753906" Y="-1.132159301758" />
                  <Point X="-3.105435546875" Y="-1.12448059082" />
                  <Point X="-3.134700439453" Y="-1.113256103516" />
                  <Point X="-3.149798583984" Y="-1.108859130859" />
                  <Point X="-3.181685058594" Y="-1.102378051758" />
                  <Point X="-3.197301269531" Y="-1.100531982422" />
                  <Point X="-3.228623046875" Y="-1.09944140625" />
                  <Point X="-3.244329101562" Y="-1.100196533203" />
                  <Point X="-3.333233398438" Y="-1.111901000977" />
                  <Point X="-4.660920898438" Y="-1.286694335938" />
                  <Point X="-4.736556640625" Y="-0.990583251953" />
                  <Point X="-4.740760742188" Y="-0.974123535156" />
                  <Point X="-4.786452148438" Y="-0.654654418945" />
                  <Point X="-3.508288085938" Y="-0.312171325684" />
                  <Point X="-3.5004765625" Y="-0.309712799072" />
                  <Point X="-3.473933105469" Y="-0.299251983643" />
                  <Point X="-3.444168457031" Y="-0.283894195557" />
                  <Point X="-3.433562011719" Y="-0.277514251709" />
                  <Point X="-3.407324951172" Y="-0.259304229736" />
                  <Point X="-3.393671386719" Y="-0.248242218018" />
                  <Point X="-3.371214599609" Y="-0.226356307983" />
                  <Point X="-3.360895507812" Y="-0.214480300903" />
                  <Point X="-3.341649658203" Y="-0.188220718384" />
                  <Point X="-3.333433349609" Y="-0.1748072052" />
                  <Point X="-3.319326904297" Y="-0.146808441162" />
                  <Point X="-3.313436767578" Y="-0.132223480225" />
                  <Point X="-3.304690917969" Y="-0.104044281006" />
                  <Point X="-3.300989746094" Y="-0.088497581482" />
                  <Point X="-3.296720214844" Y="-0.060338001251" />
                  <Point X="-3.295646728516" Y="-0.046096954346" />
                  <Point X="-3.295646728516" Y="-0.016463186264" />
                  <Point X="-3.296720214844" Y="-0.002222288847" />
                  <Point X="-3.300989746094" Y="0.025937587738" />
                  <Point X="-3.304185791016" Y="0.03985641861" />
                  <Point X="-3.312925537109" Y="0.068015945435" />
                  <Point X="-3.319322021484" Y="0.084235214233" />
                  <Point X="-3.333431396484" Y="0.112243049622" />
                  <Point X="-3.341649658203" Y="0.125660575867" />
                  <Point X="-3.360895507812" Y="0.151920166016" />
                  <Point X="-3.371214599609" Y="0.163796173096" />
                  <Point X="-3.393671386719" Y="0.18568208313" />
                  <Point X="-3.405809082031" Y="0.195692123413" />
                  <Point X="-3.432046142578" Y="0.213902008057" />
                  <Point X="-3.438283447266" Y="0.217880340576" />
                  <Point X="-3.461548095703" Y="0.230632339478" />
                  <Point X="-3.4954453125" Y="0.245163497925" />
                  <Point X="-3.508288330078" Y="0.249611328125" />
                  <Point X="-3.589328857422" Y="0.271326019287" />
                  <Point X="-4.785446289062" Y="0.591824645996" />
                  <Point X="-4.7340390625" Y="0.939227966309" />
                  <Point X="-4.731330566406" Y="0.957532714844" />
                  <Point X="-4.633586425781" Y="1.318237182617" />
                  <Point X="-3.77806640625" Y="1.205605834961" />
                  <Point X="-3.767740478516" Y="1.204815673828" />
                  <Point X="-3.747061279297" Y="1.204364257812" />
                  <Point X="-3.736707763672" Y="1.204703125" />
                  <Point X="-3.715147216797" Y="1.206589233398" />
                  <Point X="-3.704893310547" Y="1.208053222656" />
                  <Point X="-3.684605224609" Y="1.212088623047" />
                  <Point X="-3.674571533203" Y="1.214660400391" />
                  <Point X="-3.616500488281" Y="1.232970092773" />
                  <Point X="-3.603452392578" Y="1.23767590332" />
                  <Point X="-3.584520996094" Y="1.246005615234" />
                  <Point X="-3.575282714844" Y="1.250687255859" />
                  <Point X="-3.556537841797" Y="1.261509155273" />
                  <Point X="-3.547863037109" Y="1.267169433594" />
                  <Point X="-3.531181640625" Y="1.279400390625" />
                  <Point X="-3.515931152344" Y="1.293374511719" />
                  <Point X="-3.502292236328" Y="1.308926147461" />
                  <Point X="-3.495897216797" Y="1.317074584961" />
                  <Point X="-3.483482177734" Y="1.3348046875" />
                  <Point X="-3.478011962891" Y="1.343601318359" />
                  <Point X="-3.468062255859" Y="1.361737182617" />
                  <Point X="-3.463582763672" Y="1.371076293945" />
                  <Point X="-3.440281494141" Y="1.427330444336" />
                  <Point X="-3.435499023438" Y="1.440352050781" />
                  <Point X="-3.4297109375" Y="1.460210449219" />
                  <Point X="-3.427359130859" Y="1.470297119141" />
                  <Point X="-3.423600830078" Y="1.491611572266" />
                  <Point X="-3.422360839844" Y="1.501893920898" />
                  <Point X="-3.4210078125" Y="1.522533325195" />
                  <Point X="-3.421909912109" Y="1.543198364258" />
                  <Point X="-3.425056152344" Y="1.563641357422" />
                  <Point X="-3.427187255859" Y="1.573776611328" />
                  <Point X="-3.432788574219" Y="1.594682739258" />
                  <Point X="-3.436010742188" Y="1.60452734375" />
                  <Point X="-3.443508056641" Y="1.623807495117" />
                  <Point X="-3.447783203125" Y="1.633242919922" />
                  <Point X="-3.475898681641" Y="1.687252319336" />
                  <Point X="-3.482799072266" Y="1.699285400391" />
                  <Point X="-3.494290527344" Y="1.716483764648" />
                  <Point X="-3.500506103516" Y="1.72476965332" />
                  <Point X="-3.514419189453" Y="1.741350708008" />
                  <Point X="-3.521499511719" Y="1.748910522461" />
                  <Point X="-3.536441894531" Y="1.76321472168" />
                  <Point X="-3.544303710938" Y="1.769958984375" />
                  <Point X="-3.590788818359" Y="1.805628173828" />
                  <Point X="-4.227614746094" Y="2.294281738281" />
                  <Point X="-4.012813720703" Y="2.662287353516" />
                  <Point X="-4.002292236328" Y="2.680313232422" />
                  <Point X="-3.726338623047" Y="3.035012695312" />
                  <Point X="-3.254156982422" Y="2.7623984375" />
                  <Point X="-3.244917236328" Y="2.757716064453" />
                  <Point X="-3.225983642578" Y="2.749385253906" />
                  <Point X="-3.216289794922" Y="2.745736816406" />
                  <Point X="-3.195646728516" Y="2.739228271484" />
                  <Point X="-3.185613769531" Y="2.736656982422" />
                  <Point X="-3.165327392578" Y="2.732621826172" />
                  <Point X="-3.155073974609" Y="2.731157958984" />
                  <Point X="-3.074197021484" Y="2.72408203125" />
                  <Point X="-3.059172607422" Y="2.723334472656" />
                  <Point X="-3.038492431641" Y="2.723785644531" />
                  <Point X="-3.028164306641" Y="2.724575683594" />
                  <Point X="-3.006706787109" Y="2.727400634766" />
                  <Point X="-2.996527587891" Y="2.729310302734" />
                  <Point X="-2.976436767578" Y="2.734226318359" />
                  <Point X="-2.956999755859" Y="2.741300537109" />
                  <Point X="-2.938448974609" Y="2.750448486328" />
                  <Point X="-2.929423583984" Y="2.755528564453" />
                  <Point X="-2.911170166016" Y="2.767156738281" />
                  <Point X="-2.902749511719" Y="2.773190673828" />
                  <Point X="-2.886616210938" Y="2.786138916016" />
                  <Point X="-2.878903564453" Y="2.793053222656" />
                  <Point X="-2.821496582031" Y="2.850460205078" />
                  <Point X="-2.811267089844" Y="2.861487304688" />
                  <Point X="-2.798321533203" Y="2.877616455078" />
                  <Point X="-2.792288818359" Y="2.886034912109" />
                  <Point X="-2.780658691406" Y="2.904289306641" />
                  <Point X="-2.775577880859" Y="2.913314697266" />
                  <Point X="-2.766428466797" Y="2.931866210938" />
                  <Point X="-2.759353027344" Y="2.951304443359" />
                  <Point X="-2.754436035156" Y="2.971396484375" />
                  <Point X="-2.752525878906" Y="2.981576416016" />
                  <Point X="-2.749700195312" Y="3.003035400391" />
                  <Point X="-2.748909912109" Y="3.013364746094" />
                  <Point X="-2.748458496094" Y="3.034047119141" />
                  <Point X="-2.748797363281" Y="3.044400146484" />
                  <Point X="-2.755873291016" Y="3.125277099609" />
                  <Point X="-2.757745605469" Y="3.140202392578" />
                  <Point X="-2.761780517578" Y="3.16048828125" />
                  <Point X="-2.764351806641" Y="3.170521484375" />
                  <Point X="-2.770860351562" Y="3.191164794922" />
                  <Point X="-2.774509033203" Y="3.200859375" />
                  <Point X="-2.782840087891" Y="3.219793457031" />
                  <Point X="-2.787522460938" Y="3.229032958984" />
                  <Point X="-2.808121337891" Y="3.264711181641" />
                  <Point X="-3.059386962891" Y="3.699916503906" />
                  <Point X="-2.666690429688" Y="4.000993164062" />
                  <Point X="-2.648371582031" Y="4.015038085938" />
                  <Point X="-2.192525146484" Y="4.268297363281" />
                  <Point X="-2.118564208984" Y="4.171909179688" />
                  <Point X="-2.111820556641" Y="4.164047851562" />
                  <Point X="-2.097518066406" Y="4.149106933594" />
                  <Point X="-2.089959472656" Y="4.14202734375" />
                  <Point X="-2.073381591797" Y="4.128116210938" />
                  <Point X="-2.065096191406" Y="4.121900878906" />
                  <Point X="-2.047896362305" Y="4.110407714844" />
                  <Point X="-2.038981567383" Y="4.105129882812" />
                  <Point X="-1.948966064453" Y="4.058270751953" />
                  <Point X="-1.934330932617" Y="4.051288330078" />
                  <Point X="-1.915056030273" Y="4.043792724609" />
                  <Point X="-1.905215820312" Y="4.040571533203" />
                  <Point X="-1.88430847168" Y="4.034968505859" />
                  <Point X="-1.87417175293" Y="4.032836669922" />
                  <Point X="-1.85372668457" Y="4.029689208984" />
                  <Point X="-1.83306262207" Y="4.028786132812" />
                  <Point X="-1.812421020508" Y="4.030138183594" />
                  <Point X="-1.802135253906" Y="4.031378173828" />
                  <Point X="-1.780818969727" Y="4.035136474609" />
                  <Point X="-1.770730834961" Y="4.037488525391" />
                  <Point X="-1.750870727539" Y="4.043277099609" />
                  <Point X="-1.741098876953" Y="4.046713623047" />
                  <Point X="-1.647341552734" Y="4.085549560547" />
                  <Point X="-1.632585327148" Y="4.092272949219" />
                  <Point X="-1.614451660156" Y="4.102221679688" />
                  <Point X="-1.605657348633" Y="4.107690429687" />
                  <Point X="-1.587927734375" Y="4.120104492188" />
                  <Point X="-1.579780273438" Y="4.126498535156" />
                  <Point X="-1.564229003906" Y="4.140136230469" />
                  <Point X="-1.55025378418" Y="4.15538671875" />
                  <Point X="-1.538022705078" Y="4.172067382812" />
                  <Point X="-1.532362670898" Y="4.180741210937" />
                  <Point X="-1.521540649414" Y="4.199484863281" />
                  <Point X="-1.516858032227" Y="4.208724121094" />
                  <Point X="-1.508526489258" Y="4.227658691406" />
                  <Point X="-1.504877197266" Y="4.237354003906" />
                  <Point X="-1.474361206055" Y="4.334139160156" />
                  <Point X="-1.470026367188" Y="4.349764160156" />
                  <Point X="-1.465991210938" Y="4.37005078125" />
                  <Point X="-1.46452746582" Y="4.380303710938" />
                  <Point X="-1.462640869141" Y="4.401866210938" />
                  <Point X="-1.462301879883" Y="4.412217285156" />
                  <Point X="-1.462752807617" Y="4.432896972656" />
                  <Point X="-1.465884887695" Y="4.461014648438" />
                  <Point X="-1.479266113281" Y="4.562655273438" />
                  <Point X="-0.954886657715" Y="4.709673339844" />
                  <Point X="-0.931178405762" Y="4.7163203125" />
                  <Point X="-0.365222167969" Y="4.782557128906" />
                  <Point X="-0.225666229248" Y="4.261727539062" />
                  <Point X="-0.220435150146" Y="4.247107910156" />
                  <Point X="-0.207661743164" Y="4.218916503906" />
                  <Point X="-0.200119293213" Y="4.205344726562" />
                  <Point X="-0.182261199951" Y="4.178618164062" />
                  <Point X="-0.172608291626" Y="4.166455566406" />
                  <Point X="-0.15145123291" Y="4.143866699219" />
                  <Point X="-0.126897842407" Y="4.125026367188" />
                  <Point X="-0.099602653503" Y="4.110436523438" />
                  <Point X="-0.085356552124" Y="4.104260742188" />
                  <Point X="-0.054918582916" Y="4.093927978516" />
                  <Point X="-0.039857139587" Y="4.090155273438" />
                  <Point X="-0.009319890022" Y="4.08511328125" />
                  <Point X="0.021631868362" Y="4.08511328125" />
                  <Point X="0.052169116974" Y="4.090155273438" />
                  <Point X="0.067230560303" Y="4.093927978516" />
                  <Point X="0.097668525696" Y="4.104260742188" />
                  <Point X="0.111914779663" Y="4.110436523438" />
                  <Point X="0.139209823608" Y="4.125026367188" />
                  <Point X="0.163763214111" Y="4.1438671875" />
                  <Point X="0.184920272827" Y="4.166456054688" />
                  <Point X="0.194572891235" Y="4.178618164062" />
                  <Point X="0.212431121826" Y="4.205344726562" />
                  <Point X="0.219973724365" Y="4.218916992188" />
                  <Point X="0.232747421265" Y="4.247108886719" />
                  <Point X="0.237978347778" Y="4.261728515625" />
                  <Point X="0.248532958984" Y="4.301119140625" />
                  <Point X="0.378190002441" Y="4.785006347656" />
                  <Point X="0.80716003418" Y="4.740081542969" />
                  <Point X="0.827875915527" Y="4.737912109375" />
                  <Point X="1.430838378906" Y="4.592338378906" />
                  <Point X="1.453596191406" Y="4.586844238281" />
                  <Point X="1.845016357422" Y="4.444873046875" />
                  <Point X="1.858261962891" Y="4.440068847656" />
                  <Point X="2.237759033203" Y="4.262590332031" />
                  <Point X="2.250448730469" Y="4.256655761719" />
                  <Point X="2.617114257812" Y="4.043035400391" />
                  <Point X="2.629433105469" Y="4.035858398438" />
                  <Point X="2.817780029297" Y="3.901916503906" />
                  <Point X="2.06530859375" Y="2.59859765625" />
                  <Point X="2.062372558594" Y="2.593104003906" />
                  <Point X="2.053181152344" Y="2.5734375" />
                  <Point X="2.044182006836" Y="2.549563232422" />
                  <Point X="2.041301513672" Y="2.540597900391" />
                  <Point X="2.021004394531" Y="2.464696777344" />
                  <Point X="2.019389404297" Y="2.457570068359" />
                  <Point X="2.015649536133" Y="2.43598046875" />
                  <Point X="2.013138916016" Y="2.412696289062" />
                  <Point X="2.012593383789" Y="2.401913085938" />
                  <Point X="2.012729370117" Y="2.380356201172" />
                  <Point X="2.013410766602" Y="2.369582519531" />
                  <Point X="2.021325073242" Y="2.303949707031" />
                  <Point X="2.023800170898" Y="2.289037597656" />
                  <Point X="2.029142700195" Y="2.267114990234" />
                  <Point X="2.032467163086" Y="2.2563125" />
                  <Point X="2.040734863281" Y="2.234219482422" />
                  <Point X="2.045318847656" Y="2.223888183594" />
                  <Point X="2.055681640625" Y="2.203842773438" />
                  <Point X="2.061460449219" Y="2.194128662109" />
                  <Point X="2.102071777344" Y="2.134277832031" />
                  <Point X="2.106436523438" Y="2.128353027344" />
                  <Point X="2.120420898438" Y="2.111290527344" />
                  <Point X="2.136066650391" Y="2.094534179688" />
                  <Point X="2.143729248047" Y="2.087195800781" />
                  <Point X="2.159824951172" Y="2.073419189453" />
                  <Point X="2.168258056641" Y="2.066980957031" />
                  <Point X="2.228108886719" Y="2.026369750977" />
                  <Point X="2.241280029297" Y="2.018245117188" />
                  <Point X="2.261323974609" Y="2.00788293457" />
                  <Point X="2.271654541016" Y="2.003298706055" />
                  <Point X="2.293741943359" Y="1.995032958984" />
                  <Point X="2.304545410156" Y="1.991708007812" />
                  <Point X="2.326469482422" Y="1.986364990234" />
                  <Point X="2.337590087891" Y="1.984346801758" />
                  <Point X="2.403223144531" Y="1.976432495117" />
                  <Point X="2.410737304688" Y="1.975827636719" />
                  <Point X="2.433333984375" Y="1.975208618164" />
                  <Point X="2.456029052734" Y="1.976088989258" />
                  <Point X="2.466547363281" Y="1.977084960938" />
                  <Point X="2.487406982422" Y="1.980238525391" />
                  <Point X="2.497748291016" Y="1.982395874023" />
                  <Point X="2.573649658203" Y="2.002692993164" />
                  <Point X="2.594692382812" Y="2.009696777344" />
                  <Point X="2.625311035156" Y="2.022552612305" />
                  <Point X="2.636033935547" Y="2.027872680664" />
                  <Point X="2.717544921875" Y="2.074933105469" />
                  <Point X="3.940404785156" Y="2.780951171875" />
                  <Point X="4.036643798828" Y="2.647200927734" />
                  <Point X="4.043949462891" Y="2.637047607422" />
                  <Point X="4.136884277344" Y="2.483471923828" />
                  <Point X="3.172951660156" Y="1.743820068359" />
                  <Point X="3.168136474609" Y="1.739867919922" />
                  <Point X="3.152118408203" Y="1.725215820312" />
                  <Point X="3.134668701172" Y="1.706603149414" />
                  <Point X="3.128576660156" Y="1.699422607422" />
                  <Point X="3.073950439453" Y="1.628158325195" />
                  <Point X="3.069837646484" Y="1.62235534668" />
                  <Point X="3.058389648438" Y="1.604361328125" />
                  <Point X="3.046763671875" Y="1.583291381836" />
                  <Point X="3.042019775391" Y="1.573378295898" />
                  <Point X="3.033707763672" Y="1.553068237305" />
                  <Point X="3.030139892578" Y="1.542671630859" />
                  <Point X="3.009791503906" Y="1.469910766602" />
                  <Point X="3.006224609375" Y="1.454659179688" />
                  <Point X="3.002771240234" Y="1.432358764648" />
                  <Point X="3.001709228516" Y="1.421106201172" />
                  <Point X="3.000893554688" Y="1.397536010742" />
                  <Point X="3.001175292969" Y="1.386237548828" />
                  <Point X="3.003078369141" Y="1.363754882813" />
                  <Point X="3.004699707031" Y="1.35257019043" />
                  <Point X="3.021403564453" Y="1.271614379883" />
                  <Point X="3.023116699219" Y="1.264650756836" />
                  <Point X="3.029298095703" Y="1.244054931641" />
                  <Point X="3.037461669922" Y="1.221999633789" />
                  <Point X="3.041778564453" Y="1.212104003906" />
                  <Point X="3.051507324219" Y="1.192866455078" />
                  <Point X="3.056919189453" Y="1.183524414062" />
                  <Point X="3.102351806641" Y="1.11446875" />
                  <Point X="3.111740722656" Y="1.101421875" />
                  <Point X="3.126295654297" Y="1.08417578125" />
                  <Point X="3.134086669922" Y="1.075986816406" />
                  <Point X="3.151330078125" Y="1.05989831543" />
                  <Point X="3.160037353516" Y="1.052693603516" />
                  <Point X="3.178245605469" Y="1.039369262695" />
                  <Point X="3.187746582031" Y="1.033249633789" />
                  <Point X="3.253584960938" Y="0.996188415527" />
                  <Point X="3.260137695312" Y="0.992827331543" />
                  <Point X="3.280275878906" Y="0.983780029297" />
                  <Point X="3.3020546875" Y="0.975641296387" />
                  <Point X="3.312311523438" Y="0.972456481934" />
                  <Point X="3.333119628906" Y="0.967264465332" />
                  <Point X="3.343670898438" Y="0.965257629395" />
                  <Point X="3.432688720703" Y="0.953492614746" />
                  <Point X="3.454199707031" Y="0.951921691895" />
                  <Point X="3.488378662109" Y="0.951984680176" />
                  <Point X="3.500603515625" Y="0.952797241211" />
                  <Point X="3.578033691406" Y="0.962991088867" />
                  <Point X="4.704703613281" Y="1.111320068359" />
                  <Point X="4.749544921875" Y="0.927124389648" />
                  <Point X="4.752684570312" Y="0.914227111816" />
                  <Point X="4.783871582031" Y="0.713921020508" />
                  <Point X="3.691991943359" Y="0.421352813721" />
                  <Point X="3.686031982422" Y="0.419544525146" />
                  <Point X="3.665626708984" Y="0.412138031006" />
                  <Point X="3.642381103516" Y="0.401619262695" />
                  <Point X="3.634004394531" Y="0.397316619873" />
                  <Point X="3.546547119141" Y="0.346764770508" />
                  <Point X="3.540679443359" Y="0.343081298828" />
                  <Point X="3.523645507812" Y="0.331196075439" />
                  <Point X="3.504280029297" Y="0.3158543396" />
                  <Point X="3.495943359375" Y="0.30841116333" />
                  <Point X="3.480201416016" Y="0.29259677124" />
                  <Point X="3.472796142578" Y="0.284225891113" />
                  <Point X="3.420321777344" Y="0.217361206055" />
                  <Point X="3.410854003906" Y="0.204207778931" />
                  <Point X="3.399129882813" Y="0.184927825928" />
                  <Point X="3.393842285156" Y="0.174938278198" />
                  <Point X="3.384069580078" Y="0.153477142334" />
                  <Point X="3.380005859375" Y="0.142930999756" />
                  <Point X="3.373159423828" Y="0.121429733276" />
                  <Point X="3.370376708984" Y="0.110474586487" />
                  <Point X="3.352885253906" Y="0.019140766144" />
                  <Point X="3.351823486328" Y="0.012226828575" />
                  <Point X="3.349661132812" Y="-0.008631948471" />
                  <Point X="3.348650390625" Y="-0.032721488953" />
                  <Point X="3.348824462891" Y="-0.043693576813" />
                  <Point X="3.350436523438" Y="-0.06554485321" />
                  <Point X="3.351874511719" Y="-0.076424201965" />
                  <Point X="3.369365966797" Y="-0.167757873535" />
                  <Point X="3.373159423828" Y="-0.183989715576" />
                  <Point X="3.380005859375" Y="-0.205491149902" />
                  <Point X="3.384069580078" Y="-0.216037719727" />
                  <Point X="3.393842285156" Y="-0.237498718262" />
                  <Point X="3.399130126953" Y="-0.247488265991" />
                  <Point X="3.410854248047" Y="-0.266768066406" />
                  <Point X="3.417290527344" Y="-0.276058349609" />
                  <Point X="3.469764892578" Y="-0.34292288208" />
                  <Point X="3.474417724609" Y="-0.348410644531" />
                  <Point X="3.489181396484" Y="-0.364138519287" />
                  <Point X="3.506525634766" Y="-0.380422973633" />
                  <Point X="3.51489453125" Y="-0.387421325684" />
                  <Point X="3.532379150391" Y="-0.400412017822" />
                  <Point X="3.541494628906" Y="-0.406404327393" />
                  <Point X="3.628951904297" Y="-0.456956176758" />
                  <Point X="3.647700195312" Y="-0.466409790039" />
                  <Point X="3.680140869141" Y="-0.479883636475" />
                  <Point X="3.6919921875" Y="-0.483912811279" />
                  <Point X="3.762999267578" Y="-0.502939208984" />
                  <Point X="4.784876953125" Y="-0.776750366211" />
                  <Point X="4.763366210938" Y="-0.919426940918" />
                  <Point X="4.761612304688" Y="-0.931059265137" />
                  <Point X="4.727802246094" Y="-1.079219726563" />
                  <Point X="3.436781982422" Y="-0.909253662109" />
                  <Point X="3.428624267578" Y="-0.908535705566" />
                  <Point X="3.400097900391" Y="-0.908042419434" />
                  <Point X="3.366721191406" Y="-0.910840820312" />
                  <Point X="3.354480957031" Y="-0.912676208496" />
                  <Point X="3.182833251953" Y="-0.949984680176" />
                  <Point X="3.157873535156" Y="-0.956742553711" />
                  <Point X="3.128752929688" Y="-0.968367431641" />
                  <Point X="3.114675537109" Y="-0.975389404297" />
                  <Point X="3.086848876953" Y="-0.992281860352" />
                  <Point X="3.074123291016" Y="-1.001530822754" />
                  <Point X="3.050373535156" Y="-1.022001525879" />
                  <Point X="3.039349365234" Y="-1.033223144531" />
                  <Point X="2.935599121094" Y="-1.158002197266" />
                  <Point X="2.921325927734" Y="-1.176847290039" />
                  <Point X="2.906604980469" Y="-1.201230102539" />
                  <Point X="2.900163330078" Y="-1.21397644043" />
                  <Point X="2.8888203125" Y="-1.241361328125" />
                  <Point X="2.884362548828" Y="-1.254928833008" />
                  <Point X="2.877531005859" Y="-1.282578369141" />
                  <Point X="2.875157226562" Y="-1.29666027832" />
                  <Point X="2.860287353516" Y="-1.458254638672" />
                  <Point X="2.859288818359" Y="-1.483321411133" />
                  <Point X="2.861607666016" Y="-1.51458984375" />
                  <Point X="2.864065917969" Y="-1.530127441406" />
                  <Point X="2.871797607422" Y="-1.561749023438" />
                  <Point X="2.876786376953" Y="-1.576668457031" />
                  <Point X="2.889157714844" Y="-1.605479736328" />
                  <Point X="2.896540283203" Y="-1.619371704102" />
                  <Point X="2.991532470703" Y="-1.767125732422" />
                  <Point X="3.003590087891" Y="-1.783817138672" />
                  <Point X="3.020697998047" Y="-1.804981689453" />
                  <Point X="3.028259277344" Y="-1.813280883789" />
                  <Point X="3.044308349609" Y="-1.828928833008" />
                  <Point X="3.052796142578" Y="-1.836277832031" />
                  <Point X="3.118691162109" Y="-1.886840820312" />
                  <Point X="4.087170410156" Y="-2.629981689453" />
                  <Point X="4.050428222656" Y="-2.689436279297" />
                  <Point X="4.045486816406" Y="-2.697432373047" />
                  <Point X="4.001274658203" Y="-2.760251953125" />
                  <Point X="2.848454589844" Y="-2.094670654297" />
                  <Point X="2.841192626953" Y="-2.090885498047" />
                  <Point X="2.815024414062" Y="-2.079512451172" />
                  <Point X="2.783117675781" Y="-2.069325683594" />
                  <Point X="2.771107910156" Y="-2.066337646484" />
                  <Point X="2.566819824219" Y="-2.029443115234" />
                  <Point X="2.539358642578" Y="-2.025807495117" />
                  <Point X="2.508006591797" Y="-2.025403198242" />
                  <Point X="2.492313476562" Y="-2.026503417969" />
                  <Point X="2.460141601562" Y="-2.03146081543" />
                  <Point X="2.444845458984" Y="-2.035135864258" />
                  <Point X="2.415069335938" Y="-2.044959594727" />
                  <Point X="2.400589355469" Y="-2.051108398438" />
                  <Point X="2.230876220703" Y="-2.140427246094" />
                  <Point X="2.20896875" Y="-2.153170166016" />
                  <Point X="2.186038085938" Y="-2.170063232422" />
                  <Point X="2.175209960938" Y="-2.179373535156" />
                  <Point X="2.154249511719" Y="-2.200333984375" />
                  <Point X="2.144939208984" Y="-2.211162109375" />
                  <Point X="2.128046142578" Y="-2.234092773438" />
                  <Point X="2.120463378906" Y="-2.2461953125" />
                  <Point X="2.03114465332" Y="-2.415908447266" />
                  <Point X="2.019835327148" Y="-2.440194335938" />
                  <Point X="2.01001159668" Y="-2.469971435547" />
                  <Point X="2.006336791992" Y="-2.485267578125" />
                  <Point X="2.001379516602" Y="-2.517440185547" />
                  <Point X="2.000279418945" Y="-2.533132568359" />
                  <Point X="2.00068371582" Y="-2.564483642578" />
                  <Point X="2.002188110352" Y="-2.580142333984" />
                  <Point X="2.03908215332" Y="-2.784430175781" />
                  <Point X="2.04346862793" Y="-2.803211425781" />
                  <Point X="2.051502685547" Y="-2.831127441406" />
                  <Point X="2.055367431641" Y="-2.842015625" />
                  <Point X="2.064389648438" Y="-2.863241455078" />
                  <Point X="2.069547119141" Y="-2.873579101562" />
                  <Point X="2.111891357422" Y="-2.946921142578" />
                  <Point X="2.735893310547" Y="-4.027724121094" />
                  <Point X="2.723754394531" Y="-4.036083740234" />
                  <Point X="1.833914794922" Y="-2.876423095703" />
                  <Point X="1.828654052734" Y="-2.870146972656" />
                  <Point X="1.808832519531" Y="-2.849627685547" />
                  <Point X="1.783252441406" Y="-2.828004882812" />
                  <Point X="1.773298950195" Y="-2.820646972656" />
                  <Point X="1.571815917969" Y="-2.691111816406" />
                  <Point X="1.546283447266" Y="-2.676245361328" />
                  <Point X="1.517471313477" Y="-2.663873779297" />
                  <Point X="1.502551513672" Y="-2.658885009766" />
                  <Point X="1.470930053711" Y="-2.651153564453" />
                  <Point X="1.455392333984" Y="-2.6486953125" />
                  <Point X="1.424124267578" Y="-2.646376708984" />
                  <Point X="1.408393920898" Y="-2.646516357422" />
                  <Point X="1.188037841797" Y="-2.666793701172" />
                  <Point X="1.161224853516" Y="-2.670339111328" />
                  <Point X="1.133574951172" Y="-2.677170898438" />
                  <Point X="1.1200078125" Y="-2.68162890625" />
                  <Point X="1.092623291016" Y="-2.692971923828" />
                  <Point X="1.079877563477" Y="-2.699413085938" />
                  <Point X="1.055495483398" Y="-2.714133544922" />
                  <Point X="1.043859130859" Y="-2.722412841797" />
                  <Point X="0.873705627441" Y="-2.863889648438" />
                  <Point X="0.852653198242" Y="-2.883088134766" />
                  <Point X="0.832182312012" Y="-2.906838378906" />
                  <Point X="0.82293371582" Y="-2.919563476563" />
                  <Point X="0.80604107666" Y="-2.947390380859" />
                  <Point X="0.799018798828" Y="-2.961468017578" />
                  <Point X="0.787394226074" Y="-2.990588378906" />
                  <Point X="0.782791809082" Y="-3.005631347656" />
                  <Point X="0.731916870117" Y="-3.239696289062" />
                  <Point X="0.728914978027" Y="-3.257583007812" />
                  <Point X="0.725262573242" Y="-3.288251953125" />
                  <Point X="0.72459777832" Y="-3.300073974609" />
                  <Point X="0.724743896484" Y="-3.323707763672" />
                  <Point X="0.725554931641" Y="-3.335520263672" />
                  <Point X="0.737555053711" Y="-3.426669189453" />
                  <Point X="0.833091674805" Y="-4.152341308594" />
                  <Point X="0.655065002441" Y="-3.487936767578" />
                  <Point X="0.652606506348" Y="-3.480125" />
                  <Point X="0.642145080566" Y="-3.453580078125" />
                  <Point X="0.626786865234" Y="-3.423815429688" />
                  <Point X="0.620407348633" Y="-3.413209960938" />
                  <Point X="0.465622253418" Y="-3.190193847656" />
                  <Point X="0.446670593262" Y="-3.165172851562" />
                  <Point X="0.424786621094" Y="-3.142717285156" />
                  <Point X="0.412911346436" Y="-3.132398193359" />
                  <Point X="0.386655639648" Y="-3.113154296875" />
                  <Point X="0.373241973877" Y="-3.1049375" />
                  <Point X="0.345241119385" Y="-3.090829345703" />
                  <Point X="0.330654388428" Y="-3.084938232422" />
                  <Point X="0.091133430481" Y="-3.010599609375" />
                  <Point X="0.063375728607" Y="-3.003108886719" />
                  <Point X="0.035215702057" Y="-2.998839599609" />
                  <Point X="0.020974954605" Y="-2.997766357422" />
                  <Point X="-0.008666692734" Y="-2.997766601562" />
                  <Point X="-0.022906698227" Y="-2.998840087891" />
                  <Point X="-0.051065387726" Y="-3.003109375" />
                  <Point X="-0.064984069824" Y="-3.006305175781" />
                  <Point X="-0.3045050354" Y="-3.080643554688" />
                  <Point X="-0.332930938721" Y="-3.090830078125" />
                  <Point X="-0.360932830811" Y="-3.104939208984" />
                  <Point X="-0.374347076416" Y="-3.113156494141" />
                  <Point X="-0.400602203369" Y="-3.132400634766" />
                  <Point X="-0.41247644043" Y="-3.142718994141" />
                  <Point X="-0.434359375" Y="-3.165173583984" />
                  <Point X="-0.44436807251" Y="-3.177310058594" />
                  <Point X="-0.599153198242" Y="-3.400325927734" />
                  <Point X="-0.608269836426" Y="-3.414813232422" />
                  <Point X="-0.624432434082" Y="-3.443265625" />
                  <Point X="-0.629724914551" Y="-3.454141357422" />
                  <Point X="-0.638885131836" Y="-3.476476806641" />
                  <Point X="-0.642752868652" Y="-3.487936523438" />
                  <Point X="-0.664631958008" Y="-3.56958984375" />
                  <Point X="-0.985425109863" Y="-4.766807128906" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.884130640169" Y="2.83219330456" />
                  <Point X="-4.150104560006" Y="2.234806099706" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.562362176693" Y="1.308860332307" />
                  <Point X="-4.737576479396" Y="0.915322565122" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.697302901208" Y="3.018248909773" />
                  <Point X="-4.072594373919" Y="2.175330461131" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.464129674389" Y="1.295927777994" />
                  <Point X="-4.778451102419" Y="0.589950291812" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.614577312149" Y="2.970487258063" />
                  <Point X="-3.995084187831" Y="2.115854822556" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.365897172085" Y="1.282995223682" />
                  <Point X="-4.685544309904" Y="0.565055997468" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.53185172309" Y="2.922725606353" />
                  <Point X="-3.917574001744" Y="2.056379183982" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.267664669781" Y="1.270062669369" />
                  <Point X="-4.592637517388" Y="0.540161703124" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.449126134031" Y="2.874963954643" />
                  <Point X="-3.840063815656" Y="1.996903545407" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.169432167477" Y="1.257130115056" />
                  <Point X="-4.499730724872" Y="0.51526740878" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.935582012175" Y="3.794836570473" />
                  <Point X="-3.013347173333" Y="3.620173158785" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.366400544972" Y="2.827202302933" />
                  <Point X="-3.762553629569" Y="1.937427906832" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.071199665174" Y="1.244197560744" />
                  <Point X="-4.406823932357" Y="0.490373114436" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.777697171809" Y="3.915885361098" />
                  <Point X="-2.954633973393" Y="3.518478798082" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.283674955913" Y="2.779440651222" />
                  <Point X="-3.685043443481" Y="1.877952268257" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.97296716287" Y="1.231265006431" />
                  <Point X="-4.313917139841" Y="0.465478820092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.623378888807" Y="4.028923532727" />
                  <Point X="-2.895920773454" Y="3.41678443738" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.197349224814" Y="2.739765050925" />
                  <Point X="-3.607533257394" Y="1.818476629682" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.874734660566" Y="1.218332452118" />
                  <Point X="-4.221010347325" Y="0.440584525748" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.698102097494" Y="-0.630981089656" />
                  <Point X="-4.767534028726" Y="-0.786927760488" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.485211261426" Y="4.105686737907" />
                  <Point X="-2.837207573515" Y="3.315090076678" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.099361090717" Y="2.726283636633" />
                  <Point X="-3.530625865532" Y="1.757647093109" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.776465027794" Y="1.205483294203" />
                  <Point X="-4.12810355481" Y="0.415690231404" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.580025236254" Y="-0.599342484045" />
                  <Point X="-4.742250600226" Y="-0.963706617186" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.347043634046" Y="4.182449943088" />
                  <Point X="-2.779264235687" Y="3.211666577363" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.993716879067" Y="2.729998054069" />
                  <Point X="-3.466238945687" Y="1.66869611596" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.667378981637" Y="1.216928198513" />
                  <Point X="-4.035196762294" Y="0.39079593706" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.461948375015" Y="-0.567703878434" />
                  <Point X="-4.705084328239" Y="-1.113796170432" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.208876006665" Y="4.259213148269" />
                  <Point X="-2.748953095506" Y="3.046180145988" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.847807685486" Y="2.824149101623" />
                  <Point X="-3.421571832275" Y="1.535453728389" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.537702991127" Y="1.274618875009" />
                  <Point X="-3.942289969778" Y="0.365901642716" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.343871513776" Y="-0.536065272822" />
                  <Point X="-4.667173859226" Y="-1.262214229793" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.13450586925" Y="4.192684844903" />
                  <Point X="-3.849383177263" Y="0.341007348372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.225794652536" Y="-0.504426667211" />
                  <Point X="-4.568675661735" Y="-1.274550022944" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.062733418651" Y="4.120322041422" />
                  <Point X="-3.756476384747" Y="0.316113054028" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.107717791297" Y="-0.4727880616" />
                  <Point X="-4.458210233982" Y="-1.260006976847" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.979332022266" Y="4.074078277818" />
                  <Point X="-3.663569592232" Y="0.291218759684" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.989640930058" Y="-0.441149455989" />
                  <Point X="-4.34774480623" Y="-1.24546393075" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.89185404363" Y="4.036990667861" />
                  <Point X="-3.570662795342" Y="0.266324475164" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.871564068818" Y="-0.409510850378" />
                  <Point X="-4.237279378478" Y="-1.230920884653" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.789359563819" Y="4.03363067176" />
                  <Point X="-3.479195326768" Y="0.238197406318" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.753487207579" Y="-0.377872244766" />
                  <Point X="-4.126813950726" Y="-1.216377838557" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.445633996699" Y="4.572084568763" />
                  <Point X="-1.472548879299" Y="4.511632752678" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.665625246333" Y="4.077976132148" />
                  <Point X="-3.397266239339" Y="0.188646782656" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.635410346339" Y="-0.346233639155" />
                  <Point X="-4.016348522974" Y="-1.20183479246" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.326811279077" Y="4.605398395238" />
                  <Point X="-3.330172842517" Y="0.105774652325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.5173334851" Y="-0.314595033544" />
                  <Point X="-3.905883095222" Y="-1.187291746363" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.207988561455" Y="4.638712221713" />
                  <Point X="-3.29602311601" Y="-0.051090173" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.376235055006" Y="-0.231249137693" />
                  <Point X="-3.795417667469" Y="-1.172748700266" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.089165843832" Y="4.672026048189" />
                  <Point X="-3.684952239717" Y="-1.158205654169" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.97034312621" Y="4.705339874664" />
                  <Point X="-3.574486811965" Y="-1.143662608073" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.138568620824" Y="-2.410611094261" />
                  <Point X="-4.165170919417" Y="-2.470360835169" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.85763153468" Y="4.724927887206" />
                  <Point X="-3.464021384213" Y="-1.129119561976" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.980615790581" Y="-2.289409595871" />
                  <Point X="-4.105631467071" Y="-2.570199402582" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.747924537449" Y="4.737767470461" />
                  <Point X="-3.353555956461" Y="-1.114576515879" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.822662960337" Y="-2.168208097481" />
                  <Point X="-4.046092014725" Y="-2.670037969995" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.638217540219" Y="4.750607053717" />
                  <Point X="-3.243137624434" Y="-1.100139248512" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.664710130093" Y="-2.047006599091" />
                  <Point X="-3.984771911717" Y="-2.765877130539" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.528510542988" Y="4.763446636972" />
                  <Point X="-3.143806470265" Y="-1.110604190335" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.50675729985" Y="-1.925805100701" />
                  <Point X="-3.919160260245" Y="-2.852077315417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.418803545757" Y="4.776286220228" />
                  <Point X="-3.058074395981" Y="-1.151613165669" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.348804469606" Y="-1.804603602311" />
                  <Point X="-3.853548608774" Y="-2.938277500294" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.34523390804" Y="4.707959965103" />
                  <Point X="-2.986510017727" Y="-1.224443307288" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.190851639362" Y="-1.683402103921" />
                  <Point X="-3.782097677426" Y="-3.011362447838" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.306163479982" Y="4.562147216415" />
                  <Point X="-2.949308541076" Y="-1.374453789566" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.03057112083" Y="-1.556972532037" />
                  <Point X="-3.642127487724" Y="-2.930550621396" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.267093051923" Y="4.416334467727" />
                  <Point X="-3.502157298022" Y="-2.849738794954" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.228022623865" Y="4.270521719038" />
                  <Point X="-3.36218710832" Y="-2.768926968513" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.171088067099" Y="4.16483246036" />
                  <Point X="-3.222216918618" Y="-2.688115142071" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.092656857703" Y="4.107425474005" />
                  <Point X="-3.082246728916" Y="-2.607303315629" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.001399560508" Y="4.08511328125" />
                  <Point X="-2.942276539214" Y="-2.526491489188" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.41527337101" Y="4.781122712514" />
                  <Point X="0.319527027943" Y="4.566072905019" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.11814807229" Y="4.113768365132" />
                  <Point X="-2.802306349512" Y="-2.445679662746" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.514631016187" Y="4.770717270471" />
                  <Point X="-2.664228213246" Y="-2.369117457901" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.613988661364" Y="4.760311828427" />
                  <Point X="-2.550863489164" Y="-2.348062485629" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.713346306541" Y="4.749906386383" />
                  <Point X="-2.458976891045" Y="-2.375248174104" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.812703960986" Y="4.739500965157" />
                  <Point X="-2.380805414753" Y="-2.43323853056" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.907435039284" Y="4.718704083766" />
                  <Point X="-2.321767955009" Y="-2.534204591818" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.62480648742" Y="-3.214840279523" />
                  <Point X="-2.920183820789" Y="-3.878268632447" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.001332288891" Y="4.696034392473" />
                  <Point X="-2.842741477655" Y="-3.93789664879" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.095229538498" Y="4.67336470118" />
                  <Point X="-2.76529913452" Y="-3.997524665132" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.189126788106" Y="4.650695009887" />
                  <Point X="-2.684713453025" Y="-4.050092627923" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.283024037713" Y="4.628025318593" />
                  <Point X="-2.603195479219" Y="-4.1005666279" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.37692128732" Y="4.6053556273" />
                  <Point X="-2.521677505413" Y="-4.151040627878" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.470018006798" Y="4.580887915898" />
                  <Point X="-2.341786550339" Y="-3.980565294368" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.559550097876" Y="4.548413918023" />
                  <Point X="-2.193665511139" Y="-3.881446360216" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.649082188953" Y="4.515939920148" />
                  <Point X="-2.050978076544" Y="-3.794531501822" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.738614280031" Y="4.483465922273" />
                  <Point X="-1.939063834664" Y="-3.776734365913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.828146371108" Y="4.450991924398" />
                  <Point X="-1.837859650141" Y="-3.782992412682" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.915380402272" Y="4.413356399447" />
                  <Point X="-1.736817802798" Y="-3.789615074726" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.001449600513" Y="4.373104616918" />
                  <Point X="-1.635811860469" Y="-3.796318380751" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.087518798754" Y="4.332852834388" />
                  <Point X="-1.544112576148" Y="-3.823924782906" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.173587996995" Y="4.292601051858" />
                  <Point X="-1.465345912004" Y="-3.880578325561" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.25928302776" Y="4.251508875419" />
                  <Point X="-1.390557174754" Y="-3.946166438302" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.341855006667" Y="4.203402209658" />
                  <Point X="-1.315768437504" Y="-4.011754551044" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.424426985574" Y="4.155295543898" />
                  <Point X="-1.242376633956" Y="-4.080480228252" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.506998964481" Y="4.107188878138" />
                  <Point X="-1.185984975755" Y="-4.187388857071" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.589570943388" Y="4.059082212378" />
                  <Point X="-1.153230997087" Y="-4.347388583371" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.670286423698" Y="4.006805782497" />
                  <Point X="-1.121700537841" Y="-4.510136379286" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.749269190976" Y="3.950637615428" />
                  <Point X="2.423607747872" Y="3.219190038375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.020215035811" Y="2.313155172761" />
                  <Point X="-0.406637689383" Y="-3.137645292875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.763904083662" Y="-3.940078752506" />
                  <Point X="-1.118766370148" Y="-4.737112497625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.067348194653" Y="2.185451613911" />
                  <Point X="-0.272901363102" Y="-3.070834952921" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.921080193178" Y="-4.526668441338" />
                  <Point X="-1.024610377711" Y="-4.759201043008" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.132540821853" Y="2.098310285108" />
                  <Point X="-0.152237263339" Y="-3.033385314443" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.209978554627" Y="2.038671913728" />
                  <Point X="-0.033587266432" Y="-3.000459425046" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.294443548046" Y="1.994817028175" />
                  <Point X="0.068596311295" Y="-3.00451771866" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.390909748759" Y="1.977917295535" />
                  <Point X="0.16031870718" Y="-3.032072211393" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.496806733837" Y="1.982199451388" />
                  <Point X="0.251684032984" Y="-3.060428696657" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.617258181557" Y="2.019171465555" />
                  <Point X="0.342611991404" Y="-3.089767525148" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.755946555261" Y="2.097104286128" />
                  <Point X="0.423518357084" Y="-3.141615219468" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.89591671735" Y="2.17791605055" />
                  <Point X="0.490147494909" Y="-3.225530092578" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.035886879439" Y="2.258727814972" />
                  <Point X="0.553498726801" Y="-3.316807262957" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.175857041528" Y="2.339539579394" />
                  <Point X="0.848392490991" Y="-2.888031391071" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.770161364687" Y="-3.063741377615" />
                  <Point X="0.616849958693" Y="-3.408084433336" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.315827203617" Y="2.420351343817" />
                  <Point X="1.015694872029" Y="-2.745830457777" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.731852131197" Y="-3.383351691691" />
                  <Point X="0.666420078965" Y="-3.530314487196" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.455797365707" Y="2.501163108239" />
                  <Point X="3.048684244387" Y="1.586772066616" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.034042593913" Y="1.553886381221" />
                  <Point X="1.152316210321" Y="-2.672540274753" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.755584292882" Y="-3.563614750703" />
                  <Point X="0.705490496327" Y="-3.67612725991" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.595767527796" Y="2.581974872661" />
                  <Point X="3.248358755556" Y="1.801681994632" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.018538889491" Y="1.285498124078" />
                  <Point X="1.261890981619" Y="-2.659997675806" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.779316407243" Y="-3.743877916004" />
                  <Point X="0.744560913688" Y="-3.821940032624" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.735737689885" Y="2.662786637083" />
                  <Point X="3.406311604332" Y="1.922883534647" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.068971541101" Y="1.16520534732" />
                  <Point X="1.370323959941" Y="-2.650019585869" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.803048521605" Y="-3.924141081305" />
                  <Point X="0.78363133105" Y="-3.967752805338" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.875707851974" Y="2.743598401505" />
                  <Point X="3.564264453108" Y="2.044085074663" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.133509456796" Y="1.076593512403" />
                  <Point X="1.473526844175" Y="-2.651788479586" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.826780635967" Y="-4.104404246605" />
                  <Point X="0.822701748411" Y="-4.113565578052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.974952108512" Y="2.732938284409" />
                  <Point X="3.722217301885" Y="2.165286614678" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.212098720229" Y="1.019541521228" />
                  <Point X="1.562440001602" Y="-2.685652625202" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.039192658319" Y="2.643658554772" />
                  <Point X="3.880170150661" Y="2.286488154693" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.297329682182" Y="0.977407029169" />
                  <Point X="1.643485161344" Y="-2.737188582953" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.099424359046" Y="2.545374802678" />
                  <Point X="4.038122999438" Y="2.407689694708" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.393007715377" Y="0.9587370433" />
                  <Point X="1.724333479274" Y="-2.789166654654" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.494162994374" Y="0.952369152923" />
                  <Point X="1.803435946825" Y="-2.845065970508" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.604429855675" Y="0.966466211467" />
                  <Point X="2.217753221801" Y="-2.148060501727" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.008880142579" Y="-2.617197118739" />
                  <Point X="1.871599434882" Y="-2.925534636574" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.714895288279" Y="0.981009268461" />
                  <Point X="2.353990429289" Y="-2.075633090614" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.038889235301" Y="-2.783361959813" />
                  <Point X="1.937406474131" Y="-3.011295973318" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.825360720882" Y="0.995552325455" />
                  <Point X="3.53224024007" Y="0.337192946366" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.351007273478" Y="-0.069862961242" />
                  <Point X="2.478937120385" Y="-2.028564594513" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.088272357212" Y="-2.90601201887" />
                  <Point X="2.00321351338" Y="-3.097057310062" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.935826153486" Y="1.010095382449" />
                  <Point X="3.670363946916" Y="0.41385750441" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.387025973915" Y="-0.222530002394" />
                  <Point X="3.010788771614" Y="-1.067572594473" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.866394433008" Y="-1.391887588924" />
                  <Point X="2.581366729058" Y="-2.032070293578" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.146985634099" Y="-3.007706206745" />
                  <Point X="2.069020552629" Y="-3.182818646806" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.04629158609" Y="1.024638439444" />
                  <Point X="3.78930027148" Y="0.447426496254" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.449124093624" Y="-0.316621708817" />
                  <Point X="3.164980669488" Y="-0.954818288488" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.882348193415" Y="-1.589621223248" />
                  <Point X="2.677617771198" Y="-2.049453280284" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.205698865592" Y="-3.109400496576" />
                  <Point X="2.134827591878" Y="-3.26857998355" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.156757018694" Y="1.039181496438" />
                  <Point X="3.907377126373" Y="0.479065087611" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.519927603426" Y="-0.391160788958" />
                  <Point X="3.280582643863" Y="-0.928738369786" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.941770133549" Y="-1.689723727409" />
                  <Point X="2.773793339918" Y="-2.067005783069" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.264412097085" Y="-3.211094786407" />
                  <Point X="2.200634631127" Y="-3.354341320295" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.267222451298" Y="1.053724553432" />
                  <Point X="4.025453981266" Y="0.510703678969" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.601650160713" Y="-0.441175286913" />
                  <Point X="3.393542824872" Y="-0.908592016133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.003768967198" Y="-1.784038433976" />
                  <Point X="2.861987831432" Y="-2.102484078751" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.323125328578" Y="-3.312789076238" />
                  <Point X="2.266441670377" Y="-3.440102657039" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.377687883902" Y="1.068267610426" />
                  <Point X="4.143530836159" Y="0.542342270326" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.687319872009" Y="-0.482324331813" />
                  <Point X="3.493891203384" Y="-0.916772234671" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.076427588849" Y="-1.854410864685" />
                  <Point X="2.94471341606" Y="-2.150245740414" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.381838560072" Y="-3.414483366069" />
                  <Point X="2.332248709626" Y="-3.525863993783" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.488153316506" Y="1.08281066742" />
                  <Point X="4.261607691052" Y="0.573980861683" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.780092754657" Y="-0.507519392645" />
                  <Point X="3.592123704991" Y="-0.929704790549" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.15393776952" Y="-1.913886515425" />
                  <Point X="3.027439000688" Y="-2.198007402077" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.440551791565" Y="-3.5161776559" />
                  <Point X="2.398055748875" Y="-3.611625330527" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.59861874911" Y="1.097353724414" />
                  <Point X="4.379684545945" Y="0.60561945304" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.872999550098" Y="-0.532413680419" />
                  <Point X="3.690356206598" Y="-0.942637346427" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.231447933221" Y="-1.973362204281" />
                  <Point X="3.110164585315" Y="-2.245769063739" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.499265023058" Y="-3.617871945731" />
                  <Point X="2.463862788124" Y="-3.697386667271" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.70616136506" Y="1.105332027719" />
                  <Point X="4.497761400838" Y="0.637258044397" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.965906345538" Y="-0.557307968193" />
                  <Point X="3.788588708205" Y="-0.955569902305" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.308958096922" Y="-2.032837893136" />
                  <Point X="3.192890169943" Y="-2.293530725402" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.557978254551" Y="-3.719566235562" />
                  <Point X="2.529669827373" Y="-3.783148004015" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.742921704114" Y="0.954330734177" />
                  <Point X="4.615838255731" Y="0.668896635754" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.058813140979" Y="-0.582202255967" />
                  <Point X="3.886821209812" Y="-0.968502458183" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.386468260623" Y="-2.092313581992" />
                  <Point X="3.275615754571" Y="-2.341292387065" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.616691486045" Y="-3.821260525393" />
                  <Point X="2.595476866622" Y="-3.86890934076" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.772472249414" Y="0.78713597873" />
                  <Point X="4.733915110624" Y="0.700535227111" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.15171993642" Y="-0.607096543741" />
                  <Point X="3.985053711419" Y="-0.981435014061" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.463978424324" Y="-2.151789270848" />
                  <Point X="3.358341339199" Y="-2.389054048727" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.675404717538" Y="-3.922954815224" />
                  <Point X="2.661283905872" Y="-3.954670677504" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.244626731861" Y="-0.631990831515" />
                  <Point X="4.083286213026" Y="-0.994367569939" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.541488588025" Y="-2.211264959703" />
                  <Point X="3.441066923827" Y="-2.43681571039" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.734117949031" Y="-4.024649105055" />
                  <Point X="2.731358405554" Y="-4.030847141183" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.337533527301" Y="-0.656885119289" />
                  <Point X="4.181518714632" Y="-1.007300125817" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.618998751726" Y="-2.270740648559" />
                  <Point X="3.523792508455" Y="-2.484577372053" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.430440322742" Y="-0.681779407063" />
                  <Point X="4.279751216239" Y="-1.020232681695" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.696508915427" Y="-2.330216337415" />
                  <Point X="3.606518093083" Y="-2.532339033715" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.523347118183" Y="-0.706673694837" />
                  <Point X="4.377983717846" Y="-1.033165237573" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.774019079128" Y="-2.38969202627" />
                  <Point X="3.68924367771" Y="-2.580100695378" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.616253913624" Y="-0.731567982611" />
                  <Point X="4.476216219453" Y="-1.046097793451" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.851529242829" Y="-2.449167715126" />
                  <Point X="3.771969262338" Y="-2.627862357041" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.709160709064" Y="-0.756462270385" />
                  <Point X="4.57444872106" Y="-1.059030349328" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.929039406531" Y="-2.508643403982" />
                  <Point X="3.854694846966" Y="-2.675624018703" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.775025328242" Y="-0.842094280493" />
                  <Point X="4.672681222667" Y="-1.071962905206" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.006549570232" Y="-2.568119092838" />
                  <Point X="3.937420431594" Y="-2.723385680366" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.084059733933" Y="-2.627594781693" />
                  <Point X="4.072241460575" Y="-2.654139058259" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.0001640625" Y="0.001626220703" />
                  <Width Value="9.996463867188" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.850241394043" Y="-4.950448730469" />
                  <Point X="0.4715390625" Y="-3.537112304688" />
                  <Point X="0.46431854248" Y="-3.521543945312" />
                  <Point X="0.30953338623" Y="-3.298527832031" />
                  <Point X="0.300590911865" Y="-3.285643554688" />
                  <Point X="0.274335357666" Y="-3.266399658203" />
                  <Point X="0.034814399719" Y="-3.192061035156" />
                  <Point X="0.020976379395" Y="-3.187766357422" />
                  <Point X="-0.00866515255" Y="-3.187766601562" />
                  <Point X="-0.248186248779" Y="-3.262104980469" />
                  <Point X="-0.262024261475" Y="-3.266399902344" />
                  <Point X="-0.28827923584" Y="-3.285644042969" />
                  <Point X="-0.443064422607" Y="-3.508659912109" />
                  <Point X="-0.459227081299" Y="-3.537112304688" />
                  <Point X="-0.481106109619" Y="-3.618765625" />
                  <Point X="-0.847743835449" Y="-4.987077148438" />
                  <Point X="-1.085347412109" Y="-4.940957519531" />
                  <Point X="-1.10024597168" Y="-4.938065429688" />
                  <Point X="-1.351589477539" Y="-4.873396972656" />
                  <Point X="-1.309150268555" Y="-4.551038574219" />
                  <Point X="-1.309683349609" Y="-4.534757324219" />
                  <Point X="-1.366904785156" Y="-4.247085449219" />
                  <Point X="-1.37021081543" Y="-4.230465820313" />
                  <Point X="-1.386282714844" Y="-4.20262890625" />
                  <Point X="-1.606803344727" Y="-4.009236816406" />
                  <Point X="-1.619543579102" Y="-3.998063964844" />
                  <Point X="-1.649241088867" Y="-3.985762939453" />
                  <Point X="-1.941921142578" Y="-3.966579589844" />
                  <Point X="-1.958830200195" Y="-3.965471435547" />
                  <Point X="-1.989878540039" Y="-3.973791015625" />
                  <Point X="-2.233755126953" Y="-4.136744628906" />
                  <Point X="-2.259734130859" Y="-4.157294433594" />
                  <Point X="-2.272017333984" Y="-4.173302246094" />
                  <Point X="-2.457095214844" Y="-4.414500488281" />
                  <Point X="-2.834000976563" Y="-4.181129882812" />
                  <Point X="-2.855832275391" Y="-4.167612304688" />
                  <Point X="-3.228581054688" Y="-3.880608398438" />
                  <Point X="-3.209809082031" Y="-3.848094482422" />
                  <Point X="-2.506033691406" Y="-2.629119628906" />
                  <Point X="-2.499762695312" Y="-2.597593261719" />
                  <Point X="-2.513979736328" Y="-2.568764404297" />
                  <Point X="-2.530380859375" Y="-2.55236328125" />
                  <Point X="-2.531328369141" Y="-2.551415771484" />
                  <Point X="-2.560157226562" Y="-2.537198730469" />
                  <Point X="-2.59168359375" Y="-2.543469726562" />
                  <Point X="-2.662108154297" Y="-2.584129394531" />
                  <Point X="-3.842959472656" Y="-3.265894042969" />
                  <Point X="-4.144454101563" Y="-2.869792236328" />
                  <Point X="-4.161701171875" Y="-2.847132568359" />
                  <Point X="-4.431020019531" Y="-2.395526855469" />
                  <Point X="-4.395165527344" Y="-2.368014892578" />
                  <Point X="-3.163786865234" Y="-1.423144897461" />
                  <Point X="-3.145822509766" Y="-1.396015991211" />
                  <Point X="-3.138538818359" Y="-1.367893554688" />
                  <Point X="-3.138117919922" Y="-1.366268798828" />
                  <Point X="-3.140326171875" Y="-1.334596191406" />
                  <Point X="-3.16116015625" Y="-1.310638549805" />
                  <Point X="-3.186196289062" Y="-1.295903198242" />
                  <Point X="-3.187642822266" Y="-1.295052001953" />
                  <Point X="-3.219529296875" Y="-1.288571044922" />
                  <Point X="-3.30843359375" Y="-1.300275512695" />
                  <Point X="-4.803283691406" Y="-1.497076293945" />
                  <Point X="-4.920646484375" Y="-1.037605224609" />
                  <Point X="-4.927391601562" Y="-1.011198608398" />
                  <Point X="-4.998395996094" Y="-0.514742126465" />
                  <Point X="-4.960189453125" Y="-0.504504699707" />
                  <Point X="-3.557463867188" Y="-0.128645431519" />
                  <Point X="-3.541896484375" Y="-0.12142552948" />
                  <Point X="-3.515659423828" Y="-0.103215538025" />
                  <Point X="-3.514143554688" Y="-0.102163497925" />
                  <Point X="-3.494897705078" Y="-0.075903968811" />
                  <Point X="-3.486151855469" Y="-0.047724880219" />
                  <Point X="-3.485646728516" Y="-0.046096931458" />
                  <Point X="-3.485646728516" Y="-0.016463157654" />
                  <Point X="-3.494392578125" Y="0.011715777397" />
                  <Point X="-3.494897705078" Y="0.013343877792" />
                  <Point X="-3.514143554688" Y="0.039603408813" />
                  <Point X="-3.540380615234" Y="0.057813404083" />
                  <Point X="-3.557463867188" Y="0.066085334778" />
                  <Point X="-3.638504394531" Y="0.087800086975" />
                  <Point X="-4.998186523438" Y="0.452125762939" />
                  <Point X="-4.9219921875" Y="0.967040344238" />
                  <Point X="-4.917645019531" Y="0.996418701172" />
                  <Point X="-4.773516113281" Y="1.528298706055" />
                  <Point X="-4.758774414062" Y="1.526357910156" />
                  <Point X="-3.753266357422" Y="1.393980224609" />
                  <Point X="-3.731705810547" Y="1.395866333008" />
                  <Point X="-3.673634765625" Y="1.414176147461" />
                  <Point X="-3.670279785156" Y="1.415233886719" />
                  <Point X="-3.651534912109" Y="1.426055786133" />
                  <Point X="-3.639119873047" Y="1.443785888672" />
                  <Point X="-3.615818603516" Y="1.500040161133" />
                  <Point X="-3.614472412109" Y="1.503290161133" />
                  <Point X="-3.610714111328" Y="1.524604736328" />
                  <Point X="-3.616315429688" Y="1.545510742188" />
                  <Point X="-3.644430908203" Y="1.599520141602" />
                  <Point X="-3.646055175781" Y="1.60264050293" />
                  <Point X="-3.659968261719" Y="1.619221679688" />
                  <Point X="-3.706453369141" Y="1.654890869141" />
                  <Point X="-4.476105957031" Y="2.245466308594" />
                  <Point X="-4.17690625" Y="2.758066650391" />
                  <Point X="-4.160014160156" Y="2.787007080078" />
                  <Point X="-3.774671386719" Y="3.282310791016" />
                  <Point X="-3.159156982422" Y="2.926943359375" />
                  <Point X="-3.138513916016" Y="2.920434814453" />
                  <Point X="-3.057636962891" Y="2.913358886719" />
                  <Point X="-3.052964599609" Y="2.912950195312" />
                  <Point X="-3.031507080078" Y="2.915775146484" />
                  <Point X="-3.013253662109" Y="2.927403320312" />
                  <Point X="-2.955846679688" Y="2.984810302734" />
                  <Point X="-2.952530029297" Y="2.988126708984" />
                  <Point X="-2.940899902344" Y="3.006381103516" />
                  <Point X="-2.93807421875" Y="3.027840087891" />
                  <Point X="-2.945150146484" Y="3.108717041016" />
                  <Point X="-2.945558837891" Y="3.113389648438" />
                  <Point X="-2.952067382812" Y="3.134032958984" />
                  <Point X="-2.972666259766" Y="3.169711181641" />
                  <Point X="-3.307278564453" Y="3.749276855469" />
                  <Point X="-2.782294433594" Y="4.151776855469" />
                  <Point X="-2.752873779297" Y="4.174333496094" />
                  <Point X="-2.141548583984" Y="4.513972167969" />
                  <Point X="-1.967826904297" Y="4.287573242188" />
                  <Point X="-1.951248901367" Y="4.273662109375" />
                  <Point X="-1.861233398438" Y="4.226802734375" />
                  <Point X="-1.856032836914" Y="4.224095214844" />
                  <Point X="-1.835125488281" Y="4.2184921875" />
                  <Point X="-1.813809204102" Y="4.222250488281" />
                  <Point X="-1.720051879883" Y="4.261086425781" />
                  <Point X="-1.714635253906" Y="4.263330078125" />
                  <Point X="-1.696905761719" Y="4.275744140625" />
                  <Point X="-1.686083618164" Y="4.294487792969" />
                  <Point X="-1.655567382812" Y="4.391272949219" />
                  <Point X="-1.653804321289" Y="4.396864257812" />
                  <Point X="-1.651917724609" Y="4.418426757812" />
                  <Point X="-1.654259521484" Y="4.436215332031" />
                  <Point X="-1.689137573242" Y="4.701141113281" />
                  <Point X="-1.006178405762" Y="4.892619140625" />
                  <Point X="-0.968094421387" Y="4.903296386719" />
                  <Point X="-0.224463867188" Y="4.990327148438" />
                  <Point X="-0.224199737549" Y="4.990357910156" />
                  <Point X="-0.042140319824" Y="4.310903320312" />
                  <Point X="-0.024282100677" Y="4.284176757813" />
                  <Point X="0.006155994415" Y="4.273844238281" />
                  <Point X="0.036594051361" Y="4.284176757813" />
                  <Point X="0.054452308655" Y="4.310903320312" />
                  <Point X="0.065006896973" Y="4.350293945312" />
                  <Point X="0.236648406982" Y="4.990868652344" />
                  <Point X="0.826949951172" Y="4.929048339844" />
                  <Point X="0.860205627441" Y="4.925565429688" />
                  <Point X="1.475428955078" Y="4.777031738281" />
                  <Point X="1.508454956055" Y="4.769058105469" />
                  <Point X="1.90980078125" Y="4.623487304688" />
                  <Point X="1.931037353516" Y="4.615784667969" />
                  <Point X="2.318248779297" Y="4.434698730469" />
                  <Point X="2.338695800781" Y="4.425136230469" />
                  <Point X="2.712760253906" Y="4.207205566406" />
                  <Point X="2.732523925781" Y="4.19569140625" />
                  <Point X="3.068740234375" Y="3.956592529297" />
                  <Point X="3.044420898438" Y="3.914469970703" />
                  <Point X="2.229853515625" Y="2.503597900391" />
                  <Point X="2.224851806641" Y="2.491513916016" />
                  <Point X="2.2045546875" Y="2.415612792969" />
                  <Point X="2.202044189453" Y="2.392328613281" />
                  <Point X="2.209958496094" Y="2.326695800781" />
                  <Point X="2.210415527344" Y="2.322903808594" />
                  <Point X="2.218683105469" Y="2.300810791016" />
                  <Point X="2.259294433594" Y="2.240959960938" />
                  <Point X="2.274940185547" Y="2.224203613281" />
                  <Point X="2.334791015625" Y="2.183592285156" />
                  <Point X="2.338248779297" Y="2.18124609375" />
                  <Point X="2.360336181641" Y="2.172980224609" />
                  <Point X="2.425969238281" Y="2.165065917969" />
                  <Point X="2.448664306641" Y="2.165946289062" />
                  <Point X="2.524565673828" Y="2.186243408203" />
                  <Point X="2.541033935547" Y="2.192417480469" />
                  <Point X="2.622544921875" Y="2.239478027344" />
                  <Point X="3.994248046875" Y="3.031430664062" />
                  <Point X="4.190868652344" Y="2.758172363281" />
                  <Point X="4.202590820313" Y="2.741881103516" />
                  <Point X="4.387513183594" Y="2.436296142578" />
                  <Point X="4.3610703125" Y="2.416005859375" />
                  <Point X="3.288616210938" Y="1.593082885742" />
                  <Point X="3.279371582031" Y="1.583833496094" />
                  <Point X="3.224745361328" Y="1.512569335938" />
                  <Point X="3.213119384766" Y="1.491499511719" />
                  <Point X="3.192770996094" Y="1.418738647461" />
                  <Point X="3.191595458984" Y="1.414534912109" />
                  <Point X="3.190779785156" Y="1.39096472168" />
                  <Point X="3.207483642578" Y="1.310008911133" />
                  <Point X="3.215647216797" Y="1.287953613281" />
                  <Point X="3.261079833984" Y="1.218897949219" />
                  <Point X="3.263704833984" Y="1.214908325195" />
                  <Point X="3.280948242188" Y="1.198819702148" />
                  <Point X="3.346786621094" Y="1.161758422852" />
                  <Point X="3.368565429688" Y="1.153619628906" />
                  <Point X="3.457583251953" Y="1.141854736328" />
                  <Point X="3.475803710938" Y="1.14117175293" />
                  <Point X="3.553233886719" Y="1.151365600586" />
                  <Point X="4.848975097656" Y="1.321953125" />
                  <Point X="4.934153320312" Y="0.972066223145" />
                  <Point X="4.939189453125" Y="0.951380615234" />
                  <Point X="4.997858886719" Y="0.574556213379" />
                  <Point X="4.970223144531" Y="0.567151245117" />
                  <Point X="3.741167724609" Y="0.237826904297" />
                  <Point X="3.729086914062" Y="0.232819396973" />
                  <Point X="3.641629638672" Y="0.182267501831" />
                  <Point X="3.622264160156" Y="0.166925811768" />
                  <Point X="3.569789794922" Y="0.10006111908" />
                  <Point X="3.566758300781" Y="0.096198120117" />
                  <Point X="3.556985595703" Y="0.074736946106" />
                  <Point X="3.539494140625" Y="-0.016596868515" />
                  <Point X="3.538483398438" Y="-0.040686458588" />
                  <Point X="3.555974853516" Y="-0.132020278931" />
                  <Point X="3.556985595703" Y="-0.137297042847" />
                  <Point X="3.566758300781" Y="-0.158758056641" />
                  <Point X="3.619232666016" Y="-0.225622756958" />
                  <Point X="3.636576904297" Y="-0.241907104492" />
                  <Point X="3.724034179688" Y="-0.292458862305" />
                  <Point X="3.741167724609" Y="-0.300386993408" />
                  <Point X="3.812174804688" Y="-0.319413238525" />
                  <Point X="4.998067871094" Y="-0.637172302246" />
                  <Point X="4.951243164062" Y="-0.94775213623" />
                  <Point X="4.948430664062" Y="-0.966405761719" />
                  <Point X="4.874546386719" Y="-1.290178344727" />
                  <Point X="4.837340820312" Y="-1.285280151367" />
                  <Point X="3.411982177734" Y="-1.097628173828" />
                  <Point X="3.3948359375" Y="-1.098341186523" />
                  <Point X="3.223188232422" Y="-1.135649536133" />
                  <Point X="3.213271728516" Y="-1.137804931641" />
                  <Point X="3.185445068359" Y="-1.154697387695" />
                  <Point X="3.081694824219" Y="-1.2794765625" />
                  <Point X="3.075700927734" Y="-1.286685424805" />
                  <Point X="3.064357910156" Y="-1.3140703125" />
                  <Point X="3.049488037109" Y="-1.475664794922" />
                  <Point X="3.04862890625" Y="-1.485000488281" />
                  <Point X="3.056360595703" Y="-1.516622070312" />
                  <Point X="3.151352783203" Y="-1.664376220703" />
                  <Point X="3.168460693359" Y="-1.685540771484" />
                  <Point X="3.234355712891" Y="-1.736103759766" />
                  <Point X="4.33907421875" Y="-2.583784179688" />
                  <Point X="4.212055175781" Y="-2.789319824219" />
                  <Point X="4.204130859375" Y="-2.802143310547" />
                  <Point X="4.056688232422" Y="-3.011638183594" />
                  <Point X="4.022928955078" Y="-2.992147216797" />
                  <Point X="2.753454589844" Y="-2.259215576172" />
                  <Point X="2.737340332031" Y="-2.253312744141" />
                  <Point X="2.533052246094" Y="-2.216418457031" />
                  <Point X="2.52125" Y="-2.214287109375" />
                  <Point X="2.489078125" Y="-2.219244628906" />
                  <Point X="2.319364990234" Y="-2.308563476562" />
                  <Point X="2.309560058594" Y="-2.313723632812" />
                  <Point X="2.288599609375" Y="-2.334684082031" />
                  <Point X="2.199280761719" Y="-2.504397216797" />
                  <Point X="2.194120605469" Y="-2.514202148438" />
                  <Point X="2.189163330078" Y="-2.546374755859" />
                  <Point X="2.226057373047" Y="-2.750662597656" />
                  <Point X="2.234091552734" Y="-2.778578613281" />
                  <Point X="2.276435791016" Y="-2.851920654297" />
                  <Point X="2.986673583984" Y="-4.082088378906" />
                  <Point X="2.844703369141" Y="-4.183494628906" />
                  <Point X="2.835296386719" Y="-4.190213867188" />
                  <Point X="2.679775878906" Y="-4.290879394531" />
                  <Point X="2.651903564453" Y="-4.254555175781" />
                  <Point X="1.683177368164" Y="-2.992087402344" />
                  <Point X="1.670549194336" Y="-2.980467285156" />
                  <Point X="1.469066162109" Y="-2.850932128906" />
                  <Point X="1.45742578125" Y="-2.843448486328" />
                  <Point X="1.425804321289" Y="-2.835717041016" />
                  <Point X="1.205448242188" Y="-2.855994384766" />
                  <Point X="1.192717529297" Y="-2.857166015625" />
                  <Point X="1.165333007812" Y="-2.868509033203" />
                  <Point X="0.995179626465" Y="-3.009985839844" />
                  <Point X="0.985349243164" Y="-3.018159423828" />
                  <Point X="0.968456726074" Y="-3.045986328125" />
                  <Point X="0.917581726074" Y="-3.280051269531" />
                  <Point X="0.913929443359" Y="-3.310720214844" />
                  <Point X="0.925929443359" Y="-3.401869140625" />
                  <Point X="1.127642211914" Y="-4.934028320312" />
                  <Point X="1.003239318848" Y="-4.961297363281" />
                  <Point X="0.994346191406" Y="-4.963246582031" />
                  <Point X="0.860200317383" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#212" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.18268991153" Y="5.036932540226" Z="2.4" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="2.4" />
                  <Point X="-0.236432156461" Y="5.071268555169" Z="2.4" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="2.4" />
                  <Point X="-1.025832072416" Y="4.97204254565" Z="2.4" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="2.4" />
                  <Point X="-1.709987757464" Y="4.460968804913" Z="2.4" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="2.4" />
                  <Point X="-1.709408924438" Y="4.437588953684" Z="2.4" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="2.4" />
                  <Point X="-1.745341835888" Y="4.338560414803" Z="2.4" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="2.4" />
                  <Point X="-1.84429957467" Y="4.302431820138" Z="2.4" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="2.4" />
                  <Point X="-2.123367513053" Y="4.595669203726" Z="2.4" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="2.4" />
                  <Point X="-2.169913915662" Y="4.590111323244" Z="2.4" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="2.4" />
                  <Point X="-2.818872437545" Y="4.222736520606" Z="2.4" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="2.4" />
                  <Point X="-3.02212360544" Y="3.17599128166" Z="2.4" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="2.4" />
                  <Point X="-3.001115888145" Y="3.135640367216" Z="2.4" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="2.4" />
                  <Point X="-2.997356616673" Y="3.051446951214" Z="2.4" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="2.4" />
                  <Point X="-3.05943609839" Y="2.994448811028" Z="2.4" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="2.4" />
                  <Point X="-3.757868066637" Y="3.358070343305" Z="2.4" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="2.4" />
                  <Point X="-3.81616537674" Y="3.34959580224" Z="2.4" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="2.4" />
                  <Point X="-4.226244446013" Y="2.814551610882" Z="2.4" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="2.4" />
                  <Point X="-3.743047307239" Y="1.646503325818" Z="2.4" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="2.4" />
                  <Point X="-3.694937955841" Y="1.607713804039" Z="2.4" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="2.4" />
                  <Point X="-3.66816863159" Y="1.550454371025" Z="2.4" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="2.4" />
                  <Point X="-3.694824887816" Y="1.493142290329" Z="2.4" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="2.4" />
                  <Point X="-4.758404097995" Y="1.607210217955" Z="2.4" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="2.4" />
                  <Point X="-4.825034608714" Y="1.583347681407" Z="2.4" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="2.4" />
                  <Point X="-4.977609652661" Y="1.005639324835" Z="2.4" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="2.4" />
                  <Point X="-3.657601226934" Y="0.070783766587" Z="2.4" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="2.4" />
                  <Point X="-3.57504492644" Y="0.048016973882" Z="2.4" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="2.4" />
                  <Point X="-3.548302580985" Y="0.028178952145" Z="2.4" />
                  <Point X="-3.539556741714" Y="0" Z="2.4" />
                  <Point X="-3.54006202015" Y="-0.001627998916" Z="2.4" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="2.4" />
                  <Point X="-3.550323668683" Y="-0.030859009252" Z="2.4" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="2.4" />
                  <Point X="-4.979286561839" Y="-0.424928290169" Z="2.4" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="2.4" />
                  <Point X="-5.056085120672" Y="-0.476302168585" Z="2.4" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="2.4" />
                  <Point X="-4.975246858762" Y="-1.018699734387" Z="2.4" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="2.4" />
                  <Point X="-3.308063189821" Y="-1.318567779107" Z="2.4" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="2.4" />
                  <Point X="-3.217712459651" Y="-1.307714612656" Z="2.4" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="2.4" />
                  <Point X="-3.193097110694" Y="-1.324074547895" Z="2.4" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="2.4" />
                  <Point X="-4.43175944899" Y="-2.29706719851" Z="2.4" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="2.4" />
                  <Point X="-4.486867691943" Y="-2.378540461854" Z="2.4" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="2.4" />
                  <Point X="-4.1903960628" Y="-2.868795069957" Z="2.4" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="2.4" />
                  <Point X="-2.643264037996" Y="-2.596150596871" Z="2.4" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="2.4" />
                  <Point X="-2.571891954956" Y="-2.556438514175" Z="2.4" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="2.4" />
                  <Point X="-3.259266416298" Y="-3.791814344497" Z="2.4" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="2.4" />
                  <Point X="-3.277562634128" Y="-3.879457963605" Z="2.4" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="2.4" />
                  <Point X="-2.86647862735" Y="-4.192360430116" Z="2.4" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="2.4" />
                  <Point X="-2.238505639467" Y="-4.172460163397" Z="2.4" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="2.4" />
                  <Point X="-2.212132649631" Y="-4.147037777288" Z="2.4" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="2.4" />
                  <Point X="-1.951346826908" Y="-3.985192654499" Z="2.4" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="2.4" />
                  <Point X="-1.645926584536" Y="-4.015548862008" Z="2.4" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="2.4" />
                  <Point X="-1.422100085413" Y="-4.225560340652" Z="2.4" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="2.4" />
                  <Point X="-1.410465337788" Y="-4.859498224429" Z="2.4" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="2.4" />
                  <Point X="-1.396948631784" Y="-4.883658612471" Z="2.4" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="2.4" />
                  <Point X="-1.101030951122" Y="-4.958761219271" Z="2.4" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="2.4" />
                  <Point X="-0.438965768003" Y="-3.600426579333" Z="2.4" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="2.4" />
                  <Point X="-0.408144284789" Y="-3.505888648101" Z="2.4" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="2.4" />
                  <Point X="-0.23952108883" Y="-3.278577875258" Z="2.4" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="2.4" />
                  <Point X="0.01383799053" Y="-3.20853417003" Z="2.4" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="2.4" />
                  <Point X="0.262301574644" Y="-3.295757107667" Z="2.4" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="2.4" />
                  <Point X="0.795789092398" Y="-4.932109450482" Z="2.4" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="2.4" />
                  <Point X="0.827518021279" Y="-5.011973576113" Z="2.4" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="2.4" />
                  <Point X="1.007789136268" Y="-4.978857830813" Z="2.4" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="2.4" />
                  <Point X="0.96934577217" Y="-3.364062645198" Z="2.4" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="2.4" />
                  <Point X="0.960285014622" Y="-3.259390926651" Z="2.4" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="2.4" />
                  <Point X="1.020990384989" Y="-3.017152384156" Z="2.4" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="2.4" />
                  <Point X="1.203874503196" Y="-2.874503944451" Z="2.4" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="2.4" />
                  <Point X="1.435870852908" Y="-2.861710288658" Z="2.4" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="2.4" />
                  <Point X="2.606079902576" Y="-4.253713321856" Z="2.4" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="2.4" />
                  <Point X="2.672709592911" Y="-4.319748715576" Z="2.4" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="2.4" />
                  <Point X="2.867609453893" Y="-4.19290132919" Z="2.4" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="2.4" />
                  <Point X="2.313580979555" Y="-2.795640694662" Z="2.4" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="2.4" />
                  <Point X="2.269105382521" Y="-2.710496173684" Z="2.4" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="2.4" />
                  <Point X="2.237371380661" Y="-2.496403250868" Z="2.4" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="2.4" />
                  <Point X="2.336495127504" Y="-2.321529929306" Z="2.4" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="2.4" />
                  <Point X="2.518010629829" Y="-2.23434262697" Z="2.4" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="2.4" />
                  <Point X="3.991772902444" Y="-3.00416852072" Z="2.4" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="2.4" />
                  <Point X="4.074651691996" Y="-3.032962242477" Z="2.4" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="2.4" />
                  <Point X="4.248433089604" Y="-2.784324266097" Z="2.4" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="2.4" />
                  <Point X="3.258637685187" Y="-1.665155940725" Z="2.4" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="2.4" />
                  <Point X="3.187254780891" Y="-1.60605673778" Z="2.4" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="2.4" />
                  <Point X="3.093121591814" Y="-1.448966612158" Z="2.4" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="2.4" />
                  <Point X="3.113985469248" Y="-1.280163295571" Z="2.4" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="2.4" />
                  <Point X="3.227652227018" Y="-1.15322874758" Z="2.4" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="2.4" />
                  <Point X="4.824658661359" Y="-1.303572456453" Z="2.4" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="2.4" />
                  <Point X="4.911618236444" Y="-1.294205581716" Z="2.4" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="2.4" />
                  <Point X="4.994528502033" Y="-0.923926713606" Z="2.4" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="2.4" />
                  <Point X="3.818959633672" Y="-0.239836886629" Z="2.4" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="2.4" />
                  <Point X="3.742899980417" Y="-0.217890074587" Z="2.4" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="2.4" />
                  <Point X="3.652411083145" Y="-0.163475196355" Z="2.4" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="2.4" />
                  <Point X="3.59892617986" Y="-0.09133384794" Z="2.4" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="2.4" />
                  <Point X="3.582445270564" Y="0.005276683236" Z="2.4" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="2.4" />
                  <Point X="3.602968355256" Y="0.10047354222" Z="2.4" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="2.4" />
                  <Point X="3.660495433935" Y="0.170258769655" Z="2.4" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="2.4" />
                  <Point X="4.977008183635" Y="0.550135046139" Z="2.4" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="2.4" />
                  <Point X="5.044415663147" Y="0.592280004557" Z="2.4" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="2.4" />
                  <Point X="4.976578573803" Y="1.015173865753" Z="2.4" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="2.4" />
                  <Point X="3.540551525485" Y="1.232217953817" Z="2.4" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="2.4" />
                  <Point X="3.457978530828" Y="1.222703777824" Z="2.4" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="2.4" />
                  <Point X="3.365156901871" Y="1.236609742565" Z="2.4" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="2.4" />
                  <Point X="3.2966937398" Y="1.277660630194" Z="2.4" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="2.4" />
                  <Point X="3.250295950775" Y="1.351393504514" Z="2.4" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="2.4" />
                  <Point X="3.23476763069" Y="1.436552859337" Z="2.4" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="2.4" />
                  <Point X="3.258271938154" Y="1.51343089103" Z="2.4" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="2.4" />
                  <Point X="4.385352360735" Y="2.407618124804" Z="2.4" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="2.4" />
                  <Point X="4.435889710313" Y="2.474036585782" Z="2.4" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="2.4" />
                  <Point X="4.225298320337" Y="2.818655531677" Z="2.4" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="2.4" />
                  <Point X="2.59138905138" Y="2.314059238726" Z="2.4" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="2.4" />
                  <Point X="2.50549283725" Y="2.265826139558" Z="2.4" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="2.4" />
                  <Point X="2.425799801643" Y="2.245986402175" Z="2.4" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="2.4" />
                  <Point X="2.35670898658" Y="2.256246787236" Z="2.4" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="2.4" />
                  <Point X="2.294511812999" Y="2.300315873802" Z="2.4" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="2.4" />
                  <Point X="2.253443300584" Y="2.363958642189" Z="2.4" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="2.4" />
                  <Point X="2.2467018031" Y="2.433976738632" Z="2.4" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="2.4" />
                  <Point X="3.081566114134" Y="3.920750030575" Z="2.4" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="2.4" />
                  <Point X="3.108137767126" Y="4.016831677149" Z="2.4" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="2.4" />
                  <Point X="2.731774304051" Y="4.281688183243" Z="2.4" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="2.4" />
                  <Point X="2.333274730504" Y="4.511270283326" Z="2.4" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="2.4" />
                  <Point X="1.92069327249" Y="4.701771298796" Z="2.4" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="2.4" />
                  <Point X="1.481007573357" Y="4.856915338043" Z="2.4" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="2.4" />
                  <Point X="0.82600200102" Y="5.010056428966" Z="2.4" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="2.4" />
                  <Point X="0.010554650023" Y="4.394514965725" Z="2.4" />
                  <Point X="0" Y="4.355124473572" Z="2.4" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>