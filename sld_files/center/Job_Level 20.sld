<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#155" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1543" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="0" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000475830078" Y="0.004715820312" />
                  <Width Value="9.783896972656" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.71332611084" Y="-4.072421386719" />
                  <Point X="0.563302062988" Y="-3.512524414063" />
                  <Point X="0.557721191406" Y="-3.497141845703" />
                  <Point X="0.54236315918" Y="-3.467377197266" />
                  <Point X="0.481044555664" Y="-3.379028564453" />
                  <Point X="0.378635528564" Y="-3.231476806641" />
                  <Point X="0.356751586914" Y="-3.209021484375" />
                  <Point X="0.330496398926" Y="-3.189777587891" />
                  <Point X="0.302495178223" Y="-3.175669189453" />
                  <Point X="0.207608261108" Y="-3.146219726563" />
                  <Point X="0.049136341095" Y="-3.097035888672" />
                  <Point X="0.020976791382" Y="-3.092766601562" />
                  <Point X="-0.008664840698" Y="-3.092766601562" />
                  <Point X="-0.036824237823" Y="-3.097035888672" />
                  <Point X="-0.131711151123" Y="-3.126485107422" />
                  <Point X="-0.290183227539" Y="-3.175669189453" />
                  <Point X="-0.31818460083" Y="-3.189777587891" />
                  <Point X="-0.344439788818" Y="-3.209021484375" />
                  <Point X="-0.366323577881" Y="-3.231476806641" />
                  <Point X="-0.427642181396" Y="-3.319825195312" />
                  <Point X="-0.530051208496" Y="-3.467377197266" />
                  <Point X="-0.538189025879" Y="-3.481573730469" />
                  <Point X="-0.550990112305" Y="-3.512524414063" />
                  <Point X="-0.801547180176" Y="-4.447616699219" />
                  <Point X="-0.916584594727" Y="-4.876941894531" />
                  <Point X="-1.079340576172" Y="-4.845350585938" />
                  <Point X="-1.185858154297" Y="-4.817943847656" />
                  <Point X="-1.24641796875" Y="-4.802362304688" />
                  <Point X="-1.233022338867" Y="-4.700612792969" />
                  <Point X="-1.214963012695" Y="-4.563438476562" />
                  <Point X="-1.214201171875" Y="-4.5479296875" />
                  <Point X="-1.216508666992" Y="-4.516224121094" />
                  <Point X="-1.239177124023" Y="-4.402262207031" />
                  <Point X="-1.277036132812" Y="-4.211932617188" />
                  <Point X="-1.287938354492" Y="-4.182966308594" />
                  <Point X="-1.304010375977" Y="-4.15512890625" />
                  <Point X="-1.323644775391" Y="-4.131204101562" />
                  <Point X="-1.411004638672" Y="-4.054591552734" />
                  <Point X="-1.556905517578" Y="-3.926639404297" />
                  <Point X="-1.583188476562" Y="-3.910295410156" />
                  <Point X="-1.612885742188" Y="-3.897994384766" />
                  <Point X="-1.64302734375" Y="-3.890966308594" />
                  <Point X="-1.758973388672" Y="-3.883366699219" />
                  <Point X="-1.952616577148" Y="-3.870674804688" />
                  <Point X="-1.98341796875" Y="-3.873708496094" />
                  <Point X="-2.014466674805" Y="-3.882028076172" />
                  <Point X="-2.042657714844" Y="-3.894801513672" />
                  <Point X="-2.139270263672" Y="-3.959355957031" />
                  <Point X="-2.300624267578" Y="-4.067169677734" />
                  <Point X="-2.312785644531" Y="-4.076821533203" />
                  <Point X="-2.335102783203" Y="-4.099461914062" />
                  <Point X="-2.475771240234" Y="-4.282784667969" />
                  <Point X="-2.480149169922" Y="-4.288489746094" />
                  <Point X="-2.533175537109" Y="-4.255657226562" />
                  <Point X="-2.801706787109" Y="-4.089389404297" />
                  <Point X="-2.949204101562" Y="-3.975821533203" />
                  <Point X="-3.104721923828" Y="-3.856077880859" />
                  <Point X="-2.702563964844" Y="-3.159520019531" />
                  <Point X="-2.423761230469" Y="-2.676619628906" />
                  <Point X="-2.412859130859" Y="-2.647653320312" />
                  <Point X="-2.406588134766" Y="-2.616126953125" />
                  <Point X="-2.405575439453" Y="-2.585192871094" />
                  <Point X="-2.414560058594" Y="-2.555575195312" />
                  <Point X="-2.428777099609" Y="-2.526746337891" />
                  <Point X="-2.4468046875" Y="-2.501589355469" />
                  <Point X="-2.464153320312" Y="-2.484240722656" />
                  <Point X="-2.489310302734" Y="-2.466213134766" />
                  <Point X="-2.518139160156" Y="-2.45199609375" />
                  <Point X="-2.547756835938" Y="-2.443011474609" />
                  <Point X="-2.578690917969" Y="-2.444024169922" />
                  <Point X="-2.610217285156" Y="-2.450295166016" />
                  <Point X="-2.63918359375" Y="-2.461197265625" />
                  <Point X="-3.445682617188" Y="-2.926829589844" />
                  <Point X="-3.818024414062" Y="-3.141801269531" />
                  <Point X="-3.870703857422" Y="-3.072591064453" />
                  <Point X="-4.082859863281" Y="-2.793861572266" />
                  <Point X="-4.188600097656" Y="-2.61655078125" />
                  <Point X="-4.306142578125" Y="-2.419450439453" />
                  <Point X="-3.593768798828" Y="-1.872826416016" />
                  <Point X="-3.105954589844" Y="-1.498513549805" />
                  <Point X="-3.084576904297" Y="-1.475592895508" />
                  <Point X="-3.066612060547" Y="-1.448461425781" />
                  <Point X="-3.053856689453" Y="-1.419832641602" />
                  <Point X="-3.046152099609" Y="-1.390085449219" />
                  <Point X="-3.04334765625" Y="-1.35965612793" />
                  <Point X="-3.045556640625" Y="-1.327985595703" />
                  <Point X="-3.052557861328" Y="-1.298240722656" />
                  <Point X="-3.068640136719" Y="-1.272257324219" />
                  <Point X="-3.08947265625" Y="-1.248301025391" />
                  <Point X="-3.112972412109" Y="-1.228767089844" />
                  <Point X="-3.139455078125" Y="-1.213180664062" />
                  <Point X="-3.168718261719" Y="-1.201956420898" />
                  <Point X="-3.200605957031" Y="-1.195474853516" />
                  <Point X="-3.231929199219" Y="-1.194383789062" />
                  <Point X="-4.250057617188" Y="-1.328422973633" />
                  <Point X="-4.7321015625" Y="-1.391885375977" />
                  <Point X="-4.751101074219" Y="-1.31750378418" />
                  <Point X="-4.834077636719" Y="-0.992654541016" />
                  <Point X="-4.862053710938" Y="-0.79704888916" />
                  <Point X="-4.892424316406" Y="-0.584698181152" />
                  <Point X="-4.088569824219" Y="-0.369306091309" />
                  <Point X="-3.532875976562" Y="-0.220408340454" />
                  <Point X="-3.511300537109" Y="-0.211675231934" />
                  <Point X="-3.485338867188" Y="-0.197240875244" />
                  <Point X="-3.477335449219" Y="-0.192255783081" />
                  <Point X="-3.459976318359" Y="-0.180207717896" />
                  <Point X="-3.436020996094" Y="-0.158681106567" />
                  <Point X="-3.414750488281" Y="-0.12746647644" />
                  <Point X="-3.403860351562" Y="-0.101565864563" />
                  <Point X="-3.399058837891" Y="-0.086920219421" />
                  <Point X="-3.391025390625" Y="-0.053455444336" />
                  <Point X="-3.388405273438" Y="-0.032193256378" />
                  <Point X="-3.390615966797" Y="-0.010884608269" />
                  <Point X="-3.396327636719" Y="0.015099481583" />
                  <Point X="-3.400779296875" Y="0.029664682388" />
                  <Point X="-3.413990966797" Y="0.063045822144" />
                  <Point X="-3.425201416016" Y="0.083556602478" />
                  <Point X="-3.441080810547" Y="0.100709121704" />
                  <Point X="-3.463293212891" Y="0.119440818787" />
                  <Point X="-3.470369384766" Y="0.124861099243" />
                  <Point X="-3.487728271484" Y="0.136909164429" />
                  <Point X="-3.501924804688" Y="0.145046951294" />
                  <Point X="-3.532875976562" Y="0.157848342896" />
                  <Point X="-4.460948242188" Y="0.40652444458" />
                  <Point X="-4.89181640625" Y="0.521975219727" />
                  <Point X="-4.877964355469" Y="0.61558581543" />
                  <Point X="-4.824488769531" Y="0.976970214844" />
                  <Point X="-4.768171875" Y="1.184797241211" />
                  <Point X="-4.703551269531" Y="1.423267822266" />
                  <Point X="-4.164001464844" Y="1.352234741211" />
                  <Point X="-3.765666259766" Y="1.29979284668" />
                  <Point X="-3.744985839844" Y="1.299341674805" />
                  <Point X="-3.723424072266" Y="1.301228271484" />
                  <Point X="-3.703137451172" Y="1.305263671875" />
                  <Point X="-3.680132568359" Y="1.312516967773" />
                  <Point X="-3.641711425781" Y="1.324631225586" />
                  <Point X="-3.622778320312" Y="1.332962036133" />
                  <Point X="-3.604034179688" Y="1.343784057617" />
                  <Point X="-3.587353271484" Y="1.356015136719" />
                  <Point X="-3.57371484375" Y="1.37156652832" />
                  <Point X="-3.561300292969" Y="1.389296508789" />
                  <Point X="-3.5513515625" Y="1.407430908203" />
                  <Point X="-3.542120605469" Y="1.429715942383" />
                  <Point X="-3.526704101563" Y="1.466934936523" />
                  <Point X="-3.520915527344" Y="1.486794189453" />
                  <Point X="-3.517157226562" Y="1.508109130859" />
                  <Point X="-3.5158046875" Y="1.528749389648" />
                  <Point X="-3.518951171875" Y="1.549192871094" />
                  <Point X="-3.524552978516" Y="1.570099365234" />
                  <Point X="-3.532049804688" Y="1.589377563477" />
                  <Point X="-3.543187744141" Y="1.61077355957" />
                  <Point X="-3.561789550781" Y="1.646507446289" />
                  <Point X="-3.573281738281" Y="1.663706665039" />
                  <Point X="-3.587194335938" Y="1.680286865234" />
                  <Point X="-3.602135986328" Y="1.694590209961" />
                  <Point X="-4.13448046875" Y="2.103072509766" />
                  <Point X="-4.351859863281" Y="2.269874267578" />
                  <Point X="-4.288942382813" Y="2.377667480469" />
                  <Point X="-4.081153808594" Y="2.733659423828" />
                  <Point X="-3.931977539063" Y="2.925404296875" />
                  <Point X="-3.750504882813" Y="3.158661865234" />
                  <Point X="-3.451303222656" Y="2.985917480469" />
                  <Point X="-3.206656982422" Y="2.844670898438" />
                  <Point X="-3.187724121094" Y="2.836340332031" />
                  <Point X="-3.167081787109" Y="2.829831787109" />
                  <Point X="-3.146794921875" Y="2.825796386719" />
                  <Point X="-3.114755371094" Y="2.822993164062" />
                  <Point X="-3.061245605469" Y="2.818311767578" />
                  <Point X="-3.040564941406" Y="2.818762939453" />
                  <Point X="-3.019106201172" Y="2.821588134766" />
                  <Point X="-2.999014648438" Y="2.826504638672" />
                  <Point X="-2.980463134766" Y="2.835653076172" />
                  <Point X="-2.962208984375" Y="2.847282226562" />
                  <Point X="-2.946077392578" Y="2.860229492188" />
                  <Point X="-2.923335449219" Y="2.882971435547" />
                  <Point X="-2.885353759766" Y="2.920952880859" />
                  <Point X="-2.872406738281" Y="2.937084228516" />
                  <Point X="-2.86077734375" Y="2.955338623047" />
                  <Point X="-2.851628662109" Y="2.973890136719" />
                  <Point X="-2.846712158203" Y="2.993981933594" />
                  <Point X="-2.843886962891" Y="3.015440673828" />
                  <Point X="-2.843435791016" Y="3.03612109375" />
                  <Point X="-2.846239013672" Y="3.068160888672" />
                  <Point X="-2.850920410156" Y="3.121670654297" />
                  <Point X="-2.854955810547" Y="3.141957763672" />
                  <Point X="-2.861464355469" Y="3.162600097656" />
                  <Point X="-2.869794921875" Y="3.181532958984" />
                  <Point X="-3.105692871094" Y="3.590120117188" />
                  <Point X="-3.183333007812" Y="3.724596435547" />
                  <Point X="-3.062523925781" Y="3.817219726562" />
                  <Point X="-2.700625244141" Y="4.094683837891" />
                  <Point X="-2.465679931641" Y="4.225214355469" />
                  <Point X="-2.167036621094" Y="4.391134277344" />
                  <Point X="-2.118089111328" Y="4.327344726562" />
                  <Point X="-2.04319543457" Y="4.229741210938" />
                  <Point X="-2.028892456055" Y="4.214799804688" />
                  <Point X="-2.01231237793" Y="4.200887207031" />
                  <Point X="-1.99511315918" Y="4.18939453125" />
                  <Point X="-1.959453369141" Y="4.170831054688" />
                  <Point X="-1.899897094727" Y="4.139827636719" />
                  <Point X="-1.88061730957" Y="4.132330566406" />
                  <Point X="-1.85971081543" Y="4.126729003906" />
                  <Point X="-1.839267822266" Y="4.123582519531" />
                  <Point X="-1.818628417969" Y="4.124935546875" />
                  <Point X="-1.797313110352" Y="4.128693847656" />
                  <Point X="-1.777453125" Y="4.134482421875" />
                  <Point X="-1.740310913086" Y="4.149867675781" />
                  <Point X="-1.678279174805" Y="4.175562011719" />
                  <Point X="-1.660144775391" Y="4.185510742187" />
                  <Point X="-1.642415161133" Y="4.197925292969" />
                  <Point X="-1.626863769531" Y="4.211563476563" />
                  <Point X="-1.6146328125" Y="4.228244628906" />
                  <Point X="-1.603810913086" Y="4.246988769531" />
                  <Point X="-1.59548046875" Y="4.265921386719" />
                  <Point X="-1.583391235352" Y="4.304263183594" />
                  <Point X="-1.563201171875" Y="4.368297851562" />
                  <Point X="-1.559165649414" Y="4.388584960938" />
                  <Point X="-1.557279174805" Y="4.410146484375" />
                  <Point X="-1.55773034668" Y="4.430826660156" />
                  <Point X="-1.584202026367" Y="4.6318984375" />
                  <Point X="-1.418134887695" Y="4.678458007812" />
                  <Point X="-0.949638366699" Y="4.809808105469" />
                  <Point X="-0.664816101074" Y="4.843142089844" />
                  <Point X="-0.294710784912" Y="4.886457519531" />
                  <Point X="-0.206276519775" Y="4.556416015625" />
                  <Point X="-0.133903305054" Y="4.286315429688" />
                  <Point X="-0.121129844666" Y="4.258124023438" />
                  <Point X="-0.10327155304" Y="4.231397460938" />
                  <Point X="-0.082114547729" Y="4.20880859375" />
                  <Point X="-0.054819446564" Y="4.19421875" />
                  <Point X="-0.024381370544" Y="4.183886230469" />
                  <Point X="0.006155913353" Y="4.178844238281" />
                  <Point X="0.03669316864" Y="4.183886230469" />
                  <Point X="0.067131248474" Y="4.19421875" />
                  <Point X="0.094426460266" Y="4.20880859375" />
                  <Point X="0.115583503723" Y="4.231397460938" />
                  <Point X="0.133441802979" Y="4.258124023438" />
                  <Point X="0.146215255737" Y="4.286315429688" />
                  <Point X="0.267086730957" Y="4.7374140625" />
                  <Point X="0.307419403076" Y="4.8879375" />
                  <Point X="0.434966461182" Y="4.874580078125" />
                  <Point X="0.84404119873" Y="4.831738769531" />
                  <Point X="1.079685302734" Y="4.774847167969" />
                  <Point X="1.481026000977" Y="4.677951171875" />
                  <Point X="1.633480712891" Y="4.622654296875" />
                  <Point X="1.894646484375" Y="4.527927734375" />
                  <Point X="2.0429609375" Y="4.458565917969" />
                  <Point X="2.294576416016" Y="4.340893554688" />
                  <Point X="2.437883789062" Y="4.25740234375" />
                  <Point X="2.680978759766" Y="4.115774902344" />
                  <Point X="2.816112060547" Y="4.019675537109" />
                  <Point X="2.943260009766" Y="3.929254638672" />
                  <Point X="2.470274169922" Y="3.110018798828" />
                  <Point X="2.147581054688" Y="2.551097900391" />
                  <Point X="2.142075683594" Y="2.539930664062" />
                  <Point X="2.133076904297" Y="2.516056640625" />
                  <Point X="2.125036132813" Y="2.485988037109" />
                  <Point X="2.111607177734" Y="2.435770263672" />
                  <Point X="2.108619384766" Y="2.417934326172" />
                  <Point X="2.107727783203" Y="2.380952880859" />
                  <Point X="2.110863037109" Y="2.354952148438" />
                  <Point X="2.116099121094" Y="2.311528076172" />
                  <Point X="2.121442138672" Y="2.289604980469" />
                  <Point X="2.129708251953" Y="2.267516357422" />
                  <Point X="2.140071044922" Y="2.247470947266" />
                  <Point X="2.156159423828" Y="2.223760986328" />
                  <Point X="2.183028808594" Y="2.184162353516" />
                  <Point X="2.194464111328" Y="2.170329345703" />
                  <Point X="2.221598388672" Y="2.145592529297" />
                  <Point X="2.245308349609" Y="2.129504150391" />
                  <Point X="2.284906982422" Y="2.102635009766" />
                  <Point X="2.304952880859" Y="2.092271972656" />
                  <Point X="2.327041259766" Y="2.084006103516" />
                  <Point X="2.348963867188" Y="2.078663330078" />
                  <Point X="2.374964599609" Y="2.075528076172" />
                  <Point X="2.418388671875" Y="2.070291748047" />
                  <Point X="2.436467529297" Y="2.069845703125" />
                  <Point X="2.473206298828" Y="2.074171142578" />
                  <Point X="2.503274902344" Y="2.082211914062" />
                  <Point X="2.553492675781" Y="2.095640869141" />
                  <Point X="2.565283447266" Y="2.099638183594" />
                  <Point X="2.588533935547" Y="2.110145019531" />
                  <Point X="3.52199609375" Y="2.649079589844" />
                  <Point X="3.967326416016" Y="2.906191162109" />
                  <Point X="3.979075927734" Y="2.889862060547" />
                  <Point X="4.123270507813" Y="2.689464111328" />
                  <Point X="4.198607421875" Y="2.564969726562" />
                  <Point X="4.262198730469" Y="2.459884277344" />
                  <Point X="3.655639648438" Y="1.994454589844" />
                  <Point X="3.230783935547" Y="1.668451293945" />
                  <Point X="3.221424560547" Y="1.660241455078" />
                  <Point X="3.203973876953" Y="1.641627929688" />
                  <Point X="3.182333496094" Y="1.613396362305" />
                  <Point X="3.146191650391" Y="1.566246459961" />
                  <Point X="3.13660546875" Y="1.550911254883" />
                  <Point X="3.121629882812" Y="1.5170859375" />
                  <Point X="3.113568847656" Y="1.488261474609" />
                  <Point X="3.100105957031" Y="1.440121459961" />
                  <Point X="3.096652587891" Y="1.417823242188" />
                  <Point X="3.095836425781" Y="1.394252929688" />
                  <Point X="3.097739257812" Y="1.371768188477" />
                  <Point X="3.104356445312" Y="1.339697265625" />
                  <Point X="3.115408203125" Y="1.286135498047" />
                  <Point X="3.120679931641" Y="1.268977416992" />
                  <Point X="3.136282470703" Y="1.235740600586" />
                  <Point X="3.154280761719" Y="1.208383789062" />
                  <Point X="3.184340087891" Y="1.16269543457" />
                  <Point X="3.198893798828" Y="1.145450195312" />
                  <Point X="3.216137695313" Y="1.129360595703" />
                  <Point X="3.234347167969" Y="1.116034912109" />
                  <Point X="3.260429199219" Y="1.101352905273" />
                  <Point X="3.303989257812" Y="1.076832519531" />
                  <Point X="3.320520507812" Y="1.069502197266" />
                  <Point X="3.356118164062" Y="1.059438598633" />
                  <Point X="3.3913828125" Y="1.054777832031" />
                  <Point X="3.450278808594" Y="1.046994018555" />
                  <Point X="3.462702392578" Y="1.046175048828" />
                  <Point X="3.488203613281" Y="1.04698449707" />
                  <Point X="4.374930175781" Y="1.163724365234" />
                  <Point X="4.776839355469" Y="1.21663659668" />
                  <Point X="4.784004882812" Y="1.187203369141" />
                  <Point X="4.845936035156" Y="0.932809204102" />
                  <Point X="4.869676269531" Y="0.780328979492" />
                  <Point X="4.890864746094" Y="0.644238464355" />
                  <Point X="4.203473632812" Y="0.460052734375" />
                  <Point X="3.716579833984" Y="0.329589935303" />
                  <Point X="3.704791259766" Y="0.325586608887" />
                  <Point X="3.681545654297" Y="0.315067993164" />
                  <Point X="3.646899169922" Y="0.2950418396" />
                  <Point X="3.589035644531" Y="0.261595581055" />
                  <Point X="3.574311035156" Y="0.251096221924" />
                  <Point X="3.547531005859" Y="0.22557661438" />
                  <Point X="3.526743164062" Y="0.199087982178" />
                  <Point X="3.492025146484" Y="0.154848968506" />
                  <Point X="3.480301025391" Y="0.135569152832" />
                  <Point X="3.470527099609" Y="0.114105384827" />
                  <Point X="3.463680908203" Y="0.092604423523" />
                  <Point X="3.456751464844" Y="0.056422294617" />
                  <Point X="3.445178710938" Y="-0.004006073952" />
                  <Point X="3.443482910156" Y="-0.021875295639" />
                  <Point X="3.445178710938" Y="-0.058554080963" />
                  <Point X="3.452108154297" Y="-0.094736061096" />
                  <Point X="3.463680908203" Y="-0.155164428711" />
                  <Point X="3.470527099609" Y="-0.176665542603" />
                  <Point X="3.480301025391" Y="-0.198129302979" />
                  <Point X="3.492025146484" Y="-0.217408966064" />
                  <Point X="3.512812988281" Y="-0.243897613525" />
                  <Point X="3.547531005859" Y="-0.288136779785" />
                  <Point X="3.559999023438" Y="-0.30123614502" />
                  <Point X="3.589035888672" Y="-0.324155578613" />
                  <Point X="3.623682373047" Y="-0.344181884766" />
                  <Point X="3.681545898438" Y="-0.377628143311" />
                  <Point X="3.692710205078" Y="-0.383139038086" />
                  <Point X="3.716579833984" Y="-0.392149932861" />
                  <Point X="4.529748535156" Y="-0.610037780762" />
                  <Point X="4.89147265625" Y="-0.706961547852" />
                  <Point X="4.889602539062" Y="-0.719366027832" />
                  <Point X="4.855022460938" Y="-0.948725097656" />
                  <Point X="4.824606933594" Y="-1.082010620117" />
                  <Point X="4.801173339844" Y="-1.184698974609" />
                  <Point X="3.989041992188" Y="-1.077779785156" />
                  <Point X="3.424382080078" Y="-1.003441040039" />
                  <Point X="3.40803515625" Y="-1.002710327148" />
                  <Point X="3.374658447266" Y="-1.005508850098" />
                  <Point X="3.306659667969" Y="-1.020288696289" />
                  <Point X="3.193094238281" Y="-1.04497253418" />
                  <Point X="3.163973876953" Y="-1.056597167969" />
                  <Point X="3.136147460938" Y="-1.073489379883" />
                  <Point X="3.112397460938" Y="-1.093960083008" />
                  <Point X="3.071296386719" Y="-1.143391601562" />
                  <Point X="3.002653320312" Y="-1.225948120117" />
                  <Point X="2.987932617188" Y="-1.250330688477" />
                  <Point X="2.976589355469" Y="-1.277715942383" />
                  <Point X="2.969757568359" Y="-1.305365356445" />
                  <Point X="2.963866699219" Y="-1.369381347656" />
                  <Point X="2.954028564453" Y="-1.476295532227" />
                  <Point X="2.956347167969" Y="-1.507564208984" />
                  <Point X="2.964078613281" Y="-1.539185180664" />
                  <Point X="2.976450195312" Y="-1.567996826172" />
                  <Point X="3.014081787109" Y="-1.626530151367" />
                  <Point X="3.076930419922" Y="-1.724287109375" />
                  <Point X="3.086932861328" Y="-1.737239013672" />
                  <Point X="3.110628417969" Y="-1.760909179688" />
                  <Point X="3.86525390625" Y="-2.339953857422" />
                  <Point X="4.213122558594" Y="-2.606883056641" />
                  <Point X="4.124810546875" Y="-2.749785400391" />
                  <Point X="4.061908203125" Y="-2.839160400391" />
                  <Point X="4.028981445312" Y="-2.885945068359" />
                  <Point X="3.303860351562" Y="-2.467296142578" />
                  <Point X="2.800954589844" Y="-2.176943115234" />
                  <Point X="2.786131591797" Y="-2.170012451172" />
                  <Point X="2.754224609375" Y="-2.159825195312" />
                  <Point X="2.673295410156" Y="-2.145209472656" />
                  <Point X="2.538134277344" Y="-2.120799560547" />
                  <Point X="2.506781982422" Y="-2.120395263672" />
                  <Point X="2.474609130859" Y="-2.125353027344" />
                  <Point X="2.444833496094" Y="-2.135176757812" />
                  <Point X="2.377601074219" Y="-2.170560546875" />
                  <Point X="2.265315429688" Y="-2.229655761719" />
                  <Point X="2.242384033203" Y="-2.246549072266" />
                  <Point X="2.221424316406" Y="-2.267509033203" />
                  <Point X="2.204531982422" Y="-2.290439453125" />
                  <Point X="2.169147949219" Y="-2.357671875" />
                  <Point X="2.110052978516" Y="-2.469957519531" />
                  <Point X="2.100229003906" Y="-2.499733398438" />
                  <Point X="2.095271240234" Y="-2.531906005859" />
                  <Point X="2.095675537109" Y="-2.563258300781" />
                  <Point X="2.110291259766" Y="-2.6441875" />
                  <Point X="2.134701171875" Y="-2.779348632812" />
                  <Point X="2.138985595703" Y="-2.795141845703" />
                  <Point X="2.151819091797" Y="-2.826078613281" />
                  <Point X="2.636741943359" Y="-3.665989257812" />
                  <Point X="2.861283447266" Y="-4.054906738281" />
                  <Point X="2.781851806641" Y="-4.111643066406" />
                  <Point X="2.7115234375" Y="-4.157165039062" />
                  <Point X="2.701765136719" Y="-4.163481445312" />
                  <Point X="2.142309814453" Y="-3.434385498047" />
                  <Point X="1.758546142578" Y="-2.934255126953" />
                  <Point X="1.747503540039" Y="-2.922178955078" />
                  <Point X="1.721924194336" Y="-2.900557373047" />
                  <Point X="1.642106079102" Y="-2.849241699219" />
                  <Point X="1.50880078125" Y="-2.763538574219" />
                  <Point X="1.479989746094" Y="-2.751167236328" />
                  <Point X="1.448368774414" Y="-2.743435546875" />
                  <Point X="1.417099731445" Y="-2.741116699219" />
                  <Point X="1.329805053711" Y="-2.749149658203" />
                  <Point X="1.184012817383" Y="-2.762565673828" />
                  <Point X="1.15636315918" Y="-2.769397460938" />
                  <Point X="1.128978027344" Y="-2.780740722656" />
                  <Point X="1.104595947266" Y="-2.795461181641" />
                  <Point X="1.037189086914" Y="-2.851507568359" />
                  <Point X="0.924612121582" Y="-2.945111572266" />
                  <Point X="0.904141052246" Y="-2.968861816406" />
                  <Point X="0.887248779297" Y="-2.996688476562" />
                  <Point X="0.875624206543" Y="-3.025808837891" />
                  <Point X="0.855470092773" Y="-3.118534423828" />
                  <Point X="0.821809936523" Y="-3.273396728516" />
                  <Point X="0.819724487305" Y="-3.289627441406" />
                  <Point X="0.819742248535" Y="-3.323120117188" />
                  <Point X="0.957165710449" Y="-4.366954101563" />
                  <Point X="1.022065307617" Y="-4.859915039062" />
                  <Point X="0.975679077148" Y="-4.870083007812" />
                  <Point X="0.929315673828" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058431762695" Y="-4.752636230469" />
                  <Point X="-1.141246337891" Y="-4.731328125" />
                  <Point X="-1.138835083008" Y="-4.713012695312" />
                  <Point X="-1.120775756836" Y="-4.575838378906" />
                  <Point X="-1.120077392578" Y="-4.568099609375" />
                  <Point X="-1.119451660156" Y="-4.541033691406" />
                  <Point X="-1.121759277344" Y="-4.509328125" />
                  <Point X="-1.123334106445" Y="-4.497690429688" />
                  <Point X="-1.146002563477" Y="-4.383728515625" />
                  <Point X="-1.183861572266" Y="-4.193398925781" />
                  <Point X="-1.18812512207" Y="-4.17846875" />
                  <Point X="-1.19902734375" Y="-4.149502441406" />
                  <Point X="-1.205666137695" Y="-4.135466308594" />
                  <Point X="-1.221738037109" Y="-4.10762890625" />
                  <Point X="-1.23057409668" Y="-4.094861816406" />
                  <Point X="-1.250208496094" Y="-4.070937011719" />
                  <Point X="-1.261006958008" Y="-4.059779296875" />
                  <Point X="-1.348366821289" Y="-3.983166748047" />
                  <Point X="-1.494267700195" Y="-3.855214599609" />
                  <Point X="-1.506738647461" Y="-3.845965576172" />
                  <Point X="-1.533021606445" Y="-3.829621582031" />
                  <Point X="-1.546833496094" Y="-3.822526855469" />
                  <Point X="-1.576530761719" Y="-3.810225830078" />
                  <Point X="-1.591313354492" Y="-3.805476074219" />
                  <Point X="-1.621455078125" Y="-3.798447998047" />
                  <Point X="-1.636813964844" Y="-3.796169677734" />
                  <Point X="-1.752759887695" Y="-3.788570068359" />
                  <Point X="-1.946403198242" Y="-3.775878173828" />
                  <Point X="-1.961928222656" Y="-3.776132324219" />
                  <Point X="-1.992729614258" Y="-3.779166015625" />
                  <Point X="-2.008006103516" Y="-3.781945556641" />
                  <Point X="-2.03905480957" Y="-3.790265136719" />
                  <Point X="-2.053674560547" Y="-3.795496337891" />
                  <Point X="-2.081865478516" Y="-3.808269775391" />
                  <Point X="-2.095436767578" Y="-3.815812011719" />
                  <Point X="-2.192049316406" Y="-3.880366455078" />
                  <Point X="-2.353403320312" Y="-3.988180175781" />
                  <Point X="-2.359681640625" Y="-3.992757080078" />
                  <Point X="-2.380442138672" Y="-4.010131103516" />
                  <Point X="-2.402759277344" Y="-4.032771484375" />
                  <Point X="-2.410471435547" Y="-4.041629638672" />
                  <Point X="-2.503202880859" Y="-4.162479492188" />
                  <Point X="-2.747583740234" Y="-4.011164794922" />
                  <Point X="-2.891246826172" Y="-3.900549072266" />
                  <Point X="-2.980862792969" Y="-3.831547607422" />
                  <Point X="-2.620291503906" Y="-3.207020019531" />
                  <Point X="-2.341488769531" Y="-2.724119628906" />
                  <Point X="-2.334850097656" Y="-2.710083251953" />
                  <Point X="-2.323947998047" Y="-2.681116943359" />
                  <Point X="-2.319684570312" Y="-2.666187011719" />
                  <Point X="-2.313413574219" Y="-2.634660644531" />
                  <Point X="-2.311638916016" Y="-2.619235351562" />
                  <Point X="-2.310626220703" Y="-2.588301269531" />
                  <Point X="-2.314666259766" Y="-2.557615234375" />
                  <Point X="-2.323650878906" Y="-2.527997558594" />
                  <Point X="-2.329357421875" Y="-2.513557128906" />
                  <Point X="-2.343574462891" Y="-2.484728271484" />
                  <Point X="-2.351557128906" Y="-2.47141015625" />
                  <Point X="-2.369584716797" Y="-2.446253173828" />
                  <Point X="-2.379629638672" Y="-2.434414306641" />
                  <Point X="-2.396978271484" Y="-2.417065673828" />
                  <Point X="-2.408817138672" Y="-2.407020751953" />
                  <Point X="-2.433974121094" Y="-2.388993164062" />
                  <Point X="-2.447292236328" Y="-2.381010498047" />
                  <Point X="-2.47612109375" Y="-2.366793457031" />
                  <Point X="-2.490561523438" Y="-2.361086914062" />
                  <Point X="-2.520179199219" Y="-2.352102294922" />
                  <Point X="-2.550865234375" Y="-2.348062255859" />
                  <Point X="-2.581799316406" Y="-2.349074951172" />
                  <Point X="-2.597224609375" Y="-2.350849609375" />
                  <Point X="-2.628750976562" Y="-2.357120605469" />
                  <Point X="-2.643680908203" Y="-2.361384033203" />
                  <Point X="-2.672647216797" Y="-2.372286132812" />
                  <Point X="-2.68668359375" Y="-2.378924804688" />
                  <Point X="-3.493182617188" Y="-2.844557128906" />
                  <Point X="-3.793089111328" Y="-3.017708251953" />
                  <Point X="-3.795110351562" Y="-3.015052978516" />
                  <Point X="-4.004016601563" Y="-2.740592773438" />
                  <Point X="-4.107007324219" Y="-2.567892578125" />
                  <Point X="-4.181265136719" Y="-2.443373779297" />
                  <Point X="-3.535936523438" Y="-1.948194946289" />
                  <Point X="-3.048122314453" Y="-1.573882202148" />
                  <Point X="-3.036481689453" Y="-1.563309570312" />
                  <Point X="-3.015104003906" Y="-1.540388916016" />
                  <Point X="-3.005366943359" Y="-1.528040893555" />
                  <Point X="-2.987402099609" Y="-1.500909545898" />
                  <Point X="-2.979835449219" Y="-1.487124267578" />
                  <Point X="-2.967080078125" Y="-1.458495483398" />
                  <Point X="-2.961891357422" Y="-1.443651977539" />
                  <Point X="-2.954186767578" Y="-1.413904541016" />
                  <Point X="-2.951552978516" Y="-1.398803955078" />
                  <Point X="-2.948748535156" Y="-1.368374511719" />
                  <Point X="-2.948577880859" Y="-1.353046020508" />
                  <Point X="-2.950786865234" Y="-1.321375488281" />
                  <Point X="-2.953083740234" Y="-1.306219726562" />
                  <Point X="-2.960084960938" Y="-1.276474975586" />
                  <Point X="-2.971779052734" Y="-1.248243164062" />
                  <Point X="-2.987861328125" Y="-1.222259643555" />
                  <Point X="-2.996954101562" Y="-1.209918823242" />
                  <Point X="-3.017786621094" Y="-1.185962402344" />
                  <Point X="-3.028745361328" Y="-1.175244873047" />
                  <Point X="-3.052245117188" Y="-1.1557109375" />
                  <Point X="-3.064786132812" Y="-1.14689465332" />
                  <Point X="-3.091268798828" Y="-1.131308227539" />
                  <Point X="-3.10543359375" Y="-1.124481567383" />
                  <Point X="-3.134696777344" Y="-1.113257446289" />
                  <Point X="-3.149795410156" Y="-1.108860107422" />
                  <Point X="-3.181683105469" Y="-1.102378540039" />
                  <Point X="-3.197298828125" Y="-1.100532470703" />
                  <Point X="-3.228622070312" Y="-1.09944140625" />
                  <Point X="-3.244329101562" Y="-1.100196533203" />
                  <Point X="-4.262457519531" Y="-1.234235717773" />
                  <Point X="-4.660920898438" Y="-1.286694458008" />
                  <Point X="-4.74076171875" Y="-0.974121337891" />
                  <Point X="-4.768010742188" Y="-0.783598571777" />
                  <Point X="-4.786452636719" Y="-0.654654296875" />
                  <Point X="-4.063981933594" Y="-0.461069000244" />
                  <Point X="-3.508288085938" Y="-0.312171295166" />
                  <Point X="-3.497231933594" Y="-0.308468048096" />
                  <Point X="-3.475656494141" Y="-0.299734832764" />
                  <Point X="-3.465136962891" Y="-0.294704986572" />
                  <Point X="-3.439175292969" Y="-0.280270568848" />
                  <Point X="-3.423168701172" Y="-0.270300506592" />
                  <Point X="-3.405809570312" Y="-0.258252380371" />
                  <Point X="-3.396478759766" Y="-0.250869247437" />
                  <Point X="-3.3725234375" Y="-0.229342712402" />
                  <Point X="-3.357515136719" Y="-0.212177215576" />
                  <Point X="-3.336244628906" Y="-0.180962539673" />
                  <Point X="-3.327176513672" Y="-0.164287643433" />
                  <Point X="-3.316286376953" Y="-0.138387130737" />
                  <Point X="-3.313587890625" Y="-0.131161224365" />
                  <Point X="-3.306683349609" Y="-0.10909563446" />
                  <Point X="-3.298649902344" Y="-0.075630805969" />
                  <Point X="-3.296738525391" Y="-0.065074272156" />
                  <Point X="-3.294118408203" Y="-0.04381212616" />
                  <Point X="-3.293912353516" Y="-0.022389919281" />
                  <Point X="-3.296123046875" Y="-0.001081258297" />
                  <Point X="-3.297831054688" Y="0.009510799408" />
                  <Point X="-3.303542724609" Y="0.035494838715" />
                  <Point X="-3.305476318359" Y="0.042866981506" />
                  <Point X="-3.312446044922" Y="0.06462537384" />
                  <Point X="-3.325657714844" Y="0.098006523132" />
                  <Point X="-3.330629882812" Y="0.108608093262" />
                  <Point X="-3.341840332031" Y="0.129118804932" />
                  <Point X="-3.355489013672" Y="0.148094680786" />
                  <Point X="-3.371368408203" Y="0.165247253418" />
                  <Point X="-3.379837158203" Y="0.173332931519" />
                  <Point X="-3.402049560547" Y="0.192064620972" />
                  <Point X="-3.416202148438" Y="0.202905471802" />
                  <Point X="-3.433561035156" Y="0.214953430176" />
                  <Point X="-3.440483642578" Y="0.219328445435" />
                  <Point X="-3.465615966797" Y="0.23283454895" />
                  <Point X="-3.496567138672" Y="0.245635894775" />
                  <Point X="-3.508288085938" Y="0.249611251831" />
                  <Point X="-4.436360351563" Y="0.498287414551" />
                  <Point X="-4.785446289062" Y="0.591824584961" />
                  <Point X="-4.783987792969" Y="0.601679626465" />
                  <Point X="-4.73133203125" Y="0.957523010254" />
                  <Point X="-4.676478515625" Y="1.159950317383" />
                  <Point X="-4.6335859375" Y="1.318236938477" />
                  <Point X="-4.176401367188" Y="1.258047363281" />
                  <Point X="-3.778066162109" Y="1.20560546875" />
                  <Point X="-3.76773828125" Y="1.204815429688" />
                  <Point X="-3.747057861328" Y="1.204364257812" />
                  <Point X="-3.736705322266" Y="1.20470324707" />
                  <Point X="-3.715143554688" Y="1.20658984375" />
                  <Point X="-3.704889892578" Y="1.208053833008" />
                  <Point X="-3.684603271484" Y="1.212089233398" />
                  <Point X="-3.674570800781" Y="1.214660400391" />
                  <Point X="-3.651565917969" Y="1.221913818359" />
                  <Point X="-3.613144775391" Y="1.234028076172" />
                  <Point X="-3.603450439453" Y="1.237676757812" />
                  <Point X="-3.584517333984" Y="1.246007568359" />
                  <Point X="-3.575278076172" Y="1.250689819336" />
                  <Point X="-3.556533935547" Y="1.26151184082" />
                  <Point X="-3.547859375" Y="1.267172119141" />
                  <Point X="-3.531178466797" Y="1.279403198242" />
                  <Point X="-3.515928955078" Y="1.293376708984" />
                  <Point X="-3.502290527344" Y="1.308928222656" />
                  <Point X="-3.495895263672" Y="1.317077148438" />
                  <Point X="-3.483480712891" Y="1.334807128906" />
                  <Point X="-3.478010986328" Y="1.343603027344" />
                  <Point X="-3.468062255859" Y="1.361737426758" />
                  <Point X="-3.463583251953" Y="1.371075317383" />
                  <Point X="-3.454352294922" Y="1.393360351562" />
                  <Point X="-3.438935791016" Y="1.430579345703" />
                  <Point X="-3.435499511719" Y="1.440350708008" />
                  <Point X="-3.4297109375" Y="1.460209838867" />
                  <Point X="-3.427358642578" Y="1.470297973633" />
                  <Point X="-3.423600341797" Y="1.491612915039" />
                  <Point X="-3.422360595703" Y="1.501897216797" />
                  <Point X="-3.421008056641" Y="1.522537475586" />
                  <Point X="-3.421910400391" Y="1.543200805664" />
                  <Point X="-3.425056884766" Y="1.56364440918" />
                  <Point X="-3.427188232422" Y="1.573780395508" />
                  <Point X="-3.432790039062" Y="1.594686889648" />
                  <Point X="-3.436012207031" Y="1.604530761719" />
                  <Point X="-3.443509033203" Y="1.623808959961" />
                  <Point X="-3.447783691406" Y="1.633243286133" />
                  <Point X="-3.458921630859" Y="1.654639282227" />
                  <Point X="-3.4775234375" Y="1.690373168945" />
                  <Point X="-3.482800048828" Y="1.699286743164" />
                  <Point X="-3.494292236328" Y="1.716485961914" />
                  <Point X="-3.5005078125" Y="1.724771850586" />
                  <Point X="-3.514420410156" Y="1.741352050781" />
                  <Point X="-3.521500976563" Y="1.748911865234" />
                  <Point X="-3.536442626953" Y="1.763215209961" />
                  <Point X="-3.544303710938" Y="1.769958862305" />
                  <Point X="-4.076648193359" Y="2.178441162109" />
                  <Point X="-4.227614257812" Y="2.294282226563" />
                  <Point X="-4.206895996094" Y="2.329778076172" />
                  <Point X="-4.002293212891" Y="2.680312011719" />
                  <Point X="-3.856996826172" Y="2.867069824219" />
                  <Point X="-3.726338378906" Y="3.035012695312" />
                  <Point X="-3.498803222656" Y="2.903645019531" />
                  <Point X="-3.254156982422" Y="2.7623984375" />
                  <Point X="-3.244917480469" Y="2.757716064453" />
                  <Point X="-3.225984619141" Y="2.749385498047" />
                  <Point X="-3.216291259766" Y="2.745737304688" />
                  <Point X="-3.195648925781" Y="2.739228759766" />
                  <Point X="-3.185615722656" Y="2.736657226562" />
                  <Point X="-3.165328857422" Y="2.732621826172" />
                  <Point X="-3.155075195312" Y="2.731157958984" />
                  <Point X="-3.123035644531" Y="2.728354736328" />
                  <Point X="-3.069525878906" Y="2.723673339844" />
                  <Point X="-3.059173583984" Y="2.723334472656" />
                  <Point X="-3.038492919922" Y="2.723785644531" />
                  <Point X="-3.028164550781" Y="2.724575683594" />
                  <Point X="-3.006705810547" Y="2.727400878906" />
                  <Point X="-2.996525390625" Y="2.729310791016" />
                  <Point X="-2.976433837891" Y="2.734227294922" />
                  <Point X="-2.956997802734" Y="2.741301513672" />
                  <Point X="-2.938446289062" Y="2.750449951172" />
                  <Point X="-2.929419677734" Y="2.755530761719" />
                  <Point X="-2.911165527344" Y="2.767159912109" />
                  <Point X="-2.902745361328" Y="2.773193847656" />
                  <Point X="-2.886613769531" Y="2.786141113281" />
                  <Point X="-2.87890234375" Y="2.793054443359" />
                  <Point X="-2.856160400391" Y="2.815796386719" />
                  <Point X="-2.818178710938" Y="2.853777832031" />
                  <Point X="-2.811265380859" Y="2.861489501953" />
                  <Point X="-2.798318359375" Y="2.877620849609" />
                  <Point X="-2.792284667969" Y="2.886040527344" />
                  <Point X="-2.780655273438" Y="2.904294921875" />
                  <Point X="-2.775574707031" Y="2.913320800781" />
                  <Point X="-2.766426025391" Y="2.931872314453" />
                  <Point X="-2.759351318359" Y="2.951309570312" />
                  <Point X="-2.754434814453" Y="2.971401367188" />
                  <Point X="-2.752524902344" Y="2.981581542969" />
                  <Point X="-2.749699707031" Y="3.003040283203" />
                  <Point X="-2.748909667969" Y="3.013368652344" />
                  <Point X="-2.748458496094" Y="3.034049072266" />
                  <Point X="-2.748797363281" Y="3.044401123047" />
                  <Point X="-2.751600585938" Y="3.076440917969" />
                  <Point X="-2.756281982422" Y="3.129950683594" />
                  <Point X="-2.757745849609" Y="3.140204345703" />
                  <Point X="-2.76178125" Y="3.160491455078" />
                  <Point X="-2.764352783203" Y="3.170524902344" />
                  <Point X="-2.770861328125" Y="3.191167236328" />
                  <Point X="-2.774509521484" Y="3.200860595703" />
                  <Point X="-2.782840087891" Y="3.219793457031" />
                  <Point X="-2.787522460938" Y="3.229032958984" />
                  <Point X="-3.023420410156" Y="3.637620117188" />
                  <Point X="-3.059387207031" Y="3.699916503906" />
                  <Point X="-3.004721679688" Y="3.741828125" />
                  <Point X="-2.648374023438" Y="4.015036376953" />
                  <Point X="-2.419542480469" Y="4.142170410156" />
                  <Point X="-2.192524658203" Y="4.268296875" />
                  <Point X="-2.118563964844" Y="4.171908691406" />
                  <Point X="-2.111820800781" Y="4.164048339844" />
                  <Point X="-2.097517822266" Y="4.149106933594" />
                  <Point X="-2.089958007813" Y="4.142026367188" />
                  <Point X="-2.073377929688" Y="4.128113769531" />
                  <Point X="-2.065093261719" Y="4.121898925781" />
                  <Point X="-2.047893920898" Y="4.11040625" />
                  <Point X="-2.038979614258" Y="4.105128417969" />
                  <Point X="-2.003319946289" Y="4.086565185547" />
                  <Point X="-1.943763549805" Y="4.055561767578" />
                  <Point X="-1.934327026367" Y="4.051286132812" />
                  <Point X="-1.915047241211" Y="4.0437890625" />
                  <Point X="-1.905203857422" Y="4.040567382812" />
                  <Point X="-1.884297363281" Y="4.034965820313" />
                  <Point X="-1.874162597656" Y="4.032834716797" />
                  <Point X="-1.853719604492" Y="4.029688232422" />
                  <Point X="-1.833053344727" Y="4.028785888672" />
                  <Point X="-1.81241394043" Y="4.030138916016" />
                  <Point X="-1.802132568359" Y="4.031378662109" />
                  <Point X="-1.780817260742" Y="4.035136962891" />
                  <Point X="-1.770729736328" Y="4.037489013672" />
                  <Point X="-1.750869750977" Y="4.043277587891" />
                  <Point X="-1.741097412109" Y="4.046714355469" />
                  <Point X="-1.703955200195" Y="4.062099609375" />
                  <Point X="-1.641923339844" Y="4.087793945312" />
                  <Point X="-1.632585693359" Y="4.092272705078" />
                  <Point X="-1.614451293945" Y="4.102221191406" />
                  <Point X="-1.605654418945" Y="4.10769140625" />
                  <Point X="-1.587924926758" Y="4.120105957031" />
                  <Point X="-1.57977734375" Y="4.126500488281" />
                  <Point X="-1.564225952148" Y="4.140138671875" />
                  <Point X="-1.550251220703" Y="4.155389648437" />
                  <Point X="-1.538020141602" Y="4.172070800781" />
                  <Point X="-1.532360351562" Y="4.180744628906" />
                  <Point X="-1.521538574219" Y="4.199488769531" />
                  <Point X="-1.516856201172" Y="4.208728027344" />
                  <Point X="-1.508525634766" Y="4.227660644531" />
                  <Point X="-1.504877441406" Y="4.237354003906" />
                  <Point X="-1.492788208008" Y="4.275695800781" />
                  <Point X="-1.472598144531" Y="4.33973046875" />
                  <Point X="-1.470026733398" Y="4.349763671875" />
                  <Point X="-1.465991088867" Y="4.37005078125" />
                  <Point X="-1.46452722168" Y="4.3803046875" />
                  <Point X="-1.46264074707" Y="4.401866210938" />
                  <Point X="-1.462301757812" Y="4.41221875" />
                  <Point X="-1.462752929688" Y="4.432898925781" />
                  <Point X="-1.46354309082" Y="4.4432265625" />
                  <Point X="-1.479266235352" Y="4.562655761719" />
                  <Point X="-1.392488891602" Y="4.586985351562" />
                  <Point X="-0.931181884766" Y="4.716319824219" />
                  <Point X="-0.653773193359" Y="4.748786132813" />
                  <Point X="-0.365221954346" Y="4.782556640625" />
                  <Point X="-0.298039520264" Y="4.531828125" />
                  <Point X="-0.225666244507" Y="4.261727539062" />
                  <Point X="-0.220435317993" Y="4.247107910156" />
                  <Point X="-0.207661773682" Y="4.218916503906" />
                  <Point X="-0.200119155884" Y="4.205344726562" />
                  <Point X="-0.182260925293" Y="4.178618164062" />
                  <Point X="-0.172608306885" Y="4.166456054688" />
                  <Point X="-0.151451248169" Y="4.1438671875" />
                  <Point X="-0.126898002625" Y="4.125026367188" />
                  <Point X="-0.099602806091" Y="4.110436523438" />
                  <Point X="-0.085356712341" Y="4.104260742188" />
                  <Point X="-0.054918586731" Y="4.093927978516" />
                  <Point X="-0.039857292175" Y="4.090155273438" />
                  <Point X="-0.009319890976" Y="4.08511328125" />
                  <Point X="0.021631721497" Y="4.08511328125" />
                  <Point X="0.052169120789" Y="4.090155273438" />
                  <Point X="0.067230415344" Y="4.093927978516" />
                  <Point X="0.097668540955" Y="4.104260742188" />
                  <Point X="0.111914489746" Y="4.110436523438" />
                  <Point X="0.139209686279" Y="4.125026367188" />
                  <Point X="0.16376322937" Y="4.143866699219" />
                  <Point X="0.184920150757" Y="4.166455566406" />
                  <Point X="0.194572906494" Y="4.178617675781" />
                  <Point X="0.212431137085" Y="4.205344238281" />
                  <Point X="0.219973754883" Y="4.218916503906" />
                  <Point X="0.232747146606" Y="4.247107910156" />
                  <Point X="0.237978225708" Y="4.261727539062" />
                  <Point X="0.35884967041" Y="4.712826171875" />
                  <Point X="0.378190338135" Y="4.785006347656" />
                  <Point X="0.425071716309" Y="4.780096679688" />
                  <Point X="0.827876647949" Y="4.737912109375" />
                  <Point X="1.057390014648" Y="4.682500488281" />
                  <Point X="1.453594604492" Y="4.586844726562" />
                  <Point X="1.601088134766" Y="4.533347167969" />
                  <Point X="1.858258422852" Y="4.440069824219" />
                  <Point X="2.002716064453" Y="4.37251171875" />
                  <Point X="2.250453125" Y="4.256653320312" />
                  <Point X="2.390060791016" Y="4.175317382812" />
                  <Point X="2.629435302734" Y="4.035857177734" />
                  <Point X="2.761055664062" Y="3.942256103516" />
                  <Point X="2.817779785156" Y="3.901916748047" />
                  <Point X="2.388001708984" Y="3.157518798828" />
                  <Point X="2.06530859375" Y="2.598597900391" />
                  <Point X="2.062373046875" Y="2.593104980469" />
                  <Point X="2.053180908203" Y="2.5734375" />
                  <Point X="2.044182128906" Y="2.549563476562" />
                  <Point X="2.041301513672" Y="2.540598632812" />
                  <Point X="2.033260864258" Y="2.510530029297" />
                  <Point X="2.01983190918" Y="2.460312255859" />
                  <Point X="2.017912719727" Y="2.451465576172" />
                  <Point X="2.013646972656" Y="2.420224121094" />
                  <Point X="2.012755371094" Y="2.383242675781" />
                  <Point X="2.013411010742" Y="2.369579833984" />
                  <Point X="2.016546142578" Y="2.343579101563" />
                  <Point X="2.021782348633" Y="2.300155029297" />
                  <Point X="2.02380078125" Y="2.289033447266" />
                  <Point X="2.029143798828" Y="2.267110351562" />
                  <Point X="2.032468261719" Y="2.256308837891" />
                  <Point X="2.040734375" Y="2.234220214844" />
                  <Point X="2.045318115234" Y="2.223889404297" />
                  <Point X="2.055680908203" Y="2.203843994141" />
                  <Point X="2.061459960938" Y="2.194129394531" />
                  <Point X="2.077548339844" Y="2.170419433594" />
                  <Point X="2.104417724609" Y="2.130820800781" />
                  <Point X="2.109808349609" Y="2.123633300781" />
                  <Point X="2.130462158203" Y="2.100124267578" />
                  <Point X="2.157596435547" Y="2.075387451172" />
                  <Point X="2.168256835938" Y="2.066981445312" />
                  <Point X="2.191966796875" Y="2.050893066406" />
                  <Point X="2.231565429688" Y="2.024024047852" />
                  <Point X="2.241280273438" Y="2.018244873047" />
                  <Point X="2.261326171875" Y="2.007881835938" />
                  <Point X="2.271657226562" Y="2.003297851562" />
                  <Point X="2.293745605469" Y="1.995031982422" />
                  <Point X="2.304547119141" Y="1.991707641602" />
                  <Point X="2.326469726562" Y="1.986364868164" />
                  <Point X="2.337590820312" Y="1.984346557617" />
                  <Point X="2.363591552734" Y="1.981211303711" />
                  <Point X="2.407015625" Y="1.975974975586" />
                  <Point X="2.416045410156" Y="1.975320800781" />
                  <Point X="2.447575683594" Y="1.975497314453" />
                  <Point X="2.484314453125" Y="1.979822631836" />
                  <Point X="2.497748291016" Y="1.982395996094" />
                  <Point X="2.527816894531" Y="1.990436645508" />
                  <Point X="2.578034667969" Y="2.003865600586" />
                  <Point X="2.583994384766" Y="2.005670532227" />
                  <Point X="2.604404541016" Y="2.013067260742" />
                  <Point X="2.627655029297" Y="2.02357409668" />
                  <Point X="2.636033935547" Y="2.027872558594" />
                  <Point X="3.56949609375" Y="2.566807128906" />
                  <Point X="3.940404785156" Y="2.780951416016" />
                  <Point X="4.043949951172" Y="2.637047363281" />
                  <Point X="4.117330566406" Y="2.515785644531" />
                  <Point X="4.136884765625" Y="2.483472412109" />
                  <Point X="3.597807373047" Y="2.069823242188" />
                  <Point X="3.172951660156" Y="1.743819702148" />
                  <Point X="3.168137939453" Y="1.739868896484" />
                  <Point X="3.152119384766" Y="1.725216918945" />
                  <Point X="3.134668701172" Y="1.706603393555" />
                  <Point X="3.128576416016" Y="1.699422485352" />
                  <Point X="3.106936035156" Y="1.671190917969" />
                  <Point X="3.070794189453" Y="1.624041015625" />
                  <Point X="3.065635742188" Y="1.616602661133" />
                  <Point X="3.04973828125" Y="1.589370239258" />
                  <Point X="3.034762695312" Y="1.555544799805" />
                  <Point X="3.030140136719" Y="1.542671875" />
                  <Point X="3.022079101562" Y="1.513847412109" />
                  <Point X="3.008616210938" Y="1.465707397461" />
                  <Point X="3.006225097656" Y="1.454661010742" />
                  <Point X="3.002771728516" Y="1.432362792969" />
                  <Point X="3.001709472656" Y="1.421110839844" />
                  <Point X="3.000893310547" Y="1.397540527344" />
                  <Point X="3.001174804688" Y="1.386241943359" />
                  <Point X="3.003077636719" Y="1.363757202148" />
                  <Point X="3.004698974609" Y="1.352571166992" />
                  <Point X="3.011316162109" Y="1.320500244141" />
                  <Point X="3.022367919922" Y="1.266938476562" />
                  <Point X="3.02459765625" Y="1.25823449707" />
                  <Point X="3.034684082031" Y="1.228607910156" />
                  <Point X="3.050286621094" Y="1.19537109375" />
                  <Point X="3.056918457031" Y="1.183526245117" />
                  <Point X="3.074916748047" Y="1.156169433594" />
                  <Point X="3.104976074219" Y="1.110481079102" />
                  <Point X="3.111738769531" Y="1.101425170898" />
                  <Point X="3.126292480469" Y="1.08417980957" />
                  <Point X="3.134083740234" Y="1.075990478516" />
                  <Point X="3.151327636719" Y="1.059900878906" />
                  <Point X="3.160034667969" Y="1.052696044922" />
                  <Point X="3.178244140625" Y="1.039370361328" />
                  <Point X="3.18774609375" Y="1.03325012207" />
                  <Point X="3.213828125" Y="1.018568054199" />
                  <Point X="3.257388183594" Y="0.994047668457" />
                  <Point X="3.265480224609" Y="0.989987426758" />
                  <Point X="3.294676513672" Y="0.978085083008" />
                  <Point X="3.330274169922" Y="0.96802154541" />
                  <Point X="3.343670654297" Y="0.965257629395" />
                  <Point X="3.378935302734" Y="0.960596801758" />
                  <Point X="3.437831298828" Y="0.952813110352" />
                  <Point X="3.444030029297" Y="0.952199768066" />
                  <Point X="3.465716308594" Y="0.951222839355" />
                  <Point X="3.491217529297" Y="0.952032409668" />
                  <Point X="3.500603515625" Y="0.952797241211" />
                  <Point X="4.387330078125" Y="1.069537109375" />
                  <Point X="4.704703613281" Y="1.111320068359" />
                  <Point X="4.75268359375" Y="0.914232421875" />
                  <Point X="4.775807128906" Y="0.765714111328" />
                  <Point X="4.78387109375" Y="0.713920837402" />
                  <Point X="4.178885742188" Y="0.551815734863" />
                  <Point X="3.691991943359" Y="0.421352905273" />
                  <Point X="3.686031738281" Y="0.419544464111" />
                  <Point X="3.665626708984" Y="0.412138000488" />
                  <Point X="3.642381103516" Y="0.401619354248" />
                  <Point X="3.634004638672" Y="0.397316711426" />
                  <Point X="3.599358154297" Y="0.377290527344" />
                  <Point X="3.541494628906" Y="0.343844268799" />
                  <Point X="3.533881347656" Y="0.338945526123" />
                  <Point X="3.508773925781" Y="0.319870361328" />
                  <Point X="3.481993896484" Y="0.294350769043" />
                  <Point X="3.472796875" Y="0.284226715088" />
                  <Point X="3.452009033203" Y="0.257737945557" />
                  <Point X="3.417291015625" Y="0.213499008179" />
                  <Point X="3.410854980469" Y="0.204208892822" />
                  <Point X="3.399130859375" Y="0.184929077148" />
                  <Point X="3.393843017578" Y="0.17493939209" />
                  <Point X="3.384069091797" Y="0.153475570679" />
                  <Point X="3.380005126953" Y="0.142928695679" />
                  <Point X="3.373158935547" Y="0.121427864075" />
                  <Point X="3.370376708984" Y="0.110473609924" />
                  <Point X="3.363447265625" Y="0.074291511536" />
                  <Point X="3.351874511719" Y="0.013863221169" />
                  <Point X="3.350603515625" Y="0.004969180107" />
                  <Point X="3.348584228516" Y="-0.02626288414" />
                  <Point X="3.350280029297" Y="-0.062941680908" />
                  <Point X="3.351874511719" Y="-0.076423416138" />
                  <Point X="3.358803955078" Y="-0.112605361938" />
                  <Point X="3.370376708984" Y="-0.173033798218" />
                  <Point X="3.373158935547" Y="-0.183987609863" />
                  <Point X="3.380005126953" Y="-0.205488739014" />
                  <Point X="3.384069091797" Y="-0.216035766602" />
                  <Point X="3.393843017578" Y="-0.237499588013" />
                  <Point X="3.399131103516" Y="-0.247489425659" />
                  <Point X="3.410855224609" Y="-0.266769073486" />
                  <Point X="3.417291015625" Y="-0.276058898926" />
                  <Point X="3.438078857422" Y="-0.302547668457" />
                  <Point X="3.472796875" Y="-0.346786773682" />
                  <Point X="3.478718017578" Y="-0.353633239746" />
                  <Point X="3.501139892578" Y="-0.375805541992" />
                  <Point X="3.530176757813" Y="-0.398724975586" />
                  <Point X="3.541494628906" Y="-0.406404174805" />
                  <Point X="3.576141113281" Y="-0.426430511475" />
                  <Point X="3.634004638672" Y="-0.45987677002" />
                  <Point X="3.63949609375" Y="-0.462815032959" />
                  <Point X="3.659158447266" Y="-0.472016876221" />
                  <Point X="3.683028076172" Y="-0.481027740479" />
                  <Point X="3.691991943359" Y="-0.48391293335" />
                  <Point X="4.505160644531" Y="-0.70180065918" />
                  <Point X="4.784876953125" Y="-0.776750549316" />
                  <Point X="4.761613769531" Y="-0.931050598145" />
                  <Point X="4.731987792969" Y="-1.06087512207" />
                  <Point X="4.727801757812" Y="-1.079219604492" />
                  <Point X="4.001441894531" Y="-0.983592529297" />
                  <Point X="3.436781982422" Y="-0.909253723145" />
                  <Point X="3.428624267578" Y="-0.908535888672" />
                  <Point X="3.40009765625" Y="-0.908042602539" />
                  <Point X="3.366720947266" Y="-0.910841003418" />
                  <Point X="3.354480957031" Y="-0.912676391602" />
                  <Point X="3.286482177734" Y="-0.927456176758" />
                  <Point X="3.172916748047" Y="-0.952140075684" />
                  <Point X="3.157873535156" Y="-0.956742736816" />
                  <Point X="3.128753173828" Y="-0.96836730957" />
                  <Point X="3.114676025391" Y="-0.975389282227" />
                  <Point X="3.086849609375" Y="-0.992281494141" />
                  <Point X="3.074124267578" Y="-1.001530273438" />
                  <Point X="3.050374267578" Y="-1.022000976562" />
                  <Point X="3.039349609375" Y="-1.03322265625" />
                  <Point X="2.998248535156" Y="-1.082654174805" />
                  <Point X="2.92960546875" Y="-1.16521081543" />
                  <Point X="2.921325927734" Y="-1.17684765625" />
                  <Point X="2.906605224609" Y="-1.201230224609" />
                  <Point X="2.9001640625" Y="-1.213976074219" />
                  <Point X="2.888820800781" Y="-1.241361328125" />
                  <Point X="2.884363037109" Y="-1.254928222656" />
                  <Point X="2.87753125" Y="-1.282577514648" />
                  <Point X="2.875157226562" Y="-1.29666003418" />
                  <Point X="2.869266357422" Y="-1.360676025391" />
                  <Point X="2.859428222656" Y="-1.467590209961" />
                  <Point X="2.859288574219" Y="-1.483320556641" />
                  <Point X="2.861607177734" Y="-1.514589233398" />
                  <Point X="2.864065429688" Y="-1.530127441406" />
                  <Point X="2.871796875" Y="-1.561748413086" />
                  <Point X="2.876785888672" Y="-1.576668212891" />
                  <Point X="2.889157470703" Y="-1.605479858398" />
                  <Point X="2.896540039062" Y="-1.619371704102" />
                  <Point X="2.934171630859" Y="-1.677904907227" />
                  <Point X="2.997020263672" Y="-1.775661987305" />
                  <Point X="3.001741943359" Y="-1.782353393555" />
                  <Point X="3.019793701172" Y="-1.804450195312" />
                  <Point X="3.043489257812" Y="-1.828120361328" />
                  <Point X="3.052796142578" Y="-1.836277709961" />
                  <Point X="3.807421630859" Y="-2.415322509766" />
                  <Point X="4.087170654297" Y="-2.629981933594" />
                  <Point X="4.045488037109" Y="-2.697430664062" />
                  <Point X="4.001274414063" Y="-2.760251953125" />
                  <Point X="3.351360351562" Y="-2.385023681641" />
                  <Point X="2.848454589844" Y="-2.094670654297" />
                  <Point X="2.841191894531" Y="-2.090885253906" />
                  <Point X="2.815026123047" Y="-2.079513183594" />
                  <Point X="2.783119140625" Y="-2.069325927734" />
                  <Point X="2.771108398438" Y="-2.066337646484" />
                  <Point X="2.690179199219" Y="-2.051721923828" />
                  <Point X="2.555018066406" Y="-2.027311889648" />
                  <Point X="2.539359130859" Y="-2.025807495117" />
                  <Point X="2.508006835938" Y="-2.025403198242" />
                  <Point X="2.492313476562" Y="-2.026503540039" />
                  <Point X="2.460140625" Y="-2.031461303711" />
                  <Point X="2.444844482422" Y="-2.035136352539" />
                  <Point X="2.415068847656" Y="-2.044960083008" />
                  <Point X="2.400589355469" Y="-2.051108642578" />
                  <Point X="2.333356933594" Y="-2.086492431641" />
                  <Point X="2.221071289062" Y="-2.145587646484" />
                  <Point X="2.208968994141" Y="-2.153169921875" />
                  <Point X="2.186037597656" Y="-2.170063232422" />
                  <Point X="2.175208496094" Y="-2.179374267578" />
                  <Point X="2.154248779297" Y="-2.200334228516" />
                  <Point X="2.144937988281" Y="-2.211163330078" />
                  <Point X="2.128045654297" Y="-2.23409375" />
                  <Point X="2.120464111328" Y="-2.246195068359" />
                  <Point X="2.085080078125" Y="-2.313427490234" />
                  <Point X="2.025984985352" Y="-2.425713134766" />
                  <Point X="2.019836425781" Y="-2.440192382812" />
                  <Point X="2.010012451172" Y="-2.469968261719" />
                  <Point X="2.006337280273" Y="-2.485264892578" />
                  <Point X="2.001379516602" Y="-2.5174375" />
                  <Point X="2.000279174805" Y="-2.533130859375" />
                  <Point X="2.00068347168" Y="-2.564483154297" />
                  <Point X="2.002187866211" Y="-2.580142089844" />
                  <Point X="2.016803588867" Y="-2.661071289062" />
                  <Point X="2.041213378906" Y="-2.796232421875" />
                  <Point X="2.043015014648" Y="-2.804221435547" />
                  <Point X="2.051236083984" Y="-2.83154296875" />
                  <Point X="2.064069580078" Y="-2.862479736328" />
                  <Point X="2.069546630859" Y="-2.873578613281" />
                  <Point X="2.554469482422" Y="-3.713489257813" />
                  <Point X="2.735893310547" Y="-4.027724853516" />
                  <Point X="2.723754394531" Y="-4.036083740234" />
                  <Point X="2.217678466797" Y="-3.376553222656" />
                  <Point X="1.833914672852" Y="-2.876422851562" />
                  <Point X="1.828654418945" Y="-2.870147216797" />
                  <Point X="1.808830932617" Y="-2.849625732422" />
                  <Point X="1.783251586914" Y="-2.828004150391" />
                  <Point X="1.773299072266" Y="-2.820647216797" />
                  <Point X="1.693480957031" Y="-2.769331542969" />
                  <Point X="1.56017565918" Y="-2.683628417969" />
                  <Point X="1.546283935547" Y="-2.676245849609" />
                  <Point X="1.517472900391" Y="-2.663874511719" />
                  <Point X="1.502553588867" Y="-2.658885742188" />
                  <Point X="1.470932617188" Y="-2.651154052734" />
                  <Point X="1.45539453125" Y="-2.648695800781" />
                  <Point X="1.424125488281" Y="-2.646376953125" />
                  <Point X="1.40839453125" Y="-2.646516357422" />
                  <Point X="1.321099975586" Y="-2.654549316406" />
                  <Point X="1.175307739258" Y="-2.667965332031" />
                  <Point X="1.161225097656" Y="-2.670339111328" />
                  <Point X="1.133575439453" Y="-2.677170898438" />
                  <Point X="1.120008422852" Y="-2.68162890625" />
                  <Point X="1.092623168945" Y="-2.692972167969" />
                  <Point X="1.079877563477" Y="-2.699413330078" />
                  <Point X="1.055495361328" Y="-2.714133789062" />
                  <Point X="1.043859008789" Y="-2.722413085938" />
                  <Point X="0.976452148438" Y="-2.778459472656" />
                  <Point X="0.863875244141" Y="-2.872063476562" />
                  <Point X="0.852653320312" Y="-2.883087890625" />
                  <Point X="0.832182128906" Y="-2.906838134766" />
                  <Point X="0.822933044434" Y="-2.919564208984" />
                  <Point X="0.80604083252" Y="-2.947390869141" />
                  <Point X="0.799018859863" Y="-2.961468017578" />
                  <Point X="0.787394287109" Y="-2.990588378906" />
                  <Point X="0.782791748047" Y="-3.005631591797" />
                  <Point X="0.762637573242" Y="-3.098357177734" />
                  <Point X="0.728977478027" Y="-3.253219482422" />
                  <Point X="0.727584594727" Y="-3.261289794922" />
                  <Point X="0.724724487305" Y="-3.289677734375" />
                  <Point X="0.7247421875" Y="-3.323170410156" />
                  <Point X="0.725554992676" Y="-3.335520019531" />
                  <Point X="0.833091430664" Y="-4.15233984375" />
                  <Point X="0.805089050293" Y="-4.047833496094" />
                  <Point X="0.655064941406" Y="-3.487936523438" />
                  <Point X="0.652606262207" Y="-3.480124511719" />
                  <Point X="0.642145141602" Y="-3.453580566406" />
                  <Point X="0.62678717041" Y="-3.423815917969" />
                  <Point X="0.620407714844" Y="-3.413210205078" />
                  <Point X="0.559088989258" Y="-3.324861572266" />
                  <Point X="0.456679962158" Y="-3.177309814453" />
                  <Point X="0.446670654297" Y="-3.165172851562" />
                  <Point X="0.424786804199" Y="-3.142717529297" />
                  <Point X="0.41291229248" Y="-3.132399169922" />
                  <Point X="0.386657165527" Y="-3.113155273438" />
                  <Point X="0.373242767334" Y="-3.104937988281" />
                  <Point X="0.345241607666" Y="-3.090829589844" />
                  <Point X="0.330654724121" Y="-3.084938476562" />
                  <Point X="0.23576776123" Y="-3.055489013672" />
                  <Point X="0.077295906067" Y="-3.006305175781" />
                  <Point X="0.063376625061" Y="-3.003109130859" />
                  <Point X="0.03521704483" Y="-2.99883984375" />
                  <Point X="0.020976739883" Y="-2.997766601562" />
                  <Point X="-0.008664910316" Y="-2.997766601562" />
                  <Point X="-0.02290521431" Y="-2.998840087891" />
                  <Point X="-0.051064647675" Y="-3.003109375" />
                  <Point X="-0.064983482361" Y="-3.006305175781" />
                  <Point X="-0.159870437622" Y="-3.035754394531" />
                  <Point X="-0.318342437744" Y="-3.084938476562" />
                  <Point X="-0.332929473877" Y="-3.090829589844" />
                  <Point X="-0.360930786133" Y="-3.104937988281" />
                  <Point X="-0.374345336914" Y="-3.113155273438" />
                  <Point X="-0.400600463867" Y="-3.132399169922" />
                  <Point X="-0.412475128174" Y="-3.142717773438" />
                  <Point X="-0.434358978271" Y="-3.165173095703" />
                  <Point X="-0.444367980957" Y="-3.177309814453" />
                  <Point X="-0.505686523438" Y="-3.265658203125" />
                  <Point X="-0.608095581055" Y="-3.413210205078" />
                  <Point X="-0.612470458984" Y="-3.420132324219" />
                  <Point X="-0.62597668457" Y="-3.445265136719" />
                  <Point X="-0.638777893066" Y="-3.476215820312" />
                  <Point X="-0.642753112793" Y="-3.487936523438" />
                  <Point X="-0.893310058594" Y="-4.423028808594" />
                  <Point X="-0.985425170898" Y="-4.766807128906" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.706233033551" Y="0.693117811879" />
                  <Point X="4.612486318034" Y="1.099179448433" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.614414138752" Y="0.668515048659" />
                  <Point X="4.517863427222" Y="1.086722126294" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.522595243953" Y="0.643912285438" />
                  <Point X="4.42324053641" Y="1.074264804156" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.103766312756" Y="2.458059696371" />
                  <Point X="4.073851513057" Y="2.587634929548" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.772316328905" Y="-0.860062660176" />
                  <Point X="4.750985661568" Y="-0.767669389224" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.430776349154" Y="0.619309522218" />
                  <Point X="4.328617649575" Y="1.061807464794" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.020940202471" Y="2.394504903982" />
                  <Point X="3.932743219502" Y="2.776528007901" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.725338950496" Y="-1.078895369809" />
                  <Point X="4.647057676683" Y="-0.739821920879" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.338957454355" Y="0.594706758997" />
                  <Point X="4.233994765171" Y="1.049350114899" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.938114092187" Y="2.330950111593" />
                  <Point X="3.84671163763" Y="2.726857638358" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.624783748313" Y="-1.065657028366" />
                  <Point X="4.543129691798" Y="-0.711974452533" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.247138559556" Y="0.570103995777" />
                  <Point X="4.139371880767" Y="1.036892765003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.855287981903" Y="2.267395319204" />
                  <Point X="3.760680055759" Y="2.677187268816" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.524228546131" Y="-1.052418686923" />
                  <Point X="4.439201717936" Y="-0.684127031932" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.155319667047" Y="0.545501222638" />
                  <Point X="4.044748996363" Y="1.024435415108" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.772461871619" Y="2.203840526815" />
                  <Point X="3.674648473887" Y="2.627516899273" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.423673343948" Y="-1.03918034548" />
                  <Point X="4.335273750419" Y="-0.656279638814" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.063500781169" Y="0.520898420774" />
                  <Point X="3.950126111959" Y="1.011978065213" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.689635761335" Y="2.140285734426" />
                  <Point X="3.588616892016" Y="2.57784652973" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.323118141765" Y="-1.025942004038" />
                  <Point X="4.231345782902" Y="-0.628432245697" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.971681895292" Y="0.49629561891" />
                  <Point X="3.855503227555" Y="0.999520715317" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.606809651051" Y="2.076730942037" />
                  <Point X="3.502585307302" Y="2.528176172497" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.222562939582" Y="-1.012703662595" />
                  <Point X="4.127417815385" Y="-0.600584852579" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.879863009415" Y="0.471692817046" />
                  <Point X="3.760880343152" Y="0.987063365422" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.523983536048" Y="2.013176170086" />
                  <Point X="3.416553721777" Y="2.478505818781" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.1220077374" Y="-0.999465321152" />
                  <Point X="4.023489847868" Y="-0.572737459462" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.788044123537" Y="0.447090015182" />
                  <Point X="3.666257458748" Y="0.974606015526" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.44115742047" Y="1.949621400627" />
                  <Point X="3.330522136251" Y="2.428835465065" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.021452535217" Y="-0.98622697971" />
                  <Point X="3.919561880351" Y="-0.544890066344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.69622523766" Y="0.422487213318" />
                  <Point X="3.571634574344" Y="0.962148665631" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.358331304892" Y="1.886066631168" />
                  <Point X="3.244490550726" Y="2.379165111349" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.92089733541" Y="-0.972988648556" />
                  <Point X="3.815633912834" Y="-0.517042673227" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.608006715158" Y="0.382289524355" />
                  <Point X="3.476578498483" Y="0.951567674082" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.275505189314" Y="1.822511861709" />
                  <Point X="3.158458965201" Y="2.329494757633" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.801819053352" Y="3.874271931613" />
                  <Point X="2.79104784361" Y="3.920927166747" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.820342136192" Y="-0.959750319959" />
                  <Point X="3.711705945317" Y="-0.489195280109" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.522509245306" Y="0.330305661485" />
                  <Point X="3.37693400386" Y="0.960861307701" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.192679073736" Y="1.75895709225" />
                  <Point X="3.072427379675" Y="2.279824403917" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.732170805005" Y="3.753637548169" />
                  <Point X="2.674397129613" Y="4.003882829296" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.719786936975" Y="-0.946511991361" />
                  <Point X="3.603347222956" Y="-0.442156179277" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.444092748805" Y="0.247650733378" />
                  <Point X="3.273462045744" Y="0.986733507101" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.113473585447" Y="1.679719663038" />
                  <Point X="2.98639579415" Y="2.230154050201" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.662522556657" Y="3.633003164726" />
                  <Point X="2.560204536677" Y="4.076191199777" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.031204813208" Y="-2.717725099931" />
                  <Point X="3.994537597775" Y="-2.558901940906" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.619231737758" Y="-0.933273662764" />
                  <Point X="3.487391298351" Y="-0.362209980218" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.374652088" Y="0.126117189504" />
                  <Point X="3.16087677747" Y="1.052079789582" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.041256460981" Y="1.570212304527" />
                  <Point X="2.900364208624" Y="2.180483696485" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.59287430831" Y="3.512368781283" />
                  <Point X="2.447553599644" Y="4.141821924903" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.934642531182" Y="-2.721781995816" />
                  <Point X="3.876048080589" Y="-2.467981546703" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.518676538541" Y="-0.920035334167" />
                  <Point X="2.814332623099" Y="2.130813342769" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.523226059963" Y="3.39173439784" />
                  <Point X="2.334902646501" Y="4.20745271981" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.822149191305" Y="-2.656833898966" />
                  <Point X="3.757558580318" Y="-2.377061225769" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.418482303321" Y="-0.908360512415" />
                  <Point X="2.728301037574" Y="2.081142989053" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.453577811615" Y="3.271100014397" />
                  <Point X="2.223090586027" Y="4.269449871364" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.709655851428" Y="-2.591885802115" />
                  <Point X="3.639069103328" Y="-2.286141005675" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.323532792785" Y="-0.919403089099" />
                  <Point X="2.642269452048" Y="2.031472635337" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.383929562567" Y="3.150465633989" />
                  <Point X="2.113790648383" Y="4.320565823482" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.597162511551" Y="-2.526937705265" />
                  <Point X="3.520579626338" Y="-2.195220785582" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.230692630596" Y="-0.93958225726" />
                  <Point X="2.552707766921" Y="1.997092822879" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.314281302232" Y="3.029831302473" />
                  <Point X="2.00449071074" Y="4.371681775599" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.484669171673" Y="-2.461989608415" />
                  <Point X="3.402090149348" Y="-2.104300565488" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.13890380693" Y="-0.964315272864" />
                  <Point X="2.45986068027" Y="1.976943647859" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.244633041896" Y="2.909196970956" />
                  <Point X="1.895190761069" Y="4.422797779813" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.372175831796" Y="-2.397041511564" />
                  <Point X="3.283600672357" Y="-2.013380345395" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.054001019981" Y="-1.018874990383" />
                  <Point X="2.361313094391" Y="1.981486047718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.174984781561" Y="2.788562639439" />
                  <Point X="1.787804692921" Y="4.465623852385" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.259682487744" Y="-2.332093396633" />
                  <Point X="3.165111195367" Y="-1.922460125301" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.977099138791" Y="-1.108090438167" />
                  <Point X="2.257231560838" Y="2.009998608416" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.105336521225" Y="2.667928307923" />
                  <Point X="1.681395361816" Y="4.504219212018" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.147189142745" Y="-2.267145277595" />
                  <Point X="3.04642664174" Y="-1.83069493546" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.902828595732" Y="-1.208703463582" />
                  <Point X="2.141181651583" Y="2.090351899722" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.039118609877" Y="2.532435502529" />
                  <Point X="1.574985999877" Y="4.542814705209" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.034695797745" Y="-2.202197158558" />
                  <Point X="2.902156972787" Y="-1.628108435846" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.861044161208" Y="-1.450029284367" />
                  <Point X="1.468576543072" Y="4.581410609313" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.922202452746" Y="-2.137249039521" />
                  <Point X="1.364878161363" Y="4.608263557041" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.811083578813" Y="-2.078254408749" />
                  <Point X="1.261624034966" Y="4.633192223604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.708211009756" Y="-2.054978448597" />
                  <Point X="1.158369908569" Y="4.658120890167" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.606470065845" Y="-2.036604095468" />
                  <Point X="1.055115781789" Y="4.683049558391" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.506411075853" Y="-2.025515085154" />
                  <Point X="0.951861637987" Y="4.707978300342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.413550289929" Y="-2.045604922104" />
                  <Point X="0.848607494186" Y="4.732907042293" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.326343222193" Y="-2.090183702985" />
                  <Point X="0.748022364845" Y="4.746275012497" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.658439157983" Y="-3.950963327653" />
                  <Point X="2.636359979331" Y="-3.855327898" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.239407418828" Y="-2.135937458943" />
                  <Point X="0.648107724082" Y="4.756738777604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.518981091166" Y="-3.769218166605" />
                  <Point X="2.473895527748" Y="-3.573931136382" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.156301728178" Y="-2.198281255722" />
                  <Point X="0.548193083319" Y="4.76720254271" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.379523024348" Y="-3.587473005557" />
                  <Point X="2.311431153768" Y="-3.292534710903" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.085293027035" Y="-2.313022870703" />
                  <Point X="0.448278442556" Y="4.777666307817" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.240064957531" Y="-3.40572784451" />
                  <Point X="2.148966779788" Y="-3.011138285424" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.018258018696" Y="-2.444976440194" />
                  <Point X="0.362555821522" Y="4.72665768186" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.100606889143" Y="-3.223982676658" />
                  <Point X="0.310182457225" Y="4.531197554922" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.961148820455" Y="-3.042237507506" />
                  <Point X="0.257809098167" Y="4.335737405294" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.822432808544" Y="-2.863706539384" />
                  <Point X="0.196067685035" Y="4.180854755872" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.704830278375" Y="-2.776628108047" />
                  <Point X="0.114506287109" Y="4.111821892417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.590337563392" Y="-2.703019766165" />
                  <Point X="0.023116951098" Y="4.085358505671" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.481458727756" Y="-2.653727807232" />
                  <Point X="-0.07818369858" Y="4.101825734955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.382837888707" Y="-2.648868113036" />
                  <Point X="-0.199298165416" Y="4.204116035235" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.287367253554" Y="-2.657653451011" />
                  <Point X="-0.428627402704" Y="4.775136002972" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.191896620668" Y="-2.66643879881" />
                  <Point X="-0.523561220159" Y="4.764025452086" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.09983389384" Y="-2.689985409477" />
                  <Point X="-0.618495037615" Y="4.752914901201" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.015303079414" Y="-2.746156317005" />
                  <Point X="-0.713428861571" Y="4.741804378478" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.933505894413" Y="-2.814167874434" />
                  <Point X="-0.808362689373" Y="4.73069387241" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.852073695281" Y="-2.883760359351" />
                  <Point X="-0.903296517175" Y="4.719583366341" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.782752589502" Y="-3.005811752934" />
                  <Point X="-0.995855784223" Y="4.698187507655" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.735472960507" Y="-3.223335271445" />
                  <Point X="-1.087427474535" Y="4.672513984164" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.807401660844" Y="-3.957206792466" />
                  <Point X="-1.178999164847" Y="4.646840460672" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.566319515404" Y="-3.335279386619" />
                  <Point X="-1.270570855159" Y="4.621166937181" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.424258165761" Y="-3.142258168819" />
                  <Point X="-1.362142545471" Y="4.59549341369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.312203925062" Y="-3.079212019464" />
                  <Point X="-1.453714199875" Y="4.569819734663" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.207179729151" Y="-3.046616339505" />
                  <Point X="-1.487316051723" Y="4.293051254427" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.102155540847" Y="-3.014020692494" />
                  <Point X="-1.552473378332" Y="4.152964551823" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.000904098034" Y="-2.997766601562" />
                  <Point X="-1.635624004661" Y="4.090815392849" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.092641724723" Y="-3.014889217996" />
                  <Point X="-1.724526107067" Y="4.053578613748" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.183621656448" Y="-3.043125929529" />
                  <Point X="-1.816550905017" Y="4.029867715058" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.274601538865" Y="-3.071362854642" />
                  <Point X="-1.91748240766" Y="4.04473599287" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.363925483305" Y="-3.106772435146" />
                  <Point X="-2.027550398764" Y="4.099178750019" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.444946949772" Y="-3.178143998695" />
                  <Point X="-2.151857543339" Y="4.215298056902" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.518109312104" Y="-3.283557082191" />
                  <Point X="-2.253740309475" Y="4.234286709582" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.591271676994" Y="-3.388970154612" />
                  <Point X="-2.340155144619" Y="4.18627639234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.655198916908" Y="-3.53438494806" />
                  <Point X="-2.426569980377" Y="4.138266077759" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.707572263154" Y="-3.729845153186" />
                  <Point X="-2.512984823078" Y="4.090255793256" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.759945609399" Y="-3.925305358313" />
                  <Point X="-2.59939966578" Y="4.042245508753" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.812318955645" Y="-4.12076556344" />
                  <Point X="-2.684264127471" Y="3.987519786305" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.864692301891" Y="-4.316225768567" />
                  <Point X="-2.767100599566" Y="3.924009875851" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.917065658973" Y="-4.511685926758" />
                  <Point X="-2.849937071661" Y="3.860499965396" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.969439029108" Y="-4.707146028405" />
                  <Point X="-2.749643982776" Y="3.003768779687" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.811063996433" Y="3.269808087042" />
                  <Point X="-2.932773543756" Y="3.796990054942" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.056342032603" Y="-4.75304185621" />
                  <Point X="-1.173377823438" Y="-4.246104151779" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.190369197067" Y="-4.172506426835" />
                  <Point X="-2.813674656478" Y="2.858802007198" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.97352839844" Y="3.551204633917" />
                  <Point X="-3.015610012039" Y="3.733480127971" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.327319161294" Y="-4.001625051649" />
                  <Point X="-2.89318148861" Y="2.780869841572" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.449569709548" Y="-3.894413842116" />
                  <Point X="-2.979726103241" Y="2.733421661048" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.565444795606" Y="-3.814817793269" />
                  <Point X="-3.075086733857" Y="2.724159841074" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.667716564515" Y="-3.794144184467" />
                  <Point X="-3.174982559922" Y="2.734542110769" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.766713482632" Y="-3.787655512856" />
                  <Point X="-3.282719776807" Y="2.778889175621" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.86571036515" Y="-3.781166995446" />
                  <Point X="-3.395213108032" Y="2.843837234998" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.964317276901" Y="-3.776367627007" />
                  <Point X="-3.507706441169" Y="2.908785302652" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.057047157455" Y="-3.797024477409" />
                  <Point X="-2.318961171213" Y="-2.662550245678" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.368570551289" Y="-2.447668412739" />
                  <Point X="-3.620199796541" Y="2.973733466617" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.142888623474" Y="-3.847518329178" />
                  <Point X="-2.384890405249" Y="-2.799293449884" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.485606267301" Y="-2.363045123248" />
                  <Point X="-3.730585720297" Y="3.029553341382" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.227357259533" Y="-3.9039585608" />
                  <Point X="-2.454538671124" Y="-2.919927757408" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.586213188771" Y="-2.349582760963" />
                  <Point X="-3.451775769901" Y="1.399580676886" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.535524507255" Y="1.762336312235" />
                  <Point X="-3.805772935317" Y="2.932910858448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.311825870078" Y="-3.960398902936" />
                  <Point X="-2.524186936999" Y="-3.040562064933" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.677897323664" Y="-2.374769233467" />
                  <Point X="-3.523214320718" Y="1.286700945399" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.654262566994" Y="1.854333262508" />
                  <Point X="-3.880960151808" Y="2.83626838189" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.394541043184" Y="-4.024434217035" />
                  <Point X="-2.593835202873" Y="-3.161196372457" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.764115754027" Y="-2.423630273276" />
                  <Point X="-2.980180197975" Y="-1.487752347021" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.057727840371" Y="-1.151856604876" />
                  <Point X="-3.302495621154" Y="-0.091650867613" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.35862805156" Y="0.151485400455" />
                  <Point X="-3.608919836205" Y="1.235618227177" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.772752031628" Y="1.945253429081" />
                  <Point X="-3.956147371446" Y="2.739625918959" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.47012262569" Y="-4.119368506717" />
                  <Point X="-2.663483469962" Y="-3.281830674721" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.850147340724" Y="-2.473300621914" />
                  <Point X="-3.05633887095" Y="-1.580186983258" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.165909433892" Y="-1.105584733343" />
                  <Point X="-3.369107759743" Y="-0.225436087226" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.475888789981" Y="0.237083369083" />
                  <Point X="-3.700267274381" Y="1.208973360966" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.891241496262" Y="2.036173595655" />
                  <Point X="-4.029278856775" Y="2.634079092469" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.566752908899" Y="-4.123130857116" />
                  <Point X="-2.733131737795" Y="-3.402464973762" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.936178927422" Y="-2.522970970552" />
                  <Point X="-3.139165006222" Y="-1.643741667416" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.264052798571" Y="-1.102793207863" />
                  <Point X="-3.452266695906" Y="-0.287549252352" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.580763357755" Y="0.269030938374" />
                  <Point X="-3.797581791568" Y="1.208174753535" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.009730960896" Y="2.127093762229" />
                  <Point X="-4.099143844599" Y="2.514383510838" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.680513670302" Y="-4.052692954506" />
                  <Point X="-2.802780005628" Y="-3.523099272803" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.02221051412" Y="-2.57264131919" />
                  <Point X="-3.221991141493" Y="-1.707296351575" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.358675684663" Y="-1.115250550444" />
                  <Point X="-3.541995940111" Y="-0.321203286704" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.684691329233" Y="0.29687834865" />
                  <Point X="-3.898136998445" Y="1.221413115313" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.128220495082" Y="2.218014230067" />
                  <Point X="-4.169008832422" Y="2.394687929208" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.796251265318" Y="-3.973692444792" />
                  <Point X="-2.872428273461" Y="-3.643733571845" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.108242100818" Y="-2.622311667828" />
                  <Point X="-3.304817276764" Y="-1.770851035733" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.453298570756" Y="-1.127707893024" />
                  <Point X="-3.633814827648" Y="-0.345806081381" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.788619300712" Y="0.324725758926" />
                  <Point X="-3.998692205323" Y="1.234651477091" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.914828549522" Y="-3.88239188987" />
                  <Point X="-2.942076541294" Y="-3.764367870886" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.194273687515" Y="-2.671982016466" />
                  <Point X="-3.387643412035" Y="-1.834405719891" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.547921456849" Y="-1.140165235605" />
                  <Point X="-3.725633715185" Y="-0.370408876057" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.89254727219" Y="0.352573169201" />
                  <Point X="-4.0992474122" Y="1.247889838868" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.280305274213" Y="-2.721652365104" />
                  <Point X="-3.470469547306" Y="-1.89796040405" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.642544342941" Y="-1.152622578185" />
                  <Point X="-3.817452602721" Y="-0.395011670734" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.996475243668" Y="0.380420579477" />
                  <Point X="-4.199802619132" Y="1.26112820088" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.366336860911" Y="-2.771322713742" />
                  <Point X="-3.553295676666" Y="-1.961515113814" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.737167229034" Y="-1.165079920766" />
                  <Point X="-3.909271490258" Y="-0.419614465411" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.100403215146" Y="0.408267989753" />
                  <Point X="-4.300357826241" Y="1.27436656366" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.452368447609" Y="-2.820993062379" />
                  <Point X="-3.636121783731" Y="-2.025069920148" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.831790115126" Y="-1.177537263346" />
                  <Point X="-4.001090377795" Y="-0.444217260088" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.204331186625" Y="0.436115400029" />
                  <Point X="-4.40091303335" Y="1.287604926441" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.53840003226" Y="-2.870663419881" />
                  <Point X="-3.718947890795" Y="-2.088624726482" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.926413001219" Y="-1.189994605926" />
                  <Point X="-4.092909266449" Y="-0.468820049923" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.308259158103" Y="0.463962810305" />
                  <Point X="-4.501468240459" Y="1.300843289222" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.624431615065" Y="-2.920333785382" />
                  <Point X="-3.80177399786" Y="-2.152179532816" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.021035887312" Y="-1.202451948507" />
                  <Point X="-4.184728157534" Y="-0.493422829233" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.412187129581" Y="0.491810220581" />
                  <Point X="-4.602023447568" Y="1.314081652003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.710463197869" Y="-2.970004150883" />
                  <Point X="-3.884600104925" Y="-2.21573433915" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.115658773404" Y="-1.214909291087" />
                  <Point X="-4.276547048618" Y="-0.518025608543" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.516115095139" Y="0.51965760521" />
                  <Point X="-4.669707252812" Y="1.18493833065" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.798629001574" Y="-3.010430190046" />
                  <Point X="-3.967426211989" Y="-2.279289145484" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.210281659497" Y="-1.227366633668" />
                  <Point X="-4.368365939702" Y="-0.542628387853" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.620043058901" Y="0.547504982067" />
                  <Point X="-4.722353066229" Y="0.990658310504" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.938575621087" Y="-2.826568874784" />
                  <Point X="-4.050252319054" Y="-2.342843951818" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.304904544937" Y="-1.239823979073" />
                  <Point X="-4.460184830787" Y="-0.567231167163" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.723971022664" Y="0.575352358923" />
                  <Point X="-4.762919563075" Y="0.744057022049" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.088711899829" Y="-2.5985712964" />
                  <Point X="-4.133078426119" Y="-2.406398758152" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.399527429576" Y="-1.252281327949" />
                  <Point X="-4.552003721871" Y="-0.591833946473" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.494150314215" Y="-1.264738676825" />
                  <Point X="-4.643822612956" Y="-0.616436725783" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.588773198854" Y="-1.277196025702" />
                  <Point X="-4.73564150404" Y="-0.641039505093" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="0.001626220703" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.621563110352" Y="-4.097009277344" />
                  <Point X="0.471539031982" Y="-3.537112304688" />
                  <Point X="0.464318664551" Y="-3.521544189453" />
                  <Point X="0.40300012207" Y="-3.433195556641" />
                  <Point X="0.300591033936" Y="-3.285643798828" />
                  <Point X="0.274335754395" Y="-3.266399902344" />
                  <Point X="0.179448806763" Y="-3.236950439453" />
                  <Point X="0.02097677803" Y="-3.187766601562" />
                  <Point X="-0.008664908409" Y="-3.187766601562" />
                  <Point X="-0.103551864624" Y="-3.217215820312" />
                  <Point X="-0.262023895264" Y="-3.266399902344" />
                  <Point X="-0.288279144287" Y="-3.285643798828" />
                  <Point X="-0.349597686768" Y="-3.3739921875" />
                  <Point X="-0.45200680542" Y="-3.521544189453" />
                  <Point X="-0.459227142334" Y="-3.537112304688" />
                  <Point X="-0.709784301758" Y="-4.472204589844" />
                  <Point X="-0.84774395752" Y="-4.987077148438" />
                  <Point X="-0.914239868164" Y="-4.974169921875" />
                  <Point X="-1.100246948242" Y="-4.938065429688" />
                  <Point X="-1.209530395508" Y="-4.909947265625" />
                  <Point X="-1.351589599609" Y="-4.873396972656" />
                  <Point X="-1.327209594727" Y="-4.688212890625" />
                  <Point X="-1.309150146484" Y="-4.551038574219" />
                  <Point X="-1.309683227539" Y="-4.5347578125" />
                  <Point X="-1.33235168457" Y="-4.420795898438" />
                  <Point X="-1.370210693359" Y="-4.230466308594" />
                  <Point X="-1.386282714844" Y="-4.20262890625" />
                  <Point X="-1.473642578125" Y="-4.126016113281" />
                  <Point X="-1.619543457031" Y="-3.998063964844" />
                  <Point X="-1.649240722656" Y="-3.985762939453" />
                  <Point X="-1.765186767578" Y="-3.978163330078" />
                  <Point X="-1.958829833984" Y="-3.965471435547" />
                  <Point X="-1.989878540039" Y="-3.973791015625" />
                  <Point X="-2.086490966797" Y="-4.038345458984" />
                  <Point X="-2.247844970703" Y="-4.146159179688" />
                  <Point X="-2.259734130859" Y="-4.157294433594" />
                  <Point X="-2.400402587891" Y="-4.3406171875" />
                  <Point X="-2.457095214844" Y="-4.414500488281" />
                  <Point X="-2.583186767578" Y="-4.336427734375" />
                  <Point X="-2.855831054688" Y="-4.16761328125" />
                  <Point X="-3.007161376953" Y="-4.051093994141" />
                  <Point X="-3.228581054688" Y="-3.880608154297" />
                  <Point X="-2.784836425781" Y="-3.112020019531" />
                  <Point X="-2.506033691406" Y="-2.629119628906" />
                  <Point X="-2.499762695312" Y="-2.597593261719" />
                  <Point X="-2.513979736328" Y="-2.568764404297" />
                  <Point X="-2.531328369141" Y="-2.551415771484" />
                  <Point X="-2.560157226562" Y="-2.537198730469" />
                  <Point X="-2.59168359375" Y="-2.543469726562" />
                  <Point X="-3.398182617188" Y="-3.009102050781" />
                  <Point X="-3.842959472656" Y="-3.265894042969" />
                  <Point X="-3.946297363281" Y="-3.130129150391" />
                  <Point X="-4.161703613281" Y="-2.847129394531" />
                  <Point X="-4.270192871094" Y="-2.665209228516" />
                  <Point X="-4.431020019531" Y="-2.395527099609" />
                  <Point X="-3.651601074219" Y="-1.797457885742" />
                  <Point X="-3.163786865234" Y="-1.423144897461" />
                  <Point X="-3.145822021484" Y="-1.396013427734" />
                  <Point X="-3.138117431641" Y="-1.366266235352" />
                  <Point X="-3.140326416016" Y="-1.334595703125" />
                  <Point X="-3.161158935547" Y="-1.310639282227" />
                  <Point X="-3.187641601562" Y="-1.295052734375" />
                  <Point X="-3.219529296875" Y="-1.288571044922" />
                  <Point X="-4.237657714844" Y="-1.422610229492" />
                  <Point X="-4.803283203125" Y="-1.497076293945" />
                  <Point X="-4.843145996094" Y="-1.341014648438" />
                  <Point X="-4.927393066406" Y="-1.011191589355" />
                  <Point X="-4.956096679688" Y="-0.810499267578" />
                  <Point X="-4.998396484375" Y="-0.514742126465" />
                  <Point X="-4.113157714844" Y="-0.27754309082" />
                  <Point X="-3.557463867188" Y="-0.128645431519" />
                  <Point X="-3.531502197266" Y="-0.114211112976" />
                  <Point X="-3.514143066406" Y="-0.102163047791" />
                  <Point X="-3.502324462891" Y="-0.090645385742" />
                  <Point X="-3.491434326172" Y="-0.064744720459" />
                  <Point X="-3.483400878906" Y="-0.031280040741" />
                  <Point X="-3.489112548828" Y="-0.005295996666" />
                  <Point X="-3.50232421875" Y="0.028085174561" />
                  <Point X="-3.524536621094" Y="0.046816764832" />
                  <Point X="-3.541895507813" Y="0.058864833832" />
                  <Point X="-3.557463867188" Y="0.066085334778" />
                  <Point X="-4.485536132812" Y="0.314761566162" />
                  <Point X="-4.998186523438" Y="0.452125793457" />
                  <Point X="-4.971940917969" Y="0.629492004395" />
                  <Point X="-4.917645019531" Y="0.996418212891" />
                  <Point X="-4.859865234375" Y="1.209644165039" />
                  <Point X="-4.773516113281" Y="1.528298583984" />
                  <Point X="-4.1516015625" Y="1.44642199707" />
                  <Point X="-3.753266357422" Y="1.393980102539" />
                  <Point X="-3.731704589844" Y="1.395866699219" />
                  <Point X="-3.708699707031" Y="1.403120117188" />
                  <Point X="-3.670278564453" Y="1.41523425293" />
                  <Point X="-3.651534423828" Y="1.426056274414" />
                  <Point X="-3.639119873047" Y="1.443786132813" />
                  <Point X="-3.629888916016" Y="1.466071289062" />
                  <Point X="-3.614472412109" Y="1.503290283203" />
                  <Point X="-3.610714111328" Y="1.52460534668" />
                  <Point X="-3.616315917969" Y="1.54551171875" />
                  <Point X="-3.627453857422" Y="1.566907592773" />
                  <Point X="-3.646055664062" Y="1.602641479492" />
                  <Point X="-3.659968261719" Y="1.619221679688" />
                  <Point X="-4.192312988281" Y="2.027703979492" />
                  <Point X="-4.47610546875" Y="2.245466308594" />
                  <Point X="-4.370988769531" Y="2.425556884766" />
                  <Point X="-4.160014648438" Y="2.787006591797" />
                  <Point X="-4.006958251953" Y="2.983738769531" />
                  <Point X="-3.774671142578" Y="3.282310791016" />
                  <Point X="-3.403803222656" Y="3.068189941406" />
                  <Point X="-3.159156982422" Y="2.926943359375" />
                  <Point X="-3.138514648438" Y="2.920434814453" />
                  <Point X="-3.106475097656" Y="2.917631591797" />
                  <Point X="-3.052965332031" Y="2.912950195312" />
                  <Point X="-3.031506591797" Y="2.915775390625" />
                  <Point X="-3.013252441406" Y="2.927404541016" />
                  <Point X="-2.990510498047" Y="2.950146484375" />
                  <Point X="-2.952528808594" Y="2.988127929688" />
                  <Point X="-2.940899414062" Y="3.006382324219" />
                  <Point X="-2.93807421875" Y="3.027841064453" />
                  <Point X="-2.940877441406" Y="3.059880859375" />
                  <Point X="-2.945558837891" Y="3.113390625" />
                  <Point X="-2.952067382812" Y="3.134032958984" />
                  <Point X="-3.187965332031" Y="3.542620117188" />
                  <Point X="-3.307278564453" Y="3.749276611328" />
                  <Point X="-3.120326171875" Y="3.892611328125" />
                  <Point X="-2.752876220703" Y="4.174331542969" />
                  <Point X="-2.511817382812" Y="4.308258300781" />
                  <Point X="-2.141548583984" Y="4.513972167969" />
                  <Point X="-2.042720581055" Y="4.385176757813" />
                  <Point X="-1.967826904297" Y="4.287573242188" />
                  <Point X="-1.951246826172" Y="4.273660644531" />
                  <Point X="-1.915586914063" Y="4.255097167969" />
                  <Point X="-1.856030761719" Y="4.22409375" />
                  <Point X="-1.835124267578" Y="4.2184921875" />
                  <Point X="-1.813808959961" Y="4.222250488281" />
                  <Point X="-1.776666748047" Y="4.237635742188" />
                  <Point X="-1.714635009766" Y="4.263330078125" />
                  <Point X="-1.696905395508" Y="4.275744628906" />
                  <Point X="-1.686083496094" Y="4.294488769531" />
                  <Point X="-1.673994262695" Y="4.332830566406" />
                  <Point X="-1.653804077148" Y="4.396865234375" />
                  <Point X="-1.651917602539" Y="4.418426757812" />
                  <Point X="-1.678736572266" Y="4.622137207031" />
                  <Point X="-1.689137573242" Y="4.701141113281" />
                  <Point X="-1.443780761719" Y="4.769930664062" />
                  <Point X="-0.968094177246" Y="4.903296386719" />
                  <Point X="-0.675859008789" Y="4.937498046875" />
                  <Point X="-0.224199615479" Y="4.990358398438" />
                  <Point X="-0.114513511658" Y="4.58100390625" />
                  <Point X="-0.042140380859" Y="4.310903320312" />
                  <Point X="-0.024282125473" Y="4.284176757813" />
                  <Point X="0.006155934334" Y="4.273844238281" />
                  <Point X="0.036594028473" Y="4.284176757813" />
                  <Point X="0.054452251434" Y="4.310903320312" />
                  <Point X="0.17532371521" Y="4.762001953125" />
                  <Point X="0.236648422241" Y="4.990868652344" />
                  <Point X="0.444861328125" Y="4.969063476562" />
                  <Point X="0.860205749512" Y="4.925565429688" />
                  <Point X="1.10198059082" Y="4.867193847656" />
                  <Point X="1.508455932617" Y="4.769058105469" />
                  <Point X="1.665873168945" Y="4.711961425781" />
                  <Point X="1.931033691406" Y="4.615786132813" />
                  <Point X="2.083205810547" Y="4.544620117188" />
                  <Point X="2.33869921875" Y="4.425134277344" />
                  <Point X="2.485706787109" Y="4.339487304688" />
                  <Point X="2.732523681641" Y="4.19569140625" />
                  <Point X="2.871168457031" Y="4.097095214844" />
                  <Point X="3.068740234375" Y="3.956592773438" />
                  <Point X="2.552546630859" Y="3.062518798828" />
                  <Point X="2.229853515625" Y="2.503597900391" />
                  <Point X="2.224852050781" Y="2.491514648438" />
                  <Point X="2.216811279297" Y="2.461446044922" />
                  <Point X="2.203382324219" Y="2.411228271484" />
                  <Point X="2.202044677734" Y="2.392325927734" />
                  <Point X="2.205179931641" Y="2.366325195312" />
                  <Point X="2.210416015625" Y="2.322901123047" />
                  <Point X="2.218682128906" Y="2.3008125" />
                  <Point X="2.234770507812" Y="2.277102539062" />
                  <Point X="2.261639892578" Y="2.23750390625" />
                  <Point X="2.274939941406" Y="2.224203613281" />
                  <Point X="2.298649902344" Y="2.208115234375" />
                  <Point X="2.338248535156" Y="2.18124609375" />
                  <Point X="2.360336914062" Y="2.172980224609" />
                  <Point X="2.386337646484" Y="2.169844970703" />
                  <Point X="2.42976171875" Y="2.164608642578" />
                  <Point X="2.448664306641" Y="2.165946289062" />
                  <Point X="2.478732910156" Y="2.173987060547" />
                  <Point X="2.528950683594" Y="2.187416015625" />
                  <Point X="2.541033935547" Y="2.192417480469" />
                  <Point X="3.47449609375" Y="2.731352050781" />
                  <Point X="3.994248046875" Y="3.031430908203" />
                  <Point X="4.056188476563" Y="2.94534765625" />
                  <Point X="4.202591796875" Y="2.741880126953" />
                  <Point X="4.279884277344" Y="2.614153808594" />
                  <Point X="4.387512695312" Y="2.436296142578" />
                  <Point X="3.713471923828" Y="1.91908605957" />
                  <Point X="3.288616210938" Y="1.593082763672" />
                  <Point X="3.279371337891" Y="1.583833374023" />
                  <Point X="3.257730957031" Y="1.555601806641" />
                  <Point X="3.221589111328" Y="1.508451904297" />
                  <Point X="3.213119628906" Y="1.4915" />
                  <Point X="3.20505859375" Y="1.462675537109" />
                  <Point X="3.191595703125" Y="1.414535522461" />
                  <Point X="3.190779541016" Y="1.390965209961" />
                  <Point X="3.197396728516" Y="1.358894287109" />
                  <Point X="3.208448486328" Y="1.305332397461" />
                  <Point X="3.215646484375" Y="1.287954956055" />
                  <Point X="3.233644775391" Y="1.260598144531" />
                  <Point X="3.263704101562" Y="1.214909667969" />
                  <Point X="3.280947998047" Y="1.198819946289" />
                  <Point X="3.307030029297" Y="1.184138061523" />
                  <Point X="3.350590087891" Y="1.159617675781" />
                  <Point X="3.368565673828" Y="1.153619628906" />
                  <Point X="3.403830322266" Y="1.148958862305" />
                  <Point X="3.462726318359" Y="1.141175048828" />
                  <Point X="3.475803710938" Y="1.14117175293" />
                  <Point X="4.362530273438" Y="1.257911621094" />
                  <Point X="4.848975097656" Y="1.32195324707" />
                  <Point X="4.876309082031" Y="1.209674438477" />
                  <Point X="4.939188476562" Y="0.951385620117" />
                  <Point X="4.963545410156" Y="0.794943725586" />
                  <Point X="4.997858398438" Y="0.574556152344" />
                  <Point X="4.228061523438" Y="0.368289764404" />
                  <Point X="3.741167724609" Y="0.237826919556" />
                  <Point X="3.729086914062" Y="0.232819412231" />
                  <Point X="3.694440429688" Y="0.212793136597" />
                  <Point X="3.636576904297" Y="0.179346878052" />
                  <Point X="3.622264892578" Y="0.166926574707" />
                  <Point X="3.601477050781" Y="0.140437957764" />
                  <Point X="3.566759033203" Y="0.096198890686" />
                  <Point X="3.556985107422" Y="0.07473513031" />
                  <Point X="3.550055664062" Y="0.03855304718" />
                  <Point X="3.538482910156" Y="-0.021875303268" />
                  <Point X="3.538482910156" Y="-0.04068478775" />
                  <Point X="3.545412353516" Y="-0.076866867065" />
                  <Point X="3.556985107422" Y="-0.137295227051" />
                  <Point X="3.566759033203" Y="-0.158758987427" />
                  <Point X="3.587546875" Y="-0.18524760437" />
                  <Point X="3.622264892578" Y="-0.229486679077" />
                  <Point X="3.636576904297" Y="-0.241906967163" />
                  <Point X="3.671223388672" Y="-0.261933227539" />
                  <Point X="3.729086914062" Y="-0.295379486084" />
                  <Point X="3.741167724609" Y="-0.300387023926" />
                  <Point X="4.554336425781" Y="-0.518274780273" />
                  <Point X="4.998067871094" Y="-0.637172363281" />
                  <Point X="4.983541015625" Y="-0.733528442383" />
                  <Point X="4.948431640625" Y="-0.966399353027" />
                  <Point X="4.917226074219" Y="-1.103146240234" />
                  <Point X="4.874545410156" Y="-1.290178344727" />
                  <Point X="3.976642089844" Y="-1.171967041016" />
                  <Point X="3.411982177734" Y="-1.097628295898" />
                  <Point X="3.3948359375" Y="-1.098341308594" />
                  <Point X="3.326837158203" Y="-1.11312109375" />
                  <Point X="3.213271728516" Y="-1.137805053711" />
                  <Point X="3.1854453125" Y="-1.154697265625" />
                  <Point X="3.144344238281" Y="-1.20412890625" />
                  <Point X="3.075701171875" Y="-1.286685424805" />
                  <Point X="3.064357910156" Y="-1.314070678711" />
                  <Point X="3.058467041016" Y="-1.378086669922" />
                  <Point X="3.04862890625" Y="-1.485000854492" />
                  <Point X="3.056360351562" Y="-1.516621826172" />
                  <Point X="3.093991943359" Y="-1.575155273438" />
                  <Point X="3.156840576172" Y="-1.672912353516" />
                  <Point X="3.168460693359" Y="-1.685540649414" />
                  <Point X="3.923086181641" Y="-2.264585205078" />
                  <Point X="4.33907421875" Y="-2.583784179688" />
                  <Point X="4.303101074219" Y="-2.641994140625" />
                  <Point X="4.204131835938" Y="-2.802141845703" />
                  <Point X="4.139596191406" Y="-2.893837402344" />
                  <Point X="4.056688476562" Y="-3.011638427734" />
                  <Point X="3.256360351562" Y="-2.549568603516" />
                  <Point X="2.753454589844" Y="-2.259215576172" />
                  <Point X="2.737340820313" Y="-2.253312744141" />
                  <Point X="2.656411621094" Y="-2.238697021484" />
                  <Point X="2.521250488281" Y="-2.214287109375" />
                  <Point X="2.489077636719" Y="-2.219244873047" />
                  <Point X="2.421845214844" Y="-2.254628662109" />
                  <Point X="2.309559570312" Y="-2.313723876953" />
                  <Point X="2.288599853516" Y="-2.334683837891" />
                  <Point X="2.253215820312" Y="-2.401916259766" />
                  <Point X="2.194120849609" Y="-2.514201904297" />
                  <Point X="2.189163085938" Y="-2.546374511719" />
                  <Point X="2.203778808594" Y="-2.627303710938" />
                  <Point X="2.228188720703" Y="-2.76246484375" />
                  <Point X="2.234091552734" Y="-2.778578613281" />
                  <Point X="2.719014404297" Y="-3.618489257812" />
                  <Point X="2.986673583984" Y="-4.082088623047" />
                  <Point X="2.952734130859" Y="-4.106331054688" />
                  <Point X="2.835298339844" Y="-4.190212402344" />
                  <Point X="2.763144775391" Y="-4.236916015625" />
                  <Point X="2.679775878906" Y="-4.290879394531" />
                  <Point X="2.066941162109" Y="-3.492217773438" />
                  <Point X="1.683177612305" Y="-2.992087402344" />
                  <Point X="1.670549316406" Y="-2.980467529297" />
                  <Point X="1.590731201172" Y="-2.929151855469" />
                  <Point X="1.45742590332" Y="-2.843448730469" />
                  <Point X="1.425804931641" Y="-2.835717041016" />
                  <Point X="1.338510253906" Y="-2.84375" />
                  <Point X="1.192718017578" Y="-2.857166015625" />
                  <Point X="1.165332885742" Y="-2.868509277344" />
                  <Point X="1.097926147461" Y="-2.924555664062" />
                  <Point X="0.985349060059" Y="-3.018159667969" />
                  <Point X="0.968456726074" Y="-3.045986328125" />
                  <Point X="0.94830255127" Y="-3.138711914062" />
                  <Point X="0.91464251709" Y="-3.29357421875" />
                  <Point X="0.91392956543" Y="-3.310720214844" />
                  <Point X="1.051352905273" Y="-4.354554199219" />
                  <Point X="1.127642211914" Y="-4.934028320312" />
                  <Point X="1.105448730469" Y="-4.938893066406" />
                  <Point X="0.994345458984" Y="-4.963247070312" />
                  <Point X="0.927688354492" Y="-4.975356445312" />
                  <Point X="0.860200378418" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#154" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.072373132609" Y="4.625224695354" Z="0.95" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.95" />
                  <Point X="-0.687827167114" Y="5.018439361996" Z="0.95" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.95" />
                  <Point X="-1.463434592406" Y="4.849354145221" Z="0.95" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.95" />
                  <Point X="-1.734464829951" Y="4.646890816506" Z="0.95" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.95" />
                  <Point X="-1.727836057336" Y="4.379145697495" Z="0.95" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.95" />
                  <Point X="-1.80195686037" Y="4.315109506691" Z="0.95" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.95" />
                  <Point X="-1.898655254551" Y="4.330727797472" Z="0.95" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.95" />
                  <Point X="-2.009208819016" Y="4.446894628308" Z="0.95" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.95" />
                  <Point X="-2.542256355709" Y="4.383246003837" Z="0.95" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.95" />
                  <Point X="-3.156904143837" Y="3.963571320746" Z="0.95" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.95" />
                  <Point X="-3.237422677147" Y="3.548900191118" Z="0.95" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.95" />
                  <Point X="-2.996843140853" Y="3.086803136521" Z="0.95" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.95" />
                  <Point X="-3.032021631993" Y="3.016781935893" Z="0.95" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.95" />
                  <Point X="-3.108273329085" Y="2.998721558319" Z="0.95" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.95" />
                  <Point X="-3.384959157179" Y="3.142771271599" Z="0.95" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.95" />
                  <Point X="-4.052577615423" Y="3.045721167049" Z="0.95" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.95" />
                  <Point X="-4.420326774766" Y="2.482042096666" Z="0.95" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.95" />
                  <Point X="-4.228906841898" Y="2.019316417288" Z="0.95" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.95" />
                  <Point X="-3.677960478325" Y="1.575100368889" Z="0.95" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.95" />
                  <Point X="-3.682239040352" Y="1.516485397167" Z="0.95" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.95" />
                  <Point X="-3.729891004856" Y="1.482085996979" Z="0.95" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.95" />
                  <Point X="-4.151230960485" Y="1.527274333781" Z="0.95" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.95" />
                  <Point X="-4.914280898113" Y="1.254001521842" Z="0.95" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.95" />
                  <Point X="-5.027558317573" Y="0.668091012806" Z="0.95" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.95" />
                  <Point X="-4.504633192483" Y="0.29774530121" Z="0.95" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.95" />
                  <Point X="-3.559201748797" Y="0.037020903848" Z="0.95" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.95" />
                  <Point X="-3.543021431034" Y="0.011163172741" Z="0.95" />
                  <Point X="-3.539556741714" Y="0" Z="0.95" />
                  <Point X="-3.545343170101" Y="-0.01864377832" Z="0.95" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.95" />
                  <Point X="-3.566166846326" Y="-0.041855079286" Z="0.95" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.95" />
                  <Point X="-4.13225459629" Y="-0.197966755546" Z="0.95" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.95" />
                  <Point X="-5.011748613306" Y="-0.786298351659" Z="0.95" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.95" />
                  <Point X="-4.897746983604" Y="-1.322108856122" Z="0.95" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.95" />
                  <Point X="-4.237287389832" Y="-1.44090244239" Z="0.95" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.95" />
                  <Point X="-3.202594449962" Y="-1.316612415281" Z="0.95" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.95" />
                  <Point X="-3.197495346885" Y="-1.341056312722" Z="0.95" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.95" />
                  <Point X="-3.688194986049" Y="-1.726510142878" Z="0.95" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.95" />
                  <Point X="-4.319292375137" Y="-2.659538688545" Z="0.95" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.95" />
                  <Point X="-3.992239291881" Y="-3.129132270803" Z="0.95" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.95" />
                  <Point X="-3.379338501375" Y="-3.021123380545" Z="0.95" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.95" />
                  <Point X="-2.561988267616" Y="-2.566342201516" Z="0.95" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.95" />
                  <Point X="-2.834293632624" Y="-3.055739881117" Z="0.95" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.95" />
                  <Point X="-3.043821185752" Y="-4.059431093643" Z="0.95" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.95" />
                  <Point X="-2.61566415568" Y="-4.347658522805" Z="0.95" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.95" />
                  <Point X="-2.366890854811" Y="-4.339774975048" Z="0.95" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.95" />
                  <Point X="-2.064868432468" Y="-4.04863882847" Z="0.95" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.95" />
                  <Point X="-1.774612862059" Y="-3.99677642315" Z="0.95" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.95" />
                  <Point X="-1.512765849674" Y="-4.132327851361" Z="0.95" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.95" />
                  <Point X="-1.387547016534" Y="-4.399270186363" Z="0.95" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.95" />
                  <Point X="-1.382937877881" Y="-4.650406497854" Z="0.95" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.95" />
                  <Point X="-1.228145102589" Y="-4.927090293157" Z="0.95" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.95" />
                  <Point X="-0.929923094591" Y="-4.991973857108" Z="0.95" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.95" />
                  <Point X="-0.667644072766" Y="-4.453865692296" Z="0.95" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.95" />
                  <Point X="-0.314677682586" Y="-3.371221093716" Z="0.95" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.95" />
                  <Point X="-0.094886966551" Y="-3.2336887812" Z="0.95" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.95" />
                  <Point X="0.15847211281" Y="-3.253423264088" Z="0.95" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.95" />
                  <Point X="0.355768176847" Y="-3.430424662052" Z="0.95" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.95" />
                  <Point X="0.567110787635" Y="-4.07867033752" Z="0.95" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.95" />
                  <Point X="0.930469209204" Y="-4.993271102605" Z="0.95" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.95" />
                  <Point X="1.109998619471" Y="-4.956453570892" Z="0.95" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.95" />
                  <Point X="1.094769170485" Y="-4.316747825099" Z="0.95" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.95" />
                  <Point X="0.991005738275" Y="-3.11805150044" Z="0.95" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.95" />
                  <Point X="1.123737006955" Y="-2.931721937239" Z="0.95" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.95" />
                  <Point X="1.336935813286" Y="-2.862259521662" Z="0.95" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.95" />
                  <Point X="1.557535811065" Y="-2.939929615357" Z="0.95" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.95" />
                  <Point X="2.02111748354" Y="-3.491375610757" Z="0.95" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.95" />
                  <Point X="2.784158026163" Y="-4.247610292375" Z="0.95" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.95" />
                  <Point X="2.975639607649" Y="-4.11573790583" Z="0.95" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.95" />
                  <Point X="2.756159637975" Y="-3.562209096578" Z="0.95" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.95" />
                  <Point X="2.24682688371" Y="-2.587137455655" Z="0.95" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.95" />
                  <Point X="2.291306291637" Y="-2.393922510208" Z="0.95" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.95" />
                  <Point X="2.438975868164" Y="-2.26759501833" Z="0.95" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.95" />
                  <Point X="2.641369347858" Y="-2.256621125781" Z="0.95" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.95" />
                  <Point X="3.225204500529" Y="-2.561589862299" Z="0.95" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.95" />
                  <Point X="4.174329075985" Y="-2.891334409567" Z="0.95" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.95" />
                  <Point X="4.339478416168" Y="-2.636999209956" Z="0.95" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.95" />
                  <Point X="3.947368126884" Y="-2.193637466852" Z="0.95" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.95" />
                  <Point X="3.129893968534" Y="-1.516835763929" Z="0.95" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.95" />
                  <Point X="3.102100717235" Y="-1.351388309018" Z="0.95" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.95" />
                  <Point X="3.176634645127" Y="-1.204815786407" Z="0.95" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.95" />
                  <Point X="3.33130115772" Y="-1.130700223706" Z="0.95" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.95" />
                  <Point X="3.963959838658" Y="-1.190259315328" Z="0.95" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.95" />
                  <Point X="4.959817406447" Y="-1.082990241349" Z="0.95" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.95" />
                  <Point X="5.026826241412" Y="-0.709702633977" Z="0.95" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.95" />
                  <Point X="4.561121261384" Y="-0.438698486297" Z="0.95" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.95" />
                  <Point X="3.690089161432" Y="-0.187364474603" Z="0.95" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.95" />
                  <Point X="3.620724591754" Y="-0.123099160149" Z="0.95" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.95" />
                  <Point X="3.588364016063" Y="-0.036182165931" Z="0.95" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.95" />
                  <Point X="3.593007434361" Y="0.060428365289" Z="0.95" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.95" />
                  <Point X="3.634654846647" Y="0.140849578426" Z="0.95" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.95" />
                  <Point X="3.71330625292" Y="0.200784369639" Z="0.95" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.95" />
                  <Point X="4.234846555923" Y="0.351273446472" Z="0.95" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.95" />
                  <Point X="5.006794309027" Y="0.833915820735" Z="0.95" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.95" />
                  <Point X="4.918734180038" Y="1.252781243288" Z="0.95" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.95" />
                  <Point X="4.349847944739" Y="1.338763881496" Z="0.95" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.95" />
                  <Point X="3.404225320156" Y="1.229807919436" Z="0.95" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.95" />
                  <Point X="3.325400603209" Y="1.258989095253" Z="0.95" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.95" />
                  <Point X="3.269259307726" Y="1.319359761055" Z="0.95" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.95" />
                  <Point X="3.240209338185" Y="1.400278371993" Z="0.95" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.95" />
                  <Point X="3.247054926584" Y="1.480489285773" Z="0.95" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.95" />
                  <Point X="3.2912578249" Y="1.556463579626" Z="0.95" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.95" />
                  <Point X="3.737753968014" Y="1.9106984178" Z="0.95" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.95" />
                  <Point X="4.316505669415" Y="2.671319970297" Z="0.95" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.95" />
                  <Point X="4.090617972073" Y="3.005830662892" Z="0.95" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.95" />
                  <Point X="3.443340127324" Y="2.805933393701" Z="0.95" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.95" />
                  <Point X="2.459660126558" Y="2.253569875626" Z="0.95" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.95" />
                  <Point X="2.386167494724" Y="2.25076532432" Z="0.95" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.95" />
                  <Point X="2.320568183699" Y="2.280769795192" Z="0.95" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.95" />
                  <Point X="2.269988805043" Y="2.336456676683" Z="0.95" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.95" />
                  <Point X="2.248664378438" Y="2.403590949109" Z="0.95" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.95" />
                  <Point X="2.258958067032" Y="2.479809449324" Z="0.95" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.95" />
                  <Point X="2.589691959159" Y="3.068798954631" Z="0.95" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.95" />
                  <Point X="2.89398946401" Y="4.169122119417" Z="0.95" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.95" />
                  <Point X="2.504720787903" Y="4.413970087498" Z="0.95" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.95" />
                  <Point X="2.09823116467" Y="4.621192297922" Z="0.95" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.95" />
                  <Point X="1.676765786751" Y="4.790245600089" Z="0.95" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.95" />
                  <Point X="1.107558973165" Y="4.947077342532" Z="0.95" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.95" />
                  <Point X="0.443913236643" Y="5.050071327311" Z="0.95" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.95" />
                  <Point X="0.120871428944" Y="4.806222810597" Z="0.95" />
                  <Point X="0" Y="4.355124473572" Z="0.95" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>