<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#163" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1811" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="0" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000475830078" Y="0.004715820312" />
                  <Width Value="9.783896972656" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.744867858887" Y="-4.190137207031" />
                  <Point X="0.563302062988" Y="-3.512524414063" />
                  <Point X="0.557721191406" Y="-3.497141845703" />
                  <Point X="0.542363037109" Y="-3.467377197266" />
                  <Point X="0.468152557373" Y="-3.360453857422" />
                  <Point X="0.378635528564" Y="-3.231476806641" />
                  <Point X="0.356751586914" Y="-3.209021484375" />
                  <Point X="0.330496398926" Y="-3.189777587891" />
                  <Point X="0.302495178223" Y="-3.175669189453" />
                  <Point X="0.187658752441" Y="-3.140028076172" />
                  <Point X="0.049136188507" Y="-3.097035888672" />
                  <Point X="0.020976638794" Y="-3.092766601562" />
                  <Point X="-0.00866468811" Y="-3.092766601562" />
                  <Point X="-0.036824085236" Y="-3.097035888672" />
                  <Point X="-0.15166065979" Y="-3.132676757812" />
                  <Point X="-0.290183227539" Y="-3.175669189453" />
                  <Point X="-0.31818460083" Y="-3.189777587891" />
                  <Point X="-0.344439788818" Y="-3.209021484375" />
                  <Point X="-0.366323577881" Y="-3.2314765625" />
                  <Point X="-0.440534057617" Y="-3.338399658203" />
                  <Point X="-0.530051086426" Y="-3.467376953125" />
                  <Point X="-0.538189025879" Y="-3.481573730469" />
                  <Point X="-0.550990112305" Y="-3.512524414063" />
                  <Point X="-0.770005432129" Y="-4.329901367188" />
                  <Point X="-0.916584594727" Y="-4.876941894531" />
                  <Point X="-0.919738830566" Y="-4.876329589844" />
                  <Point X="-1.079341308594" Y="-4.845350097656" />
                  <Point X="-1.209141601562" Y="-4.811953613281" />
                  <Point X="-1.24641809082" Y="-4.802362792969" />
                  <Point X="-1.236819458008" Y="-4.729453125" />
                  <Point X="-1.214963012695" Y="-4.563438476562" />
                  <Point X="-1.214201171875" Y="-4.5479296875" />
                  <Point X="-1.216508666992" Y="-4.516224121094" />
                  <Point X="-1.243943115234" Y="-4.378302246094" />
                  <Point X="-1.277036132812" Y="-4.211932617188" />
                  <Point X="-1.287938354492" Y="-4.182966308594" />
                  <Point X="-1.304010375977" Y="-4.15512890625" />
                  <Point X="-1.323644775391" Y="-4.131204101562" />
                  <Point X="-1.429371826172" Y="-4.038483886719" />
                  <Point X="-1.556905517578" Y="-3.926639160156" />
                  <Point X="-1.583188476562" Y="-3.910295410156" />
                  <Point X="-1.612885742188" Y="-3.897994384766" />
                  <Point X="-1.64302734375" Y="-3.890966308594" />
                  <Point X="-1.783350585938" Y="-3.881769042969" />
                  <Point X="-1.952616577148" Y="-3.870674804688" />
                  <Point X="-1.98341796875" Y="-3.873708496094" />
                  <Point X="-2.014466674805" Y="-3.882028076172" />
                  <Point X="-2.042657714844" Y="-3.894801513672" />
                  <Point X="-2.159582519531" Y="-3.972928222656" />
                  <Point X="-2.300624267578" Y="-4.067169677734" />
                  <Point X="-2.312785644531" Y="-4.076821533203" />
                  <Point X="-2.335102539062" Y="-4.099461914062" />
                  <Point X="-2.458062744141" Y="-4.259706542969" />
                  <Point X="-2.480148925781" Y="-4.288489746094" />
                  <Point X="-2.567770507812" Y="-4.234236816406" />
                  <Point X="-2.801706787109" Y="-4.089389404297" />
                  <Point X="-2.981444335938" Y="-3.950997558594" />
                  <Point X="-3.104721923828" Y="-3.856077880859" />
                  <Point X="-2.761180908203" Y="-3.261047363281" />
                  <Point X="-2.423761230469" Y="-2.676619628906" />
                  <Point X="-2.412859130859" Y="-2.647653076172" />
                  <Point X="-2.406588134766" Y="-2.616126464844" />
                  <Point X="-2.405575439453" Y="-2.585192382812" />
                  <Point X="-2.414560302734" Y="-2.555574707031" />
                  <Point X="-2.428777587891" Y="-2.526745605469" />
                  <Point X="-2.446804931641" Y="-2.501588867188" />
                  <Point X="-2.464153564453" Y="-2.484240234375" />
                  <Point X="-2.489312011719" Y="-2.466211914062" />
                  <Point X="-2.518141113281" Y="-2.451995361328" />
                  <Point X="-2.547758300781" Y="-2.443011230469" />
                  <Point X="-2.578691650391" Y="-2.444024169922" />
                  <Point X="-2.610217529297" Y="-2.450295166016" />
                  <Point X="-2.63918359375" Y="-2.461197265625" />
                  <Point X="-3.344155029297" Y="-2.868212646484" />
                  <Point X="-3.818024414062" Y="-3.141801269531" />
                  <Point X="-3.898035888672" Y="-3.036682373047" />
                  <Point X="-4.082860595703" Y="-2.793860595703" />
                  <Point X="-4.211714355469" Y="-2.577792236328" />
                  <Point X="-4.306142578125" Y="-2.419450439453" />
                  <Point X="-3.696329345703" Y="-1.951523925781" />
                  <Point X="-3.105954589844" Y="-1.498513549805" />
                  <Point X="-3.084576904297" Y="-1.475592895508" />
                  <Point X="-3.066612060547" Y="-1.448461425781" />
                  <Point X="-3.053856445312" Y="-1.419832641602" />
                  <Point X="-3.046151855469" Y="-1.390085327148" />
                  <Point X="-3.04334765625" Y="-1.359655883789" />
                  <Point X="-3.045556640625" Y="-1.327985473633" />
                  <Point X="-3.052557861328" Y="-1.298240600586" />
                  <Point X="-3.068640136719" Y="-1.272257202148" />
                  <Point X="-3.08947265625" Y="-1.24830078125" />
                  <Point X="-3.11297265625" Y="-1.228766723633" />
                  <Point X="-3.139455322266" Y="-1.213180297852" />
                  <Point X="-3.168718261719" Y="-1.201956420898" />
                  <Point X="-3.200605957031" Y="-1.195474853516" />
                  <Point X="-3.231929199219" Y="-1.194383789062" />
                  <Point X="-4.121888671875" Y="-1.311549194336" />
                  <Point X="-4.732102050781" Y="-1.391885375977" />
                  <Point X="-4.761791015625" Y="-1.275654174805" />
                  <Point X="-4.834077636719" Y="-0.992654785156" />
                  <Point X="-4.868168945312" Y="-0.754290893555" />
                  <Point X="-4.892424316406" Y="-0.584698181152" />
                  <Point X="-4.205401855469" Y="-0.400611175537" />
                  <Point X="-3.532875976562" Y="-0.220408340454" />
                  <Point X="-3.510625732422" Y="-0.211296524048" />
                  <Point X="-3.482478515625" Y="-0.195345626831" />
                  <Point X="-3.469217041016" Y="-0.186267807007" />
                  <Point X="-3.442224609375" Y="-0.164218566895" />
                  <Point X="-3.426013671875" Y="-0.147228851318" />
                  <Point X="-3.414465576172" Y="-0.126781745911" />
                  <Point X="-3.402847167969" Y="-0.098533851624" />
                  <Point X="-3.398219970703" Y="-0.084108757019" />
                  <Point X="-3.390915039062" Y="-0.052991420746" />
                  <Point X="-3.388401367188" Y="-0.031615196228" />
                  <Point X="-3.390764160156" Y="-0.010221739769" />
                  <Point X="-3.397204345703" Y="0.01810902977" />
                  <Point X="-3.401703857422" Y="0.032502555847" />
                  <Point X="-3.414187011719" Y="0.063537014008" />
                  <Point X="-3.425575683594" Y="0.084073120117" />
                  <Point X="-3.441653564453" Y="0.101188323975" />
                  <Point X="-3.466051269531" Y="0.121436859131" />
                  <Point X="-3.479216308594" Y="0.130602874756" />
                  <Point X="-3.509958251953" Y="0.14835446167" />
                  <Point X="-3.532875976562" Y="0.157848342896" />
                  <Point X="-4.344116210938" Y="0.375219482422" />
                  <Point X="-4.89181640625" Y="0.521975219727" />
                  <Point X="-4.871074707031" Y="0.662144226074" />
                  <Point X="-4.824487792969" Y="0.976975341797" />
                  <Point X="-4.755862304688" Y="1.230223999023" />
                  <Point X="-4.703551269531" Y="1.423267822266" />
                  <Point X="-4.247749511719" Y="1.363260253906" />
                  <Point X="-3.765666259766" Y="1.29979284668" />
                  <Point X="-3.744986083984" Y="1.299341674805" />
                  <Point X="-3.723424560547" Y="1.301228149414" />
                  <Point X="-3.703137695312" Y="1.305263671875" />
                  <Point X="-3.675296142578" Y="1.314041992188" />
                  <Point X="-3.641711669922" Y="1.324631225586" />
                  <Point X="-3.622779052734" Y="1.332961669922" />
                  <Point X="-3.604034667969" Y="1.343783569336" />
                  <Point X="-3.587353515625" Y="1.356014648438" />
                  <Point X="-3.573715087891" Y="1.37156628418" />
                  <Point X="-3.561300537109" Y="1.389296020508" />
                  <Point X="-3.551351318359" Y="1.407430908203" />
                  <Point X="-3.5401796875" Y="1.434401611328" />
                  <Point X="-3.526703857422" Y="1.466935180664" />
                  <Point X="-3.520915527344" Y="1.486794067383" />
                  <Point X="-3.517157226562" Y="1.508109130859" />
                  <Point X="-3.5158046875" Y="1.528749267578" />
                  <Point X="-3.518951171875" Y="1.549192871094" />
                  <Point X="-3.524552978516" Y="1.570099121094" />
                  <Point X="-3.532049804688" Y="1.589377563477" />
                  <Point X="-3.545529541016" Y="1.615271972656" />
                  <Point X="-3.561789550781" Y="1.646507080078" />
                  <Point X="-3.57328125" Y="1.663705932617" />
                  <Point X="-3.587193847656" Y="1.680286376953" />
                  <Point X="-3.602135986328" Y="1.694590209961" />
                  <Point X="-4.067465576172" Y="2.051650146484" />
                  <Point X="-4.351859863281" Y="2.269874267578" />
                  <Point X="-4.262172363281" Y="2.423531005859" />
                  <Point X="-4.081153808594" Y="2.733659423828" />
                  <Point X="-3.899368896484" Y="2.967318115234" />
                  <Point X="-3.750504882813" Y="3.158661621094" />
                  <Point X="-3.502739013672" Y="3.015614013672" />
                  <Point X="-3.206656982422" Y="2.844670898438" />
                  <Point X="-3.187724609375" Y="2.836340332031" />
                  <Point X="-3.167082519531" Y="2.829831787109" />
                  <Point X="-3.146794433594" Y="2.825796386719" />
                  <Point X="-3.108018554688" Y="2.822404052734" />
                  <Point X="-3.061245117188" Y="2.818311767578" />
                  <Point X="-3.040565185547" Y="2.818762939453" />
                  <Point X="-3.019106445312" Y="2.821588134766" />
                  <Point X="-2.999014648438" Y="2.826504638672" />
                  <Point X="-2.980463134766" Y="2.835653320313" />
                  <Point X="-2.962208740234" Y="2.847282714844" />
                  <Point X="-2.946077636719" Y="2.860229248047" />
                  <Point X="-2.918554199219" Y="2.887752441406" />
                  <Point X="-2.885354003906" Y="2.920952636719" />
                  <Point X="-2.872406982422" Y="2.937083984375" />
                  <Point X="-2.860777587891" Y="2.955338134766" />
                  <Point X="-2.85162890625" Y="2.973889892578" />
                  <Point X="-2.846712158203" Y="2.993981933594" />
                  <Point X="-2.843886962891" Y="3.015440673828" />
                  <Point X="-2.843435791016" Y="3.03612109375" />
                  <Point X="-2.846828369141" Y="3.074896972656" />
                  <Point X="-2.850920410156" Y="3.121670654297" />
                  <Point X="-2.854955810547" Y="3.141957763672" />
                  <Point X="-2.861464355469" Y="3.162600097656" />
                  <Point X="-2.869794921875" Y="3.181532958984" />
                  <Point X="-3.075996582031" Y="3.538684326172" />
                  <Point X="-3.183333007812" Y="3.724596435547" />
                  <Point X="-3.015898925781" Y="3.852966308594" />
                  <Point X="-2.700626464844" Y="4.094683349609" />
                  <Point X="-2.414322509766" Y="4.253747558594" />
                  <Point X="-2.167036621094" Y="4.391134277344" />
                  <Point X="-2.133835205078" Y="4.347865234375" />
                  <Point X="-2.04319543457" Y="4.229741210938" />
                  <Point X="-2.028892700195" Y="4.214799804688" />
                  <Point X="-2.012312866211" Y="4.200887207031" />
                  <Point X="-1.99511315918" Y="4.18939453125" />
                  <Point X="-1.951955932617" Y="4.166928222656" />
                  <Point X="-1.899896972656" Y="4.139827636719" />
                  <Point X="-1.8806171875" Y="4.132330566406" />
                  <Point X="-1.859710571289" Y="4.126729003906" />
                  <Point X="-1.839267578125" Y="4.123582519531" />
                  <Point X="-1.818628295898" Y="4.124935546875" />
                  <Point X="-1.797312988281" Y="4.128693847656" />
                  <Point X="-1.777453491211" Y="4.134481933594" />
                  <Point X="-1.732502319336" Y="4.1531015625" />
                  <Point X="-1.678279541016" Y="4.175561523437" />
                  <Point X="-1.66014465332" Y="4.185510742187" />
                  <Point X="-1.642415039062" Y="4.197925292969" />
                  <Point X="-1.626863525391" Y="4.211563964844" />
                  <Point X="-1.614632568359" Y="4.228245117188" />
                  <Point X="-1.603810791016" Y="4.246989257813" />
                  <Point X="-1.59548046875" Y="4.265921386719" />
                  <Point X="-1.580849609375" Y="4.31232421875" />
                  <Point X="-1.563201171875" Y="4.368297851562" />
                  <Point X="-1.559165649414" Y="4.388584960938" />
                  <Point X="-1.557279174805" Y="4.410146484375" />
                  <Point X="-1.55773034668" Y="4.430826660156" />
                  <Point X="-1.581173217773" Y="4.608892578125" />
                  <Point X="-1.584201904297" Y="4.631897949219" />
                  <Point X="-1.357776123047" Y="4.695379882812" />
                  <Point X="-0.94963470459" Y="4.80980859375" />
                  <Point X="-0.6025546875" Y="4.850429199219" />
                  <Point X="-0.294710784912" Y="4.886458007812" />
                  <Point X="-0.221492446899" Y="4.613203125" />
                  <Point X="-0.133903305054" Y="4.286315429688" />
                  <Point X="-0.121129844666" Y="4.258124023438" />
                  <Point X="-0.10327155304" Y="4.231397460938" />
                  <Point X="-0.0821145401" Y="4.20880859375" />
                  <Point X="-0.054819446564" Y="4.19421875" />
                  <Point X="-0.024381370544" Y="4.183886230469" />
                  <Point X="0.006155907631" Y="4.178844238281" />
                  <Point X="0.03669316864" Y="4.183886230469" />
                  <Point X="0.067131248474" Y="4.19421875" />
                  <Point X="0.094426467896" Y="4.20880859375" />
                  <Point X="0.115583503723" Y="4.231397460938" />
                  <Point X="0.133441802979" Y="4.258124023438" />
                  <Point X="0.146215255737" Y="4.286315429688" />
                  <Point X="0.251870498657" Y="4.680626464844" />
                  <Point X="0.307419372559" Y="4.8879375" />
                  <Point X="0.487668395996" Y="4.869060546875" />
                  <Point X="0.844041564941" Y="4.831738769531" />
                  <Point X="1.13119519043" Y="4.762411132812" />
                  <Point X="1.48102734375" Y="4.677950195312" />
                  <Point X="1.667126342773" Y="4.610451171875" />
                  <Point X="1.894645385742" Y="4.527928222656" />
                  <Point X="2.075380615234" Y="4.443404296875" />
                  <Point X="2.294576416016" Y="4.340893554688" />
                  <Point X="2.469201660156" Y="4.239156738281" />
                  <Point X="2.680978759766" Y="4.115774902344" />
                  <Point X="2.845649658203" Y="3.998669921875" />
                  <Point X="2.943260009766" Y="3.929254882812" />
                  <Point X="2.538118896484" Y="3.227529296875" />
                  <Point X="2.147581054688" Y="2.551097900391" />
                  <Point X="2.142075927734" Y="2.539931152344" />
                  <Point X="2.133076904297" Y="2.516056884766" />
                  <Point X="2.123345703125" Y="2.479666748047" />
                  <Point X="2.111607177734" Y="2.435770507813" />
                  <Point X="2.108619384766" Y="2.417934326172" />
                  <Point X="2.107728027344" Y="2.380952880859" />
                  <Point X="2.111522460938" Y="2.349485839844" />
                  <Point X="2.116099365234" Y="2.311528076172" />
                  <Point X="2.121442138672" Y="2.289604980469" />
                  <Point X="2.129708251953" Y="2.267516357422" />
                  <Point X="2.140071044922" Y="2.247471191406" />
                  <Point X="2.159541992188" Y="2.218776123047" />
                  <Point X="2.183028808594" Y="2.184162597656" />
                  <Point X="2.19446484375" Y="2.170328857422" />
                  <Point X="2.221598632812" Y="2.145592773438" />
                  <Point X="2.250293457031" Y="2.126121826172" />
                  <Point X="2.284907226562" Y="2.102635253906" />
                  <Point X="2.304952636719" Y="2.092271972656" />
                  <Point X="2.327040771484" Y="2.084006103516" />
                  <Point X="2.348963867188" Y="2.078663330078" />
                  <Point X="2.380431152344" Y="2.074868896484" />
                  <Point X="2.418388671875" Y="2.070291748047" />
                  <Point X="2.436467529297" Y="2.069845703125" />
                  <Point X="2.473206542969" Y="2.074171142578" />
                  <Point X="2.509596923828" Y="2.083902587891" />
                  <Point X="2.553492919922" Y="2.095640869141" />
                  <Point X="2.565283447266" Y="2.099638183594" />
                  <Point X="2.588533935547" Y="2.110145019531" />
                  <Point X="3.404485595703" Y="2.581234863281" />
                  <Point X="3.967326416016" Y="2.906191162109" />
                  <Point X="3.997652587891" Y="2.864044921875" />
                  <Point X="4.123270507813" Y="2.689464355469" />
                  <Point X="4.21507421875" Y="2.537758056641" />
                  <Point X="4.262198730469" Y="2.459884277344" />
                  <Point X="3.744963623047" Y="2.062995117188" />
                  <Point X="3.230783935547" Y="1.668451293945" />
                  <Point X="3.221424560547" Y="1.660241455078" />
                  <Point X="3.203973876953" Y="1.641627929688" />
                  <Point X="3.177783691406" Y="1.60746105957" />
                  <Point X="3.146191650391" Y="1.566246582031" />
                  <Point X="3.13660546875" Y="1.550911254883" />
                  <Point X="3.121629882812" Y="1.51708605957" />
                  <Point X="3.111874023438" Y="1.482201416016" />
                  <Point X="3.100105957031" Y="1.440121704102" />
                  <Point X="3.096652587891" Y="1.417823242188" />
                  <Point X="3.095836425781" Y="1.394252929688" />
                  <Point X="3.097739501953" Y="1.371768066406" />
                  <Point X="3.105748046875" Y="1.332954467773" />
                  <Point X="3.115408447266" Y="1.286135253906" />
                  <Point X="3.1206796875" Y="1.268978027344" />
                  <Point X="3.136282470703" Y="1.235740478516" />
                  <Point X="3.158064697266" Y="1.202632202148" />
                  <Point X="3.184340087891" Y="1.162695068359" />
                  <Point X="3.198894042969" Y="1.145449707031" />
                  <Point X="3.216137939453" Y="1.129360107422" />
                  <Point X="3.234346923828" Y="1.11603503418" />
                  <Point X="3.265912597656" Y="1.098266113281" />
                  <Point X="3.303989013672" Y="1.076832763672" />
                  <Point X="3.320521728516" Y="1.069501342773" />
                  <Point X="3.356118164062" Y="1.059438598633" />
                  <Point X="3.398796875" Y="1.053797973633" />
                  <Point X="3.450278808594" Y="1.046994018555" />
                  <Point X="3.462702392578" Y="1.046175048828" />
                  <Point X="3.488203613281" Y="1.04698449707" />
                  <Point X="4.263303222656" Y="1.149028320312" />
                  <Point X="4.776839355469" Y="1.21663659668" />
                  <Point X="4.791983398438" Y="1.15442980957" />
                  <Point X="4.845936035156" Y="0.932809448242" />
                  <Point X="4.874865722656" Y="0.746999755859" />
                  <Point X="4.890864746094" Y="0.644238525391" />
                  <Point X="4.305840820313" Y="0.487481872559" />
                  <Point X="3.716579833984" Y="0.329589935303" />
                  <Point X="3.704791259766" Y="0.325586608887" />
                  <Point X="3.681545654297" Y="0.315067993164" />
                  <Point X="3.639614990234" Y="0.290831298828" />
                  <Point X="3.589035644531" Y="0.261595428467" />
                  <Point X="3.574311035156" Y="0.251096221924" />
                  <Point X="3.547531005859" Y="0.22557661438" />
                  <Point X="3.522372558594" Y="0.193518936157" />
                  <Point X="3.492025146484" Y="0.154848968506" />
                  <Point X="3.48030078125" Y="0.13556854248" />
                  <Point X="3.470527099609" Y="0.114104927063" />
                  <Point X="3.463680908203" Y="0.092604270935" />
                  <Point X="3.455294677734" Y="0.048815013885" />
                  <Point X="3.445178710938" Y="-0.004006225586" />
                  <Point X="3.443483154297" Y="-0.021875295639" />
                  <Point X="3.445178710938" Y="-0.058553779602" />
                  <Point X="3.453564941406" Y="-0.102343032837" />
                  <Point X="3.463680908203" Y="-0.155164428711" />
                  <Point X="3.470527099609" Y="-0.176665084839" />
                  <Point X="3.48030078125" Y="-0.198128707886" />
                  <Point X="3.492025146484" Y="-0.217409118652" />
                  <Point X="3.51718359375" Y="-0.249466796875" />
                  <Point X="3.547531005859" Y="-0.288136779785" />
                  <Point X="3.559999023438" Y="-0.30123614502" />
                  <Point X="3.589035644531" Y="-0.324155578613" />
                  <Point X="3.630966308594" Y="-0.348392242432" />
                  <Point X="3.681545654297" Y="-0.377628143311" />
                  <Point X="3.692710205078" Y="-0.383139038086" />
                  <Point X="3.716579833984" Y="-0.392149932861" />
                  <Point X="4.427381347656" Y="-0.582608520508" />
                  <Point X="4.89147265625" Y="-0.706961486816" />
                  <Point X="4.885147460938" Y="-0.74891418457" />
                  <Point X="4.855022460938" Y="-0.948726074219" />
                  <Point X="4.817958984375" Y="-1.111143920898" />
                  <Point X="4.801173828125" Y="-1.184698974609" />
                  <Point X="4.107759277344" Y="-1.093409301758" />
                  <Point X="3.424382080078" Y="-1.003441040039" />
                  <Point X="3.408035644531" Y="-1.002710327148" />
                  <Point X="3.374658691406" Y="-1.005508728027" />
                  <Point X="3.292363525391" Y="-1.023395996094" />
                  <Point X="3.193094482422" Y="-1.044972412109" />
                  <Point X="3.163974121094" Y="-1.056596923828" />
                  <Point X="3.136147460938" Y="-1.073489257812" />
                  <Point X="3.112397460938" Y="-1.093960205078" />
                  <Point X="3.062655273438" Y="-1.153784667969" />
                  <Point X="3.002653320312" Y="-1.225948364258" />
                  <Point X="2.987932617188" Y="-1.250330688477" />
                  <Point X="2.976589355469" Y="-1.277715942383" />
                  <Point X="2.969757568359" Y="-1.305365356445" />
                  <Point X="2.962628173828" Y="-1.382840454102" />
                  <Point X="2.954028564453" Y="-1.476295532227" />
                  <Point X="2.956347412109" Y="-1.507564819336" />
                  <Point X="2.964079101562" Y="-1.539185791016" />
                  <Point X="2.976450439453" Y="-1.567996582031" />
                  <Point X="3.021993652344" Y="-1.638836181641" />
                  <Point X="3.076930664062" Y="-1.724287109375" />
                  <Point X="3.086932373047" Y="-1.73723815918" />
                  <Point X="3.110628417969" Y="-1.760909179688" />
                  <Point X="3.770256591797" Y="-2.267059814453" />
                  <Point X="4.213122070312" Y="-2.606882568359" />
                  <Point X="4.209729492188" Y="-2.612372314453" />
                  <Point X="4.124807128906" Y="-2.749790527344" />
                  <Point X="4.048159667969" Y="-2.8586953125" />
                  <Point X="4.028981445312" Y="-2.885945068359" />
                  <Point X="3.409593994141" Y="-2.528341552734" />
                  <Point X="2.800954589844" Y="-2.176943115234" />
                  <Point X="2.786131591797" Y="-2.170012451172" />
                  <Point X="2.754224365234" Y="-2.159825195312" />
                  <Point X="2.656280029297" Y="-2.14213671875" />
                  <Point X="2.538134033203" Y="-2.120799560547" />
                  <Point X="2.506781982422" Y="-2.120395263672" />
                  <Point X="2.474609375" Y="-2.125353027344" />
                  <Point X="2.444833496094" Y="-2.135176757812" />
                  <Point X="2.363465820312" Y="-2.178" />
                  <Point X="2.265315429688" Y="-2.229655761719" />
                  <Point X="2.242384765625" Y="-2.246548828125" />
                  <Point X="2.221424804688" Y="-2.267508789062" />
                  <Point X="2.204531738281" Y="-2.290439453125" />
                  <Point X="2.161708496094" Y="-2.371807128906" />
                  <Point X="2.110052734375" Y="-2.469957519531" />
                  <Point X="2.100229003906" Y="-2.499733398438" />
                  <Point X="2.095271240234" Y="-2.531906005859" />
                  <Point X="2.095675537109" Y="-2.563258056641" />
                  <Point X="2.113364013672" Y="-2.661202148438" />
                  <Point X="2.134701171875" Y="-2.779348388672" />
                  <Point X="2.138985595703" Y="-2.795141845703" />
                  <Point X="2.151819091797" Y="-2.826078613281" />
                  <Point X="2.575696533203" Y="-3.560255615234" />
                  <Point X="2.861283203125" Y="-4.054906494141" />
                  <Point X="2.781847412109" Y="-4.111645507812" />
                  <Point X="2.701765136719" Y="-4.163481445312" />
                  <Point X="2.222994384766" Y="-3.539535400391" />
                  <Point X="1.758546020508" Y="-2.934255126953" />
                  <Point X="1.747503540039" Y="-2.922179443359" />
                  <Point X="1.721924072266" Y="-2.900557373047" />
                  <Point X="1.625324584961" Y="-2.838452880859" />
                  <Point X="1.50880078125" Y="-2.763538574219" />
                  <Point X="1.479989746094" Y="-2.751167236328" />
                  <Point X="1.448368774414" Y="-2.743435546875" />
                  <Point X="1.417099609375" Y="-2.741116699219" />
                  <Point X="1.311451538086" Y="-2.750838623047" />
                  <Point X="1.184012695312" Y="-2.762565673828" />
                  <Point X="1.15636315918" Y="-2.769397460938" />
                  <Point X="1.128978027344" Y="-2.780740722656" />
                  <Point X="1.104595825195" Y="-2.795461181641" />
                  <Point X="1.02301739502" Y="-2.863291015625" />
                  <Point X="0.924612121582" Y="-2.945111572266" />
                  <Point X="0.904141052246" Y="-2.968861816406" />
                  <Point X="0.887248779297" Y="-2.996688476562" />
                  <Point X="0.875624328613" Y="-3.025808837891" />
                  <Point X="0.851232666016" Y="-3.138029541016" />
                  <Point X="0.821810058594" Y="-3.273396728516" />
                  <Point X="0.819724487305" Y="-3.289627441406" />
                  <Point X="0.819742248535" Y="-3.323120117188" />
                  <Point X="0.939865844727" Y="-4.235549316406" />
                  <Point X="1.022065246582" Y="-4.859915039062" />
                  <Point X="0.9756796875" Y="-4.870082519531" />
                  <Point X="0.929315612793" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058440551758" Y="-4.752633789062" />
                  <Point X="-1.141246582031" Y="-4.731328613281" />
                  <Point X="-1.120775756836" Y="-4.575838378906" />
                  <Point X="-1.120077392578" Y="-4.568099609375" />
                  <Point X="-1.119451660156" Y="-4.541033691406" />
                  <Point X="-1.121759277344" Y="-4.509328125" />
                  <Point X="-1.123334106445" Y="-4.497690429688" />
                  <Point X="-1.150768554688" Y="-4.359768554688" />
                  <Point X="-1.183861572266" Y="-4.193398925781" />
                  <Point X="-1.18812512207" Y="-4.17846875" />
                  <Point X="-1.19902734375" Y="-4.149502441406" />
                  <Point X="-1.205666137695" Y="-4.135466308594" />
                  <Point X="-1.221738037109" Y="-4.10762890625" />
                  <Point X="-1.23057409668" Y="-4.094861816406" />
                  <Point X="-1.250208496094" Y="-4.070937011719" />
                  <Point X="-1.261006835938" Y="-4.059779296875" />
                  <Point X="-1.366733886719" Y="-3.967059082031" />
                  <Point X="-1.494267578125" Y="-3.855214355469" />
                  <Point X="-1.506739135742" Y="-3.84596484375" />
                  <Point X="-1.533022216797" Y="-3.82962109375" />
                  <Point X="-1.546833496094" Y="-3.822526855469" />
                  <Point X="-1.576530761719" Y="-3.810225830078" />
                  <Point X="-1.591313354492" Y="-3.805476074219" />
                  <Point X="-1.621455078125" Y="-3.798447998047" />
                  <Point X="-1.636814208984" Y="-3.796169677734" />
                  <Point X="-1.777137329102" Y="-3.786972412109" />
                  <Point X="-1.946403198242" Y="-3.775878173828" />
                  <Point X="-1.961928222656" Y="-3.776132324219" />
                  <Point X="-1.992729614258" Y="-3.779166015625" />
                  <Point X="-2.008006103516" Y="-3.781945556641" />
                  <Point X="-2.03905480957" Y="-3.790265136719" />
                  <Point X="-2.053674560547" Y="-3.795496337891" />
                  <Point X="-2.081865478516" Y="-3.808269775391" />
                  <Point X="-2.095437011719" Y="-3.815812011719" />
                  <Point X="-2.212361816406" Y="-3.893938720703" />
                  <Point X="-2.353403564453" Y="-3.988180175781" />
                  <Point X="-2.359681640625" Y="-3.992757080078" />
                  <Point X="-2.380442382812" Y="-4.010131347656" />
                  <Point X="-2.402759277344" Y="-4.032771728516" />
                  <Point X="-2.410470947266" Y="-4.041629638672" />
                  <Point X="-2.503202636719" Y="-4.162479492188" />
                  <Point X="-2.517759521484" Y="-4.153466308594" />
                  <Point X="-2.747583740234" Y="-4.011164794922" />
                  <Point X="-2.923487060547" Y="-3.875725097656" />
                  <Point X="-2.980862792969" Y="-3.831547607422" />
                  <Point X="-2.678908447266" Y="-3.308547363281" />
                  <Point X="-2.341488769531" Y="-2.724119628906" />
                  <Point X="-2.334850097656" Y="-2.710083007812" />
                  <Point X="-2.323947998047" Y="-2.681116455078" />
                  <Point X="-2.319684570312" Y="-2.666186523438" />
                  <Point X="-2.313413574219" Y="-2.634659912109" />
                  <Point X="-2.311638916016" Y="-2.619234863281" />
                  <Point X="-2.310626220703" Y="-2.58830078125" />
                  <Point X="-2.314666503906" Y="-2.557614013672" />
                  <Point X="-2.323651367188" Y="-2.527996337891" />
                  <Point X="-2.329357910156" Y="-2.513556396484" />
                  <Point X="-2.343575195312" Y="-2.484727294922" />
                  <Point X="-2.351557617188" Y="-2.471409667969" />
                  <Point X="-2.369584960938" Y="-2.446252929688" />
                  <Point X="-2.379629882812" Y="-2.434413818359" />
                  <Point X="-2.396978515625" Y="-2.417065185547" />
                  <Point X="-2.408818115234" Y="-2.407019775391" />
                  <Point X="-2.4339765625" Y="-2.388991455078" />
                  <Point X="-2.447295410156" Y="-2.381008544922" />
                  <Point X="-2.476124511719" Y="-2.366791992188" />
                  <Point X="-2.490564453125" Y="-2.3610859375" />
                  <Point X="-2.520181640625" Y="-2.352101806641" />
                  <Point X="-2.550867431641" Y="-2.348062011719" />
                  <Point X="-2.58180078125" Y="-2.349074951172" />
                  <Point X="-2.597225585938" Y="-2.350849609375" />
                  <Point X="-2.628751464844" Y="-2.357120605469" />
                  <Point X="-2.643681396484" Y="-2.361384033203" />
                  <Point X="-2.672647460938" Y="-2.372286132812" />
                  <Point X="-2.68668359375" Y="-2.378924804688" />
                  <Point X="-3.391655029297" Y="-2.785940185547" />
                  <Point X="-3.793089355469" Y="-3.017708251953" />
                  <Point X="-3.822442382812" Y="-2.979144287109" />
                  <Point X="-4.004015869141" Y="-2.74059375" />
                  <Point X="-4.130121582031" Y="-2.529133789062" />
                  <Point X="-4.181265136719" Y="-2.443373535156" />
                  <Point X="-3.638497070312" Y="-2.026892456055" />
                  <Point X="-3.048122314453" Y="-1.573882202148" />
                  <Point X="-3.036481689453" Y="-1.563309570312" />
                  <Point X="-3.015104003906" Y="-1.540388916016" />
                  <Point X="-3.005366943359" Y="-1.528040893555" />
                  <Point X="-2.987402099609" Y="-1.500909545898" />
                  <Point X="-2.979835693359" Y="-1.48712487793" />
                  <Point X="-2.967080078125" Y="-1.45849609375" />
                  <Point X="-2.961890869141" Y="-1.443651977539" />
                  <Point X="-2.954186279297" Y="-1.413904541016" />
                  <Point X="-2.951552734375" Y="-1.398802978516" />
                  <Point X="-2.948748535156" Y="-1.368373535156" />
                  <Point X="-2.948577880859" Y="-1.353045776367" />
                  <Point X="-2.950786865234" Y="-1.321375366211" />
                  <Point X="-2.953083740234" Y="-1.306219604492" />
                  <Point X="-2.960084960938" Y="-1.276474731445" />
                  <Point X="-2.971779052734" Y="-1.248242919922" />
                  <Point X="-2.987861328125" Y="-1.222259521484" />
                  <Point X="-2.996953857422" Y="-1.209918823242" />
                  <Point X="-3.017786376953" Y="-1.185962402344" />
                  <Point X="-3.028745605469" Y="-1.175244384766" />
                  <Point X="-3.052245605469" Y="-1.155710449219" />
                  <Point X="-3.064786376953" Y="-1.146894287109" />
                  <Point X="-3.091269042969" Y="-1.131307739258" />
                  <Point X="-3.105434326172" Y="-1.124480957031" />
                  <Point X="-3.134697265625" Y="-1.113257080078" />
                  <Point X="-3.149795410156" Y="-1.108860107422" />
                  <Point X="-3.181683105469" Y="-1.102378540039" />
                  <Point X="-3.197298828125" Y="-1.100532470703" />
                  <Point X="-3.228622070312" Y="-1.09944140625" />
                  <Point X="-3.244329101562" Y="-1.100196533203" />
                  <Point X="-4.134288574219" Y="-1.217361938477" />
                  <Point X="-4.660920898438" Y="-1.286694458008" />
                  <Point X="-4.66974609375" Y="-1.252143066406" />
                  <Point X="-4.740762207031" Y="-0.974118347168" />
                  <Point X="-4.774125976562" Y="-0.740840637207" />
                  <Point X="-4.786452636719" Y="-0.65465435791" />
                  <Point X="-4.180813964844" Y="-0.492374206543" />
                  <Point X="-3.508288085938" Y="-0.312171295166" />
                  <Point X="-3.496873779297" Y="-0.308322265625" />
                  <Point X="-3.474623535156" Y="-0.299210327148" />
                  <Point X="-3.463787841797" Y="-0.293947601318" />
                  <Point X="-3.435640625" Y="-0.277996643066" />
                  <Point X="-3.428816650391" Y="-0.273738311768" />
                  <Point X="-3.409117431641" Y="-0.259841186523" />
                  <Point X="-3.382125" Y="-0.237791931152" />
                  <Point X="-3.373492675781" Y="-0.229800018311" />
                  <Point X="-3.357281738281" Y="-0.212810195923" />
                  <Point X="-3.343294677734" Y="-0.19394682312" />
                  <Point X="-3.331746582031" Y="-0.173499740601" />
                  <Point X="-3.326606933594" Y="-0.162918228149" />
                  <Point X="-3.314988525391" Y="-0.13467036438" />
                  <Point X="-3.312387207031" Y="-0.12755103302" />
                  <Point X="-3.305734130859" Y="-0.105820129395" />
                  <Point X="-3.298429199219" Y="-0.074702804565" />
                  <Point X="-3.296565185547" Y="-0.064086227417" />
                  <Point X="-3.294051513672" Y="-0.042709941864" />
                  <Point X="-3.293975585938" Y="-0.02118637085" />
                  <Point X="-3.296338378906" Y="0.000207153708" />
                  <Point X="-3.298127441406" Y="0.010836515427" />
                  <Point X="-3.304567626953" Y="0.039167312622" />
                  <Point X="-3.306531494141" Y="0.046453994751" />
                  <Point X="-3.313566650391" Y="0.067954376221" />
                  <Point X="-3.326049804688" Y="0.098988922119" />
                  <Point X="-3.331107177734" Y="0.109610404968" />
                  <Point X="-3.342495849609" Y="0.130146530151" />
                  <Point X="-3.356334960938" Y="0.149117202759" />
                  <Point X="-3.372412841797" Y="0.166232467651" />
                  <Point X="-3.380982666016" Y="0.174291397095" />
                  <Point X="-3.405380371094" Y="0.194539932251" />
                  <Point X="-3.411769287109" Y="0.199401382446" />
                  <Point X="-3.4317109375" Y="0.212872116089" />
                  <Point X="-3.462452880859" Y="0.230623794556" />
                  <Point X="-3.473599853516" Y="0.236121643066" />
                  <Point X="-3.496517578125" Y="0.245615524292" />
                  <Point X="-3.508288085938" Y="0.249611251831" />
                  <Point X="-4.319528320312" Y="0.46698236084" />
                  <Point X="-4.785445800781" Y="0.591824584961" />
                  <Point X="-4.777098144531" Y="0.648237976074" />
                  <Point X="-4.731330566406" Y="0.957532775879" />
                  <Point X="-4.664169433594" Y="1.205376953125" />
                  <Point X="-4.633586425781" Y="1.318237060547" />
                  <Point X="-4.260149414062" Y="1.269072998047" />
                  <Point X="-3.778066162109" Y="1.20560546875" />
                  <Point X="-3.76773828125" Y="1.204815429688" />
                  <Point X="-3.747058105469" Y="1.204364257812" />
                  <Point X="-3.736705810547" Y="1.20470324707" />
                  <Point X="-3.715144287109" Y="1.20658972168" />
                  <Point X="-3.704890136719" Y="1.208053588867" />
                  <Point X="-3.684603271484" Y="1.212089233398" />
                  <Point X="-3.674570800781" Y="1.214660522461" />
                  <Point X="-3.646729248047" Y="1.223438842773" />
                  <Point X="-3.613144775391" Y="1.234028076172" />
                  <Point X="-3.603451171875" Y="1.237676513672" />
                  <Point X="-3.584518554688" Y="1.246007080078" />
                  <Point X="-3.575279541016" Y="1.250688964844" />
                  <Point X="-3.55653515625" Y="1.261510864258" />
                  <Point X="-3.547860351562" Y="1.267171264648" />
                  <Point X="-3.531179199219" Y="1.27940234375" />
                  <Point X="-3.515928710938" Y="1.293376831055" />
                  <Point X="-3.502290283203" Y="1.308928466797" />
                  <Point X="-3.495895751953" Y="1.317076416016" />
                  <Point X="-3.483481201172" Y="1.334806152344" />
                  <Point X="-3.47801171875" Y="1.343601806641" />
                  <Point X="-3.4680625" Y="1.361736694336" />
                  <Point X="-3.463582763672" Y="1.371075927734" />
                  <Point X="-3.452411132812" Y="1.398046630859" />
                  <Point X="-3.438935302734" Y="1.430580200195" />
                  <Point X="-3.435499023438" Y="1.44035144043" />
                  <Point X="-3.429710693359" Y="1.460210449219" />
                  <Point X="-3.427358642578" Y="1.470297973633" />
                  <Point X="-3.423600341797" Y="1.491613037109" />
                  <Point X="-3.422360595703" Y="1.501896972656" />
                  <Point X="-3.421008056641" Y="1.522537231445" />
                  <Point X="-3.421910400391" Y="1.543200561523" />
                  <Point X="-3.425056884766" Y="1.563644165039" />
                  <Point X="-3.427188232422" Y="1.573780639648" />
                  <Point X="-3.432790039062" Y="1.594686889648" />
                  <Point X="-3.436011962891" Y="1.604530273438" />
                  <Point X="-3.443508789062" Y="1.62380859375" />
                  <Point X="-3.447783691406" Y="1.633243530273" />
                  <Point X="-3.461263427734" Y="1.659137939453" />
                  <Point X="-3.4775234375" Y="1.690373046875" />
                  <Point X="-3.482799560547" Y="1.699285644531" />
                  <Point X="-3.494291259766" Y="1.71648449707" />
                  <Point X="-3.500506835938" Y="1.724770629883" />
                  <Point X="-3.514419433594" Y="1.741351074219" />
                  <Point X="-3.521500488281" Y="1.748911254883" />
                  <Point X="-3.536442626953" Y="1.763215087891" />
                  <Point X="-3.544303710938" Y="1.769958862305" />
                  <Point X="-4.009633300781" Y="2.127018798828" />
                  <Point X="-4.227614257812" Y="2.294282226563" />
                  <Point X="-4.180125976563" Y="2.375641601562" />
                  <Point X="-4.002293212891" Y="2.680312011719" />
                  <Point X="-3.824388183594" Y="2.908983642578" />
                  <Point X="-3.726338623047" Y="3.035012695312" />
                  <Point X="-3.550239013672" Y="2.933341552734" />
                  <Point X="-3.254156982422" Y="2.7623984375" />
                  <Point X="-3.244918457031" Y="2.757716552734" />
                  <Point X="-3.225986083984" Y="2.749385986328" />
                  <Point X="-3.216292236328" Y="2.745737304688" />
                  <Point X="-3.195650146484" Y="2.739228759766" />
                  <Point X="-3.185615478516" Y="2.736656982422" />
                  <Point X="-3.165327392578" Y="2.732621582031" />
                  <Point X="-3.155073974609" Y="2.731157958984" />
                  <Point X="-3.116298095703" Y="2.727765625" />
                  <Point X="-3.069524658203" Y="2.723673339844" />
                  <Point X="-3.059173095703" Y="2.723334472656" />
                  <Point X="-3.038493164062" Y="2.723785644531" />
                  <Point X="-3.028164794922" Y="2.724575683594" />
                  <Point X="-3.006706054688" Y="2.727400878906" />
                  <Point X="-2.996525878906" Y="2.729310791016" />
                  <Point X="-2.976434082031" Y="2.734227294922" />
                  <Point X="-2.956996826172" Y="2.741302001953" />
                  <Point X="-2.9384453125" Y="2.750450683594" />
                  <Point X="-2.929419433594" Y="2.75553125" />
                  <Point X="-2.911165039062" Y="2.767160644531" />
                  <Point X="-2.90274609375" Y="2.773193603516" />
                  <Point X="-2.886614990234" Y="2.786140136719" />
                  <Point X="-2.878902832031" Y="2.793053710938" />
                  <Point X="-2.851379394531" Y="2.820576904297" />
                  <Point X="-2.818179199219" Y="2.853777099609" />
                  <Point X="-2.811265625" Y="2.861489257812" />
                  <Point X="-2.798318603516" Y="2.877620605469" />
                  <Point X="-2.79228515625" Y="2.886039794922" />
                  <Point X="-2.780655761719" Y="2.904293945312" />
                  <Point X="-2.775574707031" Y="2.913320800781" />
                  <Point X="-2.766426025391" Y="2.931872558594" />
                  <Point X="-2.7593515625" Y="2.95130859375" />
                  <Point X="-2.754434814453" Y="2.971400634766" />
                  <Point X="-2.752524902344" Y="2.981581542969" />
                  <Point X="-2.749699707031" Y="3.003040283203" />
                  <Point X="-2.748909667969" Y="3.013368652344" />
                  <Point X="-2.748458496094" Y="3.034049072266" />
                  <Point X="-2.748797363281" Y="3.044401123047" />
                  <Point X="-2.752189941406" Y="3.083177001953" />
                  <Point X="-2.756281982422" Y="3.129950683594" />
                  <Point X="-2.757745849609" Y="3.140204345703" />
                  <Point X="-2.76178125" Y="3.160491455078" />
                  <Point X="-2.764352783203" Y="3.170524902344" />
                  <Point X="-2.770861328125" Y="3.191167236328" />
                  <Point X="-2.774509521484" Y="3.200860595703" />
                  <Point X="-2.782840087891" Y="3.219793457031" />
                  <Point X="-2.787522460938" Y="3.229032958984" />
                  <Point X="-2.993724121094" Y="3.586184326172" />
                  <Point X="-3.059387207031" Y="3.699916259766" />
                  <Point X="-2.958096923828" Y="3.777574462891" />
                  <Point X="-2.648374755859" Y="4.015036132812" />
                  <Point X="-2.368185058594" Y="4.170703613281" />
                  <Point X="-2.192524902344" Y="4.268296875" />
                  <Point X="-2.118563964844" Y="4.171909179688" />
                  <Point X="-2.111821289062" Y="4.164048828125" />
                  <Point X="-2.097518554688" Y="4.149107421875" />
                  <Point X="-2.089958740234" Y="4.142026367188" />
                  <Point X="-2.07337890625" Y="4.128113769531" />
                  <Point X="-2.065092773438" Y="4.121897949219" />
                  <Point X="-2.047892944336" Y="4.110405273438" />
                  <Point X="-2.038979370117" Y="4.105128417969" />
                  <Point X="-1.995822143555" Y="4.082662353516" />
                  <Point X="-1.943763061523" Y="4.055561767578" />
                  <Point X="-1.934326904297" Y="4.051286132812" />
                  <Point X="-1.915047119141" Y="4.0437890625" />
                  <Point X="-1.905203613281" Y="4.040567138672" />
                  <Point X="-1.88429699707" Y="4.034965576172" />
                  <Point X="-1.874162475586" Y="4.032834716797" />
                  <Point X="-1.853719360352" Y="4.029688232422" />
                  <Point X="-1.833053222656" Y="4.028785888672" />
                  <Point X="-1.812413818359" Y="4.030138916016" />
                  <Point X="-1.802132446289" Y="4.031378662109" />
                  <Point X="-1.780817138672" Y="4.035136962891" />
                  <Point X="-1.770731079102" Y="4.037488525391" />
                  <Point X="-1.750871582031" Y="4.043276611328" />
                  <Point X="-1.741098144531" Y="4.046713623047" />
                  <Point X="-1.696146972656" Y="4.065333251953" />
                  <Point X="-1.641924316406" Y="4.087793212891" />
                  <Point X="-1.632585449219" Y="4.092272705078" />
                  <Point X="-1.614450439453" Y="4.102221679688" />
                  <Point X="-1.605654418945" Y="4.10769140625" />
                  <Point X="-1.587924804688" Y="4.120105957031" />
                  <Point X="-1.579776245117" Y="4.126501464844" />
                  <Point X="-1.564224853516" Y="4.140140136719" />
                  <Point X="-1.550250976562" Y="4.155390136719" />
                  <Point X="-1.538020019531" Y="4.172071289062" />
                  <Point X="-1.532359863281" Y="4.180745605469" />
                  <Point X="-1.521538085938" Y="4.199489746094" />
                  <Point X="-1.516856201172" Y="4.208728515625" />
                  <Point X="-1.508525878906" Y="4.227660644531" />
                  <Point X="-1.504877441406" Y="4.237354003906" />
                  <Point X="-1.490246582031" Y="4.283756835938" />
                  <Point X="-1.472598144531" Y="4.33973046875" />
                  <Point X="-1.470026733398" Y="4.349763671875" />
                  <Point X="-1.465991088867" Y="4.37005078125" />
                  <Point X="-1.46452722168" Y="4.3803046875" />
                  <Point X="-1.46264074707" Y="4.401866210938" />
                  <Point X="-1.462301757812" Y="4.41221875" />
                  <Point X="-1.462752929688" Y="4.432898925781" />
                  <Point X="-1.46354309082" Y="4.4432265625" />
                  <Point X="-1.479266113281" Y="4.562654785156" />
                  <Point X="-1.332130249023" Y="4.603906738281" />
                  <Point X="-0.931175598145" Y="4.716320800781" />
                  <Point X="-0.591511779785" Y="4.756073242188" />
                  <Point X="-0.365221923828" Y="4.782557128906" />
                  <Point X="-0.313255371094" Y="4.588615234375" />
                  <Point X="-0.225666244507" Y="4.261727539062" />
                  <Point X="-0.220435317993" Y="4.247107910156" />
                  <Point X="-0.207661773682" Y="4.218916503906" />
                  <Point X="-0.200119155884" Y="4.205344726562" />
                  <Point X="-0.182260925293" Y="4.178618164062" />
                  <Point X="-0.172608306885" Y="4.166456054688" />
                  <Point X="-0.151451248169" Y="4.1438671875" />
                  <Point X="-0.126898002625" Y="4.125026367188" />
                  <Point X="-0.099602806091" Y="4.110436523438" />
                  <Point X="-0.085356712341" Y="4.104260742188" />
                  <Point X="-0.054918586731" Y="4.093927978516" />
                  <Point X="-0.039857292175" Y="4.090155273438" />
                  <Point X="-0.009319890976" Y="4.08511328125" />
                  <Point X="0.021631721497" Y="4.08511328125" />
                  <Point X="0.052168972015" Y="4.090155273438" />
                  <Point X="0.067230415344" Y="4.093927978516" />
                  <Point X="0.097668540955" Y="4.104260742188" />
                  <Point X="0.111914489746" Y="4.110436523438" />
                  <Point X="0.139209686279" Y="4.125026367188" />
                  <Point X="0.16376322937" Y="4.143866699219" />
                  <Point X="0.184920150757" Y="4.166455566406" />
                  <Point X="0.194572906494" Y="4.178617675781" />
                  <Point X="0.212431137085" Y="4.205344238281" />
                  <Point X="0.219973754883" Y="4.218916503906" />
                  <Point X="0.232747146606" Y="4.247107910156" />
                  <Point X="0.237978225708" Y="4.261727539062" />
                  <Point X="0.343633514404" Y="4.656038574219" />
                  <Point X="0.378190307617" Y="4.785006347656" />
                  <Point X="0.477773406982" Y="4.774577148438" />
                  <Point X="0.827877075195" Y="4.737912109375" />
                  <Point X="1.108899902344" Y="4.670064453125" />
                  <Point X="1.453598999023" Y="4.586842773438" />
                  <Point X="1.63473425293" Y="4.521144042969" />
                  <Point X="1.858256347656" Y="4.440070800781" />
                  <Point X="2.035135742188" Y="4.357350097656" />
                  <Point X="2.250453125" Y="4.256653320312" />
                  <Point X="2.421378662109" Y="4.157071777344" />
                  <Point X="2.62943359375" Y="4.035858154297" />
                  <Point X="2.790593017578" Y="3.921250488281" />
                  <Point X="2.817779785156" Y="3.901916748047" />
                  <Point X="2.455846435547" Y="3.275029296875" />
                  <Point X="2.06530859375" Y="2.598597900391" />
                  <Point X="2.062373046875" Y="2.593104980469" />
                  <Point X="2.053181396484" Y="2.573438476562" />
                  <Point X="2.044182373047" Y="2.549564208984" />
                  <Point X="2.041301513672" Y="2.540598876953" />
                  <Point X="2.03157043457" Y="2.504208740234" />
                  <Point X="2.01983190918" Y="2.4603125" />
                  <Point X="2.017912597656" Y="2.451465576172" />
                  <Point X="2.013646972656" Y="2.420223388672" />
                  <Point X="2.012755615234" Y="2.383241943359" />
                  <Point X="2.013411254883" Y="2.369579833984" />
                  <Point X="2.017205688477" Y="2.338112792969" />
                  <Point X="2.021782592773" Y="2.300155029297" />
                  <Point X="2.02380078125" Y="2.289034423828" />
                  <Point X="2.029143676758" Y="2.267111328125" />
                  <Point X="2.032468261719" Y="2.256308837891" />
                  <Point X="2.040734375" Y="2.234220214844" />
                  <Point X="2.045318359375" Y="2.223889160156" />
                  <Point X="2.055681152344" Y="2.203843994141" />
                  <Point X="2.061459960938" Y="2.194129882813" />
                  <Point X="2.080930908203" Y="2.165434814453" />
                  <Point X="2.104417724609" Y="2.130821289063" />
                  <Point X="2.10980859375" Y="2.123633300781" />
                  <Point X="2.130463134766" Y="2.100123535156" />
                  <Point X="2.157596923828" Y="2.075387451172" />
                  <Point X="2.168257080078" Y="2.066981933594" />
                  <Point X="2.196951904297" Y="2.047511108398" />
                  <Point X="2.231565673828" Y="2.024024414062" />
                  <Point X="2.241278808594" Y="2.01824597168" />
                  <Point X="2.26132421875" Y="2.00788269043" />
                  <Point X="2.271656494141" Y="2.003297973633" />
                  <Point X="2.293744628906" Y="1.995032104492" />
                  <Point X="2.304547119141" Y="1.991707519531" />
                  <Point X="2.326470214844" Y="1.986364746094" />
                  <Point X="2.337590820312" Y="1.984346557617" />
                  <Point X="2.369058105469" Y="1.980552124023" />
                  <Point X="2.407015625" Y="1.975974975586" />
                  <Point X="2.416045410156" Y="1.975320800781" />
                  <Point X="2.447575439453" Y="1.975497314453" />
                  <Point X="2.484314453125" Y="1.979822631836" />
                  <Point X="2.497748779297" Y="1.982395996094" />
                  <Point X="2.534139160156" Y="1.992127441406" />
                  <Point X="2.57803515625" Y="2.003865722656" />
                  <Point X="2.583995361328" Y="2.005670776367" />
                  <Point X="2.604404541016" Y="2.013067260742" />
                  <Point X="2.627655029297" Y="2.02357409668" />
                  <Point X="2.636033935547" Y="2.027872558594" />
                  <Point X="3.451985595703" Y="2.498962402344" />
                  <Point X="3.940405029297" Y="2.780951660156" />
                  <Point X="4.043949707031" Y="2.637047607422" />
                  <Point X="4.133797363281" Y="2.488573974609" />
                  <Point X="4.136884765625" Y="2.483472167969" />
                  <Point X="3.687131103516" Y="2.138363525391" />
                  <Point X="3.172951416016" Y="1.743819702148" />
                  <Point X="3.168137939453" Y="1.739868896484" />
                  <Point X="3.152119384766" Y="1.725216918945" />
                  <Point X="3.134668701172" Y="1.706603393555" />
                  <Point X="3.128576660156" Y="1.699422729492" />
                  <Point X="3.102386474609" Y="1.665255859375" />
                  <Point X="3.070794433594" Y="1.624041381836" />
                  <Point X="3.065635498047" Y="1.616602539062" />
                  <Point X="3.04973828125" Y="1.589370361328" />
                  <Point X="3.034762695312" Y="1.555545166016" />
                  <Point X="3.030140136719" Y="1.542672119141" />
                  <Point X="3.020384277344" Y="1.507787353516" />
                  <Point X="3.008616210938" Y="1.465707763672" />
                  <Point X="3.006225097656" Y="1.454661010742" />
                  <Point X="3.002771728516" Y="1.432362670898" />
                  <Point X="3.001709472656" Y="1.421110839844" />
                  <Point X="3.000893310547" Y="1.397540527344" />
                  <Point X="3.001174804688" Y="1.386240966797" />
                  <Point X="3.003077880859" Y="1.363756103516" />
                  <Point X="3.004699462891" Y="1.352570800781" />
                  <Point X="3.012708007812" Y="1.313757202148" />
                  <Point X="3.022368408203" Y="1.266937988281" />
                  <Point X="3.02459765625" Y="1.258235473633" />
                  <Point X="3.03468359375" Y="1.228608642578" />
                  <Point X="3.050286376953" Y="1.19537109375" />
                  <Point X="3.056918457031" Y="1.183526123047" />
                  <Point X="3.078700683594" Y="1.15041784668" />
                  <Point X="3.104976074219" Y="1.110480712891" />
                  <Point X="3.111739013672" Y="1.101424560547" />
                  <Point X="3.12629296875" Y="1.084179321289" />
                  <Point X="3.134083984375" Y="1.075989990234" />
                  <Point X="3.151327880859" Y="1.059900390625" />
                  <Point X="3.160035644531" Y="1.05269519043" />
                  <Point X="3.178244628906" Y="1.039370117188" />
                  <Point X="3.187745849609" Y="1.033250244141" />
                  <Point X="3.219311523438" Y="1.015481323242" />
                  <Point X="3.257387939453" Y="0.994047973633" />
                  <Point X="3.265478027344" Y="0.989988586426" />
                  <Point X="3.294678955078" Y="0.97808392334" />
                  <Point X="3.330275390625" Y="0.968021118164" />
                  <Point X="3.343670654297" Y="0.965257629395" />
                  <Point X="3.386349365234" Y="0.959616943359" />
                  <Point X="3.437831298828" Y="0.952812988281" />
                  <Point X="3.444030029297" Y="0.952199768066" />
                  <Point X="3.465716308594" Y="0.951222839355" />
                  <Point X="3.491217529297" Y="0.952032409668" />
                  <Point X="3.500603515625" Y="0.952797241211" />
                  <Point X="4.275703125" Y="1.054841064453" />
                  <Point X="4.704703613281" Y="1.111320068359" />
                  <Point X="4.75268359375" Y="0.914233703613" />
                  <Point X="4.780996582031" Y="0.73238482666" />
                  <Point X="4.78387109375" Y="0.713920898438" />
                  <Point X="4.281252929687" Y="0.579244812012" />
                  <Point X="3.691991943359" Y="0.421352905273" />
                  <Point X="3.686031738281" Y="0.419544464111" />
                  <Point X="3.665626708984" Y="0.412138000488" />
                  <Point X="3.642381103516" Y="0.401619354248" />
                  <Point X="3.634004394531" Y="0.397316558838" />
                  <Point X="3.592073730469" Y="0.37307989502" />
                  <Point X="3.541494384766" Y="0.343843963623" />
                  <Point X="3.533881835938" Y="0.338945800781" />
                  <Point X="3.508773925781" Y="0.319870361328" />
                  <Point X="3.481993896484" Y="0.294350769043" />
                  <Point X="3.472797119141" Y="0.284226867676" />
                  <Point X="3.447638671875" Y="0.252169052124" />
                  <Point X="3.417291259766" Y="0.213499160767" />
                  <Point X="3.410854736328" Y="0.204208450317" />
                  <Point X="3.399130371094" Y="0.184928039551" />
                  <Point X="3.393842529297" Y="0.174938201904" />
                  <Point X="3.384068847656" Y="0.153474533081" />
                  <Point X="3.380005371094" Y="0.142928695679" />
                  <Point X="3.373159179688" Y="0.121428009033" />
                  <Point X="3.370376464844" Y="0.110473312378" />
                  <Point X="3.361990234375" Y="0.06668409729" />
                  <Point X="3.351874267578" Y="0.013862774849" />
                  <Point X="3.350603515625" Y="0.004967842579" />
                  <Point X="3.348584472656" Y="-0.026262289047" />
                  <Point X="3.350280029297" Y="-0.062940639496" />
                  <Point X="3.351874267578" Y="-0.076422821045" />
                  <Point X="3.360260498047" Y="-0.120212028503" />
                  <Point X="3.370376464844" Y="-0.173033508301" />
                  <Point X="3.373159179688" Y="-0.183988204956" />
                  <Point X="3.380005371094" Y="-0.205488876343" />
                  <Point X="3.384068847656" Y="-0.216034713745" />
                  <Point X="3.393842529297" Y="-0.237498397827" />
                  <Point X="3.399130371094" Y="-0.247488235474" />
                  <Point X="3.410854736328" Y="-0.26676864624" />
                  <Point X="3.417291259766" Y="-0.276059356689" />
                  <Point X="3.442449707031" Y="-0.308117004395" />
                  <Point X="3.472797119141" Y="-0.34678692627" />
                  <Point X="3.478718017578" Y="-0.353633239746" />
                  <Point X="3.501139404297" Y="-0.375805236816" />
                  <Point X="3.530176025391" Y="-0.39872467041" />
                  <Point X="3.541494384766" Y="-0.406404174805" />
                  <Point X="3.583425048828" Y="-0.430640838623" />
                  <Point X="3.634004394531" Y="-0.45987677002" />
                  <Point X="3.639496582031" Y="-0.462815490723" />
                  <Point X="3.659158447266" Y="-0.472016876221" />
                  <Point X="3.683028076172" Y="-0.481027740479" />
                  <Point X="3.691991943359" Y="-0.48391293335" />
                  <Point X="4.402793457031" Y="-0.674371398926" />
                  <Point X="4.784876953125" Y="-0.776750427246" />
                  <Point X="4.76161328125" Y="-0.931053039551" />
                  <Point X="4.727801757812" Y="-1.079219604492" />
                  <Point X="4.120159179687" Y="-0.999221984863" />
                  <Point X="3.436781982422" Y="-0.909253723145" />
                  <Point X="3.428624511719" Y="-0.908535888672" />
                  <Point X="3.400098388672" Y="-0.908042480469" />
                  <Point X="3.366721435547" Y="-0.910840881348" />
                  <Point X="3.354481201172" Y="-0.912676208496" />
                  <Point X="3.272186035156" Y="-0.930563598633" />
                  <Point X="3.172916992188" Y="-0.952139892578" />
                  <Point X="3.157874023438" Y="-0.956742492676" />
                  <Point X="3.128753662109" Y="-0.968367004395" />
                  <Point X="3.114676269531" Y="-0.975388977051" />
                  <Point X="3.086849609375" Y="-0.992281311035" />
                  <Point X="3.074123779297" Y="-1.001530578613" />
                  <Point X="3.050373779297" Y="-1.022001464844" />
                  <Point X="3.039349609375" Y="-1.033223266602" />
                  <Point X="2.989607421875" Y="-1.093047607422" />
                  <Point X="2.92960546875" Y="-1.165211303711" />
                  <Point X="2.921326171875" Y="-1.17684753418" />
                  <Point X="2.90660546875" Y="-1.201229858398" />
                  <Point X="2.9001640625" Y="-1.213976074219" />
                  <Point X="2.888820800781" Y="-1.241361328125" />
                  <Point X="2.884363037109" Y="-1.254928222656" />
                  <Point X="2.87753125" Y="-1.282577514648" />
                  <Point X="2.875157226562" Y="-1.29666003418" />
                  <Point X="2.868027832031" Y="-1.374135131836" />
                  <Point X="2.859428222656" Y="-1.467590209961" />
                  <Point X="2.859288818359" Y="-1.483321166992" />
                  <Point X="2.861607666016" Y="-1.514590576172" />
                  <Point X="2.864065917969" Y="-1.530128662109" />
                  <Point X="2.871797607422" Y="-1.561749633789" />
                  <Point X="2.876786621094" Y="-1.576669067383" />
                  <Point X="2.889157958984" Y="-1.605479980469" />
                  <Point X="2.896540283203" Y="-1.619371337891" />
                  <Point X="2.942083496094" Y="-1.6902109375" />
                  <Point X="2.997020507812" Y="-1.775661865234" />
                  <Point X="3.001741943359" Y="-1.782353149414" />
                  <Point X="3.019792724609" Y="-1.804448730469" />
                  <Point X="3.043488769531" Y="-1.828119750977" />
                  <Point X="3.052796142578" Y="-1.836277709961" />
                  <Point X="3.712424316406" Y="-2.342428466797" />
                  <Point X="4.087170410156" Y="-2.629981445312" />
                  <Point X="4.045484130859" Y="-2.697436523438" />
                  <Point X="4.001274658203" Y="-2.760251953125" />
                  <Point X="3.457093994141" Y="-2.446069091797" />
                  <Point X="2.848454589844" Y="-2.094670654297" />
                  <Point X="2.841191894531" Y="-2.090885253906" />
                  <Point X="2.815025878906" Y="-2.079513183594" />
                  <Point X="2.783118652344" Y="-2.069325927734" />
                  <Point X="2.771107910156" Y="-2.066337646484" />
                  <Point X="2.673163574219" Y="-2.048649169922" />
                  <Point X="2.555017578125" Y="-2.027311889648" />
                  <Point X="2.539358886719" Y="-2.025807495117" />
                  <Point X="2.508006835938" Y="-2.025403198242" />
                  <Point X="2.492313476562" Y="-2.026503540039" />
                  <Point X="2.460140869141" Y="-2.031461303711" />
                  <Point X="2.444844726562" Y="-2.035136108398" />
                  <Point X="2.415068847656" Y="-2.044959960938" />
                  <Point X="2.400589111328" Y="-2.051108642578" />
                  <Point X="2.319221435547" Y="-2.093931884766" />
                  <Point X="2.221071044922" Y="-2.145587646484" />
                  <Point X="2.208968505859" Y="-2.153170410156" />
                  <Point X="2.186037841797" Y="-2.170063476562" />
                  <Point X="2.175209716797" Y="-2.179373779297" />
                  <Point X="2.154249755859" Y="-2.200333740234" />
                  <Point X="2.144939453125" Y="-2.211161865234" />
                  <Point X="2.128046386719" Y="-2.234092529297" />
                  <Point X="2.120463623047" Y="-2.246195068359" />
                  <Point X="2.077640380859" Y="-2.327562744141" />
                  <Point X="2.025984741211" Y="-2.425713134766" />
                  <Point X="2.0198359375" Y="-2.440192871094" />
                  <Point X="2.010012084961" Y="-2.46996875" />
                  <Point X="2.006337280273" Y="-2.485264892578" />
                  <Point X="2.001379516602" Y="-2.5174375" />
                  <Point X="2.000279174805" Y="-2.533130859375" />
                  <Point X="2.00068347168" Y="-2.564482910156" />
                  <Point X="2.002187866211" Y="-2.580141601562" />
                  <Point X="2.019876342773" Y="-2.678085693359" />
                  <Point X="2.041213378906" Y="-2.796231933594" />
                  <Point X="2.043015014648" Y="-2.804220947266" />
                  <Point X="2.051236083984" Y="-2.83154296875" />
                  <Point X="2.064069580078" Y="-2.862479736328" />
                  <Point X="2.069546630859" Y="-2.873578613281" />
                  <Point X="2.493424072266" Y="-3.607755615234" />
                  <Point X="2.735892822266" Y="-4.027724365234" />
                  <Point X="2.723754394531" Y="-4.036083496094" />
                  <Point X="2.298363037109" Y="-3.481703125" />
                  <Point X="1.833914550781" Y="-2.876422851562" />
                  <Point X="1.828653442383" Y="-2.870146240234" />
                  <Point X="1.808831542969" Y="-2.849626953125" />
                  <Point X="1.783252075195" Y="-2.828004882812" />
                  <Point X="1.773298828125" Y="-2.820647216797" />
                  <Point X="1.676699462891" Y="-2.758542724609" />
                  <Point X="1.560175415039" Y="-2.683628417969" />
                  <Point X="1.546283935547" Y="-2.676245849609" />
                  <Point X="1.517472900391" Y="-2.663874511719" />
                  <Point X="1.502553588867" Y="-2.658885742188" />
                  <Point X="1.470932617188" Y="-2.651154052734" />
                  <Point X="1.45539440918" Y="-2.648695800781" />
                  <Point X="1.42412512207" Y="-2.646376953125" />
                  <Point X="1.408394287109" Y="-2.646516357422" />
                  <Point X="1.30274621582" Y="-2.65623828125" />
                  <Point X="1.175307373047" Y="-2.667965332031" />
                  <Point X="1.161224975586" Y="-2.670339111328" />
                  <Point X="1.133575439453" Y="-2.677170898438" />
                  <Point X="1.120008422852" Y="-2.68162890625" />
                  <Point X="1.092623168945" Y="-2.692972167969" />
                  <Point X="1.079877563477" Y="-2.699413330078" />
                  <Point X="1.055495361328" Y="-2.714133789062" />
                  <Point X="1.043858764648" Y="-2.722413085938" />
                  <Point X="0.962280395508" Y="-2.790242919922" />
                  <Point X="0.863875061035" Y="-2.872063476562" />
                  <Point X="0.852653320312" Y="-2.883087890625" />
                  <Point X="0.832182128906" Y="-2.906838134766" />
                  <Point X="0.822933044434" Y="-2.919564208984" />
                  <Point X="0.80604083252" Y="-2.947390869141" />
                  <Point X="0.799018676758" Y="-2.961468261719" />
                  <Point X="0.787394287109" Y="-2.990588623047" />
                  <Point X="0.782791931152" Y="-3.005631347656" />
                  <Point X="0.75840020752" Y="-3.117852050781" />
                  <Point X="0.728977661133" Y="-3.253219238281" />
                  <Point X="0.727584716797" Y="-3.261289306641" />
                  <Point X="0.724724487305" Y="-3.289677734375" />
                  <Point X="0.7247421875" Y="-3.323170410156" />
                  <Point X="0.725554992676" Y="-3.335520019531" />
                  <Point X="0.833091308594" Y="-4.152339355469" />
                  <Point X="0.655064941406" Y="-3.487936523438" />
                  <Point X="0.652606262207" Y="-3.480124511719" />
                  <Point X="0.642145141602" Y="-3.453580322266" />
                  <Point X="0.62678692627" Y="-3.423815673828" />
                  <Point X="0.620407409668" Y="-3.413210205078" />
                  <Point X="0.546197021484" Y="-3.306286865234" />
                  <Point X="0.456679962158" Y="-3.177309814453" />
                  <Point X="0.446670654297" Y="-3.165172851562" />
                  <Point X="0.424786804199" Y="-3.142717529297" />
                  <Point X="0.41291229248" Y="-3.132399169922" />
                  <Point X="0.386657165527" Y="-3.113155273438" />
                  <Point X="0.373242767334" Y="-3.104937988281" />
                  <Point X="0.345241607666" Y="-3.090829589844" />
                  <Point X="0.330654724121" Y="-3.084938476562" />
                  <Point X="0.215818252563" Y="-3.049297363281" />
                  <Point X="0.077295753479" Y="-3.006305175781" />
                  <Point X="0.063376476288" Y="-3.003109130859" />
                  <Point X="0.035216896057" Y="-2.99883984375" />
                  <Point X="0.02097659111" Y="-2.997766601562" />
                  <Point X="-0.008664761543" Y="-2.997766601562" />
                  <Point X="-0.022905065536" Y="-2.998840087891" />
                  <Point X="-0.051064498901" Y="-3.003109375" />
                  <Point X="-0.064983482361" Y="-3.006305175781" />
                  <Point X="-0.179819946289" Y="-3.041946044922" />
                  <Point X="-0.318342590332" Y="-3.084938476562" />
                  <Point X="-0.332929473877" Y="-3.090829589844" />
                  <Point X="-0.360930786133" Y="-3.104937988281" />
                  <Point X="-0.374345336914" Y="-3.113155273438" />
                  <Point X="-0.400600463867" Y="-3.132399169922" />
                  <Point X="-0.412474853516" Y="-3.142717285156" />
                  <Point X="-0.434358673096" Y="-3.165172363281" />
                  <Point X="-0.444367980957" Y="-3.177309570312" />
                  <Point X="-0.518578369141" Y="-3.284232666016" />
                  <Point X="-0.608095458984" Y="-3.413209960938" />
                  <Point X="-0.612470275879" Y="-3.420132324219" />
                  <Point X="-0.62597668457" Y="-3.445265136719" />
                  <Point X="-0.638777893066" Y="-3.476215820312" />
                  <Point X="-0.642753112793" Y="-3.487936523438" />
                  <Point X="-0.861768432617" Y="-4.305313476563" />
                  <Point X="-0.985425048828" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.137056530388" Y="-4.699502240584" />
                  <Point X="-0.958068598339" Y="-4.66471051102" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.665630325099" Y="-3.96033479406" />
                  <Point X="2.69382169916" Y="-3.954854946055" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.123980741463" Y="-4.600182478669" />
                  <Point X="-0.93071214785" Y="-4.562614869696" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.601008406314" Y="-3.87611793659" />
                  <Point X="2.643584776576" Y="-3.867841928576" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.122597275469" Y="-4.503135474101" />
                  <Point X="-0.903355697362" Y="-4.460519228373" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.825616006498" Y="-4.124441174086" />
                  <Point X="0.829323564155" Y="-4.123720497883" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.536386487529" Y="-3.79190107912" />
                  <Point X="2.593347853991" Y="-3.780828911098" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.14079771627" Y="-4.40989519539" />
                  <Point X="-0.875999246873" Y="-4.358423587049" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.800968134589" Y="-4.032454149027" />
                  <Point X="0.81690040304" Y="-4.02935722976" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.471764568743" Y="-3.70768422165" />
                  <Point X="2.543110931406" Y="-3.693815893619" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.159331512005" Y="-4.316719714313" />
                  <Point X="-0.848642810386" Y="-4.256327948447" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.77632026268" Y="-3.940467123967" />
                  <Point X="0.804477241926" Y="-3.934993961637" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.407142649958" Y="-3.623467364179" />
                  <Point X="2.49287400799" Y="-3.606802876302" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.177865291041" Y="-4.223544229991" />
                  <Point X="-0.821286389081" Y="-4.154232312796" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.751672390772" Y="-3.848480098908" />
                  <Point X="0.792054080811" Y="-3.840630693514" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.342520731172" Y="-3.539250506709" />
                  <Point X="2.442637009469" Y="-3.519789873584" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.207377188492" Y="-4.13250267572" />
                  <Point X="-0.793929967776" Y="-4.052136677145" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.727024518863" Y="-3.756493073848" />
                  <Point X="0.779630919696" Y="-3.746267425391" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.277898803599" Y="-3.455033650947" />
                  <Point X="2.392400010948" Y="-3.432776870866" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.273729161943" Y="-4.048622106811" />
                  <Point X="-0.766573546471" Y="-3.950041041494" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.702376646954" Y="-3.664506048788" />
                  <Point X="0.767207758582" Y="-3.651904157269" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.213276857062" Y="-3.370816798871" />
                  <Point X="2.342163012427" Y="-3.345763868148" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.364061365324" Y="-3.969402822408" />
                  <Point X="-0.739217125166" Y="-3.847945405843" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.677728775045" Y="-3.572519023729" />
                  <Point X="0.754784597467" Y="-3.557540889146" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.148654910525" Y="-3.286599946796" />
                  <Point X="2.291926013906" Y="-3.25875086543" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.587312674349" Y="-4.110400703989" />
                  <Point X="-2.44149120705" Y="-4.082055882097" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.454393193589" Y="-3.890183465091" />
                  <Point X="-0.711860703861" Y="-3.745849770193" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.652754480521" Y="-3.480595448798" />
                  <Point X="0.742361436352" Y="-3.463177621023" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.084032963988" Y="-3.20238309472" />
                  <Point X="2.241689015385" Y="-3.171737862712" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.706269592959" Y="-4.036745500582" />
                  <Point X="-2.31113922914" Y="-3.959939938313" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.565159169357" Y="-3.814936103682" />
                  <Point X="-0.684504282556" Y="-3.643754134542" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.60627745975" Y="-3.392851580443" />
                  <Point X="0.729938275238" Y="-3.3688143529" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.019411017451" Y="-3.118166242644" />
                  <Point X="2.191452016864" Y="-3.084724859994" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.965872436421" Y="-2.7398124703" />
                  <Point X="4.023550606855" Y="-2.728600969701" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.813085809211" Y="-3.960730383697" />
                  <Point X="-2.106879795339" Y="-3.823457840406" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.883360914108" Y="-3.780010171174" />
                  <Point X="-0.657147861251" Y="-3.541658498891" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.547092990643" Y="-3.307577789823" />
                  <Point X="0.726433280986" Y="-3.272717568746" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.954789070914" Y="-3.033949390568" />
                  <Point X="2.141215018343" Y="-2.997711857276" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.840468458929" Y="-2.667410448191" />
                  <Point X="4.076226338886" Y="-2.621583758603" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.913441890712" Y="-3.883459543822" />
                  <Point X="-0.622109854458" Y="-3.438069714278" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.487908382286" Y="-3.222304026271" />
                  <Point X="0.746627902543" Y="-3.172014045944" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.890167124377" Y="-2.949732538492" />
                  <Point X="2.090978019822" Y="-2.910698854558" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.715064481436" Y="-2.595008426083" />
                  <Point X="3.975594851471" Y="-2.544366452216" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.960207616529" Y="-3.795771794043" />
                  <Point X="-0.54821054326" Y="-3.326927057301" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.420157954443" Y="-3.138695289359" />
                  <Point X="0.768590953528" Y="-3.070966775283" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.824396044222" Y="-2.865739055364" />
                  <Point X="2.048422631005" Y="-2.822192698171" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.589660503943" Y="-2.522606403974" />
                  <Point X="3.874963364055" Y="-2.467149145828" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.897269496067" Y="-3.68675977671" />
                  <Point X="-0.470566363874" Y="-3.215056471689" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.279889209277" Y="-3.069182685386" />
                  <Point X="0.796073338442" Y="-2.968846654787" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.723430747085" Y="-2.788586635013" />
                  <Point X="2.029102118127" Y="-2.729170139416" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.464256526451" Y="-2.450204381865" />
                  <Point X="3.77433187664" Y="-2.38993183944" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.834331375604" Y="-3.577747759377" />
                  <Point X="-0.335174926824" Y="-3.09196095628" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.088151980014" Y="-3.009674541262" />
                  <Point X="0.884553999163" Y="-2.854869670582" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.607845645571" Y="-2.714276016757" />
                  <Point X="2.012216925539" Y="-2.63567420235" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.338852625522" Y="-2.377802344874" />
                  <Point X="3.673700384806" Y="-2.312714533911" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.771393255142" Y="-3.468735742045" />
                  <Point X="1.036461374369" Y="-2.728563782009" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.449568963478" Y="-2.64826380113" />
                  <Point X="2.000383182607" Y="-2.541196362939" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.213448729232" Y="-2.305400306982" />
                  <Point X="3.573068885909" Y="-2.235497229754" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.70845513468" Y="-3.359723724712" />
                  <Point X="2.019679350844" Y="-2.440667481771" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.088044832941" Y="-2.232998269089" />
                  <Point X="3.472437387012" Y="-2.158279925598" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.645516997233" Y="-3.250711704078" />
                  <Point X="2.074673775701" Y="-2.333199562446" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.962640936651" Y="-2.160596231196" />
                  <Point X="3.371805888114" Y="-2.081062621442" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.582578844757" Y="-3.141699680522" />
                  <Point X="2.134964267066" Y="-2.224702192075" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.835691342168" Y="-2.088494646596" />
                  <Point X="3.271174389217" Y="-2.003845317286" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.519640692281" Y="-3.032687656966" />
                  <Point X="2.324716084777" Y="-2.091040089068" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.605585767863" Y="-2.036444553243" />
                  <Point X="3.17054289032" Y="-1.92662801313" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.456702539805" Y="-2.923675633411" />
                  <Point X="3.069911391422" Y="-1.849410708974" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.393764387329" Y="-2.814663609855" />
                  <Point X="2.991956720867" Y="-1.767785475915" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.811536443497" Y="-2.993472494399" />
                  <Point X="-3.72044257308" Y="-2.975765639707" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.33336821796" Y="-2.706145697762" />
                  <Point X="2.936649114706" Y="-1.681758099477" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.875705368965" Y="-2.909167583948" />
                  <Point X="-3.467738208612" Y="-2.829866801201" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.311174661827" Y="-2.605053621439" />
                  <Point X="2.884691997516" Y="-1.595079453958" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.939874312214" Y="-2.824862676953" />
                  <Point X="-3.215033972346" Y="-2.683967987614" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.330143820658" Y="-2.511962766376" />
                  <Point X="2.86074489577" Y="-1.502956212977" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.004037942038" Y="-2.740556737133" />
                  <Point X="-2.962329791305" Y="-2.538069184762" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.387675914609" Y="-2.426367786563" />
                  <Point X="2.865158218527" Y="-1.405320263915" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.61750434388" Y="-1.064698682352" />
                  <Point X="4.736388839535" Y="-1.041589877335" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.055756990797" Y="-2.653831815798" />
                  <Point X="-2.709625610265" Y="-2.39217038191" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.513663147974" Y="-2.354079137911" />
                  <Point X="2.874226008361" Y="-1.306779578103" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.320668856051" Y="-1.025619570219" />
                  <Point X="4.759498634237" Y="-0.940319702276" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.107476039556" Y="-2.567106894464" />
                  <Point X="2.905216943083" Y="-1.20397746461" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.02383336638" Y="-0.986540458443" />
                  <Point X="4.775269250704" Y="-0.840476118951" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.159194982606" Y="-2.480381952581" />
                  <Point X="2.99184094227" Y="-1.090361378849" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.726997872873" Y="-0.947461347413" />
                  <Point X="4.709346558261" Y="-0.756512106267" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.084433129105" Y="-2.369071634365" />
                  <Point X="3.135573939911" Y="-0.965644428313" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.429138996416" Y="-0.908581161878" />
                  <Point X="4.500019551063" Y="-0.700423068616" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.915520495514" Y="-2.23946025841" />
                  <Point X="4.290692426096" Y="-0.644334053857" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.746607861924" Y="-2.109848882455" />
                  <Point X="4.081365198987" Y="-0.588245058953" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.577695090624" Y="-1.980237479731" />
                  <Point X="3.872037971879" Y="-0.532156064048" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.408782074464" Y="-1.850626029412" />
                  <Point X="3.667448052588" Y="-0.475146229786" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.239869058303" Y="-1.721014579092" />
                  <Point X="3.537423816336" Y="-0.403642295003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.070956042143" Y="-1.591403128773" />
                  <Point X="3.454167022605" Y="-0.323047690286" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.974854372926" Y="-1.475944770581" />
                  <Point X="3.394081108043" Y="-0.237949122912" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.949285148317" Y="-1.374196530776" />
                  <Point X="3.365344175201" Y="-0.146756930781" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.95940005256" Y="-1.279384582969" />
                  <Point X="3.349820328582" Y="-0.052996374865" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.011840496119" Y="-1.192799886579" />
                  <Point X="3.357905097447" Y="0.045353231027" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.12319825417" Y="-1.117667555993" />
                  <Point X="3.381462527889" Y="0.14671041766" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.133274980855" Y="-1.217228496358" />
                  <Point X="3.451512141637" Y="0.257104769256" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.676417063401" Y="-1.226026536248" />
                  <Point X="3.61325353891" Y="0.385322198079" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.699967865326" Y="-1.133826262386" />
                  <Point X="4.725753896265" Y="0.698348447478" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.723518667252" Y="-1.041625988524" />
                  <Point X="4.769892197823" Y="0.8037061502" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.74436886712" Y="-0.9489007708" />
                  <Point X="4.755266939024" Y="0.897641373894" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.757835841442" Y="-0.854740399411" />
                  <Point X="4.734161393893" Y="0.990316957528" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.771302815764" Y="-0.760580028022" />
                  <Point X="4.711665636398" Y="1.082722311253" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.784769919355" Y="-0.66641968176" />
                  <Point X="3.639353416215" Y="0.971064016422" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.635343701416" Y="-0.346215772166" />
                  <Point X="3.258037851871" Y="0.993721865167" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.342484058006" Y="-0.192511538125" />
                  <Point X="3.142584856076" Y="1.068058162174" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.301495879912" Y="-0.087766157375" />
                  <Point X="3.077533723008" Y="1.152191588839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.297936781511" Y="0.009703747293" />
                  <Point X="3.03084179834" Y="1.239893684109" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.326933755078" Y="0.100845392628" />
                  <Point X="3.008861531442" Y="1.332399239055" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.393504224744" Y="0.184683490175" />
                  <Point X="3.002351544013" Y="1.427911911707" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.528919698999" Y="0.255139474448" />
                  <Point X="3.026419611777" Y="1.52936835618" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.738246813778" Y="0.311228491187" />
                  <Point X="3.080459912197" Y="1.636650812502" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.947573928558" Y="0.367317507926" />
                  <Point X="3.18619475444" Y="1.753981669844" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.156901043337" Y="0.423406524665" />
                  <Point X="3.355107729291" Y="1.883593112134" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.366228148571" Y="0.47949554326" />
                  <Point X="3.524020704142" Y="2.013204554424" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.575555220562" Y="0.535584568316" />
                  <Point X="3.692933664776" Y="2.14281599395" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.784882292553" Y="0.591673593372" />
                  <Point X="2.363639655071" Y="1.981205499429" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.652434769245" Y="2.037341583" />
                  <Point X="3.861846225775" Y="2.272427355795" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.770740808687" Y="0.691200505399" />
                  <Point X="2.199160981038" Y="2.046012169945" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.905138941455" Y="2.183240384135" />
                  <Point X="4.030758786774" Y="2.40203871764" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.755996065117" Y="0.790844679233" />
                  <Point X="2.108637840852" Y="2.125194339992" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.157843113664" Y="2.32913918527" />
                  <Point X="4.117406788441" Y="2.515659469011" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.741251321547" Y="0.890488853067" />
                  <Point X="2.05200068104" Y="2.21096327738" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.410547285873" Y="2.475037986406" />
                  <Point X="4.065006065223" Y="2.602251886253" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.72227353999" Y="0.990955846133" />
                  <Point X="2.021580826535" Y="2.301828342678" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.663251224976" Y="2.620936742229" />
                  <Point X="4.007406992124" Y="2.687833846639" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.694590320189" Y="1.093115004977" />
                  <Point X="-3.979468465318" Y="1.232120612198" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.488567584166" Y="1.327542077232" />
                  <Point X="2.013086144219" Y="2.396955229724" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.915955118356" Y="2.766835489166" />
                  <Point X="3.946315797969" Y="2.772737007454" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.666907100388" Y="1.19527416382" />
                  <Point X="-4.276303752242" Y="1.271199763383" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.437656600879" Y="1.434216255923" />
                  <Point X="2.029629050651" Y="2.496948931011" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.63922385368" Y="1.297433327894" />
                  <Point X="-4.573138816097" Y="1.310278957929" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.42151438984" Y="1.534132069915" />
                  <Point X="2.066651220287" Y="2.600923397811" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.4446857045" Y="1.626406108629" />
                  <Point X="2.129589336191" Y="2.709935414258" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.492551092413" Y="1.713880105751" />
                  <Point X="2.192527452095" Y="2.818947430704" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.576163342939" Y="1.794405616666" />
                  <Point X="2.255465567999" Y="2.927959447151" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.676794886223" Y="1.871622912195" />
                  <Point X="2.318403683903" Y="3.036971463598" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.777426429506" Y="1.948840207723" />
                  <Point X="2.381341799807" Y="3.145983480045" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.878057972789" Y="2.026057503251" />
                  <Point X="2.444279915711" Y="3.254995496491" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.978689516073" Y="2.10327479878" />
                  <Point X="2.507217986443" Y="3.364007504157" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.079320805362" Y="2.18049214368" />
                  <Point X="2.570156047004" Y="3.473019509846" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.179951981868" Y="2.257709510502" />
                  <Point X="2.633094107565" Y="3.582031515536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.194075694334" Y="2.351742224927" />
                  <Point X="2.696032168125" Y="3.691043521225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.130358244488" Y="2.460905728547" />
                  <Point X="2.758970228686" Y="3.800055526914" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.066640698523" Y="2.57006925085" />
                  <Point X="-3.195932485349" Y="2.739317782495" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.869111015221" Y="2.802845440892" />
                  <Point X="2.810769177127" Y="3.906902308545" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.002923152558" Y="2.679232773153" />
                  <Point X="-3.334987931214" Y="2.809066227962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.773128826092" Y="2.918280574507" />
                  <Point X="2.703894075456" Y="3.982905979264" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.914462538734" Y="2.793205860635" />
                  <Point X="-3.460391870181" Y="2.881468257559" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.74876949813" Y="3.019793634227" />
                  <Point X="2.591656432188" Y="4.05786727749" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.825754861487" Y="2.907226972382" />
                  <Point X="-3.585795824528" Y="2.953870284166" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.755005464761" Y="3.115359571127" />
                  <Point X="2.467100386108" Y="4.130434120768" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.737047410979" Y="3.021248040055" />
                  <Point X="-3.711199817736" Y="3.02627230322" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.777543821994" Y="3.207756644301" />
                  <Point X="2.342544133151" Y="4.203000923834" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.825708197784" Y="3.295172524066" />
                  <Point X="2.212351586531" Y="4.274472142395" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.875945228806" Y="3.382185520467" />
                  <Point X="2.066171512287" Y="4.342835700395" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.926182259828" Y="3.469198516867" />
                  <Point X="1.919991530716" Y="4.411199276408" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.97641929085" Y="3.556211513268" />
                  <Point X="-0.106423304841" Y="4.114082220252" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.188916168342" Y="4.17149039835" />
                  <Point X="1.75790159036" Y="4.476470269715" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.026656255457" Y="3.643224522578" />
                  <Point X="-0.192610448532" Y="4.194107222639" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.24252292189" Y="4.278688581697" />
                  <Point X="1.584180077214" Y="4.539480314306" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.00046085554" Y="3.745094478533" />
                  <Point X="-0.231455810601" Y="4.283334535172" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.269879356723" Y="4.380784219978" />
                  <Point X="1.398453262972" Y="4.600156764759" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.831360477812" Y="3.874742348251" />
                  <Point X="-1.930592364404" Y="4.049833932597" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.595168662535" Y="4.115033695458" />
                  <Point X="-0.256103616039" Y="4.375321573152" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.297235791556" Y="4.482879858258" />
                  <Point X="1.176390005795" Y="4.653770126202" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.662260318963" Y="4.004390175425" />
                  <Point X="-2.063434741397" Y="4.120790076311" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.508122711667" Y="4.228731800318" />
                  <Point X="-0.280751421476" Y="4.467308611132" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.324592226389" Y="4.584975496539" />
                  <Point X="0.954326141238" Y="4.707383369582" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.40244020946" Y="4.151672174651" />
                  <Point X="-2.141896522433" Y="4.202316737079" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.475055244756" Y="4.33193755078" />
                  <Point X="-0.305399226914" Y="4.559295649112" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.351948659657" Y="4.687071134515" />
                  <Point X="0.688561709202" Y="4.752502083145" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.462714000211" Y="4.43111453173" />
                  <Point X="-0.330047056316" Y="4.651282682434" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.474390771334" Y="4.52562288337" />
                  <Point X="-0.35469489693" Y="4.743269713577" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="0.001626220703" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.653104858398" Y="-4.214725097656" />
                  <Point X="0.471539031982" Y="-3.537112304688" />
                  <Point X="0.464318664551" Y="-3.521544189453" />
                  <Point X="0.390108123779" Y="-3.414620849609" />
                  <Point X="0.300591033936" Y="-3.285643798828" />
                  <Point X="0.274335754395" Y="-3.266399902344" />
                  <Point X="0.159499206543" Y="-3.230758789062" />
                  <Point X="0.020976625443" Y="-3.187766601562" />
                  <Point X="-0.008664756775" Y="-3.187766601562" />
                  <Point X="-0.123501304626" Y="-3.223407470703" />
                  <Point X="-0.262023895264" Y="-3.266399902344" />
                  <Point X="-0.288279144287" Y="-3.285643798828" />
                  <Point X="-0.362489715576" Y="-3.392566894531" />
                  <Point X="-0.45200680542" Y="-3.521544189453" />
                  <Point X="-0.459227142334" Y="-3.537112304688" />
                  <Point X="-0.678242553711" Y="-4.354489257813" />
                  <Point X="-0.84774395752" Y="-4.987077148438" />
                  <Point X="-0.937840881348" Y="-4.969588867188" />
                  <Point X="-1.100246948242" Y="-4.938065429688" />
                  <Point X="-1.232813476562" Y="-4.90395703125" />
                  <Point X="-1.35158972168" Y="-4.873396972656" />
                  <Point X="-1.331006591797" Y="-4.717053222656" />
                  <Point X="-1.309150146484" Y="-4.551038574219" />
                  <Point X="-1.309683227539" Y="-4.5347578125" />
                  <Point X="-1.337117675781" Y="-4.3968359375" />
                  <Point X="-1.370210693359" Y="-4.230466308594" />
                  <Point X="-1.386282714844" Y="-4.20262890625" />
                  <Point X="-1.492009643555" Y="-4.109908691406" />
                  <Point X="-1.619543457031" Y="-3.998063964844" />
                  <Point X="-1.649240722656" Y="-3.985762939453" />
                  <Point X="-1.789563842773" Y="-3.976565673828" />
                  <Point X="-1.958829833984" Y="-3.965471435547" />
                  <Point X="-1.989878540039" Y="-3.973791015625" />
                  <Point X="-2.106803222656" Y="-4.051917724609" />
                  <Point X="-2.247844970703" Y="-4.146159179688" />
                  <Point X="-2.259734130859" Y="-4.157294433594" />
                  <Point X="-2.382694335938" Y="-4.3175390625" />
                  <Point X="-2.457095214844" Y="-4.414500488281" />
                  <Point X="-2.617781738281" Y="-4.315007324219" />
                  <Point X="-2.855831054688" Y="-4.16761328125" />
                  <Point X="-3.039401611328" Y="-4.026270019531" />
                  <Point X="-3.228581054688" Y="-3.880608154297" />
                  <Point X="-2.843453369141" Y="-3.213547363281" />
                  <Point X="-2.506033691406" Y="-2.629119628906" />
                  <Point X="-2.499762695312" Y="-2.597593017578" />
                  <Point X="-2.513979980469" Y="-2.568763916016" />
                  <Point X="-2.531328613281" Y="-2.551415283203" />
                  <Point X="-2.560157714844" Y="-2.537198730469" />
                  <Point X="-2.59168359375" Y="-2.543469726562" />
                  <Point X="-3.296655029297" Y="-2.950485107422" />
                  <Point X="-3.842959472656" Y="-3.265894042969" />
                  <Point X="-3.973629394531" Y="-3.094220458984" />
                  <Point X="-4.161703613281" Y="-2.847129394531" />
                  <Point X="-4.293307128906" Y="-2.626450683594" />
                  <Point X="-4.431020019531" Y="-2.395527099609" />
                  <Point X="-3.754161621094" Y="-1.876155395508" />
                  <Point X="-3.163786865234" Y="-1.423144897461" />
                  <Point X="-3.145822021484" Y="-1.396013427734" />
                  <Point X="-3.138117431641" Y="-1.366266113281" />
                  <Point X="-3.140326416016" Y="-1.334595581055" />
                  <Point X="-3.161158935547" Y="-1.310639160156" />
                  <Point X="-3.187641601562" Y="-1.295052734375" />
                  <Point X="-3.219529296875" Y="-1.288571044922" />
                  <Point X="-4.109488769531" Y="-1.405736450195" />
                  <Point X="-4.803283691406" Y="-1.497076293945" />
                  <Point X="-4.8538359375" Y="-1.299165039062" />
                  <Point X="-4.927393066406" Y="-1.011191650391" />
                  <Point X="-4.962211914062" Y="-0.767741210938" />
                  <Point X="-4.998396484375" Y="-0.514742126465" />
                  <Point X="-4.229989746094" Y="-0.308848175049" />
                  <Point X="-3.557463867188" Y="-0.128645431519" />
                  <Point X="-3.529316650391" Y="-0.112694519043" />
                  <Point X="-3.50232421875" Y="-0.09064528656" />
                  <Point X="-3.490705810547" Y="-0.062397411346" />
                  <Point X="-3.483400878906" Y="-0.031280040741" />
                  <Point X="-3.489841064453" Y="-0.002949298382" />
                  <Point X="-3.50232421875" Y="0.028085205078" />
                  <Point X="-3.526721923828" Y="0.048333667755" />
                  <Point X="-3.557463867188" Y="0.066085334778" />
                  <Point X="-4.368704101562" Y="0.283456481934" />
                  <Point X="-4.998186523438" Y="0.452125793457" />
                  <Point X="-4.965051269531" Y="0.676050415039" />
                  <Point X="-4.917645019531" Y="0.996418212891" />
                  <Point X="-4.847555175781" Y="1.255071166992" />
                  <Point X="-4.773516113281" Y="1.528298583984" />
                  <Point X="-4.235349609375" Y="1.457447631836" />
                  <Point X="-3.753266357422" Y="1.393980102539" />
                  <Point X="-3.731704833984" Y="1.395866577148" />
                  <Point X="-3.70386328125" Y="1.404645019531" />
                  <Point X="-3.670278808594" Y="1.415234130859" />
                  <Point X="-3.651534423828" Y="1.426056152344" />
                  <Point X="-3.639119873047" Y="1.443785888672" />
                  <Point X="-3.627948242188" Y="1.470756591797" />
                  <Point X="-3.614472412109" Y="1.503290161133" />
                  <Point X="-3.610714111328" Y="1.524605224609" />
                  <Point X="-3.616315917969" Y="1.54551159668" />
                  <Point X="-3.629795654297" Y="1.571406005859" />
                  <Point X="-3.646055664062" Y="1.602641235352" />
                  <Point X="-3.659968261719" Y="1.619221679688" />
                  <Point X="-4.125297851563" Y="1.976281494141" />
                  <Point X="-4.47610546875" Y="2.245466308594" />
                  <Point X="-4.34421875" Y="2.471420410156" />
                  <Point X="-4.160014648438" Y="2.787006591797" />
                  <Point X="-3.974349609375" Y="3.025652587891" />
                  <Point X="-3.774671142578" Y="3.282310791016" />
                  <Point X="-3.455239013672" Y="3.097886474609" />
                  <Point X="-3.159156982422" Y="2.926943359375" />
                  <Point X="-3.138514892578" Y="2.920434814453" />
                  <Point X="-3.099739013672" Y="2.917042480469" />
                  <Point X="-3.052965576172" Y="2.912950195312" />
                  <Point X="-3.031506835938" Y="2.915775390625" />
                  <Point X="-3.013252441406" Y="2.927404785156" />
                  <Point X="-2.985729003906" Y="2.954927978516" />
                  <Point X="-2.952528808594" Y="2.988128173828" />
                  <Point X="-2.940899414062" Y="3.006382324219" />
                  <Point X="-2.93807421875" Y="3.027841064453" />
                  <Point X="-2.941466796875" Y="3.066616943359" />
                  <Point X="-2.945558837891" Y="3.113390625" />
                  <Point X="-2.952067382812" Y="3.134032958984" />
                  <Point X="-3.158269042969" Y="3.491184326172" />
                  <Point X="-3.307278564453" Y="3.749276611328" />
                  <Point X="-3.073700927734" Y="3.928358154297" />
                  <Point X="-2.752876220703" Y="4.174331542969" />
                  <Point X="-2.460459960938" Y="4.336791503906" />
                  <Point X="-2.141548583984" Y="4.513972167969" />
                  <Point X="-2.058466552734" Y="4.405697265625" />
                  <Point X="-1.967826904297" Y="4.287573242188" />
                  <Point X="-1.951246948242" Y="4.273660644531" />
                  <Point X="-1.90808972168" Y="4.251194335938" />
                  <Point X="-1.856030761719" Y="4.22409375" />
                  <Point X="-1.835124145508" Y="4.2184921875" />
                  <Point X="-1.813808837891" Y="4.222250488281" />
                  <Point X="-1.768857666016" Y="4.240870117188" />
                  <Point X="-1.714634887695" Y="4.263330078125" />
                  <Point X="-1.696905273438" Y="4.275744628906" />
                  <Point X="-1.686083496094" Y="4.294488769531" />
                  <Point X="-1.671452636719" Y="4.340891601563" />
                  <Point X="-1.653804077148" Y="4.396865234375" />
                  <Point X="-1.651917602539" Y="4.418426757812" />
                  <Point X="-1.675360473633" Y="4.596492675781" />
                  <Point X="-1.689137573242" Y="4.701141113281" />
                  <Point X="-1.383421875" Y="4.786853027344" />
                  <Point X="-0.968094177246" Y="4.903296386719" />
                  <Point X="-0.61359765625" Y="4.94478515625" />
                  <Point X="-0.224199615479" Y="4.990358398438" />
                  <Point X="-0.129729553223" Y="4.637791015625" />
                  <Point X="-0.042140380859" Y="4.310903320312" />
                  <Point X="-0.024282117844" Y="4.284176757813" />
                  <Point X="0.006155934334" Y="4.273844238281" />
                  <Point X="0.036594036102" Y="4.284176757813" />
                  <Point X="0.054452251434" Y="4.310903320312" />
                  <Point X="0.160107513428" Y="4.705214355469" />
                  <Point X="0.236648406982" Y="4.990868652344" />
                  <Point X="0.497563232422" Y="4.963543945312" />
                  <Point X="0.860205810547" Y="4.925565429688" />
                  <Point X="1.153490478516" Y="4.8547578125" />
                  <Point X="1.508456176758" Y="4.769057617188" />
                  <Point X="1.699518432617" Y="4.699758300781" />
                  <Point X="1.931033691406" Y="4.615786132813" />
                  <Point X="2.115625488281" Y="4.529458496094" />
                  <Point X="2.33869921875" Y="4.425134277344" />
                  <Point X="2.517024658203" Y="4.321241699219" />
                  <Point X="2.732523681641" Y="4.19569140625" />
                  <Point X="2.900706054688" Y="4.076089355469" />
                  <Point X="3.068740234375" Y="3.956592773438" />
                  <Point X="2.620391357422" Y="3.180029296875" />
                  <Point X="2.229853515625" Y="2.503597900391" />
                  <Point X="2.224852050781" Y="2.491514892578" />
                  <Point X="2.215120849609" Y="2.455124755859" />
                  <Point X="2.203382324219" Y="2.411228515625" />
                  <Point X="2.202044677734" Y="2.392325927734" />
                  <Point X="2.205839111328" Y="2.360858886719" />
                  <Point X="2.210416015625" Y="2.322901123047" />
                  <Point X="2.218682128906" Y="2.3008125" />
                  <Point X="2.238153076172" Y="2.272117431641" />
                  <Point X="2.261639892578" Y="2.23750390625" />
                  <Point X="2.274940185547" Y="2.224203613281" />
                  <Point X="2.303635009766" Y="2.204732666016" />
                  <Point X="2.338248779297" Y="2.18124609375" />
                  <Point X="2.360336914062" Y="2.172980224609" />
                  <Point X="2.391804199219" Y="2.169185791016" />
                  <Point X="2.42976171875" Y="2.164608642578" />
                  <Point X="2.448664306641" Y="2.165946289062" />
                  <Point X="2.4850546875" Y="2.175677734375" />
                  <Point X="2.528950683594" Y="2.187416015625" />
                  <Point X="2.541033935547" Y="2.192417480469" />
                  <Point X="3.356985595703" Y="2.663507324219" />
                  <Point X="3.994248046875" Y="3.031430908203" />
                  <Point X="4.074765136719" Y="2.919530517578" />
                  <Point X="4.202591796875" Y="2.741880126953" />
                  <Point X="4.296351074219" Y="2.586942138672" />
                  <Point X="4.387512695312" Y="2.436296142578" />
                  <Point X="3.802795898438" Y="1.987626586914" />
                  <Point X="3.288616210938" Y="1.593082763672" />
                  <Point X="3.279371337891" Y="1.583833374023" />
                  <Point X="3.253181152344" Y="1.549666381836" />
                  <Point X="3.221589111328" Y="1.508451904297" />
                  <Point X="3.213119628906" Y="1.4915" />
                  <Point X="3.203363769531" Y="1.456615356445" />
                  <Point X="3.191595703125" Y="1.414535522461" />
                  <Point X="3.190779541016" Y="1.390965332031" />
                  <Point X="3.198788085938" Y="1.352151733398" />
                  <Point X="3.208448486328" Y="1.305332519531" />
                  <Point X="3.215646484375" Y="1.287954833984" />
                  <Point X="3.237428710938" Y="1.254846557617" />
                  <Point X="3.263704101562" Y="1.214909423828" />
                  <Point X="3.280947998047" Y="1.198819824219" />
                  <Point X="3.312513671875" Y="1.18105090332" />
                  <Point X="3.350590087891" Y="1.159617431641" />
                  <Point X="3.368565673828" Y="1.153619628906" />
                  <Point X="3.411244384766" Y="1.147979003906" />
                  <Point X="3.462726318359" Y="1.141175048828" />
                  <Point X="3.475803710938" Y="1.14117175293" />
                  <Point X="4.250903320312" Y="1.243215576172" />
                  <Point X="4.848975097656" Y="1.32195324707" />
                  <Point X="4.884287597656" Y="1.176900878906" />
                  <Point X="4.939188476562" Y="0.951385559082" />
                  <Point X="4.968734863281" Y="0.761614746094" />
                  <Point X="4.997858398438" Y="0.574556152344" />
                  <Point X="4.330428710938" Y="0.395718933105" />
                  <Point X="3.741167724609" Y="0.237826919556" />
                  <Point X="3.729086914062" Y="0.232819412231" />
                  <Point X="3.68715625" Y="0.208582717896" />
                  <Point X="3.636576904297" Y="0.179346878052" />
                  <Point X="3.622264892578" Y="0.166926574707" />
                  <Point X="3.597106445312" Y="0.134868835449" />
                  <Point X="3.566759033203" Y="0.096198890686" />
                  <Point X="3.556985351562" Y="0.074735275269" />
                  <Point X="3.548599121094" Y="0.030946016312" />
                  <Point X="3.538483154297" Y="-0.021875303268" />
                  <Point X="3.538483154297" Y="-0.04068478775" />
                  <Point X="3.546869384766" Y="-0.084474052429" />
                  <Point X="3.556985351562" Y="-0.13729536438" />
                  <Point X="3.566759033203" Y="-0.158758987427" />
                  <Point X="3.591917480469" Y="-0.190816726685" />
                  <Point X="3.622264892578" Y="-0.229486679077" />
                  <Point X="3.636576904297" Y="-0.241906967163" />
                  <Point X="3.678507568359" Y="-0.26614364624" />
                  <Point X="3.729086914062" Y="-0.295379486084" />
                  <Point X="3.741167724609" Y="-0.300387023926" />
                  <Point X="4.451969238281" Y="-0.490845581055" />
                  <Point X="4.998067871094" Y="-0.637172363281" />
                  <Point X="4.9790859375" Y="-0.763076721191" />
                  <Point X="4.948431640625" Y="-0.966399169922" />
                  <Point X="4.910578125" Y="-1.132279418945" />
                  <Point X="4.874545898438" Y="-1.290178344727" />
                  <Point X="4.095359130859" Y="-1.187596557617" />
                  <Point X="3.411982177734" Y="-1.097628295898" />
                  <Point X="3.394836181641" Y="-1.098341186523" />
                  <Point X="3.312541015625" Y="-1.116228393555" />
                  <Point X="3.213271972656" Y="-1.137804931641" />
                  <Point X="3.1854453125" Y="-1.154697265625" />
                  <Point X="3.135703125" Y="-1.214521728516" />
                  <Point X="3.075701171875" Y="-1.286685424805" />
                  <Point X="3.064357910156" Y="-1.314070678711" />
                  <Point X="3.057228515625" Y="-1.391545776367" />
                  <Point X="3.04862890625" Y="-1.485000854492" />
                  <Point X="3.056360595703" Y="-1.516621826172" />
                  <Point X="3.101903808594" Y="-1.587461425781" />
                  <Point X="3.156840820312" Y="-1.672912353516" />
                  <Point X="3.168460693359" Y="-1.685540649414" />
                  <Point X="3.828088867188" Y="-2.191691162109" />
                  <Point X="4.33907421875" Y="-2.583783935547" />
                  <Point X="4.290542480469" Y="-2.662314697266" />
                  <Point X="4.204127929688" Y="-2.802147216797" />
                  <Point X="4.12584765625" Y="-2.913372314453" />
                  <Point X="4.056688476562" Y="-3.011638427734" />
                  <Point X="3.362093994141" Y="-2.610614013672" />
                  <Point X="2.753454589844" Y="-2.259215576172" />
                  <Point X="2.737340820313" Y="-2.253312744141" />
                  <Point X="2.639396484375" Y="-2.235624267578" />
                  <Point X="2.521250488281" Y="-2.214287109375" />
                  <Point X="2.489077880859" Y="-2.219244873047" />
                  <Point X="2.407710205078" Y="-2.262068115234" />
                  <Point X="2.309559814453" Y="-2.313723876953" />
                  <Point X="2.288599853516" Y="-2.334683837891" />
                  <Point X="2.245776611328" Y="-2.416051513672" />
                  <Point X="2.194120849609" Y="-2.514201904297" />
                  <Point X="2.189163085938" Y="-2.546374511719" />
                  <Point X="2.2068515625" Y="-2.644318603516" />
                  <Point X="2.228188720703" Y="-2.76246484375" />
                  <Point X="2.234091552734" Y="-2.778578613281" />
                  <Point X="2.657968994141" Y="-3.512755615234" />
                  <Point X="2.986673583984" Y="-4.082088623047" />
                  <Point X="2.937833007812" Y="-4.116974121094" />
                  <Point X="2.835296875" Y="-4.190213378906" />
                  <Point X="2.747772705078" Y="-4.246866210938" />
                  <Point X="2.679775878906" Y="-4.290879394531" />
                  <Point X="2.147625732422" Y="-3.597367675781" />
                  <Point X="1.683177490234" Y="-2.992087402344" />
                  <Point X="1.670549316406" Y="-2.980467529297" />
                  <Point X="1.573949829102" Y="-2.918363037109" />
                  <Point X="1.45742590332" Y="-2.843448730469" />
                  <Point X="1.425804931641" Y="-2.835717041016" />
                  <Point X="1.320156860352" Y="-2.845438964844" />
                  <Point X="1.192718017578" Y="-2.857166015625" />
                  <Point X="1.165332885742" Y="-2.868509277344" />
                  <Point X="1.083754394531" Y="-2.936339111328" />
                  <Point X="0.985349060059" Y="-3.018159667969" />
                  <Point X="0.968456726074" Y="-3.045986328125" />
                  <Point X="0.944065185547" Y="-3.15820703125" />
                  <Point X="0.91464251709" Y="-3.29357421875" />
                  <Point X="0.91392956543" Y="-3.310720214844" />
                  <Point X="1.034053100586" Y="-4.223149414062" />
                  <Point X="1.127642211914" Y="-4.934028320312" />
                  <Point X="1.091350830078" Y="-4.941983398438" />
                  <Point X="0.994346252441" Y="-4.963246582031" />
                  <Point X="0.91348815918" Y="-4.977936035156" />
                  <Point X="0.860200378418" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#162" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.087589240046" Y="4.682011984302" Z="1.15" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.15" />
                  <Point X="-0.625565786334" Y="5.025726147262" Z="1.15" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.15" />
                  <Point X="-1.403075624132" Y="4.866276683211" Z="1.15" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.15" />
                  <Point X="-1.731088682022" Y="4.621246401114" Z="1.15" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.15" />
                  <Point X="-1.725294383833" Y="4.38720683628" Z="1.15" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.15" />
                  <Point X="-1.794147891476" Y="4.318344114707" Z="1.15" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.15" />
                  <Point X="-1.891157919395" Y="4.326824904047" Z="1.15" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.15" />
                  <Point X="-2.02495484578" Y="4.467415259401" Z="1.15" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.15" />
                  <Point X="-2.490898777771" Y="4.411779151341" Z="1.15" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.15" />
                  <Point X="-3.1102790809" Y="3.999318244864" Z="1.15" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.15" />
                  <Point X="-3.207726253463" Y="3.497464479469" Z="1.15" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.15" />
                  <Point X="-2.997432485308" Y="3.093539306272" Z="1.15" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.15" />
                  <Point X="-3.02724025057" Y="3.021563317317" Z="1.15" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.15" />
                  <Point X="-3.101537159334" Y="2.998132213865" Z="1.15" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.15" />
                  <Point X="-3.436394868828" Y="3.172467695282" Z="1.15" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.15" />
                  <Point X="-4.019969030777" Y="3.087634909834" Z="1.15" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.15" />
                  <Point X="-4.393556798386" Y="2.527905477937" Z="1.15" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.15" />
                  <Point X="-4.161891733669" Y="1.967893921913" Z="1.15" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.15" />
                  <Point X="-3.680302199361" Y="1.579598773737" Z="1.15" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.15" />
                  <Point X="-3.680298294316" Y="1.521170772872" Z="1.15" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.15" />
                  <Point X="-3.725054299057" Y="1.483611002958" Z="1.15" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.15" />
                  <Point X="-4.234978979452" Y="1.538299972978" Z="1.15" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.15" />
                  <Point X="-4.901971065093" Y="1.299428578334" Z="1.15" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.15" />
                  <Point X="-5.020668846551" Y="0.714649400672" Z="1.15" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.15" />
                  <Point X="-4.387801197235" Y="0.266440261951" Z="1.15" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.15" />
                  <Point X="-3.561387014679" Y="0.038537603163" Z="1.15" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.15" />
                  <Point X="-3.54374986551" Y="0.013510176796" Z="1.15" />
                  <Point X="-3.539556741714" Y="0" Z="1.15" />
                  <Point X="-3.544614735625" Y="-0.016296774265" Z="1.15" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.15" />
                  <Point X="-3.563981580444" Y="-0.040338379971" Z="1.15" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.15" />
                  <Point X="-4.249086591538" Y="-0.229271794805" Z="1.15" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.15" />
                  <Point X="-5.017863993632" Y="-0.743540257442" Z="1.15" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.15" />
                  <Point X="-4.908436621557" Y="-1.28025932209" Z="1.15" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.15" />
                  <Point X="-4.109118534658" Y="-1.42402869573" Z="1.15" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.15" />
                  <Point X="-3.204679692677" Y="-1.31538513216" Z="1.15" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.15" />
                  <Point X="-3.196888693617" Y="-1.338714000332" Z="1.15" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.15" />
                  <Point X="-3.790755601627" Y="-1.805207667793" Z="1.15" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.15" />
                  <Point X="-4.342406211938" Y="-2.62078031245" Z="1.15" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.15" />
                  <Point X="-4.019571260284" Y="-3.093223691376" Z="1.15" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.15" />
                  <Point X="-3.277810989185" Y="-2.962506444866" Z="1.15" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.15" />
                  <Point X="-2.563354293456" Y="-2.564976175676" Z="1.15" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.15" />
                  <Point X="-2.892910568303" Y="-3.157267393307" Z="1.15" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.15" />
                  <Point X="-3.076061385528" Y="-4.034607213638" Z="1.15" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.15" />
                  <Point X="-2.65025925522" Y="-4.326238096227" Z="1.15" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.15" />
                  <Point X="-2.349182549246" Y="-4.316697069993" Z="1.15" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.15" />
                  <Point X="-2.085180738284" Y="-4.062211097272" Z="1.15" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.15" />
                  <Point X="-1.798989960659" Y="-3.995178661957" Z="1.15" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.15" />
                  <Point X="-1.531132847586" Y="-4.116220404554" Z="1.15" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.15" />
                  <Point X="-1.392312957069" Y="-4.375310207644" Z="1.15" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.15" />
                  <Point X="-1.386734768903" Y="-4.679246736002" Z="1.15" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.15" />
                  <Point X="-1.251428347995" Y="-4.92109971651" Z="1.15" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.15" />
                  <Point X="-0.95352417825" Y="-4.987392803613" Z="1.15" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.15" />
                  <Point X="-0.636102237626" Y="-4.336149952577" Z="1.15" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.15" />
                  <Point X="-0.327569627717" Y="-3.389795928804" Z="1.15" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.15" />
                  <Point X="-0.114836500658" Y="-3.23988038038" Z="1.15" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.15" />
                  <Point X="0.138522578703" Y="-3.247231664908" Z="1.15" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.15" />
                  <Point X="0.342876231715" Y="-3.411849826964" Z="1.15" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.15" />
                  <Point X="0.598652622775" Y="-4.196386077239" Z="1.15" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.15" />
                  <Point X="0.916269045352" Y="-4.995850754123" Z="1.15" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.15" />
                  <Point X="1.095900759719" Y="-4.95954381364" Z="1.15" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.15" />
                  <Point X="1.077469391407" Y="-4.185342972699" Z="1.15" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.15" />
                  <Point X="0.986768397081" Y="-3.137546593711" Z="1.15" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.15" />
                  <Point X="1.109565059097" Y="-2.943505447159" Z="1.15" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.15" />
                  <Point X="1.318582529136" Y="-2.863948407564" Z="1.15" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.15" />
                  <Point X="1.540754437526" Y="-2.929140742709" Z="1.15" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.15" />
                  <Point X="2.101801955131" Y="-3.596525639874" Z="1.15" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.15" />
                  <Point X="2.768785828473" Y="-4.257560419713" Z="1.15" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.15" />
                  <Point X="2.960738896786" Y="-4.126381136638" Z="1.15" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.15" />
                  <Point X="2.69511430578" Y="-3.4564755239" Z="1.15" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.15" />
                  <Point X="2.249899780098" Y="-2.604152451245" Z="1.15" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.15" />
                  <Point X="2.283866993571" Y="-2.408057784782" Z="1.15" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.15" />
                  <Point X="2.42484059359" Y="-2.275034316396" Z="1.15" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.15" />
                  <Point X="2.624354352267" Y="-2.253548229393" Z="1.15" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.15" />
                  <Point X="3.330938073207" Y="-2.622635194495" Z="1.15" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.15" />
                  <Point X="4.160580471297" Y="-2.910869283072" Z="1.15" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.15" />
                  <Point X="4.326920440091" Y="-2.657319907355" Z="1.15" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.15" />
                  <Point X="3.852370824581" Y="-2.120743463248" Z="1.15" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.15" />
                  <Point X="3.137805804721" Y="-1.52914210515" Z="1.15" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.15" />
                  <Point X="3.100862217177" Y="-1.364847385313" Z="1.15" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.15" />
                  <Point X="3.167993379489" Y="-1.215208546291" Z="1.15" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.15" />
                  <Point X="3.317004753485" Y="-1.133807606309" Z="1.15" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.15" />
                  <Point X="4.082676917651" Y="-1.205888714104" Z="1.15" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.15" />
                  <Point X="4.953169245067" Y="-1.112123391744" Z="1.15" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.15" />
                  <Point X="5.022371380808" Y="-0.739250782891" Z="1.15" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.15" />
                  <Point X="4.45875414032" Y="-0.411269300136" Z="1.15" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.15" />
                  <Point X="3.697373412327" Y="-0.191574902187" Z="1.15" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.15" />
                  <Point X="3.62509514229" Y="-0.128668268591" Z="1.15" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.15" />
                  <Point X="3.589820866242" Y="-0.043789294484" Z="1.15" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.15" />
                  <Point X="3.591550584182" Y="0.05282123673" Z="1.15" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.15" />
                  <Point X="3.63028429611" Y="0.135280469984" Z="1.15" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.15" />
                  <Point X="3.706022002026" Y="0.196573942055" Z="1.15" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.15" />
                  <Point X="4.337213676987" Y="0.378702632633" Z="1.15" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.15" />
                  <Point X="5.011983461319" Y="0.800586742641" Z="1.15" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.15" />
                  <Point X="4.926712717109" Y="1.220007811904" Z="1.15" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.15" />
                  <Point X="4.238220852428" Y="1.324067891471" Z="1.15" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.15" />
                  <Point X="3.41163955611" Y="1.228828037834" Z="1.15" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.15" />
                  <Point X="3.33088423061" Y="1.255902287985" Z="1.15" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.15" />
                  <Point X="3.273043367322" Y="1.313608156799" Z="1.15" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.15" />
                  <Point X="3.241600595094" Y="1.393535631651" Z="1.15" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.15" />
                  <Point X="3.245360127151" Y="1.474429089023" Z="1.15" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.15" />
                  <Point X="3.286708047418" Y="1.550528036371" Z="1.15" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.15" />
                  <Point X="3.827077884251" Y="1.979239067042" Z="1.15" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.15" />
                  <Point X="4.332972433677" Y="2.644108468985" Z="1.15" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.15" />
                  <Point X="4.109194571834" Y="2.980013403414" Z="1.15" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.15" />
                  <Point X="3.32582963409" Y="2.73808868267" Z="1.15" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.15" />
                  <Point X="2.465981879757" Y="2.255260394789" Z="1.15" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.15" />
                  <Point X="2.391634019816" Y="2.250106162645" Z="1.15" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.15" />
                  <Point X="2.325553122027" Y="2.277387311336" Z="1.15" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.15" />
                  <Point X="2.273371288899" Y="2.331471738355" Z="1.15" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.15" />
                  <Point X="2.249323540114" Y="2.398124424016" Z="1.15" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.15" />
                  <Point X="2.257267547869" Y="2.473487696125" Z="1.15" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.15" />
                  <Point X="2.65753667019" Y="3.186309447865" Z="1.15" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.15" />
                  <Point X="2.923527160991" Y="4.148116541173" Z="1.15" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.15" />
                  <Point X="2.536038514268" Y="4.3957243076" Z="1.15" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.15" />
                  <Point X="2.130650966854" Y="4.606030640736" Z="1.15" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.15" />
                  <Point X="1.710410957197" Y="4.778042248186" Z="1.15" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.15" />
                  <Point X="1.159069124916" Y="4.934641203982" Z="1.15" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.15" />
                  <Point X="0.496615135178" Y="5.044552030988" Z="1.15" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.15" />
                  <Point X="0.105655321507" Y="4.749435521649" Z="1.15" />
                  <Point X="0" Y="4.355124473572" Z="1.15" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>