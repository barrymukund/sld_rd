<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#171" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2079" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="0" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="0.004716064453" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.776409606934" Y="-4.307853027344" />
                  <Point X="0.563302062988" Y="-3.512524414063" />
                  <Point X="0.557721191406" Y="-3.497141845703" />
                  <Point X="0.54236315918" Y="-3.467377197266" />
                  <Point X="0.45526083374" Y="-3.34187890625" />
                  <Point X="0.378635528564" Y="-3.231476806641" />
                  <Point X="0.356751586914" Y="-3.209021484375" />
                  <Point X="0.330496398926" Y="-3.189777587891" />
                  <Point X="0.302495330811" Y="-3.175669189453" />
                  <Point X="0.167709243774" Y="-3.133836425781" />
                  <Point X="0.049136188507" Y="-3.097035888672" />
                  <Point X="0.020976638794" Y="-3.092766601562" />
                  <Point X="-0.00866468811" Y="-3.092766601562" />
                  <Point X="-0.036824085236" Y="-3.097035888672" />
                  <Point X="-0.171610321045" Y="-3.138868408203" />
                  <Point X="-0.290183227539" Y="-3.175669189453" />
                  <Point X="-0.31818460083" Y="-3.189777587891" />
                  <Point X="-0.344439788818" Y="-3.209021484375" />
                  <Point X="-0.366323577881" Y="-3.231476806641" />
                  <Point X="-0.453426055908" Y="-3.356974853516" />
                  <Point X="-0.530051208496" Y="-3.467377197266" />
                  <Point X="-0.538189025879" Y="-3.481573730469" />
                  <Point X="-0.550990112305" Y="-3.512524414063" />
                  <Point X="-0.738463745117" Y="-4.212185546875" />
                  <Point X="-0.916584594727" Y="-4.876941894531" />
                  <Point X="-0.94333996582" Y="-4.871748535156" />
                  <Point X="-1.079341308594" Y="-4.845350097656" />
                  <Point X="-1.232424926758" Y="-4.805962890625" />
                  <Point X="-1.24641796875" Y="-4.802362792969" />
                  <Point X="-1.240616088867" Y="-4.758293457031" />
                  <Point X="-1.214963012695" Y="-4.563438476562" />
                  <Point X="-1.214201171875" Y="-4.5479296875" />
                  <Point X="-1.216508666992" Y="-4.516224121094" />
                  <Point X="-1.248709106445" Y="-4.354342285156" />
                  <Point X="-1.277036132812" Y="-4.211932617188" />
                  <Point X="-1.287938354492" Y="-4.182966308594" />
                  <Point X="-1.304010375977" Y="-4.15512890625" />
                  <Point X="-1.32364453125" Y="-4.131204101562" />
                  <Point X="-1.447738525391" Y="-4.022376464844" />
                  <Point X="-1.556905517578" Y="-3.926639404297" />
                  <Point X="-1.583188476562" Y="-3.910295410156" />
                  <Point X="-1.612885742188" Y="-3.897994384766" />
                  <Point X="-1.64302734375" Y="-3.890966308594" />
                  <Point X="-1.807727783203" Y="-3.880171142578" />
                  <Point X="-1.952616577148" Y="-3.870674804688" />
                  <Point X="-1.98341796875" Y="-3.873708496094" />
                  <Point X="-2.014466674805" Y="-3.882028076172" />
                  <Point X="-2.042657714844" Y="-3.894801513672" />
                  <Point X="-2.179895019531" Y="-3.986500488281" />
                  <Point X="-2.300624267578" Y="-4.067169677734" />
                  <Point X="-2.312785644531" Y="-4.076821533203" />
                  <Point X="-2.335102783203" Y="-4.099461914062" />
                  <Point X="-2.440354736328" Y="-4.23662890625" />
                  <Point X="-2.480149169922" Y="-4.288489746094" />
                  <Point X="-2.602365722656" Y="-4.21281640625" />
                  <Point X="-2.801706787109" Y="-4.089389404297" />
                  <Point X="-3.013684326172" Y="-3.926173828125" />
                  <Point X="-3.104721923828" Y="-3.856077880859" />
                  <Point X="-2.819797851562" Y="-3.362574951172" />
                  <Point X="-2.423761230469" Y="-2.676619628906" />
                  <Point X="-2.412859130859" Y="-2.647653320312" />
                  <Point X="-2.406588134766" Y="-2.616126953125" />
                  <Point X="-2.405575439453" Y="-2.585192871094" />
                  <Point X="-2.414560058594" Y="-2.555575195312" />
                  <Point X="-2.428777099609" Y="-2.526746337891" />
                  <Point X="-2.4468046875" Y="-2.501589355469" />
                  <Point X="-2.464153320312" Y="-2.484240722656" />
                  <Point X="-2.489310302734" Y="-2.466213134766" />
                  <Point X="-2.518139160156" Y="-2.45199609375" />
                  <Point X="-2.547756835938" Y="-2.443011474609" />
                  <Point X="-2.578690917969" Y="-2.444024169922" />
                  <Point X="-2.610217285156" Y="-2.450295166016" />
                  <Point X="-2.63918359375" Y="-2.461197265625" />
                  <Point X="-3.242627441406" Y="-2.809595703125" />
                  <Point X="-3.818024658203" Y="-3.141801269531" />
                  <Point X="-3.925368164062" Y="-3.000773925781" />
                  <Point X="-4.082859375" Y="-2.793862548828" />
                  <Point X="-4.234828125" Y="-2.539033935547" />
                  <Point X="-4.306142578125" Y="-2.419450439453" />
                  <Point X="-3.798889892578" Y="-2.030221435547" />
                  <Point X="-3.105954589844" Y="-1.498513549805" />
                  <Point X="-3.084576904297" Y="-1.475592895508" />
                  <Point X="-3.066612060547" Y="-1.448461181641" />
                  <Point X="-3.053856445312" Y="-1.419832275391" />
                  <Point X="-3.046151855469" Y="-1.390084960938" />
                  <Point X="-3.04334765625" Y="-1.359655761719" />
                  <Point X="-3.045556640625" Y="-1.327985351562" />
                  <Point X="-3.052558105469" Y="-1.298240112305" />
                  <Point X="-3.068640625" Y="-1.272256591797" />
                  <Point X="-3.089473388672" Y="-1.248300170898" />
                  <Point X="-3.112972900391" Y="-1.228766723633" />
                  <Point X="-3.139455566406" Y="-1.213180297852" />
                  <Point X="-3.168718505859" Y="-1.201956420898" />
                  <Point X="-3.200605957031" Y="-1.195474853516" />
                  <Point X="-3.231929199219" Y="-1.194383789062" />
                  <Point X="-3.993719970703" Y="-1.294675415039" />
                  <Point X="-4.732102050781" Y="-1.391885375977" />
                  <Point X="-4.77248046875" Y="-1.23380456543" />
                  <Point X="-4.834077636719" Y="-0.992654418945" />
                  <Point X="-4.874284179688" Y="-0.711532897949" />
                  <Point X="-4.892424316406" Y="-0.584698181152" />
                  <Point X="-4.322233886719" Y="-0.431915985107" />
                  <Point X="-3.532875976562" Y="-0.220408340454" />
                  <Point X="-3.510055175781" Y="-0.210970474243" />
                  <Point X="-3.47972265625" Y="-0.193502731323" />
                  <Point X="-3.466557861328" Y="-0.184361465454" />
                  <Point X="-3.441750732422" Y="-0.163828933716" />
                  <Point X="-3.425645019531" Y="-0.146728042603" />
                  <Point X="-3.414227783203" Y="-0.1261979599" />
                  <Point X="-3.401880615234" Y="-0.095602935791" />
                  <Point X="-3.397365478516" Y="-0.081218666077" />
                  <Point X="-3.390789306641" Y="-0.052448162079" />
                  <Point X="-3.388401123047" Y="-0.031049869537" />
                  <Point X="-3.390892822266" Y="-0.009663302422" />
                  <Point X="-3.398061279297" Y="0.021014450073" />
                  <Point X="-3.402663330078" Y="0.035419361115" />
                  <Point X="-3.414418457031" Y="0.064106529236" />
                  <Point X="-3.425944824219" Y="0.084575592041" />
                  <Point X="-3.442141357422" Y="0.101590568542" />
                  <Point X="-3.468724121094" Y="0.12335533905" />
                  <Point X="-3.481955078125" Y="0.132436660767" />
                  <Point X="-3.51051171875" Y="0.148671707153" />
                  <Point X="-3.532875976562" Y="0.157848342896" />
                  <Point X="-4.227284667969" Y="0.343914398193" />
                  <Point X="-4.89181640625" Y="0.521975219727" />
                  <Point X="-4.864185546875" Y="0.708702575684" />
                  <Point X="-4.824488769531" Y="0.976970214844" />
                  <Point X="-4.743552246094" Y="1.275651245117" />
                  <Point X="-4.703551269531" Y="1.423267822266" />
                  <Point X="-4.331497070313" Y="1.374286010742" />
                  <Point X="-3.765666259766" Y="1.29979284668" />
                  <Point X="-3.744985839844" Y="1.299341674805" />
                  <Point X="-3.723424072266" Y="1.301228271484" />
                  <Point X="-3.703137695312" Y="1.305263671875" />
                  <Point X="-3.670459228516" Y="1.315567016602" />
                  <Point X="-3.641711669922" Y="1.324631225586" />
                  <Point X="-3.622778564453" Y="1.332961669922" />
                  <Point X="-3.604034423828" Y="1.343783569336" />
                  <Point X="-3.587353027344" Y="1.356015014648" />
                  <Point X="-3.573714599609" Y="1.371566894531" />
                  <Point X="-3.561300292969" Y="1.389296630859" />
                  <Point X="-3.5513515625" Y="1.407431030273" />
                  <Point X="-3.538239257812" Y="1.439087036133" />
                  <Point X="-3.526704101563" Y="1.466935302734" />
                  <Point X="-3.520916015625" Y="1.48679309082" />
                  <Point X="-3.517157470703" Y="1.508108154297" />
                  <Point X="-3.515804443359" Y="1.528748779297" />
                  <Point X="-3.518951171875" Y="1.549192871094" />
                  <Point X="-3.524552978516" Y="1.570099121094" />
                  <Point X="-3.532049804688" Y="1.589377807617" />
                  <Point X="-3.547871337891" Y="1.619770629883" />
                  <Point X="-3.561789550781" Y="1.646507446289" />
                  <Point X="-3.57328125" Y="1.663705932617" />
                  <Point X="-3.587193847656" Y="1.680286376953" />
                  <Point X="-3.602135986328" Y="1.694590209961" />
                  <Point X="-4.000450439453" Y="2.000227539063" />
                  <Point X="-4.351859863281" Y="2.269874267578" />
                  <Point X="-4.23540234375" Y="2.469394287109" />
                  <Point X="-4.081153808594" Y="2.733659423828" />
                  <Point X="-3.866760498047" Y="3.009231689453" />
                  <Point X="-3.750504882813" Y="3.158661621094" />
                  <Point X="-3.554174560547" Y="3.045310302734" />
                  <Point X="-3.206656982422" Y="2.844670898438" />
                  <Point X="-3.187724609375" Y="2.836340332031" />
                  <Point X="-3.167082519531" Y="2.829831787109" />
                  <Point X="-3.146794433594" Y="2.825796386719" />
                  <Point X="-3.101282470703" Y="2.821814697266" />
                  <Point X="-3.061245117188" Y="2.818311767578" />
                  <Point X="-3.040565185547" Y="2.818762939453" />
                  <Point X="-3.019106445312" Y="2.821588134766" />
                  <Point X="-2.999015136719" Y="2.826504394531" />
                  <Point X="-2.980463867188" Y="2.835652832031" />
                  <Point X="-2.962209472656" Y="2.847281982422" />
                  <Point X="-2.946077392578" Y="2.860229492188" />
                  <Point X="-2.913772705078" Y="2.892534179688" />
                  <Point X="-2.885353759766" Y="2.920952880859" />
                  <Point X="-2.872406738281" Y="2.937084228516" />
                  <Point X="-2.86077734375" Y="2.955338623047" />
                  <Point X="-2.851628662109" Y="2.973890136719" />
                  <Point X="-2.846712158203" Y="2.993981933594" />
                  <Point X="-2.843886962891" Y="3.015440673828" />
                  <Point X="-2.843435791016" Y="3.03612109375" />
                  <Point X="-2.847417724609" Y="3.081633056641" />
                  <Point X="-2.850920410156" Y="3.121670654297" />
                  <Point X="-2.854955810547" Y="3.141957763672" />
                  <Point X="-2.861464355469" Y="3.162600097656" />
                  <Point X="-2.869794921875" Y="3.181532958984" />
                  <Point X="-3.046300048828" Y="3.487248779297" />
                  <Point X="-3.183333007812" Y="3.724596435547" />
                  <Point X="-2.969273925781" Y="3.888713378906" />
                  <Point X="-2.700626464844" Y="4.094683349609" />
                  <Point X="-2.36296484375" Y="4.282280761719" />
                  <Point X="-2.167037109375" Y="4.391133789062" />
                  <Point X="-2.149581298828" Y="4.368385253906" />
                  <Point X="-2.04319519043" Y="4.229740722656" />
                  <Point X="-2.028891723633" Y="4.214799316406" />
                  <Point X="-2.012312011719" Y="4.200887207031" />
                  <Point X="-1.99511315918" Y="4.189395019531" />
                  <Point X="-1.944458496094" Y="4.163025878906" />
                  <Point X="-1.899896972656" Y="4.139828125" />
                  <Point X="-1.880619262695" Y="4.132331542969" />
                  <Point X="-1.859712768555" Y="4.126729492187" />
                  <Point X="-1.839268798828" Y="4.123582519531" />
                  <Point X="-1.818628295898" Y="4.124935546875" />
                  <Point X="-1.797313110352" Y="4.128693847656" />
                  <Point X="-1.777453491211" Y="4.134482421875" />
                  <Point X="-1.724693237305" Y="4.156336914062" />
                  <Point X="-1.678279296875" Y="4.175562011719" />
                  <Point X="-1.660146362305" Y="4.185509765625" />
                  <Point X="-1.642416625977" Y="4.197923828125" />
                  <Point X="-1.626864379883" Y="4.2115625" />
                  <Point X="-1.6146328125" Y="4.228244140625" />
                  <Point X="-1.603810913086" Y="4.24698828125" />
                  <Point X="-1.59548046875" Y="4.265920898438" />
                  <Point X="-1.578307861328" Y="4.320384765625" />
                  <Point X="-1.563201171875" Y="4.368297363281" />
                  <Point X="-1.559165649414" Y="4.388584960938" />
                  <Point X="-1.557279174805" Y="4.410146972656" />
                  <Point X="-1.55773034668" Y="4.430826660156" />
                  <Point X="-1.57779699707" Y="4.583248046875" />
                  <Point X="-1.584201904297" Y="4.631897949219" />
                  <Point X="-1.297417236328" Y="4.712302246094" />
                  <Point X="-0.94963494873" Y="4.809808105469" />
                  <Point X="-0.540293273926" Y="4.857715820312" />
                  <Point X="-0.29471081543" Y="4.886457519531" />
                  <Point X="-0.236708679199" Y="4.669990234375" />
                  <Point X="-0.133903305054" Y="4.286315429688" />
                  <Point X="-0.121129844666" Y="4.258124023438" />
                  <Point X="-0.10327155304" Y="4.231397460938" />
                  <Point X="-0.082114532471" Y="4.20880859375" />
                  <Point X="-0.054819446564" Y="4.19421875" />
                  <Point X="-0.024381370544" Y="4.183886230469" />
                  <Point X="0.006155906677" Y="4.178844238281" />
                  <Point X="0.03669316864" Y="4.183886230469" />
                  <Point X="0.067131248474" Y="4.19421875" />
                  <Point X="0.094426467896" Y="4.20880859375" />
                  <Point X="0.115583503723" Y="4.231397460938" />
                  <Point X="0.133441802979" Y="4.258124023438" />
                  <Point X="0.146215255737" Y="4.286315429688" />
                  <Point X="0.236654418945" Y="4.623839355469" />
                  <Point X="0.307419403076" Y="4.8879375" />
                  <Point X="0.540370300293" Y="4.863541503906" />
                  <Point X="0.844041137695" Y="4.831738769531" />
                  <Point X="1.182705810547" Y="4.749974609375" />
                  <Point X="1.481027099609" Y="4.677950683594" />
                  <Point X="1.700771240234" Y="4.598248046875" />
                  <Point X="1.894645874023" Y="4.527928222656" />
                  <Point X="2.107800537109" Y="4.428242675781" />
                  <Point X="2.294577880859" Y="4.340893066406" />
                  <Point X="2.50051953125" Y="4.220911132812" />
                  <Point X="2.680977294922" Y="4.115775878906" />
                  <Point X="2.8751875" Y="3.977664306641" />
                  <Point X="2.943260009766" Y="3.929254882812" />
                  <Point X="2.605963623047" Y="3.345039794922" />
                  <Point X="2.147581054688" Y="2.551097900391" />
                  <Point X="2.142075683594" Y="2.539930664062" />
                  <Point X="2.133076904297" Y="2.516056884766" />
                  <Point X="2.121655029297" Y="2.473344970703" />
                  <Point X="2.111607177734" Y="2.435770507813" />
                  <Point X="2.108619384766" Y="2.417934326172" />
                  <Point X="2.107728027344" Y="2.380952880859" />
                  <Point X="2.112181640625" Y="2.344019287109" />
                  <Point X="2.116099365234" Y="2.311528076172" />
                  <Point X="2.121441894531" Y="2.289605224609" />
                  <Point X="2.129708007812" Y="2.267516357422" />
                  <Point X="2.140070800781" Y="2.247471191406" />
                  <Point X="2.162924072266" Y="2.213791259766" />
                  <Point X="2.183028564453" Y="2.184162597656" />
                  <Point X="2.194465332031" Y="2.170327880859" />
                  <Point X="2.221598876953" Y="2.145592529297" />
                  <Point X="2.255278808594" Y="2.122739257812" />
                  <Point X="2.284907470703" Y="2.102635009766" />
                  <Point X="2.304951904297" Y="2.092272460938" />
                  <Point X="2.327040283203" Y="2.084006347656" />
                  <Point X="2.348963867188" Y="2.078663330078" />
                  <Point X="2.385897705078" Y="2.074209716797" />
                  <Point X="2.418388671875" Y="2.070291748047" />
                  <Point X="2.436467529297" Y="2.069845703125" />
                  <Point X="2.473206298828" Y="2.074171142578" />
                  <Point X="2.515918457031" Y="2.085593017578" />
                  <Point X="2.553492675781" Y="2.095640869141" />
                  <Point X="2.565283447266" Y="2.099638183594" />
                  <Point X="2.588533935547" Y="2.110145019531" />
                  <Point X="3.286975097656" Y="2.513390136719" />
                  <Point X="3.967326660156" Y="2.906191162109" />
                  <Point X="4.016229248047" Y="2.838227539063" />
                  <Point X="4.123270507813" Y="2.689464355469" />
                  <Point X="4.231541015625" Y="2.510546630859" />
                  <Point X="4.262198730469" Y="2.459884277344" />
                  <Point X="3.834287597656" Y="2.131535888672" />
                  <Point X="3.230783935547" Y="1.668451293945" />
                  <Point X="3.221423828125" Y="1.660240722656" />
                  <Point X="3.203973876953" Y="1.641627807617" />
                  <Point X="3.173233886719" Y="1.601525268555" />
                  <Point X="3.146191650391" Y="1.566246459961" />
                  <Point X="3.136605712891" Y="1.550911376953" />
                  <Point X="3.121629882812" Y="1.51708605957" />
                  <Point X="3.110179199219" Y="1.476141113281" />
                  <Point X="3.100105957031" Y="1.440121704102" />
                  <Point X="3.096652587891" Y="1.417823242188" />
                  <Point X="3.095836425781" Y="1.394252929688" />
                  <Point X="3.097739257812" Y="1.371768188477" />
                  <Point X="3.107138916016" Y="1.326211791992" />
                  <Point X="3.115408203125" Y="1.286135498047" />
                  <Point X="3.120679931641" Y="1.268977416992" />
                  <Point X="3.136282714844" Y="1.235740234375" />
                  <Point X="3.161849121094" Y="1.196880615234" />
                  <Point X="3.184340332031" Y="1.162694946289" />
                  <Point X="3.198893554688" Y="1.145450439453" />
                  <Point X="3.216137451172" Y="1.129360595703" />
                  <Point X="3.234347167969" Y="1.116034912109" />
                  <Point X="3.271396484375" Y="1.095179199219" />
                  <Point X="3.303989257812" Y="1.076832397461" />
                  <Point X="3.320521240234" Y="1.069501586914" />
                  <Point X="3.356118164062" Y="1.059438598633" />
                  <Point X="3.406211181641" Y="1.052818115234" />
                  <Point X="3.450278808594" Y="1.046994018555" />
                  <Point X="3.462702148438" Y="1.046175048828" />
                  <Point X="3.488203613281" Y="1.04698449707" />
                  <Point X="4.15167578125" Y="1.134332397461" />
                  <Point X="4.776839355469" Y="1.21663659668" />
                  <Point X="4.799961914062" Y="1.12165637207" />
                  <Point X="4.845936035156" Y="0.932809448242" />
                  <Point X="4.8800546875" Y="0.713670837402" />
                  <Point X="4.890864746094" Y="0.644238464355" />
                  <Point X="4.408208007812" Y="0.514911010742" />
                  <Point X="3.716579833984" Y="0.329589935303" />
                  <Point X="3.704791259766" Y="0.325586608887" />
                  <Point X="3.681545654297" Y="0.315067993164" />
                  <Point X="3.632330810547" Y="0.286620788574" />
                  <Point X="3.589035644531" Y="0.261595428467" />
                  <Point X="3.574311035156" Y="0.251096221924" />
                  <Point X="3.547530761719" Y="0.22557661438" />
                  <Point X="3.518001953125" Y="0.187949905396" />
                  <Point X="3.492024902344" Y="0.154849121094" />
                  <Point X="3.48030078125" Y="0.13556854248" />
                  <Point X="3.470527099609" Y="0.114104774475" />
                  <Point X="3.463680908203" Y="0.092604118347" />
                  <Point X="3.453837890625" Y="0.041207736969" />
                  <Point X="3.445178710938" Y="-0.004006225586" />
                  <Point X="3.443483154297" Y="-0.021875144958" />
                  <Point X="3.445178710938" Y="-0.05855393219" />
                  <Point X="3.455021728516" Y="-0.109950164795" />
                  <Point X="3.463680908203" Y="-0.155164276123" />
                  <Point X="3.470527099609" Y="-0.176665084839" />
                  <Point X="3.48030078125" Y="-0.198128707886" />
                  <Point X="3.492024902344" Y="-0.217408813477" />
                  <Point X="3.521553710938" Y="-0.255035690308" />
                  <Point X="3.547530761719" Y="-0.288136474609" />
                  <Point X="3.559999023438" Y="-0.30123614502" />
                  <Point X="3.589035888672" Y="-0.324155578613" />
                  <Point X="3.638250976562" Y="-0.352602752686" />
                  <Point X="3.681545898438" Y="-0.377627990723" />
                  <Point X="3.692709228516" Y="-0.383138458252" />
                  <Point X="3.716579833984" Y="-0.392149932861" />
                  <Point X="4.325014160156" Y="-0.555179382324" />
                  <Point X="4.891472167969" Y="-0.706961425781" />
                  <Point X="4.880692382812" Y="-0.778462158203" />
                  <Point X="4.855022460938" Y="-0.948726623535" />
                  <Point X="4.811311035156" Y="-1.14027722168" />
                  <Point X="4.801173828125" Y="-1.18469909668" />
                  <Point X="4.226476074219" Y="-1.109038574219" />
                  <Point X="3.424382080078" Y="-1.003441040039" />
                  <Point X="3.408035644531" Y="-1.002710327148" />
                  <Point X="3.374658691406" Y="-1.005508728027" />
                  <Point X="3.278067138672" Y="-1.026503295898" />
                  <Point X="3.193094482422" Y="-1.044972412109" />
                  <Point X="3.163974121094" Y="-1.056596923828" />
                  <Point X="3.136147460938" Y="-1.073489257812" />
                  <Point X="3.112397460938" Y="-1.093960083008" />
                  <Point X="3.054013916016" Y="-1.164177246094" />
                  <Point X="3.002653320312" Y="-1.225948364258" />
                  <Point X="2.987932617188" Y="-1.250330566406" />
                  <Point X="2.976589355469" Y="-1.277715698242" />
                  <Point X="2.969757568359" Y="-1.305365478516" />
                  <Point X="2.961389892578" Y="-1.396299804688" />
                  <Point X="2.954028564453" Y="-1.476295776367" />
                  <Point X="2.956347412109" Y="-1.507564819336" />
                  <Point X="2.964079101562" Y="-1.539185791016" />
                  <Point X="2.976450439453" Y="-1.567996826172" />
                  <Point X="3.029905517578" Y="-1.651142700195" />
                  <Point X="3.076930664062" Y="-1.724287109375" />
                  <Point X="3.086932861328" Y="-1.737238647461" />
                  <Point X="3.110628417969" Y="-1.760909179688" />
                  <Point X="3.675259277344" Y="-2.194165771484" />
                  <Point X="4.213122558594" Y="-2.6068828125" />
                  <Point X="4.197171875" Y="-2.632693359375" />
                  <Point X="4.124809082031" Y="-2.749787597656" />
                  <Point X="4.034411132812" Y="-2.878229980469" />
                  <Point X="4.028981445312" Y="-2.885945068359" />
                  <Point X="3.515327636719" Y="-2.58938671875" />
                  <Point X="2.800954589844" Y="-2.176943115234" />
                  <Point X="2.786131591797" Y="-2.170012451172" />
                  <Point X="2.754224609375" Y="-2.159825195312" />
                  <Point X="2.639265380859" Y="-2.139063720703" />
                  <Point X="2.538134277344" Y="-2.120799560547" />
                  <Point X="2.506781982422" Y="-2.120395263672" />
                  <Point X="2.474609130859" Y="-2.125353027344" />
                  <Point X="2.444833251953" Y="-2.135176757812" />
                  <Point X="2.349330322266" Y="-2.185439208984" />
                  <Point X="2.265315185547" Y="-2.229655761719" />
                  <Point X="2.242384521484" Y="-2.246548828125" />
                  <Point X="2.221424804688" Y="-2.267508544922" />
                  <Point X="2.204531738281" Y="-2.290439208984" />
                  <Point X="2.154269287109" Y="-2.385942138672" />
                  <Point X="2.110052734375" Y="-2.469957275391" />
                  <Point X="2.100229003906" Y="-2.499733154297" />
                  <Point X="2.095271240234" Y="-2.531906005859" />
                  <Point X="2.095675537109" Y="-2.563258300781" />
                  <Point X="2.116437011719" Y="-2.678217529297" />
                  <Point X="2.134701171875" Y="-2.779348632812" />
                  <Point X="2.138985595703" Y="-2.795141845703" />
                  <Point X="2.151819091797" Y="-2.826078613281" />
                  <Point X="2.514651123047" Y="-3.454521972656" />
                  <Point X="2.861283203125" Y="-4.05490625" />
                  <Point X="2.781847412109" Y="-4.111645507812" />
                  <Point X="2.701765136719" Y="-4.163481445312" />
                  <Point X="2.303678710938" Y="-3.644685546875" />
                  <Point X="1.758546142578" Y="-2.934255126953" />
                  <Point X="1.747503540039" Y="-2.922178955078" />
                  <Point X="1.721924194336" Y="-2.900557373047" />
                  <Point X="1.608543212891" Y="-2.827663818359" />
                  <Point X="1.50880078125" Y="-2.763538574219" />
                  <Point X="1.479989746094" Y="-2.751167236328" />
                  <Point X="1.448368408203" Y="-2.743435546875" />
                  <Point X="1.417099487305" Y="-2.741116699219" />
                  <Point X="1.293098266602" Y="-2.75252734375" />
                  <Point X="1.184012573242" Y="-2.762565673828" />
                  <Point X="1.156362670898" Y="-2.769397460938" />
                  <Point X="1.128977661133" Y="-2.780740722656" />
                  <Point X="1.104595825195" Y="-2.795461181641" />
                  <Point X="1.008845153809" Y="-2.875074707031" />
                  <Point X="0.924612121582" Y="-2.945111572266" />
                  <Point X="0.904141479492" Y="-2.968861572266" />
                  <Point X="0.887249084473" Y="-2.996688232422" />
                  <Point X="0.875624328613" Y="-3.025808837891" />
                  <Point X="0.846995300293" Y="-3.157524658203" />
                  <Point X="0.821810058594" Y="-3.273396728516" />
                  <Point X="0.819724487305" Y="-3.289627441406" />
                  <Point X="0.819742248535" Y="-3.323120117188" />
                  <Point X="0.922566162109" Y="-4.10414453125" />
                  <Point X="1.022065246582" Y="-4.859915039062" />
                  <Point X="0.975678344727" Y="-4.870083007812" />
                  <Point X="0.929315551758" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058435424805" Y="-4.752635253906" />
                  <Point X="-1.141246337891" Y="-4.731328613281" />
                  <Point X="-1.120775634766" Y="-4.575838378906" />
                  <Point X="-1.120077514648" Y="-4.568099609375" />
                  <Point X="-1.119451782227" Y="-4.541033691406" />
                  <Point X="-1.121759277344" Y="-4.509328125" />
                  <Point X="-1.123334106445" Y="-4.497690429688" />
                  <Point X="-1.155534545898" Y="-4.33580859375" />
                  <Point X="-1.183861450195" Y="-4.193398925781" />
                  <Point X="-1.188125" Y="-4.17846875" />
                  <Point X="-1.19902734375" Y="-4.149502441406" />
                  <Point X="-1.205666015625" Y="-4.135466308594" />
                  <Point X="-1.221738037109" Y="-4.10762890625" />
                  <Point X="-1.230573730469" Y="-4.094862304688" />
                  <Point X="-1.250207763672" Y="-4.0709375" />
                  <Point X="-1.261006591797" Y="-4.059779296875" />
                  <Point X="-1.385100585938" Y="-3.950951660156" />
                  <Point X="-1.494267578125" Y="-3.855214599609" />
                  <Point X="-1.506738647461" Y="-3.845965576172" />
                  <Point X="-1.533021606445" Y="-3.829621582031" />
                  <Point X="-1.546833618164" Y="-3.822526855469" />
                  <Point X="-1.576530761719" Y="-3.810225830078" />
                  <Point X="-1.591313476562" Y="-3.805476074219" />
                  <Point X="-1.621454833984" Y="-3.798447998047" />
                  <Point X="-1.636813964844" Y="-3.796169677734" />
                  <Point X="-1.801514404297" Y="-3.785374511719" />
                  <Point X="-1.946403198242" Y="-3.775878173828" />
                  <Point X="-1.961928222656" Y="-3.776132324219" />
                  <Point X="-1.992729614258" Y="-3.779166015625" />
                  <Point X="-2.008006103516" Y="-3.781945556641" />
                  <Point X="-2.03905480957" Y="-3.790265136719" />
                  <Point X="-2.053674560547" Y="-3.795496337891" />
                  <Point X="-2.081865478516" Y="-3.808269775391" />
                  <Point X="-2.095436767578" Y="-3.815811767578" />
                  <Point X="-2.232674072266" Y="-3.907510742188" />
                  <Point X="-2.353403320312" Y="-3.988179931641" />
                  <Point X="-2.359681640625" Y="-3.992757080078" />
                  <Point X="-2.380442138672" Y="-4.010131103516" />
                  <Point X="-2.402759277344" Y="-4.032771484375" />
                  <Point X="-2.410471435547" Y="-4.041629638672" />
                  <Point X="-2.503202880859" Y="-4.162479492188" />
                  <Point X="-2.552354736328" Y="-4.132045898438" />
                  <Point X="-2.747581787109" Y="-4.011166259766" />
                  <Point X="-2.955727050781" Y="-3.850901367188" />
                  <Point X="-2.980862792969" Y="-3.831547607422" />
                  <Point X="-2.737525390625" Y="-3.410074951172" />
                  <Point X="-2.341488769531" Y="-2.724119628906" />
                  <Point X="-2.334850097656" Y="-2.710083251953" />
                  <Point X="-2.323947998047" Y="-2.681116943359" />
                  <Point X="-2.319684570312" Y="-2.666187011719" />
                  <Point X="-2.313413574219" Y="-2.634660644531" />
                  <Point X="-2.311638916016" Y="-2.619235351562" />
                  <Point X="-2.310626220703" Y="-2.588301269531" />
                  <Point X="-2.314666259766" Y="-2.557615234375" />
                  <Point X="-2.323650878906" Y="-2.527997558594" />
                  <Point X="-2.329357421875" Y="-2.513557128906" />
                  <Point X="-2.343574462891" Y="-2.484728271484" />
                  <Point X="-2.351557128906" Y="-2.47141015625" />
                  <Point X="-2.369584716797" Y="-2.446253173828" />
                  <Point X="-2.379629638672" Y="-2.434414306641" />
                  <Point X="-2.396978271484" Y="-2.417065673828" />
                  <Point X="-2.408817138672" Y="-2.407020751953" />
                  <Point X="-2.433974121094" Y="-2.388993164062" />
                  <Point X="-2.447292236328" Y="-2.381010498047" />
                  <Point X="-2.47612109375" Y="-2.366793457031" />
                  <Point X="-2.490561523438" Y="-2.361086914062" />
                  <Point X="-2.520179199219" Y="-2.352102294922" />
                  <Point X="-2.550865234375" Y="-2.348062255859" />
                  <Point X="-2.581799316406" Y="-2.349074951172" />
                  <Point X="-2.597224609375" Y="-2.350849609375" />
                  <Point X="-2.628750976562" Y="-2.357120605469" />
                  <Point X="-2.643680908203" Y="-2.361384033203" />
                  <Point X="-2.672647216797" Y="-2.372286132812" />
                  <Point X="-2.68668359375" Y="-2.378924804688" />
                  <Point X="-3.290127441406" Y="-2.727323242188" />
                  <Point X="-3.79308984375" Y="-3.017708496094" />
                  <Point X="-3.849774902344" Y="-2.943235839844" />
                  <Point X="-4.004014648438" Y="-2.740595947266" />
                  <Point X="-4.153235351563" Y="-2.490375488281" />
                  <Point X="-4.181265136719" Y="-2.443373535156" />
                  <Point X="-3.741057617188" Y="-2.105590087891" />
                  <Point X="-3.048122314453" Y="-1.573882080078" />
                  <Point X="-3.036481689453" Y="-1.563309570312" />
                  <Point X="-3.015104003906" Y="-1.540388916016" />
                  <Point X="-3.005366943359" Y="-1.528040527344" />
                  <Point X="-2.987402099609" Y="-1.500908813477" />
                  <Point X="-2.979835693359" Y="-1.487124389648" />
                  <Point X="-2.967080078125" Y="-1.458495483398" />
                  <Point X="-2.961890869141" Y="-1.443651489258" />
                  <Point X="-2.954186279297" Y="-1.413904174805" />
                  <Point X="-2.951552734375" Y="-1.398802734375" />
                  <Point X="-2.948748535156" Y="-1.368373535156" />
                  <Point X="-2.948577880859" Y="-1.353045532227" />
                  <Point X="-2.950786865234" Y="-1.321375244141" />
                  <Point X="-2.953083740234" Y="-1.306218994141" />
                  <Point X="-2.960085205078" Y="-1.276473754883" />
                  <Point X="-2.971779296875" Y="-1.24824206543" />
                  <Point X="-2.987861816406" Y="-1.222258666992" />
                  <Point X="-2.996954833984" Y="-1.209917724609" />
                  <Point X="-3.017787597656" Y="-1.185961303711" />
                  <Point X="-3.028746582031" Y="-1.175243530273" />
                  <Point X="-3.05224609375" Y="-1.155710083008" />
                  <Point X="-3.064786621094" Y="-1.146894287109" />
                  <Point X="-3.091269287109" Y="-1.131307739258" />
                  <Point X="-3.105434570312" Y="-1.124480957031" />
                  <Point X="-3.134697509766" Y="-1.113257080078" />
                  <Point X="-3.149795410156" Y="-1.108860107422" />
                  <Point X="-3.181682861328" Y="-1.102378540039" />
                  <Point X="-3.197298828125" Y="-1.100532470703" />
                  <Point X="-3.228622070312" Y="-1.09944140625" />
                  <Point X="-3.244329101562" Y="-1.100196533203" />
                  <Point X="-4.006119873047" Y="-1.20048815918" />
                  <Point X="-4.660920410156" Y="-1.286694458008" />
                  <Point X="-4.680435546875" Y="-1.210293701172" />
                  <Point X="-4.740762207031" Y="-0.974118103027" />
                  <Point X="-4.780241210938" Y="-0.698082763672" />
                  <Point X="-4.786452148438" Y="-0.654654296875" />
                  <Point X="-4.297645996094" Y="-0.523678955078" />
                  <Point X="-3.508288085938" Y="-0.312171325684" />
                  <Point X="-3.496569824219" Y="-0.308197021484" />
                  <Point X="-3.473749023438" Y="-0.298759155273" />
                  <Point X="-3.462646240234" Y="-0.293295349121" />
                  <Point X="-3.432313720703" Y="-0.275827697754" />
                  <Point X="-3.425538818359" Y="-0.271535461426" />
                  <Point X="-3.405984619141" Y="-0.257545288086" />
                  <Point X="-3.381177490234" Y="-0.237012741089" />
                  <Point X="-3.372593505859" Y="-0.228961685181" />
                  <Point X="-3.356487792969" Y="-0.211860687256" />
                  <Point X="-3.342620117188" Y="-0.192900115967" />
                  <Point X="-3.331202880859" Y="-0.172369949341" />
                  <Point X="-3.326131347656" Y="-0.161750839233" />
                  <Point X="-3.313784179688" Y="-0.131155776978" />
                  <Point X="-3.311240966797" Y="-0.124054130554" />
                  <Point X="-3.30475390625" Y="-0.102387138367" />
                  <Point X="-3.298177734375" Y="-0.073616714478" />
                  <Point X="-3.296375488281" Y="-0.062985275269" />
                  <Point X="-3.293987304688" Y="-0.041586997986" />
                  <Point X="-3.294039306641" Y="-0.020055997849" />
                  <Point X="-3.296531005859" Y="0.001330541372" />
                  <Point X="-3.298384765625" Y="0.011953065872" />
                  <Point X="-3.305553222656" Y="0.042630764008" />
                  <Point X="-3.307567382812" Y="0.049925327301" />
                  <Point X="-3.314757324219" Y="0.071440574646" />
                  <Point X="-3.326512451172" Y="0.100127761841" />
                  <Point X="-3.331640380859" Y="0.110719818115" />
                  <Point X="-3.343166748047" Y="0.131188903809" />
                  <Point X="-3.357135253906" Y="0.150075454712" />
                  <Point X="-3.373331787109" Y="0.167090408325" />
                  <Point X="-3.381958496094" Y="0.175095825195" />
                  <Point X="-3.408541259766" Y="0.196860610962" />
                  <Point X="-3.414964111328" Y="0.201680603027" />
                  <Point X="-3.435002929688" Y="0.215023071289" />
                  <Point X="-3.463559570312" Y="0.231258041382" />
                  <Point X="-3.474448730469" Y="0.23656060791" />
                  <Point X="-3.496812988281" Y="0.245737182617" />
                  <Point X="-3.508288085938" Y="0.249611343384" />
                  <Point X="-4.202696777344" Y="0.435677398682" />
                  <Point X="-4.785446289062" Y="0.591824707031" />
                  <Point X="-4.770208984375" Y="0.694796386719" />
                  <Point X="-4.73133203125" Y="0.957522827148" />
                  <Point X="-4.651858886719" Y="1.250804321289" />
                  <Point X="-4.6335859375" Y="1.318237060547" />
                  <Point X="-4.343896972656" Y="1.280098754883" />
                  <Point X="-3.778066162109" Y="1.20560559082" />
                  <Point X="-3.76773828125" Y="1.204815429688" />
                  <Point X="-3.747057861328" Y="1.204364257812" />
                  <Point X="-3.736705322266" Y="1.20470324707" />
                  <Point X="-3.715143554688" Y="1.20658984375" />
                  <Point X="-3.704889648438" Y="1.208053833008" />
                  <Point X="-3.684603271484" Y="1.212089233398" />
                  <Point X="-3.674571044922" Y="1.214660400391" />
                  <Point X="-3.641892578125" Y="1.224963745117" />
                  <Point X="-3.613145019531" Y="1.234027954102" />
                  <Point X="-3.603451904297" Y="1.237676147461" />
                  <Point X="-3.584518798828" Y="1.246006591797" />
                  <Point X="-3.575278564453" Y="1.250689331055" />
                  <Point X="-3.556534423828" Y="1.261511108398" />
                  <Point X="-3.547859619141" Y="1.267171508789" />
                  <Point X="-3.531178222656" Y="1.279403076172" />
                  <Point X="-3.515927734375" Y="1.293377685547" />
                  <Point X="-3.502289306641" Y="1.30892956543" />
                  <Point X="-3.495894775391" Y="1.317077636719" />
                  <Point X="-3.48348046875" Y="1.334807373047" />
                  <Point X="-3.478010986328" Y="1.343603149414" />
                  <Point X="-3.468062255859" Y="1.361737426758" />
                  <Point X="-3.463583007813" Y="1.371076171875" />
                  <Point X="-3.450470703125" Y="1.402732177734" />
                  <Point X="-3.438935546875" Y="1.430580444336" />
                  <Point X="-3.435499511719" Y="1.440351074219" />
                  <Point X="-3.429711425781" Y="1.460208984375" />
                  <Point X="-3.427359375" Y="1.470296020508" />
                  <Point X="-3.423600830078" Y="1.491610961914" />
                  <Point X="-3.422360839844" Y="1.501894042969" />
                  <Point X="-3.4210078125" Y="1.522534667969" />
                  <Point X="-3.42191015625" Y="1.543201049805" />
                  <Point X="-3.425056884766" Y="1.563645019531" />
                  <Point X="-3.427188232422" Y="1.573780639648" />
                  <Point X="-3.432790039062" Y="1.594686889648" />
                  <Point X="-3.436011962891" Y="1.604529785156" />
                  <Point X="-3.443508789062" Y="1.623808349609" />
                  <Point X="-3.447783691406" Y="1.633244018555" />
                  <Point X="-3.463605224609" Y="1.66363684082" />
                  <Point X="-3.4775234375" Y="1.690373657227" />
                  <Point X="-3.482800048828" Y="1.699286743164" />
                  <Point X="-3.494291748047" Y="1.716485229492" />
                  <Point X="-3.500506835938" Y="1.724770629883" />
                  <Point X="-3.514419433594" Y="1.741351196289" />
                  <Point X="-3.521500488281" Y="1.748911254883" />
                  <Point X="-3.536442626953" Y="1.763215087891" />
                  <Point X="-3.544303710938" Y="1.769958618164" />
                  <Point X="-3.942618164062" Y="2.075596191406" />
                  <Point X="-4.227614257812" Y="2.294282226563" />
                  <Point X="-4.153355957031" Y="2.421504882812" />
                  <Point X="-4.002293212891" Y="2.680312011719" />
                  <Point X="-3.791779785156" Y="2.950897216797" />
                  <Point X="-3.726338623047" Y="3.035012695312" />
                  <Point X="-3.601674560547" Y="2.963037841797" />
                  <Point X="-3.254156982422" Y="2.7623984375" />
                  <Point X="-3.244918457031" Y="2.757716552734" />
                  <Point X="-3.225986083984" Y="2.749385986328" />
                  <Point X="-3.216292236328" Y="2.745737304688" />
                  <Point X="-3.195650146484" Y="2.739228759766" />
                  <Point X="-3.185615478516" Y="2.736656982422" />
                  <Point X="-3.165327392578" Y="2.732621582031" />
                  <Point X="-3.155073974609" Y="2.731157958984" />
                  <Point X="-3.109562011719" Y="2.727176269531" />
                  <Point X="-3.069524658203" Y="2.723673339844" />
                  <Point X="-3.059173095703" Y="2.723334472656" />
                  <Point X="-3.038493164062" Y="2.723785644531" />
                  <Point X="-3.028164794922" Y="2.724575683594" />
                  <Point X="-3.006706054688" Y="2.727400878906" />
                  <Point X="-2.996526611328" Y="2.729310546875" />
                  <Point X="-2.976435302734" Y="2.734226806641" />
                  <Point X="-2.956997802734" Y="2.741301513672" />
                  <Point X="-2.938446533203" Y="2.750449951172" />
                  <Point X="-2.929420898438" Y="2.755530273438" />
                  <Point X="-2.911166503906" Y="2.767159423828" />
                  <Point X="-2.902746337891" Y="2.773193359375" />
                  <Point X="-2.886614257812" Y="2.786140869141" />
                  <Point X="-2.87890234375" Y="2.793054443359" />
                  <Point X="-2.84659765625" Y="2.825359130859" />
                  <Point X="-2.818178710938" Y="2.853777832031" />
                  <Point X="-2.811265380859" Y="2.861489501953" />
                  <Point X="-2.798318359375" Y="2.877620849609" />
                  <Point X="-2.792284667969" Y="2.886040527344" />
                  <Point X="-2.780655273438" Y="2.904294921875" />
                  <Point X="-2.775574707031" Y="2.913320800781" />
                  <Point X="-2.766426025391" Y="2.931872314453" />
                  <Point X="-2.759351318359" Y="2.951309570312" />
                  <Point X="-2.754434814453" Y="2.971401367188" />
                  <Point X="-2.752524902344" Y="2.981581542969" />
                  <Point X="-2.749699707031" Y="3.003040283203" />
                  <Point X="-2.748909667969" Y="3.013368652344" />
                  <Point X="-2.748458496094" Y="3.034049072266" />
                  <Point X="-2.748797363281" Y="3.044401123047" />
                  <Point X="-2.752779296875" Y="3.089913085938" />
                  <Point X="-2.756281982422" Y="3.129950683594" />
                  <Point X="-2.757745849609" Y="3.140204345703" />
                  <Point X="-2.76178125" Y="3.160491455078" />
                  <Point X="-2.764352783203" Y="3.170524902344" />
                  <Point X="-2.770861328125" Y="3.191167236328" />
                  <Point X="-2.774509521484" Y="3.200860595703" />
                  <Point X="-2.782840087891" Y="3.219793457031" />
                  <Point X="-2.787522460938" Y="3.229032958984" />
                  <Point X="-2.964027587891" Y="3.534748779297" />
                  <Point X="-3.059387207031" Y="3.699916259766" />
                  <Point X="-2.911471923828" Y="3.813321533203" />
                  <Point X="-2.648374755859" Y="4.015036132812" />
                  <Point X="-2.316827392578" Y="4.199236816406" />
                  <Point X="-2.192525146484" Y="4.268296386719" />
                  <Point X="-2.118563476562" Y="4.171907714844" />
                  <Point X="-2.111819335938" Y="4.164046386719" />
                  <Point X="-2.097515869141" Y="4.149104980469" />
                  <Point X="-2.089956787109" Y="4.142025390625" />
                  <Point X="-2.073376953125" Y="4.12811328125" />
                  <Point X="-2.065092041016" Y="4.121897949219" />
                  <Point X="-2.047893188477" Y="4.110405761719" />
                  <Point X="-2.038979248047" Y="4.10512890625" />
                  <Point X="-1.988324584961" Y="4.078759765625" />
                  <Point X="-1.943763061523" Y="4.055562011719" />
                  <Point X="-1.934328125" Y="4.051287109375" />
                  <Point X="-1.915050415039" Y="4.043790527344" />
                  <Point X="-1.905207763672" Y="4.040568847656" />
                  <Point X="-1.884301269531" Y="4.034966796875" />
                  <Point X="-1.874166015625" Y="4.032835449219" />
                  <Point X="-1.853722045898" Y="4.029688476562" />
                  <Point X="-1.8330546875" Y="4.028785888672" />
                  <Point X="-1.81241418457" Y="4.030138916016" />
                  <Point X="-1.802132324219" Y="4.031378662109" />
                  <Point X="-1.780817138672" Y="4.035136962891" />
                  <Point X="-1.770729248047" Y="4.037489257812" />
                  <Point X="-1.750869628906" Y="4.043277832031" />
                  <Point X="-1.741097900391" Y="4.046714111328" />
                  <Point X="-1.688337524414" Y="4.068568603516" />
                  <Point X="-1.641923706055" Y="4.087793701172" />
                  <Point X="-1.632586425781" Y="4.092272460938" />
                  <Point X="-1.614453613281" Y="4.102220214844" />
                  <Point X="-1.605657958984" Y="4.107689453125" />
                  <Point X="-1.587928100586" Y="4.120103515625" />
                  <Point X="-1.579779541016" Y="4.126498535156" />
                  <Point X="-1.564227294922" Y="4.140137207031" />
                  <Point X="-1.550252319336" Y="4.155387695312" />
                  <Point X="-1.538020874023" Y="4.172069335938" />
                  <Point X="-1.532360351562" Y="4.180744140625" />
                  <Point X="-1.521538330078" Y="4.19948828125" />
                  <Point X="-1.516856201172" Y="4.208727539062" />
                  <Point X="-1.508525756836" Y="4.22766015625" />
                  <Point X="-1.504877441406" Y="4.237353515625" />
                  <Point X="-1.487704833984" Y="4.291817382812" />
                  <Point X="-1.472598144531" Y="4.339729980469" />
                  <Point X="-1.470026611328" Y="4.349763671875" />
                  <Point X="-1.465990966797" Y="4.370051269531" />
                  <Point X="-1.46452722168" Y="4.380305175781" />
                  <Point X="-1.46264074707" Y="4.4018671875" />
                  <Point X="-1.462301757812" Y="4.412219238281" />
                  <Point X="-1.462752929688" Y="4.432898925781" />
                  <Point X="-1.46354309082" Y="4.4432265625" />
                  <Point X="-1.479266113281" Y="4.562654785156" />
                  <Point X="-1.271771484375" Y="4.620829101562" />
                  <Point X="-0.93117578125" Y="4.7163203125" />
                  <Point X="-0.529250305176" Y="4.763359863281" />
                  <Point X="-0.365222015381" Y="4.782557128906" />
                  <Point X="-0.328471618652" Y="4.64540234375" />
                  <Point X="-0.22566633606" Y="4.261727539062" />
                  <Point X="-0.220435256958" Y="4.247107910156" />
                  <Point X="-0.207661849976" Y="4.218916503906" />
                  <Point X="-0.200119247437" Y="4.205344726562" />
                  <Point X="-0.182260864258" Y="4.178618164062" />
                  <Point X="-0.17260824585" Y="4.166455566406" />
                  <Point X="-0.151451187134" Y="4.143866699219" />
                  <Point X="-0.126897941589" Y="4.125026367188" />
                  <Point X="-0.099602897644" Y="4.110436523438" />
                  <Point X="-0.085356651306" Y="4.104260742188" />
                  <Point X="-0.054918682098" Y="4.093927978516" />
                  <Point X="-0.039857234955" Y="4.090155273438" />
                  <Point X="-0.009319986343" Y="4.08511328125" />
                  <Point X="0.021631772995" Y="4.08511328125" />
                  <Point X="0.052169025421" Y="4.090155273438" />
                  <Point X="0.06723046875" Y="4.093927978516" />
                  <Point X="0.097668441772" Y="4.104260742188" />
                  <Point X="0.111914535522" Y="4.110436523438" />
                  <Point X="0.139209732056" Y="4.125026367188" />
                  <Point X="0.163763122559" Y="4.143866699219" />
                  <Point X="0.184920181274" Y="4.166455566406" />
                  <Point X="0.194572952271" Y="4.178617675781" />
                  <Point X="0.212431182861" Y="4.205344238281" />
                  <Point X="0.2199737854" Y="4.218916503906" />
                  <Point X="0.232747192383" Y="4.247107910156" />
                  <Point X="0.237978271484" Y="4.261727539062" />
                  <Point X="0.328417388916" Y="4.599251464844" />
                  <Point X="0.378190307617" Y="4.785006347656" />
                  <Point X="0.530475402832" Y="4.769058105469" />
                  <Point X="0.827876464844" Y="4.737912109375" />
                  <Point X="1.160410522461" Y="4.657627929688" />
                  <Point X="1.453597045898" Y="4.58684375" />
                  <Point X="1.66837902832" Y="4.508940917969" />
                  <Point X="1.858256103516" Y="4.440070800781" />
                  <Point X="2.067555664063" Y="4.342188476562" />
                  <Point X="2.250454589844" Y="4.256652832031" />
                  <Point X="2.452696533203" Y="4.138826171875" />
                  <Point X="2.629433105469" Y="4.035858642578" />
                  <Point X="2.817780029297" Y="3.901916992188" />
                  <Point X="2.523691162109" Y="3.392539794922" />
                  <Point X="2.06530859375" Y="2.598597900391" />
                  <Point X="2.062373046875" Y="2.593104980469" />
                  <Point X="2.053180908203" Y="2.573437988281" />
                  <Point X="2.044182250977" Y="2.549564208984" />
                  <Point X="2.041301757812" Y="2.540599121094" />
                  <Point X="2.029879882813" Y="2.497887207031" />
                  <Point X="2.01983203125" Y="2.460312744141" />
                  <Point X="2.017912597656" Y="2.451465576172" />
                  <Point X="2.013646972656" Y="2.420223388672" />
                  <Point X="2.012755615234" Y="2.383241943359" />
                  <Point X="2.013411254883" Y="2.369579833984" />
                  <Point X="2.017864868164" Y="2.332646240234" />
                  <Point X="2.021782592773" Y="2.300155029297" />
                  <Point X="2.023800537109" Y="2.28903515625" />
                  <Point X="2.029143066406" Y="2.267112304688" />
                  <Point X="2.032468017578" Y="2.256309326172" />
                  <Point X="2.040734130859" Y="2.234220458984" />
                  <Point X="2.045318115234" Y="2.223889160156" />
                  <Point X="2.055680908203" Y="2.203843994141" />
                  <Point X="2.061459472656" Y="2.194130126953" />
                  <Point X="2.084312744141" Y="2.160450195312" />
                  <Point X="2.104417236328" Y="2.130821533203" />
                  <Point X="2.109808105469" Y="2.123633300781" />
                  <Point X="2.130464355469" Y="2.100121826172" />
                  <Point X="2.157597900391" Y="2.075386474609" />
                  <Point X="2.1682578125" Y="2.066981201172" />
                  <Point X="2.201937744141" Y="2.044128051758" />
                  <Point X="2.23156640625" Y="2.024023925781" />
                  <Point X="2.241279785156" Y="2.018245239258" />
                  <Point X="2.26132421875" Y="2.00788269043" />
                  <Point X="2.271655273438" Y="2.003298706055" />
                  <Point X="2.293743652344" Y="1.995032592773" />
                  <Point X="2.304546142578" Y="1.991707885742" />
                  <Point X="2.326469726562" Y="1.986364868164" />
                  <Point X="2.337590820312" Y="1.984346557617" />
                  <Point X="2.374524658203" Y="1.979892944336" />
                  <Point X="2.407015625" Y="1.975974975586" />
                  <Point X="2.416045410156" Y="1.975320800781" />
                  <Point X="2.447575683594" Y="1.975497314453" />
                  <Point X="2.484314453125" Y="1.979822875977" />
                  <Point X="2.497748291016" Y="1.982395996094" />
                  <Point X="2.540460449219" Y="1.993817871094" />
                  <Point X="2.578034667969" Y="2.003865722656" />
                  <Point X="2.583994384766" Y="2.005670776367" />
                  <Point X="2.604404541016" Y="2.013067260742" />
                  <Point X="2.627655029297" Y="2.02357409668" />
                  <Point X="2.636033935547" Y="2.027872558594" />
                  <Point X="3.334475097656" Y="2.431117675781" />
                  <Point X="3.940405029297" Y="2.780951416016" />
                  <Point X="4.043949462891" Y="2.637048095703" />
                  <Point X="4.136884765625" Y="2.483472167969" />
                  <Point X="3.776455078125" Y="2.206904296875" />
                  <Point X="3.172951416016" Y="1.743819824219" />
                  <Point X="3.168137451172" Y="1.739868652344" />
                  <Point X="3.152118408203" Y="1.725215698242" />
                  <Point X="3.134668457031" Y="1.706602905273" />
                  <Point X="3.128576416016" Y="1.699422485352" />
                  <Point X="3.097836425781" Y="1.659319946289" />
                  <Point X="3.070794189453" Y="1.624041137695" />
                  <Point X="3.065635498047" Y="1.616601928711" />
                  <Point X="3.049738769531" Y="1.589370849609" />
                  <Point X="3.034762939453" Y="1.555545410156" />
                  <Point X="3.030140136719" Y="1.54267199707" />
                  <Point X="3.018689453125" Y="1.501727172852" />
                  <Point X="3.008616210938" Y="1.465707885742" />
                  <Point X="3.006225097656" Y="1.454661010742" />
                  <Point X="3.002771728516" Y="1.432362548828" />
                  <Point X="3.001709472656" Y="1.421110839844" />
                  <Point X="3.000893310547" Y="1.397540649414" />
                  <Point X="3.001174804688" Y="1.386241943359" />
                  <Point X="3.003077636719" Y="1.363757324219" />
                  <Point X="3.004698974609" Y="1.352571166992" />
                  <Point X="3.014098632812" Y="1.307014770508" />
                  <Point X="3.022367919922" Y="1.266938476562" />
                  <Point X="3.02459765625" Y="1.258234619141" />
                  <Point X="3.034684082031" Y="1.228607666016" />
                  <Point X="3.050286865234" Y="1.195370483398" />
                  <Point X="3.056918945312" Y="1.183525390625" />
                  <Point X="3.082485351562" Y="1.144665771484" />
                  <Point X="3.1049765625" Y="1.110480102539" />
                  <Point X="3.111739257813" Y="1.101424438477" />
                  <Point X="3.126292480469" Y="1.084179931641" />
                  <Point X="3.134083007812" Y="1.075991210938" />
                  <Point X="3.151326904297" Y="1.059901367188" />
                  <Point X="3.160034912109" Y="1.052695678711" />
                  <Point X="3.178244628906" Y="1.039369995117" />
                  <Point X="3.18774609375" Y="1.03325" />
                  <Point X="3.224795410156" Y="1.012394287109" />
                  <Point X="3.257388183594" Y="0.994047546387" />
                  <Point X="3.265479492188" Y="0.989987609863" />
                  <Point X="3.294678222656" Y="0.978084289551" />
                  <Point X="3.330275146484" Y="0.96802130127" />
                  <Point X="3.343670898438" Y="0.965257507324" />
                  <Point X="3.393763916016" Y="0.958637084961" />
                  <Point X="3.437831542969" Y="0.952812988281" />
                  <Point X="3.444029785156" Y="0.952199829102" />
                  <Point X="3.465716064453" Y="0.951222900391" />
                  <Point X="3.491217529297" Y="0.952032287598" />
                  <Point X="3.500603515625" Y="0.952797241211" />
                  <Point X="4.164075683594" Y="1.040145141602" />
                  <Point X="4.704703613281" Y="1.111320068359" />
                  <Point X="4.707657714844" Y="1.099185302734" />
                  <Point X="4.75268359375" Y="0.914233825684" />
                  <Point X="4.78387109375" Y="0.713920776367" />
                  <Point X="4.383620117188" Y="0.60667401123" />
                  <Point X="3.691991943359" Y="0.421352966309" />
                  <Point X="3.686031738281" Y="0.419544525146" />
                  <Point X="3.665626708984" Y="0.412138061523" />
                  <Point X="3.642381103516" Y="0.401619415283" />
                  <Point X="3.634004394531" Y="0.397316497803" />
                  <Point X="3.584789550781" Y="0.36886932373" />
                  <Point X="3.541494384766" Y="0.343843902588" />
                  <Point X="3.533881835938" Y="0.338945739746" />
                  <Point X="3.508774169922" Y="0.319870574951" />
                  <Point X="3.481993896484" Y="0.294350982666" />
                  <Point X="3.472796630859" Y="0.284226654053" />
                  <Point X="3.443267822266" Y="0.246599945068" />
                  <Point X="3.417290771484" Y="0.213499099731" />
                  <Point X="3.410853759766" Y="0.204207641602" />
                  <Point X="3.399129638672" Y="0.184926940918" />
                  <Point X="3.393842529297" Y="0.174937988281" />
                  <Point X="3.384068847656" Y="0.15347416687" />
                  <Point X="3.380005371094" Y="0.142928482056" />
                  <Point X="3.373159179688" Y="0.12142780304" />
                  <Point X="3.370376464844" Y="0.110472961426" />
                  <Point X="3.360533447266" Y="0.059076633453" />
                  <Point X="3.351874267578" Y="0.013862721443" />
                  <Point X="3.350603515625" Y="0.004967938423" />
                  <Point X="3.348584472656" Y="-0.026262042999" />
                  <Point X="3.350280029297" Y="-0.06294083786" />
                  <Point X="3.351874267578" Y="-0.076422866821" />
                  <Point X="3.361717285156" Y="-0.127819046021" />
                  <Point X="3.370376464844" Y="-0.173033248901" />
                  <Point X="3.373159179688" Y="-0.183987792969" />
                  <Point X="3.380005371094" Y="-0.205488632202" />
                  <Point X="3.384068847656" Y="-0.216034759521" />
                  <Point X="3.393842529297" Y="-0.237498428345" />
                  <Point X="3.399130126953" Y="-0.247487976074" />
                  <Point X="3.410854248047" Y="-0.266768218994" />
                  <Point X="3.417290771484" Y="-0.276058654785" />
                  <Point X="3.446819580078" Y="-0.313685516357" />
                  <Point X="3.472796630859" Y="-0.346786346436" />
                  <Point X="3.478717773438" Y="-0.3536328125" />
                  <Point X="3.501139892578" Y="-0.37580557251" />
                  <Point X="3.530176757813" Y="-0.398725006104" />
                  <Point X="3.541494628906" Y="-0.406404205322" />
                  <Point X="3.590709716797" Y="-0.434851379395" />
                  <Point X="3.634004638672" Y="-0.459876647949" />
                  <Point X="3.639495849609" Y="-0.462814758301" />
                  <Point X="3.659156738281" Y="-0.472016021729" />
                  <Point X="3.68302734375" Y="-0.481027618408" />
                  <Point X="3.691991943359" Y="-0.483912963867" />
                  <Point X="4.300426269531" Y="-0.646942321777" />
                  <Point X="4.784876953125" Y="-0.776750488281" />
                  <Point X="4.76161328125" Y="-0.931052856445" />
                  <Point X="4.727802246094" Y="-1.079219726563" />
                  <Point X="4.238875976563" Y="-1.014851318359" />
                  <Point X="3.436781982422" Y="-0.909253723145" />
                  <Point X="3.428624511719" Y="-0.908535888672" />
                  <Point X="3.400098388672" Y="-0.908042480469" />
                  <Point X="3.366721435547" Y="-0.910840881348" />
                  <Point X="3.354481201172" Y="-0.912676208496" />
                  <Point X="3.257889648438" Y="-0.933670837402" />
                  <Point X="3.172916992188" Y="-0.952139892578" />
                  <Point X="3.157874023438" Y="-0.956742492676" />
                  <Point X="3.128753662109" Y="-0.968367004395" />
                  <Point X="3.114676269531" Y="-0.975388977051" />
                  <Point X="3.086849609375" Y="-0.992281311035" />
                  <Point X="3.074124023438" Y="-1.001530273438" />
                  <Point X="3.050374023438" Y="-1.022001159668" />
                  <Point X="3.039349609375" Y="-1.033222900391" />
                  <Point X="2.980966064453" Y="-1.103440063477" />
                  <Point X="2.92960546875" Y="-1.165211181641" />
                  <Point X="2.921326171875" Y="-1.176847290039" />
                  <Point X="2.90660546875" Y="-1.201229492188" />
                  <Point X="2.9001640625" Y="-1.213975708008" />
                  <Point X="2.888820800781" Y="-1.241360839844" />
                  <Point X="2.884362792969" Y="-1.254928222656" />
                  <Point X="2.877531005859" Y="-1.282577758789" />
                  <Point X="2.875157226562" Y="-1.296660400391" />
                  <Point X="2.866789550781" Y="-1.387594726562" />
                  <Point X="2.859428222656" Y="-1.467590698242" />
                  <Point X="2.859288818359" Y="-1.483321533203" />
                  <Point X="2.861607666016" Y="-1.514590454102" />
                  <Point X="2.864065917969" Y="-1.53012878418" />
                  <Point X="2.871797607422" Y="-1.561749633789" />
                  <Point X="2.876786376953" Y="-1.576668945312" />
                  <Point X="2.889157714844" Y="-1.605479980469" />
                  <Point X="2.896540283203" Y="-1.619371582031" />
                  <Point X="2.949995361328" Y="-1.702517456055" />
                  <Point X="2.997020507812" Y="-1.775661987305" />
                  <Point X="3.0017421875" Y="-1.782353515625" />
                  <Point X="3.019793212891" Y="-1.80444909668" />
                  <Point X="3.043488769531" Y="-1.828119750977" />
                  <Point X="3.052796142578" Y="-1.836277587891" />
                  <Point X="3.617427001953" Y="-2.269534423828" />
                  <Point X="4.087170654297" Y="-2.629981689453" />
                  <Point X="4.045486572266" Y="-2.697432861328" />
                  <Point X="4.001274658203" Y="-2.760251953125" />
                  <Point X="3.562827636719" Y="-2.507114257812" />
                  <Point X="2.848454589844" Y="-2.094670654297" />
                  <Point X="2.841191894531" Y="-2.090885253906" />
                  <Point X="2.815026123047" Y="-2.079513183594" />
                  <Point X="2.783119140625" Y="-2.069325927734" />
                  <Point X="2.771108398438" Y="-2.066337646484" />
                  <Point X="2.656149169922" Y="-2.045575927734" />
                  <Point X="2.555018066406" Y="-2.027311889648" />
                  <Point X="2.539359130859" Y="-2.025807495117" />
                  <Point X="2.508006835938" Y="-2.025403198242" />
                  <Point X="2.492313476562" Y="-2.026503540039" />
                  <Point X="2.460140625" Y="-2.031461303711" />
                  <Point X="2.444844482422" Y="-2.035136230469" />
                  <Point X="2.415068603516" Y="-2.044959960938" />
                  <Point X="2.400588867188" Y="-2.051108642578" />
                  <Point X="2.3050859375" Y="-2.10137109375" />
                  <Point X="2.221070800781" Y="-2.145587646484" />
                  <Point X="2.208968261719" Y="-2.153170410156" />
                  <Point X="2.186037597656" Y="-2.170063476562" />
                  <Point X="2.175209472656" Y="-2.179373779297" />
                  <Point X="2.154249755859" Y="-2.200333496094" />
                  <Point X="2.144939453125" Y="-2.211161621094" />
                  <Point X="2.128046386719" Y="-2.234092285156" />
                  <Point X="2.120463623047" Y="-2.246194824219" />
                  <Point X="2.070201171875" Y="-2.341697753906" />
                  <Point X="2.025984741211" Y="-2.425712890625" />
                  <Point X="2.0198359375" Y="-2.440192626953" />
                  <Point X="2.010012207031" Y="-2.469968505859" />
                  <Point X="2.006337280273" Y="-2.485264648438" />
                  <Point X="2.001379516602" Y="-2.5174375" />
                  <Point X="2.000279174805" Y="-2.533130859375" />
                  <Point X="2.00068347168" Y="-2.564483154297" />
                  <Point X="2.002187866211" Y="-2.580142089844" />
                  <Point X="2.02294921875" Y="-2.695101318359" />
                  <Point X="2.041213500977" Y="-2.796232421875" />
                  <Point X="2.043015014648" Y="-2.804221435547" />
                  <Point X="2.051236083984" Y="-2.83154296875" />
                  <Point X="2.064069580078" Y="-2.862479736328" />
                  <Point X="2.069546630859" Y="-2.873578613281" />
                  <Point X="2.432378662109" Y="-3.502021972656" />
                  <Point X="2.735893066406" Y="-4.027724365234" />
                  <Point X="2.723754394531" Y="-4.036083496094" />
                  <Point X="2.379047363281" Y="-3.586853271484" />
                  <Point X="1.833914550781" Y="-2.876422851562" />
                  <Point X="1.828654418945" Y="-2.870147216797" />
                  <Point X="1.808830932617" Y="-2.849625732422" />
                  <Point X="1.783251586914" Y="-2.828004150391" />
                  <Point X="1.773299072266" Y="-2.820647216797" />
                  <Point X="1.65991809082" Y="-2.747753662109" />
                  <Point X="1.56017565918" Y="-2.683628417969" />
                  <Point X="1.546283935547" Y="-2.676245849609" />
                  <Point X="1.517473022461" Y="-2.663874511719" />
                  <Point X="1.502553344727" Y="-2.658885742188" />
                  <Point X="1.470932006836" Y="-2.651154052734" />
                  <Point X="1.455394165039" Y="-2.648695800781" />
                  <Point X="1.424125244141" Y="-2.646376953125" />
                  <Point X="1.408394287109" Y="-2.646516357422" />
                  <Point X="1.284393066406" Y="-2.657927001953" />
                  <Point X="1.175307373047" Y="-2.667965332031" />
                  <Point X="1.161225097656" Y="-2.670339111328" />
                  <Point X="1.133575317383" Y="-2.677170898438" />
                  <Point X="1.120007568359" Y="-2.68162890625" />
                  <Point X="1.092622680664" Y="-2.692972167969" />
                  <Point X="1.079876708984" Y="-2.699413574219" />
                  <Point X="1.055494873047" Y="-2.714134033203" />
                  <Point X="1.043858764648" Y="-2.722413085938" />
                  <Point X="0.948108154297" Y="-2.802026611328" />
                  <Point X="0.863875061035" Y="-2.872063476562" />
                  <Point X="0.852653015137" Y="-2.883088378906" />
                  <Point X="0.832182312012" Y="-2.906838378906" />
                  <Point X="0.822933654785" Y="-2.919563720703" />
                  <Point X="0.806041137695" Y="-2.947390380859" />
                  <Point X="0.799019287109" Y="-2.961467529297" />
                  <Point X="0.787394470215" Y="-2.990588134766" />
                  <Point X="0.782791931152" Y="-3.005631347656" />
                  <Point X="0.754162841797" Y="-3.137347167969" />
                  <Point X="0.728977661133" Y="-3.253219238281" />
                  <Point X="0.727584777832" Y="-3.261289306641" />
                  <Point X="0.724724487305" Y="-3.289677734375" />
                  <Point X="0.7247421875" Y="-3.323170410156" />
                  <Point X="0.725554992676" Y="-3.335520019531" />
                  <Point X="0.82837890625" Y="-4.116544433594" />
                  <Point X="0.833091674805" Y="-4.152341796875" />
                  <Point X="0.655064941406" Y="-3.487936523438" />
                  <Point X="0.652606262207" Y="-3.480124511719" />
                  <Point X="0.642145141602" Y="-3.453580566406" />
                  <Point X="0.626787231445" Y="-3.423815917969" />
                  <Point X="0.620407592773" Y="-3.413210205078" />
                  <Point X="0.533305358887" Y="-3.287711914062" />
                  <Point X="0.456679992676" Y="-3.177309814453" />
                  <Point X="0.446670684814" Y="-3.165172851562" />
                  <Point X="0.424786712646" Y="-3.142717529297" />
                  <Point X="0.412912322998" Y="-3.132399169922" />
                  <Point X="0.386657196045" Y="-3.113155273438" />
                  <Point X="0.373242950439" Y="-3.104938232422" />
                  <Point X="0.345241943359" Y="-3.090829833984" />
                  <Point X="0.330654907227" Y="-3.084938476562" />
                  <Point X="0.195868789673" Y="-3.043105712891" />
                  <Point X="0.077295654297" Y="-3.006305175781" />
                  <Point X="0.063376529694" Y="-3.003109130859" />
                  <Point X="0.035216945648" Y="-2.99883984375" />
                  <Point X="0.020976644516" Y="-2.997766601562" />
                  <Point X="-0.008664708138" Y="-2.997766601562" />
                  <Point X="-0.022905010223" Y="-2.998840087891" />
                  <Point X="-0.051064441681" Y="-3.003109375" />
                  <Point X="-0.064983421326" Y="-3.006305175781" />
                  <Point X="-0.199769683838" Y="-3.048137695312" />
                  <Point X="-0.318342529297" Y="-3.084938476562" />
                  <Point X="-0.332929412842" Y="-3.090829589844" />
                  <Point X="-0.360930847168" Y="-3.104937988281" />
                  <Point X="-0.374345275879" Y="-3.113155273438" />
                  <Point X="-0.400600524902" Y="-3.132399169922" />
                  <Point X="-0.412475219727" Y="-3.142717773438" />
                  <Point X="-0.434358886719" Y="-3.165173095703" />
                  <Point X="-0.444368041992" Y="-3.177309814453" />
                  <Point X="-0.531470458984" Y="-3.302807861328" />
                  <Point X="-0.60809564209" Y="-3.413210205078" />
                  <Point X="-0.612470336914" Y="-3.420132324219" />
                  <Point X="-0.625976745605" Y="-3.445265136719" />
                  <Point X="-0.638777832031" Y="-3.476215820312" />
                  <Point X="-0.642752990723" Y="-3.487936523438" />
                  <Point X="-0.83022668457" Y="-4.18759765625" />
                  <Point X="-0.985425231934" Y="-4.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.698892080096" Y="-0.631192624341" />
                  <Point X="-4.596415822505" Y="-1.278202250925" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.606623674269" Y="-0.606469355347" />
                  <Point X="-4.502196277844" Y="-1.2657979877" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.744286139522" Y="0.869980299204" />
                  <Point X="-4.696453872966" Y="0.567979253783" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.514355268442" Y="-0.581746086353" />
                  <Point X="-4.407976733183" Y="-1.253393724474" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.687554713293" Y="1.119075226963" />
                  <Point X="-4.596006813696" Y="0.541064537218" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.422086862614" Y="-0.557022817359" />
                  <Point X="-4.313757188521" Y="-1.240989461249" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.12959756356" Y="-2.403727572289" />
                  <Point X="-4.102362815108" Y="-2.575681006583" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.622687408782" Y="1.316802240896" />
                  <Point X="-4.495559754427" Y="0.514149820652" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.329818456787" Y="-0.532299548366" />
                  <Point X="-4.21953764386" Y="-1.228585198023" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.043836194001" Y="-2.337920493203" />
                  <Point X="-3.973763944217" Y="-2.780339266409" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.524454904821" Y="1.303869676259" />
                  <Point X="-4.395112695157" Y="0.487235104087" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.237550046426" Y="-0.507576307994" />
                  <Point X="-4.125318099199" Y="-1.216180934798" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.958074824441" Y="-2.272113414117" />
                  <Point X="-3.852306200243" Y="-2.939910225355" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.426222400859" Y="1.290937111623" />
                  <Point X="-4.294665635888" Y="0.460320387521" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.145281633639" Y="-0.482853082945" />
                  <Point X="-4.031098554538" Y="-1.203776671572" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.872313454882" Y="-2.206306335031" />
                  <Point X="-3.747929578208" Y="-2.991635224782" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.327989896596" Y="1.278004545086" />
                  <Point X="-4.194218577121" Y="0.433405674132" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.053013220851" Y="-0.458129857897" />
                  <Point X="-3.936879006596" Y="-1.19137242906" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.786552085322" Y="-2.140499255945" />
                  <Point X="-3.659803888816" Y="-2.940755873621" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.229757390775" Y="1.265071968712" />
                  <Point X="-4.093771523813" Y="0.406490995202" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.960744808064" Y="-0.433406632848" />
                  <Point X="-3.84265945747" Y="-1.17896819402" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.70079071181" Y="-2.074692201819" />
                  <Point X="-3.571678199424" Y="-2.889876522459" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.131524884954" Y="1.252139392337" />
                  <Point X="-3.993324470504" Y="0.379576316272" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.868476395276" Y="-0.4086834078" />
                  <Point X="-3.748439908345" Y="-1.166563958981" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.615029333831" Y="-2.008885175892" />
                  <Point X="-3.483552510032" Y="-2.838997171298" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.206208412334" Y="2.330955682492" />
                  <Point X="-4.196634887981" Y="2.270510828607" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.033292379133" Y="1.239206815963" />
                  <Point X="-3.892877417195" Y="0.352661637342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.776207982489" Y="-0.383960182751" />
                  <Point X="-3.654220359219" Y="-1.154159723941" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.529267955852" Y="-1.943078149965" />
                  <Point X="-3.39542682064" Y="-2.788117820137" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.130553325893" Y="2.460571321927" />
                  <Point X="-4.087143922008" Y="2.186495132396" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.935059873313" Y="1.226274239589" />
                  <Point X="-3.792430363886" Y="0.325746958412" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.683939569701" Y="-0.359236957702" />
                  <Point X="-3.560000810094" Y="-1.141755488901" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.443506577873" Y="-1.877271124039" />
                  <Point X="-3.307301131247" Y="-2.737238468975" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.054898224222" Y="2.590186865203" />
                  <Point X="-3.977652956035" Y="2.102479436184" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.836827367492" Y="1.213341663214" />
                  <Point X="-3.691983310578" Y="0.298832279482" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.591671156914" Y="-0.334513732654" />
                  <Point X="-3.465781260968" Y="-1.129351253862" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.357745199894" Y="-1.811464098112" />
                  <Point X="-3.219175445932" Y="-2.686359092074" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.977945237753" Y="2.711607886366" />
                  <Point X="-3.868162024832" Y="2.0184639595" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.739261731714" Y="1.204619538587" />
                  <Point X="-3.591536257269" Y="0.271917600552" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.499497843596" Y="-0.309190073183" />
                  <Point X="-3.371561711843" Y="-1.116947018822" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.271983821915" Y="-1.745657072186" />
                  <Point X="-3.131049761604" Y="-2.635479708942" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.950061053255" Y="-3.778197440415" />
                  <Point X="-2.936159620206" Y="-3.865967634389" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.898030213238" Y="2.814327335333" />
                  <Point X="-3.75867110999" Y="1.934448586114" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.646090133985" Y="1.223640278343" />
                  <Point X="-3.490815745784" Y="0.24327637465" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.410932686255" Y="-0.26108541345" />
                  <Point X="-3.277342162717" Y="-1.104542783782" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.186222443935" Y="-1.679850046259" />
                  <Point X="-3.042924077275" Y="-2.584600325809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.874582802733" Y="-3.647465302936" />
                  <Point X="-2.826616580393" Y="-3.950312111888" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.818115188723" Y="2.917046784301" />
                  <Point X="-3.649180195147" Y="1.850433212728" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.555963138198" Y="1.261883878221" />
                  <Point X="-3.384112095429" Y="0.176859096642" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.329398158818" Y="-0.168591103515" />
                  <Point X="-3.181494700689" Y="-1.102416786303" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.100461065956" Y="-1.614043020332" />
                  <Point X="-2.954798392947" Y="-2.533720942677" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.79910455221" Y="-3.516733165457" />
                  <Point X="-2.717881396403" Y="-4.029555988456" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.738200178252" Y="3.019766321932" />
                  <Point X="-3.539612730087" Y="1.765934520286" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.473906866956" Y="1.35108402742" />
                  <Point X="-3.079651662149" Y="-1.138145369097" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.015820816742" Y="-1.541157465968" />
                  <Point X="-2.866672708619" Y="-2.482841559545" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.723626301321" Y="-3.386001030289" />
                  <Point X="-2.611239047149" Y="-4.095586226548" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.636187016601" Y="2.982963624089" />
                  <Point X="-2.962470458726" Y="-1.270715313662" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.948633887417" Y="-1.358075986721" />
                  <Point X="-2.77854702429" Y="-2.431962176413" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.64814804881" Y="-3.255268905361" />
                  <Point X="-2.504596695951" Y="-4.161616476908" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.530322199244" Y="2.921842529191" />
                  <Point X="-2.690421339962" Y="-2.381082793281" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.5726697963" Y="-3.124536780433" />
                  <Point X="-2.424517336857" Y="-4.059934595638" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.424457382379" Y="2.860721437399" />
                  <Point X="-2.598970611675" Y="-2.351196911477" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.497191543789" Y="-2.993804655505" />
                  <Point X="-2.341009596701" Y="-3.979898660497" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.318592565514" Y="2.799600345607" />
                  <Point X="-2.501757950667" Y="-2.357690441121" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.421713291278" Y="-2.863072530576" />
                  <Point X="-2.254030395621" Y="-3.92178066702" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.213750371956" Y="2.744935843277" />
                  <Point X="-2.396017475596" Y="-2.418026469716" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.346235038768" Y="-2.732340405648" />
                  <Point X="-2.16705115263" Y="-3.863662938158" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.114826289455" Y="2.727636823588" />
                  <Point X="-2.079789371996" Y="-3.807329081764" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.018361684284" Y="2.725866332622" />
                  <Point X="-1.988137413035" Y="-3.778713720431" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.927109068646" Y="2.75700304846" />
                  <Point X="-1.891835876109" Y="-3.779454639024" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.98883495542" Y="3.754008015616" />
                  <Point X="-2.950357624729" Y="3.511071710682" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.842414036181" Y="2.829542714988" />
                  <Point X="-1.794642737365" Y="-3.785824909942" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.903065871713" Y="3.819766389489" />
                  <Point X="-2.817812294007" Y="3.281496484117" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.76364669238" Y="2.939508334801" />
                  <Point X="-1.69744956981" Y="-3.792195362764" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.817296806495" Y="3.885524880099" />
                  <Point X="-1.599462887302" Y="-3.803575871828" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.731527741277" Y="3.951283370708" />
                  <Point X="-1.495210713823" Y="-3.854515133995" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.645678276656" Y="4.016534239471" />
                  <Point X="-1.383535026028" Y="-3.952324620921" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.557273289718" Y="4.065650175324" />
                  <Point X="-1.271835873042" Y="-4.050282261234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.468868302779" Y="4.114766111177" />
                  <Point X="-1.064663710263" Y="-4.751032761734" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.38046331584" Y="4.16388204703" />
                  <Point X="-0.973204718435" Y="-4.721199053876" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.292058325379" Y="4.212997960646" />
                  <Point X="-0.912753299058" Y="-4.495591238492" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.203653325869" Y="4.262113817129" />
                  <Point X="-0.85230187968" Y="-4.269983423107" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.088217902792" Y="4.14056629587" />
                  <Point X="-0.791850452904" Y="-4.044375654435" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.981698165056" Y="4.075310196437" />
                  <Point X="-0.731399021872" Y="-3.818767912635" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.878945839406" Y="4.033840600773" />
                  <Point X="-0.670947590841" Y="-3.593160170835" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.782908569903" Y="4.034768201023" />
                  <Point X="-0.604162279397" Y="-3.407542976075" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.691847560412" Y="4.067114670467" />
                  <Point X="-0.525849270848" Y="-3.294708796381" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.602445986903" Y="4.109938406347" />
                  <Point X="-0.447536262072" Y="-3.181874618115" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.520705423704" Y="4.201131857683" />
                  <Point X="-0.363307092881" Y="-3.106393606634" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.466881292517" Y="4.468582723921" />
                  <Point X="-0.27276167718" Y="-3.070791806122" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.389579248073" Y="4.587799879763" />
                  <Point X="-0.181084068254" Y="-3.042338392295" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.297484572908" Y="4.613620040991" />
                  <Point X="-0.089406432516" Y="-3.013885147757" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.205389896486" Y="4.639440194284" />
                  <Point X="0.004224827516" Y="-2.997766601562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.113295219578" Y="4.665260344504" />
                  <Point X="0.103026214335" Y="-3.014290951198" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.02120054267" Y="4.691080494723" />
                  <Point X="0.204182906871" Y="-3.045686115874" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.929053293839" Y="4.716568718885" />
                  <Point X="0.305339638528" Y="-3.077081527554" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.834619587008" Y="4.727620815386" />
                  <Point X="0.40994025459" Y="-3.130220769609" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.740185880177" Y="4.738672911887" />
                  <Point X="0.530407233622" Y="-3.283536284894" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.645752173346" Y="4.749725008389" />
                  <Point X="0.664603431978" Y="-3.523534679488" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.551318466515" Y="4.76077710489" />
                  <Point X="0.724727755384" Y="-3.295861661416" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.45688476874" Y="4.771829258571" />
                  <Point X="0.778252104012" Y="-3.026518042598" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.358319813301" Y="4.756797677915" />
                  <Point X="0.851864211895" Y="-2.884003544198" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.167825914098" Y="4.161349589331" />
                  <Point X="0.936582371379" Y="-2.811609895922" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.061306653141" Y="4.096096500163" />
                  <Point X="1.021573899343" Y="-2.740942228293" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.036235213366" Y="4.087524448805" />
                  <Point X="1.109080644075" Y="-2.686155014347" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.127473284553" Y="4.11875299469" />
                  <Point X="1.201994899795" Y="-2.665509481092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.210419589144" Y="4.20233369449" />
                  <Point X="1.296797352487" Y="-2.656785554326" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.275196833329" Y="4.400629326943" />
                  <Point X="1.391599830961" Y="-2.648061790347" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.335648228058" Y="4.626237297952" />
                  <Point X="1.488972423837" Y="-2.655565090064" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.407166496069" Y="4.781971781007" />
                  <Point X="1.592937427441" Y="-2.704691232998" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.504972999843" Y="4.771728875702" />
                  <Point X="1.700026096779" Y="-2.773539425193" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.602779515655" Y="4.761485894394" />
                  <Point X="1.808173101535" Y="-2.849069684239" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.700586035712" Y="4.751242886278" />
                  <Point X="1.928138392877" Y="-2.999217668113" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.79839255577" Y="4.740999878163" />
                  <Point X="2.049339825474" Y="-3.157170340711" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.897737200894" Y="4.721045530576" />
                  <Point X="2.028819445103" Y="-2.420326702019" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.112349636763" Y="-2.94771557613" />
                  <Point X="2.170541258071" Y="-3.315123013309" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.997745607824" Y="4.696900355884" />
                  <Point X="2.102753581126" Y="-2.279845409276" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.244894959742" Y="-3.177290753811" />
                  <Point X="2.291742690668" Y="-3.473075685906" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.097754014754" Y="4.672755181192" />
                  <Point X="2.182087899981" Y="-2.173459529071" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.377440282721" Y="-3.406865931492" />
                  <Point X="2.412944115159" Y="-3.631028307324" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.197762416066" Y="4.64861004197" />
                  <Point X="2.269796044984" Y="-2.119943906393" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.509985614172" Y="-3.636441162659" />
                  <Point X="2.534145518772" Y="-3.788980796922" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.297770807955" Y="4.624464962249" />
                  <Point X="2.358579547188" Y="-2.073217821869" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.64253095162" Y="-3.866016431691" />
                  <Point X="2.655346922384" Y="-3.94693328652" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.397779199843" Y="4.600319882527" />
                  <Point X="2.448589689394" Y="-2.034236437515" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.498688148406" Y="4.570488911735" />
                  <Point X="2.543501901084" Y="-2.026205501792" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.600734605748" Y="4.533475993165" />
                  <Point X="2.642359628045" Y="-2.043085569088" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.702781077039" Y="4.496462986526" />
                  <Point X="2.023250738358" Y="2.473097176968" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.069254216008" Y="2.182642650273" />
                  <Point X="2.741376109674" Y="-2.060967973905" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.804827575757" Y="4.45944980672" />
                  <Point X="2.092184656069" Y="2.645148605649" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.185623688011" Y="2.055197776193" />
                  <Point X="2.842398320936" Y="-2.091514057234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.907746933181" Y="4.416925613937" />
                  <Point X="2.167662903265" Y="2.775880764135" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.291185475449" Y="1.995989936909" />
                  <Point X="2.948203869949" Y="-2.152260946532" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.011625515243" Y="4.368345115143" />
                  <Point X="2.24314115046" Y="2.906612922622" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.390218920424" Y="1.978000429739" />
                  <Point X="2.896991415261" Y="-1.221635177133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.980726506629" Y="-1.750317737093" />
                  <Point X="3.054068688274" Y="-2.21338204754" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.115504097489" Y="4.319764615187" />
                  <Point X="2.318619397656" Y="3.037345081109" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.48606146128" Y="1.980157498284" />
                  <Point X="2.975497106814" Y="-1.110017550048" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.095745101564" Y="-1.869233509035" />
                  <Point X="3.159933506599" Y="-2.274503148548" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.21938267995" Y="4.271184113876" />
                  <Point X="2.394097644851" Y="3.168077239596" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.578469762543" Y="2.003997502253" />
                  <Point X="3.056855973134" Y="-1.016414159466" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.205236021149" Y="-1.953248912365" />
                  <Point X="3.265798324924" Y="-2.335624249557" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.324721376144" Y="4.213384817272" />
                  <Point X="2.469575892047" Y="3.298809398082" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.667953667046" Y="2.046301420704" />
                  <Point X="3.144438557097" Y="-0.962105775575" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.314726940734" Y="-2.037264315696" />
                  <Point X="3.371663143249" Y="-2.396745350565" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.430683209211" Y="4.151651189286" />
                  <Point X="2.545054136178" Y="3.429541575916" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.756079352118" Y="2.097180799143" />
                  <Point X="3.236843608944" Y="-0.938245255595" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.424217860319" Y="-2.121279719026" />
                  <Point X="3.477527961574" Y="-2.457866451573" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.536645072272" Y="4.089917371929" />
                  <Point X="2.620532372547" Y="3.560273802756" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.844205037189" Y="2.148060177581" />
                  <Point X="3.329826799354" Y="-0.918034958847" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.533708779904" Y="-2.205295122357" />
                  <Point X="3.583392780782" Y="-2.518987558159" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.642909186105" Y="4.026275218307" />
                  <Point X="2.696010608917" Y="3.691006029596" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.932330722261" Y="2.19893955602" />
                  <Point X="3.034396130087" Y="1.554523932762" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.104646243427" Y="1.11098217326" />
                  <Point X="3.424495168931" Y="-0.908464464609" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.643199699504" Y="-2.28931052578" />
                  <Point X="3.689257603656" Y="-2.580108687882" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.75130210442" Y="3.949192322156" />
                  <Point X="2.771488845286" Y="3.821738256437" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.020456407333" Y="2.249818934458" />
                  <Point X="3.111217424315" Y="1.676776426018" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.21562786295" Y="1.017554860933" />
                  <Point X="3.36431143049" Y="0.078803761171" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.421331645559" Y="-0.281207708089" />
                  <Point X="3.522593681168" Y="-0.92055103879" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.752690619151" Y="-2.373325929502" />
                  <Point X="3.795122426529" Y="-2.641229817605" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.108582092405" Y="2.300698312897" />
                  <Point X="3.194200520982" Y="1.760124829782" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.319159711247" Y="0.971163552973" />
                  <Point X="3.435489187909" Y="0.236688143497" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.536845382196" Y="-0.403249681701" />
                  <Point X="3.620826181856" Y="-0.933483582756" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.862181538798" Y="-2.457341333224" />
                  <Point X="3.900987249402" Y="-2.702350947327" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.196707777477" Y="2.351577691336" />
                  <Point X="3.279961903324" Y="1.825931828164" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.417831694984" Y="0.955456222245" />
                  <Point X="3.517454113114" Y="0.32646502888" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.642701354186" Y="-0.464314929149" />
                  <Point X="3.719058682544" Y="-0.946416126723" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.971672458445" Y="-2.541356736946" />
                  <Point X="4.005411164397" Y="-2.754374542753" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.284833462548" Y="2.402457069774" />
                  <Point X="3.365723285666" Y="1.891738826546" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.514154462355" Y="0.954581259897" />
                  <Point X="3.605065837535" Y="0.380589427156" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.744205451321" Y="-0.497903520138" />
                  <Point X="3.817291183232" Y="-0.95934867069" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.081163378092" Y="-2.625372140668" />
                  <Point X="4.082970035709" Y="-2.636778927933" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.37295914765" Y="2.453336448028" />
                  <Point X="3.451484668007" Y="1.957545824928" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.608374007955" Y="0.966985517198" />
                  <Point X="3.694679650687" Y="0.422073134673" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.844652505735" Y="-0.524818206047" />
                  <Point X="3.91552368392" Y="-0.972281214656" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.461084832788" Y="2.504215826043" />
                  <Point X="3.537246050349" Y="2.02335282331" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.702593553554" Y="0.9793897745" />
                  <Point X="3.786948063545" Y="0.446796359277" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.945099560149" Y="-0.551732891955" />
                  <Point X="4.013756184608" Y="-0.985213758623" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.549210517927" Y="2.555095204059" />
                  <Point X="3.623007432691" Y="2.089159821692" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.796813099154" Y="0.991794031801" />
                  <Point X="3.879216476403" Y="0.471519583881" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.045546614563" Y="-0.578647577864" />
                  <Point X="4.111988685296" Y="-0.998146302589" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.637336203066" Y="2.605974582074" />
                  <Point X="3.708768815032" Y="2.154966820074" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.891032644753" Y="1.004198289103" />
                  <Point X="3.971484889261" Y="0.496242808485" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.145993668977" Y="-0.605562263773" />
                  <Point X="4.210221185984" Y="-1.011078846556" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.725461888205" Y="2.65685396009" />
                  <Point X="3.794530193487" Y="2.220773842999" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.985252190353" Y="1.016602546404" />
                  <Point X="4.063753302119" Y="0.520966033089" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.246440723391" Y="-0.632476949682" />
                  <Point X="4.308453688833" Y="-1.024011404173" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.813587573344" Y="2.707733338105" />
                  <Point X="3.880291557384" Y="2.286580957834" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.079471735952" Y="1.029006803706" />
                  <Point X="4.156021714977" Y="0.545689257693" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.346887779381" Y="-0.659391645539" />
                  <Point X="4.406686192574" Y="-1.036943967411" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.901713258482" Y="2.758612716121" />
                  <Point X="3.966052921282" Y="2.352388072669" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.173691282099" Y="1.041411057552" />
                  <Point X="4.248290127835" Y="0.570412482297" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.447334837201" Y="-0.686306352955" />
                  <Point X="4.504918696314" Y="-1.049876530649" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.009587737976" Y="2.684803113865" />
                  <Point X="4.051814285179" Y="2.418195187504" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.267910833062" Y="1.053815280994" />
                  <Point X="4.340558540692" Y="0.595135706902" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.547781895022" Y="-0.713221060371" />
                  <Point X="4.603151200054" Y="-1.062809093888" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.362130384024" Y="1.066219504436" />
                  <Point X="4.432826954763" Y="0.619858923852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.648228952842" Y="-0.740135767787" />
                  <Point X="4.701383703795" Y="-1.075741657126" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.456349934986" Y="1.078623727878" />
                  <Point X="4.525095369894" Y="0.644582134104" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.748676010663" Y="-0.767050475203" />
                  <Point X="4.767971724993" Y="-0.888878820781" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.550569485948" Y="1.09102795132" />
                  <Point X="4.617363785025" Y="0.669305344356" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.644789036911" Y="1.103432174762" />
                  <Point X="4.709632200156" Y="0.694028554608" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="0.001626220703" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.68464666748" Y="-4.332440917969" />
                  <Point X="0.471539031982" Y="-3.537112304688" />
                  <Point X="0.464318664551" Y="-3.521544189453" />
                  <Point X="0.377216278076" Y="-3.396045898438" />
                  <Point X="0.300591033936" Y="-3.285643798828" />
                  <Point X="0.274335754395" Y="-3.266399902344" />
                  <Point X="0.139549758911" Y="-3.224567138672" />
                  <Point X="0.020976625443" Y="-3.187766601562" />
                  <Point X="-0.008664756775" Y="-3.187766601562" />
                  <Point X="-0.143450897217" Y="-3.229599121094" />
                  <Point X="-0.262023895264" Y="-3.266399902344" />
                  <Point X="-0.288279144287" Y="-3.285643798828" />
                  <Point X="-0.375381561279" Y="-3.411141845703" />
                  <Point X="-0.45200680542" Y="-3.521544189453" />
                  <Point X="-0.459227142334" Y="-3.537112304688" />
                  <Point X="-0.646700744629" Y="-4.2367734375" />
                  <Point X="-0.84774395752" Y="-4.987077148438" />
                  <Point X="-0.961442016602" Y="-4.9650078125" />
                  <Point X="-1.100246948242" Y="-4.938065429688" />
                  <Point X="-1.256096801758" Y="-4.897966308594" />
                  <Point X="-1.351589599609" Y="-4.873396972656" />
                  <Point X="-1.334803344727" Y="-4.745893554688" />
                  <Point X="-1.309150146484" Y="-4.551038574219" />
                  <Point X="-1.309683227539" Y="-4.5347578125" />
                  <Point X="-1.341883666992" Y="-4.372875976562" />
                  <Point X="-1.370210693359" Y="-4.230466308594" />
                  <Point X="-1.386282714844" Y="-4.20262890625" />
                  <Point X="-1.510376586914" Y="-4.093801025391" />
                  <Point X="-1.619543457031" Y="-3.998063964844" />
                  <Point X="-1.649240722656" Y="-3.985762939453" />
                  <Point X="-1.813941040039" Y="-3.974967773438" />
                  <Point X="-1.958829833984" Y="-3.965471435547" />
                  <Point X="-1.989878540039" Y="-3.973791015625" />
                  <Point X="-2.127115722656" Y="-4.065489990234" />
                  <Point X="-2.247844970703" Y="-4.146159179688" />
                  <Point X="-2.259734130859" Y="-4.157294433594" />
                  <Point X="-2.364986083984" Y="-4.294461425781" />
                  <Point X="-2.457095214844" Y="-4.414500488281" />
                  <Point X="-2.652376953125" Y="-4.293586914062" />
                  <Point X="-2.855831054688" Y="-4.16761328125" />
                  <Point X="-3.071641601562" Y="-4.001446289062" />
                  <Point X="-3.228581054688" Y="-3.880608154297" />
                  <Point X="-2.9020703125" Y="-3.315074951172" />
                  <Point X="-2.506033691406" Y="-2.629119628906" />
                  <Point X="-2.499762695312" Y="-2.597593261719" />
                  <Point X="-2.513979736328" Y="-2.568764404297" />
                  <Point X="-2.531328369141" Y="-2.551415771484" />
                  <Point X="-2.560157226562" Y="-2.537198730469" />
                  <Point X="-2.59168359375" Y="-2.543469726562" />
                  <Point X="-3.195127441406" Y="-2.891868164062" />
                  <Point X="-3.842959472656" Y="-3.265894042969" />
                  <Point X="-4.000961425781" Y="-3.058312011719" />
                  <Point X="-4.161703613281" Y="-2.847129394531" />
                  <Point X="-4.316420898438" Y="-2.587692382812" />
                  <Point X="-4.431020019531" Y="-2.395527099609" />
                  <Point X="-3.856722167969" Y="-1.954852905273" />
                  <Point X="-3.163786865234" Y="-1.423144897461" />
                  <Point X="-3.145822021484" Y="-1.396013305664" />
                  <Point X="-3.138117431641" Y="-1.366265869141" />
                  <Point X="-3.140326416016" Y="-1.334595458984" />
                  <Point X="-3.161159179688" Y="-1.310639160156" />
                  <Point X="-3.187641845703" Y="-1.295052734375" />
                  <Point X="-3.219529296875" Y="-1.288571044922" />
                  <Point X="-3.981320068359" Y="-1.388862670898" />
                  <Point X="-4.803283203125" Y="-1.497076293945" />
                  <Point X="-4.864525390625" Y="-1.257315551758" />
                  <Point X="-4.927393066406" Y="-1.011191650391" />
                  <Point X="-4.968327148438" Y="-0.724983154297" />
                  <Point X="-4.998396484375" Y="-0.514742126465" />
                  <Point X="-4.346821777344" Y="-0.340153106689" />
                  <Point X="-3.557463867188" Y="-0.128645431519" />
                  <Point X="-3.527131347656" Y="-0.111177764893" />
                  <Point X="-3.50232421875" Y="-0.090645141602" />
                  <Point X="-3.489977050781" Y="-0.060050106049" />
                  <Point X="-3.483400878906" Y="-0.031279644012" />
                  <Point X="-3.490569335938" Y="-0.000601839721" />
                  <Point X="-3.502324462891" Y="0.028085380554" />
                  <Point X="-3.528907226562" Y="0.049850269318" />
                  <Point X="-3.557463867188" Y="0.066085334778" />
                  <Point X="-4.251872558594" Y="0.252151412964" />
                  <Point X="-4.998186523438" Y="0.452125793457" />
                  <Point X="-4.958162109375" Y="0.722608642578" />
                  <Point X="-4.917645019531" Y="0.996418212891" />
                  <Point X="-4.835245605469" Y="1.300498168945" />
                  <Point X="-4.773516113281" Y="1.528298583984" />
                  <Point X="-4.319097167969" Y="1.468473388672" />
                  <Point X="-3.753266357422" Y="1.393980102539" />
                  <Point X="-3.731704589844" Y="1.395866699219" />
                  <Point X="-3.699026123047" Y="1.406170166016" />
                  <Point X="-3.670278564453" Y="1.41523425293" />
                  <Point X="-3.651534423828" Y="1.426056152344" />
                  <Point X="-3.639120117188" Y="1.443785888672" />
                  <Point X="-3.6260078125" Y="1.475441894531" />
                  <Point X="-3.61447265625" Y="1.503290161133" />
                  <Point X="-3.610714111328" Y="1.524605224609" />
                  <Point X="-3.616315917969" Y="1.54551159668" />
                  <Point X="-3.632137451172" Y="1.575904418945" />
                  <Point X="-3.646055664062" Y="1.602641235352" />
                  <Point X="-3.659968261719" Y="1.619221679688" />
                  <Point X="-4.058282714844" Y="1.924859008789" />
                  <Point X="-4.47610546875" Y="2.245466308594" />
                  <Point X="-4.317448730469" Y="2.517283691406" />
                  <Point X="-4.160014648438" Y="2.787006591797" />
                  <Point X="-3.941741210938" Y="3.067566162109" />
                  <Point X="-3.774671142578" Y="3.282310791016" />
                  <Point X="-3.506674560547" Y="3.127582763672" />
                  <Point X="-3.159156982422" Y="2.926943359375" />
                  <Point X="-3.138514892578" Y="2.920434814453" />
                  <Point X="-3.093002929688" Y="2.916453125" />
                  <Point X="-3.052965576172" Y="2.912950195312" />
                  <Point X="-3.031506835938" Y="2.915775390625" />
                  <Point X="-3.013252441406" Y="2.927404541016" />
                  <Point X="-2.980947753906" Y="2.959709228516" />
                  <Point X="-2.952528808594" Y="2.988127929688" />
                  <Point X="-2.940899414062" Y="3.006382324219" />
                  <Point X="-2.93807421875" Y="3.027841064453" />
                  <Point X="-2.942056152344" Y="3.073353027344" />
                  <Point X="-2.945558837891" Y="3.113390625" />
                  <Point X="-2.952067382812" Y="3.134032958984" />
                  <Point X="-3.128572509766" Y="3.439748779297" />
                  <Point X="-3.307278564453" Y="3.749276611328" />
                  <Point X="-3.027075927734" Y="3.964105224609" />
                  <Point X="-2.752876220703" Y="4.174331542969" />
                  <Point X="-2.409102294922" Y="4.365324707031" />
                  <Point X="-2.141548583984" Y="4.513971679688" />
                  <Point X="-2.074212646484" Y="4.426217773438" />
                  <Point X="-1.967826660156" Y="4.287573242188" />
                  <Point X="-1.951246948242" Y="4.273661132813" />
                  <Point X="-1.900592407227" Y="4.247291992188" />
                  <Point X="-1.856030761719" Y="4.224094238281" />
                  <Point X="-1.835124267578" Y="4.2184921875" />
                  <Point X="-1.813809082031" Y="4.222250488281" />
                  <Point X="-1.761048950195" Y="4.244104980469" />
                  <Point X="-1.714635009766" Y="4.263330078125" />
                  <Point X="-1.696905395508" Y="4.275744140625" />
                  <Point X="-1.686083496094" Y="4.29448828125" />
                  <Point X="-1.668911010742" Y="4.348952148438" />
                  <Point X="-1.653804077148" Y="4.396864746094" />
                  <Point X="-1.651917602539" Y="4.418426757812" />
                  <Point X="-1.67198425293" Y="4.570848144531" />
                  <Point X="-1.689137573242" Y="4.701141113281" />
                  <Point X="-1.323062866211" Y="4.803775390625" />
                  <Point X="-0.968094177246" Y="4.903296386719" />
                  <Point X="-0.551336303711" Y="4.952071777344" />
                  <Point X="-0.224199630737" Y="4.990358398438" />
                  <Point X="-0.144945755005" Y="4.694578125" />
                  <Point X="-0.042140380859" Y="4.310903320312" />
                  <Point X="-0.024282115936" Y="4.284176757813" />
                  <Point X="0.006155934334" Y="4.273844238281" />
                  <Point X="0.036594036102" Y="4.284176757813" />
                  <Point X="0.054452251434" Y="4.310903320312" />
                  <Point X="0.144891479492" Y="4.648427246094" />
                  <Point X="0.236648422241" Y="4.990868652344" />
                  <Point X="0.550265136719" Y="4.958024902344" />
                  <Point X="0.860205871582" Y="4.925565429688" />
                  <Point X="1.205000976562" Y="4.842321289062" />
                  <Point X="1.508455932617" Y="4.769058105469" />
                  <Point X="1.733163452148" Y="4.687555175781" />
                  <Point X="1.931033691406" Y="4.615786132813" />
                  <Point X="2.148045410156" Y="4.514296875" />
                  <Point X="2.33869921875" Y="4.425134277344" />
                  <Point X="2.548342285156" Y="4.30299609375" />
                  <Point X="2.732523681641" Y="4.19569140625" />
                  <Point X="2.930243896484" Y="4.055083740234" />
                  <Point X="3.068740234375" Y="3.956592773438" />
                  <Point X="2.688236083984" Y="3.297539794922" />
                  <Point X="2.229853515625" Y="2.503597900391" />
                  <Point X="2.224852050781" Y="2.491514648438" />
                  <Point X="2.213430175781" Y="2.448802734375" />
                  <Point X="2.203382324219" Y="2.411228271484" />
                  <Point X="2.202044677734" Y="2.392325927734" />
                  <Point X="2.206498291016" Y="2.355392333984" />
                  <Point X="2.210416015625" Y="2.322901123047" />
                  <Point X="2.218682128906" Y="2.300812255859" />
                  <Point X="2.241535400391" Y="2.267132324219" />
                  <Point X="2.261639892578" Y="2.237503662109" />
                  <Point X="2.274939941406" Y="2.224203857422" />
                  <Point X="2.308619873047" Y="2.201350585938" />
                  <Point X="2.338248535156" Y="2.181246337891" />
                  <Point X="2.360336914062" Y="2.172980224609" />
                  <Point X="2.397270751953" Y="2.168526611328" />
                  <Point X="2.42976171875" Y="2.164608642578" />
                  <Point X="2.448664306641" Y="2.165946289062" />
                  <Point X="2.491376464844" Y="2.177368164062" />
                  <Point X="2.528950683594" Y="2.187416015625" />
                  <Point X="2.541033935547" Y="2.192417480469" />
                  <Point X="3.239475097656" Y="2.595662597656" />
                  <Point X="3.994248046875" Y="3.031430908203" />
                  <Point X="4.093341796875" Y="2.893713134766" />
                  <Point X="4.202591796875" Y="2.741880126953" />
                  <Point X="4.312817871094" Y="2.559730712891" />
                  <Point X="4.387513183594" Y="2.436296142578" />
                  <Point X="3.892119873047" Y="2.056167236328" />
                  <Point X="3.288616210938" Y="1.593082763672" />
                  <Point X="3.279371337891" Y="1.583833129883" />
                  <Point X="3.248631347656" Y="1.54373059082" />
                  <Point X="3.221589111328" Y="1.508451782227" />
                  <Point X="3.213119628906" Y="1.4915" />
                  <Point X="3.201668945312" Y="1.450555053711" />
                  <Point X="3.191595703125" Y="1.414535522461" />
                  <Point X="3.190779541016" Y="1.390965209961" />
                  <Point X="3.200179199219" Y="1.345408813477" />
                  <Point X="3.208448486328" Y="1.305332397461" />
                  <Point X="3.215646484375" Y="1.287954956055" />
                  <Point X="3.241212890625" Y="1.249095214844" />
                  <Point X="3.263704101562" Y="1.214909667969" />
                  <Point X="3.280947998047" Y="1.198819824219" />
                  <Point X="3.317997314453" Y="1.177964233398" />
                  <Point X="3.350590087891" Y="1.159617431641" />
                  <Point X="3.368565429688" Y="1.153619628906" />
                  <Point X="3.418658447266" Y="1.146999145508" />
                  <Point X="3.462726074219" Y="1.141175048828" />
                  <Point X="3.475803710938" Y="1.14117175293" />
                  <Point X="4.139275878906" Y="1.22851965332" />
                  <Point X="4.848975097656" Y="1.32195324707" />
                  <Point X="4.892266113281" Y="1.144127441406" />
                  <Point X="4.939188476562" Y="0.951385620117" />
                  <Point X="4.973923828125" Y="0.728285644531" />
                  <Point X="4.997858398438" Y="0.574556152344" />
                  <Point X="4.432795898438" Y="0.423148071289" />
                  <Point X="3.741167724609" Y="0.237826919556" />
                  <Point X="3.729086914062" Y="0.232819412231" />
                  <Point X="3.679872070313" Y="0.204372283936" />
                  <Point X="3.636576904297" Y="0.179346878052" />
                  <Point X="3.622264892578" Y="0.166926574707" />
                  <Point X="3.592736083984" Y="0.129299850464" />
                  <Point X="3.566759033203" Y="0.096199043274" />
                  <Point X="3.556985351562" Y="0.074735275269" />
                  <Point X="3.547142333984" Y="0.023338832855" />
                  <Point X="3.538483154297" Y="-0.021875152588" />
                  <Point X="3.538483154297" Y="-0.040684940338" />
                  <Point X="3.548326171875" Y="-0.092081237793" />
                  <Point X="3.556985351562" Y="-0.13729536438" />
                  <Point X="3.566759033203" Y="-0.158758987427" />
                  <Point X="3.596287841797" Y="-0.196385864258" />
                  <Point X="3.622264892578" Y="-0.229486679077" />
                  <Point X="3.636576904297" Y="-0.241906967163" />
                  <Point X="3.685791992188" Y="-0.270354095459" />
                  <Point X="3.729086914062" Y="-0.295379333496" />
                  <Point X="3.741167724609" Y="-0.300387023926" />
                  <Point X="4.349602050781" Y="-0.463416442871" />
                  <Point X="4.998067871094" Y="-0.637172363281" />
                  <Point X="4.974630859375" Y="-0.79262487793" />
                  <Point X="4.948431640625" Y="-0.966399169922" />
                  <Point X="4.903930175781" Y="-1.161412597656" />
                  <Point X="4.874545898438" Y="-1.290178344727" />
                  <Point X="4.214076171875" Y="-1.203225830078" />
                  <Point X="3.411982177734" Y="-1.097628295898" />
                  <Point X="3.394836181641" Y="-1.098341186523" />
                  <Point X="3.298244628906" Y="-1.11933581543" />
                  <Point X="3.213271972656" Y="-1.137804931641" />
                  <Point X="3.1854453125" Y="-1.154697265625" />
                  <Point X="3.127061767578" Y="-1.224914428711" />
                  <Point X="3.075701171875" Y="-1.286685424805" />
                  <Point X="3.064357910156" Y="-1.314070556641" />
                  <Point X="3.055990234375" Y="-1.405004882813" />
                  <Point X="3.04862890625" Y="-1.485000854492" />
                  <Point X="3.056360595703" Y="-1.516621948242" />
                  <Point X="3.109815673828" Y="-1.599767944336" />
                  <Point X="3.156840820312" Y="-1.672912475586" />
                  <Point X="3.168460693359" Y="-1.685540649414" />
                  <Point X="3.733091552734" Y="-2.118797119141" />
                  <Point X="4.339074707031" Y="-2.583784179688" />
                  <Point X="4.277985351562" Y="-2.682635498047" />
                  <Point X="4.204131835938" Y="-2.802141845703" />
                  <Point X="4.112099121094" Y="-2.932906982422" />
                  <Point X="4.056688476562" Y="-3.011638427734" />
                  <Point X="3.467827636719" Y="-2.671659179688" />
                  <Point X="2.753454589844" Y="-2.259215576172" />
                  <Point X="2.737340820313" Y="-2.253312744141" />
                  <Point X="2.622381591797" Y="-2.232551269531" />
                  <Point X="2.521250488281" Y="-2.214287109375" />
                  <Point X="2.489077636719" Y="-2.219244873047" />
                  <Point X="2.393574707031" Y="-2.269507324219" />
                  <Point X="2.309559570312" Y="-2.313723876953" />
                  <Point X="2.288599853516" Y="-2.33468359375" />
                  <Point X="2.238337402344" Y="-2.430186523438" />
                  <Point X="2.194120849609" Y="-2.514201660156" />
                  <Point X="2.189163085938" Y="-2.546374511719" />
                  <Point X="2.209924560547" Y="-2.661333740234" />
                  <Point X="2.228188720703" Y="-2.76246484375" />
                  <Point X="2.234091552734" Y="-2.778578613281" />
                  <Point X="2.596923583984" Y="-3.407021972656" />
                  <Point X="2.986673583984" Y="-4.082088623047" />
                  <Point X="2.922932373047" Y="-4.1276171875" />
                  <Point X="2.835296875" Y="-4.190213378906" />
                  <Point X="2.732400390625" Y="-4.25681640625" />
                  <Point X="2.679775878906" Y="-4.290879394531" />
                  <Point X="2.228310058594" Y="-3.702517822266" />
                  <Point X="1.683177612305" Y="-2.992087402344" />
                  <Point X="1.670549316406" Y="-2.980467529297" />
                  <Point X="1.557168457031" Y="-2.907573974609" />
                  <Point X="1.45742590332" Y="-2.843448730469" />
                  <Point X="1.4258046875" Y="-2.835717041016" />
                  <Point X="1.301803466797" Y="-2.847127685547" />
                  <Point X="1.192717773438" Y="-2.857166015625" />
                  <Point X="1.165332885742" Y="-2.868509277344" />
                  <Point X="1.069582275391" Y="-2.948122802734" />
                  <Point X="0.985349243164" Y="-3.018159667969" />
                  <Point X="0.968456726074" Y="-3.045986328125" />
                  <Point X="0.939827819824" Y="-3.177702148438" />
                  <Point X="0.91464251709" Y="-3.29357421875" />
                  <Point X="0.91392956543" Y="-3.310720214844" />
                  <Point X="1.016753356934" Y="-4.091744628906" />
                  <Point X="1.127642211914" Y="-4.934028320312" />
                  <Point X="1.077253173828" Y="-4.945073730469" />
                  <Point X="0.994346313477" Y="-4.963246582031" />
                  <Point X="0.899287902832" Y="-4.980515625" />
                  <Point X="0.860200378418" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#170" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.102805347483" Y="4.73879927325" Z="1.35" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.35" />
                  <Point X="-0.563304405554" Y="5.033012932527" Z="1.35" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.35" />
                  <Point X="-1.342716655857" Y="4.883199221201" Z="1.35" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.35" />
                  <Point X="-1.727712534092" Y="4.595601985722" Z="1.35" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.35" />
                  <Point X="-1.72275271033" Y="4.395267975064" Z="1.35" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.35" />
                  <Point X="-1.786338922581" Y="4.321578722722" Z="1.35" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.35" />
                  <Point X="-1.883660584239" Y="4.322922010621" Z="1.35" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.35" />
                  <Point X="-2.040700872544" Y="4.487935890493" Z="1.35" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.35" />
                  <Point X="-2.439541199834" Y="4.440312298846" Z="1.35" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.35" />
                  <Point X="-3.063654017963" Y="4.035065168983" Z="1.35" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.35" />
                  <Point X="-3.17802982978" Y="3.446028767819" Z="1.35" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.35" />
                  <Point X="-2.998021829762" Y="3.100275476023" Z="1.35" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.35" />
                  <Point X="-3.022458869146" Y="3.02634469874" Z="1.35" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.35" />
                  <Point X="-3.094800989583" Y="2.997542869411" Z="1.35" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.35" />
                  <Point X="-3.487830580478" Y="3.202164118966" Z="1.35" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.35" />
                  <Point X="-3.987360446131" Y="3.129548652619" Z="1.35" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.35" />
                  <Point X="-4.366786822006" Y="2.573768859208" Z="1.35" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.35" />
                  <Point X="-4.094876625441" Y="1.916471426538" Z="1.35" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.35" />
                  <Point X="-3.682643920398" Y="1.584097178585" Z="1.35" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.35" />
                  <Point X="-3.678357548279" Y="1.525856148576" Z="1.35" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.35" />
                  <Point X="-3.720217593258" Y="1.485136008938" Z="1.35" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.35" />
                  <Point X="-4.318726998419" Y="1.549325612174" Z="1.35" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.35" />
                  <Point X="-4.889661232072" Y="1.344855634826" Z="1.35" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.35" />
                  <Point X="-5.013779375529" Y="0.761207788538" Z="1.35" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.35" />
                  <Point X="-4.270969201986" Y="0.235135222693" Z="1.35" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.35" />
                  <Point X="-3.563572280561" Y="0.040054302478" Z="1.35" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.35" />
                  <Point X="-3.544478299986" Y="0.015857180852" Z="1.35" />
                  <Point X="-3.539556741714" Y="0" Z="1.35" />
                  <Point X="-3.543886301149" Y="-0.013949770209" Z="1.35" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.35" />
                  <Point X="-3.561796314562" Y="-0.038821680656" Z="1.35" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.35" />
                  <Point X="-4.365918586786" Y="-0.260576834063" Z="1.35" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.35" />
                  <Point X="-5.023979373959" Y="-0.700782163225" Z="1.35" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.35" />
                  <Point X="-4.919126259509" Y="-1.238409788058" Z="1.35" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.35" />
                  <Point X="-3.980949679484" Y="-1.407154949071" Z="1.35" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.35" />
                  <Point X="-3.206764935393" Y="-1.31415784904" Z="1.35" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.35" />
                  <Point X="-3.19628204035" Y="-1.336371687942" Z="1.35" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.35" />
                  <Point X="-3.893316217205" Y="-1.883905192708" Z="1.35" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.35" />
                  <Point X="-4.365520048739" Y="-2.582021936355" Z="1.35" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.35" />
                  <Point X="-4.046903228686" Y="-3.057315111949" Z="1.35" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.35" />
                  <Point X="-3.176283476995" Y="-2.903889509186" Z="1.35" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.35" />
                  <Point X="-2.564720319296" Y="-2.563610149835" Z="1.35" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.35" />
                  <Point X="-2.951527503982" Y="-3.258794905497" Z="1.35" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.35" />
                  <Point X="-3.108301585304" Y="-4.009783333633" Z="1.35" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.35" />
                  <Point X="-2.684854354761" Y="-4.304817669649" Z="1.35" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.35" />
                  <Point X="-2.331474243682" Y="-4.293619164938" Z="1.35" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.35" />
                  <Point X="-2.105493044099" Y="-4.075783366075" Z="1.35" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.35" />
                  <Point X="-1.823367059259" Y="-3.993580900763" Z="1.35" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.35" />
                  <Point X="-1.549499845498" Y="-4.100112957746" Z="1.35" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.35" />
                  <Point X="-1.397078897604" Y="-4.351350228926" Z="1.35" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.35" />
                  <Point X="-1.390531659925" Y="-4.70808697415" Z="1.35" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.35" />
                  <Point X="-1.274711593401" Y="-4.915109139864" Z="1.35" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.35" />
                  <Point X="-0.97712526191" Y="-4.982811750118" Z="1.35" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.35" />
                  <Point X="-0.604560402486" Y="-4.218434212858" Z="1.35" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.35" />
                  <Point X="-0.340461572849" Y="-3.408370763891" Z="1.35" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.35" />
                  <Point X="-0.134786034766" Y="-3.246071979561" Z="1.35" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.35" />
                  <Point X="0.118573044595" Y="-3.241040065727" Z="1.35" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.35" />
                  <Point X="0.329984286584" Y="-3.393274991876" Z="1.35" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.35" />
                  <Point X="0.630194457915" Y="-4.314101816958" Z="1.35" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.35" />
                  <Point X="0.9020688815" Y="-4.998430405642" Z="1.35" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.35" />
                  <Point X="1.081802899966" Y="-4.962634056388" Z="1.35" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.35" />
                  <Point X="1.060169612329" Y="-4.053938120299" Z="1.35" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.35" />
                  <Point X="0.982531055887" Y="-3.157041686981" Z="1.35" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.35" />
                  <Point X="1.09539311124" Y="-2.955288957078" Z="1.35" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.35" />
                  <Point X="1.300229244985" Y="-2.865637293466" Z="1.35" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.35" />
                  <Point X="1.523973063987" Y="-2.918351870061" Z="1.35" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.35" />
                  <Point X="2.182486426722" Y="-3.701675668991" Z="1.35" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.35" />
                  <Point X="2.753413630783" Y="-4.267510547051" Z="1.35" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.35" />
                  <Point X="2.945838185923" Y="-4.137024367446" Z="1.35" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.35" />
                  <Point X="2.634068973584" Y="-3.350741951222" Z="1.35" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.35" />
                  <Point X="2.252972676485" Y="-2.621167446835" Z="1.35" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.35" />
                  <Point X="2.276427695506" Y="-2.422193059356" Z="1.35" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.35" />
                  <Point X="2.410705319016" Y="-2.282473614461" Z="1.35" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.35" />
                  <Point X="2.607339356677" Y="-2.250475333006" Z="1.35" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.35" />
                  <Point X="3.436671645885" Y="-2.683680526691" Z="1.35" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.35" />
                  <Point X="4.146831866609" Y="-2.930404156577" Z="1.35" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.35" />
                  <Point X="4.314362464013" Y="-2.677640604754" Z="1.35" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.35" />
                  <Point X="3.757373522278" Y="-2.047849459645" Z="1.35" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.35" />
                  <Point X="3.145717640908" Y="-1.541448446371" Z="1.35" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.35" />
                  <Point X="3.099623717119" Y="-1.378306461608" Z="1.35" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.35" />
                  <Point X="3.15935211385" Y="-1.225601306176" Z="1.35" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.35" />
                  <Point X="3.30270834925" Y="-1.136914988913" Z="1.35" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.35" />
                  <Point X="4.201393996645" Y="-1.22151811288" Z="1.35" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.35" />
                  <Point X="4.946521083687" Y="-1.14125654214" Z="1.35" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.35" />
                  <Point X="5.017916520204" Y="-0.768798931806" Z="1.35" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.35" />
                  <Point X="4.356387019256" Y="-0.383840113975" Z="1.35" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.35" />
                  <Point X="3.704657663221" Y="-0.195785329771" Z="1.35" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.35" />
                  <Point X="3.629465692827" Y="-0.134237377034" Z="1.35" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.35" />
                  <Point X="3.591277716421" Y="-0.051396423037" Z="1.35" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.35" />
                  <Point X="3.590093734003" Y="0.045214108171" Z="1.35" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.35" />
                  <Point X="3.625913745573" Y="0.129711361541" Z="1.35" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.35" />
                  <Point X="3.698737751131" Y="0.192363514471" Z="1.35" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.35" />
                  <Point X="4.43958079805" Y="0.406131818794" Z="1.35" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.35" />
                  <Point X="5.017172613612" Y="0.767257664548" Z="1.35" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.35" />
                  <Point X="4.93469125418" Y="1.18723438052" Z="1.35" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.35" />
                  <Point X="4.126593760117" Y="1.309371901446" Z="1.35" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.35" />
                  <Point X="3.419053792065" Y="1.227848156233" Z="1.35" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.35" />
                  <Point X="3.336367858012" Y="1.252815480718" Z="1.35" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.35" />
                  <Point X="3.276827426919" Y="1.307856552542" Z="1.35" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.35" />
                  <Point X="3.242991852003" Y="1.386792891309" Z="1.35" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.35" />
                  <Point X="3.243665327717" Y="1.468368892274" Z="1.35" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.35" />
                  <Point X="3.282158269935" Y="1.544592493117" Z="1.35" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.35" />
                  <Point X="3.916401800489" Y="2.047779716284" Z="1.35" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.35" />
                  <Point X="4.349439197939" Y="2.616896967672" Z="1.35" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.35" />
                  <Point X="4.127771171594" Y="2.954196143936" Z="1.35" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.35" />
                  <Point X="3.208319140857" Y="2.670243971639" Z="1.35" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.35" />
                  <Point X="2.472303632956" Y="2.256950913952" Z="1.35" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.35" />
                  <Point X="2.397100544908" Y="2.24944700097" Z="1.35" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.35" />
                  <Point X="2.330538060356" Y="2.27400482748" Z="1.35" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.35" />
                  <Point X="2.276753772755" Y="2.326486800026" Z="1.35" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.35" />
                  <Point X="2.249982701789" Y="2.392657898924" Z="1.35" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.35" />
                  <Point X="2.255577028706" Y="2.467165942926" Z="1.35" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.35" />
                  <Point X="2.725381381221" Y="3.303819941098" Z="1.35" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.35" />
                  <Point X="2.953064857973" Y="4.127110962929" Z="1.35" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.35" />
                  <Point X="2.567356240633" Y="4.377478527703" Z="1.35" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.35" />
                  <Point X="2.163070769038" Y="4.590868983551" Z="1.35" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.35" />
                  <Point X="1.744056127644" Y="4.765838896284" Z="1.35" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.35" />
                  <Point X="1.210579276667" Y="4.922205065431" Z="1.35" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.35" />
                  <Point X="0.549317033713" Y="5.039032734664" Z="1.35" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.35" />
                  <Point X="0.09043921407" Y="4.692648232701" Z="1.35" />
                  <Point X="0" Y="4.355124473572" Z="1.35" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>