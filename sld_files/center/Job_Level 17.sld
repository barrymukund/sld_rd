<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#149" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1342" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="0" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="0.004716064453" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.689669555664" Y="-3.984134765625" />
                  <Point X="0.563302062988" Y="-3.512524414063" />
                  <Point X="0.557721191406" Y="-3.497141845703" />
                  <Point X="0.54236315918" Y="-3.467377197266" />
                  <Point X="0.490713623047" Y="-3.392959716797" />
                  <Point X="0.378635528564" Y="-3.231476806641" />
                  <Point X="0.356751586914" Y="-3.209021484375" />
                  <Point X="0.330496398926" Y="-3.189777587891" />
                  <Point X="0.302495330811" Y="-3.175669433594" />
                  <Point X="0.222570541382" Y="-3.150863525391" />
                  <Point X="0.049136341095" Y="-3.097036132812" />
                  <Point X="0.020976638794" Y="-3.092766601562" />
                  <Point X="-0.00866468811" Y="-3.092766601562" />
                  <Point X="-0.036824085236" Y="-3.097035888672" />
                  <Point X="-0.116749023438" Y="-3.121841552734" />
                  <Point X="-0.290183227539" Y="-3.175669189453" />
                  <Point X="-0.31818460083" Y="-3.189777587891" />
                  <Point X="-0.344439788818" Y="-3.209021484375" />
                  <Point X="-0.366323577881" Y="-3.231476806641" />
                  <Point X="-0.417973144531" Y="-3.305894042969" />
                  <Point X="-0.530051208496" Y="-3.467377197266" />
                  <Point X="-0.538189025879" Y="-3.481573730469" />
                  <Point X="-0.550990112305" Y="-3.512524414063" />
                  <Point X="-0.825203613281" Y="-4.535903808594" />
                  <Point X="-0.916584594727" Y="-4.87694140625" />
                  <Point X="-1.07933996582" Y="-4.845350585938" />
                  <Point X="-1.168395874023" Y="-4.822437011719" />
                  <Point X="-1.24641809082" Y="-4.802362792969" />
                  <Point X="-1.230174682617" Y="-4.678982421875" />
                  <Point X="-1.214963012695" Y="-4.563438476562" />
                  <Point X="-1.214201171875" Y="-4.5479296875" />
                  <Point X="-1.216508666992" Y="-4.516224121094" />
                  <Point X="-1.235602783203" Y="-4.420231933594" />
                  <Point X="-1.277036132812" Y="-4.211932617188" />
                  <Point X="-1.287938354492" Y="-4.182966308594" />
                  <Point X="-1.304010375977" Y="-4.15512890625" />
                  <Point X="-1.323644775391" Y="-4.131204101562" />
                  <Point X="-1.397229370117" Y="-4.066672119141" />
                  <Point X="-1.556905517578" Y="-3.926639404297" />
                  <Point X="-1.583188476562" Y="-3.910295410156" />
                  <Point X="-1.612885742188" Y="-3.897994384766" />
                  <Point X="-1.64302734375" Y="-3.890966308594" />
                  <Point X="-1.740690673828" Y="-3.884565185547" />
                  <Point X="-1.952616577148" Y="-3.870674804688" />
                  <Point X="-1.98341796875" Y="-3.873708496094" />
                  <Point X="-2.014466674805" Y="-3.882028076172" />
                  <Point X="-2.042657714844" Y="-3.894801513672" />
                  <Point X="-2.124036132812" Y="-3.949176757812" />
                  <Point X="-2.300624267578" Y="-4.067169677734" />
                  <Point X="-2.312785644531" Y="-4.076821533203" />
                  <Point X="-2.335102539062" Y="-4.099461914062" />
                  <Point X="-2.480148925781" Y="-4.288489746094" />
                  <Point X="-2.507229003906" Y="-4.27172265625" />
                  <Point X="-2.801706787109" Y="-4.089389404297" />
                  <Point X="-2.925023925781" Y="-3.994439453125" />
                  <Point X="-3.104721923828" Y="-3.856077880859" />
                  <Point X="-2.658601318359" Y="-3.083374267578" />
                  <Point X="-2.423761230469" Y="-2.676619628906" />
                  <Point X="-2.412859130859" Y="-2.647653320312" />
                  <Point X="-2.406588134766" Y="-2.616126953125" />
                  <Point X="-2.405575439453" Y="-2.585192871094" />
                  <Point X="-2.414560058594" Y="-2.555575195312" />
                  <Point X="-2.428777099609" Y="-2.526746337891" />
                  <Point X="-2.4468046875" Y="-2.501589355469" />
                  <Point X="-2.464153320312" Y="-2.484240722656" />
                  <Point X="-2.489310302734" Y="-2.466213134766" />
                  <Point X="-2.518139160156" Y="-2.45199609375" />
                  <Point X="-2.547756835938" Y="-2.443011474609" />
                  <Point X="-2.578690917969" Y="-2.444024169922" />
                  <Point X="-2.610217285156" Y="-2.450295166016" />
                  <Point X="-2.63918359375" Y="-2.461197265625" />
                  <Point X="-3.521828125" Y="-2.970792480469" />
                  <Point X="-3.818024414062" Y="-3.141801269531" />
                  <Point X="-3.850204833984" Y="-3.099522460938" />
                  <Point X="-4.082860595703" Y="-2.793860595703" />
                  <Point X="-4.171265136719" Y="-2.645619384766" />
                  <Point X="-4.306142578125" Y="-2.419450439453" />
                  <Point X="-3.516848388672" Y="-1.813803466797" />
                  <Point X="-3.105954589844" Y="-1.498513549805" />
                  <Point X="-3.083062744141" Y="-1.475877441406" />
                  <Point X="-3.063553955078" Y="-1.444357055664" />
                  <Point X="-3.054409912109" Y="-1.419905395508" />
                  <Point X="-3.051426269531" Y="-1.41044909668" />
                  <Point X="-3.046152099609" Y="-1.39008581543" />
                  <Point X="-3.042037597656" Y="-1.358602783203" />
                  <Point X="-3.042736816406" Y="-1.335734619141" />
                  <Point X="-3.048883056641" Y="-1.313696655273" />
                  <Point X="-3.060887939453" Y="-1.284713989258" />
                  <Point X="-3.073874267578" Y="-1.26248046875" />
                  <Point X="-3.092354003906" Y="-1.24455065918" />
                  <Point X="-3.113210449219" Y="-1.229204223633" />
                  <Point X="-3.121327392578" Y="-1.223849609375" />
                  <Point X="-3.139455810547" Y="-1.213180175781" />
                  <Point X="-3.168718261719" Y="-1.201956420898" />
                  <Point X="-3.200605957031" Y="-1.195474853516" />
                  <Point X="-3.231929199219" Y="-1.194383789062" />
                  <Point X="-4.346184570313" Y="-1.34107824707" />
                  <Point X="-4.732102050781" Y="-1.391885375977" />
                  <Point X="-4.743083984375" Y="-1.34889074707" />
                  <Point X="-4.834077636719" Y="-0.992654418945" />
                  <Point X="-4.857467285156" Y="-0.829117614746" />
                  <Point X="-4.892424316406" Y="-0.584698181152" />
                  <Point X="-4.000945800781" Y="-0.345827148438" />
                  <Point X="-3.532875976562" Y="-0.220408340454" />
                  <Point X="-3.51189453125" Y="-0.212002655029" />
                  <Point X="-3.487571533203" Y="-0.198705963135" />
                  <Point X="-3.478974121094" Y="-0.1933931427" />
                  <Point X="-3.459976074219" Y="-0.180207565308" />
                  <Point X="-3.436020507812" Y="-0.158680648804" />
                  <Point X="-3.415002929688" Y="-0.128061187744" />
                  <Point X="-3.404659179688" Y="-0.10392074585" />
                  <Point X="-3.401250244141" Y="-0.094665023804" />
                  <Point X="-3.394917480469" Y="-0.074261047363" />
                  <Point X="-3.389474365234" Y="-0.045520599365" />
                  <Point X="-3.390489501953" Y="-0.011468545914" />
                  <Point X="-3.395654785156" Y="0.012755381584" />
                  <Point X="-3.397835693359" Y="0.021103855133" />
                  <Point X="-3.404168457031" Y="0.041507823944" />
                  <Point X="-3.417485107422" Y="0.07083190918" />
                  <Point X="-3.440580566406" Y="0.100284255981" />
                  <Point X="-3.461153808594" Y="0.117878440857" />
                  <Point X="-3.468730957031" Y="0.123723884583" />
                  <Point X="-3.487729003906" Y="0.136909622192" />
                  <Point X="-3.501925292969" Y="0.145047103882" />
                  <Point X="-3.532875976562" Y="0.157848342896" />
                  <Point X="-4.548572265625" Y="0.430003234863" />
                  <Point X="-4.89181640625" Y="0.521975219727" />
                  <Point X="-4.883131347656" Y="0.580666931152" />
                  <Point X="-4.82448828125" Y="0.976970825195" />
                  <Point X="-4.777404296875" Y="1.150726806641" />
                  <Point X="-4.703551269531" Y="1.423267822266" />
                  <Point X="-4.101190429687" Y="1.343965454102" />
                  <Point X="-3.765666259766" Y="1.29979284668" />
                  <Point X="-3.744986083984" Y="1.299341674805" />
                  <Point X="-3.723424560547" Y="1.301228149414" />
                  <Point X="-3.703137451172" Y="1.305263671875" />
                  <Point X="-3.683760009766" Y="1.311373291016" />
                  <Point X="-3.641711425781" Y="1.324631225586" />
                  <Point X="-3.622779052734" Y="1.332961669922" />
                  <Point X="-3.604034667969" Y="1.343783569336" />
                  <Point X="-3.587353271484" Y="1.356014892578" />
                  <Point X="-3.57371484375" Y="1.371566772461" />
                  <Point X="-3.561300292969" Y="1.38929675293" />
                  <Point X="-3.5513515625" Y="1.407430664062" />
                  <Point X="-3.543576171875" Y="1.426201660156" />
                  <Point X="-3.526704101563" Y="1.466934814453" />
                  <Point X="-3.520915527344" Y="1.486794067383" />
                  <Point X="-3.517157226562" Y="1.508109008789" />
                  <Point X="-3.5158046875" Y="1.528749145508" />
                  <Point X="-3.518951171875" Y="1.549192871094" />
                  <Point X="-3.524552978516" Y="1.570099121094" />
                  <Point X="-3.532050048828" Y="1.589377929688" />
                  <Point X="-3.541431884766" Y="1.607400146484" />
                  <Point X="-3.561789794922" Y="1.646507568359" />
                  <Point X="-3.57328125" Y="1.663705932617" />
                  <Point X="-3.587193847656" Y="1.680286376953" />
                  <Point X="-3.602135986328" Y="1.694590209961" />
                  <Point X="-4.184741699219" Y="2.141639648438" />
                  <Point X="-4.351859863281" Y="2.269874023438" />
                  <Point X="-4.30901953125" Y="2.343270019531" />
                  <Point X="-4.081153808594" Y="2.733659423828" />
                  <Point X="-3.956434082031" Y="2.893968994141" />
                  <Point X="-3.750504882813" Y="3.158661865234" />
                  <Point X="-3.412726318359" Y="2.963645263672" />
                  <Point X="-3.206656982422" Y="2.844670898438" />
                  <Point X="-3.187724609375" Y="2.836340332031" />
                  <Point X="-3.167082519531" Y="2.829831787109" />
                  <Point X="-3.146794677734" Y="2.825796386719" />
                  <Point X="-3.119807128906" Y="2.823435302734" />
                  <Point X="-3.061245361328" Y="2.818311767578" />
                  <Point X="-3.040565185547" Y="2.818762939453" />
                  <Point X="-3.019106445312" Y="2.821588134766" />
                  <Point X="-2.999014648438" Y="2.826504638672" />
                  <Point X="-2.980463134766" Y="2.835653320313" />
                  <Point X="-2.962208740234" Y="2.847282714844" />
                  <Point X="-2.946077636719" Y="2.860229248047" />
                  <Point X="-2.926921630859" Y="2.879385009766" />
                  <Point X="-2.885354003906" Y="2.920952636719" />
                  <Point X="-2.872406982422" Y="2.937083984375" />
                  <Point X="-2.860777587891" Y="2.955338134766" />
                  <Point X="-2.85162890625" Y="2.973889892578" />
                  <Point X="-2.846712158203" Y="2.993981933594" />
                  <Point X="-2.843886962891" Y="3.015440917969" />
                  <Point X="-2.843435791016" Y="3.03612109375" />
                  <Point X="-2.845796875" Y="3.063108642578" />
                  <Point X="-2.850920410156" Y="3.121670654297" />
                  <Point X="-2.854955810547" Y="3.141958496094" />
                  <Point X="-2.861464355469" Y="3.162600585938" />
                  <Point X="-2.869794921875" Y="3.181532958984" />
                  <Point X="-3.127965332031" Y="3.628697021484" />
                  <Point X="-3.183333007812" Y="3.724596435547" />
                  <Point X="-3.097492675781" Y="3.790409667969" />
                  <Point X="-2.700625244141" Y="4.094683837891" />
                  <Point X="-2.504198242188" Y="4.203814453125" />
                  <Point X="-2.167036621094" Y="4.391134277344" />
                  <Point X="-2.106279541016" Y="4.311954101562" />
                  <Point X="-2.04319543457" Y="4.229741210938" />
                  <Point X="-2.028892700195" Y="4.214799804688" />
                  <Point X="-2.012312866211" Y="4.200887207031" />
                  <Point X="-1.99511315918" Y="4.18939453125" />
                  <Point X="-1.965076293945" Y="4.173758300781" />
                  <Point X="-1.899896972656" Y="4.139827636719" />
                  <Point X="-1.8806171875" Y="4.132330566406" />
                  <Point X="-1.859710571289" Y="4.126729003906" />
                  <Point X="-1.839267700195" Y="4.123582519531" />
                  <Point X="-1.818628417969" Y="4.124935058594" />
                  <Point X="-1.797312988281" Y="4.128693359375" />
                  <Point X="-1.777453491211" Y="4.134481933594" />
                  <Point X="-1.74616796875" Y="4.147440917969" />
                  <Point X="-1.678279541016" Y="4.175561523437" />
                  <Point X="-1.660144287109" Y="4.185511230469" />
                  <Point X="-1.642414794922" Y="4.19792578125" />
                  <Point X="-1.626863525391" Y="4.211563964844" />
                  <Point X="-1.614632568359" Y="4.228245117188" />
                  <Point X="-1.603810791016" Y="4.246989257813" />
                  <Point X="-1.59548046875" Y="4.265921386719" />
                  <Point X="-1.585297607422" Y="4.298217285156" />
                  <Point X="-1.563201171875" Y="4.368297851562" />
                  <Point X="-1.559165649414" Y="4.388584960938" />
                  <Point X="-1.557279174805" Y="4.410146484375" />
                  <Point X="-1.55773034668" Y="4.430826660156" />
                  <Point X="-1.584201904297" Y="4.6318984375" />
                  <Point X="-1.463404296875" Y="4.665766113281" />
                  <Point X="-0.949638427734" Y="4.809808105469" />
                  <Point X="-0.711512268066" Y="4.837677246094" />
                  <Point X="-0.29471081543" Y="4.886458007812" />
                  <Point X="-0.194864471436" Y="4.513825195312" />
                  <Point X="-0.133903305054" Y="4.286315429688" />
                  <Point X="-0.121129844666" Y="4.258124023438" />
                  <Point X="-0.10327155304" Y="4.231397460938" />
                  <Point X="-0.082114532471" Y="4.20880859375" />
                  <Point X="-0.054819446564" Y="4.19421875" />
                  <Point X="-0.024381370544" Y="4.183886230469" />
                  <Point X="0.006155906677" Y="4.178844238281" />
                  <Point X="0.03669316864" Y="4.183886230469" />
                  <Point X="0.067131248474" Y="4.19421875" />
                  <Point X="0.094426467896" Y="4.20880859375" />
                  <Point X="0.115583503723" Y="4.231397460938" />
                  <Point X="0.133441802979" Y="4.258124023438" />
                  <Point X="0.146215255737" Y="4.286315429688" />
                  <Point X="0.278498779297" Y="4.780004394531" />
                  <Point X="0.307419372559" Y="4.8879375" />
                  <Point X="0.395440093994" Y="4.878719238281" />
                  <Point X="0.844041625977" Y="4.831738769531" />
                  <Point X="1.041052612305" Y="4.784174316406" />
                  <Point X="1.481026245117" Y="4.677951171875" />
                  <Point X="1.608246948242" Y="4.631807128906" />
                  <Point X="1.894646484375" Y="4.527927734375" />
                  <Point X="2.018645996094" Y="4.4699375" />
                  <Point X="2.294576660156" Y="4.340893554688" />
                  <Point X="2.414395751953" Y="4.271086914062" />
                  <Point X="2.680977294922" Y="4.115775878906" />
                  <Point X="2.793958740234" Y="4.0354296875" />
                  <Point X="2.943260009766" Y="3.929254882812" />
                  <Point X="2.419390625" Y="3.021885986328" />
                  <Point X="2.147581054688" Y="2.551097900391" />
                  <Point X="2.142075683594" Y="2.539930664062" />
                  <Point X="2.133076904297" Y="2.516057128906" />
                  <Point X="2.126303955078" Y="2.490729980469" />
                  <Point X="2.111607177734" Y="2.435770751953" />
                  <Point X="2.108619384766" Y="2.417934326172" />
                  <Point X="2.107728027344" Y="2.380952880859" />
                  <Point X="2.110368896484" Y="2.359052246094" />
                  <Point X="2.116099365234" Y="2.311528076172" />
                  <Point X="2.121441894531" Y="2.289605224609" />
                  <Point X="2.129708007812" Y="2.267516357422" />
                  <Point X="2.140071044922" Y="2.247470947266" />
                  <Point X="2.153622558594" Y="2.227499511719" />
                  <Point X="2.183028808594" Y="2.184162353516" />
                  <Point X="2.194465332031" Y="2.170327880859" />
                  <Point X="2.221598632812" Y="2.145592773438" />
                  <Point X="2.241569824219" Y="2.132041259766" />
                  <Point X="2.284907226562" Y="2.102635253906" />
                  <Point X="2.304952636719" Y="2.092271972656" />
                  <Point X="2.327040771484" Y="2.084006103516" />
                  <Point X="2.348963867188" Y="2.078663330078" />
                  <Point X="2.370864746094" Y="2.076022460938" />
                  <Point X="2.418388671875" Y="2.070291748047" />
                  <Point X="2.436467529297" Y="2.069845703125" />
                  <Point X="2.473206787109" Y="2.074171142578" />
                  <Point X="2.498533935547" Y="2.080944091797" />
                  <Point X="2.553493164062" Y="2.095640869141" />
                  <Point X="2.565283447266" Y="2.099638183594" />
                  <Point X="2.588533935547" Y="2.110145019531" />
                  <Point X="3.61012890625" Y="2.699963134766" />
                  <Point X="3.967326416016" Y="2.906191162109" />
                  <Point X="4.123272460938" Y="2.689461669922" />
                  <Point X="4.186257324219" Y="2.585378417969" />
                  <Point X="4.262198730469" Y="2.459884277344" />
                  <Point X="3.588646728516" Y="1.943049072266" />
                  <Point X="3.230783935547" Y="1.668451171875" />
                  <Point X="3.221425048828" Y="1.660241943359" />
                  <Point X="3.203973876953" Y="1.641627929688" />
                  <Point X="3.185745849609" Y="1.617848022461" />
                  <Point X="3.146191650391" Y="1.566246459961" />
                  <Point X="3.13660546875" Y="1.550911254883" />
                  <Point X="3.121630126953" Y="1.517086425781" />
                  <Point X="3.114840087891" Y="1.492807006836" />
                  <Point X="3.100106201172" Y="1.440121826172" />
                  <Point X="3.096652587891" Y="1.417823242188" />
                  <Point X="3.095836425781" Y="1.394252929688" />
                  <Point X="3.097739257812" Y="1.371768188477" />
                  <Point X="3.103312988281" Y="1.344754516602" />
                  <Point X="3.115408203125" Y="1.286135498047" />
                  <Point X="3.120679931641" Y="1.268977416992" />
                  <Point X="3.136282226562" Y="1.235740844727" />
                  <Point X="3.151442382812" Y="1.212697753906" />
                  <Point X="3.18433984375" Y="1.162695556641" />
                  <Point X="3.198893554688" Y="1.145450439453" />
                  <Point X="3.216137451172" Y="1.129360595703" />
                  <Point X="3.234347412109" Y="1.116034667969" />
                  <Point X="3.256316650391" Y="1.10366809082" />
                  <Point X="3.303989501953" Y="1.076832275391" />
                  <Point X="3.320521240234" Y="1.069501586914" />
                  <Point X="3.356118164062" Y="1.059438598633" />
                  <Point X="3.385822021484" Y="1.055512817383" />
                  <Point X="3.450278808594" Y="1.046994018555" />
                  <Point X="3.462702148438" Y="1.046175048828" />
                  <Point X="3.488203613281" Y="1.04698449707" />
                  <Point X="4.458650390625" Y="1.174746337891" />
                  <Point X="4.776839355469" Y="1.21663659668" />
                  <Point X="4.778020996094" Y="1.211783203125" />
                  <Point X="4.845936035156" Y="0.932809753418" />
                  <Point X="4.865784667969" Y="0.805325683594" />
                  <Point X="4.890864746094" Y="0.644238525391" />
                  <Point X="4.126698242188" Y="0.439480865479" />
                  <Point X="3.716579833984" Y="0.329589935303" />
                  <Point X="3.704791259766" Y="0.325586608887" />
                  <Point X="3.681545654297" Y="0.315067993164" />
                  <Point X="3.652362304688" Y="0.298199523926" />
                  <Point X="3.589035644531" Y="0.261595581055" />
                  <Point X="3.574311035156" Y="0.251096221924" />
                  <Point X="3.547531005859" Y="0.225576766968" />
                  <Point X="3.530020996094" Y="0.203264938354" />
                  <Point X="3.492025146484" Y="0.154849121094" />
                  <Point X="3.48030078125" Y="0.13556854248" />
                  <Point X="3.470527099609" Y="0.114104927063" />
                  <Point X="3.463680908203" Y="0.092604118347" />
                  <Point X="3.457844238281" Y="0.062127338409" />
                  <Point X="3.445178710938" Y="-0.004006377697" />
                  <Point X="3.443483154297" Y="-0.021875295639" />
                  <Point X="3.445178710938" Y="-0.058553627014" />
                  <Point X="3.451015380859" Y="-0.089030563354" />
                  <Point X="3.463680908203" Y="-0.155164276123" />
                  <Point X="3.470527099609" Y="-0.176664779663" />
                  <Point X="3.48030078125" Y="-0.198128555298" />
                  <Point X="3.492024902344" Y="-0.217409118652" />
                  <Point X="3.509534667969" Y="-0.239720794678" />
                  <Point X="3.547530761719" Y="-0.288136627197" />
                  <Point X="3.559999023438" Y="-0.30123614502" />
                  <Point X="3.589035644531" Y="-0.324155578613" />
                  <Point X="3.618218994141" Y="-0.341024047852" />
                  <Point X="3.681545654297" Y="-0.377628143311" />
                  <Point X="3.692710205078" Y="-0.383139038086" />
                  <Point X="3.716579833984" Y="-0.392149932861" />
                  <Point X="4.606523925781" Y="-0.630609619141" />
                  <Point X="4.891472167969" Y="-0.706961486816" />
                  <Point X="4.855022460938" Y="-0.948726135254" />
                  <Point X="4.829593261719" Y="-1.060161010742" />
                  <Point X="4.801173828125" Y="-1.184698974609" />
                  <Point X="3.900004150391" Y="-1.066057617188" />
                  <Point X="3.424382080078" Y="-1.003441040039" />
                  <Point X="3.408035644531" Y="-1.002710327148" />
                  <Point X="3.374658447266" Y="-1.005508728027" />
                  <Point X="3.317382080078" Y="-1.017958129883" />
                  <Point X="3.193094238281" Y="-1.04497253418" />
                  <Point X="3.163974121094" Y="-1.056596923828" />
                  <Point X="3.136147460938" Y="-1.073489257812" />
                  <Point X="3.112397460938" Y="-1.093960083008" />
                  <Point X="3.07777734375" Y="-1.135597045898" />
                  <Point X="3.002653320312" Y="-1.225948120117" />
                  <Point X="2.987932617188" Y="-1.250330566406" />
                  <Point X="2.976589355469" Y="-1.277715698242" />
                  <Point X="2.969757568359" Y="-1.305365356445" />
                  <Point X="2.964795654297" Y="-1.359287231445" />
                  <Point X="2.954028564453" Y="-1.476295654297" />
                  <Point X="2.956347412109" Y="-1.507564819336" />
                  <Point X="2.964079101562" Y="-1.539185791016" />
                  <Point X="2.976450439453" Y="-1.567996582031" />
                  <Point X="3.008147949219" Y="-1.617300170898" />
                  <Point X="3.076930664062" Y="-1.724287109375" />
                  <Point X="3.086932861328" Y="-1.737238647461" />
                  <Point X="3.110628417969" Y="-1.760909179688" />
                  <Point X="3.936501708984" Y="-2.394624267578" />
                  <Point X="4.213122558594" Y="-2.606883056641" />
                  <Point X="4.124809570313" Y="-2.749786865234" />
                  <Point X="4.072219726563" Y="-2.824509277344" />
                  <Point X="4.028981445312" Y="-2.885945068359" />
                  <Point X="3.224560302734" Y="-2.421512207031" />
                  <Point X="2.800954589844" Y="-2.176943115234" />
                  <Point X="2.786131591797" Y="-2.170012451172" />
                  <Point X="2.754224609375" Y="-2.159825195312" />
                  <Point X="2.686056640625" Y="-2.147514160156" />
                  <Point X="2.538134277344" Y="-2.120799560547" />
                  <Point X="2.506781982422" Y="-2.120395263672" />
                  <Point X="2.474609130859" Y="-2.125353027344" />
                  <Point X="2.444833251953" Y="-2.135176757812" />
                  <Point X="2.388202392578" Y="-2.164981201172" />
                  <Point X="2.265315185547" Y="-2.229655761719" />
                  <Point X="2.242384521484" Y="-2.246548828125" />
                  <Point X="2.221424804688" Y="-2.267508544922" />
                  <Point X="2.204531738281" Y="-2.290439208984" />
                  <Point X="2.174727294922" Y="-2.347070068359" />
                  <Point X="2.110052734375" Y="-2.469957275391" />
                  <Point X="2.100229003906" Y="-2.499733154297" />
                  <Point X="2.095271240234" Y="-2.531906005859" />
                  <Point X="2.095675537109" Y="-2.563258300781" />
                  <Point X="2.107986572266" Y="-2.631426269531" />
                  <Point X="2.134701171875" Y="-2.779348632812" />
                  <Point X="2.138985595703" Y="-2.795141845703" />
                  <Point X="2.151819091797" Y="-2.826078613281" />
                  <Point X="2.682525878906" Y="-3.745289306641" />
                  <Point X="2.861283447266" Y="-4.054906738281" />
                  <Point X="2.781852783203" Y="-4.111642089844" />
                  <Point X="2.723052490234" Y="-4.149702636719" />
                  <Point X="2.701765136719" Y="-4.163481445312" />
                  <Point X="2.081796630859" Y="-3.355522949219" />
                  <Point X="1.758546142578" Y="-2.934255126953" />
                  <Point X="1.747503540039" Y="-2.922178955078" />
                  <Point X="1.721924194336" Y="-2.900557373047" />
                  <Point X="1.654692138672" Y="-2.857333251953" />
                  <Point X="1.50880078125" Y="-2.763538574219" />
                  <Point X="1.479989746094" Y="-2.751167236328" />
                  <Point X="1.448368774414" Y="-2.743435546875" />
                  <Point X="1.417099609375" Y="-2.741116699219" />
                  <Point X="1.343569702148" Y="-2.747883056641" />
                  <Point X="1.184012695312" Y="-2.762565673828" />
                  <Point X="1.15636315918" Y="-2.769397460938" />
                  <Point X="1.128978027344" Y="-2.780740722656" />
                  <Point X="1.104595947266" Y="-2.795461181641" />
                  <Point X="1.047818237305" Y="-2.842669921875" />
                  <Point X="0.924612121582" Y="-2.945111572266" />
                  <Point X="0.904141052246" Y="-2.968861816406" />
                  <Point X="0.887248779297" Y="-2.996688476562" />
                  <Point X="0.875624206543" Y="-3.025808837891" />
                  <Point X="0.858647949219" Y="-3.103913085938" />
                  <Point X="0.821809936523" Y="-3.273396728516" />
                  <Point X="0.819724487305" Y="-3.289627441406" />
                  <Point X="0.819742248535" Y="-3.323120117188" />
                  <Point X="0.970140441895" Y="-4.4655078125" />
                  <Point X="1.022065246582" Y="-4.859915527344" />
                  <Point X="0.975688232422" Y="-4.870081054688" />
                  <Point X="0.929315429688" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058432128906" Y="-4.752635742188" />
                  <Point X="-1.141246459961" Y="-4.731328125" />
                  <Point X="-1.135987426758" Y="-4.691382324219" />
                  <Point X="-1.120775634766" Y="-4.575838378906" />
                  <Point X="-1.120077514648" Y="-4.568099609375" />
                  <Point X="-1.119451782227" Y="-4.541033691406" />
                  <Point X="-1.121759277344" Y="-4.509328125" />
                  <Point X="-1.123334106445" Y="-4.497690429688" />
                  <Point X="-1.142428222656" Y="-4.401698242188" />
                  <Point X="-1.183861450195" Y="-4.193398925781" />
                  <Point X="-1.188125" Y="-4.17846875" />
                  <Point X="-1.19902734375" Y="-4.149502441406" />
                  <Point X="-1.205666015625" Y="-4.135466308594" />
                  <Point X="-1.221738037109" Y="-4.10762890625" />
                  <Point X="-1.23057409668" Y="-4.094861816406" />
                  <Point X="-1.250208496094" Y="-4.070937011719" />
                  <Point X="-1.261006958008" Y="-4.059779296875" />
                  <Point X="-1.334591552734" Y="-3.995247314453" />
                  <Point X="-1.494267700195" Y="-3.855214599609" />
                  <Point X="-1.506738647461" Y="-3.845965576172" />
                  <Point X="-1.533021606445" Y="-3.829621582031" />
                  <Point X="-1.546833618164" Y="-3.822526855469" />
                  <Point X="-1.576530761719" Y="-3.810225830078" />
                  <Point X="-1.591313476562" Y="-3.805476074219" />
                  <Point X="-1.621454833984" Y="-3.798447998047" />
                  <Point X="-1.636813964844" Y="-3.796169677734" />
                  <Point X="-1.734477416992" Y="-3.789768554688" />
                  <Point X="-1.946403198242" Y="-3.775878173828" />
                  <Point X="-1.961928222656" Y="-3.776132324219" />
                  <Point X="-1.992729614258" Y="-3.779166015625" />
                  <Point X="-2.008006103516" Y="-3.781945556641" />
                  <Point X="-2.03905480957" Y="-3.790265136719" />
                  <Point X="-2.053674560547" Y="-3.795496337891" />
                  <Point X="-2.081865478516" Y="-3.808269775391" />
                  <Point X="-2.095436767578" Y="-3.815811767578" />
                  <Point X="-2.176815185547" Y="-3.870187011719" />
                  <Point X="-2.353403320312" Y="-3.988179931641" />
                  <Point X="-2.359681640625" Y="-3.992757080078" />
                  <Point X="-2.380442382812" Y="-4.010131347656" />
                  <Point X="-2.402759277344" Y="-4.032771728516" />
                  <Point X="-2.410470947266" Y="-4.041629638672" />
                  <Point X="-2.503202392578" Y="-4.162479492188" />
                  <Point X="-2.747583496094" Y="-4.011164794922" />
                  <Point X="-2.867066650391" Y="-3.919166992188" />
                  <Point X="-2.980862792969" Y="-3.831547851562" />
                  <Point X="-2.576328857422" Y="-3.130874267578" />
                  <Point X="-2.341488769531" Y="-2.724119628906" />
                  <Point X="-2.334850097656" Y="-2.710083251953" />
                  <Point X="-2.323947998047" Y="-2.681116943359" />
                  <Point X="-2.319684570312" Y="-2.666187011719" />
                  <Point X="-2.313413574219" Y="-2.634660644531" />
                  <Point X="-2.311638916016" Y="-2.619235351562" />
                  <Point X="-2.310626220703" Y="-2.588301269531" />
                  <Point X="-2.314666259766" Y="-2.557615234375" />
                  <Point X="-2.323650878906" Y="-2.527997558594" />
                  <Point X="-2.329357421875" Y="-2.513557128906" />
                  <Point X="-2.343574462891" Y="-2.484728271484" />
                  <Point X="-2.351557128906" Y="-2.47141015625" />
                  <Point X="-2.369584716797" Y="-2.446253173828" />
                  <Point X="-2.379629638672" Y="-2.434414306641" />
                  <Point X="-2.396978271484" Y="-2.417065673828" />
                  <Point X="-2.408817138672" Y="-2.407020751953" />
                  <Point X="-2.433974121094" Y="-2.388993164062" />
                  <Point X="-2.447292236328" Y="-2.381010498047" />
                  <Point X="-2.47612109375" Y="-2.366793457031" />
                  <Point X="-2.490561523438" Y="-2.361086914062" />
                  <Point X="-2.520179199219" Y="-2.352102294922" />
                  <Point X="-2.550865234375" Y="-2.348062255859" />
                  <Point X="-2.581799316406" Y="-2.349074951172" />
                  <Point X="-2.597224609375" Y="-2.350849609375" />
                  <Point X="-2.628750976562" Y="-2.357120605469" />
                  <Point X="-2.643680908203" Y="-2.361384033203" />
                  <Point X="-2.672647216797" Y="-2.372286132812" />
                  <Point X="-2.68668359375" Y="-2.378924804688" />
                  <Point X="-3.569328125" Y="-2.888520019531" />
                  <Point X="-3.793089355469" Y="-3.017708496094" />
                  <Point X="-4.004016113281" Y="-2.740593994141" />
                  <Point X="-4.089672363281" Y="-2.596961181641" />
                  <Point X="-4.181265136719" Y="-2.443373779297" />
                  <Point X="-3.459016113281" Y="-1.889171875" />
                  <Point X="-3.048122314453" Y="-1.573882080078" />
                  <Point X="-3.039157714844" Y="-1.566065063477" />
                  <Point X="-3.016265869141" Y="-1.543428833008" />
                  <Point X="-3.002283203125" Y="-1.525873901367" />
                  <Point X="-2.982774414062" Y="-1.494353759766" />
                  <Point X="-2.974572509766" Y="-1.47763293457" />
                  <Point X="-2.965428466797" Y="-1.453181274414" />
                  <Point X="-2.9594609375" Y="-1.434268554688" />
                  <Point X="-2.954186767578" Y="-1.413905273438" />
                  <Point X="-2.951953125" Y="-1.402396606445" />
                  <Point X="-2.947838623047" Y="-1.370913574219" />
                  <Point X="-2.94708203125" Y="-1.35569934082" />
                  <Point X="-2.94778125" Y="-1.332831176758" />
                  <Point X="-2.951229003906" Y="-1.310213745117" />
                  <Point X="-2.957375244141" Y="-1.28817590332" />
                  <Point X="-2.961114257812" Y="-1.277342041016" />
                  <Point X="-2.973119140625" Y="-1.248359375" />
                  <Point X="-2.978855957031" Y="-1.236800048828" />
                  <Point X="-2.991842285156" Y="-1.21456652832" />
                  <Point X="-3.007721191406" Y="-1.194298461914" />
                  <Point X="-3.026200927734" Y="-1.176368652344" />
                  <Point X="-3.036051269531" Y="-1.168032714844" />
                  <Point X="-3.056907714844" Y="-1.152686279297" />
                  <Point X="-3.073141601562" Y="-1.141976928711" />
                  <Point X="-3.091270019531" Y="-1.131307617188" />
                  <Point X="-3.105434814453" Y="-1.124480834961" />
                  <Point X="-3.134697265625" Y="-1.113257080078" />
                  <Point X="-3.149795410156" Y="-1.108860107422" />
                  <Point X="-3.181683105469" Y="-1.102378540039" />
                  <Point X="-3.197298828125" Y="-1.100532470703" />
                  <Point X="-3.228622070312" Y="-1.09944140625" />
                  <Point X="-3.244329101562" Y="-1.100196533203" />
                  <Point X="-4.358584472656" Y="-1.246890991211" />
                  <Point X="-4.660920410156" Y="-1.286694335938" />
                  <Point X="-4.74076171875" Y="-0.97411907959" />
                  <Point X="-4.763424316406" Y="-0.815667236328" />
                  <Point X="-4.786452636719" Y="-0.654654296875" />
                  <Point X="-3.976357910156" Y="-0.437590026855" />
                  <Point X="-3.508288085938" Y="-0.312171325684" />
                  <Point X="-3.497546386719" Y="-0.308594573975" />
                  <Point X="-3.476564941406" Y="-0.300188903809" />
                  <Point X="-3.466325439453" Y="-0.295360015869" />
                  <Point X="-3.442002441406" Y="-0.282063323975" />
                  <Point X="-3.424807373047" Y="-0.271437805176" />
                  <Point X="-3.405809326172" Y="-0.25825213623" />
                  <Point X="-3.396478271484" Y="-0.250869003296" />
                  <Point X="-3.372522705078" Y="-0.229342163086" />
                  <Point X="-3.357696777344" Y="-0.212442993164" />
                  <Point X="-3.336679199219" Y="-0.181823547363" />
                  <Point X="-3.327681396484" Y="-0.165476959229" />
                  <Point X="-3.317337646484" Y="-0.14133644104" />
                  <Point X="-3.310519775391" Y="-0.122825019836" />
                  <Point X="-3.304187011719" Y="-0.102421028137" />
                  <Point X="-3.301576660156" Y="-0.091938796997" />
                  <Point X="-3.296133544922" Y="-0.063198253632" />
                  <Point X="-3.294516601562" Y="-0.042689777374" />
                  <Point X="-3.295531738281" Y="-0.008637744904" />
                  <Point X="-3.297578125" Y="0.008343015671" />
                  <Point X="-3.302743408203" Y="0.032566913605" />
                  <Point X="-3.307105224609" Y="0.049263805389" />
                  <Point X="-3.313437988281" Y="0.069667800903" />
                  <Point X="-3.317669677734" Y="0.080788658142" />
                  <Point X="-3.330986328125" Y="0.110112693787" />
                  <Point X="-3.342728515625" Y="0.129453292847" />
                  <Point X="-3.365823974609" Y="0.158905593872" />
                  <Point X="-3.378836425781" Y="0.172483032227" />
                  <Point X="-3.399409667969" Y="0.190077163696" />
                  <Point X="-3.414563720703" Y="0.2017681427" />
                  <Point X="-3.433561767578" Y="0.214953964233" />
                  <Point X="-3.440485107422" Y="0.219329269409" />
                  <Point X="-3.465616210938" Y="0.232834640503" />
                  <Point X="-3.496566894531" Y="0.245635818481" />
                  <Point X="-3.508288085938" Y="0.249611343384" />
                  <Point X="-4.523984375" Y="0.521766174316" />
                  <Point X="-4.785445800781" Y="0.591824584961" />
                  <Point X="-4.731331542969" Y="0.957524169922" />
                  <Point X="-4.6857109375" Y="1.125880126953" />
                  <Point X="-4.6335859375" Y="1.318237060547" />
                  <Point X="-4.113590332031" Y="1.249778198242" />
                  <Point X="-3.778066162109" Y="1.20560559082" />
                  <Point X="-3.76773828125" Y="1.204815429688" />
                  <Point X="-3.747058105469" Y="1.204364257812" />
                  <Point X="-3.736705810547" Y="1.20470324707" />
                  <Point X="-3.715144287109" Y="1.20658972168" />
                  <Point X="-3.704890136719" Y="1.208053710938" />
                  <Point X="-3.684603027344" Y="1.212089233398" />
                  <Point X="-3.674570556641" Y="1.214660522461" />
                  <Point X="-3.655193115234" Y="1.220770141602" />
                  <Point X="-3.61314453125" Y="1.234028076172" />
                  <Point X="-3.603450439453" Y="1.237676635742" />
                  <Point X="-3.584518066406" Y="1.246007080078" />
                  <Point X="-3.575279541016" Y="1.250688964844" />
                  <Point X="-3.55653515625" Y="1.261510864258" />
                  <Point X="-3.547860107422" Y="1.267171386719" />
                  <Point X="-3.531178710938" Y="1.279402832031" />
                  <Point X="-3.515927978516" Y="1.293377441406" />
                  <Point X="-3.502289550781" Y="1.308929443359" />
                  <Point X="-3.495895263672" Y="1.317077392578" />
                  <Point X="-3.483480712891" Y="1.334807373047" />
                  <Point X="-3.478011474609" Y="1.343602416992" />
                  <Point X="-3.468062744141" Y="1.361736328125" />
                  <Point X="-3.463583251953" Y="1.371075073242" />
                  <Point X="-3.455807861328" Y="1.389845947266" />
                  <Point X="-3.438935791016" Y="1.430579223633" />
                  <Point X="-3.435499511719" Y="1.440350585938" />
                  <Point X="-3.4297109375" Y="1.460209716797" />
                  <Point X="-3.427358642578" Y="1.470297851562" />
                  <Point X="-3.423600341797" Y="1.491612792969" />
                  <Point X="-3.422360595703" Y="1.501896850586" />
                  <Point X="-3.421008056641" Y="1.522537109375" />
                  <Point X="-3.42191015625" Y="1.543200439453" />
                  <Point X="-3.425056640625" Y="1.563644165039" />
                  <Point X="-3.427188232422" Y="1.573780639648" />
                  <Point X="-3.432790039062" Y="1.594686889648" />
                  <Point X="-3.436012207031" Y="1.604530517578" />
                  <Point X="-3.443509277344" Y="1.623809326172" />
                  <Point X="-3.447784179688" Y="1.633244384766" />
                  <Point X="-3.457166015625" Y="1.651266479492" />
                  <Point X="-3.477523925781" Y="1.690373901367" />
                  <Point X="-3.482799804688" Y="1.699286376953" />
                  <Point X="-3.494291259766" Y="1.716484741211" />
                  <Point X="-3.500506835938" Y="1.724770629883" />
                  <Point X="-3.514419433594" Y="1.741351196289" />
                  <Point X="-3.521500488281" Y="1.748911254883" />
                  <Point X="-3.536442626953" Y="1.763215087891" />
                  <Point X="-3.544303710938" Y="1.769958618164" />
                  <Point X="-4.126909179688" Y="2.217008300781" />
                  <Point X="-4.227614257812" Y="2.294282226563" />
                  <Point X="-4.226973144531" Y="2.295380615234" />
                  <Point X="-4.002293212891" Y="2.680312011719" />
                  <Point X="-3.881453369141" Y="2.835634521484" />
                  <Point X="-3.726338623047" Y="3.035012695312" />
                  <Point X="-3.460226318359" Y="2.881372802734" />
                  <Point X="-3.254156982422" Y="2.7623984375" />
                  <Point X="-3.244918457031" Y="2.757716552734" />
                  <Point X="-3.225986083984" Y="2.749385986328" />
                  <Point X="-3.216292236328" Y="2.745737304688" />
                  <Point X="-3.195650146484" Y="2.739228759766" />
                  <Point X="-3.185615722656" Y="2.736656982422" />
                  <Point X="-3.165327880859" Y="2.732621582031" />
                  <Point X="-3.155074462891" Y="2.731157958984" />
                  <Point X="-3.128086914062" Y="2.728796875" />
                  <Point X="-3.069525146484" Y="2.723673339844" />
                  <Point X="-3.059173339844" Y="2.723334472656" />
                  <Point X="-3.038493164062" Y="2.723785644531" />
                  <Point X="-3.028164794922" Y="2.724575683594" />
                  <Point X="-3.006706054688" Y="2.727400878906" />
                  <Point X="-2.996525878906" Y="2.729310791016" />
                  <Point X="-2.976434082031" Y="2.734227294922" />
                  <Point X="-2.956996826172" Y="2.741302001953" />
                  <Point X="-2.9384453125" Y="2.750450683594" />
                  <Point X="-2.929419433594" Y="2.75553125" />
                  <Point X="-2.911165039062" Y="2.767160644531" />
                  <Point X="-2.90274609375" Y="2.773193603516" />
                  <Point X="-2.886614990234" Y="2.786140136719" />
                  <Point X="-2.878902832031" Y="2.793053710938" />
                  <Point X="-2.859746826172" Y="2.812209472656" />
                  <Point X="-2.818179199219" Y="2.853777099609" />
                  <Point X="-2.811265625" Y="2.861489257812" />
                  <Point X="-2.798318603516" Y="2.877620605469" />
                  <Point X="-2.79228515625" Y="2.886039794922" />
                  <Point X="-2.780655761719" Y="2.904293945312" />
                  <Point X="-2.775574707031" Y="2.913320800781" />
                  <Point X="-2.766426025391" Y="2.931872558594" />
                  <Point X="-2.7593515625" Y="2.95130859375" />
                  <Point X="-2.754434814453" Y="2.971400634766" />
                  <Point X="-2.752524902344" Y="2.981581542969" />
                  <Point X="-2.749699707031" Y="3.003040527344" />
                  <Point X="-2.748909667969" Y="3.013368896484" />
                  <Point X="-2.748458496094" Y="3.034049072266" />
                  <Point X="-2.748797363281" Y="3.044400878906" />
                  <Point X="-2.751158447266" Y="3.071388427734" />
                  <Point X="-2.756281982422" Y="3.129950439453" />
                  <Point X="-2.757745605469" Y="3.140203857422" />
                  <Point X="-2.761781005859" Y="3.160491699219" />
                  <Point X="-2.764352783203" Y="3.170526123047" />
                  <Point X="-2.770861328125" Y="3.191168212891" />
                  <Point X="-2.774510009766" Y="3.200862060547" />
                  <Point X="-2.782840576172" Y="3.219794433594" />
                  <Point X="-2.787522460938" Y="3.229032958984" />
                  <Point X="-3.045692871094" Y="3.676197021484" />
                  <Point X="-3.059387451172" Y="3.699916503906" />
                  <Point X="-3.039690429688" Y="3.715018066406" />
                  <Point X="-2.648374023438" Y="4.015036376953" />
                  <Point X="-2.458060791016" Y="4.120770507812" />
                  <Point X="-2.192524902344" Y="4.268296386719" />
                  <Point X="-2.181648193359" Y="4.254121582031" />
                  <Point X="-2.118563964844" Y="4.171908691406" />
                  <Point X="-2.111821289062" Y="4.164048828125" />
                  <Point X="-2.097518554688" Y="4.149107421875" />
                  <Point X="-2.089958740234" Y="4.142026367188" />
                  <Point X="-2.07337890625" Y="4.128113769531" />
                  <Point X="-2.065092773438" Y="4.121897949219" />
                  <Point X="-2.047892944336" Y="4.110405273438" />
                  <Point X="-2.038979370117" Y="4.105128417969" />
                  <Point X="-2.008942382813" Y="4.0894921875" />
                  <Point X="-1.943763061523" Y="4.055561523438" />
                  <Point X="-1.934326904297" Y="4.051286132812" />
                  <Point X="-1.915047119141" Y="4.0437890625" />
                  <Point X="-1.905203613281" Y="4.040567138672" />
                  <Point X="-1.88429699707" Y="4.034965576172" />
                  <Point X="-1.874162475586" Y="4.032834716797" />
                  <Point X="-1.853719482422" Y="4.029688232422" />
                  <Point X="-1.833055419922" Y="4.028785888672" />
                  <Point X="-1.812416137695" Y="4.030138427734" />
                  <Point X="-1.802132568359" Y="4.031378173828" />
                  <Point X="-1.780817138672" Y="4.035136474609" />
                  <Point X="-1.770729003906" Y="4.037488769531" />
                  <Point X="-1.750869506836" Y="4.04327734375" />
                  <Point X="-1.741098388672" Y="4.046713378906" />
                  <Point X="-1.709812866211" Y="4.059672363281" />
                  <Point X="-1.641924438477" Y="4.08779296875" />
                  <Point X="-1.632584350586" Y="4.092273193359" />
                  <Point X="-1.61444909668" Y="4.10222265625" />
                  <Point X="-1.605653808594" Y="4.107692382812" />
                  <Point X="-1.587924316406" Y="4.120106933594" />
                  <Point X="-1.579776733398" Y="4.126500976563" />
                  <Point X="-1.564225463867" Y="4.140139160156" />
                  <Point X="-1.550250976562" Y="4.155390136719" />
                  <Point X="-1.538020019531" Y="4.172071289062" />
                  <Point X="-1.532359863281" Y="4.180745605469" />
                  <Point X="-1.521538085938" Y="4.199489746094" />
                  <Point X="-1.516856201172" Y="4.208728515625" />
                  <Point X="-1.508525878906" Y="4.227660644531" />
                  <Point X="-1.504877319336" Y="4.237354492188" />
                  <Point X="-1.494694458008" Y="4.269650390625" />
                  <Point X="-1.472598022461" Y="4.339730957031" />
                  <Point X="-1.470026733398" Y="4.349763671875" />
                  <Point X="-1.465991210938" Y="4.37005078125" />
                  <Point X="-1.46452722168" Y="4.3803046875" />
                  <Point X="-1.46264074707" Y="4.401866210938" />
                  <Point X="-1.462301757812" Y="4.41221875" />
                  <Point X="-1.462752929688" Y="4.432898925781" />
                  <Point X="-1.46354309082" Y="4.4432265625" />
                  <Point X="-1.479266235352" Y="4.56265625" />
                  <Point X="-1.437758300781" Y="4.574293457031" />
                  <Point X="-0.931181640625" Y="4.716319824219" />
                  <Point X="-0.700469238281" Y="4.743321289062" />
                  <Point X="-0.365222045898" Y="4.782557128906" />
                  <Point X="-0.286627441406" Y="4.489237304688" />
                  <Point X="-0.22566633606" Y="4.261727539062" />
                  <Point X="-0.220435256958" Y="4.247107910156" />
                  <Point X="-0.207661849976" Y="4.218916503906" />
                  <Point X="-0.200119247437" Y="4.205344726562" />
                  <Point X="-0.182260864258" Y="4.178618164062" />
                  <Point X="-0.17260824585" Y="4.166455566406" />
                  <Point X="-0.151451187134" Y="4.143866699219" />
                  <Point X="-0.126897941589" Y="4.125026367188" />
                  <Point X="-0.099602897644" Y="4.110436523438" />
                  <Point X="-0.085356651306" Y="4.104260742188" />
                  <Point X="-0.054918682098" Y="4.093927978516" />
                  <Point X="-0.039857234955" Y="4.090155273438" />
                  <Point X="-0.009319986343" Y="4.08511328125" />
                  <Point X="0.021631772995" Y="4.08511328125" />
                  <Point X="0.052169025421" Y="4.090155273438" />
                  <Point X="0.06723046875" Y="4.093927978516" />
                  <Point X="0.097668441772" Y="4.104260742188" />
                  <Point X="0.111914535522" Y="4.110436523438" />
                  <Point X="0.139209732056" Y="4.125026367188" />
                  <Point X="0.163763122559" Y="4.143866699219" />
                  <Point X="0.184920181274" Y="4.166455566406" />
                  <Point X="0.194572952271" Y="4.178617675781" />
                  <Point X="0.212431182861" Y="4.205344238281" />
                  <Point X="0.2199737854" Y="4.218916503906" />
                  <Point X="0.232747192383" Y="4.247107910156" />
                  <Point X="0.237978271484" Y="4.261727539062" />
                  <Point X="0.37026171875" Y="4.755416503906" />
                  <Point X="0.3781902771" Y="4.785006347656" />
                  <Point X="0.385545043945" Y="4.784235839844" />
                  <Point X="0.827877197266" Y="4.737912109375" />
                  <Point X="1.018757385254" Y="4.691827636719" />
                  <Point X="1.453595336914" Y="4.586844238281" />
                  <Point X="1.575854370117" Y="4.5425" />
                  <Point X="1.858254882812" Y="4.440071289062" />
                  <Point X="1.978401367188" Y="4.3838828125" />
                  <Point X="2.250457519531" Y="4.256650878906" />
                  <Point X="2.366572998047" Y="4.189001953125" />
                  <Point X="2.629432373047" Y="4.035859130859" />
                  <Point X="2.73890234375" Y="3.958010253906" />
                  <Point X="2.817779785156" Y="3.901916992188" />
                  <Point X="2.337118164062" Y="3.069385986328" />
                  <Point X="2.06530859375" Y="2.598597900391" />
                  <Point X="2.062373046875" Y="2.593104980469" />
                  <Point X="2.053181152344" Y="2.573438232422" />
                  <Point X="2.044182373047" Y="2.549564697266" />
                  <Point X="2.041301757812" Y="2.540599609375" />
                  <Point X="2.034528808594" Y="2.515272460938" />
                  <Point X="2.01983203125" Y="2.460313232422" />
                  <Point X="2.017912597656" Y="2.451465576172" />
                  <Point X="2.013646972656" Y="2.420223388672" />
                  <Point X="2.012755615234" Y="2.383241943359" />
                  <Point X="2.013411254883" Y="2.369579833984" />
                  <Point X="2.016052124023" Y="2.347679199219" />
                  <Point X="2.021782592773" Y="2.300155029297" />
                  <Point X="2.023800537109" Y="2.28903515625" />
                  <Point X="2.029143066406" Y="2.267112304688" />
                  <Point X="2.032468017578" Y="2.256309326172" />
                  <Point X="2.040734130859" Y="2.234220458984" />
                  <Point X="2.045318237305" Y="2.223888671875" />
                  <Point X="2.055681396484" Y="2.203843261719" />
                  <Point X="2.061459960938" Y="2.194129638672" />
                  <Point X="2.075011474609" Y="2.174158203125" />
                  <Point X="2.104417724609" Y="2.130821044922" />
                  <Point X="2.109808349609" Y="2.123633300781" />
                  <Point X="2.130464599609" Y="2.100121826172" />
                  <Point X="2.157597900391" Y="2.07538671875" />
                  <Point X="2.168257080078" Y="2.066981933594" />
                  <Point X="2.188228271484" Y="2.053430419922" />
                  <Point X="2.231565673828" Y="2.024024414062" />
                  <Point X="2.241278808594" Y="2.01824597168" />
                  <Point X="2.26132421875" Y="2.00788269043" />
                  <Point X="2.271656494141" Y="2.003297973633" />
                  <Point X="2.293744628906" Y="1.995032104492" />
                  <Point X="2.304547119141" Y="1.991707397461" />
                  <Point X="2.326470214844" Y="1.986364746094" />
                  <Point X="2.337590820312" Y="1.984346557617" />
                  <Point X="2.359491699219" Y="1.981705688477" />
                  <Point X="2.407015625" Y="1.975974975586" />
                  <Point X="2.416045410156" Y="1.975320800781" />
                  <Point X="2.447575439453" Y="1.975497314453" />
                  <Point X="2.484314697266" Y="1.979822875977" />
                  <Point X="2.497749267578" Y="1.982395996094" />
                  <Point X="2.523076416016" Y="1.989168945312" />
                  <Point X="2.578035644531" Y="2.003865722656" />
                  <Point X="2.58399609375" Y="2.005671020508" />
                  <Point X="2.604404541016" Y="2.013067260742" />
                  <Point X="2.627655029297" Y="2.02357409668" />
                  <Point X="2.636033935547" Y="2.027872558594" />
                  <Point X="3.65762890625" Y="2.617690673828" />
                  <Point X="3.940405029297" Y="2.780951416016" />
                  <Point X="4.043952636719" Y="2.637043945312" />
                  <Point X="4.10498046875" Y="2.536194580078" />
                  <Point X="4.136884765625" Y="2.483472412109" />
                  <Point X="3.530814453125" Y="2.018417724609" />
                  <Point X="3.172951660156" Y="1.743819824219" />
                  <Point X="3.168138671875" Y="1.739869506836" />
                  <Point X="3.152120117188" Y="1.725217407227" />
                  <Point X="3.134668945312" Y="1.706603393555" />
                  <Point X="3.128576416016" Y="1.699422485352" />
                  <Point X="3.110348388672" Y="1.675642578125" />
                  <Point X="3.070794189453" Y="1.624041015625" />
                  <Point X="3.065635742188" Y="1.616602783203" />
                  <Point X="3.04973828125" Y="1.589370117188" />
                  <Point X="3.034762939453" Y="1.555545288086" />
                  <Point X="3.030140625" Y="1.542672607422" />
                  <Point X="3.023350585938" Y="1.518393188477" />
                  <Point X="3.008616699219" Y="1.465708007812" />
                  <Point X="3.006225585938" Y="1.454662231445" />
                  <Point X="3.002771972656" Y="1.432363525391" />
                  <Point X="3.001709472656" Y="1.421110839844" />
                  <Point X="3.000893310547" Y="1.397540649414" />
                  <Point X="3.001174804688" Y="1.386241943359" />
                  <Point X="3.003077636719" Y="1.363757324219" />
                  <Point X="3.004698974609" Y="1.352571166992" />
                  <Point X="3.010272705078" Y="1.325557495117" />
                  <Point X="3.022367919922" Y="1.266938476562" />
                  <Point X="3.02459765625" Y="1.258234619141" />
                  <Point X="3.034683837891" Y="1.228608276367" />
                  <Point X="3.050286132812" Y="1.195371582031" />
                  <Point X="3.05691796875" Y="1.183526733398" />
                  <Point X="3.072078125" Y="1.160483642578" />
                  <Point X="3.104975585938" Y="1.110481445312" />
                  <Point X="3.111738769531" Y="1.101425048828" />
                  <Point X="3.126292480469" Y="1.084179931641" />
                  <Point X="3.134083007812" Y="1.075991210938" />
                  <Point X="3.151326904297" Y="1.059901367188" />
                  <Point X="3.160034667969" Y="1.052695800781" />
                  <Point X="3.178244628906" Y="1.039369750977" />
                  <Point X="3.187747070312" Y="1.033249267578" />
                  <Point X="3.209716308594" Y="1.020882751465" />
                  <Point X="3.257389160156" Y="0.994046813965" />
                  <Point X="3.265479736328" Y="0.989987609863" />
                  <Point X="3.294678222656" Y="0.978084289551" />
                  <Point X="3.330275146484" Y="0.96802130127" />
                  <Point X="3.343670898438" Y="0.965257507324" />
                  <Point X="3.373374755859" Y="0.961331787109" />
                  <Point X="3.437831542969" Y="0.952812988281" />
                  <Point X="3.444029785156" Y="0.952199829102" />
                  <Point X="3.465716064453" Y="0.951222900391" />
                  <Point X="3.491217529297" Y="0.952032287598" />
                  <Point X="3.500603515625" Y="0.952797241211" />
                  <Point X="4.471050292969" Y="1.080558959961" />
                  <Point X="4.704703613281" Y="1.111320068359" />
                  <Point X="4.75268359375" Y="0.91423425293" />
                  <Point X="4.771915527344" Y="0.790710754395" />
                  <Point X="4.78387109375" Y="0.713920959473" />
                  <Point X="4.102110351562" Y="0.531243835449" />
                  <Point X="3.691991943359" Y="0.421352966309" />
                  <Point X="3.686031738281" Y="0.419544525146" />
                  <Point X="3.665626708984" Y="0.412138061523" />
                  <Point X="3.642381103516" Y="0.401619415283" />
                  <Point X="3.634004638672" Y="0.397316650391" />
                  <Point X="3.604821289062" Y="0.380448242188" />
                  <Point X="3.541494628906" Y="0.343844207764" />
                  <Point X="3.533881347656" Y="0.33894543457" />
                  <Point X="3.508773925781" Y="0.319870574951" />
                  <Point X="3.481993896484" Y="0.294351135254" />
                  <Point X="3.472797119141" Y="0.284226959229" />
                  <Point X="3.455287109375" Y="0.261915100098" />
                  <Point X="3.417291259766" Y="0.213499237061" />
                  <Point X="3.410854492188" Y="0.204208374023" />
                  <Point X="3.399130126953" Y="0.18492767334" />
                  <Point X="3.393842529297" Y="0.174938293457" />
                  <Point X="3.384068847656" Y="0.153474609375" />
                  <Point X="3.380005371094" Y="0.142928482056" />
                  <Point X="3.373159179688" Y="0.121427658081" />
                  <Point X="3.370376464844" Y="0.110472961426" />
                  <Point X="3.364539794922" Y="0.079996200562" />
                  <Point X="3.351874267578" Y="0.01386257267" />
                  <Point X="3.350603515625" Y="0.00496778965" />
                  <Point X="3.348584472656" Y="-0.026262191772" />
                  <Point X="3.350280029297" Y="-0.062940540314" />
                  <Point X="3.351874267578" Y="-0.076422424316" />
                  <Point X="3.3577109375" Y="-0.106899330139" />
                  <Point X="3.370376464844" Y="-0.173033096313" />
                  <Point X="3.373159179688" Y="-0.183988250732" />
                  <Point X="3.380005371094" Y="-0.20548878479" />
                  <Point X="3.384068847656" Y="-0.216034164429" />
                  <Point X="3.393842529297" Y="-0.23749798584" />
                  <Point X="3.399129638672" Y="-0.247487075806" />
                  <Point X="3.410853759766" Y="-0.26676763916" />
                  <Point X="3.417290771484" Y="-0.276058929443" />
                  <Point X="3.434800537109" Y="-0.298370666504" />
                  <Point X="3.472796630859" Y="-0.346786499023" />
                  <Point X="3.478718017578" Y="-0.353633270264" />
                  <Point X="3.501139404297" Y="-0.375805267334" />
                  <Point X="3.530176025391" Y="-0.398724700928" />
                  <Point X="3.541494628906" Y="-0.406404205322" />
                  <Point X="3.570677978516" Y="-0.423272766113" />
                  <Point X="3.634004638672" Y="-0.459876800537" />
                  <Point X="3.639496582031" Y="-0.46281552124" />
                  <Point X="3.659158447266" Y="-0.472016906738" />
                  <Point X="3.683028076172" Y="-0.481027770996" />
                  <Point X="3.691991943359" Y="-0.483912963867" />
                  <Point X="4.581936035156" Y="-0.722372619629" />
                  <Point X="4.784876953125" Y="-0.776750488281" />
                  <Point X="4.76161328125" Y="-0.931052612305" />
                  <Point X="4.736974121094" Y="-1.039025512695" />
                  <Point X="4.727801757812" Y="-1.079219604492" />
                  <Point X="3.912404052734" Y="-0.971870361328" />
                  <Point X="3.436781982422" Y="-0.909253723145" />
                  <Point X="3.428624511719" Y="-0.908535888672" />
                  <Point X="3.400098632812" Y="-0.908042480469" />
                  <Point X="3.366721435547" Y="-0.910840881348" />
                  <Point X="3.354480712891" Y="-0.912676391602" />
                  <Point X="3.297204345703" Y="-0.925125793457" />
                  <Point X="3.172916503906" Y="-0.952140075684" />
                  <Point X="3.157874023438" Y="-0.956742614746" />
                  <Point X="3.12875390625" Y="-0.968367004395" />
                  <Point X="3.114676269531" Y="-0.975388977051" />
                  <Point X="3.086849609375" Y="-0.992281311035" />
                  <Point X="3.074124023438" Y="-1.001530273438" />
                  <Point X="3.050374023438" Y="-1.022001159668" />
                  <Point X="3.039349853516" Y="-1.03322265625" />
                  <Point X="3.004729736328" Y="-1.074859619141" />
                  <Point X="2.929605712891" Y="-1.16521081543" />
                  <Point X="2.921326171875" Y="-1.17684753418" />
                  <Point X="2.90660546875" Y="-1.201229858398" />
                  <Point X="2.9001640625" Y="-1.213975708008" />
                  <Point X="2.888820800781" Y="-1.241360839844" />
                  <Point X="2.884362792969" Y="-1.254927978516" />
                  <Point X="2.877531005859" Y="-1.282577636719" />
                  <Point X="2.875157226562" Y="-1.29666015625" />
                  <Point X="2.8701953125" Y="-1.35058203125" />
                  <Point X="2.859428222656" Y="-1.467590454102" />
                  <Point X="2.859288818359" Y="-1.483321289062" />
                  <Point X="2.861607666016" Y="-1.514590454102" />
                  <Point X="2.864065917969" Y="-1.53012878418" />
                  <Point X="2.871797607422" Y="-1.561749633789" />
                  <Point X="2.876786621094" Y="-1.576669189453" />
                  <Point X="2.889157958984" Y="-1.605479980469" />
                  <Point X="2.896540283203" Y="-1.619371337891" />
                  <Point X="2.928237792969" Y="-1.668674804688" />
                  <Point X="2.997020507812" Y="-1.775661743164" />
                  <Point X="3.0017421875" Y="-1.782353515625" />
                  <Point X="3.019793212891" Y="-1.80444909668" />
                  <Point X="3.043488769531" Y="-1.828119750977" />
                  <Point X="3.052796142578" Y="-1.836277587891" />
                  <Point X="3.878669433594" Y="-2.469992919922" />
                  <Point X="4.087170654297" Y="-2.629981933594" />
                  <Point X="4.045487548828" Y="-2.697431640625" />
                  <Point X="4.001274414063" Y="-2.760251953125" />
                  <Point X="3.272060302734" Y="-2.339239746094" />
                  <Point X="2.848454589844" Y="-2.094670654297" />
                  <Point X="2.841191894531" Y="-2.090885253906" />
                  <Point X="2.815026123047" Y="-2.079513183594" />
                  <Point X="2.783119140625" Y="-2.069325927734" />
                  <Point X="2.771108398438" Y="-2.066337646484" />
                  <Point X="2.702940429688" Y="-2.054026611328" />
                  <Point X="2.555018066406" Y="-2.027311889648" />
                  <Point X="2.539359130859" Y="-2.025807495117" />
                  <Point X="2.508006835938" Y="-2.025403198242" />
                  <Point X="2.492313476562" Y="-2.026503540039" />
                  <Point X="2.460140625" Y="-2.031461303711" />
                  <Point X="2.444844482422" Y="-2.035136230469" />
                  <Point X="2.415068603516" Y="-2.044959960938" />
                  <Point X="2.400588867188" Y="-2.051108642578" />
                  <Point X="2.343958007812" Y="-2.080913085938" />
                  <Point X="2.221070800781" Y="-2.145587646484" />
                  <Point X="2.208968261719" Y="-2.153170410156" />
                  <Point X="2.186037597656" Y="-2.170063476562" />
                  <Point X="2.175209472656" Y="-2.179373779297" />
                  <Point X="2.154249755859" Y="-2.200333496094" />
                  <Point X="2.144939453125" Y="-2.211161621094" />
                  <Point X="2.128046386719" Y="-2.234092285156" />
                  <Point X="2.120463623047" Y="-2.246194824219" />
                  <Point X="2.090659179688" Y="-2.302825683594" />
                  <Point X="2.025984741211" Y="-2.425712890625" />
                  <Point X="2.0198359375" Y="-2.440192626953" />
                  <Point X="2.010012207031" Y="-2.469968505859" />
                  <Point X="2.006337280273" Y="-2.485264648438" />
                  <Point X="2.001379516602" Y="-2.5174375" />
                  <Point X="2.000279174805" Y="-2.533130859375" />
                  <Point X="2.00068347168" Y="-2.564483154297" />
                  <Point X="2.002187866211" Y="-2.580142089844" />
                  <Point X="2.014498901367" Y="-2.648310058594" />
                  <Point X="2.041213500977" Y="-2.796232421875" />
                  <Point X="2.043015014648" Y="-2.804221435547" />
                  <Point X="2.051236083984" Y="-2.83154296875" />
                  <Point X="2.064069580078" Y="-2.862479736328" />
                  <Point X="2.069546630859" Y="-2.873578613281" />
                  <Point X="2.600253417969" Y="-3.792789306641" />
                  <Point X="2.735893310547" Y="-4.027724609375" />
                  <Point X="2.723754394531" Y="-4.036083496094" />
                  <Point X="2.157165283203" Y="-3.297690673828" />
                  <Point X="1.833914550781" Y="-2.876422851562" />
                  <Point X="1.828654418945" Y="-2.870147216797" />
                  <Point X="1.808830932617" Y="-2.849625732422" />
                  <Point X="1.783251586914" Y="-2.828004150391" />
                  <Point X="1.773299072266" Y="-2.820647216797" />
                  <Point X="1.706067016602" Y="-2.777423095703" />
                  <Point X="1.56017565918" Y="-2.683628417969" />
                  <Point X="1.546283935547" Y="-2.676245849609" />
                  <Point X="1.517473022461" Y="-2.663874511719" />
                  <Point X="1.502553588867" Y="-2.658885742188" />
                  <Point X="1.470932617188" Y="-2.651154052734" />
                  <Point X="1.45539440918" Y="-2.648695800781" />
                  <Point X="1.424125244141" Y="-2.646376953125" />
                  <Point X="1.408394287109" Y="-2.646516357422" />
                  <Point X="1.334864379883" Y="-2.653282714844" />
                  <Point X="1.175307373047" Y="-2.667965332031" />
                  <Point X="1.161224975586" Y="-2.670339111328" />
                  <Point X="1.133575439453" Y="-2.677170898438" />
                  <Point X="1.120008422852" Y="-2.68162890625" />
                  <Point X="1.092623168945" Y="-2.692972167969" />
                  <Point X="1.079877563477" Y="-2.699413330078" />
                  <Point X="1.055495361328" Y="-2.714133789062" />
                  <Point X="1.043859008789" Y="-2.722413085938" />
                  <Point X="0.987081237793" Y="-2.769621826172" />
                  <Point X="0.863875061035" Y="-2.872063476562" />
                  <Point X="0.852653320312" Y="-2.883087890625" />
                  <Point X="0.832182128906" Y="-2.906838134766" />
                  <Point X="0.822933044434" Y="-2.919564208984" />
                  <Point X="0.806040710449" Y="-2.947390869141" />
                  <Point X="0.799018859863" Y="-2.961468017578" />
                  <Point X="0.787394348145" Y="-2.990588378906" />
                  <Point X="0.782791748047" Y="-3.005631347656" />
                  <Point X="0.765815490723" Y="-3.083735595703" />
                  <Point X="0.728977478027" Y="-3.253219238281" />
                  <Point X="0.727584472656" Y="-3.261289794922" />
                  <Point X="0.724724487305" Y="-3.289677734375" />
                  <Point X="0.7247421875" Y="-3.323170410156" />
                  <Point X="0.725554992676" Y="-3.335520019531" />
                  <Point X="0.833091552734" Y="-4.152341308594" />
                  <Point X="0.781432495117" Y="-3.959546875" />
                  <Point X="0.655064941406" Y="-3.487936523438" />
                  <Point X="0.652606262207" Y="-3.480124511719" />
                  <Point X="0.642145141602" Y="-3.453580566406" />
                  <Point X="0.626787231445" Y="-3.423815917969" />
                  <Point X="0.620407714844" Y="-3.413210205078" />
                  <Point X="0.568758178711" Y="-3.338792724609" />
                  <Point X="0.456679992676" Y="-3.177309814453" />
                  <Point X="0.446670684814" Y="-3.165172851562" />
                  <Point X="0.424786712646" Y="-3.142717529297" />
                  <Point X="0.412912322998" Y="-3.132399169922" />
                  <Point X="0.386657196045" Y="-3.113155273438" />
                  <Point X="0.373242340088" Y="-3.104937744141" />
                  <Point X="0.345241210938" Y="-3.090829589844" />
                  <Point X="0.330654907227" Y="-3.084938964844" />
                  <Point X="0.250730255127" Y="-3.060133056641" />
                  <Point X="0.077295951843" Y="-3.006305664062" />
                  <Point X="0.063377269745" Y="-3.003109619141" />
                  <Point X="0.035217689514" Y="-2.998840087891" />
                  <Point X="0.020976644516" Y="-2.997766601562" />
                  <Point X="-0.008664708138" Y="-2.997766601562" />
                  <Point X="-0.022905010223" Y="-2.998840087891" />
                  <Point X="-0.051064441681" Y="-3.003109375" />
                  <Point X="-0.064983421326" Y="-3.006305175781" />
                  <Point X="-0.144908355713" Y="-3.031110839844" />
                  <Point X="-0.318342529297" Y="-3.084938476562" />
                  <Point X="-0.332929412842" Y="-3.090829589844" />
                  <Point X="-0.360930847168" Y="-3.104937988281" />
                  <Point X="-0.374345275879" Y="-3.113155273438" />
                  <Point X="-0.400600524902" Y="-3.132399169922" />
                  <Point X="-0.412475219727" Y="-3.142717773438" />
                  <Point X="-0.434358886719" Y="-3.165173095703" />
                  <Point X="-0.444368041992" Y="-3.177309814453" />
                  <Point X="-0.49601763916" Y="-3.251727050781" />
                  <Point X="-0.60809564209" Y="-3.413210205078" />
                  <Point X="-0.612470336914" Y="-3.420132324219" />
                  <Point X="-0.625976745605" Y="-3.445265136719" />
                  <Point X="-0.638777832031" Y="-3.476215820312" />
                  <Point X="-0.642752990723" Y="-3.487936523438" />
                  <Point X="-0.916966552734" Y="-4.511315917969" />
                  <Point X="-0.985425231934" Y="-4.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.588260209118" Y="1.312269802813" />
                  <Point X="-4.692993725088" Y="0.567052114358" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.49406936434" Y="1.299869317107" />
                  <Point X="-4.600541649395" Y="0.542279643756" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.768067396607" Y="-0.649727985676" />
                  <Point X="-4.777528798389" Y="-0.717049357445" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.399878519563" Y="1.287468831401" />
                  <Point X="-4.508089573073" Y="0.517507177628" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.668379759054" Y="-0.6230167585" />
                  <Point X="-4.725900682105" Y="-1.032299392774" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.141779046565" Y="2.44133883617" />
                  <Point X="-4.168790158276" Y="2.249144789738" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.305687674786" Y="1.275068345694" />
                  <Point X="-4.415637493722" Y="0.492734733054" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.568692121502" Y="-0.596305531324" />
                  <Point X="-4.664016459271" Y="-1.274573438089" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.015420808637" Y="2.657821245738" />
                  <Point X="-4.082195043439" Y="2.182697877192" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.211496830008" Y="1.262667859988" />
                  <Point X="-4.323185414371" Y="0.46796228848" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.469004483949" Y="-0.569594304148" />
                  <Point X="-4.568068317582" Y="-1.274470106559" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.897372963691" Y="2.815172136701" />
                  <Point X="-3.995599925113" Y="2.116250989477" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.117305985231" Y="1.250267374282" />
                  <Point X="-4.230733335019" Y="0.443189843905" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.369316846397" Y="-0.542883076972" />
                  <Point X="-4.470326217021" Y="-1.26160209439" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.780288584383" Y="2.965667613428" />
                  <Point X="-3.909004806786" Y="2.049804101762" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.023115137505" Y="1.237866909556" />
                  <Point X="-4.138281255668" Y="0.418417399331" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.269629208844" Y="-0.516171849796" />
                  <Point X="-4.372584116461" Y="-1.248734082221" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.678491521777" Y="3.007388179761" />
                  <Point X="-3.82240968846" Y="1.983357214047" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.928924289658" Y="1.225466445691" />
                  <Point X="-4.045829176317" Y="0.393644954757" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.169941571292" Y="-0.48946062262" />
                  <Point X="-4.274842018896" Y="-1.235866091367" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.589757860483" Y="2.956157815929" />
                  <Point X="-3.735814570133" Y="1.916910326333" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.834733441811" Y="1.213065981827" />
                  <Point X="-3.953377096966" Y="0.368872510182" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.070253933739" Y="-0.462749395443" />
                  <Point X="-4.177099921832" Y="-1.222998104077" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.501024199189" Y="2.904927452097" />
                  <Point X="-3.649219451807" Y="1.850463438618" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.739990243285" Y="1.204595697254" />
                  <Point X="-3.860925017615" Y="0.344100065608" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.970566296647" Y="-0.436038171541" />
                  <Point X="-4.079357824768" Y="-1.210130116787" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.412290546064" Y="2.853697030136" />
                  <Point X="-3.56262433348" Y="1.784016550903" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.641161685135" Y="1.225194256864" />
                  <Point X="-3.768472938264" Y="0.319327621033" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.870878667014" Y="-0.409327000719" />
                  <Point X="-3.981615727704" Y="-1.197262129496" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.153781431028" Y="-2.422284762162" />
                  <Point X="-4.16142171803" Y="-2.476648228968" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.323556899892" Y="2.802466558702" />
                  <Point X="-3.47940478757" Y="1.693551217425" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.538348381761" Y="1.274145751986" />
                  <Point X="-3.676020858912" Y="0.294555176459" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.771191037382" Y="-0.382615829897" />
                  <Point X="-3.88387363064" Y="-1.184394142206" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.046251707461" Y="-2.339774193597" />
                  <Point X="-4.0837845415" Y="-2.60683418451" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.234553458905" Y="2.753155777127" />
                  <Point X="-3.583568779561" Y="0.269782731885" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.671503407749" Y="-0.355904659075" />
                  <Point X="-3.786131533576" Y="-1.171526154916" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.938721983894" Y="-2.257263625032" />
                  <Point X="-4.00614737196" Y="-2.737020189798" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.999061044293" Y="3.74616820315" />
                  <Point X="-3.016098761776" Y="3.62493854403" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.141873742317" Y="2.730003055657" />
                  <Point X="-3.491333019982" Y="0.243471092154" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.571815778117" Y="-0.329193488253" />
                  <Point X="-3.688389436512" Y="-1.158658167626" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.831192260327" Y="-2.174753056467" />
                  <Point X="-3.925258051069" Y="-2.844065935828" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.89154212969" Y="3.828601861938" />
                  <Point X="-2.938945965695" Y="3.491306042504" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.046839514057" Y="2.723603555248" />
                  <Point X="-3.402561675265" Y="0.192508859809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.471468015022" Y="-0.29778522378" />
                  <Point X="-3.590647339448" Y="-1.145790180335" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.72366253676" Y="-2.092242487902" />
                  <Point X="-3.844276933712" Y="-2.950458516062" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.784023215087" Y="3.911035520726" />
                  <Point X="-2.861793169613" Y="3.357673540979" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.94777972372" Y="2.745847417355" />
                  <Point X="-3.321228061124" Y="0.088624424517" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.364654990776" Y="-0.220374235866" />
                  <Point X="-3.492905242384" Y="-1.132922193045" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.616132813192" Y="-2.009731919336" />
                  <Point X="-3.754677933508" Y="-2.995531673615" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.676504300484" Y="3.993469179514" />
                  <Point X="-2.784716860087" Y="3.223496809337" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.839702475979" Y="2.832253822849" />
                  <Point X="-3.39516314532" Y="-1.120054205755" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.508603089625" Y="-1.927221350771" />
                  <Point X="-3.65027275164" Y="-2.935253374455" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.571540309298" Y="4.057723613578" />
                  <Point X="-3.297421048256" Y="-1.107186218465" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.401073377724" Y="-1.844710865213" />
                  <Point X="-3.545867567048" Y="-2.874975055906" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.467481620253" Y="4.115536488198" />
                  <Point X="-3.20053645619" Y="-1.10041969627" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.293543675806" Y="-1.762200450691" />
                  <Point X="-3.441462373054" Y="-2.814696670462" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.36342295391" Y="4.173349201286" />
                  <Point X="-3.107854002689" Y="-1.123552943595" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.186013973888" Y="-1.67969003617" />
                  <Point X="-3.33705717906" Y="-2.754418285019" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.259364289827" Y="4.231161898295" />
                  <Point X="-3.020166053859" Y="-1.182223938233" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.078484271971" Y="-1.597179621648" />
                  <Point X="-3.232651985066" Y="-2.694139899576" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.163523540133" Y="4.230501096081" />
                  <Point X="-2.947355559334" Y="-1.346753520782" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.959810850883" Y="-1.435377525154" />
                  <Point X="-3.128246791072" Y="-2.633861514132" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.081072233841" Y="4.134569453678" />
                  <Point X="-3.023841597078" Y="-2.573583128689" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.992664716292" Y="4.081018456514" />
                  <Point X="-2.919436403084" Y="-2.513304743246" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.902517312762" Y="4.039847391387" />
                  <Point X="-2.81503120909" Y="-2.453026357802" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.807871194055" Y="4.030686348008" />
                  <Point X="-2.710626015096" Y="-2.392747972359" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.89094897361" Y="-3.675812491625" />
                  <Point X="-2.919478631808" Y="-3.878811557757" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.707743381809" Y="4.060529580871" />
                  <Point X="-2.609136979424" Y="-2.353219131547" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.764149268586" Y="-3.45618888045" />
                  <Point X="-2.832912472697" Y="-3.945464500991" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.605129905834" Y="4.108059230173" />
                  <Point X="-2.513338060721" Y="-2.354177576735" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.637349563562" Y="-3.236565269276" />
                  <Point X="-2.746322185897" Y="-4.011945766805" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.44391388232" Y="4.572567671889" />
                  <Point X="-1.463031442176" Y="4.436539165324" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.479885032983" Y="4.316619635584" />
                  <Point X="-2.423365835625" Y="-2.396595101189" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.510549883138" Y="-3.016941833141" />
                  <Point X="-2.658068360322" Y="-4.066590339185" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.344045166151" Y="4.600567340366" />
                  <Point X="-2.340651518458" Y="-2.490655323971" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.383750225535" Y="-2.797318559383" />
                  <Point X="-2.569814534747" Y="-4.121234911565" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.244176446399" Y="4.628567034347" />
                  <Point X="-2.474402603099" Y="-4.124946912726" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.144307726646" Y="4.656566728328" />
                  <Point X="-2.35991880908" Y="-3.992955561816" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.044439006893" Y="4.684566422309" />
                  <Point X="-2.253977234925" Y="-3.921745263497" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.94457028714" Y="4.712566116289" />
                  <Point X="-2.148101148175" Y="-3.851000932273" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.846719872745" Y="4.72620482144" />
                  <Point X="-2.043874017494" Y="-3.791989533131" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.749181927754" Y="4.73762019125" />
                  <Point X="-1.945682730844" Y="-3.775925395863" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.651643980094" Y="4.749035580052" />
                  <Point X="-1.850624743215" Y="-3.782155839578" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.554106029772" Y="4.7604509878" />
                  <Point X="-1.755566755586" Y="-3.788386283293" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.456568079449" Y="4.771866395548" />
                  <Point X="-1.66050875823" Y="-3.7946166578" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.361227265423" Y="4.767648366218" />
                  <Point X="-1.567305884099" Y="-3.814046919929" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.298299627029" Y="4.532798608383" />
                  <Point X="-1.479035406822" Y="-3.868573009292" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.235371928815" Y="4.297949276193" />
                  <Point X="-1.39362833212" Y="-3.943473266641" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.159835170258" Y="4.152818070194" />
                  <Point X="-1.308221248368" Y="-4.018373459595" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.071392078731" Y="4.099520195037" />
                  <Point X="-1.224309490824" Y="-4.103913451375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.022537805113" Y="4.085262875811" />
                  <Point X="-1.158717581944" Y="-4.319805939659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.122829287583" Y="4.116270682827" />
                  <Point X="-1.121339476302" Y="-4.736450069255" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.24055929969" Y="4.271360075631" />
                  <Point X="-1.028497213698" Y="-4.758446215728" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.408238892294" Y="4.781859201143" />
                  <Point X="-0.876724515345" Y="-4.361130523942" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.502781012731" Y="4.771958171629" />
                  <Point X="-0.674968926147" Y="-3.608168084004" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.597323133168" Y="4.762057142115" />
                  <Point X="-0.537300204497" Y="-3.311207401018" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.691865253605" Y="4.752156112601" />
                  <Point X="-0.418565154543" Y="-3.148966792349" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.786407374042" Y="4.742255083086" />
                  <Point X="-0.313418146825" Y="-3.083410127995" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.879963296328" Y="4.725336889111" />
                  <Point X="-0.213109176016" Y="-3.052277884973" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.972748619542" Y="4.702935597832" />
                  <Point X="-0.112800212179" Y="-3.021145691567" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.065533944815" Y="4.680534321195" />
                  <Point X="-0.013633517946" Y="-2.998141168715" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.158319272111" Y="4.65813305896" />
                  <Point X="0.080991463714" Y="-3.007452609992" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.251104599407" Y="4.635731796725" />
                  <Point X="0.172915493473" Y="-3.035982322647" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.343889926704" Y="4.613330534491" />
                  <Point X="0.264839515037" Y="-3.064512093608" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.436675254" Y="4.590929272256" />
                  <Point X="0.356291944432" Y="-3.096397417213" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.528230232067" Y="4.559773620364" />
                  <Point X="0.443077919852" Y="-3.161486286142" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.619510807733" Y="4.526665493942" />
                  <Point X="0.523298057965" Y="-3.273293515051" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.710791383484" Y="4.493557368115" />
                  <Point X="0.603077021522" Y="-3.388239864038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.802071959234" Y="4.460449242289" />
                  <Point X="0.674700319735" Y="-3.561216787276" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.892866737373" Y="4.423884486853" />
                  <Point X="0.867966188607" Y="-2.868661846291" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.76270036222" Y="-3.617667120173" />
                  <Point X="0.737628004615" Y="-3.796066214345" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.982883869166" Y="4.381786490145" />
                  <Point X="0.976593423488" Y="-2.77834207896" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.809100839129" Y="-3.970113742431" />
                  <Point X="0.800555685546" Y="-4.030915669503" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.072900984125" Y="4.339688373656" />
                  <Point X="1.083905861747" Y="-2.697377575696" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.162918099083" Y="4.297590257168" />
                  <Point X="1.184086642307" Y="-2.667157453692" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.252898221482" Y="4.255228924941" />
                  <Point X="1.281277200695" Y="-2.658213868002" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.341571359819" Y="4.203567917891" />
                  <Point X="2.132319620593" Y="2.714664428247" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.059602131007" Y="2.197252604554" />
                  <Point X="1.378467763452" Y="-2.64927025122" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.43024447167" Y="4.151906722383" />
                  <Point X="2.259119319495" Y="2.93428799586" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.140587919613" Y="2.090893261989" />
                  <Point X="1.474030187389" Y="-2.651911444098" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.51891757312" Y="4.100245452872" />
                  <Point X="2.385919027445" Y="3.153911627851" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.22751044987" Y="2.026776031211" />
                  <Point X="1.565064542769" Y="-2.686771518896" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.607590674571" Y="4.048584183362" />
                  <Point X="2.512718749856" Y="3.373535362738" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.318052923688" Y="1.98841603724" />
                  <Point X="1.653048399647" Y="-2.743337018371" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.695166159256" Y="3.989112964752" />
                  <Point X="2.639518472266" Y="3.593159097624" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.412185428664" Y="1.975600442278" />
                  <Point X="1.741032248848" Y="-2.799902572461" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.782382918323" Y="3.927089280739" />
                  <Point X="2.766318194677" Y="3.812782832511" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.509516345608" Y="1.985542730996" />
                  <Point X="1.827291864481" Y="-2.868736685889" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.609651521324" Y="2.015438357667" />
                  <Point X="1.908486888058" Y="-2.97360724428" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.713628800893" Y="2.072671973766" />
                  <Point X="2.1016195555" Y="-2.282000080717" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.034541231184" Y="-2.759287158588" />
                  <Point X="1.989569699045" Y="-3.079276236741" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.818033991792" Y="2.132950337191" />
                  <Point X="2.216305043996" Y="-2.148573599038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.105628942418" Y="-2.936074981202" />
                  <Point X="2.070652510031" Y="-3.184945229203" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.922439182692" Y="2.193228700615" />
                  <Point X="2.319973717781" Y="-2.093535827192" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.182781738575" Y="-3.069707482187" />
                  <Point X="2.151735321018" Y="-3.290614221664" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.026844373591" Y="2.25350706404" />
                  <Point X="2.42310694852" Y="-2.042307930581" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.259934534733" Y="-3.203339983171" />
                  <Point X="2.232818119269" Y="-3.396283304745" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.131249564491" Y="2.313785427465" />
                  <Point X="3.015538624949" Y="1.490459311701" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.001132194768" Y="1.387952234583" />
                  <Point X="2.521392114767" Y="-2.025575805269" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.33708733089" Y="-3.336972484155" />
                  <Point X="2.313900916605" Y="-3.50195239433" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.23565475539" Y="2.37406379089" />
                  <Point X="3.1431155002" Y="1.715612776379" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.066325959397" Y="1.169226802755" />
                  <Point X="2.615545460074" Y="-2.038243113569" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.414240127048" Y="-3.470604985139" />
                  <Point X="2.394983713942" Y="-3.607621483915" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.34005994629" Y="2.434342154315" />
                  <Point X="3.251482154815" Y="1.804078418779" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.147408704355" Y="1.063557340471" />
                  <Point X="2.709104402824" Y="-2.055139815834" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.491392923206" Y="-3.604237486123" />
                  <Point X="2.476066511279" Y="-3.713290573499" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.444465137189" Y="2.49462051774" />
                  <Point X="3.359011866851" Y="1.886588905298" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.235319272615" Y="1.006470365384" />
                  <Point X="2.930211838748" Y="-1.164481831627" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.873615470106" Y="-1.567185919455" />
                  <Point X="2.802188611693" Y="-2.075414425177" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.568545719363" Y="-3.737869987107" />
                  <Point X="2.557149308616" Y="-3.818959663084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.548870328089" Y="2.554898881164" />
                  <Point X="3.466541578887" Y="1.969099391817" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.326018350351" Y="0.969224666196" />
                  <Point X="3.045467967861" Y="-1.026995030978" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.950433690911" Y="-1.703199047776" />
                  <Point X="2.891891420951" Y="-2.11974894293" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.645698506959" Y="-3.871502549012" />
                  <Point X="2.638232105952" Y="-3.924628752669" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.653275518988" Y="2.615177244589" />
                  <Point X="3.574071295135" Y="2.051609908302" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.419977092578" Y="0.955172685031" />
                  <Point X="3.150882691665" Y="-0.959533467687" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.030617840495" Y="-1.815262348371" />
                  <Point X="2.980625071148" Y="-2.170979385721" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.722851288581" Y="-4.005135153422" />
                  <Point X="2.719314903289" Y="-4.030297842254" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.757680703171" Y="2.675455560221" />
                  <Point X="3.681601017641" Y="2.134120469314" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.515859132909" Y="0.954805680963" />
                  <Point X="3.410223794538" Y="0.203171192704" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.362864398678" Y="-0.133808418668" />
                  <Point X="3.250216665785" Y="-0.935338686594" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.116705879875" Y="-1.885317290266" />
                  <Point X="3.069358721345" Y="-2.222209828513" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.862085887061" Y="2.735733873774" />
                  <Point X="3.789130740146" Y="2.216631030325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.613601230197" Y="0.967673669847" />
                  <Point X="3.524206193197" Y="0.331594930244" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.435538200458" Y="-0.299310620632" />
                  <Point X="3.349173129676" Y="-0.913830030346" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.203301006753" Y="-1.951764117133" />
                  <Point X="3.158092371542" Y="-2.273440271305" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.960457731953" Y="2.753082749691" />
                  <Point X="3.896660462652" Y="2.299141591337" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.711343327485" Y="0.980541658732" />
                  <Point X="3.628967181777" Y="0.394404925714" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.518765846189" Y="-0.389718320893" />
                  <Point X="3.445586992453" Y="-0.910412921091" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.289896133631" Y="-2.018210943999" />
                  <Point X="3.246826021739" Y="-2.324670714097" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.040715447402" Y="2.641542897422" />
                  <Point X="4.004190185158" Y="2.381652152348" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.809085424773" Y="0.993409647616" />
                  <Point X="3.730124073169" Y="0.431570437173" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.607030580552" Y="-0.444285273216" />
                  <Point X="3.539777846118" Y="-0.922813343553" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.37649126051" Y="-2.084657770866" />
                  <Point X="3.335559673455" Y="-2.375901146082" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.118665130358" Y="2.513580540637" />
                  <Point X="4.111719907663" Y="2.46416271336" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.906827522061" Y="1.0062776365" />
                  <Point X="3.829811708999" Y="0.458281652093" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.697198812101" Y="-0.485308139294" />
                  <Point X="3.633968699784" Y="-0.935213766015" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.463086387388" Y="-2.151104597732" />
                  <Point X="3.424293325775" Y="-2.427131573772" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.004569619349" Y="1.019145625385" />
                  <Point X="3.929499344829" Y="0.484992867012" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.789650891402" Y="-0.510080584223" />
                  <Point X="3.72815955345" Y="-0.947614188478" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.549681514266" Y="-2.217551424599" />
                  <Point X="3.513026978094" Y="-2.478362001462" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.102311716637" Y="1.032013614269" />
                  <Point X="4.029186980659" Y="0.511704081932" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.882102970703" Y="-0.534853029153" />
                  <Point X="3.822350407116" Y="-0.96001461094" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.636276641144" Y="-2.283998251466" />
                  <Point X="3.601760630414" Y="-2.529592429152" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.200053813925" Y="1.044881603154" />
                  <Point X="4.128874616081" Y="0.538415293948" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.974555050005" Y="-0.559625474083" />
                  <Point X="3.916541260478" Y="-0.972415035562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.722871768022" Y="-2.350445078332" />
                  <Point X="3.690494282733" Y="-2.580822856842" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.297795911213" Y="1.057749592038" />
                  <Point X="4.228562250391" Y="0.565126498055" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.067007129306" Y="-0.584397919013" />
                  <Point X="4.010732107233" Y="-0.984815507196" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.8094668949" Y="-2.416891905199" />
                  <Point X="3.779227935053" Y="-2.632053284532" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.395538008501" Y="1.070617580922" />
                  <Point X="4.328249884702" Y="0.591837702162" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.159459208607" Y="-0.609170363942" />
                  <Point X="4.104922953988" Y="-0.997215978829" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.896062018793" Y="-2.483338753303" />
                  <Point X="3.867961587372" Y="-2.683283712222" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.493280106754" Y="1.083485576671" />
                  <Point X="4.427937519012" Y="0.618548906268" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.251911287908" Y="-0.633942808872" />
                  <Point X="4.199113800743" Y="-1.009616450463" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.982657130811" Y="-2.54978568591" />
                  <Point X="3.956695239692" Y="-2.734514139912" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.591022208284" Y="1.096353595735" />
                  <Point X="4.527625153322" Y="0.645260110375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.344363367209" Y="-0.658715253802" />
                  <Point X="4.293304647498" Y="-1.022016922097" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.069252242828" Y="-2.616232618516" />
                  <Point X="4.061476655928" Y="-2.671558794122" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.688764309813" Y="1.1092216148" />
                  <Point X="4.627312787633" Y="0.671971314482" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.436815446511" Y="-0.683487698732" />
                  <Point X="4.387495494254" Y="-1.034417393731" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.755106845334" Y="0.898670112589" />
                  <Point X="4.727000421943" Y="0.698682518588" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.529267525812" Y="-0.708260143662" />
                  <Point X="4.481686341009" Y="-1.046817865365" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.621719603875" Y="-0.7330325974" />
                  <Point X="4.575877187764" Y="-1.059218336999" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.7141716803" Y="-0.757805062799" />
                  <Point X="4.670068034519" Y="-1.071618808633" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="0.001626220703" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.597906616211" Y="-4.00872265625" />
                  <Point X="0.471539031982" Y="-3.537112304688" />
                  <Point X="0.464318664551" Y="-3.521544189453" />
                  <Point X="0.4126690979" Y="-3.447126708984" />
                  <Point X="0.300591033936" Y="-3.285643798828" />
                  <Point X="0.274335754395" Y="-3.266399902344" />
                  <Point X="0.194410964966" Y="-3.241593994141" />
                  <Point X="0.020976625443" Y="-3.187766601562" />
                  <Point X="-0.008664756775" Y="-3.187766601562" />
                  <Point X="-0.088589553833" Y="-3.212572265625" />
                  <Point X="-0.262023895264" Y="-3.266399902344" />
                  <Point X="-0.288279144287" Y="-3.285643798828" />
                  <Point X="-0.339928710938" Y="-3.360061035156" />
                  <Point X="-0.45200680542" Y="-3.521544189453" />
                  <Point X="-0.459227142334" Y="-3.537112304688" />
                  <Point X="-0.733440673828" Y="-4.560491699219" />
                  <Point X="-0.84774395752" Y="-4.987076660156" />
                  <Point X="-0.896539001465" Y="-4.97760546875" />
                  <Point X="-1.100246459961" Y="-4.938065429688" />
                  <Point X="-1.192067749023" Y="-4.914440429688" />
                  <Point X="-1.35158972168" Y="-4.873396972656" />
                  <Point X="-1.324361938477" Y="-4.666582519531" />
                  <Point X="-1.309150146484" Y="-4.551038574219" />
                  <Point X="-1.309683227539" Y="-4.5347578125" />
                  <Point X="-1.32877734375" Y="-4.438765625" />
                  <Point X="-1.370210693359" Y="-4.230466308594" />
                  <Point X="-1.386282714844" Y="-4.20262890625" />
                  <Point X="-1.45986730957" Y="-4.138096679688" />
                  <Point X="-1.619543457031" Y="-3.998063964844" />
                  <Point X="-1.649240722656" Y="-3.985762939453" />
                  <Point X="-1.746903930664" Y="-3.979361816406" />
                  <Point X="-1.958829833984" Y="-3.965471435547" />
                  <Point X="-1.989878540039" Y="-3.973791015625" />
                  <Point X="-2.071256835938" Y="-4.028166259766" />
                  <Point X="-2.247844970703" Y="-4.146159179688" />
                  <Point X="-2.259734130859" Y="-4.157294433594" />
                  <Point X="-2.413683837891" Y="-4.357925292969" />
                  <Point X="-2.457095214844" Y="-4.414500488281" />
                  <Point X="-2.557240234375" Y="-4.352493164063" />
                  <Point X="-2.855831054688" Y="-4.16761328125" />
                  <Point X="-2.982981201172" Y="-4.069711914062" />
                  <Point X="-3.228581054688" Y="-3.880608154297" />
                  <Point X="-2.740873779297" Y="-3.035874267578" />
                  <Point X="-2.506033691406" Y="-2.629119628906" />
                  <Point X="-2.499762695312" Y="-2.597593261719" />
                  <Point X="-2.513979736328" Y="-2.568764404297" />
                  <Point X="-2.531328369141" Y="-2.551415771484" />
                  <Point X="-2.560157226562" Y="-2.537198730469" />
                  <Point X="-2.59168359375" Y="-2.543469726562" />
                  <Point X="-3.474328125" Y="-3.053064941406" />
                  <Point X="-3.842959472656" Y="-3.265894042969" />
                  <Point X="-3.925798339844" Y="-3.157060546875" />
                  <Point X="-4.161703613281" Y="-2.847129394531" />
                  <Point X="-4.252857910156" Y="-2.694277832031" />
                  <Point X="-4.431020019531" Y="-2.395527099609" />
                  <Point X="-3.574680664062" Y="-1.738434936523" />
                  <Point X="-3.163786865234" Y="-1.423144897461" />
                  <Point X="-3.152535644531" Y="-1.411081542969" />
                  <Point X="-3.143391601562" Y="-1.386629638672" />
                  <Point X="-3.138117431641" Y="-1.366266357422" />
                  <Point X="-3.136651855469" Y="-1.350051269531" />
                  <Point X="-3.148656738281" Y="-1.321068603516" />
                  <Point X="-3.169513183594" Y="-1.305722167969" />
                  <Point X="-3.187641601562" Y="-1.295052734375" />
                  <Point X="-3.219529296875" Y="-1.288571044922" />
                  <Point X="-4.333784667969" Y="-1.435265625" />
                  <Point X="-4.803283203125" Y="-1.497076293945" />
                  <Point X="-4.83512890625" Y="-1.372401733398" />
                  <Point X="-4.927393066406" Y="-1.011191589355" />
                  <Point X="-4.951510253906" Y="-0.842567993164" />
                  <Point X="-4.998396484375" Y="-0.514742126465" />
                  <Point X="-4.025533691406" Y="-0.254064254761" />
                  <Point X="-3.557463867188" Y="-0.128645431519" />
                  <Point X="-3.533140869141" Y="-0.115348640442" />
                  <Point X="-3.514142822266" Y="-0.102163047791" />
                  <Point X="-3.502324462891" Y="-0.090645431519" />
                  <Point X="-3.491980712891" Y="-0.066505012512" />
                  <Point X="-3.485647949219" Y="-0.046101043701" />
                  <Point X="-3.483400878906" Y="-0.031280042648" />
                  <Point X="-3.488566162109" Y="-0.007056134224" />
                  <Point X="-3.494898925781" Y="0.013347833633" />
                  <Point X="-3.502324462891" Y="0.028085344315" />
                  <Point X="-3.522897705078" Y="0.045679393768" />
                  <Point X="-3.541895751953" Y="0.05886498642" />
                  <Point X="-3.557463867188" Y="0.066085334778" />
                  <Point X="-4.57316015625" Y="0.338240264893" />
                  <Point X="-4.998186523438" Y="0.452125793457" />
                  <Point X="-4.977107910156" Y="0.594573120117" />
                  <Point X="-4.917645019531" Y="0.996418212891" />
                  <Point X="-4.86909765625" Y="1.175573852539" />
                  <Point X="-4.773516113281" Y="1.528298583984" />
                  <Point X="-4.088790283203" Y="1.438152709961" />
                  <Point X="-3.753266357422" Y="1.393980102539" />
                  <Point X="-3.731704833984" Y="1.395866577148" />
                  <Point X="-3.712327392578" Y="1.401976318359" />
                  <Point X="-3.670278808594" Y="1.415234130859" />
                  <Point X="-3.651534423828" Y="1.426056152344" />
                  <Point X="-3.639119873047" Y="1.443786132813" />
                  <Point X="-3.631344482422" Y="1.462557250977" />
                  <Point X="-3.614472412109" Y="1.503290283203" />
                  <Point X="-3.610714111328" Y="1.524605224609" />
                  <Point X="-3.616315917969" Y="1.54551159668" />
                  <Point X="-3.625697753906" Y="1.563533813477" />
                  <Point X="-3.646055664062" Y="1.602641235352" />
                  <Point X="-3.659968261719" Y="1.619221679688" />
                  <Point X="-4.24257421875" Y="2.066270996094" />
                  <Point X="-4.47610546875" Y="2.245466064453" />
                  <Point X="-4.391065917969" Y="2.391159423828" />
                  <Point X="-4.160014648438" Y="2.787006591797" />
                  <Point X="-4.031414794922" Y="2.952303466797" />
                  <Point X="-3.774671142578" Y="3.282310791016" />
                  <Point X="-3.365226318359" Y="3.045917724609" />
                  <Point X="-3.159156982422" Y="2.926943359375" />
                  <Point X="-3.138514892578" Y="2.920434814453" />
                  <Point X="-3.11152734375" Y="2.918073730469" />
                  <Point X="-3.052965576172" Y="2.912950195312" />
                  <Point X="-3.031506835938" Y="2.915775390625" />
                  <Point X="-3.013252441406" Y="2.927404785156" />
                  <Point X="-2.994096435547" Y="2.946560546875" />
                  <Point X="-2.952528808594" Y="2.988128173828" />
                  <Point X="-2.940899414062" Y="3.006382324219" />
                  <Point X="-2.93807421875" Y="3.027841308594" />
                  <Point X="-2.940435302734" Y="3.054828857422" />
                  <Point X="-2.945558837891" Y="3.113390869141" />
                  <Point X="-2.952067382812" Y="3.134032958984" />
                  <Point X="-3.210237792969" Y="3.581197021484" />
                  <Point X="-3.307278564453" Y="3.749276611328" />
                  <Point X="-3.155294921875" Y="3.865801269531" />
                  <Point X="-2.752876220703" Y="4.174331542969" />
                  <Point X="-2.550335693359" Y="4.286858398438" />
                  <Point X="-2.141548583984" Y="4.513972167969" />
                  <Point X="-2.030911010742" Y="4.369786132812" />
                  <Point X="-1.967826904297" Y="4.287573242188" />
                  <Point X="-1.951246948242" Y="4.273660644531" />
                  <Point X="-1.921210083008" Y="4.258024414062" />
                  <Point X="-1.856030761719" Y="4.22409375" />
                  <Point X="-1.835124145508" Y="4.2184921875" />
                  <Point X="-1.81380859375" Y="4.222250488281" />
                  <Point X="-1.782523193359" Y="4.235209472656" />
                  <Point X="-1.714634765625" Y="4.263330078125" />
                  <Point X="-1.696905273438" Y="4.275744628906" />
                  <Point X="-1.686083496094" Y="4.294488769531" />
                  <Point X="-1.675900512695" Y="4.326784667969" />
                  <Point X="-1.653804077148" Y="4.396865234375" />
                  <Point X="-1.651917602539" Y="4.418426757812" />
                  <Point X="-1.681268676758" Y="4.641370605469" />
                  <Point X="-1.689137573242" Y="4.701141113281" />
                  <Point X="-1.489050048828" Y="4.757238769531" />
                  <Point X="-0.968094177246" Y="4.903296386719" />
                  <Point X="-0.722555236816" Y="4.932033203125" />
                  <Point X="-0.224199630737" Y="4.990358398438" />
                  <Point X="-0.103101448059" Y="4.538413085938" />
                  <Point X="-0.042140380859" Y="4.310903320312" />
                  <Point X="-0.024282115936" Y="4.284176757813" />
                  <Point X="0.006155934334" Y="4.273844238281" />
                  <Point X="0.036594036102" Y="4.284176757813" />
                  <Point X="0.054452251434" Y="4.310903320312" />
                  <Point X="0.186735778809" Y="4.804592285156" />
                  <Point X="0.236648406982" Y="4.990868652344" />
                  <Point X="0.405334960938" Y="4.973202636719" />
                  <Point X="0.860205810547" Y="4.925565429688" />
                  <Point X="1.063347900391" Y="4.876520996094" />
                  <Point X="1.508455932617" Y="4.769058105469" />
                  <Point X="1.640639282227" Y="4.721114257812" />
                  <Point X="1.931033691406" Y="4.615786132813" />
                  <Point X="2.058890869141" Y="4.555991699219" />
                  <Point X="2.33869921875" Y="4.425134277344" />
                  <Point X="2.462218505859" Y="4.353171875" />
                  <Point X="2.732523681641" Y="4.19569140625" />
                  <Point X="2.849015136719" Y="4.112849121094" />
                  <Point X="3.068740234375" Y="3.956592773438" />
                  <Point X="2.501663085938" Y="2.974385986328" />
                  <Point X="2.229853515625" Y="2.503597900391" />
                  <Point X="2.224852050781" Y="2.491514648438" />
                  <Point X="2.218079101562" Y="2.4661875" />
                  <Point X="2.203382324219" Y="2.411228271484" />
                  <Point X="2.202044677734" Y="2.392325927734" />
                  <Point X="2.204685546875" Y="2.370425292969" />
                  <Point X="2.210416015625" Y="2.322901123047" />
                  <Point X="2.218682128906" Y="2.300812255859" />
                  <Point X="2.232233642578" Y="2.280840820312" />
                  <Point X="2.261639892578" Y="2.237503662109" />
                  <Point X="2.274940185547" Y="2.224203613281" />
                  <Point X="2.294911376953" Y="2.210652099609" />
                  <Point X="2.338248779297" Y="2.18124609375" />
                  <Point X="2.360336914062" Y="2.172980224609" />
                  <Point X="2.382237792969" Y="2.170339355469" />
                  <Point X="2.42976171875" Y="2.164608642578" />
                  <Point X="2.448664306641" Y="2.165946289062" />
                  <Point X="2.473991455078" Y="2.172719238281" />
                  <Point X="2.528950683594" Y="2.187416015625" />
                  <Point X="2.541033935547" Y="2.192417480469" />
                  <Point X="3.56262890625" Y="2.782235595703" />
                  <Point X="3.994248046875" Y="3.031430908203" />
                  <Point X="4.042256103516" Y="2.964710693359" />
                  <Point X="4.202591796875" Y="2.741880126953" />
                  <Point X="4.267534179688" Y="2.634562255859" />
                  <Point X="4.387512695312" Y="2.436296142578" />
                  <Point X="3.646479003906" Y="1.867680541992" />
                  <Point X="3.288616210938" Y="1.593082641602" />
                  <Point X="3.279371337891" Y="1.583833374023" />
                  <Point X="3.261143310547" Y="1.560053466797" />
                  <Point X="3.221589111328" Y="1.508451904297" />
                  <Point X="3.213119628906" Y="1.4915" />
                  <Point X="3.206329589844" Y="1.467220703125" />
                  <Point X="3.191595703125" Y="1.414535522461" />
                  <Point X="3.190779541016" Y="1.390965209961" />
                  <Point X="3.196353271484" Y="1.363951538086" />
                  <Point X="3.208448486328" Y="1.305332397461" />
                  <Point X="3.215646484375" Y="1.287954956055" />
                  <Point X="3.230806640625" Y="1.264911865234" />
                  <Point X="3.263704101562" Y="1.214909667969" />
                  <Point X="3.280947998047" Y="1.198819824219" />
                  <Point X="3.302917236328" Y="1.186453125" />
                  <Point X="3.350590087891" Y="1.159617431641" />
                  <Point X="3.368565429688" Y="1.153619628906" />
                  <Point X="3.398269287109" Y="1.149693847656" />
                  <Point X="3.462726074219" Y="1.141175048828" />
                  <Point X="3.475803710938" Y="1.14117175293" />
                  <Point X="4.446250488281" Y="1.26893359375" />
                  <Point X="4.848975097656" Y="1.32195324707" />
                  <Point X="4.870325195312" Y="1.234254394531" />
                  <Point X="4.939188476562" Y="0.951385986328" />
                  <Point X="4.959653808594" Y="0.819940551758" />
                  <Point X="4.997858398438" Y="0.574556152344" />
                  <Point X="4.151286132813" Y="0.34771786499" />
                  <Point X="3.741167724609" Y="0.237826919556" />
                  <Point X="3.729086914062" Y="0.232819412231" />
                  <Point X="3.699903564453" Y="0.215950927734" />
                  <Point X="3.636576904297" Y="0.179346878052" />
                  <Point X="3.622264892578" Y="0.166926574707" />
                  <Point X="3.604754882812" Y="0.144614761353" />
                  <Point X="3.566759033203" Y="0.096198890686" />
                  <Point X="3.556985351562" Y="0.074735275269" />
                  <Point X="3.551148681641" Y="0.044258472443" />
                  <Point X="3.538483154297" Y="-0.021875303268" />
                  <Point X="3.538483154297" Y="-0.04068478775" />
                  <Point X="3.544319824219" Y="-0.071161590576" />
                  <Point X="3.556985351562" Y="-0.13729536438" />
                  <Point X="3.566759033203" Y="-0.158759140015" />
                  <Point X="3.584268798828" Y="-0.181070785522" />
                  <Point X="3.622264892578" Y="-0.229486679077" />
                  <Point X="3.636576904297" Y="-0.241906967163" />
                  <Point X="3.665760253906" Y="-0.25877545166" />
                  <Point X="3.729086914062" Y="-0.295379486084" />
                  <Point X="3.741167724609" Y="-0.300387023926" />
                  <Point X="4.631111816406" Y="-0.538846679688" />
                  <Point X="4.998067871094" Y="-0.637172363281" />
                  <Point X="4.986881835938" Y="-0.711367553711" />
                  <Point X="4.948431640625" Y="-0.966399169922" />
                  <Point X="4.922212402344" Y="-1.081296386719" />
                  <Point X="4.874545898438" Y="-1.290178344727" />
                  <Point X="3.887604248047" Y="-1.160244873047" />
                  <Point X="3.411982177734" Y="-1.097628295898" />
                  <Point X="3.394836181641" Y="-1.098341186523" />
                  <Point X="3.337559814453" Y="-1.110790527344" />
                  <Point X="3.213271972656" Y="-1.137804931641" />
                  <Point X="3.1854453125" Y="-1.154697265625" />
                  <Point X="3.150825195312" Y="-1.196334350586" />
                  <Point X="3.075701171875" Y="-1.286685424805" />
                  <Point X="3.064357910156" Y="-1.314070556641" />
                  <Point X="3.059395996094" Y="-1.367992431641" />
                  <Point X="3.04862890625" Y="-1.485000854492" />
                  <Point X="3.056360595703" Y="-1.516621948242" />
                  <Point X="3.088058105469" Y="-1.565925537109" />
                  <Point X="3.156840820312" Y="-1.672912475586" />
                  <Point X="3.168460693359" Y="-1.685540649414" />
                  <Point X="3.994333984375" Y="-2.319255615234" />
                  <Point X="4.33907421875" Y="-2.583784179688" />
                  <Point X="4.31251953125" Y="-2.626753662109" />
                  <Point X="4.204133300781" Y="-2.802139160156" />
                  <Point X="4.149907714844" Y="-2.879186035156" />
                  <Point X="4.056688232422" Y="-3.011638427734" />
                  <Point X="3.177060302734" Y="-2.503784667969" />
                  <Point X="2.753454589844" Y="-2.259215576172" />
                  <Point X="2.737340820313" Y="-2.253312744141" />
                  <Point X="2.669172851562" Y="-2.241001708984" />
                  <Point X="2.521250488281" Y="-2.214287109375" />
                  <Point X="2.489077636719" Y="-2.219244873047" />
                  <Point X="2.432446777344" Y="-2.249049316406" />
                  <Point X="2.309559570312" Y="-2.313723876953" />
                  <Point X="2.288599853516" Y="-2.33468359375" />
                  <Point X="2.258795410156" Y="-2.391314453125" />
                  <Point X="2.194120849609" Y="-2.514201660156" />
                  <Point X="2.189163085938" Y="-2.546374511719" />
                  <Point X="2.201474121094" Y="-2.614542480469" />
                  <Point X="2.228188720703" Y="-2.76246484375" />
                  <Point X="2.234091552734" Y="-2.778578613281" />
                  <Point X="2.764798339844" Y="-3.697789306641" />
                  <Point X="2.986673583984" Y="-4.082088623047" />
                  <Point X="2.963909667969" Y="-4.098348632812" />
                  <Point X="2.835298339844" Y="-4.190212402344" />
                  <Point X="2.774674072266" Y="-4.229453613281" />
                  <Point X="2.679775878906" Y="-4.290879394531" />
                  <Point X="2.006427978516" Y="-3.413355224609" />
                  <Point X="1.683177612305" Y="-2.992087402344" />
                  <Point X="1.670549316406" Y="-2.980467529297" />
                  <Point X="1.603317260742" Y="-2.937243408203" />
                  <Point X="1.45742590332" Y="-2.843448730469" />
                  <Point X="1.425804931641" Y="-2.835717041016" />
                  <Point X="1.352275024414" Y="-2.842483398438" />
                  <Point X="1.192718017578" Y="-2.857166015625" />
                  <Point X="1.165332885742" Y="-2.868509277344" />
                  <Point X="1.108555175781" Y="-2.915718017578" />
                  <Point X="0.985349060059" Y="-3.018159667969" />
                  <Point X="0.968456726074" Y="-3.045986328125" />
                  <Point X="0.951480529785" Y="-3.124090576172" />
                  <Point X="0.91464251709" Y="-3.29357421875" />
                  <Point X="0.91392956543" Y="-3.310720214844" />
                  <Point X="1.064327758789" Y="-4.453107910156" />
                  <Point X="1.127642211914" Y="-4.934028808594" />
                  <Point X="1.116022338867" Y="-4.936575683594" />
                  <Point X="0.994346435547" Y="-4.963246582031" />
                  <Point X="0.938338317871" Y="-4.973421386719" />
                  <Point X="0.860200317383" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#148" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.06096105203" Y="4.582634228643" Z="0.8" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.8" />
                  <Point X="-0.734523202698" Y="5.012974273047" Z="0.8" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.8" />
                  <Point X="-1.508703818612" Y="4.836662241728" Z="0.8" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.8" />
                  <Point X="-1.736996940898" Y="4.66612412805" Z="0.8" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.8" />
                  <Point X="-1.729742312464" Y="4.373099843406" Z="0.8" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.8" />
                  <Point X="-1.80781358704" Y="4.31268355068" Z="0.8" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.8" />
                  <Point X="-1.904278255919" Y="4.333654967541" Z="0.8" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.8" />
                  <Point X="-1.997399298944" Y="4.431504154989" Z="0.8" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.8" />
                  <Point X="-2.580774539162" Y="4.361846143208" Z="0.8" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.8" />
                  <Point X="-3.19187294104" Y="3.936761127657" Z="0.8" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.8" />
                  <Point X="-3.25969499491" Y="3.587476974855" Z="0.8" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.8" />
                  <Point X="-2.996401132513" Y="3.081751009208" Z="0.8" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.8" />
                  <Point X="-3.035607668061" Y="3.013195899825" Z="0.8" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.8" />
                  <Point X="-3.113325456399" Y="2.99916356666" Z="0.8" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.8" />
                  <Point X="-3.346382373442" Y="3.120498953836" Z="0.8" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.8" />
                  <Point X="-4.077034053908" Y="3.01428585996" Z="0.8" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.8" />
                  <Point X="-4.44040425705" Y="2.447644560713" Z="0.8" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.8" />
                  <Point X="-4.27916817307" Y="2.05788328882" Z="0.8" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.8" />
                  <Point X="-3.676204187547" Y="1.571726565253" Z="0.8" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.8" />
                  <Point X="-3.683694599879" Y="1.512971365389" Z="0.8" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.8" />
                  <Point X="-3.733518534205" Y="1.480942242495" Z="0.8" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.8" />
                  <Point X="-4.08841994626" Y="1.519005104384" Z="0.8" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.8" />
                  <Point X="-4.923513272879" Y="1.219931229473" Z="0.8" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.8" />
                  <Point X="-5.03272542084" Y="0.633172221906" Z="0.8" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.8" />
                  <Point X="-4.592257188919" Y="0.321224080653" Z="0.8" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.8" />
                  <Point X="-3.557562799386" Y="0.035883379362" Z="0.8" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.8" />
                  <Point X="-3.542475105177" Y="0.009402919699" Z="0.8" />
                  <Point X="-3.539556741714" Y="0" Z="0.8" />
                  <Point X="-3.545889495958" Y="-0.020404031362" Z="0.8" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.8" />
                  <Point X="-3.567805795737" Y="-0.042992603772" Z="0.8" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.8" />
                  <Point X="-4.044630599854" Y="-0.174487976103" Z="0.8" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.8" />
                  <Point X="-5.007162078061" Y="-0.818366922321" Z="0.8" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.8" />
                  <Point X="-4.889729755139" Y="-1.353496006647" Z="0.8" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.8" />
                  <Point X="-4.333414031213" Y="-1.453557752385" Z="0.8" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.8" />
                  <Point X="-3.201030517925" Y="-1.317532877622" Z="0.8" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.8" />
                  <Point X="-3.197950336836" Y="-1.342813047014" Z="0.8" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.8" />
                  <Point X="-3.611274524366" Y="-1.667486999192" Z="0.8" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.8" />
                  <Point X="-4.301956997537" Y="-2.688607470617" Z="0.8" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.8" />
                  <Point X="-3.971740315579" Y="-3.156063705373" Z="0.8" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.8" />
                  <Point X="-3.455484135518" Y="-3.065086082304" Z="0.8" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.8" />
                  <Point X="-2.560963748236" Y="-2.567366720896" Z="0.8" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.8" />
                  <Point X="-2.790330930864" Y="-2.979594246974" Z="0.8" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.8" />
                  <Point X="-3.01964103592" Y="-4.078049003647" Z="0.8" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.8" />
                  <Point X="-2.589717831024" Y="-4.363723842738" Z="0.8" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.8" />
                  <Point X="-2.380172083985" Y="-4.35708340384" Z="0.8" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.8" />
                  <Point X="-2.049634203106" Y="-4.038459626868" Z="0.8" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.8" />
                  <Point X="-1.756330038109" Y="-3.997974744045" Z="0.8" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.8" />
                  <Point X="-1.49899060124" Y="-4.144408436467" Z="0.8" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.8" />
                  <Point X="-1.383972561133" Y="-4.417240170402" Z="0.8" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.8" />
                  <Point X="-1.380090209615" Y="-4.628776319243" Z="0.8" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.8" />
                  <Point X="-1.210682668534" Y="-4.931583225641" Z="0.8" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.8" />
                  <Point X="-0.912222281846" Y="-4.995409647229" Z="0.8" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.8" />
                  <Point X="-0.691300449121" Y="-4.542152497085" Z="0.8" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.8" />
                  <Point X="-0.305008723737" Y="-3.357289967401" Z="0.8" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.8" />
                  <Point X="-0.07992481597" Y="-3.229045081815" Z="0.8" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.8" />
                  <Point X="0.173434263391" Y="-3.258066963473" Z="0.8" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.8" />
                  <Point X="0.365437135695" Y="-3.444355788367" Z="0.8" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.8" />
                  <Point X="0.54345441128" Y="-3.99038353273" Z="0.8" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.8" />
                  <Point X="0.941119332093" Y="-4.991336363966" Z="0.8" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.8" />
                  <Point X="1.120572014285" Y="-4.954135888832" Z="0.8" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.8" />
                  <Point X="1.107744004794" Y="-4.415301464399" Z="0.8" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.8" />
                  <Point X="0.99418374417" Y="-3.103430180487" Z="0.8" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.8" />
                  <Point X="1.134365967848" Y="-2.9228843048" Z="0.8" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.8" />
                  <Point X="1.350700776399" Y="-2.860992857236" Z="0.8" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.8" />
                  <Point X="1.570121841219" Y="-2.948021269843" Z="0.8" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.8" />
                  <Point X="1.960604129846" Y="-3.412513088919" Z="0.8" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.8" />
                  <Point X="2.79568717443" Y="-4.240147696871" Z="0.8" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.8" />
                  <Point X="2.986815140796" Y="-4.107755482724" Z="0.8" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.8" />
                  <Point X="2.801943637122" Y="-3.641509276087" Z="0.8" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.8" />
                  <Point X="2.244522211419" Y="-2.574376208962" Z="0.8" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.8" />
                  <Point X="2.296885765186" Y="-2.383321054278" Z="0.8" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.8" />
                  <Point X="2.449577324094" Y="-2.262015544781" Z="0.8" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.8" />
                  <Point X="2.65413059455" Y="-2.258925798072" Z="0.8" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.8" />
                  <Point X="3.14590432102" Y="-2.515805863152" Z="0.8" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.8" />
                  <Point X="4.184640529501" Y="-2.876683254438" Z="0.8" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.8" />
                  <Point X="4.348896898227" Y="-2.621758686907" Z="0.8" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.8" />
                  <Point X="4.018616103612" Y="-2.248307969555" Z="0.8" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.8" />
                  <Point X="3.123960091393" Y="-1.507606008013" Z="0.8" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.8" />
                  <Point X="3.103029592278" Y="-1.341294001797" Z="0.8" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.8" />
                  <Point X="3.183115594356" Y="-1.197021216493" Z="0.8" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.8" />
                  <Point X="3.342023460896" Y="-1.128369686754" Z="0.8" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.8" />
                  <Point X="3.874922029413" Y="-1.178537266246" Z="0.8" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.8" />
                  <Point X="4.964803527481" Y="-1.061140378552" Z="0.8" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.8" />
                  <Point X="5.030167386865" Y="-0.687541522291" Z="0.8" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.8" />
                  <Point X="4.637896602181" Y="-0.459270375917" Z="0.8" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.8" />
                  <Point X="3.684625973261" Y="-0.184206653915" Z="0.8" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.8" />
                  <Point X="3.617446678851" Y="-0.118922328818" Z="0.8" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.8" />
                  <Point X="3.587271378429" Y="-0.030476819516" Z="0.8" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.8" />
                  <Point X="3.594100071995" Y="0.066133711709" Z="0.8" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.8" />
                  <Point X="3.637932759549" Y="0.145026409757" Z="0.8" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.8" />
                  <Point X="3.718769441091" Y="0.203942190327" Z="0.8" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.8" />
                  <Point X="4.158071215125" Y="0.330701556851" Z="0.8" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.8" />
                  <Point X="5.002902444808" Y="0.858912629305" Z="0.8" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.8" />
                  <Point X="4.912750277234" Y="1.277361316826" Z="0.8" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.8" />
                  <Point X="4.433568263972" Y="1.349785874014" Z="0.8" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.8" />
                  <Point X="3.39866464319" Y="1.230542830637" Z="0.8" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.8" />
                  <Point X="3.321287882657" Y="1.261304200703" Z="0.8" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.8" />
                  <Point X="3.266421263028" Y="1.323673464248" Z="0.8" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.8" />
                  <Point X="3.239165895503" Y="1.405335427249" Z="0.8" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.8" />
                  <Point X="3.24832602616" Y="1.485034433335" Z="0.8" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.8" />
                  <Point X="3.294670158012" Y="1.560915237067" Z="0.8" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.8" />
                  <Point X="3.670761030836" Y="1.859292930868" Z="0.8" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.8" />
                  <Point X="4.304155596219" Y="2.691728596281" Z="0.8" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.8" />
                  <Point X="4.076685522253" Y="3.0251936075" Z="0.8" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.8" />
                  <Point X="3.531472997249" Y="2.856816926974" Z="0.8" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.8" />
                  <Point X="2.454918811659" Y="2.252301986253" Z="0.8" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.8" />
                  <Point X="2.382067600904" Y="2.251259695577" Z="0.8" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.8" />
                  <Point X="2.316829479953" Y="2.283306658084" Z="0.8" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.8" />
                  <Point X="2.26745194215" Y="2.34019538043" Z="0.8" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.8" />
                  <Point X="2.248170007182" Y="2.407690842928" Z="0.8" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.8" />
                  <Point X="2.260225956404" Y="2.484550764223" Z="0.8" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.8" />
                  <Point X="2.538808425886" Y="2.980666084706" Z="0.8" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.8" />
                  <Point X="2.871836191273" Y="4.1848763031" Z="0.8" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.8" />
                  <Point X="2.481232493129" Y="4.42765442242" Z="0.8" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.8" />
                  <Point X="2.073916313032" Y="4.632563540811" Z="0.8" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.8" />
                  <Point X="1.651531908916" Y="4.799398114016" Z="0.8" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.8" />
                  <Point X="1.068926359353" Y="4.956404446444" Z="0.8" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.8" />
                  <Point X="0.404386812742" Y="5.054210799554" Z="0.8" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.8" />
                  <Point X="0.132283509523" Y="4.848813277308" Z="0.8" />
                  <Point X="0" Y="4.355124473572" Z="0.8" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>