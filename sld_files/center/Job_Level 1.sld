<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#117" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="270" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="0" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="0.004717285156" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766446289062" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.563501525879" Y="-3.513269042969" />
                  <Point X="0.558157897949" Y="-3.498401855469" />
                  <Point X="0.54228137207" Y="-3.467258789062" />
                  <Point X="0.378635375977" Y="-3.2314765625" />
                  <Point X="0.356650665283" Y="-3.208947509766" />
                  <Point X="0.330268737793" Y="-3.1896640625" />
                  <Point X="0.302368438721" Y="-3.175629638672" />
                  <Point X="0.049136035919" Y="-3.097035888672" />
                  <Point X="0.020851108551" Y="-3.092766601562" />
                  <Point X="-0.008916964531" Y="-3.092805908203" />
                  <Point X="-0.03695098114" Y="-3.097075195312" />
                  <Point X="-0.290183227539" Y="-3.175669189453" />
                  <Point X="-0.318294036865" Y="-3.189857910156" />
                  <Point X="-0.34463104248" Y="-3.209219726562" />
                  <Point X="-0.366405395508" Y="-3.231594726562" />
                  <Point X="-0.530051208496" Y="-3.467377197266" />
                  <Point X="-0.538190856934" Y="-3.481577636719" />
                  <Point X="-0.550990844727" Y="-3.512527587891" />
                  <Point X="-0.916584655762" Y="-4.876941894531" />
                  <Point X="-1.079338378906" Y="-4.845351074219" />
                  <Point X="-1.24641809082" Y="-4.802362792969" />
                  <Point X="-1.214987182617" Y="-4.563621582031" />
                  <Point X="-1.214224365234" Y="-4.548141113281" />
                  <Point X="-1.21653894043" Y="-4.516072265625" />
                  <Point X="-1.277036010742" Y="-4.211932617188" />
                  <Point X="-1.28801184082" Y="-4.182838867188" />
                  <Point X="-1.304200561523" Y="-4.154898925781" />
                  <Point X="-1.323761474609" Y="-4.1311015625" />
                  <Point X="-1.556905517578" Y="-3.926639160156" />
                  <Point X="-1.583323730469" Y="-3.910239501953" />
                  <Point X="-1.61317578125" Y="-3.897928222656" />
                  <Point X="-1.643182250977" Y="-3.890956054688" />
                  <Point X="-1.952616699219" Y="-3.870674804688" />
                  <Point X="-1.983559814453" Y="-3.873746582031" />
                  <Point X="-2.014737304688" Y="-3.88215234375" />
                  <Point X="-2.042786865234" Y="-3.894887695312" />
                  <Point X="-2.300624267578" Y="-4.067169677734" />
                  <Point X="-2.312785644531" Y="-4.076821533203" />
                  <Point X="-2.335102783203" Y="-4.099461914062" />
                  <Point X="-2.480149169922" Y="-4.288490234375" />
                  <Point X="-2.801705810547" Y="-4.089390136719" />
                  <Point X="-3.104721923828" Y="-3.856077880859" />
                  <Point X="-2.424133544922" Y="-2.677264160156" />
                  <Point X="-2.422826171875" Y="-2.674950439453" />
                  <Point X="-2.412263916016" Y="-2.645848144531" />
                  <Point X="-2.406436523438" Y="-2.614618408203" />
                  <Point X="-2.405738525391" Y="-2.584047119141" />
                  <Point X="-2.4147890625" Y="-2.554837646484" />
                  <Point X="-2.428952880859" Y="-2.526400878906" />
                  <Point X="-2.446813476563" Y="-2.501580566406" />
                  <Point X="-2.464153320312" Y="-2.484240722656" />
                  <Point X="-2.489310302734" Y="-2.466213134766" />
                  <Point X="-2.518139160156" Y="-2.45199609375" />
                  <Point X="-2.547756835938" Y="-2.443011474609" />
                  <Point X="-2.578690917969" Y="-2.444024169922" />
                  <Point X="-2.610217285156" Y="-2.450295166016" />
                  <Point X="-2.63918359375" Y="-2.461197265625" />
                  <Point X="-3.818024414062" Y="-3.141801513672" />
                  <Point X="-4.082858398438" Y="-2.793864013672" />
                  <Point X="-4.306142578125" Y="-2.419450439453" />
                  <Point X="-3.106605712891" Y="-1.499013183594" />
                  <Point X="-3.105949462891" Y="-1.498509521484" />
                  <Point X="-3.084560546875" Y="-1.475572021484" />
                  <Point X="-3.066596923828" Y="-1.44842980957" />
                  <Point X="-3.053852539062" Y="-1.419817626953" />
                  <Point X="-3.046151855469" Y="-1.390085205078" />
                  <Point X="-3.0421171875" Y="-1.362409179688" />
                  <Point X="-3.042044433594" Y="-1.342280395508" />
                  <Point X="-3.046214355469" Y="-1.322587890625" />
                  <Point X="-3.054777832031" Y="-1.296290405273" />
                  <Point X="-3.064923095703" Y="-1.274762817383" />
                  <Point X="-3.080100097656" Y="-1.256432250977" />
                  <Point X="-3.096163330078" Y="-1.241357910156" />
                  <Point X="-3.112986083984" Y="-1.228759155273" />
                  <Point X="-3.139455322266" Y="-1.213180541992" />
                  <Point X="-3.16871875" Y="-1.201956542969" />
                  <Point X="-3.200607421875" Y="-1.195474853516" />
                  <Point X="-3.231930175781" Y="-1.194383911133" />
                  <Point X="-4.732102050781" Y="-1.391885131836" />
                  <Point X="-4.834076660156" Y="-0.992657775879" />
                  <Point X="-4.892424316406" Y="-0.584698303223" />
                  <Point X="-3.533619140625" Y="-0.220607467651" />
                  <Point X="-3.518858398438" Y="-0.215312255859" />
                  <Point X="-3.487714599609" Y="-0.199459716797" />
                  <Point X="-3.459975585938" Y="-0.180207229614" />
                  <Point X="-3.43751171875" Y="-0.15831161499" />
                  <Point X="-3.418263183594" Y="-0.132041549683" />
                  <Point X="-3.404163574219" Y="-0.104052314758" />
                  <Point X="-3.394917236328" Y="-0.074260261536" />
                  <Point X="-3.390916015625" Y="-0.053233642578" />
                  <Point X="-3.389235107422" Y="-0.030909221649" />
                  <Point X="-3.391390136719" Y="-0.002456564426" />
                  <Point X="-3.394933837891" Y="0.011753294945" />
                  <Point X="-3.404168457031" Y="0.04150725174" />
                  <Point X="-3.413899902344" Y="0.062988021851" />
                  <Point X="-3.426771484375" Y="0.08399080658" />
                  <Point X="-3.44740625" Y="0.107706687927" />
                  <Point X="-3.460044433594" Y="0.117694801331" />
                  <Point X="-3.487728759766" Y="0.136909347534" />
                  <Point X="-3.501921386719" Y="0.145045303345" />
                  <Point X="-3.532872558594" Y="0.157847457886" />
                  <Point X="-4.89181640625" Y="0.521975219727" />
                  <Point X="-4.824487792969" Y="0.976976074219" />
                  <Point X="-4.703551269531" Y="1.423267944336" />
                  <Point X="-3.766201171875" Y="1.29986340332" />
                  <Point X="-3.765669189453" Y="1.299793334961" />
                  <Point X="-3.744961914062" Y="1.299344482422" />
                  <Point X="-3.723366943359" Y="1.301240112305" />
                  <Point X="-3.703089111328" Y="1.305321655273" />
                  <Point X="-3.703076904297" Y="1.305282836914" />
                  <Point X="-3.641711914063" Y="1.324631103516" />
                  <Point X="-3.622846435547" Y="1.332922851562" />
                  <Point X="-3.604162109375" Y="1.343689819336" />
                  <Point X="-3.587527587891" Y="1.35585546875" />
                  <Point X="-3.573907958984" Y="1.371322021484" />
                  <Point X="-3.56149609375" Y="1.388956542969" />
                  <Point X="-3.551511474609" Y="1.407046508789" />
                  <Point X="-3.551338867188" Y="1.40746105957" />
                  <Point X="-3.526703857422" Y="1.466935668945" />
                  <Point X="-3.520912841797" Y="1.486809204102" />
                  <Point X="-3.517155273438" Y="1.508140014648" />
                  <Point X="-3.515806640625" Y="1.528795166016" />
                  <Point X="-3.518963623047" Y="1.549252319336" />
                  <Point X="-3.524579345703" Y="1.570170898438" />
                  <Point X="-3.532064453125" Y="1.589405639648" />
                  <Point X="-3.561789550781" Y="1.646507324219" />
                  <Point X="-3.573281005859" Y="1.663705810547" />
                  <Point X="-3.587193603516" Y="1.680286499023" />
                  <Point X="-3.602135986328" Y="1.694590576172" />
                  <Point X="-4.351860351563" Y="2.269874023438" />
                  <Point X="-4.081152587891" Y="2.733660888672" />
                  <Point X="-3.750505126953" Y="3.158661621094" />
                  <Point X="-3.206983642578" Y="2.844859375" />
                  <Point X="-3.206610107422" Y="2.844644042969" />
                  <Point X="-3.187620361328" Y="2.836304443359" />
                  <Point X="-3.166946777344" Y="2.829809082031" />
                  <Point X="-3.146762451172" Y="2.825803710938" />
                  <Point X="-3.061256591797" Y="2.818312744141" />
                  <Point X="-3.040660644531" Y="2.818750488281" />
                  <Point X="-3.019296142578" Y="2.821541259766" />
                  <Point X="-2.999288574219" Y="2.826405029297" />
                  <Point X="-2.980798828125" Y="2.835465576172" />
                  <Point X="-2.962592285156" Y="2.846987548828" />
                  <Point X="-2.946314697266" Y="2.859992675781" />
                  <Point X="-2.946046875" Y="2.860260009766" />
                  <Point X="-2.885353759766" Y="2.920953125" />
                  <Point X="-2.872435546875" Y="2.937039306641" />
                  <Point X="-2.860825439453" Y="2.955240722656" />
                  <Point X="-2.851684570312" Y="2.973737304688" />
                  <Point X="-2.846752441406" Y="2.993770996094" />
                  <Point X="-2.8438984375" Y="3.015170654297" />
                  <Point X="-2.84340625" Y="3.035779052734" />
                  <Point X="-2.843439453125" Y="3.0361640625" />
                  <Point X="-2.850920410156" Y="3.121670654297" />
                  <Point X="-2.854955810547" Y="3.141958496094" />
                  <Point X="-2.861464355469" Y="3.162600585938" />
                  <Point X="-2.869794921875" Y="3.181532958984" />
                  <Point X="-3.183333007812" Y="3.724596679688" />
                  <Point X="-2.700625732422" Y="4.09468359375" />
                  <Point X="-2.167036865234" Y="4.391134765625" />
                  <Point X="-2.043295776367" Y="4.229871582031" />
                  <Point X="-2.043160522461" Y="4.2296953125" />
                  <Point X="-2.028943237305" Y="4.214837890625" />
                  <Point X="-2.012459350586" Y="4.200982910156" />
                  <Point X="-1.995454467773" Y="4.189573242188" />
                  <Point X="-1.995065429688" Y="4.189370117188" />
                  <Point X="-1.899897094727" Y="4.139828125" />
                  <Point X="-1.880627075195" Y="4.132333007812" />
                  <Point X="-1.859729492188" Y="4.126731445312" />
                  <Point X="-1.839293579102" Y="4.123583496094" />
                  <Point X="-1.818660644531" Y="4.124931152344" />
                  <Point X="-1.797353027344" Y="4.128682617187" />
                  <Point X="-1.777540283203" Y="4.134446289062" />
                  <Point X="-1.777404052734" Y="4.134502441406" />
                  <Point X="-1.678279663086" Y="4.175561523437" />
                  <Point X="-1.660126586914" Y="4.1855234375" />
                  <Point X="-1.642381347656" Y="4.197955078125" />
                  <Point X="-1.626818481445" Y="4.21161328125" />
                  <Point X="-1.614584472656" Y="4.228318359375" />
                  <Point X="-1.603764282227" Y="4.247089355469" />
                  <Point X="-1.595475341797" Y="4.266024414062" />
                  <Point X="-1.595450561523" Y="4.266016601562" />
                  <Point X="-1.563201171875" Y="4.368297851562" />
                  <Point X="-1.559165649414" Y="4.388584960938" />
                  <Point X="-1.557279174805" Y="4.410146484375" />
                  <Point X="-1.55773034668" Y="4.430826660156" />
                  <Point X="-1.584202026367" Y="4.6318984375" />
                  <Point X="-0.949640563965" Y="4.809807617188" />
                  <Point X="-0.294710876465" Y="4.886457519531" />
                  <Point X="-0.133999847412" Y="4.286676269531" />
                  <Point X="-0.121227752686" Y="4.258486816406" />
                  <Point X="-0.103371124268" Y="4.231761230469" />
                  <Point X="-0.08221572876" Y="4.209172363281" />
                  <Point X="-0.054922359467" Y="4.19458203125" />
                  <Point X="-0.024486865997" Y="4.184248535156" />
                  <Point X="0.006047563553" Y="4.179205078125" />
                  <Point X="0.036582817078" Y="4.184243652344" />
                  <Point X="0.067020133972" Y="4.194572265625" />
                  <Point X="0.094315765381" Y="4.209158203125" />
                  <Point X="0.115474822998" Y="4.231743164063" />
                  <Point X="0.133335693359" Y="4.258465820312" />
                  <Point X="0.14611038208" Y="4.286645507813" />
                  <Point X="0.307388122559" Y="4.887940429688" />
                  <Point X="0.844044311523" Y="4.83173828125" />
                  <Point X="1.481028076172" Y="4.677950683594" />
                  <Point X="1.894645996094" Y="4.527927734375" />
                  <Point X="2.294576660156" Y="4.340893554688" />
                  <Point X="2.680977539062" Y="4.115775390625" />
                  <Point X="2.943260009766" Y="3.929254882812" />
                  <Point X="2.148009277344" Y="2.55183984375" />
                  <Point X="2.142772460938" Y="2.541314453125" />
                  <Point X="2.133058349609" Y="2.515987304688" />
                  <Point X="2.111607177734" Y="2.435770263672" />
                  <Point X="2.108616943359" Y="2.417901367188" />
                  <Point X="2.107731933594" Y="2.380918212891" />
                  <Point X="2.116099121094" Y="2.311528076172" />
                  <Point X="2.121423583984" Y="2.289654052734" />
                  <Point X="2.129658447266" Y="2.267611572266" />
                  <Point X="2.139987548828" Y="2.247594238281" />
                  <Point X="2.140092529297" Y="2.247439208984" />
                  <Point X="2.183028808594" Y="2.184162353516" />
                  <Point X="2.194489990234" Y="2.170303222656" />
                  <Point X="2.221630615234" Y="2.145570800781" />
                  <Point X="2.284907714844" Y="2.102634765625" />
                  <Point X="2.304682617188" Y="2.092373535156" />
                  <Point X="2.326469238281" Y="2.084145019531" />
                  <Point X="2.348336181641" Y="2.078740722656" />
                  <Point X="2.348998779297" Y="2.078658935547" />
                  <Point X="2.418388916016" Y="2.070291748047" />
                  <Point X="2.436506835938" Y="2.069848388672" />
                  <Point X="2.473246582031" Y="2.074181884766" />
                  <Point X="2.553492675781" Y="2.095640869141" />
                  <Point X="2.565283447266" Y="2.099638183594" />
                  <Point X="2.588533935547" Y="2.110145019531" />
                  <Point X="3.967326416016" Y="2.90619140625" />
                  <Point X="4.123271972656" Y="2.689462890625" />
                  <Point X="4.26219921875" Y="2.459884277344" />
                  <Point X="3.231351074219" Y="1.668886474609" />
                  <Point X="3.222416503906" Y="1.661098876953" />
                  <Point X="3.203944824219" Y="1.641590087891" />
                  <Point X="3.146191650391" Y="1.566246459961" />
                  <Point X="3.136582763672" Y="1.550865356445" />
                  <Point X="3.121615722656" Y="1.517034301758" />
                  <Point X="3.100106201172" Y="1.440121582031" />
                  <Point X="3.096654541016" Y="1.417878295898" />
                  <Point X="3.095826660156" Y="1.394364868164" />
                  <Point X="3.097724121094" Y="1.371842529297" />
                  <Point X="3.097748291016" Y="1.371725219727" />
                  <Point X="3.115408447266" Y="1.286135131836" />
                  <Point X="3.120695556641" Y="1.268940063477" />
                  <Point X="3.136306640625" Y="1.235703613281" />
                  <Point X="3.184340332031" Y="1.162694824219" />
                  <Point X="3.198788574219" Y="1.145548217773" />
                  <Point X="3.215900634766" Y="1.129532958984" />
                  <Point X="3.234068603516" Y="1.116191894531" />
                  <Point X="3.234382080078" Y="1.116015014648" />
                  <Point X="3.303989501953" Y="1.076832275391" />
                  <Point X="3.320563720703" Y="1.069487426758" />
                  <Point X="3.356165039062" Y="1.059432373047" />
                  <Point X="3.450278808594" Y="1.046994140625" />
                  <Point X="3.462702148438" Y="1.046175048828" />
                  <Point X="3.488203613281" Y="1.04698449707" />
                  <Point X="4.77683984375" Y="1.21663659668" />
                  <Point X="4.845936035156" Y="0.932809631348" />
                  <Point X="4.890864746094" Y="0.644238525391" />
                  <Point X="3.717231933594" Y="0.329764526367" />
                  <Point X="3.7059609375" Y="0.325973999023" />
                  <Point X="3.681499511719" Y="0.315041320801" />
                  <Point X="3.589035644531" Y="0.261595611572" />
                  <Point X="3.574278808594" Y="0.25106817627" />
                  <Point X="3.547503417969" Y="0.225541427612" />
                  <Point X="3.492025146484" Y="0.154848999023" />
                  <Point X="3.480311523438" Y="0.135592407227" />
                  <Point X="3.470542724609" Y="0.114155052185" />
                  <Point X="3.463708984375" Y="0.092750930786" />
                  <Point X="3.463671386719" Y="0.092555885315" />
                  <Point X="3.445178466797" Y="-0.004006192684" />
                  <Point X="3.443482910156" Y="-0.021922014236" />
                  <Point X="3.445187744141" Y="-0.058602169037" />
                  <Point X="3.463680664062" Y="-0.155164245605" />
                  <Point X="3.470470947266" Y="-0.176541793823" />
                  <Point X="3.480156494141" Y="-0.197892486572" />
                  <Point X="3.491805419922" Y="-0.21712828064" />
                  <Point X="3.492052734375" Y="-0.217444458008" />
                  <Point X="3.547531005859" Y="-0.288136749268" />
                  <Point X="3.560071777344" Y="-0.301298950195" />
                  <Point X="3.589123046875" Y="-0.324205932617" />
                  <Point X="3.681545654297" Y="-0.377627960205" />
                  <Point X="3.692706542969" Y="-0.383137359619" />
                  <Point X="3.716576171875" Y="-0.392148986816" />
                  <Point X="4.891472167969" Y="-0.706961425781" />
                  <Point X="4.855021972656" Y="-0.948728942871" />
                  <Point X="4.801173828125" Y="-1.184698974609" />
                  <Point X="3.425135253906" Y="-1.003539978027" />
                  <Point X="3.409373291016" Y="-1.002786804199" />
                  <Point X="3.374549560547" Y="-1.005532531738" />
                  <Point X="3.193094482422" Y="-1.044972412109" />
                  <Point X="3.163904785156" Y="-1.056639282227" />
                  <Point X="3.136023193359" Y="-1.07359765625" />
                  <Point X="3.112342529297" Y="-1.094026123047" />
                  <Point X="3.002653320312" Y="-1.225948120117" />
                  <Point X="2.987888427734" Y="-1.2504375" />
                  <Point X="2.976534179688" Y="-1.277943969727" />
                  <Point X="2.969746582031" Y="-1.305486572266" />
                  <Point X="2.954028564453" Y="-1.476295654297" />
                  <Point X="2.956368164062" Y="-1.507649780273" />
                  <Point X="2.964149902344" Y="-1.539348754883" />
                  <Point X="2.976500488281" Y="-1.568074707031" />
                  <Point X="3.076930419922" Y="-1.724287109375" />
                  <Point X="3.086932617188" Y="-1.737238647461" />
                  <Point X="3.110628417969" Y="-1.760909301758" />
                  <Point X="4.213122558594" Y="-2.606883056641" />
                  <Point X="4.124811523438" Y="-2.749783447266" />
                  <Point X="4.028981445312" Y="-2.885945068359" />
                  <Point X="2.801629394531" Y="-2.177332763672" />
                  <Point X="2.787374023438" Y="-2.170612060547" />
                  <Point X="2.754116455078" Y="-2.159805664062" />
                  <Point X="2.538134277344" Y="-2.120799560547" />
                  <Point X="2.5066875" Y="-2.120409912109" />
                  <Point X="2.474424804688" Y="-2.125414794922" />
                  <Point X="2.444743408203" Y="-2.135223876953" />
                  <Point X="2.265315185547" Y="-2.229655761719" />
                  <Point X="2.242316162109" Y="-2.246616943359" />
                  <Point X="2.221309082031" Y="-2.267666748047" />
                  <Point X="2.204484375" Y="-2.290529296875" />
                  <Point X="2.110052734375" Y="-2.469957519531" />
                  <Point X="2.100213378906" Y="-2.499835449219" />
                  <Point X="2.095274902344" Y="-2.532115966797" />
                  <Point X="2.095694824219" Y="-2.563366210938" />
                  <Point X="2.134701171875" Y="-2.779348632812" />
                  <Point X="2.138985595703" Y="-2.795141845703" />
                  <Point X="2.151819091797" Y="-2.826078613281" />
                  <Point X="2.861283691406" Y="-4.05490625" />
                  <Point X="2.781831542969" Y="-4.111657226563" />
                  <Point X="2.701765625" Y="-4.163481933594" />
                  <Point X="1.759059448242" Y="-2.934924072266" />
                  <Point X="1.748489990234" Y="-2.923286376953" />
                  <Point X="1.721817626953" Y="-2.900488769531" />
                  <Point X="1.50880078125" Y="-2.763538574219" />
                  <Point X="1.479876464844" Y="-2.751139648438" />
                  <Point X="1.448132568359" Y="-2.743419189453" />
                  <Point X="1.41697668457" Y="-2.741127929688" />
                  <Point X="1.184012695312" Y="-2.762565673828" />
                  <Point X="1.156261108398" Y="-2.769439697266" />
                  <Point X="1.128786010742" Y="-2.780857666016" />
                  <Point X="1.104505859375" Y="-2.795535888672" />
                  <Point X="0.924612121582" Y="-2.945111572266" />
                  <Point X="0.904078979492" Y="-2.968964355469" />
                  <Point X="0.887159790039" Y="-2.996914794922" />
                  <Point X="0.875597473145" Y="-3.025932617188" />
                  <Point X="0.821810058594" Y="-3.273396728516" />
                  <Point X="0.819724609375" Y="-3.289627441406" />
                  <Point X="0.819742370605" Y="-3.323120117188" />
                  <Point X="1.022065246582" Y="-4.859915527344" />
                  <Point X="0.975671691895" Y="-4.870084472656" />
                  <Point X="0.929315612793" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058431762695" Y="-4.752636230469" />
                  <Point X="-1.141246459961" Y="-4.731328613281" />
                  <Point X="-1.120799926758" Y="-4.576021484375" />
                  <Point X="-1.120102294922" Y="-4.568297363281" />
                  <Point X="-1.119470825195" Y="-4.541302246094" />
                  <Point X="-1.121785400391" Y="-4.509233398438" />
                  <Point X="-1.123364379883" Y="-4.497538574219" />
                  <Point X="-1.183861450195" Y="-4.193398925781" />
                  <Point X="-1.188150878906" Y="-4.178399902344" />
                  <Point X="-1.199126708984" Y="-4.149306152344" />
                  <Point X="-1.205812866211" Y="-4.135211914063" />
                  <Point X="-1.222001586914" Y="-4.107271972656" />
                  <Point X="-1.230811279297" Y="-4.094574707031" />
                  <Point X="-1.250372314453" Y="-4.07077734375" />
                  <Point X="-1.261123535156" Y="-4.059676757813" />
                  <Point X="-1.494267578125" Y="-3.855214355469" />
                  <Point X="-1.506801269531" Y="-3.845926269531" />
                  <Point X="-1.533219604492" Y="-3.829526611328" />
                  <Point X="-1.547104003906" Y="-3.822415039062" />
                  <Point X="-1.576956054688" Y="-3.810103759766" />
                  <Point X="-1.591674926758" Y="-3.805393310547" />
                  <Point X="-1.621681274414" Y="-3.798421142578" />
                  <Point X="-1.636968994141" Y="-3.796159423828" />
                  <Point X="-1.946403442383" Y="-3.775878173828" />
                  <Point X="-1.962001342773" Y="-3.776139404297" />
                  <Point X="-1.992944458008" Y="-3.779211181641" />
                  <Point X="-2.008289672852" Y="-3.782021728516" />
                  <Point X="-2.039467163086" Y="-3.790427490234" />
                  <Point X="-2.05401171875" Y="-3.795650634766" />
                  <Point X="-2.082061279297" Y="-3.808385986328" />
                  <Point X="-2.095566162109" Y="-3.815898193359" />
                  <Point X="-2.353403564453" Y="-3.988180175781" />
                  <Point X="-2.359681640625" Y="-3.992757080078" />
                  <Point X="-2.380442138672" Y="-4.010131103516" />
                  <Point X="-2.402759277344" Y="-4.032771484375" />
                  <Point X="-2.410471435547" Y="-4.041629638672" />
                  <Point X="-2.503202880859" Y="-4.162479492188" />
                  <Point X="-2.747581298828" Y="-4.011166503906" />
                  <Point X="-2.980862792969" Y="-3.831547607422" />
                  <Point X="-2.341861083984" Y="-2.724764160156" />
                  <Point X="-2.333525634766" Y="-2.707360839844" />
                  <Point X="-2.322963378906" Y="-2.678258544922" />
                  <Point X="-2.318875732422" Y="-2.663274169922" />
                  <Point X="-2.313048339844" Y="-2.632044433594" />
                  <Point X="-2.311461181641" Y="-2.616786865234" />
                  <Point X="-2.310763183594" Y="-2.586215576172" />
                  <Point X="-2.314994628906" Y="-2.555930175781" />
                  <Point X="-2.324045166016" Y="-2.526720703125" />
                  <Point X="-2.329753417969" Y="-2.512482910156" />
                  <Point X="-2.343917236328" Y="-2.484046142578" />
                  <Point X="-2.351842285156" Y="-2.470912353516" />
                  <Point X="-2.369702880859" Y="-2.446092041016" />
                  <Point X="-2.379638427734" Y="-2.434405517578" />
                  <Point X="-2.396978271484" Y="-2.417065673828" />
                  <Point X="-2.408817138672" Y="-2.407020751953" />
                  <Point X="-2.433974121094" Y="-2.388993164062" />
                  <Point X="-2.447292236328" Y="-2.381010498047" />
                  <Point X="-2.47612109375" Y="-2.366793457031" />
                  <Point X="-2.490561523438" Y="-2.361086914062" />
                  <Point X="-2.520179199219" Y="-2.352102294922" />
                  <Point X="-2.550865234375" Y="-2.348062255859" />
                  <Point X="-2.581799316406" Y="-2.349074951172" />
                  <Point X="-2.597224609375" Y="-2.350849609375" />
                  <Point X="-2.628750976562" Y="-2.357120605469" />
                  <Point X="-2.643680908203" Y="-2.361384033203" />
                  <Point X="-2.672647216797" Y="-2.372286132812" />
                  <Point X="-2.68668359375" Y="-2.378924804688" />
                  <Point X="-3.793089599609" Y="-3.017708740234" />
                  <Point X="-4.004013183594" Y="-2.740598388672" />
                  <Point X="-4.181265136719" Y="-2.443373779297" />
                  <Point X="-3.0487734375" Y="-1.574381713867" />
                  <Point X="-3.036469970703" Y="-1.563298339844" />
                  <Point X="-3.015081054688" Y="-1.540360839844" />
                  <Point X="-3.005339599609" Y="-1.528003295898" />
                  <Point X="-2.987375976562" Y="-1.500860961914" />
                  <Point X="-2.979816162109" Y="-1.487083496094" />
                  <Point X="-2.967071777344" Y="-1.458471313477" />
                  <Point X="-2.961886962891" Y="-1.44363659668" />
                  <Point X="-2.954186279297" Y="-1.413904052734" />
                  <Point X="-2.952145507812" Y="-1.403789672852" />
                  <Point X="-2.948110839844" Y="-1.376113647461" />
                  <Point X="-2.947117919922" Y="-1.362752563477" />
                  <Point X="-2.947045166016" Y="-1.342623779297" />
                  <Point X="-2.949105224609" Y="-1.322600341797" />
                  <Point X="-2.953275146484" Y="-1.302907714844" />
                  <Point X="-2.955883056641" Y="-1.293172485352" />
                  <Point X="-2.964446533203" Y="-1.266875" />
                  <Point X="-2.968842529297" Y="-1.255791870117" />
                  <Point X="-2.978987792969" Y="-1.234264404297" />
                  <Point X="-2.991749023438" Y="-1.214177612305" />
                  <Point X="-3.006926025391" Y="-1.195847167969" />
                  <Point X="-3.015091064453" Y="-1.187158813477" />
                  <Point X="-3.031154296875" Y="-1.172084350586" />
                  <Point X="-3.039216308594" Y="-1.165318237305" />
                  <Point X="-3.0560390625" Y="-1.152719360352" />
                  <Point X="-3.064799804688" Y="-1.14688684082" />
                  <Point X="-3.091269042969" Y="-1.131308227539" />
                  <Point X="-3.105434570312" Y="-1.124481079102" />
                  <Point X="-3.134697998047" Y="-1.113257080078" />
                  <Point X="-3.149795898438" Y="-1.108860229492" />
                  <Point X="-3.181684570312" Y="-1.102378662109" />
                  <Point X="-3.19730078125" Y="-1.100532470703" />
                  <Point X="-3.228623535156" Y="-1.09944152832" />
                  <Point X="-3.244330078125" Y="-1.100196655273" />
                  <Point X="-4.660920410156" Y="-1.286694213867" />
                  <Point X="-4.740761230469" Y="-0.974119995117" />
                  <Point X="-4.786452148438" Y="-0.654654418945" />
                  <Point X="-3.50903125" Y="-0.312370452881" />
                  <Point X="-3.501540771484" Y="-0.310027709961" />
                  <Point X="-3.475763916016" Y="-0.299975463867" />
                  <Point X="-3.444620117188" Y="-0.284122894287" />
                  <Point X="-3.433547363281" Y="-0.277503967285" />
                  <Point X="-3.405808349609" Y="-0.258251525879" />
                  <Point X="-3.393666503906" Y="-0.248237304688" />
                  <Point X="-3.371202636719" Y="-0.226341720581" />
                  <Point X="-3.360880859375" Y="-0.214460510254" />
                  <Point X="-3.341632324219" Y="-0.188190383911" />
                  <Point X="-3.333420166016" Y="-0.174781326294" />
                  <Point X="-3.319320556641" Y="-0.146792068481" />
                  <Point X="-3.313432861328" Y="-0.132211715698" />
                  <Point X="-3.304186523438" Y="-0.102419654846" />
                  <Point X="-3.301592041016" Y="-0.092019470215" />
                  <Point X="-3.297590820312" Y="-0.070992752075" />
                  <Point X="-3.296184082031" Y="-0.060366508484" />
                  <Point X="-3.294503173828" Y="-0.038042011261" />
                  <Point X="-3.294506347656" Y="-0.023734382629" />
                  <Point X="-3.296661376953" Y="0.004718283653" />
                  <Point X="-3.299213134766" Y="0.02053086853" />
                  <Point X="-3.304203369141" Y="0.039912929535" />
                  <Point X="-3.313437988281" Y="0.069666938782" />
                  <Point X="-3.317634277344" Y="0.080709915161" />
                  <Point X="-3.327365722656" Y="0.102190681458" />
                  <Point X="-3.332900878906" Y="0.112628318787" />
                  <Point X="-3.345772460938" Y="0.133631118774" />
                  <Point X="-3.355102294922" Y="0.146348922729" />
                  <Point X="-3.375737060547" Y="0.170064834595" />
                  <Point X="-3.388501464844" Y="0.182240158081" />
                  <Point X="-3.405877197266" Y="0.195738983154" />
                  <Point X="-3.433561523438" Y="0.214953552246" />
                  <Point X="-3.440482177734" Y="0.219327667236" />
                  <Point X="-3.465610595703" Y="0.232832138062" />
                  <Point X="-3.496561767578" Y="0.245634216309" />
                  <Point X="-3.508284667969" Y="0.249610473633" />
                  <Point X="-4.785446289062" Y="0.591824707031" />
                  <Point X="-4.731330566406" Y="0.957533569336" />
                  <Point X="-4.633586425781" Y="1.318237304688" />
                  <Point X="-3.778601074219" Y="1.205676147461" />
                  <Point X="-3.767728027344" Y="1.204815673828" />
                  <Point X="-3.747020751953" Y="1.204366821289" />
                  <Point X="-3.736654541016" Y="1.204708374023" />
                  <Point X="-3.715059570312" Y="1.206604003906" />
                  <Point X="-3.70462109375" Y="1.208107910156" />
                  <Point X="-3.684513671875" Y="1.212155151367" />
                  <Point X="-3.674510009766" Y="1.21467956543" />
                  <Point X="-3.613145019531" Y="1.234027954102" />
                  <Point X="-3.603486816406" Y="1.237660766602" />
                  <Point X="-3.584621337891" Y="1.245952514648" />
                  <Point X="-3.5754140625" Y="1.250611450195" />
                  <Point X="-3.556729736328" Y="1.261378417969" />
                  <Point X="-3.548081542969" Y="1.267008789062" />
                  <Point X="-3.531447021484" Y="1.279174560547" />
                  <Point X="-3.51623046875" Y="1.293072265625" />
                  <Point X="-3.502610839844" Y="1.308538818359" />
                  <Point X="-3.496221191406" Y="1.316643066406" />
                  <Point X="-3.483809326172" Y="1.334277709961" />
                  <Point X="-3.478323974609" Y="1.343050292969" />
                  <Point X="-3.468339355469" Y="1.361140258789" />
                  <Point X="-3.4635703125" Y="1.371106201172" />
                  <Point X="-3.438935302734" Y="1.430580932617" />
                  <Point X="-3.435497070312" Y="1.440358642578" />
                  <Point X="-3.429706054688" Y="1.460232177734" />
                  <Point X="-3.427353271484" Y="1.470328125" />
                  <Point X="-3.423595703125" Y="1.491658935547" />
                  <Point X="-3.422357177734" Y="1.501950317383" />
                  <Point X="-3.421008544922" Y="1.52260546875" />
                  <Point X="-3.42191796875" Y="1.543284057617" />
                  <Point X="-3.425074951172" Y="1.563741333008" />
                  <Point X="-3.427212402344" Y="1.573883422852" />
                  <Point X="-3.432828125" Y="1.594802124023" />
                  <Point X="-3.436046630859" Y="1.604623046875" />
                  <Point X="-3.443531738281" Y="1.623857788086" />
                  <Point X="-3.447798339844" Y="1.633271606445" />
                  <Point X="-3.4775234375" Y="1.690373168945" />
                  <Point X="-3.482799560547" Y="1.699285888672" />
                  <Point X="-3.494291015625" Y="1.716484375" />
                  <Point X="-3.500506347656" Y="1.724770019531" />
                  <Point X="-3.514418945312" Y="1.741350708008" />
                  <Point X="-3.521500244141" Y="1.748911376953" />
                  <Point X="-3.536442626953" Y="1.763215332031" />
                  <Point X="-3.544303710938" Y="1.769959106445" />
                  <Point X="-4.227614746094" Y="2.294281982422" />
                  <Point X="-4.002291992188" Y="2.680313720703" />
                  <Point X="-3.726338623047" Y="3.035012695312" />
                  <Point X="-3.254483642578" Y="2.762586914062" />
                  <Point X="-3.244809326172" Y="2.757662353516" />
                  <Point X="-3.225819580078" Y="2.749322753906" />
                  <Point X="-3.216095703125" Y="2.745672363281" />
                  <Point X="-3.195422119141" Y="2.739177001953" />
                  <Point X="-3.185437988281" Y="2.736625976562" />
                  <Point X="-3.165253662109" Y="2.732620605469" />
                  <Point X="-3.155053466797" Y="2.731166259766" />
                  <Point X="-3.069547607422" Y="2.723675292969" />
                  <Point X="-3.059238037109" Y="2.723334228516" />
                  <Point X="-3.038642089844" Y="2.723771972656" />
                  <Point X="-3.028355712891" Y="2.72455078125" />
                  <Point X="-3.006991210938" Y="2.727341552734" />
                  <Point X="-2.99685546875" Y="2.729229736328" />
                  <Point X="-2.976847900391" Y="2.734093505859" />
                  <Point X="-2.957485107422" Y="2.741096923828" />
                  <Point X="-2.938995361328" Y="2.750157470703" />
                  <Point X="-2.929996582031" Y="2.755190185547" />
                  <Point X="-2.911790039062" Y="2.766712158203" />
                  <Point X="-2.903293457031" Y="2.772767333984" />
                  <Point X="-2.887015869141" Y="2.785772460938" />
                  <Point X="-2.878871826172" Y="2.793084960938" />
                  <Point X="-2.818178710938" Y="2.853778076172" />
                  <Point X="-2.811281982422" Y="2.86146875" />
                  <Point X="-2.798363769531" Y="2.877554931641" />
                  <Point X="-2.792342285156" Y="2.885950439453" />
                  <Point X="-2.780732177734" Y="2.904151855469" />
                  <Point X="-2.775657958984" Y="2.913151611328" />
                  <Point X="-2.766517089844" Y="2.931648193359" />
                  <Point X="-2.759438964844" Y="2.951027099609" />
                  <Point X="-2.754506835938" Y="2.971060791016" />
                  <Point X="-2.752586181641" Y="2.981212402344" />
                  <Point X="-2.749732177734" Y="3.002612060547" />
                  <Point X="-2.748925537109" Y="3.01290234375" />
                  <Point X="-2.748433349609" Y="3.033510742188" />
                  <Point X="-2.748801025391" Y="3.044443847656" />
                  <Point X="-2.756281982422" Y="3.129950439453" />
                  <Point X="-2.757745605469" Y="3.140203857422" />
                  <Point X="-2.761781005859" Y="3.160491699219" />
                  <Point X="-2.764352783203" Y="3.170526123047" />
                  <Point X="-2.770861328125" Y="3.191168212891" />
                  <Point X="-2.774510009766" Y="3.200862060547" />
                  <Point X="-2.782840576172" Y="3.219794433594" />
                  <Point X="-2.787522460938" Y="3.229032958984" />
                  <Point X="-3.059387207031" Y="3.699916503906" />
                  <Point X="-2.648374511719" Y="4.015036132812" />
                  <Point X="-2.192525146484" Y="4.268296875" />
                  <Point X="-2.118664550781" Y="4.172039550781" />
                  <Point X="-2.111798095703" Y="4.164015136719" />
                  <Point X="-2.097580810547" Y="4.149157714844" />
                  <Point X="-2.090068603516" Y="4.142114257812" />
                  <Point X="-2.073584716797" Y="4.128259277344" />
                  <Point X="-2.065390380859" Y="4.122094726562" />
                  <Point X="-2.048385498047" Y="4.110685058594" />
                  <Point X="-2.038931884766" Y="4.105104492188" />
                  <Point X="-1.943763549805" Y="4.055562255859" />
                  <Point X="-1.934334350586" Y="4.051289550781" />
                  <Point X="-1.915064331055" Y="4.043794433594" />
                  <Point X="-1.905223388672" Y="4.040572265625" />
                  <Point X="-1.884325805664" Y="4.034970703125" />
                  <Point X="-1.874192749023" Y="4.032838867188" />
                  <Point X="-1.853756835938" Y="4.029690917969" />
                  <Point X="-1.833101806641" Y="4.028785400391" />
                  <Point X="-1.81246887207" Y="4.030133056641" />
                  <Point X="-1.802188110352" Y="4.031370117188" />
                  <Point X="-1.780880493164" Y="4.035121582031" />
                  <Point X="-1.770816894531" Y="4.037464111328" />
                  <Point X="-1.751004150391" Y="4.043227783203" />
                  <Point X="-1.741048706055" Y="4.046734130859" />
                  <Point X="-1.641924438477" Y="4.087793212891" />
                  <Point X="-1.632575927734" Y="4.092277832031" />
                  <Point X="-1.614422851562" Y="4.102239746094" />
                  <Point X="-1.605618286133" Y="4.107716796875" />
                  <Point X="-1.587873046875" Y="4.1201484375" />
                  <Point X="-1.579717773438" Y="4.126552734375" />
                  <Point X="-1.564154907227" Y="4.1402109375" />
                  <Point X="-1.550174194336" Y="4.155482421875" />
                  <Point X="-1.537940063477" Y="4.1721875" />
                  <Point X="-1.532279418945" Y="4.180875" />
                  <Point X="-1.521459228516" Y="4.199645996094" />
                  <Point X="-1.516737548828" Y="4.208992675781" />
                  <Point X="-1.508554931641" Y="4.227684570313" />
                  <Point X="-1.50484753418" Y="4.23744921875" />
                  <Point X="-1.472598144531" Y="4.33973046875" />
                  <Point X="-1.470026733398" Y="4.349763671875" />
                  <Point X="-1.465991210938" Y="4.37005078125" />
                  <Point X="-1.46452722168" Y="4.3803046875" />
                  <Point X="-1.46264074707" Y="4.401866210938" />
                  <Point X="-1.462301757812" Y="4.41221875" />
                  <Point X="-1.462752929688" Y="4.432898925781" />
                  <Point X="-1.46354309082" Y="4.4432265625" />
                  <Point X="-1.479266235352" Y="4.562655761719" />
                  <Point X="-0.931183898926" Y="4.716319335938" />
                  <Point X="-0.365221984863" Y="4.782556640625" />
                  <Point X="-0.225762786865" Y="4.262088378906" />
                  <Point X="-0.220532302856" Y="4.247470214844" />
                  <Point X="-0.207760238647" Y="4.219280761719" />
                  <Point X="-0.200218521118" Y="4.205709472656" />
                  <Point X="-0.182361923218" Y="4.178983886719" />
                  <Point X="-0.172710357666" Y="4.166822265625" />
                  <Point X="-0.151554931641" Y="4.144233398437" />
                  <Point X="-0.127002571106" Y="4.125392089844" />
                  <Point X="-0.09970916748" Y="4.110801757812" />
                  <Point X="-0.085464553833" Y="4.104625488281" />
                  <Point X="-0.055028961182" Y="4.094291992188" />
                  <Point X="-0.03996855545" Y="4.090518554688" />
                  <Point X="-0.009434128761" Y="4.085475097656" />
                  <Point X="0.021514213562" Y="4.08547265625" />
                  <Point X="0.052049530029" Y="4.090511230469" />
                  <Point X="0.06711038208" Y="4.094282226562" />
                  <Point X="0.09754776001" Y="4.104610839844" />
                  <Point X="0.111793556213" Y="4.110784667969" />
                  <Point X="0.139089202881" Y="4.125370605469" />
                  <Point X="0.163643783569" Y="4.14420703125" />
                  <Point X="0.184802780151" Y="4.166791992188" />
                  <Point X="0.194457168579" Y="4.178953125" />
                  <Point X="0.212317932129" Y="4.20567578125" />
                  <Point X="0.219860092163" Y="4.219241699219" />
                  <Point X="0.23263482666" Y="4.247421386719" />
                  <Point X="0.237867248535" Y="4.262034667969" />
                  <Point X="0.378138641357" Y="4.785011230469" />
                  <Point X="0.827879638672" Y="4.737911621094" />
                  <Point X="1.453598632813" Y="4.586843261719" />
                  <Point X="1.85825793457" Y="4.440069824219" />
                  <Point X="2.250453369141" Y="4.256653320312" />
                  <Point X="2.629432861328" Y="4.035858398438" />
                  <Point X="2.817779785156" Y="3.901916748047" />
                  <Point X="2.065736816406" Y="2.59933984375" />
                  <Point X="2.062955322266" Y="2.594157714844" />
                  <Point X="2.054072753906" Y="2.575334716797" />
                  <Point X="2.044358642578" Y="2.550007568359" />
                  <Point X="2.041283081055" Y="2.540529296875" />
                  <Point X="2.01983190918" Y="2.460312255859" />
                  <Point X="2.01791003418" Y="2.451449707031" />
                  <Point X="2.013644165039" Y="2.420174072266" />
                  <Point X="2.012759033203" Y="2.383190917969" />
                  <Point X="2.013415161133" Y="2.369545410156" />
                  <Point X="2.021782226562" Y="2.300155273438" />
                  <Point X="2.023794311523" Y="2.289059814453" />
                  <Point X="2.029118774414" Y="2.267185791016" />
                  <Point X="2.032431152344" Y="2.256407226562" />
                  <Point X="2.040666015625" Y="2.234364746094" />
                  <Point X="2.045235473633" Y="2.224048583984" />
                  <Point X="2.055564453125" Y="2.20403125" />
                  <Point X="2.061481445312" Y="2.194097900391" />
                  <Point X="2.104417724609" Y="2.130821044922" />
                  <Point X="2.109819335938" Y="2.123619873047" />
                  <Point X="2.130502441406" Y="2.100085205078" />
                  <Point X="2.157643066406" Y="2.075352783203" />
                  <Point X="2.168289794922" Y="2.066959472656" />
                  <Point X="2.231566894531" Y="2.0240234375" />
                  <Point X="2.241152099609" Y="2.018311279297" />
                  <Point X="2.260927001953" Y="2.008050048828" />
                  <Point X="2.271116699219" Y="2.003500976562" />
                  <Point X="2.292903320312" Y="1.995272460938" />
                  <Point X="2.303676269531" Y="1.991919921875" />
                  <Point X="2.325543212891" Y="1.98651550293" />
                  <Point X="2.337625976563" Y="1.984342163086" />
                  <Point X="2.407016113281" Y="1.975974975586" />
                  <Point X="2.416064941406" Y="1.97532019043" />
                  <Point X="2.447635009766" Y="1.975502441406" />
                  <Point X="2.484374755859" Y="1.9798359375" />
                  <Point X="2.497788574219" Y="1.982406616211" />
                  <Point X="2.578034667969" Y="2.003865722656" />
                  <Point X="2.583994384766" Y="2.005670654297" />
                  <Point X="2.604404541016" Y="2.013067260742" />
                  <Point X="2.627655029297" Y="2.023573974609" />
                  <Point X="2.636033935547" Y="2.027872436523" />
                  <Point X="3.940404785156" Y="2.780951660156" />
                  <Point X="4.043954101562" Y="2.637041992188" />
                  <Point X="4.136885253906" Y="2.483472412109" />
                  <Point X="3.173518798828" Y="1.744255004883" />
                  <Point X="3.168930175781" Y="1.740500854492" />
                  <Point X="3.153432861328" Y="1.726415405273" />
                  <Point X="3.134961181641" Y="1.706906616211" />
                  <Point X="3.128547363281" Y="1.699384643555" />
                  <Point X="3.070794189453" Y="1.624041015625" />
                  <Point X="3.065621582031" Y="1.616580200195" />
                  <Point X="3.049705078125" Y="1.589300537109" />
                  <Point X="3.034738037109" Y="1.555469604492" />
                  <Point X="3.030126220703" Y="1.542620361328" />
                  <Point X="3.008616699219" Y="1.465707763672" />
                  <Point X="3.006229736328" Y="1.454689086914" />
                  <Point X="3.002778076172" Y="1.432445800781" />
                  <Point X="3.001713378906" Y="1.421221069336" />
                  <Point X="3.000885498047" Y="1.397707641602" />
                  <Point X="3.001162109375" Y="1.386389648438" />
                  <Point X="3.003059570313" Y="1.3638671875" />
                  <Point X="3.004708251953" Y="1.352527832031" />
                  <Point X="3.022368408203" Y="1.266937866211" />
                  <Point X="3.024604003906" Y="1.25821472168" />
                  <Point X="3.034708251953" Y="1.228551879883" />
                  <Point X="3.050319335938" Y="1.195315551758" />
                  <Point X="3.056942871094" Y="1.183488891602" />
                  <Point X="3.1049765625" Y="1.110480102539" />
                  <Point X="3.111692626953" Y="1.101479614258" />
                  <Point X="3.126140869141" Y="1.084333007812" />
                  <Point X="3.133873046875" Y="1.076187011719" />
                  <Point X="3.150985107422" Y="1.06017175293" />
                  <Point X="3.159672119141" Y="1.052960449219" />
                  <Point X="3.177840087891" Y="1.039619384766" />
                  <Point X="3.187781494141" Y="1.033229980469" />
                  <Point X="3.257388916016" Y="0.994047119141" />
                  <Point X="3.265500244141" Y="0.98997845459" />
                  <Point X="3.294742431641" Y="0.978063903809" />
                  <Point X="3.33034375" Y="0.968008850098" />
                  <Point X="3.343718017578" Y="0.965251281738" />
                  <Point X="3.437831787109" Y="0.952813049316" />
                  <Point X="3.444028808594" Y="0.952199951172" />
                  <Point X="3.465716064453" Y="0.95122277832" />
                  <Point X="3.491217529297" Y="0.952032348633" />
                  <Point X="3.500603515625" Y="0.952797302246" />
                  <Point X="4.704704101562" Y="1.111319946289" />
                  <Point X="4.75268359375" Y="0.91423449707" />
                  <Point X="4.78387109375" Y="0.713920959473" />
                  <Point X="3.692644042969" Y="0.421527496338" />
                  <Point X="3.68694921875" Y="0.419808807373" />
                  <Point X="3.667197509766" Y="0.412705688477" />
                  <Point X="3.642736083984" Y="0.401772979736" />
                  <Point X="3.633958496094" Y="0.397289916992" />
                  <Point X="3.541494628906" Y="0.343844238281" />
                  <Point X="3.533863769531" Y="0.338932830811" />
                  <Point X="3.508726074219" Y="0.319827514648" />
                  <Point X="3.481950683594" Y="0.294300628662" />
                  <Point X="3.472769287109" Y="0.284191467285" />
                  <Point X="3.417291015625" Y="0.213498977661" />
                  <Point X="3.410861816406" Y="0.204220001221" />
                  <Point X="3.399148193359" Y="0.184963378906" />
                  <Point X="3.393864013672" Y="0.174985733032" />
                  <Point X="3.384095214844" Y="0.153548370361" />
                  <Point X="3.380043212891" Y="0.143049041748" />
                  <Point X="3.373209472656" Y="0.121644821167" />
                  <Point X="3.370366943359" Y="0.110424835205" />
                  <Point X="3.351874023438" Y="0.013862752914" />
                  <Point X="3.350601074219" Y="0.004944636345" />
                  <Point X="3.348585449219" Y="-0.026332756042" />
                  <Point X="3.350290283203" Y="-0.063012889862" />
                  <Point X="3.351883300781" Y="-0.076471138" />
                  <Point X="3.370376220703" Y="-0.173033218384" />
                  <Point X="3.373138427734" Y="-0.183923706055" />
                  <Point X="3.379928710938" Y="-0.205301330566" />
                  <Point X="3.383956787109" Y="-0.215788162231" />
                  <Point X="3.393642333984" Y="-0.237138885498" />
                  <Point X="3.398895507812" Y="-0.247103012085" />
                  <Point X="3.410544433594" Y="-0.266338684082" />
                  <Point X="3.417318847656" Y="-0.276094573975" />
                  <Point X="3.472797119141" Y="-0.34678692627" />
                  <Point X="3.478751953125" Y="-0.353668609619" />
                  <Point X="3.501250244141" Y="-0.375897979736" />
                  <Point X="3.530301513672" Y="-0.398805084229" />
                  <Point X="3.541581787109" Y="-0.406454559326" />
                  <Point X="3.634004394531" Y="-0.459876617432" />
                  <Point X="3.639494628906" Y="-0.462814300537" />
                  <Point X="3.65915234375" Y="-0.472014343262" />
                  <Point X="3.683021972656" Y="-0.481025939941" />
                  <Point X="3.69198828125" Y="-0.483911895752" />
                  <Point X="4.784876464844" Y="-0.776750488281" />
                  <Point X="4.761612792969" Y="-0.931054931641" />
                  <Point X="4.727801757812" Y="-1.079219604492" />
                  <Point X="3.43753515625" Y="-0.909352661133" />
                  <Point X="3.429669677734" Y="-0.908648193359" />
                  <Point X="3.401906005859" Y="-0.908080749512" />
                  <Point X="3.367082275391" Y="-0.910826416016" />
                  <Point X="3.354372070312" Y="-0.912700134277" />
                  <Point X="3.172916992188" Y="-0.952139892578" />
                  <Point X="3.1578359375" Y="-0.956757751465" />
                  <Point X="3.128646240234" Y="-0.968424621582" />
                  <Point X="3.114537597656" Y="-0.975473510742" />
                  <Point X="3.086656005859" Y="-0.992432006836" />
                  <Point X="3.073969238281" Y="-1.001664916992" />
                  <Point X="3.050288574219" Y="-1.022093383789" />
                  <Point X="3.039294677734" Y="-1.033288818359" />
                  <Point X="2.92960546875" Y="-1.16521081543" />
                  <Point X="2.921296142578" Y="-1.176897094727" />
                  <Point X="2.90653125" Y="-1.201386474609" />
                  <Point X="2.900075683594" Y="-1.214189697266" />
                  <Point X="2.888721435547" Y="-1.241696289062" />
                  <Point X="2.884293945313" Y="-1.255212280273" />
                  <Point X="2.877506347656" Y="-1.282755004883" />
                  <Point X="2.875146240234" Y="-1.29678137207" />
                  <Point X="2.859428222656" Y="-1.467590454102" />
                  <Point X="2.859291992188" Y="-1.483364746094" />
                  <Point X="2.861631591797" Y="-1.51471887207" />
                  <Point X="2.864107421875" Y="-1.530298706055" />
                  <Point X="2.871889160156" Y="-1.561997558594" />
                  <Point X="2.876874511719" Y="-1.576872436523" />
                  <Point X="2.889225097656" Y="-1.605598266602" />
                  <Point X="2.896590332031" Y="-1.619449462891" />
                  <Point X="2.997020263672" Y="-1.775661865234" />
                  <Point X="3.001741943359" Y="-1.782353515625" />
                  <Point X="3.019793212891" Y="-1.804449462891" />
                  <Point X="3.043489013672" Y="-1.828120117188" />
                  <Point X="3.052796142578" Y="-1.836277954102" />
                  <Point X="4.087170654297" Y="-2.629981933594" />
                  <Point X="4.045490478516" Y="-2.697426757812" />
                  <Point X="4.001274414063" Y="-2.760251708984" />
                  <Point X="2.849129394531" Y="-2.095060302734" />
                  <Point X="2.842140869141" Y="-2.091403564453" />
                  <Point X="2.816731445312" Y="-2.080261962891" />
                  <Point X="2.783473876953" Y="-2.069455566406" />
                  <Point X="2.771000244141" Y="-2.066318115234" />
                  <Point X="2.555018066406" Y="-2.027311889648" />
                  <Point X="2.539311279297" Y="-2.025806884766" />
                  <Point X="2.507864501953" Y="-2.025417236328" />
                  <Point X="2.492124511719" Y="-2.026532714844" />
                  <Point X="2.459861816406" Y="-2.031537597656" />
                  <Point X="2.444614990234" Y="-2.035212890625" />
                  <Point X="2.41493359375" Y="-2.045022094727" />
                  <Point X="2.400499023438" Y="-2.051155761719" />
                  <Point X="2.221070800781" Y="-2.145587646484" />
                  <Point X="2.208929931641" Y="-2.153198486328" />
                  <Point X="2.185930908203" Y="-2.170159667969" />
                  <Point X="2.175072753906" Y="-2.179510009766" />
                  <Point X="2.154065673828" Y="-2.200559814453" />
                  <Point X="2.144794677734" Y="-2.211359130859" />
                  <Point X="2.127969970703" Y="-2.234221679688" />
                  <Point X="2.120416259766" Y="-2.246284912109" />
                  <Point X="2.025984619141" Y="-2.425713134766" />
                  <Point X="2.019819702148" Y="-2.4402421875" />
                  <Point X="2.009980224609" Y="-2.470120117188" />
                  <Point X="2.006305908203" Y="-2.485468994141" />
                  <Point X="2.001367431641" Y="-2.517749511719" />
                  <Point X="2.000283325195" Y="-2.533392333984" />
                  <Point X="2.000703369141" Y="-2.564642578125" />
                  <Point X="2.00220715332" Y="-2.58025" />
                  <Point X="2.041213500977" Y="-2.796232421875" />
                  <Point X="2.043015014648" Y="-2.804221435547" />
                  <Point X="2.051236083984" Y="-2.83154296875" />
                  <Point X="2.064069580078" Y="-2.862479736328" />
                  <Point X="2.069546630859" Y="-2.873578613281" />
                  <Point X="2.735893554688" Y="-4.027724365234" />
                  <Point X="2.723755126953" Y="-4.036084472656" />
                  <Point X="1.834427978516" Y="-2.877091796875" />
                  <Point X="1.829384643555" Y="-2.871054199219" />
                  <Point X="1.810214599609" Y="-2.851070800781" />
                  <Point X="1.783542236328" Y="-2.828273193359" />
                  <Point X="1.773192504883" Y="-2.820578613281" />
                  <Point X="1.56017565918" Y="-2.683628417969" />
                  <Point X="1.546230224609" Y="-2.676222900391" />
                  <Point X="1.517305908203" Y="-2.663823974609" />
                  <Point X="1.502326904297" Y="-2.658830566406" />
                  <Point X="1.470583129883" Y="-2.651110107422" />
                  <Point X="1.455100219727" Y="-2.648675048828" />
                  <Point X="1.423944335938" Y="-2.646383789062" />
                  <Point X="1.408271362305" Y="-2.646527587891" />
                  <Point X="1.175307373047" Y="-2.667965332031" />
                  <Point X="1.161171508789" Y="-2.670352294922" />
                  <Point X="1.133420043945" Y="-2.677226318359" />
                  <Point X="1.119804199219" Y="-2.681713378906" />
                  <Point X="1.092329101563" Y="-2.693131347656" />
                  <Point X="1.079638061523" Y="-2.699559082031" />
                  <Point X="1.055357910156" Y="-2.714237304688" />
                  <Point X="1.043768798828" Y="-2.722487792969" />
                  <Point X="0.863875061035" Y="-2.872063476562" />
                  <Point X="0.852614074707" Y="-2.883133544922" />
                  <Point X="0.832080932617" Y="-2.906986328125" />
                  <Point X="0.822808776855" Y="-2.919769287109" />
                  <Point X="0.805889709473" Y="-2.947719726562" />
                  <Point X="0.798907531738" Y="-2.961750244141" />
                  <Point X="0.787345275879" Y="-2.990768066406" />
                  <Point X="0.782765014648" Y="-3.005755126953" />
                  <Point X="0.728977661133" Y="-3.253219238281" />
                  <Point X="0.727584594727" Y="-3.261289794922" />
                  <Point X="0.72472467041" Y="-3.289677734375" />
                  <Point X="0.724742370605" Y="-3.323170410156" />
                  <Point X="0.725555175781" Y="-3.335520019531" />
                  <Point X="0.83309173584" Y="-4.152341796875" />
                  <Point X="0.655264404297" Y="-3.488681152344" />
                  <Point X="0.65290222168" Y="-3.481136230469" />
                  <Point X="0.642794372559" Y="-3.455254882812" />
                  <Point X="0.62691784668" Y="-3.424111816406" />
                  <Point X="0.620325683594" Y="-3.413091552734" />
                  <Point X="0.4566796875" Y="-3.177309326172" />
                  <Point X="0.446626831055" Y="-3.165127929688" />
                  <Point X="0.424642242432" Y="-3.142598876953" />
                  <Point X="0.412710510254" Y="-3.132251464844" />
                  <Point X="0.386328582764" Y="-3.112968017578" />
                  <Point X="0.37295892334" Y="-3.104796386719" />
                  <Point X="0.345058685303" Y="-3.090761962891" />
                  <Point X="0.330527832031" Y="-3.084898925781" />
                  <Point X="0.07729535675" Y="-3.006305175781" />
                  <Point X="0.063314552307" Y="-3.003099853516" />
                  <Point X="0.035029682159" Y="-2.998830566406" />
                  <Point X="0.02072562027" Y="-2.997766601562" />
                  <Point X="-0.009042358398" Y="-2.997805908203" />
                  <Point X="-0.023219497681" Y="-2.998888671875" />
                  <Point X="-0.051253639221" Y="-3.003157958984" />
                  <Point X="-0.065110496521" Y="-3.006344482422" />
                  <Point X="-0.318342681885" Y="-3.084938476562" />
                  <Point X="-0.33299005127" Y="-3.090860107422" />
                  <Point X="-0.361100738525" Y="-3.105048828125" />
                  <Point X="-0.374564331055" Y="-3.113315917969" />
                  <Point X="-0.4009012146" Y="-3.132677734375" />
                  <Point X="-0.412713745117" Y="-3.142964599609" />
                  <Point X="-0.434488189697" Y="-3.165339599609" />
                  <Point X="-0.444449798584" Y="-3.177427734375" />
                  <Point X="-0.60809564209" Y="-3.413210205078" />
                  <Point X="-0.612471374512" Y="-3.420134277344" />
                  <Point X="-0.625979431152" Y="-3.445270996094" />
                  <Point X="-0.638779296875" Y="-3.476220947266" />
                  <Point X="-0.64275378418" Y="-3.487939697266" />
                  <Point X="-0.985425231934" Y="-4.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.691452148438" Y="1.104696296731" />
                  <Point X="-4.691452148438" Y="0.566639067057" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.691452148438" Y="-0.629199240612" />
                  <Point X="-4.691452148438" Y="-1.167163449992" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.596452148438" Y="1.31334847563" />
                  <Point X="-4.596452148438" Y="0.541183908026" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.596452148438" Y="-0.603744062279" />
                  <Point X="-4.596452148438" Y="-1.278206810596" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.501452148438" Y="1.300841466104" />
                  <Point X="-4.501452148438" Y="0.515728748995" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.501452148438" Y="-0.578288883946" />
                  <Point X="-4.501452148438" Y="-1.265699829809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.406452148438" Y="1.288334456578" />
                  <Point X="-4.406452148438" Y="0.490273589965" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.406452148438" Y="-0.552833705613" />
                  <Point X="-4.406452148438" Y="-1.253192849022" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.311452148438" Y="1.275827447052" />
                  <Point X="-4.311452148438" Y="0.464818430934" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.311452148438" Y="-0.52737852728" />
                  <Point X="-4.311452148438" Y="-1.240685868234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.216452148438" Y="2.313406180499" />
                  <Point X="-4.216452148438" Y="2.285716621985" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.216452148438" Y="1.263320437526" />
                  <Point X="-4.216452148438" Y="0.439363271903" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.216452148438" Y="-0.501923348947" />
                  <Point X="-4.216452148438" Y="-1.228178887447" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.121452148437" Y="2.476163889168" />
                  <Point X="-4.121452148438" Y="2.212820575156" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.121452148438" Y="1.250813428" />
                  <Point X="-4.121452148438" Y="0.413908112873" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.121452148438" Y="-0.476468170614" />
                  <Point X="-4.121452148438" Y="-1.215671906659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.121452148438" Y="-2.397477626361" />
                  <Point X="-4.121452148438" Y="-2.543671077939" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.026452148437" Y="2.638921597836" />
                  <Point X="-4.026452148438" Y="2.139924528326" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.026452148438" Y="1.238306418474" />
                  <Point X="-4.026452148438" Y="0.388452953842" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.026452148438" Y="-0.451012992281" />
                  <Point X="-4.026452148438" Y="-1.203164925872" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.026452148438" Y="-2.324581510413" />
                  <Point X="-4.026452148438" Y="-2.702971652025" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.931452148438" Y="2.771368307273" />
                  <Point X="-3.931452148438" Y="2.067028481497" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.931452148438" Y="1.225799408948" />
                  <Point X="-3.931452148438" Y="0.362997794811" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.931452148438" Y="-0.425557813948" />
                  <Point X="-3.931452148438" Y="-1.190657945084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.931452148438" Y="-2.251685394466" />
                  <Point X="-3.931452148438" Y="-2.835928714385" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.836452148437" Y="2.893477353061" />
                  <Point X="-3.836452148437" Y="1.994132434668" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.836452148437" Y="1.213292399422" />
                  <Point X="-3.836452148438" Y="0.337542635781" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.836452148438" Y="-0.400102635615" />
                  <Point X="-3.836452148438" Y="-1.178150964297" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.836452148438" Y="-2.178789278518" />
                  <Point X="-3.836452148438" Y="-2.96073924008" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.741452148438" Y="3.01558639885" />
                  <Point X="-3.741452148438" Y="1.921236387838" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.741452148438" Y="1.204550299299" />
                  <Point X="-3.741452148438" Y="0.31208747675" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.741452148438" Y="-0.374647457281" />
                  <Point X="-3.741452148438" Y="-1.165643983509" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.741452148438" Y="-2.105893162571" />
                  <Point X="-3.741452148438" Y="-2.987895837822" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.646452148437" Y="2.988890187005" />
                  <Point X="-3.646452148437" Y="1.848340341009" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.646452148437" Y="1.223526212422" />
                  <Point X="-3.646452148438" Y="0.286632317719" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.646452148438" Y="-0.349192278948" />
                  <Point X="-3.646452148438" Y="-1.153137002722" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.646452148438" Y="-2.032997046623" />
                  <Point X="-3.646452148438" Y="-2.933047551067" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.551452148437" Y="2.934041874938" />
                  <Point X="-3.551452148437" Y="1.77544429418" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.551452148437" Y="1.264814370088" />
                  <Point X="-3.551452148438" Y="0.261177158688" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.551452148438" Y="-0.323737100615" />
                  <Point X="-3.551452148438" Y="-1.140630021935" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.551452148438" Y="-1.960100930676" />
                  <Point X="-3.551452148438" Y="-2.878199264312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.456452148437" Y="2.879193562872" />
                  <Point X="-3.456452148437" Y="1.649895470831" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.456452148437" Y="1.388291130264" />
                  <Point X="-3.456452148438" Y="0.227910221208" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.456452148438" Y="-0.290145541033" />
                  <Point X="-3.456452148438" Y="-1.128123041147" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.456452148438" Y="-1.887204814728" />
                  <Point X="-3.456452148438" Y="-2.823350977557" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.361452148438" Y="2.824345250806" />
                  <Point X="-3.361452148438" Y="0.153646924998" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.361452148438" Y="-0.215118110712" />
                  <Point X="-3.361452148438" Y="-1.11561606036" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.361452148438" Y="-1.814308698781" />
                  <Point X="-3.361452148438" Y="-2.768502690802" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.266452148438" Y="2.76949693874" />
                  <Point X="-3.266452148438" Y="-1.103109079572" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.266452148438" Y="-1.741412582834" />
                  <Point X="-3.266452148438" Y="-2.713654404046" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.171452148437" Y="2.733850631071" />
                  <Point X="-3.171452148438" Y="-1.104458464371" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.171452148438" Y="-1.668516466886" />
                  <Point X="-3.171452148438" Y="-2.658806117291" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.076452148438" Y="2.724280183533" />
                  <Point X="-3.076452148438" Y="-1.140028790915" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.076452148438" Y="-1.595620350939" />
                  <Point X="-3.076452148438" Y="-2.603957830536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.981452148437" Y="3.759668591915" />
                  <Point X="-2.981452148438" Y="3.564929025053" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.981452148438" Y="2.732974229345" />
                  <Point X="-2.981452148438" Y="-1.230385389979" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.981452148438" Y="-1.490065017116" />
                  <Point X="-2.981452148438" Y="-2.549109543781" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.886452148437" Y="3.832504212535" />
                  <Point X="-2.886452148438" Y="3.400384201164" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.886452148438" Y="2.786278623252" />
                  <Point X="-2.886452148438" Y="-2.494261257026" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.886452148438" Y="-3.668023571189" />
                  <Point X="-2.886452148438" Y="-3.904240625141" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.791452148437" Y="3.905339833156" />
                  <Point X="-2.791452148437" Y="3.235839377275" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.791452148437" Y="2.887345926001" />
                  <Point X="-2.791452148438" Y="-2.439412970271" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.791452148438" Y="-3.503478741321" />
                  <Point X="-2.791452148438" Y="-3.977387426729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.696452148437" Y="3.978175453777" />
                  <Point X="-2.696452148438" Y="-2.384564683516" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.696452148438" Y="-3.338933911453" />
                  <Point X="-2.696452148438" Y="-4.042824391694" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.601452148437" Y="4.041105254448" />
                  <Point X="-2.601452148438" Y="-2.351690520755" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.601452148438" Y="-3.174389081585" />
                  <Point X="-2.601452148438" Y="-4.101646009439" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.506452148437" Y="4.093885344414" />
                  <Point X="-2.506452148438" Y="-2.356266440823" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.506452148438" Y="-3.009844251717" />
                  <Point X="-2.506452148438" Y="-4.160467627183" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.411452148437" Y="4.146665434379" />
                  <Point X="-2.411452148438" Y="-2.405132494079" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.411452148438" Y="-2.845299421849" />
                  <Point X="-2.411452148438" Y="-4.042907727337" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.316452148437" Y="4.199445524345" />
                  <Point X="-2.316452148438" Y="-2.551226214416" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.316452148438" Y="-2.650285875794" />
                  <Point X="-2.316452148438" Y="-3.963489951839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.221452148437" Y="4.25222561431" />
                  <Point X="-2.221452148438" Y="-3.900012782967" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.126452148437" Y="4.182188579575" />
                  <Point X="-2.126452148438" Y="-3.836535614095" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.031452148437" Y="4.101210729992" />
                  <Point X="-2.031452148438" Y="-3.788266562501" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.936452148437" Y="4.052249200209" />
                  <Point X="-1.936452148438" Y="-3.7765304111" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.841452148437" Y="4.029151479817" />
                  <Point X="-1.841452148438" Y="-3.782756992403" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.746452148437" Y="4.044831016654" />
                  <Point X="-1.746452148438" Y="-3.788983573706" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.651452148437" Y="4.083846661411" />
                  <Point X="-1.651452148438" Y="-3.79521015501" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.556452148437" Y="4.148624854688" />
                  <Point X="-1.556452148438" Y="-3.818559772331" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.461452148437" Y="4.567650223472" />
                  <Point X="-1.461452148438" Y="-3.883992794336" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.366452148437" Y="4.594284979932" />
                  <Point X="-1.366452148438" Y="-3.967305788206" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.271452148437" Y="4.620919736392" />
                  <Point X="-1.271452148438" Y="-4.050618782076" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.176452148437" Y="4.647554492852" />
                  <Point X="-1.176452148438" Y="-4.230648042476" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.081452148437" Y="4.674189249312" />
                  <Point X="-1.081452148438" Y="-4.74671325323" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.986452148437" Y="4.700824005772" />
                  <Point X="-0.986452148438" Y="-4.766607318347" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.891452148437" Y="4.720969337991" />
                  <Point X="-0.891452148438" Y="-4.416094403476" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.796452148437" Y="4.732087654947" />
                  <Point X="-0.796452148438" Y="-4.061549662689" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.701452148437" Y="4.743205971903" />
                  <Point X="-0.701452148438" Y="-3.707004921902" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.606452148437" Y="4.754324288858" />
                  <Point X="-0.606452148438" Y="-3.410842243964" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.511452148437" Y="4.765442605814" />
                  <Point X="-0.511452148438" Y="-3.27396535032" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.416452148437" Y="4.776560922769" />
                  <Point X="-0.416452148438" Y="-3.146806110881" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.321452148437" Y="4.619205560114" />
                  <Point X="-0.321452148438" Y="-3.08619557008" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.226452148437" Y="4.264661108596" />
                  <Point X="-0.226452148438" Y="-3.05641902104" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.131452148437" Y="4.128806664157" />
                  <Point X="-0.131452148438" Y="-3.026934501003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.036452148437" Y="4.089937739863" />
                  <Point X="-0.036452148438" Y="-3.00090385701" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.058547851563" Y="4.092138306084" />
                  <Point X="0.058547851562" Y="-3.002380372962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.153547851563" Y="4.136462192258" />
                  <Point X="0.153547851562" Y="-3.029971056342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.248547851563" Y="4.301855368001" />
                  <Point X="0.248547851562" Y="-3.059455451034" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.343547851563" Y="4.656045715996" />
                  <Point X="0.343547851562" Y="-3.09015235834" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.438547851563" Y="4.778684810996" />
                  <Point X="0.438547851562" Y="-3.156848868471" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.533547851563" Y="4.768735833879" />
                  <Point X="0.533547851562" Y="-3.288061482088" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.628547851563" Y="4.758786856763" />
                  <Point X="0.628547851562" Y="-3.427309200437" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.723547851563" Y="4.748837879646" />
                  <Point X="0.723547851562" Y="-3.743518450018" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.818547851563" Y="4.73888890253" />
                  <Point X="0.818547851562" Y="-2.926808370986" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.818547851562" Y="-4.041869959219" />
                  <Point X="0.818547851562" Y="-4.098063281139" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.913547851563" Y="4.717228604447" />
                  <Point X="0.913547851562" Y="-2.830762196761" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.008547851563" Y="4.694292599187" />
                  <Point X="1.008547851562" Y="-2.751772843848" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.103547851563" Y="4.671356593928" />
                  <Point X="1.103547851562" Y="-2.688469113566" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.198547851563" Y="4.648420588668" />
                  <Point X="1.198547851562" Y="-2.665826703509" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.293547851563" Y="4.625484583409" />
                  <Point X="1.293547851562" Y="-2.657084641271" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.388547851563" Y="4.602548578149" />
                  <Point X="1.388547851562" Y="-2.648342579032" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.483547851563" Y="4.575980420392" />
                  <Point X="1.483547851562" Y="-2.654263280093" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.578547851563" Y="4.541523096658" />
                  <Point X="1.578547851562" Y="-2.695440044072" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.673547851563" Y="4.507065772925" />
                  <Point X="1.673547851562" Y="-2.756516286713" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.768547851563" Y="4.472608449191" />
                  <Point X="1.768547851562" Y="-2.817592529353" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.863547851563" Y="4.437595909443" />
                  <Point X="1.863547851562" Y="-2.915041520596" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.958547851563" Y="4.393167631255" />
                  <Point X="1.958547851562" Y="-3.038847821115" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.053547851563" Y="4.348739353067" />
                  <Point X="2.053547851562" Y="2.573966163449" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.053547851562" Y="2.207939379261" />
                  <Point X="2.053547851562" Y="-2.373340627295" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.053547851562" Y="-2.837115777261" />
                  <Point X="2.053547851562" Y="-3.162654121634" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.148547851563" Y="4.304311074879" />
                  <Point X="2.148547851563" Y="2.742772794123" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.148547851563" Y="2.08364097445" />
                  <Point X="2.148547851562" Y="-2.206987247699" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.148547851562" Y="-3.010412625735" />
                  <Point X="2.148547851562" Y="-3.286460422153" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.243547851563" Y="4.259882796691" />
                  <Point X="2.243547851563" Y="2.907317655322" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.243547851563" Y="2.017068119533" />
                  <Point X="2.243547851562" Y="-2.133758123582" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.243547851562" Y="-3.174957313367" />
                  <Point X="2.243547851562" Y="-3.410266722673" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.338547851563" Y="4.205329133213" />
                  <Point X="2.338547851562" Y="3.07186251652" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.338547851562" Y="1.98423100174" />
                  <Point X="2.338547851562" Y="-2.083760253013" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.338547851562" Y="-3.339502000998" />
                  <Point X="2.338547851562" Y="-3.534073023192" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.433547851563" Y="4.149981764792" />
                  <Point X="2.433547851562" Y="3.236407377719" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.433547851562" Y="1.975421117593" />
                  <Point X="2.433547851562" Y="-2.03887039448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.433547851562" Y="-3.50404668863" />
                  <Point X="2.433547851562" Y="-3.657879323711" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.528547851563" Y="4.094634396371" />
                  <Point X="2.528547851563" Y="3.400952238917" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.528547851563" Y="1.990632145652" />
                  <Point X="2.528547851562" Y="-2.025673518069" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.528547851562" Y="-3.668591376261" />
                  <Point X="2.528547851562" Y="-3.78168562423" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.623547851563" Y="4.03928702795" />
                  <Point X="2.623547851563" Y="3.565497100115" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.623547851563" Y="2.021717973104" />
                  <Point X="2.623547851562" Y="-2.039688319441" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.623547851562" Y="-3.833136063893" />
                  <Point X="2.623547851562" Y="-3.905491924749" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.718547851563" Y="3.972484871417" />
                  <Point X="2.718547851563" Y="3.730041961314" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.718547851562" Y="2.075511891078" />
                  <Point X="2.718547851562" Y="-2.05684525095" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.718547851562" Y="-3.997680751524" />
                  <Point X="2.718547851562" Y="-4.029298225268" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.813547851563" Y="3.90492625921" />
                  <Point X="2.813547851563" Y="3.894586822512" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.813547851562" Y="2.130360193051" />
                  <Point X="2.813547851562" Y="-2.079227516357" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.908547851563" Y="2.185208495025" />
                  <Point X="2.908547851562" Y="-1.198041694316" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.908547851562" Y="-1.638048627717" />
                  <Point X="2.908547851562" Y="-2.129365573616" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.003547851563" Y="2.240056796999" />
                  <Point X="3.003547851563" Y="1.437406409154" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.003547851563" Y="1.360508871057" />
                  <Point X="3.003547851562" Y="-1.076281130376" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.003547851562" Y="-1.784564066653" />
                  <Point X="3.003547851562" Y="-2.18421386313" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.098547851562" Y="2.294905098973" />
                  <Point X="3.098547851562" Y="1.660247884496" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.098547851562" Y="1.120251419026" />
                  <Point X="3.098547851562" Y="-0.985198997316" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.098547851562" Y="-1.87138449731" />
                  <Point X="3.098547851562" Y="-2.239062152645" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.193547851563" Y="2.349753400946" />
                  <Point X="3.193547851563" Y="1.759623844387" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.193547851562" Y="1.029984027995" />
                  <Point X="3.193547851562" Y="-0.947655718778" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.193547851562" Y="-1.944280607179" />
                  <Point X="3.193547851562" Y="-2.293910442159" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.288547851562" Y="2.40460170292" />
                  <Point X="3.288547851562" Y="1.832519940484" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.288547851562" Y="0.980587847645" />
                  <Point X="3.288547851562" Y="-0.927007208539" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.288547851562" Y="-2.017176717048" />
                  <Point X="3.288547851562" Y="-2.348758731673" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.383547851562" Y="2.459450004894" />
                  <Point X="3.383547851562" Y="1.905416036582" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.383547851562" Y="0.959987304587" />
                  <Point X="3.383547851562" Y="0.15213007219" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.383547851562" Y="-0.214723525403" />
                  <Point X="3.383547851562" Y="-0.909528192639" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.383547851562" Y="-2.090072826917" />
                  <Point X="3.383547851562" Y="-2.403607021188" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.478547851563" Y="2.514298306868" />
                  <Point X="3.478547851563" Y="1.978312132679" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.478547851563" Y="0.951630136641" />
                  <Point X="3.478547851563" Y="0.290553946096" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.478547851563" Y="-0.353432740353" />
                  <Point X="3.478547851563" Y="-0.914752088804" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.478547851562" Y="-2.162968936785" />
                  <Point X="3.478547851562" Y="-2.458455310702" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.573547851562" Y="2.569146608841" />
                  <Point X="3.573547851562" Y="2.051208228777" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.573547851562" Y="0.962400593811" />
                  <Point X="3.573547851562" Y="0.362371542966" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.573547851562" Y="-0.424931564036" />
                  <Point X="3.573547851562" Y="-0.92725908511" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.573547851562" Y="-2.235865046654" />
                  <Point X="3.573547851562" Y="-2.513303600216" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.668547851563" Y="2.623994910815" />
                  <Point X="3.668547851563" Y="2.124104324874" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.668547851562" Y="0.974907564873" />
                  <Point X="3.668547851562" Y="0.41319129903" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.668547851562" Y="-0.475561466987" />
                  <Point X="3.668547851562" Y="-0.939766081415" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.668547851562" Y="-2.308761156523" />
                  <Point X="3.668547851562" Y="-2.568151889731" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.763547851563" Y="2.678843212789" />
                  <Point X="3.763547851563" Y="2.197000420972" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.763547851563" Y="0.987414535935" />
                  <Point X="3.763547851563" Y="0.440526118237" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.763547851563" Y="-0.50308623048" />
                  <Point X="3.763547851563" Y="-0.952273077721" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.763547851562" Y="-2.381657266392" />
                  <Point X="3.763547851562" Y="-2.623000179245" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.858547851562" Y="2.733691514763" />
                  <Point X="3.858547851562" Y="2.269896517069" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.858547851562" Y="0.999921506998" />
                  <Point X="3.858547851562" Y="0.465981296404" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.858547851562" Y="-0.528541411264" />
                  <Point X="3.858547851562" Y="-0.964780074027" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.858547851562" Y="-2.454553376261" />
                  <Point X="3.858547851562" Y="-2.677848468759" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.953547851563" Y="2.76268582907" />
                  <Point X="3.953547851563" Y="2.342792613167" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.953547851562" Y="1.01242847806" />
                  <Point X="3.953547851562" Y="0.49143647457" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.953547851562" Y="-0.553996592048" />
                  <Point X="3.953547851562" Y="-0.977287070333" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.953547851562" Y="-2.527449486129" />
                  <Point X="3.953547851562" Y="-2.732696758274" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.048547851563" Y="2.629450778254" />
                  <Point X="4.048547851563" Y="2.415688709265" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.048547851563" Y="1.024935449122" />
                  <Point X="4.048547851563" Y="0.516891652737" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.048547851563" Y="-0.579451772832" />
                  <Point X="4.048547851563" Y="-0.989794066638" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.048547851562" Y="-2.600345595998" />
                  <Point X="4.048547851562" Y="-2.692479466112" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.143547851562" Y="1.037442420184" />
                  <Point X="4.143547851562" Y="0.542346830903" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.143547851562" Y="-0.604906953617" />
                  <Point X="4.143547851562" Y="-1.002301062944" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.238547851562" Y="1.049949391246" />
                  <Point X="4.238547851562" Y="0.56780200907" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.238547851562" Y="-0.630362134401" />
                  <Point X="4.238547851562" Y="-1.01480805925" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.333547851563" Y="1.062456362308" />
                  <Point X="4.333547851563" Y="0.593257187237" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.333547851563" Y="-0.655817315185" />
                  <Point X="4.333547851563" Y="-1.027315055555" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.428547851562" Y="1.07496333337" />
                  <Point X="4.428547851562" Y="0.618712365403" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.428547851562" Y="-0.681272495969" />
                  <Point X="4.428547851562" Y="-1.039822051861" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.523547851562" Y="1.087470304432" />
                  <Point X="4.523547851562" Y="0.64416754357" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.523547851562" Y="-0.706727676753" />
                  <Point X="4.523547851562" Y="-1.052329048167" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.618547851562" Y="1.099977275494" />
                  <Point X="4.618547851562" Y="0.669622721736" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.618547851562" Y="-0.732182857537" />
                  <Point X="4.618547851562" Y="-1.064836044473" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.713547851562" Y="1.07499245798" />
                  <Point X="4.713547851562" Y="0.695077899903" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.713547851562" Y="-0.757638038322" />
                  <Point X="4.713547851562" Y="-1.077343040778" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="0.001626708984" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978485839844" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.471738555908" Y="-3.537856933594" />
                  <Point X="0.464236938477" Y="-3.521426025391" />
                  <Point X="0.300591003418" Y="-3.285643798828" />
                  <Point X="0.274209014893" Y="-3.266360351562" />
                  <Point X="0.020976623535" Y="-3.187766601562" />
                  <Point X="-0.00879146862" Y="-3.187805908203" />
                  <Point X="-0.262023864746" Y="-3.266399902344" />
                  <Point X="-0.288360961914" Y="-3.28576171875" />
                  <Point X="-0.452006896973" Y="-3.521544189453" />
                  <Point X="-0.459227874756" Y="-3.537115478516" />
                  <Point X="-0.84774395752" Y="-4.987076660156" />
                  <Point X="-1.100244140625" Y="-4.938065917969" />
                  <Point X="-1.351589599609" Y="-4.873396972656" />
                  <Point X="-1.309174438477" Y="-4.551221679688" />
                  <Point X="-1.309713500977" Y="-4.534605957031" />
                  <Point X="-1.370210571289" Y="-4.230466308594" />
                  <Point X="-1.386399414062" Y="-4.202526367187" />
                  <Point X="-1.619543457031" Y="-3.998063964844" />
                  <Point X="-1.649395507812" Y="-3.985752685547" />
                  <Point X="-1.958829956055" Y="-3.965471435547" />
                  <Point X="-1.990007568359" Y="-3.973877197266" />
                  <Point X="-2.247844970703" Y="-4.146159179688" />
                  <Point X="-2.259734130859" Y="-4.157294433594" />
                  <Point X="-2.457095214844" Y="-4.414500488281" />
                  <Point X="-2.855829589844" Y="-4.167614257812" />
                  <Point X="-3.228581054688" Y="-3.880608154297" />
                  <Point X="-2.506406005859" Y="-2.629764160156" />
                  <Point X="-2.505652099609" Y="-2.628422119141" />
                  <Point X="-2.499824707031" Y="-2.597192382812" />
                  <Point X="-2.513988525391" Y="-2.568755615234" />
                  <Point X="-2.531328369141" Y="-2.551415771484" />
                  <Point X="-2.560157226562" Y="-2.537198730469" />
                  <Point X="-2.59168359375" Y="-2.543469726562" />
                  <Point X="-3.842959228516" Y="-3.265894287109" />
                  <Point X="-4.161703613281" Y="-2.847129638672" />
                  <Point X="-4.431020019531" Y="-2.395527099609" />
                  <Point X="-3.164437988281" Y="-1.42364465332" />
                  <Point X="-3.163781738281" Y="-1.423140991211" />
                  <Point X="-3.145818115234" Y="-1.395998657227" />
                  <Point X="-3.138117431641" Y="-1.366266235352" />
                  <Point X="-3.136545654297" Y="-1.352003295898" />
                  <Point X="-3.145109130859" Y="-1.325705810547" />
                  <Point X="-3.161172363281" Y="-1.310631469727" />
                  <Point X="-3.187641601562" Y="-1.295052856445" />
                  <Point X="-3.219530273438" Y="-1.288571166992" />
                  <Point X="-4.803283691406" Y="-1.497076171875" />
                  <Point X="-4.927392578125" Y="-1.011193359375" />
                  <Point X="-4.998396484375" Y="-0.5147421875" />
                  <Point X="-3.55820703125" Y="-0.128844482422" />
                  <Point X="-3.541881835938" Y="-0.121415420532" />
                  <Point X="-3.514142822266" Y="-0.102162979126" />
                  <Point X="-3.494894287109" Y="-0.075892799377" />
                  <Point X="-3.485647949219" Y="-0.046100826263" />
                  <Point X="-3.483967041016" Y="-0.023776391983" />
                  <Point X="-3.485664306641" Y="-0.016406383514" />
                  <Point X="-3.494898925781" Y="0.013347587585" />
                  <Point X="-3.507770507812" Y="0.034350284576" />
                  <Point X="-3.514211425781" Y="0.039650455475" />
                  <Point X="-3.541895751953" Y="0.058864887238" />
                  <Point X="-3.557460449219" Y="0.066084480286" />
                  <Point X="-4.998186523438" Y="0.452125793457" />
                  <Point X="-4.917645019531" Y="0.996419433594" />
                  <Point X="-4.773516113281" Y="1.528298583984" />
                  <Point X="-3.753801269531" Y="1.39405065918" />
                  <Point X="-3.753269287109" Y="1.39398059082" />
                  <Point X="-3.731674316406" Y="1.395876220703" />
                  <Point X="-3.731643798828" Y="1.395885986328" />
                  <Point X="-3.670278808594" Y="1.41523425293" />
                  <Point X="-3.651594482422" Y="1.426001220703" />
                  <Point X="-3.639182617188" Y="1.443635620117" />
                  <Point X="-3.639107421875" Y="1.443815795898" />
                  <Point X="-3.614472412109" Y="1.503290283203" />
                  <Point X="-3.61071484375" Y="1.52462109375" />
                  <Point X="-3.616330566406" Y="1.545539672852" />
                  <Point X="-3.616330810547" Y="1.545540283203" />
                  <Point X="-3.646055664062" Y="1.602641357422" />
                  <Point X="-3.659968261719" Y="1.619221923828" />
                  <Point X="-4.476105957031" Y="2.245465820312" />
                  <Point X="-4.160013671875" Y="2.787007568359" />
                  <Point X="-3.774671386719" Y="3.282310791016" />
                  <Point X="-3.159483642578" Y="2.927131835938" />
                  <Point X="-3.159145019531" Y="2.926936523438" />
                  <Point X="-3.138471435547" Y="2.920441162109" />
                  <Point X="-3.138472167969" Y="2.920431152344" />
                  <Point X="-3.052965576172" Y="2.912950195312" />
                  <Point X="-3.031601074219" Y="2.915740966797" />
                  <Point X="-3.01339453125" Y="2.927262939453" />
                  <Point X="-3.013221923828" Y="2.927435058594" />
                  <Point X="-2.952528808594" Y="2.988128173828" />
                  <Point X="-2.940918701172" Y="3.006329589844" />
                  <Point X="-2.938064697266" Y="3.027729248047" />
                  <Point X="-2.938077880859" Y="3.027884277344" />
                  <Point X="-2.945558837891" Y="3.113390869141" />
                  <Point X="-2.952067382812" Y="3.134032958984" />
                  <Point X="-3.307278808594" Y="3.749276855469" />
                  <Point X="-2.752875488281" Y="4.17433203125" />
                  <Point X="-2.141548583984" Y="4.513972167969" />
                  <Point X="-1.967927124023" Y="4.287703613281" />
                  <Point X="-1.967817993164" Y="4.287561523437" />
                  <Point X="-1.951334106445" Y="4.273706542969" />
                  <Point X="-1.95119909668" Y="4.273635742188" />
                  <Point X="-1.856030761719" Y="4.22409375" />
                  <Point X="-1.835133178711" Y="4.2184921875" />
                  <Point X="-1.813825561523" Y="4.222243652344" />
                  <Point X="-1.813759399414" Y="4.222270996094" />
                  <Point X="-1.714634887695" Y="4.263330078125" />
                  <Point X="-1.696889648438" Y="4.27576171875" />
                  <Point X="-1.686069458008" Y="4.294532714844" />
                  <Point X="-1.686053466797" Y="4.294583984375" />
                  <Point X="-1.653804077148" Y="4.396865234375" />
                  <Point X="-1.651917602539" Y="4.418426757812" />
                  <Point X="-1.689137695313" Y="4.701141113281" />
                  <Point X="-0.968097167969" Y="4.903295898438" />
                  <Point X="-0.22419972229" Y="4.990358398438" />
                  <Point X="-0.042236972809" Y="4.311264160156" />
                  <Point X="-0.024380346298" Y="4.284538574219" />
                  <Point X="0.006055233002" Y="4.274205078125" />
                  <Point X="0.036492576599" Y="4.284533691406" />
                  <Point X="0.054353523254" Y="4.311256347656" />
                  <Point X="0.236637588501" Y="4.990869628906" />
                  <Point X="0.860209106445" Y="4.925564941406" />
                  <Point X="1.508457519531" Y="4.769057617188" />
                  <Point X="1.931033203125" Y="4.615786132813" />
                  <Point X="2.338699462891" Y="4.425134277344" />
                  <Point X="2.732523681641" Y="4.19569140625" />
                  <Point X="3.068740234375" Y="3.956592773438" />
                  <Point X="2.230281738281" Y="2.50433984375" />
                  <Point X="2.224833496094" Y="2.4914453125" />
                  <Point X="2.203382324219" Y="2.411228271484" />
                  <Point X="2.202048828125" Y="2.392291015625" />
                  <Point X="2.210416015625" Y="2.322900878906" />
                  <Point X="2.218650878906" Y="2.300858398438" />
                  <Point X="2.218703613281" Y="2.300780517578" />
                  <Point X="2.261639892578" Y="2.237503662109" />
                  <Point X="2.274971435547" Y="2.224182128906" />
                  <Point X="2.338248535156" Y="2.18124609375" />
                  <Point X="2.36003515625" Y="2.173017578125" />
                  <Point X="2.360371582031" Y="2.172975830078" />
                  <Point X="2.42976171875" Y="2.164608642578" />
                  <Point X="2.448704589844" Y="2.16595703125" />
                  <Point X="2.528950683594" Y="2.187416015625" />
                  <Point X="2.541033935547" Y="2.192417480469" />
                  <Point X="3.994248046875" Y="3.031431152344" />
                  <Point X="4.202591796875" Y="2.741880371094" />
                  <Point X="4.387513183594" Y="2.436296142578" />
                  <Point X="3.289183349609" Y="1.593518066406" />
                  <Point X="3.279342285156" Y="1.583795532227" />
                  <Point X="3.221589111328" Y="1.508451904297" />
                  <Point X="3.213105224609" Y="1.491448242188" />
                  <Point X="3.191595703125" Y="1.414535522461" />
                  <Point X="3.190767822266" Y="1.391022094727" />
                  <Point X="3.190788330078" Y="1.390922607422" />
                  <Point X="3.208448486328" Y="1.305332519531" />
                  <Point X="3.215670410156" Y="1.287918334961" />
                  <Point X="3.263704101562" Y="1.214909545898" />
                  <Point X="3.280816162109" Y="1.198894287109" />
                  <Point X="3.280982666016" Y="1.198800170898" />
                  <Point X="3.350590087891" Y="1.159617431641" />
                  <Point X="3.368612304688" Y="1.15361340332" />
                  <Point X="3.462726074219" Y="1.141175048828" />
                  <Point X="3.475803710938" Y="1.14117175293" />
                  <Point X="4.848975585938" Y="1.32195324707" />
                  <Point X="4.939187988281" Y="0.951387023926" />
                  <Point X="4.997858398438" Y="0.574556091309" />
                  <Point X="3.741819824219" Y="0.238001586914" />
                  <Point X="3.729040771484" Y="0.232792663574" />
                  <Point X="3.636576904297" Y="0.179346923828" />
                  <Point X="3.622237304688" Y="0.166891326904" />
                  <Point X="3.566759033203" Y="0.096198936462" />
                  <Point X="3.556990234375" Y="0.074761650085" />
                  <Point X="3.556975830078" Y="0.074686958313" />
                  <Point X="3.538482910156" Y="-0.021875242233" />
                  <Point X="3.5384921875" Y="-0.040733097076" />
                  <Point X="3.556985107422" Y="-0.137295303345" />
                  <Point X="3.566670654297" Y="-0.158645889282" />
                  <Point X="3.566786621094" Y="-0.158794342041" />
                  <Point X="3.622264892578" Y="-0.229486740112" />
                  <Point X="3.636664306641" Y="-0.241957397461" />
                  <Point X="3.729086914062" Y="-0.295379394531" />
                  <Point X="3.7411640625" Y="-0.300386016846" />
                  <Point X="4.998067871094" Y="-0.637172363281" />
                  <Point X="4.948431152344" Y="-0.966402038574" />
                  <Point X="4.874545898438" Y="-1.290178344727" />
                  <Point X="3.412735351562" Y="-1.097727294922" />
                  <Point X="3.394727050781" Y="-1.098364990234" />
                  <Point X="3.213271972656" Y="-1.137804931641" />
                  <Point X="3.185390380859" Y="-1.154763305664" />
                  <Point X="3.075701171875" Y="-1.286685302734" />
                  <Point X="3.064346923828" Y="-1.314191772461" />
                  <Point X="3.04862890625" Y="-1.485000854492" />
                  <Point X="3.056410644531" Y="-1.516699951172" />
                  <Point X="3.156840576172" Y="-1.672912353516" />
                  <Point X="3.168460693359" Y="-1.685540771484" />
                  <Point X="4.33907421875" Y="-2.583784179688" />
                  <Point X="4.204134765625" Y="-2.802136962891" />
                  <Point X="4.056688232422" Y="-3.011638427734" />
                  <Point X="2.754129394531" Y="-2.259605224609" />
                  <Point X="2.737232666016" Y="-2.253293212891" />
                  <Point X="2.521250488281" Y="-2.214287109375" />
                  <Point X="2.488987792969" Y="-2.219291992188" />
                  <Point X="2.309559570312" Y="-2.313723876953" />
                  <Point X="2.288552490234" Y="-2.334773681641" />
                  <Point X="2.194120849609" Y="-2.514201904297" />
                  <Point X="2.189182373047" Y="-2.546482421875" />
                  <Point X="2.228188720703" Y="-2.76246484375" />
                  <Point X="2.234091552734" Y="-2.778578613281" />
                  <Point X="2.986673828125" Y="-4.082088378906" />
                  <Point X="2.835286865234" Y="-4.190220703125" />
                  <Point X="2.679776123047" Y="-4.290879394531" />
                  <Point X="1.683690917969" Y="-2.992756347656" />
                  <Point X="1.670442749023" Y="-2.980398925781" />
                  <Point X="1.45742590332" Y="-2.843448730469" />
                  <Point X="1.425682006836" Y="-2.835728271484" />
                  <Point X="1.192718017578" Y="-2.857166015625" />
                  <Point X="1.165242919922" Y="-2.868583984375" />
                  <Point X="0.985349121094" Y="-3.018159667969" />
                  <Point X="0.968429870605" Y="-3.046110107422" />
                  <Point X="0.914642578125" Y="-3.29357421875" />
                  <Point X="0.913929626465" Y="-3.310720214844" />
                  <Point X="1.127642211914" Y="-4.934028808594" />
                  <Point X="0.994337219238" Y="-4.963248535156" />
                  <Point X="0.860200500488" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#116" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-9.6622281E-05" Y="4.355485072851" Z="0" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0" />
                  <Point X="-0.983568725817" Y="4.983827131987" Z="0" />
                  <Point X="-0.983964085579" Y="4.983780860901" />
                  <Point X="-1.75013969171" Y="4.768972089767" Z="0" />
                  <Point X="-1.750522971153" Y="4.768864631653" />
                  <Point X="-1.750501532614" Y="4.768701789618" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0" />
                  <Point X="-1.739909006476" Y="4.340855288267" Z="0" />
                  <Point X="-1.739925146103" Y="4.340804100037" />
                  <Point X="-1.839049462616" Y="4.299745118618" Z="0" />
                  <Point X="-1.839099049568" Y="4.299724578857" />
                  <Point X="-1.934267596543" Y="4.349266541243" Z="0" />
                  <Point X="-1.93431520462" Y="4.349291324615" />
                  <Point X="-1.934415191889" Y="4.349421630621" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0" />
                  <Point X="-2.786204850912" Y="4.24771355319" Z="0" />
                  <Point X="-2.786530971527" Y="4.247532367706" />
                  <Point X="-3.378373192787" Y="3.793773431182" Z="0" />
                  <Point X="-3.378669261932" Y="3.793546438217" />
                  <Point X="-3.378480689645" Y="3.793219821453" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0" />
                  <Point X="-2.994043754697" Y="3.054806330204" Z="0" />
                  <Point X="-2.99404001236" Y="3.054763555527" />
                  <Point X="-3.054733193755" Y="2.994070374131" Z="0" />
                  <Point X="-3.054763555527" Y="2.99404001236" />
                  <Point X="-3.140270135403" Y="3.001520944476" Z="0" />
                  <Point X="-3.14031291008" Y="3.001524686813" />
                  <Point X="-3.140639526844" Y="3.001713259101" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0" />
                  <Point X="-4.207468392491" Y="2.84663088882" Z="0" />
                  <Point X="-4.207675457001" Y="2.846364736557" />
                  <Point X="-4.547484162569" Y="2.264191035628" Z="0" />
                  <Point X="-4.547654151917" Y="2.263899803162" />
                  <Point X="-4.547228605986" Y="2.263573270321" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0" />
                  <Point X="-3.6668373034" Y="1.553732945859" Z="0" />
                  <Point X="-3.666822433472" Y="1.553704380989" />
                  <Point X="-3.691457584023" Y="1.494229862571" Z="0" />
                  <Point X="-3.691469907761" Y="1.494200110435" />
                  <Point X="-3.752865357399" Y="1.474842218578" Z="0" />
                  <Point X="-3.75289607048" Y="1.47483253479" />
                  <Point X="-3.753427870393" Y="1.474902547598" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0" />
                  <Point X="-4.972752604961" Y="1.038223003507" Z="0" />
                  <Point X="-4.9728307724" Y="1.037934541702" />
                  <Point X="-5.06028330493" Y="0.446938670442" Z="0" />
                  <Point X="-5.06032705307" Y="0.446643024683" />
                  <Point X="-5.059585169911" Y="0.446444237687" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0" />
                  <Point X="-3.548821735859" Y="0.029816582101" Z="0" />
                  <Point X="-3.548807859421" Y="0.029806951061" />
                  <Point X="-3.539561367273" Y="1.4903476E-05" Z="0" />
                  <Point X="-3.539556741714" Y="0" />
                  <Point X="-3.548803233862" Y="-0.029792047585" Z="0" />
                  <Point X="-3.548807859421" Y="-0.029806951061" />
                  <Point X="-3.576546859264" Y="-0.049059401033" Z="0" />
                  <Point X="-3.576560735703" Y="-0.049069032073" />
                  <Point X="-3.577302618861" Y="-0.049267819069" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0" />
                  <Point X="-4.982700556755" Y="-0.989399299189" Z="0" />
                  <Point X="-4.982661724091" Y="-0.989670813084" />
                  <Point X="-4.846971203327" Y="-1.520894142777" Z="0" />
                  <Point X="-4.846903324127" Y="-1.521159887314" />
                  <Point X="-4.846089451909" Y="-1.521052739024" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0" />
                  <Point X="-3.192689547062" Y="-1.322442010105" Z="0" />
                  <Point X="-3.192676305771" Y="-1.322449803352" />
                  <Point X="-3.200376949906" Y="-1.352182296574" Z="0" />
                  <Point X="-3.200380802155" Y="-1.352197170258" />
                  <Point X="-3.201032062054" Y="-1.352696899533" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0" />
                  <Point X="-4.209501650333" Y="-2.843640974998" Z="0" />
                  <Point X="-4.209354877472" Y="-2.843887090683" />
                  <Point X="-3.862412441969" Y="-3.299698023081" Z="0" />
                  <Point X="-3.862238883972" Y="-3.299926042557" />
                  <Point X="-3.861594184279" Y="-3.299553825021" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0" />
                  <Point X="-2.555499644876" Y="-2.572830824256" Z="0" />
                  <Point X="-2.555490970612" Y="-2.57283949852" />
                  <Point X="-2.555863188148" Y="-2.573484198213" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0" />
                  <Point X="-2.890680236816" Y="-4.177344523668" Z="0" />
                  <Point X="-2.890475511551" Y="-4.177502155304" />
                  <Point X="-2.451337432861" Y="-4.449405549049" Z="0" />
                  <Point X="-2.451117753983" Y="-4.449541568756" />
                  <Point X="-2.451005306244" Y="-4.449395024061" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0" />
                  <Point X="-1.968384979844" Y="-3.984170551658" Z="0" />
                  <Point X="-1.968255996704" Y="-3.984084367752" />
                  <Point X="-1.65882164371" Y="-4.004365788817" Z="0" />
                  <Point X="-1.658666849136" Y="-4.004375934601" />
                  <Point X="-1.425522609591" Y="-4.208838223696" Z="0" />
                  <Point X="-1.425405979156" Y="-4.208940505981" />
                  <Point X="-1.364908798993" Y="-4.513080085278" Z="0" />
                  <Point X="-1.364878535271" Y="-4.51323223114" />
                  <Point X="-1.364902645528" Y="-4.51341536665" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0" />
                  <Point X="-1.117549686909" Y="-4.955545532227" Z="0" />
                  <Point X="-1.117401838303" Y="-4.955583572388" />
                  <Point X="-0.817817947209" Y="-5.013733861208" Z="0" />
                  <Point X="-0.81766808033" Y="-5.013762950897" />
                  <Point X="-0.81746778968" Y="-5.013015455961" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0" />
                  <Point X="-0.253440943211" Y="-3.28299062705" Z="0" />
                  <Point X="-0.253359079361" Y="-3.282872676849" />
                  <Point X="-0.00012667954" Y="-3.204278685093" Z="0" />
                  <Point X="-1E-12" Y="-3.204239368439" />
                  <Point X="0.253232399821" Y="-3.282833360195" Z="0" />
                  <Point X="0.253359079361" Y="-3.282872676849" />
                  <Point X="0.417004916221" Y="-3.518655128717" Z="0" />
                  <Point X="0.417086780071" Y="-3.518773078918" />
                  <Point X="0.417287070721" Y="-3.519520573854" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0" />
                  <Point X="0.9979199875" Y="-4.981017757893" Z="0" />
                  <Point X="0.998010158539" Y="-4.981001377106" />
                  <Point X="1.176963453293" Y="-4.941774917841" Z="0" />
                  <Point X="1.177052974701" Y="-4.9417552948" />
                  <Point X="1.176943121105" Y="-4.940920874" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0" />
                  <Point X="1.011133108944" Y="-3.025449807405" Z="0" />
                  <Point X="1.01116001606" Y="-3.025326013565" />
                  <Point X="1.191053759277" Y="-2.875750265121" Z="0" />
                  <Point X="1.191143751144" Y="-2.875675439835" />
                  <Point X="1.424113913" Y="-2.854237313628" Z="0" />
                  <Point X="1.424230456352" Y="-2.854226589203" />
                  <Point X="1.637247335374" Y="-2.991176760435" Z="0" />
                  <Point X="1.637353897095" Y="-2.991245269775" />
                  <Point X="1.637866243482" Y="-2.99191297245" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0" />
                  <Point X="2.85717596519" Y="-4.200347187519" Z="0" />
                  <Point X="2.857273578644" Y="-4.200284004211" />
                  <Point X="3.046417984247" Y="-4.06518255949" Z="0" />
                  <Point X="3.04651260376" Y="-4.065114974976" />
                  <Point X="3.046124965906" Y="-4.064443566799" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0" />
                  <Point X="2.232230625868" Y="-2.506316226602" Z="0" />
                  <Point X="2.232211112976" Y="-2.506208181381" />
                  <Point X="2.326642957449" Y="-2.326779955983" Z="0" />
                  <Point X="2.326690196991" Y="-2.326690196991" />
                  <Point X="2.506118422389" Y="-2.232258352518" Z="0" />
                  <Point X="2.506208181381" Y="-2.232211112976" />
                  <Point X="2.722190576911" Y="-2.271217383623" Z="0" />
                  <Point X="2.722298622131" Y="-2.271236896515" />
                  <Point X="2.722970030308" Y="-2.271624534369" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0" />
                  <Point X="4.239634948254" Y="-2.798543760419" Z="0" />
                  <Point X="4.239722251892" Y="-2.798419713974" />
                  <Point X="4.399128802538" Y="-2.540475897312" Z="0" />
                  <Point X="4.399208545685" Y="-2.540346860886" />
                  <Point X="4.398605312824" Y="-2.53988398397" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0" />
                  <Point X="3.092312746644" Y="-1.458380643129" Z="0" />
                  <Point X="3.092262506485" Y="-1.458302497864" />
                  <Point X="3.10798359251" Y="-1.287457696617" Z="0" />
                  <Point X="3.107991456985" Y="-1.287372231483" />
                  <Point X="3.21768065691" Y="-1.155450176954" Z="0" />
                  <Point X="3.217735528946" Y="-1.15538418293" />
                  <Point X="3.399209077835" Y="-1.115940156341" Z="0" />
                  <Point X="3.399299860001" Y="-1.115920424461" />
                  <Point X="3.400053713441" Y="-1.116019671142" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0" />
                  <Point X="4.991396173" Y="-0.94460777697" Z="0" />
                  <Point X="4.991438388824" Y="-0.944422781467" />
                  <Point X="5.047986829281" Y="-0.569348926634" Z="0" />
                  <Point X="5.048015117645" Y="-0.569161295891" />
                  <Point X="5.047365086436" Y="-0.568987120561" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0" />
                  <Point X="3.655488969684" Y="-0.167364943579" Z="0" />
                  <Point X="3.655442714691" Y="-0.167338207364" />
                  <Point X="3.599964476705" Y="-0.096645895049" Z="0" />
                  <Point X="3.599936723709" Y="-0.096610531211" />
                  <Point X="3.581443977714" Y="-4.8305305E-05" Z="0" />
                  <Point X="3.581434726715" Y="-3.9E-11" />
                  <Point X="3.599927472711" Y="0.096562225945" Z="0" />
                  <Point X="3.599936723709" Y="0.096610531211" />
                  <Point X="3.655414961696" Y="0.167302843526" Z="0" />
                  <Point X="3.655442714691" Y="0.167338207364" />
                  <Point X="3.747906444669" Y="0.220783900663" Z="0" />
                  <Point X="3.747952699661" Y="0.220810636878" />
                  <Point X="3.74860273087" Y="0.220984812208" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0" />
                  <Point X="4.982145835638" Y="0.992228941679" Z="0" />
                  <Point X="4.982112884521" Y="0.992440581322" />
                  <Point X="4.88083612895" Y="1.408455042362" Z="0" />
                  <Point X="4.88078546524" Y="1.408663153648" />
                  <Point X="4.880076633215" Y="1.408569834113" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0" />
                  <Point X="3.36900769937" Y="1.234462357044" Z="0" />
                  <Point X="3.368960618973" Y="1.234468579292" />
                  <Point X="3.299353373051" Y="1.273651429772" Z="0" />
                  <Point X="3.299318552017" Y="1.273671030998" />
                  <Point X="3.251285024643" Y="1.346679881275" Z="0" />
                  <Point X="3.251260995865" Y="1.346716403961" />
                  <Point X="3.233600867867" Y="1.432306388617" Z="0" />
                  <Point X="3.233592033386" Y="1.432349205017" />
                  <Point X="3.255105223894" Y="1.509275220335" Z="0" />
                  <Point X="3.25511598587" Y="1.509313702583" />
                  <Point X="3.312869267941" Y="1.584657410085" Z="0" />
                  <Point X="3.312898159027" Y="1.584695100784" />
                  <Point X="3.313465365887" Y="1.5851303339" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0" />
                  <Point X="4.238288539171" Y="2.800574601531" Z="0" />
                  <Point X="4.23818397522" Y="2.800747394562" />
                  <Point X="4.002379123211" Y="3.128462645411" Z="0" />
                  <Point X="4.002261161804" Y="3.128626585007" />
                  <Point X="4.001514970183" Y="3.128195771098" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0" />
                  <Point X="2.429631798863" Y="2.245539909601" Z="0" />
                  <Point X="2.429591655731" Y="2.245529174805" />
                  <Point X="2.360201500535" Y="2.253896342278" Z="0" />
                  <Point X="2.360166788101" Y="2.253900527954" />
                  <Point X="2.296889726639" Y="2.296836593509" Z="0" />
                  <Point X="2.296858072281" Y="2.296858072281" />
                  <Point X="2.253922006726" Y="2.360135133743" Z="0" />
                  <Point X="2.253900527954" Y="2.360166788101" />
                  <Point X="2.245533360481" Y="2.429556943297" Z="0" />
                  <Point X="2.245529174805" Y="2.429591655731" />
                  <Point X="2.266988033056" Y="2.509837777019" Z="0" />
                  <Point X="2.266998767853" Y="2.509877920151" />
                  <Point X="2.267429581761" Y="2.510624111772" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0" />
                  <Point X="2.753685403347" Y="4.268898616076" Z="0" />
                  <Point X="2.753497838974" Y="4.269032001495" />
                  <Point X="2.355961587667" Y="4.500637542009" Z="0" />
                  <Point X="2.355762720108" Y="4.50075340271" />
                  <Point X="1.944237104297" Y="4.693210169554" Z="0" />
                  <Point X="1.944031238556" Y="4.693306446075" />
                  <Point X="1.516951227129" Y="4.848211521626" Z="0" />
                  <Point X="1.516737580299" Y="4.848289012909" />
                  <Point X="0.86288575235" Y="5.006149000645" Z="0" />
                  <Point X="0.862558662891" Y="5.006227970123" />
                  <Point X="0.193579218604" Y="5.076287984848" Z="0" />
                  <Point X="0.193244561553" Y="5.076323032379" />
                  <Point X="0.193147939272" Y="5.0759624331" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>