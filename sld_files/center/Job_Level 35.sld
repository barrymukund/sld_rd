<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#185" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2548" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="0" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476318359" Y="0.004716064453" />
                  <Width Value="9.783896972656" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.831607849121" Y="-4.51385546875" />
                  <Point X="0.563302062988" Y="-3.512524414063" />
                  <Point X="0.557721191406" Y="-3.497141845703" />
                  <Point X="0.542363037109" Y="-3.467377197266" />
                  <Point X="0.432699768066" Y="-3.309373046875" />
                  <Point X="0.378635528564" Y="-3.231476806641" />
                  <Point X="0.356751586914" Y="-3.209021484375" />
                  <Point X="0.330496398926" Y="-3.189777587891" />
                  <Point X="0.302495178223" Y="-3.175669189453" />
                  <Point X="0.132797439575" Y="-3.123001220703" />
                  <Point X="0.049136035919" Y="-3.097035888672" />
                  <Point X="0.020976638794" Y="-3.092766601562" />
                  <Point X="-0.008664840698" Y="-3.092766601562" />
                  <Point X="-0.036824237823" Y="-3.097035888672" />
                  <Point X="-0.206521972656" Y="-3.149703613281" />
                  <Point X="-0.290183227539" Y="-3.175669189453" />
                  <Point X="-0.31818460083" Y="-3.189777587891" />
                  <Point X="-0.344439788818" Y="-3.209021484375" />
                  <Point X="-0.366323577881" Y="-3.231476806641" />
                  <Point X="-0.475986968994" Y="-3.389480712891" />
                  <Point X="-0.530051208496" Y="-3.467377197266" />
                  <Point X="-0.538189025879" Y="-3.481573730469" />
                  <Point X="-0.550990112305" Y="-3.512524414063" />
                  <Point X="-0.683265441895" Y="-4.006182861328" />
                  <Point X="-0.916584533691" Y="-4.876941894531" />
                  <Point X="-0.984641784668" Y="-4.863731445312" />
                  <Point X="-1.079342163086" Y="-4.845350097656" />
                  <Point X="-1.24641809082" Y="-4.802362792969" />
                  <Point X="-1.214963012695" Y="-4.563438476562" />
                  <Point X="-1.214201171875" Y="-4.5479296875" />
                  <Point X="-1.216508666992" Y="-4.516224121094" />
                  <Point X="-1.257049438477" Y="-4.312412109375" />
                  <Point X="-1.277036132812" Y="-4.211932617188" />
                  <Point X="-1.287938354492" Y="-4.182966308594" />
                  <Point X="-1.304010375977" Y="-4.15512890625" />
                  <Point X="-1.323644775391" Y="-4.131204101562" />
                  <Point X="-1.479880737305" Y="-3.994188476562" />
                  <Point X="-1.556905517578" Y="-3.926639404297" />
                  <Point X="-1.583188476562" Y="-3.910295410156" />
                  <Point X="-1.612885742188" Y="-3.897994384766" />
                  <Point X="-1.64302734375" Y="-3.890966308594" />
                  <Point X="-1.850387451172" Y="-3.877375244141" />
                  <Point X="-1.952616577148" Y="-3.870674804688" />
                  <Point X="-1.98341796875" Y="-3.873708496094" />
                  <Point X="-2.014466674805" Y="-3.882028076172" />
                  <Point X="-2.042657714844" Y="-3.894801513672" />
                  <Point X="-2.21544140625" Y="-4.010251953125" />
                  <Point X="-2.300624267578" Y="-4.067169677734" />
                  <Point X="-2.312785644531" Y="-4.076821533203" />
                  <Point X="-2.335102539062" Y="-4.099461914062" />
                  <Point X="-2.409364990234" Y="-4.1962421875" />
                  <Point X="-2.480148925781" Y="-4.288489746094" />
                  <Point X="-2.662906982422" Y="-4.175330566406" />
                  <Point X="-2.801706787109" Y="-4.089389404297" />
                  <Point X="-3.070104736328" Y="-3.882731933594" />
                  <Point X="-3.104721923828" Y="-3.856077880859" />
                  <Point X="-2.922377441406" Y="-3.540248046875" />
                  <Point X="-2.423761230469" Y="-2.676619628906" />
                  <Point X="-2.412859130859" Y="-2.647653320312" />
                  <Point X="-2.406588134766" Y="-2.616126953125" />
                  <Point X="-2.405575439453" Y="-2.585192871094" />
                  <Point X="-2.414560058594" Y="-2.555575195312" />
                  <Point X="-2.428777099609" Y="-2.526746337891" />
                  <Point X="-2.4468046875" Y="-2.501589355469" />
                  <Point X="-2.464153320312" Y="-2.484240722656" />
                  <Point X="-2.489310302734" Y="-2.466213134766" />
                  <Point X="-2.518139160156" Y="-2.45199609375" />
                  <Point X="-2.547756835938" Y="-2.443011474609" />
                  <Point X="-2.578690917969" Y="-2.444024169922" />
                  <Point X="-2.610217285156" Y="-2.450295166016" />
                  <Point X="-2.63918359375" Y="-2.461197265625" />
                  <Point X="-3.064954345703" Y="-2.707016113281" />
                  <Point X="-3.818024658203" Y="-3.141801269531" />
                  <Point X="-3.973198974609" Y="-2.937933837891" />
                  <Point X="-4.082859375" Y="-2.793862304688" />
                  <Point X="-4.27527734375" Y="-2.471206787109" />
                  <Point X="-4.306142578125" Y="-2.419450439453" />
                  <Point X="-3.97837109375" Y="-2.167942382812" />
                  <Point X="-3.105954589844" Y="-1.498513549805" />
                  <Point X="-3.084577392578" Y="-1.475593505859" />
                  <Point X="-3.066612548828" Y="-1.448462280273" />
                  <Point X="-3.053856445312" Y="-1.419832641602" />
                  <Point X="-3.048696044922" Y="-1.399907958984" />
                  <Point X="-3.046151855469" Y="-1.390085327148" />
                  <Point X="-3.04334765625" Y="-1.359657348633" />
                  <Point X="-3.045556396484" Y="-1.327986328125" />
                  <Point X="-3.052557617188" Y="-1.298240966797" />
                  <Point X="-3.068640136719" Y="-1.272257202148" />
                  <Point X="-3.089472900391" Y="-1.248300537109" />
                  <Point X="-3.112972900391" Y="-1.228766723633" />
                  <Point X="-3.130710693359" Y="-1.218327270508" />
                  <Point X="-3.139455566406" Y="-1.213180297852" />
                  <Point X="-3.168718261719" Y="-1.201956420898" />
                  <Point X="-3.200605957031" Y="-1.195474853516" />
                  <Point X="-3.231929199219" Y="-1.194383789062" />
                  <Point X="-3.769424560547" Y="-1.265146484375" />
                  <Point X="-4.732102050781" Y="-1.391885375977" />
                  <Point X="-4.7911875" Y="-1.160567993164" />
                  <Point X="-4.834077636719" Y="-0.992654968262" />
                  <Point X="-4.884986328125" Y="-0.636706298828" />
                  <Point X="-4.892424804688" Y="-0.584698242188" />
                  <Point X="-4.526689941406" Y="-0.486700042725" />
                  <Point X="-3.532875976562" Y="-0.220408340454" />
                  <Point X="-3.517492919922" Y="-0.214827316284" />
                  <Point X="-3.487728759766" Y="-0.199469314575" />
                  <Point X="-3.469140136719" Y="-0.186567733765" />
                  <Point X="-3.459975830078" Y="-0.180207260132" />
                  <Point X="-3.437520751953" Y="-0.158323638916" />
                  <Point X="-3.418276611328" Y="-0.132068145752" />
                  <Point X="-3.404168457031" Y="-0.104067070007" />
                  <Point X="-3.397972167969" Y="-0.084102531433" />
                  <Point X="-3.394917480469" Y="-0.074259986877" />
                  <Point X="-3.390647949219" Y="-0.046100437164" />
                  <Point X="-3.390647949219" Y="-0.016459566116" />
                  <Point X="-3.394917480469" Y="0.011700286865" />
                  <Point X="-3.401113769531" Y="0.031664672852" />
                  <Point X="-3.404168457031" Y="0.041507217407" />
                  <Point X="-3.418276367188" Y="0.069507537842" />
                  <Point X="-3.437520263672" Y="0.095763183594" />
                  <Point X="-3.459975585938" Y="0.117647262573" />
                  <Point X="-3.478564208984" Y="0.130548843384" />
                  <Point X="-3.487006103516" Y="0.135776351929" />
                  <Point X="-3.511738525391" Y="0.149357192993" />
                  <Point X="-3.532875976562" Y="0.157848342896" />
                  <Point X="-4.022828369141" Y="0.289130645752" />
                  <Point X="-4.89181640625" Y="0.521975219727" />
                  <Point X="-4.85212890625" Y="0.790179870605" />
                  <Point X="-4.824487792969" Y="0.976975402832" />
                  <Point X="-4.722010253906" Y="1.355148681641" />
                  <Point X="-4.703551269531" Y="1.423267822266" />
                  <Point X="-4.478056152344" Y="1.393580932617" />
                  <Point X="-3.765666259766" Y="1.29979284668" />
                  <Point X="-3.744986083984" Y="1.299341674805" />
                  <Point X="-3.723424560547" Y="1.301228149414" />
                  <Point X="-3.703137695312" Y="1.305263671875" />
                  <Point X="-3.661995117188" Y="1.318235839844" />
                  <Point X="-3.641711669922" Y="1.324631225586" />
                  <Point X="-3.622779052734" Y="1.332961669922" />
                  <Point X="-3.604034667969" Y="1.343783569336" />
                  <Point X="-3.587353271484" Y="1.356014892578" />
                  <Point X="-3.57371484375" Y="1.371566772461" />
                  <Point X="-3.561300292969" Y="1.38929675293" />
                  <Point X="-3.551351318359" Y="1.407431030273" />
                  <Point X="-3.534842529297" Y="1.447286499023" />
                  <Point X="-3.526703857422" Y="1.466935424805" />
                  <Point X="-3.520915527344" Y="1.486794311523" />
                  <Point X="-3.517157226562" Y="1.508109130859" />
                  <Point X="-3.5158046875" Y="1.528749511719" />
                  <Point X="-3.518951171875" Y="1.549193359375" />
                  <Point X="-3.524552978516" Y="1.570099487305" />
                  <Point X="-3.532049804688" Y="1.589377563477" />
                  <Point X="-3.551969238281" Y="1.627642578125" />
                  <Point X="-3.561789550781" Y="1.646507080078" />
                  <Point X="-3.57328125" Y="1.663705932617" />
                  <Point X="-3.587193847656" Y="1.680286376953" />
                  <Point X="-3.602135986328" Y="1.694590209961" />
                  <Point X="-3.883174072266" Y="1.91023815918" />
                  <Point X="-4.351859863281" Y="2.269874267578" />
                  <Point X="-4.1885546875" Y="2.549655029297" />
                  <Point X="-4.081153320312" Y="2.733659912109" />
                  <Point X="-3.8096953125" Y="3.082580810547" />
                  <Point X="-3.750504882813" Y="3.158661865234" />
                  <Point X="-3.644187011719" Y="3.097279052734" />
                  <Point X="-3.206656982422" Y="2.844670898438" />
                  <Point X="-3.187724121094" Y="2.836340332031" />
                  <Point X="-3.167081787109" Y="2.829831787109" />
                  <Point X="-3.146794433594" Y="2.825796386719" />
                  <Point X="-3.089494140625" Y="2.820783203125" />
                  <Point X="-3.061245117188" Y="2.818311767578" />
                  <Point X="-3.040564697266" Y="2.818762939453" />
                  <Point X="-3.019106201172" Y="2.821588134766" />
                  <Point X="-2.999014892578" Y="2.826504638672" />
                  <Point X="-2.980463867188" Y="2.835652832031" />
                  <Point X="-2.962209472656" Y="2.847281982422" />
                  <Point X="-2.946077392578" Y="2.860229492188" />
                  <Point X="-2.905405273438" Y="2.900901611328" />
                  <Point X="-2.885353759766" Y="2.920952880859" />
                  <Point X="-2.872406738281" Y="2.937084228516" />
                  <Point X="-2.86077734375" Y="2.955338623047" />
                  <Point X="-2.851628662109" Y="2.973890136719" />
                  <Point X="-2.846712158203" Y="2.993981933594" />
                  <Point X="-2.843886962891" Y="3.015440673828" />
                  <Point X="-2.843435791016" Y="3.036120849609" />
                  <Point X="-2.848448974609" Y="3.093421142578" />
                  <Point X="-2.850920410156" Y="3.121670410156" />
                  <Point X="-2.854955810547" Y="3.141957763672" />
                  <Point X="-2.861464355469" Y="3.162600097656" />
                  <Point X="-2.869794921875" Y="3.181532958984" />
                  <Point X="-2.994331298828" Y="3.397236328125" />
                  <Point X="-3.183333007812" Y="3.724596435547" />
                  <Point X="-2.887680175781" Y="3.951270507812" />
                  <Point X="-2.700626464844" Y="4.094683349609" />
                  <Point X="-2.273089111328" Y="4.332213867188" />
                  <Point X="-2.167036865234" Y="4.391134277344" />
                  <Point X="-2.043195678711" Y="4.229741210938" />
                  <Point X="-2.028891723633" Y="4.214798828125" />
                  <Point X="-2.012311889648" Y="4.20088671875" />
                  <Point X="-1.99511328125" Y="4.189395019531" />
                  <Point X="-1.931338378906" Y="4.156195800781" />
                  <Point X="-1.899897094727" Y="4.139828125" />
                  <Point X="-1.880619506836" Y="4.132331542969" />
                  <Point X="-1.859713012695" Y="4.126729492187" />
                  <Point X="-1.839270141602" Y="4.123582519531" />
                  <Point X="-1.818630737305" Y="4.124935058594" />
                  <Point X="-1.797315307617" Y="4.128692871094" />
                  <Point X="-1.777453613281" Y="4.134481933594" />
                  <Point X="-1.711027709961" Y="4.161997070312" />
                  <Point X="-1.678279663086" Y="4.175561523437" />
                  <Point X="-1.660145385742" Y="4.185510253906" />
                  <Point X="-1.642415649414" Y="4.197924804688" />
                  <Point X="-1.626863891602" Y="4.211563476563" />
                  <Point X="-1.61463269043" Y="4.228244628906" />
                  <Point X="-1.603810668945" Y="4.246989257813" />
                  <Point X="-1.59548046875" Y="4.265921386719" />
                  <Point X="-1.573859985352" Y="4.3344921875" />
                  <Point X="-1.563201171875" Y="4.368297851562" />
                  <Point X="-1.559165649414" Y="4.388584960938" />
                  <Point X="-1.557279174805" Y="4.410146484375" />
                  <Point X="-1.55773034668" Y="4.430826660156" />
                  <Point X="-1.571888793945" Y="4.538370605469" />
                  <Point X="-1.584201904297" Y="4.631897949219" />
                  <Point X="-1.19178894043" Y="4.741916992188" />
                  <Point X="-0.949634765625" Y="4.80980859375" />
                  <Point X="-0.431335845947" Y="4.870467773438" />
                  <Point X="-0.294710784912" Y="4.886457519531" />
                  <Point X="-0.26333682251" Y="4.769368164063" />
                  <Point X="-0.133903305054" Y="4.286315429688" />
                  <Point X="-0.121129844666" Y="4.258124023438" />
                  <Point X="-0.10327155304" Y="4.231397460938" />
                  <Point X="-0.0821145401" Y="4.20880859375" />
                  <Point X="-0.054819446564" Y="4.19421875" />
                  <Point X="-0.024381370544" Y="4.183886230469" />
                  <Point X="0.006155907631" Y="4.178844238281" />
                  <Point X="0.03669316864" Y="4.183886230469" />
                  <Point X="0.067131248474" Y="4.19421875" />
                  <Point X="0.094426467896" Y="4.20880859375" />
                  <Point X="0.115583503723" Y="4.231397460938" />
                  <Point X="0.133441802979" Y="4.258124023438" />
                  <Point X="0.146215255737" Y="4.286315429688" />
                  <Point X="0.210026275635" Y="4.524461425781" />
                  <Point X="0.307419372559" Y="4.8879375" />
                  <Point X="0.632598510742" Y="4.853882324219" />
                  <Point X="0.844041320801" Y="4.831738769531" />
                  <Point X="1.272848510742" Y="4.728211425781" />
                  <Point X="1.481030395508" Y="4.677949707031" />
                  <Point X="1.759650512695" Y="4.576892578125" />
                  <Point X="1.89464440918" Y="4.527929199219" />
                  <Point X="2.164534912109" Y="4.401710449219" />
                  <Point X="2.294572753906" Y="4.340895507812" />
                  <Point X="2.555325195312" Y="4.188980957031" />
                  <Point X="2.680978759766" Y="4.115774902344" />
                  <Point X="2.926878417969" Y="3.940904541016" />
                  <Point X="2.943260009766" Y="3.929254638672" />
                  <Point X="2.724691894531" Y="3.550683349609" />
                  <Point X="2.147581054688" Y="2.551097900391" />
                  <Point X="2.142075927734" Y="2.539931152344" />
                  <Point X="2.133076904297" Y="2.516056640625" />
                  <Point X="2.118696777344" Y="2.462281494141" />
                  <Point X="2.111607177734" Y="2.435770263672" />
                  <Point X="2.108619384766" Y="2.417934570312" />
                  <Point X="2.107728027344" Y="2.380953125" />
                  <Point X="2.113335205078" Y="2.334453125" />
                  <Point X="2.116099365234" Y="2.311528320312" />
                  <Point X="2.121441894531" Y="2.289605712891" />
                  <Point X="2.129708007812" Y="2.267516601562" />
                  <Point X="2.140071044922" Y="2.247471191406" />
                  <Point X="2.16884375" Y="2.205067626953" />
                  <Point X="2.183028808594" Y="2.184162597656" />
                  <Point X="2.194465332031" Y="2.170327880859" />
                  <Point X="2.221599121094" Y="2.145592529297" />
                  <Point X="2.264002685547" Y="2.116819824219" />
                  <Point X="2.284907714844" Y="2.102635009766" />
                  <Point X="2.304952392578" Y="2.092272216797" />
                  <Point X="2.327040283203" Y="2.084006347656" />
                  <Point X="2.348963623047" Y="2.078663574219" />
                  <Point X="2.395463867188" Y="2.073056396484" />
                  <Point X="2.418388427734" Y="2.070291992188" />
                  <Point X="2.436467285156" Y="2.069845703125" />
                  <Point X="2.473206542969" Y="2.074171142578" />
                  <Point X="2.526981689453" Y="2.088551513672" />
                  <Point X="2.553492919922" Y="2.095640869141" />
                  <Point X="2.565283447266" Y="2.099638183594" />
                  <Point X="2.588533935547" Y="2.110145019531" />
                  <Point X="3.081331787109" Y="2.394661865234" />
                  <Point X="3.967326416016" Y="2.906191162109" />
                  <Point X="4.048738037109" Y="2.793047363281" />
                  <Point X="4.123272949219" Y="2.689460205078" />
                  <Point X="4.260357910156" Y="2.462926513672" />
                  <Point X="4.262198730469" Y="2.459884277344" />
                  <Point X="3.990604248047" Y="2.251482177734" />
                  <Point X="3.230783935547" Y="1.668451293945" />
                  <Point X="3.221424560547" Y="1.660241455078" />
                  <Point X="3.203973632812" Y="1.641627807617" />
                  <Point X="3.165271484375" Y="1.591138061523" />
                  <Point X="3.14619140625" Y="1.566246459961" />
                  <Point X="3.136604980469" Y="1.550910644531" />
                  <Point X="3.121629882812" Y="1.51708605957" />
                  <Point X="3.107213378906" Y="1.465535888672" />
                  <Point X="3.100105957031" Y="1.440121704102" />
                  <Point X="3.096652587891" Y="1.417823242188" />
                  <Point X="3.095836425781" Y="1.394252929688" />
                  <Point X="3.097739501953" Y="1.371768066406" />
                  <Point X="3.109573974609" Y="1.314411865234" />
                  <Point X="3.115408447266" Y="1.286135253906" />
                  <Point X="3.120679931641" Y="1.268977661133" />
                  <Point X="3.136282470703" Y="1.235740478516" />
                  <Point X="3.168470947266" Y="1.186815185547" />
                  <Point X="3.184340087891" Y="1.162695068359" />
                  <Point X="3.198893554688" Y="1.145450439453" />
                  <Point X="3.216137451172" Y="1.129360595703" />
                  <Point X="3.234347167969" Y="1.116034912109" />
                  <Point X="3.280992675781" Y="1.08977734375" />
                  <Point X="3.303989257812" Y="1.076832397461" />
                  <Point X="3.320521728516" Y="1.069501342773" />
                  <Point X="3.356118408203" Y="1.059438598633" />
                  <Point X="3.419186279297" Y="1.051103271484" />
                  <Point X="3.450279052734" Y="1.046994018555" />
                  <Point X="3.462702392578" Y="1.046175048828" />
                  <Point X="3.488203613281" Y="1.04698449707" />
                  <Point X="3.956328613281" Y="1.108614379883" />
                  <Point X="4.776839355469" Y="1.21663659668" />
                  <Point X="4.813924316406" Y="1.064302978516" />
                  <Point X="4.845936035156" Y="0.932809387207" />
                  <Point X="4.889135742188" Y="0.655344909668" />
                  <Point X="4.890864746094" Y="0.644238525391" />
                  <Point X="4.587350585938" Y="0.562912109375" />
                  <Point X="3.716579833984" Y="0.329589935303" />
                  <Point X="3.704791259766" Y="0.325586608887" />
                  <Point X="3.681545654297" Y="0.315067993164" />
                  <Point X="3.619583251953" Y="0.279252441406" />
                  <Point X="3.589035644531" Y="0.261595428467" />
                  <Point X="3.574310791016" Y="0.251095916748" />
                  <Point X="3.547531005859" Y="0.225576461792" />
                  <Point X="3.510353515625" Y="0.178203735352" />
                  <Point X="3.492025146484" Y="0.154848815918" />
                  <Point X="3.48030078125" Y="0.135568695068" />
                  <Point X="3.470527099609" Y="0.114105232239" />
                  <Point X="3.463680908203" Y="0.092604270935" />
                  <Point X="3.451288330078" Y="0.027895412445" />
                  <Point X="3.445178710938" Y="-0.004006377697" />
                  <Point X="3.443483154297" Y="-0.021875295639" />
                  <Point X="3.445178710938" Y="-0.058553779602" />
                  <Point X="3.457571289062" Y="-0.123262634277" />
                  <Point X="3.463680908203" Y="-0.155164276123" />
                  <Point X="3.470527099609" Y="-0.176664779663" />
                  <Point X="3.48030078125" Y="-0.198128555298" />
                  <Point X="3.492024902344" Y="-0.217409118652" />
                  <Point X="3.529202148438" Y="-0.264781860352" />
                  <Point X="3.547530761719" Y="-0.288136779785" />
                  <Point X="3.559999511719" Y="-0.301236602783" />
                  <Point X="3.589035644531" Y="-0.324155578613" />
                  <Point X="3.650998046875" Y="-0.359970977783" />
                  <Point X="3.681545654297" Y="-0.377627990723" />
                  <Point X="3.692710205078" Y="-0.383139038086" />
                  <Point X="3.716579833984" Y="-0.392149932861" />
                  <Point X="4.145871582031" Y="-0.507178375244" />
                  <Point X="4.891472167969" Y="-0.706961486816" />
                  <Point X="4.872896484375" Y="-0.830171508789" />
                  <Point X="4.855022460938" Y="-0.948725646973" />
                  <Point X="4.801173828125" Y="-1.18469909668" />
                  <Point X="4.434230957031" Y="-1.136390136719" />
                  <Point X="3.424382080078" Y="-1.003441040039" />
                  <Point X="3.408035644531" Y="-1.002710327148" />
                  <Point X="3.374658691406" Y="-1.005508728027" />
                  <Point X="3.253048583984" Y="-1.031941162109" />
                  <Point X="3.193094482422" Y="-1.044972412109" />
                  <Point X="3.163974365234" Y="-1.056596801758" />
                  <Point X="3.136147705078" Y="-1.073489013672" />
                  <Point X="3.112397460938" Y="-1.093960083008" />
                  <Point X="3.038891845703" Y="-1.182364379883" />
                  <Point X="3.002653320312" Y="-1.225947998047" />
                  <Point X="2.987932373047" Y="-1.250330566406" />
                  <Point X="2.976589111328" Y="-1.277715942383" />
                  <Point X="2.969757568359" Y="-1.305365478516" />
                  <Point X="2.959222412109" Y="-1.419853027344" />
                  <Point X="2.954028564453" Y="-1.476295776367" />
                  <Point X="2.956347412109" Y="-1.507564819336" />
                  <Point X="2.964079101562" Y="-1.539185791016" />
                  <Point X="2.976450439453" Y="-1.567996582031" />
                  <Point X="3.043751220703" Y="-1.672678588867" />
                  <Point X="3.076930664062" Y="-1.724287109375" />
                  <Point X="3.086932373047" Y="-1.73723815918" />
                  <Point X="3.110628417969" Y="-1.760909179688" />
                  <Point X="3.509013916016" Y="-2.066601318359" />
                  <Point X="4.213122558594" Y="-2.606883056641" />
                  <Point X="4.1751953125" Y="-2.668254882812" />
                  <Point X="4.124810546875" Y="-2.749785400391" />
                  <Point X="4.028981445312" Y="-2.885945068359" />
                  <Point X="3.700361328125" Y="-2.696216064453" />
                  <Point X="2.800954589844" Y="-2.176943115234" />
                  <Point X="2.786131591797" Y="-2.170012451172" />
                  <Point X="2.754224609375" Y="-2.159825195312" />
                  <Point X="2.609489013672" Y="-2.133686035156" />
                  <Point X="2.538134277344" Y="-2.120799560547" />
                  <Point X="2.506781982422" Y="-2.120395263672" />
                  <Point X="2.474609130859" Y="-2.125353027344" />
                  <Point X="2.444833251953" Y="-2.135176757812" />
                  <Point X="2.324593505859" Y="-2.198458007812" />
                  <Point X="2.265315185547" Y="-2.229655761719" />
                  <Point X="2.242384033203" Y="-2.246549072266" />
                  <Point X="2.221424316406" Y="-2.267509033203" />
                  <Point X="2.204531738281" Y="-2.290439453125" />
                  <Point X="2.141250488281" Y="-2.410678955078" />
                  <Point X="2.110052734375" Y="-2.469957519531" />
                  <Point X="2.100229003906" Y="-2.499733398438" />
                  <Point X="2.095271240234" Y="-2.531906005859" />
                  <Point X="2.095675537109" Y="-2.563258300781" />
                  <Point X="2.121814453125" Y="-2.707993652344" />
                  <Point X="2.134701171875" Y="-2.779348632812" />
                  <Point X="2.138985595703" Y="-2.795141845703" />
                  <Point X="2.151819091797" Y="-2.826078613281" />
                  <Point X="2.407821777344" Y="-3.26948828125" />
                  <Point X="2.861283447266" Y="-4.054906738281" />
                  <Point X="2.841639160156" Y="-4.068938232422" />
                  <Point X="2.781853515625" Y="-4.111641601562" />
                  <Point X="2.701765136719" Y="-4.163481445312" />
                  <Point X="2.444876708984" Y="-3.828697998047" />
                  <Point X="1.758546020508" Y="-2.934255126953" />
                  <Point X="1.747503540039" Y="-2.922179443359" />
                  <Point X="1.721924072266" Y="-2.900557373047" />
                  <Point X="1.57917578125" Y="-2.808783447266" />
                  <Point X="1.50880078125" Y="-2.763538574219" />
                  <Point X="1.479989746094" Y="-2.751167236328" />
                  <Point X="1.448368408203" Y="-2.743435546875" />
                  <Point X="1.417099487305" Y="-2.741116699219" />
                  <Point X="1.260979980469" Y="-2.755482910156" />
                  <Point X="1.184012573242" Y="-2.762565673828" />
                  <Point X="1.156362670898" Y="-2.769397460938" />
                  <Point X="1.128977661133" Y="-2.780740722656" />
                  <Point X="1.104595825195" Y="-2.795461181641" />
                  <Point X="0.984044433594" Y="-2.895695800781" />
                  <Point X="0.924612243652" Y="-2.945111572266" />
                  <Point X="0.904141479492" Y="-2.968861572266" />
                  <Point X="0.887249084473" Y="-2.996688232422" />
                  <Point X="0.875624328613" Y="-3.025808837891" />
                  <Point X="0.83958001709" Y="-3.191641113281" />
                  <Point X="0.821810058594" Y="-3.273396728516" />
                  <Point X="0.819724487305" Y="-3.289627441406" />
                  <Point X="0.819742248535" Y="-3.323120117188" />
                  <Point X="0.892291381836" Y="-3.874186035156" />
                  <Point X="1.022065307617" Y="-4.859915527344" />
                  <Point X="0.975678161621" Y="-4.870083007812" />
                  <Point X="0.929315429688" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.05843737793" Y="-4.752634765625" />
                  <Point X="-1.141246459961" Y="-4.731328613281" />
                  <Point X="-1.120775634766" Y="-4.575838378906" />
                  <Point X="-1.120077514648" Y="-4.568099609375" />
                  <Point X="-1.119451782227" Y="-4.541033691406" />
                  <Point X="-1.121759277344" Y="-4.509328125" />
                  <Point X="-1.123334106445" Y="-4.497690429688" />
                  <Point X="-1.16387487793" Y="-4.293878417969" />
                  <Point X="-1.183861572266" Y="-4.193398925781" />
                  <Point X="-1.188125" Y="-4.17846875" />
                  <Point X="-1.19902734375" Y="-4.149502441406" />
                  <Point X="-1.205666015625" Y="-4.135466308594" />
                  <Point X="-1.221738037109" Y="-4.10762890625" />
                  <Point X="-1.23057409668" Y="-4.094861816406" />
                  <Point X="-1.250208618164" Y="-4.070937011719" />
                  <Point X="-1.261006958008" Y="-4.059779296875" />
                  <Point X="-1.417242675781" Y="-3.922763671875" />
                  <Point X="-1.494267578125" Y="-3.855214599609" />
                  <Point X="-1.506738647461" Y="-3.845965576172" />
                  <Point X="-1.533021606445" Y="-3.829621582031" />
                  <Point X="-1.546833374023" Y="-3.822526855469" />
                  <Point X="-1.576530761719" Y="-3.810225830078" />
                  <Point X="-1.591313354492" Y="-3.805476074219" />
                  <Point X="-1.621454956055" Y="-3.798447998047" />
                  <Point X="-1.636814086914" Y="-3.796169677734" />
                  <Point X="-1.844174194336" Y="-3.782578613281" />
                  <Point X="-1.946403320312" Y="-3.775878173828" />
                  <Point X="-1.961928100586" Y="-3.776132324219" />
                  <Point X="-1.992729614258" Y="-3.779166015625" />
                  <Point X="-2.008006103516" Y="-3.781945556641" />
                  <Point X="-2.03905480957" Y="-3.790265136719" />
                  <Point X="-2.053674560547" Y="-3.795496337891" />
                  <Point X="-2.081865478516" Y="-3.808269775391" />
                  <Point X="-2.095437011719" Y="-3.815812011719" />
                  <Point X="-2.268220703125" Y="-3.931262451172" />
                  <Point X="-2.353403564453" Y="-3.988180175781" />
                  <Point X="-2.359681640625" Y="-3.992757080078" />
                  <Point X="-2.380442382812" Y="-4.010131347656" />
                  <Point X="-2.402759277344" Y="-4.032771728516" />
                  <Point X="-2.410470947266" Y="-4.041629394531" />
                  <Point X="-2.484733398438" Y="-4.138409667969" />
                  <Point X="-2.503202636719" Y="-4.162479492188" />
                  <Point X="-2.612895751953" Y="-4.094560058594" />
                  <Point X="-2.747582275391" Y="-4.011165771484" />
                  <Point X="-2.980862792969" Y="-3.831547607422" />
                  <Point X="-2.840104980469" Y="-3.587748046875" />
                  <Point X="-2.341488769531" Y="-2.724119628906" />
                  <Point X="-2.334850097656" Y="-2.710083251953" />
                  <Point X="-2.323947998047" Y="-2.681116943359" />
                  <Point X="-2.319684570312" Y="-2.666187011719" />
                  <Point X="-2.313413574219" Y="-2.634660644531" />
                  <Point X="-2.311638916016" Y="-2.619235351562" />
                  <Point X="-2.310626220703" Y="-2.588301269531" />
                  <Point X="-2.314666259766" Y="-2.557615234375" />
                  <Point X="-2.323650878906" Y="-2.527997558594" />
                  <Point X="-2.329357421875" Y="-2.513557128906" />
                  <Point X="-2.343574462891" Y="-2.484728271484" />
                  <Point X="-2.351557128906" Y="-2.47141015625" />
                  <Point X="-2.369584716797" Y="-2.446253173828" />
                  <Point X="-2.379629638672" Y="-2.434414306641" />
                  <Point X="-2.396978271484" Y="-2.417065673828" />
                  <Point X="-2.408817138672" Y="-2.407020751953" />
                  <Point X="-2.433974121094" Y="-2.388993164062" />
                  <Point X="-2.447292236328" Y="-2.381010498047" />
                  <Point X="-2.47612109375" Y="-2.366793457031" />
                  <Point X="-2.490561523438" Y="-2.361086914062" />
                  <Point X="-2.520179199219" Y="-2.352102294922" />
                  <Point X="-2.550865234375" Y="-2.348062255859" />
                  <Point X="-2.581799316406" Y="-2.349074951172" />
                  <Point X="-2.597224609375" Y="-2.350849609375" />
                  <Point X="-2.628750976562" Y="-2.357120605469" />
                  <Point X="-2.643680908203" Y="-2.361384033203" />
                  <Point X="-2.672647216797" Y="-2.372286132812" />
                  <Point X="-2.68668359375" Y="-2.378924804688" />
                  <Point X="-3.112454345703" Y="-2.624743652344" />
                  <Point X="-3.793089599609" Y="-3.017708496094" />
                  <Point X="-3.89760546875" Y="-2.880395751953" />
                  <Point X="-4.004014892578" Y="-2.740595458984" />
                  <Point X="-4.181265136719" Y="-2.443373779297" />
                  <Point X="-3.920538818359" Y="-2.243311035156" />
                  <Point X="-3.048122314453" Y="-1.573882080078" />
                  <Point X="-3.036481933594" Y="-1.563309814453" />
                  <Point X="-3.015104736328" Y="-1.540389526367" />
                  <Point X="-3.005367675781" Y="-1.528041870117" />
                  <Point X="-2.987402832031" Y="-1.500910522461" />
                  <Point X="-2.979836181641" Y="-1.487125976562" />
                  <Point X="-2.967080078125" Y="-1.458496337891" />
                  <Point X="-2.961890869141" Y="-1.443651245117" />
                  <Point X="-2.95673046875" Y="-1.4237265625" />
                  <Point X="-2.951552734375" Y="-1.398803466797" />
                  <Point X="-2.948748535156" Y="-1.368375488281" />
                  <Point X="-2.948577880859" Y="-1.353047973633" />
                  <Point X="-2.950786621094" Y="-1.321377075195" />
                  <Point X="-2.953083251953" Y="-1.306220825195" />
                  <Point X="-2.960084472656" Y="-1.276475463867" />
                  <Point X="-2.971778808594" Y="-1.248243286133" />
                  <Point X="-2.987861328125" Y="-1.222259521484" />
                  <Point X="-2.996953857422" Y="-1.209918701172" />
                  <Point X="-3.017786621094" Y="-1.185962036133" />
                  <Point X="-3.028746337891" Y="-1.175243896484" />
                  <Point X="-3.052246337891" Y="-1.155710083008" />
                  <Point X="-3.064787353516" Y="-1.146894042969" />
                  <Point X="-3.082525146484" Y="-1.136454711914" />
                  <Point X="-3.105434326172" Y="-1.124480957031" />
                  <Point X="-3.134697021484" Y="-1.113257202148" />
                  <Point X="-3.149795410156" Y="-1.108860107422" />
                  <Point X="-3.181683105469" Y="-1.102378540039" />
                  <Point X="-3.197298828125" Y="-1.100532470703" />
                  <Point X="-3.228622070312" Y="-1.09944140625" />
                  <Point X="-3.244329101562" Y="-1.100196411133" />
                  <Point X="-3.781824462891" Y="-1.170959228516" />
                  <Point X="-4.660920410156" Y="-1.286694458008" />
                  <Point X="-4.699142578125" Y="-1.137057006836" />
                  <Point X="-4.740762207031" Y="-0.974118408203" />
                  <Point X="-4.786452636719" Y="-0.65465435791" />
                  <Point X="-4.502102050781" Y="-0.578463012695" />
                  <Point X="-3.508288085938" Y="-0.312171356201" />
                  <Point X="-3.500476074219" Y="-0.309712524414" />
                  <Point X="-3.473931152344" Y="-0.29925112915" />
                  <Point X="-3.444166992188" Y="-0.283893005371" />
                  <Point X="-3.433561523438" Y="-0.277513519287" />
                  <Point X="-3.414972900391" Y="-0.264612030029" />
                  <Point X="-3.393671875" Y="-0.248242538452" />
                  <Point X="-3.371216796875" Y="-0.226358856201" />
                  <Point X="-3.3608984375" Y="-0.214484481812" />
                  <Point X="-3.341654296875" Y="-0.188228912354" />
                  <Point X="-3.333436767578" Y="-0.174814056396" />
                  <Point X="-3.319328613281" Y="-0.146813064575" />
                  <Point X="-3.313437988281" Y="-0.132226608276" />
                  <Point X="-3.307241699219" Y="-0.11226209259" />
                  <Point X="-3.300990966797" Y="-0.08850100708" />
                  <Point X="-3.296721435547" Y="-0.060341575623" />
                  <Point X="-3.295647949219" Y="-0.046100379944" />
                  <Point X="-3.295647949219" Y="-0.016459621429" />
                  <Point X="-3.296721435547" Y="-0.002218573809" />
                  <Point X="-3.300990966797" Y="0.025941156387" />
                  <Point X="-3.304187011719" Y="0.039859989166" />
                  <Point X="-3.310383300781" Y="0.059824504852" />
                  <Point X="-3.319328857422" Y="0.084253501892" />
                  <Point X="-3.333436767578" Y="0.112253761292" />
                  <Point X="-3.341653564453" Y="0.125667572021" />
                  <Point X="-3.360897460938" Y="0.151923294067" />
                  <Point X="-3.371216064453" Y="0.163798126221" />
                  <Point X="-3.393671386719" Y="0.185682250977" />
                  <Point X="-3.405808349609" Y="0.195691558838" />
                  <Point X="-3.424396972656" Y="0.208593063354" />
                  <Point X="-3.441280761719" Y="0.219048095703" />
                  <Point X="-3.466013183594" Y="0.232628952026" />
                  <Point X="-3.476326416016" Y="0.2375103302" />
                  <Point X="-3.497463867188" Y="0.246001449585" />
                  <Point X="-3.508288085938" Y="0.249611358643" />
                  <Point X="-3.998240478516" Y="0.380893676758" />
                  <Point X="-4.785446289062" Y="0.591824707031" />
                  <Point X="-4.75815234375" Y="0.776273742676" />
                  <Point X="-4.731330566406" Y="0.957532653809" />
                  <Point X="-4.633586425781" Y="1.318237182617" />
                  <Point X="-4.490456054688" Y="1.299393676758" />
                  <Point X="-3.778066162109" Y="1.20560559082" />
                  <Point X="-3.76773828125" Y="1.204815429688" />
                  <Point X="-3.747058105469" Y="1.204364379883" />
                  <Point X="-3.736705810547" Y="1.204703125" />
                  <Point X="-3.715144287109" Y="1.206589599609" />
                  <Point X="-3.704890136719" Y="1.208053710938" />
                  <Point X="-3.684603271484" Y="1.212089233398" />
                  <Point X="-3.674570800781" Y="1.214660522461" />
                  <Point X="-3.633428222656" Y="1.22763269043" />
                  <Point X="-3.613144775391" Y="1.234028076172" />
                  <Point X="-3.603451171875" Y="1.237676513672" />
                  <Point X="-3.584518554688" Y="1.246006958008" />
                  <Point X="-3.575279541016" Y="1.250688842773" />
                  <Point X="-3.55653515625" Y="1.261510864258" />
                  <Point X="-3.547860107422" Y="1.267171264648" />
                  <Point X="-3.531178710938" Y="1.279402709961" />
                  <Point X="-3.515927978516" Y="1.293377563477" />
                  <Point X="-3.502289550781" Y="1.308929443359" />
                  <Point X="-3.495895263672" Y="1.317077392578" />
                  <Point X="-3.483480712891" Y="1.334807373047" />
                  <Point X="-3.478011474609" Y="1.343602172852" />
                  <Point X="-3.4680625" Y="1.361736572266" />
                  <Point X="-3.463582763672" Y="1.371075927734" />
                  <Point X="-3.447073974609" Y="1.410931396484" />
                  <Point X="-3.438935302734" Y="1.430580322266" />
                  <Point X="-3.435499023438" Y="1.44035168457" />
                  <Point X="-3.429710693359" Y="1.460210693359" />
                  <Point X="-3.427358642578" Y="1.470298095703" />
                  <Point X="-3.423600341797" Y="1.491612915039" />
                  <Point X="-3.422360595703" Y="1.501897216797" />
                  <Point X="-3.421008056641" Y="1.522537597656" />
                  <Point X="-3.42191015625" Y="1.543200683594" />
                  <Point X="-3.425056640625" Y="1.56364465332" />
                  <Point X="-3.427188232422" Y="1.57378125" />
                  <Point X="-3.432790039062" Y="1.594687255859" />
                  <Point X="-3.436012207031" Y="1.604531005859" />
                  <Point X="-3.443509033203" Y="1.623809204102" />
                  <Point X="-3.447783691406" Y="1.633243530273" />
                  <Point X="-3.467703125" Y="1.671508544922" />
                  <Point X="-3.4775234375" Y="1.690372924805" />
                  <Point X="-3.482799560547" Y="1.699285644531" />
                  <Point X="-3.494291259766" Y="1.71648449707" />
                  <Point X="-3.500506835938" Y="1.724770507812" />
                  <Point X="-3.514419433594" Y="1.741351074219" />
                  <Point X="-3.521500488281" Y="1.748911254883" />
                  <Point X="-3.536442626953" Y="1.763215087891" />
                  <Point X="-3.544303710938" Y="1.769958740234" />
                  <Point X="-3.825341796875" Y="1.985606689453" />
                  <Point X="-4.227614257812" Y="2.294282226563" />
                  <Point X="-4.106508300781" Y="2.501765380859" />
                  <Point X="-4.00229296875" Y="2.680312255859" />
                  <Point X="-3.734714599609" Y="3.024246337891" />
                  <Point X="-3.726338378906" Y="3.035012695312" />
                  <Point X="-3.691687011719" Y="3.015006591797" />
                  <Point X="-3.254156982422" Y="2.7623984375" />
                  <Point X="-3.244917480469" Y="2.757716064453" />
                  <Point X="-3.225984619141" Y="2.749385498047" />
                  <Point X="-3.216291259766" Y="2.745737304688" />
                  <Point X="-3.195648925781" Y="2.739228759766" />
                  <Point X="-3.185615234375" Y="2.736657226562" />
                  <Point X="-3.165327880859" Y="2.732621826172" />
                  <Point X="-3.15507421875" Y="2.731157958984" />
                  <Point X="-3.097773925781" Y="2.726144775391" />
                  <Point X="-3.069524902344" Y="2.723673339844" />
                  <Point X="-3.059173095703" Y="2.723334472656" />
                  <Point X="-3.038492675781" Y="2.723785644531" />
                  <Point X="-3.0281640625" Y="2.724575683594" />
                  <Point X="-3.006705566406" Y="2.727400878906" />
                  <Point X="-2.996525146484" Y="2.729310791016" />
                  <Point X="-2.976433837891" Y="2.734227294922" />
                  <Point X="-2.956998046875" Y="2.741301513672" />
                  <Point X="-2.938447021484" Y="2.750449707031" />
                  <Point X="-2.929420898438" Y="2.755530273438" />
                  <Point X="-2.911166503906" Y="2.767159423828" />
                  <Point X="-2.902746337891" Y="2.773193359375" />
                  <Point X="-2.886614257812" Y="2.786140869141" />
                  <Point X="-2.87890234375" Y="2.793054443359" />
                  <Point X="-2.838230224609" Y="2.8337265625" />
                  <Point X="-2.818178710938" Y="2.853777832031" />
                  <Point X="-2.811265380859" Y="2.861489501953" />
                  <Point X="-2.798318359375" Y="2.877620849609" />
                  <Point X="-2.792284667969" Y="2.886040527344" />
                  <Point X="-2.780655273438" Y="2.904294921875" />
                  <Point X="-2.775574707031" Y="2.913320800781" />
                  <Point X="-2.766426025391" Y="2.931872314453" />
                  <Point X="-2.759351318359" Y="2.951309570312" />
                  <Point X="-2.754434814453" Y="2.971401367188" />
                  <Point X="-2.752524902344" Y="2.981581542969" />
                  <Point X="-2.749699707031" Y="3.003040283203" />
                  <Point X="-2.748909667969" Y="3.013368652344" />
                  <Point X="-2.748458496094" Y="3.034048828125" />
                  <Point X="-2.748797363281" Y="3.044400634766" />
                  <Point X="-2.753810546875" Y="3.101700927734" />
                  <Point X="-2.756281982422" Y="3.129950195312" />
                  <Point X="-2.757745849609" Y="3.140203857422" />
                  <Point X="-2.76178125" Y="3.160491210938" />
                  <Point X="-2.764352783203" Y="3.170524902344" />
                  <Point X="-2.770861328125" Y="3.191167236328" />
                  <Point X="-2.774509521484" Y="3.200860595703" />
                  <Point X="-2.782840087891" Y="3.219793457031" />
                  <Point X="-2.787522460938" Y="3.229032958984" />
                  <Point X="-2.912058837891" Y="3.444736328125" />
                  <Point X="-3.059387207031" Y="3.699916259766" />
                  <Point X="-2.829878173828" Y="3.875878662109" />
                  <Point X="-2.648375488281" Y="4.015035644531" />
                  <Point X="-2.226951660156" Y="4.249169921875" />
                  <Point X="-2.192525146484" Y="4.268296386719" />
                  <Point X="-2.118564208984" Y="4.171908691406" />
                  <Point X="-2.111820800781" Y="4.164048339844" />
                  <Point X="-2.097516845703" Y="4.149105957031" />
                  <Point X="-2.089956542969" Y="4.142024414062" />
                  <Point X="-2.073376708984" Y="4.128112304688" />
                  <Point X="-2.065090820313" Y="4.121896972656" />
                  <Point X="-2.047892333984" Y="4.110405273438" />
                  <Point X="-2.038979492188" Y="4.10512890625" />
                  <Point X="-1.975204589844" Y="4.071929931641" />
                  <Point X="-1.943763305664" Y="4.055562255859" />
                  <Point X="-1.934328491211" Y="4.051287353516" />
                  <Point X="-1.91505090332" Y="4.043790771484" />
                  <Point X="-1.905208007812" Y="4.040568847656" />
                  <Point X="-1.884301513672" Y="4.034966796875" />
                  <Point X="-1.874167114258" Y="4.032835449219" />
                  <Point X="-1.853724121094" Y="4.029688476562" />
                  <Point X="-1.833057983398" Y="4.028785888672" />
                  <Point X="-1.812418579102" Y="4.030138427734" />
                  <Point X="-1.802137084961" Y="4.031377929688" />
                  <Point X="-1.780821655273" Y="4.035135742188" />
                  <Point X="-1.770731933594" Y="4.037488037109" />
                  <Point X="-1.750870239258" Y="4.043277099609" />
                  <Point X="-1.741098022461" Y="4.046713623047" />
                  <Point X="-1.674672119141" Y="4.074228759766" />
                  <Point X="-1.641923950195" Y="4.087793212891" />
                  <Point X="-1.6325859375" Y="4.092272216797" />
                  <Point X="-1.614451660156" Y="4.102221191406" />
                  <Point X="-1.605655517578" Y="4.107690917969" />
                  <Point X="-1.58792578125" Y="4.12010546875" />
                  <Point X="-1.57977746582" Y="4.126500488281" />
                  <Point X="-1.564225708008" Y="4.140139160156" />
                  <Point X="-1.550251708984" Y="4.155388671875" />
                  <Point X="-1.538020629883" Y="4.172069824219" />
                  <Point X="-1.532359741211" Y="4.180745117188" />
                  <Point X="-1.521537841797" Y="4.199489746094" />
                  <Point X="-1.516855834961" Y="4.208729003906" />
                  <Point X="-1.508525634766" Y="4.227661132812" />
                  <Point X="-1.504877441406" Y="4.237354003906" />
                  <Point X="-1.483256958008" Y="4.305924804688" />
                  <Point X="-1.472598022461" Y="4.33973046875" />
                  <Point X="-1.470026733398" Y="4.349763671875" />
                  <Point X="-1.465991210938" Y="4.37005078125" />
                  <Point X="-1.46452722168" Y="4.3803046875" />
                  <Point X="-1.46264074707" Y="4.401866210938" />
                  <Point X="-1.462301757812" Y="4.41221875" />
                  <Point X="-1.462753051758" Y="4.432898925781" />
                  <Point X="-1.46354309082" Y="4.4432265625" />
                  <Point X="-1.477701538086" Y="4.550770507812" />
                  <Point X="-1.479266113281" Y="4.562655273438" />
                  <Point X="-1.166143188477" Y="4.650444335938" />
                  <Point X="-0.931179260254" Y="4.7163203125" />
                  <Point X="-0.420292877197" Y="4.776111816406" />
                  <Point X="-0.365221984863" Y="4.782557128906" />
                  <Point X="-0.355099761963" Y="4.744780273438" />
                  <Point X="-0.225666290283" Y="4.261727539062" />
                  <Point X="-0.220435211182" Y="4.247107910156" />
                  <Point X="-0.207661819458" Y="4.218916503906" />
                  <Point X="-0.20011920166" Y="4.205344726562" />
                  <Point X="-0.182260971069" Y="4.178618164062" />
                  <Point X="-0.172608352661" Y="4.166456054688" />
                  <Point X="-0.151451293945" Y="4.1438671875" />
                  <Point X="-0.126898040771" Y="4.125026367188" />
                  <Point X="-0.099602851868" Y="4.110436523438" />
                  <Point X="-0.085356750488" Y="4.104260742188" />
                  <Point X="-0.054918632507" Y="4.093927978516" />
                  <Point X="-0.039857185364" Y="4.090155273438" />
                  <Point X="-0.009319933891" Y="4.08511328125" />
                  <Point X="0.021631828308" Y="4.08511328125" />
                  <Point X="0.052169078827" Y="4.090155273438" />
                  <Point X="0.067230377197" Y="4.093927978516" />
                  <Point X="0.097668495178" Y="4.104260742188" />
                  <Point X="0.111914451599" Y="4.110436523438" />
                  <Point X="0.139209793091" Y="4.125026367188" />
                  <Point X="0.163763183594" Y="4.143866699219" />
                  <Point X="0.184920257568" Y="4.166455566406" />
                  <Point X="0.194572860718" Y="4.178617675781" />
                  <Point X="0.212431091309" Y="4.205344238281" />
                  <Point X="0.219973709106" Y="4.218916503906" />
                  <Point X="0.232747253418" Y="4.247107910156" />
                  <Point X="0.237978179932" Y="4.261727539062" />
                  <Point X="0.301789276123" Y="4.499873535156" />
                  <Point X="0.378190246582" Y="4.785006347656" />
                  <Point X="0.622703430176" Y="4.759398925781" />
                  <Point X="0.82787677002" Y="4.737912109375" />
                  <Point X="1.250553100586" Y="4.635864746094" />
                  <Point X="1.453601928711" Y="4.586842285156" />
                  <Point X="1.727258300781" Y="4.487585449219" />
                  <Point X="1.858255371094" Y="4.440071777344" />
                  <Point X="2.124290039063" Y="4.31565625" />
                  <Point X="2.250450195312" Y="4.256654785156" />
                  <Point X="2.507502441406" Y="4.106895996094" />
                  <Point X="2.629433837891" Y="4.035858154297" />
                  <Point X="2.817780029297" Y="3.901916992188" />
                  <Point X="2.642419433594" Y="3.598183349609" />
                  <Point X="2.06530859375" Y="2.598597900391" />
                  <Point X="2.062373046875" Y="2.593104980469" />
                  <Point X="2.053181152344" Y="2.573438232422" />
                  <Point X="2.044182128906" Y="2.549563720703" />
                  <Point X="2.041301635742" Y="2.540598388672" />
                  <Point X="2.026921508789" Y="2.486823242188" />
                  <Point X="2.01983190918" Y="2.460312011719" />
                  <Point X="2.017912719727" Y="2.451465820312" />
                  <Point X="2.013646972656" Y="2.420223632812" />
                  <Point X="2.012755615234" Y="2.3832421875" />
                  <Point X="2.013411254883" Y="2.369580078125" />
                  <Point X="2.019018432617" Y="2.323080078125" />
                  <Point X="2.021782592773" Y="2.300155273438" />
                  <Point X="2.02380078125" Y="2.28903515625" />
                  <Point X="2.029143188477" Y="2.267112548828" />
                  <Point X="2.032467773437" Y="2.256310058594" />
                  <Point X="2.040733886719" Y="2.234220947266" />
                  <Point X="2.045318237305" Y="2.223888916016" />
                  <Point X="2.055681396484" Y="2.203843505859" />
                  <Point X="2.061459960938" Y="2.194130126953" />
                  <Point X="2.090232666016" Y="2.1517265625" />
                  <Point X="2.104417724609" Y="2.130821533203" />
                  <Point X="2.109807861328" Y="2.123634277344" />
                  <Point X="2.13046484375" Y="2.100121582031" />
                  <Point X="2.157598632812" Y="2.075386230469" />
                  <Point X="2.168258056641" Y="2.066981445312" />
                  <Point X="2.210661621094" Y="2.038208740234" />
                  <Point X="2.231566650391" Y="2.024023803711" />
                  <Point X="2.241279541016" Y="2.018245483398" />
                  <Point X="2.26132421875" Y="2.00788269043" />
                  <Point X="2.271656005859" Y="2.003298339844" />
                  <Point X="2.293743896484" Y="1.995032348633" />
                  <Point X="2.304546875" Y="1.991707763672" />
                  <Point X="2.326470214844" Y="1.986364868164" />
                  <Point X="2.337590576172" Y="1.984346801758" />
                  <Point X="2.384090820312" Y="1.978739624023" />
                  <Point X="2.407015380859" Y="1.975975219727" />
                  <Point X="2.416043945312" Y="1.975321044922" />
                  <Point X="2.447575195312" Y="1.975497314453" />
                  <Point X="2.484314453125" Y="1.979822753906" />
                  <Point X="2.497748779297" Y="1.982395996094" />
                  <Point X="2.551523925781" Y="1.996776367188" />
                  <Point X="2.57803515625" Y="2.003865600586" />
                  <Point X="2.583995361328" Y="2.005670776367" />
                  <Point X="2.604404541016" Y="2.013067138672" />
                  <Point X="2.627655029297" Y="2.02357409668" />
                  <Point X="2.636033935547" Y="2.027872558594" />
                  <Point X="3.128831787109" Y="2.312389404297" />
                  <Point X="3.940405029297" Y="2.780951416016" />
                  <Point X="3.971625488281" Y="2.737561767578" />
                  <Point X="4.043952636719" Y="2.637042724609" />
                  <Point X="4.136884765625" Y="2.483472167969" />
                  <Point X="3.932771972656" Y="2.326850585938" />
                  <Point X="3.172951660156" Y="1.743819824219" />
                  <Point X="3.168137939453" Y="1.739868896484" />
                  <Point X="3.152119628906" Y="1.725217163086" />
                  <Point X="3.134668701172" Y="1.706603515625" />
                  <Point X="3.128576171875" Y="1.699422485352" />
                  <Point X="3.089874023438" Y="1.648932739258" />
                  <Point X="3.070793945312" Y="1.624041137695" />
                  <Point X="3.065635253906" Y="1.616602172852" />
                  <Point X="3.049737548828" Y="1.589369262695" />
                  <Point X="3.034762451172" Y="1.555544677734" />
                  <Point X="3.030140136719" Y="1.54267199707" />
                  <Point X="3.015723632812" Y="1.491121826172" />
                  <Point X="3.008616210938" Y="1.465707641602" />
                  <Point X="3.006225097656" Y="1.454661010742" />
                  <Point X="3.002771728516" Y="1.432362548828" />
                  <Point X="3.001709472656" Y="1.421110717773" />
                  <Point X="3.000893310547" Y="1.397540527344" />
                  <Point X="3.001174804688" Y="1.386240966797" />
                  <Point X="3.003077880859" Y="1.363756103516" />
                  <Point X="3.004699462891" Y="1.352570800781" />
                  <Point X="3.016533935547" Y="1.295214477539" />
                  <Point X="3.022368408203" Y="1.266937988281" />
                  <Point X="3.024597900391" Y="1.258234741211" />
                  <Point X="3.034683837891" Y="1.228608642578" />
                  <Point X="3.050286376953" Y="1.195371337891" />
                  <Point X="3.056918457031" Y="1.183526000977" />
                  <Point X="3.089106933594" Y="1.134600708008" />
                  <Point X="3.104976074219" Y="1.11048059082" />
                  <Point X="3.111739257813" Y="1.101424194336" />
                  <Point X="3.126292724609" Y="1.08417956543" />
                  <Point X="3.134083007812" Y="1.075991333008" />
                  <Point X="3.151326904297" Y="1.059901367188" />
                  <Point X="3.160034912109" Y="1.052695678711" />
                  <Point X="3.178244628906" Y="1.039369995117" />
                  <Point X="3.18774609375" Y="1.03325" />
                  <Point X="3.234391601563" Y="1.006992492676" />
                  <Point X="3.257388183594" Y="0.994047424316" />
                  <Point X="3.265479492188" Y="0.989987670898" />
                  <Point X="3.294679199219" Y="0.978083862305" />
                  <Point X="3.330275878906" Y="0.968021057129" />
                  <Point X="3.343671142578" Y="0.965257568359" />
                  <Point X="3.406739013672" Y="0.956922180176" />
                  <Point X="3.437831787109" Y="0.952812927246" />
                  <Point X="3.444030029297" Y="0.952199707031" />
                  <Point X="3.465716308594" Y="0.951222961426" />
                  <Point X="3.491217529297" Y="0.952032348633" />
                  <Point X="3.500603515625" Y="0.952797302246" />
                  <Point X="3.968728515625" Y="1.014427062988" />
                  <Point X="4.704703613281" Y="1.111319946289" />
                  <Point X="4.721620117188" Y="1.04183190918" />
                  <Point X="4.75268359375" Y="0.914233642578" />
                  <Point X="4.78387109375" Y="0.713920959473" />
                  <Point X="4.562762695312" Y="0.654675048828" />
                  <Point X="3.691991943359" Y="0.421352844238" />
                  <Point X="3.686031738281" Y="0.419544403076" />
                  <Point X="3.665626708984" Y="0.412138092041" />
                  <Point X="3.642381103516" Y="0.401619445801" />
                  <Point X="3.634004394531" Y="0.39731652832" />
                  <Point X="3.572041992188" Y="0.361500915527" />
                  <Point X="3.541494384766" Y="0.343843933105" />
                  <Point X="3.533881591797" Y="0.338945465088" />
                  <Point X="3.5087734375" Y="0.319869873047" />
                  <Point X="3.481993652344" Y="0.294350402832" />
                  <Point X="3.472797119141" Y="0.284226654053" />
                  <Point X="3.435619628906" Y="0.236853851318" />
                  <Point X="3.417291259766" Y="0.213498962402" />
                  <Point X="3.410854980469" Y="0.204208831787" />
                  <Point X="3.399130615234" Y="0.184928726196" />
                  <Point X="3.393842529297" Y="0.174938598633" />
                  <Point X="3.384068847656" Y="0.153475067139" />
                  <Point X="3.380005126953" Y="0.142928634644" />
                  <Point X="3.373158935547" Y="0.121427658081" />
                  <Point X="3.370376464844" Y="0.110473258972" />
                  <Point X="3.357983886719" Y="0.045764331818" />
                  <Point X="3.351874267578" Y="0.013862573624" />
                  <Point X="3.350603515625" Y="0.00496778965" />
                  <Point X="3.348584472656" Y="-0.02626219368" />
                  <Point X="3.350280029297" Y="-0.062940692902" />
                  <Point X="3.351874267578" Y="-0.076422721863" />
                  <Point X="3.364266845703" Y="-0.141131652832" />
                  <Point X="3.370376464844" Y="-0.17303326416" />
                  <Point X="3.373159179688" Y="-0.183988250732" />
                  <Point X="3.380005371094" Y="-0.205488632202" />
                  <Point X="3.384068847656" Y="-0.216034179688" />
                  <Point X="3.393842529297" Y="-0.237498001099" />
                  <Point X="3.399129638672" Y="-0.247487091064" />
                  <Point X="3.410853759766" Y="-0.26676763916" />
                  <Point X="3.417290771484" Y="-0.276059112549" />
                  <Point X="3.454468017578" Y="-0.323431762695" />
                  <Point X="3.472796630859" Y="-0.346786682129" />
                  <Point X="3.478718505859" Y="-0.353634033203" />
                  <Point X="3.501140136719" Y="-0.375805877686" />
                  <Point X="3.530176269531" Y="-0.398724731445" />
                  <Point X="3.541494384766" Y="-0.40640423584" />
                  <Point X="3.603456787109" Y="-0.442219665527" />
                  <Point X="3.634004394531" Y="-0.459876647949" />
                  <Point X="3.639495605469" Y="-0.462814788818" />
                  <Point X="3.659158447266" Y="-0.472016937256" />
                  <Point X="3.683028076172" Y="-0.481027770996" />
                  <Point X="3.691991943359" Y="-0.483912841797" />
                  <Point X="4.121283691406" Y="-0.598941345215" />
                  <Point X="4.784876953125" Y="-0.776750549316" />
                  <Point X="4.778958007812" Y="-0.816008972168" />
                  <Point X="4.76161328125" Y="-0.931051757812" />
                  <Point X="4.727801757812" Y="-1.079219848633" />
                  <Point X="4.446630859375" Y="-1.042202880859" />
                  <Point X="3.436781982422" Y="-0.90925378418" />
                  <Point X="3.428624511719" Y="-0.908535766602" />
                  <Point X="3.400098388672" Y="-0.908042480469" />
                  <Point X="3.366721435547" Y="-0.910840942383" />
                  <Point X="3.354481201172" Y="-0.912676269531" />
                  <Point X="3.23287109375" Y="-0.939108703613" />
                  <Point X="3.172916992188" Y="-0.952139953613" />
                  <Point X="3.157874267578" Y="-0.956742370605" />
                  <Point X="3.128754150391" Y="-0.968366760254" />
                  <Point X="3.114676757812" Y="-0.97538873291" />
                  <Point X="3.086850097656" Y="-0.992280944824" />
                  <Point X="3.074124023438" Y="-1.001530151367" />
                  <Point X="3.050373779297" Y="-1.022001220703" />
                  <Point X="3.039349609375" Y="-1.033223022461" />
                  <Point X="2.965843994141" Y="-1.121627197266" />
                  <Point X="2.92960546875" Y="-1.16521081543" />
                  <Point X="2.921326416016" Y="-1.176846923828" />
                  <Point X="2.90660546875" Y="-1.201229370117" />
                  <Point X="2.900163574219" Y="-1.213975952148" />
                  <Point X="2.8888203125" Y="-1.241361328125" />
                  <Point X="2.884362548828" Y="-1.254928955078" />
                  <Point X="2.877531005859" Y="-1.282578491211" />
                  <Point X="2.875157226562" Y="-1.296660400391" />
                  <Point X="2.864622070312" Y="-1.411147949219" />
                  <Point X="2.859428222656" Y="-1.467590698242" />
                  <Point X="2.859288818359" Y="-1.483321533203" />
                  <Point X="2.861607666016" Y="-1.514590576172" />
                  <Point X="2.864065917969" Y="-1.530128662109" />
                  <Point X="2.871797607422" Y="-1.561749633789" />
                  <Point X="2.876786621094" Y="-1.576669189453" />
                  <Point X="2.889157958984" Y="-1.605479980469" />
                  <Point X="2.896540283203" Y="-1.619371337891" />
                  <Point X="2.963841064453" Y="-1.724053344727" />
                  <Point X="2.997020507812" Y="-1.775661865234" />
                  <Point X="3.001741943359" Y="-1.782353149414" />
                  <Point X="3.019792724609" Y="-1.804448852539" />
                  <Point X="3.043488769531" Y="-1.828119873047" />
                  <Point X="3.052796142578" Y="-1.836277709961" />
                  <Point X="3.451181640625" Y="-2.141969970703" />
                  <Point X="4.087170654297" Y="-2.629981689453" />
                  <Point X="4.045487304687" Y="-2.697432128906" />
                  <Point X="4.001274658203" Y="-2.760251953125" />
                  <Point X="3.747861328125" Y="-2.613943603516" />
                  <Point X="2.848454589844" Y="-2.094670654297" />
                  <Point X="2.841191894531" Y="-2.090885253906" />
                  <Point X="2.815026123047" Y="-2.079513183594" />
                  <Point X="2.783119140625" Y="-2.069325927734" />
                  <Point X="2.771108398438" Y="-2.066337646484" />
                  <Point X="2.626372802734" Y="-2.040198364258" />
                  <Point X="2.555018066406" Y="-2.027311767578" />
                  <Point X="2.539359130859" Y="-2.025807495117" />
                  <Point X="2.508006835938" Y="-2.025403198242" />
                  <Point X="2.492313476562" Y="-2.026503662109" />
                  <Point X="2.460140625" Y="-2.031461425781" />
                  <Point X="2.444844482422" Y="-2.035136230469" />
                  <Point X="2.415068603516" Y="-2.044959838867" />
                  <Point X="2.400588867188" Y="-2.051108642578" />
                  <Point X="2.280349121094" Y="-2.114389892578" />
                  <Point X="2.221070800781" Y="-2.145587646484" />
                  <Point X="2.208968505859" Y="-2.153170166016" />
                  <Point X="2.186037353516" Y="-2.170063476562" />
                  <Point X="2.175208496094" Y="-2.179374267578" />
                  <Point X="2.154248779297" Y="-2.200334228516" />
                  <Point X="2.144938476562" Y="-2.211162841797" />
                  <Point X="2.128045898438" Y="-2.234093261719" />
                  <Point X="2.120463623047" Y="-2.246195068359" />
                  <Point X="2.057182373047" Y="-2.366434570313" />
                  <Point X="2.025984741211" Y="-2.425713134766" />
                  <Point X="2.0198359375" Y="-2.440192871094" />
                  <Point X="2.010012207031" Y="-2.46996875" />
                  <Point X="2.006337280273" Y="-2.485264892578" />
                  <Point X="2.001379516602" Y="-2.5174375" />
                  <Point X="2.000279174805" Y="-2.533130859375" />
                  <Point X="2.00068347168" Y="-2.564483154297" />
                  <Point X="2.002187866211" Y="-2.580142089844" />
                  <Point X="2.028326782227" Y="-2.724877441406" />
                  <Point X="2.041213500977" Y="-2.796232421875" />
                  <Point X="2.043015014648" Y="-2.804221435547" />
                  <Point X="2.051236083984" Y="-2.83154296875" />
                  <Point X="2.064069580078" Y="-2.862479736328" />
                  <Point X="2.069546630859" Y="-2.873578613281" />
                  <Point X="2.325549316406" Y="-3.31698828125" />
                  <Point X="2.735893310547" Y="-4.027724609375" />
                  <Point X="2.723754394531" Y="-4.036083496094" />
                  <Point X="2.520245361328" Y="-3.770865722656" />
                  <Point X="1.833914550781" Y="-2.876422851562" />
                  <Point X="1.828653442383" Y="-2.870146240234" />
                  <Point X="1.808831542969" Y="-2.849626953125" />
                  <Point X="1.783252075195" Y="-2.828004882812" />
                  <Point X="1.773298950195" Y="-2.820647216797" />
                  <Point X="1.63055065918" Y="-2.728873291016" />
                  <Point X="1.560175537109" Y="-2.683628417969" />
                  <Point X="1.546283935547" Y="-2.676245849609" />
                  <Point X="1.517472900391" Y="-2.663874511719" />
                  <Point X="1.502553344727" Y="-2.658885742188" />
                  <Point X="1.470932006836" Y="-2.651154052734" />
                  <Point X="1.455394165039" Y="-2.648695800781" />
                  <Point X="1.424125244141" Y="-2.646376953125" />
                  <Point X="1.408394287109" Y="-2.646516357422" />
                  <Point X="1.252274780273" Y="-2.660882568359" />
                  <Point X="1.175307373047" Y="-2.667965332031" />
                  <Point X="1.161224975586" Y="-2.670339111328" />
                  <Point X="1.133575195312" Y="-2.677170898438" />
                  <Point X="1.12000769043" Y="-2.68162890625" />
                  <Point X="1.092622680664" Y="-2.692972167969" />
                  <Point X="1.079876708984" Y="-2.699413574219" />
                  <Point X="1.055494873047" Y="-2.714134033203" />
                  <Point X="1.043858764648" Y="-2.722413085938" />
                  <Point X="0.923307373047" Y="-2.822647705078" />
                  <Point X="0.863875183105" Y="-2.872063476562" />
                  <Point X="0.852653259277" Y="-2.883088134766" />
                  <Point X="0.832182556152" Y="-2.906838134766" />
                  <Point X="0.82293359375" Y="-2.919563720703" />
                  <Point X="0.806041259766" Y="-2.947390380859" />
                  <Point X="0.799019287109" Y="-2.961467529297" />
                  <Point X="0.787394592285" Y="-2.990588134766" />
                  <Point X="0.782791870117" Y="-3.005631347656" />
                  <Point X="0.746747558594" Y="-3.171463623047" />
                  <Point X="0.728977600098" Y="-3.253219238281" />
                  <Point X="0.727584838867" Y="-3.261289306641" />
                  <Point X="0.72472442627" Y="-3.289677734375" />
                  <Point X="0.72474230957" Y="-3.323170410156" />
                  <Point X="0.725554931641" Y="-3.335520019531" />
                  <Point X="0.798104187012" Y="-3.8865859375" />
                  <Point X="0.833091369629" Y="-4.15233984375" />
                  <Point X="0.655065063477" Y="-3.487936523438" />
                  <Point X="0.652606201172" Y="-3.480124511719" />
                  <Point X="0.642145080566" Y="-3.453580322266" />
                  <Point X="0.626786865234" Y="-3.423815673828" />
                  <Point X="0.620407531738" Y="-3.413210205078" />
                  <Point X="0.510744171143" Y="-3.255206054688" />
                  <Point X="0.456680053711" Y="-3.177309814453" />
                  <Point X="0.44667074585" Y="-3.165172851562" />
                  <Point X="0.424786773682" Y="-3.142717529297" />
                  <Point X="0.412912261963" Y="-3.132399169922" />
                  <Point X="0.38665713501" Y="-3.113155273438" />
                  <Point X="0.373242706299" Y="-3.104937988281" />
                  <Point X="0.345241577148" Y="-3.090829589844" />
                  <Point X="0.330654663086" Y="-3.084938476562" />
                  <Point X="0.160956893921" Y="-3.032270507812" />
                  <Point X="0.077295562744" Y="-3.006305175781" />
                  <Point X="0.063376434326" Y="-3.003109375" />
                  <Point X="0.035217002869" Y="-2.998840087891" />
                  <Point X="0.020976697922" Y="-2.997766601562" />
                  <Point X="-0.008664803505" Y="-2.997766601562" />
                  <Point X="-0.022905256271" Y="-2.998840087891" />
                  <Point X="-0.051064540863" Y="-3.003109375" />
                  <Point X="-0.064983520508" Y="-3.006305175781" />
                  <Point X="-0.234681304932" Y="-3.058972900391" />
                  <Point X="-0.31834262085" Y="-3.084938476562" />
                  <Point X="-0.332929382324" Y="-3.090829589844" />
                  <Point X="-0.36093081665" Y="-3.104937988281" />
                  <Point X="-0.374345367432" Y="-3.113155273438" />
                  <Point X="-0.400600494385" Y="-3.132399169922" />
                  <Point X="-0.412475189209" Y="-3.142717773438" />
                  <Point X="-0.434359008789" Y="-3.165173095703" />
                  <Point X="-0.444368011475" Y="-3.177309814453" />
                  <Point X="-0.55403137207" Y="-3.335313720703" />
                  <Point X="-0.60809564209" Y="-3.413210205078" />
                  <Point X="-0.612470336914" Y="-3.420132324219" />
                  <Point X="-0.625976745605" Y="-3.445265136719" />
                  <Point X="-0.638777770996" Y="-3.476215820312" />
                  <Point X="-0.642752990723" Y="-3.487936523438" />
                  <Point X="-0.775028442383" Y="-3.981594970703" />
                  <Point X="-0.985425109863" Y="-4.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.665117837569" Y="-3.959666951655" />
                  <Point X="2.689205645212" Y="-3.946859237144" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.606481280608" Y="-3.883250407216" />
                  <Point X="2.641676675571" Y="-3.864536683763" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.547844723646" Y="-3.806833862778" />
                  <Point X="2.59414770593" Y="-3.782214130383" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.489208175711" Y="-3.730417313539" />
                  <Point X="2.54661873629" Y="-3.699891577003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.430571635802" Y="-3.654000760033" />
                  <Point X="2.499089766649" Y="-3.617569023622" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.371935095893" Y="-3.577584206527" />
                  <Point X="2.451560797008" Y="-3.535246470242" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.957090433848" Y="-2.734742162688" />
                  <Point X="4.054407537986" Y="-2.682997740556" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.313298555984" Y="-3.501167653021" />
                  <Point X="2.404031827368" Y="-3.452923916861" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.860076613708" Y="-2.678731171043" />
                  <Point X="4.031746076126" Y="-2.587452898747" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.254662016076" Y="-3.424751099515" />
                  <Point X="2.356502857727" Y="-3.370601363481" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.763062793569" Y="-2.622720179397" />
                  <Point X="3.948919968128" Y="-2.523898166742" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.814487985857" Y="-4.082911059801" />
                  <Point X="0.823331816185" Y="-4.078208711804" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.196025476167" Y="-3.348334546009" />
                  <Point X="2.308973894044" Y="-3.288278806933" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.666048964294" Y="-2.566709192608" />
                  <Point X="3.866093860129" Y="-2.460343434737" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.789253419533" Y="-3.988734361904" />
                  <Point X="0.810093466555" Y="-3.977653512346" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.137388936258" Y="-3.271917992503" />
                  <Point X="2.261444941488" Y="-3.205956244468" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.569035133323" Y="-2.510698206722" />
                  <Point X="3.78326775213" Y="-2.396788702733" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.764018853209" Y="-3.894557664007" />
                  <Point X="0.796855116676" Y="-3.877098313021" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.078752396349" Y="-3.195501438997" />
                  <Point X="2.213915988932" Y="-3.123633682004" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.472021302351" Y="-2.454687220836" />
                  <Point X="3.700441644131" Y="-2.333233970728" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.053827492644" Y="-4.753529556593" />
                  <Point X="-0.969912005238" Y="-4.708910900477" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.738784286885" Y="-3.80038096611" />
                  <Point X="0.783616764412" Y="-3.776543114964" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.020115856441" Y="-3.119084885491" />
                  <Point X="2.166387036375" Y="-3.04131111954" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.375007471379" Y="-2.39867623495" />
                  <Point X="3.617615536132" Y="-2.269679238723" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.1357379977" Y="-4.689487989868" />
                  <Point X="-0.936292449208" Y="-4.583440910632" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.713549720561" Y="-3.706204268213" />
                  <Point X="0.770378412148" Y="-3.675987916907" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.961479316532" Y="-3.042668331985" />
                  <Point X="2.118858083819" Y="-2.958988557076" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.277993640407" Y="-2.342665249064" />
                  <Point X="3.534789428134" Y="-2.206124506718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.120595582348" Y="-4.573842469991" />
                  <Point X="-0.902672893178" Y="-4.457970920787" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.688315154237" Y="-3.612027570315" />
                  <Point X="0.757140059884" Y="-3.57543271885" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.902842776623" Y="-2.96625177848" />
                  <Point X="2.071329131262" Y="-2.876665994612" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.180979809435" Y="-2.286654263178" />
                  <Point X="3.451963320135" Y="-2.142569774714" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.128728202404" Y="-4.470572505964" />
                  <Point X="-0.869053337149" Y="-4.332500930942" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.663080587914" Y="-3.517850872418" />
                  <Point X="0.74390170762" Y="-3.474877520793" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.844206236715" Y="-2.889835224974" />
                  <Point X="2.039376595813" Y="-2.78606130426" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.083965978463" Y="-2.230643277291" />
                  <Point X="3.369137243986" Y="-2.079015025774" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.148082997884" Y="-4.373269478453" />
                  <Point X="-0.835433781119" Y="-4.207030941098" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.629110496402" Y="-3.428318935654" />
                  <Point X="0.730663355356" Y="-3.374322322736" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.77269954311" Y="-2.820261853575" />
                  <Point X="2.02164761331" Y="-2.687893816655" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.986952147492" Y="-2.174632291405" />
                  <Point X="3.28631116814" Y="-2.015460276674" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.167437803052" Y="-4.275966456093" />
                  <Point X="-0.801814225089" Y="-4.081560951253" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.575866555161" Y="-3.349035086575" />
                  <Point X="0.72683153044" Y="-3.268765585369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.68110017529" Y="-2.761371946563" />
                  <Point X="2.003918749328" Y="-2.589726266032" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.88993831652" Y="-2.118621305519" />
                  <Point X="3.203485092294" Y="-1.951905527573" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.187901100062" Y="-4.179252829301" />
                  <Point X="-0.768194658616" Y="-3.956090955855" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.521320013616" Y="-3.270443842364" />
                  <Point X="0.751879365841" Y="-3.147853260228" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.589500901657" Y="-2.702481989472" />
                  <Point X="2.007554365668" Y="-2.480199019718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.781194740628" Y="-2.068847135638" />
                  <Point X="3.120659016448" Y="-1.888350778472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.659359489195" Y="-1.070209224611" />
                  <Point X="4.7395930583" Y="-1.027548279182" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.230867493022" Y="-4.094504310867" />
                  <Point X="-0.73457505121" Y="-3.830620938693" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.466773504181" Y="-3.191852581079" />
                  <Point X="0.778321255339" Y="-3.026199703375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.479664065632" Y="-2.653289116315" />
                  <Point X="2.071957150017" Y="-2.338361297039" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.631110771824" Y="-2.041054042577" />
                  <Point X="3.039352561182" Y="-1.823988032777" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.497164339419" Y="-1.048855760701" />
                  <Point X="4.76535134696" Y="-0.906258199342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.301359142976" Y="-4.024391231186" />
                  <Point X="-0.700955443803" Y="-3.705150921531" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.397491434413" Y="-3.121096356204" />
                  <Point X="0.861540562007" Y="-2.874357058308" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.265278114524" Y="-2.659685993719" />
                  <Point X="2.185896710385" Y="-2.170184403337" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.431777504353" Y="-2.03944726612" />
                  <Point X="2.979408115239" Y="-1.748266905245" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.334969114725" Y="-1.027502336626" />
                  <Point X="4.782986833964" Y="-0.789287089755" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.377738038185" Y="-3.957408455333" />
                  <Point X="-0.667335836397" Y="-3.579680904369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.288023510409" Y="-3.071707329046" />
                  <Point X="2.927857240432" Y="-1.668082836775" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.172773856126" Y="-1.006148930578" />
                  <Point X="4.664747413906" Y="-0.744561949778" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.454117014626" Y="-3.890425722672" />
                  <Point X="-0.628537874615" Y="-3.451457507344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.160250522897" Y="-3.032051276802" />
                  <Point X="2.880623033003" Y="-1.585603555546" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.010578597527" Y="-0.98479552453" />
                  <Point X="4.530197286295" Y="-0.708509366845" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.537647378036" Y="-3.827245449911" />
                  <Point X="-0.519858884075" Y="-3.286077708236" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.022201760034" Y="-2.99785895123" />
                  <Point X="2.859719132287" Y="-1.4891242019" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.848383338929" Y="-0.963442118483" />
                  <Point X="4.395647158685" Y="-0.672456783911" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.676647322001" Y="-3.793558876302" />
                  <Point X="2.86773995526" Y="-1.37726529986" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.68618808033" Y="-0.942088712435" />
                  <Point X="4.261097031074" Y="-0.636404200978" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.535375680944" Y="-4.142558689172" />
                  <Point X="-2.455209534268" Y="-4.099933592885" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.856795774243" Y="-3.781751352643" />
                  <Point X="2.882683245458" Y="-1.261725656707" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.523992821731" Y="-0.920735306387" />
                  <Point X="4.126546903463" Y="-0.600351618044" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.628863819128" Y="-4.084673059178" />
                  <Point X="2.983300713557" Y="-1.100632245113" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.324565420116" Y="-0.919178581942" />
                  <Point X="3.991996841531" Y="-0.564299000189" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.722352200034" Y="-4.026787558241" />
                  <Point X="3.857446782273" Y="-0.528246380911" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.807933167793" Y="-3.964697611153" />
                  <Point X="3.722896723015" Y="-0.492193761634" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.89059134648" Y="-3.90105358955" />
                  <Point X="3.60876196047" Y="-0.445286136544" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.973249525167" Y="-3.837409567946" />
                  <Point X="3.515605837269" Y="-0.387223971052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.899482428478" Y="-3.690592752075" />
                  <Point X="3.44821401898" Y="-0.315462681637" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.809846079807" Y="-3.535338105251" />
                  <Point X="3.393578711476" Y="-0.236918635123" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.720209754971" Y="-3.380083471101" />
                  <Point X="3.364923557014" Y="-0.144560696201" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.630573430136" Y="-3.224828836951" />
                  <Point X="3.349459411057" Y="-0.045188973643" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.770297049274" Y="0.710283799456" />
                  <Point X="4.783356282325" Y="0.717227516839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.5409371053" Y="-3.069574202801" />
                  <Point X="3.362498587222" Y="0.06933823412" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.362372728674" Y="0.600980745604" />
                  <Point X="4.767885261953" Y="0.816595584206" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.451300780464" Y="-2.914319568651" />
                  <Point X="3.40944863676" Y="0.201896173092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.954448846106" Y="0.491677924658" />
                  <Point X="4.752279826547" Y="0.915892181831" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.361664455628" Y="-2.759064934501" />
                  <Point X="4.729088290116" Y="1.011155177991" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.312329625605" Y="-2.625238985253" />
                  <Point X="4.705896904838" Y="1.106418254521" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.325065904712" Y="-2.524416830162" />
                  <Point X="4.449595043791" Y="1.077734292466" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.372952208177" Y="-2.442284274547" />
                  <Point X="4.180648007238" Y="1.042326771328" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.453920524365" Y="-2.377741737114" />
                  <Point X="3.911700975573" Y="1.00691925279" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.81233204995" Y="-2.99242780253" />
                  <Point X="-3.015010283814" Y="-2.568484299406" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.61076408738" Y="-2.353542784049" />
                  <Point X="3.642753962073" Y="0.97151174391" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.870632629278" Y="-2.915832615614" />
                  <Point X="3.41172139461" Y="0.956263703585" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.928933240449" Y="-2.83923744563" />
                  <Point X="3.269618412296" Y="0.988300362437" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.987233879037" Y="-2.762642290224" />
                  <Point X="3.171960319434" Y="1.043968788199" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.038709352719" Y="-2.682418130265" />
                  <Point X="3.102311778236" Y="1.114530156758" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.087426295116" Y="-2.600727233203" />
                  <Point X="3.05067874569" Y="1.194670541184" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.136143237512" Y="-2.519036336141" />
                  <Point X="3.018598748984" Y="1.285207459183" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.147567280963" Y="-2.417516452977" />
                  <Point X="3.001393796333" Y="1.383653578402" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.690921582061" Y="-2.067119473127" />
                  <Point X="3.018267998785" Y="1.500219905813" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.234274642225" Y="-1.716721833461" />
                  <Point X="3.085934092616" Y="1.643792760922" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.972078957559" Y="-1.469715760168" />
                  <Point X="3.401436814968" Y="1.919142688928" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.94880800681" Y="-1.349748221356" />
                  <Point X="3.858083927409" Y="2.269540420369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.969699782158" Y="-1.253262420537" />
                  <Point X="4.117700067609" Y="2.51517492554" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.027540787498" Y="-1.176422873798" />
                  <Point X="4.06844012027" Y="2.596577101752" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.120987821514" Y="-1.118515388329" />
                  <Point X="4.015793116301" Y="2.676178348009" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.303554369003" Y="-1.10799358872" />
                  <Point X="2.519625977518" Y="1.988246323791" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.123954423164" Y="2.309573458163" />
                  <Point X="3.959798017065" Y="2.753999380434" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.572501738841" Y="-1.143401287068" />
                  <Point X="2.317735733911" Y="1.98849353192" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.841449059164" Y="-1.178808959088" />
                  <Point X="2.209879445707" Y="2.038739481034" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.11039620565" Y="-1.214216538677" />
                  <Point X="2.128077615562" Y="2.102838831234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.379343352136" Y="-1.249624118267" />
                  <Point X="2.070993160835" Y="2.18008064307" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.648290498623" Y="-1.285031697857" />
                  <Point X="2.029590102783" Y="2.26566040142" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.683980806407" Y="-1.19641441631" />
                  <Point X="2.013969686898" Y="2.364949033783" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.708177478438" Y="-1.101685860228" />
                  <Point X="2.024616634461" Y="2.478204271036" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.732374126255" Y="-1.006957291271" />
                  <Point X="2.07267051781" Y="2.611349128856" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.750105464232" Y="-0.908791056094" />
                  <Point X="-3.749667584527" Y="-0.376848799663" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.320168545255" Y="-0.148480109593" />
                  <Point X="2.162306793964" Y="2.766603737121" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.764406286272" Y="-0.808800783238" />
                  <Point X="-4.157591682865" Y="-0.486151735337" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.295647949219" Y="-0.027848122595" />
                  <Point X="2.251943070118" Y="2.921858345387" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.778707108312" Y="-0.708810510381" />
                  <Point X="-4.565515640248" Y="-0.595454596062" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.314087935871" Y="0.069941317398" />
                  <Point X="2.341579346272" Y="3.077112953652" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.361327238347" Y="0.152417889542" />
                  <Point X="2.431215622426" Y="3.232367561918" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.439935697755" Y="0.218215185081" />
                  <Point X="2.52085189858" Y="3.387622170184" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.558127321847" Y="0.262965738624" />
                  <Point X="2.610488174734" Y="3.542876778449" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.692677440809" Y="0.299018326156" />
                  <Point X="2.700124413967" Y="3.698131367083" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.827227559771" Y="0.335070913688" />
                  <Point X="2.789760632768" Y="3.853385944854" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.961777678733" Y="0.37112350122" />
                  <Point X="2.75827074592" Y="3.94423662983" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.096327810636" Y="0.407176081872" />
                  <Point X="2.671700440139" Y="4.00580053656" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.230877947349" Y="0.443228659965" />
                  <Point X="2.58001961132" Y="4.06464712999" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.365428084063" Y="0.479281238059" />
                  <Point X="2.483463147942" Y="4.120901302539" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.499978220776" Y="0.515333816152" />
                  <Point X="2.38690639698" Y="4.177155322178" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.63452835749" Y="0.551386394246" />
                  <Point X="2.290349646018" Y="4.233409341816" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.769078494203" Y="0.587438972339" />
                  <Point X="2.187277267368" Y="4.28619894076" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.770267520097" Y="0.694400910873" />
                  <Point X="-3.802725602669" Y="1.208852073897" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.453966980384" Y="1.394290322739" />
                  <Point X="2.079616221585" Y="4.336548702109" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.752986573132" Y="0.811183508178" />
                  <Point X="-3.964920793105" Y="1.230205516187" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.421220467564" Y="1.519296107275" />
                  <Point X="1.971954888977" Y="4.386898310952" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.735705678908" Y="0.927966077439" />
                  <Point X="-4.127115983542" Y="1.251558958478" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.440682945759" Y="1.616541878871" />
                  <Point X="1.86429355637" Y="4.437247919795" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.705890725268" Y="1.05141312431" />
                  <Point X="-4.289311173978" Y="1.272912400768" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.484010691467" Y="1.70109826264" />
                  <Point X="1.744706880964" Y="4.481256711396" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.6718266745" Y="1.177119456199" />
                  <Point X="-4.451506364415" Y="1.294265843058" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.549442375957" Y="1.773901773683" />
                  <Point X="1.624411426032" Y="4.524888638238" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.637762623732" Y="1.302825788089" />
                  <Point X="-4.613701555634" Y="1.315619284932" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.632268538474" Y="1.8374564767" />
                  <Point X="1.504115936079" Y="4.568520546459" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.71509470099" Y="1.901011179718" />
                  <Point X="1.372874588472" Y="4.606332438929" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.797920863506" Y="1.964565882735" />
                  <Point X="1.233709651999" Y="4.639931284465" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.880746893642" Y="2.02812065614" />
                  <Point X="-0.011446172403" Y="4.085464343557" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.211558199295" Y="4.204037871291" />
                  <Point X="1.094544648467" Y="4.673530094345" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.963572858262" Y="2.091675464381" />
                  <Point X="-0.132013517708" Y="4.128951703724" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.257961148277" Y="4.336304911737" />
                  <Point X="0.955379644935" Y="4.707128904225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.046398822881" Y="2.155230272622" />
                  <Point X="-0.1976485177" Y="4.201647109996" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.291580777426" Y="4.46177494046" />
                  <Point X="0.813709614049" Y="4.739395767435" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.1292247875" Y="2.218785080862" />
                  <Point X="-3.163365362726" Y="2.732341646674" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.761224229366" Y="2.946163880141" />
                  <Point X="-0.233315966594" Y="4.290276545831" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.32520032838" Y="4.587244927606" />
                  <Point X="0.644651942387" Y="4.757100363733" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.21205075212" Y="2.282339889103" />
                  <Point X="-3.280542035445" Y="2.777631859434" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.750132037684" Y="3.059655857891" />
                  <Point X="-0.25855051606" Y="4.384453252692" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.358819845236" Y="4.712714896622" />
                  <Point X="0.475594963622" Y="4.774805328451" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.153662037981" Y="2.420979873929" />
                  <Point X="-3.377555873656" Y="2.833642841471" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.761908643795" Y="3.160988280165" />
                  <Point X="-0.283785065526" Y="4.478629959553" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.062598887908" Y="2.576993164515" />
                  <Point X="-3.474569711866" Y="2.889653823509" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.798977015179" Y="3.248872832299" />
                  <Point X="-0.309019614993" Y="4.572806666413" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.954073844149" Y="2.742291108668" />
                  <Point X="-3.571583550077" Y="2.945664805546" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.846505962317" Y="3.331195397644" />
                  <Point X="-0.334254164459" Y="4.666983373274" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.811309545002" Y="2.925794387845" />
                  <Point X="-3.668597388288" Y="3.001675787583" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.894034909456" Y="3.413517962989" />
                  <Point X="-0.359488696559" Y="4.761160089368" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.94156388603" Y="3.495840512683" />
                  <Point X="-1.912555544306" Y="4.042973953236" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.496397345259" Y="4.264249192732" />
                  <Point X="-0.565737695944" Y="4.75908970594" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.989092880584" Y="3.578163052816" />
                  <Point X="-2.018279943257" Y="4.094353447972" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.463748689577" Y="4.389202945705" />
                  <Point X="-0.825204168849" Y="4.728723089912" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.036621875139" Y="3.66048559295" />
                  <Point X="-2.1043114593" Y="4.156203834287" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.470147847796" Y="4.493394607741" />
                  <Point X="-1.184414677685" Y="4.645321629227" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.820819436612" Y="3.882823939705" />
                  <Point X="-2.164511902969" Y="4.231788845414" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="0.001626220703" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.739844970703" Y="-4.538443359375" />
                  <Point X="0.471539031982" Y="-3.537112304688" />
                  <Point X="0.464318664551" Y="-3.521544189453" />
                  <Point X="0.354655303955" Y="-3.363540039062" />
                  <Point X="0.300591033936" Y="-3.285643798828" />
                  <Point X="0.274335754395" Y="-3.266399902344" />
                  <Point X="0.104638000488" Y="-3.213731933594" />
                  <Point X="0.020976625443" Y="-3.187766601562" />
                  <Point X="-0.008664908409" Y="-3.187766601562" />
                  <Point X="-0.17836265564" Y="-3.240434326172" />
                  <Point X="-0.262023895264" Y="-3.266399902344" />
                  <Point X="-0.288279144287" Y="-3.285643798828" />
                  <Point X="-0.3979425354" Y="-3.443647705078" />
                  <Point X="-0.45200680542" Y="-3.521544189453" />
                  <Point X="-0.459227142334" Y="-3.537112304688" />
                  <Point X="-0.591502441406" Y="-4.030770751953" />
                  <Point X="-0.84774395752" Y="-4.987077148438" />
                  <Point X="-1.002744018555" Y="-4.956990722656" />
                  <Point X="-1.100246582031" Y="-4.938065429688" />
                  <Point X="-1.296842529297" Y="-4.887482910156" />
                  <Point X="-1.351589599609" Y="-4.873396972656" />
                  <Point X="-1.341447998047" Y="-4.796363769531" />
                  <Point X="-1.309150146484" Y="-4.551038574219" />
                  <Point X="-1.309683227539" Y="-4.5347578125" />
                  <Point X="-1.350223999023" Y="-4.330945800781" />
                  <Point X="-1.370210693359" Y="-4.230466308594" />
                  <Point X="-1.386282714844" Y="-4.20262890625" />
                  <Point X="-1.542518798828" Y="-4.065613037109" />
                  <Point X="-1.619543457031" Y="-3.998063964844" />
                  <Point X="-1.649240722656" Y="-3.985762939453" />
                  <Point X="-1.856600830078" Y="-3.972171875" />
                  <Point X="-1.958829833984" Y="-3.965471435547" />
                  <Point X="-1.989878540039" Y="-3.973791015625" />
                  <Point X="-2.162662109375" Y="-4.089241455078" />
                  <Point X="-2.247844970703" Y="-4.146159179688" />
                  <Point X="-2.259734130859" Y="-4.157294433594" />
                  <Point X="-2.333996582031" Y="-4.254074707031" />
                  <Point X="-2.457095214844" Y="-4.414500488281" />
                  <Point X="-2.712918212891" Y="-4.256101074219" />
                  <Point X="-2.855831054688" Y="-4.16761328125" />
                  <Point X="-3.128062011719" Y="-3.958004394531" />
                  <Point X="-3.228581054688" Y="-3.880608154297" />
                  <Point X="-3.004649902344" Y="-3.492748046875" />
                  <Point X="-2.506033691406" Y="-2.629119628906" />
                  <Point X="-2.499762695312" Y="-2.597593261719" />
                  <Point X="-2.513979736328" Y="-2.568764404297" />
                  <Point X="-2.531328369141" Y="-2.551415771484" />
                  <Point X="-2.560157226562" Y="-2.537198730469" />
                  <Point X="-2.59168359375" Y="-2.543469726562" />
                  <Point X="-3.017454345703" Y="-2.789288574219" />
                  <Point X="-3.842959472656" Y="-3.265894042969" />
                  <Point X="-4.048792236328" Y="-2.995471923828" />
                  <Point X="-4.161703613281" Y="-2.847129394531" />
                  <Point X="-4.356870117188" Y="-2.519865234375" />
                  <Point X="-4.431020019531" Y="-2.395527099609" />
                  <Point X="-4.036203369141" Y="-2.092573730469" />
                  <Point X="-3.163786865234" Y="-1.423144897461" />
                  <Point X="-3.145822021484" Y="-1.396013793945" />
                  <Point X="-3.140661621094" Y="-1.376089233398" />
                  <Point X="-3.138117431641" Y="-1.366266479492" />
                  <Point X="-3.140326171875" Y="-1.334595703125" />
                  <Point X="-3.161158935547" Y="-1.310639160156" />
                  <Point X="-3.178896728516" Y="-1.300199584961" />
                  <Point X="-3.187641601562" Y="-1.295052734375" />
                  <Point X="-3.219529296875" Y="-1.288571044922" />
                  <Point X="-3.757024658203" Y="-1.359333740234" />
                  <Point X="-4.803283691406" Y="-1.497076293945" />
                  <Point X="-4.883232421875" Y="-1.184078857422" />
                  <Point X="-4.927393066406" Y="-1.011191650391" />
                  <Point X="-4.979029296875" Y="-0.650156555176" />
                  <Point X="-4.998396484375" Y="-0.514742126465" />
                  <Point X="-4.551277832031" Y="-0.394937042236" />
                  <Point X="-3.557463867188" Y="-0.128645431519" />
                  <Point X="-3.541895996094" Y="-0.121425079346" />
                  <Point X="-3.523307373047" Y="-0.108523483276" />
                  <Point X="-3.514143066406" Y="-0.102163047791" />
                  <Point X="-3.494898925781" Y="-0.075907470703" />
                  <Point X="-3.488702636719" Y="-0.055942966461" />
                  <Point X="-3.485647949219" Y="-0.04610043335" />
                  <Point X="-3.485647949219" Y="-0.016459506989" />
                  <Point X="-3.491844238281" Y="0.00350484395" />
                  <Point X="-3.494898925781" Y="0.013347376823" />
                  <Point X="-3.514142822266" Y="0.039602954865" />
                  <Point X="-3.532731445313" Y="0.052504547119" />
                  <Point X="-3.557463867188" Y="0.066085334778" />
                  <Point X="-4.047416259766" Y="0.197367630005" />
                  <Point X="-4.998186523438" Y="0.452125793457" />
                  <Point X="-4.94610546875" Y="0.8040859375" />
                  <Point X="-4.917645019531" Y="0.996418518066" />
                  <Point X="-4.813703125" Y="1.379995605469" />
                  <Point X="-4.773516113281" Y="1.528298583984" />
                  <Point X="-4.46565625" Y="1.487768188477" />
                  <Point X="-3.753266357422" Y="1.393980102539" />
                  <Point X="-3.731704833984" Y="1.395866577148" />
                  <Point X="-3.690562255859" Y="1.408838867188" />
                  <Point X="-3.670278808594" Y="1.415234130859" />
                  <Point X="-3.651534423828" Y="1.426056152344" />
                  <Point X="-3.639119873047" Y="1.443786132813" />
                  <Point X="-3.622611083984" Y="1.483641601562" />
                  <Point X="-3.614472412109" Y="1.503290527344" />
                  <Point X="-3.610714111328" Y="1.52460546875" />
                  <Point X="-3.616315917969" Y="1.54551159668" />
                  <Point X="-3.636235351562" Y="1.583776611328" />
                  <Point X="-3.646055664062" Y="1.602641235352" />
                  <Point X="-3.659968261719" Y="1.619221679688" />
                  <Point X="-3.941006347656" Y="1.834869628906" />
                  <Point X="-4.47610546875" Y="2.245466308594" />
                  <Point X="-4.270601074219" Y="2.597544677734" />
                  <Point X="-4.160014648438" Y="2.787006591797" />
                  <Point X="-3.884676025391" Y="3.140915283203" />
                  <Point X="-3.774671142578" Y="3.282310791016" />
                  <Point X="-3.596687011719" Y="3.179551513672" />
                  <Point X="-3.159156982422" Y="2.926943359375" />
                  <Point X="-3.138514648438" Y="2.920434814453" />
                  <Point X="-3.081214355469" Y="2.915421630859" />
                  <Point X="-3.052965332031" Y="2.912950195312" />
                  <Point X="-3.031506835938" Y="2.915775390625" />
                  <Point X="-3.013252441406" Y="2.927404541016" />
                  <Point X="-2.972580322266" Y="2.968076660156" />
                  <Point X="-2.952528808594" Y="2.988127929688" />
                  <Point X="-2.940899414062" Y="3.006382324219" />
                  <Point X="-2.93807421875" Y="3.027841064453" />
                  <Point X="-2.943087402344" Y="3.085141357422" />
                  <Point X="-2.945558837891" Y="3.113390625" />
                  <Point X="-2.952067382812" Y="3.134032958984" />
                  <Point X="-3.076603759766" Y="3.349736328125" />
                  <Point X="-3.307278564453" Y="3.749276611328" />
                  <Point X="-2.945482177734" Y="4.026662353516" />
                  <Point X="-2.752876220703" Y="4.174331542969" />
                  <Point X="-2.3192265625" Y="4.4152578125" />
                  <Point X="-2.141548583984" Y="4.513972167969" />
                  <Point X="-2.101768066406" Y="4.46212890625" />
                  <Point X="-1.967826904297" Y="4.287573242188" />
                  <Point X="-1.951247070312" Y="4.273661132813" />
                  <Point X="-1.887472167969" Y="4.240461914062" />
                  <Point X="-1.856031005859" Y="4.224094238281" />
                  <Point X="-1.835124511719" Y="4.2184921875" />
                  <Point X="-1.813809204102" Y="4.22225" />
                  <Point X="-1.747383300781" Y="4.249765136719" />
                  <Point X="-1.714635253906" Y="4.263329589844" />
                  <Point X="-1.696905517578" Y="4.275744140625" />
                  <Point X="-1.686083496094" Y="4.294488769531" />
                  <Point X="-1.664463012695" Y="4.363059570312" />
                  <Point X="-1.653804077148" Y="4.396865234375" />
                  <Point X="-1.651917602539" Y="4.418426757812" />
                  <Point X="-1.666076049805" Y="4.525970703125" />
                  <Point X="-1.689137573242" Y="4.701141113281" />
                  <Point X="-1.217434692383" Y="4.833390136719" />
                  <Point X="-0.968094177246" Y="4.903296386719" />
                  <Point X="-0.44237890625" Y="4.964823730469" />
                  <Point X="-0.224199630737" Y="4.990358398438" />
                  <Point X="-0.171573852539" Y="4.793956054688" />
                  <Point X="-0.042140380859" Y="4.310903320312" />
                  <Point X="-0.024282117844" Y="4.284176757813" />
                  <Point X="0.006155934334" Y="4.273844238281" />
                  <Point X="0.036594036102" Y="4.284176757813" />
                  <Point X="0.054452251434" Y="4.310903320312" />
                  <Point X="0.118263366699" Y="4.549049316406" />
                  <Point X="0.236648422241" Y="4.990868652344" />
                  <Point X="0.642493408203" Y="4.948365722656" />
                  <Point X="0.860205810547" Y="4.925565429688" />
                  <Point X="1.295143676758" Y="4.820558105469" />
                  <Point X="1.508459716797" Y="4.769057128906" />
                  <Point X="1.792042724609" Y="4.666199707031" />
                  <Point X="1.931033691406" Y="4.615786621094" />
                  <Point X="2.204779785156" Y="4.487764648438" />
                  <Point X="2.338695800781" Y="4.425136230469" />
                  <Point X="2.603148193359" Y="4.271065917969" />
                  <Point X="2.732523681641" Y="4.19569140625" />
                  <Point X="2.981934814453" Y="4.018323974609" />
                  <Point X="3.068740234375" Y="3.956592773438" />
                  <Point X="2.806964355469" Y="3.503183349609" />
                  <Point X="2.229853515625" Y="2.503597900391" />
                  <Point X="2.224852050781" Y="2.491514892578" />
                  <Point X="2.210471923828" Y="2.437739746094" />
                  <Point X="2.203382324219" Y="2.411228515625" />
                  <Point X="2.202044677734" Y="2.392326171875" />
                  <Point X="2.207651855469" Y="2.345826171875" />
                  <Point X="2.210416015625" Y="2.322901367188" />
                  <Point X="2.218682128906" Y="2.300812255859" />
                  <Point X="2.247454833984" Y="2.258408691406" />
                  <Point X="2.261639892578" Y="2.237503662109" />
                  <Point X="2.274940185547" Y="2.224203613281" />
                  <Point X="2.31734375" Y="2.195430908203" />
                  <Point X="2.338248779297" Y="2.18124609375" />
                  <Point X="2.360336669922" Y="2.172980224609" />
                  <Point X="2.406836914062" Y="2.167373046875" />
                  <Point X="2.429761474609" Y="2.164608642578" />
                  <Point X="2.448664306641" Y="2.165946289062" />
                  <Point X="2.502439453125" Y="2.180326660156" />
                  <Point X="2.528950683594" Y="2.187416015625" />
                  <Point X="2.541033935547" Y="2.192417480469" />
                  <Point X="3.033831787109" Y="2.476934326172" />
                  <Point X="3.994248046875" Y="3.031430908203" />
                  <Point X="4.125850585938" Y="2.848532958984" />
                  <Point X="4.202591796875" Y="2.741880126953" />
                  <Point X="4.341634765625" Y="2.512110595703" />
                  <Point X="4.387513183594" Y="2.436296142578" />
                  <Point X="4.048436523438" Y="2.176113525391" />
                  <Point X="3.288616210938" Y="1.593082763672" />
                  <Point X="3.27937109375" Y="1.583833129883" />
                  <Point X="3.240668945312" Y="1.533343383789" />
                  <Point X="3.221588867188" Y="1.508451782227" />
                  <Point X="3.213119628906" Y="1.49150012207" />
                  <Point X="3.198703125" Y="1.439949951172" />
                  <Point X="3.191595703125" Y="1.414535644531" />
                  <Point X="3.190779541016" Y="1.390965332031" />
                  <Point X="3.202614013672" Y="1.333609130859" />
                  <Point X="3.208448486328" Y="1.305332519531" />
                  <Point X="3.215646484375" Y="1.287954956055" />
                  <Point X="3.247834960938" Y="1.239029785156" />
                  <Point X="3.263704101562" Y="1.214909667969" />
                  <Point X="3.280947998047" Y="1.198819824219" />
                  <Point X="3.327593505859" Y="1.17256237793" />
                  <Point X="3.350590087891" Y="1.159617431641" />
                  <Point X="3.368565673828" Y="1.153619628906" />
                  <Point X="3.431633544922" Y="1.145284301758" />
                  <Point X="3.462726318359" Y="1.141175048828" />
                  <Point X="3.475803710938" Y="1.14117175293" />
                  <Point X="3.943928710938" Y="1.202801635742" />
                  <Point X="4.848975097656" Y="1.32195324707" />
                  <Point X="4.906228515625" Y="1.086774047852" />
                  <Point X="4.939188476562" Y="0.951385559082" />
                  <Point X="4.983004882812" Y="0.669959838867" />
                  <Point X="4.997858398438" Y="0.574556152344" />
                  <Point X="4.611938476562" Y="0.471149139404" />
                  <Point X="3.741167724609" Y="0.237826919556" />
                  <Point X="3.729086914062" Y="0.232819412231" />
                  <Point X="3.667124511719" Y="0.197003921509" />
                  <Point X="3.636576904297" Y="0.179346878052" />
                  <Point X="3.622264892578" Y="0.166926422119" />
                  <Point X="3.585087402344" Y="0.119553611755" />
                  <Point X="3.566759033203" Y="0.096198738098" />
                  <Point X="3.556985351562" Y="0.074735275269" />
                  <Point X="3.544592773438" Y="0.010026373863" />
                  <Point X="3.538483154297" Y="-0.021875303268" />
                  <Point X="3.538483154297" Y="-0.04068478775" />
                  <Point X="3.550875732422" Y="-0.105393692017" />
                  <Point X="3.556985351562" Y="-0.13729536438" />
                  <Point X="3.566759033203" Y="-0.158759140015" />
                  <Point X="3.603936279297" Y="-0.20613180542" />
                  <Point X="3.622264892578" Y="-0.229486831665" />
                  <Point X="3.636576904297" Y="-0.241906967163" />
                  <Point X="3.698539306641" Y="-0.277722442627" />
                  <Point X="3.729086914062" Y="-0.295379486084" />
                  <Point X="3.741167724609" Y="-0.300387023926" />
                  <Point X="4.170459472656" Y="-0.415415374756" />
                  <Point X="4.998067871094" Y="-0.637172363281" />
                  <Point X="4.966834960938" Y="-0.844334228516" />
                  <Point X="4.948431640625" Y="-0.966398681641" />
                  <Point X="4.892295898438" Y="-1.212395629883" />
                  <Point X="4.874545898438" Y="-1.290178344727" />
                  <Point X="4.421831054688" Y="-1.230577392578" />
                  <Point X="3.411982177734" Y="-1.097628295898" />
                  <Point X="3.394836181641" Y="-1.098341186523" />
                  <Point X="3.273226074219" Y="-1.124773681641" />
                  <Point X="3.213271972656" Y="-1.137804931641" />
                  <Point X="3.1854453125" Y="-1.154697143555" />
                  <Point X="3.111939697266" Y="-1.2431015625" />
                  <Point X="3.075701171875" Y="-1.286685180664" />
                  <Point X="3.064357910156" Y="-1.314070556641" />
                  <Point X="3.053822753906" Y="-1.428558105469" />
                  <Point X="3.04862890625" Y="-1.485000854492" />
                  <Point X="3.056360595703" Y="-1.516621826172" />
                  <Point X="3.123661376953" Y="-1.621303833008" />
                  <Point X="3.156840820312" Y="-1.672912353516" />
                  <Point X="3.168460693359" Y="-1.685540649414" />
                  <Point X="3.566846191406" Y="-1.991232666016" />
                  <Point X="4.339074707031" Y="-2.583784179688" />
                  <Point X="4.256008789062" Y="-2.718196777344" />
                  <Point X="4.204131835938" Y="-2.802141845703" />
                  <Point X="4.0880390625" Y="-2.967093017578" />
                  <Point X="4.056688232422" Y="-3.011638427734" />
                  <Point X="3.652861328125" Y="-2.778488525391" />
                  <Point X="2.753454589844" Y="-2.259215576172" />
                  <Point X="2.737340820313" Y="-2.253312744141" />
                  <Point X="2.592605224609" Y="-2.227173583984" />
                  <Point X="2.521250488281" Y="-2.214287109375" />
                  <Point X="2.489077636719" Y="-2.219244873047" />
                  <Point X="2.368837890625" Y="-2.282526123047" />
                  <Point X="2.309559570312" Y="-2.313723876953" />
                  <Point X="2.288599853516" Y="-2.334683837891" />
                  <Point X="2.225318603516" Y="-2.454923339844" />
                  <Point X="2.194120849609" Y="-2.514201904297" />
                  <Point X="2.189163085938" Y="-2.546374511719" />
                  <Point X="2.215302001953" Y="-2.691109863281" />
                  <Point X="2.228188720703" Y="-2.76246484375" />
                  <Point X="2.234091552734" Y="-2.778578613281" />
                  <Point X="2.490094238281" Y="-3.22198828125" />
                  <Point X="2.986673583984" Y="-4.082088623047" />
                  <Point X="2.896856445312" Y="-4.146243164062" />
                  <Point X="2.835298583984" Y="-4.190212402344" />
                  <Point X="2.705499023438" Y="-4.274229003906" />
                  <Point X="2.679775878906" Y="-4.290879394531" />
                  <Point X="2.369508056641" Y="-3.886530273438" />
                  <Point X="1.683177490234" Y="-2.992087402344" />
                  <Point X="1.670549316406" Y="-2.980467529297" />
                  <Point X="1.527801025391" Y="-2.888693603516" />
                  <Point X="1.45742590332" Y="-2.843448730469" />
                  <Point X="1.4258046875" Y="-2.835717041016" />
                  <Point X="1.269685180664" Y="-2.850083251953" />
                  <Point X="1.192717773438" Y="-2.857166015625" />
                  <Point X="1.165332885742" Y="-2.868509277344" />
                  <Point X="1.04478137207" Y="-2.968743896484" />
                  <Point X="0.985349243164" Y="-3.018159667969" />
                  <Point X="0.968456726074" Y="-3.045986328125" />
                  <Point X="0.932412475586" Y="-3.211818603516" />
                  <Point X="0.91464251709" Y="-3.29357421875" />
                  <Point X="0.91392956543" Y="-3.310720214844" />
                  <Point X="0.98647869873" Y="-3.861786132812" />
                  <Point X="1.127642211914" Y="-4.934028320312" />
                  <Point X="1.05258215332" Y="-4.950481933594" />
                  <Point X="0.994348999023" Y="-4.96324609375" />
                  <Point X="0.874437438965" Y="-4.985029785156" />
                  <Point X="0.860200317383" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#184" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.129433535499" Y="4.838177028909" Z="1.7" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.7" />
                  <Point X="-0.45434698919" Y="5.045764806741" Z="1.7" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.7" />
                  <Point X="-1.237088461377" Y="4.912813662684" Z="1.7" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.7" />
                  <Point X="-1.721804275216" Y="4.550724258786" Z="1.7" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.7" />
                  <Point X="-1.718304781699" Y="4.409374967938" Z="1.7" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.7" />
                  <Point X="-1.772673227017" Y="4.327239286749" Z="1.7" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.7" />
                  <Point X="-1.870540247716" Y="4.316091947127" Z="1.7" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.7" />
                  <Point X="-2.06825641938" Y="4.523846994904" Z="1.7" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.7" />
                  <Point X="-2.349665438443" Y="4.490245306979" Z="1.7" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.7" />
                  <Point X="-2.982060157824" Y="4.097622286191" Z="1.7" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.7" />
                  <Point X="-3.126061088333" Y="3.356016272433" Z="1.7" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.7" />
                  <Point X="-2.999053182556" Y="3.112063773088" Z="1.7" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.7" />
                  <Point X="-3.014091451655" Y="3.034712116231" Z="1.7" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.7" />
                  <Point X="-3.083012692519" Y="2.996511516617" Z="1.7" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.7" />
                  <Point X="-3.577843075864" Y="3.254132860412" Z="1.7" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.7" />
                  <Point X="-3.930295423001" Y="3.202897702492" Z="1.7" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.7" />
                  <Point X="-4.319939363342" Y="2.654029776433" Z="1.7" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.7" />
                  <Point X="-3.97760018604" Y="1.826482059631" Z="1.7" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.7" />
                  <Point X="-3.686741932212" Y="1.59196938707" Z="1.7" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.7" />
                  <Point X="-3.674961242716" Y="1.534055556059" Z="1.7" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.7" />
                  <Point X="-3.711753358111" Y="1.487804769401" Z="1.7" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.7" />
                  <Point X="-4.465286031611" Y="1.568620480768" Z="1.7" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.7" />
                  <Point X="-4.868119024286" Y="1.424352983686" Z="1.7" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.7" />
                  <Point X="-5.00172280124" Y="0.842684967303" Z="1.7" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.7" />
                  <Point X="-4.066513210302" Y="0.180351403991" Z="1.7" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.7" />
                  <Point X="-3.567396495854" Y="0.04270852628" Z="1.7" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.7" />
                  <Point X="-3.545753060319" Y="0.01996443795" Z="1.7" />
                  <Point X="-3.539556741714" Y="0" Z="1.7" />
                  <Point X="-3.542611540816" Y="-0.009842513111" Z="1.7" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.7" />
                  <Point X="-3.557972099269" Y="-0.036167456854" Z="1.7" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.7" />
                  <Point X="-4.57037457847" Y="-0.315360652765" Z="1.7" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.7" />
                  <Point X="-5.03468128953" Y="-0.625955498345" Z="1.7" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.7" />
                  <Point X="-4.937833125927" Y="-1.165173103501" Z="1.7" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.7" />
                  <Point X="-3.75665418293" Y="-1.377625892416" Z="1.7" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.7" />
                  <Point X="-3.210414110146" Y="-1.312010103578" Z="1.7" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.7" />
                  <Point X="-3.195220397131" Y="-1.33227264126" Z="1.7" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.7" />
                  <Point X="-4.072797294467" Y="-2.021625861309" Z="1.7" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.7" />
                  <Point X="-4.40596926314" Y="-2.514194778188" Z="1.7" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.7" />
                  <Point X="-4.094734173391" Y="-2.994475097951" Z="1.7" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.7" />
                  <Point X="-2.998610330662" Y="-2.801309871748" Z="1.7" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.7" />
                  <Point X="-2.567110864516" Y="-2.561219604615" Z="1.7" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.7" />
                  <Point X="-3.054107141421" Y="-3.43646805183" Z="1.7" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.7" />
                  <Point X="-3.164721934912" Y="-3.966341543624" Z="1.7" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.7" />
                  <Point X="-2.745395778957" Y="-4.267331923138" Z="1.7" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.7" />
                  <Point X="-2.300484708943" Y="-4.253232831091" Z="1.7" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.7" />
                  <Point X="-2.141039579276" Y="-4.099534836479" Z="1.7" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.7" />
                  <Point X="-1.866026981808" Y="-3.990784818675" Z="1.7" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.7" />
                  <Point X="-1.581642091844" Y="-4.071924925834" Z="1.7" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.7" />
                  <Point X="-1.40541929354" Y="-4.309420266168" Z="1.7" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.7" />
                  <Point X="-1.397176219213" Y="-4.75855739091" Z="1.7" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.7" />
                  <Point X="-1.315457272862" Y="-4.904625630733" Z="1.7" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.7" />
                  <Point X="-1.018427158314" Y="-4.974794906503" Z="1.7" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.7" />
                  <Point X="-0.549362190992" Y="-4.01243166835" Z="1.7" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.7" />
                  <Point X="-0.363022476829" Y="-3.440876725295" Z="1.7" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.7" />
                  <Point X="-0.169697719454" Y="-3.256907278127" Z="1.7" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.7" />
                  <Point X="0.083661359907" Y="-3.230204767161" Z="1.7" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.7" />
                  <Point X="0.307423382604" Y="-3.360769030473" Z="1.7" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.7" />
                  <Point X="0.685392669409" Y="-4.520104361466" Z="1.7" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.7" />
                  <Point X="0.87721859476" Y="-5.002944795799" Z="1.7" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.7" />
                  <Point X="1.0571316454" Y="-4.968041981196" Z="1.7" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.7" />
                  <Point X="1.029894998943" Y="-3.823979628599" Z="1.7" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.7" />
                  <Point X="0.975115708799" Y="-3.191158100205" Z="1.7" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.7" />
                  <Point X="1.07059220249" Y="-2.975910099437" Z="1.7" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.7" />
                  <Point X="1.268110997722" Y="-2.868592843794" Z="1.7" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.7" />
                  <Point X="1.494605660294" Y="-2.899471342927" Z="1.7" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.7" />
                  <Point X="2.323684252007" Y="-3.885688219946" Z="1.7" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.7" />
                  <Point X="2.726512284826" Y="-4.284923269893" Z="1.7" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.7" />
                  <Point X="2.919761941913" Y="-4.155650021361" Z="1.7" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.7" />
                  <Point X="2.527239642241" Y="-3.165708199036" Z="1.7" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.7" />
                  <Point X="2.258350245164" Y="-2.650943689118" Z="1.7" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.7" />
                  <Point X="2.263408923891" Y="-2.44692978986" Z="1.7" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.7" />
                  <Point X="2.385968588512" Y="-2.295492386076" Z="1.7" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.7" />
                  <Point X="2.577563114394" Y="-2.245097764327" Z="1.7" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.7" />
                  <Point X="3.621705398071" Y="-2.790509858034" Z="1.7" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.7" />
                  <Point X="4.122771808405" Y="-2.96459018521" Z="1.7" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.7" />
                  <Point X="4.292386005877" Y="-2.713201825202" Z="1.7" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.7" />
                  <Point X="3.591128243248" Y="-1.920284953338" Z="1.7" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.7" />
                  <Point X="3.159563354236" Y="-1.562984543507" Z="1.7" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.7" />
                  <Point X="3.097456342017" Y="-1.401859845125" Z="1.7" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.7" />
                  <Point X="3.144229898983" Y="-1.243788635974" Z="1.7" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.7" />
                  <Point X="3.27768964184" Y="-1.142352908468" Z="1.7" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.7" />
                  <Point X="4.409148884883" Y="-1.248869560737" Z="1.7" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.7" />
                  <Point X="4.934886801273" Y="-1.192239555332" Z="1.7" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.7" />
                  <Point X="5.010120514147" Y="-0.820508192406" Z="1.7" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.7" />
                  <Point X="4.177244557395" Y="-0.335839038193" Z="1.7" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.7" />
                  <Point X="3.717405102286" Y="-0.203153578043" Z="1.7" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.7" />
                  <Point X="3.637114156266" Y="-0.143983316807" Z="1.7" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.7" />
                  <Point X="3.593827204234" Y="-0.064708898005" Z="1.7" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.7" />
                  <Point X="3.58754424619" Y="0.031901633193" Z="1.7" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.7" />
                  <Point X="3.618265282134" Y="0.119965421768" Z="1.7" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.7" />
                  <Point X="3.685990312066" Y="0.184995266199" Z="1.7" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.7" />
                  <Point X="4.618723259912" Y="0.454132894576" Z="1.7" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.7" />
                  <Point X="5.026253630123" Y="0.708931777884" Z="1.7" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.7" />
                  <Point X="4.948653694054" Y="1.129880875598" Z="1.7" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.7" />
                  <Point X="3.931246348573" Y="1.283653918903" Z="1.7" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.7" />
                  <Point X="3.432028704986" Y="1.22613336343" Z="1.7" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.7" />
                  <Point X="3.345964205965" Y="1.247413568" Z="1.7" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.7" />
                  <Point X="3.283449531212" Y="1.297791245093" Z="1.7" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.7" />
                  <Point X="3.245426551594" Y="1.374993095711" Z="1.7" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.7" />
                  <Point X="3.240699428708" Y="1.457763547961" Z="1.7" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.7" />
                  <Point X="3.274196159341" Y="1.534205292421" Z="1.7" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.7" />
                  <Point X="4.072718653904" Y="2.167725852457" Z="1.7" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.7" />
                  <Point X="4.378256035397" Y="2.569276840375" Z="1.7" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.7" />
                  <Point X="4.160280221175" Y="2.909015939849" Z="1.7" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.7" />
                  <Point X="3.002675777698" Y="2.551515727334" Z="1.7" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.7" />
                  <Point X="2.483366701054" Y="2.259909322487" Z="1.7" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.7" />
                  <Point X="2.40666696382" Y="2.248293468038" Z="1.7" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.7" />
                  <Point X="2.33926170243" Y="2.268085480732" Z="1.7" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.7" />
                  <Point X="2.282673119503" Y="2.317763157952" Z="1.7" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.7" />
                  <Point X="2.251136234721" Y="2.383091480013" Z="1.7" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.7" />
                  <Point X="2.25261862017" Y="2.456102874828" Z="1.7" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.7" />
                  <Point X="2.844109625525" Y="3.509463304257" Z="1.7" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.7" />
                  <Point X="3.004755827691" Y="4.090351201002" Z="1.7" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.7" />
                  <Point X="2.622162261773" Y="4.345548412883" Z="1.7" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.7" />
                  <Point X="2.21980542286" Y="4.564336083476" Z="1.7" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.7" />
                  <Point X="1.802935175926" Y="4.744483030455" Z="1.7" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.7" />
                  <Point X="1.30072204223" Y="4.900441822969" Z="1.7" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.7" />
                  <Point X="0.641545356148" Y="5.029373966098" Z="1.7" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.7" />
                  <Point X="0.063811026054" Y="4.593270477042" Z="1.7" />
                  <Point X="0" Y="4.355124473572" Z="1.7" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>