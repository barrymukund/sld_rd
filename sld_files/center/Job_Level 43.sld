<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#201" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="3084" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="0" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="0.004715820312" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.894691650391" Y="-4.749287109375" />
                  <Point X="0.563302062988" Y="-3.512524658203" />
                  <Point X="0.557721191406" Y="-3.497141845703" />
                  <Point X="0.542363037109" Y="-3.467377197266" />
                  <Point X="0.406915893555" Y="-3.272223388672" />
                  <Point X="0.378635528564" Y="-3.231476806641" />
                  <Point X="0.356751434326" Y="-3.209021240234" />
                  <Point X="0.330495941162" Y="-3.18977734375" />
                  <Point X="0.302495178223" Y="-3.175669189453" />
                  <Point X="0.092898277283" Y="-3.110617919922" />
                  <Point X="0.049136188507" Y="-3.097035888672" />
                  <Point X="0.020976638794" Y="-3.092766601562" />
                  <Point X="-0.008664840698" Y="-3.092766601562" />
                  <Point X="-0.036824237823" Y="-3.097035888672" />
                  <Point X="-0.246421142578" Y="-3.162086914062" />
                  <Point X="-0.290183227539" Y="-3.175669189453" />
                  <Point X="-0.31818460083" Y="-3.189777587891" />
                  <Point X="-0.344439788818" Y="-3.209021484375" />
                  <Point X="-0.366323577881" Y="-3.231476806641" />
                  <Point X="-0.501770874023" Y="-3.426630371094" />
                  <Point X="-0.530051208496" Y="-3.467377197266" />
                  <Point X="-0.538189025879" Y="-3.481573730469" />
                  <Point X="-0.550990112305" Y="-3.512524414063" />
                  <Point X="-0.620181640625" Y="-3.770751464844" />
                  <Point X="-0.916584533691" Y="-4.876941894531" />
                  <Point X="-1.031844238281" Y="-4.854569335938" />
                  <Point X="-1.079340332031" Y="-4.845350585938" />
                  <Point X="-1.24641796875" Y="-4.802362792969" />
                  <Point X="-1.214963012695" Y="-4.563438476562" />
                  <Point X="-1.214201171875" Y="-4.5479296875" />
                  <Point X="-1.216508666992" Y="-4.516224121094" />
                  <Point X="-1.266581054688" Y="-4.2644921875" />
                  <Point X="-1.277036132812" Y="-4.211932617188" />
                  <Point X="-1.287938354492" Y="-4.182966308594" />
                  <Point X="-1.304010375977" Y="-4.15512890625" />
                  <Point X="-1.32364453125" Y="-4.131204101562" />
                  <Point X="-1.516614868164" Y="-3.961973388672" />
                  <Point X="-1.556905517578" Y="-3.926639404297" />
                  <Point X="-1.583188476562" Y="-3.910295410156" />
                  <Point X="-1.612885742188" Y="-3.897994384766" />
                  <Point X="-1.64302734375" Y="-3.890966308594" />
                  <Point X="-1.899141845703" Y="-3.8741796875" />
                  <Point X="-1.952616577148" Y="-3.870674804688" />
                  <Point X="-1.98341796875" Y="-3.873708496094" />
                  <Point X="-2.014466674805" Y="-3.882028076172" />
                  <Point X="-2.042657836914" Y="-3.894801513672" />
                  <Point X="-2.256066162109" Y="-4.037396728516" />
                  <Point X="-2.300624267578" Y="-4.067169677734" />
                  <Point X="-2.312785644531" Y="-4.076821533203" />
                  <Point X="-2.335102539062" Y="-4.099461914062" />
                  <Point X="-2.373948242188" Y="-4.150086425781" />
                  <Point X="-2.480148925781" Y="-4.288489746094" />
                  <Point X="-2.732097167969" Y="-4.132489746094" />
                  <Point X="-2.801707763672" Y="-4.089388671875" />
                  <Point X="-3.104721923828" Y="-3.856077880859" />
                  <Point X="-3.039611328125" Y="-3.743303222656" />
                  <Point X="-2.423761230469" Y="-2.676619628906" />
                  <Point X="-2.412859130859" Y="-2.647653320312" />
                  <Point X="-2.406588134766" Y="-2.616126953125" />
                  <Point X="-2.405575439453" Y="-2.585192871094" />
                  <Point X="-2.414560058594" Y="-2.555575195312" />
                  <Point X="-2.428777099609" Y="-2.526746337891" />
                  <Point X="-2.4468046875" Y="-2.501589355469" />
                  <Point X="-2.461156738281" Y="-2.487237304688" />
                  <Point X="-2.464153320312" Y="-2.484240722656" />
                  <Point X="-2.489310302734" Y="-2.466213134766" />
                  <Point X="-2.518139160156" Y="-2.45199609375" />
                  <Point X="-2.547756835938" Y="-2.443011474609" />
                  <Point X="-2.578690917969" Y="-2.444024169922" />
                  <Point X="-2.610217285156" Y="-2.450295166016" />
                  <Point X="-2.63918359375" Y="-2.461197265625" />
                  <Point X="-2.861899414062" Y="-2.589782226562" />
                  <Point X="-3.818024169922" Y="-3.141801269531" />
                  <Point X="-4.027862792969" Y="-2.866116699219" />
                  <Point X="-4.082858398438" Y="-2.793864013672" />
                  <Point X="-4.306142578125" Y="-2.419450195312" />
                  <Point X="-4.1834921875" Y="-2.325337402344" />
                  <Point X="-3.105954589844" Y="-1.498513427734" />
                  <Point X="-3.084577148438" Y="-1.475593261719" />
                  <Point X="-3.066612304688" Y="-1.448462036133" />
                  <Point X="-3.053856445312" Y="-1.419832763672" />
                  <Point X="-3.047482666016" Y="-1.395223754883" />
                  <Point X="-3.046151855469" Y="-1.390085449219" />
                  <Point X="-3.04334765625" Y="-1.359656982422" />
                  <Point X="-3.045556396484" Y="-1.327986572266" />
                  <Point X="-3.052557373047" Y="-1.298241577148" />
                  <Point X="-3.068639648438" Y="-1.27225793457" />
                  <Point X="-3.089471923828" Y="-1.248301513672" />
                  <Point X="-3.112972167969" Y="-1.228767211914" />
                  <Point X="-3.134880615234" Y="-1.215872924805" />
                  <Point X="-3.139454833984" Y="-1.213180664062" />
                  <Point X="-3.168717285156" Y="-1.201956665039" />
                  <Point X="-3.20060546875" Y="-1.195474853516" />
                  <Point X="-3.231929199219" Y="-1.194383789062" />
                  <Point X="-3.513086669922" Y="-1.231398925781" />
                  <Point X="-4.732102050781" Y="-1.391885253906" />
                  <Point X="-4.812566894531" Y="-1.076868774414" />
                  <Point X="-4.834077636719" Y="-0.992654846191" />
                  <Point X="-4.892424316406" Y="-0.584698120117" />
                  <Point X="-4.760354003906" Y="-0.549309997559" />
                  <Point X="-3.532875976562" Y="-0.220408447266" />
                  <Point X="-3.517492675781" Y="-0.214827255249" />
                  <Point X="-3.487728759766" Y="-0.199469406128" />
                  <Point X="-3.46476953125" Y="-0.183534454346" />
                  <Point X="-3.459975830078" Y="-0.180207351685" />
                  <Point X="-3.437520751953" Y="-0.158323730469" />
                  <Point X="-3.418276855469" Y="-0.13206854248" />
                  <Point X="-3.404168212891" Y="-0.10406716156" />
                  <Point X="-3.396515136719" Y="-0.079408660889" />
                  <Point X="-3.394917236328" Y="-0.07426007843" />
                  <Point X="-3.390647949219" Y="-0.046100681305" />
                  <Point X="-3.390647949219" Y="-0.016459354401" />
                  <Point X="-3.394917236328" Y="0.011700042725" />
                  <Point X="-3.4025703125" Y="0.036358547211" />
                  <Point X="-3.404168212891" Y="0.041507125854" />
                  <Point X="-3.418276611328" Y="0.069508506775" />
                  <Point X="-3.437520507812" Y="0.095763694763" />
                  <Point X="-3.459975830078" Y="0.117647171021" />
                  <Point X="-3.482935058594" Y="0.133582122803" />
                  <Point X="-3.493405761719" Y="0.139892059326" />
                  <Point X="-3.513767578125" Y="0.150439666748" />
                  <Point X="-3.532875976562" Y="0.157848251343" />
                  <Point X="-3.789164550781" Y="0.226520507812" />
                  <Point X="-4.89181640625" Y="0.521975280762" />
                  <Point X="-4.838350097656" Y="0.883296508789" />
                  <Point X="-4.824487792969" Y="0.976975402832" />
                  <Point X="-4.703551269531" Y="1.423267822266" />
                  <Point X="-4.645552246094" Y="1.415632202148" />
                  <Point X="-3.765666259766" Y="1.29979284668" />
                  <Point X="-3.744986083984" Y="1.299341674805" />
                  <Point X="-3.723424560547" Y="1.301228149414" />
                  <Point X="-3.703137695312" Y="1.305263427734" />
                  <Point X="-3.652321777344" Y="1.321285644531" />
                  <Point X="-3.641711669922" Y="1.324630981445" />
                  <Point X="-3.622779052734" Y="1.332961669922" />
                  <Point X="-3.604034667969" Y="1.343783691406" />
                  <Point X="-3.587353027344" Y="1.356014892578" />
                  <Point X="-3.573714599609" Y="1.371566894531" />
                  <Point X="-3.561300292969" Y="1.389296630859" />
                  <Point X="-3.5513515625" Y="1.407431030273" />
                  <Point X="-3.530961425781" Y="1.456657104492" />
                  <Point X="-3.526704101563" Y="1.466935302734" />
                  <Point X="-3.520916015625" Y="1.486793212891" />
                  <Point X="-3.517157470703" Y="1.508108520508" />
                  <Point X="-3.5158046875" Y="1.528749389648" />
                  <Point X="-3.518951416016" Y="1.549193847656" />
                  <Point X="-3.524553466797" Y="1.570100463867" />
                  <Point X="-3.532050048828" Y="1.589377929688" />
                  <Point X="-3.556652832031" Y="1.636639770508" />
                  <Point X="-3.561789794922" Y="1.64650769043" />
                  <Point X="-3.573281982422" Y="1.66370690918" />
                  <Point X="-3.587194335938" Y="1.680286987305" />
                  <Point X="-3.602135986328" Y="1.694590087891" />
                  <Point X="-3.749143798828" Y="1.807393188477" />
                  <Point X="-4.351859863281" Y="2.269874267578" />
                  <Point X="-4.135015136719" Y="2.641382080078" />
                  <Point X="-4.081153808594" Y="2.733659423828" />
                  <Point X="-3.750505126953" Y="3.158661865234" />
                  <Point X="-3.74705859375" Y="3.156671875" />
                  <Point X="-3.206656982422" Y="2.844670898438" />
                  <Point X="-3.187724121094" Y="2.836340332031" />
                  <Point X="-3.167081787109" Y="2.829831787109" />
                  <Point X="-3.146794677734" Y="2.825796386719" />
                  <Point X="-3.076021972656" Y="2.819604492188" />
                  <Point X="-3.061245361328" Y="2.818311767578" />
                  <Point X="-3.040564697266" Y="2.818762939453" />
                  <Point X="-3.019106201172" Y="2.821588134766" />
                  <Point X="-2.999015136719" Y="2.826504394531" />
                  <Point X="-2.980464111328" Y="2.835652587891" />
                  <Point X="-2.962209960938" Y="2.847281494141" />
                  <Point X="-2.946077636719" Y="2.860229248047" />
                  <Point X="-2.895842773438" Y="2.910464111328" />
                  <Point X="-2.885354003906" Y="2.920952636719" />
                  <Point X="-2.872407470703" Y="2.937083251953" />
                  <Point X="-2.860777832031" Y="2.955337646484" />
                  <Point X="-2.85162890625" Y="2.973889404297" />
                  <Point X="-2.846712158203" Y="2.993981689453" />
                  <Point X="-2.843886962891" Y="3.015440429688" />
                  <Point X="-2.843435791016" Y="3.036120849609" />
                  <Point X="-2.849627685547" Y="3.106893554688" />
                  <Point X="-2.850920410156" Y="3.121670410156" />
                  <Point X="-2.854955566406" Y="3.141957275391" />
                  <Point X="-2.861464111328" Y="3.162599853516" />
                  <Point X="-2.869794921875" Y="3.181532958984" />
                  <Point X="-2.934938476562" Y="3.294364746094" />
                  <Point X="-3.183332763672" Y="3.724596435547" />
                  <Point X="-2.794429931641" Y="4.022764404297" />
                  <Point X="-2.700625488281" Y="4.094683837891" />
                  <Point X="-2.170374023438" Y="4.389280273438" />
                  <Point X="-2.167036865234" Y="4.391134277344" />
                  <Point X="-2.043195922852" Y="4.229741699219" />
                  <Point X="-2.028892822266" Y="4.214800292969" />
                  <Point X="-2.012313720703" Y="4.200888183594" />
                  <Point X="-1.995113769531" Y="4.189395019531" />
                  <Point X="-1.916344116211" Y="4.148390136719" />
                  <Point X="-1.899897705078" Y="4.139828125" />
                  <Point X="-1.880619506836" Y="4.132331054688" />
                  <Point X="-1.859712402344" Y="4.126729003906" />
                  <Point X="-1.839268920898" Y="4.123582519531" />
                  <Point X="-1.81862902832" Y="4.124935058594" />
                  <Point X="-1.797313110352" Y="4.128693359375" />
                  <Point X="-1.777453491211" Y="4.134481933594" />
                  <Point X="-1.695409667969" Y="4.168465820312" />
                  <Point X="-1.678279541016" Y="4.175561523437" />
                  <Point X="-1.660145385742" Y="4.185510253906" />
                  <Point X="-1.642416015625" Y="4.197924316406" />
                  <Point X="-1.626863891602" Y="4.211562988281" />
                  <Point X="-1.614632568359" Y="4.228244628906" />
                  <Point X="-1.603810791016" Y="4.246988769531" />
                  <Point X="-1.59548046875" Y="4.265920898438" />
                  <Point X="-1.568776489258" Y="4.350614257813" />
                  <Point X="-1.563201171875" Y="4.368297363281" />
                  <Point X="-1.559165649414" Y="4.388584960938" />
                  <Point X="-1.557279174805" Y="4.410146972656" />
                  <Point X="-1.55773034668" Y="4.430826660156" />
                  <Point X="-1.565136474609" Y="4.487081542969" />
                  <Point X="-1.584201904297" Y="4.6318984375" />
                  <Point X="-1.071070922852" Y="4.775762695312" />
                  <Point X="-0.949637634277" Y="4.809808105469" />
                  <Point X="-0.306813079834" Y="4.885041015625" />
                  <Point X="-0.293769134521" Y="4.882942871094" />
                  <Point X="-0.133903305054" Y="4.286315429688" />
                  <Point X="-0.121129844666" Y="4.258124023438" />
                  <Point X="-0.10327155304" Y="4.231397460938" />
                  <Point X="-0.0821145401" Y="4.20880859375" />
                  <Point X="-0.054819446564" Y="4.19421875" />
                  <Point X="-0.024381370544" Y="4.183886230469" />
                  <Point X="0.006155909061" Y="4.178844238281" />
                  <Point X="0.03669316864" Y="4.183886230469" />
                  <Point X="0.067131248474" Y="4.19421875" />
                  <Point X="0.094426467896" Y="4.20880859375" />
                  <Point X="0.115583503723" Y="4.231397460938" />
                  <Point X="0.133441802979" Y="4.258124023438" />
                  <Point X="0.146215255737" Y="4.286315429688" />
                  <Point X="0.17959413147" Y="4.410887207031" />
                  <Point X="0.307419342041" Y="4.8879375" />
                  <Point X="0.738002197266" Y="4.84284375" />
                  <Point X="0.84404095459" Y="4.831738769531" />
                  <Point X="1.375868408203" Y="4.703339355469" />
                  <Point X="1.481028930664" Y="4.677950195312" />
                  <Point X="1.826940673828" Y="4.552485839844" />
                  <Point X="1.894645751953" Y="4.527928710938" />
                  <Point X="2.229374755859" Y="4.371387207031" />
                  <Point X="2.294570068359" Y="4.340896972656" />
                  <Point X="2.617960693359" Y="4.152489257813" />
                  <Point X="2.680977539062" Y="4.115775390625" />
                  <Point X="2.943260253906" Y="3.929254638672" />
                  <Point X="2.860381347656" Y="3.785704345703" />
                  <Point X="2.147581054688" Y="2.551097900391" />
                  <Point X="2.142075927734" Y="2.539931152344" />
                  <Point X="2.133076904297" Y="2.516056884766" />
                  <Point X="2.115315673828" Y="2.449638427734" />
                  <Point X="2.113197509766" Y="2.439555419922" />
                  <Point X="2.108151123047" Y="2.406785400391" />
                  <Point X="2.107727783203" Y="2.380953369141" />
                  <Point X="2.114653320313" Y="2.323520263672" />
                  <Point X="2.116099121094" Y="2.311528564453" />
                  <Point X="2.121442138672" Y="2.289604492188" />
                  <Point X="2.129708984375" Y="2.267514648438" />
                  <Point X="2.140071289062" Y="2.247470703125" />
                  <Point X="2.175608886719" Y="2.195097167969" />
                  <Point X="2.182013427734" Y="2.186703369141" />
                  <Point X="2.202733398438" Y="2.16246875" />
                  <Point X="2.221599121094" Y="2.145592529297" />
                  <Point X="2.273972412109" Y="2.110054931641" />
                  <Point X="2.284907714844" Y="2.102635009766" />
                  <Point X="2.304952392578" Y="2.092272216797" />
                  <Point X="2.327040283203" Y="2.084006347656" />
                  <Point X="2.348963623047" Y="2.078663574219" />
                  <Point X="2.406396972656" Y="2.071738037109" />
                  <Point X="2.417436767578" Y="2.071055175781" />
                  <Point X="2.448331298828" Y="2.070946777344" />
                  <Point X="2.473206542969" Y="2.074171142578" />
                  <Point X="2.539625244141" Y="2.091932373047" />
                  <Point X="2.545311279297" Y="2.093645019531" />
                  <Point X="2.571261962891" Y="2.102354980469" />
                  <Point X="2.588533935547" Y="2.110145019531" />
                  <Point X="2.846310791016" Y="2.258972412109" />
                  <Point X="3.967326416016" Y="2.906191162109" />
                  <Point X="4.085891357422" Y="2.741412841797" />
                  <Point X="4.123270019531" Y="2.689465087891" />
                  <Point X="4.262198730469" Y="2.459884277344" />
                  <Point X="4.169251953125" Y="2.388563476562" />
                  <Point X="3.230783935547" Y="1.668451171875" />
                  <Point X="3.221424560547" Y="1.660241455078" />
                  <Point X="3.203973876953" Y="1.641627929688" />
                  <Point X="3.156172363281" Y="1.579267089844" />
                  <Point X="3.150668945312" Y="1.571272705078" />
                  <Point X="3.13221875" Y="1.541300170898" />
                  <Point X="3.121630126953" Y="1.517086181641" />
                  <Point X="3.103823974609" Y="1.453415649414" />
                  <Point X="3.100106201172" Y="1.440121582031" />
                  <Point X="3.096652587891" Y="1.417823120117" />
                  <Point X="3.095836425781" Y="1.394252685547" />
                  <Point X="3.097739501953" Y="1.371767822266" />
                  <Point X="3.112356445312" Y="1.300926147461" />
                  <Point X="3.114880615234" Y="1.29128125" />
                  <Point X="3.125130859375" Y="1.259112670898" />
                  <Point X="3.136282714844" Y="1.235740234375" />
                  <Point X="3.176039306641" Y="1.175312011719" />
                  <Point X="3.184340332031" Y="1.162694824219" />
                  <Point X="3.198893798828" Y="1.145450195312" />
                  <Point X="3.216137451172" Y="1.129360595703" />
                  <Point X="3.234347167969" Y="1.116034790039" />
                  <Point X="3.291959960938" Y="1.083603637695" />
                  <Point X="3.301360107422" Y="1.078975341797" />
                  <Point X="3.331364746094" Y="1.066206176758" />
                  <Point X="3.356118164062" Y="1.059438598633" />
                  <Point X="3.434014648438" Y="1.049143676758" />
                  <Point X="3.439510253906" Y="1.048579223633" />
                  <Point X="3.468852050781" Y="1.046426391602" />
                  <Point X="3.488203613281" Y="1.04698449707" />
                  <Point X="3.733074462891" Y="1.079222412109" />
                  <Point X="4.776839355469" Y="1.21663671875" />
                  <Point X="4.829881347656" Y="0.998755859375" />
                  <Point X="4.845936035156" Y="0.932809265137" />
                  <Point X="4.890865234375" Y="0.644238586426" />
                  <Point X="4.792084472656" Y="0.617770446777" />
                  <Point X="3.716579833984" Y="0.32958984375" />
                  <Point X="3.704790527344" Y="0.325586364746" />
                  <Point X="3.681545898438" Y="0.315068054199" />
                  <Point X="3.605015136719" Y="0.270831787109" />
                  <Point X="3.597305175781" Y="0.265863983154" />
                  <Point X="3.567013671875" Y="0.244207275391" />
                  <Point X="3.547530761719" Y="0.225576522827" />
                  <Point X="3.501612304688" Y="0.167065414429" />
                  <Point X="3.492024902344" Y="0.154848876953" />
                  <Point X="3.480301025391" Y="0.135569213867" />
                  <Point X="3.47052734375" Y="0.114106048584" />
                  <Point X="3.463680908203" Y="0.092604484558" />
                  <Point X="3.448374755859" Y="0.012681367874" />
                  <Point X="3.447062011719" Y="0.003332292795" />
                  <Point X="3.443865722656" Y="-0.032164955139" />
                  <Point X="3.445178710938" Y="-0.058553718567" />
                  <Point X="3.460485107422" Y="-0.138476837158" />
                  <Point X="3.463680908203" Y="-0.155164367676" />
                  <Point X="3.470527099609" Y="-0.176665481567" />
                  <Point X="3.480301025391" Y="-0.198129257202" />
                  <Point X="3.492025146484" Y="-0.217409057617" />
                  <Point X="3.537943603516" Y="-0.275920013428" />
                  <Point X="3.544489257812" Y="-0.283416473389" />
                  <Point X="3.568388671875" Y="-0.308053588867" />
                  <Point X="3.589035888672" Y="-0.324155670166" />
                  <Point X="3.665566894531" Y="-0.368391784668" />
                  <Point X="3.670106689453" Y="-0.370853820801" />
                  <Point X="3.698166503906" Y="-0.385097686768" />
                  <Point X="3.716579833984" Y="-0.392150024414" />
                  <Point X="3.941137451172" Y="-0.452320037842" />
                  <Point X="4.891472167969" Y="-0.706961425781" />
                  <Point X="4.863986816406" Y="-0.889267883301" />
                  <Point X="4.855021484375" Y="-0.948733032227" />
                  <Point X="4.801174316406" Y="-1.18469909668" />
                  <Point X="4.671665039062" Y="-1.167648803711" />
                  <Point X="3.424382324219" Y="-1.003440979004" />
                  <Point X="3.40803515625" Y="-1.002710266113" />
                  <Point X="3.374658447266" Y="-1.005508789063" />
                  <Point X="3.224455322266" Y="-1.038156005859" />
                  <Point X="3.193094238281" Y="-1.04497253418" />
                  <Point X="3.163973876953" Y="-1.056597167969" />
                  <Point X="3.136147460938" Y="-1.073489379883" />
                  <Point X="3.112397460938" Y="-1.093960083008" />
                  <Point X="3.021609130859" Y="-1.203150268555" />
                  <Point X="3.002653320312" Y="-1.225948242188" />
                  <Point X="2.987932617188" Y="-1.250330688477" />
                  <Point X="2.976589355469" Y="-1.277715942383" />
                  <Point X="2.969757568359" Y="-1.305365478516" />
                  <Point X="2.956745361328" Y="-1.446771484375" />
                  <Point X="2.954028564453" Y="-1.476295776367" />
                  <Point X="2.956347167969" Y="-1.507564086914" />
                  <Point X="2.964078613281" Y="-1.539185058594" />
                  <Point X="2.976450195312" Y="-1.567996582031" />
                  <Point X="3.059574707031" Y="-1.697291381836" />
                  <Point X="3.076930419922" Y="-1.724287109375" />
                  <Point X="3.086932861328" Y="-1.737239013672" />
                  <Point X="3.110628417969" Y="-1.760909057617" />
                  <Point X="3.319019287109" Y="-1.920813232422" />
                  <Point X="4.213122070312" Y="-2.606882568359" />
                  <Point X="4.150078613281" Y="-2.708896484375" />
                  <Point X="4.124806640625" Y="-2.749790527344" />
                  <Point X="4.028981445312" Y="-2.8859453125" />
                  <Point X="3.911828613281" Y="-2.818306884766" />
                  <Point X="2.800954589844" Y="-2.176943115234" />
                  <Point X="2.786131347656" Y="-2.170012207031" />
                  <Point X="2.754224365234" Y="-2.159825195312" />
                  <Point X="2.575458984375" Y="-2.127540283203" />
                  <Point X="2.538134033203" Y="-2.120799560547" />
                  <Point X="2.506781494141" Y="-2.120395263672" />
                  <Point X="2.474609130859" Y="-2.125353027344" />
                  <Point X="2.444833496094" Y="-2.135176757812" />
                  <Point X="2.296323242188" Y="-2.213336669922" />
                  <Point X="2.265315429688" Y="-2.229655761719" />
                  <Point X="2.242384765625" Y="-2.246548828125" />
                  <Point X="2.221424804688" Y="-2.267508789062" />
                  <Point X="2.204531738281" Y="-2.290439453125" />
                  <Point X="2.126371826172" Y="-2.438949707031" />
                  <Point X="2.110052734375" Y="-2.469957519531" />
                  <Point X="2.100229003906" Y="-2.499733154297" />
                  <Point X="2.095271240234" Y="-2.531905517578" />
                  <Point X="2.095675537109" Y="-2.563258056641" />
                  <Point X="2.127960449219" Y="-2.7420234375" />
                  <Point X="2.134701171875" Y="-2.779348388672" />
                  <Point X="2.138985351562" Y="-2.795141357422" />
                  <Point X="2.151819091797" Y="-2.826078613281" />
                  <Point X="2.285731201172" Y="-3.058020996094" />
                  <Point X="2.861283203125" Y="-4.054906494141" />
                  <Point X="2.811837646484" Y="-4.090224121094" />
                  <Point X="2.781848632813" Y="-4.11164453125" />
                  <Point X="2.701764892578" Y="-4.163481445312" />
                  <Point X="2.606245605469" Y="-4.038998291016" />
                  <Point X="1.758546142578" Y="-2.934255126953" />
                  <Point X="1.747503540039" Y="-2.922178955078" />
                  <Point X="1.721924194336" Y="-2.900557373047" />
                  <Point X="1.54561328125" Y="-2.787205566406" />
                  <Point X="1.50880078125" Y="-2.763538574219" />
                  <Point X="1.479989746094" Y="-2.751167236328" />
                  <Point X="1.448368774414" Y="-2.743435546875" />
                  <Point X="1.417099731445" Y="-2.741116699219" />
                  <Point X="1.22427355957" Y="-2.758860839844" />
                  <Point X="1.184012817383" Y="-2.762565673828" />
                  <Point X="1.15636315918" Y="-2.769397460938" />
                  <Point X="1.128978027344" Y="-2.780740722656" />
                  <Point X="1.104595947266" Y="-2.795461181641" />
                  <Point X="0.955700439453" Y="-2.919262695313" />
                  <Point X="0.924612121582" Y="-2.945111572266" />
                  <Point X="0.904141052246" Y="-2.968861816406" />
                  <Point X="0.887248779297" Y="-2.996688476562" />
                  <Point X="0.875624328613" Y="-3.025808837891" />
                  <Point X="0.831105285645" Y="-3.230631103516" />
                  <Point X="0.821810058594" Y="-3.273396728516" />
                  <Point X="0.819724487305" Y="-3.289627441406" />
                  <Point X="0.819742248535" Y="-3.323120117188" />
                  <Point X="0.857691833496" Y="-3.611376220703" />
                  <Point X="1.022065307617" Y="-4.859915039062" />
                  <Point X="1.00404510498" Y="-4.863865234375" />
                  <Point X="0.975677612305" Y="-4.870083496094" />
                  <Point X="0.929315612793" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.01374206543" Y="-4.761310058594" />
                  <Point X="-1.058435302734" Y="-4.752635253906" />
                  <Point X="-1.141246337891" Y="-4.731328613281" />
                  <Point X="-1.120775634766" Y="-4.575838378906" />
                  <Point X="-1.120077514648" Y="-4.568099609375" />
                  <Point X="-1.119451782227" Y="-4.541033691406" />
                  <Point X="-1.121759277344" Y="-4.509328125" />
                  <Point X="-1.123334106445" Y="-4.497690429688" />
                  <Point X="-1.173406494141" Y="-4.245958496094" />
                  <Point X="-1.183861450195" Y="-4.193398925781" />
                  <Point X="-1.188125" Y="-4.17846875" />
                  <Point X="-1.19902734375" Y="-4.149502441406" />
                  <Point X="-1.205666015625" Y="-4.135466308594" />
                  <Point X="-1.221738037109" Y="-4.10762890625" />
                  <Point X="-1.230573730469" Y="-4.094862304688" />
                  <Point X="-1.250207763672" Y="-4.0709375" />
                  <Point X="-1.261006591797" Y="-4.059779296875" />
                  <Point X="-1.453976928711" Y="-3.890548583984" />
                  <Point X="-1.494267578125" Y="-3.855214599609" />
                  <Point X="-1.506738647461" Y="-3.845965576172" />
                  <Point X="-1.533021606445" Y="-3.829621582031" />
                  <Point X="-1.546833618164" Y="-3.822526855469" />
                  <Point X="-1.576530761719" Y="-3.810225830078" />
                  <Point X="-1.591313476562" Y="-3.805476074219" />
                  <Point X="-1.621454833984" Y="-3.798447998047" />
                  <Point X="-1.636813964844" Y="-3.796169677734" />
                  <Point X="-1.892928588867" Y="-3.779383056641" />
                  <Point X="-1.946403198242" Y="-3.775878173828" />
                  <Point X="-1.961928222656" Y="-3.776132324219" />
                  <Point X="-1.992729614258" Y="-3.779166015625" />
                  <Point X="-2.008006103516" Y="-3.781945556641" />
                  <Point X="-2.03905480957" Y="-3.790265136719" />
                  <Point X="-2.053674316406" Y="-3.795496337891" />
                  <Point X="-2.081865478516" Y="-3.808269775391" />
                  <Point X="-2.095437011719" Y="-3.815812011719" />
                  <Point X="-2.308845458984" Y="-3.958407226562" />
                  <Point X="-2.353403564453" Y="-3.988180175781" />
                  <Point X="-2.359681640625" Y="-3.992757080078" />
                  <Point X="-2.380442382812" Y="-4.010131347656" />
                  <Point X="-2.402759277344" Y="-4.032771728516" />
                  <Point X="-2.410470947266" Y="-4.041629394531" />
                  <Point X="-2.449316650391" Y="-4.09225390625" />
                  <Point X="-2.503202636719" Y="-4.162479492188" />
                  <Point X="-2.6820859375" Y="-4.051719238281" />
                  <Point X="-2.747584960938" Y="-4.011163818359" />
                  <Point X="-2.980862792969" Y="-3.831547607422" />
                  <Point X="-2.957338867188" Y="-3.790803222656" />
                  <Point X="-2.341488769531" Y="-2.724119628906" />
                  <Point X="-2.334850097656" Y="-2.710083251953" />
                  <Point X="-2.323947998047" Y="-2.681116943359" />
                  <Point X="-2.319684570312" Y="-2.666187011719" />
                  <Point X="-2.313413574219" Y="-2.634660644531" />
                  <Point X="-2.311638916016" Y="-2.619235351562" />
                  <Point X="-2.310626220703" Y="-2.588301269531" />
                  <Point X="-2.314666259766" Y="-2.557615234375" />
                  <Point X="-2.323650878906" Y="-2.527997558594" />
                  <Point X="-2.329357421875" Y="-2.513557128906" />
                  <Point X="-2.343574462891" Y="-2.484728271484" />
                  <Point X="-2.351557128906" Y="-2.47141015625" />
                  <Point X="-2.369584716797" Y="-2.446253173828" />
                  <Point X="-2.379629638672" Y="-2.434414306641" />
                  <Point X="-2.393981689453" Y="-2.420062255859" />
                  <Point X="-2.408817138672" Y="-2.407020751953" />
                  <Point X="-2.433974121094" Y="-2.388993164062" />
                  <Point X="-2.447292236328" Y="-2.381010498047" />
                  <Point X="-2.47612109375" Y="-2.366793457031" />
                  <Point X="-2.490561523438" Y="-2.361086914062" />
                  <Point X="-2.520179199219" Y="-2.352102294922" />
                  <Point X="-2.550865234375" Y="-2.348062255859" />
                  <Point X="-2.581799316406" Y="-2.349074951172" />
                  <Point X="-2.597224609375" Y="-2.350849609375" />
                  <Point X="-2.628750976562" Y="-2.357120605469" />
                  <Point X="-2.643680908203" Y="-2.361384033203" />
                  <Point X="-2.672647216797" Y="-2.372286132812" />
                  <Point X="-2.68668359375" Y="-2.378924804688" />
                  <Point X="-2.909399414062" Y="-2.507509765625" />
                  <Point X="-3.793089111328" Y="-3.017708496094" />
                  <Point X="-3.95226953125" Y="-2.808578369141" />
                  <Point X="-4.004013671875" Y="-2.74059765625" />
                  <Point X="-4.181265136719" Y="-2.443373291016" />
                  <Point X="-4.12566015625" Y="-2.400706054688" />
                  <Point X="-3.048122314453" Y="-1.573882080078" />
                  <Point X="-3.036482177734" Y="-1.563309814453" />
                  <Point X="-3.015104736328" Y="-1.540389648438" />
                  <Point X="-3.005367431641" Y="-1.528041503906" />
                  <Point X="-2.987402587891" Y="-1.500910400391" />
                  <Point X="-2.9798359375" Y="-1.487125488281" />
                  <Point X="-2.967080078125" Y="-1.45849609375" />
                  <Point X="-2.961891113281" Y="-1.443651977539" />
                  <Point X="-2.955517333984" Y="-1.41904296875" />
                  <Point X="-2.951552734375" Y="-1.398803344727" />
                  <Point X="-2.948748535156" Y="-1.368375" />
                  <Point X="-2.948577880859" Y="-1.353047607422" />
                  <Point X="-2.950786621094" Y="-1.321377319336" />
                  <Point X="-2.953083251953" Y="-1.306221435547" />
                  <Point X="-2.960084228516" Y="-1.27647644043" />
                  <Point X="-2.971778320312" Y="-1.248244262695" />
                  <Point X="-2.987860595703" Y="-1.222260620117" />
                  <Point X="-2.996953125" Y="-1.209919921875" />
                  <Point X="-3.017785400391" Y="-1.185963500977" />
                  <Point X="-3.028744873047" Y="-1.175245239258" />
                  <Point X="-3.052245117188" Y="-1.1557109375" />
                  <Point X="-3.064785888672" Y="-1.146894897461" />
                  <Point X="-3.086694335938" Y="-1.134000610352" />
                  <Point X="-3.105433105469" Y="-1.124481689453" />
                  <Point X="-3.134695556641" Y="-1.11325769043" />
                  <Point X="-3.149793945312" Y="-1.108860473633" />
                  <Point X="-3.181682128906" Y="-1.102378540039" />
                  <Point X="-3.197298339844" Y="-1.100532348633" />
                  <Point X="-3.228622070312" Y="-1.09944140625" />
                  <Point X="-3.244329101562" Y="-1.100196533203" />
                  <Point X="-3.525486572266" Y="-1.137211669922" />
                  <Point X="-4.660920898438" Y="-1.286694335938" />
                  <Point X="-4.720521972656" Y="-1.053357910156" />
                  <Point X="-4.740762207031" Y="-0.974118225098" />
                  <Point X="-4.786452636719" Y="-0.65465423584" />
                  <Point X="-4.735766113281" Y="-0.641072875977" />
                  <Point X="-3.508288085938" Y="-0.312171417236" />
                  <Point X="-3.500475585938" Y="-0.309712615967" />
                  <Point X="-3.473930908203" Y="-0.299251037598" />
                  <Point X="-3.444166992188" Y="-0.283893249512" />
                  <Point X="-3.433561767578" Y="-0.277513763428" />
                  <Point X="-3.410602539062" Y="-0.261578857422" />
                  <Point X="-3.393671875" Y="-0.248242630005" />
                  <Point X="-3.371216796875" Y="-0.226358947754" />
                  <Point X="-3.3608984375" Y="-0.214484420776" />
                  <Point X="-3.341654541016" Y="-0.188229293823" />
                  <Point X="-3.3334375" Y="-0.174815338135" />
                  <Point X="-3.319328857422" Y="-0.14681388855" />
                  <Point X="-3.3134375" Y="-0.132226547241" />
                  <Point X="-3.305784423828" Y="-0.107568077087" />
                  <Point X="-3.300990722656" Y="-0.08850050354" />
                  <Point X="-3.296721435547" Y="-0.060341072083" />
                  <Point X="-3.295647949219" Y="-0.04610062027" />
                  <Point X="-3.295647949219" Y="-0.016459417343" />
                  <Point X="-3.296721435547" Y="-0.002218966246" />
                  <Point X="-3.300990722656" Y="0.025940465927" />
                  <Point X="-3.304186523438" Y="0.039859447479" />
                  <Point X="-3.311839599609" Y="0.064517921448" />
                  <Point X="-3.319328613281" Y="0.084253257751" />
                  <Point X="-3.333437011719" Y="0.112254699707" />
                  <Point X="-3.341654296875" Y="0.125669258118" />
                  <Point X="-3.360898193359" Y="0.151924377441" />
                  <Point X="-3.371217285156" Y="0.163799499512" />
                  <Point X="-3.393672607422" Y="0.185683029175" />
                  <Point X="-3.405808837891" Y="0.195691604614" />
                  <Point X="-3.428768066406" Y="0.211626495361" />
                  <Point X="-3.449709472656" Y="0.22424621582" />
                  <Point X="-3.470071289062" Y="0.234793838501" />
                  <Point X="-3.479425537109" Y="0.239015167236" />
                  <Point X="-3.498533935547" Y="0.246423873901" />
                  <Point X="-3.508288085938" Y="0.249611251831" />
                  <Point X="-3.764576660156" Y="0.318283508301" />
                  <Point X="-4.785446289062" Y="0.591824707031" />
                  <Point X="-4.744373535156" Y="0.869390441895" />
                  <Point X="-4.731330566406" Y="0.957532592773" />
                  <Point X="-4.633586425781" Y="1.318237182617" />
                  <Point X="-3.778066162109" Y="1.20560559082" />
                  <Point X="-3.76773828125" Y="1.204815429688" />
                  <Point X="-3.747058105469" Y="1.204364257812" />
                  <Point X="-3.736705810547" Y="1.204703125" />
                  <Point X="-3.715144287109" Y="1.20658984375" />
                  <Point X="-3.704891113281" Y="1.208053466797" />
                  <Point X="-3.684604248047" Y="1.212088745117" />
                  <Point X="-3.674570556641" Y="1.21466027832" />
                  <Point X="-3.623754638672" Y="1.230682495117" />
                  <Point X="-3.603450195312" Y="1.237676635742" />
                  <Point X="-3.584517578125" Y="1.246007324219" />
                  <Point X="-3.575279296875" Y="1.250689208984" />
                  <Point X="-3.556534912109" Y="1.261511108398" />
                  <Point X="-3.547861083984" Y="1.267170776367" />
                  <Point X="-3.531179443359" Y="1.279401977539" />
                  <Point X="-3.515927490234" Y="1.293377807617" />
                  <Point X="-3.5022890625" Y="1.3089296875" />
                  <Point X="-3.495894775391" Y="1.317077636719" />
                  <Point X="-3.48348046875" Y="1.334807373047" />
                  <Point X="-3.478010986328" Y="1.343603149414" />
                  <Point X="-3.468062255859" Y="1.361737548828" />
                  <Point X="-3.463583007813" Y="1.371076049805" />
                  <Point X="-3.443192871094" Y="1.420302124023" />
                  <Point X="-3.435499511719" Y="1.44035144043" />
                  <Point X="-3.429711425781" Y="1.460209228516" />
                  <Point X="-3.427359375" Y="1.470296264648" />
                  <Point X="-3.423600830078" Y="1.491611572266" />
                  <Point X="-3.422360839844" Y="1.501895629883" />
                  <Point X="-3.421008056641" Y="1.522536499023" />
                  <Point X="-3.421910400391" Y="1.543201171875" />
                  <Point X="-3.425057128906" Y="1.563645629883" />
                  <Point X="-3.427188720703" Y="1.573782348633" />
                  <Point X="-3.432790771484" Y="1.594688720703" />
                  <Point X="-3.436012695312" Y="1.604531982422" />
                  <Point X="-3.443509277344" Y="1.623809448242" />
                  <Point X="-3.447783935547" Y="1.633243774414" />
                  <Point X="-3.47238671875" Y="1.680505615234" />
                  <Point X="-3.482800292969" Y="1.699286987305" />
                  <Point X="-3.494292480469" Y="1.716486083984" />
                  <Point X="-3.5005078125" Y="1.724771728516" />
                  <Point X="-3.514420166016" Y="1.741351806641" />
                  <Point X="-3.521501708984" Y="1.748912475586" />
                  <Point X="-3.536443359375" Y="1.763215576172" />
                  <Point X="-3.544303710938" Y="1.769958618164" />
                  <Point X="-3.691311523438" Y="1.88276171875" />
                  <Point X="-4.227614257812" Y="2.294282226563" />
                  <Point X="-4.05296875" Y="2.593492675781" />
                  <Point X="-4.002293212891" Y="2.680312011719" />
                  <Point X="-3.726338867188" Y="3.035012695312" />
                  <Point X="-3.254159423828" Y="2.762399902344" />
                  <Point X="-3.244917480469" Y="2.757716064453" />
                  <Point X="-3.225984619141" Y="2.749385498047" />
                  <Point X="-3.216291259766" Y="2.745737304688" />
                  <Point X="-3.195648925781" Y="2.739228759766" />
                  <Point X="-3.185615478516" Y="2.736657226562" />
                  <Point X="-3.165328369141" Y="2.732621826172" />
                  <Point X="-3.155074707031" Y="2.731157958984" />
                  <Point X="-3.084302001953" Y="2.724966064453" />
                  <Point X="-3.059173339844" Y="2.723334472656" />
                  <Point X="-3.038492675781" Y="2.723785644531" />
                  <Point X="-3.0281640625" Y="2.724575683594" />
                  <Point X="-3.006705566406" Y="2.727400878906" />
                  <Point X="-2.996526123047" Y="2.729310546875" />
                  <Point X="-2.976435058594" Y="2.734226806641" />
                  <Point X="-2.956998291016" Y="2.741301269531" />
                  <Point X="-2.938447265625" Y="2.750449462891" />
                  <Point X="-2.929421386719" Y="2.755529785156" />
                  <Point X="-2.911167236328" Y="2.767158691406" />
                  <Point X="-2.902746582031" Y="2.773192871094" />
                  <Point X="-2.886614257812" Y="2.786140625" />
                  <Point X="-2.878902587891" Y="2.793054199219" />
                  <Point X="-2.828667724609" Y="2.8432890625" />
                  <Point X="-2.811265869141" Y="2.861489013672" />
                  <Point X="-2.798319335938" Y="2.877619628906" />
                  <Point X="-2.792285888672" Y="2.886038818359" />
                  <Point X="-2.78065625" Y="2.904293212891" />
                  <Point X="-2.775575195312" Y="2.913319335938" />
                  <Point X="-2.766426269531" Y="2.93187109375" />
                  <Point X="-2.7593515625" Y="2.951308349609" />
                  <Point X="-2.754434814453" Y="2.971400634766" />
                  <Point X="-2.752524902344" Y="2.981581298828" />
                  <Point X="-2.749699707031" Y="3.003040039063" />
                  <Point X="-2.748909667969" Y="3.013368408203" />
                  <Point X="-2.748458496094" Y="3.034048828125" />
                  <Point X="-2.748797363281" Y="3.044400878906" />
                  <Point X="-2.754989257812" Y="3.115173583984" />
                  <Point X="-2.757745605469" Y="3.140203369141" />
                  <Point X="-2.761780761719" Y="3.160490234375" />
                  <Point X="-2.764352294922" Y="3.170524169922" />
                  <Point X="-2.770860839844" Y="3.191166748047" />
                  <Point X="-2.774509521484" Y="3.200860839844" />
                  <Point X="-2.782840332031" Y="3.219793945312" />
                  <Point X="-2.787522460938" Y="3.229032958984" />
                  <Point X="-2.852666015625" Y="3.341864746094" />
                  <Point X="-3.059387207031" Y="3.699916259766" />
                  <Point X="-2.736627929688" Y="3.947372558594" />
                  <Point X="-2.648374023438" Y="4.015036376953" />
                  <Point X="-2.192525146484" Y="4.268296386719" />
                  <Point X="-2.118564453125" Y="4.171909179688" />
                  <Point X="-2.111820800781" Y="4.164048339844" />
                  <Point X="-2.097517822266" Y="4.149106933594" />
                  <Point X="-2.089959228516" Y="4.14202734375" />
                  <Point X="-2.073380126953" Y="4.128115234375" />
                  <Point X="-2.065094482422" Y="4.121899902344" />
                  <Point X="-2.047894775391" Y="4.110406738281" />
                  <Point X="-2.038979858398" Y="4.10512890625" />
                  <Point X="-1.960210205078" Y="4.064124023438" />
                  <Point X="-1.934330078125" Y="4.051287597656" />
                  <Point X="-1.915051879883" Y="4.043790527344" />
                  <Point X="-1.905207397461" Y="4.040568115234" />
                  <Point X="-1.884300292969" Y="4.034966064453" />
                  <Point X="-1.874163818359" Y="4.032834716797" />
                  <Point X="-1.853720336914" Y="4.029688232422" />
                  <Point X="-1.833056884766" Y="4.028785888672" />
                  <Point X="-1.812416992188" Y="4.030138427734" />
                  <Point X="-1.802133544922" Y="4.031378173828" />
                  <Point X="-1.780817626953" Y="4.035136474609" />
                  <Point X="-1.770729248047" Y="4.037488769531" />
                  <Point X="-1.750869628906" Y="4.04327734375" />
                  <Point X="-1.741098388672" Y="4.046713378906" />
                  <Point X="-1.65905456543" Y="4.080697265625" />
                  <Point X="-1.632585693359" Y="4.092272460938" />
                  <Point X="-1.614451538086" Y="4.102221191406" />
                  <Point X="-1.60565612793" Y="4.107690429687" />
                  <Point X="-1.587926757812" Y="4.120104492188" />
                  <Point X="-1.579778442383" Y="4.126499023438" />
                  <Point X="-1.56422644043" Y="4.140137695312" />
                  <Point X="-1.550251342773" Y="4.155389160156" />
                  <Point X="-1.538020019531" Y="4.172070800781" />
                  <Point X="-1.532359863281" Y="4.180745117188" />
                  <Point X="-1.521538085938" Y="4.199489257812" />
                  <Point X="-1.516856201172" Y="4.208728027344" />
                  <Point X="-1.508525878906" Y="4.22766015625" />
                  <Point X="-1.504877441406" Y="4.237353515625" />
                  <Point X="-1.478173461914" Y="4.322046875" />
                  <Point X="-1.470026611328" Y="4.349763671875" />
                  <Point X="-1.465990966797" Y="4.370051269531" />
                  <Point X="-1.46452722168" Y="4.380305175781" />
                  <Point X="-1.46264074707" Y="4.4018671875" />
                  <Point X="-1.462301757812" Y="4.412219238281" />
                  <Point X="-1.462752929688" Y="4.432898925781" />
                  <Point X="-1.46354309082" Y="4.4432265625" />
                  <Point X="-1.47094921875" Y="4.499481445312" />
                  <Point X="-1.479266235352" Y="4.562655761719" />
                  <Point X="-1.045425048828" Y="4.684290039062" />
                  <Point X="-0.931181091309" Y="4.716319824219" />
                  <Point X="-0.365222045898" Y="4.782556640625" />
                  <Point X="-0.22566633606" Y="4.261727539062" />
                  <Point X="-0.220435256958" Y="4.247107910156" />
                  <Point X="-0.207661849976" Y="4.218916503906" />
                  <Point X="-0.200119247437" Y="4.205344726562" />
                  <Point X="-0.182260864258" Y="4.178618164062" />
                  <Point X="-0.17260824585" Y="4.166456054688" />
                  <Point X="-0.151451339722" Y="4.1438671875" />
                  <Point X="-0.126897941589" Y="4.125026367188" />
                  <Point X="-0.099602897644" Y="4.110436523438" />
                  <Point X="-0.085356651306" Y="4.104260742188" />
                  <Point X="-0.054918682098" Y="4.093927978516" />
                  <Point X="-0.039857234955" Y="4.090155273438" />
                  <Point X="-0.009319986343" Y="4.08511328125" />
                  <Point X="0.021631772995" Y="4.08511328125" />
                  <Point X="0.052169025421" Y="4.090155273438" />
                  <Point X="0.06723046875" Y="4.093927978516" />
                  <Point X="0.097668441772" Y="4.104260742188" />
                  <Point X="0.111914535522" Y="4.110436523438" />
                  <Point X="0.139209732056" Y="4.125026367188" />
                  <Point X="0.163763122559" Y="4.143866699219" />
                  <Point X="0.184920181274" Y="4.166455566406" />
                  <Point X="0.194572952271" Y="4.178617675781" />
                  <Point X="0.212431182861" Y="4.205344238281" />
                  <Point X="0.2199737854" Y="4.218916503906" />
                  <Point X="0.232747192383" Y="4.247107910156" />
                  <Point X="0.237978271484" Y="4.261727539062" />
                  <Point X="0.271357025146" Y="4.386299316406" />
                  <Point X="0.378190216064" Y="4.785006347656" />
                  <Point X="0.728107177734" Y="4.748360351562" />
                  <Point X="0.827876281738" Y="4.737912109375" />
                  <Point X="1.353573120117" Y="4.610992675781" />
                  <Point X="1.453598999023" Y="4.586843261719" />
                  <Point X="1.794548461914" Y="4.463178710938" />
                  <Point X="1.858256713867" Y="4.440071289062" />
                  <Point X="2.189129882812" Y="4.285333007813" />
                  <Point X="2.2504453125" Y="4.256657226562" />
                  <Point X="2.570137695312" Y="4.070404052734" />
                  <Point X="2.629432861328" Y="4.035858398438" />
                  <Point X="2.817780029297" Y="3.901916503906" />
                  <Point X="2.778108886719" Y="3.833204345703" />
                  <Point X="2.06530859375" Y="2.598597900391" />
                  <Point X="2.062373046875" Y="2.593104980469" />
                  <Point X="2.053181396484" Y="2.573438476562" />
                  <Point X="2.044182250977" Y="2.549564208984" />
                  <Point X="2.041301635742" Y="2.540598876953" />
                  <Point X="2.023540283203" Y="2.474180419922" />
                  <Point X="2.019304321289" Y="2.454014404297" />
                  <Point X="2.01425793457" Y="2.421244384766" />
                  <Point X="2.013163818359" Y="2.408342041016" />
                  <Point X="2.012740478516" Y="2.382510009766" />
                  <Point X="2.013410888672" Y="2.369580322266" />
                  <Point X="2.020336425781" Y="2.312147216797" />
                  <Point X="2.023800537109" Y="2.289034912109" />
                  <Point X="2.029143554687" Y="2.267110839844" />
                  <Point X="2.032468505859" Y="2.256307373047" />
                  <Point X="2.040735473633" Y="2.234217529297" />
                  <Point X="2.045319335938" Y="2.223886962891" />
                  <Point X="2.055681640625" Y="2.203843017578" />
                  <Point X="2.061459960938" Y="2.194129638672" />
                  <Point X="2.096997558594" Y="2.141756103516" />
                  <Point X="2.109806640625" Y="2.124968505859" />
                  <Point X="2.130526611328" Y="2.100733886719" />
                  <Point X="2.139395507812" Y="2.0916640625" />
                  <Point X="2.158261230469" Y="2.074787841797" />
                  <Point X="2.168258056641" Y="2.066981445312" />
                  <Point X="2.220631347656" Y="2.031443725586" />
                  <Point X="2.241279541016" Y="2.018245483398" />
                  <Point X="2.26132421875" Y="2.00788269043" />
                  <Point X="2.271656005859" Y="2.003298339844" />
                  <Point X="2.293743896484" Y="1.995032470703" />
                  <Point X="2.304546875" Y="1.991707641602" />
                  <Point X="2.326470214844" Y="1.986364868164" />
                  <Point X="2.337590576172" Y="1.984346801758" />
                  <Point X="2.395023925781" Y="1.977421264648" />
                  <Point X="2.417103515625" Y="1.976055786133" />
                  <Point X="2.447998046875" Y="1.975947387695" />
                  <Point X="2.460543212891" Y="1.976734985352" />
                  <Point X="2.485418457031" Y="1.979959350586" />
                  <Point X="2.497748535156" Y="1.982395874023" />
                  <Point X="2.564167236328" Y="2.000157226562" />
                  <Point X="2.575539306641" Y="2.003582519531" />
                  <Point X="2.601489990234" Y="2.012292480469" />
                  <Point X="2.6103203125" Y="2.015755615234" />
                  <Point X="2.636033935547" Y="2.027872558594" />
                  <Point X="2.893810791016" Y="2.176699951172" />
                  <Point X="3.940404785156" Y="2.780951416016" />
                  <Point X="4.008778808594" Y="2.685927246094" />
                  <Point X="4.043950439453" Y="2.637046875" />
                  <Point X="4.136884765625" Y="2.483472167969" />
                  <Point X="4.111419433594" Y="2.463931884766" />
                  <Point X="3.172951416016" Y="1.743819702148" />
                  <Point X="3.168138427734" Y="1.739869262695" />
                  <Point X="3.152119384766" Y="1.725217041016" />
                  <Point X="3.134668701172" Y="1.706603515625" />
                  <Point X="3.128576416016" Y="1.699422485352" />
                  <Point X="3.080774902344" Y="1.637061645508" />
                  <Point X="3.069768066406" Y="1.621072875977" />
                  <Point X="3.051317871094" Y="1.591100219727" />
                  <Point X="3.045177246094" Y="1.579362915039" />
                  <Point X="3.034588623047" Y="1.555149047852" />
                  <Point X="3.030140380859" Y="1.542672241211" />
                  <Point X="3.012334228516" Y="1.479001708984" />
                  <Point X="3.006225585938" Y="1.454661987305" />
                  <Point X="3.002771972656" Y="1.432363647461" />
                  <Point X="3.001709472656" Y="1.421110717773" />
                  <Point X="3.000893310547" Y="1.397540405273" />
                  <Point X="3.001174804688" Y="1.386240722656" />
                  <Point X="3.003077880859" Y="1.363755859375" />
                  <Point X="3.004699462891" Y="1.352570678711" />
                  <Point X="3.01931640625" Y="1.281728881836" />
                  <Point X="3.024364746094" Y="1.262439208984" />
                  <Point X="3.034614990234" Y="1.230270507812" />
                  <Point X="3.039390625" Y="1.218202880859" />
                  <Point X="3.050542480469" Y="1.194830444336" />
                  <Point X="3.056918701172" Y="1.183525634766" />
                  <Point X="3.096675292969" Y="1.123097412109" />
                  <Point X="3.111739501953" Y="1.101423950195" />
                  <Point X="3.12629296875" Y="1.084179321289" />
                  <Point X="3.134083251953" Y="1.075990966797" />
                  <Point X="3.151326904297" Y="1.059901367188" />
                  <Point X="3.160034667969" Y="1.052695922852" />
                  <Point X="3.178244384766" Y="1.039370117188" />
                  <Point X="3.18774609375" Y="1.03324987793" />
                  <Point X="3.245358886719" Y="1.000818725586" />
                  <Point X="3.264159423828" Y="0.991561889648" />
                  <Point X="3.2941640625" Y="0.978792785645" />
                  <Point X="3.306311279297" Y="0.974569213867" />
                  <Point X="3.331064697266" Y="0.967801696777" />
                  <Point X="3.343671142578" Y="0.965257568359" />
                  <Point X="3.421567626953" Y="0.954962585449" />
                  <Point X="3.43255859375" Y="0.953833984375" />
                  <Point X="3.461900390625" Y="0.951681152344" />
                  <Point X="3.471590820312" Y="0.951465942383" />
                  <Point X="3.500603515625" Y="0.952797180176" />
                  <Point X="3.745474365234" Y="0.985035095215" />
                  <Point X="4.704703613281" Y="1.111320068359" />
                  <Point X="4.737577148438" Y="0.976284912109" />
                  <Point X="4.75268359375" Y="0.914233764648" />
                  <Point X="4.783871582031" Y="0.713921020508" />
                  <Point X="4.767496582031" Y="0.709533447266" />
                  <Point X="3.6919921875" Y="0.421352874756" />
                  <Point X="3.686032470703" Y="0.419544586182" />
                  <Point X="3.665625732422" Y="0.412137512207" />
                  <Point X="3.642381103516" Y="0.40161932373" />
                  <Point X="3.634004638672" Y="0.397316558838" />
                  <Point X="3.557473876953" Y="0.353080291748" />
                  <Point X="3.542053955078" Y="0.343144683838" />
                  <Point X="3.511762451172" Y="0.321487945557" />
                  <Point X="3.501356933594" Y="0.312867218018" />
                  <Point X="3.481874023438" Y="0.294236450195" />
                  <Point X="3.472796630859" Y="0.284226409912" />
                  <Point X="3.426878173828" Y="0.225715362549" />
                  <Point X="3.410854492188" Y="0.204208282471" />
                  <Point X="3.399130615234" Y="0.184928619385" />
                  <Point X="3.393843017578" Y="0.174939529419" />
                  <Point X="3.384069335938" Y="0.153476455688" />
                  <Point X="3.380005615234" Y="0.142929580688" />
                  <Point X="3.373159179688" Y="0.121428001404" />
                  <Point X="3.370376464844" Y="0.110473304749" />
                  <Point X="3.3550703125" Y="0.030550153732" />
                  <Point X="3.352444824219" Y="0.011851906776" />
                  <Point X="3.349248535156" Y="-0.023645336151" />
                  <Point X="3.348983154297" Y="-0.036885852814" />
                  <Point X="3.350296142578" Y="-0.063274593353" />
                  <Point X="3.351874511719" Y="-0.076422813416" />
                  <Point X="3.367180908203" Y="-0.156345962524" />
                  <Point X="3.373158935547" Y="-0.183987594604" />
                  <Point X="3.380005126953" Y="-0.205488723755" />
                  <Point X="3.384069091797" Y="-0.216035751343" />
                  <Point X="3.393843017578" Y="-0.237499420166" />
                  <Point X="3.399130859375" Y="-0.247489105225" />
                  <Point X="3.410854980469" Y="-0.266768920898" />
                  <Point X="3.417291259766" Y="-0.276059051514" />
                  <Point X="3.463209716797" Y="-0.334570098877" />
                  <Point X="3.476301025391" Y="-0.349563018799" />
                  <Point X="3.500200439453" Y="-0.37420010376" />
                  <Point X="3.509966796875" Y="-0.382966156006" />
                  <Point X="3.530614013672" Y="-0.399068267822" />
                  <Point X="3.541494873047" Y="-0.406404449463" />
                  <Point X="3.618025878906" Y="-0.450640563965" />
                  <Point X="3.62710546875" Y="-0.455564453125" />
                  <Point X="3.655165283203" Y="-0.469808319092" />
                  <Point X="3.664188232422" Y="-0.473813415527" />
                  <Point X="3.691991943359" Y="-0.48391293335" />
                  <Point X="3.916549560547" Y="-0.544083068848" />
                  <Point X="4.784876953125" Y="-0.776750488281" />
                  <Point X="4.770048339844" Y="-0.87510534668" />
                  <Point X="4.761612304688" Y="-0.931059143066" />
                  <Point X="4.727802246094" Y="-1.079219604492" />
                  <Point X="4.684064941406" Y="-1.073461425781" />
                  <Point X="3.436782470703" Y="-0.909253662109" />
                  <Point X="3.428624511719" Y="-0.908535827637" />
                  <Point X="3.40009765625" Y="-0.908042541504" />
                  <Point X="3.366720947266" Y="-0.910841003418" />
                  <Point X="3.354480957031" Y="-0.912676330566" />
                  <Point X="3.204277832031" Y="-0.945323547363" />
                  <Point X="3.172916748047" Y="-0.952140014648" />
                  <Point X="3.157873535156" Y="-0.956742736816" />
                  <Point X="3.128753173828" Y="-0.968367431641" />
                  <Point X="3.114676025391" Y="-0.975389221191" />
                  <Point X="3.086849609375" Y="-0.992281433105" />
                  <Point X="3.074124267578" Y="-1.001530212402" />
                  <Point X="3.050374267578" Y="-1.022000976562" />
                  <Point X="3.039349609375" Y="-1.033222900391" />
                  <Point X="2.948561279297" Y="-1.142413085938" />
                  <Point X="2.92960546875" Y="-1.16521105957" />
                  <Point X="2.921326171875" Y="-1.17684753418" />
                  <Point X="2.90660546875" Y="-1.201229858398" />
                  <Point X="2.9001640625" Y="-1.213975952148" />
                  <Point X="2.888820800781" Y="-1.241361206055" />
                  <Point X="2.884362792969" Y="-1.254928222656" />
                  <Point X="2.877531005859" Y="-1.282577758789" />
                  <Point X="2.875157226562" Y="-1.296660400391" />
                  <Point X="2.862145019531" Y="-1.43806640625" />
                  <Point X="2.859428222656" Y="-1.467590698242" />
                  <Point X="2.859288574219" Y="-1.483320922852" />
                  <Point X="2.861607177734" Y="-1.514589233398" />
                  <Point X="2.864065429688" Y="-1.530127319336" />
                  <Point X="2.871796875" Y="-1.561748291016" />
                  <Point X="2.876785888672" Y="-1.576668212891" />
                  <Point X="2.889157470703" Y="-1.605479736328" />
                  <Point X="2.896540039062" Y="-1.619371337891" />
                  <Point X="2.979664550781" Y="-1.748666137695" />
                  <Point X="2.997020263672" Y="-1.775661865234" />
                  <Point X="3.001741943359" Y="-1.782353271484" />
                  <Point X="3.019793945312" Y="-1.804450317383" />
                  <Point X="3.043489501953" Y="-1.828120361328" />
                  <Point X="3.052796142578" Y="-1.836277587891" />
                  <Point X="3.261187011719" Y="-1.996181762695" />
                  <Point X="4.087170410156" Y="-2.629981201172" />
                  <Point X="4.069265136719" Y="-2.658954589844" />
                  <Point X="4.045484863281" Y="-2.697435058594" />
                  <Point X="4.001274414063" Y="-2.760251953125" />
                  <Point X="3.959328613281" Y="-2.736034423828" />
                  <Point X="2.848454589844" Y="-2.094670654297" />
                  <Point X="2.841192626953" Y="-2.090885498047" />
                  <Point X="2.815025390625" Y="-2.079512695312" />
                  <Point X="2.783118408203" Y="-2.069325683594" />
                  <Point X="2.771108154297" Y="-2.066337646484" />
                  <Point X="2.592342773438" Y="-2.034052612305" />
                  <Point X="2.555017822266" Y="-2.027312011719" />
                  <Point X="2.539358886719" Y="-2.025807495117" />
                  <Point X="2.508006347656" Y="-2.025403198242" />
                  <Point X="2.492312744141" Y="-2.026503662109" />
                  <Point X="2.460140380859" Y="-2.031461425781" />
                  <Point X="2.444844482422" Y="-2.035136352539" />
                  <Point X="2.415068847656" Y="-2.044960083008" />
                  <Point X="2.400589111328" Y="-2.051108642578" />
                  <Point X="2.252078857422" Y="-2.129268554688" />
                  <Point X="2.221071044922" Y="-2.145587646484" />
                  <Point X="2.208968505859" Y="-2.153170410156" />
                  <Point X="2.186037841797" Y="-2.170063476562" />
                  <Point X="2.175209716797" Y="-2.179373779297" />
                  <Point X="2.154249755859" Y="-2.200333740234" />
                  <Point X="2.144939453125" Y="-2.211161865234" />
                  <Point X="2.128046386719" Y="-2.234092529297" />
                  <Point X="2.120463623047" Y="-2.246195068359" />
                  <Point X="2.042303833008" Y="-2.394705322266" />
                  <Point X="2.025984741211" Y="-2.425713134766" />
                  <Point X="2.01983605957" Y="-2.440192871094" />
                  <Point X="2.010012329102" Y="-2.469968505859" />
                  <Point X="2.006337280273" Y="-2.485264404297" />
                  <Point X="2.001379516602" Y="-2.517436767578" />
                  <Point X="2.000279174805" Y="-2.533130371094" />
                  <Point X="2.00068347168" Y="-2.564482910156" />
                  <Point X="2.002187866211" Y="-2.580141845703" />
                  <Point X="2.03447277832" Y="-2.758907226562" />
                  <Point X="2.041213500977" Y="-2.796232177734" />
                  <Point X="2.043014648438" Y="-2.804220214844" />
                  <Point X="2.051236083984" Y="-2.831542480469" />
                  <Point X="2.064069824219" Y="-2.862479736328" />
                  <Point X="2.069546630859" Y="-2.873578613281" />
                  <Point X="2.203458740234" Y="-3.105520996094" />
                  <Point X="2.735892822266" Y="-4.027724365234" />
                  <Point X="2.72375390625" Y="-4.036083496094" />
                  <Point X="2.681614257812" Y="-3.981166015625" />
                  <Point X="1.833914550781" Y="-2.876422851562" />
                  <Point X="1.828654418945" Y="-2.870147216797" />
                  <Point X="1.808830932617" Y="-2.849625732422" />
                  <Point X="1.783251586914" Y="-2.828004150391" />
                  <Point X="1.773299072266" Y="-2.820647216797" />
                  <Point X="1.59698815918" Y="-2.707295410156" />
                  <Point X="1.56017565918" Y="-2.683628417969" />
                  <Point X="1.546283935547" Y="-2.676245849609" />
                  <Point X="1.517473022461" Y="-2.663874511719" />
                  <Point X="1.502553588867" Y="-2.658885742188" />
                  <Point X="1.470932617188" Y="-2.651154052734" />
                  <Point X="1.45539453125" Y="-2.648695800781" />
                  <Point X="1.424125488281" Y="-2.646376953125" />
                  <Point X="1.40839440918" Y="-2.646516357422" />
                  <Point X="1.215568237305" Y="-2.664260498047" />
                  <Point X="1.175307739258" Y="-2.667965332031" />
                  <Point X="1.161225097656" Y="-2.670339111328" />
                  <Point X="1.133575439453" Y="-2.677170898438" />
                  <Point X="1.120008422852" Y="-2.68162890625" />
                  <Point X="1.092623168945" Y="-2.692972167969" />
                  <Point X="1.079877563477" Y="-2.699413330078" />
                  <Point X="1.055495361328" Y="-2.714133789062" />
                  <Point X="1.043859008789" Y="-2.722413085938" />
                  <Point X="0.894963562012" Y="-2.846214599609" />
                  <Point X="0.863875244141" Y="-2.872063476562" />
                  <Point X="0.852653320312" Y="-2.883087890625" />
                  <Point X="0.832182128906" Y="-2.906838134766" />
                  <Point X="0.822933044434" Y="-2.919564208984" />
                  <Point X="0.806040710449" Y="-2.947390869141" />
                  <Point X="0.799018737793" Y="-2.961468261719" />
                  <Point X="0.787394348145" Y="-2.990588623047" />
                  <Point X="0.782791931152" Y="-3.005631347656" />
                  <Point X="0.738272827148" Y="-3.210453613281" />
                  <Point X="0.728977661133" Y="-3.253219238281" />
                  <Point X="0.727584777832" Y="-3.261289306641" />
                  <Point X="0.724724487305" Y="-3.289677734375" />
                  <Point X="0.7247421875" Y="-3.323170410156" />
                  <Point X="0.725554992676" Y="-3.335520019531" />
                  <Point X="0.763504516602" Y="-3.623776123047" />
                  <Point X="0.833091369629" Y="-4.15233984375" />
                  <Point X="0.655065124512" Y="-3.487937011719" />
                  <Point X="0.652606445312" Y="-3.480125" />
                  <Point X="0.642145019531" Y="-3.453580322266" />
                  <Point X="0.62678692627" Y="-3.423815673828" />
                  <Point X="0.620407409668" Y="-3.413210205078" />
                  <Point X="0.484960388184" Y="-3.218056396484" />
                  <Point X="0.456679992676" Y="-3.177309814453" />
                  <Point X="0.446670837402" Y="-3.165172851562" />
                  <Point X="0.424786712646" Y="-3.142717285156" />
                  <Point X="0.412911743164" Y="-3.132398681641" />
                  <Point X="0.386656311035" Y="-3.113154785156" />
                  <Point X="0.373242340088" Y="-3.104937744141" />
                  <Point X="0.345241485596" Y="-3.090829589844" />
                  <Point X="0.330654602051" Y="-3.084938476562" />
                  <Point X="0.121057815552" Y="-3.019887207031" />
                  <Point X="0.077295654297" Y="-3.006305175781" />
                  <Point X="0.063376529694" Y="-3.003109130859" />
                  <Point X="0.035216945648" Y="-2.99883984375" />
                  <Point X="0.020976644516" Y="-2.997766601562" />
                  <Point X="-0.008664855957" Y="-2.997766601562" />
                  <Point X="-0.022905158997" Y="-2.998840087891" />
                  <Point X="-0.051064590454" Y="-3.003109375" />
                  <Point X="-0.064983573914" Y="-3.006305175781" />
                  <Point X="-0.274580505371" Y="-3.071356201172" />
                  <Point X="-0.318342681885" Y="-3.084938476562" />
                  <Point X="-0.332929412842" Y="-3.090829589844" />
                  <Point X="-0.360930847168" Y="-3.104937988281" />
                  <Point X="-0.374345275879" Y="-3.113155273438" />
                  <Point X="-0.400600524902" Y="-3.132399169922" />
                  <Point X="-0.412475219727" Y="-3.142717773438" />
                  <Point X="-0.434358886719" Y="-3.165173095703" />
                  <Point X="-0.444367889404" Y="-3.177309814453" />
                  <Point X="-0.579815246582" Y="-3.372463378906" />
                  <Point X="-0.60809564209" Y="-3.413210205078" />
                  <Point X="-0.612470336914" Y="-3.420132324219" />
                  <Point X="-0.625976745605" Y="-3.445265136719" />
                  <Point X="-0.638777832031" Y="-3.476215820312" />
                  <Point X="-0.642753173828" Y="-3.487936767578" />
                  <Point X="-0.711944702148" Y="-3.746163818359" />
                  <Point X="-0.985425109863" Y="-4.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.989244567236" Y="4.700040970617" />
                  <Point X="-1.469219422691" Y="4.486342396741" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.133049380959" Y="4.190786257153" />
                  <Point X="-2.815383247435" Y="3.886991646841" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.643492213276" Y="4.749989390154" />
                  <Point X="-1.46415070447" Y="4.384608689033" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.060739878841" Y="4.118990075258" />
                  <Point X="-3.047651172738" Y="3.679588857361" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.362190447473" Y="4.77124255906" />
                  <Point X="-1.495659198002" Y="4.266589757423" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.955571883675" Y="4.061823437025" />
                  <Point X="-2.999889519606" Y="3.596863268935" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.337296152407" Y="4.678335766866" />
                  <Point X="-1.58376035525" Y="4.12337414855" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.782684852822" Y="4.03480725602" />
                  <Point X="-2.952127866474" Y="3.514137680508" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.312401857341" Y="4.585428974672" />
                  <Point X="-2.904366213343" Y="3.431412092082" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.287507562274" Y="4.492522182478" />
                  <Point X="-2.856604560211" Y="3.348686503656" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.654406158151" Y="2.993482347069" />
                  <Point X="-3.813892860941" Y="2.922474292061" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.560180768305" Y="4.765946896995" />
                  <Point X="0.347739275056" Y="4.671361850251" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.262613267208" Y="4.399615390284" />
                  <Point X="-2.808842866871" Y="3.265960933132" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.552711833972" Y="2.934769130868" />
                  <Point X="-3.937671658419" Y="2.763373974333" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.749269437792" Y="4.746144150259" />
                  <Point X="0.316100688402" Y="4.553284997451" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.237718972141" Y="4.30670859809" />
                  <Point X="-2.767464070389" Y="3.180393513831" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.451017509793" Y="2.876055914667" />
                  <Point X="-4.041488344237" Y="2.613161361335" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.916363196754" Y="4.716548638427" />
                  <Point X="0.284462101747" Y="4.435208144652" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.206407244797" Y="4.216659030832" />
                  <Point X="-2.752192118788" Y="3.083202578307" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.349323185614" Y="2.817342698467" />
                  <Point X="-4.123498761104" Y="2.472657524793" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.067807167719" Y="4.67998539206" />
                  <Point X="0.252823604737" Y="4.317131331765" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.145900599156" Y="4.139607878665" />
                  <Point X="-2.753040199169" Y="2.978834542136" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.247144931562" Y="2.758844941728" />
                  <Point X="-4.205509133768" Y="2.332153707933" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.219251138684" Y="4.643422145692" />
                  <Point X="0.202741691427" Y="4.190842980886" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.027856850809" Y="4.088173895093" />
                  <Point X="-2.832482829124" Y="2.839473957985" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.088789876965" Y="2.72535870804" />
                  <Point X="-4.16496914256" Y="2.246212828463" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.370695089389" Y="4.606858890305" />
                  <Point X="-4.079207888481" Y="2.180405752408" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.511850732719" Y="4.56571498535" />
                  <Point X="-3.993446634402" Y="2.114598676354" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.640562121678" Y="4.519030541383" />
                  <Point X="-3.907685380324" Y="2.0487916003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.769273510637" Y="4.472346097415" />
                  <Point X="-3.821924126245" Y="1.982984524246" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.893417119676" Y="4.423627946799" />
                  <Point X="-3.736162872166" Y="1.917177448192" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.007329925338" Y="4.370354749045" />
                  <Point X="-3.650401567437" Y="1.851370394689" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.121242731" Y="4.317081551292" />
                  <Point X="-3.564640207176" Y="1.78556336591" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.618679833509" Y="1.316274688814" />
                  <Point X="-4.636236399248" Y="1.308458002132" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.235155033653" Y="4.263808129585" />
                  <Point X="-3.492442494127" Y="1.713717412315" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.438416713827" Y="1.292542554142" />
                  <Point X="-4.668282119394" Y="1.190199881823" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.338039448251" Y="4.205624775777" />
                  <Point X="-3.446413960397" Y="1.630220189416" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.258153594144" Y="1.26881041947" />
                  <Point X="-4.70032783954" Y="1.071941761515" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.439214210167" Y="4.146680235553" />
                  <Point X="-3.421650756197" Y="1.537255031808" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.077890474461" Y="1.245078284798" />
                  <Point X="-4.73186672915" Y="0.9539092967" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.540388972082" Y="4.087735695329" />
                  <Point X="-3.441632683649" Y="1.424368058061" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.897627354778" Y="1.221346150126" />
                  <Point X="-4.748340154463" Y="0.842584408747" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.640215076819" Y="4.028190694242" />
                  <Point X="-3.528829955645" Y="1.281554884829" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.685054707984" Y="1.211999143732" />
                  <Point X="-4.764813430607" Y="0.731259587208" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.730143244686" Y="3.964238847736" />
                  <Point X="-4.781286706752" Y="0.61993476567" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.815720920638" Y="3.898350037433" />
                  <Point X="-4.676451948437" Y="0.562619760831" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.734908896004" Y="3.758379759491" />
                  <Point X="-4.530639173203" Y="0.523549344592" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.65409707998" Y="3.618409574427" />
                  <Point X="-4.384826397969" Y="0.484478928352" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.573285263957" Y="3.478439389363" />
                  <Point X="-4.239013622735" Y="0.445408512112" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.492473447933" Y="3.338469204299" />
                  <Point X="-4.093200847501" Y="0.406338095873" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.411661631909" Y="3.198499019236" />
                  <Point X="-3.947388072268" Y="0.367267679633" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.330849815886" Y="3.058528834172" />
                  <Point X="-3.801575297034" Y="0.328197263394" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.250037999862" Y="2.918558649108" />
                  <Point X="-3.655762485158" Y="0.289126863469" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.169226183838" Y="2.778588464045" />
                  <Point X="-3.509949660822" Y="0.25005646909" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.088414367814" Y="2.638618278981" />
                  <Point X="-3.403242444126" Y="0.193575136435" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.033121665956" Y="2.510009935567" />
                  <Point X="-3.337484436121" Y="0.118862041429" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.012978783724" Y="2.397051300135" />
                  <Point X="-3.302069577229" Y="0.030639306036" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.022557892638" Y="2.297325747745" />
                  <Point X="-3.298449550606" Y="-0.071739400728" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.337519288858" Y="-0.534363054233" />
                  <Point X="-4.775751387017" Y="-0.729476555157" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.053899816123" Y="2.207289624675" />
                  <Point X="-3.348984109163" Y="-0.198229282255" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.750929675922" Y="-0.377186978508" />
                  <Point X="-4.761768824363" Y="-0.827241563627" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.107971614096" Y="2.127373493741" />
                  <Point X="-4.747786261709" Y="-0.925006572098" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.18316952903" Y="2.056863316087" />
                  <Point X="-4.728893111449" Y="-1.020585246104" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.96531325112" Y="2.746334376046" />
                  <Point X="3.594458606577" Y="2.581219250215" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.285114649637" Y="1.99826176165" />
                  <Point X="-4.70504315758" Y="-1.113957008956" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.021983855338" Y="2.667575308199" />
                  <Point X="2.807377512998" Y="2.126797723232" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.474351012903" Y="1.978524772421" />
                  <Point X="-4.681193225349" Y="-1.207328781443" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.074307854542" Y="2.586881007117" />
                  <Point X="-4.611175200954" Y="-1.280145194951" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.123880545317" Y="2.5049617446" />
                  <Point X="-4.279547922951" Y="-1.236485664412" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.898724886089" Y="2.300725539994" />
                  <Point X="-3.947920644947" Y="-1.192826133872" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.575872121018" Y="2.052991781395" />
                  <Point X="-3.616293366944" Y="-1.149166603332" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.253019355946" Y="1.805258022795" />
                  <Point X="-3.284665875971" Y="-1.105506977972" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.068585971812" Y="1.619152543192" />
                  <Point X="-3.099960889297" Y="-1.127261466043" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.015884902806" Y="1.491698069066" />
                  <Point X="-3.012330565666" Y="-1.192236378718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.001589352722" Y="1.381342833639" />
                  <Point X="-2.961312149196" Y="-1.273511962684" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.018651961265" Y="1.28494914995" />
                  <Point X="-2.949088128466" Y="-1.372059924464" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.050413859828" Y="1.195100011832" />
                  <Point X="-2.98173536117" Y="-1.490585855417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.102721538344" Y="1.114398444311" />
                  <Point X="-3.20413549947" Y="-1.693595223064" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.174282856464" Y="1.042269149439" />
                  <Point X="-3.526988532932" Y="-1.941329101158" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.279395489028" Y="0.985077862186" />
                  <Point X="-3.849841566393" Y="-2.189062979252" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.441339860887" Y="0.953189695504" />
                  <Point X="-4.172694524818" Y="-2.436796823937" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.746834292181" Y="0.98521413306" />
                  <Point X="-4.133562006748" Y="-2.523364350822" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.078461713961" Y="1.028873727614" />
                  <Point X="-4.08455799052" Y="-2.60553680356" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.410089135742" Y="1.072533322167" />
                  <Point X="-4.035553974291" Y="-2.687709256298" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.70725281521" Y="1.10084867004" />
                  <Point X="-3.982945396892" Y="-2.768276855004" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.730093075033" Y="1.007027362435" />
                  <Point X="-3.923826927942" Y="-2.845946063255" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.752849421468" Y="0.913168694183" />
                  <Point X="-2.573672192454" Y="-2.348808891868" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.839573512787" Y="-2.467195787142" />
                  <Point X="-3.864708586608" Y="-2.923615328323" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.767990761644" Y="0.815919606705" />
                  <Point X="4.168633418354" Y="0.549068524722" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.419188401964" Y="0.215394105364" />
                  <Point X="-2.432589596471" Y="-2.389985319597" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.626653626827" Y="-2.921616878007" />
                  <Point X="-3.805590245273" Y="-3.001284593391" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.78313210182" Y="0.718670519227" />
                  <Point X="4.755223924621" Y="0.706244998184" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.366021283112" Y="0.087732132478" />
                  <Point X="-2.358898014946" Y="-2.461166160094" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.349246903391" Y="-0.02372674901" />
                  <Point X="-2.317913934535" Y="-2.546909318312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.360719832539" Y="-0.122609118307" />
                  <Point X="-2.316520321442" Y="-2.650279288245" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.384132718468" Y="-0.216175476344" />
                  <Point X="-2.373557520757" Y="-2.779664331968" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.434356862789" Y="-0.297804693055" />
                  <Point X="-2.454369354203" Y="-2.919634524789" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.499034922262" Y="-0.372998612126" />
                  <Point X="-2.535181187649" Y="-3.059604717609" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.592001113154" Y="-0.435597843635" />
                  <Point X="-2.615993021095" Y="-3.19957491043" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.707635395041" Y="-0.488104590792" />
                  <Point X="-2.69680485454" Y="-3.33954510325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.853448077425" Y="-0.527175048371" />
                  <Point X="-2.777616687986" Y="-3.479515296071" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.999260840438" Y="-0.566245470051" />
                  <Point X="-2.858428521432" Y="-3.619485488891" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.145073664966" Y="-0.605315864344" />
                  <Point X="3.45656775963" Y="-0.911858443404" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.954337926981" Y="-1.135465571517" />
                  <Point X="-2.939240354878" Y="-3.759455681712" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.290886489493" Y="-0.644386258637" />
                  <Point X="3.636830979118" Y="-0.93559053364" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.88000884976" Y="-1.272549455306" />
                  <Point X="-2.939363280929" Y="-3.863500858374" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.436699314021" Y="-0.683456652929" />
                  <Point X="3.817094198606" Y="-0.959322623875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.867285420722" Y="-1.382204737347" />
                  <Point X="-2.853788212136" Y="-3.929390829458" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.582512138548" Y="-0.722527047222" />
                  <Point X="3.997357418094" Y="-0.983054714111" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.85975046897" Y="-1.489549960468" />
                  <Point X="-2.768213143344" Y="-3.995280800543" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.728324963075" Y="-0.761597441514" />
                  <Point X="4.177620637582" Y="-1.006786804347" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.880133863674" Y="-1.5844651349" />
                  <Point X="-2.673437247891" Y="-4.057074299669" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.774588734463" Y="-0.84498992986" />
                  <Point X="4.35788385707" Y="-1.030518894583" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.927419659043" Y="-1.667402588852" />
                  <Point X="-0.113912288454" Y="-3.021490813423" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.432619731192" Y="-3.163388509151" />
                  <Point X="-1.826016463276" Y="-3.78376870429" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.413215975361" Y="-4.045206771069" />
                  <Point X="-2.575739028476" Y="-4.11756669634" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.755592686515" Y="-0.957437961772" />
                  <Point X="4.538147076558" Y="-1.054250984819" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.979397625974" Y="-1.748250953428" />
                  <Point X="0.122352987601" Y="-3.020289181639" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.540090859144" Y="-3.315228184616" />
                  <Point X="-1.624429321877" Y="-3.798006772808" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.72917847545" Y="-1.073188772696" />
                  <Point X="4.71841021924" Y="-1.077983109252" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.040432688593" Y="-1.825066839199" />
                  <Point X="2.577189416854" Y="-2.031316032053" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.131121613394" Y="-2.229918213746" />
                  <Point X="0.259980662599" Y="-3.063003839296" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.632157637462" Y="-3.460209401745" />
                  <Point X="-1.50367817084" Y="-3.84823534304" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.124772261708" Y="-1.891506888399" />
                  <Point X="2.743353991031" Y="-2.061325243605" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.056911761313" Y="-2.366949015083" />
                  <Point X="1.428566616125" Y="-2.646706297995" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.826384720035" Y="-2.914814951908" />
                  <Point X="0.384236073836" Y="-3.111672212366" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.667391029695" Y="-3.579886765106" />
                  <Point X="-1.424065726866" Y="-3.916780045734" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.210533560153" Y="-1.9573139447" />
                  <Point X="2.871779172082" Y="-2.108137115544" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.004981855945" Y="-2.494060145037" />
                  <Point X="1.567961645785" Y="-2.688634078659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.774951125492" Y="-3.041705110045" />
                  <Point X="0.459913619056" Y="-3.181968844859" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.699029523929" Y="-3.697963576757" />
                  <Point X="-1.345416557479" Y="-3.985753625905" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.296294888521" Y="-2.023120987679" />
                  <Point X="2.97347343606" Y="-2.166850358548" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.005389403985" Y="-2.597869139417" />
                  <Point X="1.663529098362" Y="-2.750075153848" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.749926615288" Y="-3.156837186282" />
                  <Point X="0.515050581735" Y="-3.261410733911" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.730668079091" Y="-3.816040415535" />
                  <Point X="-1.266767388091" Y="-4.054727206076" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.38205626006" Y="-2.088928011437" />
                  <Point X="3.075167700038" Y="-2.225563601551" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.022772288158" Y="-2.694120227207" />
                  <Point X="1.75909649184" Y="-2.811516255349" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.726584169566" Y="-3.271220359161" />
                  <Point X="0.570187462178" Y="-3.340852659577" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.762306676281" Y="-3.934117273025" />
                  <Point X="-1.207481472651" Y="-4.132321862345" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.467817631599" Y="-2.154735035194" />
                  <Point X="3.176861964016" Y="-2.284276844555" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.040155063587" Y="-2.790371363414" />
                  <Point X="1.837216156875" Y="-2.880725586048" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.730548025765" Y="-3.373445983134" />
                  <Point X="0.624807344643" Y="-3.420524767574" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.79394527347" Y="-4.052194130515" />
                  <Point X="-1.177941439498" Y="-4.223160238679" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.553579003138" Y="-2.220542058952" />
                  <Point X="3.278556227994" Y="-2.342990087559" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.073081883404" Y="-2.879701845174" />
                  <Point X="1.896691833836" Y="-2.958235755044" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.743480543971" Y="-3.471678501514" />
                  <Point X="0.660596613551" Y="-3.508580804888" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.82558387066" Y="-4.170270988004" />
                  <Point X="-1.158939282804" Y="-4.318690379894" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.639340374676" Y="-2.286349082709" />
                  <Point X="3.380250491972" Y="-2.401703330563" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.120843558524" Y="-2.96242742381" />
                  <Point X="1.956167510797" Y="-3.035745924041" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.756413062176" Y="-3.569911019894" />
                  <Point X="0.685490918321" Y="-3.601487592762" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.857222467849" Y="-4.288347845494" />
                  <Point X="-1.139937226026" Y="-4.414220565594" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.725101746215" Y="-2.352156106467" />
                  <Point X="3.48194475595" Y="-2.460416573566" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.168605233644" Y="-3.045153002446" />
                  <Point X="2.015643187758" Y="-3.113256093038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.769345606113" Y="-3.668143526817" />
                  <Point X="0.710385223092" Y="-3.694394380635" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.888861065039" Y="-4.406424702984" />
                  <Point X="-1.121703618827" Y="-4.510092887091" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.810863117754" Y="-2.417963130224" />
                  <Point X="3.583639019928" Y="-2.51912981657" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.216366894731" Y="-3.12787858733" />
                  <Point X="2.075118864719" Y="-3.190766262035" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.782278181291" Y="-3.766376019831" />
                  <Point X="0.735279527862" Y="-3.787301168508" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.920499662228" Y="-4.524501560474" />
                  <Point X="-1.126066412318" Y="-4.616025774359" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.896624489293" Y="-2.483770153982" />
                  <Point X="3.685333283906" Y="-2.577843059574" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.26412851793" Y="-3.210604189083" />
                  <Point X="2.13459454168" Y="-3.268276431032" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.795210756468" Y="-3.864608512846" />
                  <Point X="0.760173832633" Y="-3.880207956382" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.952138259418" Y="-4.642578417963" />
                  <Point X="-1.140609481171" Y="-4.726491212243" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.982385860832" Y="-2.549577177739" />
                  <Point X="3.787027547884" Y="-2.636556302578" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.311890141129" Y="-3.293329790836" />
                  <Point X="2.194070218641" Y="-3.345786600029" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.808143331645" Y="-3.96284100586" />
                  <Point X="0.785068137404" Y="-3.973114744255" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.983776856607" Y="-4.760655275453" />
                  <Point X="-0.993898744125" Y="-4.765161830126" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.068147232371" Y="-2.615384201497" />
                  <Point X="3.888721811862" Y="-2.695269545581" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.359651764329" Y="-3.376055392589" />
                  <Point X="2.253545895602" Y="-3.423296769026" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.821075906822" Y="-4.061073498874" />
                  <Point X="0.809962442174" Y="-4.066021532129" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.01265535156" Y="-2.744081225094" />
                  <Point X="3.990416005832" Y="-2.753982819754" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.407413387528" Y="-3.458780994342" />
                  <Point X="2.313021572563" Y="-3.500806938023" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.455175010727" Y="-3.541506596095" />
                  <Point X="2.372497249524" Y="-3.57831710702" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.502936633926" Y="-3.624232197848" />
                  <Point X="2.431972926485" Y="-3.655827276017" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.550698257125" Y="-3.706957799601" />
                  <Point X="2.491448603446" Y="-3.733337445013" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.598459880324" Y="-3.789683401354" />
                  <Point X="2.550924280407" Y="-3.81084761401" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.646221503523" Y="-3.872409003107" />
                  <Point X="2.610399957367" Y="-3.888357783007" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.693983126722" Y="-3.95513460486" />
                  <Point X="2.669875634328" Y="-3.965867952004" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="0.001625976562" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978485351562" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.802928649902" Y="-4.773875" />
                  <Point X="0.471539031982" Y="-3.537112304688" />
                  <Point X="0.464318664551" Y="-3.521544189453" />
                  <Point X="0.328871429443" Y="-3.326390380859" />
                  <Point X="0.300591033936" Y="-3.285643798828" />
                  <Point X="0.274335601807" Y="-3.266399902344" />
                  <Point X="0.064738815308" Y="-3.201348632812" />
                  <Point X="0.020976625443" Y="-3.187766601562" />
                  <Point X="-0.008664908409" Y="-3.187766601562" />
                  <Point X="-0.218261703491" Y="-3.252817626953" />
                  <Point X="-0.262023895264" Y="-3.266399902344" />
                  <Point X="-0.288279144287" Y="-3.285643798828" />
                  <Point X="-0.423726409912" Y="-3.480797363281" />
                  <Point X="-0.45200680542" Y="-3.521544189453" />
                  <Point X="-0.459227142334" Y="-3.537112304688" />
                  <Point X="-0.528418762207" Y="-3.795339355469" />
                  <Point X="-0.84774395752" Y="-4.987077148438" />
                  <Point X="-1.049946411133" Y="-4.947828613281" />
                  <Point X="-1.100245605469" Y="-4.938065917969" />
                  <Point X="-1.343409057617" Y="-4.875501464844" />
                  <Point X="-1.349041748047" Y="-4.854044433594" />
                  <Point X="-1.309150146484" Y="-4.551038574219" />
                  <Point X="-1.309683227539" Y="-4.5347578125" />
                  <Point X="-1.359755737305" Y="-4.283025878906" />
                  <Point X="-1.370210693359" Y="-4.230466308594" />
                  <Point X="-1.386282714844" Y="-4.20262890625" />
                  <Point X="-1.579252807617" Y="-4.033397949219" />
                  <Point X="-1.619543457031" Y="-3.998063964844" />
                  <Point X="-1.649240722656" Y="-3.985762939453" />
                  <Point X="-1.905355102539" Y="-3.968976318359" />
                  <Point X="-1.958829833984" Y="-3.965471435547" />
                  <Point X="-1.989878540039" Y="-3.973791015625" />
                  <Point X="-2.203286865234" Y="-4.116386230469" />
                  <Point X="-2.247844970703" Y="-4.146159179688" />
                  <Point X="-2.259734130859" Y="-4.157294433594" />
                  <Point X="-2.298579833984" Y="-4.207918945312" />
                  <Point X="-2.457095214844" Y="-4.414500488281" />
                  <Point X="-2.782108398438" Y="-4.213260253906" />
                  <Point X="-2.855831054688" Y="-4.16761328125" />
                  <Point X="-3.192542480469" Y="-3.908356689453" />
                  <Point X="-3.228581054688" Y="-3.880608154297" />
                  <Point X="-3.121883789062" Y="-3.695803222656" />
                  <Point X="-2.506033691406" Y="-2.629119628906" />
                  <Point X="-2.499762695312" Y="-2.597593261719" />
                  <Point X="-2.513979736328" Y="-2.568764404297" />
                  <Point X="-2.528331787109" Y="-2.554412353516" />
                  <Point X="-2.531328369141" Y="-2.551415771484" />
                  <Point X="-2.560157226562" Y="-2.537198730469" />
                  <Point X="-2.59168359375" Y="-2.543469726562" />
                  <Point X="-2.814399414062" Y="-2.6720546875" />
                  <Point X="-3.842959228516" Y="-3.265894042969" />
                  <Point X="-4.103456054688" Y="-2.923654785156" />
                  <Point X="-4.161703613281" Y="-2.847129394531" />
                  <Point X="-4.40309765625" Y="-2.442348388672" />
                  <Point X="-4.431020019531" Y="-2.395527099609" />
                  <Point X="-4.241324707031" Y="-2.24996875" />
                  <Point X="-3.163786865234" Y="-1.423144897461" />
                  <Point X="-3.145822021484" Y="-1.396013671875" />
                  <Point X="-3.139448242188" Y="-1.371404541016" />
                  <Point X="-3.138117431641" Y="-1.366266357422" />
                  <Point X="-3.140326171875" Y="-1.334595947266" />
                  <Point X="-3.161158447266" Y="-1.310639526367" />
                  <Point X="-3.183066894531" Y="-1.297745239258" />
                  <Point X="-3.187641113281" Y="-1.295052978516" />
                  <Point X="-3.219529296875" Y="-1.288571044922" />
                  <Point X="-3.500686767578" Y="-1.325586181641" />
                  <Point X="-4.803283691406" Y="-1.497076293945" />
                  <Point X="-4.904611816406" Y="-1.100379882812" />
                  <Point X="-4.927393066406" Y="-1.011192016602" />
                  <Point X="-4.991259765625" Y="-0.564640258789" />
                  <Point X="-4.998396484375" Y="-0.51474206543" />
                  <Point X="-4.784941894531" Y="-0.457547027588" />
                  <Point X="-3.557463867188" Y="-0.128645431519" />
                  <Point X="-3.541895751953" Y="-0.121424926758" />
                  <Point X="-3.518936523438" Y="-0.105489982605" />
                  <Point X="-3.514142822266" Y="-0.102162895203" />
                  <Point X="-3.494898925781" Y="-0.075907775879" />
                  <Point X="-3.487245849609" Y="-0.051249267578" />
                  <Point X="-3.485647949219" Y="-0.046100738525" />
                  <Point X="-3.485647949219" Y="-0.016459356308" />
                  <Point X="-3.493301025391" Y="0.008199152946" />
                  <Point X="-3.494898925781" Y="0.013347681046" />
                  <Point X="-3.514142822266" Y="0.039602802277" />
                  <Point X="-3.537102050781" Y="0.055537746429" />
                  <Point X="-3.557463867188" Y="0.066085334778" />
                  <Point X="-3.813752441406" Y="0.134757629395" />
                  <Point X="-4.998186523438" Y="0.452125793457" />
                  <Point X="-4.932326660156" Y="0.897202575684" />
                  <Point X="-4.917645019531" Y="0.996418640137" />
                  <Point X="-4.789083496094" Y="1.470849487305" />
                  <Point X="-4.773516113281" Y="1.528298583984" />
                  <Point X="-4.63315234375" Y="1.509819458008" />
                  <Point X="-3.753266357422" Y="1.393980102539" />
                  <Point X="-3.731704833984" Y="1.395866577148" />
                  <Point X="-3.680888916016" Y="1.411888793945" />
                  <Point X="-3.670278808594" Y="1.415234130859" />
                  <Point X="-3.651534423828" Y="1.426056152344" />
                  <Point X="-3.639120117188" Y="1.443785888672" />
                  <Point X="-3.618729980469" Y="1.493012084961" />
                  <Point X="-3.61447265625" Y="1.503290161133" />
                  <Point X="-3.610714111328" Y="1.52460546875" />
                  <Point X="-3.616316162109" Y="1.545512084961" />
                  <Point X="-3.640918945312" Y="1.592773803711" />
                  <Point X="-3.646055908203" Y="1.602641723633" />
                  <Point X="-3.659968261719" Y="1.619221679688" />
                  <Point X="-3.806976074219" Y="1.732024658203" />
                  <Point X="-4.47610546875" Y="2.245466308594" />
                  <Point X="-4.217061523437" Y="2.689271484375" />
                  <Point X="-4.160014648438" Y="2.787006591797" />
                  <Point X="-3.819458984375" Y="3.224742675781" />
                  <Point X="-3.774671386719" Y="3.282310791016" />
                  <Point X="-3.69955859375" Y="3.238944335938" />
                  <Point X="-3.159156982422" Y="2.926943359375" />
                  <Point X="-3.138514648438" Y="2.920434814453" />
                  <Point X="-3.067741943359" Y="2.914242919922" />
                  <Point X="-3.052965332031" Y="2.912950195312" />
                  <Point X="-3.031506835938" Y="2.915775390625" />
                  <Point X="-3.013252685547" Y="2.927404296875" />
                  <Point X="-2.963017822266" Y="2.977639160156" />
                  <Point X="-2.952529052734" Y="2.988127685547" />
                  <Point X="-2.940899414062" Y="3.006382080078" />
                  <Point X="-2.93807421875" Y="3.027840820312" />
                  <Point X="-2.944266113281" Y="3.098613525391" />
                  <Point X="-2.945558837891" Y="3.113390380859" />
                  <Point X="-2.952067382812" Y="3.134032958984" />
                  <Point X="-3.0172109375" Y="3.246864746094" />
                  <Point X="-3.307278564453" Y="3.749276611328" />
                  <Point X="-2.852231933594" Y="4.09815625" />
                  <Point X="-2.752876220703" Y="4.174331542969" />
                  <Point X="-2.216511474609" Y="4.47232421875" />
                  <Point X="-2.141548583984" Y="4.513972167969" />
                  <Point X="-2.133260253906" Y="4.503170410156" />
                  <Point X="-1.967826904297" Y="4.287573242188" />
                  <Point X="-1.951247680664" Y="4.273661132813" />
                  <Point X="-1.872478027344" Y="4.23265625" />
                  <Point X="-1.856031616211" Y="4.224094238281" />
                  <Point X="-1.835124511719" Y="4.2184921875" />
                  <Point X="-1.81380859375" Y="4.222250488281" />
                  <Point X="-1.731764770508" Y="4.256234375" />
                  <Point X="-1.714634643555" Y="4.263330078125" />
                  <Point X="-1.696905273438" Y="4.275744140625" />
                  <Point X="-1.686083496094" Y="4.29448828125" />
                  <Point X="-1.659379516602" Y="4.379181640625" />
                  <Point X="-1.653804077148" Y="4.396864746094" />
                  <Point X="-1.651917602539" Y="4.418426757812" />
                  <Point X="-1.659323730469" Y="4.474681640625" />
                  <Point X="-1.689137573242" Y="4.701141113281" />
                  <Point X="-1.096716796875" Y="4.867235351562" />
                  <Point X="-0.968094177246" Y="4.903296386719" />
                  <Point X="-0.317856048584" Y="4.979396972656" />
                  <Point X="-0.22419960022" Y="4.990358398438" />
                  <Point X="-0.202006088257" Y="4.907530761719" />
                  <Point X="-0.042140380859" Y="4.310903320312" />
                  <Point X="-0.024282119751" Y="4.284176757813" />
                  <Point X="0.006155934334" Y="4.273844238281" />
                  <Point X="0.036594032288" Y="4.284176757813" />
                  <Point X="0.054452251434" Y="4.310903320312" />
                  <Point X="0.087831130981" Y="4.435475097656" />
                  <Point X="0.236648406982" Y="4.990868652344" />
                  <Point X="0.747897216797" Y="4.937327148438" />
                  <Point X="0.860205505371" Y="4.925565429688" />
                  <Point X="1.398163818359" Y="4.795686035156" />
                  <Point X="1.508459472656" Y="4.769057128906" />
                  <Point X="1.859332885742" Y="4.64179296875" />
                  <Point X="1.931033447266" Y="4.615786621094" />
                  <Point X="2.269619384766" Y="4.45744140625" />
                  <Point X="2.338695800781" Y="4.425136230469" />
                  <Point X="2.665783691406" Y="4.23457421875" />
                  <Point X="2.732523681641" Y="4.19569140625" />
                  <Point X="3.041010253906" Y="3.976312988281" />
                  <Point X="3.068740478516" Y="3.956592773438" />
                  <Point X="2.942653808594" Y="3.738204345703" />
                  <Point X="2.229853515625" Y="2.503597900391" />
                  <Point X="2.224852050781" Y="2.491514892578" />
                  <Point X="2.207090820312" Y="2.425096435547" />
                  <Point X="2.202044433594" Y="2.392326416016" />
                  <Point X="2.208969970703" Y="2.334893310547" />
                  <Point X="2.210415771484" Y="2.322901611328" />
                  <Point X="2.218682617188" Y="2.300811767578" />
                  <Point X="2.254220214844" Y="2.248438232422" />
                  <Point X="2.274940185547" Y="2.224203613281" />
                  <Point X="2.327313476562" Y="2.188666015625" />
                  <Point X="2.338248779297" Y="2.18124609375" />
                  <Point X="2.360336669922" Y="2.172980224609" />
                  <Point X="2.417770019531" Y="2.1660546875" />
                  <Point X="2.448664550781" Y="2.165946289062" />
                  <Point X="2.515083251953" Y="2.183707519531" />
                  <Point X="2.541033935547" Y="2.192417480469" />
                  <Point X="2.798810791016" Y="2.341244873047" />
                  <Point X="3.994248046875" Y="3.031430908203" />
                  <Point X="4.16300390625" Y="2.7968984375" />
                  <Point X="4.202591796875" Y="2.741880126953" />
                  <Point X="4.374568359375" Y="2.4576875" />
                  <Point X="4.387513183594" Y="2.436296142578" />
                  <Point X="4.227084472656" Y="2.313194824219" />
                  <Point X="3.288616210938" Y="1.593082763672" />
                  <Point X="3.279371337891" Y="1.583833374023" />
                  <Point X="3.231569824219" Y="1.52147253418" />
                  <Point X="3.213119628906" Y="1.4915" />
                  <Point X="3.195313476562" Y="1.427829589844" />
                  <Point X="3.191595703125" Y="1.414535522461" />
                  <Point X="3.190779541016" Y="1.390965087891" />
                  <Point X="3.205396484375" Y="1.320123413086" />
                  <Point X="3.215646728516" Y="1.287954833984" />
                  <Point X="3.255403320312" Y="1.227526489258" />
                  <Point X="3.263704345703" Y="1.214909423828" />
                  <Point X="3.280947998047" Y="1.198819824219" />
                  <Point X="3.338560791016" Y="1.166388793945" />
                  <Point X="3.368565429688" Y="1.153619628906" />
                  <Point X="3.446461914062" Y="1.143324584961" />
                  <Point X="3.475803710938" Y="1.14117175293" />
                  <Point X="3.720674560547" Y="1.173409667969" />
                  <Point X="4.848975097656" Y="1.32195324707" />
                  <Point X="4.922185546875" Y="1.021226928711" />
                  <Point X="4.939188476562" Y="0.951385192871" />
                  <Point X="4.993383300781" Y="0.603301818848" />
                  <Point X="4.997858886719" Y="0.574556213379" />
                  <Point X="4.816672363281" Y="0.526007446289" />
                  <Point X="3.741167724609" Y="0.237826919556" />
                  <Point X="3.729087158203" Y="0.232819412231" />
                  <Point X="3.652556396484" Y="0.188583221436" />
                  <Point X="3.622264892578" Y="0.166926574707" />
                  <Point X="3.576346435547" Y="0.108415504456" />
                  <Point X="3.566759033203" Y="0.096198890686" />
                  <Point X="3.556985351562" Y="0.074735733032" />
                  <Point X="3.541679199219" Y="-0.005187385559" />
                  <Point X="3.538482910156" Y="-0.040684635162" />
                  <Point X="3.553789306641" Y="-0.120607757568" />
                  <Point X="3.556985107422" Y="-0.137295227051" />
                  <Point X="3.566759033203" Y="-0.158758987427" />
                  <Point X="3.612677490234" Y="-0.21726991272" />
                  <Point X="3.636576904297" Y="-0.241906967163" />
                  <Point X="3.713107910156" Y="-0.286143157959" />
                  <Point X="3.741167724609" Y="-0.300387023926" />
                  <Point X="3.965725341797" Y="-0.360557067871" />
                  <Point X="4.998067871094" Y="-0.637172363281" />
                  <Point X="4.957925292969" Y="-0.903430480957" />
                  <Point X="4.948430664062" Y="-0.966406555176" />
                  <Point X="4.879" Y="-1.270661743164" />
                  <Point X="4.874546386719" Y="-1.290178466797" />
                  <Point X="4.659265136719" Y="-1.26183605957" />
                  <Point X="3.411982177734" Y="-1.097628295898" />
                  <Point X="3.3948359375" Y="-1.098341308594" />
                  <Point X="3.2446328125" Y="-1.130988525391" />
                  <Point X="3.213271728516" Y="-1.137805053711" />
                  <Point X="3.1854453125" Y="-1.154697265625" />
                  <Point X="3.094656982422" Y="-1.263887451172" />
                  <Point X="3.075701171875" Y="-1.286685424805" />
                  <Point X="3.064357910156" Y="-1.314070678711" />
                  <Point X="3.051345703125" Y="-1.4554765625" />
                  <Point X="3.04862890625" Y="-1.485000854492" />
                  <Point X="3.056360351562" Y="-1.516621826172" />
                  <Point X="3.139484863281" Y="-1.645916625977" />
                  <Point X="3.156840576172" Y="-1.672912353516" />
                  <Point X="3.168460693359" Y="-1.685540649414" />
                  <Point X="3.3768515625" Y="-1.845444702148" />
                  <Point X="4.33907421875" Y="-2.583783935547" />
                  <Point X="4.230892089844" Y="-2.758838378906" />
                  <Point X="4.204130859375" Y="-2.802142578125" />
                  <Point X="4.060542236328" Y="-3.006162597656" />
                  <Point X="4.056688476562" Y="-3.011638427734" />
                  <Point X="3.864328613281" Y="-2.900579345703" />
                  <Point X="2.753454589844" Y="-2.259215576172" />
                  <Point X="2.737340576172" Y="-2.253312744141" />
                  <Point X="2.558575195312" Y="-2.221027832031" />
                  <Point X="2.521250244141" Y="-2.214287109375" />
                  <Point X="2.489077880859" Y="-2.219244873047" />
                  <Point X="2.340567626953" Y="-2.297404785156" />
                  <Point X="2.309559814453" Y="-2.313723876953" />
                  <Point X="2.288599853516" Y="-2.334683837891" />
                  <Point X="2.210439941406" Y="-2.483194091797" />
                  <Point X="2.194120849609" Y="-2.514201904297" />
                  <Point X="2.189163085938" Y="-2.546374267578" />
                  <Point X="2.221447998047" Y="-2.725139648438" />
                  <Point X="2.228188720703" Y="-2.762464599609" />
                  <Point X="2.234091552734" Y="-2.778578613281" />
                  <Point X="2.368003662109" Y="-3.010520996094" />
                  <Point X="2.986673583984" Y="-4.082088623047" />
                  <Point X="2.8670546875" Y="-4.167529296875" />
                  <Point X="2.835296630859" Y="-4.190213378906" />
                  <Point X="2.679775878906" Y="-4.290879394531" />
                  <Point X="2.530876953125" Y="-4.096830566406" />
                  <Point X="1.683177612305" Y="-2.992087402344" />
                  <Point X="1.670549316406" Y="-2.980467529297" />
                  <Point X="1.49423840332" Y="-2.867115722656" />
                  <Point X="1.45742590332" Y="-2.843448730469" />
                  <Point X="1.425804931641" Y="-2.835717041016" />
                  <Point X="1.232978759766" Y="-2.853461181641" />
                  <Point X="1.192718017578" Y="-2.857166015625" />
                  <Point X="1.165332885742" Y="-2.868509277344" />
                  <Point X="1.01643737793" Y="-2.992310791016" />
                  <Point X="0.985349060059" Y="-3.018159667969" />
                  <Point X="0.968456726074" Y="-3.045986328125" />
                  <Point X="0.923937744141" Y="-3.25080859375" />
                  <Point X="0.91464251709" Y="-3.29357421875" />
                  <Point X="0.91392956543" Y="-3.310720214844" />
                  <Point X="0.951879089355" Y="-3.598976318359" />
                  <Point X="1.127642211914" Y="-4.934028320312" />
                  <Point X="1.024386230469" Y="-4.956662109375" />
                  <Point X="0.994345703125" Y="-4.963247070312" />
                  <Point X="0.860200439453" Y="-4.987616699219" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#200" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.159865750374" Y="4.951751606804" Z="2.1" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="2.1" />
                  <Point X="-0.329824227631" Y="5.060338377271" Z="2.1" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="2.1" />
                  <Point X="-1.116370524828" Y="4.946658738665" Z="2.1" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="2.1" />
                  <Point X="-1.715051979358" Y="4.499435428002" Z="2.1" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="2.1" />
                  <Point X="-1.713221434693" Y="4.425497245507" Z="2.1" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="2.1" />
                  <Point X="-1.757055289229" Y="4.33370850278" Z="2.1" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="2.1" />
                  <Point X="-1.855545577404" Y="4.308286160276" Z="2.1" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="2.1" />
                  <Point X="-2.099748472908" Y="4.564888257088" Z="2.1" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="2.1" />
                  <Point X="-2.246950282568" Y="4.547311601988" Z="2.1" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="2.1" />
                  <Point X="-2.88881003195" Y="4.169116134428" Z="2.1" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="2.1" />
                  <Point X="-3.066668240966" Y="3.253144849134" Z="2.1" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="2.1" />
                  <Point X="-3.000231871464" Y="3.12553611259" Z="2.1" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="2.1" />
                  <Point X="-3.004528688808" Y="3.044274879078" Z="2.1" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="2.1" />
                  <Point X="-3.069540353017" Y="2.995332827709" Z="2.1" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="2.1" />
                  <Point X="-3.680714499163" Y="3.31352570778" Z="2.1" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="2.1" />
                  <Point X="-3.865078253709" Y="3.286725188062" Z="2.1" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="2.1" />
                  <Point X="-4.266399410583" Y="2.745756538975" Z="2.1" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="2.1" />
                  <Point X="-3.843569969582" Y="1.723637068881" Z="2.1" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="2.1" />
                  <Point X="-3.691425374286" Y="1.600966196766" Z="2.1" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="2.1" />
                  <Point X="-3.671079750644" Y="1.543426307468" Z="2.1" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="2.1" />
                  <Point X="-3.702079946514" Y="1.49085478136" Z="2.1" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="2.1" />
                  <Point X="-4.632782069545" Y="1.590671759161" Z="2.1" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="2.1" />
                  <Point X="-4.843499358245" Y="1.515207096669" Z="2.1" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="2.1" />
                  <Point X="-4.987943859195" Y="0.935801743036" Z="2.1" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="2.1" />
                  <Point X="-3.832849219806" Y="0.117741325474" Z="2.1" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="2.1" />
                  <Point X="-3.571767027618" Y="0.04574192491" Z="2.1" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="2.1" />
                  <Point X="-3.547209929271" Y="0.024658446061" Z="2.1" />
                  <Point X="-3.539556741714" Y="0" Z="2.1" />
                  <Point X="-3.541154671864" Y="-0.005148505" Z="2.1" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="2.1" />
                  <Point X="-3.553601567506" Y="-0.033134058224" Z="2.1" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="2.1" />
                  <Point X="-4.804038568967" Y="-0.377970731282" Z="2.1" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="2.1" />
                  <Point X="-5.046912050183" Y="-0.540439309911" Z="2.1" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="2.1" />
                  <Point X="-4.959212401833" Y="-1.081474035436" Z="2.1" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="2.1" />
                  <Point X="-3.500316472582" Y="-1.343878399096" Z="2.1" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="2.1" />
                  <Point X="-3.214584595577" Y="-1.309555537337" Z="2.1" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="2.1" />
                  <Point X="-3.194007090596" Y="-1.32758801648" Z="2.1" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="2.1" />
                  <Point X="-4.277918525623" Y="-2.179020911138" Z="2.1" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="2.1" />
                  <Point X="-4.452196936742" Y="-2.436678025997" Z="2.1" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="2.1" />
                  <Point X="-4.149398110196" Y="-2.922657939097" Z="2.1" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="2.1" />
                  <Point X="-2.795555306281" Y="-2.684076000389" Z="2.1" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="2.1" />
                  <Point X="-2.569842916196" Y="-2.558487552935" Z="2.1" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="2.1" />
                  <Point X="-3.171341012779" Y="-3.639523076211" Z="2.1" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="2.1" />
                  <Point X="-3.229202334464" Y="-3.916693783613" Z="2.1" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="2.1" />
                  <Point X="-2.814585978039" Y="-4.224491069983" Z="2.1" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="2.1" />
                  <Point X="-2.265068097814" Y="-4.20707702098" Z="2.1" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="2.1" />
                  <Point X="-2.181664190908" Y="-4.126679374084" Z="2.1" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="2.1" />
                  <Point X="-1.914781179008" Y="-3.987589296289" Z="2.1" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="2.1" />
                  <Point X="-1.618376087668" Y="-4.039710032219" Z="2.1" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="2.1" />
                  <Point X="-1.41495117461" Y="-4.26150030873" Z="2.1" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="2.1" />
                  <Point X="-1.404770001256" Y="-4.816237867206" Z="2.1" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="2.1" />
                  <Point X="-1.362023763675" Y="-4.89264447744" Z="2.1" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="2.1" />
                  <Point X="-1.065629325632" Y="-4.965632799513" Z="2.1" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="2.1" />
                  <Point X="-0.486278520712" Y="-3.777000188912" Z="2.1" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="2.1" />
                  <Point X="-0.388806367091" Y="-3.47802639547" Z="2.1" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="2.1" />
                  <Point X="-0.209596787669" Y="-3.269290476488" Z="2.1" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="2.1" />
                  <Point X="0.043762291691" Y="-3.217821568801" Z="2.1" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="2.1" />
                  <Point X="0.281639492341" Y="-3.323619360298" Z="2.1" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="2.1" />
                  <Point X="0.748476339689" Y="-4.755535840904" Z="2.1" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="2.1" />
                  <Point X="0.848818267056" Y="-5.008104098836" Z="2.1" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="2.1" />
                  <Point X="1.028935925896" Y="-4.974222466691" Z="2.1" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="2.1" />
                  <Point X="0.995295440787" Y="-3.561169923798" Z="2.1" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="2.1" />
                  <Point X="0.966641026412" Y="-3.230148286745" Z="2.1" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="2.1" />
                  <Point X="1.042248306775" Y="-2.999477119276" Z="2.1" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="2.1" />
                  <Point X="1.231404429422" Y="-2.871970615598" Z="2.1" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="2.1" />
                  <Point X="1.461042913217" Y="-2.877893597631" Z="2.1" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="2.1" />
                  <Point X="2.485053195189" Y="-4.09598827818" Z="2.1" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="2.1" />
                  <Point X="2.695767889446" Y="-4.304823524569" Z="2.1" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="2.1" />
                  <Point X="2.889960520187" Y="-4.176936482978" Z="2.1" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="2.1" />
                  <Point X="2.405148977849" Y="-2.95424105368" Z="2.1" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="2.1" />
                  <Point X="2.26449603794" Y="-2.684973680299" Z="2.1" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="2.1" />
                  <Point X="2.24853032776" Y="-2.475200339007" Z="2.1" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="2.1" />
                  <Point X="2.357698039365" Y="-2.310370982207" Z="2.1" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="2.1" />
                  <Point X="2.543533123214" Y="-2.238951971551" Z="2.1" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="2.1" />
                  <Point X="3.833172543427" Y="-2.912600522426" Z="2.1" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="2.1" />
                  <Point X="4.095274599029" Y="-3.00365993222" Z="2.1" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="2.1" />
                  <Point X="4.267270053721" Y="-2.753843219999" Z="2.1" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="2.1" />
                  <Point X="3.401133638641" Y="-1.774496946131" Z="2.1" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="2.1" />
                  <Point X="3.175387026611" Y="-1.587597225949" Z="2.1" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="2.1" />
                  <Point X="3.094979341901" Y="-1.428777997715" Z="2.1" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="2.1" />
                  <Point X="3.126947367706" Y="-1.264574155744" Z="2.1" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="2.1" />
                  <Point X="3.24909683337" Y="-1.148567673675" Z="2.1" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="2.1" />
                  <Point X="4.646583042869" Y="-1.280128358289" Z="2.1" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="2.1" />
                  <Point X="4.921590478513" Y="-1.250505856123" Z="2.1" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="2.1" />
                  <Point X="5.001210792939" Y="-0.879604490234" Z="2.1" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="2.1" />
                  <Point X="3.972510315267" Y="-0.280980665871" Z="2.1" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="2.1" />
                  <Point X="3.731973604075" Y="-0.211574433211" Z="2.1" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="2.1" />
                  <Point X="3.64585525734" Y="-0.155121533692" Z="2.1" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="2.1" />
                  <Point X="3.596740904592" Y="-0.079923155111" Z="2.1" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="2.1" />
                  <Point X="3.584630545832" Y="0.016687376075" Z="2.1" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="2.1" />
                  <Point X="3.609524181061" Y="0.108827204883" Z="2.1" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="2.1" />
                  <Point X="3.671421810277" Y="0.176574411031" Z="2.1" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="2.1" />
                  <Point X="4.823457502039" Y="0.508991266898" Z="2.1" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="2.1" />
                  <Point X="5.036631934708" Y="0.642273621697" Z="2.1" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="2.1" />
                  <Point X="4.964610768197" Y="1.064334012829" Z="2.1" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="2.1" />
                  <Point X="3.707992163952" Y="1.254261938854" Z="2.1" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="2.1" />
                  <Point X="3.446857176896" Y="1.224173600226" Z="2.1" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="2.1" />
                  <Point X="3.356931460768" Y="1.241239953466" Z="2.1" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="2.1" />
                  <Point X="3.291017650405" Y="1.286288036579" Z="2.1" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="2.1" />
                  <Point X="3.248209065412" Y="1.361507615027" Z="2.1" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="2.1" />
                  <Point X="3.237309829841" Y="1.445643154462" Z="2.1" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="2.1" />
                  <Point X="3.265096604377" Y="1.522334205912" Z="2.1" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="2.1" />
                  <Point X="4.251366486379" Y="2.304807150941" Z="2.1" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="2.1" />
                  <Point X="4.411189563921" Y="2.514853837751" Z="2.1" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="2.1" />
                  <Point X="4.197433420696" Y="2.857381420894" Z="2.1" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="2.1" />
                  <Point X="2.767654791231" Y="2.415826305272" Z="2.1" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="2.1" />
                  <Point X="2.496010207451" Y="2.263290360813" Z="2.1" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="2.1" />
                  <Point X="2.417600014005" Y="2.246975144688" Z="2.1" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="2.1" />
                  <Point X="2.349231579087" Y="2.26132051302" Z="2.1" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="2.1" />
                  <Point X="2.289438087215" Y="2.307793281295" Z="2.1" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="2.1" />
                  <Point X="2.252454558071" Y="2.372158429828" Z="2.1" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="2.1" />
                  <Point X="2.249237581844" Y="2.443459368431" Z="2.1" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="2.1" />
                  <Point X="2.979799047587" Y="3.744484290724" Z="2.1" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="2.1" />
                  <Point X="3.063831221654" Y="4.048340044514" Z="2.1" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="2.1" />
                  <Point X="2.684797714503" Y="4.309056853089" Z="2.1" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="2.1" />
                  <Point X="2.284645027228" Y="4.534012769105" Z="2.1" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="2.1" />
                  <Point X="1.870225516819" Y="4.72007632665" Z="2.1" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="2.1" />
                  <Point X="1.403742345732" Y="4.875569545869" Z="2.1" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="2.1" />
                  <Point X="0.746949153218" Y="5.018335373451" Z="2.1" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="2.1" />
                  <Point X="0.033378811179" Y="4.479695899146" Z="2.1" />
                  <Point X="0" Y="4.355124473572" Z="2.1" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>