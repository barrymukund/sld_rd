<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#215" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="3553" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="0" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000475585938" Y="0.004715820312" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.563301818848" Y="-3.512523681641" />
                  <Point X="0.557721069336" Y="-3.497141845703" />
                  <Point X="0.542363037109" Y="-3.467377197266" />
                  <Point X="0.384354858398" Y="-3.239717529297" />
                  <Point X="0.378635406494" Y="-3.231476806641" />
                  <Point X="0.356751617432" Y="-3.209021240234" />
                  <Point X="0.330495849609" Y="-3.189777099609" />
                  <Point X="0.302494750977" Y="-3.175668945312" />
                  <Point X="0.057986362457" Y="-3.099782470703" />
                  <Point X="0.049135917664" Y="-3.097035644531" />
                  <Point X="0.020974849701" Y="-3.092766357422" />
                  <Point X="-0.008667236328" Y="-3.092766845703" />
                  <Point X="-0.036824962616" Y="-3.097036132812" />
                  <Point X="-0.281333496094" Y="-3.172922363281" />
                  <Point X="-0.290184112549" Y="-3.175669433594" />
                  <Point X="-0.318186096191" Y="-3.189778320312" />
                  <Point X="-0.34444052124" Y="-3.209021972656" />
                  <Point X="-0.366323547363" Y="-3.231476806641" />
                  <Point X="-0.524331726074" Y="-3.459136474609" />
                  <Point X="-0.529756286621" Y="-3.467939208984" />
                  <Point X="-0.542696105957" Y="-3.491748046875" />
                  <Point X="-0.550989929199" Y="-3.512524169922" />
                  <Point X="-0.564983642578" Y="-3.564748535156" />
                  <Point X="-0.916584472656" Y="-4.876941894531" />
                  <Point X="-1.073146484375" Y="-4.846552734375" />
                  <Point X="-1.079339111328" Y="-4.845351074219" />
                  <Point X="-1.24641784668" Y="-4.802362792969" />
                  <Point X="-1.214963134766" Y="-4.563438476562" />
                  <Point X="-1.214201293945" Y="-4.547930175781" />
                  <Point X="-1.216508544922" Y="-4.516224121094" />
                  <Point X="-1.27492175293" Y="-4.2225625" />
                  <Point X="-1.277035888672" Y="-4.211932617188" />
                  <Point X="-1.287938476562" Y="-4.182965820312" />
                  <Point X="-1.304010620117" Y="-4.155128417969" />
                  <Point X="-1.32364465332" Y="-4.131204101562" />
                  <Point X="-1.548757202148" Y="-3.933785400391" />
                  <Point X="-1.556905639648" Y="-3.926639404297" />
                  <Point X="-1.583188964844" Y="-3.910295166016" />
                  <Point X="-1.61288659668" Y="-3.897994140625" />
                  <Point X="-1.643027954102" Y="-3.890966308594" />
                  <Point X="-1.941802246094" Y="-3.871383544922" />
                  <Point X="-1.95261730957" Y="-3.870674804688" />
                  <Point X="-1.983418823242" Y="-3.873708740234" />
                  <Point X="-2.014467163086" Y="-3.882028320312" />
                  <Point X="-2.042657958984" Y="-3.894801513672" />
                  <Point X="-2.291612548828" Y="-4.061148193359" />
                  <Point X="-2.300624267578" Y="-4.067169677734" />
                  <Point X="-2.317625244141" Y="-4.081694580078" />
                  <Point X="-2.337370605469" Y="-4.103067871094" />
                  <Point X="-2.342958984375" Y="-4.109700683594" />
                  <Point X="-2.480148681641" Y="-4.288490234375" />
                  <Point X="-2.792639160156" Y="-4.095003417969" />
                  <Point X="-2.801710693359" Y="-4.089386962891" />
                  <Point X="-3.104721923828" Y="-3.856078125" />
                  <Point X="-2.423761230469" Y="-2.676619384766" />
                  <Point X="-2.412859130859" Y="-2.647653320312" />
                  <Point X="-2.406588134766" Y="-2.616127197266" />
                  <Point X="-2.405575439453" Y="-2.585193847656" />
                  <Point X="-2.414559570312" Y="-2.555576660156" />
                  <Point X="-2.428776123047" Y="-2.526747802734" />
                  <Point X="-2.446803710938" Y="-2.501590087891" />
                  <Point X="-2.463546142578" Y="-2.484847412109" />
                  <Point X="-2.464173583984" Y="-2.484219970703" />
                  <Point X="-2.489330566406" Y="-2.46619921875" />
                  <Point X="-2.518157226562" Y="-2.451988769531" />
                  <Point X="-2.547771484375" Y="-2.443009521484" />
                  <Point X="-2.578700683594" Y="-2.444024414062" />
                  <Point X="-2.610222167969" Y="-2.450296142578" />
                  <Point X="-2.63918359375" Y="-2.461197265625" />
                  <Point X="-2.684226074219" Y="-2.487202636719" />
                  <Point X="-3.818024658203" Y="-3.141801269531" />
                  <Point X="-4.075693847656" Y="-2.803276855469" />
                  <Point X="-4.082858398438" Y="-2.793863769531" />
                  <Point X="-4.306142578125" Y="-2.419450439453" />
                  <Point X="-3.105955078125" Y="-1.498513793945" />
                  <Point X="-3.084576904297" Y="-1.475592895508" />
                  <Point X="-3.066612060547" Y="-1.448461181641" />
                  <Point X="-3.053856445312" Y="-1.419832275391" />
                  <Point X="-3.046422607422" Y="-1.391130493164" />
                  <Point X="-3.046156982422" Y="-1.39010534668" />
                  <Point X="-3.043348388672" Y="-1.359673217773" />
                  <Point X="-3.045553710938" Y="-1.328001586914" />
                  <Point X="-3.052551757812" Y="-1.298254882812" />
                  <Point X="-3.068632080078" Y="-1.272268920898" />
                  <Point X="-3.089462890625" Y="-1.248309936523" />
                  <Point X="-3.11296875" Y="-1.228769042969" />
                  <Point X="-3.138526367188" Y="-1.213727050781" />
                  <Point X="-3.139451416016" Y="-1.213182495117" />
                  <Point X="-3.168710449219" Y="-1.201959472656" />
                  <Point X="-3.200602050781" Y="-1.195475585938" />
                  <Point X="-3.2319296875" Y="-1.194383789062" />
                  <Point X="-3.288791748047" Y="-1.201869995117" />
                  <Point X="-4.732102050781" Y="-1.391885253906" />
                  <Point X="-4.831273925781" Y="-1.003631530762" />
                  <Point X="-4.834076171875" Y="-0.992660766602" />
                  <Point X="-4.892423828125" Y="-0.584698181152" />
                  <Point X="-3.532876220703" Y="-0.220408493042" />
                  <Point X="-3.517492431641" Y="-0.214827163696" />
                  <Point X="-3.487728759766" Y="-0.199469314575" />
                  <Point X="-3.4609453125" Y="-0.180880310059" />
                  <Point X="-3.459975830078" Y="-0.180207260132" />
                  <Point X="-3.437520263672" Y="-0.158323486328" />
                  <Point X="-3.418276611328" Y="-0.132068756104" />
                  <Point X="-3.404168457031" Y="-0.104067832947" />
                  <Point X="-3.395240478516" Y="-0.075302177429" />
                  <Point X="-3.394909423828" Y="-0.074234786987" />
                  <Point X="-3.390645751953" Y="-0.046083591461" />
                  <Point X="-3.390648925781" Y="-0.016445905685" />
                  <Point X="-3.394918212891" Y="0.011703322411" />
                  <Point X="-3.403845947266" Y="0.040468971252" />
                  <Point X="-3.404169189453" Y="0.041510101318" />
                  <Point X="-3.418279541016" Y="0.069514068604" />
                  <Point X="-3.437522216797" Y="0.095765762329" />
                  <Point X="-3.459975341797" Y="0.117646957397" />
                  <Point X="-3.486758789063" Y="0.13623626709" />
                  <Point X="-3.500004882812" Y="0.143926727295" />
                  <Point X="-3.532876220703" Y="0.157848342896" />
                  <Point X="-3.584708740234" Y="0.171736709595" />
                  <Point X="-4.89181640625" Y="0.521975219727" />
                  <Point X="-4.826293457031" Y="0.9647734375" />
                  <Point X="-4.82448828125" Y="0.976973144531" />
                  <Point X="-4.703551269531" Y="1.423267944336" />
                  <Point X="-3.765666259766" Y="1.29979296875" />
                  <Point X="-3.744987548828" Y="1.299341674805" />
                  <Point X="-3.723426757812" Y="1.301227783203" />
                  <Point X="-3.703138427734" Y="1.305263183594" />
                  <Point X="-3.643858154297" Y="1.323954223633" />
                  <Point X="-3.641712402344" Y="1.324630737305" />
                  <Point X="-3.622781738281" Y="1.332960205078" />
                  <Point X="-3.604037597656" Y="1.343781494141" />
                  <Point X="-3.587356201172" Y="1.356012207031" />
                  <Point X="-3.573717285156" Y="1.371563598633" />
                  <Point X="-3.561302734375" Y="1.389292358398" />
                  <Point X="-3.551352294922" Y="1.407429321289" />
                  <Point X="-3.527565917969" Y="1.464854980469" />
                  <Point X="-3.526704833984" Y="1.46693359375" />
                  <Point X="-3.520917236328" Y="1.486788085938" />
                  <Point X="-3.517157958984" Y="1.508103149414" />
                  <Point X="-3.515804443359" Y="1.528743530273" />
                  <Point X="-3.518950195312" Y="1.54918762207" />
                  <Point X="-3.524551269531" Y="1.570094360352" />
                  <Point X="-3.532049072266" Y="1.589376220703" />
                  <Point X="-3.56075" Y="1.644510375977" />
                  <Point X="-3.561788818359" Y="1.646505859375" />
                  <Point X="-3.573279785156" Y="1.663703613281" />
                  <Point X="-3.587193115234" Y="1.680285400391" />
                  <Point X="-3.602135742188" Y="1.694590087891" />
                  <Point X="-3.631866943359" Y="1.717403930664" />
                  <Point X="-4.351860351563" Y="2.269874023438" />
                  <Point X="-4.088167724609" Y="2.721642822266" />
                  <Point X="-4.081154296875" Y="2.733658691406" />
                  <Point X="-3.750504882813" Y="3.158661865234" />
                  <Point X="-3.206656982422" Y="2.844670898438" />
                  <Point X="-3.187724609375" Y="2.836340332031" />
                  <Point X="-3.167082519531" Y="2.829831787109" />
                  <Point X="-3.146794677734" Y="2.825796386719" />
                  <Point X="-3.064233886719" Y="2.818573242188" />
                  <Point X="-3.061245361328" Y="2.818311767578" />
                  <Point X="-3.040566650391" Y="2.818762695312" />
                  <Point X="-3.019109130859" Y="2.821587402344" />
                  <Point X="-2.999018798828" Y="2.826503173828" />
                  <Point X="-2.980468261719" Y="2.835650390625" />
                  <Point X="-2.962214599609" Y="2.847278076172" />
                  <Point X="-2.946079345703" Y="2.860227539062" />
                  <Point X="-2.887477050781" Y="2.918829833984" />
                  <Point X="-2.885355712891" Y="2.920950927734" />
                  <Point X="-2.872410644531" Y="2.937079101562" />
                  <Point X="-2.860779785156" Y="2.955334228516" />
                  <Point X="-2.851630126953" Y="2.973886474609" />
                  <Point X="-2.846712890625" Y="2.993979492188" />
                  <Point X="-2.843887207031" Y="3.015439697266" />
                  <Point X="-2.843435791016" Y="3.03612109375" />
                  <Point X="-2.850658935547" Y="3.118682128906" />
                  <Point X="-2.850920410156" Y="3.121670654297" />
                  <Point X="-2.854955810547" Y="3.141958496094" />
                  <Point X="-2.861464355469" Y="3.162600585938" />
                  <Point X="-2.869795166016" Y="3.181533203125" />
                  <Point X="-2.882969970703" Y="3.204352539062" />
                  <Point X="-3.183333007812" Y="3.724596679688" />
                  <Point X="-2.7128359375" Y="4.085321533203" />
                  <Point X="-2.700622802734" Y="4.094685546875" />
                  <Point X="-2.167036865234" Y="4.391133789062" />
                  <Point X="-2.043195556641" Y="4.229740722656" />
                  <Point X="-2.028891845703" Y="4.214799316406" />
                  <Point X="-2.012311523438" Y="4.20088671875" />
                  <Point X="-1.995112792969" Y="4.18939453125" />
                  <Point X="-1.903223022461" Y="4.141559570313" />
                  <Point X="-1.899901489258" Y="4.139830078125" />
                  <Point X="-1.880629272461" Y="4.132334960938" />
                  <Point X="-1.859720458984" Y="4.126730957031" />
                  <Point X="-1.839274780273" Y="4.123583007812" />
                  <Point X="-1.818632446289" Y="4.124935058594" />
                  <Point X="-1.797314697266" Y="4.128693359375" />
                  <Point X="-1.777453613281" Y="4.134481933594" />
                  <Point X="-1.681744140625" Y="4.174126464844" />
                  <Point X="-1.678279418945" Y="4.175561523437" />
                  <Point X="-1.660144775391" Y="4.185510742187" />
                  <Point X="-1.642415771484" Y="4.197924804688" />
                  <Point X="-1.626865234375" Y="4.2115625" />
                  <Point X="-1.614634643555" Y="4.2282421875" />
                  <Point X="-1.60381262207" Y="4.246985351563" />
                  <Point X="-1.59548059082" Y="4.265920410156" />
                  <Point X="-1.564329101562" Y="4.364720703125" />
                  <Point X="-1.563201293945" Y="4.368296875" />
                  <Point X="-1.559165893555" Y="4.388583496094" />
                  <Point X="-1.557279296875" Y="4.410146484375" />
                  <Point X="-1.55773046875" Y="4.430826171875" />
                  <Point X="-1.559228149414" Y="4.442203613281" />
                  <Point X="-1.584201782227" Y="4.631897949219" />
                  <Point X="-0.965442565918" Y="4.805376464844" />
                  <Point X="-0.949634765625" Y="4.809808105469" />
                  <Point X="-0.294710845947" Y="4.88645703125" />
                  <Point X="-0.133903274536" Y="4.286315429688" />
                  <Point X="-0.121129814148" Y="4.258124023438" />
                  <Point X="-0.103271522522" Y="4.231397460938" />
                  <Point X="-0.082114517212" Y="4.20880859375" />
                  <Point X="-0.054819263458" Y="4.19421875" />
                  <Point X="-0.024381185532" Y="4.183886230469" />
                  <Point X="0.006156013012" Y="4.178844238281" />
                  <Point X="0.036693202972" Y="4.183886230469" />
                  <Point X="0.067131278992" Y="4.19421875" />
                  <Point X="0.094426460266" Y="4.20880859375" />
                  <Point X="0.115583534241" Y="4.231397460938" />
                  <Point X="0.133441680908" Y="4.258124023438" />
                  <Point X="0.146215301514" Y="4.286315917969" />
                  <Point X="0.152965866089" Y="4.311509765625" />
                  <Point X="0.307419281006" Y="4.8879375" />
                  <Point X="0.830230407715" Y="4.833185058594" />
                  <Point X="0.844040100098" Y="4.831738769531" />
                  <Point X="1.466010864258" Y="4.681576171875" />
                  <Point X="1.481029174805" Y="4.677950195312" />
                  <Point X="1.885819458008" Y="4.531129882812" />
                  <Point X="1.89464440918" Y="4.527929199219" />
                  <Point X="2.286109375" Y="4.344854003906" />
                  <Point X="2.294568847656" Y="4.340897460937" />
                  <Point X="2.672766845703" Y="4.120559082031" />
                  <Point X="2.680978515625" Y="4.115774902344" />
                  <Point X="2.943260009766" Y="3.929254638672" />
                  <Point X="2.147580810547" Y="2.551097412109" />
                  <Point X="2.142075683594" Y="2.539930664062" />
                  <Point X="2.133076904297" Y="2.516056640625" />
                  <Point X="2.112357177734" Y="2.438574951172" />
                  <Point X="2.109569091797" Y="2.423131347656" />
                  <Point X="2.107480957031" Y="2.401428466797" />
                  <Point X="2.107727539062" Y="2.38095703125" />
                  <Point X="2.115806640625" Y="2.313957519531" />
                  <Point X="2.116098876953" Y="2.311532226562" />
                  <Point X="2.121440917969" Y="2.289610839844" />
                  <Point X="2.129708984375" Y="2.267515625" />
                  <Point X="2.140072265625" Y="2.247468994141" />
                  <Point X="2.181529296875" Y="2.186371826172" />
                  <Point X="2.191413574219" Y="2.174126220703" />
                  <Point X="2.206215820312" Y="2.158615234375" />
                  <Point X="2.2216015625" Y="2.145590820312" />
                  <Point X="2.282698486328" Y="2.104133789063" />
                  <Point X="2.28491015625" Y="2.102633300781" />
                  <Point X="2.304956298828" Y="2.092270019531" />
                  <Point X="2.327040527344" Y="2.084005859375" />
                  <Point X="2.348962646484" Y="2.078663574219" />
                  <Point X="2.415962402344" Y="2.070584472656" />
                  <Point X="2.431985107422" Y="2.070014892578" />
                  <Point X="2.453313964844" Y="2.071060058594" />
                  <Point X="2.473206298828" Y="2.074171142578" />
                  <Point X="2.550687988281" Y="2.094890869141" />
                  <Point X="2.560380371094" Y="2.098048828125" />
                  <Point X="2.588533935547" Y="2.110145019531" />
                  <Point X="2.640667480469" Y="2.140244384766" />
                  <Point X="3.967326416016" Y="2.906191162109" />
                  <Point X="4.118400390625" Y="2.696233154297" />
                  <Point X="4.123268066406" Y="2.689468017578" />
                  <Point X="4.262198730469" Y="2.459883789063" />
                  <Point X="3.230784667969" Y="1.668452026367" />
                  <Point X="3.221424560547" Y="1.660241577148" />
                  <Point X="3.203974121094" Y="1.641628417969" />
                  <Point X="3.148210449219" Y="1.568880493164" />
                  <Point X="3.139860595703" Y="1.555934082031" />
                  <Point X="3.129372558594" Y="1.536349243164" />
                  <Point X="3.121630126953" Y="1.517087036133" />
                  <Point X="3.100858154297" Y="1.442811035156" />
                  <Point X="3.100106201172" Y="1.440122436523" />
                  <Point X="3.096652832031" Y="1.417824951172" />
                  <Point X="3.095836425781" Y="1.39425402832" />
                  <Point X="3.097739501953" Y="1.371768310547" />
                  <Point X="3.114791259766" Y="1.289126953125" />
                  <Point X="3.119136230469" Y="1.274292358398" />
                  <Point X="3.126952392578" Y="1.253921630859" />
                  <Point X="3.136283447266" Y="1.235739013672" />
                  <Point X="3.182662109375" Y="1.165245361328" />
                  <Point X="3.184341064453" Y="1.162693603516" />
                  <Point X="3.198896484375" Y="1.145447021484" />
                  <Point X="3.216140625" Y="1.129357910156" />
                  <Point X="3.234348388672" Y="1.116033935547" />
                  <Point X="3.301557617188" Y="1.078201049805" />
                  <Point X="3.315903076172" Y="1.071629638672" />
                  <Point X="3.336310546875" Y="1.064263061523" />
                  <Point X="3.356118652344" Y="1.059438598633" />
                  <Point X="3.446989990234" Y="1.047428588867" />
                  <Point X="3.456895751953" Y="1.046643676758" />
                  <Point X="3.488203613281" Y="1.04698449707" />
                  <Point X="3.537727050781" Y="1.053504394531" />
                  <Point X="4.776839355469" Y="1.216636474609" />
                  <Point X="4.84384375" Y="0.941401489258" />
                  <Point X="4.8459375" Y="0.932801208496" />
                  <Point X="4.890865234375" Y="0.644238525391" />
                  <Point X="3.716580078125" Y="0.329589935303" />
                  <Point X="3.704791503906" Y="0.325586608887" />
                  <Point X="3.681545898438" Y="0.315068145752" />
                  <Point X="3.592267578125" Y="0.263463653564" />
                  <Point X="3.579816650391" Y="0.254876113892" />
                  <Point X="3.562272460938" Y="0.240587478638" />
                  <Point X="3.547530517578" Y="0.225576309204" />
                  <Point X="3.493963623047" Y="0.157319351196" />
                  <Point X="3.492024658203" Y="0.15484866333" />
                  <Point X="3.480301269531" Y="0.135570068359" />
                  <Point X="3.470528076172" Y="0.114108573914" />
                  <Point X="3.463681152344" Y="0.09260609436" />
                  <Point X="3.445825439453" Y="-0.000629496276" />
                  <Point X="3.444170166016" Y="-0.015731579781" />
                  <Point X="3.443523681641" Y="-0.037919998169" />
                  <Point X="3.445178955078" Y="-0.058555599213" />
                  <Point X="3.463034667969" Y="-0.15179133606" />
                  <Point X="3.463681152344" Y="-0.155166244507" />
                  <Point X="3.470528076172" Y="-0.176668884277" />
                  <Point X="3.480301269531" Y="-0.198130218506" />
                  <Point X="3.492024902344" Y="-0.217408660889" />
                  <Point X="3.545591796875" Y="-0.285665618896" />
                  <Point X="3.556145019531" Y="-0.297057189941" />
                  <Point X="3.572396484375" Y="-0.311948730469" />
                  <Point X="3.589036132812" Y="-0.324155731201" />
                  <Point X="3.678314453125" Y="-0.375760223389" />
                  <Point X="3.686941650391" Y="-0.380175811768" />
                  <Point X="3.716579833984" Y="-0.392149932861" />
                  <Point X="3.761995117188" Y="-0.404318969727" />
                  <Point X="4.89147265625" Y="-0.706961486816" />
                  <Point X="4.85619140625" Y="-0.940976257324" />
                  <Point X="4.855022949219" Y="-0.948725891113" />
                  <Point X="4.801173828125" Y="-1.18469909668" />
                  <Point X="3.424382080078" Y="-1.003440856934" />
                  <Point X="3.408034912109" Y="-1.002710144043" />
                  <Point X="3.374658935547" Y="-1.005508728027" />
                  <Point X="3.199437255859" Y="-1.04359387207" />
                  <Point X="3.193094726563" Y="-1.044972412109" />
                  <Point X="3.163975341797" Y="-1.056596557617" />
                  <Point X="3.136148681641" Y="-1.073488525391" />
                  <Point X="3.112397705078" Y="-1.093959716797" />
                  <Point X="3.006487304688" Y="-1.221337036133" />
                  <Point X="3.002653564453" Y="-1.225947875977" />
                  <Point X="2.987933349609" Y="-1.250329101562" />
                  <Point X="2.97658984375" Y="-1.277713500977" />
                  <Point X="2.969757568359" Y="-1.305364624023" />
                  <Point X="2.954578125" Y="-1.470323852539" />
                  <Point X="2.954028564453" Y="-1.476294921875" />
                  <Point X="2.956346923828" Y="-1.507562011719" />
                  <Point X="2.964078125" Y="-1.53918359375" />
                  <Point X="2.976449951172" Y="-1.56799621582" />
                  <Point X="3.073420166016" Y="-1.718826904297" />
                  <Point X="3.080461669922" Y="-1.728404296875" />
                  <Point X="3.095592041016" Y="-1.746492675781" />
                  <Point X="3.110628417969" Y="-1.760909423828" />
                  <Point X="3.152774169922" Y="-1.793248901367" />
                  <Point X="4.213122558594" Y="-2.6068828125" />
                  <Point X="4.128103027344" Y="-2.744457275391" />
                  <Point X="4.124807617188" Y="-2.749789794922" />
                  <Point X="4.028981445312" Y="-2.885945068359" />
                  <Point X="2.800955566406" Y="-2.176943603516" />
                  <Point X="2.786131591797" Y="-2.170012451172" />
                  <Point X="2.754224609375" Y="-2.159825195312" />
                  <Point X="2.545682861328" Y="-2.122162841797" />
                  <Point X="2.538134277344" Y="-2.120799560547" />
                  <Point X="2.506781005859" Y="-2.120395507812" />
                  <Point X="2.474607421875" Y="-2.125353759766" />
                  <Point X="2.444832519531" Y="-2.135177246094" />
                  <Point X="2.271585449219" Y="-2.226355712891" />
                  <Point X="2.265314453125" Y="-2.22965625" />
                  <Point X="2.242382568359" Y="-2.246550292969" />
                  <Point X="2.221422851562" Y="-2.267510742188" />
                  <Point X="2.204531005859" Y="-2.290440429688" />
                  <Point X="2.113352294922" Y="-2.463687255859" />
                  <Point X="2.110052001953" Y="-2.469958496094" />
                  <Point X="2.100228027344" Y="-2.499735839844" />
                  <Point X="2.095270996094" Y="-2.531907470703" />
                  <Point X="2.095675537109" Y="-2.563258300781" />
                  <Point X="2.133337890625" Y="-2.771800048828" />
                  <Point X="2.136010742188" Y="-2.782803222656" />
                  <Point X="2.143276855469" Y="-2.806465576172" />
                  <Point X="2.151819091797" Y="-2.826078613281" />
                  <Point X="2.178901855469" Y="-2.872987304688" />
                  <Point X="2.861283447266" Y="-4.054906982422" />
                  <Point X="2.785762451172" Y="-4.108850097656" />
                  <Point X="2.781852539062" Y="-4.111643066406" />
                  <Point X="2.701765136719" Y="-4.163481445312" />
                  <Point X="1.758545288086" Y="-2.934254150391" />
                  <Point X="1.747504516602" Y="-2.922180175781" />
                  <Point X="1.721924804688" Y="-2.900557861328" />
                  <Point X="1.516246337891" Y="-2.768325683594" />
                  <Point X="1.508801391602" Y="-2.7635390625" />
                  <Point X="1.479991455078" Y="-2.75116796875" />
                  <Point X="1.448369506836" Y="-2.743435791016" />
                  <Point X="1.417099609375" Y="-2.741116699219" />
                  <Point X="1.192155395508" Y="-2.76181640625" />
                  <Point X="1.184012817383" Y="-2.762565673828" />
                  <Point X="1.156364257812" Y="-2.769396972656" />
                  <Point X="1.128979980469" Y="-2.780739501953" />
                  <Point X="1.104596679688" Y="-2.795460449219" />
                  <Point X="0.930900390625" Y="-2.939883300781" />
                  <Point X="0.924612915039" Y="-2.945110839844" />
                  <Point X="0.904142578125" Y="-2.968860351563" />
                  <Point X="0.887249267578" Y="-2.996687988281" />
                  <Point X="0.875624206543" Y="-3.025809082031" />
                  <Point X="0.823689880371" Y="-3.264747802734" />
                  <Point X="0.821998779297" Y="-3.275423583984" />
                  <Point X="0.819405761719" Y="-3.301218505859" />
                  <Point X="0.819742126465" Y="-3.323120361328" />
                  <Point X="0.827417419434" Y="-3.38141796875" />
                  <Point X="1.022065307617" Y="-4.859915527344" />
                  <Point X="0.979376281738" Y="-4.869272949219" />
                  <Point X="0.975682861328" Y="-4.870082519531" />
                  <Point X="0.929315490723" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.055044555664" Y="-4.753293457031" />
                  <Point X="-1.058435058594" Y="-4.752635742188" />
                  <Point X="-1.141246459961" Y="-4.731328613281" />
                  <Point X="-1.120776000977" Y="-4.575838378906" />
                  <Point X="-1.120077514648" Y="-4.568099609375" />
                  <Point X="-1.119451660156" Y="-4.54103515625" />
                  <Point X="-1.121759033203" Y="-4.509329101562" />
                  <Point X="-1.123334106445" Y="-4.497690429688" />
                  <Point X="-1.181747192383" Y="-4.204028808594" />
                  <Point X="-1.18812512207" Y="-4.178468261719" />
                  <Point X="-1.199027709961" Y="-4.149501464844" />
                  <Point X="-1.205666381836" Y="-4.135465332031" />
                  <Point X="-1.22173840332" Y="-4.107627929688" />
                  <Point X="-1.230574584961" Y="-4.094861328125" />
                  <Point X="-1.250208496094" Y="-4.070937011719" />
                  <Point X="-1.261006713867" Y="-4.059779296875" />
                  <Point X="-1.486119262695" Y="-3.862360595703" />
                  <Point X="-1.506738769531" Y="-3.845965576172" />
                  <Point X="-1.533022094727" Y="-3.829621337891" />
                  <Point X="-1.546834350586" Y="-3.822526367188" />
                  <Point X="-1.576531982422" Y="-3.810225341797" />
                  <Point X="-1.591314819336" Y="-3.805475830078" />
                  <Point X="-1.621456176758" Y="-3.798447998047" />
                  <Point X="-1.636814697266" Y="-3.796169677734" />
                  <Point X="-1.935588989258" Y="-3.776586914062" />
                  <Point X="-1.9619296875" Y="-3.776132324219" />
                  <Point X="-1.992731201172" Y="-3.779166259766" />
                  <Point X="-2.008007202148" Y="-3.781946044922" />
                  <Point X="-2.039055419922" Y="-3.790265625" />
                  <Point X="-2.053674560547" Y="-3.795496337891" />
                  <Point X="-2.081865478516" Y="-3.80826953125" />
                  <Point X="-2.095437255859" Y="-3.815812011719" />
                  <Point X="-2.344391845703" Y="-3.982158691406" />
                  <Point X="-2.362333251953" Y="-3.994940917969" />
                  <Point X="-2.379334228516" Y="-4.009465820312" />
                  <Point X="-2.387405273438" Y="-4.017229492188" />
                  <Point X="-2.407150634766" Y="-4.038602783203" />
                  <Point X="-2.418327636719" Y="-4.051868408203" />
                  <Point X="-2.503202392578" Y="-4.162479492188" />
                  <Point X="-2.742627929688" Y="-4.014232910156" />
                  <Point X="-2.747588134766" Y="-4.011161865234" />
                  <Point X="-2.980862792969" Y="-3.831547851562" />
                  <Point X="-2.341488769531" Y="-2.724119384766" />
                  <Point X="-2.334850097656" Y="-2.710083251953" />
                  <Point X="-2.323947998047" Y="-2.6811171875" />
                  <Point X="-2.319684570312" Y="-2.666187011719" />
                  <Point X="-2.313413574219" Y="-2.634660888672" />
                  <Point X="-2.311638916016" Y="-2.619235595703" />
                  <Point X="-2.310626220703" Y="-2.588302246094" />
                  <Point X="-2.314666015625" Y="-2.5576171875" />
                  <Point X="-2.323650146484" Y="-2.528" />
                  <Point X="-2.329356445312" Y="-2.513559814453" />
                  <Point X="-2.343572998047" Y="-2.484730957031" />
                  <Point X="-2.351555419922" Y="-2.471412841797" />
                  <Point X="-2.369583007812" Y="-2.446255126953" />
                  <Point X="-2.379628173828" Y="-2.434415527344" />
                  <Point X="-2.396370605469" Y="-2.417672851562" />
                  <Point X="-2.408851318359" Y="-2.406989990234" />
                  <Point X="-2.434008300781" Y="-2.388969238281" />
                  <Point X="-2.447325683594" Y="-2.380989990234" />
                  <Point X="-2.47615234375" Y="-2.366779541016" />
                  <Point X="-2.490591796875" Y="-2.361075927734" />
                  <Point X="-2.520206054688" Y="-2.352096679688" />
                  <Point X="-2.550886962891" Y="-2.348060546875" />
                  <Point X="-2.581816162109" Y="-2.349075439453" />
                  <Point X="-2.597239257812" Y="-2.350850830078" />
                  <Point X="-2.628760742188" Y="-2.357122558594" />
                  <Point X="-2.643688232422" Y="-2.361385986328" />
                  <Point X="-2.672649658203" Y="-2.372287109375" />
                  <Point X="-2.68668359375" Y="-2.378924804688" />
                  <Point X="-2.731726074219" Y="-2.404930175781" />
                  <Point X="-3.79308984375" Y="-3.017708496094" />
                  <Point X="-4.000100585938" Y="-2.745738769531" />
                  <Point X="-4.004012939453" Y="-2.740598388672" />
                  <Point X="-4.181265136719" Y="-2.443373779297" />
                  <Point X="-3.048122802734" Y="-1.573882324219" />
                  <Point X="-3.036482666016" Y="-1.563310424805" />
                  <Point X="-3.015104492188" Y="-1.540389404297" />
                  <Point X="-3.005366943359" Y="-1.528040649414" />
                  <Point X="-2.987402099609" Y="-1.500908935547" />
                  <Point X="-2.979835693359" Y="-1.487124389648" />
                  <Point X="-2.967080078125" Y="-1.458495361328" />
                  <Point X="-2.961891113281" Y="-1.443651489258" />
                  <Point X="-2.954457275391" Y="-1.414949829102" />
                  <Point X="-2.951559082031" Y="-1.39883581543" />
                  <Point X="-2.948750488281" Y="-1.368403686523" />
                  <Point X="-2.948577880859" Y="-1.35307421875" />
                  <Point X="-2.950783203125" Y="-1.32140246582" />
                  <Point X="-2.953078369141" Y="-1.306246459961" />
                  <Point X="-2.960076416016" Y="-1.276499633789" />
                  <Point X="-2.971767822266" Y="-1.248265136719" />
                  <Point X="-2.987848144531" Y="-1.222279174805" />
                  <Point X="-2.996939941406" Y="-1.209937255859" />
                  <Point X="-3.017770751953" Y="-1.185978271484" />
                  <Point X="-3.028732177734" Y="-1.175256591797" />
                  <Point X="-3.052238037109" Y="-1.155715576172" />
                  <Point X="-3.064782470703" Y="-1.146896606445" />
                  <Point X="-3.090340087891" Y="-1.131854614258" />
                  <Point X="-3.105428955078" Y="-1.124483764648" />
                  <Point X="-3.134687988281" Y="-1.113260742188" />
                  <Point X="-3.149783203125" Y="-1.108864013672" />
                  <Point X="-3.181674804688" Y="-1.102380126953" />
                  <Point X="-3.197293212891" Y="-1.100533081055" />
                  <Point X="-3.228620849609" Y="-1.09944140625" />
                  <Point X="-3.244330078125" Y="-1.100196533203" />
                  <Point X="-3.301192138672" Y="-1.107682739258" />
                  <Point X="-4.660920898438" Y="-1.286694335938" />
                  <Point X="-4.739229003906" Y="-0.980120483398" />
                  <Point X="-4.740760742188" Y="-0.974123474121" />
                  <Point X="-4.786451660156" Y="-0.654654418945" />
                  <Point X="-3.508288330078" Y="-0.312171417236" />
                  <Point X="-3.500476074219" Y="-0.309712615967" />
                  <Point X="-3.473930419922" Y="-0.299250762939" />
                  <Point X="-3.444166748047" Y="-0.283892944336" />
                  <Point X="-3.433562011719" Y="-0.277513916016" />
                  <Point X="-3.406778564453" Y="-0.25892489624" />
                  <Point X="-3.393672363281" Y="-0.24824307251" />
                  <Point X="-3.371216796875" Y="-0.226359237671" />
                  <Point X="-3.360898193359" Y="-0.214484420776" />
                  <Point X="-3.341654541016" Y="-0.188229736328" />
                  <Point X="-3.333437011719" Y="-0.17481489563" />
                  <Point X="-3.319328857422" Y="-0.146814041138" />
                  <Point X="-3.313437988281" Y="-0.132227737427" />
                  <Point X="-3.304510009766" Y="-0.10346207428" />
                  <Point X="-3.300980712891" Y="-0.088460823059" />
                  <Point X="-3.296717041016" Y="-0.060309715271" />
                  <Point X="-3.295645751953" Y="-0.046073421478" />
                  <Point X="-3.295648925781" Y="-0.016435787201" />
                  <Point X="-3.296723144531" Y="-0.002200536966" />
                  <Point X="-3.300992431641" Y="0.025948640823" />
                  <Point X="-3.3041875" Y="0.039862567902" />
                  <Point X="-3.313115234375" Y="0.068628234863" />
                  <Point X="-3.319330322266" Y="0.084257865906" />
                  <Point X="-3.333440673828" Y="0.112261833191" />
                  <Point X="-3.341659179688" Y="0.125677284241" />
                  <Point X="-3.360901855469" Y="0.151928985596" />
                  <Point X="-3.371219238281" Y="0.163801879883" />
                  <Point X="-3.393672363281" Y="0.185683029175" />
                  <Point X="-3.405808105469" Y="0.195691146851" />
                  <Point X="-3.432591552734" Y="0.214280456543" />
                  <Point X="-3.439059814453" Y="0.218393447876" />
                  <Point X="-3.462956298828" Y="0.231404785156" />
                  <Point X="-3.495827636719" Y="0.245326293945" />
                  <Point X="-3.508288574219" Y="0.24961138916" />
                  <Point X="-3.56012109375" Y="0.263499755859" />
                  <Point X="-4.785446289062" Y="0.591824584961" />
                  <Point X="-4.732316894531" Y="0.95086730957" />
                  <Point X="-4.731331054688" Y="0.957530761719" />
                  <Point X="-4.633586425781" Y="1.318237182617" />
                  <Point X="-3.778066162109" Y="1.205605712891" />
                  <Point X="-3.767739013672" Y="1.204815551758" />
                  <Point X="-3.747060302734" Y="1.204364257812" />
                  <Point X="-3.736708740234" Y="1.204703125" />
                  <Point X="-3.715147949219" Y="1.206589233398" />
                  <Point X="-3.704894042969" Y="1.208052978516" />
                  <Point X="-3.684605712891" Y="1.212088378906" />
                  <Point X="-3.674571289062" Y="1.21466003418" />
                  <Point X="-3.615291015625" Y="1.233351074219" />
                  <Point X="-3.603452392578" Y="1.23767578125" />
                  <Point X="-3.584521728516" Y="1.246005126953" />
                  <Point X="-3.575283935547" Y="1.250686523438" />
                  <Point X="-3.556539794922" Y="1.26150769043" />
                  <Point X="-3.547864990234" Y="1.26716784668" />
                  <Point X="-3.53118359375" Y="1.279398681641" />
                  <Point X="-3.515933105469" Y="1.293372436523" />
                  <Point X="-3.502294189453" Y="1.308923950195" />
                  <Point X="-3.495899414062" Y="1.317071655273" />
                  <Point X="-3.483484863281" Y="1.334800415039" />
                  <Point X="-3.478013916016" Y="1.343598022461" />
                  <Point X="-3.468063476562" Y="1.361734863281" />
                  <Point X="-3.463583740234" Y="1.371074584961" />
                  <Point X="-3.439797363281" Y="1.428500244141" />
                  <Point X="-3.435500732422" Y="1.440347412109" />
                  <Point X="-3.429713134766" Y="1.460202026367" />
                  <Point X="-3.427361083984" Y="1.470287841797" />
                  <Point X="-3.423601806641" Y="1.491602905273" />
                  <Point X="-3.422361572266" Y="1.50188671875" />
                  <Point X="-3.421008056641" Y="1.522527099609" />
                  <Point X="-3.421909423828" Y="1.54319128418" />
                  <Point X="-3.425055175781" Y="1.563635375977" />
                  <Point X="-3.427186279297" Y="1.573771850586" />
                  <Point X="-3.432787353516" Y="1.594678466797" />
                  <Point X="-3.436009765625" Y="1.604523925781" />
                  <Point X="-3.443507568359" Y="1.623805786133" />
                  <Point X="-3.447782958984" Y="1.6332421875" />
                  <Point X="-3.476483886719" Y="1.688376342773" />
                  <Point X="-3.482798828125" Y="1.699284423828" />
                  <Point X="-3.494289794922" Y="1.716482177734" />
                  <Point X="-3.500504638672" Y="1.724767456055" />
                  <Point X="-3.51441796875" Y="1.741349121094" />
                  <Point X="-3.521498779297" Y="1.748909423828" />
                  <Point X="-3.53644140625" Y="1.763214111328" />
                  <Point X="-3.544302978516" Y="1.769958251953" />
                  <Point X="-3.574034179688" Y="1.792772094727" />
                  <Point X="-4.227614746094" Y="2.294281982422" />
                  <Point X="-4.006121337891" Y="2.673753173828" />
                  <Point X="-4.002292724609" Y="2.6803125" />
                  <Point X="-3.726338378906" Y="3.035012695312" />
                  <Point X="-3.254156982422" Y="2.7623984375" />
                  <Point X="-3.244918457031" Y="2.757716552734" />
                  <Point X="-3.225986083984" Y="2.749385986328" />
                  <Point X="-3.216292236328" Y="2.745737304688" />
                  <Point X="-3.195650146484" Y="2.739228759766" />
                  <Point X="-3.185615722656" Y="2.736656982422" />
                  <Point X="-3.165327880859" Y="2.732621582031" />
                  <Point X="-3.155074462891" Y="2.731157958984" />
                  <Point X="-3.072513671875" Y="2.723934814453" />
                  <Point X="-3.059174316406" Y="2.723334228516" />
                  <Point X="-3.038495605469" Y="2.72378515625" />
                  <Point X="-3.028167724609" Y="2.724575195312" />
                  <Point X="-3.006710205078" Y="2.727399902344" />
                  <Point X="-2.996530273438" Y="2.729309570312" />
                  <Point X="-2.976439941406" Y="2.734225341797" />
                  <Point X="-2.957004638672" Y="2.741298583984" />
                  <Point X="-2.938454101562" Y="2.750445800781" />
                  <Point X="-2.929428466797" Y="2.755525878906" />
                  <Point X="-2.911174804688" Y="2.767153564453" />
                  <Point X="-2.902753173828" Y="2.773187988281" />
                  <Point X="-2.886617919922" Y="2.786137451172" />
                  <Point X="-2.878904296875" Y="2.793052490234" />
                  <Point X="-2.820302001953" Y="2.851654785156" />
                  <Point X="-2.811268554688" Y="2.861485839844" />
                  <Point X="-2.798323486328" Y="2.877614013672" />
                  <Point X="-2.792290527344" Y="2.886032226562" />
                  <Point X="-2.780659667969" Y="2.904287353516" />
                  <Point X="-2.775578125" Y="2.913314208984" />
                  <Point X="-2.766428466797" Y="2.931866455078" />
                  <Point X="-2.759353271484" Y="2.951304199219" />
                  <Point X="-2.754436035156" Y="2.971397216797" />
                  <Point X="-2.752525878906" Y="2.981577880859" />
                  <Point X="-2.749700195312" Y="3.003038085938" />
                  <Point X="-2.748909912109" Y="3.013366699219" />
                  <Point X="-2.748458496094" Y="3.034048095703" />
                  <Point X="-2.748797363281" Y="3.044400878906" />
                  <Point X="-2.756020507812" Y="3.126961914062" />
                  <Point X="-2.757745605469" Y="3.140203857422" />
                  <Point X="-2.761781005859" Y="3.160491699219" />
                  <Point X="-2.764352783203" Y="3.170526123047" />
                  <Point X="-2.770861328125" Y="3.191168212891" />
                  <Point X="-2.774510253906" Y="3.200862548828" />
                  <Point X="-2.782841064453" Y="3.219795166016" />
                  <Point X="-2.787522949219" Y="3.229033447266" />
                  <Point X="-2.800697753906" Y="3.251852783203" />
                  <Point X="-3.059387207031" Y="3.699916259766" />
                  <Point X="-2.655033935547" Y="4.0099296875" />
                  <Point X="-2.648370605469" Y="4.015038574219" />
                  <Point X="-2.192525146484" Y="4.268295898437" />
                  <Point X="-2.118564208984" Y="4.171908203125" />
                  <Point X="-2.111819091797" Y="4.164045898438" />
                  <Point X="-2.097515380859" Y="4.149104492188" />
                  <Point X="-2.089956787109" Y="4.142025390625" />
                  <Point X="-2.073376464844" Y="4.128112792969" />
                  <Point X="-2.065091796875" Y="4.121897949219" />
                  <Point X="-2.047893066406" Y="4.110405761719" />
                  <Point X="-2.038978881836" Y="4.105128417969" />
                  <Point X="-1.947089233398" Y="4.057293701172" />
                  <Point X="-1.934335327148" Y="4.051290283203" />
                  <Point X="-1.915063110352" Y="4.043795166016" />
                  <Point X="-1.905223266602" Y="4.040573730469" />
                  <Point X="-1.884314453125" Y="4.034969726562" />
                  <Point X="-1.874177001953" Y="4.032837402344" />
                  <Point X="-1.853731201172" Y="4.029689453125" />
                  <Point X="-1.833065673828" Y="4.028786132812" />
                  <Point X="-1.812423339844" Y="4.030138183594" />
                  <Point X="-1.802138427734" Y="4.031377929688" />
                  <Point X="-1.780820678711" Y="4.035136230469" />
                  <Point X="-1.770732788086" Y="4.037488037109" />
                  <Point X="-1.750871704102" Y="4.043276611328" />
                  <Point X="-1.741098388672" Y="4.046713378906" />
                  <Point X="-1.645388916016" Y="4.086357910156" />
                  <Point X="-1.632584716797" Y="4.092272949219" />
                  <Point X="-1.614450073242" Y="4.102222167969" />
                  <Point X="-1.605654785156" Y="4.10769140625" />
                  <Point X="-1.58792578125" Y="4.12010546875" />
                  <Point X="-1.57977746582" Y="4.126500488281" />
                  <Point X="-1.564226806641" Y="4.140138183594" />
                  <Point X="-1.550254272461" Y="4.15538671875" />
                  <Point X="-1.53802355957" Y="4.17206640625" />
                  <Point X="-1.532363647461" Y="4.180740234375" />
                  <Point X="-1.521541503906" Y="4.199483398437" />
                  <Point X="-1.516858764648" Y="4.20872265625" />
                  <Point X="-1.508526733398" Y="4.227657714844" />
                  <Point X="-1.504877441406" Y="4.237353515625" />
                  <Point X="-1.473725952148" Y="4.336153808594" />
                  <Point X="-1.470026855469" Y="4.349762695312" />
                  <Point X="-1.465991455078" Y="4.370049316406" />
                  <Point X="-1.46452746582" Y="4.380303222656" />
                  <Point X="-1.462640869141" Y="4.401866210938" />
                  <Point X="-1.462301879883" Y="4.41221875" />
                  <Point X="-1.462753051758" Y="4.4328984375" />
                  <Point X="-1.465040649414" Y="4.454602050781" />
                  <Point X="-1.479265991211" Y="4.562654785156" />
                  <Point X="-0.939796813965" Y="4.713903320312" />
                  <Point X="-0.931176574707" Y="4.716319824219" />
                  <Point X="-0.365221954346" Y="4.782556640625" />
                  <Point X="-0.225666290283" Y="4.261727539062" />
                  <Point X="-0.220435211182" Y="4.247107910156" />
                  <Point X="-0.207661819458" Y="4.218916503906" />
                  <Point X="-0.20011920166" Y="4.205344726562" />
                  <Point X="-0.182260818481" Y="4.178618164062" />
                  <Point X="-0.172608215332" Y="4.166456054688" />
                  <Point X="-0.151451293945" Y="4.1438671875" />
                  <Point X="-0.126897750854" Y="4.125026367188" />
                  <Point X="-0.099602561951" Y="4.110436523438" />
                  <Point X="-0.085356460571" Y="4.104260742188" />
                  <Point X="-0.054918491364" Y="4.093927978516" />
                  <Point X="-0.03985704422" Y="4.090155273438" />
                  <Point X="-0.009319943428" Y="4.08511328125" />
                  <Point X="0.02163196373" Y="4.08511328125" />
                  <Point X="0.052169067383" Y="4.090155273438" />
                  <Point X="0.067230514526" Y="4.093927978516" />
                  <Point X="0.097668479919" Y="4.104260742188" />
                  <Point X="0.111914581299" Y="4.110436523438" />
                  <Point X="0.139209777832" Y="4.125026367188" />
                  <Point X="0.163763168335" Y="4.143866699219" />
                  <Point X="0.184920227051" Y="4.166455566406" />
                  <Point X="0.194573135376" Y="4.178618164062" />
                  <Point X="0.212431228638" Y="4.205344726562" />
                  <Point X="0.219973678589" Y="4.218916992188" />
                  <Point X="0.232747375488" Y="4.247108886719" />
                  <Point X="0.237978302002" Y="4.261728515625" />
                  <Point X="0.244728912354" Y="4.286922363281" />
                  <Point X="0.378190093994" Y="4.785006347656" />
                  <Point X="0.820335449219" Y="4.738701660156" />
                  <Point X="0.827875671387" Y="4.737912109375" />
                  <Point X="1.443715576172" Y="4.589229492188" />
                  <Point X="1.453598510742" Y="4.586843261719" />
                  <Point X="1.853427001953" Y="4.441822753906" />
                  <Point X="1.858256713867" Y="4.440071289062" />
                  <Point X="2.245864501953" Y="4.258799804688" />
                  <Point X="2.250446044922" Y="4.256657226562" />
                  <Point X="2.624944091797" Y="4.038473876953" />
                  <Point X="2.629432617188" Y="4.035858886719" />
                  <Point X="2.817779785156" Y="3.901916503906" />
                  <Point X="2.065308349609" Y="2.598597412109" />
                  <Point X="2.062372802734" Y="2.593104492188" />
                  <Point X="2.053180908203" Y="2.5734375" />
                  <Point X="2.044182128906" Y="2.549563476562" />
                  <Point X="2.041301513672" Y="2.540598632812" />
                  <Point X="2.02058190918" Y="2.463116943359" />
                  <Point X="2.018868408203" Y="2.455452880859" />
                  <Point X="2.015005737305" Y="2.432229736328" />
                  <Point X="2.012917602539" Y="2.410526855469" />
                  <Point X="2.012487792969" Y="2.400284179688" />
                  <Point X="2.01273425293" Y="2.379812744141" />
                  <Point X="2.013410888672" Y="2.369583984375" />
                  <Point X="2.021489868164" Y="2.302584472656" />
                  <Point X="2.023799926758" Y="2.289039794922" />
                  <Point X="2.029141967773" Y="2.267118408203" />
                  <Point X="2.032466430664" Y="2.25631640625" />
                  <Point X="2.040734375" Y="2.234221191406" />
                  <Point X="2.045318603516" Y="2.223889160156" />
                  <Point X="2.055681884766" Y="2.203842529297" />
                  <Point X="2.0614609375" Y="2.194127929688" />
                  <Point X="2.10291796875" Y="2.133030761719" />
                  <Point X="2.107605957031" Y="2.126703125" />
                  <Point X="2.122686523437" Y="2.108539550781" />
                  <Point X="2.137488769531" Y="2.093028564453" />
                  <Point X="2.144835693359" Y="2.086106689453" />
                  <Point X="2.160221435547" Y="2.073082275391" />
                  <Point X="2.168260253906" Y="2.066979736328" />
                  <Point X="2.229357177734" Y="2.025522583008" />
                  <Point X="2.241282958984" Y="2.018243286133" />
                  <Point X="2.261329101562" Y="2.007880004883" />
                  <Point X="2.271661132812" Y="2.003295654297" />
                  <Point X="2.293745361328" Y="1.995031494141" />
                  <Point X="2.304547851562" Y="1.99170703125" />
                  <Point X="2.326469970703" Y="1.986364868164" />
                  <Point X="2.337589599609" Y="1.984346801758" />
                  <Point X="2.404589355469" Y="1.976267700195" />
                  <Point X="2.412587402344" Y="1.975644287109" />
                  <Point X="2.436634765625" Y="1.97512878418" />
                  <Point X="2.457963623047" Y="1.976173950195" />
                  <Point X="2.467993164062" Y="1.977201049805" />
                  <Point X="2.487885498047" Y="1.980312133789" />
                  <Point X="2.497748291016" Y="1.982395874023" />
                  <Point X="2.575229980469" Y="2.003115600586" />
                  <Point X="2.597882324219" Y="2.010764038086" />
                  <Point X="2.626035888672" Y="2.022860351562" />
                  <Point X="2.636033935547" Y="2.027872680664" />
                  <Point X="2.688167480469" Y="2.057971923828" />
                  <Point X="3.940404785156" Y="2.780951416016" />
                  <Point X="4.041287841797" Y="2.640747314453" />
                  <Point X="4.043949951172" Y="2.637047607422" />
                  <Point X="4.136884765625" Y="2.483471923828" />
                  <Point X="3.172952392578" Y="1.743820556641" />
                  <Point X="3.168138916016" Y="1.739869750977" />
                  <Point X="3.152119628906" Y="1.725217163086" />
                  <Point X="3.134669189453" Y="1.706604003906" />
                  <Point X="3.128576660156" Y="1.699423095703" />
                  <Point X="3.072812988281" Y="1.626675048828" />
                  <Point X="3.068374755859" Y="1.62037097168" />
                  <Point X="3.05611328125" Y="1.600782348633" />
                  <Point X="3.045625244141" Y="1.581197509766" />
                  <Point X="3.0412265625" Y="1.571779418945" />
                  <Point X="3.033484130859" Y="1.552517211914" />
                  <Point X="3.030140380859" Y="1.542672973633" />
                  <Point X="3.009368408203" Y="1.468396972656" />
                  <Point X="3.006225585938" Y="1.454662353516" />
                  <Point X="3.002772216797" Y="1.432364868164" />
                  <Point X="3.001709716797" Y="1.42111340332" />
                  <Point X="3.000893310547" Y="1.397542480469" />
                  <Point X="3.001174804688" Y="1.38624230957" />
                  <Point X="3.003077880859" Y="1.363756591797" />
                  <Point X="3.004699462891" Y="1.352570922852" />
                  <Point X="3.021751220703" Y="1.26992956543" />
                  <Point X="3.023621337891" Y="1.262423828125" />
                  <Point X="3.030441162109" Y="1.240260253906" />
                  <Point X="3.038257324219" Y="1.219889648438" />
                  <Point X="3.042432373047" Y="1.210547119141" />
                  <Point X="3.051763427734" Y="1.192364501953" />
                  <Point X="3.056919433594" Y="1.183524414062" />
                  <Point X="3.103298095703" Y="1.113030761719" />
                  <Point X="3.111740966797" Y="1.10142199707" />
                  <Point X="3.126296386719" Y="1.084175292969" />
                  <Point X="3.134087890625" Y="1.075985839844" />
                  <Point X="3.15133203125" Y="1.059896728516" />
                  <Point X="3.160038818359" Y="1.052692504883" />
                  <Point X="3.178246582031" Y="1.039368530273" />
                  <Point X="3.187747558594" Y="1.033248779297" />
                  <Point X="3.254956787109" Y="0.99541595459" />
                  <Point X="3.261993408203" Y="0.991831787109" />
                  <Point X="3.283647705078" Y="0.982273071289" />
                  <Point X="3.304055175781" Y="0.974906616211" />
                  <Point X="3.313829589844" Y="0.971961364746" />
                  <Point X="3.333637695312" Y="0.967136901855" />
                  <Point X="3.343671142578" Y="0.965257568359" />
                  <Point X="3.434542480469" Y="0.953247497559" />
                  <Point X="3.457929931641" Y="0.951649353027" />
                  <Point X="3.489237792969" Y="0.95199017334" />
                  <Point X="3.500603515625" Y="0.952797180176" />
                  <Point X="3.550126953125" Y="0.959317077637" />
                  <Point X="4.704703613281" Y="1.111319824219" />
                  <Point X="4.751539550781" Y="0.918930603027" />
                  <Point X="4.752685058594" Y="0.914224609375" />
                  <Point X="4.783871582031" Y="0.713920959473" />
                  <Point X="3.6919921875" Y="0.421352874756" />
                  <Point X="3.686031982422" Y="0.419544433594" />
                  <Point X="3.665627441406" Y="0.412138275146" />
                  <Point X="3.642381835938" Y="0.401619781494" />
                  <Point X="3.634004638672" Y="0.397316833496" />
                  <Point X="3.544726318359" Y="0.345712310791" />
                  <Point X="3.538330078125" Y="0.341666778564" />
                  <Point X="3.519824462891" Y="0.328537139893" />
                  <Point X="3.502280273438" Y="0.314248382568" />
                  <Point X="3.4944921875" Y="0.307152099609" />
                  <Point X="3.479750244141" Y="0.292140869141" />
                  <Point X="3.472796386719" Y="0.284226257324" />
                  <Point X="3.419229492188" Y="0.215969406128" />
                  <Point X="3.410854492188" Y="0.204208587646" />
                  <Point X="3.399131103516" Y="0.184929962158" />
                  <Point X="3.39384375" Y="0.174941314697" />
                  <Point X="3.384070556641" Y="0.153479873657" />
                  <Point X="3.380006591797" Y="0.142932846069" />
                  <Point X="3.373159667969" Y="0.121430381775" />
                  <Point X="3.370376708984" Y="0.110474945068" />
                  <Point X="3.352520996094" Y="0.017239337921" />
                  <Point X="3.351390869141" Y="0.009721099854" />
                  <Point X="3.349210449219" Y="-0.012964847565" />
                  <Point X="3.348563964844" Y="-0.035153205872" />
                  <Point X="3.348827880859" Y="-0.045515937805" />
                  <Point X="3.350483154297" Y="-0.066151634216" />
                  <Point X="3.351874511719" Y="-0.076424446106" />
                  <Point X="3.369730224609" Y="-0.169660202026" />
                  <Point X="3.373159423828" Y="-0.18399041748" />
                  <Point X="3.380006347656" Y="-0.205493041992" />
                  <Point X="3.384070556641" Y="-0.216040359497" />
                  <Point X="3.39384375" Y="-0.237501647949" />
                  <Point X="3.399131591797" Y="-0.247491195679" />
                  <Point X="3.410855224609" Y="-0.26676965332" />
                  <Point X="3.417290771484" Y="-0.27605859375" />
                  <Point X="3.470857666016" Y="-0.344315582275" />
                  <Point X="3.475901123047" Y="-0.350227355957" />
                  <Point X="3.491964355469" Y="-0.367098876953" />
                  <Point X="3.508215820312" Y="-0.381990447998" />
                  <Point X="3.516203125" Y="-0.388547119141" />
                  <Point X="3.532842773438" Y="-0.400754089355" />
                  <Point X="3.541494873047" Y="-0.406404296875" />
                  <Point X="3.630773193359" Y="-0.458008850098" />
                  <Point X="3.651355224609" Y="-0.468258758545" />
                  <Point X="3.680993408203" Y="-0.480232879639" />
                  <Point X="3.691991943359" Y="-0.48391293335" />
                  <Point X="3.737407226562" Y="-0.496081878662" />
                  <Point X="4.784877441406" Y="-0.776750610352" />
                  <Point X="4.762252929688" Y="-0.926813598633" />
                  <Point X="4.761614257812" Y="-0.931050964355" />
                  <Point X="4.727801757812" Y="-1.079219726563" />
                  <Point X="3.436781982422" Y="-0.909253540039" />
                  <Point X="3.428624267578" Y="-0.908535705566" />
                  <Point X="3.400096923828" Y="-0.908042419434" />
                  <Point X="3.366720947266" Y="-0.910841003418" />
                  <Point X="3.354481445312" Y="-0.912676330566" />
                  <Point X="3.179259765625" Y="-0.950761413574" />
                  <Point X="3.157874267578" Y="-0.956742553711" />
                  <Point X="3.128754882813" Y="-0.968366638184" />
                  <Point X="3.114678466797" Y="-0.975388061523" />
                  <Point X="3.086851806641" Y="-0.992280090332" />
                  <Point X="3.074125976562" Y="-1.001528869629" />
                  <Point X="3.050375" Y="-1.022000061035" />
                  <Point X="3.039349853516" Y="-1.03322253418" />
                  <Point X="2.933439453125" Y="-1.160599853516" />
                  <Point X="2.921326660156" Y="-1.176846557617" />
                  <Point X="2.906606445312" Y="-1.201227783203" />
                  <Point X="2.900165283203" Y="-1.21397277832" />
                  <Point X="2.888821777344" Y="-1.241357177734" />
                  <Point X="2.884363525391" Y="-1.254925415039" />
                  <Point X="2.87753125" Y="-1.282576538086" />
                  <Point X="2.875157226562" Y="-1.296659545898" />
                  <Point X="2.859977783203" Y="-1.461618774414" />
                  <Point X="2.859288574219" Y="-1.483319580078" />
                  <Point X="2.861606933594" Y="-1.514586791992" />
                  <Point X="2.864064941406" Y="-1.530124267578" />
                  <Point X="2.871796142578" Y="-1.561745727539" />
                  <Point X="2.87678515625" Y="-1.576666259766" />
                  <Point X="2.889156982422" Y="-1.605478881836" />
                  <Point X="2.896539794922" Y="-1.61937109375" />
                  <Point X="2.993510009766" Y="-1.770201782227" />
                  <Point X="3.007593261719" Y="-1.789356567383" />
                  <Point X="3.022723632812" Y="-1.807444946289" />
                  <Point X="3.029844726562" Y="-1.815065917969" />
                  <Point X="3.044881103516" Y="-1.829482666016" />
                  <Point X="3.052796142578" Y="-1.836278076172" />
                  <Point X="3.094941894531" Y="-1.868617431641" />
                  <Point X="4.087170898438" Y="-2.629981445312" />
                  <Point X="4.047289550781" Y="-2.694515380859" />
                  <Point X="4.045484619141" Y="-2.697436035156" />
                  <Point X="4.001274658203" Y="-2.760251953125" />
                  <Point X="2.848455566406" Y="-2.094671142578" />
                  <Point X="2.841193115234" Y="-2.090885742188" />
                  <Point X="2.815026123047" Y="-2.079513183594" />
                  <Point X="2.783119140625" Y="-2.069325927734" />
                  <Point X="2.771108398438" Y="-2.066337646484" />
                  <Point X="2.562566650391" Y="-2.028675170898" />
                  <Point X="2.539358398438" Y="-2.025807495117" />
                  <Point X="2.508005126953" Y="-2.025403442383" />
                  <Point X="2.492311523438" Y="-2.02650390625" />
                  <Point X="2.460137939453" Y="-2.031462158203" />
                  <Point X="2.444842773438" Y="-2.035137084961" />
                  <Point X="2.415067871094" Y="-2.044960693359" />
                  <Point X="2.400588134766" Y="-2.051109130859" />
                  <Point X="2.227341064453" Y="-2.142287597656" />
                  <Point X="2.208967285156" Y="-2.153170898438" />
                  <Point X="2.186035400391" Y="-2.170064941406" />
                  <Point X="2.175206298828" Y="-2.179376220703" />
                  <Point X="2.154246582031" Y="-2.200336669922" />
                  <Point X="2.144936523438" Y="-2.211164794922" />
                  <Point X="2.128044677734" Y="-2.234094482422" />
                  <Point X="2.120462890625" Y="-2.246196044922" />
                  <Point X="2.029284301758" Y="-2.419442871094" />
                  <Point X="2.019834960938" Y="-2.440194580078" />
                  <Point X="2.010010986328" Y="-2.469971923828" />
                  <Point X="2.00633605957" Y="-2.485268798828" />
                  <Point X="2.00137902832" Y="-2.517440429688" />
                  <Point X="2.000278930664" Y="-2.533133300781" />
                  <Point X="2.00068347168" Y="-2.564484130859" />
                  <Point X="2.002187866211" Y="-2.580142089844" />
                  <Point X="2.039850219727" Y="-2.788683837891" />
                  <Point X="2.045196044922" Y="-2.810690185547" />
                  <Point X="2.052462158203" Y="-2.834352539062" />
                  <Point X="2.056179199219" Y="-2.844399902344" />
                  <Point X="2.064721435547" Y="-2.864012939453" />
                  <Point X="2.069546630859" Y="-2.873578613281" />
                  <Point X="2.096629394531" Y="-2.920487304688" />
                  <Point X="2.735893310547" Y="-4.027725097656" />
                  <Point X="2.723754638672" Y="-4.036083984375" />
                  <Point X="1.833913818359" Y="-2.876421875" />
                  <Point X="1.828653076172" Y="-2.870145751953" />
                  <Point X="1.808832397461" Y="-2.849627685547" />
                  <Point X="1.783252807617" Y="-2.828005371094" />
                  <Point X="1.773299560547" Y="-2.820647705078" />
                  <Point X="1.56762109375" Y="-2.688415527344" />
                  <Point X="1.54628503418" Y="-2.676246582031" />
                  <Point X="1.517475097656" Y="-2.663875488281" />
                  <Point X="1.502556030273" Y="-2.65888671875" />
                  <Point X="1.470934082031" Y="-2.651154541016" />
                  <Point X="1.455395751953" Y="-2.648696044922" />
                  <Point X="1.424125732422" Y="-2.646376953125" />
                  <Point X="1.40839440918" Y="-2.646516357422" />
                  <Point X="1.183450073242" Y="-2.667216064453" />
                  <Point X="1.161225830078" Y="-2.670339111328" />
                  <Point X="1.133577270508" Y="-2.677170410156" />
                  <Point X="1.120010498047" Y="-2.681627929688" />
                  <Point X="1.092626220703" Y="-2.692970458984" />
                  <Point X="1.079880126953" Y="-2.699411865234" />
                  <Point X="1.055496704102" Y="-2.7141328125" />
                  <Point X="1.043859741211" Y="-2.722412353516" />
                  <Point X="0.870163330078" Y="-2.866835205078" />
                  <Point X="0.852653930664" Y="-2.883087402344" />
                  <Point X="0.832183654785" Y="-2.906836914062" />
                  <Point X="0.822935180664" Y="-2.919561767578" />
                  <Point X="0.806041809082" Y="-2.947389404297" />
                  <Point X="0.79901965332" Y="-2.961467041016" />
                  <Point X="0.78739453125" Y="-2.990588134766" />
                  <Point X="0.782791809082" Y="-3.005631591797" />
                  <Point X="0.73085748291" Y="-3.2445703125" />
                  <Point X="0.727475097656" Y="-3.265921630859" />
                  <Point X="0.724882080078" Y="-3.291716552734" />
                  <Point X="0.724417053223" Y="-3.302677246094" />
                  <Point X="0.724753356934" Y="-3.324579101562" />
                  <Point X="0.725554931641" Y="-3.335520751953" />
                  <Point X="0.733230224609" Y="-3.393818359375" />
                  <Point X="0.83309173584" Y="-4.152341796875" />
                  <Point X="0.655064697266" Y="-3.487935791016" />
                  <Point X="0.652605712891" Y="-3.480123046875" />
                  <Point X="0.642145080566" Y="-3.453580566406" />
                  <Point X="0.626787109375" Y="-3.423815917969" />
                  <Point X="0.620407470703" Y="-3.413210205078" />
                  <Point X="0.46239932251" Y="-3.185550537109" />
                  <Point X="0.446671173096" Y="-3.165173339844" />
                  <Point X="0.424787322998" Y="-3.142717773438" />
                  <Point X="0.412912078857" Y="-3.132398681641" />
                  <Point X="0.386656188965" Y="-3.113154541016" />
                  <Point X="0.373241790771" Y="-3.104937255859" />
                  <Point X="0.345240631104" Y="-3.090829101562" />
                  <Point X="0.330654205322" Y="-3.084938232422" />
                  <Point X="0.086145896912" Y="-3.009051757812" />
                  <Point X="0.06337538147" Y="-3.003108886719" />
                  <Point X="0.035214313507" Y="-2.998839599609" />
                  <Point X="0.020973268509" Y="-2.997766357422" />
                  <Point X="-0.008668827057" Y="-2.997766845703" />
                  <Point X="-0.02290838623" Y="-2.998840332031" />
                  <Point X="-0.051066184998" Y="-3.003109619141" />
                  <Point X="-0.064984275818" Y="-3.006305419922" />
                  <Point X="-0.309492889404" Y="-3.082191650391" />
                  <Point X="-0.33293069458" Y="-3.090830078125" />
                  <Point X="-0.360932739258" Y="-3.104938964844" />
                  <Point X="-0.374347442627" Y="-3.113156494141" />
                  <Point X="-0.400601837158" Y="-3.132400146484" />
                  <Point X="-0.412476348877" Y="-3.14271875" />
                  <Point X="-0.434359436035" Y="-3.165173583984" />
                  <Point X="-0.444368011475" Y="-3.177309814453" />
                  <Point X="-0.602376159668" Y="-3.404969482422" />
                  <Point X="-0.613225341797" Y="-3.422574707031" />
                  <Point X="-0.626165039062" Y="-3.446383544922" />
                  <Point X="-0.630925720215" Y="-3.456526855469" />
                  <Point X="-0.639219604492" Y="-3.477302978516" />
                  <Point X="-0.642752807617" Y="-3.487936035156" />
                  <Point X="-0.656746582031" Y="-3.540160400391" />
                  <Point X="-0.985425170898" Y="-4.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.150211594594" Y="-2.495445782183" />
                  <Point X="-3.708653311674" Y="-2.968959068439" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.951577233171" Y="-3.780823766134" />
                  <Point X="-2.722022011172" Y="-4.026991603429" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.115751409208" Y="-2.393103284097" />
                  <Point X="-3.624216779599" Y="-2.920209640784" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.901906879535" Y="-3.694792176553" />
                  <Point X="-2.486310167605" Y="-4.140465086416" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.040034282763" Y="-2.335003438674" />
                  <Point X="-3.539780247523" Y="-2.871460213129" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.852236525899" Y="-3.608760586973" />
                  <Point X="-2.427673710748" Y="-4.064048465381" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.964317156318" Y="-2.276903593251" />
                  <Point X="-3.455343715448" Y="-2.822710785474" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.802566172262" Y="-3.522728997393" />
                  <Point X="-2.36226606743" Y="-3.994893052836" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.888600029873" Y="-2.218803747828" />
                  <Point X="-3.370907183372" Y="-2.773961357819" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.752895818626" Y="-3.436697407812" />
                  <Point X="-2.282690409561" Y="-3.940930975779" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.673374227495" Y="-1.237939931228" />
                  <Point X="-4.631519550664" Y="-1.282823577029" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.812882903428" Y="-2.160703902405" />
                  <Point X="-3.286470651296" Y="-2.725211930164" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.70322546499" Y="-3.350665818232" />
                  <Point X="-2.202660211642" Y="-3.887456333247" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.722377421367" Y="-1.046093916792" />
                  <Point X="-4.515826795867" Y="-1.267592344615" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.737165776983" Y="-2.102604056981" />
                  <Point X="-3.202034119221" Y="-2.676462502509" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.653555111354" Y="-3.264634228652" />
                  <Point X="-2.122630013724" Y="-3.833981690716" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.755464423365" Y="-0.871315928506" />
                  <Point X="-4.400134041069" Y="-1.252361112201" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.661448650538" Y="-2.044504211558" />
                  <Point X="-3.117597587145" Y="-2.627713074855" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.603884757717" Y="-3.178602639071" />
                  <Point X="-2.034610471533" Y="-3.789074570997" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.778995867321" Y="-0.70678502167" />
                  <Point X="-4.284441286272" Y="-1.237129879786" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.585731524093" Y="-1.986404366135" />
                  <Point X="-3.03316105507" Y="-2.5789636472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.554214404081" Y="-3.092571049491" />
                  <Point X="-1.915107450088" Y="-3.777929349312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.126994954976" Y="-4.623076529049" />
                  <Point X="-0.994630671595" Y="-4.765019844873" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.715452631299" Y="-0.635630277078" />
                  <Point X="-4.168748531474" Y="-1.221898647372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.510014397648" Y="-1.928304520712" />
                  <Point X="-2.948724522994" Y="-2.530214219545" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.504544050445" Y="-3.006539459911" />
                  <Point X="-1.776755187151" Y="-3.786997464411" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.125858696048" Y="-4.484998494935" />
                  <Point X="-0.95811457906" Y="-4.664882037284" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.611524668695" Y="-0.607782849635" />
                  <Point X="-4.053055776677" Y="-1.206667414957" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.434297271203" Y="-1.870204675289" />
                  <Point X="-2.864287990918" Y="-2.48146479189" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.454873696808" Y="-2.92050787033" />
                  <Point X="-1.638402924213" Y="-3.796065579511" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.161079436801" Y="-4.307932351972" />
                  <Point X="-0.929121184593" Y="-4.556677123672" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.507596706091" Y="-0.579935422192" />
                  <Point X="-3.937363021879" Y="-1.191436182543" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.358580144758" Y="-1.812104829865" />
                  <Point X="-2.779851458843" Y="-2.432715364235" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.405203343172" Y="-2.83447628075" />
                  <Point X="-1.269695178445" Y="-4.052159706581" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.235705708929" Y="-4.088608950161" />
                  <Point X="-0.900127790126" Y="-4.448472210059" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.403668743487" Y="-0.55208799475" />
                  <Point X="-3.821670267082" Y="-1.176204950128" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.282863018313" Y="-1.754004984442" />
                  <Point X="-2.695414974267" Y="-2.383965885643" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.355532989536" Y="-2.74844469117" />
                  <Point X="-0.871134395659" Y="-4.340267296447" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.299740780883" Y="-0.524240567307" />
                  <Point X="-3.705977512284" Y="-1.160973717714" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.207145891868" Y="-1.695905139019" />
                  <Point X="-2.5964805926" Y="-2.350763498252" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.316635856025" Y="-2.65086023742" />
                  <Point X="-0.842141001192" Y="-4.232062382835" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.195812818279" Y="-0.496393139865" />
                  <Point X="-3.590284757487" Y="-1.1457424853" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.131428765423" Y="-1.637805293596" />
                  <Point X="-2.424818066365" Y="-2.395552497435" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.347018598663" Y="-2.478982212255" />
                  <Point X="-0.813147606725" Y="-4.123857469223" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.091884855675" Y="-0.468545712422" />
                  <Point X="-3.47459200269" Y="-1.130511252885" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.055711638978" Y="-1.579705448173" />
                  <Point X="-0.784154212258" Y="-4.015652555611" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.987956893071" Y="-0.440698284979" />
                  <Point X="-3.358899247892" Y="-1.115280020471" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.99234007893" Y="-1.508366603639" />
                  <Point X="-0.755160817792" Y="-3.907447641999" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.884028930467" Y="-0.412850857537" />
                  <Point X="-3.243122833349" Y="-1.10013850215" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.953664888968" Y="-1.410544144572" />
                  <Point X="-0.726167423325" Y="-3.799242728387" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.772512767379" Y="0.679227931167" />
                  <Point X="-4.65955073205" Y="0.558090979059" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.780100967863" Y="-0.385003430094" />
                  <Point X="-3.075513807378" Y="-1.140580654483" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.969109219979" Y="-1.254685604612" />
                  <Point X="-0.697174028858" Y="-3.691037814775" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.754723255256" Y="0.799447537634" />
                  <Point X="-4.486386717431" Y="0.511691830715" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.676173005259" Y="-0.357156002652" />
                  <Point X="-0.668180634391" Y="-3.582832901163" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.736933743132" Y="0.919667144102" />
                  <Point X="-4.313222702813" Y="0.465292682372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.572245042655" Y="-0.329308575209" />
                  <Point X="-0.638462147505" Y="-3.475405553974" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.711295044924" Y="1.031469529014" />
                  <Point X="-4.140058688194" Y="0.418893534029" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.471532233526" Y="-0.298013317781" />
                  <Point X="-0.590374239275" Y="-3.387676999453" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.682047430695" Y="1.139401825308" />
                  <Point X="-3.966894673576" Y="0.372494385685" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.390725582898" Y="-0.245371318841" />
                  <Point X="-0.534947802496" Y="-3.307818053328" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.652799816466" Y="1.247334121602" />
                  <Point X="-3.793730658957" Y="0.326095237342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.331047284475" Y="-0.170071936102" />
                  <Point X="-0.479521365716" Y="-3.227959107203" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.582785102954" Y="1.311549056225" />
                  <Point X="-3.620566644339" Y="0.279696088998" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.297665634366" Y="-0.066572850532" />
                  <Point X="-0.420978062664" Y="-3.151442590942" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.434710118788" Y="1.292054599104" />
                  <Point X="-3.424308514299" Y="0.208531533891" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.332153635359" Y="0.10970752524" />
                  <Point X="-0.342913581136" Y="-3.095859975661" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.286635134622" Y="1.272560141983" />
                  <Point X="-0.24455697683" Y="-3.062037997908" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.138560150457" Y="1.253065684862" />
                  <Point X="-0.143816824179" Y="-3.030772062817" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.805282565127" Y="-4.048556550612" />
                  <Point X="0.821753517641" Y="-4.066219484713" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.990485166291" Y="1.233571227741" />
                  <Point X="-0.041122306447" Y="-3.001601927688" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.752909197935" Y="-3.853096467761" />
                  <Point X="0.800400106166" Y="-3.90402423176" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.842410182125" Y="1.21407677062" />
                  <Point X="0.099620758601" Y="-3.013233864163" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.700535830743" Y="-3.65763638491" />
                  <Point X="0.779046694691" Y="-3.741828978806" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.706661516382" Y="1.207800671686" />
                  <Point X="0.282423364055" Y="-3.069969135727" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.64360779901" Y="-3.457292022321" />
                  <Point X="0.757693283216" Y="-3.579633725852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.604326597874" Y="1.23735642977" />
                  <Point X="0.736339871741" Y="-3.417438472898" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.521720897581" Y="1.288069184142" />
                  <Point X="0.727228821626" Y="-3.268371545203" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.465501024336" Y="1.367077273829" />
                  <Point X="0.750529787447" Y="-3.154062249227" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.217075314992" Y="2.312338549023" />
                  <Point X="-4.131369862251" Y="2.220430703225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.428259163871" Y="1.466436790599" />
                  <Point X="0.775083414095" Y="-3.041096267526" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.1670695257" Y="2.398010427902" />
                  <Point X="-3.674722692644" Y="1.870033089653" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.442359991683" Y="1.620854599766" />
                  <Point X="0.810597902578" Y="-2.939884371092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.117063736407" Y="2.48368230678" />
                  <Point X="0.871408845031" Y="-2.86579960037" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.067057947115" Y="2.569354185659" />
                  <Point X="0.944575092755" Y="-2.804964272423" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.017052157823" Y="2.655026064537" />
                  <Point X="1.017741340479" Y="-2.744128944477" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.960649848053" Y="2.733838515002" />
                  <Point X="1.097895968458" Y="-2.69078773685" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.901568993949" Y="2.809778578336" />
                  <Point X="1.20404394787" Y="-2.665320985968" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.842488139845" Y="2.885718641669" />
                  <Point X="1.323674399746" Y="-2.65431241669" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.783407285741" Y="2.961658705003" />
                  <Point X="1.447808429178" Y="-2.648133343067" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.638567802383" Y="-3.92506643606" />
                  <Point X="2.734922167645" Y="-4.028393842441" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.716755652249" Y="3.029480101409" />
                  <Point X="1.686596506608" Y="-2.764905682993" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.035175133176" Y="-3.138710495108" />
                  <Point X="2.527328781864" Y="-3.666480668486" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.435358776503" Y="2.867015419396" />
                  <Point X="2.316171842084" Y="-3.300746050726" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.183511081105" Y="2.736238353796" />
                  <Point X="2.105014902304" Y="-2.935011432965" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.041932293445" Y="2.723710214542" />
                  <Point X="2.02577346615" Y="-2.710738873661" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.937479209773" Y="2.750994518582" />
                  <Point X="2.000422463804" Y="-2.544256729341" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.86229304086" Y="2.809663746249" />
                  <Point X="2.024245899124" Y="-2.430507713308" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.797210197474" Y="2.879167464078" />
                  <Point X="2.070606649264" Y="-2.340927008495" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.75424905084" Y="2.972393797316" />
                  <Point X="2.117469161197" Y="-2.251884377329" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.754729493433" Y="3.112205531555" />
                  <Point X="2.177728215724" Y="-2.177207779264" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.874891275134" Y="3.380359789028" />
                  <Point X="2.259351039401" Y="-2.125441018764" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.049823770801" Y="3.707248446385" />
                  <Point X="2.346484326761" Y="-2.079583507094" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.974080372309" Y="3.765320118486" />
                  <Point X="2.437264911174" Y="-2.037637242661" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.898336973817" Y="3.823391790588" />
                  <Point X="2.558313711636" Y="-2.028149666027" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.822593575324" Y="3.881463462689" />
                  <Point X="2.714244084437" Y="-2.056067996125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.746850176832" Y="3.939535134791" />
                  <Point X="2.917090381125" Y="-2.134297495002" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.67110677834" Y="3.997606806893" />
                  <Point X="3.198487210394" Y="-2.296762127174" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.588489501987" Y="4.04830714746" />
                  <Point X="3.479884039663" Y="-2.459226759347" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.502923682237" Y="4.095845562349" />
                  <Point X="3.761280868932" Y="-2.621691391519" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.417357862488" Y="4.143383977238" />
                  <Point X="2.862183427446" Y="-1.51823090537" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.425334740794" Y="-2.122136752814" />
                  <Point X="4.009495043239" Y="-2.748571982794" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.331792042738" Y="4.190922392127" />
                  <Point X="2.867101080795" Y="-1.384207920313" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.881982065018" Y="-2.472534532193" />
                  <Point X="4.063907276285" Y="-2.66762543632" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.246226222989" Y="4.238460807015" />
                  <Point X="2.882703658948" Y="-1.261643114285" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.947671904947" Y="4.05759702074" />
                  <Point X="2.926746604807" Y="-1.169576868685" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.794570309056" Y="4.032712182488" />
                  <Point X="2.987566556099" Y="-1.09550175876" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.695387137665" Y="4.065647775562" />
                  <Point X="3.049629169269" Y="-1.02275924055" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.605075559175" Y="4.108096987273" />
                  <Point X="3.128790206586" Y="-0.968352537387" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.536709622192" Y="4.174080018257" />
                  <Point X="3.231661291408" Y="-0.93937174728" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.495007642469" Y="4.268656642691" />
                  <Point X="3.339666331831" Y="-0.915896450518" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.465151012339" Y="4.375935849389" />
                  <Point X="3.467088499659" Y="-0.913243473625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.474316756573" Y="4.525061429345" />
                  <Point X="3.615163492655" Y="-0.932737940215" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.400159118679" Y="4.584833621494" />
                  <Point X="3.763238485651" Y="-0.952232406806" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.297185065388" Y="4.613703991436" />
                  <Point X="3.911313478647" Y="-0.971726873396" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.194211012096" Y="4.642574361378" />
                  <Point X="4.059388471644" Y="-0.991221339987" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.091236958805" Y="4.67144473132" />
                  <Point X="3.355919708224" Y="-0.09754692698" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.724279357899" Y="-0.492564289327" />
                  <Point X="4.20746346464" Y="-1.010715806577" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.988262905514" Y="4.700315101262" />
                  <Point X="3.356994306541" Y="0.040597230045" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.897443356237" Y="-0.538963420212" />
                  <Point X="4.355538457636" Y="-1.030210273168" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.878987250798" Y="4.722427831012" />
                  <Point X="3.383355694884" Y="0.151624624668" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.070607369044" Y="-0.585362566613" />
                  <Point X="4.503613450632" Y="-1.049704739758" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.7618727204" Y="4.736134395761" />
                  <Point X="3.434776127465" Y="0.235779484349" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.243771381851" Y="-0.631761713013" />
                  <Point X="4.651688443628" Y="-1.069199206349" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.644758190003" Y="4.749840960509" />
                  <Point X="-0.24021402633" Y="4.316020457563" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.027723095971" Y="4.088151832682" />
                  <Point X="3.496451448438" Y="0.308937322592" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.416935394658" Y="-0.678160859414" />
                  <Point X="4.740212842149" Y="-1.02483347876" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.527643659605" Y="4.763547525257" />
                  <Point X="-0.292587360868" Y="4.511480505397" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.089680037231" Y="4.101548908813" />
                  <Point X="3.575478540313" Y="0.363487664656" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.590099407464" Y="-0.724560005814" />
                  <Point X="4.764542513638" Y="-0.911627334555" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.410529129207" Y="4.777254090005" />
                  <Point X="-0.344960695406" Y="4.706940553231" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.171956598525" Y="4.152614621549" />
                  <Point X="3.661674987747" Y="0.41034981415" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.763263420271" Y="-0.770959152215" />
                  <Point X="4.782620926389" Y="-0.79171753608" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.22653041629" Y="4.233387889626" />
                  <Point X="2.020666531495" Y="2.309412458156" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.320573307354" Y="1.9878018158" />
                  <Point X="3.0397244676" Y="1.216606613774" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.236078382774" Y="1.006042819051" />
                  <Point X="3.763454527247" Y="0.440501143304" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.258517579929" Y="4.338382378853" />
                  <Point X="2.018188670301" Y="2.451366161603" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.461020641726" Y="1.976487011649" />
                  <Point X="3.000893376461" Y="1.397544383523" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.412488899157" Y="0.956162223798" />
                  <Point X="3.86738250847" Y="0.468348550781" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.287510990294" Y="4.446587275417" />
                  <Point X="2.047731987643" Y="2.558981355131" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.567910447314" Y="2.001158251351" />
                  <Point X="3.022138132333" Y="1.51405869471" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.540611280998" Y="0.958064313094" />
                  <Point X="3.971310489692" Y="0.496195958258" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.316504400658" Y="4.554792171982" />
                  <Point X="2.094131466743" Y="2.648520528219" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.659994761716" Y="2.041706436538" />
                  <Point X="3.062157971685" Y="1.610439193844" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.656304039075" Y="0.973295541991" />
                  <Point X="4.075238470915" Y="0.524043365734" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.345497811022" Y="4.662997068546" />
                  <Point X="2.14380181731" Y="2.73455212109" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.744431302865" Y="2.090455854463" />
                  <Point X="3.119778522441" Y="1.687945240795" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.77199680032" Y="0.988526767492" />
                  <Point X="4.179166452137" Y="0.551890773211" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.374491221386" Y="4.77120196511" />
                  <Point X="2.193472167878" Y="2.820583713962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.82886782081" Y="2.139205297271" />
                  <Point X="3.187302163389" Y="1.754831523692" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.887689561564" Y="1.003757992993" />
                  <Point X="4.28309443336" Y="0.579738180687" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.50377957692" Y="4.771853700701" />
                  <Point X="2.243142518445" Y="2.906615306833" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.913304338756" Y="2.187954740079" />
                  <Point X="3.263019315648" Y="1.812931341432" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.003382322808" Y="1.018989218494" />
                  <Point X="4.387022414583" Y="0.607585588164" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.647734269867" Y="4.756777714959" />
                  <Point X="2.292812869012" Y="2.992646899705" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.997740856701" Y="2.236704182886" />
                  <Point X="3.338736467908" Y="1.871031159173" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.119075084053" Y="1.034220443994" />
                  <Point X="4.490950395805" Y="0.63543299564" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.791688962813" Y="4.741701729218" />
                  <Point X="2.342483219579" Y="3.078678492576" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.082177374647" Y="2.285453625694" />
                  <Point X="3.414453620167" Y="1.929130976913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.234767845297" Y="1.049451669495" />
                  <Point X="4.594878377028" Y="0.663280403117" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.953373167765" Y="4.707613169558" />
                  <Point X="2.392153570146" Y="3.164710085448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.166613892592" Y="2.334203068501" />
                  <Point X="3.490170772427" Y="1.987230794654" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.350460606542" Y="1.064682894996" />
                  <Point X="4.698806358251" Y="0.691127810594" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.121010836872" Y="4.667140301223" />
                  <Point X="2.441823920713" Y="3.250741678319" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.251050410538" Y="2.382952511309" />
                  <Point X="3.565887924686" Y="2.045330612394" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.466153367786" Y="1.079914120497" />
                  <Point X="4.779146306111" Y="0.744270286979" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.288648505978" Y="4.626667432888" />
                  <Point X="2.491494271281" Y="3.336773271191" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.335486928483" Y="2.431701954116" />
                  <Point X="3.641605076945" Y="2.103430430135" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.581846129031" Y="1.095145345998" />
                  <Point X="4.753111491816" Y="0.911485729835" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.456745741709" Y="4.585701739684" />
                  <Point X="2.541164621848" Y="3.422804864063" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.419923446429" Y="2.480451396924" />
                  <Point X="3.717322229205" Y="2.161530247875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.697538890275" Y="1.110376571499" />
                  <Point X="4.707545610209" Y="1.099645678152" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.653031491754" Y="4.514507565748" />
                  <Point X="2.590834972415" Y="3.508836456934" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.504359964374" Y="2.529200839731" />
                  <Point X="3.793039381464" Y="2.219630065616" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.849317241799" Y="4.443313391811" />
                  <Point X="2.640505322982" Y="3.594868049806" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.58879648232" Y="2.577950282539" />
                  <Point X="3.868756533724" Y="2.277729883357" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.078120949599" Y="4.337247977465" />
                  <Point X="2.690175673549" Y="3.680899642677" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.673233000266" Y="2.626699725346" />
                  <Point X="3.944473685983" Y="2.335829701097" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.322094934097" Y="4.214914433065" />
                  <Point X="2.739846024117" Y="3.766931235549" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.757669518211" Y="2.675449168154" />
                  <Point X="4.020190838243" Y="2.393929518838" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.606509021511" Y="4.049214187667" />
                  <Point X="2.789516374684" Y="3.85296282842" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.842106036157" Y="2.724198610961" />
                  <Point X="4.095907990502" Y="2.452029336578" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.926542554102" Y="2.772948053769" />
                  <Point X="4.012455179068" Y="2.680818042959" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000163818359" Y="0.001626220703" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.85812677002" Y="-4.979877441406" />
                  <Point X="0.4715390625" Y="-3.537112304688" />
                  <Point X="0.464318695068" Y="-3.521544189453" />
                  <Point X="0.30631048584" Y="-3.293884521484" />
                  <Point X="0.300591064453" Y="-3.285643798828" />
                  <Point X="0.274335357666" Y="-3.266399658203" />
                  <Point X="0.029826963425" Y="-3.190513183594" />
                  <Point X="0.020976379395" Y="-3.187766357422" />
                  <Point X="-0.008665608406" Y="-3.187766845703" />
                  <Point X="-0.25317414856" Y="-3.263653076172" />
                  <Point X="-0.262024719238" Y="-3.266400146484" />
                  <Point X="-0.288279083252" Y="-3.285643798828" />
                  <Point X="-0.44628729248" Y="-3.513303466797" />
                  <Point X="-0.459227081299" Y="-3.537112304688" />
                  <Point X="-0.473220703125" Y="-3.589336669922" />
                  <Point X="-0.847743896484" Y="-4.987077148438" />
                  <Point X="-1.091248535156" Y="-4.939812011719" />
                  <Point X="-1.100242919922" Y="-4.93806640625" />
                  <Point X="-1.351589477539" Y="-4.873396972656" />
                  <Point X="-1.309150268555" Y="-4.551038574219" />
                  <Point X="-1.309683227539" Y="-4.5347578125" />
                  <Point X="-1.368096313477" Y="-4.241096191406" />
                  <Point X="-1.370210571289" Y="-4.230466308594" />
                  <Point X="-1.386282714844" Y="-4.20262890625" />
                  <Point X="-1.611395141602" Y="-4.005209960938" />
                  <Point X="-1.619543579102" Y="-3.998063964844" />
                  <Point X="-1.649241210938" Y="-3.985762939453" />
                  <Point X="-1.94801550293" Y="-3.966180175781" />
                  <Point X="-1.958830444336" Y="-3.965471435547" />
                  <Point X="-1.989878662109" Y="-3.973791015625" />
                  <Point X="-2.238833251953" Y="-4.140137695312" />
                  <Point X="-2.247844970703" Y="-4.146159179688" />
                  <Point X="-2.267590332031" Y="-4.167532714844" />
                  <Point X="-2.457094970703" Y="-4.414500488281" />
                  <Point X="-2.842650390625" Y="-4.175773925781" />
                  <Point X="-2.855833984375" Y="-4.167611328125" />
                  <Point X="-3.228581054688" Y="-3.880608398438" />
                  <Point X="-3.224463378906" Y="-3.873476318359" />
                  <Point X="-2.506033691406" Y="-2.629119628906" />
                  <Point X="-2.499762695312" Y="-2.597593505859" />
                  <Point X="-2.513979248047" Y="-2.568764648438" />
                  <Point X="-2.530721679688" Y="-2.552021972656" />
                  <Point X="-2.531335449219" Y="-2.551408447266" />
                  <Point X="-2.560162109375" Y="-2.537197998047" />
                  <Point X="-2.59168359375" Y="-2.543469726562" />
                  <Point X="-2.636726074219" Y="-2.569475097656" />
                  <Point X="-3.842959472656" Y="-3.265894042969" />
                  <Point X="-4.151287109375" Y="-2.860814941406" />
                  <Point X="-4.161702636719" Y="-2.847130859375" />
                  <Point X="-4.431020019531" Y="-2.395527099609" />
                  <Point X="-4.420805664062" Y="-2.387689453125" />
                  <Point X="-3.163786865234" Y="-1.423144897461" />
                  <Point X="-3.145822021484" Y="-1.396013305664" />
                  <Point X="-3.138388183594" Y="-1.367311279297" />
                  <Point X="-3.138118896484" Y="-1.366272216797" />
                  <Point X="-3.14032421875" Y="-1.334600585938" />
                  <Point X="-3.161155029297" Y="-1.310641479492" />
                  <Point X="-3.186712646484" Y="-1.295599487305" />
                  <Point X="-3.187637695312" Y="-1.295054931641" />
                  <Point X="-3.219529296875" Y="-1.288571044922" />
                  <Point X="-3.276391357422" Y="-1.296057250977" />
                  <Point X="-4.803283691406" Y="-1.497076293945" />
                  <Point X="-4.923318847656" Y="-1.027142456055" />
                  <Point X="-4.927391601562" Y="-1.011197753906" />
                  <Point X="-4.998395996094" Y="-0.51474206543" />
                  <Point X="-4.989397460938" Y="-0.512330932617" />
                  <Point X="-3.557463867188" Y="-0.128645431519" />
                  <Point X="-3.541895507813" Y="-0.121424766541" />
                  <Point X="-3.515112060547" Y="-0.102835700989" />
                  <Point X="-3.514142578125" Y="-0.102162734985" />
                  <Point X="-3.494898925781" Y="-0.075907920837" />
                  <Point X="-3.485970947266" Y="-0.047142276764" />
                  <Point X="-3.485645751953" Y="-0.046093738556" />
                  <Point X="-3.485648925781" Y="-0.016456008911" />
                  <Point X="-3.494576660156" Y="0.012309638977" />
                  <Point X="-3.494899902344" Y="0.013350874901" />
                  <Point X="-3.514142578125" Y="0.039602649689" />
                  <Point X="-3.540926025391" Y="0.058191867828" />
                  <Point X="-3.557463867188" Y="0.066085334778" />
                  <Point X="-3.609296386719" Y="0.079973701477" />
                  <Point X="-4.998186523438" Y="0.452125762939" />
                  <Point X="-4.920270019531" Y="0.978679504395" />
                  <Point X="-4.91764453125" Y="0.996421264648" />
                  <Point X="-4.773516601562" Y="1.528298828125" />
                  <Point X="-3.753266357422" Y="1.393980224609" />
                  <Point X="-3.731705566406" Y="1.395866333008" />
                  <Point X="-3.672425292969" Y="1.414557373047" />
                  <Point X="-3.670279541016" Y="1.415233886719" />
                  <Point X="-3.651535400391" Y="1.426055175781" />
                  <Point X="-3.639120849609" Y="1.443784057617" />
                  <Point X="-3.615334472656" Y="1.501209716797" />
                  <Point X="-3.614473388672" Y="1.503288330078" />
                  <Point X="-3.610714111328" Y="1.524603393555" />
                  <Point X="-3.616315185547" Y="1.545510131836" />
                  <Point X="-3.645016113281" Y="1.600644165039" />
                  <Point X="-3.646054931641" Y="1.602639892578" />
                  <Point X="-3.659968261719" Y="1.619221679688" />
                  <Point X="-3.689699462891" Y="1.642035400391" />
                  <Point X="-4.476105957031" Y="2.245466308594" />
                  <Point X="-4.170213867188" Y="2.769532470703" />
                  <Point X="-4.160014160156" Y="2.787007080078" />
                  <Point X="-3.774671386719" Y="3.282310791016" />
                  <Point X="-3.159156982422" Y="2.926943359375" />
                  <Point X="-3.138514892578" Y="2.920434814453" />
                  <Point X="-3.055954101562" Y="2.913211669922" />
                  <Point X="-3.052965576172" Y="2.912950195312" />
                  <Point X="-3.031508056641" Y="2.915774902344" />
                  <Point X="-3.013254394531" Y="2.927402587891" />
                  <Point X="-2.954652099609" Y="2.986004882812" />
                  <Point X="-2.952530761719" Y="2.988125976562" />
                  <Point X="-2.940899902344" Y="3.006381103516" />
                  <Point X="-2.93807421875" Y="3.027841308594" />
                  <Point X="-2.945297363281" Y="3.11040234375" />
                  <Point X="-2.945558837891" Y="3.113390869141" />
                  <Point X="-2.952067382812" Y="3.134032958984" />
                  <Point X="-2.9652421875" Y="3.156852294922" />
                  <Point X="-3.307278564453" Y="3.749276855469" />
                  <Point X="-2.770637939453" Y="4.160713378906" />
                  <Point X="-2.752874267578" Y="4.174333007812" />
                  <Point X="-2.141548339844" Y="4.513971679688" />
                  <Point X="-1.967826904297" Y="4.287573242188" />
                  <Point X="-1.951246582031" Y="4.273660644531" />
                  <Point X="-1.859356811523" Y="4.225825683594" />
                  <Point X="-1.85603527832" Y="4.224096191406" />
                  <Point X="-1.835126464844" Y="4.2184921875" />
                  <Point X="-1.81380871582" Y="4.222250488281" />
                  <Point X="-1.718099243164" Y="4.261895019531" />
                  <Point X="-1.714634765625" Y="4.263330078125" />
                  <Point X="-1.696905761719" Y="4.275744140625" />
                  <Point X="-1.686083740234" Y="4.294487304688" />
                  <Point X="-1.654932128906" Y="4.393287597656" />
                  <Point X="-1.653804321289" Y="4.396863769531" />
                  <Point X="-1.651917724609" Y="4.418426757812" />
                  <Point X="-1.653415527344" Y="4.429804199219" />
                  <Point X="-1.689137573242" Y="4.701141113281" />
                  <Point X="-0.991088439941" Y="4.896849609375" />
                  <Point X="-0.968093811035" Y="4.903296386719" />
                  <Point X="-0.22419972229" Y="4.990357910156" />
                  <Point X="-0.042140319824" Y="4.310903320312" />
                  <Point X="-0.024282100677" Y="4.284176757813" />
                  <Point X="0.006155994415" Y="4.273844238281" />
                  <Point X="0.036594047546" Y="4.284176757813" />
                  <Point X="0.054452308655" Y="4.310903320312" />
                  <Point X="0.061202926636" Y="4.336097167969" />
                  <Point X="0.236648422241" Y="4.990868652344" />
                  <Point X="0.840125366211" Y="4.927668457031" />
                  <Point X="0.860204833984" Y="4.925565429688" />
                  <Point X="1.488306152344" Y="4.773922851562" />
                  <Point X="1.508460571289" Y="4.769056640625" />
                  <Point X="1.918211791992" Y="4.620437011719" />
                  <Point X="1.931032836914" Y="4.615786621094" />
                  <Point X="2.326354003906" Y="4.430908203125" />
                  <Point X="2.338694335938" Y="4.42513671875" />
                  <Point X="2.72058984375" Y="4.202644042969" />
                  <Point X="2.732523925781" Y="4.19569140625" />
                  <Point X="3.068740234375" Y="3.956592529297" />
                  <Point X="3.061382080078" Y="3.94384765625" />
                  <Point X="2.229853515625" Y="2.503597900391" />
                  <Point X="2.224852050781" Y="2.491514648438" />
                  <Point X="2.204132324219" Y="2.414032958984" />
                  <Point X="2.202044189453" Y="2.392330078125" />
                  <Point X="2.210123291016" Y="2.325330566406" />
                  <Point X="2.210415527344" Y="2.322905273438" />
                  <Point X="2.21868359375" Y="2.300810058594" />
                  <Point X="2.260140625" Y="2.239712890625" />
                  <Point X="2.274942871094" Y="2.224201904297" />
                  <Point X="2.336039794922" Y="2.182744873047" />
                  <Point X="2.338251464844" Y="2.181244384766" />
                  <Point X="2.360335693359" Y="2.172980224609" />
                  <Point X="2.427335449219" Y="2.164901123047" />
                  <Point X="2.448664306641" Y="2.165946289062" />
                  <Point X="2.526145996094" Y="2.186666015625" />
                  <Point X="2.541033935547" Y="2.192417480469" />
                  <Point X="2.593167480469" Y="2.222516845703" />
                  <Point X="3.994248046875" Y="3.031430908203" />
                  <Point X="4.195512695312" Y="2.751718994141" />
                  <Point X="4.202589355469" Y="2.741883300781" />
                  <Point X="4.387513183594" Y="2.436295898438" />
                  <Point X="4.383401367188" Y="2.433140869141" />
                  <Point X="3.288616210938" Y="1.593082885742" />
                  <Point X="3.279371582031" Y="1.583833740234" />
                  <Point X="3.223607910156" Y="1.51108581543" />
                  <Point X="3.213119873047" Y="1.491500976562" />
                  <Point X="3.192347900391" Y="1.417225097656" />
                  <Point X="3.191595947266" Y="1.414536499023" />
                  <Point X="3.190779541016" Y="1.390965698242" />
                  <Point X="3.207831298828" Y="1.30832434082" />
                  <Point X="3.215647460938" Y="1.287953613281" />
                  <Point X="3.262026123047" Y="1.217459960938" />
                  <Point X="3.263705078125" Y="1.214908203125" />
                  <Point X="3.28094921875" Y="1.198819091797" />
                  <Point X="3.348158447266" Y="1.160986206055" />
                  <Point X="3.368565917969" Y="1.153619628906" />
                  <Point X="3.459437255859" Y="1.141609741211" />
                  <Point X="3.475803710938" Y="1.14117175293" />
                  <Point X="3.525327148438" Y="1.147691650391" />
                  <Point X="4.848975097656" Y="1.321953125" />
                  <Point X="4.936147949219" Y="0.963872497559" />
                  <Point X="4.939189941406" Y="0.951377929688" />
                  <Point X="4.997858886719" Y="0.574556152344" />
                  <Point X="4.995814941406" Y="0.574008483887" />
                  <Point X="3.741167724609" Y="0.237826904297" />
                  <Point X="3.729087158203" Y="0.232819549561" />
                  <Point X="3.639808837891" Y="0.181215011597" />
                  <Point X="3.622264648438" Y="0.166926422119" />
                  <Point X="3.568697753906" Y="0.098669403076" />
                  <Point X="3.566758789062" Y="0.096198730469" />
                  <Point X="3.556985595703" Y="0.074737251282" />
                  <Point X="3.539129882812" Y="-0.018498321533" />
                  <Point X="3.538483398438" Y="-0.040686763763" />
                  <Point X="3.556339111328" Y="-0.133922485352" />
                  <Point X="3.556985595703" Y="-0.137297348022" />
                  <Point X="3.566758789062" Y="-0.158758666992" />
                  <Point X="3.620325683594" Y="-0.227015533447" />
                  <Point X="3.636577148438" Y="-0.241907104492" />
                  <Point X="3.72585546875" Y="-0.293511505127" />
                  <Point X="3.741167724609" Y="-0.300386993408" />
                  <Point X="3.786583007812" Y="-0.312555999756" />
                  <Point X="4.998068359375" Y="-0.637172424316" />
                  <Point X="4.950129882812" Y="-0.955139038086" />
                  <Point X="4.948432128906" Y="-0.966399658203" />
                  <Point X="4.874545898438" Y="-1.290178344727" />
                  <Point X="4.867020507812" Y="-1.28918762207" />
                  <Point X="3.411982177734" Y="-1.097628173828" />
                  <Point X="3.394836425781" Y="-1.098341186523" />
                  <Point X="3.219614746094" Y="-1.136426391602" />
                  <Point X="3.213272216797" Y="-1.137804931641" />
                  <Point X="3.185445556641" Y="-1.154696899414" />
                  <Point X="3.07953515625" Y="-1.28207421875" />
                  <Point X="3.075701416016" Y="-1.286685058594" />
                  <Point X="3.064357910156" Y="-1.314069702148" />
                  <Point X="3.049178466797" Y="-1.479028930664" />
                  <Point X="3.04862890625" Y="-1.48499987793" />
                  <Point X="3.056360107422" Y="-1.516621459961" />
                  <Point X="3.153330322266" Y="-1.667452270508" />
                  <Point X="3.168460693359" Y="-1.685540771484" />
                  <Point X="3.210606445312" Y="-1.717880249023" />
                  <Point X="4.33907421875" Y="-2.583784179688" />
                  <Point X="4.208916503906" Y="-2.794399169922" />
                  <Point X="4.204130859375" Y="-2.802142822266" />
                  <Point X="4.056688232422" Y="-3.011638427734" />
                  <Point X="4.049362304688" Y="-3.007408691406" />
                  <Point X="2.753454589844" Y="-2.259215576172" />
                  <Point X="2.737340820313" Y="-2.253312744141" />
                  <Point X="2.528799072266" Y="-2.215650390625" />
                  <Point X="2.521250488281" Y="-2.214287109375" />
                  <Point X="2.489076904297" Y="-2.219245361328" />
                  <Point X="2.315829833984" Y="-2.310423828125" />
                  <Point X="2.309558837891" Y="-2.313724365234" />
                  <Point X="2.288599121094" Y="-2.334684814453" />
                  <Point X="2.197420410156" Y="-2.507931640625" />
                  <Point X="2.194120117188" Y="-2.514202880859" />
                  <Point X="2.189163085938" Y="-2.546374511719" />
                  <Point X="2.226825439453" Y="-2.754916259766" />
                  <Point X="2.234091552734" Y="-2.778578613281" />
                  <Point X="2.261174316406" Y="-2.825487304688" />
                  <Point X="2.986673583984" Y="-4.082088623047" />
                  <Point X="2.840979736328" Y="-4.186154785156" />
                  <Point X="2.835300292969" Y="-4.190211425781" />
                  <Point X="2.679775878906" Y="-4.29087890625" />
                  <Point X="2.672074707031" Y="-4.280842773438" />
                  <Point X="1.683177368164" Y="-2.992087402344" />
                  <Point X="1.670550048828" Y="-2.980468017578" />
                  <Point X="1.464871582031" Y="-2.848235839844" />
                  <Point X="1.457426635742" Y="-2.84344921875" />
                  <Point X="1.42580480957" Y="-2.835717041016" />
                  <Point X="1.200860473633" Y="-2.856416748047" />
                  <Point X="1.192718017578" Y="-2.857166015625" />
                  <Point X="1.165333740234" Y="-2.868508544922" />
                  <Point X="0.991637451172" Y="-3.012931396484" />
                  <Point X="0.985350036621" Y="-3.018158935547" />
                  <Point X="0.968456726074" Y="-3.045986572266" />
                  <Point X="0.916522399902" Y="-3.284925292969" />
                  <Point X="0.913929443359" Y="-3.310720214844" />
                  <Point X="0.921604614258" Y="-3.369017822266" />
                  <Point X="1.127642211914" Y="-4.934028320312" />
                  <Point X="0.999717712402" Y="-4.962069824219" />
                  <Point X="0.994349975586" Y="-4.96324609375" />
                  <Point X="0.860200439453" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#214" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.186493938389" Y="5.051129362463" Z="2.45" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="2.45" />
                  <Point X="-0.220866811266" Y="5.073090251485" Z="2.45" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="2.45" />
                  <Point X="-1.010742330347" Y="4.976273180148" Z="2.45" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="2.45" />
                  <Point X="-1.709143720482" Y="4.454557701065" Z="2.45" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="2.45" />
                  <Point X="-1.708773506062" Y="4.439604238381" Z="2.45" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="2.45" />
                  <Point X="-1.743389593664" Y="4.339369066807" Z="2.45" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="2.45" />
                  <Point X="-1.842425240881" Y="4.301456096782" Z="2.45" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="2.45" />
                  <Point X="-2.127304019744" Y="4.600799361499" Z="2.45" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="2.45" />
                  <Point X="-2.157074521177" Y="4.59724461012" Z="2.45" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="2.45" />
                  <Point X="-2.80721617181" Y="4.231673251636" Z="2.45" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="2.45" />
                  <Point X="-3.014699499519" Y="3.163132353748" Z="2.45" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="2.45" />
                  <Point X="-3.001263224259" Y="3.137324409654" Z="2.45" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="2.45" />
                  <Point X="-2.996161271317" Y="3.05264229657" Z="2.45" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="2.45" />
                  <Point X="-3.057752055953" Y="2.994301474914" Z="2.45" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="2.45" />
                  <Point X="-3.77072699455" Y="3.365494449226" Z="2.45" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="2.45" />
                  <Point X="-3.808013230579" Y="3.360074237936" Z="2.45" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="2.45" />
                  <Point X="-4.219551951918" Y="2.826017456199" Z="2.45" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="2.45" />
                  <Point X="-3.726293530182" Y="1.633647701974" Z="2.45" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="2.45" />
                  <Point X="-3.6955233861" Y="1.608838405251" Z="2.45" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="2.45" />
                  <Point X="-3.667683445081" Y="1.551625714951" Z="2.45" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="2.45" />
                  <Point X="-3.693615711366" Y="1.493523541824" Z="2.45" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="2.45" />
                  <Point X="-4.779341102737" Y="1.609966627755" Z="2.45" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="2.45" />
                  <Point X="-4.821957150459" Y="1.59470444553" Z="2.45" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="2.45" />
                  <Point X="-4.975887284906" Y="1.017278921801" Z="2.45" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="2.45" />
                  <Point X="-3.628393228122" Y="0.062957506772" Z="2.45" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="2.45" />
                  <Point X="-3.575591242911" Y="0.048396148711" Z="2.45" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="2.45" />
                  <Point X="-3.548484689604" Y="0.028765703159" Z="2.45" />
                  <Point X="-3.539556741714" Y="0" Z="2.45" />
                  <Point X="-3.539879911531" Y="-0.001041247902" Z="2.45" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="2.45" />
                  <Point X="-3.549777352213" Y="-0.030479834423" Z="2.45" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="2.45" />
                  <Point X="-5.008494560651" Y="-0.432754549984" Z="2.45" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="2.45" />
                  <Point X="-5.057613965754" Y="-0.465612645031" Z="2.45" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="2.45" />
                  <Point X="-4.977919268251" Y="-1.008237350879" Z="2.45" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="2.45" />
                  <Point X="-3.276020976027" Y="-1.314349342442" Z="2.45" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="2.45" />
                  <Point X="-3.21823377033" Y="-1.307407791875" Z="2.45" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="2.45" />
                  <Point X="-3.192945447377" Y="-1.323488969798" Z="2.45" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="2.45" />
                  <Point X="-4.457399602884" Y="-2.316741579739" Z="2.45" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="2.45" />
                  <Point X="-4.492646151143" Y="-2.36885086783" Z="2.45" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="2.45" />
                  <Point X="-4.197229054901" Y="-2.8598179251" Z="2.45" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="2.45" />
                  <Point X="-2.617882159948" Y="-2.581496362951" Z="2.45" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="2.45" />
                  <Point X="-2.572233461416" Y="-2.556097007715" Z="2.45" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="2.45" />
                  <Point X="-3.273920650217" Y="-3.817196222544" Z="2.45" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="2.45" />
                  <Point X="-3.285622684072" Y="-3.873251993604" Z="2.45" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="2.45" />
                  <Point X="-2.875127402235" Y="-4.187005323472" Z="2.45" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="2.45" />
                  <Point X="-2.234078563076" Y="-4.166690687133" Z="2.45" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="2.45" />
                  <Point X="-2.217210726085" Y="-4.150430844488" Z="2.45" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="2.45" />
                  <Point X="-1.957441101558" Y="-3.984793214201" Z="2.45" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="2.45" />
                  <Point X="-1.650518334014" Y="-4.011522000306" Z="2.45" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="2.45" />
                  <Point X="-1.423291570547" Y="-4.219570345972" Z="2.45" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="2.45" />
                  <Point X="-1.411414560544" Y="-4.866708283966" Z="2.45" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="2.45" />
                  <Point X="-1.402769443136" Y="-4.882160968309" Z="2.45" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="2.45" />
                  <Point X="-1.106931222036" Y="-4.957615955897" Z="2.45" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="2.45" />
                  <Point X="-0.431080309218" Y="-3.570997644404" Z="2.45" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="2.45" />
                  <Point X="-0.411367271071" Y="-3.510532356873" Z="2.45" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="2.45" />
                  <Point X="-0.244508472357" Y="-3.280125775053" Z="2.45" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="2.45" />
                  <Point X="0.008850607003" Y="-3.206986270235" Z="2.45" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="2.45" />
                  <Point X="0.259078588361" Y="-3.291113398895" Z="2.45" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="2.45" />
                  <Point X="0.803674551183" Y="-4.961538385412" Z="2.45" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="2.45" />
                  <Point X="0.823967980316" Y="-5.012618488993" Z="2.45" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="2.45" />
                  <Point X="1.00426467133" Y="-4.9796303915" Z="2.45" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="2.45" />
                  <Point X="0.9650208274" Y="-3.331211432098" Z="2.45" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="2.45" />
                  <Point X="0.959225679323" Y="-3.264264699969" Z="2.45" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="2.45" />
                  <Point X="1.017447398025" Y="-3.020098261636" Z="2.45" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="2.45" />
                  <Point X="1.199286182159" Y="-2.874926165926" Z="2.45" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="2.45" />
                  <Point X="1.431675509524" Y="-2.859013070496" Z="2.45" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="2.45" />
                  <Point X="2.626251020474" Y="-4.280000829135" Z="2.45" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="2.45" />
                  <Point X="2.668866543488" Y="-4.32223624741" Z="2.45" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="2.45" />
                  <Point X="2.863884276177" Y="-4.195562136892" Z="2.45" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="2.45" />
                  <Point X="2.298319646506" Y="-2.769207301493" Z="2.45" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="2.45" />
                  <Point X="2.269873606618" Y="-2.714749922581" Z="2.45" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="2.45" />
                  <Point X="2.235511556145" Y="-2.499937069511" Z="2.45" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="2.45" />
                  <Point X="2.332961308861" Y="-2.323389753822" Z="2.45" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="2.45" />
                  <Point X="2.513756880931" Y="-2.233574402873" Z="2.45" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="2.45" />
                  <Point X="4.018206295614" Y="-3.019429853769" Z="2.45" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="2.45" />
                  <Point X="4.071214540824" Y="-3.037845960853" Z="2.45" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="2.45" />
                  <Point X="4.245293595585" Y="-2.789404440447" Z="2.45" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="2.45" />
                  <Point X="3.234888359611" Y="-1.646932439825" Z="2.45" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="2.45" />
                  <Point X="3.189232739938" Y="-1.609133323086" Z="2.45" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="2.45" />
                  <Point X="3.0928119668" Y="-1.452331381231" Z="2.45" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="2.45" />
                  <Point X="3.111825152839" Y="-1.282761485542" Z="2.45" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="2.45" />
                  <Point X="3.224078125959" Y="-1.15400559323" Z="2.45" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="2.45" />
                  <Point X="4.854337931107" Y="-1.307479806147" Z="2.45" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="2.45" />
                  <Point X="4.909956196099" Y="-1.301488869315" Z="2.45" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="2.45" />
                  <Point X="4.993414786882" Y="-0.931313750835" Z="2.45" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="2.45" />
                  <Point X="3.793367853406" Y="-0.232979590089" Z="2.45" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="2.45" />
                  <Point X="3.744721043141" Y="-0.218942681483" Z="2.45" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="2.45" />
                  <Point X="3.653503720779" Y="-0.164867473465" Z="2.45" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="2.45" />
                  <Point X="3.599290392405" Y="-0.093235630079" Z="2.45" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="2.45" />
                  <Point X="3.582081058019" Y="0.003374901096" Z="2.45" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="2.45" />
                  <Point X="3.601875717621" Y="0.09908126511" Z="2.45" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="2.45" />
                  <Point X="3.658674371212" Y="0.169206162759" Z="2.45" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="2.45" />
                  <Point X="5.002599963901" Y="0.55699234268" Z="2.45" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="2.45" />
                  <Point X="5.04571295122" Y="0.583947735033" Z="2.45" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="2.45" />
                  <Point X="4.978573208071" Y="1.006980507907" Z="2.45" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="2.45" />
                  <Point X="3.512644752408" Y="1.22854395631" Z="2.45" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="2.45" />
                  <Point X="3.459832089817" Y="1.222458807423" Z="2.45" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="2.45" />
                  <Point X="3.366527808721" Y="1.235838040748" Z="2.45" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="2.45" />
                  <Point X="3.297639754699" Y="1.27622272913" Z="2.45" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="2.45" />
                  <Point X="3.250643765002" Y="1.349707819428" Z="2.45" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="2.45" />
                  <Point X="3.234343930832" Y="1.43503781015" Z="2.45" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="2.45" />
                  <Point X="3.257134493783" Y="1.511947005217" Z="2.45" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="2.45" />
                  <Point X="4.407683339794" Y="2.424753287114" Z="2.45" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="2.45" />
                  <Point X="4.440006401379" Y="2.467233710454" Z="2.45" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="2.45" />
                  <Point X="4.229942470277" Y="2.812201216807" Z="2.45" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="2.45" />
                  <Point X="2.562011428072" Y="2.297098060968" Z="2.45" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="2.45" />
                  <Point X="2.507073275549" Y="2.266248769348" Z="2.45" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="2.45" />
                  <Point X="2.427166432916" Y="2.245821611756" Z="2.45" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="2.45" />
                  <Point X="2.357955221162" Y="2.255401166272" Z="2.45" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="2.45" />
                  <Point X="2.295357433963" Y="2.29906963922" Z="2.45" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="2.45" />
                  <Point X="2.253608091003" Y="2.362592010916" Z="2.45" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="2.45" />
                  <Point X="2.246279173309" Y="2.432396300333" Z="2.45" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="2.45" />
                  <Point X="3.098527291892" Y="3.950127653883" Z="2.45" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="2.45" />
                  <Point X="3.115522191372" Y="4.011580282588" Z="2.45" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="2.45" />
                  <Point X="2.739603735642" Y="4.277126738268" Z="2.45" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="2.45" />
                  <Point X="2.34137968105" Y="4.50747986903" Z="2.45" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="2.45" />
                  <Point X="1.929104565101" Y="4.698720460821" Z="2.45" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="2.45" />
                  <Point X="1.493885111295" Y="4.853806303406" Z="2.45" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="2.45" />
                  <Point X="0.839177475654" Y="5.008676604885" Z="2.45" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="2.45" />
                  <Point X="0.006750623164" Y="4.380318143488" Z="2.45" />
                  <Point X="0" Y="4.355124473572" Z="2.45" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>