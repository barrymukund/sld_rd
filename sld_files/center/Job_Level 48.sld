<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#211" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="3419" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="0" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="0.004715820312" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.563302062988" Y="-3.512524658203" />
                  <Point X="0.557721191406" Y="-3.497141845703" />
                  <Point X="0.542363037109" Y="-3.467377197266" />
                  <Point X="0.39080090332" Y="-3.249004882812" />
                  <Point X="0.378635528564" Y="-3.231476806641" />
                  <Point X="0.356751739502" Y="-3.209021484375" />
                  <Point X="0.33049609375" Y="-3.18977734375" />
                  <Point X="0.302494873047" Y="-3.175668945312" />
                  <Point X="0.067961082458" Y="-3.102878173828" />
                  <Point X="0.049135734558" Y="-3.097035644531" />
                  <Point X="0.020975576401" Y="-3.092766357422" />
                  <Point X="-0.008665750504" Y="-3.092766601562" />
                  <Point X="-0.036824390411" Y="-3.097035888672" />
                  <Point X="-0.271358032227" Y="-3.169826416016" />
                  <Point X="-0.290183532715" Y="-3.175669189453" />
                  <Point X="-0.318185211182" Y="-3.189777832031" />
                  <Point X="-0.34444039917" Y="-3.209021972656" />
                  <Point X="-0.366323730469" Y="-3.231477050781" />
                  <Point X="-0.517885864258" Y="-3.449849121094" />
                  <Point X="-0.530051391602" Y="-3.467377441406" />
                  <Point X="-0.538189147949" Y="-3.48157421875" />
                  <Point X="-0.550990112305" Y="-3.512524414063" />
                  <Point X="-0.580754516602" Y="-3.623606689453" />
                  <Point X="-0.916584594727" Y="-4.876941894531" />
                  <Point X="-1.061345581055" Y="-4.848843261719" />
                  <Point X="-1.079338867188" Y="-4.845350585938" />
                  <Point X="-1.24641784668" Y="-4.802362792969" />
                  <Point X="-1.214963012695" Y="-4.563438476562" />
                  <Point X="-1.214201171875" Y="-4.5479296875" />
                  <Point X="-1.216508666992" Y="-4.516224121094" />
                  <Point X="-1.272538818359" Y="-4.234542480469" />
                  <Point X="-1.277036132812" Y="-4.211932617188" />
                  <Point X="-1.287938354492" Y="-4.182966308594" />
                  <Point X="-1.304010620117" Y="-4.155128417969" />
                  <Point X="-1.323645019531" Y="-4.131203613281" />
                  <Point X="-1.539573730469" Y="-3.941838867188" />
                  <Point X="-1.556905761719" Y="-3.926639160156" />
                  <Point X="-1.583188720703" Y="-3.910295410156" />
                  <Point X="-1.612885864258" Y="-3.897994384766" />
                  <Point X="-1.643027587891" Y="-3.890966308594" />
                  <Point X="-1.92961340332" Y="-3.872182373047" />
                  <Point X="-1.952616821289" Y="-3.870674804688" />
                  <Point X="-1.983419189453" Y="-3.873708740234" />
                  <Point X="-2.014467651367" Y="-3.882028564453" />
                  <Point X="-2.042657836914" Y="-3.894801757812" />
                  <Point X="-2.281456787109" Y="-4.054362060547" />
                  <Point X="-2.300624511719" Y="-4.067169677734" />
                  <Point X="-2.312786621094" Y="-4.076822265625" />
                  <Point X="-2.335102539062" Y="-4.099461914062" />
                  <Point X="-2.351812988281" Y="-4.121239257813" />
                  <Point X="-2.480148925781" Y="-4.288490234375" />
                  <Point X="-2.775341064453" Y="-4.105714355469" />
                  <Point X="-2.801707763672" Y="-4.089388671875" />
                  <Point X="-3.104722167969" Y="-3.856077880859" />
                  <Point X="-2.423761230469" Y="-2.676619384766" />
                  <Point X="-2.412859130859" Y="-2.647653320312" />
                  <Point X="-2.406588134766" Y="-2.616126953125" />
                  <Point X="-2.405575439453" Y="-2.585192871094" />
                  <Point X="-2.414560058594" Y="-2.555575195312" />
                  <Point X="-2.428777099609" Y="-2.526746337891" />
                  <Point X="-2.4468046875" Y="-2.501589355469" />
                  <Point X="-2.462864257812" Y="-2.485529785156" />
                  <Point X="-2.464153320312" Y="-2.484240722656" />
                  <Point X="-2.489310302734" Y="-2.466213134766" />
                  <Point X="-2.518139160156" Y="-2.45199609375" />
                  <Point X="-2.547756835938" Y="-2.443011474609" />
                  <Point X="-2.578690917969" Y="-2.444024169922" />
                  <Point X="-2.610217285156" Y="-2.450295166016" />
                  <Point X="-2.63918359375" Y="-2.461197265625" />
                  <Point X="-2.734989990234" Y="-2.516511230469" />
                  <Point X="-3.818024169922" Y="-3.141801269531" />
                  <Point X="-4.062027832031" Y="-2.821230957031" />
                  <Point X="-4.082857666016" Y="-2.793864746094" />
                  <Point X="-4.306142089844" Y="-2.419449951172" />
                  <Point X="-3.105954345703" Y="-1.498513427734" />
                  <Point X="-3.084577392578" Y="-1.47559375" />
                  <Point X="-3.066612792969" Y="-1.448463012695" />
                  <Point X="-3.053856689453" Y="-1.419833374023" />
                  <Point X="-3.046724609375" Y="-1.392296386719" />
                  <Point X="-3.046152099609" Y="-1.39008605957" />
                  <Point X="-3.04334765625" Y="-1.359659057617" />
                  <Point X="-3.045555908203" Y="-1.327988647461" />
                  <Point X="-3.052556640625" Y="-1.298243164062" />
                  <Point X="-3.068638671875" Y="-1.272259155273" />
                  <Point X="-3.089470703125" Y="-1.248302490234" />
                  <Point X="-3.112971923828" Y="-1.228767211914" />
                  <Point X="-3.137486816406" Y="-1.214338989258" />
                  <Point X="-3.139454589844" Y="-1.213180908203" />
                  <Point X="-3.168716064453" Y="-1.201957275391" />
                  <Point X="-3.200604736328" Y="-1.195475097656" />
                  <Point X="-3.231929443359" Y="-1.194383789062" />
                  <Point X="-3.352875976563" Y="-1.210306884766" />
                  <Point X="-4.732102539062" Y="-1.391885375977" />
                  <Point X="-4.825929199219" Y="-1.024556640625" />
                  <Point X="-4.834076171875" Y="-0.992661804199" />
                  <Point X="-4.892424316406" Y="-0.584698242188" />
                  <Point X="-3.532876220703" Y="-0.220408447266" />
                  <Point X="-3.517494384766" Y="-0.214828018188" />
                  <Point X="-3.487729492188" Y="-0.199470016479" />
                  <Point X="-3.462038818359" Y="-0.181639038086" />
                  <Point X="-3.4599765625" Y="-0.180207962036" />
                  <Point X="-3.437521240234" Y="-0.158323730469" />
                  <Point X="-3.418275390625" Y="-0.132065200806" />
                  <Point X="-3.404167724609" Y="-0.104065193176" />
                  <Point X="-3.395604003906" Y="-0.076472877502" />
                  <Point X="-3.394916748047" Y="-0.074258262634" />
                  <Point X="-3.390647216797" Y="-0.046098403931" />
                  <Point X="-3.390647216797" Y="-0.016461631775" />
                  <Point X="-3.394916748047" Y="0.011698069572" />
                  <Point X="-3.40348046875" Y="0.039290378571" />
                  <Point X="-3.404167724609" Y="0.041505001068" />
                  <Point X="-3.418275390625" Y="0.069505172729" />
                  <Point X="-3.437521240234" Y="0.095763542175" />
                  <Point X="-3.459976806641" Y="0.117648078918" />
                  <Point X="-3.485667480469" Y="0.135478744507" />
                  <Point X="-3.497984130859" Y="0.142719451904" />
                  <Point X="-3.532875976562" Y="0.157848251343" />
                  <Point X="-3.643124511719" Y="0.187389251709" />
                  <Point X="-4.89181640625" Y="0.521975280762" />
                  <Point X="-4.82973828125" Y="0.94149432373" />
                  <Point X="-4.824487304688" Y="0.97697857666" />
                  <Point X="-4.703551757812" Y="1.423267944336" />
                  <Point X="-3.765666015625" Y="1.29979284668" />
                  <Point X="-3.744986083984" Y="1.299341674805" />
                  <Point X="-3.723424560547" Y="1.301228149414" />
                  <Point X="-3.703137695312" Y="1.305263427734" />
                  <Point X="-3.646275878906" Y="1.323191894531" />
                  <Point X="-3.641711669922" Y="1.324630981445" />
                  <Point X="-3.622778320312" Y="1.332962036133" />
                  <Point X="-3.604033691406" Y="1.343784423828" />
                  <Point X="-3.587352539062" Y="1.35601574707" />
                  <Point X="-3.573714111328" Y="1.371567749023" />
                  <Point X="-3.561299560547" Y="1.389297851562" />
                  <Point X="-3.551351074219" Y="1.407431884766" />
                  <Point X="-3.528534912109" Y="1.462514648438" />
                  <Point X="-3.526703613281" Y="1.466936157227" />
                  <Point X="-3.520915283203" Y="1.486795776367" />
                  <Point X="-3.517157226562" Y="1.508110351562" />
                  <Point X="-3.5158046875" Y="1.528750366211" />
                  <Point X="-3.518951171875" Y="1.549193847656" />
                  <Point X="-3.524552978516" Y="1.570099731445" />
                  <Point X="-3.532049804688" Y="1.589377563477" />
                  <Point X="-3.559579833984" Y="1.642262573242" />
                  <Point X="-3.561789550781" Y="1.646507202148" />
                  <Point X="-3.573281005859" Y="1.663705688477" />
                  <Point X="-3.587193603516" Y="1.680286376953" />
                  <Point X="-3.602135986328" Y="1.694590332031" />
                  <Point X="-3.665374755859" Y="1.743115112305" />
                  <Point X="-4.351859863281" Y="2.269874023438" />
                  <Point X="-4.101551757813" Y="2.698711181641" />
                  <Point X="-4.08115234375" Y="2.733660888672" />
                  <Point X="-3.750505126953" Y="3.158661621094" />
                  <Point X="-3.206656982422" Y="2.844670898438" />
                  <Point X="-3.187724853516" Y="2.836340576172" />
                  <Point X="-3.167083007812" Y="2.82983203125" />
                  <Point X="-3.146794921875" Y="2.825796386719" />
                  <Point X="-3.067602050781" Y="2.818867919922" />
                  <Point X="-3.061245605469" Y="2.818311767578" />
                  <Point X="-3.04056640625" Y="2.818762939453" />
                  <Point X="-3.019107910156" Y="2.821587890625" />
                  <Point X="-2.999016601562" Y="2.82650390625" />
                  <Point X="-2.980465576172" Y="2.835651855469" />
                  <Point X="-2.962211425781" Y="2.847280517578" />
                  <Point X="-2.946078125" Y="2.860228759766" />
                  <Point X="-2.889866455078" Y="2.916440429688" />
                  <Point X="-2.885354492188" Y="2.920952148438" />
                  <Point X="-2.872408203125" Y="2.937082519531" />
                  <Point X="-2.860778320312" Y="2.955337158203" />
                  <Point X="-2.851629394531" Y="2.973888671875" />
                  <Point X="-2.846712646484" Y="2.993980712891" />
                  <Point X="-2.843887207031" Y="3.015440185547" />
                  <Point X="-2.843435791016" Y="3.03612109375" />
                  <Point X="-2.850364257812" Y="3.115313964844" />
                  <Point X="-2.850920410156" Y="3.121670654297" />
                  <Point X="-2.854955810547" Y="3.141958496094" />
                  <Point X="-2.861464355469" Y="3.162600585938" />
                  <Point X="-2.869795166016" Y="3.181533203125" />
                  <Point X="-2.897818359375" Y="3.2300703125" />
                  <Point X="-3.183333007812" Y="3.724596679688" />
                  <Point X="-2.7361484375" Y="4.067447998047" />
                  <Point X="-2.700621826172" Y="4.094686279297" />
                  <Point X="-2.167037109375" Y="4.391134277344" />
                  <Point X="-2.04319519043" Y="4.229740722656" />
                  <Point X="-2.028893066406" Y="4.214800292969" />
                  <Point X="-2.012314086914" Y="4.200888183594" />
                  <Point X="-1.995113891602" Y="4.189395019531" />
                  <Point X="-1.90697265625" Y="4.143511230469" />
                  <Point X="-1.899897705078" Y="4.139828125" />
                  <Point X="-1.880619750977" Y="4.132331542969" />
                  <Point X="-1.859713012695" Y="4.126729492187" />
                  <Point X="-1.839269287109" Y="4.123582519531" />
                  <Point X="-1.81862902832" Y="4.124935058594" />
                  <Point X="-1.797313598633" Y="4.128693359375" />
                  <Point X="-1.777453857422" Y="4.134481933594" />
                  <Point X="-1.685648803711" Y="4.172509277344" />
                  <Point X="-1.678279907227" Y="4.175561523437" />
                  <Point X="-1.660146484375" Y="4.185509765625" />
                  <Point X="-1.642416870117" Y="4.197923828125" />
                  <Point X="-1.626864990234" Y="4.2115625" />
                  <Point X="-1.614633666992" Y="4.228243652344" />
                  <Point X="-1.603811523438" Y="4.246987792969" />
                  <Point X="-1.59548046875" Y="4.265921386719" />
                  <Point X="-1.565599609375" Y="4.360690917969" />
                  <Point X="-1.563201171875" Y="4.368297851562" />
                  <Point X="-1.559165771484" Y="4.388584472656" />
                  <Point X="-1.557279174805" Y="4.410146484375" />
                  <Point X="-1.55773034668" Y="4.430827148438" />
                  <Point X="-1.560916381836" Y="4.455026855469" />
                  <Point X="-1.584201782227" Y="4.631897949219" />
                  <Point X="-0.995622009277" Y="4.796915039062" />
                  <Point X="-0.949634216309" Y="4.80980859375" />
                  <Point X="-0.294710998535" Y="4.886458007812" />
                  <Point X="-0.133903305054" Y="4.286315429688" />
                  <Point X="-0.121129844666" Y="4.258124023438" />
                  <Point X="-0.10327155304" Y="4.231397460938" />
                  <Point X="-0.082114532471" Y="4.20880859375" />
                  <Point X="-0.054819446564" Y="4.19421875" />
                  <Point X="-0.024381370544" Y="4.183886230469" />
                  <Point X="0.006155906677" Y="4.178844238281" />
                  <Point X="0.03669316864" Y="4.183886230469" />
                  <Point X="0.067131248474" Y="4.19421875" />
                  <Point X="0.094426467896" Y="4.20880859375" />
                  <Point X="0.115583503723" Y="4.231397460938" />
                  <Point X="0.133441802979" Y="4.258124023438" />
                  <Point X="0.146215255737" Y="4.286315429688" />
                  <Point X="0.160573883057" Y="4.339902832031" />
                  <Point X="0.307419372559" Y="4.8879375" />
                  <Point X="0.803879394531" Y="4.835944824219" />
                  <Point X="0.844041137695" Y="4.831738769531" />
                  <Point X="1.440256225586" Y="4.687793945312" />
                  <Point X="1.481025878906" Y="4.677950683594" />
                  <Point X="1.868997192383" Y="4.537230957031" />
                  <Point X="1.894649291992" Y="4.527926757812" />
                  <Point X="2.269899169922" Y="4.352435058594" />
                  <Point X="2.294569335938" Y="4.340897460937" />
                  <Point X="2.657107666016" Y="4.129682128906" />
                  <Point X="2.680979736328" Y="4.115773925781" />
                  <Point X="2.943260253906" Y="3.929254638672" />
                  <Point X="2.147581054688" Y="2.551097900391" />
                  <Point X="2.142076660156" Y="2.539932861328" />
                  <Point X="2.133076904297" Y="2.516057861328" />
                  <Point X="2.113202636719" Y="2.441737060547" />
                  <Point X="2.110632324219" Y="2.428325195312" />
                  <Point X="2.107698730469" Y="2.403457519531" />
                  <Point X="2.107727783203" Y="2.380954589844" />
                  <Point X="2.115477294922" Y="2.316688232422" />
                  <Point X="2.116099121094" Y="2.311529785156" />
                  <Point X="2.121441650391" Y="2.289607177734" />
                  <Point X="2.129708496094" Y="2.267516113281" />
                  <Point X="2.140071289062" Y="2.247470703125" />
                  <Point X="2.179837158203" Y="2.188865966797" />
                  <Point X="2.188397705078" Y="2.178036132813" />
                  <Point X="2.204890869141" Y="2.160031738281" />
                  <Point X="2.221600341797" Y="2.145591552734" />
                  <Point X="2.280204833984" Y="2.105825683594" />
                  <Point X="2.284908935547" Y="2.102634033203" />
                  <Point X="2.304955078125" Y="2.092270751953" />
                  <Point X="2.327041259766" Y="2.084005859375" />
                  <Point X="2.348963378906" Y="2.078663574219" />
                  <Point X="2.413229736328" Y="2.0709140625" />
                  <Point X="2.42742578125" Y="2.070272705078" />
                  <Point X="2.451486816406" Y="2.070988037109" />
                  <Point X="2.473205810547" Y="2.074170898438" />
                  <Point X="2.547526611328" Y="2.094045410156" />
                  <Point X="2.555596679688" Y="2.096593505859" />
                  <Point X="2.588533935547" Y="2.110145019531" />
                  <Point X="2.699422607422" Y="2.174166748047" />
                  <Point X="3.967326416016" Y="2.906191162109" />
                  <Point X="4.109112304688" Y="2.709141357422" />
                  <Point X="4.123270507813" Y="2.689464355469" />
                  <Point X="4.26219921875" Y="2.459884033203" />
                  <Point X="3.230783691406" Y="1.668451171875" />
                  <Point X="3.221424316406" Y="1.660241088867" />
                  <Point X="3.203974121094" Y="1.641628051758" />
                  <Point X="3.150485351562" Y="1.57184765625" />
                  <Point X="3.143204589844" Y="1.560843505859" />
                  <Point X="3.13044140625" Y="1.538290893555" />
                  <Point X="3.121629882812" Y="1.517086791992" />
                  <Point X="3.101705322266" Y="1.445840820312" />
                  <Point X="3.100105957031" Y="1.440122192383" />
                  <Point X="3.096652587891" Y="1.417822631836" />
                  <Point X="3.095836669922" Y="1.394250854492" />
                  <Point X="3.097739746094" Y="1.371767211914" />
                  <Point X="3.114095703125" Y="1.292497192383" />
                  <Point X="3.117708984375" Y="1.279634033203" />
                  <Point X="3.126219726563" Y="1.255894775391" />
                  <Point X="3.136282470703" Y="1.235740356445" />
                  <Point X="3.180769287109" Y="1.168122436523" />
                  <Point X="3.184340087891" Y="1.162694946289" />
                  <Point X="3.198894042969" Y="1.145449829102" />
                  <Point X="3.216138671875" Y="1.129359741211" />
                  <Point X="3.234347900391" Y="1.116034423828" />
                  <Point X="3.298815185547" Y="1.079744873047" />
                  <Point X="3.311290771484" Y="1.073870605469" />
                  <Point X="3.334440185547" Y="1.064960327148" />
                  <Point X="3.356118164062" Y="1.059438598633" />
                  <Point X="3.443282470703" Y="1.047918701172" />
                  <Point X="3.451342773438" Y="1.047201049805" />
                  <Point X="3.471416748047" Y="1.046273071289" />
                  <Point X="3.488203857422" Y="1.04698449707" />
                  <Point X="3.593540771484" Y="1.060852416992" />
                  <Point X="4.776839355469" Y="1.21663671875" />
                  <Point X="4.839854492188" Y="0.957788696289" />
                  <Point X="4.845936523438" Y="0.932807006836" />
                  <Point X="4.890865234375" Y="0.644238708496" />
                  <Point X="3.716580078125" Y="0.32958984375" />
                  <Point X="3.704790283203" Y="0.325586212158" />
                  <Point X="3.681545654297" Y="0.315067901611" />
                  <Point X="3.595909423828" Y="0.265568603516" />
                  <Point X="3.585312988281" Y="0.258453277588" />
                  <Point X="3.564127197266" Y="0.242059906006" />
                  <Point X="3.547530761719" Y="0.225576522827" />
                  <Point X="3.496149169922" Y="0.160104156494" />
                  <Point X="3.492024902344" Y="0.154848876953" />
                  <Point X="3.480301025391" Y="0.135569213867" />
                  <Point X="3.470527099609" Y="0.114105445862" />
                  <Point X="3.463680664062" Y="0.092604179382" />
                  <Point X="3.446553222656" Y="0.00317215395" />
                  <Point X="3.444990234375" Y="-0.009678421974" />
                  <Point X="3.443615478516" Y="-0.035666152954" />
                  <Point X="3.445178466797" Y="-0.058553718567" />
                  <Point X="3.462305908203" Y="-0.147985748291" />
                  <Point X="3.463680664062" Y="-0.155164215088" />
                  <Point X="3.470527099609" Y="-0.176665481567" />
                  <Point X="3.480301025391" Y="-0.198129257202" />
                  <Point X="3.492024902344" Y="-0.217408905029" />
                  <Point X="3.543406494141" Y="-0.282881286621" />
                  <Point X="3.552395019531" Y="-0.292806243896" />
                  <Point X="3.570831298828" Y="-0.310481933594" />
                  <Point X="3.589035644531" Y="-0.324155670166" />
                  <Point X="3.674671875" Y="-0.373654815674" />
                  <Point X="3.681536376953" Y="-0.377257263184" />
                  <Point X="3.700490966797" Y="-0.386238098145" />
                  <Point X="3.716579833984" Y="-0.392150024414" />
                  <Point X="3.813178466797" Y="-0.418033477783" />
                  <Point X="4.891472167969" Y="-0.706961425781" />
                  <Point X="4.858418457031" Y="-0.926202575684" />
                  <Point X="4.855022460938" Y="-0.948726135254" />
                  <Point X="4.801173339844" Y="-1.184698852539" />
                  <Point X="3.424382324219" Y="-1.003440979004" />
                  <Point X="3.408035644531" Y="-1.002710266113" />
                  <Point X="3.374658691406" Y="-1.005508666992" />
                  <Point X="3.206585205078" Y="-1.042040161133" />
                  <Point X="3.193094482422" Y="-1.044972290039" />
                  <Point X="3.163973876953" Y="-1.056597167969" />
                  <Point X="3.136147216797" Y="-1.073489624023" />
                  <Point X="3.112397460938" Y="-1.093960327148" />
                  <Point X="3.010807617188" Y="-1.216141113281" />
                  <Point X="3.002653320312" Y="-1.225948242188" />
                  <Point X="2.987932617188" Y="-1.250330688477" />
                  <Point X="2.976589355469" Y="-1.277715942383" />
                  <Point X="2.969757568359" Y="-1.305365600586" />
                  <Point X="2.955197265625" Y="-1.463595214844" />
                  <Point X="2.954028564453" Y="-1.476295776367" />
                  <Point X="2.956347412109" Y="-1.507564208984" />
                  <Point X="2.964078857422" Y="-1.539184936523" />
                  <Point X="2.976450195312" Y="-1.567996459961" />
                  <Point X="3.069464599609" Y="-1.712674194336" />
                  <Point X="3.074733154297" Y="-1.720066894531" />
                  <Point X="3.093819091797" Y="-1.744308227539" />
                  <Point X="3.110628417969" Y="-1.760909301758" />
                  <Point X="3.200272705078" Y="-1.829695678711" />
                  <Point X="4.213122070312" Y="-2.6068828125" />
                  <Point X="4.134381347656" Y="-2.734297363281" />
                  <Point X="4.124806640625" Y="-2.749790527344" />
                  <Point X="4.028981445312" Y="-2.885945068359" />
                  <Point X="2.800954589844" Y="-2.176943115234" />
                  <Point X="2.786131347656" Y="-2.170012207031" />
                  <Point X="2.754224365234" Y="-2.159825195312" />
                  <Point X="2.554190185547" Y="-2.12369921875" />
                  <Point X="2.538134033203" Y="-2.120799560547" />
                  <Point X="2.506781738281" Y="-2.120395263672" />
                  <Point X="2.474609130859" Y="-2.125353027344" />
                  <Point X="2.444833251953" Y="-2.135176757812" />
                  <Point X="2.278653808594" Y="-2.222635742188" />
                  <Point X="2.265315185547" Y="-2.229655761719" />
                  <Point X="2.242384033203" Y="-2.246549072266" />
                  <Point X="2.221424072266" Y="-2.267509277344" />
                  <Point X="2.204531494141" Y="-2.290439697266" />
                  <Point X="2.117072509766" Y="-2.456618896484" />
                  <Point X="2.110052490234" Y="-2.469957763672" />
                  <Point X="2.100228759766" Y="-2.499734130859" />
                  <Point X="2.095271240234" Y="-2.53190625" />
                  <Point X="2.095675537109" Y="-2.563258056641" />
                  <Point X="2.131801513672" Y="-2.763292236328" />
                  <Point X="2.133657470703" Y="-2.771480957031" />
                  <Point X="2.142459960938" Y="-2.803651123047" />
                  <Point X="2.151819091797" Y="-2.826078613281" />
                  <Point X="2.209424560547" Y="-2.925854248047" />
                  <Point X="2.861283691406" Y="-4.054906494141" />
                  <Point X="2.793211914063" Y="-4.103528320312" />
                  <Point X="2.781852294922" Y="-4.111642089844" />
                  <Point X="2.701764648438" Y="-4.163481445312" />
                  <Point X="1.758546020508" Y="-2.934254882812" />
                  <Point X="1.747503540039" Y="-2.922178955078" />
                  <Point X="1.721924194336" Y="-2.900557373047" />
                  <Point X="1.52463659668" Y="-2.773719482422" />
                  <Point X="1.50880078125" Y="-2.763538574219" />
                  <Point X="1.479989746094" Y="-2.751167236328" />
                  <Point X="1.448368408203" Y="-2.743435546875" />
                  <Point X="1.417099487305" Y="-2.741116699219" />
                  <Point X="1.201331787109" Y="-2.760971923828" />
                  <Point X="1.184012573242" Y="-2.762565673828" />
                  <Point X="1.15636328125" Y="-2.769397216797" />
                  <Point X="1.128978271484" Y="-2.780740234375" />
                  <Point X="1.104595825195" Y="-2.7954609375" />
                  <Point X="0.937985595703" Y="-2.933991943359" />
                  <Point X="0.924612121582" Y="-2.945111328125" />
                  <Point X="0.904141357422" Y="-2.968861572266" />
                  <Point X="0.887248901367" Y="-2.996688476562" />
                  <Point X="0.875624328613" Y="-3.025808837891" />
                  <Point X="0.825808532715" Y="-3.255" />
                  <Point X="0.824464904785" Y="-3.262693603516" />
                  <Point X="0.819753356934" Y="-3.298236328125" />
                  <Point X="0.819742248535" Y="-3.323120117188" />
                  <Point X="0.836067138672" Y="-3.447120117188" />
                  <Point X="1.022065307617" Y="-4.859915039062" />
                  <Point X="0.986422851562" Y="-4.867728027344" />
                  <Point X="0.975678405762" Y="-4.870083007812" />
                  <Point X="0.929315673828" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.043243530273" Y="-4.755583984375" />
                  <Point X="-1.058433105469" Y="-4.752635253906" />
                  <Point X="-1.14124621582" Y="-4.731328613281" />
                  <Point X="-1.120775634766" Y="-4.575838378906" />
                  <Point X="-1.120077514648" Y="-4.568099609375" />
                  <Point X="-1.119451782227" Y="-4.541033691406" />
                  <Point X="-1.121759277344" Y="-4.509328125" />
                  <Point X="-1.123334106445" Y="-4.497690429688" />
                  <Point X="-1.179364379883" Y="-4.216008789062" />
                  <Point X="-1.183861450195" Y="-4.193398925781" />
                  <Point X="-1.188125" Y="-4.17846875" />
                  <Point X="-1.19902734375" Y="-4.149502441406" />
                  <Point X="-1.205666015625" Y="-4.135466308594" />
                  <Point X="-1.22173840332" Y="-4.107628417969" />
                  <Point X="-1.23057434082" Y="-4.094861328125" />
                  <Point X="-1.250208740234" Y="-4.070936523438" />
                  <Point X="-1.261007080078" Y="-4.059778808594" />
                  <Point X="-1.476935791016" Y="-3.8704140625" />
                  <Point X="-1.494267822266" Y="-3.855214355469" />
                  <Point X="-1.506739379883" Y="-3.84596484375" />
                  <Point X="-1.533022338867" Y="-3.82962109375" />
                  <Point X="-1.546833618164" Y="-3.822526855469" />
                  <Point X="-1.576530761719" Y="-3.810225830078" />
                  <Point X="-1.591313598633" Y="-3.805476074219" />
                  <Point X="-1.621455322266" Y="-3.798447998047" />
                  <Point X="-1.636814208984" Y="-3.796169677734" />
                  <Point X="-1.923400024414" Y="-3.777385742188" />
                  <Point X="-1.946403442383" Y="-3.775878173828" />
                  <Point X="-1.961928955078" Y="-3.776132324219" />
                  <Point X="-1.992731445312" Y="-3.779166259766" />
                  <Point X="-2.008008178711" Y="-3.781946044922" />
                  <Point X="-2.039056762695" Y="-3.790265869141" />
                  <Point X="-2.05367578125" Y="-3.795496826172" />
                  <Point X="-2.081865966797" Y="-3.808270019531" />
                  <Point X="-2.095437011719" Y="-3.815812255859" />
                  <Point X="-2.334235839844" Y="-3.975372558594" />
                  <Point X="-2.353403564453" Y="-3.988180175781" />
                  <Point X="-2.359682373047" Y="-3.992757568359" />
                  <Point X="-2.380443847656" Y="-4.010132568359" />
                  <Point X="-2.402759765625" Y="-4.032772216797" />
                  <Point X="-2.410470947266" Y="-4.041629394531" />
                  <Point X="-2.427181396484" Y="-4.063406738281" />
                  <Point X="-2.503202636719" Y="-4.162479492188" />
                  <Point X="-2.725329833984" Y="-4.024943847656" />
                  <Point X="-2.747584960938" Y="-4.0111640625" />
                  <Point X="-2.980863037109" Y="-3.831547607422" />
                  <Point X="-2.341488769531" Y="-2.724119384766" />
                  <Point X="-2.334850097656" Y="-2.710083251953" />
                  <Point X="-2.323947998047" Y="-2.6811171875" />
                  <Point X="-2.319684570312" Y="-2.666187011719" />
                  <Point X="-2.313413574219" Y="-2.634660644531" />
                  <Point X="-2.311638916016" Y="-2.619235351562" />
                  <Point X="-2.310626220703" Y="-2.588301269531" />
                  <Point X="-2.314666259766" Y="-2.557615234375" />
                  <Point X="-2.323650878906" Y="-2.527997558594" />
                  <Point X="-2.329357421875" Y="-2.513557128906" />
                  <Point X="-2.343574462891" Y="-2.484728271484" />
                  <Point X="-2.351557128906" Y="-2.47141015625" />
                  <Point X="-2.369584716797" Y="-2.446253173828" />
                  <Point X="-2.379629638672" Y="-2.434414306641" />
                  <Point X="-2.395689208984" Y="-2.418354736328" />
                  <Point X="-2.408817138672" Y="-2.407020751953" />
                  <Point X="-2.433974121094" Y="-2.388993164062" />
                  <Point X="-2.447292236328" Y="-2.381010498047" />
                  <Point X="-2.47612109375" Y="-2.366793457031" />
                  <Point X="-2.490561523438" Y="-2.361086914062" />
                  <Point X="-2.520179199219" Y="-2.352102294922" />
                  <Point X="-2.550865234375" Y="-2.348062255859" />
                  <Point X="-2.581799316406" Y="-2.349074951172" />
                  <Point X="-2.597224609375" Y="-2.350849609375" />
                  <Point X="-2.628750976562" Y="-2.357120605469" />
                  <Point X="-2.643680908203" Y="-2.361384033203" />
                  <Point X="-2.672647216797" Y="-2.372286132812" />
                  <Point X="-2.68668359375" Y="-2.378924804688" />
                  <Point X="-2.782489990234" Y="-2.434238769531" />
                  <Point X="-3.793089111328" Y="-3.017708496094" />
                  <Point X="-3.986434570312" Y="-2.763692626953" />
                  <Point X="-4.004013183594" Y="-2.740597900391" />
                  <Point X="-4.181264648438" Y="-2.443373291016" />
                  <Point X="-3.048122070312" Y="-1.573881958008" />
                  <Point X="-3.036481689453" Y="-1.563309814453" />
                  <Point X="-3.015104736328" Y="-1.540390136719" />
                  <Point X="-3.005367919922" Y="-1.528042358398" />
                  <Point X="-2.987403320312" Y="-1.500911499023" />
                  <Point X="-2.979836425781" Y="-1.487126708984" />
                  <Point X="-2.967080322266" Y="-1.458497070312" />
                  <Point X="-2.961891113281" Y="-1.44365246582" />
                  <Point X="-2.954759033203" Y="-1.416115478516" />
                  <Point X="-2.951552978516" Y="-1.398805175781" />
                  <Point X="-2.948748535156" Y="-1.368378173828" />
                  <Point X="-2.948577636719" Y="-1.353051147461" />
                  <Point X="-2.950785888672" Y="-1.321380737305" />
                  <Point X="-2.953082519531" Y="-1.306224609375" />
                  <Point X="-2.960083251953" Y="-1.276479125977" />
                  <Point X="-2.971776855469" Y="-1.248246948242" />
                  <Point X="-2.987858886719" Y="-1.222262817383" />
                  <Point X="-2.996951416016" Y="-1.209921875" />
                  <Point X="-3.017783447266" Y="-1.185965209961" />
                  <Point X="-3.028743164062" Y="-1.175246459961" />
                  <Point X="-3.052244384766" Y="-1.155711181641" />
                  <Point X="-3.064785888672" Y="-1.146894775391" />
                  <Point X="-3.08930078125" Y="-1.132466430664" />
                  <Point X="-3.105432861328" Y="-1.124481811523" />
                  <Point X="-3.134694335938" Y="-1.113258178711" />
                  <Point X="-3.149791992188" Y="-1.108861206055" />
                  <Point X="-3.181680664062" Y="-1.10237902832" />
                  <Point X="-3.197297119141" Y="-1.100532714844" />
                  <Point X="-3.228621826172" Y="-1.09944140625" />
                  <Point X="-3.244329589844" Y="-1.100196533203" />
                  <Point X="-3.365276123047" Y="-1.116119628906" />
                  <Point X="-4.660920898438" Y="-1.286694335938" />
                  <Point X="-4.733884277344" Y="-1.001045715332" />
                  <Point X="-4.740760742188" Y="-0.974125305176" />
                  <Point X="-4.786452148438" Y="-0.65465435791" />
                  <Point X="-3.508288330078" Y="-0.312171417236" />
                  <Point X="-3.500477050781" Y="-0.309712921143" />
                  <Point X="-3.473933349609" Y="-0.299252227783" />
                  <Point X="-3.444168457031" Y="-0.283894134521" />
                  <Point X="-3.433562011719" Y="-0.277514068604" />
                  <Point X="-3.407871337891" Y="-0.259683013916" />
                  <Point X="-3.393672119141" Y="-0.248242630005" />
                  <Point X="-3.371216796875" Y="-0.226358505249" />
                  <Point X="-3.360898193359" Y="-0.214483535767" />
                  <Point X="-3.34165234375" Y="-0.188224990845" />
                  <Point X="-3.333435791016" Y="-0.174811325073" />
                  <Point X="-3.319328125" Y="-0.146811218262" />
                  <Point X="-3.313437255859" Y="-0.132224914551" />
                  <Point X="-3.304873535156" Y="-0.104632629395" />
                  <Point X="-3.300990234375" Y="-0.088499168396" />
                  <Point X="-3.296720703125" Y="-0.060339290619" />
                  <Point X="-3.295647216797" Y="-0.046098392487" />
                  <Point X="-3.295647216797" Y="-0.016461647034" />
                  <Point X="-3.296720703125" Y="-0.002220601082" />
                  <Point X="-3.300990234375" Y="0.025939128876" />
                  <Point X="-3.304186279297" Y="0.039857810974" />
                  <Point X="-3.31275" Y="0.06745009613" />
                  <Point X="-3.319327880859" Y="0.08425087738" />
                  <Point X="-3.333435546875" Y="0.112250984192" />
                  <Point X="-3.341652587891" Y="0.125665245056" />
                  <Point X="-3.3608984375" Y="0.15192364502" />
                  <Point X="-3.371216796875" Y="0.163798171997" />
                  <Point X="-3.393672363281" Y="0.185682739258" />
                  <Point X="-3.405809814453" Y="0.195692642212" />
                  <Point X="-3.431500488281" Y="0.213523223877" />
                  <Point X="-3.437522216797" Y="0.217375228882" />
                  <Point X="-3.460192382812" Y="0.22987902832" />
                  <Point X="-3.495084228516" Y="0.245007797241" />
                  <Point X="-3.508288085938" Y="0.249611251831" />
                  <Point X="-3.618536621094" Y="0.279152130127" />
                  <Point X="-4.785446289062" Y="0.591824707031" />
                  <Point X="-4.73576171875" Y="0.927588195801" />
                  <Point X="-4.731330078125" Y="0.957535705566" />
                  <Point X="-4.633586914062" Y="1.318237182617" />
                  <Point X="-3.778065917969" Y="1.20560559082" />
                  <Point X="-3.767738037109" Y="1.204815429688" />
                  <Point X="-3.747058105469" Y="1.204364257812" />
                  <Point X="-3.736705810547" Y="1.204703125" />
                  <Point X="-3.715144287109" Y="1.20658984375" />
                  <Point X="-3.704891113281" Y="1.208053466797" />
                  <Point X="-3.684604248047" Y="1.212088745117" />
                  <Point X="-3.674570556641" Y="1.21466027832" />
                  <Point X="-3.617708740234" Y="1.232588745117" />
                  <Point X="-3.603449951172" Y="1.237676757812" />
                  <Point X="-3.584516601562" Y="1.2460078125" />
                  <Point X="-3.575277832031" Y="1.250689941406" />
                  <Point X="-3.556533203125" Y="1.261512329102" />
                  <Point X="-3.547858642578" Y="1.267172607422" />
                  <Point X="-3.531177490234" Y="1.279403930664" />
                  <Point X="-3.515927001953" Y="1.29337878418" />
                  <Point X="-3.502288574219" Y="1.308930664062" />
                  <Point X="-3.495894287109" Y="1.317078613281" />
                  <Point X="-3.483479736328" Y="1.33480871582" />
                  <Point X="-3.478010253906" Y="1.343604614258" />
                  <Point X="-3.468061767578" Y="1.361738525391" />
                  <Point X="-3.463582519531" Y="1.371076904297" />
                  <Point X="-3.440766357422" Y="1.426159423828" />
                  <Point X="-3.435498535156" Y="1.440353393555" />
                  <Point X="-3.429710205078" Y="1.460213012695" />
                  <Point X="-3.427358398438" Y="1.470300415039" />
                  <Point X="-3.423600341797" Y="1.491614990234" />
                  <Point X="-3.422360595703" Y="1.50189831543" />
                  <Point X="-3.421008056641" Y="1.522538330078" />
                  <Point X="-3.421910400391" Y="1.543201782227" />
                  <Point X="-3.425056884766" Y="1.563645263672" />
                  <Point X="-3.427188232422" Y="1.573781982422" />
                  <Point X="-3.432790039062" Y="1.594687866211" />
                  <Point X="-3.436012451172" Y="1.604531738281" />
                  <Point X="-3.443509277344" Y="1.623809448242" />
                  <Point X="-3.447783691406" Y="1.633243530273" />
                  <Point X="-3.475313720703" Y="1.686128540039" />
                  <Point X="-3.482799560547" Y="1.699285766602" />
                  <Point X="-3.494291015625" Y="1.71648425293" />
                  <Point X="-3.500506347656" Y="1.724769897461" />
                  <Point X="-3.514418945312" Y="1.741350585938" />
                  <Point X="-3.521500488281" Y="1.748911621094" />
                  <Point X="-3.536442871094" Y="1.763215576172" />
                  <Point X="-3.544303710938" Y="1.769958862305" />
                  <Point X="-3.607542480469" Y="1.818483642578" />
                  <Point X="-4.227614257812" Y="2.294281982422" />
                  <Point X="-4.019505615234" Y="2.650821533203" />
                  <Point X="-4.002291992188" Y="2.680313232422" />
                  <Point X="-3.726338867188" Y="3.035012451172" />
                  <Point X="-3.254156982422" Y="2.7623984375" />
                  <Point X="-3.24491796875" Y="2.757716308594" />
                  <Point X="-3.225985839844" Y="2.749385986328" />
                  <Point X="-3.216292724609" Y="2.745737792969" />
                  <Point X="-3.195650878906" Y="2.739229248047" />
                  <Point X="-3.185616943359" Y="2.736657470703" />
                  <Point X="-3.165328857422" Y="2.732621826172" />
                  <Point X="-3.155074707031" Y="2.731157958984" />
                  <Point X="-3.075881835938" Y="2.724229492188" />
                  <Point X="-3.059173339844" Y="2.723334472656" />
                  <Point X="-3.038494140625" Y="2.723785644531" />
                  <Point X="-3.028166992188" Y="2.724575683594" />
                  <Point X="-3.006708496094" Y="2.727400634766" />
                  <Point X="-2.996529052734" Y="2.729310058594" />
                  <Point X="-2.976437744141" Y="2.734226074219" />
                  <Point X="-2.957000732422" Y="2.741300292969" />
                  <Point X="-2.938449707031" Y="2.750448242188" />
                  <Point X="-2.929423828125" Y="2.755528564453" />
                  <Point X="-2.911169677734" Y="2.767157226562" />
                  <Point X="-2.902749023438" Y="2.773191162109" />
                  <Point X="-2.886615722656" Y="2.786139404297" />
                  <Point X="-2.878903076172" Y="2.793053710938" />
                  <Point X="-2.82269140625" Y="2.849265380859" />
                  <Point X="-2.811266113281" Y="2.861488525391" />
                  <Point X="-2.798319824219" Y="2.877618896484" />
                  <Point X="-2.792286865234" Y="2.886037841797" />
                  <Point X="-2.780656982422" Y="2.904292480469" />
                  <Point X="-2.775575927734" Y="2.913318603516" />
                  <Point X="-2.766427001953" Y="2.931870117188" />
                  <Point X="-2.759352050781" Y="2.951307373047" />
                  <Point X="-2.754435302734" Y="2.971399414062" />
                  <Point X="-2.752525634766" Y="2.981579589844" />
                  <Point X="-2.749700195312" Y="3.0030390625" />
                  <Point X="-2.748909912109" Y="3.013366943359" />
                  <Point X="-2.748458496094" Y="3.034047851562" />
                  <Point X="-2.748797363281" Y="3.044400878906" />
                  <Point X="-2.755725830078" Y="3.12359375" />
                  <Point X="-2.757745605469" Y="3.140203857422" />
                  <Point X="-2.761781005859" Y="3.160491699219" />
                  <Point X="-2.764352783203" Y="3.170526123047" />
                  <Point X="-2.770861328125" Y="3.191168212891" />
                  <Point X="-2.774510253906" Y="3.200862548828" />
                  <Point X="-2.782841064453" Y="3.219795166016" />
                  <Point X="-2.787522949219" Y="3.229033447266" />
                  <Point X="-2.815546142578" Y="3.277570556641" />
                  <Point X="-3.059387451172" Y="3.699916259766" />
                  <Point X="-2.678346435547" Y="3.992056152344" />
                  <Point X="-2.648369873047" Y="4.015039306641" />
                  <Point X="-2.192525390625" Y="4.268296875" />
                  <Point X="-2.118563720703" Y="4.171908203125" />
                  <Point X="-2.1118203125" Y="4.164047363281" />
                  <Point X="-2.097518066406" Y="4.149106933594" />
                  <Point X="-2.089959716797" Y="4.14202734375" />
                  <Point X="-2.073380615234" Y="4.128115234375" />
                  <Point X="-2.065094482422" Y="4.121899414062" />
                  <Point X="-2.047894165039" Y="4.11040625" />
                  <Point X="-2.038980224609" Y="4.10512890625" />
                  <Point X="-1.950839111328" Y="4.059245361328" />
                  <Point X="-1.934328491211" Y="4.051287109375" />
                  <Point X="-1.915050537109" Y="4.043790527344" />
                  <Point X="-1.905208007812" Y="4.040568603516" />
                  <Point X="-1.884301269531" Y="4.034966552734" />
                  <Point X="-1.874166381836" Y="4.032835449219" />
                  <Point X="-1.85372277832" Y="4.029688476562" />
                  <Point X="-1.833057373047" Y="4.028785888672" />
                  <Point X="-1.812417236328" Y="4.030138427734" />
                  <Point X="-1.802133178711" Y="4.031378173828" />
                  <Point X="-1.780817749023" Y="4.035136474609" />
                  <Point X="-1.770729858398" Y="4.037488525391" />
                  <Point X="-1.750870117188" Y="4.043277099609" />
                  <Point X="-1.741098510742" Y="4.046713623047" />
                  <Point X="-1.649293457031" Y="4.084740966797" />
                  <Point X="-1.632586303711" Y="4.092272216797" />
                  <Point X="-1.614452758789" Y="4.102220703125" />
                  <Point X="-1.605657714844" Y="4.107689453125" />
                  <Point X="-1.587928100586" Y="4.120103515625" />
                  <Point X="-1.579778930664" Y="4.126499023438" />
                  <Point X="-1.564227050781" Y="4.140137695312" />
                  <Point X="-1.550253173828" Y="4.155387695312" />
                  <Point X="-1.538021850586" Y="4.172068847656" />
                  <Point X="-1.532361694336" Y="4.180743164062" />
                  <Point X="-1.521539550781" Y="4.199487304687" />
                  <Point X="-1.516857055664" Y="4.2087265625" />
                  <Point X="-1.508526000977" Y="4.22766015625" />
                  <Point X="-1.504877441406" Y="4.237354003906" />
                  <Point X="-1.474996582031" Y="4.332123535156" />
                  <Point X="-1.470026733398" Y="4.349763671875" />
                  <Point X="-1.465991333008" Y="4.370050292969" />
                  <Point X="-1.46452734375" Y="4.380304199219" />
                  <Point X="-1.46264074707" Y="4.401866210938" />
                  <Point X="-1.462301757812" Y="4.41221875" />
                  <Point X="-1.462752929688" Y="4.432899414062" />
                  <Point X="-1.46354309082" Y="4.443227539062" />
                  <Point X="-1.466729125977" Y="4.467427246094" />
                  <Point X="-1.479265991211" Y="4.562654785156" />
                  <Point X="-0.969976196289" Y="4.705441894531" />
                  <Point X="-0.931174072266" Y="4.716320800781" />
                  <Point X="-0.365222106934" Y="4.782557128906" />
                  <Point X="-0.22566633606" Y="4.261727539062" />
                  <Point X="-0.220435256958" Y="4.247107910156" />
                  <Point X="-0.207661849976" Y="4.218916503906" />
                  <Point X="-0.200119247437" Y="4.205344726562" />
                  <Point X="-0.182260864258" Y="4.178618164062" />
                  <Point X="-0.17260824585" Y="4.166455566406" />
                  <Point X="-0.151451187134" Y="4.143866699219" />
                  <Point X="-0.126897941589" Y="4.125026367188" />
                  <Point X="-0.099602897644" Y="4.110436523438" />
                  <Point X="-0.085356651306" Y="4.104260742188" />
                  <Point X="-0.054918682098" Y="4.093927978516" />
                  <Point X="-0.039857234955" Y="4.090155273438" />
                  <Point X="-0.009319986343" Y="4.08511328125" />
                  <Point X="0.021631772995" Y="4.08511328125" />
                  <Point X="0.052169025421" Y="4.090155273438" />
                  <Point X="0.06723046875" Y="4.093927978516" />
                  <Point X="0.097668441772" Y="4.104260742188" />
                  <Point X="0.111914535522" Y="4.110436523438" />
                  <Point X="0.139209732056" Y="4.125026367188" />
                  <Point X="0.163763122559" Y="4.143866699219" />
                  <Point X="0.184920181274" Y="4.166455566406" />
                  <Point X="0.194572952271" Y="4.178617675781" />
                  <Point X="0.212431182861" Y="4.205344238281" />
                  <Point X="0.2199737854" Y="4.218916503906" />
                  <Point X="0.232747192383" Y="4.247107910156" />
                  <Point X="0.237978271484" Y="4.261727539062" />
                  <Point X="0.252336868286" Y="4.315314941406" />
                  <Point X="0.378190368652" Y="4.785006347656" />
                  <Point X="0.793984436035" Y="4.741461425781" />
                  <Point X="0.827876586914" Y="4.737912109375" />
                  <Point X="1.41796081543" Y="4.595447265625" />
                  <Point X="1.453596801758" Y="4.586843261719" />
                  <Point X="1.836604980469" Y="4.447923828125" />
                  <Point X="1.858260253906" Y="4.440069335938" />
                  <Point X="2.229654541016" Y="4.266380859375" />
                  <Point X="2.2504453125" Y="4.256657714844" />
                  <Point X="2.609284667969" Y="4.047596923828" />
                  <Point X="2.629437011719" Y="4.035855712891" />
                  <Point X="2.817780029297" Y="3.901916748047" />
                  <Point X="2.06530859375" Y="2.598597900391" />
                  <Point X="2.062373291016" Y="2.59310546875" />
                  <Point X="2.053182617187" Y="2.573441650391" />
                  <Point X="2.044182861328" Y="2.549566650391" />
                  <Point X="2.041301635742" Y="2.540599609375" />
                  <Point X="2.021427368164" Y="2.466278808594" />
                  <Point X="2.019900512695" Y="2.459617919922" />
                  <Point X="2.016286499023" Y="2.439455078125" />
                  <Point X="2.013352783203" Y="2.414587402344" />
                  <Point X="2.012698852539" Y="2.403334960938" />
                  <Point X="2.012727905273" Y="2.38083203125" />
                  <Point X="2.013410888672" Y="2.369581542969" />
                  <Point X="2.021160644531" Y="2.305315185547" />
                  <Point X="2.023800415039" Y="2.289036621094" />
                  <Point X="2.029142944336" Y="2.267114013672" />
                  <Point X="2.032467529297" Y="2.256311523438" />
                  <Point X="2.040734375" Y="2.234220458984" />
                  <Point X="2.045318359375" Y="2.223889160156" />
                  <Point X="2.055681152344" Y="2.20384375" />
                  <Point X="2.061459960938" Y="2.194129638672" />
                  <Point X="2.101225830078" Y="2.135524902344" />
                  <Point X="2.105309082031" Y="2.129954589844" />
                  <Point X="2.118346923828" Y="2.113865234375" />
                  <Point X="2.134840087891" Y="2.095860839844" />
                  <Point X="2.142774169922" Y="2.088153320313" />
                  <Point X="2.159483642578" Y="2.073713134766" />
                  <Point X="2.168259033203" Y="2.06698046875" />
                  <Point X="2.226863525391" Y="2.027214599609" />
                  <Point X="2.241281738281" Y="2.018244018555" />
                  <Point X="2.261327880859" Y="2.007880859375" />
                  <Point X="2.271659912109" Y="2.003296386719" />
                  <Point X="2.29374609375" Y="1.995031494141" />
                  <Point X="2.304548583984" Y="1.99170703125" />
                  <Point X="2.326470703125" Y="1.986364868164" />
                  <Point X="2.337590332031" Y="1.984346801758" />
                  <Point X="2.401856689453" Y="1.976597290039" />
                  <Point X="2.408942138672" Y="1.976010864258" />
                  <Point X="2.430248779297" Y="1.975314697266" />
                  <Point X="2.454309814453" Y="1.976030029297" />
                  <Point X="2.46526171875" Y="1.97699206543" />
                  <Point X="2.486980712891" Y="1.980174926758" />
                  <Point X="2.497747802734" Y="1.982395751953" />
                  <Point X="2.572068603516" Y="2.002270263672" />
                  <Point X="2.591743164062" Y="2.008738891602" />
                  <Point X="2.624680419922" Y="2.022290405273" />
                  <Point X="2.636033935547" Y="2.027872680664" />
                  <Point X="2.746922607422" Y="2.091894287109" />
                  <Point X="3.940404785156" Y="2.780951416016" />
                  <Point X="4.031999755859" Y="2.653655517578" />
                  <Point X="4.043950439453" Y="2.637046386719" />
                  <Point X="4.136884765625" Y="2.483472167969" />
                  <Point X="3.172951416016" Y="1.743819702148" />
                  <Point X="3.16813671875" Y="1.739867919922" />
                  <Point X="3.152119140625" Y="1.725216430664" />
                  <Point X="3.134668945312" Y="1.706603515625" />
                  <Point X="3.128576660156" Y="1.699422607422" />
                  <Point X="3.075087890625" Y="1.629642211914" />
                  <Point X="3.060526367188" Y="1.607633544922" />
                  <Point X="3.047763183594" Y="1.585080932617" />
                  <Point X="3.042714599609" Y="1.574746459961" />
                  <Point X="3.033903076172" Y="1.553542358398" />
                  <Point X="3.030140136719" Y="1.542672729492" />
                  <Point X="3.010215576172" Y="1.471426757812" />
                  <Point X="3.006225097656" Y="1.454660888672" />
                  <Point X="3.002771728516" Y="1.432361328125" />
                  <Point X="3.001709472656" Y="1.421109008789" />
                  <Point X="3.000893554688" Y="1.397537231445" />
                  <Point X="3.001175048828" Y="1.386238525391" />
                  <Point X="3.003078125" Y="1.363754760742" />
                  <Point X="3.004699707031" Y="1.352570068359" />
                  <Point X="3.021055664062" Y="1.273300048828" />
                  <Point X="3.022635498047" Y="1.266805908203" />
                  <Point X="3.028282226562" Y="1.247573730469" />
                  <Point X="3.03679296875" Y="1.223834472656" />
                  <Point X="3.041224853516" Y="1.213458251953" />
                  <Point X="3.051287597656" Y="1.193303833008" />
                  <Point X="3.056918457031" Y="1.183525634766" />
                  <Point X="3.101405273438" Y="1.115907714844" />
                  <Point X="3.111739501953" Y="1.101423828125" />
                  <Point X="3.126293457031" Y="1.084178710938" />
                  <Point X="3.134084472656" Y="1.075989746094" />
                  <Point X="3.151329101562" Y="1.059899536133" />
                  <Point X="3.160036132812" Y="1.052694824219" />
                  <Point X="3.178245361328" Y="1.039369506836" />
                  <Point X="3.187747070312" Y="1.033249389648" />
                  <Point X="3.252214355469" Y="0.996959838867" />
                  <Point X="3.258345214844" Y="0.993796142578" />
                  <Point X="3.277165527344" Y="0.985211364746" />
                  <Point X="3.300314941406" Y="0.976300964355" />
                  <Point X="3.310990966797" Y="0.972899902344" />
                  <Point X="3.332668945312" Y="0.967378112793" />
                  <Point X="3.343670898438" Y="0.965257568359" />
                  <Point X="3.430835205078" Y="0.953737670898" />
                  <Point X="3.446955810547" Y="0.952302429199" />
                  <Point X="3.467029785156" Y="0.951374389648" />
                  <Point X="3.475439208984" Y="0.951358337402" />
                  <Point X="3.500604003906" Y="0.952797180176" />
                  <Point X="3.605940917969" Y="0.966665161133" />
                  <Point X="4.704703613281" Y="1.111320068359" />
                  <Point X="4.747550292969" Y="0.935317749023" />
                  <Point X="4.752684082031" Y="0.914231506348" />
                  <Point X="4.783871582031" Y="0.713921081543" />
                  <Point X="3.6919921875" Y="0.421352722168" />
                  <Point X="3.686032714844" Y="0.419544586182" />
                  <Point X="3.665625488281" Y="0.412137359619" />
                  <Point X="3.642380859375" Y="0.401619171143" />
                  <Point X="3.634004394531" Y="0.397316558838" />
                  <Point X="3.548368164062" Y="0.347817230225" />
                  <Point X="3.527175537109" Y="0.333586608887" />
                  <Point X="3.505989746094" Y="0.317193359375" />
                  <Point X="3.497182128906" Y="0.309464202881" />
                  <Point X="3.480585693359" Y="0.292980895996" />
                  <Point X="3.472796630859" Y="0.2842265625" />
                  <Point X="3.421415039062" Y="0.218754150391" />
                  <Point X="3.410854492188" Y="0.204208282471" />
                  <Point X="3.399130615234" Y="0.184928619385" />
                  <Point X="3.393843017578" Y="0.174939529419" />
                  <Point X="3.384069091797" Y="0.153475708008" />
                  <Point X="3.380005371094" Y="0.142929428101" />
                  <Point X="3.373158935547" Y="0.121428153992" />
                  <Point X="3.370376220703" Y="0.11047315979" />
                  <Point X="3.353248779297" Y="0.021041261673" />
                  <Point X="3.350122802734" Y="-0.004659948826" />
                  <Point X="3.348748046875" Y="-0.030647703171" />
                  <Point X="3.348836181641" Y="-0.042138629913" />
                  <Point X="3.350399169922" Y="-0.065026115417" />
                  <Point X="3.351874023438" Y="-0.076422813416" />
                  <Point X="3.369001464844" Y="-0.165854705811" />
                  <Point X="3.373158935547" Y="-0.183988189697" />
                  <Point X="3.380005371094" Y="-0.205489471436" />
                  <Point X="3.384069091797" Y="-0.216035751343" />
                  <Point X="3.393843017578" Y="-0.237499420166" />
                  <Point X="3.399130615234" Y="-0.24748866272" />
                  <Point X="3.410854492188" Y="-0.266768310547" />
                  <Point X="3.417290771484" Y="-0.276058898926" />
                  <Point X="3.468672363281" Y="-0.341531280518" />
                  <Point X="3.472991699219" Y="-0.346652404785" />
                  <Point X="3.486649414062" Y="-0.361381072998" />
                  <Point X="3.505085693359" Y="-0.379056640625" />
                  <Point X="3.513776611328" Y="-0.386440826416" />
                  <Point X="3.531980957031" Y="-0.400114562988" />
                  <Point X="3.541494628906" Y="-0.406404296875" />
                  <Point X="3.627130859375" Y="-0.455903442383" />
                  <Point X="3.640859619141" Y="-0.46310824585" />
                  <Point X="3.659814208984" Y="-0.472089080811" />
                  <Point X="3.667724853516" Y="-0.475408569336" />
                  <Point X="3.6919921875" Y="-0.48391293335" />
                  <Point X="3.788590820313" Y="-0.509796478271" />
                  <Point X="4.784876953125" Y="-0.776750610352" />
                  <Point X="4.764479980469" Y="-0.912040039062" />
                  <Point X="4.761613769531" Y="-0.93105078125" />
                  <Point X="4.727801269531" Y="-1.079219604492" />
                  <Point X="3.436782226562" Y="-0.909253662109" />
                  <Point X="3.428624755859" Y="-0.908535827637" />
                  <Point X="3.400098388672" Y="-0.908042419434" />
                  <Point X="3.366721435547" Y="-0.910840820312" />
                  <Point X="3.354481201172" Y="-0.912676147461" />
                  <Point X="3.186407714844" Y="-0.949207702637" />
                  <Point X="3.157873291016" Y="-0.956742553711" />
                  <Point X="3.128752685547" Y="-0.968367553711" />
                  <Point X="3.11467578125" Y="-0.975389404297" />
                  <Point X="3.086849121094" Y="-0.992281860352" />
                  <Point X="3.074123535156" Y="-1.001530822754" />
                  <Point X="3.050373779297" Y="-1.022001525879" />
                  <Point X="3.039349609375" Y="-1.033223144531" />
                  <Point X="2.937759765625" Y="-1.155403930664" />
                  <Point X="2.921326171875" Y="-1.17684753418" />
                  <Point X="2.90660546875" Y="-1.201229858398" />
                  <Point X="2.9001640625" Y="-1.213975952148" />
                  <Point X="2.888820800781" Y="-1.241361206055" />
                  <Point X="2.884362792969" Y="-1.254928222656" />
                  <Point X="2.877531005859" Y="-1.282577880859" />
                  <Point X="2.875157226562" Y="-1.296660522461" />
                  <Point X="2.860596923828" Y="-1.454890136719" />
                  <Point X="2.859288818359" Y="-1.483321655273" />
                  <Point X="2.861607666016" Y="-1.514590209961" />
                  <Point X="2.864065917969" Y="-1.530127685547" />
                  <Point X="2.871797363281" Y="-1.561748291016" />
                  <Point X="2.876785888672" Y="-1.576667358398" />
                  <Point X="2.889157226562" Y="-1.605479003906" />
                  <Point X="2.896540039062" Y="-1.619371337891" />
                  <Point X="2.989554443359" Y="-1.764049072266" />
                  <Point X="3.000091552734" Y="-1.778834594727" />
                  <Point X="3.019177490234" Y="-1.803075805664" />
                  <Point X="3.027063964844" Y="-1.811900756836" />
                  <Point X="3.043873291016" Y="-1.828501831055" />
                  <Point X="3.052796142578" Y="-1.836277832031" />
                  <Point X="3.142440429688" Y="-1.905064331055" />
                  <Point X="4.087170410156" Y="-2.629981689453" />
                  <Point X="4.053567871094" Y="-2.68435546875" />
                  <Point X="4.045485107422" Y="-2.697434570312" />
                  <Point X="4.001274414063" Y="-2.760251953125" />
                  <Point X="2.848454589844" Y="-2.094670654297" />
                  <Point X="2.841192626953" Y="-2.090885498047" />
                  <Point X="2.815025390625" Y="-2.079512695312" />
                  <Point X="2.783118408203" Y="-2.069325683594" />
                  <Point X="2.771108154297" Y="-2.066337646484" />
                  <Point X="2.571073974609" Y="-2.030211547852" />
                  <Point X="2.539358886719" Y="-2.025807495117" />
                  <Point X="2.508006591797" Y="-2.025403198242" />
                  <Point X="2.492313232422" Y="-2.026503662109" />
                  <Point X="2.460140625" Y="-2.031461425781" />
                  <Point X="2.444844482422" Y="-2.035136230469" />
                  <Point X="2.415068603516" Y="-2.044959838867" />
                  <Point X="2.400588867188" Y="-2.051108642578" />
                  <Point X="2.234409423828" Y="-2.138567626953" />
                  <Point X="2.208968505859" Y="-2.153170166016" />
                  <Point X="2.186037353516" Y="-2.170063476562" />
                  <Point X="2.175208496094" Y="-2.179374267578" />
                  <Point X="2.154248535156" Y="-2.200334472656" />
                  <Point X="2.144938232422" Y="-2.211163085938" />
                  <Point X="2.128045654297" Y="-2.234093505859" />
                  <Point X="2.120463378906" Y="-2.2461953125" />
                  <Point X="2.033004516602" Y="-2.412374511719" />
                  <Point X="2.019835571289" Y="-2.440193603516" />
                  <Point X="2.01001184082" Y="-2.469969970703" />
                  <Point X="2.006336914062" Y="-2.485266113281" />
                  <Point X="2.001379272461" Y="-2.517438232422" />
                  <Point X="2.000279174805" Y="-2.533131103516" />
                  <Point X="2.00068347168" Y="-2.564482910156" />
                  <Point X="2.002187866211" Y="-2.580141845703" />
                  <Point X="2.038313842773" Y="-2.780176025391" />
                  <Point X="2.042025756836" Y="-2.796553466797" />
                  <Point X="2.050828369141" Y="-2.828723632813" />
                  <Point X="2.054787597656" Y="-2.840237304688" />
                  <Point X="2.064146728516" Y="-2.862664794922" />
                  <Point X="2.069546630859" Y="-2.873578613281" />
                  <Point X="2.127152099609" Y="-2.973354248047" />
                  <Point X="2.735893310547" Y="-4.027724121094" />
                  <Point X="2.72375390625" Y="-4.036083496094" />
                  <Point X="1.833914550781" Y="-2.876422607422" />
                  <Point X="1.828654052734" Y="-2.870146728516" />
                  <Point X="1.808830932617" Y="-2.849625732422" />
                  <Point X="1.783251586914" Y="-2.828004150391" />
                  <Point X="1.773299072266" Y="-2.820647216797" />
                  <Point X="1.576011474609" Y="-2.693809326172" />
                  <Point X="1.546283935547" Y="-2.676245849609" />
                  <Point X="1.517473022461" Y="-2.663874511719" />
                  <Point X="1.502553344727" Y="-2.658885742188" />
                  <Point X="1.470932006836" Y="-2.651154052734" />
                  <Point X="1.455394165039" Y="-2.648695800781" />
                  <Point X="1.424125244141" Y="-2.646376953125" />
                  <Point X="1.408394287109" Y="-2.646516357422" />
                  <Point X="1.192626586914" Y="-2.666371582031" />
                  <Point X="1.161225341797" Y="-2.670339111328" />
                  <Point X="1.133576049805" Y="-2.677170654297" />
                  <Point X="1.120008911133" Y="-2.681628417969" />
                  <Point X="1.092623901367" Y="-2.692971435547" />
                  <Point X="1.079877563477" Y="-2.699413085938" />
                  <Point X="1.055495239258" Y="-2.714133789062" />
                  <Point X="1.043858764648" Y="-2.722412841797" />
                  <Point X="0.877248474121" Y="-2.860943847656" />
                  <Point X="0.852652832031" Y="-2.883088378906" />
                  <Point X="0.832182128906" Y="-2.906838623047" />
                  <Point X="0.822933349609" Y="-2.919563964844" />
                  <Point X="0.80604083252" Y="-2.947390869141" />
                  <Point X="0.799019042969" Y="-2.961468017578" />
                  <Point X="0.787394470215" Y="-2.990588378906" />
                  <Point X="0.782791931152" Y="-3.005631347656" />
                  <Point X="0.732976074219" Y="-3.234822509766" />
                  <Point X="0.730288818359" Y="-3.250209716797" />
                  <Point X="0.725577148438" Y="-3.285752441406" />
                  <Point X="0.724753356934" Y="-3.298193847656" />
                  <Point X="0.7247421875" Y="-3.323077636719" />
                  <Point X="0.725554992676" Y="-3.335520019531" />
                  <Point X="0.741879882812" Y="-3.459520019531" />
                  <Point X="0.83309161377" Y="-4.152341308594" />
                  <Point X="0.655064941406" Y="-3.487936767578" />
                  <Point X="0.652606445312" Y="-3.480125" />
                  <Point X="0.642145019531" Y="-3.453580322266" />
                  <Point X="0.62678692627" Y="-3.423815673828" />
                  <Point X="0.620407409668" Y="-3.413210205078" />
                  <Point X="0.468845367432" Y="-3.194837890625" />
                  <Point X="0.456679992676" Y="-3.177309814453" />
                  <Point X="0.446670837402" Y="-3.165173095703" />
                  <Point X="0.424787139893" Y="-3.142717773438" />
                  <Point X="0.412912322998" Y="-3.132399169922" />
                  <Point X="0.386656616211" Y="-3.113155029297" />
                  <Point X="0.373242492676" Y="-3.104937744141" />
                  <Point X="0.345241210938" Y="-3.090829345703" />
                  <Point X="0.330654296875" Y="-3.084938232422" />
                  <Point X="0.096120529175" Y="-3.012147460938" />
                  <Point X="0.063375785828" Y="-3.003108886719" />
                  <Point X="0.035215610504" Y="-2.998839599609" />
                  <Point X="0.020974859238" Y="-2.997766357422" />
                  <Point X="-0.008666491508" Y="-2.997766601562" />
                  <Point X="-0.022906497955" Y="-2.998840087891" />
                  <Point X="-0.051065185547" Y="-3.003109375" />
                  <Point X="-0.06498387146" Y="-3.006305175781" />
                  <Point X="-0.299517486572" Y="-3.079095703125" />
                  <Point X="-0.332929992676" Y="-3.090829589844" />
                  <Point X="-0.360931610107" Y="-3.104938232422" />
                  <Point X="-0.374346466064" Y="-3.113156005859" />
                  <Point X="-0.400601593018" Y="-3.132400146484" />
                  <Point X="-0.412476104736" Y="-3.142718505859" />
                  <Point X="-0.43435949707" Y="-3.165173583984" />
                  <Point X="-0.44436819458" Y="-3.177310058594" />
                  <Point X="-0.595930236816" Y="-3.395682128906" />
                  <Point X="-0.60809576416" Y="-3.413210449219" />
                  <Point X="-0.612471130371" Y="-3.420133544922" />
                  <Point X="-0.625976745605" Y="-3.445265380859" />
                  <Point X="-0.638777648926" Y="-3.476215576172" />
                  <Point X="-0.642752990723" Y="-3.487936523438" />
                  <Point X="-0.672517456055" Y="-3.599018798828" />
                  <Point X="-0.985425170898" Y="-4.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.076874064498" Y="4.677796307217" />
                  <Point X="0.352849823786" Y="4.690434197348" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.343796118952" Y="4.702594197511" />
                  <Point X="-0.942838290539" Y="4.71305051751" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.484406681946" Y="4.575668327755" />
                  <Point X="0.32750927892" Y="4.59586204704" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.318217452709" Y="4.607133249066" />
                  <Point X="-1.261870937256" Y="4.623604781909" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.759610633167" Y="4.475850153753" />
                  <Point X="0.302168734053" Y="4.501289896732" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.292638786465" Y="4.511672300622" />
                  <Point X="-1.475271738624" Y="4.532315235588" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.993653656496" Y="4.376750446421" />
                  <Point X="0.276828189187" Y="4.406717746423" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.267060120221" Y="4.416211352178" />
                  <Point X="-1.463073368512" Y="4.437087841082" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.204698005044" Y="4.27805218245" />
                  <Point X="0.251487650917" Y="4.312145596" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.241481453977" Y="4.320750403734" />
                  <Point X="-1.47214873269" Y="4.342231780989" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.38212510049" Y="4.179940709815" />
                  <Point X="0.219293878172" Y="4.217693069229" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.210506730882" Y="4.225195266767" />
                  <Point X="-1.501605420886" Y="4.247731478229" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.185910407743" Y="4.259676066206" />
                  <Point X="-2.207368043263" Y="4.260050610627" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.55024836399" Y="4.081991636171" />
                  <Point X="0.137488831515" Y="4.124106510466" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.131825501435" Y="4.128807409634" />
                  <Point X="-1.551896009473" Y="4.153594832554" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.111159255218" Y="4.163356808821" />
                  <Point X="-2.373177541129" Y="4.167930355014" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.701891399423" Y="3.984330225977" />
                  <Point X="-1.705957875456" Y="4.061269521264" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.963358295194" Y="4.065762462303" />
                  <Point X="-2.538987038996" Y="4.075810099401" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.809421191512" Y="3.88743881531" />
                  <Point X="-2.689604569145" Y="3.983424667004" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.755111877002" Y="3.793372316757" />
                  <Point X="-2.81077425637" Y="3.890525220598" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.700802562493" Y="3.699305818204" />
                  <Point X="-2.931943943595" Y="3.797625774192" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.646493247984" Y="3.605239319651" />
                  <Point X="-3.053113630821" Y="3.704726327785" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.592183933475" Y="3.511172821097" />
                  <Point X="-3.006841620785" Y="3.608904175682" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.537874618965" Y="3.417106322544" />
                  <Point X="-2.951426556639" Y="3.512922430975" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.483565304456" Y="3.323039823991" />
                  <Point X="-2.896011492494" Y="3.416940686268" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.429255989947" Y="3.228973325438" />
                  <Point X="-2.840596428348" Y="3.320958941561" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.374946675438" Y="3.134906826885" />
                  <Point X="-2.785469826718" Y="3.224982231986" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.320637360928" Y="3.040840328331" />
                  <Point X="-2.756439286601" Y="3.12946103086" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.266328046419" Y="2.946773829778" />
                  <Point X="-2.748466991521" Y="3.034307402767" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.21201873191" Y="2.852707331225" />
                  <Point X="-2.763628847516" Y="2.939557582784" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.585864724064" Y="2.953909763395" />
                  <Point X="-3.786708662211" Y="2.957415507376" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.979337295523" Y="2.726844199784" />
                  <Point X="3.850581017143" Y="2.729091648983" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.157709417401" Y="2.758640832672" />
                  <Point X="-2.826319407692" Y="2.845637379418" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.416164060257" Y="2.855933156126" />
                  <Point X="-3.859638769216" Y="2.863674035965" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.047830745349" Y="2.630634171006" />
                  <Point X="3.69084053937" Y="2.63686545823" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.103400102891" Y="2.664574334119" />
                  <Point X="-2.934776010945" Y="2.752516025305" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.245353805097" Y="2.757937180868" />
                  <Point X="-3.932568876221" Y="2.769932564554" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.105941881065" Y="2.534605366195" />
                  <Point X="3.531100061598" Y="2.544639267477" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.052057188376" Y="2.570456056861" />
                  <Point X="-4.004706067904" Y="2.676177252754" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.080281330556" Y="2.440038801606" />
                  <Point X="3.371359583826" Y="2.452413076724" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.024008526449" Y="2.475931176912" />
                  <Point X="-4.05960541231" Y="2.582121053211" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.959210236269" Y="2.347137634253" />
                  <Point X="3.211619106053" Y="2.360186885971" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.012727541729" Y="2.381113616068" />
                  <Point X="-4.114505091057" Y="2.488064859504" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.838139141982" Y="2.254236466901" />
                  <Point X="3.051878628281" Y="2.267960695218" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.024566637672" Y="2.285892492716" />
                  <Point X="-4.169404769803" Y="2.394008665797" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.717068047696" Y="2.161335299549" />
                  <Point X="2.892138150508" Y="2.175734504465" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.06413497321" Y="2.190187353686" />
                  <Point X="-4.22430444855" Y="2.29995247209" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.595996953409" Y="2.068434132196" />
                  <Point X="2.732397675764" Y="2.083508313659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.136854910935" Y="2.093903551287" />
                  <Point X="-4.108545782347" Y="2.202917425891" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.474925859122" Y="1.975532964844" />
                  <Point X="2.533532230271" Y="1.991965051758" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.290609383119" Y="1.996205285828" />
                  <Point X="-3.981838327622" Y="2.105691267878" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.353854764836" Y="1.882631797491" />
                  <Point X="-3.855130872898" Y="2.008465109864" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.232783670549" Y="1.789730630139" />
                  <Point X="-3.728423418174" Y="1.911238951851" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.12639252169" Y="1.696573223386" />
                  <Point X="-3.60171595259" Y="1.814012793648" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.057766376013" Y="1.60275662605" />
                  <Point X="-3.494776679674" Y="1.717131690532" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.020553150261" Y="1.508391714158" />
                  <Point X="-3.442496313232" Y="1.621204662177" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.001453383793" Y="1.413710630657" />
                  <Point X="-3.421151258234" Y="1.525817611692" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.011725982153" Y="1.318516850622" />
                  <Point X="-3.438927771857" Y="1.431113430727" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.037124165706" Y="1.223059052515" />
                  <Point X="-3.482207676916" Y="1.336854413116" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.7077310904" Y="1.09888402901" />
                  <Point X="4.621654905094" Y="1.100386494414" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.094074263442" Y="1.127050513697" />
                  <Point X="-3.589723576118" Y="1.243716638953" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.14058305915" Y="1.253331926996" />
                  <Point X="-4.648771266637" Y="1.262202385153" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.730960487018" Y="1.00346408722" />
                  <Point X="3.984434108461" Y="1.016494753628" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.192969115037" Y="1.030309826477" />
                  <Point X="-4.674397151114" Y="1.167635215467" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.753645950154" Y="0.908053639824" />
                  <Point X="-4.700023035591" Y="1.07306804578" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.768479621311" Y="0.812780245966" />
                  <Point X="-4.725648920068" Y="0.978500876093" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.783313292468" Y="0.717506852109" />
                  <Point X="-4.74224481134" Y="0.883776087289" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.463489537817" Y="0.628074925348" />
                  <Point X="-4.756268347119" Y="0.789006397852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.130577814428" Y="0.538871449931" />
                  <Point X="-4.770291882899" Y="0.694236708415" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.79766609104" Y="0.449667974514" />
                  <Point X="-4.784315418679" Y="0.599467018979" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.567156208736" Y="0.358677068312" />
                  <Point X="-4.436725947243" Y="0.498385351022" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.458151586124" Y="0.265565279913" />
                  <Point X="-4.057417864676" Y="0.396750032649" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.392367420377" Y="0.171699075633" />
                  <Point X="-3.67810978211" Y="0.295114714276" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.364000075335" Y="0.077179758318" />
                  <Point X="-3.405381930029" Y="0.195339760746" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.349439300926" Y="-0.017580553583" />
                  <Point X="-3.326735197714" Y="0.098952505763" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.358832902762" Y="-0.112758990677" />
                  <Point X="-3.297577298579" Y="0.003429081576" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.381034770405" Y="-0.208160996883" />
                  <Point X="-3.301715696261" Y="-0.091513153588" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.439370493548" Y="-0.304193721882" />
                  <Point X="-3.340201129965" Y="-0.185855859008" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.533084241175" Y="-0.400843972596" />
                  <Point X="-3.436351495787" Y="-0.279192019294" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.750753034933" Y="-0.499657866688" />
                  <Point X="-3.721247363618" Y="-0.369233614587" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.130061090875" Y="-0.601293184596" />
                  <Point X="-4.05415927544" Y="-0.458437086715" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.509369166736" Y="-0.702928502852" />
                  <Point X="-4.387071187261" Y="-0.547640558843" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.780967133841" Y="-0.802683734166" />
                  <Point X="-4.719983099083" Y="-0.636844030971" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.766679832358" Y="-0.897448819556" />
                  <Point X="-4.775548967457" Y="-0.730888596295" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.747674981465" Y="-0.992131559814" />
                  <Point X="3.962154411873" Y="-0.978420247269" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.139539468073" Y="-0.964061450014" />
                  <Point X="-4.761925788394" Y="-0.826140860934" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.019594568945" Y="-1.056982275176" />
                  <Point X="-4.748302609332" Y="-0.921393125573" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.941722923851" Y="-1.150637491719" />
                  <Point X="-4.729878237714" Y="-1.01672919534" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.887720628999" Y="-1.24470934932" />
                  <Point X="-3.07544395606" Y="-1.14062192431" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.495673751791" Y="-1.133286785941" />
                  <Point X="-4.705499948891" Y="-1.112169191119" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.871221040082" Y="-1.339435819089" />
                  <Point X="-2.978534813167" Y="-1.237327950856" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.132895298705" Y="-1.21717851363" />
                  <Point X="-4.681121660069" Y="-1.207609186897" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.862491821277" Y="-1.434297921172" />
                  <Point X="-2.949986828957" Y="-1.332840728938" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.863940928781" Y="-1.529337686601" />
                  <Point X="-2.957764459655" Y="-1.427719441053" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.900148567501" Y="-1.62498416445" />
                  <Point X="-3.001349207509" Y="-1.521973137614" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.961927367341" Y="-1.721076988577" />
                  <Point X="-3.102008631277" Y="-1.615230592001" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.032555522074" Y="-1.817324278768" />
                  <Point X="-3.223079726828" Y="-1.708131759331" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.154699136382" Y="-1.91447077465" />
                  <Point X="-3.34415082238" Y="-1.801032926661" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.281406637678" Y="-2.011696933476" />
                  <Point X="-3.465221917931" Y="-1.893934093992" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.408114138974" Y="-2.108923092303" />
                  <Point X="2.856462411643" Y="-2.099293975584" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.326605942247" Y="-2.090045296508" />
                  <Point X="-3.586293013482" Y="-1.986835261322" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.534821640271" Y="-2.206149251129" />
                  <Point X="3.026162745361" Y="-2.197270577092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.172217884408" Y="-2.182364914099" />
                  <Point X="-3.707364109034" Y="-2.079736428652" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.661529141567" Y="-2.303375409955" />
                  <Point X="3.195863079079" Y="-2.295247178599" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.104671944728" Y="-2.2762003665" />
                  <Point X="-3.828435204585" Y="-2.172637595983" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.788236642863" Y="-2.400601568781" />
                  <Point X="3.365563412796" Y="-2.393223780107" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.055121734877" Y="-2.370349935534" />
                  <Point X="-3.949506300136" Y="-2.265538763313" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.914944144159" Y="-2.497827727607" />
                  <Point X="3.535263746514" Y="-2.491200381614" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.01178088276" Y="-2.464607889311" />
                  <Point X="-2.437387281474" Y="-2.386947370127" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.692855486362" Y="-2.382488156024" />
                  <Point X="-4.070577395687" Y="-2.358439930644" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.041651645455" Y="-2.595053886434" />
                  <Point X="3.704964080232" Y="-2.589176983122" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.000618279907" Y="-2.559427516517" />
                  <Point X="-2.344258274593" Y="-2.483587414153" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.852595746817" Y="-2.47471435057" />
                  <Point X="-4.176353795785" Y="-2.451608067876" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.049947866771" Y="-2.690213168679" />
                  <Point X="3.87466441395" Y="-2.68715358463" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.015653796418" Y="-2.654704433598" />
                  <Point X="-2.311828689002" Y="-2.57916794584" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.012336182051" Y="-2.566940542066" />
                  <Point X="-4.1190953875" Y="-2.547621988275" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.032867580851" Y="-2.750019372487" />
                  <Point X="1.653132462217" Y="-2.743391071336" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.031674708481" Y="-2.732543485895" />
                  <Point X="-2.321917433797" Y="-2.674006317308" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.172076617286" Y="-2.659166733561" />
                  <Point X="-4.061836979215" Y="-2.643635908673" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.056964749486" Y="-2.845454461295" />
                  <Point X="1.798560024158" Y="-2.840943990036" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.919751078232" Y="-2.825604322826" />
                  <Point X="-2.366958706583" Y="-2.768234590132" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.33181705252" Y="-2.751392925057" />
                  <Point X="-4.00457857093" Y="-2.739649829072" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.108687001116" Y="-2.941371747719" />
                  <Point X="1.88069816889" Y="-2.93739218785" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.823389179751" Y="-2.918936790795" />
                  <Point X="-2.421268048783" Y="-2.862301088201" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.491557487755" Y="-2.843619116552" />
                  <Point X="-3.931442436044" Y="-2.835940896219" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.164102110223" Y="-3.037353493211" />
                  <Point X="1.954595115006" Y="-3.033696535007" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.781143832337" Y="-3.013213866677" />
                  <Point X="0.045308933928" Y="-3.000369820749" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.024911746599" Y="-2.999144114211" />
                  <Point X="-2.475577390984" Y="-2.956367586271" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.651297922989" Y="-2.935845308048" />
                  <Point X="-3.858147900485" Y="-2.93223472826" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.219517240815" Y="-3.133335239078" />
                  <Point X="2.028492061121" Y="-3.130000882163" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.760570012997" Y="-3.107869220489" />
                  <Point X="0.365369540659" Y="-3.100970970585" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.327375802694" Y="-3.088879055638" />
                  <Point X="-2.529886733185" Y="-3.050434084341" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.274932371407" Y="-3.229316984944" />
                  <Point X="2.102389007237" Y="-3.22630522932" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.739996193658" Y="-3.202524574301" />
                  <Point X="0.470920552948" Y="-3.197827841522" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.447482415208" Y="-3.181797058082" />
                  <Point X="-2.584196075386" Y="-3.144500582411" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.330347501998" Y="-3.325298730811" />
                  <Point X="2.176285953353" Y="-3.322609576476" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.724814260545" Y="-3.297274043837" />
                  <Point X="0.537674373127" Y="-3.294007504951" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.512638256093" Y="-3.275674229813" />
                  <Point X="-2.638505417587" Y="-3.238567080481" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.38576263259" Y="-3.421280476678" />
                  <Point X="2.250182899469" Y="-3.418913923633" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.733047618189" Y="-3.392432228793" />
                  <Point X="0.604428193305" Y="-3.390187168381" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.577794096978" Y="-3.369551401545" />
                  <Point X="-2.692814759787" Y="-3.332633578551" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.441177763182" Y="-3.517262222544" />
                  <Point X="2.324079845584" Y="-3.51521827079" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.745585314867" Y="-3.487665546267" />
                  <Point X="0.654479100043" Y="-3.486075281372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.633556797256" Y="-3.463592531155" />
                  <Point X="-2.747124101988" Y="-3.426700076621" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.496592893773" Y="-3.613243968411" />
                  <Point X="2.3979767917" Y="-3.611522617946" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.758123029711" Y="-3.582898864058" />
                  <Point X="0.680145252616" Y="-3.581537756896" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.661558161607" Y="-3.558118236686" />
                  <Point X="-2.801433444189" Y="-3.52076657469" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.552008024365" Y="-3.709225714278" />
                  <Point X="2.471873737816" Y="-3.707826965103" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.770660744556" Y="-3.678132181849" />
                  <Point X="0.705723938498" Y="-3.676998705683" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.686898710408" Y="-3.652690386926" />
                  <Point X="-2.85574278639" Y="-3.61483307276" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.607423154956" Y="-3.805207460144" />
                  <Point X="2.545770683931" Y="-3.804131312259" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.7831984594" Y="-3.77336549964" />
                  <Point X="0.73130262438" Y="-3.77245965447" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.712239234172" Y="-3.747262537602" />
                  <Point X="-2.91005212859" Y="-3.70889957083" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.662838285548" Y="-3.901189206011" />
                  <Point X="2.619667630047" Y="-3.900435659416" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.795736174245" Y="-3.86859881743" />
                  <Point X="0.756881310262" Y="-3.867920603257" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.737579757936" Y="-3.841834688279" />
                  <Point X="-1.536389743579" Y="-3.827891408114" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.098822121725" Y="-3.818074114436" />
                  <Point X="-2.964361470791" Y="-3.8029660689" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.71825341614" Y="-3.997170951878" />
                  <Point X="2.693564576163" Y="-3.996740006573" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.808273889089" Y="-3.963832135221" />
                  <Point X="0.782459996144" Y="-3.963381552044" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.762920281699" Y="-3.936406838955" />
                  <Point X="-1.41465754074" Y="-3.925030722783" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.237401154653" Y="-3.910669679583" />
                  <Point X="-2.892964253569" Y="-3.899226783126" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.820811603934" Y="-4.059065453012" />
                  <Point X="0.808038682026" Y="-4.058842500831" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.788260805463" Y="-4.030978989632" />
                  <Point X="-1.304114286375" Y="-4.021974733629" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.372314503916" Y="-4.003329229476" />
                  <Point X="-2.766701203101" Y="-3.996445184035" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.813601329227" Y="-4.125551140308" />
                  <Point X="-1.215440253223" Y="-4.118537015799" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.452910009486" Y="-4.096936900857" />
                  <Point X="-2.613586936896" Y="-4.094132274657" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.83894185299" Y="-4.220123290985" />
                  <Point X="-1.179729152473" Y="-4.214174826546" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.864282376754" Y="-4.314695441662" />
                  <Point X="-1.160763672235" Y="-4.309520341399" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.889622900518" Y="-4.409267592338" />
                  <Point X="-1.141798164639" Y="-4.40486585673" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.914963424282" Y="-4.503839743015" />
                  <Point X="-1.122993348581" Y="-4.500208567179" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.940303948045" Y="-4.598411893691" />
                  <Point X="-1.123326895059" Y="-4.595217216268" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.965644471809" Y="-4.692984044368" />
                  <Point X="-1.13580704914" Y="-4.690013845532" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="0.001625976562" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978485351562" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.842355957031" Y="-4.92101953125" />
                  <Point X="0.471539031982" Y="-3.537112304688" />
                  <Point X="0.464318664551" Y="-3.521544189453" />
                  <Point X="0.312756530762" Y="-3.303171875" />
                  <Point X="0.300591033936" Y="-3.285643798828" />
                  <Point X="0.274335449219" Y="-3.266399658203" />
                  <Point X="0.039801624298" Y="-3.193608886719" />
                  <Point X="0.020976322174" Y="-3.187766357422" />
                  <Point X="-0.008664908409" Y="-3.187766601562" />
                  <Point X="-0.243198577881" Y="-3.260557128906" />
                  <Point X="-0.262024047852" Y="-3.266399902344" />
                  <Point X="-0.288279296875" Y="-3.285644042969" />
                  <Point X="-0.439841461182" Y="-3.504016113281" />
                  <Point X="-0.452006958008" Y="-3.521544433594" />
                  <Point X="-0.459227142334" Y="-3.537112304688" />
                  <Point X="-0.488991607666" Y="-3.648194580078" />
                  <Point X="-0.847744018555" Y="-4.987077148438" />
                  <Point X="-1.079447631836" Y="-4.942102539062" />
                  <Point X="-1.100243652344" Y="-4.938065917969" />
                  <Point X="-1.351589477539" Y="-4.873396972656" />
                  <Point X="-1.309150146484" Y="-4.551038574219" />
                  <Point X="-1.309683227539" Y="-4.5347578125" />
                  <Point X="-1.365713378906" Y="-4.253076171875" />
                  <Point X="-1.370210693359" Y="-4.230466308594" />
                  <Point X="-1.386282958984" Y="-4.202628417969" />
                  <Point X="-1.602211669922" Y="-4.013263671875" />
                  <Point X="-1.619543701172" Y="-3.998063964844" />
                  <Point X="-1.649240966797" Y="-3.985762939453" />
                  <Point X="-1.935826782227" Y="-3.966979003906" />
                  <Point X="-1.958830200195" Y="-3.965471435547" />
                  <Point X="-1.989878662109" Y="-3.973791259766" />
                  <Point X="-2.228677490234" Y="-4.1333515625" />
                  <Point X="-2.247845214844" Y="-4.146159179688" />
                  <Point X="-2.259734130859" Y="-4.157294433594" />
                  <Point X="-2.276444580078" Y="-4.179071777344" />
                  <Point X="-2.457095214844" Y="-4.414500488281" />
                  <Point X="-2.825352294922" Y="-4.186484863281" />
                  <Point X="-2.855831054688" Y="-4.16761328125" />
                  <Point X="-3.228581054688" Y="-3.880608398438" />
                  <Point X="-3.195155029297" Y="-3.822712402344" />
                  <Point X="-2.506033691406" Y="-2.629119628906" />
                  <Point X="-2.499762695312" Y="-2.597593261719" />
                  <Point X="-2.513979736328" Y="-2.568764404297" />
                  <Point X="-2.530039306641" Y="-2.552704833984" />
                  <Point X="-2.531328369141" Y="-2.551415771484" />
                  <Point X="-2.560157226562" Y="-2.537198730469" />
                  <Point X="-2.59168359375" Y="-2.543469726562" />
                  <Point X="-2.687489990234" Y="-2.598783691406" />
                  <Point X="-3.842959228516" Y="-3.265894042969" />
                  <Point X="-4.13762109375" Y="-2.878769042969" />
                  <Point X="-4.161703125" Y="-2.847129882812" />
                  <Point X="-4.43101953125" Y="-2.395526855469" />
                  <Point X="-4.369525390625" Y="-2.348340576172" />
                  <Point X="-3.163786865234" Y="-1.423144897461" />
                  <Point X="-3.145822265625" Y="-1.396014282227" />
                  <Point X="-3.138690185547" Y="-1.368477294922" />
                  <Point X="-3.138117675781" Y="-1.366266967773" />
                  <Point X="-3.140325927734" Y="-1.334596435547" />
                  <Point X="-3.161157958984" Y="-1.310639770508" />
                  <Point X="-3.185672851562" Y="-1.296211547852" />
                  <Point X="-3.187640625" Y="-1.295053344727" />
                  <Point X="-3.219529296875" Y="-1.288571044922" />
                  <Point X="-3.340475830078" Y="-1.304494140625" />
                  <Point X="-4.803284179688" Y="-1.497076416016" />
                  <Point X="-4.917974121094" Y="-1.048067626953" />
                  <Point X="-4.927391601562" Y="-1.011198974609" />
                  <Point X="-4.998396484375" Y="-0.51474206543" />
                  <Point X="-4.930981445312" Y="-0.496678344727" />
                  <Point X="-3.557463867188" Y="-0.128645431519" />
                  <Point X="-3.541896972656" Y="-0.121425842285" />
                  <Point X="-3.516206298828" Y="-0.103594917297" />
                  <Point X="-3.514144042969" Y="-0.10216381073" />
                  <Point X="-3.494898193359" Y="-0.075905342102" />
                  <Point X="-3.486334472656" Y="-0.048313117981" />
                  <Point X="-3.485647216797" Y="-0.046098457336" />
                  <Point X="-3.485647216797" Y="-0.016461637497" />
                  <Point X="-3.4942109375" Y="0.011130586624" />
                  <Point X="-3.494898193359" Y="0.013345247269" />
                  <Point X="-3.514144042969" Y="0.039603713989" />
                  <Point X="-3.539834716797" Y="0.057434482574" />
                  <Point X="-3.557463867188" Y="0.066085334778" />
                  <Point X="-3.667712402344" Y="0.095626319885" />
                  <Point X="-4.998186523438" Y="0.452125793457" />
                  <Point X="-4.92371484375" Y="0.95540045166" />
                  <Point X="-4.91764453125" Y="0.996421936035" />
                  <Point X="-4.773696777344" Y="1.527633544922" />
                  <Point X="-4.773516601562" Y="1.528298706055" />
                  <Point X="-4.737837402344" Y="1.5236015625" />
                  <Point X="-3.753266357422" Y="1.393980102539" />
                  <Point X="-3.731704833984" Y="1.395866577148" />
                  <Point X="-3.674843017578" Y="1.413795043945" />
                  <Point X="-3.670278808594" Y="1.415234130859" />
                  <Point X="-3.651534179688" Y="1.426056518555" />
                  <Point X="-3.639119628906" Y="1.443786743164" />
                  <Point X="-3.616303466797" Y="1.498869628906" />
                  <Point X="-3.614472167969" Y="1.503291137695" />
                  <Point X="-3.610714111328" Y="1.524605712891" />
                  <Point X="-3.616315917969" Y="1.54551159668" />
                  <Point X="-3.643845947266" Y="1.598396484375" />
                  <Point X="-3.646055664062" Y="1.602641235352" />
                  <Point X="-3.659968261719" Y="1.619221801758" />
                  <Point X="-3.72320703125" Y="1.667746582031" />
                  <Point X="-4.47610546875" Y="2.245466064453" />
                  <Point X="-4.183598144531" Y="2.746600830078" />
                  <Point X="-4.160012695312" Y="2.787008789062" />
                  <Point X="-3.778698242188" Y="3.277135009766" />
                  <Point X="-3.763853271484" Y="3.276064941406" />
                  <Point X="-3.159156982422" Y="2.926943359375" />
                  <Point X="-3.138515136719" Y="2.920434814453" />
                  <Point X="-3.059322265625" Y="2.913506347656" />
                  <Point X="-3.052965820312" Y="2.912950195312" />
                  <Point X="-3.031507324219" Y="2.915775146484" />
                  <Point X="-3.013253173828" Y="2.927403808594" />
                  <Point X="-2.957041503906" Y="2.983615478516" />
                  <Point X="-2.952529541016" Y="2.988127197266" />
                  <Point X="-2.940899658203" Y="3.006381835938" />
                  <Point X="-2.93807421875" Y="3.027841308594" />
                  <Point X="-2.945002685547" Y="3.107034179688" />
                  <Point X="-2.945558837891" Y="3.113390869141" />
                  <Point X="-2.952067382812" Y="3.134032958984" />
                  <Point X="-2.980090576172" Y="3.182570068359" />
                  <Point X="-3.307278564453" Y="3.749276855469" />
                  <Point X="-2.793950439453" Y="4.14283984375" />
                  <Point X="-2.752872558594" Y="4.174333984375" />
                  <Point X="-2.152314208984" Y="4.507990722656" />
                  <Point X="-2.141548828125" Y="4.513971679688" />
                  <Point X="-1.967826660156" Y="4.287573242188" />
                  <Point X="-1.951247558594" Y="4.273661132813" />
                  <Point X="-1.863106323242" Y="4.22777734375" />
                  <Point X="-1.85603137207" Y="4.224094238281" />
                  <Point X="-1.835124755859" Y="4.2184921875" />
                  <Point X="-1.813809204102" Y="4.222250488281" />
                  <Point X="-1.722004150391" Y="4.260277832031" />
                  <Point X="-1.714635253906" Y="4.263330078125" />
                  <Point X="-1.696905639648" Y="4.275744140625" />
                  <Point X="-1.686083496094" Y="4.29448828125" />
                  <Point X="-1.656202758789" Y="4.3892578125" />
                  <Point X="-1.653804199219" Y="4.396864746094" />
                  <Point X="-1.651917602539" Y="4.418426757812" />
                  <Point X="-1.655103637695" Y="4.442626464844" />
                  <Point X="-1.689137573242" Y="4.701141113281" />
                  <Point X="-1.021267822266" Y="4.888388183594" />
                  <Point X="-0.968093688965" Y="4.903296386719" />
                  <Point X="-0.240029541016" Y="4.988505859375" />
                  <Point X="-0.221026260376" Y="4.978514648438" />
                  <Point X="-0.042140380859" Y="4.310903320312" />
                  <Point X="-0.024282115936" Y="4.284176757813" />
                  <Point X="0.006155934334" Y="4.273844238281" />
                  <Point X="0.036594036102" Y="4.284176757813" />
                  <Point X="0.054452251434" Y="4.310903320312" />
                  <Point X="0.068810966492" Y="4.364490722656" />
                  <Point X="0.236648406982" Y="4.990868652344" />
                  <Point X="0.813774353027" Y="4.930428222656" />
                  <Point X="0.860205932617" Y="4.925565429688" />
                  <Point X="1.462551635742" Y="4.780140625" />
                  <Point X="1.508456420898" Y="4.769057617188" />
                  <Point X="1.901389526367" Y="4.626538085938" />
                  <Point X="1.931036621094" Y="4.615784667969" />
                  <Point X="2.310143798828" Y="4.438489257812" />
                  <Point X="2.3386953125" Y="4.425136230469" />
                  <Point X="2.704930664062" Y="4.211767089844" />
                  <Point X="2.732524902344" Y="4.195690429687" />
                  <Point X="3.068740478516" Y="3.956592773438" />
                  <Point X="3.027459716797" Y="3.885092285156" />
                  <Point X="2.229853515625" Y="2.503597900391" />
                  <Point X="2.224852294922" Y="2.491516113281" />
                  <Point X="2.204978027344" Y="2.4171953125" />
                  <Point X="2.202044433594" Y="2.392327636719" />
                  <Point X="2.209793945312" Y="2.328061279297" />
                  <Point X="2.210415771484" Y="2.322902832031" />
                  <Point X="2.218682617188" Y="2.300811767578" />
                  <Point X="2.258448486328" Y="2.24220703125" />
                  <Point X="2.274941650391" Y="2.224202636719" />
                  <Point X="2.333546142578" Y="2.184436767578" />
                  <Point X="2.338250244141" Y="2.181245117188" />
                  <Point X="2.360336425781" Y="2.172980224609" />
                  <Point X="2.424602783203" Y="2.165230712891" />
                  <Point X="2.448663818359" Y="2.165946044922" />
                  <Point X="2.522984619141" Y="2.185820556641" />
                  <Point X="2.541033935547" Y="2.192417480469" />
                  <Point X="2.651922607422" Y="2.256439208984" />
                  <Point X="3.994248046875" Y="3.031430908203" />
                  <Point X="4.186224609375" Y="2.764626953125" />
                  <Point X="4.202592773438" Y="2.741878662109" />
                  <Point X="4.387513183594" Y="2.436296386719" />
                  <Point X="4.338739257812" Y="2.398870605469" />
                  <Point X="3.288616210938" Y="1.593082763672" />
                  <Point X="3.279371582031" Y="1.583833496094" />
                  <Point X="3.2258828125" Y="1.514053222656" />
                  <Point X="3.213119628906" Y="1.491500732422" />
                  <Point X="3.193195068359" Y="1.420254882812" />
                  <Point X="3.191595703125" Y="1.414536254883" />
                  <Point X="3.190779785156" Y="1.390964355469" />
                  <Point X="3.207135742188" Y="1.311694335938" />
                  <Point X="3.215646484375" Y="1.287955078125" />
                  <Point X="3.260133300781" Y="1.220337158203" />
                  <Point X="3.263704101562" Y="1.214909667969" />
                  <Point X="3.280948730469" Y="1.198819458008" />
                  <Point X="3.345416015625" Y="1.162529907227" />
                  <Point X="3.368565429688" Y="1.153619628906" />
                  <Point X="3.455729736328" Y="1.142099731445" />
                  <Point X="3.475803710938" Y="1.14117175293" />
                  <Point X="3.581140625" Y="1.155039672852" />
                  <Point X="4.848975097656" Y="1.32195324707" />
                  <Point X="4.932158691406" Y="0.98025970459" />
                  <Point X="4.939188964844" Y="0.951383544922" />
                  <Point X="4.997858886719" Y="0.574556213379" />
                  <Point X="4.944631347656" Y="0.560294006348" />
                  <Point X="3.741167724609" Y="0.237826919556" />
                  <Point X="3.729086914062" Y="0.232819259644" />
                  <Point X="3.643450683594" Y="0.183320007324" />
                  <Point X="3.622264892578" Y="0.166926574707" />
                  <Point X="3.570883300781" Y="0.101454200745" />
                  <Point X="3.566759033203" Y="0.096198890686" />
                  <Point X="3.556985107422" Y="0.07473513031" />
                  <Point X="3.539857666016" Y="-0.014696935654" />
                  <Point X="3.538482910156" Y="-0.040684635162" />
                  <Point X="3.555610351562" Y="-0.130116699219" />
                  <Point X="3.556985107422" Y="-0.137295227051" />
                  <Point X="3.566759033203" Y="-0.158758987427" />
                  <Point X="3.618140625" Y="-0.22423135376" />
                  <Point X="3.636576904297" Y="-0.241906967163" />
                  <Point X="3.722213134766" Y="-0.291406219482" />
                  <Point X="3.741167724609" Y="-0.300387023926" />
                  <Point X="3.837766357422" Y="-0.326270507812" />
                  <Point X="4.998067871094" Y="-0.637172363281" />
                  <Point X="4.952356933594" Y="-0.940365234375" />
                  <Point X="4.948431640625" Y="-0.966400268555" />
                  <Point X="4.874545410156" Y="-1.290178344727" />
                  <Point X="4.807661621094" Y="-1.281372802734" />
                  <Point X="3.411982177734" Y="-1.097628295898" />
                  <Point X="3.394836181641" Y="-1.098341186523" />
                  <Point X="3.226762695312" Y="-1.134872680664" />
                  <Point X="3.213271972656" Y="-1.137804931641" />
                  <Point X="3.1854453125" Y="-1.154697387695" />
                  <Point X="3.08385546875" Y="-1.276878295898" />
                  <Point X="3.075701171875" Y="-1.286685424805" />
                  <Point X="3.064357910156" Y="-1.314070678711" />
                  <Point X="3.049797607422" Y="-1.472300292969" />
                  <Point X="3.04862890625" Y="-1.485000854492" />
                  <Point X="3.056360351562" Y="-1.516621582031" />
                  <Point X="3.149374755859" Y="-1.661299316406" />
                  <Point X="3.168460693359" Y="-1.685540649414" />
                  <Point X="3.258104980469" Y="-1.754327026367" />
                  <Point X="4.33907421875" Y="-2.583783935547" />
                  <Point X="4.215194824219" Y="-2.784239257812" />
                  <Point X="4.204130859375" Y="-2.802142578125" />
                  <Point X="4.056688476562" Y="-3.011638427734" />
                  <Point X="3.996495361328" Y="-2.976885986328" />
                  <Point X="2.753454589844" Y="-2.259215576172" />
                  <Point X="2.737340576172" Y="-2.253312744141" />
                  <Point X="2.537306396484" Y="-2.217186767578" />
                  <Point X="2.521250244141" Y="-2.214287109375" />
                  <Point X="2.489077636719" Y="-2.219244873047" />
                  <Point X="2.322898193359" Y="-2.306703857422" />
                  <Point X="2.309559570312" Y="-2.313723876953" />
                  <Point X="2.288599609375" Y="-2.334684082031" />
                  <Point X="2.201140625" Y="-2.50086328125" />
                  <Point X="2.194120605469" Y="-2.514202148438" />
                  <Point X="2.189163085938" Y="-2.546374267578" />
                  <Point X="2.2252890625" Y="-2.746408447266" />
                  <Point X="2.234091552734" Y="-2.778578613281" />
                  <Point X="2.291697021484" Y="-2.878354248047" />
                  <Point X="2.986673828125" Y="-4.082088623047" />
                  <Point X="2.848428955078" Y="-4.180833496094" />
                  <Point X="2.835302734375" Y="-4.190208984375" />
                  <Point X="2.679775634766" Y="-4.29087890625" />
                  <Point X="2.631732421875" Y="-4.228268066406" />
                  <Point X="1.683177612305" Y="-2.992087402344" />
                  <Point X="1.670549316406" Y="-2.980467529297" />
                  <Point X="1.47326171875" Y="-2.853629638672" />
                  <Point X="1.45742590332" Y="-2.843448730469" />
                  <Point X="1.4258046875" Y="-2.835717041016" />
                  <Point X="1.210036987305" Y="-2.855572265625" />
                  <Point X="1.192717773438" Y="-2.857166015625" />
                  <Point X="1.165332885742" Y="-2.868509033203" />
                  <Point X="0.998722717285" Y="-3.007040039063" />
                  <Point X="0.985349243164" Y="-3.018159423828" />
                  <Point X="0.968456726074" Y="-3.045986328125" />
                  <Point X="0.918641052246" Y="-3.275177490234" />
                  <Point X="0.91392956543" Y="-3.310720214844" />
                  <Point X="0.930254394531" Y="-3.434720214844" />
                  <Point X="1.127642211914" Y="-4.934028320312" />
                  <Point X="1.006764038086" Y="-4.960524902344" />
                  <Point X="0.994344421387" Y="-4.963247070312" />
                  <Point X="0.860200500488" Y="-4.987616699219" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#210" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.17888588467" Y="5.022735717989" Z="2.35" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="2.35" />
                  <Point X="-0.251997501656" Y="5.069446858853" Z="2.35" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="2.35" />
                  <Point X="-1.040921814484" Y="4.967811911153" Z="2.35" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="2.35" />
                  <Point X="-1.710831794447" Y="4.467379908761" Z="2.35" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="2.35" />
                  <Point X="-1.710044342814" Y="4.435573668988" Z="2.35" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="2.35" />
                  <Point X="-1.747294078111" Y="4.337751762799" Z="2.35" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="2.35" />
                  <Point X="-1.846173908459" Y="4.303407543494" Z="2.35" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="2.35" />
                  <Point X="-2.119431006362" Y="4.590539045953" Z="2.35" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="2.35" />
                  <Point X="-2.182753310146" Y="4.582978036368" Z="2.35" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="2.35" />
                  <Point X="-2.830528703279" Y="4.213799789576" Z="2.35" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="2.35" />
                  <Point X="-3.029547711361" Y="3.188850209572" Z="2.35" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="2.35" />
                  <Point X="-3.000968552032" Y="3.133956324779" Z="2.35" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="2.35" />
                  <Point X="-2.998551962028" Y="3.050251605858" Z="2.35" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="2.35" />
                  <Point X="-3.061120140828" Y="2.994596147141" Z="2.35" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="2.35" />
                  <Point X="-3.745009138725" Y="3.350646237385" Z="2.35" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="2.35" />
                  <Point X="-3.824317522902" Y="3.339117366544" Z="2.35" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="2.35" />
                  <Point X="-4.232936940108" Y="2.803085765564" Z="2.35" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="2.35" />
                  <Point X="-3.759801084296" Y="1.659358949662" Z="2.35" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="2.35" />
                  <Point X="-3.694352525582" Y="1.606589202827" Z="2.35" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="2.35" />
                  <Point X="-3.668653818099" Y="1.549283027099" Z="2.35" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="2.35" />
                  <Point X="-3.696034064265" Y="1.492761038834" Z="2.35" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="2.35" />
                  <Point X="-4.737467093253" Y="1.604453808156" Z="2.35" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="2.35" />
                  <Point X="-4.828112066969" Y="1.571990917284" Z="2.35" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="2.35" />
                  <Point X="-4.979332020417" Y="0.993999727868" Z="2.35" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="2.35" />
                  <Point X="-3.686809225746" Y="0.078610026401" Z="2.35" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="2.35" />
                  <Point X="-3.57449860997" Y="0.047637799053" Z="2.35" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="2.35" />
                  <Point X="-3.548120472366" Y="0.027592201131" Z="2.35" />
                  <Point X="-3.539556741714" Y="0" Z="2.35" />
                  <Point X="-3.540244128769" Y="-0.00221474993" Z="2.35" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="2.35" />
                  <Point X="-3.550869985154" Y="-0.03123818408" Z="2.35" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="2.35" />
                  <Point X="-4.950078563027" Y="-0.417102030355" Z="2.35" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="2.35" />
                  <Point X="-5.054556275591" Y="-0.48699169214" Z="2.35" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="2.35" />
                  <Point X="-4.972574449274" Y="-1.029162117895" Z="2.35" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="2.35" />
                  <Point X="-3.340105403614" Y="-1.322786215772" Z="2.35" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="2.35" />
                  <Point X="-3.217191148972" Y="-1.308021433436" Z="2.35" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="2.35" />
                  <Point X="-3.193248774011" Y="-1.324660125993" Z="2.35" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="2.35" />
                  <Point X="-4.406119295095" Y="-2.277392817281" Z="2.35" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="2.35" />
                  <Point X="-4.481089232743" Y="-2.388230055878" Z="2.35" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="2.35" />
                  <Point X="-4.183563070699" Y="-2.877772214814" Z="2.35" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="2.35" />
                  <Point X="-2.668645916043" Y="-2.610804830791" Z="2.35" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="2.35" />
                  <Point X="-2.571550448496" Y="-2.556780020635" Z="2.35" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="2.35" />
                  <Point X="-3.244612182378" Y="-3.766432466449" Z="2.35" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="2.35" />
                  <Point X="-3.269502584184" Y="-3.885663933607" Z="2.35" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="2.35" />
                  <Point X="-2.857829852465" Y="-4.19771553676" Z="2.35" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="2.35" />
                  <Point X="-2.242932715858" Y="-4.178229639661" Z="2.35" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="2.35" />
                  <Point X="-2.207054573177" Y="-4.143644710087" Z="2.35" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="2.35" />
                  <Point X="-1.945252552258" Y="-3.985592094797" Z="2.35" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="2.35" />
                  <Point X="-1.641334835058" Y="-4.01957572371" Z="2.35" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="2.35" />
                  <Point X="-1.420908600279" Y="-4.231550335332" Z="2.35" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="2.35" />
                  <Point X="-1.409516115033" Y="-4.852288164892" Z="2.35" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="2.35" />
                  <Point X="-1.391127820433" Y="-4.885156256632" Z="2.35" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="2.35" />
                  <Point X="-1.095130680207" Y="-4.959906482645" Z="2.35" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="2.35" />
                  <Point X="-0.446851226788" Y="-3.629855514263" Z="2.35" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="2.35" />
                  <Point X="-0.404921298506" Y="-3.501244939329" Z="2.35" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="2.35" />
                  <Point X="-0.234533705304" Y="-3.277029975463" Z="2.35" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="2.35" />
                  <Point X="0.018825374057" Y="-3.210082069825" Z="2.35" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="2.35" />
                  <Point X="0.265524560927" Y="-3.300400816439" Z="2.35" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="2.35" />
                  <Point X="0.787903633613" Y="-4.902680515553" Z="2.35" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="2.35" />
                  <Point X="0.831068062241" Y="-5.011328663234" Z="2.35" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="2.35" />
                  <Point X="1.011313601206" Y="-4.978085270126" Z="2.35" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="2.35" />
                  <Point X="0.973670716939" Y="-3.396913858298" Z="2.35" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="2.35" />
                  <Point X="0.96134434992" Y="-3.254517153334" Z="2.35" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="2.35" />
                  <Point X="1.024533371954" Y="-3.014206506676" Z="2.35" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="2.35" />
                  <Point X="1.208462824234" Y="-2.874081722975" Z="2.35" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="2.35" />
                  <Point X="1.440066196293" Y="-2.86440750682" Z="2.35" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="2.35" />
                  <Point X="2.585908784678" Y="-4.227425814576" Z="2.35" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="2.35" />
                  <Point X="2.676552642333" Y="-4.317261183741" Z="2.35" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="2.35" />
                  <Point X="2.871334631609" Y="-4.190240521488" Z="2.35" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="2.35" />
                  <Point X="2.328842312604" Y="-2.822074087832" Z="2.35" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="2.35" />
                  <Point X="2.268337158424" Y="-2.706242424786" Z="2.35" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="2.35" />
                  <Point X="2.239231205178" Y="-2.492869432224" Z="2.35" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="2.35" />
                  <Point X="2.340028946148" Y="-2.31967010479" Z="2.35" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="2.35" />
                  <Point X="2.522264378726" Y="-2.235110851067" Z="2.35" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="2.35" />
                  <Point X="3.965339509275" Y="-2.988907187671" Z="2.35" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="2.35" />
                  <Point X="4.078088843168" Y="-3.028078524101" Z="2.35" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="2.35" />
                  <Point X="4.251572583624" Y="-2.779244091748" Z="2.35" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="2.35" />
                  <Point X="3.282387010763" Y="-1.683379441626" Z="2.35" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="2.35" />
                  <Point X="3.185276821845" Y="-1.602980152475" Z="2.35" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="2.35" />
                  <Point X="3.093431216829" Y="-1.445601843084" Z="2.35" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="2.35" />
                  <Point X="3.116145785658" Y="-1.2775651056" Z="2.35" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="2.35" />
                  <Point X="3.231226328077" Y="-1.152451901929" Z="2.35" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="2.35" />
                  <Point X="4.79497939161" Y="-1.299665106759" Z="2.35" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="2.35" />
                  <Point X="4.913280276789" Y="-1.286922294118" Z="2.35" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="2.35" />
                  <Point X="4.995642217184" Y="-0.916539676377" Z="2.35" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="2.35" />
                  <Point X="3.844551413938" Y="-0.24669418317" Z="2.35" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="2.35" />
                  <Point X="3.741078917693" Y="-0.216837467691" Z="2.35" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="2.35" />
                  <Point X="3.65131844551" Y="-0.162082919244" Z="2.35" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="2.35" />
                  <Point X="3.598561967316" Y="-0.089432065802" Z="2.35" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="2.35" />
                  <Point X="3.582809483109" Y="0.007178465376" Z="2.35" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="2.35" />
                  <Point X="3.60406099289" Y="0.101865819331" Z="2.35" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="2.35" />
                  <Point X="3.662316496659" Y="0.171311376551" Z="2.35" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="2.35" />
                  <Point X="4.951416403369" Y="0.543277749599" Z="2.35" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="2.35" />
                  <Point X="5.043118375074" Y="0.60061227408" Z="2.35" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="2.35" />
                  <Point X="4.974583939535" Y="1.023367223599" Z="2.35" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="2.35" />
                  <Point X="3.568458298563" Y="1.235891951323" Z="2.35" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="2.35" />
                  <Point X="3.456124971839" Y="1.222948748224" Z="2.35" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="2.35" />
                  <Point X="3.363785995021" Y="1.237381444382" Z="2.35" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="2.35" />
                  <Point X="3.2957477249" Y="1.279098531259" Z="2.35" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="2.35" />
                  <Point X="3.249948136548" Y="1.353079189599" Z="2.35" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="2.35" />
                  <Point X="3.235191330549" Y="1.438067908525" Z="2.35" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="2.35" />
                  <Point X="3.259409382524" Y="1.514914776844" Z="2.35" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="2.35" />
                  <Point X="4.363021381676" Y="2.390482962493" Z="2.35" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="2.35" />
                  <Point X="4.431773019248" Y="2.48083946111" Z="2.35" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="2.35" />
                  <Point X="4.220654170397" Y="2.825109846546" Z="2.35" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="2.35" />
                  <Point X="2.620766674689" Y="2.331020416483" Z="2.35" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="2.35" />
                  <Point X="2.50391239895" Y="2.265403509767" Z="2.35" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="2.35" />
                  <Point X="2.42443317037" Y="2.246151192594" Z="2.35" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="2.35" />
                  <Point X="2.355462751998" Y="2.2570924082" Z="2.35" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="2.35" />
                  <Point X="2.293666192035" Y="2.301562108384" Z="2.35" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="2.35" />
                  <Point X="2.253278510165" Y="2.365325273462" Z="2.35" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="2.35" />
                  <Point X="2.247124432891" Y="2.435557176932" Z="2.35" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="2.35" />
                  <Point X="3.064604936376" Y="3.891372407266" Z="2.35" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="2.35" />
                  <Point X="3.100753342881" Y="4.02208307171" Z="2.35" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="2.35" />
                  <Point X="2.72394487246" Y="4.286249628217" Z="2.35" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="2.35" />
                  <Point X="2.325169779958" Y="4.515060697623" Z="2.35" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="2.35" />
                  <Point X="1.912281979878" Y="4.704822136772" Z="2.35" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="2.35" />
                  <Point X="1.46813003542" Y="4.860024372681" Z="2.35" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="2.35" />
                  <Point X="0.812826526386" Y="5.011436253047" Z="2.35" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="2.35" />
                  <Point X="0.014358676883" Y="4.408711787962" Z="2.35" />
                  <Point X="0" Y="4.355124473572" Z="2.35" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>