<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#127" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="605" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="0" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="0.004715820312" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.6029296875" Y="-3.660416503906" />
                  <Point X="0.563302062988" Y="-3.512524414063" />
                  <Point X="0.557721191406" Y="-3.497141845703" />
                  <Point X="0.542363342285" Y="-3.467377441406" />
                  <Point X="0.526166564941" Y="-3.444040771484" />
                  <Point X="0.378635681152" Y="-3.231477050781" />
                  <Point X="0.356751586914" Y="-3.209021484375" />
                  <Point X="0.330496398926" Y="-3.189777587891" />
                  <Point X="0.302495178223" Y="-3.175669189453" />
                  <Point X="0.277431549072" Y="-3.167890380859" />
                  <Point X="0.049136188507" Y="-3.097035888672" />
                  <Point X="0.020976791382" Y="-3.092766601562" />
                  <Point X="-0.00866468811" Y="-3.092766601562" />
                  <Point X="-0.036824237823" Y="-3.097035888672" />
                  <Point X="-0.061887718201" Y="-3.104814697266" />
                  <Point X="-0.290183380127" Y="-3.175669189453" />
                  <Point X="-0.31818460083" Y="-3.189777587891" />
                  <Point X="-0.344439788818" Y="-3.209021484375" />
                  <Point X="-0.366323577881" Y="-3.231476806641" />
                  <Point X="-0.382520355225" Y="-3.254813232422" />
                  <Point X="-0.530051208496" Y="-3.467377197266" />
                  <Point X="-0.538189025879" Y="-3.481573730469" />
                  <Point X="-0.550990112305" Y="-3.512524414063" />
                  <Point X="-0.911943603516" Y="-4.859622070312" />
                  <Point X="-0.916584472656" Y="-4.876941894531" />
                  <Point X="-1.079339233398" Y="-4.845350585938" />
                  <Point X="-1.104366821289" Y="-4.838911132812" />
                  <Point X="-1.24641809082" Y="-4.802362792969" />
                  <Point X="-1.219733276367" Y="-4.599671875" />
                  <Point X="-1.214963012695" Y="-4.563438476562" />
                  <Point X="-1.214201171875" Y="-4.5479296875" />
                  <Point X="-1.216508666992" Y="-4.516223632812" />
                  <Point X="-1.222496459961" Y="-4.486121582031" />
                  <Point X="-1.277036132812" Y="-4.211932128906" />
                  <Point X="-1.287938354492" Y="-4.182966308594" />
                  <Point X="-1.304010375977" Y="-4.15512890625" />
                  <Point X="-1.32364453125" Y="-4.131204589844" />
                  <Point X="-1.346719970703" Y="-4.110967773438" />
                  <Point X="-1.556905273438" Y="-3.926639404297" />
                  <Point X="-1.583188476562" Y="-3.910295410156" />
                  <Point X="-1.612885742188" Y="-3.897994384766" />
                  <Point X="-1.64302734375" Y="-3.890966308594" />
                  <Point X="-1.673653686523" Y="-3.888958984375" />
                  <Point X="-1.952616577148" Y="-3.870674804688" />
                  <Point X="-1.98341796875" Y="-3.873708496094" />
                  <Point X="-2.014466674805" Y="-3.882028076172" />
                  <Point X="-2.042657836914" Y="-3.894801513672" />
                  <Point X="-2.068177246094" Y="-3.911853027344" />
                  <Point X="-2.300624267578" Y="-4.067169677734" />
                  <Point X="-2.312785644531" Y="-4.076821533203" />
                  <Point X="-2.335102783203" Y="-4.099461914062" />
                  <Point X="-2.480149169922" Y="-4.288490234375" />
                  <Point X="-2.801708007812" Y="-4.089388427734" />
                  <Point X="-2.83636328125" Y="-4.062705078125" />
                  <Point X="-3.104721923828" Y="-3.856077880859" />
                  <Point X="-2.497404541016" Y="-2.804173583984" />
                  <Point X="-2.423761230469" Y="-2.676619628906" />
                  <Point X="-2.412859130859" Y="-2.647653320312" />
                  <Point X="-2.406588134766" Y="-2.616126953125" />
                  <Point X="-2.405721923828" Y="-2.584127197266" />
                  <Point X="-2.415533447266" Y="-2.553656494141" />
                  <Point X="-2.431466796875" Y="-2.523111328125" />
                  <Point X="-2.448520996094" Y="-2.499873046875" />
                  <Point X="-2.464153320312" Y="-2.484240722656" />
                  <Point X="-2.489310302734" Y="-2.466213134766" />
                  <Point X="-2.518139160156" Y="-2.45199609375" />
                  <Point X="-2.547756835938" Y="-2.443011474609" />
                  <Point X="-2.578690917969" Y="-2.444024169922" />
                  <Point X="-2.610217285156" Y="-2.450295166016" />
                  <Point X="-2.63918359375" Y="-2.461197265625" />
                  <Point X="-3.801028808594" Y="-3.131989013672" />
                  <Point X="-3.818024169922" Y="-3.141801269531" />
                  <Point X="-4.082859619141" Y="-2.793861816406" />
                  <Point X="-4.107701660156" Y="-2.752205322266" />
                  <Point X="-4.306142578125" Y="-2.419450439453" />
                  <Point X="-3.234806640625" Y="-1.597385009766" />
                  <Point X="-3.105954589844" Y="-1.498513305664" />
                  <Point X="-3.083143798828" Y="-1.473361206055" />
                  <Point X="-3.064416748047" Y="-1.443287231445" />
                  <Point X="-3.053094238281" Y="-1.416889892578" />
                  <Point X="-3.046151855469" Y="-1.390085327148" />
                  <Point X="-3.04334765625" Y="-1.35965612793" />
                  <Point X="-3.045556640625" Y="-1.327985595703" />
                  <Point X="-3.053072753906" Y="-1.297021484375" />
                  <Point X="-3.070404785156" Y="-1.270284301758" />
                  <Point X="-3.093857177734" Y="-1.244786010742" />
                  <Point X="-3.115592285156" Y="-1.227225219727" />
                  <Point X="-3.139455078125" Y="-1.213180664062" />
                  <Point X="-3.168718261719" Y="-1.201956420898" />
                  <Point X="-3.200605957031" Y="-1.195474853516" />
                  <Point X="-3.231929199219" Y="-1.194383789062" />
                  <Point X="-4.698648925781" Y="-1.387481079102" />
                  <Point X="-4.732102050781" Y="-1.391885375977" />
                  <Point X="-4.834077148438" Y="-0.992657104492" />
                  <Point X="-4.840649902344" Y="-0.946702209473" />
                  <Point X="-4.892424316406" Y="-0.584698181152" />
                  <Point X="-3.679657714844" Y="-0.259738311768" />
                  <Point X="-3.532875976562" Y="-0.220408340454" />
                  <Point X="-3.51509375" Y="-0.213673400879" />
                  <Point X="-3.496780273438" Y="-0.204547775269" />
                  <Point X="-3.484983398438" Y="-0.197564041138" />
                  <Point X="-3.459975830078" Y="-0.18020741272" />
                  <Point X="-3.435960693359" Y="-0.156131027222" />
                  <Point X="-3.415801757812" Y="-0.126927314758" />
                  <Point X="-3.403253417969" Y="-0.101119148254" />
                  <Point X="-3.394917480469" Y="-0.074260902405" />
                  <Point X="-3.390685302734" Y="-0.043434848785" />
                  <Point X="-3.391600341797" Y="-0.010844231606" />
                  <Point X="-3.395832519531" Y="0.014649422646" />
                  <Point X="-3.404168457031" Y="0.041507671356" />
                  <Point X="-3.420022460938" Y="0.071815971375" />
                  <Point X="-3.442011962891" Y="0.099976577759" />
                  <Point X="-3.462721191406" Y="0.119552688599" />
                  <Point X="-3.487728515625" Y="0.136909164429" />
                  <Point X="-3.501924560547" Y="0.145046646118" />
                  <Point X="-3.532875976562" Y="0.157848342896" />
                  <Point X="-4.869860351562" Y="0.516092224121" />
                  <Point X="-4.891815917969" Y="0.521975280762" />
                  <Point X="-4.824487792969" Y="0.97697442627" />
                  <Point X="-4.811256347656" Y="1.025802856445" />
                  <Point X="-4.703551269531" Y="1.423267822266" />
                  <Point X="-3.870883300781" Y="1.313645019531" />
                  <Point X="-3.765666259766" Y="1.29979284668" />
                  <Point X="-3.744985839844" Y="1.299341674805" />
                  <Point X="-3.723424072266" Y="1.301228271484" />
                  <Point X="-3.703137451172" Y="1.305263671875" />
                  <Point X="-3.697061035156" Y="1.30717956543" />
                  <Point X="-3.641711425781" Y="1.324631225586" />
                  <Point X="-3.622778564453" Y="1.332961669922" />
                  <Point X="-3.604034423828" Y="1.343783569336" />
                  <Point X="-3.587353515625" Y="1.356014648438" />
                  <Point X="-3.573715087891" Y="1.37156628418" />
                  <Point X="-3.561300537109" Y="1.389296020508" />
                  <Point X="-3.551351074219" Y="1.407431274414" />
                  <Point X="-3.548912841797" Y="1.413317749023" />
                  <Point X="-3.526703613281" Y="1.466935546875" />
                  <Point X="-3.520915527344" Y="1.486794067383" />
                  <Point X="-3.517157226562" Y="1.508109130859" />
                  <Point X="-3.5158046875" Y="1.528749145508" />
                  <Point X="-3.518951171875" Y="1.549192749023" />
                  <Point X="-3.524552978516" Y="1.570099121094" />
                  <Point X="-3.532049072266" Y="1.589376098633" />
                  <Point X="-3.534990966797" Y="1.595027709961" />
                  <Point X="-3.561788818359" Y="1.646505859375" />
                  <Point X="-3.57328125" Y="1.663706054688" />
                  <Point X="-3.587193603516" Y="1.680286254883" />
                  <Point X="-3.602135742188" Y="1.694590209961" />
                  <Point X="-4.351860351563" Y="2.269874267578" />
                  <Point X="-4.081153808594" Y="2.733659423828" />
                  <Point X="-4.046107910156" Y="2.778705810547" />
                  <Point X="-3.750504882813" Y="3.158661865234" />
                  <Point X="-3.271278320312" Y="2.881979980469" />
                  <Point X="-3.206656982422" Y="2.844670898438" />
                  <Point X="-3.187724121094" Y="2.836340332031" />
                  <Point X="-3.167081787109" Y="2.829831787109" />
                  <Point X="-3.146794921875" Y="2.825796386719" />
                  <Point X="-3.138331787109" Y="2.825055908203" />
                  <Point X="-3.061245605469" Y="2.818311767578" />
                  <Point X="-3.040564697266" Y="2.818762939453" />
                  <Point X="-3.019106201172" Y="2.821588134766" />
                  <Point X="-2.999014892578" Y="2.826504638672" />
                  <Point X="-2.980463867188" Y="2.835652832031" />
                  <Point X="-2.962209472656" Y="2.847281982422" />
                  <Point X="-2.946077392578" Y="2.860229492188" />
                  <Point X="-2.9400703125" Y="2.866236572266" />
                  <Point X="-2.885353759766" Y="2.920952880859" />
                  <Point X="-2.872406738281" Y="2.937084228516" />
                  <Point X="-2.86077734375" Y="2.955338623047" />
                  <Point X="-2.851628662109" Y="2.973890136719" />
                  <Point X="-2.846712158203" Y="2.993981933594" />
                  <Point X="-2.843886962891" Y="3.015440673828" />
                  <Point X="-2.843435791016" Y="3.036121337891" />
                  <Point X="-2.844176269531" Y="3.044584472656" />
                  <Point X="-2.850920410156" Y="3.121670898438" />
                  <Point X="-2.854955810547" Y="3.141957763672" />
                  <Point X="-2.861464355469" Y="3.162600097656" />
                  <Point X="-2.869794921875" Y="3.181532958984" />
                  <Point X="-3.183333007812" Y="3.724596435547" />
                  <Point X="-2.700624023438" Y="4.094684570313" />
                  <Point X="-2.645431396484" Y="4.125348632812" />
                  <Point X="-2.167036865234" Y="4.391134277344" />
                  <Point X="-2.062978027344" Y="4.255521972656" />
                  <Point X="-2.04319543457" Y="4.229740722656" />
                  <Point X="-2.028892456055" Y="4.214799804688" />
                  <Point X="-2.01231237793" Y="4.200887207031" />
                  <Point X="-1.995113037109" Y="4.18939453125" />
                  <Point X="-1.985693725586" Y="4.184491210938" />
                  <Point X="-1.899896850586" Y="4.139827636719" />
                  <Point X="-1.8806171875" Y="4.132330566406" />
                  <Point X="-1.859710571289" Y="4.126729003906" />
                  <Point X="-1.839267578125" Y="4.123582519531" />
                  <Point X="-1.818628295898" Y="4.124935546875" />
                  <Point X="-1.797312988281" Y="4.128693847656" />
                  <Point X="-1.777452392578" Y="4.134482421875" />
                  <Point X="-1.767641601562" Y="4.138546386719" />
                  <Point X="-1.678278442383" Y="4.175562011719" />
                  <Point X="-1.66014465332" Y="4.185510742187" />
                  <Point X="-1.642415039062" Y="4.197925292969" />
                  <Point X="-1.626863525391" Y="4.211563964844" />
                  <Point X="-1.614632568359" Y="4.228245117188" />
                  <Point X="-1.603810791016" Y="4.246989257813" />
                  <Point X="-1.595480834961" Y="4.265920410156" />
                  <Point X="-1.592287353516" Y="4.276048339844" />
                  <Point X="-1.563201416016" Y="4.368296875" />
                  <Point X="-1.559165649414" Y="4.388584960938" />
                  <Point X="-1.557279174805" Y="4.410146484375" />
                  <Point X="-1.55773034668" Y="4.430826660156" />
                  <Point X="-1.584201904297" Y="4.631897949219" />
                  <Point X="-0.949634765625" Y="4.80980859375" />
                  <Point X="-0.882730773926" Y="4.817638671875" />
                  <Point X="-0.294710784912" Y="4.886458007812" />
                  <Point X="-0.153020095825" Y="4.35766015625" />
                  <Point X="-0.133903305054" Y="4.286315429688" />
                  <Point X="-0.121129844666" Y="4.258124023438" />
                  <Point X="-0.10327155304" Y="4.231397460938" />
                  <Point X="-0.082114532471" Y="4.20880859375" />
                  <Point X="-0.054819446564" Y="4.19421875" />
                  <Point X="-0.024381370544" Y="4.183886230469" />
                  <Point X="0.006155906677" Y="4.178844238281" />
                  <Point X="0.03669316864" Y="4.183886230469" />
                  <Point X="0.067131248474" Y="4.19421875" />
                  <Point X="0.094426467896" Y="4.20880859375" />
                  <Point X="0.115583503723" Y="4.231397460938" />
                  <Point X="0.133441802979" Y="4.258124023438" />
                  <Point X="0.146215255737" Y="4.286315429688" />
                  <Point X="0.307419311523" Y="4.8879375" />
                  <Point X="0.844041320801" Y="4.831738769531" />
                  <Point X="0.899399902344" Y="4.818373535156" />
                  <Point X="1.481025390625" Y="4.677951171875" />
                  <Point X="1.51572253418" Y="4.665366210938" />
                  <Point X="1.894645385742" Y="4.527928222656" />
                  <Point X="1.929491088867" Y="4.511631835938" />
                  <Point X="2.294577880859" Y="4.340893066406" />
                  <Point X="2.328271728516" Y="4.321262695312" />
                  <Point X="2.680976074219" Y="4.115776367188" />
                  <Point X="2.712730224609" Y="4.093195068359" />
                  <Point X="2.943260009766" Y="3.929254882812" />
                  <Point X="2.232817626953" Y="2.698732177734" />
                  <Point X="2.147581054688" Y="2.551097900391" />
                  <Point X="2.140350585938" Y="2.535444580078" />
                  <Point X="2.133225097656" Y="2.515418945312" />
                  <Point X="2.130952880859" Y="2.508114013672" />
                  <Point X="2.111607177734" Y="2.435770019531" />
                  <Point X="2.108400878906" Y="2.413106201172" />
                  <Point X="2.107891357422" Y="2.387336181641" />
                  <Point X="2.108555908203" Y="2.374085449219" />
                  <Point X="2.116099121094" Y="2.311528320312" />
                  <Point X="2.121442138672" Y="2.289604980469" />
                  <Point X="2.129708251953" Y="2.267516357422" />
                  <Point X="2.140072265625" Y="2.247469238281" />
                  <Point X="2.144322021484" Y="2.241206542969" />
                  <Point X="2.183030029297" Y="2.184160644531" />
                  <Point X="2.198200927734" Y="2.166789550781" />
                  <Point X="2.217763671875" Y="2.149239501953" />
                  <Point X="2.227861816406" Y="2.141342529297" />
                  <Point X="2.284907714844" Y="2.102634765625" />
                  <Point X="2.304952880859" Y="2.092271972656" />
                  <Point X="2.327041259766" Y="2.084006103516" />
                  <Point X="2.3489609375" Y="2.078663818359" />
                  <Point X="2.355828857422" Y="2.077835449219" />
                  <Point X="2.418385742188" Y="2.070292236328" />
                  <Point X="2.441911376953" Y="2.070388671875" />
                  <Point X="2.468756347656" Y="2.073850341797" />
                  <Point X="2.4811484375" Y="2.076295166016" />
                  <Point X="2.553492431641" Y="2.095640869141" />
                  <Point X="2.565283447266" Y="2.099638183594" />
                  <Point X="2.588533935547" Y="2.110145019531" />
                  <Point X="3.933282714844" Y="2.886536132812" />
                  <Point X="3.967326416016" Y="2.906191162109" />
                  <Point X="4.123270507813" Y="2.689464355469" />
                  <Point X="4.140973632812" Y="2.660209960938" />
                  <Point X="4.262198730469" Y="2.459884277344" />
                  <Point X="3.343005859375" Y="1.754562133789" />
                  <Point X="3.230783935547" Y="1.668451171875" />
                  <Point X="3.217845703125" Y="1.656458862305" />
                  <Point X="3.202884765625" Y="1.639752197266" />
                  <Point X="3.1982578125" Y="1.634170776367" />
                  <Point X="3.146191650391" Y="1.566246582031" />
                  <Point X="3.134361572266" Y="1.546086181641" />
                  <Point X="3.123762695312" Y="1.521520629883" />
                  <Point X="3.119500488281" Y="1.509472167969" />
                  <Point X="3.100105957031" Y="1.440121459961" />
                  <Point X="3.096652587891" Y="1.417823242188" />
                  <Point X="3.095836425781" Y="1.394252929688" />
                  <Point X="3.097739257812" Y="1.371769165039" />
                  <Point X="3.099487060547" Y="1.363297973633" />
                  <Point X="3.115408203125" Y="1.286136474609" />
                  <Point X="3.122997558594" Y="1.263821777344" />
                  <Point X="3.134949707031" Y="1.239217895508" />
                  <Point X="3.141036865234" Y="1.228514038086" />
                  <Point X="3.184340332031" Y="1.162694946289" />
                  <Point X="3.198893554688" Y="1.145450439453" />
                  <Point X="3.216137451172" Y="1.129360595703" />
                  <Point X="3.234346679688" Y="1.11603503418" />
                  <Point X="3.241235839844" Y="1.112156982422" />
                  <Point X="3.303988769531" Y="1.076832763672" />
                  <Point X="3.326264404297" Y="1.067784667969" />
                  <Point X="3.353554443359" Y="1.060555786133" />
                  <Point X="3.365432861328" Y="1.058207519531" />
                  <Point X="3.450278808594" Y="1.046994018555" />
                  <Point X="3.462702148438" Y="1.046175048828" />
                  <Point X="3.488203613281" Y="1.04698449707" />
                  <Point X="4.765625" Y="1.21516027832" />
                  <Point X="4.77683984375" Y="1.21663671875" />
                  <Point X="4.845936523438" Y="0.932809448242" />
                  <Point X="4.851514160156" Y="0.896981445312" />
                  <Point X="4.890864746094" Y="0.644238464355" />
                  <Point X="3.845188720703" Y="0.364050445557" />
                  <Point X="3.716579833984" Y="0.329589935303" />
                  <Point X="3.699712402344" Y="0.323304748535" />
                  <Point X="3.678479980469" Y="0.313007446289" />
                  <Point X="3.672394042969" Y="0.309778106689" />
                  <Point X="3.589035644531" Y="0.261595428467" />
                  <Point X="3.570065917969" Y="0.247179733276" />
                  <Point X="3.550262939453" Y="0.227762695312" />
                  <Point X="3.542039794922" Y="0.218579833984" />
                  <Point X="3.492024902344" Y="0.154848815918" />
                  <Point X="3.48030078125" Y="0.13556854248" />
                  <Point X="3.470527099609" Y="0.114104927063" />
                  <Point X="3.463680908203" Y="0.092603965759" />
                  <Point X="3.461850585938" Y="0.083046638489" />
                  <Point X="3.445178710938" Y="-0.004006529331" />
                  <Point X="3.443680175781" Y="-0.027992298126" />
                  <Point X="3.445510498047" Y="-0.056359043121" />
                  <Point X="3.447009033203" Y="-0.068110961914" />
                  <Point X="3.463680908203" Y="-0.155164428711" />
                  <Point X="3.470527099609" Y="-0.176665084839" />
                  <Point X="3.48030078125" Y="-0.198128707886" />
                  <Point X="3.492025146484" Y="-0.21740927124" />
                  <Point X="3.497516113281" Y="-0.224406051636" />
                  <Point X="3.547531005859" Y="-0.288136932373" />
                  <Point X="3.565032714844" Y="-0.305312011719" />
                  <Point X="3.588496337891" Y="-0.323022155762" />
                  <Point X="3.598187255859" Y="-0.329445465088" />
                  <Point X="3.681545654297" Y="-0.377628143311" />
                  <Point X="3.692710205078" Y="-0.383139038086" />
                  <Point X="3.716579833984" Y="-0.392149932861" />
                  <Point X="4.888033203125" Y="-0.706039733887" />
                  <Point X="4.891472167969" Y="-0.706961242676" />
                  <Point X="4.855022460938" Y="-0.948726013184" />
                  <Point X="4.847875488281" Y="-0.980044677734" />
                  <Point X="4.801173339844" Y="-1.184698974609" />
                  <Point X="3.573532226562" Y="-1.023076965332" />
                  <Point X="3.424382080078" Y="-1.003441040039" />
                  <Point X="3.399595703125" Y="-1.003439208984" />
                  <Point X="3.36448828125" Y="-1.008056091309" />
                  <Point X="3.356697265625" Y="-1.009412597656" />
                  <Point X="3.193094238281" Y="-1.044972412109" />
                  <Point X="3.162724121094" Y="-1.055693725586" />
                  <Point X="3.130097900391" Y="-1.077427124023" />
                  <Point X="3.106741455078" Y="-1.101258056641" />
                  <Point X="3.101541015625" Y="-1.107017089844" />
                  <Point X="3.002653320312" Y="-1.225948364258" />
                  <Point X="2.986626464844" Y="-1.250417114258" />
                  <Point X="2.974202148438" Y="-1.284166870117" />
                  <Point X="2.969050537109" Y="-1.31562902832" />
                  <Point X="2.968201660156" Y="-1.322274658203" />
                  <Point X="2.954028564453" Y="-1.476295654297" />
                  <Point X="2.955109130859" Y="-1.508482543945" />
                  <Point X="2.966159912109" Y="-1.546415161133" />
                  <Point X="2.982611328125" Y="-1.577040039062" />
                  <Point X="2.986390380859" Y="-1.583457885742" />
                  <Point X="3.076930664062" Y="-1.724287109375" />
                  <Point X="3.086932861328" Y="-1.737238647461" />
                  <Point X="3.110628417969" Y="-1.760909179688" />
                  <Point X="4.197744140625" Y="-2.595083007812" />
                  <Point X="4.213122558594" Y="-2.606883056641" />
                  <Point X="4.12480859375" Y="-2.749788085938" />
                  <Point X="4.110028808594" Y="-2.770787841797" />
                  <Point X="4.028981445312" Y="-2.885945068359" />
                  <Point X="2.93379296875" Y="-2.253637451172" />
                  <Point X="2.800954589844" Y="-2.176943115234" />
                  <Point X="2.786131591797" Y="-2.170012451172" />
                  <Point X="2.754224609375" Y="-2.159825195312" />
                  <Point X="2.732847900391" Y="-2.155964599609" />
                  <Point X="2.538134277344" Y="-2.120799560547" />
                  <Point X="2.506781982422" Y="-2.120395263672" />
                  <Point X="2.474609130859" Y="-2.125353027344" />
                  <Point X="2.444834228516" Y="-2.135176269531" />
                  <Point X="2.427075195313" Y="-2.144522460938" />
                  <Point X="2.265316162109" Y="-2.229655273438" />
                  <Point X="2.242384033203" Y="-2.246549072266" />
                  <Point X="2.221424316406" Y="-2.267509033203" />
                  <Point X="2.204531982422" Y="-2.290438964844" />
                  <Point X="2.195185546875" Y="-2.308197753906" />
                  <Point X="2.110052978516" Y="-2.46995703125" />
                  <Point X="2.100229003906" Y="-2.499733398438" />
                  <Point X="2.095271240234" Y="-2.531906005859" />
                  <Point X="2.095675537109" Y="-2.563258300781" />
                  <Point X="2.099536132813" Y="-2.584635009766" />
                  <Point X="2.134701171875" Y="-2.779348632812" />
                  <Point X="2.138985595703" Y="-2.795141845703" />
                  <Point X="2.151819091797" Y="-2.826078613281" />
                  <Point X="2.850400634766" Y="-4.036056640625" />
                  <Point X="2.861283447266" Y="-4.054906494141" />
                  <Point X="2.781850341797" Y="-4.111643554688" />
                  <Point X="2.765324951172" Y="-4.122340332031" />
                  <Point X="2.701765136719" Y="-4.163481445312" />
                  <Point X="1.85991394043" Y="-3.066360351562" />
                  <Point X="1.758546142578" Y="-2.934255126953" />
                  <Point X="1.747503540039" Y="-2.922178955078" />
                  <Point X="1.721924438477" Y="-2.900557617188" />
                  <Point X="1.700841308594" Y="-2.887002929688" />
                  <Point X="1.508801025391" Y="-2.763538818359" />
                  <Point X="1.479989746094" Y="-2.751167236328" />
                  <Point X="1.448368774414" Y="-2.743435546875" />
                  <Point X="1.417099853516" Y="-2.741116699219" />
                  <Point X="1.394041503906" Y="-2.743238525391" />
                  <Point X="1.184012939453" Y="-2.762565673828" />
                  <Point X="1.15636315918" Y="-2.769397460938" />
                  <Point X="1.128978027344" Y="-2.780740722656" />
                  <Point X="1.104596191406" Y="-2.7954609375" />
                  <Point X="1.086791259766" Y="-2.810265136719" />
                  <Point X="0.924612243652" Y="-2.945111328125" />
                  <Point X="0.904141052246" Y="-2.968861816406" />
                  <Point X="0.887248779297" Y="-2.996688476562" />
                  <Point X="0.875624328613" Y="-3.025808837891" />
                  <Point X="0.870300598145" Y="-3.050301513672" />
                  <Point X="0.821810058594" Y="-3.273396728516" />
                  <Point X="0.819724487305" Y="-3.289627441406" />
                  <Point X="0.819742248535" Y="-3.323120117188" />
                  <Point X="1.017714904785" Y="-4.82687109375" />
                  <Point X="1.022065246582" Y="-4.859915527344" />
                  <Point X="0.975680297852" Y="-4.870083007812" />
                  <Point X="0.96040838623" Y="-4.872857421875" />
                  <Point X="0.929315612793" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058432250977" Y="-4.752635742188" />
                  <Point X="-1.080694824219" Y="-4.746907714844" />
                  <Point X="-1.141246459961" Y="-4.731328613281" />
                  <Point X="-1.125546020508" Y="-4.612071777344" />
                  <Point X="-1.120775634766" Y="-4.575838378906" />
                  <Point X="-1.120077514648" Y="-4.568099609375" />
                  <Point X="-1.119451782227" Y="-4.541034179688" />
                  <Point X="-1.121759277344" Y="-4.509328125" />
                  <Point X="-1.123334106445" Y="-4.497689453125" />
                  <Point X="-1.129321899414" Y="-4.467587402344" />
                  <Point X="-1.183861450195" Y="-4.193397949219" />
                  <Point X="-1.188125366211" Y="-4.178467773438" />
                  <Point X="-1.199027587891" Y="-4.149501953125" />
                  <Point X="-1.205666015625" Y="-4.135466308594" />
                  <Point X="-1.221738037109" Y="-4.10762890625" />
                  <Point X="-1.23057434082" Y="-4.094861572266" />
                  <Point X="-1.250208496094" Y="-4.070937255859" />
                  <Point X="-1.261006347656" Y="-4.059780029297" />
                  <Point X="-1.284081787109" Y="-4.039543212891" />
                  <Point X="-1.494267089844" Y="-3.85521484375" />
                  <Point X="-1.506738769531" Y="-3.845965332031" />
                  <Point X="-1.533021972656" Y="-3.829621337891" />
                  <Point X="-1.546833618164" Y="-3.822526855469" />
                  <Point X="-1.576530761719" Y="-3.810225830078" />
                  <Point X="-1.591313476562" Y="-3.805476074219" />
                  <Point X="-1.621454833984" Y="-3.798447998047" />
                  <Point X="-1.636813964844" Y="-3.796169677734" />
                  <Point X="-1.667440429688" Y="-3.794162353516" />
                  <Point X="-1.946403198242" Y="-3.775878173828" />
                  <Point X="-1.961928222656" Y="-3.776132324219" />
                  <Point X="-1.992729614258" Y="-3.779166015625" />
                  <Point X="-2.008006103516" Y="-3.781945556641" />
                  <Point X="-2.03905480957" Y="-3.790265136719" />
                  <Point X="-2.053674316406" Y="-3.795496337891" />
                  <Point X="-2.081865478516" Y="-3.808269775391" />
                  <Point X="-2.095437011719" Y="-3.815811767578" />
                  <Point X="-2.120956298828" Y="-3.83286328125" />
                  <Point X="-2.353403320312" Y="-3.988179931641" />
                  <Point X="-2.359681640625" Y="-3.992757080078" />
                  <Point X="-2.380442138672" Y="-4.010131103516" />
                  <Point X="-2.402759277344" Y="-4.032771484375" />
                  <Point X="-2.410471435547" Y="-4.041629638672" />
                  <Point X="-2.503202880859" Y="-4.162479492188" />
                  <Point X="-2.747583496094" Y="-4.011164794922" />
                  <Point X="-2.778406005859" Y="-3.987432617188" />
                  <Point X="-2.980862792969" Y="-3.831547607422" />
                  <Point X="-2.415132080078" Y="-2.851673583984" />
                  <Point X="-2.341488769531" Y="-2.724119628906" />
                  <Point X="-2.334850097656" Y="-2.710083251953" />
                  <Point X="-2.323947998047" Y="-2.681116943359" />
                  <Point X="-2.319684570312" Y="-2.666187011719" />
                  <Point X="-2.313413574219" Y="-2.634660644531" />
                  <Point X="-2.311622802734" Y="-2.618697509766" />
                  <Point X="-2.310756591797" Y="-2.586697753906" />
                  <Point X="-2.315294189453" Y="-2.555009521484" />
                  <Point X="-2.325105712891" Y="-2.524538818359" />
                  <Point X="-2.331304199219" Y="-2.509719726562" />
                  <Point X="-2.347237548828" Y="-2.479174560547" />
                  <Point X="-2.354878417969" Y="-2.466904296875" />
                  <Point X="-2.371932617188" Y="-2.443666015625" />
                  <Point X="-2.381345947266" Y="-2.432697998047" />
                  <Point X="-2.396978271484" Y="-2.417065673828" />
                  <Point X="-2.408817138672" Y="-2.407020751953" />
                  <Point X="-2.433974121094" Y="-2.388993164062" />
                  <Point X="-2.447292236328" Y="-2.381010498047" />
                  <Point X="-2.47612109375" Y="-2.366793457031" />
                  <Point X="-2.490561523438" Y="-2.361086914062" />
                  <Point X="-2.520179199219" Y="-2.352102294922" />
                  <Point X="-2.550865234375" Y="-2.348062255859" />
                  <Point X="-2.581799316406" Y="-2.349074951172" />
                  <Point X="-2.597224609375" Y="-2.350849609375" />
                  <Point X="-2.628750976562" Y="-2.357120605469" />
                  <Point X="-2.643680908203" Y="-2.361384033203" />
                  <Point X="-2.672647216797" Y="-2.372286132812" />
                  <Point X="-2.68668359375" Y="-2.378924804688" />
                  <Point X="-3.793089355469" Y="-3.017708496094" />
                  <Point X="-4.004015380859" Y="-2.740594970703" />
                  <Point X="-4.026108886719" Y="-2.703547119141" />
                  <Point X="-4.181265136719" Y="-2.443373779297" />
                  <Point X="-3.176974365234" Y="-1.672753662109" />
                  <Point X="-3.048122314453" Y="-1.573881713867" />
                  <Point X="-3.035583984375" Y="-1.562333129883" />
                  <Point X="-3.012773193359" Y="-1.537181152344" />
                  <Point X="-3.002500732422" Y="-1.523577636719" />
                  <Point X="-2.983773681641" Y="-1.493503662109" />
                  <Point X="-2.977109130859" Y="-1.480735839844" />
                  <Point X="-2.965786621094" Y="-1.454338378906" />
                  <Point X="-2.961128662109" Y="-1.440708984375" />
                  <Point X="-2.954186279297" Y="-1.413904418945" />
                  <Point X="-2.951552734375" Y="-1.398803100586" />
                  <Point X="-2.948748535156" Y="-1.368373901367" />
                  <Point X="-2.948577880859" Y="-1.353046020508" />
                  <Point X="-2.950786865234" Y="-1.321375488281" />
                  <Point X="-2.953237548828" Y="-1.305576416016" />
                  <Point X="-2.960753662109" Y="-1.274612304688" />
                  <Point X="-2.973356445312" Y="-1.245346435547" />
                  <Point X="-2.990688476562" Y="-1.21860925293" />
                  <Point X="-3.000483154297" Y="-1.205972900391" />
                  <Point X="-3.023935546875" Y="-1.180474609375" />
                  <Point X="-3.034153808594" Y="-1.17089074707" />
                  <Point X="-3.055888916016" Y="-1.153329956055" />
                  <Point X="-3.067406005859" Y="-1.145352905273" />
                  <Point X="-3.091268798828" Y="-1.131308349609" />
                  <Point X="-3.10543359375" Y="-1.124481689453" />
                  <Point X="-3.134696777344" Y="-1.113257324219" />
                  <Point X="-3.149795410156" Y="-1.108859985352" />
                  <Point X="-3.181683105469" Y="-1.102378540039" />
                  <Point X="-3.197298828125" Y="-1.100532348633" />
                  <Point X="-3.228622070312" Y="-1.09944140625" />
                  <Point X="-3.244329101562" Y="-1.100196533203" />
                  <Point X="-4.660920898438" Y="-1.286694335938" />
                  <Point X="-4.74076171875" Y="-0.974120849609" />
                  <Point X="-4.746606933594" Y="-0.933251525879" />
                  <Point X="-4.786452148438" Y="-0.65465435791" />
                  <Point X="-3.655069824219" Y="-0.351501220703" />
                  <Point X="-3.508288085938" Y="-0.312171295166" />
                  <Point X="-3.499227783203" Y="-0.309249664307" />
                  <Point X="-3.472724121094" Y="-0.298701751709" />
                  <Point X="-3.454410644531" Y="-0.28957598877" />
                  <Point X="-3.448385009766" Y="-0.286296783447" />
                  <Point X="-3.43081640625" Y="-0.275608428955" />
                  <Point X="-3.405808837891" Y="-0.258251800537" />
                  <Point X="-3.392715087891" Y="-0.247296936035" />
                  <Point X="-3.368699951172" Y="-0.223220626831" />
                  <Point X="-3.357778564453" Y="-0.210099151611" />
                  <Point X="-3.337619628906" Y="-0.180895355225" />
                  <Point X="-3.330365234375" Y="-0.168467803955" />
                  <Point X="-3.317816894531" Y="-0.142659729004" />
                  <Point X="-3.312522949219" Y="-0.129278915405" />
                  <Point X="-3.304187011719" Y="-0.102420677185" />
                  <Point X="-3.300800292969" Y="-0.087182518005" />
                  <Point X="-3.296568115234" Y="-0.05635634613" />
                  <Point X="-3.29572265625" Y="-0.040768627167" />
                  <Point X="-3.296637695312" Y="-0.008178001404" />
                  <Point X="-3.297883056641" Y="0.004713697433" />
                  <Point X="-3.302115234375" Y="0.030207429886" />
                  <Point X="-3.305102050781" Y="0.042809314728" />
                  <Point X="-3.313437988281" Y="0.069667556763" />
                  <Point X="-3.319989501953" Y="0.085540924072" />
                  <Point X="-3.335843505859" Y="0.115849143982" />
                  <Point X="-3.345145996094" Y="0.130283996582" />
                  <Point X="-3.367135498047" Y="0.158444625854" />
                  <Point X="-3.376752197266" Y="0.169013931274" />
                  <Point X="-3.397461425781" Y="0.188589950562" />
                  <Point X="-3.408553955078" Y="0.197597091675" />
                  <Point X="-3.433561279297" Y="0.214953567505" />
                  <Point X="-3.440483886719" Y="0.219328582764" />
                  <Point X="-3.465615234375" Y="0.232834091187" />
                  <Point X="-3.496566650391" Y="0.245635726929" />
                  <Point X="-3.508288085938" Y="0.249611251831" />
                  <Point X="-4.785445800781" Y="0.591824584961" />
                  <Point X="-4.731331054688" Y="0.957528625488" />
                  <Point X="-4.719562988281" Y="1.000956054688" />
                  <Point X="-4.633586425781" Y="1.318237060547" />
                  <Point X="-3.883283203125" Y="1.219457763672" />
                  <Point X="-3.778066162109" Y="1.20560559082" />
                  <Point X="-3.76773828125" Y="1.204815429688" />
                  <Point X="-3.747057861328" Y="1.204364257812" />
                  <Point X="-3.736705322266" Y="1.204703125" />
                  <Point X="-3.715143554688" Y="1.20658984375" />
                  <Point X="-3.704889892578" Y="1.208053833008" />
                  <Point X="-3.684603271484" Y="1.212089233398" />
                  <Point X="-3.668493896484" Y="1.216576538086" />
                  <Point X="-3.613144287109" Y="1.234028198242" />
                  <Point X="-3.603451416016" Y="1.237676147461" />
                  <Point X="-3.584518554688" Y="1.246006713867" />
                  <Point X="-3.575278564453" Y="1.250689208984" />
                  <Point X="-3.556534423828" Y="1.261511108398" />
                  <Point X="-3.547859619141" Y="1.267171630859" />
                  <Point X="-3.531178710938" Y="1.279402709961" />
                  <Point X="-3.515928710938" Y="1.293376831055" />
                  <Point X="-3.502290283203" Y="1.308928344727" />
                  <Point X="-3.495895751953" Y="1.317076416016" />
                  <Point X="-3.483481201172" Y="1.334806152344" />
                  <Point X="-3.47801171875" Y="1.34360168457" />
                  <Point X="-3.468062255859" Y="1.361736938477" />
                  <Point X="-3.461144042969" Y="1.376963012695" />
                  <Point X="-3.438934814453" Y="1.430581054688" />
                  <Point X="-3.435498779297" Y="1.440352416992" />
                  <Point X="-3.429710693359" Y="1.4602109375" />
                  <Point X="-3.427358642578" Y="1.470297973633" />
                  <Point X="-3.423600341797" Y="1.491613037109" />
                  <Point X="-3.422360595703" Y="1.501897094727" />
                  <Point X="-3.421008056641" Y="1.522537109375" />
                  <Point X="-3.421910400391" Y="1.543200439453" />
                  <Point X="-3.425056884766" Y="1.563643920898" />
                  <Point X="-3.427188232422" Y="1.573780395508" />
                  <Point X="-3.432790039062" Y="1.594686645508" />
                  <Point X="-3.43601171875" Y="1.604529541016" />
                  <Point X="-3.4435078125" Y="1.623806518555" />
                  <Point X="-3.450724121094" Y="1.638892089844" />
                  <Point X="-3.477521972656" Y="1.690370239258" />
                  <Point X="-3.482798339844" Y="1.699283813477" />
                  <Point X="-3.494290771484" Y="1.716484008789" />
                  <Point X="-3.500506835938" Y="1.724770629883" />
                  <Point X="-3.514419189453" Y="1.741350952148" />
                  <Point X="-3.5215" Y="1.748910888672" />
                  <Point X="-3.536442138672" Y="1.76321484375" />
                  <Point X="-3.544303466797" Y="1.769958740234" />
                  <Point X="-4.227614746094" Y="2.294282226563" />
                  <Point X="-4.002291748047" Y="2.680314453125" />
                  <Point X="-3.971127441406" Y="2.720371337891" />
                  <Point X="-3.726338623047" Y="3.035012939453" />
                  <Point X="-3.318778320312" Y="2.799707519531" />
                  <Point X="-3.254156982422" Y="2.7623984375" />
                  <Point X="-3.244917480469" Y="2.757716064453" />
                  <Point X="-3.225984619141" Y="2.749385498047" />
                  <Point X="-3.216291259766" Y="2.745737304688" />
                  <Point X="-3.195648925781" Y="2.739228759766" />
                  <Point X="-3.185615722656" Y="2.736657226562" />
                  <Point X="-3.165328857422" Y="2.732621826172" />
                  <Point X="-3.146612060547" Y="2.730417480469" />
                  <Point X="-3.069525878906" Y="2.723673339844" />
                  <Point X="-3.059173583984" Y="2.723334472656" />
                  <Point X="-3.038492675781" Y="2.723785644531" />
                  <Point X="-3.0281640625" Y="2.724575683594" />
                  <Point X="-3.006705566406" Y="2.727400878906" />
                  <Point X="-2.996525146484" Y="2.729310791016" />
                  <Point X="-2.976433837891" Y="2.734227294922" />
                  <Point X="-2.956998046875" Y="2.741301513672" />
                  <Point X="-2.938447021484" Y="2.750449707031" />
                  <Point X="-2.929420898438" Y="2.755530273438" />
                  <Point X="-2.911166503906" Y="2.767159423828" />
                  <Point X="-2.902746337891" Y="2.773193359375" />
                  <Point X="-2.886614257812" Y="2.786140869141" />
                  <Point X="-2.872895263672" Y="2.799061523438" />
                  <Point X="-2.818178710938" Y="2.853777832031" />
                  <Point X="-2.811265380859" Y="2.861489501953" />
                  <Point X="-2.798318359375" Y="2.877620849609" />
                  <Point X="-2.792284667969" Y="2.886040527344" />
                  <Point X="-2.780655273438" Y="2.904294921875" />
                  <Point X="-2.775574707031" Y="2.913320800781" />
                  <Point X="-2.766426025391" Y="2.931872314453" />
                  <Point X="-2.759351318359" Y="2.951309570312" />
                  <Point X="-2.754434814453" Y="2.971401367188" />
                  <Point X="-2.752524902344" Y="2.981581542969" />
                  <Point X="-2.749699707031" Y="3.003040283203" />
                  <Point X="-2.748909667969" Y="3.013368652344" />
                  <Point X="-2.748458496094" Y="3.034049316406" />
                  <Point X="-2.749537841797" Y="3.052864746094" />
                  <Point X="-2.756281982422" Y="3.129951171875" />
                  <Point X="-2.757745849609" Y="3.140204833984" />
                  <Point X="-2.76178125" Y="3.160491699219" />
                  <Point X="-2.764352783203" Y="3.170524902344" />
                  <Point X="-2.770861328125" Y="3.191167236328" />
                  <Point X="-2.774509521484" Y="3.200860595703" />
                  <Point X="-2.782840087891" Y="3.219793457031" />
                  <Point X="-2.787522460938" Y="3.229032958984" />
                  <Point X="-3.059387207031" Y="3.699916259766" />
                  <Point X="-2.648372314453" Y="4.015037353516" />
                  <Point X="-2.599293701172" Y="4.0423046875" />
                  <Point X="-2.192525146484" Y="4.268296386719" />
                  <Point X="-2.138346679688" Y="4.197689453125" />
                  <Point X="-2.118563964844" Y="4.171908203125" />
                  <Point X="-2.111819580078" Y="4.164046386719" />
                  <Point X="-2.097516601562" Y="4.14910546875" />
                  <Point X="-2.089958007813" Y="4.142026367188" />
                  <Point X="-2.073377929688" Y="4.128113769531" />
                  <Point X="-2.065093017578" Y="4.1218984375" />
                  <Point X="-2.047893676758" Y="4.110405761719" />
                  <Point X="-2.029559448242" Y="4.100225097656" />
                  <Point X="-1.943762573242" Y="4.055561279297" />
                  <Point X="-1.934326904297" Y="4.051286376953" />
                  <Point X="-1.915047241211" Y="4.043789306641" />
                  <Point X="-1.905203613281" Y="4.040567138672" />
                  <Point X="-1.88429699707" Y="4.034965576172" />
                  <Point X="-1.874162475586" Y="4.032834716797" />
                  <Point X="-1.853719360352" Y="4.029688232422" />
                  <Point X="-1.833052978516" Y="4.028785888672" />
                  <Point X="-1.812413818359" Y="4.030138916016" />
                  <Point X="-1.802132446289" Y="4.031378662109" />
                  <Point X="-1.780817138672" Y="4.035136962891" />
                  <Point X="-1.77073034668" Y="4.037488769531" />
                  <Point X="-1.750869750977" Y="4.04327734375" />
                  <Point X="-1.73128515625" Y="4.050778564453" />
                  <Point X="-1.64192199707" Y="4.087794189453" />
                  <Point X="-1.632583862305" Y="4.0922734375" />
                  <Point X="-1.614450195312" Y="4.102222167969" />
                  <Point X="-1.605654541016" Y="4.10769140625" />
                  <Point X="-1.587924804688" Y="4.120105957031" />
                  <Point X="-1.579776245117" Y="4.126501464844" />
                  <Point X="-1.564224609375" Y="4.140140136719" />
                  <Point X="-1.550250976562" Y="4.155390136719" />
                  <Point X="-1.538020019531" Y="4.172071289062" />
                  <Point X="-1.532359863281" Y="4.180745605469" />
                  <Point X="-1.521538085938" Y="4.199489746094" />
                  <Point X="-1.516856323242" Y="4.208728027344" />
                  <Point X="-1.508526245117" Y="4.227659179688" />
                  <Point X="-1.501684692383" Y="4.247479980469" />
                  <Point X="-1.472598754883" Y="4.339728515625" />
                  <Point X="-1.470026977539" Y="4.349762207031" />
                  <Point X="-1.465991210938" Y="4.370050292969" />
                  <Point X="-1.46452722168" Y="4.3803046875" />
                  <Point X="-1.46264074707" Y="4.401866210938" />
                  <Point X="-1.462301757812" Y="4.41221875" />
                  <Point X="-1.462752929688" Y="4.432898925781" />
                  <Point X="-1.46354309082" Y="4.4432265625" />
                  <Point X="-1.479266113281" Y="4.562655273438" />
                  <Point X="-0.931178466797" Y="4.7163203125" />
                  <Point X="-0.871687866211" Y="4.723282714844" />
                  <Point X="-0.365221984863" Y="4.782557128906" />
                  <Point X="-0.244783096313" Y="4.333072265625" />
                  <Point X="-0.22566633606" Y="4.261727539062" />
                  <Point X="-0.220435256958" Y="4.247107910156" />
                  <Point X="-0.207661849976" Y="4.218916503906" />
                  <Point X="-0.200119247437" Y="4.205344726562" />
                  <Point X="-0.182260864258" Y="4.178618164062" />
                  <Point X="-0.17260824585" Y="4.166455566406" />
                  <Point X="-0.151451187134" Y="4.143866699219" />
                  <Point X="-0.126897941589" Y="4.125026367188" />
                  <Point X="-0.099602897644" Y="4.110436523438" />
                  <Point X="-0.085356651306" Y="4.104260742188" />
                  <Point X="-0.054918682098" Y="4.093927978516" />
                  <Point X="-0.039857234955" Y="4.090155273438" />
                  <Point X="-0.009319986343" Y="4.08511328125" />
                  <Point X="0.021631772995" Y="4.08511328125" />
                  <Point X="0.052169025421" Y="4.090155273438" />
                  <Point X="0.06723046875" Y="4.093927978516" />
                  <Point X="0.097668441772" Y="4.104260742188" />
                  <Point X="0.111914535522" Y="4.110436523438" />
                  <Point X="0.139209732056" Y="4.125026367188" />
                  <Point X="0.163763122559" Y="4.143866699219" />
                  <Point X="0.184920181274" Y="4.166455566406" />
                  <Point X="0.194572952271" Y="4.178617675781" />
                  <Point X="0.212431182861" Y="4.205344238281" />
                  <Point X="0.2199737854" Y="4.218916503906" />
                  <Point X="0.232747192383" Y="4.247107910156" />
                  <Point X="0.237978271484" Y="4.261727539062" />
                  <Point X="0.378190246582" Y="4.785006347656" />
                  <Point X="0.827876831055" Y="4.737912109375" />
                  <Point X="0.877104614258" Y="4.726026855469" />
                  <Point X="1.453595703125" Y="4.586844238281" />
                  <Point X="1.483330078125" Y="4.576059082031" />
                  <Point X="1.858255249023" Y="4.440071289062" />
                  <Point X="1.889245849609" Y="4.425577636719" />
                  <Point X="2.250452636719" Y="4.256653320312" />
                  <Point X="2.280448242188" Y="4.239177734375" />
                  <Point X="2.629429931641" Y="4.035860351563" />
                  <Point X="2.657674560547" Y="4.015774902344" />
                  <Point X="2.817779785156" Y="3.901916748047" />
                  <Point X="2.150545166016" Y="2.746232177734" />
                  <Point X="2.06530859375" Y="2.598597900391" />
                  <Point X="2.061337158203" Y="2.590935058594" />
                  <Point X="2.05084765625" Y="2.567291259766" />
                  <Point X="2.043722167969" Y="2.547265625" />
                  <Point X="2.039177490234" Y="2.532655761719" />
                  <Point X="2.01983190918" Y="2.460311767578" />
                  <Point X="2.017543823242" Y="2.449077392578" />
                  <Point X="2.014337402344" Y="2.426413574219" />
                  <Point X="2.013419433594" Y="2.414984130859" />
                  <Point X="2.012909912109" Y="2.389214111328" />
                  <Point X="2.014239013672" Y="2.362712646484" />
                  <Point X="2.021782226562" Y="2.300155517578" />
                  <Point X="2.02380078125" Y="2.289033935547" />
                  <Point X="2.029143676758" Y="2.267110595703" />
                  <Point X="2.032468261719" Y="2.256308837891" />
                  <Point X="2.040734375" Y="2.234220214844" />
                  <Point X="2.045318847656" Y="2.223888427734" />
                  <Point X="2.055682617188" Y="2.203841308594" />
                  <Point X="2.065712158203" Y="2.18786328125" />
                  <Point X="2.104420166016" Y="2.130817382812" />
                  <Point X="2.1114765625" Y="2.121669921875" />
                  <Point X="2.126647460938" Y="2.104298828125" />
                  <Point X="2.134761962891" Y="2.096075195312" />
                  <Point X="2.154324707031" Y="2.078525146484" />
                  <Point X="2.174520996094" Y="2.062731201172" />
                  <Point X="2.231566894531" Y="2.02402331543" />
                  <Point X="2.241280517578" Y="2.018244873047" />
                  <Point X="2.261325683594" Y="2.007882080078" />
                  <Point X="2.271657226562" Y="2.003297851562" />
                  <Point X="2.293745605469" Y="1.995032104492" />
                  <Point X="2.304546142578" Y="1.991707763672" />
                  <Point X="2.326465820312" Y="1.986365356445" />
                  <Point X="2.344452880859" Y="1.983519042969" />
                  <Point X="2.407009765625" Y="1.975975830078" />
                  <Point X="2.418775146484" Y="1.97529309082" />
                  <Point X="2.44230078125" Y="1.975389404297" />
                  <Point X="2.454061035156" Y="1.976168823242" />
                  <Point X="2.480906005859" Y="1.979630615234" />
                  <Point X="2.505690185547" Y="1.984520019531" />
                  <Point X="2.578034179688" Y="2.003865600586" />
                  <Point X="2.583993652344" Y="2.005670532227" />
                  <Point X="2.604404541016" Y="2.013067260742" />
                  <Point X="2.627655029297" Y="2.02357409668" />
                  <Point X="2.636033935547" Y="2.027872558594" />
                  <Point X="3.940404785156" Y="2.780951416016" />
                  <Point X="4.043950195312" Y="2.637047119141" />
                  <Point X="4.059696777344" Y="2.611025878906" />
                  <Point X="4.136884277344" Y="2.483472412109" />
                  <Point X="3.285173583984" Y="1.829930541992" />
                  <Point X="3.172951660156" Y="1.743819702148" />
                  <Point X="3.166204101562" Y="1.738125" />
                  <Point X="3.147074951172" Y="1.719834594727" />
                  <Point X="3.132114013672" Y="1.703127929688" />
                  <Point X="3.122860351562" Y="1.691965454102" />
                  <Point X="3.070794189453" Y="1.624041259766" />
                  <Point X="3.064256591797" Y="1.614325927734" />
                  <Point X="3.052426513672" Y="1.594165527344" />
                  <Point X="3.047134033203" Y="1.583720703125" />
                  <Point X="3.03653515625" Y="1.559155151367" />
                  <Point X="3.028010742188" Y="1.535058105469" />
                  <Point X="3.008616210938" Y="1.465707397461" />
                  <Point X="3.006225097656" Y="1.454661010742" />
                  <Point X="3.002771728516" Y="1.432362792969" />
                  <Point X="3.001709472656" Y="1.421110961914" />
                  <Point X="3.000893310547" Y="1.397540527344" />
                  <Point X="3.001174804688" Y="1.386241577148" />
                  <Point X="3.003077636719" Y="1.3637578125" />
                  <Point X="3.006446777344" Y="1.34410144043" />
                  <Point X="3.022367919922" Y="1.266940063477" />
                  <Point X="3.025467773438" Y="1.255547241211" />
                  <Point X="3.033057128906" Y="1.233232543945" />
                  <Point X="3.037546630859" Y="1.222311157227" />
                  <Point X="3.049498779297" Y="1.197707397461" />
                  <Point X="3.061673095703" Y="1.176299316406" />
                  <Point X="3.1049765625" Y="1.110480224609" />
                  <Point X="3.111739257813" Y="1.101424438477" />
                  <Point X="3.126292480469" Y="1.084179931641" />
                  <Point X="3.134083007812" Y="1.075991333008" />
                  <Point X="3.151326904297" Y="1.059901367188" />
                  <Point X="3.160034179688" Y="1.052696166992" />
                  <Point X="3.178243408203" Y="1.039370605469" />
                  <Point X="3.194634521484" Y="1.029372192383" />
                  <Point X="3.257387451172" Y="0.994047912598" />
                  <Point X="3.268237548828" Y="0.98881652832" />
                  <Point X="3.290513183594" Y="0.979768371582" />
                  <Point X="3.301938720703" Y="0.975951843262" />
                  <Point X="3.329228759766" Y="0.968723022461" />
                  <Point X="3.352985595703" Y="0.964026489258" />
                  <Point X="3.437831542969" Y="0.952812927246" />
                  <Point X="3.444029785156" Y="0.952199707031" />
                  <Point X="3.465716064453" Y="0.95122277832" />
                  <Point X="3.491217529297" Y="0.952032348633" />
                  <Point X="3.500603515625" Y="0.952797180176" />
                  <Point X="4.704704101562" Y="1.11132019043" />
                  <Point X="4.752684570312" Y="0.914232177734" />
                  <Point X="4.757645019531" Y="0.882368041992" />
                  <Point X="4.78387109375" Y="0.713920837402" />
                  <Point X="3.820600830078" Y="0.455813323975" />
                  <Point X="3.691991943359" Y="0.421352874756" />
                  <Point X="3.683408691406" Y="0.418610656738" />
                  <Point X="3.658257324219" Y="0.408782653809" />
                  <Point X="3.637024902344" Y="0.398485321045" />
                  <Point X="3.624852783203" Y="0.392026763916" />
                  <Point X="3.541494384766" Y="0.343844116211" />
                  <Point X="3.531555908203" Y="0.33723336792" />
                  <Point X="3.512586181641" Y="0.322817687988" />
                  <Point X="3.503554931641" Y="0.31501260376" />
                  <Point X="3.483751953125" Y="0.295595611572" />
                  <Point X="3.467305664062" Y="0.277229675293" />
                  <Point X="3.417290771484" Y="0.213498703003" />
                  <Point X="3.410854248047" Y="0.204207839966" />
                  <Point X="3.399130126953" Y="0.184927581787" />
                  <Point X="3.393842529297" Y="0.174938201904" />
                  <Point X="3.384068847656" Y="0.153474517822" />
                  <Point X="3.380005126953" Y="0.142928237915" />
                  <Point X="3.373158935547" Y="0.12142741394" />
                  <Point X="3.368546142578" Y="0.100915367126" />
                  <Point X="3.351874267578" Y="0.013862179756" />
                  <Point X="3.350363525391" Y="0.001917209029" />
                  <Point X="3.348864990234" Y="-0.022068595886" />
                  <Point X="3.348877197266" Y="-0.034109279633" />
                  <Point X="3.350707519531" Y="-0.062476039886" />
                  <Point X="3.353704589844" Y="-0.085980010986" />
                  <Point X="3.370376464844" Y="-0.173033493042" />
                  <Point X="3.373159179688" Y="-0.183988189697" />
                  <Point X="3.380005371094" Y="-0.205488876343" />
                  <Point X="3.384068847656" Y="-0.216034713745" />
                  <Point X="3.393842529297" Y="-0.237498382568" />
                  <Point X="3.399130126953" Y="-0.247487915039" />
                  <Point X="3.410854492188" Y="-0.266768463135" />
                  <Point X="3.422782226562" Y="-0.283056213379" />
                  <Point X="3.472797119141" Y="-0.34678704834" />
                  <Point X="3.480991455078" Y="-0.355941772461" />
                  <Point X="3.498493164062" Y="-0.373116912842" />
                  <Point X="3.507800537109" Y="-0.381137207031" />
                  <Point X="3.531264160156" Y="-0.398847412109" />
                  <Point X="3.550645996094" Y="-0.411694091797" />
                  <Point X="3.634004394531" Y="-0.459876739502" />
                  <Point X="3.639496582031" Y="-0.462815460205" />
                  <Point X="3.659158447266" Y="-0.472016845703" />
                  <Point X="3.683028076172" Y="-0.481027862549" />
                  <Point X="3.691991943359" Y="-0.48391293335" />
                  <Point X="4.784876953125" Y="-0.776750305176" />
                  <Point X="4.76161328125" Y="-0.931051147461" />
                  <Point X="4.755256347656" Y="-0.958908874512" />
                  <Point X="4.727801269531" Y="-1.079219604492" />
                  <Point X="3.585932128906" Y="-0.928889770508" />
                  <Point X="3.436781982422" Y="-0.909253845215" />
                  <Point X="3.424389160156" Y="-0.908441040039" />
                  <Point X="3.399602783203" Y="-0.908439208984" />
                  <Point X="3.387209228516" Y="-0.90925012207" />
                  <Point X="3.352101806641" Y="-0.91386706543" />
                  <Point X="3.336519775391" Y="-0.916580200195" />
                  <Point X="3.172916748047" Y="-0.952140014648" />
                  <Point X="3.161469970703" Y="-0.955390686035" />
                  <Point X="3.131099853516" Y="-0.966111877441" />
                  <Point X="3.110056884766" Y="-0.976629516602" />
                  <Point X="3.077430664062" Y="-0.998362915039" />
                  <Point X="3.062250732422" Y="-1.010930786133" />
                  <Point X="3.038894287109" Y="-1.03476171875" />
                  <Point X="3.028493164062" Y="-1.046280029297" />
                  <Point X="2.92960546875" Y="-1.165211303711" />
                  <Point X="2.923182861328" Y="-1.173895874023" />
                  <Point X="2.907156005859" Y="-1.198364624023" />
                  <Point X="2.897475585938" Y="-1.217597900391" />
                  <Point X="2.885051269531" Y="-1.25134765625" />
                  <Point X="2.880450683594" Y="-1.268816040039" />
                  <Point X="2.875299072266" Y="-1.300278198242" />
                  <Point X="2.873601318359" Y="-1.313569458008" />
                  <Point X="2.859428222656" Y="-1.467590332031" />
                  <Point X="2.85908203125" Y="-1.479483154297" />
                  <Point X="2.860162597656" Y="-1.511670043945" />
                  <Point X="2.863900878906" Y="-1.535053833008" />
                  <Point X="2.874951660156" Y="-1.572986572266" />
                  <Point X="2.882470703125" Y="-1.591372192383" />
                  <Point X="2.898922119141" Y="-1.621997070312" />
                  <Point X="2.906480224609" Y="-1.634832763672" />
                  <Point X="2.997020507812" Y="-1.775661987305" />
                  <Point X="3.0017421875" Y="-1.782353515625" />
                  <Point X="3.019793212891" Y="-1.80444921875" />
                  <Point X="3.043488769531" Y="-1.828119873047" />
                  <Point X="3.052796142578" Y="-1.836277709961" />
                  <Point X="4.087170410156" Y="-2.629981933594" />
                  <Point X="4.045486816406" Y="-2.697432617188" />
                  <Point X="4.032341064453" Y="-2.716110595703" />
                  <Point X="4.001274414063" Y="-2.760251953125" />
                  <Point X="2.98129296875" Y="-2.171364990234" />
                  <Point X="2.848454589844" Y="-2.094670654297" />
                  <Point X="2.841191894531" Y="-2.090885253906" />
                  <Point X="2.815026123047" Y="-2.079513183594" />
                  <Point X="2.783119140625" Y="-2.069325927734" />
                  <Point X="2.771108398438" Y="-2.066337646484" />
                  <Point X="2.749731689453" Y="-2.062477050781" />
                  <Point X="2.555018066406" Y="-2.027312011719" />
                  <Point X="2.539359130859" Y="-2.025807495117" />
                  <Point X="2.508006835938" Y="-2.025403198242" />
                  <Point X="2.492313476562" Y="-2.026503662109" />
                  <Point X="2.460140625" Y="-2.031461425781" />
                  <Point X="2.444844970703" Y="-2.035136108398" />
                  <Point X="2.415070068359" Y="-2.044959350586" />
                  <Point X="2.400590820312" Y="-2.051107666016" />
                  <Point X="2.382831787109" Y="-2.060453857422" />
                  <Point X="2.221072753906" Y="-2.145586669922" />
                  <Point X="2.208969970703" Y="-2.153169189453" />
                  <Point X="2.186037841797" Y="-2.170062988281" />
                  <Point X="2.175208496094" Y="-2.179374267578" />
                  <Point X="2.154248779297" Y="-2.200334228516" />
                  <Point X="2.144938476562" Y="-2.211162597656" />
                  <Point X="2.128046142578" Y="-2.234092529297" />
                  <Point X="2.120464111328" Y="-2.246194091797" />
                  <Point X="2.111117675781" Y="-2.263952880859" />
                  <Point X="2.025985229492" Y="-2.425712158203" />
                  <Point X="2.019836303711" Y="-2.440192138672" />
                  <Point X="2.010012329102" Y="-2.469968505859" />
                  <Point X="2.006337280273" Y="-2.485264892578" />
                  <Point X="2.001379516602" Y="-2.5174375" />
                  <Point X="2.000279174805" Y="-2.533130859375" />
                  <Point X="2.00068347168" Y="-2.564483154297" />
                  <Point X="2.002187866211" Y="-2.580142089844" />
                  <Point X="2.006048461914" Y="-2.601518798828" />
                  <Point X="2.041213500977" Y="-2.796232421875" />
                  <Point X="2.043015014648" Y="-2.804221435547" />
                  <Point X="2.051236083984" Y="-2.83154296875" />
                  <Point X="2.064069580078" Y="-2.862479736328" />
                  <Point X="2.069546630859" Y="-2.873578613281" />
                  <Point X="2.735893310547" Y="-4.027724121094" />
                  <Point X="2.723754150391" Y="-4.036083496094" />
                  <Point X="1.935282470703" Y="-3.008528076172" />
                  <Point X="1.833914550781" Y="-2.876422851562" />
                  <Point X="1.828654418945" Y="-2.870147216797" />
                  <Point X="1.808830810547" Y="-2.849625732422" />
                  <Point X="1.783251708984" Y="-2.828004394531" />
                  <Point X="1.773299804688" Y="-2.820647705078" />
                  <Point X="1.752216552734" Y="-2.807093017578" />
                  <Point X="1.560176391602" Y="-2.68362890625" />
                  <Point X="1.546284545898" Y="-2.676246337891" />
                  <Point X="1.517473266602" Y="-2.663874755859" />
                  <Point X="1.502553588867" Y="-2.658885742188" />
                  <Point X="1.470932617188" Y="-2.651154052734" />
                  <Point X="1.45539453125" Y="-2.648695800781" />
                  <Point X="1.424125610352" Y="-2.646376953125" />
                  <Point X="1.408394775391" Y="-2.646516357422" />
                  <Point X="1.385336303711" Y="-2.648638183594" />
                  <Point X="1.175307861328" Y="-2.667965332031" />
                  <Point X="1.161225341797" Y="-2.670339111328" />
                  <Point X="1.133575561523" Y="-2.677170898438" />
                  <Point X="1.120008422852" Y="-2.68162890625" />
                  <Point X="1.092623168945" Y="-2.692972167969" />
                  <Point X="1.079877685547" Y="-2.699413330078" />
                  <Point X="1.05549597168" Y="-2.714133544922" />
                  <Point X="1.04385925293" Y="-2.722412841797" />
                  <Point X="1.026054321289" Y="-2.737217041016" />
                  <Point X="0.863875244141" Y="-2.872063232422" />
                  <Point X="0.852653320312" Y="-2.883087890625" />
                  <Point X="0.832182128906" Y="-2.906838378906" />
                  <Point X="0.822933044434" Y="-2.919564208984" />
                  <Point X="0.806040710449" Y="-2.947390869141" />
                  <Point X="0.799018737793" Y="-2.961468261719" />
                  <Point X="0.787394348145" Y="-2.990588623047" />
                  <Point X="0.782791931152" Y="-3.005630859375" />
                  <Point X="0.777468261719" Y="-3.030123535156" />
                  <Point X="0.728977661133" Y="-3.25321875" />
                  <Point X="0.727584777832" Y="-3.261289306641" />
                  <Point X="0.724724487305" Y="-3.289677734375" />
                  <Point X="0.7247421875" Y="-3.323170410156" />
                  <Point X="0.725554992676" Y="-3.335520019531" />
                  <Point X="0.83309161377" Y="-4.152340820313" />
                  <Point X="0.694692565918" Y="-3.635828613281" />
                  <Point X="0.655064941406" Y="-3.487936523438" />
                  <Point X="0.652606262207" Y="-3.480124511719" />
                  <Point X="0.642145324707" Y="-3.453580566406" />
                  <Point X="0.626787353516" Y="-3.423816162109" />
                  <Point X="0.62040802002" Y="-3.413210693359" />
                  <Point X="0.604211242676" Y="-3.389874023438" />
                  <Point X="0.456680297852" Y="-3.177310302734" />
                  <Point X="0.44667098999" Y="-3.165173095703" />
                  <Point X="0.424786834717" Y="-3.142717529297" />
                  <Point X="0.412912322998" Y="-3.132399169922" />
                  <Point X="0.386657196045" Y="-3.113155273438" />
                  <Point X="0.373242797852" Y="-3.104937988281" />
                  <Point X="0.345241485596" Y="-3.090829589844" />
                  <Point X="0.330654602051" Y="-3.084938476562" />
                  <Point X="0.3055909729" Y="-3.077159667969" />
                  <Point X="0.077295509338" Y="-3.006305175781" />
                  <Point X="0.063376529694" Y="-3.003109375" />
                  <Point X="0.035217094421" Y="-2.998840087891" />
                  <Point X="0.020976791382" Y="-2.997766601562" />
                  <Point X="-0.008664708138" Y="-2.997766601562" />
                  <Point X="-0.022905010223" Y="-2.99883984375" />
                  <Point X="-0.051064441681" Y="-3.003109130859" />
                  <Point X="-0.064983718872" Y="-3.006305175781" />
                  <Point X="-0.090047187805" Y="-3.014083984375" />
                  <Point X="-0.318342956543" Y="-3.084938476562" />
                  <Point X="-0.332929718018" Y="-3.090829589844" />
                  <Point X="-0.360930999756" Y="-3.104937988281" />
                  <Point X="-0.374345275879" Y="-3.113155273438" />
                  <Point X="-0.400600524902" Y="-3.132399169922" />
                  <Point X="-0.412475219727" Y="-3.142717773438" />
                  <Point X="-0.434358886719" Y="-3.165173095703" />
                  <Point X="-0.444367889404" Y="-3.177309570312" />
                  <Point X="-0.460564666748" Y="-3.200645996094" />
                  <Point X="-0.60809564209" Y="-3.413209960938" />
                  <Point X="-0.612470336914" Y="-3.420132324219" />
                  <Point X="-0.625976745605" Y="-3.445265136719" />
                  <Point X="-0.638777832031" Y="-3.476215820312" />
                  <Point X="-0.642752990723" Y="-3.487936523438" />
                  <Point X="-0.985424987793" Y="-4.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.853755912098" Y="2.87123593296" />
                  <Point X="-4.150416278217" Y="2.235045724777" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.581150265203" Y="1.311333708787" />
                  <Point X="-4.71079697768" Y="1.033305436659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.683970911485" Y="3.010551891374" />
                  <Point X="-4.073217810341" Y="2.175809222991" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.482392178169" Y="1.298331959489" />
                  <Point X="-4.773175801332" Y="0.674744467245" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.601384258375" Y="2.96287039011" />
                  <Point X="-3.996019342464" Y="2.116572721206" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.383634091134" Y="1.285330210192" />
                  <Point X="-4.715731633376" Y="0.573144732569" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.518797605265" Y="2.915188888847" />
                  <Point X="-3.918820874588" Y="2.05733621942" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.284876004099" Y="1.272328460895" />
                  <Point X="-4.622553087044" Y="0.548177619623" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.041744988918" Y="3.713442375652" />
                  <Point X="-3.053116760291" Y="3.689055533243" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.436210952156" Y="2.867507387583" />
                  <Point X="-3.841622406711" Y="1.998099717634" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.186117917065" Y="1.259326711597" />
                  <Point X="-4.529374540711" Y="0.523210506677" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.878596183663" Y="3.838526967194" />
                  <Point X="-2.995129953446" Y="3.588619491421" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.353624299046" Y="2.819825886319" />
                  <Point X="-3.764423938835" Y="1.938863215849" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.08735983003" Y="1.2463249623" />
                  <Point X="-4.436195994378" Y="0.498243393731" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.715447378408" Y="3.963611558737" />
                  <Point X="-2.937143146602" Y="3.488183449598" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.271037612451" Y="2.772144456865" />
                  <Point X="-3.687225470958" Y="1.879626714063" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.988601742995" Y="1.233323213002" />
                  <Point X="-4.343017448046" Y="0.473276280784" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.565063146816" Y="4.061322433722" />
                  <Point X="-2.879156339757" Y="3.387747407775" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.18300668275" Y="2.736138244426" />
                  <Point X="-3.610027003082" Y="1.820390212278" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.889843655961" Y="1.220321463705" />
                  <Point X="-4.249838901713" Y="0.448309167838" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.760976087504" Y="-0.647828064419" />
                  <Point X="-4.781219582938" Y="-0.691240380474" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.423591013537" Y="4.139921252198" />
                  <Point X="-2.821169532912" Y="3.287311365952" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.083430981745" Y="2.724889873948" />
                  <Point X="-3.533279284308" Y="1.760187075923" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.791085602072" Y="1.207319643326" />
                  <Point X="-4.156660355381" Y="0.423342054892" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.641188050253" Y="-0.615730939941" />
                  <Point X="-4.756616142376" Y="-0.863267282319" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.282118880258" Y="4.218520070675" />
                  <Point X="-2.766956435314" Y="3.178782578535" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.973810764301" Y="2.735182038485" />
                  <Point X="-3.468814437826" Y="1.673643234932" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.683956607492" Y="1.21226936319" />
                  <Point X="-4.063481809048" Y="0.398374941946" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.521400013003" Y="-0.583633815464" />
                  <Point X="-4.727569851427" Y="-1.025766460765" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.168616386337" Y="4.237137803985" />
                  <Point X="-2.751908424913" Y="2.986263990579" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.808464316761" Y="2.864979489116" />
                  <Point X="-3.422632438891" Y="1.547891700852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.556016226767" Y="1.261849244669" />
                  <Point X="-3.970303262715" Y="0.373407828999" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.401611975753" Y="-0.551536690986" />
                  <Point X="-4.690472658807" Y="-1.171000424858" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.102444989526" Y="4.154253671986" />
                  <Point X="-3.877124716383" Y="0.348440716053" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.281823938503" Y="-0.519439566508" />
                  <Point X="-4.638206248314" Y="-1.283703896245" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.024134841477" Y="4.097401176024" />
                  <Point X="-3.78394617005" Y="0.323473603107" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.162035901253" Y="-0.487342442031" />
                  <Point X="-4.526529445331" Y="-1.269001369787" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.939685535775" Y="4.053714146134" />
                  <Point X="-3.690767623718" Y="0.298506490161" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.042247864003" Y="-0.455245317553" />
                  <Point X="-4.414852642348" Y="-1.25429884333" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.846220773091" Y="4.029360826135" />
                  <Point X="-3.597589077385" Y="0.273539377214" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.922459826753" Y="-0.423148193076" />
                  <Point X="-4.303175839366" Y="-1.239596316873" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.731440329954" Y="4.050719130382" />
                  <Point X="-3.504521717686" Y="0.248333823762" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.802671789503" Y="-0.391051068598" />
                  <Point X="-4.191499036383" Y="-1.224893790416" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.374158222078" Y="4.592123932896" />
                  <Point X="-1.462622915194" Y="4.402410786288" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.597337042794" Y="4.113515407359" />
                  <Point X="-3.419739585635" Y="0.205360542283" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.682883752253" Y="-0.35895394412" />
                  <Point X="-4.0798222334" Y="-1.210191263958" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.253572312784" Y="4.625932099493" />
                  <Point X="-3.348139696925" Y="0.134117848729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.563095762779" Y="-0.326856922099" />
                  <Point X="-3.968145430418" Y="-1.195488737501" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.132986403491" Y="4.659740266089" />
                  <Point X="-3.299399984853" Y="0.013851348172" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.435787572806" Y="-0.278632778064" />
                  <Point X="-3.856468627435" Y="-1.180786211044" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.012400494197" Y="4.693548432686" />
                  <Point X="-3.744791824452" Y="-1.166083684587" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.89498571814" Y="4.720556082111" />
                  <Point X="-3.63311502147" Y="-1.151381158129" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.784114071623" Y="4.733531944955" />
                  <Point X="-3.521438218487" Y="-1.136678631672" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.102620148168" Y="-2.383027301948" />
                  <Point X="-4.152922240866" Y="-2.490900487856" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.673242405559" Y="4.746507849718" />
                  <Point X="-3.409761415504" Y="-1.121976105215" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.939395823167" Y="-2.257780757788" />
                  <Point X="-4.094097767377" Y="-2.589540147762" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.562370739495" Y="4.759483754481" />
                  <Point X="-3.298084612522" Y="-1.107273578758" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.776171498167" Y="-2.132534213629" />
                  <Point X="-4.035273293887" Y="-2.688179807668" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.451499073432" Y="4.772459659244" />
                  <Point X="-3.190495300755" Y="-1.101336705501" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.612947173166" Y="-2.007287669469" />
                  <Point X="-3.973555003594" Y="-2.780613657411" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.356736617809" Y="4.750889250731" />
                  <Point X="-3.098112407786" Y="-1.128010102592" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.449722848166" Y="-1.88204112531" />
                  <Point X="-3.908555137917" Y="-2.866010146035" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.31848482094" Y="4.608131343439" />
                  <Point X="-3.019835019261" Y="-1.18493285158" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.286498523165" Y="-1.75679458115" />
                  <Point X="-3.84355527224" Y="-2.951406634658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.280233024071" Y="4.465373436147" />
                  <Point X="-2.959411222151" Y="-1.280142750913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.123274137085" Y="-1.631547906004" />
                  <Point X="-3.761016759574" Y="-2.999191373437" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.241981224138" Y="4.322615535427" />
                  <Point X="-3.617579181897" Y="-2.916377645847" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.195245695698" Y="4.1980510492" />
                  <Point X="-3.47414160422" Y="-2.833563918257" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.124959838698" Y="4.123990405552" />
                  <Point X="-3.330704026543" Y="-2.750750190667" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.036198231126" Y="4.089551136865" />
                  <Point X="-3.187266448867" Y="-2.667936463077" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.392232985955" Y="4.783535696458" />
                  <Point X="0.358294532845" Y="4.710754448893" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.071309292451" Y="4.095312614783" />
                  <Point X="-3.04382887119" Y="-2.585122735487" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.492173311437" Y="4.773069265694" />
                  <Point X="-2.900391293513" Y="-2.502309007897" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.59211363692" Y="4.762602834929" />
                  <Point X="-2.756953715836" Y="-2.419495280307" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.692053962402" Y="4.752136404164" />
                  <Point X="-2.62246386707" Y="-2.35587001929" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.791994287884" Y="4.7416699734" />
                  <Point X="-2.516418078918" Y="-2.353243243107" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.888264314706" Y="4.723332561757" />
                  <Point X="-2.429696904043" Y="-2.392058233832" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.982478494222" Y="4.70058637134" />
                  <Point X="-2.357873944982" Y="-2.462822551474" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.076692673738" Y="4.677840180923" />
                  <Point X="-2.310770783533" Y="-2.586598646169" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.515194506567" Y="-3.024986734932" />
                  <Point X="-2.914961362018" Y="-3.882289523037" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.170906853254" Y="4.655093990506" />
                  <Point X="-2.837832806419" Y="-3.941675952184" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.26512103277" Y="4.632347800088" />
                  <Point X="-2.760704224752" Y="-4.001062325431" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.359335212286" Y="4.609601609671" />
                  <Point X="-2.680083291019" Y="-4.052959325501" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.453549391802" Y="4.586855419254" />
                  <Point X="-2.598746492107" Y="-4.103321147743" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.543208477226" Y="4.554340798032" />
                  <Point X="-2.517409693196" Y="-4.153682969985" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.632865444361" Y="4.521821634125" />
                  <Point X="-2.327271296986" Y="-3.970719013858" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.722522411496" Y="4.489302470219" />
                  <Point X="-2.175008760818" Y="-3.868980101711" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.812179378631" Y="4.456783306313" />
                  <Point X="-2.032686794723" Y="-3.788558810878" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.900085061186" Y="4.420508500503" />
                  <Point X="-1.92267793904" Y="-3.777433208949" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.986139512256" Y="4.380263715965" />
                  <Point X="-1.820965709155" Y="-3.78409977846" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.072193963327" Y="4.340018931427" />
                  <Point X="-1.719253479271" Y="-3.790766347971" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.158248414397" Y="4.29977414689" />
                  <Point X="-1.61835196202" Y="-3.799171496336" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.244302865468" Y="4.259529362352" />
                  <Point X="-1.528919613444" Y="-3.832172356296" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.326989660931" Y="4.212062617057" />
                  <Point X="-1.452091142334" Y="-3.892202318708" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.409417304327" Y="4.164040118361" />
                  <Point X="-1.377694286003" Y="-3.957446895842" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.491844947723" Y="4.116017619665" />
                  <Point X="-1.303297429672" Y="-4.022691472976" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.574272591118" Y="4.06799512097" />
                  <Point X="-1.231566343314" Y="-4.093652812265" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.655472886802" Y="4.01734056661" />
                  <Point X="-1.180690648297" Y="-4.209338682615" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.73419016126" Y="3.96136115605" />
                  <Point X="2.239043779559" Y="2.899516313827" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.013485336217" Y="2.415804671101" />
                  <Point X="-1.149347423179" Y="-4.366912069838" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.812907419358" Y="3.905381710406" />
                  <Point X="2.784045771432" Y="3.843487706692" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.037572237295" Y="2.242670046756" />
                  <Point X="-0.582838966132" Y="-3.376819913574" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.653694045433" Y="-3.528769121486" />
                  <Point X="-1.120298139959" Y="-4.529404831336" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.096012978225" Y="2.143207469721" />
                  <Point X="-0.348260535633" Y="-3.098553996366" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.795289383972" Y="-4.057210455295" />
                  <Point X="-1.113022053584" Y="-4.738590364151" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.166303717113" Y="2.069157295314" />
                  <Point X="-0.22334158573" Y="-3.055453594196" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.936884722511" Y="-4.585651789103" />
                  <Point X="-1.018376156661" Y="-4.760410733601" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.246198257005" Y="2.015702538625" />
                  <Point X="-0.100783529268" Y="-3.017416144347" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.336591813715" Y="1.984762996159" />
                  <Point X="0.013200105313" Y="-2.997766601563" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.437031679345" Y="1.975367832699" />
                  <Point X="0.109393965631" Y="-3.0162673528" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.551879991498" Y="1.996871682521" />
                  <Point X="0.200962621713" Y="-3.04468688653" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.684096336247" Y="2.055621398438" />
                  <Point X="0.292531277795" Y="-3.073106420259" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.827533911425" Y="2.13843512067" />
                  <Point X="0.380450124662" Y="-3.109352995109" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.970971486604" Y="2.221248842902" />
                  <Point X="0.454701121407" Y="-3.174910369134" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.114409061782" Y="2.304062565134" />
                  <Point X="0.51752429135" Y="-3.264974796823" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.25784663696" Y="2.386876287365" />
                  <Point X="0.580221480981" Y="-3.355309390162" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.401284212139" Y="2.469690009597" />
                  <Point X="0.939834611528" Y="-2.808905693398" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.727226494826" Y="-3.264845271023" />
                  <Point X="0.640607526564" Y="-3.450600247907" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.544721787317" Y="2.552503731829" />
                  <Point X="3.163978387155" Y="1.735996875242" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.00116009716" Y="1.386831925563" />
                  <Point X="1.100174798522" Y="-2.689844203153" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.741745430434" Y="-3.458498463531" />
                  <Point X="0.681687498782" Y="-3.58729311359" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.688159362496" Y="2.635317454061" />
                  <Point X="3.327883284488" Y="1.86270291148" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.033665883103" Y="1.231751658076" />
                  <Point X="1.216986428552" Y="-2.664130004557" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.764823800662" Y="-3.633795889264" />
                  <Point X="0.719939353936" Y="-3.730050895891" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.831596937674" Y="2.718131176292" />
                  <Point X="3.491107702964" Y="1.987949656098" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.091493382449" Y="1.130973980218" />
                  <Point X="1.326506894112" Y="-2.654051758626" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.787902170889" Y="-3.809093314997" />
                  <Point X="0.758191198042" Y="-3.872808701881" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.955760177665" Y="2.759610953326" />
                  <Point X="3.65433212144" Y="2.113196400715" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.159874396032" Y="1.052828386679" />
                  <Point X="1.434546265044" Y="-2.647149730375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.810980541116" Y="-3.98439074073" />
                  <Point X="0.796443042149" Y="-4.015566507872" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.019362764628" Y="2.671217990831" />
                  <Point X="3.817556539915" Y="2.238443145333" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.241464990983" Y="1.003010831801" />
                  <Point X="1.529216732658" Y="-2.668917407807" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.080265654736" Y="2.577035509747" />
                  <Point X="3.980780958391" Y="2.363689889951" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.330207042032" Y="0.968529624015" />
                  <Point X="1.611723959926" Y="-2.716769238339" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.428287328542" Y="0.954074326803" />
                  <Point X="1.692368275729" Y="-2.7686160954" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.534599737045" Y="0.957272872173" />
                  <Point X="1.773012555774" Y="-2.820463029143" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.646276549213" Y="0.971975418329" />
                  <Point X="2.177828537119" Y="-2.177121506015" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.000589700541" Y="-2.55721141764" />
                  <Point X="1.845001426796" Y="-2.890871547435" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.757953361381" Y="0.986677964484" />
                  <Point X="2.322163571234" Y="-2.092383176883" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.02801972605" Y="-2.723176688506" />
                  <Point X="1.91020056192" Y="-2.97584070135" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.869630173549" Y="1.001380510639" />
                  <Point X="3.570957135756" Y="0.360874114124" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.356290151295" Y="-0.099480719659" />
                  <Point X="2.454793756786" Y="-2.032745976497" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.0660300353" Y="-2.866452467667" />
                  <Point X="1.975399707646" Y="-3.06080983253" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.981306985717" Y="1.016083056795" />
                  <Point X="3.70569144592" Y="0.4250236243" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.395389516651" Y="-0.240421010464" />
                  <Point X="3.008269214211" Y="-1.070603178116" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.868347170138" Y="-1.370666969964" />
                  <Point X="2.56159470923" Y="-2.028499745264" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.123749797904" Y="-2.967461187713" />
                  <Point X="2.040598859999" Y="-3.145778949495" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.092983797885" Y="1.03078560295" />
                  <Point X="3.825479380043" Y="0.45712052762" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.458880304803" Y="-0.329053726283" />
                  <Point X="3.167630359033" Y="-0.953641250585" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.876761396628" Y="-1.577411753425" />
                  <Point X="2.658273820288" Y="-2.045959872931" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.181736608758" Y="-3.067897220937" />
                  <Point X="2.105798012353" Y="-3.230748066461" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.204660610053" Y="1.045488149105" />
                  <Point X="3.945267410031" Y="0.489217636526" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.531184067003" Y="-0.398786958267" />
                  <Point X="3.284456270879" Y="-0.927896424536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.934490394353" Y="-1.678400668689" />
                  <Point X="2.754952932549" Y="-2.063419998018" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.239723419612" Y="-3.168333254161" />
                  <Point X="2.170997164707" Y="-3.315717183427" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.316337422221" Y="1.060190695261" />
                  <Point X="4.06505544002" Y="0.521314745432" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.613141578502" Y="-0.447817658069" />
                  <Point X="3.398310802677" Y="-0.908523743562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.995245227192" Y="-1.77290065961" />
                  <Point X="2.845837487847" Y="-2.093306590613" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.297710230466" Y="-3.268769287386" />
                  <Point X="2.236196317061" Y="-3.400686300392" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.428014234389" Y="1.074893241416" />
                  <Point X="4.184843470009" Y="0.553411854338" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.700115914611" Y="-0.486089742775" />
                  <Point X="3.498973304835" Y="-0.91744146145" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.06584419173" Y="-1.846289841978" />
                  <Point X="2.928478132878" Y="-2.140872305829" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.355697041321" Y="-3.36920532061" />
                  <Point X="2.301395469414" Y="-3.485655417358" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.539691046557" Y="1.089595787572" />
                  <Point X="4.304631499998" Y="0.585508963244" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.793294469613" Y="-0.511056837129" />
                  <Point X="3.597731408886" Y="-0.930443174254" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.143042647367" Y="-1.90552637001" />
                  <Point X="3.011064800118" Y="-2.18855377679" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.413683852175" Y="-3.469641353834" />
                  <Point X="2.366594621768" Y="-3.570624534323" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.651367858725" Y="1.104298333727" />
                  <Point X="4.424419529987" Y="0.617606072149" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.886473024615" Y="-0.536023931484" />
                  <Point X="3.696489499564" Y="-0.943444915739" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.220241103005" Y="-1.964762898042" />
                  <Point X="3.093651468806" Y="-2.236235244646" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.471670663029" Y="-3.570077387058" />
                  <Point X="2.431793774122" Y="-3.655593651289" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.723486524894" Y="1.034168162024" />
                  <Point X="4.544207559976" Y="0.649703181055" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.979651579617" Y="-0.560991025838" />
                  <Point X="3.795247590242" Y="-0.956446657224" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.297439558642" Y="-2.023999426075" />
                  <Point X="3.176238137494" Y="-2.283916712503" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.529657473884" Y="-3.670513420282" />
                  <Point X="2.496992926476" Y="-3.740562768254" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.75761419632" Y="0.882566039179" />
                  <Point X="4.663995589965" Y="0.681800289961" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.072830134619" Y="-0.585958120193" />
                  <Point X="3.894005680919" Y="-0.969448398709" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.374638014279" Y="-2.083235954107" />
                  <Point X="3.258824806182" Y="-2.331598180359" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.587644284738" Y="-3.770949453506" />
                  <Point X="2.562192078829" Y="-3.82553188522" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.783851933949" Y="0.714043898704" />
                  <Point X="4.783783619953" Y="0.713897398867" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.166008689622" Y="-0.610925214547" />
                  <Point X="3.992763771597" Y="-0.982450140194" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.451836469917" Y="-2.14247248214" />
                  <Point X="3.34141147487" Y="-2.379279648216" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.645631095592" Y="-3.87138548673" />
                  <Point X="2.627391231183" Y="-3.910501002185" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.259187244624" Y="-0.635892308902" />
                  <Point X="4.091521862274" Y="-0.995451881679" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.529034925554" Y="-2.201709010172" />
                  <Point X="3.423998143557" Y="-2.426961116072" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.703617906447" Y="-3.971821519954" />
                  <Point X="2.692590383537" Y="-3.995470119151" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.352365799626" Y="-0.660859403256" />
                  <Point X="4.190279952952" Y="-1.008453623164" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.606233381192" Y="-2.260945538204" />
                  <Point X="3.506584812245" Y="-2.474642583929" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.445544354628" Y="-0.685826497611" />
                  <Point X="4.28903804363" Y="-1.021455364649" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.683431836829" Y="-2.320182066237" />
                  <Point X="3.589171480933" Y="-2.522324051785" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.53872290963" Y="-0.710793591965" />
                  <Point X="4.387796134307" Y="-1.034457106134" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.760630292467" Y="-2.379418594269" />
                  <Point X="3.671758149621" Y="-2.570005519642" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.631901464632" Y="-0.735760686319" />
                  <Point X="4.486554224985" Y="-1.047458847619" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.837828748104" Y="-2.438655122301" />
                  <Point X="3.754344818309" Y="-2.617686987498" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.725080019634" Y="-0.760727780674" />
                  <Point X="4.585312315662" Y="-1.060460589105" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.915027203741" Y="-2.497891650334" />
                  <Point X="3.836931486997" Y="-2.665368455355" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.766933924644" Y="-0.895760942128" />
                  <Point X="4.68407040634" Y="-1.07346233059" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.992225659379" Y="-2.557128178366" />
                  <Point X="3.919518155685" Y="-2.713049923211" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.069424115016" Y="-2.616364706399" />
                  <Point X="4.004397840955" Y="-2.755814001137" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="0.001626220703" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.511166687012" Y="-3.685004394531" />
                  <Point X="0.471539031982" Y="-3.537112304688" />
                  <Point X="0.464318664551" Y="-3.521544189453" />
                  <Point X="0.448121917725" Y="-3.498207519531" />
                  <Point X="0.300591033936" Y="-3.285643798828" />
                  <Point X="0.274335754395" Y="-3.266399902344" />
                  <Point X="0.249272155762" Y="-3.25862109375" />
                  <Point X="0.02097677803" Y="-3.187766601562" />
                  <Point X="-0.008664756775" Y="-3.187766601562" />
                  <Point X="-0.033728351593" Y="-3.195545410156" />
                  <Point X="-0.262023895264" Y="-3.266399902344" />
                  <Point X="-0.288279144287" Y="-3.285643798828" />
                  <Point X="-0.304475891113" Y="-3.308980224609" />
                  <Point X="-0.45200680542" Y="-3.521544189453" />
                  <Point X="-0.459227142334" Y="-3.537112304688" />
                  <Point X="-0.820180725098" Y="-4.884209960938" />
                  <Point X="-0.847743896484" Y="-4.987077148438" />
                  <Point X="-1.100246826172" Y="-4.938065429688" />
                  <Point X="-1.1280390625" Y="-4.930914550781" />
                  <Point X="-1.35158972168" Y="-4.873396972656" />
                  <Point X="-1.313920532227" Y="-4.587271972656" />
                  <Point X="-1.309150146484" Y="-4.551038574219" />
                  <Point X="-1.309683227539" Y="-4.5347578125" />
                  <Point X="-1.315671020508" Y="-4.504655761719" />
                  <Point X="-1.370210693359" Y="-4.230466308594" />
                  <Point X="-1.386282714844" Y="-4.20262890625" />
                  <Point X="-1.409358154297" Y="-4.182392089844" />
                  <Point X="-1.619543457031" Y="-3.998063964844" />
                  <Point X="-1.649240722656" Y="-3.985762939453" />
                  <Point X="-1.679866943359" Y="-3.983755615234" />
                  <Point X="-1.958829833984" Y="-3.965471435547" />
                  <Point X="-1.989878540039" Y="-3.973791015625" />
                  <Point X="-2.015397827148" Y="-3.990842529297" />
                  <Point X="-2.247844970703" Y="-4.146159179688" />
                  <Point X="-2.259734130859" Y="-4.157294433594" />
                  <Point X="-2.457095458984" Y="-4.414500488281" />
                  <Point X="-2.462103759766" Y="-4.411399414062" />
                  <Point X="-2.855830810547" Y="-4.16761328125" />
                  <Point X="-2.894320556641" Y="-4.137977539062" />
                  <Point X="-3.228581054688" Y="-3.880608154297" />
                  <Point X="-2.579677001953" Y="-2.756673583984" />
                  <Point X="-2.506033691406" Y="-2.629119628906" />
                  <Point X="-2.499762695312" Y="-2.597593261719" />
                  <Point X="-2.515696044922" Y="-2.567048095703" />
                  <Point X="-2.531328369141" Y="-2.551415771484" />
                  <Point X="-2.560157226562" Y="-2.537198730469" />
                  <Point X="-2.59168359375" Y="-2.543469726562" />
                  <Point X="-3.753528808594" Y="-3.214261474609" />
                  <Point X="-3.842959472656" Y="-3.265894042969" />
                  <Point X="-3.850635498047" Y="-3.255809082031" />
                  <Point X="-4.161703613281" Y="-2.847129394531" />
                  <Point X="-4.189294433594" Y="-2.800863525391" />
                  <Point X="-4.431020019531" Y="-2.395527099609" />
                  <Point X="-3.292638916016" Y="-1.522016479492" />
                  <Point X="-3.163786865234" Y="-1.423144775391" />
                  <Point X="-3.145059814453" Y="-1.393070800781" />
                  <Point X="-3.138117431641" Y="-1.366266235352" />
                  <Point X="-3.140326416016" Y="-1.334595703125" />
                  <Point X="-3.163778808594" Y="-1.309097412109" />
                  <Point X="-3.187641601562" Y="-1.295052734375" />
                  <Point X="-3.219529296875" Y="-1.288571044922" />
                  <Point X="-4.686249023438" Y="-1.481668334961" />
                  <Point X="-4.803283203125" Y="-1.497076293945" />
                  <Point X="-4.805732421875" Y="-1.487488037109" />
                  <Point X="-4.927393066406" Y="-1.011191589355" />
                  <Point X="-4.934692871094" Y="-0.960152648926" />
                  <Point X="-4.998396484375" Y="-0.514742126465" />
                  <Point X="-3.704245605469" Y="-0.167975402832" />
                  <Point X="-3.557463867188" Y="-0.128645431519" />
                  <Point X="-3.539150390625" Y="-0.119519668579" />
                  <Point X="-3.514142822266" Y="-0.102163047791" />
                  <Point X="-3.493983886719" Y="-0.07295930481" />
                  <Point X="-3.485647949219" Y="-0.046101043701" />
                  <Point X="-3.486562988281" Y="-0.013510428429" />
                  <Point X="-3.494898925781" Y="0.013347833633" />
                  <Point X="-3.516888427734" Y="0.041508361816" />
                  <Point X="-3.541895751953" Y="0.058864833832" />
                  <Point X="-3.557463867188" Y="0.066085334778" />
                  <Point X="-4.894448242188" Y="0.42432925415" />
                  <Point X="-4.998186523438" Y="0.45212588501" />
                  <Point X="-4.996054199219" Y="0.466537567139" />
                  <Point X="-4.917645019531" Y="0.996418212891" />
                  <Point X="-4.902949707031" Y="1.050649414062" />
                  <Point X="-4.773516113281" Y="1.528298583984" />
                  <Point X="-3.858483398438" Y="1.407832275391" />
                  <Point X="-3.753266357422" Y="1.393980102539" />
                  <Point X="-3.731704589844" Y="1.395866699219" />
                  <Point X="-3.725628173828" Y="1.397782592773" />
                  <Point X="-3.670278564453" Y="1.41523425293" />
                  <Point X="-3.651534423828" Y="1.426056152344" />
                  <Point X="-3.639119873047" Y="1.443785888672" />
                  <Point X="-3.636681640625" Y="1.449672363281" />
                  <Point X="-3.614472412109" Y="1.503290161133" />
                  <Point X="-3.610714111328" Y="1.524605224609" />
                  <Point X="-3.616315917969" Y="1.54551171875" />
                  <Point X="-3.6192578125" Y="1.551163330078" />
                  <Point X="-3.646055664062" Y="1.602641479492" />
                  <Point X="-3.659968017578" Y="1.619221679688" />
                  <Point X="-4.426865722656" Y="2.207682861328" />
                  <Point X="-4.476105957031" Y="2.245466064453" />
                  <Point X="-4.46468359375" Y="2.26503515625" />
                  <Point X="-4.160014648438" Y="2.787006591797" />
                  <Point X="-4.121088378906" Y="2.837040527344" />
                  <Point X="-3.774671142578" Y="3.282310791016" />
                  <Point X="-3.223778320312" Y="2.964252441406" />
                  <Point X="-3.159156982422" Y="2.926943359375" />
                  <Point X="-3.138514648438" Y="2.920434814453" />
                  <Point X="-3.130051513672" Y="2.919694335938" />
                  <Point X="-3.052965332031" Y="2.912950195312" />
                  <Point X="-3.031506835938" Y="2.915775390625" />
                  <Point X="-3.013252441406" Y="2.927404541016" />
                  <Point X="-3.007245361328" Y="2.933411621094" />
                  <Point X="-2.952528808594" Y="2.988127929688" />
                  <Point X="-2.940899414062" Y="3.006382324219" />
                  <Point X="-2.93807421875" Y="3.027841064453" />
                  <Point X="-2.938814697266" Y="3.036304199219" />
                  <Point X="-2.945558837891" Y="3.113390625" />
                  <Point X="-2.952067382812" Y="3.134032958984" />
                  <Point X="-3.291902832031" Y="3.722645019531" />
                  <Point X="-3.307278564453" Y="3.749276611328" />
                  <Point X="-3.283513671875" Y="3.767497070312" />
                  <Point X="-2.752876220703" Y="4.174331542969" />
                  <Point X="-2.691568847656" Y="4.208392578125" />
                  <Point X="-2.141548583984" Y="4.513972167969" />
                  <Point X="-1.987609375" Y="4.313354492188" />
                  <Point X="-1.967826904297" Y="4.287573242188" />
                  <Point X="-1.951246826172" Y="4.273660644531" />
                  <Point X="-1.941827636719" Y="4.268757324219" />
                  <Point X="-1.856030761719" Y="4.22409375" />
                  <Point X="-1.835124145508" Y="4.2184921875" />
                  <Point X="-1.813808837891" Y="4.222250488281" />
                  <Point X="-1.803998046875" Y="4.226314453125" />
                  <Point X="-1.714634887695" Y="4.263330078125" />
                  <Point X="-1.696905273438" Y="4.275744628906" />
                  <Point X="-1.686083496094" Y="4.294488769531" />
                  <Point X="-1.682890014648" Y="4.304616699219" />
                  <Point X="-1.653804077148" Y="4.396865234375" />
                  <Point X="-1.651917602539" Y="4.418426757812" />
                  <Point X="-1.689137573242" Y="4.701141113281" />
                  <Point X="-1.655037231445" Y="4.710701660156" />
                  <Point X="-0.968094116211" Y="4.903296386719" />
                  <Point X="-0.893773986816" Y="4.911994628906" />
                  <Point X="-0.224199615479" Y="4.990358398438" />
                  <Point X="-0.061257141113" Y="4.382248046875" />
                  <Point X="-0.042140380859" Y="4.310903320312" />
                  <Point X="-0.024282115936" Y="4.284176757813" />
                  <Point X="0.006155934334" Y="4.273844238281" />
                  <Point X="0.036594036102" Y="4.284176757813" />
                  <Point X="0.054452251434" Y="4.310903320312" />
                  <Point X="0.228580078125" Y="4.960757324219" />
                  <Point X="0.236648422241" Y="4.990868652344" />
                  <Point X="0.260404632568" Y="4.988380859375" />
                  <Point X="0.860205810547" Y="4.925565429688" />
                  <Point X="0.921694946289" Y="4.910720214844" />
                  <Point X="1.508456054688" Y="4.769058105469" />
                  <Point X="1.548115112305" Y="4.754673339844" />
                  <Point X="1.931033691406" Y="4.615786132813" />
                  <Point X="1.969736206055" Y="4.597686035156" />
                  <Point X="2.338699462891" Y="4.425134277344" />
                  <Point X="2.376094726562" Y="4.40334765625" />
                  <Point X="2.732523681641" Y="4.19569140625" />
                  <Point X="2.767786376953" Y="4.170614746094" />
                  <Point X="3.068740234375" Y="3.956592773438" />
                  <Point X="2.315090087891" Y="2.651232177734" />
                  <Point X="2.229853515625" Y="2.503597900391" />
                  <Point X="2.222728027344" Y="2.483572265625" />
                  <Point X="2.203382324219" Y="2.411228271484" />
                  <Point X="2.202872802734" Y="2.385458251953" />
                  <Point X="2.210416015625" Y="2.322901123047" />
                  <Point X="2.218682128906" Y="2.3008125" />
                  <Point X="2.222931884766" Y="2.294549804688" />
                  <Point X="2.261639892578" Y="2.23750390625" />
                  <Point X="2.281202636719" Y="2.219953857422" />
                  <Point X="2.338248535156" Y="2.18124609375" />
                  <Point X="2.360336914062" Y="2.172980224609" />
                  <Point X="2.367204833984" Y="2.172151855469" />
                  <Point X="2.42976171875" Y="2.164608642578" />
                  <Point X="2.456606689453" Y="2.1680703125" />
                  <Point X="2.528950683594" Y="2.187416015625" />
                  <Point X="2.541033935547" Y="2.192417480469" />
                  <Point X="3.885782714844" Y="2.96880859375" />
                  <Point X="3.994248046875" Y="3.031430908203" />
                  <Point X="4.202591796875" Y="2.741880126953" />
                  <Point X="4.222250488281" Y="2.709394042969" />
                  <Point X="4.387512695312" Y="2.436296142578" />
                  <Point X="3.400838134766" Y="1.679193603516" />
                  <Point X="3.288616210938" Y="1.593082641602" />
                  <Point X="3.273655273438" Y="1.576376098633" />
                  <Point X="3.221589111328" Y="1.508451904297" />
                  <Point X="3.210990234375" Y="1.483886230469" />
                  <Point X="3.191595703125" Y="1.414535522461" />
                  <Point X="3.190779541016" Y="1.390965332031" />
                  <Point X="3.19252734375" Y="1.382494018555" />
                  <Point X="3.208448486328" Y="1.305332519531" />
                  <Point X="3.220400634766" Y="1.280728759766" />
                  <Point X="3.263704101562" Y="1.214909667969" />
                  <Point X="3.280947998047" Y="1.198819824219" />
                  <Point X="3.287837158203" Y="1.194941772461" />
                  <Point X="3.350590087891" Y="1.159617431641" />
                  <Point X="3.377880126953" Y="1.152388549805" />
                  <Point X="3.462726074219" Y="1.141175048828" />
                  <Point X="3.475803710938" Y="1.14117175293" />
                  <Point X="4.753225097656" Y="1.30934753418" />
                  <Point X="4.848975585938" Y="1.32195324707" />
                  <Point X="4.939188476562" Y="0.951386230469" />
                  <Point X="4.945383300781" Y="0.91159552002" />
                  <Point X="4.997858398438" Y="0.574556152344" />
                  <Point X="3.869776611328" Y="0.272287506104" />
                  <Point X="3.741167724609" Y="0.237826919556" />
                  <Point X="3.719935302734" Y="0.227529556274" />
                  <Point X="3.636576904297" Y="0.179346878052" />
                  <Point X="3.616773925781" Y="0.159929840088" />
                  <Point X="3.566759033203" Y="0.096198890686" />
                  <Point X="3.556985351562" Y="0.074735275269" />
                  <Point X="3.555155029297" Y="0.065177963257" />
                  <Point X="3.538483154297" Y="-0.021875303268" />
                  <Point X="3.540313476562" Y="-0.050241950989" />
                  <Point X="3.556985351562" Y="-0.13729536438" />
                  <Point X="3.566759033203" Y="-0.158758987427" />
                  <Point X="3.57225" Y="-0.165755722046" />
                  <Point X="3.622264892578" Y="-0.229486679077" />
                  <Point X="3.645728515625" Y="-0.247196807861" />
                  <Point X="3.729086914062" Y="-0.295379486084" />
                  <Point X="3.741167724609" Y="-0.300387023926" />
                  <Point X="4.91262109375" Y="-0.614276855469" />
                  <Point X="4.998067871094" Y="-0.637172363281" />
                  <Point X="4.948431640625" Y="-0.966399047852" />
                  <Point X="4.940494628906" Y="-1.001180236816" />
                  <Point X="4.874545410156" Y="-1.290178344727" />
                  <Point X="3.561132324219" Y="-1.117264160156" />
                  <Point X="3.411982177734" Y="-1.097628295898" />
                  <Point X="3.376874755859" Y="-1.102245117188" />
                  <Point X="3.213271728516" Y="-1.137804931641" />
                  <Point X="3.1979453125" Y="-1.143923217773" />
                  <Point X="3.174588867188" Y="-1.167754272461" />
                  <Point X="3.075701171875" Y="-1.286685424805" />
                  <Point X="3.067953613281" Y="-1.299517944336" />
                  <Point X="3.062802001953" Y="-1.330979858398" />
                  <Point X="3.04862890625" Y="-1.485000854492" />
                  <Point X="3.049849121094" Y="-1.501458129883" />
                  <Point X="3.066300537109" Y="-1.532083007812" />
                  <Point X="3.156840820312" Y="-1.672912475586" />
                  <Point X="3.168460693359" Y="-1.685540649414" />
                  <Point X="4.255576660156" Y="-2.519714355469" />
                  <Point X="4.33907421875" Y="-2.583784179688" />
                  <Point X="4.204131835938" Y="-2.802141601562" />
                  <Point X="4.187716308594" Y="-2.825465332031" />
                  <Point X="4.056688476562" Y="-3.011638427734" />
                  <Point X="2.88629296875" Y="-2.335909912109" />
                  <Point X="2.753454589844" Y="-2.259215576172" />
                  <Point X="2.737340820313" Y="-2.253312744141" />
                  <Point X="2.715964111328" Y="-2.249452148438" />
                  <Point X="2.521250488281" Y="-2.214287109375" />
                  <Point X="2.489077636719" Y="-2.219244873047" />
                  <Point X="2.471318603516" Y="-2.228591064453" />
                  <Point X="2.309559570312" Y="-2.313723876953" />
                  <Point X="2.288599853516" Y="-2.334683837891" />
                  <Point X="2.279253417969" Y="-2.352442626953" />
                  <Point X="2.194120849609" Y="-2.514201904297" />
                  <Point X="2.189163085938" Y="-2.546374511719" />
                  <Point X="2.193023681641" Y="-2.567751220703" />
                  <Point X="2.228188720703" Y="-2.76246484375" />
                  <Point X="2.234091552734" Y="-2.778578613281" />
                  <Point X="2.932673095703" Y="-3.988556640625" />
                  <Point X="2.986673828125" Y="-4.082088623047" />
                  <Point X="2.835296630859" Y="-4.190213378906" />
                  <Point X="2.816947509766" Y="-4.202090820312" />
                  <Point X="2.679775878906" Y="-4.290879394531" />
                  <Point X="1.784545410156" Y="-3.124192626953" />
                  <Point X="1.683177612305" Y="-2.992087402344" />
                  <Point X="1.670549316406" Y="-2.980467529297" />
                  <Point X="1.649466064453" Y="-2.966912841797" />
                  <Point X="1.45742590332" Y="-2.843448730469" />
                  <Point X="1.425804931641" Y="-2.835717041016" />
                  <Point X="1.402746582031" Y="-2.837838867188" />
                  <Point X="1.192718017578" Y="-2.857166015625" />
                  <Point X="1.165332885742" Y="-2.868509277344" />
                  <Point X="1.147527832031" Y="-2.883313476562" />
                  <Point X="0.985349060059" Y="-3.018159667969" />
                  <Point X="0.968456726074" Y="-3.045986328125" />
                  <Point X="0.963133117676" Y="-3.070479003906" />
                  <Point X="0.91464251709" Y="-3.29357421875" />
                  <Point X="0.91392956543" Y="-3.310720214844" />
                  <Point X="1.111902099609" Y="-4.814471191406" />
                  <Point X="1.127642211914" Y="-4.934028808594" />
                  <Point X="0.994345947266" Y="-4.963247070312" />
                  <Point X="0.97738885498" Y="-4.966327636719" />
                  <Point X="0.860200378418" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#126" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.019116756578" Y="4.426469184036" Z="0.25" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.25" />
                  <Point X="-0.905741999842" Y="4.992935613568" Z="0.25" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.25" />
                  <Point X="-1.674690981367" Y="4.790125262255" Z="0.25" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.25" />
                  <Point X="-1.746281347703" Y="4.736646270378" Z="0.25" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.25" />
                  <Point X="-1.736731914597" Y="4.350931711748" Z="0.25" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.25" />
                  <Point X="-1.829288251499" Y="4.303788378637" Z="0.25" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.25" />
                  <Point X="-1.924895927598" Y="4.344387924461" Z="0.25" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.25" />
                  <Point X="-1.954097725343" Y="4.375072419486" Z="0.25" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.25" />
                  <Point X="-2.72200787849" Y="4.283379987571" Z="0.25" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.25" />
                  <Point X="-3.320091864116" Y="3.83845708633" Z="0.25" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.25" />
                  <Point X="-3.34136016004" Y="3.728925181891" Z="0.25" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.25" />
                  <Point X="-2.994780435264" Y="3.063226542393" Z="0.25" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.25" />
                  <Point X="-3.048756466976" Y="3.000047100911" Z="0.25" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.25" />
                  <Point X="-3.131849923214" Y="3.000784263909" Z="0.25" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.25" />
                  <Point X="-3.204934166406" Y="3.038833788706" Z="0.25" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.25" />
                  <Point X="-4.166707661684" Y="2.899023067301" Z="0.25" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.25" />
                  <Point X="-4.514021692094" Y="2.321520262217" Z="0.25" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.25" />
                  <Point X="-4.4634597207" Y="2.199295151102" Z="0.25" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.25" />
                  <Point X="-3.669764454696" Y="1.55935595192" Z="0.25" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.25" />
                  <Point X="-3.689031651478" Y="1.500086582201" Z="0.25" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.25" />
                  <Point X="-3.746819475151" Y="1.476748476052" Z="0.25" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.25" />
                  <Point X="-3.858112894101" Y="1.488684596593" Z="0.25" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.25" />
                  <Point X="-4.957365313686" Y="1.095006824121" Z="0.25" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.25" />
                  <Point X="-5.051671466152" Y="0.505136655274" Z="0.25" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.25" />
                  <Point X="-4.913545175851" Y="0.407312938614" Z="0.25" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.25" />
                  <Point X="-3.551553318211" Y="0.031712456245" Z="0.25" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.25" />
                  <Point X="-3.540471910368" Y="0.002948658545" Z="0.25" />
                  <Point X="-3.539556741714" Y="0" Z="0.25" />
                  <Point X="-3.547892690767" Y="-0.026858292516" Z="0.25" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.25" />
                  <Point X="-3.573815276912" Y="-0.047163526889" Z="0.25" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.25" />
                  <Point X="-3.723342612921" Y="-0.088399118142" Z="0.25" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.25" />
                  <Point X="-4.990344782163" Y="-0.935951681418" Z="0.25" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.25" />
                  <Point X="-4.860333250768" Y="-1.468582225236" Z="0.25" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.25" />
                  <Point X="-4.685878382942" Y="-1.499960555699" Z="0.25" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.25" />
                  <Point X="-3.195296100457" Y="-1.320907906204" Z="0.25" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.25" />
                  <Point X="-3.199618633322" Y="-1.349254406087" Z="0.25" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.25" />
                  <Point X="-3.329232831526" Y="-1.451068805677" Z="0.25" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.25" />
                  <Point X="-4.238393946334" Y="-2.795193004879" Z="0.25" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.25" />
                  <Point X="-3.896577402472" Y="-3.254812298797" Z="0.25" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.25" />
                  <Point X="-3.734684794042" Y="-3.226282655422" Z="0.25" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.25" />
                  <Point X="-2.557207177176" Y="-2.571123291956" Z="0.25" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.25" />
                  <Point X="-2.629134357747" Y="-2.700393588451" Z="0.25" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.25" />
                  <Point X="-2.930980486536" Y="-4.146314673662" Z="0.25" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.25" />
                  <Point X="-2.494581307287" Y="-4.422630015827" Z="0.25" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.25" />
                  <Point X="-2.428869924288" Y="-4.420547642742" Z="0.25" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.25" />
                  <Point X="-1.993775362114" Y="-4.001135887661" Z="0.25" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.25" />
                  <Point X="-1.68929301696" Y="-4.002368587326" Z="0.25" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.25" />
                  <Point X="-1.448481356982" Y="-4.188703915187" Z="0.25" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.25" />
                  <Point X="-1.370866224661" Y="-4.483130111879" Z="0.25" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.25" />
                  <Point X="-1.369648759305" Y="-4.549465664335" Z="0.25" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.25" />
                  <Point X="-1.146653743667" Y="-4.948057311419" Z="0.25" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.25" />
                  <Point X="-0.847319301783" Y="-5.00800754434" Z="0.25" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.25" />
                  <Point X="-0.778040495755" Y="-4.865870781312" Z="0.25" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.25" />
                  <Point X="-0.269555874626" Y="-3.30620917091" Z="0.25" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.25" />
                  <Point X="-0.025063597175" Y="-3.212018184068" Z="0.25" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.25" />
                  <Point X="0.228295482187" Y="-3.27509386122" Z="0.25" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.25" />
                  <Point X="0.400889984807" Y="-3.495436584858" Z="0.25" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.25" />
                  <Point X="0.456714364646" Y="-3.666665248503" Z="0.25" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.25" />
                  <Point X="0.980169782685" Y="-4.984242322291" Z="0.25" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.25" />
                  <Point X="1.159341128603" Y="-4.945637721276" Z="0.25" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.25" />
                  <Point X="1.155318397258" Y="-4.776664808499" Z="0.25" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.25" />
                  <Point X="1.005836432452" Y="-3.049818673994" Z="0.25" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.25" />
                  <Point X="1.173338824455" Y="-2.890479652521" Z="0.25" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.25" />
                  <Point X="1.401172307812" Y="-2.856348421006" Z="0.25" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.25" />
                  <Point X="1.616270618451" Y="-2.977690669625" Z="0.25" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.25" />
                  <Point X="1.738721832971" Y="-3.123350508847" Z="0.25" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.25" />
                  <Point X="2.837960718078" Y="-4.212784846692" Z="0.25" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.25" />
                  <Point X="3.027792095669" Y="-4.078486598001" Z="0.25" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.25" />
                  <Point X="2.969818300661" Y="-3.932276600952" Z="0.25" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.25" />
                  <Point X="2.236071746353" Y="-2.527584971089" Z="0.25" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.25" />
                  <Point X="2.317343834867" Y="-2.3444490492" Z="0.25" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.25" />
                  <Point X="2.488449329172" Y="-2.2415574751" Z="0.25" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.25" />
                  <Point X="2.700921832423" Y="-2.267376263138" Z="0.25" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.25" />
                  <Point X="2.855136996155" Y="-2.347931199613" Z="0.25" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.25" />
                  <Point X="4.222449192394" Y="-2.8229623523" Z="0.25" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.25" />
                  <Point X="4.383431332441" Y="-2.565876769061" Z="0.25" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.25" />
                  <Point X="4.279858684945" Y="-2.448766479465" Z="0.25" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.25" />
                  <Point X="3.102202541878" Y="-1.473763569655" Z="0.25" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.25" />
                  <Point X="3.106435467438" Y="-1.304281541985" Z="0.25" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.25" />
                  <Point X="3.206879074862" Y="-1.16844112681" Z="0.25" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.25" />
                  <Point X="3.381338572542" Y="-1.119824384595" Z="0.25" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.25" />
                  <Point X="3.548450062182" Y="-1.135556419612" Z="0.25" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.25" />
                  <Point X="4.983085971276" Y="-0.981024214964" Z="0.25" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.25" />
                  <Point X="5.042418253526" Y="-0.606284112777" Z="0.25" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.25" />
                  <Point X="4.919406185107" Y="-0.53470063786" Z="0.25" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.25" />
                  <Point X="3.664594283302" Y="-0.172627978059" Z="0.25" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.25" />
                  <Point X="3.605427664875" Y="-0.103607280602" Z="0.25" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.25" />
                  <Point X="3.583265040437" Y="-0.009557215996" Z="0.25" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.25" />
                  <Point X="3.598106409987" Y="0.087053315246" Z="0.25" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.25" />
                  <Point X="3.649951773525" Y="0.160341457973" Z="0.25" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.25" />
                  <Point X="3.738801131051" Y="0.215520866183" Z="0.25" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.25" />
                  <Point X="3.8765616322" Y="0.255271294909" Z="0.25" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.25" />
                  <Point X="4.988632276004" Y="0.950567594062" Z="0.25" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.25" />
                  <Point X="4.890809300289" Y="1.367488253132" Z="0.25" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.25" />
                  <Point X="4.740542767826" Y="1.390199846582" Z="0.25" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.25" />
                  <Point X="3.378275494314" Y="1.233237505042" Z="0.25" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.25" />
                  <Point X="3.306207907303" Y="1.269792920688" Z="0.25" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.25" />
                  <Point X="3.256015099138" Y="1.339490375954" Z="0.25" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.25" />
                  <Point X="3.235339939004" Y="1.423877963189" Z="0.25" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.25" />
                  <Point X="3.252986724602" Y="1.501699974397" Z="0.25" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.25" />
                  <Point X="3.307182046088" Y="1.577237981017" Z="0.25" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.25" />
                  <Point X="3.425120261183" Y="1.670806145453" Z="0.25" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.25" />
                  <Point X="4.258871994499" Y="2.76656022489" Z="0.25" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.25" />
                  <Point X="4.025599872912" Y="3.096191071064" Z="0.25" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.25" />
                  <Point X="3.854626853641" Y="3.043389882309" Z="0.25" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.25" />
                  <Point X="2.437533990362" Y="2.247653058555" Z="0.25" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.25" />
                  <Point X="2.3670346569" Y="2.253072390184" Z="0.25" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.25" />
                  <Point X="2.303120899549" Y="2.292608488689" Z="0.25" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.25" />
                  <Point X="2.258150111546" Y="2.353903960833" Z="0.25" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.25" />
                  <Point X="2.246357312575" Y="2.422723786932" Z="0.25" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.25" />
                  <Point X="2.264874884102" Y="2.50193558552" Z="0.25" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.25" />
                  <Point X="2.35223547055" Y="2.657512228314" Z="0.25" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.25" />
                  <Point X="2.790607524574" Y="4.242641643271" Z="0.25" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.25" />
                  <Point X="2.395108745624" Y="4.477830317138" Z="0.25" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.25" />
                  <Point X="1.984761857027" Y="4.674258098072" Z="0.25" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.25" />
                  <Point X="1.559007690187" Y="4.832957331747" Z="0.25" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.25" />
                  <Point X="0.927273442038" Y="4.990603827457" Z="0.25" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.25" />
                  <Point X="0.259456591772" Y="5.069388864444" Z="0.25" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.25" />
                  <Point X="0.174127804975" Y="5.004978321915" Z="0.25" />
                  <Point X="0" Y="4.355124473572" Z="0.25" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>