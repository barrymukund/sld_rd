<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#157" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1610" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="0" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000475830078" Y="0.004715820312" />
                  <Width Value="9.783896972656" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.721211425781" Y="-4.101850585938" />
                  <Point X="0.563302062988" Y="-3.512524414063" />
                  <Point X="0.557721191406" Y="-3.497141845703" />
                  <Point X="0.542363037109" Y="-3.467377197266" />
                  <Point X="0.477821594238" Y="-3.374385009766" />
                  <Point X="0.378635528564" Y="-3.231476806641" />
                  <Point X="0.356751586914" Y="-3.209021484375" />
                  <Point X="0.330496398926" Y="-3.189777587891" />
                  <Point X="0.302495178223" Y="-3.175669189453" />
                  <Point X="0.202621032715" Y="-3.144671875" />
                  <Point X="0.049136188507" Y="-3.097035888672" />
                  <Point X="0.020976791382" Y="-3.092766601562" />
                  <Point X="-0.008664840698" Y="-3.092766601562" />
                  <Point X="-0.036824237823" Y="-3.097035888672" />
                  <Point X="-0.136698379517" Y="-3.128032958984" />
                  <Point X="-0.290183227539" Y="-3.175669189453" />
                  <Point X="-0.31818460083" Y="-3.189777587891" />
                  <Point X="-0.344439788818" Y="-3.209021484375" />
                  <Point X="-0.366323425293" Y="-3.2314765625" />
                  <Point X="-0.430865020752" Y="-3.324468505859" />
                  <Point X="-0.530051086426" Y="-3.467376953125" />
                  <Point X="-0.538189025879" Y="-3.481573730469" />
                  <Point X="-0.550990112305" Y="-3.512524414063" />
                  <Point X="-0.793661865234" Y="-4.418187988281" />
                  <Point X="-0.916584472656" Y="-4.87694140625" />
                  <Point X="-1.079341796875" Y="-4.845350097656" />
                  <Point X="-1.191679321289" Y="-4.816446289062" />
                  <Point X="-1.24641796875" Y="-4.802362792969" />
                  <Point X="-1.233971557617" Y="-4.707822753906" />
                  <Point X="-1.214963012695" Y="-4.563438476562" />
                  <Point X="-1.214201171875" Y="-4.5479296875" />
                  <Point X="-1.216508666992" Y="-4.516224121094" />
                  <Point X="-1.240368774414" Y="-4.396271972656" />
                  <Point X="-1.277036132812" Y="-4.211932617188" />
                  <Point X="-1.287938354492" Y="-4.182966308594" />
                  <Point X="-1.304010375977" Y="-4.15512890625" />
                  <Point X="-1.323644775391" Y="-4.131204101562" />
                  <Point X="-1.415596313477" Y="-4.050564697266" />
                  <Point X="-1.556905517578" Y="-3.926639404297" />
                  <Point X="-1.583188476562" Y="-3.910295410156" />
                  <Point X="-1.612885742188" Y="-3.897994384766" />
                  <Point X="-1.64302734375" Y="-3.890966308594" />
                  <Point X="-1.765067626953" Y="-3.882967285156" />
                  <Point X="-1.952616577148" Y="-3.870674804688" />
                  <Point X="-1.98341796875" Y="-3.873708496094" />
                  <Point X="-2.014466674805" Y="-3.882028076172" />
                  <Point X="-2.042657714844" Y="-3.894801513672" />
                  <Point X="-2.144348388672" Y="-3.962749023438" />
                  <Point X="-2.300624267578" Y="-4.067169677734" />
                  <Point X="-2.312785644531" Y="-4.076821533203" />
                  <Point X="-2.335102783203" Y="-4.099461914062" />
                  <Point X="-2.471344238281" Y="-4.277015136719" />
                  <Point X="-2.480149169922" Y="-4.288490234375" />
                  <Point X="-2.541824462891" Y="-4.250302246094" />
                  <Point X="-2.801706542969" Y="-4.089389892578" />
                  <Point X="-2.957264160156" Y="-3.969615478516" />
                  <Point X="-3.104721923828" Y="-3.856077880859" />
                  <Point X="-2.717218261719" Y="-3.184901855469" />
                  <Point X="-2.423761230469" Y="-2.676619628906" />
                  <Point X="-2.412859130859" Y="-2.647653320312" />
                  <Point X="-2.406588134766" Y="-2.616126953125" />
                  <Point X="-2.405575439453" Y="-2.585192871094" />
                  <Point X="-2.414560058594" Y="-2.555575195312" />
                  <Point X="-2.428777099609" Y="-2.526746337891" />
                  <Point X="-2.4468046875" Y="-2.501589355469" />
                  <Point X="-2.464153320312" Y="-2.484240722656" />
                  <Point X="-2.489310302734" Y="-2.466213134766" />
                  <Point X="-2.518139160156" Y="-2.45199609375" />
                  <Point X="-2.547756835938" Y="-2.443011474609" />
                  <Point X="-2.578690917969" Y="-2.444024169922" />
                  <Point X="-2.610217285156" Y="-2.450295166016" />
                  <Point X="-2.63918359375" Y="-2.461197265625" />
                  <Point X="-3.420300537109" Y="-2.912175537109" />
                  <Point X="-3.818024169922" Y="-3.141801269531" />
                  <Point X="-3.877536865234" Y="-3.063613769531" />
                  <Point X="-4.082860107422" Y="-2.793861083984" />
                  <Point X="-4.19437890625" Y="-2.606861083984" />
                  <Point X="-4.306142578125" Y="-2.419450439453" />
                  <Point X="-3.619408935547" Y="-1.892500976562" />
                  <Point X="-3.105954589844" Y="-1.498513427734" />
                  <Point X="-3.084577148438" Y="-1.475593261719" />
                  <Point X="-3.066612304688" Y="-1.448462036133" />
                  <Point X="-3.053856445312" Y="-1.419832885742" />
                  <Point X="-3.046151855469" Y="-1.390085571289" />
                  <Point X="-3.04334765625" Y="-1.359656982422" />
                  <Point X="-3.045556396484" Y="-1.327986328125" />
                  <Point X="-3.052557617188" Y="-1.298240722656" />
                  <Point X="-3.068640380859" Y="-1.272256835938" />
                  <Point X="-3.089473144531" Y="-1.248300415039" />
                  <Point X="-3.112972412109" Y="-1.228767211914" />
                  <Point X="-3.139455078125" Y="-1.213180541992" />
                  <Point X="-3.168718261719" Y="-1.201956542969" />
                  <Point X="-3.200605957031" Y="-1.195474853516" />
                  <Point X="-3.231929199219" Y="-1.194383789062" />
                  <Point X="-4.218015625" Y="-1.324204467773" />
                  <Point X="-4.7321015625" Y="-1.391885253906" />
                  <Point X="-4.7537734375" Y="-1.307041259766" />
                  <Point X="-4.834077636719" Y="-0.992654846191" />
                  <Point X="-4.863582519531" Y="-0.786359436035" />
                  <Point X="-4.892424316406" Y="-0.584698303223" />
                  <Point X="-4.117777832031" Y="-0.377132202148" />
                  <Point X="-3.532875976562" Y="-0.220408447266" />
                  <Point X="-3.511120605469" Y="-0.211574996948" />
                  <Point X="-3.484612304688" Y="-0.196761474609" />
                  <Point X="-3.476788818359" Y="-0.191876541138" />
                  <Point X="-3.459976074219" Y="-0.18020765686" />
                  <Point X="-3.436020263672" Y="-0.158680282593" />
                  <Point X="-3.414674316406" Y="-0.127284721375" />
                  <Point X="-3.403602050781" Y="-0.100797294617" />
                  <Point X="-3.398850341797" Y="-0.086223747253" />
                  <Point X="-3.390999023438" Y="-0.053345943451" />
                  <Point X="-3.388403808594" Y="-0.032046295166" />
                  <Point X="-3.390655029297" Y="-0.010707561493" />
                  <Point X="-3.396548828125" Y="0.01586319828" />
                  <Point X="-3.4010078125" Y="0.030368442535" />
                  <Point X="-3.414037597656" Y="0.063162910461" />
                  <Point X="-3.425296630859" Y="0.083688453674" />
                  <Point X="-3.441233398438" Y="0.100837287903" />
                  <Point X="-3.463991943359" Y="0.119948158264" />
                  <Point X="-3.470915771484" Y="0.125240333557" />
                  <Point X="-3.487728515625" Y="0.136909378052" />
                  <Point X="-3.501925292969" Y="0.145047164917" />
                  <Point X="-3.532875976562" Y="0.157848251343" />
                  <Point X="-4.431740234375" Y="0.39869833374" />
                  <Point X="-4.89181640625" Y="0.521975280762" />
                  <Point X="-4.8762421875" Y="0.627225463867" />
                  <Point X="-4.824487792969" Y="0.976974853516" />
                  <Point X="-4.765094726562" Y="1.196153808594" />
                  <Point X="-4.703551269531" Y="1.423267822266" />
                  <Point X="-4.184938476563" Y="1.354991333008" />
                  <Point X="-3.765666259766" Y="1.29979284668" />
                  <Point X="-3.744986083984" Y="1.299341674805" />
                  <Point X="-3.723424560547" Y="1.301228149414" />
                  <Point X="-3.703137939453" Y="1.305263427734" />
                  <Point X="-3.678923828125" Y="1.312898071289" />
                  <Point X="-3.641711914063" Y="1.324630981445" />
                  <Point X="-3.622779052734" Y="1.332961669922" />
                  <Point X="-3.604034667969" Y="1.343783691406" />
                  <Point X="-3.587353515625" Y="1.356014648438" />
                  <Point X="-3.573715087891" Y="1.37156628418" />
                  <Point X="-3.561300537109" Y="1.389296020508" />
                  <Point X="-3.551351318359" Y="1.407431030273" />
                  <Point X="-3.541635253906" Y="1.430887695312" />
                  <Point X="-3.526703857422" Y="1.466935302734" />
                  <Point X="-3.520915527344" Y="1.486794067383" />
                  <Point X="-3.517157226562" Y="1.508109130859" />
                  <Point X="-3.5158046875" Y="1.528749145508" />
                  <Point X="-3.518951171875" Y="1.549192626953" />
                  <Point X="-3.524552978516" Y="1.570099243164" />
                  <Point X="-3.532049804688" Y="1.589377563477" />
                  <Point X="-3.543773193359" Y="1.611898193359" />
                  <Point X="-3.561789550781" Y="1.646507324219" />
                  <Point X="-3.573281738281" Y="1.663706665039" />
                  <Point X="-3.587194335938" Y="1.680286987305" />
                  <Point X="-3.602135986328" Y="1.694590087891" />
                  <Point X="-4.1177265625" Y="2.090216796875" />
                  <Point X="-4.351859863281" Y="2.269874267578" />
                  <Point X="-4.28225" Y="2.389133300781" />
                  <Point X="-4.081153808594" Y="2.733659423828" />
                  <Point X="-3.923825439453" Y="2.9358828125" />
                  <Point X="-3.750504882813" Y="3.158661621094" />
                  <Point X="-3.464162109375" Y="2.993341552734" />
                  <Point X="-3.206656982422" Y="2.844670898438" />
                  <Point X="-3.187724609375" Y="2.836340332031" />
                  <Point X="-3.167082519531" Y="2.829831787109" />
                  <Point X="-3.146794677734" Y="2.825796386719" />
                  <Point X="-3.113071044922" Y="2.822845947266" />
                  <Point X="-3.061245361328" Y="2.818311767578" />
                  <Point X="-3.040565185547" Y="2.818762939453" />
                  <Point X="-3.019106445312" Y="2.821588134766" />
                  <Point X="-2.999015136719" Y="2.826504394531" />
                  <Point X="-2.980463867188" Y="2.835652832031" />
                  <Point X="-2.962209472656" Y="2.847281982422" />
                  <Point X="-2.946077392578" Y="2.860229492188" />
                  <Point X="-2.922140136719" Y="2.884166748047" />
                  <Point X="-2.885353759766" Y="2.920952880859" />
                  <Point X="-2.872406738281" Y="2.937084228516" />
                  <Point X="-2.86077734375" Y="2.955338623047" />
                  <Point X="-2.851628662109" Y="2.973890136719" />
                  <Point X="-2.846712158203" Y="2.993981933594" />
                  <Point X="-2.843886962891" Y="3.015440673828" />
                  <Point X="-2.843435791016" Y="3.036120849609" />
                  <Point X="-2.846386230469" Y="3.069844482422" />
                  <Point X="-2.850920410156" Y="3.121670410156" />
                  <Point X="-2.854955810547" Y="3.141957763672" />
                  <Point X="-2.861464355469" Y="3.162600097656" />
                  <Point X="-2.869794921875" Y="3.181532958984" />
                  <Point X="-3.098268798828" Y="3.577261230469" />
                  <Point X="-3.183333007812" Y="3.724596435547" />
                  <Point X="-3.050867675781" Y="3.82615625" />
                  <Point X="-2.700625732422" Y="4.09468359375" />
                  <Point X="-2.452840576172" Y="4.23234765625" />
                  <Point X="-2.167036865234" Y="4.391133789062" />
                  <Point X="-2.122025634766" Y="4.332474121094" />
                  <Point X="-2.04319519043" Y="4.229740722656" />
                  <Point X="-2.028892822266" Y="4.214800292969" />
                  <Point X="-2.012313232422" Y="4.200887695312" />
                  <Point X="-1.995113037109" Y="4.18939453125" />
                  <Point X="-1.957578613281" Y="4.16985546875" />
                  <Point X="-1.899896728516" Y="4.139827636719" />
                  <Point X="-1.88061730957" Y="4.132330566406" />
                  <Point X="-1.85971081543" Y="4.126729003906" />
                  <Point X="-1.839267822266" Y="4.123582519531" />
                  <Point X="-1.818628417969" Y="4.124935546875" />
                  <Point X="-1.797313110352" Y="4.128693847656" />
                  <Point X="-1.77745324707" Y="4.134482421875" />
                  <Point X="-1.738358764648" Y="4.150676269531" />
                  <Point X="-1.678279296875" Y="4.175562011719" />
                  <Point X="-1.660144775391" Y="4.185510742187" />
                  <Point X="-1.642415161133" Y="4.197925292969" />
                  <Point X="-1.626863769531" Y="4.211563476563" />
                  <Point X="-1.6146328125" Y="4.228244628906" />
                  <Point X="-1.603810913086" Y="4.246988769531" />
                  <Point X="-1.59548046875" Y="4.265921386719" />
                  <Point X="-1.582755859375" Y="4.306278320312" />
                  <Point X="-1.563201171875" Y="4.368297851562" />
                  <Point X="-1.559165649414" Y="4.388584960938" />
                  <Point X="-1.557279174805" Y="4.410146484375" />
                  <Point X="-1.55773034668" Y="4.430826660156" />
                  <Point X="-1.583705322266" Y="4.628125976562" />
                  <Point X="-1.584201904297" Y="4.631897949219" />
                  <Point X="-1.403045288086" Y="4.682687988281" />
                  <Point X="-0.949635009766" Y="4.80980859375" />
                  <Point X="-0.649250732422" Y="4.844963867188" />
                  <Point X="-0.294710754395" Y="4.886457519531" />
                  <Point X="-0.21008039856" Y="4.570612792969" />
                  <Point X="-0.133903305054" Y="4.286315429688" />
                  <Point X="-0.121129844666" Y="4.258124023438" />
                  <Point X="-0.10327155304" Y="4.231397460938" />
                  <Point X="-0.0821145401" Y="4.20880859375" />
                  <Point X="-0.054819446564" Y="4.19421875" />
                  <Point X="-0.024381370544" Y="4.183886230469" />
                  <Point X="0.006155911446" Y="4.178844238281" />
                  <Point X="0.03669316864" Y="4.183886230469" />
                  <Point X="0.067131248474" Y="4.19421875" />
                  <Point X="0.094426460266" Y="4.20880859375" />
                  <Point X="0.115583503723" Y="4.231397460938" />
                  <Point X="0.133441802979" Y="4.258124023438" />
                  <Point X="0.146215255737" Y="4.286315429688" />
                  <Point X="0.263282714844" Y="4.723217285156" />
                  <Point X="0.307419372559" Y="4.8879375" />
                  <Point X="0.448142028809" Y="4.873200195312" />
                  <Point X="0.844041503906" Y="4.831738769531" />
                  <Point X="1.092562866211" Y="4.77173828125" />
                  <Point X="1.481025268555" Y="4.677950683594" />
                  <Point X="1.641892211914" Y="4.619603515625" />
                  <Point X="1.89464465332" Y="4.527928710938" />
                  <Point X="2.051065917969" Y="4.454775878906" />
                  <Point X="2.294575195312" Y="4.340894042969" />
                  <Point X="2.445713378906" Y="4.252840820312" />
                  <Point X="2.680977539062" Y="4.115775390625" />
                  <Point X="2.823496337891" Y="4.014424072266" />
                  <Point X="2.943260009766" Y="3.929254638672" />
                  <Point X="2.487235351562" Y="3.139396484375" />
                  <Point X="2.147581054688" Y="2.551097900391" />
                  <Point X="2.142075683594" Y="2.539930664062" />
                  <Point X="2.133076904297" Y="2.516056640625" />
                  <Point X="2.124613525391" Y="2.484407714844" />
                  <Point X="2.111607177734" Y="2.435770263672" />
                  <Point X="2.108619384766" Y="2.417934326172" />
                  <Point X="2.107728027344" Y="2.380952880859" />
                  <Point X="2.111028076172" Y="2.353585693359" />
                  <Point X="2.116099365234" Y="2.311528076172" />
                  <Point X="2.121442138672" Y="2.289604980469" />
                  <Point X="2.129708251953" Y="2.267516357422" />
                  <Point X="2.140071044922" Y="2.247470947266" />
                  <Point X="2.157005126953" Y="2.222514648438" />
                  <Point X="2.183028808594" Y="2.184162353516" />
                  <Point X="2.19446484375" Y="2.170328857422" />
                  <Point X="2.221598632812" Y="2.145592529297" />
                  <Point X="2.246554931641" Y="2.128658447266" />
                  <Point X="2.284907226562" Y="2.102635009766" />
                  <Point X="2.304952636719" Y="2.092271972656" />
                  <Point X="2.327040771484" Y="2.084006103516" />
                  <Point X="2.348963867188" Y="2.078663330078" />
                  <Point X="2.376331298828" Y="2.07536328125" />
                  <Point X="2.418388671875" Y="2.070291748047" />
                  <Point X="2.436467529297" Y="2.069845703125" />
                  <Point X="2.473206298828" Y="2.074171142578" />
                  <Point X="2.504855224609" Y="2.082634521484" />
                  <Point X="2.553492675781" Y="2.095640869141" />
                  <Point X="2.565283447266" Y="2.099638183594" />
                  <Point X="2.588533935547" Y="2.110145019531" />
                  <Point X="3.492618408203" Y="2.632118408203" />
                  <Point X="3.967326416016" Y="2.906191162109" />
                  <Point X="3.983719970703" Y="2.883407714844" />
                  <Point X="4.123272460938" Y="2.689461181641" />
                  <Point X="4.202724121094" Y="2.558166748047" />
                  <Point X="4.262198730469" Y="2.459884277344" />
                  <Point X="3.677970703125" Y="2.01158996582" />
                  <Point X="3.230783935547" Y="1.668451171875" />
                  <Point X="3.221424560547" Y="1.660241455078" />
                  <Point X="3.203973632812" Y="1.641627685547" />
                  <Point X="3.181195800781" Y="1.611912231445" />
                  <Point X="3.14619140625" Y="1.566246337891" />
                  <Point X="3.136605957031" Y="1.550912109375" />
                  <Point X="3.121629882812" Y="1.517086303711" />
                  <Point X="3.113145019531" Y="1.486746704102" />
                  <Point X="3.100105957031" Y="1.440121582031" />
                  <Point X="3.09665234375" Y="1.417821899414" />
                  <Point X="3.095836425781" Y="1.394252075195" />
                  <Point X="3.097739501953" Y="1.371768066406" />
                  <Point X="3.104704589844" Y="1.33801159668" />
                  <Point X="3.115408447266" Y="1.286135253906" />
                  <Point X="3.120679931641" Y="1.268977783203" />
                  <Point X="3.136282470703" Y="1.235740356445" />
                  <Point X="3.155226806641" Y="1.206945800781" />
                  <Point X="3.184340087891" Y="1.16269519043" />
                  <Point X="3.198893554688" Y="1.145450439453" />
                  <Point X="3.216137451172" Y="1.129360595703" />
                  <Point X="3.234347167969" Y="1.116034790039" />
                  <Point X="3.261800048828" Y="1.100581176758" />
                  <Point X="3.303989257812" Y="1.076832519531" />
                  <Point X="3.320521240234" Y="1.069501586914" />
                  <Point X="3.356117919922" Y="1.059438598633" />
                  <Point X="3.393236083984" Y="1.054532836914" />
                  <Point X="3.450278564453" Y="1.046994018555" />
                  <Point X="3.462702148438" Y="1.046175048828" />
                  <Point X="3.488203613281" Y="1.04698449707" />
                  <Point X="4.3470234375" Y="1.160050292969" />
                  <Point X="4.776839355469" Y="1.21663671875" />
                  <Point X="4.785999511719" Y="1.179009765625" />
                  <Point X="4.845936035156" Y="0.932809509277" />
                  <Point X="4.870973632812" Y="0.771996826172" />
                  <Point X="4.890864746094" Y="0.644238525391" />
                  <Point X="4.229065429688" Y="0.466909912109" />
                  <Point X="3.716579833984" Y="0.32958984375" />
                  <Point X="3.704791259766" Y="0.325586517334" />
                  <Point X="3.681545654297" Y="0.315068054199" />
                  <Point X="3.645078125" Y="0.293989074707" />
                  <Point X="3.589035644531" Y="0.261595489502" />
                  <Point X="3.574311035156" Y="0.251096282959" />
                  <Point X="3.547530761719" Y="0.225576522827" />
                  <Point X="3.525650390625" Y="0.19769581604" />
                  <Point X="3.492024902344" Y="0.154848876953" />
                  <Point X="3.480301025391" Y="0.135569213867" />
                  <Point X="3.470527099609" Y="0.114105445862" />
                  <Point X="3.463680908203" Y="0.092604484558" />
                  <Point X="3.456387207031" Y="0.054520420074" />
                  <Point X="3.445178710938" Y="-0.004006166458" />
                  <Point X="3.443482910156" Y="-0.021875389099" />
                  <Point X="3.445178710938" Y="-0.058553871155" />
                  <Point X="3.452472412109" Y="-0.096637931824" />
                  <Point X="3.463680908203" Y="-0.155164520264" />
                  <Point X="3.470527099609" Y="-0.176665481567" />
                  <Point X="3.480301025391" Y="-0.198129257202" />
                  <Point X="3.492024902344" Y="-0.217408752441" />
                  <Point X="3.513905273438" Y="-0.245289779663" />
                  <Point X="3.547530761719" Y="-0.288136566162" />
                  <Point X="3.559999023438" Y="-0.301236083984" />
                  <Point X="3.589035644531" Y="-0.324155517578" />
                  <Point X="3.625503173828" Y="-0.34523449707" />
                  <Point X="3.681545654297" Y="-0.377628082275" />
                  <Point X="3.692710205078" Y="-0.383138977051" />
                  <Point X="3.716579833984" Y="-0.392150024414" />
                  <Point X="4.504156738281" Y="-0.603180419922" />
                  <Point X="4.89147265625" Y="-0.706961486816" />
                  <Point X="4.888488769531" Y="-0.726753173828" />
                  <Point X="4.855022460938" Y="-0.948725952148" />
                  <Point X="4.8229453125" Y="-1.089294189453" />
                  <Point X="4.801173828125" Y="-1.184698974609" />
                  <Point X="4.018721435547" Y="-1.081687133789" />
                  <Point X="3.424382080078" Y="-1.003440979004" />
                  <Point X="3.408035644531" Y="-1.002710266113" />
                  <Point X="3.374658447266" Y="-1.005508666992" />
                  <Point X="3.303085693359" Y="-1.021065368652" />
                  <Point X="3.193094238281" Y="-1.044972290039" />
                  <Point X="3.163973632812" Y="-1.056597167969" />
                  <Point X="3.136147216797" Y="-1.073489501953" />
                  <Point X="3.112397460938" Y="-1.093960083008" />
                  <Point X="3.069136230469" Y="-1.145989868164" />
                  <Point X="3.002653320312" Y="-1.225948242188" />
                  <Point X="2.987932617188" Y="-1.250330444336" />
                  <Point X="2.976589355469" Y="-1.277715820312" />
                  <Point X="2.969757568359" Y="-1.305365356445" />
                  <Point X="2.963557128906" Y="-1.372746337891" />
                  <Point X="2.954028564453" Y="-1.476295776367" />
                  <Point X="2.956347412109" Y="-1.507564697266" />
                  <Point X="2.964079101562" Y="-1.539185668945" />
                  <Point X="2.976450439453" Y="-1.567996704102" />
                  <Point X="3.016059814453" Y="-1.629606445312" />
                  <Point X="3.076930664062" Y="-1.724287231445" />
                  <Point X="3.086932373047" Y="-1.73723828125" />
                  <Point X="3.110628417969" Y="-1.760909057617" />
                  <Point X="3.841504394531" Y="-2.32173046875" />
                  <Point X="4.213122558594" Y="-2.6068828125" />
                  <Point X="4.124810058594" Y="-2.749786376953" />
                  <Point X="4.058471191406" Y="-2.844043945312" />
                  <Point X="4.028981201172" Y="-2.885945068359" />
                  <Point X="3.330293945312" Y="-2.482557617188" />
                  <Point X="2.800954589844" Y="-2.176943115234" />
                  <Point X="2.786131591797" Y="-2.170012451172" />
                  <Point X="2.754224365234" Y="-2.159824951172" />
                  <Point X="2.669041259766" Y="-2.144441162109" />
                  <Point X="2.538134033203" Y="-2.120799316406" />
                  <Point X="2.506781982422" Y="-2.120395263672" />
                  <Point X="2.474609130859" Y="-2.125353027344" />
                  <Point X="2.444833496094" Y="-2.135176757812" />
                  <Point X="2.374067138672" Y="-2.172420410156" />
                  <Point X="2.265315429688" Y="-2.229655761719" />
                  <Point X="2.242384521484" Y="-2.246548828125" />
                  <Point X="2.221424804688" Y="-2.267508544922" />
                  <Point X="2.204531738281" Y="-2.290439208984" />
                  <Point X="2.167288085938" Y="-2.361205322266" />
                  <Point X="2.110052734375" Y="-2.469957275391" />
                  <Point X="2.100229003906" Y="-2.499733154297" />
                  <Point X="2.095271240234" Y="-2.531906005859" />
                  <Point X="2.095675537109" Y="-2.563258056641" />
                  <Point X="2.111059326172" Y="-2.648440917969" />
                  <Point X="2.134701171875" Y="-2.779348388672" />
                  <Point X="2.138985595703" Y="-2.795141845703" />
                  <Point X="2.151819091797" Y="-2.826078613281" />
                  <Point X="2.62148046875" Y="-3.639555664062" />
                  <Point X="2.861283203125" Y="-4.054906494141" />
                  <Point X="2.781851806641" Y="-4.111642578125" />
                  <Point X="2.707680175781" Y="-4.159652832031" />
                  <Point X="2.701765136719" Y="-4.163481445312" />
                  <Point X="2.162480957031" Y="-3.460672851563" />
                  <Point X="1.758546020508" Y="-2.934255126953" />
                  <Point X="1.747503540039" Y="-2.922179443359" />
                  <Point X="1.721924194336" Y="-2.900557373047" />
                  <Point X="1.637910644531" Y="-2.846544433594" />
                  <Point X="1.50880078125" Y="-2.763538574219" />
                  <Point X="1.479989746094" Y="-2.751167236328" />
                  <Point X="1.448368408203" Y="-2.743435546875" />
                  <Point X="1.417099609375" Y="-2.741116699219" />
                  <Point X="1.325216552734" Y="-2.749571777344" />
                  <Point X="1.184012695312" Y="-2.762565673828" />
                  <Point X="1.156362792969" Y="-2.769397460938" />
                  <Point X="1.128977661133" Y="-2.780740722656" />
                  <Point X="1.104595825195" Y="-2.795461181641" />
                  <Point X="1.033645996094" Y="-2.854453369141" />
                  <Point X="0.924612121582" Y="-2.945111572266" />
                  <Point X="0.904141052246" Y="-2.968861816406" />
                  <Point X="0.887248779297" Y="-2.996688476562" />
                  <Point X="0.875624206543" Y="-3.025808837891" />
                  <Point X="0.854410705566" Y="-3.123408203125" />
                  <Point X="0.821809936523" Y="-3.273396728516" />
                  <Point X="0.819724487305" Y="-3.289627441406" />
                  <Point X="0.819742248535" Y="-3.323120117188" />
                  <Point X="0.952840759277" Y="-4.334103027344" />
                  <Point X="1.022065307617" Y="-4.859915527344" />
                  <Point X="0.975691101074" Y="-4.870080566406" />
                  <Point X="0.929315490723" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.05843347168" Y="-4.752635742188" />
                  <Point X="-1.141246459961" Y="-4.731328125" />
                  <Point X="-1.139784423828" Y="-4.72022265625" />
                  <Point X="-1.120775756836" Y="-4.575838378906" />
                  <Point X="-1.120077392578" Y="-4.568099609375" />
                  <Point X="-1.119451660156" Y="-4.541033691406" />
                  <Point X="-1.121759277344" Y="-4.509328125" />
                  <Point X="-1.123334106445" Y="-4.497690429688" />
                  <Point X="-1.147194213867" Y="-4.37773828125" />
                  <Point X="-1.183861572266" Y="-4.193398925781" />
                  <Point X="-1.18812512207" Y="-4.17846875" />
                  <Point X="-1.19902734375" Y="-4.149502441406" />
                  <Point X="-1.205666137695" Y="-4.135466308594" />
                  <Point X="-1.221738037109" Y="-4.10762890625" />
                  <Point X="-1.23057409668" Y="-4.094861816406" />
                  <Point X="-1.250208496094" Y="-4.070937011719" />
                  <Point X="-1.261006835938" Y="-4.059779296875" />
                  <Point X="-1.352958374023" Y="-3.979139892578" />
                  <Point X="-1.494267578125" Y="-3.855214599609" />
                  <Point X="-1.506738647461" Y="-3.845965576172" />
                  <Point X="-1.533021606445" Y="-3.829621582031" />
                  <Point X="-1.546833496094" Y="-3.822526855469" />
                  <Point X="-1.576530761719" Y="-3.810225830078" />
                  <Point X="-1.591313354492" Y="-3.805476074219" />
                  <Point X="-1.621455078125" Y="-3.798447998047" />
                  <Point X="-1.636813964844" Y="-3.796169677734" />
                  <Point X="-1.758854248047" Y="-3.788170654297" />
                  <Point X="-1.946403198242" Y="-3.775878173828" />
                  <Point X="-1.961928222656" Y="-3.776132324219" />
                  <Point X="-1.992729614258" Y="-3.779166015625" />
                  <Point X="-2.008006103516" Y="-3.781945556641" />
                  <Point X="-2.03905480957" Y="-3.790265136719" />
                  <Point X="-2.053674560547" Y="-3.795496337891" />
                  <Point X="-2.081865478516" Y="-3.808269775391" />
                  <Point X="-2.095436767578" Y="-3.815812011719" />
                  <Point X="-2.197127441406" Y="-3.883759521484" />
                  <Point X="-2.353403320312" Y="-3.988180175781" />
                  <Point X="-2.359681640625" Y="-3.992757080078" />
                  <Point X="-2.380442138672" Y="-4.010131103516" />
                  <Point X="-2.402759277344" Y="-4.032771484375" />
                  <Point X="-2.410471435547" Y="-4.041629638672" />
                  <Point X="-2.503202880859" Y="-4.162479492188" />
                  <Point X="-2.74758203125" Y="-4.011166259766" />
                  <Point X="-2.899306884766" Y="-3.894343017578" />
                  <Point X="-2.980862792969" Y="-3.831547607422" />
                  <Point X="-2.634945800781" Y="-3.232401855469" />
                  <Point X="-2.341488769531" Y="-2.724119628906" />
                  <Point X="-2.334850097656" Y="-2.710083251953" />
                  <Point X="-2.323947998047" Y="-2.681116943359" />
                  <Point X="-2.319684570312" Y="-2.666187011719" />
                  <Point X="-2.313413574219" Y="-2.634660644531" />
                  <Point X="-2.311638916016" Y="-2.619235351562" />
                  <Point X="-2.310626220703" Y="-2.588301269531" />
                  <Point X="-2.314666259766" Y="-2.557615234375" />
                  <Point X="-2.323650878906" Y="-2.527997558594" />
                  <Point X="-2.329357421875" Y="-2.513557128906" />
                  <Point X="-2.343574462891" Y="-2.484728271484" />
                  <Point X="-2.351557128906" Y="-2.47141015625" />
                  <Point X="-2.369584716797" Y="-2.446253173828" />
                  <Point X="-2.379629638672" Y="-2.434414306641" />
                  <Point X="-2.396978271484" Y="-2.417065673828" />
                  <Point X="-2.408817138672" Y="-2.407020751953" />
                  <Point X="-2.433974121094" Y="-2.388993164062" />
                  <Point X="-2.447292236328" Y="-2.381010498047" />
                  <Point X="-2.47612109375" Y="-2.366793457031" />
                  <Point X="-2.490561523438" Y="-2.361086914062" />
                  <Point X="-2.520179199219" Y="-2.352102294922" />
                  <Point X="-2.550865234375" Y="-2.348062255859" />
                  <Point X="-2.581799316406" Y="-2.349074951172" />
                  <Point X="-2.597224609375" Y="-2.350849609375" />
                  <Point X="-2.628750976562" Y="-2.357120605469" />
                  <Point X="-2.643680908203" Y="-2.361384033203" />
                  <Point X="-2.672647216797" Y="-2.372286132812" />
                  <Point X="-2.68668359375" Y="-2.378924804688" />
                  <Point X="-3.467800537109" Y="-2.829903076172" />
                  <Point X="-3.793089355469" Y="-3.017708496094" />
                  <Point X="-3.801943603516" Y="-3.006075683594" />
                  <Point X="-4.004014404297" Y="-2.740595947266" />
                  <Point X="-4.112786132813" Y="-2.558202636719" />
                  <Point X="-4.181265136719" Y="-2.443373779297" />
                  <Point X="-3.561576660156" Y="-1.967869384766" />
                  <Point X="-3.048122314453" Y="-1.573881958008" />
                  <Point X="-3.036482177734" Y="-1.563309814453" />
                  <Point X="-3.015104736328" Y="-1.540389648438" />
                  <Point X="-3.005367431641" Y="-1.528041625977" />
                  <Point X="-2.987402587891" Y="-1.500910400391" />
                  <Point X="-2.9798359375" Y="-1.487125610352" />
                  <Point X="-2.967080078125" Y="-1.458496582031" />
                  <Point X="-2.961890869141" Y="-1.443652099609" />
                  <Point X="-2.954186279297" Y="-1.413904785156" />
                  <Point X="-2.951552734375" Y="-1.398803466797" />
                  <Point X="-2.948748535156" Y="-1.36837487793" />
                  <Point X="-2.948577880859" Y="-1.353047729492" />
                  <Point X="-2.950786621094" Y="-1.321377075195" />
                  <Point X="-2.953083251953" Y="-1.306220947266" />
                  <Point X="-2.960084472656" Y="-1.276475341797" />
                  <Point X="-2.971779052734" Y="-1.248242675781" />
                  <Point X="-2.987861816406" Y="-1.222258789062" />
                  <Point X="-2.996954589844" Y="-1.20991796875" />
                  <Point X="-3.017787353516" Y="-1.185961547852" />
                  <Point X="-3.028746582031" Y="-1.175243652344" />
                  <Point X="-3.052245849609" Y="-1.155710693359" />
                  <Point X="-3.064785644531" Y="-1.146895019531" />
                  <Point X="-3.091268310547" Y="-1.13130859375" />
                  <Point X="-3.105434082031" Y="-1.124481201172" />
                  <Point X="-3.134697265625" Y="-1.113257202148" />
                  <Point X="-3.149794921875" Y="-1.108860351562" />
                  <Point X="-3.181682617188" Y="-1.102378662109" />
                  <Point X="-3.197298828125" Y="-1.100532470703" />
                  <Point X="-3.228622070312" Y="-1.09944140625" />
                  <Point X="-3.244329101562" Y="-1.100196533203" />
                  <Point X="-4.230415527344" Y="-1.230017211914" />
                  <Point X="-4.660920410156" Y="-1.286694335938" />
                  <Point X="-4.661728515625" Y="-1.283530151367" />
                  <Point X="-4.740762207031" Y="-0.974117980957" />
                  <Point X="-4.769539550781" Y="-0.772909179688" />
                  <Point X="-4.786452636719" Y="-0.654654418945" />
                  <Point X="-4.093189941406" Y="-0.468895202637" />
                  <Point X="-3.508288085938" Y="-0.312171447754" />
                  <Point X="-3.497136474609" Y="-0.308429412842" />
                  <Point X="-3.475381103516" Y="-0.299595855713" />
                  <Point X="-3.46477734375" Y="-0.294504486084" />
                  <Point X="-3.438269042969" Y="-0.279691101074" />
                  <Point X="-3.422622070312" Y="-0.269921081543" />
                  <Point X="-3.405809326172" Y="-0.258252258301" />
                  <Point X="-3.396478027344" Y="-0.250868804932" />
                  <Point X="-3.372522216797" Y="-0.229341369629" />
                  <Point X="-3.357458740234" Y="-0.212094573975" />
                  <Point X="-3.336112792969" Y="-0.180699035645" />
                  <Point X="-3.327024169922" Y="-0.163924255371" />
                  <Point X="-3.315951904297" Y="-0.137436828613" />
                  <Point X="-3.313281738281" Y="-0.130246307373" />
                  <Point X="-3.306448486328" Y="-0.108289649963" />
                  <Point X="-3.298597167969" Y="-0.075411735535" />
                  <Point X="-3.296696533203" Y="-0.064836029053" />
                  <Point X="-3.294101318359" Y="-0.043536434174" />
                  <Point X="-3.293928222656" Y="-0.022079149246" />
                  <Point X="-3.296179443359" Y="-0.000740465939" />
                  <Point X="-3.297909179688" Y="0.009864818573" />
                  <Point X="-3.303802978516" Y="0.036435623169" />
                  <Point X="-3.305742431641" Y="0.043777446747" />
                  <Point X="-3.312721191406" Y="0.065446220398" />
                  <Point X="-3.325750976562" Y="0.098240753174" />
                  <Point X="-3.330745605469" Y="0.1088516922" />
                  <Point X="-3.342004638672" Y="0.129377258301" />
                  <Point X="-3.355707275391" Y="0.148359222412" />
                  <Point X="-3.371644042969" Y="0.165508071899" />
                  <Point X="-3.380142089844" Y="0.173589157104" />
                  <Point X="-3.402900634766" Y="0.192699981689" />
                  <Point X="-3.416748535156" Y="0.203284606934" />
                  <Point X="-3.433561279297" Y="0.214953582764" />
                  <Point X="-3.440484375" Y="0.219329040527" />
                  <Point X="-3.465616699219" Y="0.232834854126" />
                  <Point X="-3.496567382812" Y="0.245635894775" />
                  <Point X="-3.508288085938" Y="0.249611251831" />
                  <Point X="-4.40715234375" Y="0.490461212158" />
                  <Point X="-4.785446289062" Y="0.591824829102" />
                  <Point X="-4.782265625" Y="0.613319458008" />
                  <Point X="-4.731331054688" Y="0.957528320312" />
                  <Point X="-4.673401367188" Y="1.171306884766" />
                  <Point X="-4.6335859375" Y="1.318237060547" />
                  <Point X="-4.197338378906" Y="1.260804077148" />
                  <Point X="-3.778066162109" Y="1.20560546875" />
                  <Point X="-3.76773828125" Y="1.204815429688" />
                  <Point X="-3.747058105469" Y="1.204364257812" />
                  <Point X="-3.736705810547" Y="1.20470324707" />
                  <Point X="-3.715144287109" Y="1.20658972168" />
                  <Point X="-3.704890869141" Y="1.208053588867" />
                  <Point X="-3.684604248047" Y="1.212088867188" />
                  <Point X="-3.674571044922" Y="1.21466027832" />
                  <Point X="-3.650356933594" Y="1.222295043945" />
                  <Point X="-3.613145019531" Y="1.234027832031" />
                  <Point X="-3.603450927734" Y="1.237676391602" />
                  <Point X="-3.584518066406" Y="1.246007080078" />
                  <Point X="-3.575279296875" Y="1.250689208984" />
                  <Point X="-3.556534912109" Y="1.261511230469" />
                  <Point X="-3.547860839844" Y="1.267171020508" />
                  <Point X="-3.5311796875" Y="1.279402099609" />
                  <Point X="-3.515928710938" Y="1.293376831055" />
                  <Point X="-3.502290283203" Y="1.308928466797" />
                  <Point X="-3.495895751953" Y="1.317076416016" />
                  <Point X="-3.483481201172" Y="1.334806152344" />
                  <Point X="-3.478011474609" Y="1.343602050781" />
                  <Point X="-3.468062255859" Y="1.361737060547" />
                  <Point X="-3.463582763672" Y="1.371076171875" />
                  <Point X="-3.453866699219" Y="1.394532836914" />
                  <Point X="-3.438935302734" Y="1.430580444336" />
                  <Point X="-3.435499267578" Y="1.44035144043" />
                  <Point X="-3.4297109375" Y="1.460210205078" />
                  <Point X="-3.427358642578" Y="1.470297973633" />
                  <Point X="-3.423600341797" Y="1.491613037109" />
                  <Point X="-3.422360595703" Y="1.501896972656" />
                  <Point X="-3.421008056641" Y="1.522536987305" />
                  <Point X="-3.421910400391" Y="1.543200561523" />
                  <Point X="-3.425056884766" Y="1.563644042969" />
                  <Point X="-3.427187988281" Y="1.573780029297" />
                  <Point X="-3.432789794922" Y="1.594686645508" />
                  <Point X="-3.436011962891" Y="1.604530395508" />
                  <Point X="-3.443508789062" Y="1.62380871582" />
                  <Point X="-3.447783691406" Y="1.633243408203" />
                  <Point X="-3.459507080078" Y="1.655764160156" />
                  <Point X="-3.4775234375" Y="1.690373168945" />
                  <Point X="-3.482799804688" Y="1.699286376953" />
                  <Point X="-3.494291992188" Y="1.716485717773" />
                  <Point X="-3.500507568359" Y="1.724771606445" />
                  <Point X="-3.514420166016" Y="1.741351928711" />
                  <Point X="-3.521501708984" Y="1.748912475586" />
                  <Point X="-3.536443359375" Y="1.763215698242" />
                  <Point X="-3.544303710938" Y="1.769958618164" />
                  <Point X="-4.059894287109" Y="2.165585449219" />
                  <Point X="-4.227614257812" Y="2.294282226563" />
                  <Point X="-4.200203613281" Y="2.341243896484" />
                  <Point X="-4.002293212891" Y="2.680312011719" />
                  <Point X="-3.848844726562" Y="2.877548339844" />
                  <Point X="-3.726338623047" Y="3.035012695312" />
                  <Point X="-3.511662109375" Y="2.911069091797" />
                  <Point X="-3.254156982422" Y="2.7623984375" />
                  <Point X="-3.244918457031" Y="2.757716552734" />
                  <Point X="-3.225986083984" Y="2.749385986328" />
                  <Point X="-3.216292236328" Y="2.745737304688" />
                  <Point X="-3.195650146484" Y="2.739228759766" />
                  <Point X="-3.185615722656" Y="2.736656982422" />
                  <Point X="-3.165327880859" Y="2.732621582031" />
                  <Point X="-3.155074462891" Y="2.731157958984" />
                  <Point X="-3.121350830078" Y="2.728207519531" />
                  <Point X="-3.069525146484" Y="2.723673339844" />
                  <Point X="-3.059173339844" Y="2.723334472656" />
                  <Point X="-3.038493164062" Y="2.723785644531" />
                  <Point X="-3.028164794922" Y="2.724575683594" />
                  <Point X="-3.006706054688" Y="2.727400878906" />
                  <Point X="-2.996526611328" Y="2.729310546875" />
                  <Point X="-2.976435302734" Y="2.734226806641" />
                  <Point X="-2.956997802734" Y="2.741301513672" />
                  <Point X="-2.938446533203" Y="2.750449951172" />
                  <Point X="-2.929420898438" Y="2.755530273438" />
                  <Point X="-2.911166503906" Y="2.767159423828" />
                  <Point X="-2.902746337891" Y="2.773193359375" />
                  <Point X="-2.886614257812" Y="2.786140869141" />
                  <Point X="-2.87890234375" Y="2.793054443359" />
                  <Point X="-2.854965087891" Y="2.816991699219" />
                  <Point X="-2.818178710938" Y="2.853777832031" />
                  <Point X="-2.811265380859" Y="2.861489501953" />
                  <Point X="-2.798318359375" Y="2.877620849609" />
                  <Point X="-2.792284667969" Y="2.886040527344" />
                  <Point X="-2.780655273438" Y="2.904294921875" />
                  <Point X="-2.775574707031" Y="2.913320800781" />
                  <Point X="-2.766426025391" Y="2.931872314453" />
                  <Point X="-2.759351318359" Y="2.951309570312" />
                  <Point X="-2.754434814453" Y="2.971401367188" />
                  <Point X="-2.752524902344" Y="2.981581542969" />
                  <Point X="-2.749699707031" Y="3.003040283203" />
                  <Point X="-2.748909667969" Y="3.013368652344" />
                  <Point X="-2.748458496094" Y="3.034048828125" />
                  <Point X="-2.748797363281" Y="3.044400634766" />
                  <Point X="-2.751747802734" Y="3.078124267578" />
                  <Point X="-2.756281982422" Y="3.129950195312" />
                  <Point X="-2.757745849609" Y="3.140203857422" />
                  <Point X="-2.76178125" Y="3.160491210938" />
                  <Point X="-2.764352783203" Y="3.170524902344" />
                  <Point X="-2.770861328125" Y="3.191167236328" />
                  <Point X="-2.774509521484" Y="3.200860595703" />
                  <Point X="-2.782840087891" Y="3.219793457031" />
                  <Point X="-2.787522460938" Y="3.229032958984" />
                  <Point X="-3.015996337891" Y="3.624761230469" />
                  <Point X="-3.059387207031" Y="3.699916259766" />
                  <Point X="-2.993065673828" Y="3.750764404297" />
                  <Point X="-2.648374023438" Y="4.015036376953" />
                  <Point X="-2.406703125" Y="4.149303710937" />
                  <Point X="-2.192525146484" Y="4.268296386719" />
                  <Point X="-2.118563720703" Y="4.171908203125" />
                  <Point X="-2.111819824219" Y="4.164046875" />
                  <Point X="-2.097517333984" Y="4.149106445313" />
                  <Point X="-2.089959472656" Y="4.14202734375" />
                  <Point X="-2.073379882812" Y="4.128114746094" />
                  <Point X="-2.065093505859" Y="4.121898925781" />
                  <Point X="-2.047893432617" Y="4.110405761719" />
                  <Point X="-2.038978881836" Y="4.105128417969" />
                  <Point X="-2.001444458008" Y="4.085589355469" />
                  <Point X="-1.943762573242" Y="4.055561523438" />
                  <Point X="-1.934327148438" Y="4.051286376953" />
                  <Point X="-1.915047729492" Y="4.043789306641" />
                  <Point X="-1.905203857422" Y="4.040567382812" />
                  <Point X="-1.884297363281" Y="4.034965820313" />
                  <Point X="-1.874162597656" Y="4.032834716797" />
                  <Point X="-1.853719604492" Y="4.029688232422" />
                  <Point X="-1.833053344727" Y="4.028785888672" />
                  <Point X="-1.81241394043" Y="4.030138916016" />
                  <Point X="-1.802132568359" Y="4.031378662109" />
                  <Point X="-1.780817260742" Y="4.035136962891" />
                  <Point X="-1.770729614258" Y="4.037489013672" />
                  <Point X="-1.750869750977" Y="4.043277587891" />
                  <Point X="-1.74109753418" Y="4.046714111328" />
                  <Point X="-1.702002929688" Y="4.062907958984" />
                  <Point X="-1.641923461914" Y="4.087793701172" />
                  <Point X="-1.632586181641" Y="4.092272460938" />
                  <Point X="-1.614451416016" Y="4.102221191406" />
                  <Point X="-1.605654418945" Y="4.10769140625" />
                  <Point X="-1.587924926758" Y="4.120105957031" />
                  <Point X="-1.57977734375" Y="4.126500488281" />
                  <Point X="-1.564225952148" Y="4.140138671875" />
                  <Point X="-1.550251220703" Y="4.155389648437" />
                  <Point X="-1.538020141602" Y="4.172070800781" />
                  <Point X="-1.532360351562" Y="4.180744628906" />
                  <Point X="-1.521538574219" Y="4.199488769531" />
                  <Point X="-1.516856201172" Y="4.208728027344" />
                  <Point X="-1.508525634766" Y="4.227660644531" />
                  <Point X="-1.504877441406" Y="4.237354003906" />
                  <Point X="-1.492152832031" Y="4.2777109375" />
                  <Point X="-1.472598144531" Y="4.33973046875" />
                  <Point X="-1.470026733398" Y="4.349763671875" />
                  <Point X="-1.465991088867" Y="4.37005078125" />
                  <Point X="-1.46452722168" Y="4.3803046875" />
                  <Point X="-1.46264074707" Y="4.401866210938" />
                  <Point X="-1.462301757812" Y="4.41221875" />
                  <Point X="-1.462752929688" Y="4.432898925781" />
                  <Point X="-1.46354309082" Y="4.4432265625" />
                  <Point X="-1.479266113281" Y="4.562655273438" />
                  <Point X="-1.377399414062" Y="4.591215332031" />
                  <Point X="-0.931178649902" Y="4.7163203125" />
                  <Point X="-0.638207763672" Y="4.750607910156" />
                  <Point X="-0.365221923828" Y="4.782557128906" />
                  <Point X="-0.301843353271" Y="4.546024902344" />
                  <Point X="-0.225666244507" Y="4.261727539062" />
                  <Point X="-0.220435317993" Y="4.247107910156" />
                  <Point X="-0.207661773682" Y="4.218916503906" />
                  <Point X="-0.200119155884" Y="4.205344726562" />
                  <Point X="-0.182260925293" Y="4.178618164062" />
                  <Point X="-0.172608306885" Y="4.166456054688" />
                  <Point X="-0.151451248169" Y="4.1438671875" />
                  <Point X="-0.126898002625" Y="4.125026367188" />
                  <Point X="-0.099602806091" Y="4.110436523438" />
                  <Point X="-0.085356712341" Y="4.104260742188" />
                  <Point X="-0.054918586731" Y="4.093927978516" />
                  <Point X="-0.039857292175" Y="4.090155273438" />
                  <Point X="-0.009319890976" Y="4.08511328125" />
                  <Point X="0.021631721497" Y="4.08511328125" />
                  <Point X="0.052169120789" Y="4.090155273438" />
                  <Point X="0.067230415344" Y="4.093927978516" />
                  <Point X="0.097668540955" Y="4.104260742188" />
                  <Point X="0.111914489746" Y="4.110436523438" />
                  <Point X="0.139209686279" Y="4.125026367188" />
                  <Point X="0.16376322937" Y="4.143866699219" />
                  <Point X="0.184920150757" Y="4.166455566406" />
                  <Point X="0.194572906494" Y="4.178617675781" />
                  <Point X="0.212431137085" Y="4.205344238281" />
                  <Point X="0.219973754883" Y="4.218916503906" />
                  <Point X="0.232747146606" Y="4.247107910156" />
                  <Point X="0.237978225708" Y="4.261727539062" />
                  <Point X="0.355045654297" Y="4.698629394531" />
                  <Point X="0.378190307617" Y="4.785006347656" />
                  <Point X="0.438247161865" Y="4.778716796875" />
                  <Point X="0.827877075195" Y="4.737912109375" />
                  <Point X="1.070267578125" Y="4.679391601562" />
                  <Point X="1.453595703125" Y="4.58684375" />
                  <Point X="1.60950012207" Y="4.530296386719" />
                  <Point X="1.858256469727" Y="4.440071289062" />
                  <Point X="2.010821289063" Y="4.368721679688" />
                  <Point X="2.250450927734" Y="4.256654296875" />
                  <Point X="2.397890625" Y="4.170755859375" />
                  <Point X="2.629430908203" Y="4.035859863281" />
                  <Point X="2.768439941406" Y="3.937004394531" />
                  <Point X="2.817779785156" Y="3.901916503906" />
                  <Point X="2.404962890625" Y="3.186896484375" />
                  <Point X="2.06530859375" Y="2.598597900391" />
                  <Point X="2.062373046875" Y="2.593104980469" />
                  <Point X="2.053180908203" Y="2.5734375" />
                  <Point X="2.044182128906" Y="2.549563476562" />
                  <Point X="2.041301513672" Y="2.540598632812" />
                  <Point X="2.032838256836" Y="2.508949707031" />
                  <Point X="2.01983190918" Y="2.460312255859" />
                  <Point X="2.017912719727" Y="2.451465576172" />
                  <Point X="2.013646972656" Y="2.420223388672" />
                  <Point X="2.012755615234" Y="2.383241943359" />
                  <Point X="2.013411254883" Y="2.369579833984" />
                  <Point X="2.016711303711" Y="2.342212646484" />
                  <Point X="2.021782592773" Y="2.300155029297" />
                  <Point X="2.02380078125" Y="2.289034423828" />
                  <Point X="2.029143676758" Y="2.267111328125" />
                  <Point X="2.032468261719" Y="2.256308837891" />
                  <Point X="2.040734375" Y="2.234220214844" />
                  <Point X="2.045318115234" Y="2.223889404297" />
                  <Point X="2.055680908203" Y="2.203843994141" />
                  <Point X="2.061459960938" Y="2.194129394531" />
                  <Point X="2.078394042969" Y="2.169173095703" />
                  <Point X="2.104417724609" Y="2.130820800781" />
                  <Point X="2.109809082031" Y="2.123632324219" />
                  <Point X="2.130462890625" Y="2.100123779297" />
                  <Point X="2.157596679688" Y="2.075387451172" />
                  <Point X="2.168257080078" Y="2.066981445312" />
                  <Point X="2.193213378906" Y="2.050047363281" />
                  <Point X="2.231565673828" Y="2.024024047852" />
                  <Point X="2.241279541016" Y="2.018245239258" />
                  <Point X="2.261324951172" Y="2.007882202148" />
                  <Point X="2.271656494141" Y="2.003297973633" />
                  <Point X="2.293744628906" Y="1.995032104492" />
                  <Point X="2.304547119141" Y="1.991707519531" />
                  <Point X="2.326470214844" Y="1.986364746094" />
                  <Point X="2.337590820312" Y="1.984346557617" />
                  <Point X="2.364958251953" Y="1.981046386719" />
                  <Point X="2.407015625" Y="1.975974975586" />
                  <Point X="2.416045410156" Y="1.975320800781" />
                  <Point X="2.447575683594" Y="1.975497314453" />
                  <Point X="2.484314453125" Y="1.979822631836" />
                  <Point X="2.497748291016" Y="1.982395996094" />
                  <Point X="2.529397216797" Y="1.99085925293" />
                  <Point X="2.578034667969" Y="2.003865600586" />
                  <Point X="2.583994384766" Y="2.005670532227" />
                  <Point X="2.604404541016" Y="2.013067260742" />
                  <Point X="2.627655029297" Y="2.02357409668" />
                  <Point X="2.636033935547" Y="2.027872558594" />
                  <Point X="3.540118408203" Y="2.549845947266" />
                  <Point X="3.940404785156" Y="2.780951416016" />
                  <Point X="4.043951904297" Y="2.637044189453" />
                  <Point X="4.121447265625" Y="2.508982666016" />
                  <Point X="4.136884765625" Y="2.483472412109" />
                  <Point X="3.620138427734" Y="2.086958496094" />
                  <Point X="3.172951660156" Y="1.743819702148" />
                  <Point X="3.168138427734" Y="1.739869262695" />
                  <Point X="3.152119384766" Y="1.725216918945" />
                  <Point X="3.134668457031" Y="1.706603149414" />
                  <Point X="3.128576171875" Y="1.699422241211" />
                  <Point X="3.105798339844" Y="1.669706665039" />
                  <Point X="3.070793945312" Y="1.624040893555" />
                  <Point X="3.065635253906" Y="1.616602172852" />
                  <Point X="3.049739013672" Y="1.589371704102" />
                  <Point X="3.034762939453" Y="1.555545898438" />
                  <Point X="3.030140380859" Y="1.542672607422" />
                  <Point X="3.021655517578" Y="1.512333007813" />
                  <Point X="3.008616455078" Y="1.465708007812" />
                  <Point X="3.006225097656" Y="1.454661132812" />
                  <Point X="3.002771484375" Y="1.432361450195" />
                  <Point X="3.001709228516" Y="1.421108642578" />
                  <Point X="3.000893310547" Y="1.397538574219" />
                  <Point X="3.001174804688" Y="1.386239746094" />
                  <Point X="3.003077880859" Y="1.363755737305" />
                  <Point X="3.004699462891" Y="1.352570800781" />
                  <Point X="3.011664550781" Y="1.318814208984" />
                  <Point X="3.022368408203" Y="1.266937988281" />
                  <Point X="3.024597900391" Y="1.25823449707" />
                  <Point X="3.03468359375" Y="1.228608886719" />
                  <Point X="3.050286132812" Y="1.195371459961" />
                  <Point X="3.056918457031" Y="1.183525756836" />
                  <Point X="3.075862792969" Y="1.154731201172" />
                  <Point X="3.104976074219" Y="1.11048059082" />
                  <Point X="3.111739013672" Y="1.101424560547" />
                  <Point X="3.126292480469" Y="1.08417980957" />
                  <Point X="3.134083007812" Y="1.075991210938" />
                  <Point X="3.151326904297" Y="1.059901367188" />
                  <Point X="3.160034667969" Y="1.052695922852" />
                  <Point X="3.178244384766" Y="1.039370117188" />
                  <Point X="3.187746337891" Y="1.033249755859" />
                  <Point X="3.21519921875" Y="1.017796081543" />
                  <Point X="3.257388427734" Y="0.994047485352" />
                  <Point X="3.265479003906" Y="0.989988037109" />
                  <Point X="3.294677978516" Y="0.978084228516" />
                  <Point X="3.330274658203" Y="0.968021240234" />
                  <Point X="3.343670410156" Y="0.965257629395" />
                  <Point X="3.380788574219" Y="0.960351867676" />
                  <Point X="3.437831054688" Y="0.952813110352" />
                  <Point X="3.444029785156" Y="0.952199768066" />
                  <Point X="3.465716064453" Y="0.951222839355" />
                  <Point X="3.491217529297" Y="0.952032226562" />
                  <Point X="3.500603515625" Y="0.952797241211" />
                  <Point X="4.359423339844" Y="1.065863037109" />
                  <Point X="4.704703613281" Y="1.11132019043" />
                  <Point X="4.75268359375" Y="0.914233459473" />
                  <Point X="4.777104492188" Y="0.757382019043" />
                  <Point X="4.78387109375" Y="0.713920837402" />
                  <Point X="4.204477539062" Y="0.558672912598" />
                  <Point X="3.691991943359" Y="0.421352752686" />
                  <Point X="3.686031738281" Y="0.419544311523" />
                  <Point X="3.665627197266" Y="0.412138122559" />
                  <Point X="3.642381591797" Y="0.401619659424" />
                  <Point X="3.634004394531" Y="0.397316558838" />
                  <Point X="3.597536865234" Y="0.376237670898" />
                  <Point X="3.541494384766" Y="0.343843963623" />
                  <Point X="3.533881835938" Y="0.338945800781" />
                  <Point X="3.508773925781" Y="0.319870513916" />
                  <Point X="3.481993652344" Y="0.294350769043" />
                  <Point X="3.472796875" Y="0.2842265625" />
                  <Point X="3.450916503906" Y="0.256345947266" />
                  <Point X="3.417291015625" Y="0.213499008179" />
                  <Point X="3.410854492188" Y="0.204208297729" />
                  <Point X="3.399130615234" Y="0.184928634644" />
                  <Point X="3.393843017578" Y="0.174939544678" />
                  <Point X="3.384069091797" Y="0.153475723267" />
                  <Point X="3.380005126953" Y="0.142928848267" />
                  <Point X="3.373158935547" Y="0.121427864075" />
                  <Point X="3.370376708984" Y="0.110473762512" />
                  <Point X="3.363083007812" Y="0.072389732361" />
                  <Point X="3.351874511719" Y="0.013863072395" />
                  <Point X="3.350603515625" Y="0.004969031811" />
                  <Point X="3.348584228516" Y="-0.02626288414" />
                  <Point X="3.350280029297" Y="-0.062941383362" />
                  <Point X="3.351874511719" Y="-0.076423118591" />
                  <Point X="3.359168212891" Y="-0.114507141113" />
                  <Point X="3.370376708984" Y="-0.173033798218" />
                  <Point X="3.373158935547" Y="-0.18398789978" />
                  <Point X="3.380005126953" Y="-0.205488876343" />
                  <Point X="3.384069091797" Y="-0.216035766602" />
                  <Point X="3.393843017578" Y="-0.237499435425" />
                  <Point X="3.399130615234" Y="-0.247488967896" />
                  <Point X="3.410854492188" Y="-0.266768493652" />
                  <Point X="3.417290527344" Y="-0.27605847168" />
                  <Point X="3.439170898438" Y="-0.30393951416" />
                  <Point X="3.472796386719" Y="-0.346786315918" />
                  <Point X="3.478718017578" Y="-0.353633239746" />
                  <Point X="3.501139404297" Y="-0.375805236816" />
                  <Point X="3.530176025391" Y="-0.39872467041" />
                  <Point X="3.541494384766" Y="-0.406404022217" />
                  <Point X="3.577961914062" Y="-0.427483062744" />
                  <Point X="3.634004394531" Y="-0.459876617432" />
                  <Point X="3.639496582031" Y="-0.462815338135" />
                  <Point X="3.659157958984" Y="-0.472016723633" />
                  <Point X="3.683027587891" Y="-0.481027740479" />
                  <Point X="3.691991943359" Y="-0.48391293335" />
                  <Point X="4.479568847656" Y="-0.694943359375" />
                  <Point X="4.784876953125" Y="-0.776750427246" />
                  <Point X="4.76161328125" Y="-0.93105291748" />
                  <Point X="4.730326171875" Y="-1.068158813477" />
                  <Point X="4.727802246094" Y="-1.079219604492" />
                  <Point X="4.031121337891" Y="-0.987499816895" />
                  <Point X="3.436781982422" Y="-0.909253723145" />
                  <Point X="3.428624511719" Y="-0.908535705566" />
                  <Point X="3.400098632812" Y="-0.908042480469" />
                  <Point X="3.366721435547" Y="-0.910840881348" />
                  <Point X="3.354480957031" Y="-0.912676208496" />
                  <Point X="3.282908203125" Y="-0.928232849121" />
                  <Point X="3.172916748047" Y="-0.952139770508" />
                  <Point X="3.157873046875" Y="-0.956742614746" />
                  <Point X="3.128752441406" Y="-0.968367431641" />
                  <Point X="3.114675537109" Y="-0.975389465332" />
                  <Point X="3.086849121094" Y="-0.992281738281" />
                  <Point X="3.074123779297" Y="-1.001530578613" />
                  <Point X="3.050374023438" Y="-1.022001159668" />
                  <Point X="3.039349609375" Y="-1.033222900391" />
                  <Point X="2.996088378906" Y="-1.085252685547" />
                  <Point X="2.92960546875" Y="-1.16521105957" />
                  <Point X="2.921326171875" Y="-1.176847167969" />
                  <Point X="2.90660546875" Y="-1.201229370117" />
                  <Point X="2.900163818359" Y="-1.213975830078" />
                  <Point X="2.888820556641" Y="-1.241361328125" />
                  <Point X="2.884362792969" Y="-1.254928222656" />
                  <Point X="2.877531005859" Y="-1.282577636719" />
                  <Point X="2.875157226562" Y="-1.29666015625" />
                  <Point X="2.868956787109" Y="-1.364041137695" />
                  <Point X="2.859428222656" Y="-1.467590576172" />
                  <Point X="2.859288818359" Y="-1.483321655273" />
                  <Point X="2.861607666016" Y="-1.514590576172" />
                  <Point X="2.864065917969" Y="-1.530128662109" />
                  <Point X="2.871797607422" Y="-1.561749511719" />
                  <Point X="2.876786376953" Y="-1.576668823242" />
                  <Point X="2.889157714844" Y="-1.605479858398" />
                  <Point X="2.896540283203" Y="-1.619371582031" />
                  <Point X="2.936149658203" Y="-1.680981323242" />
                  <Point X="2.997020507812" Y="-1.775662109375" />
                  <Point X="3.001741943359" Y="-1.782353271484" />
                  <Point X="3.01979296875" Y="-1.80444921875" />
                  <Point X="3.043489013672" Y="-1.828120117188" />
                  <Point X="3.052796142578" Y="-1.83627746582" />
                  <Point X="3.783672119141" Y="-2.397099121094" />
                  <Point X="4.087170898438" Y="-2.629981933594" />
                  <Point X="4.045488525391" Y="-2.697430664062" />
                  <Point X="4.001274414063" Y="-2.760251953125" />
                  <Point X="3.377793945312" Y="-2.40028515625" />
                  <Point X="2.848454589844" Y="-2.094670654297" />
                  <Point X="2.841191894531" Y="-2.090885253906" />
                  <Point X="2.815026611328" Y="-2.079513427734" />
                  <Point X="2.783119384766" Y="-2.069325927734" />
                  <Point X="2.771107910156" Y="-2.066337158203" />
                  <Point X="2.685924804688" Y="-2.050953369141" />
                  <Point X="2.555017578125" Y="-2.027311645508" />
                  <Point X="2.539358154297" Y="-2.025807250977" />
                  <Point X="2.508006103516" Y="-2.025403198242" />
                  <Point X="2.492313476562" Y="-2.026503540039" />
                  <Point X="2.460140625" Y="-2.031461303711" />
                  <Point X="2.444844482422" Y="-2.035136352539" />
                  <Point X="2.415068847656" Y="-2.044960083008" />
                  <Point X="2.400589355469" Y="-2.051108642578" />
                  <Point X="2.329822998047" Y="-2.088352294922" />
                  <Point X="2.221071289062" Y="-2.145587646484" />
                  <Point X="2.20896875" Y="-2.153170166016" />
                  <Point X="2.186037841797" Y="-2.170063232422" />
                  <Point X="2.175209472656" Y="-2.179373779297" />
                  <Point X="2.154249755859" Y="-2.200333496094" />
                  <Point X="2.144939453125" Y="-2.211161621094" />
                  <Point X="2.128046386719" Y="-2.234092285156" />
                  <Point X="2.120463623047" Y="-2.246194824219" />
                  <Point X="2.083219970703" Y="-2.3169609375" />
                  <Point X="2.025984741211" Y="-2.425712890625" />
                  <Point X="2.0198359375" Y="-2.440192626953" />
                  <Point X="2.010012084961" Y="-2.469968505859" />
                  <Point X="2.006337280273" Y="-2.485264648438" />
                  <Point X="2.001379516602" Y="-2.5174375" />
                  <Point X="2.000279174805" Y="-2.533130859375" />
                  <Point X="2.00068347168" Y="-2.564482910156" />
                  <Point X="2.002187866211" Y="-2.580141601562" />
                  <Point X="2.017571655273" Y="-2.665324462891" />
                  <Point X="2.041213378906" Y="-2.796231933594" />
                  <Point X="2.043015014648" Y="-2.804220947266" />
                  <Point X="2.051236083984" Y="-2.83154296875" />
                  <Point X="2.064069580078" Y="-2.862479736328" />
                  <Point X="2.069546630859" Y="-2.873578613281" />
                  <Point X="2.539208007812" Y="-3.687055664062" />
                  <Point X="2.735893066406" Y="-4.027724609375" />
                  <Point X="2.723754394531" Y="-4.036083496094" />
                  <Point X="2.237849609375" Y="-3.402840576172" />
                  <Point X="1.833914550781" Y="-2.876422851562" />
                  <Point X="1.828653442383" Y="-2.870146240234" />
                  <Point X="1.808831665039" Y="-2.849626953125" />
                  <Point X="1.783252319336" Y="-2.828004882812" />
                  <Point X="1.773299072266" Y="-2.820647216797" />
                  <Point X="1.689285522461" Y="-2.766634277344" />
                  <Point X="1.56017565918" Y="-2.683628417969" />
                  <Point X="1.546283935547" Y="-2.676245849609" />
                  <Point X="1.517472900391" Y="-2.663874511719" />
                  <Point X="1.502553344727" Y="-2.658885742188" />
                  <Point X="1.470932006836" Y="-2.651154052734" />
                  <Point X="1.455394042969" Y="-2.648695800781" />
                  <Point X="1.424125366211" Y="-2.646376953125" />
                  <Point X="1.40839453125" Y="-2.646516357422" />
                  <Point X="1.316511474609" Y="-2.654971435547" />
                  <Point X="1.175307739258" Y="-2.667965332031" />
                  <Point X="1.161225219727" Y="-2.670339111328" />
                  <Point X="1.133575317383" Y="-2.677170898438" />
                  <Point X="1.12000793457" Y="-2.68162890625" />
                  <Point X="1.092622802734" Y="-2.692972167969" />
                  <Point X="1.079876708984" Y="-2.699413574219" />
                  <Point X="1.055494873047" Y="-2.714134033203" />
                  <Point X="1.043859008789" Y="-2.722413085938" />
                  <Point X="0.972909118652" Y="-2.781405273438" />
                  <Point X="0.863875244141" Y="-2.872063476562" />
                  <Point X="0.852653320312" Y="-2.883087890625" />
                  <Point X="0.832182128906" Y="-2.906838134766" />
                  <Point X="0.822933044434" Y="-2.919564208984" />
                  <Point X="0.80604083252" Y="-2.947390869141" />
                  <Point X="0.799018859863" Y="-2.961468017578" />
                  <Point X="0.787394287109" Y="-2.990588378906" />
                  <Point X="0.782791748047" Y="-3.005631347656" />
                  <Point X="0.76157824707" Y="-3.103230712891" />
                  <Point X="0.728977478027" Y="-3.253219238281" />
                  <Point X="0.727584594727" Y="-3.261289794922" />
                  <Point X="0.724724487305" Y="-3.289677734375" />
                  <Point X="0.7247421875" Y="-3.323170410156" />
                  <Point X="0.725554992676" Y="-3.335520019531" />
                  <Point X="0.833091552734" Y="-4.152340820313" />
                  <Point X="0.81297442627" Y="-4.077262695312" />
                  <Point X="0.655064941406" Y="-3.487936523438" />
                  <Point X="0.652606262207" Y="-3.480124511719" />
                  <Point X="0.642145141602" Y="-3.453580322266" />
                  <Point X="0.62678692627" Y="-3.423815673828" />
                  <Point X="0.620407531738" Y="-3.413210205078" />
                  <Point X="0.555866149902" Y="-3.320218017578" />
                  <Point X="0.456679962158" Y="-3.177309814453" />
                  <Point X="0.446670654297" Y="-3.165172851562" />
                  <Point X="0.424786804199" Y="-3.142717529297" />
                  <Point X="0.41291229248" Y="-3.132399169922" />
                  <Point X="0.386657165527" Y="-3.113155273438" />
                  <Point X="0.373242767334" Y="-3.104937988281" />
                  <Point X="0.345241607666" Y="-3.090829589844" />
                  <Point X="0.330654724121" Y="-3.084938476562" />
                  <Point X="0.230780563354" Y="-3.053941162109" />
                  <Point X="0.077295608521" Y="-3.006305175781" />
                  <Point X="0.063376476288" Y="-3.003109375" />
                  <Point X="0.035217193604" Y="-2.998840087891" />
                  <Point X="0.020976739883" Y="-2.997766601562" />
                  <Point X="-0.008664910316" Y="-2.997766601562" />
                  <Point X="-0.02290521431" Y="-2.998840087891" />
                  <Point X="-0.051064647675" Y="-3.003109375" />
                  <Point X="-0.064983482361" Y="-3.006305175781" />
                  <Point X="-0.164857635498" Y="-3.037302246094" />
                  <Point X="-0.318342590332" Y="-3.084938476562" />
                  <Point X="-0.332929473877" Y="-3.090829589844" />
                  <Point X="-0.360930786133" Y="-3.104937988281" />
                  <Point X="-0.374345336914" Y="-3.113155273438" />
                  <Point X="-0.400600463867" Y="-3.132399169922" />
                  <Point X="-0.412474975586" Y="-3.142717529297" />
                  <Point X="-0.434358673096" Y="-3.165172607422" />
                  <Point X="-0.444367828369" Y="-3.177309326172" />
                  <Point X="-0.508909393311" Y="-3.270301269531" />
                  <Point X="-0.608095458984" Y="-3.413209716797" />
                  <Point X="-0.612470275879" Y="-3.420132324219" />
                  <Point X="-0.62597668457" Y="-3.445265136719" />
                  <Point X="-0.638777893066" Y="-3.476215820312" />
                  <Point X="-0.642753112793" Y="-3.487936523438" />
                  <Point X="-0.885424743652" Y="-4.393600097656" />
                  <Point X="-0.985425170898" Y="-4.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.752602188067" Y="4.478392594108" />
                  <Point X="0.361746680896" Y="4.723637946289" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.121775399456" Y="4.316831868256" />
                  <Point X="0.337064955441" Y="4.631524472288" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.357572142499" Y="4.754007734456" />
                  <Point X="-0.457941582039" Y="4.771705574685" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.395616500009" Y="4.172080765673" />
                  <Point X="0.312383238964" Y="4.539410996704" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.330442518464" Y="4.652758521633" />
                  <Point X="-0.786768932852" Y="4.7332211805" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.632184025169" Y="4.033902000098" />
                  <Point X="0.287701522487" Y="4.44729752112" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.30331289443" Y="4.55150930881" />
                  <Point X="-1.049641978491" Y="4.683107262818" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.812554799099" Y="3.905632237994" />
                  <Point X="0.26301980601" Y="4.355184045536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.276183299316" Y="4.450260101087" />
                  <Point X="-1.260868345765" Y="4.623886642276" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.768695647436" Y="3.816900261654" />
                  <Point X="0.238338089533" Y="4.263070569952" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.249053705859" Y="4.349010893655" />
                  <Point X="-1.472094440462" Y="4.564665973671" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.718147215048" Y="3.729347785987" />
                  <Point X="0.191582108537" Y="4.174849382782" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.22058473894" Y="4.247525518546" />
                  <Point X="-1.466705861841" Y="4.467250293744" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.66759878266" Y="3.641795310321" />
                  <Point X="0.079604396097" Y="4.098128546594" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.142995848956" Y="4.137378975709" />
                  <Point X="-1.465906427834" Y="4.37064380383" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.617050350273" Y="3.554242834654" />
                  <Point X="-1.491825677542" Y="4.278748538744" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.566501917885" Y="3.466690358987" />
                  <Point X="-1.527809038994" Y="4.188627848095" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.515953485497" Y="3.379137883321" />
                  <Point X="-1.60791318649" Y="4.106286842436" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.140233972203" Y="4.20014935935" />
                  <Point X="-2.273036477036" Y="4.223566024057" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.465405053109" Y="3.291585407654" />
                  <Point X="-1.768503803554" Y="4.038137772944" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.982917603462" Y="4.075944710904" />
                  <Point X="-2.404837007693" Y="4.150340485555" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.414856620721" Y="3.204032931987" />
                  <Point X="-2.536637403524" Y="4.077114923279" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.364308184157" Y="3.116480457057" />
                  <Point X="-2.663946156619" Y="4.003097363201" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.972416731647" Y="2.736462004098" />
                  <Point X="3.888864368733" Y="2.751194539981" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.313759746577" Y="3.028927982306" />
                  <Point X="-2.766240634669" Y="3.924669111529" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.050496704469" Y="2.626228870107" />
                  <Point X="3.760871223997" Y="2.677297656615" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.263211308996" Y="2.941375507555" />
                  <Point X="-2.868535112718" Y="3.846240859857" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.115844842399" Y="2.518240702122" />
                  <Point X="3.632878079261" Y="2.603400773248" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.212662871416" Y="2.853823032804" />
                  <Point X="-2.970829590767" Y="3.767812608186" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.067572201184" Y="2.430286943069" />
                  <Point X="3.50488493075" Y="2.529503890548" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.162114433835" Y="2.766270558053" />
                  <Point X="-3.051060463417" Y="3.685493947591" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.965346775948" Y="2.351846515523" />
                  <Point X="3.3768917723" Y="2.455607009599" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.111565996255" Y="2.678718083302" />
                  <Point X="-2.989053526788" Y="3.578094923543" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.863121350711" Y="2.273406087978" />
                  <Point X="3.24889861385" Y="2.381710128651" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.061432437319" Y="2.591092454252" />
                  <Point X="-2.927046664565" Y="3.470695912615" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.760895925475" Y="2.194965660432" />
                  <Point X="3.1209054554" Y="2.307813247703" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.03046817858" Y="2.500086760376" />
                  <Point X="-2.865039802342" Y="3.363296901686" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.658670500239" Y="2.116525232886" />
                  <Point X="2.99291229695" Y="2.233916366754" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.013319694429" Y="2.406644972681" />
                  <Point X="-2.803032940119" Y="3.255897890758" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.556445057114" Y="2.038084808495" />
                  <Point X="2.864919138499" Y="2.160019485806" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.020731437428" Y="2.308872554287" />
                  <Point X="-2.760063541936" Y="3.151855698385" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.454219603168" Y="1.959644386012" />
                  <Point X="2.736925980049" Y="2.086122604858" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.054315466069" Y="2.206485255788" />
                  <Point X="-2.749597366482" Y="3.053544701138" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.351994149221" Y="1.881203963529" />
                  <Point X="2.604324577608" Y="2.013038281659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.135264767827" Y="2.095746181689" />
                  <Point X="-2.757594430821" Y="2.958489271218" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.249768695275" Y="1.802763541045" />
                  <Point X="-2.804235896127" Y="2.870247891842" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.720919455974" Y="3.031883936215" />
                  <Point X="-3.727825408417" Y="3.033101641959" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.150751122481" Y="1.723757482564" />
                  <Point X="-2.884594301699" Y="2.787951718742" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.480371158959" Y="2.893003253159" />
                  <Point X="-3.793821464857" Y="2.9482729992" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.082509573826" Y="1.639324780668" />
                  <Point X="-3.066592193723" Y="2.723577329409" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.234696259323" Y="2.753218611741" />
                  <Point X="-3.859817526476" Y="2.863444357355" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.033321763676" Y="1.551532390591" />
                  <Point X="-3.92581361406" Y="2.778615720088" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.00730595467" Y="1.459654151514" />
                  <Point X="-3.991809701645" Y="2.693787082821" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.716767777795" Y="1.061764381477" />
                  <Point X="4.550704691819" Y="1.091045784034" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.003062551251" Y="1.363936849898" />
                  <Point X="-4.045235278498" Y="2.606741925351" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.741305223696" Y="0.960972239598" />
                  <Point X="4.237484322471" Y="1.049809457928" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.023138584164" Y="1.2639313755" />
                  <Point X="-4.096286923184" Y="2.51927817959" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.760965508731" Y="0.861040072768" />
                  <Point X="3.924263711211" Y="1.008573174478" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.07330409128" Y="1.158620314966" />
                  <Point X="-4.14733856787" Y="2.431814433828" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.77640861759" Y="0.761851507881" />
                  <Point X="3.611043099951" Y="0.967336891028" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.170537634047" Y="1.045009889817" />
                  <Point X="-4.198390212556" Y="2.344350688067" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.671664502648" Y="0.683855193287" />
                  <Point X="-4.157827828116" Y="2.240732917159" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.45453476379" Y="0.625675496433" />
                  <Point X="-3.9946043523" Y="2.115486686359" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.237405024932" Y="0.567495799578" />
                  <Point X="-3.831380253569" Y="1.990240345721" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.020275412242" Y="0.509316080477" />
                  <Point X="-3.668156154838" Y="1.864994005084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.803145822104" Y="0.4511363574" />
                  <Point X="-3.514497294271" Y="1.741434274012" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.617007486154" Y="0.387492040043" />
                  <Point X="-3.447764943651" Y="1.633202031983" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.50013151829" Y="0.311634898444" />
                  <Point X="-3.421425313155" Y="1.532092116335" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.428512406366" Y="0.227797752082" />
                  <Point X="-3.436242057854" Y="1.438239180063" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.379087559499" Y="0.140047157973" />
                  <Point X="-3.475241868184" Y="1.348650370737" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.358268954841" Y="0.047252511545" />
                  <Point X="-3.550492880927" Y="1.26545362648" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.349574428655" Y="-0.047679937033" />
                  <Point X="-3.75169473293" Y="1.204465413428" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.365377938269" Y="-0.146932050297" />
                  <Point X="-4.64774199976" Y="1.265997194431" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.400397605065" Y="-0.249572490537" />
                  <Point X="-4.672690351381" Y="1.173930733817" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.486366506246" Y="-0.361196655447" />
                  <Point X="-4.697638522998" Y="1.081864241463" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.801207667585" Y="-0.513177174957" />
                  <Point X="-4.722586689335" Y="0.989797748178" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.783945887701" Y="-0.782925966266" />
                  <Point X="-4.740366606119" Y="0.896467299093" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.769778724852" Y="-0.876893441345" />
                  <Point X="-4.754278184044" Y="0.802454757496" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.752648455514" Y="-0.970338440803" />
                  <Point X="-4.768189761968" Y="0.708442215899" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.731486828169" Y="-1.063072603076" />
                  <Point X="-4.782101339893" Y="0.614429674302" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.344171511343" Y="-0.914917010098" />
                  <Point X="-3.985740485665" Y="0.377544241193" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.121301011191" Y="-0.972084455847" />
                  <Point X="-3.381206644914" Y="0.174483086188" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.024210663976" Y="-1.051430336195" />
                  <Point X="-3.312932894517" Y="0.06597905379" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.954258267288" Y="-1.135561369423" />
                  <Point X="-3.294022942509" Y="-0.033820809082" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.896877774962" Y="-1.221909168589" />
                  <Point X="-3.3122916863" Y="-0.127065064778" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.873537749815" Y="-1.314259220554" />
                  <Point X="-3.360086404027" Y="-0.215103094636" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.864802716385" Y="-1.409184526612" />
                  <Point X="-3.462878812583" Y="-0.293443547725" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.860893536003" Y="-1.504960760768" />
                  <Point X="-3.66524182915" Y="-0.354227016136" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.889703150879" Y="-1.606506201303" />
                  <Point X="-3.882371662945" Y="-0.41240669625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.958033462237" Y="-1.715020206925" />
                  <Point X="-4.09950149052" Y="-0.470586377461" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.041585001662" Y="-1.826218125735" />
                  <Point X="-4.316631110307" Y="-0.528766095311" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.202344114158" Y="-1.951029822792" />
                  <Point X="-4.533760730094" Y="-0.586945813161" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.365568056142" Y="-2.07627613579" />
                  <Point X="-4.750890349881" Y="-0.64512553101" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.528791998126" Y="-2.201522448789" />
                  <Point X="2.788116098935" Y="-2.070921303801" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.531702009711" Y="-2.025708581637" />
                  <Point X="-4.774617103491" Y="-0.737407392313" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.692015940109" Y="-2.326768761787" />
                  <Point X="3.056311272955" Y="-2.214676877205" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.332349126604" Y="-2.087022817792" />
                  <Point X="-3.093283325193" Y="-1.130337429133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.255717688134" Y="-1.101695868352" />
                  <Point X="-4.760463440071" Y="-0.836368593179" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.855240010258" Y="-2.452015097384" />
                  <Point X="3.296859396933" Y="-2.353557529751" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.199590116651" Y="-2.160079350534" />
                  <Point X="-2.971789639076" Y="-1.24822557211" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.568938388364" Y="-1.142932136114" />
                  <Point X="-4.74630978254" Y="-0.935329793007" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.018464244546" Y="-2.577261461924" />
                  <Point X="3.537407618219" Y="-2.492438199454" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.122495957468" Y="-2.242951098345" />
                  <Point X="-2.948878942271" Y="-1.348730874233" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.882159088594" Y="-1.184168403877" />
                  <Point X="-4.725073093176" Y="-1.035539922452" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.056042276407" Y="-2.680353010952" />
                  <Point X="3.777955888845" Y="-2.631318877857" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.075740600962" Y="-2.331172395629" />
                  <Point X="-2.961705089389" Y="-1.442934806567" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.195379788824" Y="-1.225404671639" />
                  <Point X="-4.699270603275" Y="-1.13655512572" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.029282862532" Y="-2.41944617101" />
                  <Point X="-3.007901184954" Y="-1.531254716644" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.508600262286" Y="-1.266640979386" />
                  <Point X="-4.673468113373" Y="-1.237570328988" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.002346541702" Y="-2.511162099016" />
                  <Point X="-3.097659764777" Y="-1.611893385401" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.007309817761" Y="-2.608502786627" />
                  <Point X="-3.19988528529" Y="-1.690333796146" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.025304309092" Y="-2.708141229082" />
                  <Point X="-3.302110805804" Y="-1.768774206892" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.044129913774" Y="-2.807926219245" />
                  <Point X="1.643684300661" Y="-2.737316853347" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.224631151503" Y="-2.663426476799" />
                  <Point X="-3.404336326317" Y="-1.847214617637" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.092233692504" Y="-2.912873741438" />
                  <Point X="1.824288587358" Y="-2.865627790052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.038298411221" Y="-2.727036515427" />
                  <Point X="-3.506561846831" Y="-1.925655028383" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.154240565603" Y="-3.020272754284" />
                  <Point X="1.911444093156" Y="-2.977461185371" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.942578677375" Y="-2.806624071893" />
                  <Point X="-3.608787315535" Y="-2.004095448264" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.216247438702" Y="-3.12767176713" />
                  <Point X="1.997046908465" Y="-3.089020799463" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.84955161849" Y="-2.886686419605" />
                  <Point X="-3.711012723865" Y="-2.082535878791" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.278254311801" Y="-3.235070779976" />
                  <Point X="2.082649723775" Y="-3.200580413556" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.794255057853" Y="-2.973401672153" />
                  <Point X="-2.406447299064" Y="-2.409031489412" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.654205213939" Y="-2.365345084335" />
                  <Point X="-3.813238132196" Y="-2.160976309317" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.340261184899" Y="-3.342469792821" />
                  <Point X="2.168252539084" Y="-3.312140027649" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.76976835887" Y="-3.065549534583" />
                  <Point X="-2.327007167868" Y="-2.519504456022" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.789060264553" Y="-2.438032028556" />
                  <Point X="-3.915463540526" Y="-2.239416739844" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.402268057998" Y="-3.449868805667" />
                  <Point X="2.253855345378" Y="-3.423699640152" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.749575088115" Y="-3.15845444425" />
                  <Point X="0.328036074209" Y="-3.084125742677" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.0824908187" Y="-3.01173877515" />
                  <Point X="-2.311620833897" Y="-2.618683009964" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.917053376138" Y="-2.511928917768" />
                  <Point X="-4.017688948857" Y="-2.31785717037" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.464274931097" Y="-3.557267818513" />
                  <Point X="2.33945811247" Y="-3.535259245743" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.729381736457" Y="-3.25135933965" />
                  <Point X="0.477213963529" Y="-3.206895357618" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.280698174405" Y="-3.073254998694" />
                  <Point X="-2.335273267973" Y="-2.710977975806" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.045046487724" Y="-2.58582580698" />
                  <Point X="-4.119914357187" Y="-2.396297600897" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.526281804196" Y="-3.664666831359" />
                  <Point X="2.425060879562" Y="-3.646818851334" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.7271225122" Y="-3.347426505588" />
                  <Point X="0.553502626513" Y="-3.316812635353" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.415607940138" Y="-3.145932295163" />
                  <Point X="-2.384576649348" Y="-2.798749987559" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.17303959931" Y="-2.659722696192" />
                  <Point X="-4.155559691565" Y="-2.486477894839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.588288586947" Y="-3.772065828274" />
                  <Point X="2.510663646654" Y="-3.758378456924" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.740124268662" Y="-3.446184594178" />
                  <Point X="0.628140441533" Y="-3.426438824052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.481481234443" Y="-3.230782584198" />
                  <Point X="-2.435125117135" Y="-2.886302456984" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.301032710896" Y="-2.733619585403" />
                  <Point X="-4.091271614511" Y="-2.594279145491" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.650295345903" Y="-3.879464820993" />
                  <Point X="2.596266413746" Y="-3.869938062515" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.753126025124" Y="-3.544942682768" />
                  <Point X="0.666234354411" Y="-3.529621336822" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.541133294628" Y="-3.316729844662" />
                  <Point X="-2.485673584922" Y="-2.973854926408" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.429025822481" Y="-2.807516474615" />
                  <Point X="-4.026983456784" Y="-2.702080410367" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.71230210486" Y="-3.986863813713" />
                  <Point X="2.681869180837" Y="-3.981497668106" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.766127781586" Y="-3.643700771358" />
                  <Point X="0.693364002601" Y="-3.630870553904" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.600785275782" Y="-3.402677119061" />
                  <Point X="-2.536222052709" Y="-3.061407395833" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.557019024208" Y="-2.881413347933" />
                  <Point X="-3.949507359562" Y="-2.812207064796" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.779129538048" Y="-3.742458859948" />
                  <Point X="0.720493650791" Y="-3.732119770987" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.643726933414" Y="-3.491570874353" />
                  <Point X="-2.586770520496" Y="-3.148959865258" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.685012265111" Y="-2.955310214342" />
                  <Point X="-3.864700162775" Y="-2.923626389877" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.79213129451" Y="-3.841216948538" />
                  <Point X="0.747623298982" Y="-3.833368988069" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.668408645443" Y="-3.583684350722" />
                  <Point X="-2.637318987368" Y="-3.236512334844" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.805133050972" Y="-3.939975037128" />
                  <Point X="0.774752947172" Y="-3.934618205151" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.693090357472" Y="-3.67579782709" />
                  <Point X="-2.687867435674" Y="-3.324064807704" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.818134807435" Y="-4.038733125718" />
                  <Point X="0.801882595362" Y="-4.035867422233" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.717772069502" Y="-3.767911303458" />
                  <Point X="-2.73841588398" Y="-3.411617280564" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.831136563897" Y="-4.137491214308" />
                  <Point X="0.829012245093" Y="-4.137116639587" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.742453781531" Y="-3.860024779826" />
                  <Point X="-2.788964332286" Y="-3.499169753424" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.767135493561" Y="-3.952138256195" />
                  <Point X="-1.582088739431" Y="-3.808440010932" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.66047104958" Y="-3.794619094842" />
                  <Point X="-2.839512780591" Y="-3.586722226284" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.79181720559" Y="-4.044251732563" />
                  <Point X="-1.401245479227" Y="-3.936793085114" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.093492188476" Y="-3.814731312967" />
                  <Point X="-2.890061228897" Y="-3.674274699144" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.81649891762" Y="-4.136365208931" />
                  <Point X="-1.263565640573" Y="-4.057535283498" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.207978283477" Y="-3.891009853632" />
                  <Point X="-2.940609677203" Y="-3.761827172004" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.841180629649" Y="-4.2284786853" />
                  <Point X="-1.192626363755" Y="-4.166509320122" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.322205112521" Y="-3.96733410988" />
                  <Point X="-2.947766370005" Y="-3.857030782099" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.865862341678" Y="-4.320592161668" />
                  <Point X="-1.169200436486" Y="-4.267105471277" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.414936482564" Y="-4.047448595512" />
                  <Point X="-2.785268206336" Y="-3.982149120799" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.890544059318" Y="-4.412705637047" />
                  <Point X="-1.149314799095" Y="-4.367077373806" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.480135670331" Y="-4.132417747717" />
                  <Point X="-2.58027037688" Y="-4.114761297248" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.915225798394" Y="-4.504819108646" />
                  <Point X="-1.129429042799" Y="-4.467049297302" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.93990753747" Y="-4.596932580245" />
                  <Point X="-1.120009795044" Y="-4.565175692949" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.964589276546" Y="-4.689046051845" />
                  <Point X="-1.131798325118" Y="-4.659562585163" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="0.001625976562" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978485351562" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.629448486328" Y="-4.126438476562" />
                  <Point X="0.471539031982" Y="-3.537112304688" />
                  <Point X="0.464318664551" Y="-3.521544189453" />
                  <Point X="0.399777099609" Y="-3.428552001953" />
                  <Point X="0.300591033936" Y="-3.285643798828" />
                  <Point X="0.274335754395" Y="-3.266399902344" />
                  <Point X="0.174461517334" Y="-3.235402587891" />
                  <Point X="0.02097677803" Y="-3.187766601562" />
                  <Point X="-0.008664908409" Y="-3.187766601562" />
                  <Point X="-0.108539146423" Y="-3.218763671875" />
                  <Point X="-0.262023895264" Y="-3.266399902344" />
                  <Point X="-0.288279144287" Y="-3.285643798828" />
                  <Point X="-0.352820739746" Y="-3.378635742188" />
                  <Point X="-0.45200680542" Y="-3.521544189453" />
                  <Point X="-0.459227142334" Y="-3.537112304688" />
                  <Point X="-0.701898864746" Y="-4.442775878906" />
                  <Point X="-0.84774395752" Y="-4.987077148438" />
                  <Point X="-0.920140136719" Y="-4.973024414062" />
                  <Point X="-1.100246459961" Y="-4.938065429688" />
                  <Point X="-1.215351196289" Y="-4.908449707031" />
                  <Point X="-1.351589599609" Y="-4.873396972656" />
                  <Point X="-1.328158813477" Y="-4.695422851562" />
                  <Point X="-1.309150146484" Y="-4.551038574219" />
                  <Point X="-1.309683227539" Y="-4.5347578125" />
                  <Point X="-1.333543212891" Y="-4.414805664062" />
                  <Point X="-1.370210693359" Y="-4.230466308594" />
                  <Point X="-1.386282714844" Y="-4.20262890625" />
                  <Point X="-1.47823425293" Y="-4.121989257813" />
                  <Point X="-1.619543457031" Y="-3.998063964844" />
                  <Point X="-1.649240722656" Y="-3.985762939453" />
                  <Point X="-1.771281005859" Y="-3.977763916016" />
                  <Point X="-1.958829833984" Y="-3.965471435547" />
                  <Point X="-1.989878540039" Y="-3.973791015625" />
                  <Point X="-2.091569091797" Y="-4.041738525391" />
                  <Point X="-2.247844970703" Y="-4.146159179688" />
                  <Point X="-2.259734130859" Y="-4.157294433594" />
                  <Point X="-2.395975585938" Y="-4.33484765625" />
                  <Point X="-2.457095214844" Y="-4.414500488281" />
                  <Point X="-2.591835449219" Y="-4.331072753906" />
                  <Point X="-2.855831054688" Y="-4.16761328125" />
                  <Point X="-3.015221435547" Y="-4.044887939453" />
                  <Point X="-3.228581054688" Y="-3.880608154297" />
                  <Point X="-2.799490722656" Y="-3.137401855469" />
                  <Point X="-2.506033691406" Y="-2.629119628906" />
                  <Point X="-2.499762695312" Y="-2.597593261719" />
                  <Point X="-2.513979736328" Y="-2.568764404297" />
                  <Point X="-2.531328369141" Y="-2.551415771484" />
                  <Point X="-2.560157226562" Y="-2.537198730469" />
                  <Point X="-2.59168359375" Y="-2.543469726562" />
                  <Point X="-3.372800537109" Y="-2.994447998047" />
                  <Point X="-3.842959472656" Y="-3.265894042969" />
                  <Point X="-3.953130371094" Y="-3.121151855469" />
                  <Point X="-4.161703613281" Y="-2.847129394531" />
                  <Point X="-4.275971679688" Y="-2.65551953125" />
                  <Point X="-4.431020019531" Y="-2.395527099609" />
                  <Point X="-3.677241210938" Y="-1.817132446289" />
                  <Point X="-3.163786865234" Y="-1.423144897461" />
                  <Point X="-3.145822021484" Y="-1.396013671875" />
                  <Point X="-3.138117431641" Y="-1.366266357422" />
                  <Point X="-3.140326171875" Y="-1.334595703125" />
                  <Point X="-3.161158935547" Y="-1.310639282227" />
                  <Point X="-3.187641601562" Y="-1.295052734375" />
                  <Point X="-3.219529296875" Y="-1.288571044922" />
                  <Point X="-4.205615722656" Y="-1.418391723633" />
                  <Point X="-4.803283203125" Y="-1.497076293945" />
                  <Point X="-4.845818359375" Y="-1.330552246094" />
                  <Point X="-4.927393066406" Y="-1.011191589355" />
                  <Point X="-4.957625488281" Y="-0.799809753418" />
                  <Point X="-4.998396484375" Y="-0.514742126465" />
                  <Point X="-4.142365722656" Y="-0.28536932373" />
                  <Point X="-3.557463867188" Y="-0.128645431519" />
                  <Point X="-3.530955566406" Y="-0.113831886292" />
                  <Point X="-3.514142822266" Y="-0.102163047791" />
                  <Point X="-3.502324462891" Y="-0.090645339966" />
                  <Point X="-3.491252197266" Y="-0.064157852173" />
                  <Point X="-3.483400878906" Y="-0.031280042648" />
                  <Point X="-3.489294677734" Y="-0.004709283829" />
                  <Point X="-3.502324462891" Y="0.028085254669" />
                  <Point X="-3.525083007812" Y="0.047195991516" />
                  <Point X="-3.541895751953" Y="0.05886498642" />
                  <Point X="-3.557463867188" Y="0.066085334778" />
                  <Point X="-4.456328125" Y="0.306935333252" />
                  <Point X="-4.998186523438" Y="0.452125823975" />
                  <Point X="-4.97021875" Y="0.641131469727" />
                  <Point X="-4.917645019531" Y="0.996418151855" />
                  <Point X="-4.856787597656" Y="1.221000854492" />
                  <Point X="-4.773516113281" Y="1.528298583984" />
                  <Point X="-4.172538574219" Y="1.449178466797" />
                  <Point X="-3.753266357422" Y="1.393980102539" />
                  <Point X="-3.731704833984" Y="1.395866577148" />
                  <Point X="-3.707490722656" Y="1.403501220703" />
                  <Point X="-3.670278808594" Y="1.415234130859" />
                  <Point X="-3.651534423828" Y="1.426056152344" />
                  <Point X="-3.639119873047" Y="1.443785888672" />
                  <Point X="-3.629403808594" Y="1.467242553711" />
                  <Point X="-3.614472412109" Y="1.503290161133" />
                  <Point X="-3.610714111328" Y="1.524605224609" />
                  <Point X="-3.616315917969" Y="1.54551171875" />
                  <Point X="-3.628039306641" Y="1.568032348633" />
                  <Point X="-3.646055664062" Y="1.602641479492" />
                  <Point X="-3.659968261719" Y="1.619221679688" />
                  <Point X="-4.175559082031" Y="2.014848266602" />
                  <Point X="-4.47610546875" Y="2.245466308594" />
                  <Point X="-4.364296386719" Y="2.437022705078" />
                  <Point X="-4.160014648438" Y="2.787006591797" />
                  <Point X="-3.998806152344" Y="2.994217285156" />
                  <Point X="-3.774671142578" Y="3.282310791016" />
                  <Point X="-3.416662109375" Y="3.075614013672" />
                  <Point X="-3.159156982422" Y="2.926943359375" />
                  <Point X="-3.138514892578" Y="2.920434814453" />
                  <Point X="-3.104791259766" Y="2.917484375" />
                  <Point X="-3.052965576172" Y="2.912950195312" />
                  <Point X="-3.031506835938" Y="2.915775390625" />
                  <Point X="-3.013252441406" Y="2.927404541016" />
                  <Point X="-2.989315185547" Y="2.951341796875" />
                  <Point X="-2.952528808594" Y="2.988127929688" />
                  <Point X="-2.940899414062" Y="3.006382324219" />
                  <Point X="-2.93807421875" Y="3.027841064453" />
                  <Point X="-2.941024658203" Y="3.061564697266" />
                  <Point X="-2.945558837891" Y="3.113390625" />
                  <Point X="-2.952067382812" Y="3.134032958984" />
                  <Point X="-3.180541259766" Y="3.529761230469" />
                  <Point X="-3.307278564453" Y="3.749276611328" />
                  <Point X="-3.108669677734" Y="3.901548095703" />
                  <Point X="-2.752876220703" Y="4.174331542969" />
                  <Point X="-2.498978027344" Y="4.315391601562" />
                  <Point X="-2.141548583984" Y="4.513971679688" />
                  <Point X="-2.046656982422" Y="4.390306640625" />
                  <Point X="-1.967826660156" Y="4.287573242188" />
                  <Point X="-1.951246948242" Y="4.273660644531" />
                  <Point X="-1.913712646484" Y="4.254121582031" />
                  <Point X="-1.856030761719" Y="4.22409375" />
                  <Point X="-1.835124267578" Y="4.2184921875" />
                  <Point X="-1.813808959961" Y="4.222250488281" />
                  <Point X="-1.774714477539" Y="4.238444335938" />
                  <Point X="-1.714635009766" Y="4.263330078125" />
                  <Point X="-1.696905395508" Y="4.275744628906" />
                  <Point X="-1.686083496094" Y="4.294488769531" />
                  <Point X="-1.673358886719" Y="4.334845703125" />
                  <Point X="-1.653804077148" Y="4.396865234375" />
                  <Point X="-1.651917602539" Y="4.418426757812" />
                  <Point X="-1.677892578125" Y="4.615726074219" />
                  <Point X="-1.689137573242" Y="4.701141113281" />
                  <Point X="-1.428691040039" Y="4.774161132812" />
                  <Point X="-0.968094177246" Y="4.903296386719" />
                  <Point X="-0.660293701172" Y="4.939319824219" />
                  <Point X="-0.22419960022" Y="4.990358398438" />
                  <Point X="-0.118317489624" Y="4.595200683594" />
                  <Point X="-0.042140380859" Y="4.310903320312" />
                  <Point X="-0.024282121658" Y="4.284176757813" />
                  <Point X="0.006155934334" Y="4.273844238281" />
                  <Point X="0.036594028473" Y="4.284176757813" />
                  <Point X="0.054452251434" Y="4.310903320312" />
                  <Point X="0.171519744873" Y="4.747805175781" />
                  <Point X="0.236648422241" Y="4.990868652344" />
                  <Point X="0.458036865234" Y="4.96768359375" />
                  <Point X="0.860205810547" Y="4.925565429688" />
                  <Point X="1.114858032227" Y="4.864084960938" />
                  <Point X="1.508456176758" Y="4.769057617188" />
                  <Point X="1.674284423828" Y="4.708910644531" />
                  <Point X="1.931033691406" Y="4.615786132813" />
                  <Point X="2.091310546875" Y="4.540830078125" />
                  <Point X="2.338699951172" Y="4.425133789062" />
                  <Point X="2.493536376953" Y="4.33492578125" />
                  <Point X="2.732523681641" Y="4.19569140625" />
                  <Point X="2.878552734375" Y="4.09184375" />
                  <Point X="3.068740234375" Y="3.956592773438" />
                  <Point X="2.5695078125" Y="3.091896484375" />
                  <Point X="2.229853515625" Y="2.503597900391" />
                  <Point X="2.224852050781" Y="2.491514648438" />
                  <Point X="2.216388671875" Y="2.459865722656" />
                  <Point X="2.203382324219" Y="2.411228271484" />
                  <Point X="2.202044677734" Y="2.392325927734" />
                  <Point X="2.205344726562" Y="2.364958740234" />
                  <Point X="2.210416015625" Y="2.322901123047" />
                  <Point X="2.218682128906" Y="2.3008125" />
                  <Point X="2.235616210938" Y="2.275856201172" />
                  <Point X="2.261639892578" Y="2.23750390625" />
                  <Point X="2.274940185547" Y="2.224203613281" />
                  <Point X="2.299896484375" Y="2.20726953125" />
                  <Point X="2.338248779297" Y="2.18124609375" />
                  <Point X="2.360336914062" Y="2.172980224609" />
                  <Point X="2.387704345703" Y="2.169680175781" />
                  <Point X="2.42976171875" Y="2.164608642578" />
                  <Point X="2.448664306641" Y="2.165946289062" />
                  <Point X="2.480313232422" Y="2.174409667969" />
                  <Point X="2.528950683594" Y="2.187416015625" />
                  <Point X="2.541033935547" Y="2.192417480469" />
                  <Point X="3.445118408203" Y="2.714390869141" />
                  <Point X="3.994248046875" Y="3.031430908203" />
                  <Point X="4.060832519531" Y="2.938893310547" />
                  <Point X="4.202591796875" Y="2.741880126953" />
                  <Point X="4.284000976562" Y="2.607350830078" />
                  <Point X="4.387512695312" Y="2.436296142578" />
                  <Point X="3.735802978516" Y="1.936221435547" />
                  <Point X="3.288616210938" Y="1.593082763672" />
                  <Point X="3.279371337891" Y="1.583833374023" />
                  <Point X="3.256593505859" Y="1.554117797852" />
                  <Point X="3.221589111328" Y="1.508451904297" />
                  <Point X="3.213119384766" Y="1.4915" />
                  <Point X="3.204634521484" Y="1.461160400391" />
                  <Point X="3.191595458984" Y="1.41453527832" />
                  <Point X="3.190779541016" Y="1.390965332031" />
                  <Point X="3.197744628906" Y="1.357208740234" />
                  <Point X="3.208448486328" Y="1.305332519531" />
                  <Point X="3.215646484375" Y="1.287954956055" />
                  <Point X="3.234590820312" Y="1.259160400391" />
                  <Point X="3.263704101562" Y="1.214909667969" />
                  <Point X="3.280947998047" Y="1.198819824219" />
                  <Point X="3.308400878906" Y="1.183366210938" />
                  <Point X="3.350590087891" Y="1.159617431641" />
                  <Point X="3.368565429688" Y="1.153619628906" />
                  <Point X="3.40568359375" Y="1.148713867188" />
                  <Point X="3.462726074219" Y="1.141175048828" />
                  <Point X="3.475803710938" Y="1.14117175293" />
                  <Point X="4.334623535156" Y="1.254237670898" />
                  <Point X="4.848975097656" Y="1.32195324707" />
                  <Point X="4.878303710938" Y="1.201480957031" />
                  <Point X="4.939188476562" Y="0.951385620117" />
                  <Point X="4.964842773438" Y="0.786611572266" />
                  <Point X="4.997858398438" Y="0.574556152344" />
                  <Point X="4.253653320312" Y="0.375147033691" />
                  <Point X="3.741167724609" Y="0.237826919556" />
                  <Point X="3.729086914062" Y="0.232819412231" />
                  <Point X="3.692619384766" Y="0.211740493774" />
                  <Point X="3.636576904297" Y="0.179346878052" />
                  <Point X="3.622264892578" Y="0.166926574707" />
                  <Point X="3.600384521484" Y="0.139045791626" />
                  <Point X="3.566759033203" Y="0.096198890686" />
                  <Point X="3.556985107422" Y="0.07473513031" />
                  <Point X="3.54969140625" Y="0.036651138306" />
                  <Point X="3.538482910156" Y="-0.021875455856" />
                  <Point X="3.538482910156" Y="-0.040684635162" />
                  <Point X="3.545776611328" Y="-0.078768623352" />
                  <Point X="3.556985107422" Y="-0.137295227051" />
                  <Point X="3.566759033203" Y="-0.158758987427" />
                  <Point X="3.588639404297" Y="-0.186639923096" />
                  <Point X="3.622264892578" Y="-0.229486679077" />
                  <Point X="3.636576904297" Y="-0.241906967163" />
                  <Point X="3.673044433594" Y="-0.262985870361" />
                  <Point X="3.729086914062" Y="-0.295379486084" />
                  <Point X="3.741167724609" Y="-0.300387023926" />
                  <Point X="4.528744628906" Y="-0.511417510986" />
                  <Point X="4.998067871094" Y="-0.637172363281" />
                  <Point X="4.982427246094" Y="-0.740915527344" />
                  <Point X="4.948431640625" Y="-0.966399169922" />
                  <Point X="4.915564453125" Y="-1.11042956543" />
                  <Point X="4.874545898438" Y="-1.290178344727" />
                  <Point X="4.006321533203" Y="-1.175874389648" />
                  <Point X="3.411982177734" Y="-1.097628295898" />
                  <Point X="3.3948359375" Y="-1.098341186523" />
                  <Point X="3.323263183594" Y="-1.113897827148" />
                  <Point X="3.213271728516" Y="-1.137804931641" />
                  <Point X="3.1854453125" Y="-1.154697265625" />
                  <Point X="3.142184082031" Y="-1.206727050781" />
                  <Point X="3.075701171875" Y="-1.286685424805" />
                  <Point X="3.064357910156" Y="-1.314070556641" />
                  <Point X="3.058157470703" Y="-1.381451416016" />
                  <Point X="3.04862890625" Y="-1.485000854492" />
                  <Point X="3.056360595703" Y="-1.516621826172" />
                  <Point X="3.095969970703" Y="-1.578231567383" />
                  <Point X="3.156840820312" Y="-1.672912353516" />
                  <Point X="3.168460693359" Y="-1.685540649414" />
                  <Point X="3.899336669922" Y="-2.246361816406" />
                  <Point X="4.339074707031" Y="-2.583784179688" />
                  <Point X="4.299961914062" Y="-2.64707421875" />
                  <Point X="4.204131835938" Y="-2.802141845703" />
                  <Point X="4.136159179687" Y="-2.898720947266" />
                  <Point X="4.056688476562" Y="-3.011638427734" />
                  <Point X="3.282793945312" Y="-2.564830078125" />
                  <Point X="2.753454589844" Y="-2.259215576172" />
                  <Point X="2.737340820313" Y="-2.253312744141" />
                  <Point X="2.652157714844" Y="-2.237928955078" />
                  <Point X="2.521250488281" Y="-2.214287109375" />
                  <Point X="2.489077636719" Y="-2.219244873047" />
                  <Point X="2.418311279297" Y="-2.256488525391" />
                  <Point X="2.309559570312" Y="-2.313723876953" />
                  <Point X="2.288599853516" Y="-2.33468359375" />
                  <Point X="2.251356201172" Y="-2.405449707031" />
                  <Point X="2.194120849609" Y="-2.514201660156" />
                  <Point X="2.189163085938" Y="-2.546374511719" />
                  <Point X="2.204546875" Y="-2.631557373047" />
                  <Point X="2.228188720703" Y="-2.76246484375" />
                  <Point X="2.234091552734" Y="-2.778578613281" />
                  <Point X="2.703752929688" Y="-3.592055664062" />
                  <Point X="2.986673583984" Y="-4.082088623047" />
                  <Point X="2.949008544922" Y="-4.108991699219" />
                  <Point X="2.835296630859" Y="-4.190213378906" />
                  <Point X="2.759301757812" Y="-4.239403808594" />
                  <Point X="2.679775878906" Y="-4.290879394531" />
                  <Point X="2.087112304688" Y="-3.518505126953" />
                  <Point X="1.683177490234" Y="-2.992087402344" />
                  <Point X="1.670549316406" Y="-2.980467529297" />
                  <Point X="1.586535766602" Y="-2.926454589844" />
                  <Point X="1.45742590332" Y="-2.843448730469" />
                  <Point X="1.4258046875" Y="-2.835717041016" />
                  <Point X="1.333921630859" Y="-2.844172119141" />
                  <Point X="1.192717773438" Y="-2.857166015625" />
                  <Point X="1.165332763672" Y="-2.868509277344" />
                  <Point X="1.094383056641" Y="-2.927501464844" />
                  <Point X="0.985349060059" Y="-3.018159667969" />
                  <Point X="0.968456726074" Y="-3.045986328125" />
                  <Point X="0.947243225098" Y="-3.143585693359" />
                  <Point X="0.91464251709" Y="-3.29357421875" />
                  <Point X="0.91392956543" Y="-3.310720214844" />
                  <Point X="1.047027954102" Y="-4.321703125" />
                  <Point X="1.127642211914" Y="-4.934028320312" />
                  <Point X="1.101924438477" Y="-4.939666015625" />
                  <Point X="0.994346374512" Y="-4.963246582031" />
                  <Point X="0.924138427734" Y="-4.976001464844" />
                  <Point X="0.860200439453" Y="-4.987616699219" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#156" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.076177159468" Y="4.639421517591" Z="1" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1" />
                  <Point X="-0.672261821919" Y="5.020261058313" Z="1" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1" />
                  <Point X="-1.448344850338" Y="4.853584779719" Z="1" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1" />
                  <Point X="-1.733620792968" Y="4.640479712658" Z="1" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1" />
                  <Point X="-1.72720063896" Y="4.381160982191" Z="1" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1" />
                  <Point X="-1.800004618146" Y="4.315918158695" Z="1" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1" />
                  <Point X="-1.896780920762" Y="4.329752074116" Z="1" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1" />
                  <Point X="-2.013145325707" Y="4.452024786082" Z="1" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1" />
                  <Point X="-2.529416961224" Y="4.390379290713" Z="1" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1" />
                  <Point X="-3.145247878103" Y="3.972508051775" Z="1" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1" />
                  <Point X="-3.229998571226" Y="3.536041263206" Z="1" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1" />
                  <Point X="-2.996990476967" Y="3.088487178959" Z="1" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1" />
                  <Point X="-3.030826286637" Y="3.017977281249" Z="1" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1" />
                  <Point X="-3.106589286648" Y="2.998574222206" Z="1" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1" />
                  <Point X="-3.397818085091" Y="3.150195377519" Z="1" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1" />
                  <Point X="-4.044425469262" Y="3.056199602745" Z="1" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1" />
                  <Point X="-4.413634280671" Y="2.493507941984" Z="1" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1" />
                  <Point X="-4.212153064841" Y="2.006460793445" Z="1" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1" />
                  <Point X="-3.678545908584" Y="1.576224970101" Z="1" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1" />
                  <Point X="-3.681753853843" Y="1.517656741093" Z="1" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1" />
                  <Point X="-3.728681828406" Y="1.482467248474" Z="1" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1" />
                  <Point X="-4.172167965227" Y="1.53003074358" Z="1" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1" />
                  <Point X="-4.911203439858" Y="1.265358285965" Z="1" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1" />
                  <Point X="-5.025835949818" Y="0.679730609772" Z="1" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1" />
                  <Point X="-4.475425193671" Y="0.289919041395" Z="1" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1" />
                  <Point X="-3.559748065268" Y="0.037400078677" Z="1" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1" />
                  <Point X="-3.543203539653" Y="0.011749923755" Z="1" />
                  <Point X="-3.539556741714" Y="0" Z="1" />
                  <Point X="-3.545161061482" Y="-0.018057027306" Z="1" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1" />
                  <Point X="-3.565620529855" Y="-0.041475904457" Z="1" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1" />
                  <Point X="-4.161462595102" Y="-0.205793015361" Z="1" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1" />
                  <Point X="-5.013277458387" Y="-0.775608828104" Z="1" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1" />
                  <Point X="-4.900419393092" Y="-1.311646472614" Z="1" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1" />
                  <Point X="-4.205245176039" Y="-1.436684005725" Z="1" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1" />
                  <Point X="-3.203115760641" Y="-1.316305594501" Z="1" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1" />
                  <Point X="-3.197343683568" Y="-1.340470734625" Z="1" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1" />
                  <Point X="-3.713835139944" Y="-1.746184524107" Z="1" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1" />
                  <Point X="-4.325070834337" Y="-2.649849094522" Z="1" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1" />
                  <Point X="-3.999072283982" Y="-3.120155125946" Z="1" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1" />
                  <Point X="-3.353956623328" Y="-3.006469146625" Z="1" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1" />
                  <Point X="-2.562329774076" Y="-2.566000695056" Z="1" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1" />
                  <Point X="-2.848947866543" Y="-3.081121759164" Z="1" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1" />
                  <Point X="-3.051881235696" Y="-4.053225123642" Z="1" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1" />
                  <Point X="-2.624312930565" Y="-4.34230341616" Z="1" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1" />
                  <Point X="-2.36246377842" Y="-4.334005498785" Z="1" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1" />
                  <Point X="-2.069946508922" Y="-4.05203189567" Z="1" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1" />
                  <Point X="-1.780707136709" Y="-3.996376982851" Z="1" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1" />
                  <Point X="-1.517357599152" Y="-4.128300989659" Z="1" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1" />
                  <Point X="-1.388738501668" Y="-4.393280191684" Z="1" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1" />
                  <Point X="-1.383887100637" Y="-4.657616557391" Z="1" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1" />
                  <Point X="-1.23396591394" Y="-4.925592648995" Z="1" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1" />
                  <Point X="-0.935823365506" Y="-4.990828593734" Z="1" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1" />
                  <Point X="-0.659758613981" Y="-4.424436757366" Z="1" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1" />
                  <Point X="-0.317900668868" Y="-3.375864802488" Z="1" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1" />
                  <Point X="-0.099874350078" Y="-3.235236680995" Z="1" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1" />
                  <Point X="0.153484729283" Y="-3.251875364293" Z="1" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1" />
                  <Point X="0.352545190564" Y="-3.42578095328" Z="1" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1" />
                  <Point X="0.57499624642" Y="-4.108099272449" Z="1" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1" />
                  <Point X="0.926919168241" Y="-4.993916015485" Z="1" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1" />
                  <Point X="1.106474154533" Y="-4.957226131579" Z="1" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1" />
                  <Point X="1.090444225716" Y="-4.283896611999" Z="1" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1" />
                  <Point X="0.989946402976" Y="-3.122925273758" Z="1" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1" />
                  <Point X="1.12019401999" Y="-2.934667814719" Z="1" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1" />
                  <Point X="1.332347492248" Y="-2.862681743138" Z="1" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1" />
                  <Point X="1.55334046768" Y="-2.937232397195" Z="1" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1" />
                  <Point X="2.041288601438" Y="-3.517663118036" Z="1" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1" />
                  <Point X="2.78031497674" Y="-4.250097824209" Z="1" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1" />
                  <Point X="2.971914429933" Y="-4.118398713532" Z="1" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1" />
                  <Point X="2.740898304926" Y="-3.535775703409" Z="1" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1" />
                  <Point X="2.247595107807" Y="-2.591391204553" Z="1" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1" />
                  <Point X="2.289446467121" Y="-2.397456328852" Z="1" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1" />
                  <Point X="2.43544204952" Y="-2.269454842846" Z="1" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1" />
                  <Point X="2.63711559896" Y="-2.255852901684" Z="1" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1" />
                  <Point X="3.251637893698" Y="-2.576851195348" Z="1" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1" />
                  <Point X="4.170891924813" Y="-2.896218127943" Z="1" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1" />
                  <Point X="4.336338922149" Y="-2.642079384306" Z="1" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1" />
                  <Point X="3.923618801309" Y="-2.175413965951" Z="1" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1" />
                  <Point X="3.13187192758" Y="-1.519912349234" Z="1" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1" />
                  <Point X="3.10179109222" Y="-1.354753078092" Z="1" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1" />
                  <Point X="3.174474328718" Y="-1.207413976378" Z="1" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1" />
                  <Point X="3.327727056661" Y="-1.131477069357" Z="1" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1" />
                  <Point X="3.993639108407" Y="-1.194166665022" Z="1" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1" />
                  <Point X="4.958155366102" Y="-1.090273528948" Z="1" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1" />
                  <Point X="5.025712526261" Y="-0.717089671205" Z="1" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1" />
                  <Point X="4.535529481118" Y="-0.431841189756" Z="1" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1" />
                  <Point X="3.691910224156" Y="-0.188417081499" Z="1" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1" />
                  <Point X="3.621817229388" Y="-0.12449143726" Z="1" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1" />
                  <Point X="3.588728228608" Y="-0.038083948069" Z="1" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1" />
                  <Point X="3.592643221816" Y="0.05852658315" Z="1" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1" />
                  <Point X="3.633562209012" Y="0.139457301315" Z="1" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1" />
                  <Point X="3.711485190197" Y="0.199731762743" Z="1" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1" />
                  <Point X="4.260438336189" Y="0.358130743012" Z="1" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1" />
                  <Point X="5.0080915971" Y="0.825583551211" Z="1" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1" />
                  <Point X="4.920728814306" Y="1.244587885442" Z="1" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1" />
                  <Point X="4.321941171661" Y="1.33508988399" Z="1" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1" />
                  <Point X="3.406078879144" Y="1.229562949036" Z="1" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1" />
                  <Point X="3.326771510059" Y="1.258217393436" Z="1" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1" />
                  <Point X="3.270205322625" Y="1.317921859991" Z="1" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1" />
                  <Point X="3.240557152412" Y="1.398592686907" Z="1" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1" />
                  <Point X="3.246631226726" Y="1.478974236586" Z="1" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1" />
                  <Point X="3.290120380529" Y="1.554979693812" Z="1" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1" />
                  <Point X="3.760084947073" Y="1.92783358011" Z="1" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1" />
                  <Point X="4.32062236048" Y="2.664517094969" Z="1" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1" />
                  <Point X="4.095262122014" Y="2.999376348022" Z="1" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1" />
                  <Point X="3.413962504015" Y="2.788972215943" Z="1" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1" />
                  <Point X="2.461240564858" Y="2.253992505416" Z="1" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1" />
                  <Point X="2.387534125997" Y="2.250600533901" Z="1" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1" />
                  <Point X="2.321814418281" Y="2.279924174228" Z="1" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1" />
                  <Point X="2.270834426007" Y="2.335210442101" Z="1" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1" />
                  <Point X="2.248829168857" Y="2.402224317836" Z="1" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1" />
                  <Point X="2.258535437241" Y="2.478229011024" Z="1" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1" />
                  <Point X="2.606653136917" Y="3.09817657794" Z="1" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1" />
                  <Point X="2.901373888255" Y="4.163870724856" Z="1" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1" />
                  <Point X="2.512550219494" Y="4.409408642523" Z="1" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1" />
                  <Point X="2.106336115216" Y="4.617401883626" Z="1" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1" />
                  <Point X="1.685177079362" Y="4.787194762113" Z="1" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1" />
                  <Point X="1.120436511103" Y="4.943968307894" Z="1" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1" />
                  <Point X="0.457088711277" Y="5.04869150323" Z="1" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1" />
                  <Point X="0.117067402085" Y="4.79202598836" Z="1" />
                  <Point X="0" Y="4.355124473572" Z="1" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>