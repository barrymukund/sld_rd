<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#179" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2347" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="0" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="0.004715820312" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.807951538086" Y="-4.425568847656" />
                  <Point X="0.563302001953" Y="-3.512524414063" />
                  <Point X="0.557721130371" Y="-3.497141845703" />
                  <Point X="0.542363098145" Y="-3.467377197266" />
                  <Point X="0.442368743896" Y="-3.323304199219" />
                  <Point X="0.378635437012" Y="-3.231476806641" />
                  <Point X="0.356751678467" Y="-3.209021484375" />
                  <Point X="0.330496337891" Y="-3.189777587891" />
                  <Point X="0.302495269775" Y="-3.175669189453" />
                  <Point X="0.147759658813" Y="-3.127644775391" />
                  <Point X="0.049136127472" Y="-3.097035888672" />
                  <Point X="0.020976577759" Y="-3.092766601562" />
                  <Point X="-0.008664747238" Y="-3.092766601562" />
                  <Point X="-0.036824142456" Y="-3.097035888672" />
                  <Point X="-0.191559738159" Y="-3.145060058594" />
                  <Point X="-0.290183258057" Y="-3.175669189453" />
                  <Point X="-0.318184509277" Y="-3.189777587891" />
                  <Point X="-0.344439849854" Y="-3.209021484375" />
                  <Point X="-0.366323455811" Y="-3.231476806641" />
                  <Point X="-0.466317962646" Y="-3.375549560547" />
                  <Point X="-0.530051269531" Y="-3.467377197266" />
                  <Point X="-0.538188903809" Y="-3.481573730469" />
                  <Point X="-0.55099017334" Y="-3.512524414063" />
                  <Point X="-0.706921691895" Y="-4.094469726562" />
                  <Point X="-0.916584594727" Y="-4.876941894531" />
                  <Point X="-0.966941101074" Y="-4.867167480469" />
                  <Point X="-1.079341308594" Y="-4.845350097656" />
                  <Point X="-1.24641796875" Y="-4.802362304688" />
                  <Point X="-1.244413085938" Y="-4.787133300781" />
                  <Point X="-1.214963012695" Y="-4.563438476562" />
                  <Point X="-1.214201171875" Y="-4.5479296875" />
                  <Point X="-1.216508666992" Y="-4.516224121094" />
                  <Point X="-1.253475097656" Y="-4.330382324219" />
                  <Point X="-1.277036132812" Y="-4.211932617188" />
                  <Point X="-1.287938354492" Y="-4.182966308594" />
                  <Point X="-1.304010375977" Y="-4.15512890625" />
                  <Point X="-1.32364453125" Y="-4.131204101562" />
                  <Point X="-1.46610534668" Y="-4.006269042969" />
                  <Point X="-1.556905395508" Y="-3.926639404297" />
                  <Point X="-1.583188354492" Y="-3.910295410156" />
                  <Point X="-1.612885742188" Y="-3.897994384766" />
                  <Point X="-1.64302734375" Y="-3.890966308594" />
                  <Point X="-1.832104614258" Y="-3.878573486328" />
                  <Point X="-1.952616455078" Y="-3.870674804688" />
                  <Point X="-1.98341796875" Y="-3.873708496094" />
                  <Point X="-2.014466674805" Y="-3.882028076172" />
                  <Point X="-2.042657592773" Y="-3.894801513672" />
                  <Point X="-2.200207275391" Y="-4.000072753906" />
                  <Point X="-2.300624267578" Y="-4.067169677734" />
                  <Point X="-2.312785644531" Y="-4.076821533203" />
                  <Point X="-2.335102783203" Y="-4.099461914062" />
                  <Point X="-2.422646240234" Y="-4.21355078125" />
                  <Point X="-2.480148925781" Y="-4.288489746094" />
                  <Point X="-2.636960693359" Y="-4.191395996094" />
                  <Point X="-2.801706787109" Y="-4.089389404297" />
                  <Point X="-3.045924560547" Y="-3.901349853516" />
                  <Point X="-3.104721923828" Y="-3.856077880859" />
                  <Point X="-2.878414794922" Y="-3.464102539062" />
                  <Point X="-2.423761230469" Y="-2.676619628906" />
                  <Point X="-2.412859130859" Y="-2.647653320312" />
                  <Point X="-2.406588134766" Y="-2.616126953125" />
                  <Point X="-2.405575439453" Y="-2.585192871094" />
                  <Point X="-2.414560058594" Y="-2.555575195312" />
                  <Point X="-2.428777099609" Y="-2.526746337891" />
                  <Point X="-2.4468046875" Y="-2.501589355469" />
                  <Point X="-2.464153320312" Y="-2.484240722656" />
                  <Point X="-2.489310302734" Y="-2.466213134766" />
                  <Point X="-2.518139160156" Y="-2.45199609375" />
                  <Point X="-2.547756835938" Y="-2.443011474609" />
                  <Point X="-2.578690917969" Y="-2.444024169922" />
                  <Point X="-2.610217285156" Y="-2.450295166016" />
                  <Point X="-2.63918359375" Y="-2.461197265625" />
                  <Point X="-3.141100097656" Y="-2.750979003906" />
                  <Point X="-3.818024658203" Y="-3.141801269531" />
                  <Point X="-3.952700195312" Y="-2.964865234375" />
                  <Point X="-4.082859863281" Y="-2.793861572266" />
                  <Point X="-4.257941894531" Y="-2.500275634766" />
                  <Point X="-4.306142578125" Y="-2.419450439453" />
                  <Point X="-3.901450683594" Y="-2.108919189453" />
                  <Point X="-3.105954589844" Y="-1.498513427734" />
                  <Point X="-3.084577636719" Y="-1.47559362793" />
                  <Point X="-3.066612792969" Y="-1.448462768555" />
                  <Point X="-3.053856689453" Y="-1.419833374023" />
                  <Point X="-3.046152099609" Y="-1.39008605957" />
                  <Point X="-3.04334765625" Y="-1.359657592773" />
                  <Point X="-3.045556396484" Y="-1.327986450195" />
                  <Point X="-3.052557617188" Y="-1.298240844727" />
                  <Point X="-3.068640136719" Y="-1.272256958008" />
                  <Point X="-3.089473144531" Y="-1.248300170898" />
                  <Point X="-3.112972900391" Y="-1.228766601562" />
                  <Point X="-3.139455566406" Y="-1.213180053711" />
                  <Point X="-3.168718994141" Y="-1.201956054688" />
                  <Point X="-3.200606445313" Y="-1.195474609375" />
                  <Point X="-3.231929199219" Y="-1.194383789062" />
                  <Point X="-3.865551025391" Y="-1.277801879883" />
                  <Point X="-4.732102050781" Y="-1.391885253906" />
                  <Point X="-4.783170410156" Y="-1.191955200195" />
                  <Point X="-4.834076660156" Y="-0.992658691406" />
                  <Point X="-4.880399414062" Y="-0.668774719238" />
                  <Point X="-4.892424316406" Y="-0.584698181152" />
                  <Point X="-4.439065917969" Y="-0.463221069336" />
                  <Point X="-3.532875976562" Y="-0.220408340454" />
                  <Point X="-3.517493408203" Y="-0.214827606201" />
                  <Point X="-3.487728515625" Y="-0.199469299316" />
                  <Point X="-3.470778808594" Y="-0.187705245972" />
                  <Point X="-3.459975585938" Y="-0.18020741272" />
                  <Point X="-3.437520507812" Y="-0.158323638916" />
                  <Point X="-3.418276611328" Y="-0.132068450928" />
                  <Point X="-3.404168212891" Y="-0.104067062378" />
                  <Point X="-3.394917236328" Y="-0.074260139465" />
                  <Point X="-3.390647949219" Y="-0.046100738525" />
                  <Point X="-3.390647949219" Y="-0.016459260941" />
                  <Point X="-3.394917236328" Y="0.011700286865" />
                  <Point X="-3.404168212891" Y="0.041507217407" />
                  <Point X="-3.418276611328" Y="0.069508293152" />
                  <Point X="-3.437520507812" Y="0.095763633728" />
                  <Point X="-3.459975585938" Y="0.117647254944" />
                  <Point X="-3.476925292969" Y="0.129411315918" />
                  <Point X="-3.484794189453" Y="0.134321578979" />
                  <Point X="-3.511165527344" Y="0.149039932251" />
                  <Point X="-3.532875976562" Y="0.157848342896" />
                  <Point X="-4.110452636719" Y="0.312609436035" />
                  <Point X="-4.89181640625" Y="0.521975158691" />
                  <Point X="-4.857295898438" Y="0.755261047363" />
                  <Point X="-4.824487792969" Y="0.976975097656" />
                  <Point X="-4.731242675781" Y="1.32107824707" />
                  <Point X="-4.703551269531" Y="1.423267822266" />
                  <Point X="-4.415245117188" Y="1.385311645508" />
                  <Point X="-3.765666259766" Y="1.29979284668" />
                  <Point X="-3.744986083984" Y="1.299341552734" />
                  <Point X="-3.723424560547" Y="1.301228149414" />
                  <Point X="-3.703137695312" Y="1.305263427734" />
                  <Point X="-3.665622558594" Y="1.317091918945" />
                  <Point X="-3.641711669922" Y="1.324630981445" />
                  <Point X="-3.622778564453" Y="1.332961791992" />
                  <Point X="-3.604034179688" Y="1.343783813477" />
                  <Point X="-3.587353271484" Y="1.356015014648" />
                  <Point X="-3.57371484375" Y="1.371566650391" />
                  <Point X="-3.561300292969" Y="1.389296508789" />
                  <Point X="-3.551351318359" Y="1.407431152344" />
                  <Point X="-3.536298095703" Y="1.443772460938" />
                  <Point X="-3.526703857422" Y="1.466935302734" />
                  <Point X="-3.520915527344" Y="1.486794311523" />
                  <Point X="-3.517157226562" Y="1.50810925293" />
                  <Point X="-3.5158046875" Y="1.528749511719" />
                  <Point X="-3.518951171875" Y="1.549193115234" />
                  <Point X="-3.524552978516" Y="1.570099609375" />
                  <Point X="-3.532049804688" Y="1.589377563477" />
                  <Point X="-3.550212890625" Y="1.624268798828" />
                  <Point X="-3.561789550781" Y="1.646507324219" />
                  <Point X="-3.573281738281" Y="1.663706665039" />
                  <Point X="-3.587194335938" Y="1.680286743164" />
                  <Point X="-3.602135986328" Y="1.694590209961" />
                  <Point X="-3.933435302734" Y="1.948805175781" />
                  <Point X="-4.351859863281" Y="2.269874267578" />
                  <Point X="-4.208632324219" Y="2.5152578125" />
                  <Point X="-4.081153808594" Y="2.733659423828" />
                  <Point X="-3.834151855469" Y="3.051145507812" />
                  <Point X="-3.750504882813" Y="3.158661865234" />
                  <Point X="-3.605610351562" Y="3.075006835938" />
                  <Point X="-3.206656982422" Y="2.844670898438" />
                  <Point X="-3.187724609375" Y="2.836340332031" />
                  <Point X="-3.167082519531" Y="2.829831787109" />
                  <Point X="-3.146794677734" Y="2.825796386719" />
                  <Point X="-3.094546630859" Y="2.821225341797" />
                  <Point X="-3.061245361328" Y="2.818311767578" />
                  <Point X="-3.040565185547" Y="2.818762939453" />
                  <Point X="-3.019106445312" Y="2.821588134766" />
                  <Point X="-2.999014648438" Y="2.826504638672" />
                  <Point X="-2.980463134766" Y="2.835653320313" />
                  <Point X="-2.962208740234" Y="2.847282714844" />
                  <Point X="-2.946077636719" Y="2.860229492188" />
                  <Point X="-2.908991455078" Y="2.897315429688" />
                  <Point X="-2.885354003906" Y="2.920952880859" />
                  <Point X="-2.872406982422" Y="2.937083984375" />
                  <Point X="-2.860777587891" Y="2.955338134766" />
                  <Point X="-2.85162890625" Y="2.973889892578" />
                  <Point X="-2.846712158203" Y="2.993981933594" />
                  <Point X="-2.843886962891" Y="3.015440673828" />
                  <Point X="-2.843435791016" Y="3.03612109375" />
                  <Point X="-2.848007080078" Y="3.088369384766" />
                  <Point X="-2.850920410156" Y="3.121670654297" />
                  <Point X="-2.854955810547" Y="3.141957763672" />
                  <Point X="-2.861464355469" Y="3.162600097656" />
                  <Point X="-2.869794921875" Y="3.181532958984" />
                  <Point X="-3.016603759766" Y="3.435812988281" />
                  <Point X="-3.183333007812" Y="3.724596435547" />
                  <Point X="-2.922648925781" Y="3.924460205078" />
                  <Point X="-2.700625976563" Y="4.09468359375" />
                  <Point X="-2.311607177734" Y="4.310813964844" />
                  <Point X="-2.167036865234" Y="4.391134277344" />
                  <Point X="-2.165327392578" Y="4.38890625" />
                  <Point X="-2.0431953125" Y="4.229741210938" />
                  <Point X="-2.028891967773" Y="4.214799316406" />
                  <Point X="-2.01231237793" Y="4.200887207031" />
                  <Point X="-1.99511328125" Y="4.189395019531" />
                  <Point X="-1.936961425781" Y="4.159123046875" />
                  <Point X="-1.899897216797" Y="4.139828125" />
                  <Point X="-1.880619262695" Y="4.132331542969" />
                  <Point X="-1.859712524414" Y="4.126729492187" />
                  <Point X="-1.839268798828" Y="4.123582519531" />
                  <Point X="-1.818628417969" Y="4.124935546875" />
                  <Point X="-1.797313110352" Y="4.128693847656" />
                  <Point X="-1.777453613281" Y="4.134481933594" />
                  <Point X="-1.716884277344" Y="4.159570800781" />
                  <Point X="-1.678279418945" Y="4.175561523437" />
                  <Point X="-1.660145874023" Y="4.185510253906" />
                  <Point X="-1.642416381836" Y="4.197924316406" />
                  <Point X="-1.626864257812" Y="4.2115625" />
                  <Point X="-1.614633056641" Y="4.228244140625" />
                  <Point X="-1.603811157227" Y="4.24698828125" />
                  <Point X="-1.59548034668" Y="4.265921386719" />
                  <Point X="-1.575766113281" Y="4.328446777344" />
                  <Point X="-1.563201049805" Y="4.368297851562" />
                  <Point X="-1.559165649414" Y="4.388584960938" />
                  <Point X="-1.557279174805" Y="4.410146972656" />
                  <Point X="-1.557730224609" Y="4.430826660156" />
                  <Point X="-1.574420898438" Y="4.557604003906" />
                  <Point X="-1.584201904297" Y="4.631897949219" />
                  <Point X="-1.237058227539" Y="4.729225097656" />
                  <Point X="-0.949634765625" Y="4.80980859375" />
                  <Point X="-0.478031951904" Y="4.865002441406" />
                  <Point X="-0.29471081543" Y="4.886457519531" />
                  <Point X="-0.251924804688" Y="4.726777832031" />
                  <Point X="-0.133903366089" Y="4.286315429688" />
                  <Point X="-0.121129745483" Y="4.258124023438" />
                  <Point X="-0.103271606445" Y="4.231397460938" />
                  <Point X="-0.082114532471" Y="4.20880859375" />
                  <Point X="-0.054819351196" Y="4.19421875" />
                  <Point X="-0.024381277084" Y="4.183886230469" />
                  <Point X="0.006155913353" Y="4.178844238281" />
                  <Point X="0.036693107605" Y="4.183886230469" />
                  <Point X="0.06713117981" Y="4.19421875" />
                  <Point X="0.094426452637" Y="4.20880859375" />
                  <Point X="0.115583435059" Y="4.231397460938" />
                  <Point X="0.133441726685" Y="4.258124023438" />
                  <Point X="0.146215194702" Y="4.286315429688" />
                  <Point X="0.221438415527" Y="4.567052246094" />
                  <Point X="0.307419311523" Y="4.8879375" />
                  <Point X="0.59307220459" Y="4.858021972656" />
                  <Point X="0.844041015625" Y="4.831738769531" />
                  <Point X="1.234215698242" Y="4.737538574219" />
                  <Point X="1.481026611328" Y="4.677951171875" />
                  <Point X="1.734416381836" Y="4.586044433594" />
                  <Point X="1.894645019531" Y="4.527928710938" />
                  <Point X="2.140220214844" Y="4.413081054688" />
                  <Point X="2.294574951172" Y="4.34089453125" />
                  <Point X="2.531836914062" Y="4.202665039063" />
                  <Point X="2.680978759766" Y="4.115774902344" />
                  <Point X="2.904725097656" Y="3.956658935547" />
                  <Point X="2.943260009766" Y="3.929254882812" />
                  <Point X="2.673808349609" Y="3.462550292969" />
                  <Point X="2.147581054688" Y="2.551097900391" />
                  <Point X="2.142075683594" Y="2.539930664062" />
                  <Point X="2.133076904297" Y="2.516056640625" />
                  <Point X="2.119964599609" Y="2.467022949219" />
                  <Point X="2.111607177734" Y="2.435770263672" />
                  <Point X="2.108619384766" Y="2.417934326172" />
                  <Point X="2.107728027344" Y="2.380952880859" />
                  <Point X="2.112840820312" Y="2.338552734375" />
                  <Point X="2.116099365234" Y="2.311528076172" />
                  <Point X="2.121442138672" Y="2.289604980469" />
                  <Point X="2.129708251953" Y="2.267516357422" />
                  <Point X="2.140071044922" Y="2.247471191406" />
                  <Point X="2.166306884766" Y="2.208806396484" />
                  <Point X="2.183028808594" Y="2.184162597656" />
                  <Point X="2.19446484375" Y="2.170328857422" />
                  <Point X="2.221598876953" Y="2.145592529297" />
                  <Point X="2.260263671875" Y="2.119356689453" />
                  <Point X="2.284907470703" Y="2.102635009766" />
                  <Point X="2.304952392578" Y="2.092272216797" />
                  <Point X="2.327040283203" Y="2.084006347656" />
                  <Point X="2.348963623047" Y="2.078663574219" />
                  <Point X="2.391364013672" Y="2.07355078125" />
                  <Point X="2.418388427734" Y="2.070291992188" />
                  <Point X="2.436467285156" Y="2.069845703125" />
                  <Point X="2.473206298828" Y="2.074171142578" />
                  <Point X="2.522239990234" Y="2.087283447266" />
                  <Point X="2.553492675781" Y="2.095640869141" />
                  <Point X="2.565283447266" Y="2.099638183594" />
                  <Point X="2.588533935547" Y="2.110145019531" />
                  <Point X="3.169464599609" Y="2.445545410156" />
                  <Point X="3.967326416016" Y="2.906191162109" />
                  <Point X="4.034805908203" Y="2.81241015625" />
                  <Point X="4.123271484375" Y="2.689463134766" />
                  <Point X="4.2480078125" Y="2.483335205078" />
                  <Point X="4.262198730469" Y="2.459884277344" />
                  <Point X="3.923611328125" Y="2.200076660156" />
                  <Point X="3.230783935547" Y="1.668451293945" />
                  <Point X="3.221423828125" Y="1.660240600586" />
                  <Point X="3.203973876953" Y="1.641627807617" />
                  <Point X="3.168684082031" Y="1.59558972168" />
                  <Point X="3.146191650391" Y="1.566246459961" />
                  <Point X="3.136606201172" Y="1.550912353516" />
                  <Point X="3.121629882812" Y="1.517086181641" />
                  <Point X="3.108484375" Y="1.470081176758" />
                  <Point X="3.100105957031" Y="1.440121459961" />
                  <Point X="3.09665234375" Y="1.417821899414" />
                  <Point X="3.095836425781" Y="1.394251953125" />
                  <Point X="3.097739501953" Y="1.371768066406" />
                  <Point X="3.108530517578" Y="1.319468994141" />
                  <Point X="3.115408447266" Y="1.286135253906" />
                  <Point X="3.120680175781" Y="1.268976806641" />
                  <Point X="3.136282714844" Y="1.235740356445" />
                  <Point X="3.165633056641" Y="1.19112890625" />
                  <Point X="3.184340332031" Y="1.162694824219" />
                  <Point X="3.198893798828" Y="1.145450073242" />
                  <Point X="3.216137451172" Y="1.129360595703" />
                  <Point X="3.234347167969" Y="1.116034790039" />
                  <Point X="3.276880126953" Y="1.092092407227" />
                  <Point X="3.303989257812" Y="1.076832397461" />
                  <Point X="3.320521240234" Y="1.069501586914" />
                  <Point X="3.356118164062" Y="1.059438598633" />
                  <Point X="3.413625488281" Y="1.051838256836" />
                  <Point X="3.450278808594" Y="1.046994018555" />
                  <Point X="3.462702148438" Y="1.046175048828" />
                  <Point X="3.488203613281" Y="1.04698449707" />
                  <Point X="4.040048828125" Y="1.119636352539" />
                  <Point X="4.776839355469" Y="1.21663671875" />
                  <Point X="4.807940429688" Y="1.088883056641" />
                  <Point X="4.845936035156" Y="0.932809570312" />
                  <Point X="4.885243652344" Y="0.68034173584" />
                  <Point X="4.890864746094" Y="0.644238586426" />
                  <Point X="4.510575195312" Y="0.542340270996" />
                  <Point X="3.716579833984" Y="0.329589904785" />
                  <Point X="3.704791259766" Y="0.325586608887" />
                  <Point X="3.681545898438" Y="0.315068115234" />
                  <Point X="3.625046630859" Y="0.282410583496" />
                  <Point X="3.589035888672" Y="0.261595550537" />
                  <Point X="3.574311035156" Y="0.251096221924" />
                  <Point X="3.547531005859" Y="0.225576599121" />
                  <Point X="3.513631591797" Y="0.182380706787" />
                  <Point X="3.492025146484" Y="0.154848953247" />
                  <Point X="3.48030078125" Y="0.135568847656" />
                  <Point X="3.470527099609" Y="0.114105377197" />
                  <Point X="3.463680908203" Y="0.092604263306" />
                  <Point X="3.452381103516" Y="0.033600757599" />
                  <Point X="3.445178710938" Y="-0.00400637722" />
                  <Point X="3.443483154297" Y="-0.021873929977" />
                  <Point X="3.445178710938" Y="-0.058553775787" />
                  <Point X="3.456478759766" Y="-0.117557434082" />
                  <Point X="3.463680908203" Y="-0.155164413452" />
                  <Point X="3.470527099609" Y="-0.176665527344" />
                  <Point X="3.480301025391" Y="-0.198129302979" />
                  <Point X="3.492024902344" Y="-0.217408966064" />
                  <Point X="3.525924316406" Y="-0.260604858398" />
                  <Point X="3.547530761719" Y="-0.28813659668" />
                  <Point X="3.559999023438" Y="-0.301236114502" />
                  <Point X="3.589035888672" Y="-0.324155548096" />
                  <Point X="3.64553515625" Y="-0.356813110352" />
                  <Point X="3.681545898438" Y="-0.377628143311" />
                  <Point X="3.692710205078" Y="-0.383139038086" />
                  <Point X="3.716579833984" Y="-0.392150054932" />
                  <Point X="4.222646972656" Y="-0.527750183105" />
                  <Point X="4.891472167969" Y="-0.706961486816" />
                  <Point X="4.876237792969" Y="-0.808010375977" />
                  <Point X="4.855022460938" Y="-0.948725524902" />
                  <Point X="4.804662597656" Y="-1.16941003418" />
                  <Point X="4.801173828125" Y="-1.184698974609" />
                  <Point X="4.345193359375" Y="-1.12466809082" />
                  <Point X="3.424382080078" Y="-1.003440979004" />
                  <Point X="3.40803515625" Y="-1.002710266113" />
                  <Point X="3.374658447266" Y="-1.005508850098" />
                  <Point X="3.263770507812" Y="-1.029610717773" />
                  <Point X="3.193094238281" Y="-1.04497253418" />
                  <Point X="3.163973632812" Y="-1.056597167969" />
                  <Point X="3.136147216797" Y="-1.073489624023" />
                  <Point X="3.112397460938" Y="-1.093960083008" />
                  <Point X="3.045372558594" Y="-1.174569824219" />
                  <Point X="3.002653320312" Y="-1.225948120117" />
                  <Point X="2.987932617188" Y="-1.250330444336" />
                  <Point X="2.976589355469" Y="-1.277715820312" />
                  <Point X="2.969757568359" Y="-1.305365478516" />
                  <Point X="2.960151367188" Y="-1.409758789062" />
                  <Point X="2.954028564453" Y="-1.476295654297" />
                  <Point X="2.956347167969" Y="-1.507564086914" />
                  <Point X="2.964078613281" Y="-1.539185058594" />
                  <Point X="2.976450195312" Y="-1.567996582031" />
                  <Point X="3.037817138672" Y="-1.663448852539" />
                  <Point X="3.076930419922" Y="-1.724287109375" />
                  <Point X="3.086932861328" Y="-1.737239013672" />
                  <Point X="3.110628417969" Y="-1.760909301758" />
                  <Point X="3.580261962891" Y="-2.121271728516" />
                  <Point X="4.213122558594" Y="-2.6068828125" />
                  <Point X="4.184613769531" Y="-2.653014160156" />
                  <Point X="4.124808105469" Y="-2.749789306641" />
                  <Point X="4.028981445312" Y="-2.885945068359" />
                  <Point X="3.621061279297" Y="-2.650432128906" />
                  <Point X="2.800954589844" Y="-2.176943115234" />
                  <Point X="2.786131591797" Y="-2.170012451172" />
                  <Point X="2.754224609375" Y="-2.159825195312" />
                  <Point X="2.622250244141" Y="-2.135990722656" />
                  <Point X="2.538134277344" Y="-2.120799560547" />
                  <Point X="2.506781982422" Y="-2.120395263672" />
                  <Point X="2.474609130859" Y="-2.125353027344" />
                  <Point X="2.444833251953" Y="-2.135176757812" />
                  <Point X="2.335195068359" Y="-2.192878417969" />
                  <Point X="2.265315185547" Y="-2.229655761719" />
                  <Point X="2.242384033203" Y="-2.246549072266" />
                  <Point X="2.221424316406" Y="-2.267509033203" />
                  <Point X="2.204531982422" Y="-2.290439453125" />
                  <Point X="2.146830078125" Y="-2.400077636719" />
                  <Point X="2.110052978516" Y="-2.469957519531" />
                  <Point X="2.100229003906" Y="-2.499733398438" />
                  <Point X="2.095271240234" Y="-2.531906005859" />
                  <Point X="2.095675537109" Y="-2.563258300781" />
                  <Point X="2.119510009766" Y="-2.695232421875" />
                  <Point X="2.134701171875" Y="-2.779348632812" />
                  <Point X="2.138985595703" Y="-2.795141845703" />
                  <Point X="2.151819091797" Y="-2.826078613281" />
                  <Point X="2.453605957031" Y="-3.348788330078" />
                  <Point X="2.861283447266" Y="-4.054906738281" />
                  <Point X="2.852814697266" Y="-4.060955810547" />
                  <Point X="2.781854003906" Y="-4.111641113281" />
                  <Point X="2.701765136719" Y="-4.163481445312" />
                  <Point X="2.38436328125" Y="-3.749835449219" />
                  <Point X="1.758546020508" Y="-2.934255126953" />
                  <Point X="1.747503662109" Y="-2.922179443359" />
                  <Point X="1.721924194336" Y="-2.900557373047" />
                  <Point X="1.591761962891" Y="-2.816875" />
                  <Point X="1.50880078125" Y="-2.763538574219" />
                  <Point X="1.479989624023" Y="-2.751167236328" />
                  <Point X="1.448368408203" Y="-2.743435546875" />
                  <Point X="1.417099487305" Y="-2.741116699219" />
                  <Point X="1.274744995117" Y="-2.754216308594" />
                  <Point X="1.184012573242" Y="-2.762565673828" />
                  <Point X="1.156362670898" Y="-2.769397460938" />
                  <Point X="1.128977783203" Y="-2.780740722656" />
                  <Point X="1.104595947266" Y="-2.795461181641" />
                  <Point X="0.994673156738" Y="-2.886858154297" />
                  <Point X="0.924612182617" Y="-2.945111572266" />
                  <Point X="0.904141235352" Y="-2.968861572266" />
                  <Point X="0.887248962402" Y="-2.996687988281" />
                  <Point X="0.875624267578" Y="-3.02580859375" />
                  <Point X="0.842757995605" Y="-3.17701953125" />
                  <Point X="0.821809997559" Y="-3.273396484375" />
                  <Point X="0.81972454834" Y="-3.289627197266" />
                  <Point X="0.81974230957" Y="-3.323120117188" />
                  <Point X="0.905266174316" Y="-3.972739501953" />
                  <Point X="1.022065307617" Y="-4.859915039062" />
                  <Point X="0.97567779541" Y="-4.870083496094" />
                  <Point X="0.929315551758" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058434814453" Y="-4.752635253906" />
                  <Point X="-1.141246337891" Y="-4.731328125" />
                  <Point X="-1.120775634766" Y="-4.575838378906" />
                  <Point X="-1.120077514648" Y="-4.568099609375" />
                  <Point X="-1.119451782227" Y="-4.541033691406" />
                  <Point X="-1.121759277344" Y="-4.509328125" />
                  <Point X="-1.123334106445" Y="-4.497690429688" />
                  <Point X="-1.160300415039" Y="-4.311848632813" />
                  <Point X="-1.183861450195" Y="-4.193398925781" />
                  <Point X="-1.188125" Y="-4.17846875" />
                  <Point X="-1.19902734375" Y="-4.149502441406" />
                  <Point X="-1.205666015625" Y="-4.135466308594" />
                  <Point X="-1.221738037109" Y="-4.10762890625" />
                  <Point X="-1.230573730469" Y="-4.094862304688" />
                  <Point X="-1.250207763672" Y="-4.0709375" />
                  <Point X="-1.261006591797" Y="-4.059779296875" />
                  <Point X="-1.403467407227" Y="-3.934844238281" />
                  <Point X="-1.494267456055" Y="-3.855214599609" />
                  <Point X="-1.506738525391" Y="-3.845965576172" />
                  <Point X="-1.533021484375" Y="-3.829621582031" />
                  <Point X="-1.546833618164" Y="-3.822526855469" />
                  <Point X="-1.576530883789" Y="-3.810225830078" />
                  <Point X="-1.591313476562" Y="-3.805476074219" />
                  <Point X="-1.621454833984" Y="-3.798447998047" />
                  <Point X="-1.636813964844" Y="-3.796169677734" />
                  <Point X="-1.825891235352" Y="-3.783776855469" />
                  <Point X="-1.946403076172" Y="-3.775878173828" />
                  <Point X="-1.961928100586" Y="-3.776132324219" />
                  <Point X="-1.992729614258" Y="-3.779166015625" />
                  <Point X="-2.008006103516" Y="-3.781945556641" />
                  <Point X="-2.03905480957" Y="-3.790265136719" />
                  <Point X="-2.053674560547" Y="-3.795496337891" />
                  <Point X="-2.081865478516" Y="-3.808269775391" />
                  <Point X="-2.095436767578" Y="-3.815811767578" />
                  <Point X="-2.252986328125" Y="-3.921083007813" />
                  <Point X="-2.353403320312" Y="-3.988179931641" />
                  <Point X="-2.359681640625" Y="-3.992757080078" />
                  <Point X="-2.380442138672" Y="-4.010131103516" />
                  <Point X="-2.402759277344" Y="-4.032771484375" />
                  <Point X="-2.410471435547" Y="-4.041629638672" />
                  <Point X="-2.498014892578" Y="-4.155718261719" />
                  <Point X="-2.503202880859" Y="-4.162479492188" />
                  <Point X="-2.586949707031" Y="-4.110625488281" />
                  <Point X="-2.747581787109" Y="-4.011166259766" />
                  <Point X="-2.980862792969" Y="-3.831547607422" />
                  <Point X="-2.796142333984" Y="-3.511602539062" />
                  <Point X="-2.341488769531" Y="-2.724119628906" />
                  <Point X="-2.334850097656" Y="-2.710083251953" />
                  <Point X="-2.323947998047" Y="-2.681116943359" />
                  <Point X="-2.319684570312" Y="-2.666187011719" />
                  <Point X="-2.313413574219" Y="-2.634660644531" />
                  <Point X="-2.311638916016" Y="-2.619235351562" />
                  <Point X="-2.310626220703" Y="-2.588301269531" />
                  <Point X="-2.314666259766" Y="-2.557615234375" />
                  <Point X="-2.323650878906" Y="-2.527997558594" />
                  <Point X="-2.329357421875" Y="-2.513557128906" />
                  <Point X="-2.343574462891" Y="-2.484728271484" />
                  <Point X="-2.351557128906" Y="-2.47141015625" />
                  <Point X="-2.369584716797" Y="-2.446253173828" />
                  <Point X="-2.379629638672" Y="-2.434414306641" />
                  <Point X="-2.396978271484" Y="-2.417065673828" />
                  <Point X="-2.408817138672" Y="-2.407020751953" />
                  <Point X="-2.433974121094" Y="-2.388993164062" />
                  <Point X="-2.447292236328" Y="-2.381010498047" />
                  <Point X="-2.47612109375" Y="-2.366793457031" />
                  <Point X="-2.490561523438" Y="-2.361086914062" />
                  <Point X="-2.520179199219" Y="-2.352102294922" />
                  <Point X="-2.550865234375" Y="-2.348062255859" />
                  <Point X="-2.581799316406" Y="-2.349074951172" />
                  <Point X="-2.597224609375" Y="-2.350849609375" />
                  <Point X="-2.628750976562" Y="-2.357120605469" />
                  <Point X="-2.643680908203" Y="-2.361384033203" />
                  <Point X="-2.672647216797" Y="-2.372286132812" />
                  <Point X="-2.68668359375" Y="-2.378924804688" />
                  <Point X="-3.188600097656" Y="-2.668706542969" />
                  <Point X="-3.79308984375" Y="-3.017708496094" />
                  <Point X="-3.877106933594" Y="-2.907327148438" />
                  <Point X="-4.004014160156" Y="-2.740596435547" />
                  <Point X="-4.176349121094" Y="-2.4516171875" />
                  <Point X="-4.181265136719" Y="-2.443373535156" />
                  <Point X="-3.843618408203" Y="-2.184287841797" />
                  <Point X="-3.048122314453" Y="-1.573881958008" />
                  <Point X="-3.036481933594" Y="-1.563309570312" />
                  <Point X="-3.015104980469" Y="-1.540389770508" />
                  <Point X="-3.005368408203" Y="-1.528042480469" />
                  <Point X="-2.987403564453" Y="-1.500911621094" />
                  <Point X="-2.979836669922" Y="-1.487126708984" />
                  <Point X="-2.967080566406" Y="-1.458497314453" />
                  <Point X="-2.961891113281" Y="-1.443652587891" />
                  <Point X="-2.954186523438" Y="-1.413905273438" />
                  <Point X="-2.951552978516" Y="-1.3988046875" />
                  <Point X="-2.948748535156" Y="-1.368376220703" />
                  <Point X="-2.948577880859" Y="-1.353048339844" />
                  <Point X="-2.950786621094" Y="-1.321377319336" />
                  <Point X="-2.953083251953" Y="-1.306221191406" />
                  <Point X="-2.960084472656" Y="-1.276475463867" />
                  <Point X="-2.971778564453" Y="-1.248243408203" />
                  <Point X="-2.987861083984" Y="-1.222259399414" />
                  <Point X="-2.996954101562" Y="-1.209918212891" />
                  <Point X="-3.017787109375" Y="-1.185961425781" />
                  <Point X="-3.028746582031" Y="-1.175243286133" />
                  <Point X="-3.052246337891" Y="-1.155709838867" />
                  <Point X="-3.064786376953" Y="-1.14689440918" />
                  <Point X="-3.091269042969" Y="-1.131307861328" />
                  <Point X="-3.105434814453" Y="-1.12448059082" />
                  <Point X="-3.134698242188" Y="-1.113256591797" />
                  <Point X="-3.149796142578" Y="-1.108859741211" />
                  <Point X="-3.18168359375" Y="-1.102378295898" />
                  <Point X="-3.197300048828" Y="-1.100532104492" />
                  <Point X="-3.228622802734" Y="-1.09944140625" />
                  <Point X="-3.244329101562" Y="-1.100196533203" />
                  <Point X="-3.877950927734" Y="-1.183614624023" />
                  <Point X="-4.660920898438" Y="-1.286694458008" />
                  <Point X="-4.691125488281" Y="-1.168444091797" />
                  <Point X="-4.740761230469" Y="-0.974120605469" />
                  <Point X="-4.786356445312" Y="-0.655324462891" />
                  <Point X="-4.786452148438" Y="-0.654654296875" />
                  <Point X="-4.414478027344" Y="-0.554983947754" />
                  <Point X="-3.508288085938" Y="-0.312171295166" />
                  <Point X="-3.5004765625" Y="-0.309712768555" />
                  <Point X="-3.473931884766" Y="-0.299251495361" />
                  <Point X="-3.444166992188" Y="-0.283893096924" />
                  <Point X="-3.433561279297" Y="-0.27751361084" />
                  <Point X="-3.416611572266" Y="-0.265749511719" />
                  <Point X="-3.393671386719" Y="-0.248242477417" />
                  <Point X="-3.371216308594" Y="-0.226358642578" />
                  <Point X="-3.360898193359" Y="-0.214484420776" />
                  <Point X="-3.341654296875" Y="-0.188229141235" />
                  <Point X="-3.333437011719" Y="-0.174814590454" />
                  <Point X="-3.319328613281" Y="-0.146813140869" />
                  <Point X="-3.3134375" Y="-0.132226547241" />
                  <Point X="-3.304186523438" Y="-0.102419631958" />
                  <Point X="-3.300990722656" Y="-0.08850050354" />
                  <Point X="-3.296721435547" Y="-0.060341072083" />
                  <Point X="-3.295647949219" Y="-0.046100769043" />
                  <Point X="-3.295647949219" Y="-0.016459270477" />
                  <Point X="-3.296721191406" Y="-0.002218966246" />
                  <Point X="-3.300990478516" Y="0.0259406147" />
                  <Point X="-3.304186523438" Y="0.039859745026" />
                  <Point X="-3.3134375" Y="0.069666664124" />
                  <Point X="-3.319328857422" Y="0.084253700256" />
                  <Point X="-3.333437255859" Y="0.112254852295" />
                  <Point X="-3.341654296875" Y="0.125668807983" />
                  <Point X="-3.360898193359" Y="0.151924240112" />
                  <Point X="-3.371216552734" Y="0.163798904419" />
                  <Point X="-3.393671630859" Y="0.18568258667" />
                  <Point X="-3.405808349609" Y="0.195691604614" />
                  <Point X="-3.422758056641" Y="0.207455688477" />
                  <Point X="-3.438495605469" Y="0.217276107788" />
                  <Point X="-3.464866943359" Y="0.231994384766" />
                  <Point X="-3.475449707031" Y="0.237070449829" />
                  <Point X="-3.49716015625" Y="0.245878875732" />
                  <Point X="-3.508288085938" Y="0.249611251831" />
                  <Point X="-4.085864746094" Y="0.404372406006" />
                  <Point X="-4.785446289062" Y="0.591824584961" />
                  <Point X="-4.763319335938" Y="0.741354797363" />
                  <Point X="-4.731330566406" Y="0.957532409668" />
                  <Point X="-4.639549804688" Y="1.296231201172" />
                  <Point X="-4.633586425781" Y="1.318237182617" />
                  <Point X="-4.427645019531" Y="1.291124389648" />
                  <Point X="-3.778066162109" Y="1.20560559082" />
                  <Point X="-3.767739013672" Y="1.204815429688" />
                  <Point X="-3.747058837891" Y="1.204364135742" />
                  <Point X="-3.736705322266" Y="1.204703125" />
                  <Point X="-3.715143798828" Y="1.20658984375" />
                  <Point X="-3.704891113281" Y="1.208053466797" />
                  <Point X="-3.684604248047" Y="1.212088745117" />
                  <Point X="-3.674570556641" Y="1.21466027832" />
                  <Point X="-3.637055419922" Y="1.226488769531" />
                  <Point X="-3.61314453125" Y="1.234027832031" />
                  <Point X="-3.603450683594" Y="1.237676513672" />
                  <Point X="-3.584517578125" Y="1.246007324219" />
                  <Point X="-3.575278808594" Y="1.250689331055" />
                  <Point X="-3.556534423828" Y="1.261511352539" />
                  <Point X="-3.547858886719" Y="1.26717199707" />
                  <Point X="-3.531177978516" Y="1.279403320312" />
                  <Point X="-3.515928466797" Y="1.293377197266" />
                  <Point X="-3.502290039063" Y="1.308928833008" />
                  <Point X="-3.495895263672" Y="1.317077148438" />
                  <Point X="-3.483480712891" Y="1.334806884766" />
                  <Point X="-3.478011230469" Y="1.343602661133" />
                  <Point X="-3.468062255859" Y="1.361737182617" />
                  <Point X="-3.463583007813" Y="1.371075927734" />
                  <Point X="-3.448529785156" Y="1.407417236328" />
                  <Point X="-3.438935546875" Y="1.430580078125" />
                  <Point X="-3.435499023438" Y="1.4403515625" />
                  <Point X="-3.429710693359" Y="1.46021081543" />
                  <Point X="-3.427358642578" Y="1.470298095703" />
                  <Point X="-3.423600341797" Y="1.491613037109" />
                  <Point X="-3.422360595703" Y="1.501897460938" />
                  <Point X="-3.421008056641" Y="1.522537597656" />
                  <Point X="-3.421910400391" Y="1.543200805664" />
                  <Point X="-3.425056884766" Y="1.56364440918" />
                  <Point X="-3.427188232422" Y="1.573780639648" />
                  <Point X="-3.432790039062" Y="1.594687133789" />
                  <Point X="-3.436012207031" Y="1.60453137207" />
                  <Point X="-3.443509033203" Y="1.623809326172" />
                  <Point X="-3.447783691406" Y="1.633243286133" />
                  <Point X="-3.465946777344" Y="1.668134643555" />
                  <Point X="-3.4775234375" Y="1.690373291016" />
                  <Point X="-3.482799804688" Y="1.699286254883" />
                  <Point X="-3.494291992188" Y="1.716485717773" />
                  <Point X="-3.500508056641" Y="1.724772216797" />
                  <Point X="-3.514420654297" Y="1.741352172852" />
                  <Point X="-3.521500732422" Y="1.748911499023" />
                  <Point X="-3.536442382812" Y="1.76321496582" />
                  <Point X="-3.544303710938" Y="1.769958740234" />
                  <Point X="-3.875603027344" Y="2.024173706055" />
                  <Point X="-4.227614257812" Y="2.294282226563" />
                  <Point X="-4.1265859375" Y="2.467368408203" />
                  <Point X="-4.002293212891" Y="2.680312011719" />
                  <Point X="-3.759171142578" Y="2.992811035156" />
                  <Point X="-3.726338623047" Y="3.035012695312" />
                  <Point X="-3.653110351562" Y="2.992734375" />
                  <Point X="-3.254156982422" Y="2.7623984375" />
                  <Point X="-3.244918457031" Y="2.757716552734" />
                  <Point X="-3.225986083984" Y="2.749385986328" />
                  <Point X="-3.216292236328" Y="2.745737304688" />
                  <Point X="-3.195650146484" Y="2.739228759766" />
                  <Point X="-3.185615722656" Y="2.736656982422" />
                  <Point X="-3.165327880859" Y="2.732621582031" />
                  <Point X="-3.155074462891" Y="2.731157958984" />
                  <Point X="-3.102826416016" Y="2.726586914062" />
                  <Point X="-3.069525146484" Y="2.723673339844" />
                  <Point X="-3.059173339844" Y="2.723334472656" />
                  <Point X="-3.038493164062" Y="2.723785644531" />
                  <Point X="-3.028164794922" Y="2.724575683594" />
                  <Point X="-3.006706054688" Y="2.727400878906" />
                  <Point X="-2.996525878906" Y="2.729310791016" />
                  <Point X="-2.976434082031" Y="2.734227294922" />
                  <Point X="-2.956996826172" Y="2.741302001953" />
                  <Point X="-2.9384453125" Y="2.750450683594" />
                  <Point X="-2.929419433594" Y="2.75553125" />
                  <Point X="-2.911165039062" Y="2.767160644531" />
                  <Point X="-2.902745361328" Y="2.773194091797" />
                  <Point X="-2.886614257812" Y="2.786140869141" />
                  <Point X="-2.878902832031" Y="2.793054199219" />
                  <Point X="-2.841816650391" Y="2.830140136719" />
                  <Point X="-2.818179199219" Y="2.853777587891" />
                  <Point X="-2.811265869141" Y="2.861489013672" />
                  <Point X="-2.798318847656" Y="2.877620117188" />
                  <Point X="-2.79228515625" Y="2.886039794922" />
                  <Point X="-2.780655761719" Y="2.904293945312" />
                  <Point X="-2.775574707031" Y="2.913320800781" />
                  <Point X="-2.766426025391" Y="2.931872558594" />
                  <Point X="-2.7593515625" Y="2.95130859375" />
                  <Point X="-2.754434814453" Y="2.971400634766" />
                  <Point X="-2.752524902344" Y="2.981581542969" />
                  <Point X="-2.749699707031" Y="3.003040283203" />
                  <Point X="-2.748909667969" Y="3.013368652344" />
                  <Point X="-2.748458496094" Y="3.034049072266" />
                  <Point X="-2.748797363281" Y="3.044401123047" />
                  <Point X="-2.753368652344" Y="3.096649414062" />
                  <Point X="-2.756281982422" Y="3.129950683594" />
                  <Point X="-2.757745849609" Y="3.140204345703" />
                  <Point X="-2.76178125" Y="3.160491455078" />
                  <Point X="-2.764352783203" Y="3.170524902344" />
                  <Point X="-2.770861328125" Y="3.191167236328" />
                  <Point X="-2.774509521484" Y="3.200860595703" />
                  <Point X="-2.782840087891" Y="3.219793457031" />
                  <Point X="-2.787522460938" Y="3.229032958984" />
                  <Point X="-2.934331298828" Y="3.483312988281" />
                  <Point X="-3.059387207031" Y="3.699916259766" />
                  <Point X="-2.864846923828" Y="3.849068359375" />
                  <Point X="-2.648374267578" Y="4.015036376953" />
                  <Point X="-2.265469726562" Y="4.227770019531" />
                  <Point X="-2.192525146484" Y="4.268296386719" />
                  <Point X="-2.11856640625" Y="4.171912109375" />
                  <Point X="-2.111820800781" Y="4.164048339844" />
                  <Point X="-2.097517578125" Y="4.149106445313" />
                  <Point X="-2.089957275391" Y="4.142025390625" />
                  <Point X="-2.073377685547" Y="4.12811328125" />
                  <Point X="-2.065092041016" Y="4.121897949219" />
                  <Point X="-2.047892822266" Y="4.110405761719" />
                  <Point X="-2.038979370117" Y="4.10512890625" />
                  <Point X="-1.980827636719" Y="4.074856933594" />
                  <Point X="-1.943763427734" Y="4.055562011719" />
                  <Point X="-1.93432800293" Y="4.051287109375" />
                  <Point X="-1.915050048828" Y="4.043790527344" />
                  <Point X="-1.905207519531" Y="4.040568603516" />
                  <Point X="-1.88430078125" Y="4.034966552734" />
                  <Point X="-1.874166015625" Y="4.032835449219" />
                  <Point X="-1.853722290039" Y="4.029688476562" />
                  <Point X="-1.8330546875" Y="4.028785888672" />
                  <Point X="-1.812414306641" Y="4.030138916016" />
                  <Point X="-1.802132568359" Y="4.031378662109" />
                  <Point X="-1.780817260742" Y="4.035136962891" />
                  <Point X="-1.770731079102" Y="4.037488525391" />
                  <Point X="-1.750871704102" Y="4.043276611328" />
                  <Point X="-1.741098388672" Y="4.046713378906" />
                  <Point X="-1.680529052734" Y="4.071802246094" />
                  <Point X="-1.641924072266" Y="4.08779296875" />
                  <Point X="-1.632584350586" Y="4.092273193359" />
                  <Point X="-1.614450683594" Y="4.102221679688" />
                  <Point X="-1.605656860352" Y="4.107690429687" />
                  <Point X="-1.587927368164" Y="4.120104492188" />
                  <Point X="-1.579780273438" Y="4.126498046875" />
                  <Point X="-1.564228027344" Y="4.140136230469" />
                  <Point X="-1.550251342773" Y="4.155389160156" />
                  <Point X="-1.538020019531" Y="4.172070800781" />
                  <Point X="-1.532360717773" Y="4.180744140625" />
                  <Point X="-1.521538696289" Y="4.19948828125" />
                  <Point X="-1.516856567383" Y="4.208727050781" />
                  <Point X="-1.508525878906" Y="4.22766015625" />
                  <Point X="-1.504877319336" Y="4.237354492188" />
                  <Point X="-1.485163085938" Y="4.299879882812" />
                  <Point X="-1.472598022461" Y="4.339730957031" />
                  <Point X="-1.470026489258" Y="4.349764160156" />
                  <Point X="-1.465990966797" Y="4.370051269531" />
                  <Point X="-1.46452722168" Y="4.380305175781" />
                  <Point X="-1.46264074707" Y="4.4018671875" />
                  <Point X="-1.462301757812" Y="4.41221875" />
                  <Point X="-1.462752807617" Y="4.4328984375" />
                  <Point X="-1.46354284668" Y="4.4432265625" />
                  <Point X="-1.479266235352" Y="4.562655273438" />
                  <Point X="-1.211412353516" Y="4.637752441406" />
                  <Point X="-0.931178161621" Y="4.7163203125" />
                  <Point X="-0.466989044189" Y="4.770646484375" />
                  <Point X="-0.365222015381" Y="4.782556640625" />
                  <Point X="-0.343687774658" Y="4.702189941406" />
                  <Point X="-0.22566633606" Y="4.261727539062" />
                  <Point X="-0.22043510437" Y="4.247107421875" />
                  <Point X="-0.207661560059" Y="4.218916015625" />
                  <Point X="-0.200119384766" Y="4.205344726562" />
                  <Point X="-0.182261154175" Y="4.178618164062" />
                  <Point X="-0.17260824585" Y="4.166455566406" />
                  <Point X="-0.151451187134" Y="4.143866699219" />
                  <Point X="-0.126897789001" Y="4.125026367188" />
                  <Point X="-0.099602600098" Y="4.110436523438" />
                  <Point X="-0.085356651306" Y="4.104260742188" />
                  <Point X="-0.054918533325" Y="4.093927978516" />
                  <Point X="-0.039857234955" Y="4.090155273438" />
                  <Point X="-0.009319986343" Y="4.08511328125" />
                  <Point X="0.021631772995" Y="4.08511328125" />
                  <Point X="0.052169025421" Y="4.090155273438" />
                  <Point X="0.067230316162" Y="4.093927978516" />
                  <Point X="0.097668441772" Y="4.104260742188" />
                  <Point X="0.111914390564" Y="4.110436523438" />
                  <Point X="0.139209732056" Y="4.125026367188" />
                  <Point X="0.163763275146" Y="4.1438671875" />
                  <Point X="0.184920181274" Y="4.166456054688" />
                  <Point X="0.194572799683" Y="4.178618164062" />
                  <Point X="0.212431182861" Y="4.205344726562" />
                  <Point X="0.219973648071" Y="4.218916503906" />
                  <Point X="0.232747192383" Y="4.247107910156" />
                  <Point X="0.237978118896" Y="4.261727539062" />
                  <Point X="0.31320135498" Y="4.542464355469" />
                  <Point X="0.378190216064" Y="4.785006347656" />
                  <Point X="0.583177246094" Y="4.763538574219" />
                  <Point X="0.827876464844" Y="4.737912109375" />
                  <Point X="1.211920288086" Y="4.645191894531" />
                  <Point X="1.453596923828" Y="4.586844238281" />
                  <Point X="1.702023925781" Y="4.496737304688" />
                  <Point X="1.858257080078" Y="4.440070800781" />
                  <Point X="2.099975341797" Y="4.327026855469" />
                  <Point X="2.250452392578" Y="4.256653808594" />
                  <Point X="2.484013916016" Y="4.120580078125" />
                  <Point X="2.629432373047" Y="4.035859375" />
                  <Point X="2.817780029297" Y="3.901916992188" />
                  <Point X="2.591535888672" Y="3.510050292969" />
                  <Point X="2.06530859375" Y="2.598597900391" />
                  <Point X="2.062373046875" Y="2.593104980469" />
                  <Point X="2.053180908203" Y="2.5734375" />
                  <Point X="2.044182128906" Y="2.549563476562" />
                  <Point X="2.041301635742" Y="2.540598632812" />
                  <Point X="2.028189208984" Y="2.491564941406" />
                  <Point X="2.01983190918" Y="2.460312255859" />
                  <Point X="2.017912719727" Y="2.451465576172" />
                  <Point X="2.013646972656" Y="2.420223388672" />
                  <Point X="2.012755615234" Y="2.383241943359" />
                  <Point X="2.013411254883" Y="2.369579833984" />
                  <Point X="2.018524047852" Y="2.3271796875" />
                  <Point X="2.021782592773" Y="2.300155029297" />
                  <Point X="2.02380078125" Y="2.289034423828" />
                  <Point X="2.029143554687" Y="2.267111328125" />
                  <Point X="2.032468261719" Y="2.256308837891" />
                  <Point X="2.040734375" Y="2.234220214844" />
                  <Point X="2.045318359375" Y="2.223889160156" />
                  <Point X="2.055681152344" Y="2.203843994141" />
                  <Point X="2.061459960938" Y="2.194129882813" />
                  <Point X="2.087695800781" Y="2.155465087891" />
                  <Point X="2.104417724609" Y="2.130821289063" />
                  <Point X="2.10980859375" Y="2.123633300781" />
                  <Point X="2.130463134766" Y="2.100123535156" />
                  <Point X="2.157597167969" Y="2.075387207031" />
                  <Point X="2.168257568359" Y="2.066981445312" />
                  <Point X="2.206922363281" Y="2.040745605469" />
                  <Point X="2.231566162109" Y="2.024023925781" />
                  <Point X="2.241279785156" Y="2.018245117188" />
                  <Point X="2.261324707031" Y="2.007882446289" />
                  <Point X="2.271656005859" Y="2.003298339844" />
                  <Point X="2.293743896484" Y="1.995032470703" />
                  <Point X="2.304546875" Y="1.991707641602" />
                  <Point X="2.326470214844" Y="1.986364868164" />
                  <Point X="2.337590576172" Y="1.984346801758" />
                  <Point X="2.379990966797" Y="1.979234008789" />
                  <Point X="2.407015380859" Y="1.975975219727" />
                  <Point X="2.416043945312" Y="1.975320922852" />
                  <Point X="2.447575195312" Y="1.975497314453" />
                  <Point X="2.484314208984" Y="1.979822753906" />
                  <Point X="2.497748291016" Y="1.982395874023" />
                  <Point X="2.546781982422" Y="1.995508300781" />
                  <Point X="2.578034667969" Y="2.003865600586" />
                  <Point X="2.583994384766" Y="2.005670654297" />
                  <Point X="2.604404541016" Y="2.013067260742" />
                  <Point X="2.627655029297" Y="2.02357409668" />
                  <Point X="2.636033935547" Y="2.027872558594" />
                  <Point X="3.216964599609" Y="2.363272949219" />
                  <Point X="3.940404785156" Y="2.780951416016" />
                  <Point X="3.957693359375" Y="2.756924316406" />
                  <Point X="4.043952880859" Y="2.63704296875" />
                  <Point X="4.136884765625" Y="2.483472412109" />
                  <Point X="3.865779052734" Y="2.275445068359" />
                  <Point X="3.172951660156" Y="1.743819824219" />
                  <Point X="3.168136962891" Y="1.739868286133" />
                  <Point X="3.152118652344" Y="1.725215942383" />
                  <Point X="3.134668701172" Y="1.706603149414" />
                  <Point X="3.128576416016" Y="1.699422485352" />
                  <Point X="3.093286621094" Y="1.653384399414" />
                  <Point X="3.070794189453" Y="1.624041259766" />
                  <Point X="3.065635742188" Y="1.616602539062" />
                  <Point X="3.049739257812" Y="1.589372070312" />
                  <Point X="3.034762939453" Y="1.555546020508" />
                  <Point X="3.030140380859" Y="1.542672363281" />
                  <Point X="3.016994873047" Y="1.495667480469" />
                  <Point X="3.008616455078" Y="1.465707641602" />
                  <Point X="3.006225097656" Y="1.454661132812" />
                  <Point X="3.002771484375" Y="1.432361450195" />
                  <Point X="3.001709228516" Y="1.421108520508" />
                  <Point X="3.000893310547" Y="1.397538574219" />
                  <Point X="3.001174804688" Y="1.386239624023" />
                  <Point X="3.003077880859" Y="1.363755737305" />
                  <Point X="3.004699462891" Y="1.352570800781" />
                  <Point X="3.015490478516" Y="1.300271728516" />
                  <Point X="3.022368408203" Y="1.266937988281" />
                  <Point X="3.024597900391" Y="1.258234741211" />
                  <Point X="3.034684326172" Y="1.228606933594" />
                  <Point X="3.050286865234" Y="1.195370605469" />
                  <Point X="3.056918701172" Y="1.183525878906" />
                  <Point X="3.086269042969" Y="1.138914306641" />
                  <Point X="3.104976318359" Y="1.11048046875" />
                  <Point X="3.111739257813" Y="1.101424194336" />
                  <Point X="3.126292724609" Y="1.08417956543" />
                  <Point X="3.134083496094" Y="1.075990600586" />
                  <Point X="3.151327148438" Y="1.059901123047" />
                  <Point X="3.160034667969" Y="1.052695922852" />
                  <Point X="3.178244384766" Y="1.039370117188" />
                  <Point X="3.187746337891" Y="1.033249755859" />
                  <Point X="3.230279296875" Y="1.009307312012" />
                  <Point X="3.257388427734" Y="0.994047302246" />
                  <Point X="3.265479492188" Y="0.989987670898" />
                  <Point X="3.294678222656" Y="0.97808416748" />
                  <Point X="3.330275146484" Y="0.968021179199" />
                  <Point X="3.343670898438" Y="0.965257568359" />
                  <Point X="3.401178222656" Y="0.957657287598" />
                  <Point X="3.437831542969" Y="0.952812927246" />
                  <Point X="3.444029785156" Y="0.952199707031" />
                  <Point X="3.465716064453" Y="0.95122277832" />
                  <Point X="3.491217529297" Y="0.952032348633" />
                  <Point X="3.500603515625" Y="0.952797180176" />
                  <Point X="4.052448730469" Y="1.02544909668" />
                  <Point X="4.704703613281" Y="1.111320068359" />
                  <Point X="4.715636230469" Y="1.066411987305" />
                  <Point X="4.75268359375" Y="0.914233581543" />
                  <Point X="4.78387109375" Y="0.713920959473" />
                  <Point X="4.485987304688" Y="0.634103149414" />
                  <Point X="3.691991943359" Y="0.421352874756" />
                  <Point X="3.686031982422" Y="0.419544586182" />
                  <Point X="3.665626953125" Y="0.412137969971" />
                  <Point X="3.642381591797" Y="0.401619476318" />
                  <Point X="3.634004882812" Y="0.397316833496" />
                  <Point X="3.577505615234" Y="0.364659179688" />
                  <Point X="3.541494873047" Y="0.343844268799" />
                  <Point X="3.533882324219" Y="0.338945953369" />
                  <Point X="3.508773925781" Y="0.319870330811" />
                  <Point X="3.481993896484" Y="0.294350738525" />
                  <Point X="3.472796875" Y="0.2842265625" />
                  <Point X="3.438897460938" Y="0.241030792236" />
                  <Point X="3.417291015625" Y="0.21349899292" />
                  <Point X="3.410854980469" Y="0.204209030151" />
                  <Point X="3.399130615234" Y="0.184928924561" />
                  <Point X="3.393842529297" Y="0.174938781738" />
                  <Point X="3.384068847656" Y="0.153475265503" />
                  <Point X="3.380005126953" Y="0.142928543091" />
                  <Point X="3.373158935547" Y="0.12142741394" />
                  <Point X="3.370376464844" Y="0.110473007202" />
                  <Point X="3.359076660156" Y="0.05146957016" />
                  <Point X="3.351874267578" Y="0.013862477303" />
                  <Point X="3.350603515625" Y="0.004968437195" />
                  <Point X="3.348584472656" Y="-0.026260652542" />
                  <Point X="3.350280029297" Y="-0.062940486908" />
                  <Point X="3.351874511719" Y="-0.076422966003" />
                  <Point X="3.363174560547" Y="-0.135426544189" />
                  <Point X="3.370376708984" Y="-0.173033493042" />
                  <Point X="3.373158935547" Y="-0.183987594604" />
                  <Point X="3.380005126953" Y="-0.205488723755" />
                  <Point X="3.384069091797" Y="-0.216035751343" />
                  <Point X="3.393843017578" Y="-0.237499572754" />
                  <Point X="3.399130615234" Y="-0.24748866272" />
                  <Point X="3.410854492188" Y="-0.266768310547" />
                  <Point X="3.417290771484" Y="-0.276059051514" />
                  <Point X="3.451190185547" Y="-0.319254943848" />
                  <Point X="3.472796630859" Y="-0.346786590576" />
                  <Point X="3.478718017578" Y="-0.353633209229" />
                  <Point X="3.501139892578" Y="-0.375805511475" />
                  <Point X="3.530176757813" Y="-0.398724945068" />
                  <Point X="3.541494628906" Y="-0.406404144287" />
                  <Point X="3.597993896484" Y="-0.439061798096" />
                  <Point X="3.634004638672" Y="-0.459876739502" />
                  <Point X="3.63949609375" Y="-0.462815155029" />
                  <Point X="3.659157958984" Y="-0.472016693115" />
                  <Point X="3.683027587891" Y="-0.481027709961" />
                  <Point X="3.691991943359" Y="-0.483913085938" />
                  <Point X="4.198059082031" Y="-0.619513183594" />
                  <Point X="4.784876953125" Y="-0.776750671387" />
                  <Point X="4.782299316406" Y="-0.793848022461" />
                  <Point X="4.76161328125" Y="-0.931051330566" />
                  <Point X="4.727801757812" Y="-1.079219604492" />
                  <Point X="4.357593261719" Y="-1.030480834961" />
                  <Point X="3.436781982422" Y="-0.909253662109" />
                  <Point X="3.428624267578" Y="-0.908535827637" />
                  <Point X="3.400097412109" Y="-0.908042541504" />
                  <Point X="3.366720703125" Y="-0.910841125488" />
                  <Point X="3.354480957031" Y="-0.912676330566" />
                  <Point X="3.243593017578" Y="-0.936778198242" />
                  <Point X="3.172916748047" Y="-0.952140014648" />
                  <Point X="3.157873779297" Y="-0.956742553711" />
                  <Point X="3.128753173828" Y="-0.968367248535" />
                  <Point X="3.114675292969" Y="-0.975389526367" />
                  <Point X="3.086848876953" Y="-0.992282043457" />
                  <Point X="3.074124023438" Y="-1.001530517578" />
                  <Point X="3.050374267578" Y="-1.022000976562" />
                  <Point X="3.039349609375" Y="-1.03322277832" />
                  <Point X="2.972324707031" Y="-1.113832519531" />
                  <Point X="2.92960546875" Y="-1.16521081543" />
                  <Point X="2.921326171875" Y="-1.176847290039" />
                  <Point X="2.90660546875" Y="-1.201229614258" />
                  <Point X="2.900163818359" Y="-1.213975830078" />
                  <Point X="2.888820556641" Y="-1.241361206055" />
                  <Point X="2.884362792969" Y="-1.254928100586" />
                  <Point X="2.877531005859" Y="-1.282577758789" />
                  <Point X="2.875157226562" Y="-1.296660400391" />
                  <Point X="2.865551025391" Y="-1.401053710938" />
                  <Point X="2.859428222656" Y="-1.467590576172" />
                  <Point X="2.859288574219" Y="-1.483320800781" />
                  <Point X="2.861607177734" Y="-1.514589233398" />
                  <Point X="2.864065429688" Y="-1.530127319336" />
                  <Point X="2.871796875" Y="-1.561748291016" />
                  <Point X="2.876785888672" Y="-1.576668212891" />
                  <Point X="2.889157470703" Y="-1.605479736328" />
                  <Point X="2.896540039062" Y="-1.619371337891" />
                  <Point X="2.957906982422" Y="-1.714823486328" />
                  <Point X="2.997020263672" Y="-1.775661865234" />
                  <Point X="3.001741943359" Y="-1.782353271484" />
                  <Point X="3.019793457031" Y="-1.804449951172" />
                  <Point X="3.043489013672" Y="-1.828120239258" />
                  <Point X="3.052796142578" Y="-1.836277832031" />
                  <Point X="3.5224296875" Y="-2.196640380859" />
                  <Point X="4.087170654297" Y="-2.629981689453" />
                  <Point X="4.045486816406" Y="-2.697432861328" />
                  <Point X="4.001274658203" Y="-2.760251953125" />
                  <Point X="3.668561279297" Y="-2.568159667969" />
                  <Point X="2.848454589844" Y="-2.094670654297" />
                  <Point X="2.841191894531" Y="-2.090885253906" />
                  <Point X="2.815026123047" Y="-2.079513183594" />
                  <Point X="2.783119140625" Y="-2.069325927734" />
                  <Point X="2.771108398438" Y="-2.066337646484" />
                  <Point X="2.639134033203" Y="-2.042503051758" />
                  <Point X="2.555018066406" Y="-2.027312011719" />
                  <Point X="2.539359130859" Y="-2.025807495117" />
                  <Point X="2.508006835938" Y="-2.025403198242" />
                  <Point X="2.492313476562" Y="-2.026503662109" />
                  <Point X="2.460140625" Y="-2.031461425781" />
                  <Point X="2.444844482422" Y="-2.035136230469" />
                  <Point X="2.415068603516" Y="-2.044959838867" />
                  <Point X="2.400588867188" Y="-2.051108642578" />
                  <Point X="2.290950683594" Y="-2.108810302734" />
                  <Point X="2.221070800781" Y="-2.145587646484" />
                  <Point X="2.208968505859" Y="-2.153170166016" />
                  <Point X="2.186037353516" Y="-2.170063476562" />
                  <Point X="2.175208496094" Y="-2.179374267578" />
                  <Point X="2.154248779297" Y="-2.200334228516" />
                  <Point X="2.144937988281" Y="-2.211163330078" />
                  <Point X="2.128045654297" Y="-2.23409375" />
                  <Point X="2.120464111328" Y="-2.246195068359" />
                  <Point X="2.062762207031" Y="-2.355833251953" />
                  <Point X="2.025984985352" Y="-2.425713134766" />
                  <Point X="2.019836303711" Y="-2.440192382812" />
                  <Point X="2.010012329102" Y="-2.469968261719" />
                  <Point X="2.006337280273" Y="-2.485264892578" />
                  <Point X="2.001379516602" Y="-2.5174375" />
                  <Point X="2.000279174805" Y="-2.533130859375" />
                  <Point X="2.00068347168" Y="-2.564483154297" />
                  <Point X="2.002187866211" Y="-2.580142089844" />
                  <Point X="2.026022338867" Y="-2.712116210938" />
                  <Point X="2.041213500977" Y="-2.796232421875" />
                  <Point X="2.043015014648" Y="-2.804221435547" />
                  <Point X="2.051236083984" Y="-2.83154296875" />
                  <Point X="2.064069580078" Y="-2.862479736328" />
                  <Point X="2.069546630859" Y="-2.873578613281" />
                  <Point X="2.371333496094" Y="-3.396288330078" />
                  <Point X="2.735893066406" Y="-4.027724365234" />
                  <Point X="2.723754150391" Y="-4.036083251953" />
                  <Point X="2.459731933594" Y="-3.692003173828" />
                  <Point X="1.833914550781" Y="-2.876422851562" />
                  <Point X="1.828653686523" Y="-2.870146484375" />
                  <Point X="1.808831665039" Y="-2.849626953125" />
                  <Point X="1.783252197266" Y="-2.828004882812" />
                  <Point X="1.773299072266" Y="-2.820647216797" />
                  <Point X="1.643136962891" Y="-2.73696484375" />
                  <Point X="1.56017565918" Y="-2.683628417969" />
                  <Point X="1.546283813477" Y="-2.676245849609" />
                  <Point X="1.51747265625" Y="-2.663874511719" />
                  <Point X="1.502553344727" Y="-2.658885742188" />
                  <Point X="1.470932128906" Y="-2.651154052734" />
                  <Point X="1.455394165039" Y="-2.648695800781" />
                  <Point X="1.424125244141" Y="-2.646376953125" />
                  <Point X="1.408394287109" Y="-2.646516357422" />
                  <Point X="1.266039672852" Y="-2.659615966797" />
                  <Point X="1.175307373047" Y="-2.667965332031" />
                  <Point X="1.161225097656" Y="-2.670339111328" />
                  <Point X="1.133575317383" Y="-2.677170898438" />
                  <Point X="1.120007568359" Y="-2.68162890625" />
                  <Point X="1.092622680664" Y="-2.692972167969" />
                  <Point X="1.079876708984" Y="-2.699413574219" />
                  <Point X="1.055494995117" Y="-2.714134033203" />
                  <Point X="1.043859008789" Y="-2.722413085938" />
                  <Point X="0.933936218262" Y="-2.813810058594" />
                  <Point X="0.863875244141" Y="-2.872063476562" />
                  <Point X="0.852653442383" Y="-2.883087890625" />
                  <Point X="0.832182434082" Y="-2.906837890625" />
                  <Point X="0.82293347168" Y="-2.919563720703" />
                  <Point X="0.806041137695" Y="-2.947390136719" />
                  <Point X="0.799019165039" Y="-2.961467285156" />
                  <Point X="0.787394470215" Y="-2.990587890625" />
                  <Point X="0.782791748047" Y="-3.005631103516" />
                  <Point X="0.749925598145" Y="-3.156842041016" />
                  <Point X="0.728977478027" Y="-3.253218994141" />
                  <Point X="0.727584594727" Y="-3.261289550781" />
                  <Point X="0.724724487305" Y="-3.289677490234" />
                  <Point X="0.724742370605" Y="-3.323170410156" />
                  <Point X="0.725554992676" Y="-3.335520019531" />
                  <Point X="0.811078857422" Y="-3.985139404297" />
                  <Point X="0.833091308594" Y="-4.15233984375" />
                  <Point X="0.655064941406" Y="-3.487936523438" />
                  <Point X="0.652606140137" Y="-3.480124511719" />
                  <Point X="0.642145141602" Y="-3.453580566406" />
                  <Point X="0.62678704834" Y="-3.423815917969" />
                  <Point X="0.620407592773" Y="-3.413210205078" />
                  <Point X="0.520413208008" Y="-3.269137207031" />
                  <Point X="0.456679840088" Y="-3.177309814453" />
                  <Point X="0.446670837402" Y="-3.165173095703" />
                  <Point X="0.424787139893" Y="-3.142717773438" />
                  <Point X="0.41291217041" Y="-3.132399169922" />
                  <Point X="0.386656890869" Y="-3.113155273438" />
                  <Point X="0.373242950439" Y="-3.104938232422" />
                  <Point X="0.345241790771" Y="-3.090829833984" />
                  <Point X="0.330654754639" Y="-3.084938476562" />
                  <Point X="0.175919143677" Y="-3.0369140625" />
                  <Point X="0.077295654297" Y="-3.006305175781" />
                  <Point X="0.06337638092" Y="-3.003109130859" />
                  <Point X="0.035216796875" Y="-2.99883984375" />
                  <Point X="0.020976644516" Y="-2.997766601562" />
                  <Point X="-0.008664708138" Y="-2.997766601562" />
                  <Point X="-0.022905158997" Y="-2.998840087891" />
                  <Point X="-0.051064441681" Y="-3.003109375" />
                  <Point X="-0.064983573914" Y="-3.006305175781" />
                  <Point X="-0.219719192505" Y="-3.054329345703" />
                  <Point X="-0.318342681885" Y="-3.084938476562" />
                  <Point X="-0.33292956543" Y="-3.090829589844" />
                  <Point X="-0.360930847168" Y="-3.104937988281" />
                  <Point X="-0.374344970703" Y="-3.113155273438" />
                  <Point X="-0.400600402832" Y="-3.132399169922" />
                  <Point X="-0.412475494385" Y="-3.142718017578" />
                  <Point X="-0.434359191895" Y="-3.165173339844" />
                  <Point X="-0.444367889404" Y="-3.177309570312" />
                  <Point X="-0.544362304688" Y="-3.321382324219" />
                  <Point X="-0.60809564209" Y="-3.413209960938" />
                  <Point X="-0.612470947266" Y="-3.420133300781" />
                  <Point X="-0.62597644043" Y="-3.445264648438" />
                  <Point X="-0.638777648926" Y="-3.476215332031" />
                  <Point X="-0.642753173828" Y="-3.487936523438" />
                  <Point X="-0.798684631348" Y="-4.069881835938" />
                  <Point X="-0.985425170898" Y="-4.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.394142107554" Y="4.783335746388" />
                  <Point X="0.37718468115" Y="4.781253636233" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.341236747837" Y="4.693042576531" />
                  <Point X="-1.478055472014" Y="4.553458788657" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.814839350619" Y="4.739277439232" />
                  <Point X="0.350665904063" Y="4.682284106414" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.316407321746" Y="4.600377813293" />
                  <Point X="-1.465654778282" Y="4.459267968973" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.082525828036" Y="4.676431772403" />
                  <Point X="0.324147126977" Y="4.583314576595" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.291577895655" Y="4.507713050054" />
                  <Point X="-1.46732407198" Y="4.363349572061" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.34531994619" Y="4.612985399389" />
                  <Point X="0.297628325552" Y="4.484345043788" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.266748469564" Y="4.415048286816" />
                  <Point X="-1.496457392266" Y="4.264059016703" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.129619358206" Y="4.186316502735" />
                  <Point X="-2.399791860191" Y="4.153143490711" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.569514890822" Y="4.544799643804" />
                  <Point X="0.27110950702" Y="4.38537550888" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.241919043473" Y="4.322383523577" />
                  <Point X="-1.545133661562" Y="4.162368888933" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.033699773407" Y="4.102380513418" />
                  <Point X="-2.620944145524" Y="4.030275971045" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.766661872024" Y="4.473292815906" />
                  <Point X="0.244590688488" Y="4.286405973972" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.212795204964" Y="4.230246047882" />
                  <Point X="-1.762033273901" Y="4.040023531853" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.848177646187" Y="4.029446332928" />
                  <Point X="-2.778582013591" Y="3.915207041214" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.945046310217" Y="4.399482237403" />
                  <Point X="0.198891446022" Y="4.185081379134" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.149426722807" Y="4.14231328572" />
                  <Point X="-2.927226809285" Y="3.801242321826" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.107148095009" Y="4.323672400451" />
                  <Point X="-3.05366452993" Y="3.690004288396" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.266187402826" Y="4.247486538609" />
                  <Point X="-3.002062430844" Y="3.600626796055" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.401876474519" Y="4.168433628278" />
                  <Point X="-2.950460331758" Y="3.511249303715" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.537565848952" Y="4.089380755118" />
                  <Point X="-2.898858175828" Y="3.421871818354" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.666500592966" Y="4.009498517629" />
                  <Point X="-2.847255994053" Y="3.332494336166" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.78127467661" Y="3.927877569673" />
                  <Point X="-2.795653812278" Y="3.243116853978" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.777220861955" Y="3.831666390402" />
                  <Point X="-2.76004748581" Y="3.15177532772" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.723649138048" Y="3.033459921965" />
                  <Point X="-3.727958301144" Y="3.032930823266" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.717744475816" Y="3.728650175027" />
                  <Point X="-2.749926311337" Y="3.057304618265" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.586941960064" Y="2.954532019368" />
                  <Point X="-3.810287116828" Y="2.927108682364" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.658268089676" Y="3.625633959653" />
                  <Point X="-2.757049320345" Y="2.960716589313" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.450234865652" Y="2.875604106509" />
                  <Point X="-3.89261601558" Y="2.821286531263" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.598791703537" Y="3.522617744278" />
                  <Point X="-2.814431925789" Y="2.857957457882" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.31352777124" Y="2.79667619365" />
                  <Point X="-3.974944914332" Y="2.715464380161" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.53931526566" Y="3.41960152255" />
                  <Point X="-2.947716120911" Y="2.74587878309" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.103999751584" Y="2.726689566121" />
                  <Point X="-4.042482270433" Y="2.611458402129" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.479838820594" Y="3.31658529994" />
                  <Point X="-4.102662058172" Y="2.508355819898" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.420362375529" Y="3.21356907733" />
                  <Point X="-4.162841812282" Y="2.405253241796" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.360885930463" Y="3.11055285472" />
                  <Point X="-4.223021544203" Y="2.302150666418" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.301409485397" Y="3.00753663211" />
                  <Point X="-4.128291109674" Y="2.218068667807" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.241933040331" Y="2.9045204095" />
                  <Point X="-4.02076167906" Y="2.135558188311" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.182456595266" Y="2.80150418689" />
                  <Point X="-3.913232248447" Y="2.053047708814" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.1229801502" Y="2.69848796428" />
                  <Point X="-3.805702652669" Y="1.970537249597" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.063647324429" Y="2.595489375902" />
                  <Point X="-3.698172967979" Y="1.888026801297" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.973949750088" Y="2.73433158701" />
                  <Point X="3.828784794066" Y="2.716507571626" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.029255732074" Y="2.495553185917" />
                  <Point X="-3.590643283289" Y="1.805516352997" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.03722885681" Y="2.646387850924" />
                  <Point X="3.618224674552" Y="2.594940606389" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.013107880533" Y="2.397857045638" />
                  <Point X="-3.497827801658" Y="1.721199227736" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.092138184128" Y="2.55741643515" />
                  <Point X="3.407664555037" Y="2.473373641153" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.021419758913" Y="2.303164182556" />
                  <Point X="-3.447091403432" Y="1.631715440695" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.111634040184" Y="2.464096791857" />
                  <Point X="3.197104424269" Y="2.351806674534" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.051882877361" Y="2.21119114976" />
                  <Point X="-3.421732009646" Y="1.539115749308" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.963136294698" Y="2.350150127964" />
                  <Point X="2.986544185439" Y="2.230239694647" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.110635860077" Y="2.122691675526" />
                  <Point X="-3.435087800913" Y="1.441762430923" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.540707512921" Y="1.306009400058" />
                  <Point X="-4.640210798808" Y="1.293791932792" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.814638379671" Y="2.236203443253" />
                  <Point X="2.775983946608" Y="2.10867271476" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.209349444154" Y="2.039098746184" />
                  <Point X="-3.479912438211" Y="1.340545224096" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.164530651493" Y="1.256484677392" />
                  <Point X="-4.667039936057" Y="1.194784295537" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.666140141886" Y="2.122256718913" />
                  <Point X="-3.641998997456" Y="1.224930063672" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.78835367252" Y="1.206959969158" />
                  <Point X="-4.693869073306" Y="1.095776658282" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.5176419041" Y="2.008309994573" />
                  <Point X="-4.720698210555" Y="0.996769021027" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.369143666314" Y="1.894363270234" />
                  <Point X="-4.740039098963" Y="0.898680825118" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.220645428528" Y="1.780416545894" />
                  <Point X="-4.754464336265" Y="0.801196195271" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.106555517376" Y="1.670694632831" />
                  <Point X="-4.768889649941" Y="0.703711556047" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.039735270025" Y="1.566776704682" />
                  <Point X="-4.783315085027" Y="0.606226901915" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.00906090839" Y="1.467296933239" />
                  <Point X="-4.57667795597" Y="0.535885317654" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.002483680297" Y="1.370775917757" />
                  <Point X="-4.331719647903" Y="0.470248982531" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.020242130538" Y="1.277242947853" />
                  <Point X="-4.086761339837" Y="0.404612647408" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.055605352767" Y="1.185871572148" />
                  <Point X="-3.841803099122" Y="0.338976304015" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.115083815739" Y="1.097461175689" />
                  <Point X="-3.596844858655" Y="0.273339960592" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.220676467193" Y="1.014712889614" />
                  <Point X="-3.412408464613" Y="0.20027246883" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.707092217589" Y="1.101508361427" />
                  <Point X="3.565216313492" Y="0.961303629936" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.48918132462" Y="0.951967707214" />
                  <Point X="-3.33457705895" Y="0.11411553038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.729716977966" Y="1.008572899276" />
                  <Point X="-3.300482249685" Y="0.022588413146" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.752341826796" Y="0.915637447987" />
                  <Point X="-3.298625053463" Y="-0.07289698525" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.767085193484" Y="0.821734272373" />
                  <Point X="-3.332400571942" Y="-0.172757530874" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.781707677874" Y="0.72781625428" />
                  <Point X="-3.440618137189" Y="-0.281758410523" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.22207817438" Y="0.563388958006" />
                  <Point X="-4.015364022261" Y="-0.448041765071" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.629946249787" Y="0.39497086623" />
                  <Point X="-4.674707806729" Y="-0.62471243555" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.469093153204" Y="0.279507155978" />
                  <Point X="-4.775279300971" Y="-0.732774495728" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.393656989348" Y="0.174531326304" />
                  <Point X="-4.761826316586" Y="-0.826836110366" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.363607563725" Y="0.075128287355" />
                  <Point X="-4.748373332202" Y="-0.920897725004" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.348834132509" Y="-0.022399095329" />
                  <Point X="-4.730469448951" Y="-1.01441283798" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.359605386694" Y="-0.116789985032" />
                  <Point X="-4.706764953534" Y="-1.107215725337" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.381664381876" Y="-0.209794914413" />
                  <Point X="-4.683060436119" Y="-1.200018609993" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.435239616333" Y="-0.298930136193" />
                  <Point X="-3.154055930137" Y="-1.107993896526" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.512921983961" Y="-0.385105374213" />
                  <Point X="-3.016990241077" Y="-1.186877779499" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.643664028757" Y="-0.46476570307" />
                  <Point X="-2.960427949509" Y="-1.275646236784" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.872760033418" Y="-0.532349684151" />
                  <Point X="-2.948892970079" Y="-1.369943352818" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.117718451799" Y="-0.59798600573" />
                  <Point X="-2.971507743887" Y="-1.468433531308" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.362676636221" Y="-0.663622356034" />
                  <Point X="-3.04770484675" Y="-1.573502792544" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.607634706461" Y="-0.729258720359" />
                  <Point X="-3.196111762089" Y="-1.687438303898" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.780812648451" Y="-0.803708576212" />
                  <Point X="3.670557330748" Y="-0.940030787887" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.058250037374" Y="-1.015212670041" />
                  <Point X="-3.344609953128" Y="-1.801385022498" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.76610982862" Y="-0.901227288908" />
                  <Point X="4.046734473163" Y="-0.989555476052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.965246458608" Y="-1.122345507041" />
                  <Point X="-3.493108144167" Y="-1.915331741098" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.746014481657" Y="-0.999408120679" />
                  <Point X="4.422911706417" Y="-1.039080153064" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.894894571408" Y="-1.226697066038" />
                  <Point X="-3.641606335205" Y="-2.029278459698" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.872535090276" Y="-1.325155898529" />
                  <Point X="-3.790104526244" Y="-2.143225178298" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.863626916474" Y="-1.421963118157" />
                  <Point X="-3.93860265842" Y="-2.257171889671" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.862124796124" Y="-1.517860988763" />
                  <Point X="-4.087100757432" Y="-2.371118596972" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.891539902622" Y="-1.609962701246" />
                  <Point X="-4.161805492142" Y="-2.476004618439" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.947600685885" Y="-1.698792736008" />
                  <Point X="-2.480081656203" Y="-2.365228329102" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.712358753086" Y="-2.39374837045" />
                  <Point X="-4.108620456527" Y="-2.565187750613" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.005829322431" Y="-1.787356591856" />
                  <Point X="-2.368846191429" Y="-2.447283764821" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.922918823233" Y="-2.515315329626" />
                  <Point X="-4.055435420912" Y="-2.654370882787" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.098886306754" Y="-1.871644064316" />
                  <Point X="-2.320887028702" Y="-2.537108553503" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.13347889338" Y="-2.636882288801" />
                  <Point X="-4.001804653076" Y="-2.743499285926" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.206415998809" Y="-1.954154511712" />
                  <Point X="2.577511598219" Y="-2.031374262388" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.395659586577" Y="-2.053702881787" />
                  <Point X="-2.313091924121" Y="-2.631864868428" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.344039244418" Y="-2.758449282465" />
                  <Point X="-3.935178965496" Y="-2.83103211355" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.313945690863" Y="-2.036664959107" />
                  <Point X="2.847133813545" Y="-2.093982250488" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.179129596516" Y="-2.176002854957" />
                  <Point X="-2.345804630067" Y="-2.731594917083" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.554599695065" Y="-2.88001628836" />
                  <Point X="-3.868553256367" Y="-2.918564938528" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.421475382918" Y="-2.119175406503" />
                  <Point X="2.983946815418" Y="-2.172897159546" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.102051611458" Y="-2.281180274926" />
                  <Point X="-2.405281087106" Y="-2.834611141163" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.765160145712" Y="-3.001583294255" />
                  <Point X="-3.801927400936" Y="-3.006097745543" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.529005071611" Y="-2.201685854311" />
                  <Point X="3.120653913356" Y="-2.251825071972" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.048198223356" Y="-2.383506072956" />
                  <Point X="-2.464757544144" Y="-2.937627365243" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.636534708691" Y="-2.284196308457" />
                  <Point X="3.257361011295" Y="-2.330752984398" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.006561443349" Y="-2.484331860125" />
                  <Point X="-2.524234001183" Y="-3.040643589323" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.744064345772" Y="-2.366706762602" />
                  <Point X="3.394068109234" Y="-2.409680896823" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.00226564352" Y="-2.58057275144" />
                  <Point X="1.450440061318" Y="-2.648328413245" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.095056544646" Y="-2.691964022292" />
                  <Point X="-2.583710458222" Y="-3.143659813403" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.851593982852" Y="-2.449217216748" />
                  <Point X="3.530775207173" Y="-2.488608809249" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.019176456118" Y="-2.674209798159" />
                  <Point X="1.621479161757" Y="-2.723040885819" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.942895845768" Y="-2.806360440309" />
                  <Point X="-2.64318691526" Y="-3.246676037483" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.959123619933" Y="-2.531727670894" />
                  <Point X="3.667482305112" Y="-2.567536721675" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.036087144648" Y="-2.767846860112" />
                  <Point X="1.746481499971" Y="-2.803405962028" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.825136202528" Y="-2.916532939815" />
                  <Point X="0.082030869121" Y="-3.007774801882" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.169050744515" Y="-3.038603747563" />
                  <Point X="-2.702663372299" Y="-3.349692261563" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.066653257013" Y="-2.614238125039" />
                  <Point X="3.804189375764" Y="-2.646464637451" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.063139903189" Y="-2.860238632452" />
                  <Point X="1.84230237836" Y="-2.887354070969" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.780153417155" Y="-3.017769564784" />
                  <Point X="0.303002794411" Y="-3.076356294482" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.435937139987" Y="-3.167086709861" />
                  <Point X="-2.762139829337" Y="-3.452708485644" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.033840605225" Y="-2.7139804455" />
                  <Point X="3.940896446198" Y="-2.725392553254" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.113532676616" Y="-2.949764611312" />
                  <Point X="1.909422188478" Y="-2.974826227974" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.758779394704" Y="-3.116107398164" />
                  <Point X="0.437319690148" Y="-3.155577686836" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.510015992462" Y="-3.271895882652" />
                  <Point X="-2.821616293698" Y="-3.555724710623" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.165134837579" Y="-3.039142096055" />
                  <Point X="1.976541998595" Y="-3.06229838498" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.737405187503" Y="-3.214445254228" />
                  <Point X="0.50247456346" Y="-3.243291107744" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.582634847162" Y="-3.376525790259" />
                  <Point X="-2.881092767832" Y="-3.658740936802" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.216736998542" Y="-3.128519580799" />
                  <Point X="2.043661808713" Y="-3.149770541985" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.724736253665" Y="-3.311714237125" />
                  <Point X="0.563688305603" Y="-3.331488438712" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.639810140755" Y="-3.479259466996" />
                  <Point X="-2.940569241967" Y="-3.761757162981" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.268339159506" Y="-3.217897065542" />
                  <Point X="2.11078161883" Y="-3.23724269899" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.734858178397" Y="-3.40618485446" />
                  <Point X="0.624343937644" Y="-3.419754296984" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.666967943576" Y="-3.578307459309" />
                  <Point X="-2.94628383524" Y="-3.858172260226" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.319941320469" Y="-3.307274550285" />
                  <Point X="2.177901428948" Y="-3.324714855996" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.747258617464" Y="-3.500375705413" />
                  <Point X="0.661228407323" Y="-3.510938886989" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.693486694038" Y="-3.677276985858" />
                  <Point X="-1.653097592526" Y="-3.795102388667" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.157049183269" Y="-3.856979863453" />
                  <Point X="-2.839072099532" Y="-3.940721747752" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.371543481152" Y="-3.396652035062" />
                  <Point X="2.245021239065" Y="-3.412187013001" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.75965905653" Y="-3.594566556366" />
                  <Point X="0.686057859686" Y="-3.603603647002" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.7200054445" Y="-3.776246512408" />
                  <Point X="-1.478164297462" Y="-3.869336714264" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.332542646476" Y="-3.974241184692" />
                  <Point X="-2.728665203573" Y="-4.022878918929" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.423145573362" Y="-3.486029528247" />
                  <Point X="2.312141049183" Y="-3.499659170007" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.772059495597" Y="-3.688757407318" />
                  <Point X="0.710887312049" Y="-3.696268407015" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.746524194963" Y="-3.875216038958" />
                  <Point X="-1.382428135655" Y="-3.953295225092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.442572593054" Y="-4.083464596788" />
                  <Point X="-2.599664176258" Y="-4.102753017853" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.474747665573" Y="-3.575407021432" />
                  <Point X="2.3792608593" Y="-3.587131327012" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.784459934663" Y="-3.782948258271" />
                  <Point X="0.735716764412" Y="-3.788933167028" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.773042945425" Y="-3.974185565508" />
                  <Point X="-1.286692009445" Y="-4.037253740292" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.526349757783" Y="-3.664784514617" />
                  <Point X="2.446380669418" Y="-3.674603484017" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.79686037373" Y="-3.877139109224" />
                  <Point X="0.760546216774" Y="-3.88159792704" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.799561698233" Y="-4.073155092346" />
                  <Point X="-1.212377091485" Y="-4.12384244914" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.577951849994" Y="-3.754162007802" />
                  <Point X="2.513500486539" Y="-3.762075640163" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.809260812796" Y="-3.971329960177" />
                  <Point X="0.785375669137" Y="-3.974262687053" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.826080519616" Y="-4.172124627603" />
                  <Point X="-1.179462409656" Y="-4.215514467803" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.629553942204" Y="-3.843539500986" />
                  <Point X="2.580620305399" Y="-3.849547796095" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.821661315382" Y="-4.06552080333" />
                  <Point X="0.8102051215" Y="-4.066927447066" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.852599341" Y="-4.271094162861" />
                  <Point X="-1.160877783513" Y="-4.308945996061" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.681156034414" Y="-3.932916994171" />
                  <Point X="2.64774012426" Y="-3.937019952027" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.879118162383" Y="-4.370063698119" />
                  <Point X="-1.142293063778" Y="-4.402377512828" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.732758126625" Y="-4.022294487356" />
                  <Point X="2.71485994312" Y="-4.024492107959" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.905636983766" Y="-4.469033233377" />
                  <Point X="-1.123708341043" Y="-4.495809029226" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.93215580515" Y="-4.568002768635" />
                  <Point X="-1.122826233371" Y="-4.591414153441" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.958674626533" Y="-4.666972303892" />
                  <Point X="-1.135634242163" Y="-4.688700212595" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.985193447916" Y="-4.76594183915" />
                  <Point X="-0.988064433913" Y="-4.766294351905" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.0001640625" Y="0.001626220703" />
                  <Width Value="9.996463867188" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.716188598633" Y="-4.450156738281" />
                  <Point X="0.471539031982" Y="-3.537112304688" />
                  <Point X="0.464318664551" Y="-3.521544189453" />
                  <Point X="0.364324249268" Y="-3.377471191406" />
                  <Point X="0.300591033936" Y="-3.285643798828" />
                  <Point X="0.274335754395" Y="-3.266399902344" />
                  <Point X="0.119600166321" Y="-3.218375488281" />
                  <Point X="0.020976625443" Y="-3.187766601562" />
                  <Point X="-0.008664756775" Y="-3.187766601562" />
                  <Point X="-0.163400344849" Y="-3.235790771484" />
                  <Point X="-0.262023895264" Y="-3.266399902344" />
                  <Point X="-0.288279144287" Y="-3.285643798828" />
                  <Point X="-0.38827355957" Y="-3.429716552734" />
                  <Point X="-0.45200680542" Y="-3.521544189453" />
                  <Point X="-0.459227142334" Y="-3.537112304688" />
                  <Point X="-0.615158813477" Y="-4.119057617188" />
                  <Point X="-0.84774395752" Y="-4.987077148438" />
                  <Point X="-0.985043151855" Y="-4.960426757812" />
                  <Point X="-1.100246948242" Y="-4.938065429688" />
                  <Point X="-1.279380126953" Y="-4.891975585938" />
                  <Point X="-1.35158972168" Y="-4.873396484375" />
                  <Point X="-1.338600341797" Y="-4.774733398438" />
                  <Point X="-1.309150146484" Y="-4.551038574219" />
                  <Point X="-1.309683227539" Y="-4.5347578125" />
                  <Point X="-1.346649658203" Y="-4.348916015625" />
                  <Point X="-1.370210693359" Y="-4.230466308594" />
                  <Point X="-1.386282714844" Y="-4.20262890625" />
                  <Point X="-1.528743408203" Y="-4.077693603516" />
                  <Point X="-1.619543457031" Y="-3.998063964844" />
                  <Point X="-1.649240722656" Y="-3.985762939453" />
                  <Point X="-1.838317993164" Y="-3.973370117188" />
                  <Point X="-1.958829833984" Y="-3.965471435547" />
                  <Point X="-1.989878540039" Y="-3.973791015625" />
                  <Point X="-2.147427978516" Y="-4.079062255859" />
                  <Point X="-2.247844970703" Y="-4.146159179688" />
                  <Point X="-2.259734130859" Y="-4.157294433594" />
                  <Point X="-2.347277587891" Y="-4.271383300781" />
                  <Point X="-2.457095214844" Y="-4.414500488281" />
                  <Point X="-2.686971923828" Y="-4.272166503906" />
                  <Point X="-2.855831054688" Y="-4.16761328125" />
                  <Point X="-3.103881835938" Y="-3.976622314453" />
                  <Point X="-3.228581054688" Y="-3.880608154297" />
                  <Point X="-2.960687255859" Y="-3.416602539062" />
                  <Point X="-2.506033691406" Y="-2.629119628906" />
                  <Point X="-2.499762695312" Y="-2.597593261719" />
                  <Point X="-2.513979736328" Y="-2.568764404297" />
                  <Point X="-2.531328369141" Y="-2.551415771484" />
                  <Point X="-2.560157226562" Y="-2.537198730469" />
                  <Point X="-2.59168359375" Y="-2.543469726562" />
                  <Point X="-3.093600097656" Y="-2.833251464844" />
                  <Point X="-3.842959472656" Y="-3.265894042969" />
                  <Point X="-4.028293457031" Y="-3.022403320313" />
                  <Point X="-4.161703613281" Y="-2.847129394531" />
                  <Point X="-4.339534667969" Y="-2.548934082031" />
                  <Point X="-4.431020019531" Y="-2.395527099609" />
                  <Point X="-3.959282958984" Y="-2.033550537109" />
                  <Point X="-3.163786865234" Y="-1.423144897461" />
                  <Point X="-3.145822021484" Y="-1.396014038086" />
                  <Point X="-3.138117431641" Y="-1.366266845703" />
                  <Point X="-3.140326171875" Y="-1.334595703125" />
                  <Point X="-3.161159179688" Y="-1.310638916016" />
                  <Point X="-3.187641845703" Y="-1.295052490234" />
                  <Point X="-3.219529296875" Y="-1.288571044922" />
                  <Point X="-3.853151123047" Y="-1.371989013672" />
                  <Point X="-4.803283691406" Y="-1.497076293945" />
                  <Point X="-4.875215332031" Y="-1.215466186523" />
                  <Point X="-4.927392089844" Y="-1.011195678711" />
                  <Point X="-4.974442382812" Y="-0.682225097656" />
                  <Point X="-4.998395996094" Y="-0.514742004395" />
                  <Point X="-4.463653808594" Y="-0.371458190918" />
                  <Point X="-3.557463867188" Y="-0.128645431519" />
                  <Point X="-3.541895751953" Y="-0.121425079346" />
                  <Point X="-3.524946044922" Y="-0.109661010742" />
                  <Point X="-3.514142822266" Y="-0.102163047791" />
                  <Point X="-3.494898925781" Y="-0.075907775879" />
                  <Point X="-3.485647949219" Y="-0.046100738525" />
                  <Point X="-3.485647949219" Y="-0.01645920372" />
                  <Point X="-3.494898925781" Y="0.013347681046" />
                  <Point X="-3.514142822266" Y="0.039602954865" />
                  <Point X="-3.531092529297" Y="0.051367019653" />
                  <Point X="-3.557463867188" Y="0.066085334778" />
                  <Point X="-4.135040527344" Y="0.220846481323" />
                  <Point X="-4.998186523438" Y="0.452125793457" />
                  <Point X="-4.951272460938" Y="0.769167175293" />
                  <Point X="-4.917645019531" Y="0.996418212891" />
                  <Point X="-4.822935546875" Y="1.345925292969" />
                  <Point X="-4.773516113281" Y="1.528298583984" />
                  <Point X="-4.402845214844" Y="1.479498901367" />
                  <Point X="-3.753266357422" Y="1.393980102539" />
                  <Point X="-3.731704833984" Y="1.395866577148" />
                  <Point X="-3.694189697266" Y="1.407695068359" />
                  <Point X="-3.670278808594" Y="1.415234130859" />
                  <Point X="-3.651534423828" Y="1.426056274414" />
                  <Point X="-3.639119873047" Y="1.443786132813" />
                  <Point X="-3.624066650391" Y="1.480127563477" />
                  <Point X="-3.614472412109" Y="1.503290527344" />
                  <Point X="-3.610714111328" Y="1.52460546875" />
                  <Point X="-3.616315917969" Y="1.54551171875" />
                  <Point X="-3.634479003906" Y="1.580402954102" />
                  <Point X="-3.646055664062" Y="1.602641479492" />
                  <Point X="-3.659968261719" Y="1.619221679688" />
                  <Point X="-3.991267578125" Y="1.873436645508" />
                  <Point X="-4.47610546875" Y="2.245466308594" />
                  <Point X="-4.290678710937" Y="2.563147216797" />
                  <Point X="-4.160014648438" Y="2.787006591797" />
                  <Point X="-3.909132568359" Y="3.109479980469" />
                  <Point X="-3.774671142578" Y="3.282310791016" />
                  <Point X="-3.558110351562" Y="3.157279296875" />
                  <Point X="-3.159156982422" Y="2.926943359375" />
                  <Point X="-3.138514892578" Y="2.920434814453" />
                  <Point X="-3.086266845703" Y="2.915863769531" />
                  <Point X="-3.052965576172" Y="2.912950195312" />
                  <Point X="-3.031506835938" Y="2.915775390625" />
                  <Point X="-3.013252441406" Y="2.927404785156" />
                  <Point X="-2.976166259766" Y="2.964490722656" />
                  <Point X="-2.952528808594" Y="2.988128173828" />
                  <Point X="-2.940899414062" Y="3.006382324219" />
                  <Point X="-2.93807421875" Y="3.027841064453" />
                  <Point X="-2.942645507812" Y="3.080089355469" />
                  <Point X="-2.945558837891" Y="3.113390625" />
                  <Point X="-2.952067382812" Y="3.134032958984" />
                  <Point X="-3.098876220703" Y="3.388312988281" />
                  <Point X="-3.307278564453" Y="3.749276611328" />
                  <Point X="-2.980450927734" Y="3.999852050781" />
                  <Point X="-2.752876220703" Y="4.174331542969" />
                  <Point X="-2.357744628906" Y="4.393857910156" />
                  <Point X="-2.141548583984" Y="4.513971679688" />
                  <Point X="-2.089958740234" Y="4.44673828125" />
                  <Point X="-1.967826660156" Y="4.287573242188" />
                  <Point X="-1.951247070312" Y="4.273661132813" />
                  <Point X="-1.893095214844" Y="4.243389160156" />
                  <Point X="-1.856031005859" Y="4.224094238281" />
                  <Point X="-1.835124267578" Y="4.2184921875" />
                  <Point X="-1.813808837891" Y="4.222250488281" />
                  <Point X="-1.753239746094" Y="4.247339355469" />
                  <Point X="-1.714634887695" Y="4.263330078125" />
                  <Point X="-1.696905395508" Y="4.275744140625" />
                  <Point X="-1.686083496094" Y="4.29448828125" />
                  <Point X="-1.666369262695" Y="4.357013671875" />
                  <Point X="-1.653804077148" Y="4.396864746094" />
                  <Point X="-1.651917602539" Y="4.418426757812" />
                  <Point X="-1.668608154297" Y="4.545204101562" />
                  <Point X="-1.689137573242" Y="4.701141113281" />
                  <Point X="-1.262703979492" Y="4.820698242188" />
                  <Point X="-0.968094177246" Y="4.903296386719" />
                  <Point X="-0.489074951172" Y="4.959358398438" />
                  <Point X="-0.22419960022" Y="4.990358398438" />
                  <Point X="-0.16016178894" Y="4.751365722656" />
                  <Point X="-0.042140380859" Y="4.310903320312" />
                  <Point X="-0.024282119751" Y="4.284176757813" />
                  <Point X="0.006155934334" Y="4.273844238281" />
                  <Point X="0.036594032288" Y="4.284176757813" />
                  <Point X="0.054452251434" Y="4.310903320312" />
                  <Point X="0.129675430298" Y="4.591640136719" />
                  <Point X="0.236648406982" Y="4.990868652344" />
                  <Point X="0.602967041016" Y="4.952505371094" />
                  <Point X="0.860205749512" Y="4.925565429688" />
                  <Point X="1.256510986328" Y="4.829885253906" />
                  <Point X="1.508455932617" Y="4.769058105469" />
                  <Point X="1.76680871582" Y="4.6753515625" />
                  <Point X="1.931033691406" Y="4.615786132813" />
                  <Point X="2.180465087891" Y="4.499135253906" />
                  <Point X="2.33869921875" Y="4.425134277344" />
                  <Point X="2.579659912109" Y="4.28475" />
                  <Point X="2.732523681641" Y="4.19569140625" />
                  <Point X="2.959781494141" Y="4.034078369141" />
                  <Point X="3.068740234375" Y="3.956592773438" />
                  <Point X="2.756080810547" Y="3.415050292969" />
                  <Point X="2.229853515625" Y="2.503597900391" />
                  <Point X="2.224852050781" Y="2.491514648438" />
                  <Point X="2.211739746094" Y="2.442480957031" />
                  <Point X="2.203382324219" Y="2.411228271484" />
                  <Point X="2.202044677734" Y="2.392325927734" />
                  <Point X="2.207157470703" Y="2.34992578125" />
                  <Point X="2.210416015625" Y="2.322901123047" />
                  <Point X="2.218682128906" Y="2.3008125" />
                  <Point X="2.24491796875" Y="2.262147705078" />
                  <Point X="2.261639892578" Y="2.23750390625" />
                  <Point X="2.274940185547" Y="2.224203613281" />
                  <Point X="2.313604980469" Y="2.197967773438" />
                  <Point X="2.338248779297" Y="2.18124609375" />
                  <Point X="2.360336669922" Y="2.172980224609" />
                  <Point X="2.402737060547" Y="2.167867431641" />
                  <Point X="2.429761474609" Y="2.164608642578" />
                  <Point X="2.448664306641" Y="2.165946289062" />
                  <Point X="2.497697998047" Y="2.17905859375" />
                  <Point X="2.528950683594" Y="2.187416015625" />
                  <Point X="2.541033935547" Y="2.192417480469" />
                  <Point X="3.121964599609" Y="2.527817871094" />
                  <Point X="3.994248046875" Y="3.031430908203" />
                  <Point X="4.111918457031" Y="2.867895996094" />
                  <Point X="4.202591796875" Y="2.741880126953" />
                  <Point X="4.329284667969" Y="2.532519287109" />
                  <Point X="4.387513183594" Y="2.436296142578" />
                  <Point X="3.981443603516" Y="2.124708007812" />
                  <Point X="3.288616210938" Y="1.593082763672" />
                  <Point X="3.279371337891" Y="1.583833129883" />
                  <Point X="3.244081542969" Y="1.537795043945" />
                  <Point X="3.221589111328" Y="1.508451782227" />
                  <Point X="3.213119384766" Y="1.4915" />
                  <Point X="3.199973876953" Y="1.444494995117" />
                  <Point X="3.191595458984" Y="1.41453527832" />
                  <Point X="3.190779541016" Y="1.390965332031" />
                  <Point X="3.201570556641" Y="1.338666259766" />
                  <Point X="3.208448486328" Y="1.305332519531" />
                  <Point X="3.215646728516" Y="1.287954833984" />
                  <Point X="3.244997070312" Y="1.243343383789" />
                  <Point X="3.263704345703" Y="1.214909423828" />
                  <Point X="3.280947998047" Y="1.198819824219" />
                  <Point X="3.323480957031" Y="1.174877441406" />
                  <Point X="3.350590087891" Y="1.159617431641" />
                  <Point X="3.368565429688" Y="1.153619628906" />
                  <Point X="3.426072753906" Y="1.146019287109" />
                  <Point X="3.462726074219" Y="1.141175048828" />
                  <Point X="3.475803710938" Y="1.14117175293" />
                  <Point X="4.027648925781" Y="1.213823608398" />
                  <Point X="4.848975097656" Y="1.32195324707" />
                  <Point X="4.900244628906" Y="1.111354003906" />
                  <Point X="4.939188476562" Y="0.951385620117" />
                  <Point X="4.979112792969" Y="0.694956481934" />
                  <Point X="4.997858398438" Y="0.574556152344" />
                  <Point X="4.535163085938" Y="0.45057723999" />
                  <Point X="3.741167724609" Y="0.237826919556" />
                  <Point X="3.729086914062" Y="0.232819412231" />
                  <Point X="3.672587646484" Y="0.200161865234" />
                  <Point X="3.636576904297" Y="0.179346878052" />
                  <Point X="3.622264892578" Y="0.166926574707" />
                  <Point X="3.588365478516" Y="0.12373072052" />
                  <Point X="3.566759033203" Y="0.096198890686" />
                  <Point X="3.556985351562" Y="0.074735427856" />
                  <Point X="3.545685546875" Y="0.015731951714" />
                  <Point X="3.538483154297" Y="-0.021875152588" />
                  <Point X="3.538482910156" Y="-0.040684635162" />
                  <Point X="3.549782958984" Y="-0.099688270569" />
                  <Point X="3.556985107422" Y="-0.137295227051" />
                  <Point X="3.566759033203" Y="-0.158758987427" />
                  <Point X="3.600658447266" Y="-0.201954986572" />
                  <Point X="3.622264892578" Y="-0.229486679077" />
                  <Point X="3.636576904297" Y="-0.241906967163" />
                  <Point X="3.693076171875" Y="-0.27456451416" />
                  <Point X="3.729086914062" Y="-0.295379486084" />
                  <Point X="3.741167724609" Y="-0.300387023926" />
                  <Point X="4.247234863281" Y="-0.43598727417" />
                  <Point X="4.998067871094" Y="-0.637172363281" />
                  <Point X="4.970176269531" Y="-0.822172973633" />
                  <Point X="4.948431640625" Y="-0.966399108887" />
                  <Point X="4.897281738281" Y="-1.190545532227" />
                  <Point X="4.874545410156" Y="-1.290178344727" />
                  <Point X="4.332793457031" Y="-1.21885534668" />
                  <Point X="3.411982177734" Y="-1.097628295898" />
                  <Point X="3.3948359375" Y="-1.098341308594" />
                  <Point X="3.283947998047" Y="-1.122443237305" />
                  <Point X="3.213271728516" Y="-1.137805053711" />
                  <Point X="3.1854453125" Y="-1.154697387695" />
                  <Point X="3.118420410156" Y="-1.235307128906" />
                  <Point X="3.075701171875" Y="-1.286685424805" />
                  <Point X="3.064357910156" Y="-1.314070556641" />
                  <Point X="3.054751708984" Y="-1.418463867188" />
                  <Point X="3.04862890625" Y="-1.485000854492" />
                  <Point X="3.056360351562" Y="-1.516621826172" />
                  <Point X="3.117727294922" Y="-1.61207409668" />
                  <Point X="3.156840576172" Y="-1.672912353516" />
                  <Point X="3.168460693359" Y="-1.685540649414" />
                  <Point X="3.638094238281" Y="-2.045903198242" />
                  <Point X="4.339074707031" Y="-2.583784179688" />
                  <Point X="4.265427246094" Y="-2.702956298828" />
                  <Point X="4.204131835938" Y="-2.802141845703" />
                  <Point X="4.098350585938" Y="-2.952441894531" />
                  <Point X="4.056688476562" Y="-3.011638427734" />
                  <Point X="3.573561279297" Y="-2.732704589844" />
                  <Point X="2.753454589844" Y="-2.259215576172" />
                  <Point X="2.737340820313" Y="-2.253312744141" />
                  <Point X="2.605366455078" Y="-2.229478271484" />
                  <Point X="2.521250488281" Y="-2.214287109375" />
                  <Point X="2.489077636719" Y="-2.219244873047" />
                  <Point X="2.379439453125" Y="-2.276946533203" />
                  <Point X="2.309559570312" Y="-2.313723876953" />
                  <Point X="2.288599853516" Y="-2.334683837891" />
                  <Point X="2.230897949219" Y="-2.444322021484" />
                  <Point X="2.194120849609" Y="-2.514201904297" />
                  <Point X="2.189163085938" Y="-2.546374511719" />
                  <Point X="2.212997558594" Y="-2.678348632812" />
                  <Point X="2.228188720703" Y="-2.76246484375" />
                  <Point X="2.234091552734" Y="-2.778578613281" />
                  <Point X="2.535878417969" Y="-3.301288330078" />
                  <Point X="2.986673583984" Y="-4.082088623047" />
                  <Point X="2.908031982422" Y="-4.138260742187" />
                  <Point X="2.835298339844" Y="-4.190212402344" />
                  <Point X="2.717028320312" Y="-4.266766601562" />
                  <Point X="2.679775878906" Y="-4.290879394531" />
                  <Point X="2.308994628906" Y="-3.807667724609" />
                  <Point X="1.683177490234" Y="-2.992087402344" />
                  <Point X="1.670549316406" Y="-2.980467529297" />
                  <Point X="1.540387084961" Y="-2.89678515625" />
                  <Point X="1.45742590332" Y="-2.843448730469" />
                  <Point X="1.4258046875" Y="-2.835717041016" />
                  <Point X="1.283450195312" Y="-2.848816650391" />
                  <Point X="1.192717773438" Y="-2.857166015625" />
                  <Point X="1.165332885742" Y="-2.868509277344" />
                  <Point X="1.05541015625" Y="-2.95990625" />
                  <Point X="0.985349060059" Y="-3.018159667969" />
                  <Point X="0.968456726074" Y="-3.045986083984" />
                  <Point X="0.935590454102" Y="-3.197197021484" />
                  <Point X="0.91464251709" Y="-3.293573974609" />
                  <Point X="0.91392956543" Y="-3.310720214844" />
                  <Point X="0.999453491211" Y="-3.960339599609" />
                  <Point X="1.127642211914" Y="-4.934028320312" />
                  <Point X="1.063155151367" Y="-4.9481640625" />
                  <Point X="0.994347290039" Y="-4.963246582031" />
                  <Point X="0.88508770752" Y="-4.983095214844" />
                  <Point X="0.860200378418" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#178" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.118021454921" Y="4.795586562198" Z="1.55" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.55" />
                  <Point X="-0.501043024775" Y="5.040299717792" Z="1.55" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.55" />
                  <Point X="-1.282357687583" Y="4.900121759192" Z="1.55" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.55" />
                  <Point X="-1.724336386163" Y="4.56995757033" Z="1.55" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.55" />
                  <Point X="-1.720211036827" Y="4.403329113849" Z="1.55" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.55" />
                  <Point X="-1.778529953687" Y="4.324813330738" Z="1.55" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.55" />
                  <Point X="-1.876163249083" Y="4.319019117196" Z="1.55" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.55" />
                  <Point X="-2.056446899308" Y="4.508456521585" Z="1.55" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.55" />
                  <Point X="-2.388183621896" Y="4.46884544635" Z="1.55" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.55" />
                  <Point X="-3.017028955026" Y="4.070812093102" Z="1.55" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.55" />
                  <Point X="-3.148333406096" Y="3.39459305617" Z="1.55" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.55" />
                  <Point X="-2.998611174216" Y="3.107011645774" Z="1.55" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.55" />
                  <Point X="-3.017677487723" Y="3.031126080164" Z="1.55" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.55" />
                  <Point X="-3.088064819832" Y="2.996953524957" Z="1.55" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.55" />
                  <Point X="-3.539266292127" Y="3.23186054265" Z="1.55" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.55" />
                  <Point X="-3.954751861485" Y="3.171462395404" Z="1.55" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.55" />
                  <Point X="-4.340016845627" Y="2.619632240479" Z="1.55" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.55" />
                  <Point X="-4.027861517212" Y="1.865048931163" Z="1.55" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.55" />
                  <Point X="-3.684985641435" Y="1.588595583434" Z="1.55" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.55" />
                  <Point X="-3.676416802243" Y="1.530541524281" Z="1.55" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.55" />
                  <Point X="-3.71538088746" Y="1.486661014917" Z="1.55" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.55" />
                  <Point X="-4.402475017386" Y="1.56035125137" Z="1.55" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.55" />
                  <Point X="-4.877351399051" Y="1.390282691317" Z="1.55" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.55" />
                  <Point X="-5.006889904506" Y="0.807766176404" Z="1.55" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.55" />
                  <Point X="-4.154137206738" Y="0.203830183435" Z="1.55" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.55" />
                  <Point X="-3.565757546443" Y="0.041571001793" Z="1.55" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.55" />
                  <Point X="-3.545206734462" Y="0.018204184908" Z="1.55" />
                  <Point X="-3.539556741714" Y="0" Z="1.55" />
                  <Point X="-3.543157866673" Y="-0.011602766153" Z="1.55" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.55" />
                  <Point X="-3.559611048681" Y="-0.037304981341" Z="1.55" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.55" />
                  <Point X="-4.482750582034" Y="-0.291881873321" Z="1.55" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.55" />
                  <Point X="-5.030094754285" Y="-0.658024069008" Z="1.55" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.55" />
                  <Point X="-4.929815897462" Y="-1.196560254025" Z="1.55" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.55" />
                  <Point X="-3.85278082431" Y="-1.390281202411" Z="1.55" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.55" />
                  <Point X="-3.208850178109" Y="-1.312930565919" Z="1.55" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.55" />
                  <Point X="-3.195675387082" Y="-1.334029375552" Z="1.55" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.55" />
                  <Point X="-3.995876832783" Y="-1.962602717623" Z="1.55" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.55" />
                  <Point X="-4.38863388554" Y="-2.543263560259" Z="1.55" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.55" />
                  <Point X="-4.074235197089" Y="-3.021406532522" Z="1.55" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.55" />
                  <Point X="-3.074755964804" Y="-2.845272573507" Z="1.55" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.55" />
                  <Point X="-2.566086345136" Y="-2.562244123995" Z="1.55" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.55" />
                  <Point X="-3.010144439661" Y="-3.360322417688" Z="1.55" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.55" />
                  <Point X="-3.14054178508" Y="-3.984959453628" Z="1.55" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.55" />
                  <Point X="-2.719449454302" Y="-4.283397243072" Z="1.55" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.55" />
                  <Point X="-2.313765938117" Y="-4.270541259882" Z="1.55" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.55" />
                  <Point X="-2.125805349915" Y="-4.089355634877" Z="1.55" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.55" />
                  <Point X="-1.847744157858" Y="-3.99198313957" Z="1.55" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.55" />
                  <Point X="-1.56786684341" Y="-4.084005510939" Z="1.55" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.55" />
                  <Point X="-1.401844838139" Y="-4.327390250207" Z="1.55" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.55" />
                  <Point X="-1.394328550946" Y="-4.736927212299" Z="1.55" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.55" />
                  <Point X="-1.297994838808" Y="-4.909118563218" Z="1.55" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.55" />
                  <Point X="-1.000726345569" Y="-4.978230696624" Z="1.55" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.55" />
                  <Point X="-0.573018567347" Y="-4.100718473139" Z="1.55" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.55" />
                  <Point X="-0.35335351798" Y="-3.426945598979" Z="1.55" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.55" />
                  <Point X="-0.154735568874" Y="-3.252263578741" Z="1.55" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.55" />
                  <Point X="0.098623510487" Y="-3.234848466547" Z="1.55" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.55" />
                  <Point X="0.317092341452" Y="-3.374700156789" Z="1.55" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.55" />
                  <Point X="0.661736293054" Y="-4.431817556677" Z="1.55" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.55" />
                  <Point X="0.887868717649" Y="-5.00101005716" Z="1.55" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.55" />
                  <Point X="1.067705040214" Y="-4.965724299135" Z="1.55" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.55" />
                  <Point X="1.042869833251" Y="-3.922533267899" Z="1.55" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.55" />
                  <Point X="0.978293714694" Y="-3.176536780252" Z="1.55" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.55" />
                  <Point X="1.081221163383" Y="-2.967072466998" Z="1.55" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.55" />
                  <Point X="1.281875960835" Y="-2.867326179368" Z="1.55" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.55" />
                  <Point X="1.507191690448" Y="-2.907562997413" Z="1.55" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.55" />
                  <Point X="2.263170898313" Y="-3.806825698108" Z="1.55" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.55" />
                  <Point X="2.738041433093" Y="-4.277460674389" Z="1.55" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.55" />
                  <Point X="2.93093747506" Y="-4.147667598255" Z="1.55" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.55" />
                  <Point X="2.573023641388" Y="-3.245008378544" Z="1.55" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.55" />
                  <Point X="2.256045572873" Y="-2.638182442426" Z="1.55" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.55" />
                  <Point X="2.26898839744" Y="-2.43632833393" Z="1.55" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.55" />
                  <Point X="2.396570044443" Y="-2.289912912527" Z="1.55" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.55" />
                  <Point X="2.590324361087" Y="-2.247402436618" Z="1.55" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.55" />
                  <Point X="3.542405218563" Y="-2.744725858887" Z="1.55" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.55" />
                  <Point X="4.133083261921" Y="-2.949939030082" Z="1.55" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.55" />
                  <Point X="4.301804487935" Y="-2.697961302153" Z="1.55" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.55" />
                  <Point X="3.662376219975" Y="-1.974955456041" Z="1.55" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.55" />
                  <Point X="3.153629477095" Y="-1.553754787591" Z="1.55" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.55" />
                  <Point X="3.098385217061" Y="-1.391765537903" Z="1.55" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.55" />
                  <Point X="3.150710848212" Y="-1.235994066061" Z="1.55" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.55" />
                  <Point X="3.288411945016" Y="-1.140022371516" Z="1.55" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.55" />
                  <Point X="4.320111075638" Y="-1.237147511655" Z="1.55" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.55" />
                  <Point X="4.939872922308" Y="-1.170389692535" Z="1.55" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.55" />
                  <Point X="5.0134616596" Y="-0.79834708072" Z="1.55" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.55" />
                  <Point X="4.254019898193" Y="-0.356410927814" Z="1.55" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.55" />
                  <Point X="3.711941914116" Y="-0.199995757355" Z="1.55" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.55" />
                  <Point X="3.633836243364" Y="-0.139806485476" Z="1.55" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.55" />
                  <Point X="3.5927345666" Y="-0.05900355159" Z="1.55" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.55" />
                  <Point X="3.588636883824" Y="0.037606979612" Z="1.55" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.55" />
                  <Point X="3.621543195037" Y="0.124142253099" Z="1.55" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.55" />
                  <Point X="3.691453500237" Y="0.188153086887" Z="1.55" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.55" />
                  <Point X="4.541947919114" Y="0.433561004955" Z="1.55" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.55" />
                  <Point X="5.022361765904" Y="0.733928586454" Z="1.55" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.55" />
                  <Point X="4.942669791251" Y="1.154460949136" Z="1.55" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.55" />
                  <Point X="4.014966667806" Y="1.294675911422" Z="1.55" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.55" />
                  <Point X="3.42646802802" Y="1.226868274631" Z="1.55" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.55" />
                  <Point X="3.341851485414" Y="1.249728673451" Z="1.55" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.55" />
                  <Point X="3.280611486515" Y="1.302104948285" Z="1.55" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.55" />
                  <Point X="3.244383108912" Y="1.380050150967" Z="1.55" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.55" />
                  <Point X="3.241970528283" Y="1.462308695524" Z="1.55" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.55" />
                  <Point X="3.277608492453" Y="1.538656949862" Z="1.55" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.55" />
                  <Point X="4.005725716726" Y="2.116320365526" Z="1.55" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.55" />
                  <Point X="4.365905962201" Y="2.58968546636" Z="1.55" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.55" />
                  <Point X="4.146347771355" Y="2.928378884458" Z="1.55" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.55" />
                  <Point X="3.090808647623" Y="2.602399260608" Z="1.55" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.55" />
                  <Point X="2.478625386154" Y="2.258641433115" Z="1.55" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.55" />
                  <Point X="2.402567070001" Y="2.248787839295" Z="1.55" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.55" />
                  <Point X="2.335522998684" Y="2.270622343624" Z="1.55" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.55" />
                  <Point X="2.280136256611" Y="2.321501861698" Z="1.55" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.55" />
                  <Point X="2.250641863464" Y="2.387191373832" Z="1.55" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.55" />
                  <Point X="2.253886509543" Y="2.460844189727" Z="1.55" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.55" />
                  <Point X="2.793226092252" Y="3.421330434332" Z="1.55" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.55" />
                  <Point X="2.982602554955" Y="4.106105384685" Z="1.55" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.55" />
                  <Point X="2.598673966999" Y="4.359232747806" Z="1.55" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.55" />
                  <Point X="2.195490571222" Y="4.575707326365" Z="1.55" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.55" />
                  <Point X="1.777701298091" Y="4.753635544382" Z="1.55" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.55" />
                  <Point X="1.262089428417" Y="4.909768926881" Z="1.55" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.55" />
                  <Point X="0.602018932247" Y="5.033513438341" Z="1.55" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.55" />
                  <Point X="0.075223106632" Y="4.635860943753" Z="1.55" />
                  <Point X="0" Y="4.355124473572" Z="1.55" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>