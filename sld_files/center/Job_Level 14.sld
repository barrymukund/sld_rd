<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#143" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1141" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="0" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000475830078" Y="0.004715820312" />
                  <Width Value="9.783896972656" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.666013183594" Y="-3.895847900391" />
                  <Point X="0.563302062988" Y="-3.512524414063" />
                  <Point X="0.557721191406" Y="-3.497141845703" />
                  <Point X="0.54236315918" Y="-3.467377197266" />
                  <Point X="0.500382659912" Y="-3.406890869141" />
                  <Point X="0.378635528564" Y="-3.231476806641" />
                  <Point X="0.356751586914" Y="-3.209021484375" />
                  <Point X="0.330496398926" Y="-3.189777587891" />
                  <Point X="0.302495330811" Y="-3.175669433594" />
                  <Point X="0.237532669067" Y="-3.155507324219" />
                  <Point X="0.049136188507" Y="-3.097036132812" />
                  <Point X="0.020976638794" Y="-3.092766601562" />
                  <Point X="-0.00866468811" Y="-3.092766601562" />
                  <Point X="-0.036823932648" Y="-3.097035888672" />
                  <Point X="-0.101786735535" Y="-3.117197753906" />
                  <Point X="-0.290183074951" Y="-3.175669189453" />
                  <Point X="-0.31818460083" Y="-3.189777587891" />
                  <Point X="-0.344439788818" Y="-3.209021484375" />
                  <Point X="-0.366323577881" Y="-3.231476806641" />
                  <Point X="-0.408304107666" Y="-3.291962890625" />
                  <Point X="-0.530051208496" Y="-3.467377197266" />
                  <Point X="-0.538189025879" Y="-3.481573730469" />
                  <Point X="-0.550990112305" Y="-3.512524414063" />
                  <Point X="-0.848860107422" Y="-4.624190429688" />
                  <Point X="-0.916584594727" Y="-4.876941894531" />
                  <Point X="-1.079340576172" Y="-4.845350097656" />
                  <Point X="-1.150933227539" Y="-4.8269296875" />
                  <Point X="-1.24641796875" Y="-4.802362304688" />
                  <Point X="-1.227326904297" Y="-4.657352539062" />
                  <Point X="-1.214963012695" Y="-4.563438476562" />
                  <Point X="-1.214201171875" Y="-4.5479296875" />
                  <Point X="-1.216508666992" Y="-4.516224121094" />
                  <Point X="-1.232028320312" Y="-4.438202148438" />
                  <Point X="-1.277036132812" Y="-4.211932617188" />
                  <Point X="-1.287938354492" Y="-4.182966308594" />
                  <Point X="-1.304010375977" Y="-4.15512890625" />
                  <Point X="-1.32364453125" Y="-4.131204101562" />
                  <Point X="-1.383453979492" Y="-4.078752685547" />
                  <Point X="-1.556905517578" Y="-3.926639404297" />
                  <Point X="-1.583188476562" Y="-3.910295410156" />
                  <Point X="-1.612885742188" Y="-3.897994384766" />
                  <Point X="-1.64302734375" Y="-3.890966308594" />
                  <Point X="-1.722407836914" Y="-3.885763427734" />
                  <Point X="-1.952616577148" Y="-3.870674804688" />
                  <Point X="-1.98341796875" Y="-3.873708496094" />
                  <Point X="-2.014466674805" Y="-3.882028076172" />
                  <Point X="-2.042657836914" Y="-3.894801513672" />
                  <Point X="-2.108801757812" Y="-3.938997558594" />
                  <Point X="-2.300624267578" Y="-4.067169677734" />
                  <Point X="-2.312785644531" Y="-4.076821533203" />
                  <Point X="-2.335102783203" Y="-4.099461914062" />
                  <Point X="-2.480149169922" Y="-4.288490234375" />
                  <Point X="-2.481282958984" Y="-4.287788085937" />
                  <Point X="-2.801708007812" Y="-4.089388427734" />
                  <Point X="-2.90084375" Y="-4.013057128906" />
                  <Point X="-3.104721923828" Y="-3.856077880859" />
                  <Point X="-2.614638427734" Y="-3.007228759766" />
                  <Point X="-2.423761230469" Y="-2.676619628906" />
                  <Point X="-2.412859130859" Y="-2.647653320312" />
                  <Point X="-2.406588134766" Y="-2.616126953125" />
                  <Point X="-2.405575439453" Y="-2.585192871094" />
                  <Point X="-2.414560058594" Y="-2.555575195312" />
                  <Point X="-2.428777099609" Y="-2.526746337891" />
                  <Point X="-2.4468046875" Y="-2.501589355469" />
                  <Point X="-2.464153320312" Y="-2.484240722656" />
                  <Point X="-2.489310302734" Y="-2.466213134766" />
                  <Point X="-2.518139160156" Y="-2.45199609375" />
                  <Point X="-2.547756835938" Y="-2.443011474609" />
                  <Point X="-2.578690917969" Y="-2.444024169922" />
                  <Point X="-2.610217285156" Y="-2.450295166016" />
                  <Point X="-2.63918359375" Y="-2.461197265625" />
                  <Point X="-3.597973876953" Y="-3.014755126953" />
                  <Point X="-3.818024658203" Y="-3.141801269531" />
                  <Point X="-3.829706298828" Y="-3.126453857422" />
                  <Point X="-4.082859375" Y="-2.793862548828" />
                  <Point X="-4.1539296875" Y="-2.674688232422" />
                  <Point X="-4.306142578125" Y="-2.419450439453" />
                  <Point X="-3.439927734375" Y="-1.754780029297" />
                  <Point X="-3.105954589844" Y="-1.498513305664" />
                  <Point X="-3.083063232422" Y="-1.475878051758" />
                  <Point X="-3.063815185547" Y="-1.445048217773" />
                  <Point X="-3.055126220703" Y="-1.422352905273" />
                  <Point X="-3.051880859375" Y="-1.412204833984" />
                  <Point X="-3.046151855469" Y="-1.390084960938" />
                  <Point X="-3.042037597656" Y="-1.358602539062" />
                  <Point X="-3.042736816406" Y="-1.33573425293" />
                  <Point X="-3.048883300781" Y="-1.313696166992" />
                  <Point X="-3.060888183594" Y="-1.284713623047" />
                  <Point X="-3.073649414062" Y="-1.262768310547" />
                  <Point X="-3.091766845703" Y="-1.244985961914" />
                  <Point X="-3.111059326172" Y="-1.230560302734" />
                  <Point X="-3.119763183594" Y="-1.224770263672" />
                  <Point X="-3.139455566406" Y="-1.213180297852" />
                  <Point X="-3.168718261719" Y="-1.201956420898" />
                  <Point X="-3.200605957031" Y="-1.195474853516" />
                  <Point X="-3.231929199219" Y="-1.194383789062" />
                  <Point X="-4.442311035156" Y="-1.353733642578" />
                  <Point X="-4.732102050781" Y="-1.391885375977" />
                  <Point X="-4.735066894531" Y="-1.380277709961" />
                  <Point X="-4.834077636719" Y="-0.992654174805" />
                  <Point X="-4.852880371094" Y="-0.861186218262" />
                  <Point X="-4.892424316406" Y="-0.584698181152" />
                  <Point X="-3.913321777344" Y="-0.322348510742" />
                  <Point X="-3.532875976562" Y="-0.220408340454" />
                  <Point X="-3.512582519531" Y="-0.212375137329" />
                  <Point X="-3.4898984375" Y="-0.200215820312" />
                  <Point X="-3.480612792969" Y="-0.194530670166" />
                  <Point X="-3.459975830078" Y="-0.180207565308" />
                  <Point X="-3.436020507812" Y="-0.158680648804" />
                  <Point X="-3.415297851562" Y="-0.12874256897" />
                  <Point X="-3.405500488281" Y="-0.106362297058" />
                  <Point X="-3.401796142578" Y="-0.09642427063" />
                  <Point X="-3.394917236328" Y="-0.074259986877" />
                  <Point X="-3.389474365234" Y="-0.045520446777" />
                  <Point X="-3.39034765625" Y="-0.01214644146" />
                  <Point X="-3.394966552734" Y="0.010317173004" />
                  <Point X="-3.397289306641" Y="0.019343845367" />
                  <Point X="-3.404168457031" Y="0.04150812912" />
                  <Point X="-3.417485107422" Y="0.070832214355" />
                  <Point X="-3.440005126953" Y="0.099788208008" />
                  <Point X="-3.458939453125" Y="0.116244720459" />
                  <Point X="-3.467091796875" Y="0.122586364746" />
                  <Point X="-3.487728759766" Y="0.136909469604" />
                  <Point X="-3.501925292969" Y="0.145047103882" />
                  <Point X="-3.532875976562" Y="0.157848342896" />
                  <Point X="-4.636196289062" Y="0.453482025146" />
                  <Point X="-4.891815917969" Y="0.521975158691" />
                  <Point X="-4.888298339844" Y="0.54574810791" />
                  <Point X="-4.82448828125" Y="0.976970825195" />
                  <Point X="-4.78663671875" Y="1.116656860352" />
                  <Point X="-4.703551269531" Y="1.423267822266" />
                  <Point X="-4.038379150391" Y="1.335696289062" />
                  <Point X="-3.765666259766" Y="1.29979284668" />
                  <Point X="-3.744985839844" Y="1.299341674805" />
                  <Point X="-3.723424072266" Y="1.301228271484" />
                  <Point X="-3.703137695312" Y="1.305263671875" />
                  <Point X="-3.687387695312" Y="1.310229492188" />
                  <Point X="-3.641711669922" Y="1.324631225586" />
                  <Point X="-3.622778320312" Y="1.332962036133" />
                  <Point X="-3.604034179688" Y="1.343784057617" />
                  <Point X="-3.587353271484" Y="1.356015136719" />
                  <Point X="-3.57371484375" Y="1.37156652832" />
                  <Point X="-3.561300292969" Y="1.389296508789" />
                  <Point X="-3.5513515625" Y="1.407430541992" />
                  <Point X="-3.545031738281" Y="1.42268762207" />
                  <Point X="-3.526704101563" Y="1.466934692383" />
                  <Point X="-3.520915527344" Y="1.486794067383" />
                  <Point X="-3.517157226562" Y="1.508109008789" />
                  <Point X="-3.5158046875" Y="1.528749145508" />
                  <Point X="-3.518951171875" Y="1.549192871094" />
                  <Point X="-3.524552978516" Y="1.570099121094" />
                  <Point X="-3.532049804688" Y="1.589377563477" />
                  <Point X="-3.539675292969" Y="1.604026123047" />
                  <Point X="-3.561789550781" Y="1.646507324219" />
                  <Point X="-3.57328125" Y="1.663705932617" />
                  <Point X="-3.587193847656" Y="1.680286376953" />
                  <Point X="-3.602135986328" Y="1.694590209961" />
                  <Point X="-4.235002929688" Y="2.180206542969" />
                  <Point X="-4.351859863281" Y="2.269874267578" />
                  <Point X="-4.329097167969" Y="2.308872558594" />
                  <Point X="-4.081152832031" Y="2.733660888672" />
                  <Point X="-3.980890625" Y="2.862533691406" />
                  <Point X="-3.750504882813" Y="3.158661865234" />
                  <Point X="-3.374149658203" Y="2.941372802734" />
                  <Point X="-3.206656982422" Y="2.844670898438" />
                  <Point X="-3.187724121094" Y="2.836340332031" />
                  <Point X="-3.167081787109" Y="2.829831787109" />
                  <Point X="-3.146794921875" Y="2.825796386719" />
                  <Point X="-3.124859619141" Y="2.823877197266" />
                  <Point X="-3.061245605469" Y="2.818311767578" />
                  <Point X="-3.040564941406" Y="2.818762939453" />
                  <Point X="-3.019106201172" Y="2.821588134766" />
                  <Point X="-2.999014648438" Y="2.826504638672" />
                  <Point X="-2.980463134766" Y="2.835653076172" />
                  <Point X="-2.962208984375" Y="2.847282226562" />
                  <Point X="-2.946077392578" Y="2.860229492188" />
                  <Point X="-2.930507568359" Y="2.875799316406" />
                  <Point X="-2.885353759766" Y="2.920952880859" />
                  <Point X="-2.872406738281" Y="2.937084228516" />
                  <Point X="-2.86077734375" Y="2.955338623047" />
                  <Point X="-2.851628662109" Y="2.973890136719" />
                  <Point X="-2.846712158203" Y="2.993981933594" />
                  <Point X="-2.843886962891" Y="3.015440673828" />
                  <Point X="-2.843435791016" Y="3.03612109375" />
                  <Point X="-2.845354980469" Y="3.058056640625" />
                  <Point X="-2.850920410156" Y="3.121670654297" />
                  <Point X="-2.854955810547" Y="3.141957763672" />
                  <Point X="-2.861464355469" Y="3.162600097656" />
                  <Point X="-2.869794921875" Y="3.181532958984" />
                  <Point X="-3.150237548828" Y="3.667273681641" />
                  <Point X="-3.183333007812" Y="3.724596435547" />
                  <Point X="-3.132461425781" Y="3.763599365234" />
                  <Point X="-2.700625488281" Y="4.094683837891" />
                  <Point X="-2.542716308594" Y="4.1824140625" />
                  <Point X="-2.167036621094" Y="4.391133789062" />
                  <Point X="-2.094469970703" Y="4.296562988281" />
                  <Point X="-2.04319519043" Y="4.229740722656" />
                  <Point X="-2.028892822266" Y="4.214800292969" />
                  <Point X="-2.012313232422" Y="4.200887695312" />
                  <Point X="-1.995112792969" Y="4.18939453125" />
                  <Point X="-1.970698852539" Y="4.176685546875" />
                  <Point X="-1.899896484375" Y="4.139827636719" />
                  <Point X="-1.88061730957" Y="4.132330566406" />
                  <Point X="-1.85971081543" Y="4.126729003906" />
                  <Point X="-1.839267822266" Y="4.123582519531" />
                  <Point X="-1.818628417969" Y="4.124935546875" />
                  <Point X="-1.797312988281" Y="4.128693847656" />
                  <Point X="-1.777453125" Y="4.134482421875" />
                  <Point X="-1.752024414062" Y="4.145015625" />
                  <Point X="-1.678279174805" Y="4.175562011719" />
                  <Point X="-1.66014465332" Y="4.185510742187" />
                  <Point X="-1.642415039062" Y="4.197925292969" />
                  <Point X="-1.626863525391" Y="4.211563964844" />
                  <Point X="-1.614632568359" Y="4.228245117188" />
                  <Point X="-1.603810791016" Y="4.246989257813" />
                  <Point X="-1.59548046875" Y="4.265921386719" />
                  <Point X="-1.587203735352" Y="4.292171386719" />
                  <Point X="-1.563201171875" Y="4.368297851562" />
                  <Point X="-1.559165649414" Y="4.388584960938" />
                  <Point X="-1.557279174805" Y="4.410146484375" />
                  <Point X="-1.55773034668" Y="4.430826660156" />
                  <Point X="-1.584201904297" Y="4.6318984375" />
                  <Point X="-1.508673461914" Y="4.65307421875" />
                  <Point X="-0.949638366699" Y="4.809808105469" />
                  <Point X="-0.758208129883" Y="4.832211914062" />
                  <Point X="-0.29471081543" Y="4.886458007812" />
                  <Point X="-0.183452407837" Y="4.471234863281" />
                  <Point X="-0.133903305054" Y="4.286315429688" />
                  <Point X="-0.121129844666" Y="4.258124023438" />
                  <Point X="-0.10327155304" Y="4.231397460938" />
                  <Point X="-0.0821145401" Y="4.20880859375" />
                  <Point X="-0.054819446564" Y="4.19421875" />
                  <Point X="-0.024381370544" Y="4.183886230469" />
                  <Point X="0.006155905724" Y="4.178844238281" />
                  <Point X="0.03669316864" Y="4.183886230469" />
                  <Point X="0.067131248474" Y="4.19421875" />
                  <Point X="0.094426475525" Y="4.20880859375" />
                  <Point X="0.115583503723" Y="4.231397460938" />
                  <Point X="0.133441802979" Y="4.258124023438" />
                  <Point X="0.146215255737" Y="4.286315429688" />
                  <Point X="0.289910827637" Y="4.822594726562" />
                  <Point X="0.307419403076" Y="4.8879375" />
                  <Point X="0.355913574219" Y="4.882858886719" />
                  <Point X="0.844041503906" Y="4.831738769531" />
                  <Point X="1.002420043945" Y="4.793501464844" />
                  <Point X="1.481025756836" Y="4.677951171875" />
                  <Point X="1.583012939453" Y="4.640959472656" />
                  <Point X="1.894646362305" Y="4.527927734375" />
                  <Point X="1.994330932617" Y="4.48130859375" />
                  <Point X="2.294576416016" Y="4.340893554688" />
                  <Point X="2.390907226562" Y="4.284770996094" />
                  <Point X="2.680978759766" Y="4.115774902344" />
                  <Point X="2.771805419922" Y="4.051183837891" />
                  <Point X="2.943260009766" Y="3.929254638672" />
                  <Point X="2.368507080078" Y="2.933753173828" />
                  <Point X="2.147581054688" Y="2.551097900391" />
                  <Point X="2.142075927734" Y="2.539931152344" />
                  <Point X="2.133076660156" Y="2.516056396484" />
                  <Point X="2.127571777344" Y="2.495470458984" />
                  <Point X="2.111606933594" Y="2.435770019531" />
                  <Point X="2.108405273438" Y="2.409135498047" />
                  <Point X="2.109214111328" Y="2.372432128906" />
                  <Point X="2.109874267578" Y="2.36315234375" />
                  <Point X="2.116099121094" Y="2.311528320312" />
                  <Point X="2.121442138672" Y="2.289604980469" />
                  <Point X="2.129708251953" Y="2.267516357422" />
                  <Point X="2.140071044922" Y="2.247471191406" />
                  <Point X="2.151085693359" Y="2.231238525391" />
                  <Point X="2.183028808594" Y="2.184162597656" />
                  <Point X="2.201256835938" Y="2.164163085938" />
                  <Point X="2.230789550781" Y="2.139848144531" />
                  <Point X="2.237831787109" Y="2.134577636719" />
                  <Point X="2.284907714844" Y="2.102634765625" />
                  <Point X="2.304952880859" Y="2.092271972656" />
                  <Point X="2.327041259766" Y="2.084006103516" />
                  <Point X="2.348964111328" Y="2.078663330078" />
                  <Point X="2.366765136719" Y="2.076516845703" />
                  <Point X="2.418388916016" Y="2.070291748047" />
                  <Point X="2.436467529297" Y="2.069845703125" />
                  <Point X="2.47320703125" Y="2.074171142578" />
                  <Point X="2.49379296875" Y="2.079676269531" />
                  <Point X="2.553493408203" Y="2.095640869141" />
                  <Point X="2.565283447266" Y="2.099638183594" />
                  <Point X="2.588533935547" Y="2.110145019531" />
                  <Point X="3.69826171875" Y="2.750846679688" />
                  <Point X="3.967326416016" Y="2.906191162109" />
                  <Point X="4.123270507813" Y="2.689464355469" />
                  <Point X="4.173907226562" Y="2.605786865234" />
                  <Point X="4.262198730469" Y="2.459884277344" />
                  <Point X="3.521653808594" Y="1.891643676758" />
                  <Point X="3.230783935547" Y="1.668451171875" />
                  <Point X="3.221425048828" Y="1.660241943359" />
                  <Point X="3.203973632812" Y="1.641627807617" />
                  <Point X="3.189157958984" Y="1.622299560547" />
                  <Point X="3.14619140625" Y="1.566246337891" />
                  <Point X="3.136605712891" Y="1.550911499023" />
                  <Point X="3.121629882812" Y="1.5170859375" />
                  <Point X="3.116111083984" Y="1.497351806641" />
                  <Point X="3.100105957031" Y="1.440121459961" />
                  <Point X="3.096652587891" Y="1.417823242188" />
                  <Point X="3.095836425781" Y="1.394252929688" />
                  <Point X="3.097739257812" Y="1.371768554688" />
                  <Point X="3.10226953125" Y="1.349811767578" />
                  <Point X="3.115408203125" Y="1.286135742188" />
                  <Point X="3.120680175781" Y="1.268977172852" />
                  <Point X="3.136282714844" Y="1.235740478516" />
                  <Point X="3.148604980469" Y="1.217011108398" />
                  <Point X="3.184340332031" Y="1.162694946289" />
                  <Point X="3.198893554688" Y="1.145450439453" />
                  <Point X="3.216137451172" Y="1.129360595703" />
                  <Point X="3.234346679688" Y="1.11603503418" />
                  <Point X="3.252203125" Y="1.105983276367" />
                  <Point X="3.303988769531" Y="1.076832763672" />
                  <Point X="3.320521240234" Y="1.069501586914" />
                  <Point X="3.356117919922" Y="1.059438598633" />
                  <Point X="3.380261230469" Y="1.056247680664" />
                  <Point X="3.450278564453" Y="1.046994018555" />
                  <Point X="3.462702148438" Y="1.046175048828" />
                  <Point X="3.488203613281" Y="1.04698449707" />
                  <Point X="4.542370605469" Y="1.185768310547" />
                  <Point X="4.776839355469" Y="1.21663671875" />
                  <Point X="4.845936035156" Y="0.932809265137" />
                  <Point X="4.861892578125" Y="0.830322631836" />
                  <Point X="4.890864746094" Y="0.644238464355" />
                  <Point X="4.049923095703" Y="0.418908874512" />
                  <Point X="3.716579833984" Y="0.329589935303" />
                  <Point X="3.704791259766" Y="0.325586608887" />
                  <Point X="3.681545410156" Y="0.315067840576" />
                  <Point X="3.657825439453" Y="0.301357055664" />
                  <Point X="3.589035400391" Y="0.261595275879" />
                  <Point X="3.574311035156" Y="0.251096221924" />
                  <Point X="3.547531005859" Y="0.22557661438" />
                  <Point X="3.533299072266" Y="0.207441757202" />
                  <Point X="3.492025146484" Y="0.154848968506" />
                  <Point X="3.480301025391" Y="0.135569152832" />
                  <Point X="3.470527099609" Y="0.114105384827" />
                  <Point X="3.463680908203" Y="0.092604423523" />
                  <Point X="3.458936767578" Y="0.067833137512" />
                  <Point X="3.445178710938" Y="-0.004005922318" />
                  <Point X="3.443482910156" Y="-0.021875295639" />
                  <Point X="3.445178710938" Y="-0.058554080963" />
                  <Point X="3.449922851562" Y="-0.083325515747" />
                  <Point X="3.463680908203" Y="-0.155164581299" />
                  <Point X="3.470527099609" Y="-0.176665542603" />
                  <Point X="3.480301025391" Y="-0.198129302979" />
                  <Point X="3.492024902344" Y="-0.217408660889" />
                  <Point X="3.506256835938" Y="-0.235543670654" />
                  <Point X="3.547530761719" Y="-0.288136474609" />
                  <Point X="3.559999023438" Y="-0.30123614502" />
                  <Point X="3.589035644531" Y="-0.324155578613" />
                  <Point X="3.612755859375" Y="-0.337866333008" />
                  <Point X="3.681545654297" Y="-0.377628143311" />
                  <Point X="3.692710205078" Y="-0.383139038086" />
                  <Point X="3.716579833984" Y="-0.392149932861" />
                  <Point X="4.683299316406" Y="-0.65118145752" />
                  <Point X="4.89147265625" Y="-0.706961486816" />
                  <Point X="4.855022460938" Y="-0.9487265625" />
                  <Point X="4.834579589844" Y="-1.038311401367" />
                  <Point X="4.801173828125" Y="-1.18469909668" />
                  <Point X="3.810966552734" Y="-1.054335693359" />
                  <Point X="3.424382080078" Y="-1.003441040039" />
                  <Point X="3.408035644531" Y="-1.002710327148" />
                  <Point X="3.374658691406" Y="-1.005508728027" />
                  <Point X="3.328104492188" Y="-1.015627502441" />
                  <Point X="3.193094482422" Y="-1.044972412109" />
                  <Point X="3.163974121094" Y="-1.056596923828" />
                  <Point X="3.136147460938" Y="-1.073489257812" />
                  <Point X="3.112397460938" Y="-1.093960083008" />
                  <Point X="3.084258300781" Y="-1.127802490234" />
                  <Point X="3.002653320312" Y="-1.225948120117" />
                  <Point X="2.987932617188" Y="-1.250330566406" />
                  <Point X="2.976589355469" Y="-1.277715698242" />
                  <Point X="2.969757568359" Y="-1.305365478516" />
                  <Point X="2.965724609375" Y="-1.349192871094" />
                  <Point X="2.954028564453" Y="-1.476295776367" />
                  <Point X="2.956347412109" Y="-1.507564819336" />
                  <Point X="2.964079101562" Y="-1.539185791016" />
                  <Point X="2.976450439453" Y="-1.567996582031" />
                  <Point X="3.002214111328" Y="-1.6080703125" />
                  <Point X="3.076930664062" Y="-1.724287109375" />
                  <Point X="3.086932373047" Y="-1.73723815918" />
                  <Point X="3.110628417969" Y="-1.760909179688" />
                  <Point X="4.007749755859" Y="-2.449294921875" />
                  <Point X="4.213122070312" Y="-2.6068828125" />
                  <Point X="4.124809570313" Y="-2.749786865234" />
                  <Point X="4.08253125" Y="-2.809857910156" />
                  <Point X="4.028981445312" Y="-2.885945068359" />
                  <Point X="3.145260009766" Y="-2.375728271484" />
                  <Point X="2.800954589844" Y="-2.176943115234" />
                  <Point X="2.786131591797" Y="-2.170012451172" />
                  <Point X="2.754224609375" Y="-2.159825195312" />
                  <Point X="2.698817871094" Y="-2.149818847656" />
                  <Point X="2.538134277344" Y="-2.120799560547" />
                  <Point X="2.506781982422" Y="-2.120395263672" />
                  <Point X="2.474609130859" Y="-2.125353027344" />
                  <Point X="2.444833496094" Y="-2.135176757812" />
                  <Point X="2.398803955078" Y="-2.159401611328" />
                  <Point X="2.265315429688" Y="-2.229655761719" />
                  <Point X="2.242384033203" Y="-2.246549072266" />
                  <Point X="2.221424316406" Y="-2.267509033203" />
                  <Point X="2.204531982422" Y="-2.290439208984" />
                  <Point X="2.180306884766" Y="-2.33646875" />
                  <Point X="2.110052978516" Y="-2.469957275391" />
                  <Point X="2.100229003906" Y="-2.499733398438" />
                  <Point X="2.095271240234" Y="-2.531906005859" />
                  <Point X="2.095675537109" Y="-2.563258300781" />
                  <Point X="2.105681884766" Y="-2.618665039062" />
                  <Point X="2.134701171875" Y="-2.779348632812" />
                  <Point X="2.138985595703" Y="-2.795141845703" />
                  <Point X="2.151819091797" Y="-2.826078613281" />
                  <Point X="2.728309814453" Y="-3.824589599609" />
                  <Point X="2.861283203125" Y="-4.054906738281" />
                  <Point X="2.781853027344" Y="-4.111641601562" />
                  <Point X="2.734581298828" Y="-4.142240234375" />
                  <Point X="2.701765136719" Y="-4.163481445312" />
                  <Point X="2.021282958984" Y="-3.276660400391" />
                  <Point X="1.758546142578" Y="-2.934255126953" />
                  <Point X="1.747503540039" Y="-2.922178955078" />
                  <Point X="1.721924072266" Y="-2.900557373047" />
                  <Point X="1.667277954102" Y="-2.865425048828" />
                  <Point X="1.50880078125" Y="-2.763538574219" />
                  <Point X="1.479989746094" Y="-2.751167236328" />
                  <Point X="1.448368774414" Y="-2.743435546875" />
                  <Point X="1.417099487305" Y="-2.741116699219" />
                  <Point X="1.357334594727" Y="-2.746616455078" />
                  <Point X="1.184012573242" Y="-2.762565673828" />
                  <Point X="1.15636315918" Y="-2.769397460938" />
                  <Point X="1.128978027344" Y="-2.780740722656" />
                  <Point X="1.104595947266" Y="-2.795461181641" />
                  <Point X="1.058447143555" Y="-2.833832275391" />
                  <Point X="0.924612121582" Y="-2.945111572266" />
                  <Point X="0.904141052246" Y="-2.968861816406" />
                  <Point X="0.887248779297" Y="-2.996688476562" />
                  <Point X="0.875624206543" Y="-3.025809082031" />
                  <Point X="0.86182598877" Y="-3.089291992188" />
                  <Point X="0.821809936523" Y="-3.273396972656" />
                  <Point X="0.819724487305" Y="-3.289627441406" />
                  <Point X="0.819742248535" Y="-3.323120117188" />
                  <Point X="0.98311517334" Y="-4.564061523438" />
                  <Point X="1.022065246582" Y="-4.859915039062" />
                  <Point X="0.975686767578" Y="-4.870081054688" />
                  <Point X="0.932007629395" Y="-4.878016601562" />
                  <Point X="0.929315612793" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058433837891" Y="-4.752635253906" />
                  <Point X="-1.127261230469" Y="-4.734926269531" />
                  <Point X="-1.14124621582" Y="-4.731328125" />
                  <Point X="-1.133139648438" Y="-4.669752441406" />
                  <Point X="-1.120775756836" Y="-4.575838378906" />
                  <Point X="-1.120077392578" Y="-4.568099609375" />
                  <Point X="-1.119451660156" Y="-4.541033691406" />
                  <Point X="-1.121759277344" Y="-4.509328125" />
                  <Point X="-1.123334106445" Y="-4.497690429688" />
                  <Point X="-1.138853637695" Y="-4.419668457031" />
                  <Point X="-1.183861572266" Y="-4.193398925781" />
                  <Point X="-1.18812512207" Y="-4.17846875" />
                  <Point X="-1.19902734375" Y="-4.149502441406" />
                  <Point X="-1.205666137695" Y="-4.135466308594" />
                  <Point X="-1.221738037109" Y="-4.10762890625" />
                  <Point X="-1.230573852539" Y="-4.094862304688" />
                  <Point X="-1.250207885742" Y="-4.0709375" />
                  <Point X="-1.261006713867" Y="-4.059779296875" />
                  <Point X="-1.320816162109" Y="-4.007327880859" />
                  <Point X="-1.494267700195" Y="-3.855214599609" />
                  <Point X="-1.506738647461" Y="-3.845965576172" />
                  <Point X="-1.533021606445" Y="-3.829621582031" />
                  <Point X="-1.546833496094" Y="-3.822526855469" />
                  <Point X="-1.576530761719" Y="-3.810225830078" />
                  <Point X="-1.591313354492" Y="-3.805476074219" />
                  <Point X="-1.621455078125" Y="-3.798447998047" />
                  <Point X="-1.636814208984" Y="-3.796169677734" />
                  <Point X="-1.716194580078" Y="-3.790966796875" />
                  <Point X="-1.946403198242" Y="-3.775878173828" />
                  <Point X="-1.961928222656" Y="-3.776132324219" />
                  <Point X="-1.992729614258" Y="-3.779166015625" />
                  <Point X="-2.008006103516" Y="-3.781945556641" />
                  <Point X="-2.03905480957" Y="-3.790265136719" />
                  <Point X="-2.053674316406" Y="-3.795496337891" />
                  <Point X="-2.081865478516" Y="-3.808269775391" />
                  <Point X="-2.095437011719" Y="-3.815812011719" />
                  <Point X="-2.161581054687" Y="-3.860008056641" />
                  <Point X="-2.353403564453" Y="-3.988180175781" />
                  <Point X="-2.359681640625" Y="-3.992757080078" />
                  <Point X="-2.380442138672" Y="-4.010131103516" />
                  <Point X="-2.402759277344" Y="-4.032771484375" />
                  <Point X="-2.410471435547" Y="-4.041629638672" />
                  <Point X="-2.503202880859" Y="-4.162479492188" />
                  <Point X="-2.747585693359" Y="-4.011163330078" />
                  <Point X="-2.842886474609" Y="-3.937784667969" />
                  <Point X="-2.980862792969" Y="-3.831547607422" />
                  <Point X="-2.532365966797" Y="-3.054728759766" />
                  <Point X="-2.341488769531" Y="-2.724119628906" />
                  <Point X="-2.334850097656" Y="-2.710083251953" />
                  <Point X="-2.323947998047" Y="-2.681116943359" />
                  <Point X="-2.319684570312" Y="-2.666187011719" />
                  <Point X="-2.313413574219" Y="-2.634660644531" />
                  <Point X="-2.311638916016" Y="-2.619235351562" />
                  <Point X="-2.310626220703" Y="-2.588301269531" />
                  <Point X="-2.314666259766" Y="-2.557615234375" />
                  <Point X="-2.323650878906" Y="-2.527997558594" />
                  <Point X="-2.329357421875" Y="-2.513557128906" />
                  <Point X="-2.343574462891" Y="-2.484728271484" />
                  <Point X="-2.351557128906" Y="-2.47141015625" />
                  <Point X="-2.369584716797" Y="-2.446253173828" />
                  <Point X="-2.379629638672" Y="-2.434414306641" />
                  <Point X="-2.396978271484" Y="-2.417065673828" />
                  <Point X="-2.408817138672" Y="-2.407020751953" />
                  <Point X="-2.433974121094" Y="-2.388993164062" />
                  <Point X="-2.447292236328" Y="-2.381010498047" />
                  <Point X="-2.47612109375" Y="-2.366793457031" />
                  <Point X="-2.490561523438" Y="-2.361086914062" />
                  <Point X="-2.520179199219" Y="-2.352102294922" />
                  <Point X="-2.550865234375" Y="-2.348062255859" />
                  <Point X="-2.581799316406" Y="-2.349074951172" />
                  <Point X="-2.597224609375" Y="-2.350849609375" />
                  <Point X="-2.628750976562" Y="-2.357120605469" />
                  <Point X="-2.643680908203" Y="-2.361384033203" />
                  <Point X="-2.672647216797" Y="-2.372286132812" />
                  <Point X="-2.68668359375" Y="-2.378924804688" />
                  <Point X="-3.645473876953" Y="-2.932482666016" />
                  <Point X="-3.793089599609" Y="-3.017708496094" />
                  <Point X="-4.004014892578" Y="-2.740595947266" />
                  <Point X="-4.072336914063" Y="-2.626030029297" />
                  <Point X="-4.181265136719" Y="-2.443373779297" />
                  <Point X="-3.382095458984" Y="-1.83014855957" />
                  <Point X="-3.048122314453" Y="-1.573881835938" />
                  <Point X="-3.039158447266" Y="-1.566065307617" />
                  <Point X="-3.016267089844" Y="-1.543430053711" />
                  <Point X="-3.002479248047" Y="-1.526189208984" />
                  <Point X="-2.983231201172" Y="-1.49535925293" />
                  <Point X="-2.975094970703" Y="-1.479015014648" />
                  <Point X="-2.966406005859" Y="-1.456319702148" />
                  <Point X="-2.959915283203" Y="-1.436023681641" />
                  <Point X="-2.954186279297" Y="-1.413903808594" />
                  <Point X="-2.951952880859" Y="-1.402395263672" />
                  <Point X="-2.947838623047" Y="-1.370912841797" />
                  <Point X="-2.94708203125" Y="-1.35569921875" />
                  <Point X="-2.94778125" Y="-1.332830932617" />
                  <Point X="-2.951229248047" Y="-1.310212524414" />
                  <Point X="-2.957375732422" Y="-1.288174316406" />
                  <Point X="-2.961114746094" Y="-1.277341430664" />
                  <Point X="-2.973119628906" Y="-1.248358886719" />
                  <Point X="-2.978763671875" Y="-1.236958129883" />
                  <Point X="-2.991524902344" Y="-1.215012817383" />
                  <Point X="-3.007104248047" Y="-1.194969116211" />
                  <Point X="-3.025221679688" Y="-1.177186767578" />
                  <Point X="-3.034877197266" Y="-1.168903320312" />
                  <Point X="-3.054169677734" Y="-1.154477661133" />
                  <Point X="-3.071577148438" Y="-1.142897827148" />
                  <Point X="-3.09126953125" Y="-1.131307739258" />
                  <Point X="-3.105434326172" Y="-1.124480957031" />
                  <Point X="-3.134697021484" Y="-1.113257080078" />
                  <Point X="-3.149795410156" Y="-1.108860107422" />
                  <Point X="-3.181683105469" Y="-1.102378540039" />
                  <Point X="-3.197298828125" Y="-1.100532470703" />
                  <Point X="-3.228622070312" Y="-1.09944140625" />
                  <Point X="-3.244329101562" Y="-1.100196533203" />
                  <Point X="-4.4547109375" Y="-1.259546386719" />
                  <Point X="-4.660920898438" Y="-1.286694458008" />
                  <Point X="-4.74076171875" Y="-0.97411920166" />
                  <Point X="-4.758837402344" Y="-0.847736022949" />
                  <Point X="-4.786452148438" Y="-0.654654296875" />
                  <Point X="-3.888733886719" Y="-0.414111450195" />
                  <Point X="-3.508288085938" Y="-0.312171295166" />
                  <Point X="-3.497909912109" Y="-0.308739440918" />
                  <Point X="-3.477616455078" Y="-0.300706237793" />
                  <Point X="-3.467700927734" Y="-0.296104705811" />
                  <Point X="-3.445016845703" Y="-0.28394543457" />
                  <Point X="-3.426445800781" Y="-0.272575195313" />
                  <Point X="-3.405808837891" Y="-0.258252105713" />
                  <Point X="-3.396477783203" Y="-0.250868652344" />
                  <Point X="-3.372522460938" Y="-0.229341827393" />
                  <Point X="-3.357907714844" Y="-0.21274911499" />
                  <Point X="-3.337185058594" Y="-0.182810958862" />
                  <Point X="-3.328271484375" Y="-0.166839935303" />
                  <Point X="-3.318474121094" Y="-0.144459716797" />
                  <Point X="-3.311065429688" Y="-0.124583473206" />
                  <Point X="-3.304186523438" Y="-0.102419197083" />
                  <Point X="-3.301576416016" Y="-0.091937416077" />
                  <Point X="-3.296133544922" Y="-0.063197906494" />
                  <Point X="-3.294506835938" Y="-0.043035423279" />
                  <Point X="-3.295380126953" Y="-0.009661407471" />
                  <Point X="-3.297294433594" Y="0.006986885071" />
                  <Point X="-3.301913330078" Y="0.029450494766" />
                  <Point X="-3.306558837891" Y="0.047503868103" />
                  <Point X="-3.313437988281" Y="0.069668151855" />
                  <Point X="-3.317669677734" Y="0.080788856506" />
                  <Point X="-3.330986328125" Y="0.110113052368" />
                  <Point X="-3.342495117188" Y="0.129154464722" />
                  <Point X="-3.365015136719" Y="0.158110519409" />
                  <Point X="-3.377685546875" Y="0.171490890503" />
                  <Point X="-3.396619873047" Y="0.187947463989" />
                  <Point X="-3.412924804688" Y="0.200630935669" />
                  <Point X="-3.433561767578" Y="0.214954025269" />
                  <Point X="-3.440484863281" Y="0.219329193115" />
                  <Point X="-3.465616210938" Y="0.232834701538" />
                  <Point X="-3.496566894531" Y="0.245635894775" />
                  <Point X="-3.508288085938" Y="0.249611251831" />
                  <Point X="-4.611608398438" Y="0.545244934082" />
                  <Point X="-4.7854453125" Y="0.591824523926" />
                  <Point X="-4.731330566406" Y="0.957528137207" />
                  <Point X="-4.694943359375" Y="1.091810180664" />
                  <Point X="-4.633586425781" Y="1.318237060547" />
                  <Point X="-4.050779052734" Y="1.241509155273" />
                  <Point X="-3.778066162109" Y="1.20560546875" />
                  <Point X="-3.76773828125" Y="1.204815429688" />
                  <Point X="-3.747057861328" Y="1.204364257812" />
                  <Point X="-3.736705322266" Y="1.20470324707" />
                  <Point X="-3.715143554688" Y="1.20658984375" />
                  <Point X="-3.704889648438" Y="1.208053833008" />
                  <Point X="-3.684603271484" Y="1.212089233398" />
                  <Point X="-3.658821289063" Y="1.219626220703" />
                  <Point X="-3.613145263672" Y="1.234027832031" />
                  <Point X="-3.603450927734" Y="1.237676513672" />
                  <Point X="-3.584517578125" Y="1.246007324219" />
                  <Point X="-3.575278076172" Y="1.250689819336" />
                  <Point X="-3.556533935547" Y="1.26151184082" />
                  <Point X="-3.547859375" Y="1.267172119141" />
                  <Point X="-3.531178466797" Y="1.279403198242" />
                  <Point X="-3.515928955078" Y="1.293376708984" />
                  <Point X="-3.502290527344" Y="1.308928222656" />
                  <Point X="-3.495895263672" Y="1.317077148438" />
                  <Point X="-3.483480712891" Y="1.334807128906" />
                  <Point X="-3.478011474609" Y="1.343602539062" />
                  <Point X="-3.468062744141" Y="1.361736450195" />
                  <Point X="-3.457263427734" Y="1.38633203125" />
                  <Point X="-3.438935791016" Y="1.430579101562" />
                  <Point X="-3.435499511719" Y="1.440350708008" />
                  <Point X="-3.4297109375" Y="1.460209960938" />
                  <Point X="-3.427358642578" Y="1.470297851562" />
                  <Point X="-3.423600341797" Y="1.491612792969" />
                  <Point X="-3.422360595703" Y="1.501896972656" />
                  <Point X="-3.421008056641" Y="1.522536987305" />
                  <Point X="-3.42191015625" Y="1.543200439453" />
                  <Point X="-3.425056640625" Y="1.563644165039" />
                  <Point X="-3.427188232422" Y="1.573780639648" />
                  <Point X="-3.432790039062" Y="1.594686889648" />
                  <Point X="-3.436011962891" Y="1.604530273438" />
                  <Point X="-3.443508789062" Y="1.62380859375" />
                  <Point X="-3.455409179688" Y="1.647891967773" />
                  <Point X="-3.4775234375" Y="1.690373168945" />
                  <Point X="-3.482799804688" Y="1.699286376953" />
                  <Point X="-3.494291503906" Y="1.716484985352" />
                  <Point X="-3.500506835938" Y="1.724770629883" />
                  <Point X="-3.514419433594" Y="1.741351074219" />
                  <Point X="-3.521500488281" Y="1.748911254883" />
                  <Point X="-3.536442626953" Y="1.763215087891" />
                  <Point X="-3.544303710938" Y="1.769958862305" />
                  <Point X="-4.177170410156" Y="2.255575195312" />
                  <Point X="-4.227614257812" Y="2.294282470703" />
                  <Point X="-4.002292724609" Y="2.680312988281" />
                  <Point X="-3.905909912109" Y="2.80419921875" />
                  <Point X="-3.726338623047" Y="3.035012695312" />
                  <Point X="-3.421649658203" Y="2.859100341797" />
                  <Point X="-3.254156982422" Y="2.7623984375" />
                  <Point X="-3.244917480469" Y="2.757716064453" />
                  <Point X="-3.225984619141" Y="2.749385498047" />
                  <Point X="-3.216291259766" Y="2.745737304688" />
                  <Point X="-3.195648925781" Y="2.739228759766" />
                  <Point X="-3.185615722656" Y="2.736657226562" />
                  <Point X="-3.165328857422" Y="2.732621826172" />
                  <Point X="-3.155075195312" Y="2.731157958984" />
                  <Point X="-3.133139892578" Y="2.729238769531" />
                  <Point X="-3.069525878906" Y="2.723673339844" />
                  <Point X="-3.059173583984" Y="2.723334472656" />
                  <Point X="-3.038492919922" Y="2.723785644531" />
                  <Point X="-3.028164550781" Y="2.724575683594" />
                  <Point X="-3.006705810547" Y="2.727400878906" />
                  <Point X="-2.996525390625" Y="2.729310791016" />
                  <Point X="-2.976433837891" Y="2.734227294922" />
                  <Point X="-2.956997802734" Y="2.741301513672" />
                  <Point X="-2.938446289062" Y="2.750449951172" />
                  <Point X="-2.929419677734" Y="2.755530761719" />
                  <Point X="-2.911165527344" Y="2.767159912109" />
                  <Point X="-2.902745361328" Y="2.773193847656" />
                  <Point X="-2.886613769531" Y="2.786141113281" />
                  <Point X="-2.87890234375" Y="2.793054443359" />
                  <Point X="-2.863332519531" Y="2.808624267578" />
                  <Point X="-2.818178710938" Y="2.853777832031" />
                  <Point X="-2.811265380859" Y="2.861489501953" />
                  <Point X="-2.798318359375" Y="2.877620849609" />
                  <Point X="-2.792284667969" Y="2.886040527344" />
                  <Point X="-2.780655273438" Y="2.904294921875" />
                  <Point X="-2.775574707031" Y="2.913320800781" />
                  <Point X="-2.766426025391" Y="2.931872314453" />
                  <Point X="-2.759351318359" Y="2.951309570312" />
                  <Point X="-2.754434814453" Y="2.971401367188" />
                  <Point X="-2.752524902344" Y="2.981581542969" />
                  <Point X="-2.749699707031" Y="3.003040283203" />
                  <Point X="-2.748909667969" Y="3.013368652344" />
                  <Point X="-2.748458496094" Y="3.034049072266" />
                  <Point X="-2.748797363281" Y="3.044401123047" />
                  <Point X="-2.750716552734" Y="3.066336669922" />
                  <Point X="-2.756281982422" Y="3.129950683594" />
                  <Point X="-2.757745849609" Y="3.140204345703" />
                  <Point X="-2.76178125" Y="3.160491455078" />
                  <Point X="-2.764352783203" Y="3.170524902344" />
                  <Point X="-2.770861328125" Y="3.191167236328" />
                  <Point X="-2.774509521484" Y="3.200860595703" />
                  <Point X="-2.782840087891" Y="3.219793457031" />
                  <Point X="-2.787522460938" Y="3.229032958984" />
                  <Point X="-3.059387207031" Y="3.699916503906" />
                  <Point X="-2.648374023438" Y="4.015036621094" />
                  <Point X="-2.496579101562" Y="4.099369628906" />
                  <Point X="-2.192524902344" Y="4.268295898437" />
                  <Point X="-2.169838623047" Y="4.23873046875" />
                  <Point X="-2.118563720703" Y="4.171908203125" />
                  <Point X="-2.111819824219" Y="4.164046875" />
                  <Point X="-2.097517333984" Y="4.149106445313" />
                  <Point X="-2.089959472656" Y="4.14202734375" />
                  <Point X="-2.073379882812" Y="4.128114746094" />
                  <Point X="-2.065093017578" Y="4.1218984375" />
                  <Point X="-2.047892578125" Y="4.110405273438" />
                  <Point X="-2.038978637695" Y="4.105128417969" />
                  <Point X="-2.014564697266" Y="4.092419433594" />
                  <Point X="-1.943762329102" Y="4.055561523438" />
                  <Point X="-1.934327392578" Y="4.051286621094" />
                  <Point X="-1.915048339844" Y="4.043789550781" />
                  <Point X="-1.905203857422" Y="4.040567382812" />
                  <Point X="-1.884297363281" Y="4.034965820313" />
                  <Point X="-1.874162597656" Y="4.032834716797" />
                  <Point X="-1.853719604492" Y="4.029688232422" />
                  <Point X="-1.833053344727" Y="4.028785888672" />
                  <Point X="-1.81241394043" Y="4.030138916016" />
                  <Point X="-1.802132568359" Y="4.031378662109" />
                  <Point X="-1.780817138672" Y="4.035136962891" />
                  <Point X="-1.770729492188" Y="4.037489013672" />
                  <Point X="-1.750869628906" Y="4.043277587891" />
                  <Point X="-1.741097412109" Y="4.046714111328" />
                  <Point X="-1.715668701172" Y="4.057247314453" />
                  <Point X="-1.641923461914" Y="4.087793701172" />
                  <Point X="-1.6325859375" Y="4.092272460938" />
                  <Point X="-1.614451416016" Y="4.102221191406" />
                  <Point X="-1.605654418945" Y="4.10769140625" />
                  <Point X="-1.587924804688" Y="4.120105957031" />
                  <Point X="-1.579776245117" Y="4.126501464844" />
                  <Point X="-1.564224853516" Y="4.140140136719" />
                  <Point X="-1.550250976562" Y="4.155390136719" />
                  <Point X="-1.538020019531" Y="4.172071289062" />
                  <Point X="-1.532359863281" Y="4.180745605469" />
                  <Point X="-1.521538085938" Y="4.199489746094" />
                  <Point X="-1.516856201172" Y="4.208728515625" />
                  <Point X="-1.508525878906" Y="4.227660644531" />
                  <Point X="-1.504877441406" Y="4.237354003906" />
                  <Point X="-1.496600830078" Y="4.263604003906" />
                  <Point X="-1.472598144531" Y="4.33973046875" />
                  <Point X="-1.470026733398" Y="4.349763671875" />
                  <Point X="-1.465991088867" Y="4.37005078125" />
                  <Point X="-1.46452722168" Y="4.3803046875" />
                  <Point X="-1.46264074707" Y="4.401866210938" />
                  <Point X="-1.462301757812" Y="4.41221875" />
                  <Point X="-1.462752929688" Y="4.432898925781" />
                  <Point X="-1.46354309082" Y="4.4432265625" />
                  <Point X="-1.479266235352" Y="4.56265625" />
                  <Point X="-0.931181640625" Y="4.716319824219" />
                  <Point X="-0.747165283203" Y="4.737855957031" />
                  <Point X="-0.365222076416" Y="4.782557128906" />
                  <Point X="-0.275215332031" Y="4.446646972656" />
                  <Point X="-0.225666244507" Y="4.261727539062" />
                  <Point X="-0.220435317993" Y="4.247107910156" />
                  <Point X="-0.207661773682" Y="4.218916503906" />
                  <Point X="-0.200119155884" Y="4.205344726562" />
                  <Point X="-0.182260925293" Y="4.178618164062" />
                  <Point X="-0.172608306885" Y="4.166456054688" />
                  <Point X="-0.151451248169" Y="4.1438671875" />
                  <Point X="-0.126898002625" Y="4.125026367188" />
                  <Point X="-0.099602806091" Y="4.110436523438" />
                  <Point X="-0.085356712341" Y="4.104260742188" />
                  <Point X="-0.054918586731" Y="4.093927978516" />
                  <Point X="-0.039857292175" Y="4.090155273438" />
                  <Point X="-0.009319890976" Y="4.08511328125" />
                  <Point X="0.021631721497" Y="4.08511328125" />
                  <Point X="0.052168972015" Y="4.090155273438" />
                  <Point X="0.067230415344" Y="4.093927978516" />
                  <Point X="0.097668540955" Y="4.104260742188" />
                  <Point X="0.111914489746" Y="4.110436523438" />
                  <Point X="0.139209686279" Y="4.125026367188" />
                  <Point X="0.16376322937" Y="4.143866699219" />
                  <Point X="0.184920150757" Y="4.166455566406" />
                  <Point X="0.194572906494" Y="4.178617675781" />
                  <Point X="0.212431137085" Y="4.205344238281" />
                  <Point X="0.219973754883" Y="4.218916503906" />
                  <Point X="0.232747146606" Y="4.247107910156" />
                  <Point X="0.237978225708" Y="4.261727539062" />
                  <Point X="0.378190338135" Y="4.785006347656" />
                  <Point X="0.827877075195" Y="4.737912109375" />
                  <Point X="0.980124755859" Y="4.701154785156" />
                  <Point X="1.453594482422" Y="4.586844726562" />
                  <Point X="1.550620483398" Y="4.55165234375" />
                  <Point X="1.858258422852" Y="4.440069824219" />
                  <Point X="1.954086181641" Y="4.395254394531" />
                  <Point X="2.250453125" Y="4.256653320312" />
                  <Point X="2.343084228516" Y="4.202686035156" />
                  <Point X="2.62943359375" Y="4.035858154297" />
                  <Point X="2.716748779297" Y="3.973764404297" />
                  <Point X="2.817779785156" Y="3.901916748047" />
                  <Point X="2.286234619141" Y="2.981253173828" />
                  <Point X="2.06530859375" Y="2.598597900391" />
                  <Point X="2.062373046875" Y="2.593104980469" />
                  <Point X="2.053181396484" Y="2.573438720703" />
                  <Point X="2.044182128906" Y="2.549563964844" />
                  <Point X="2.041301391602" Y="2.540597900391" />
                  <Point X="2.035796630859" Y="2.520011962891" />
                  <Point X="2.019831665039" Y="2.460311523438" />
                  <Point X="2.017285888672" Y="2.447108154297" />
                  <Point X="2.014084228516" Y="2.420473632812" />
                  <Point X="2.013428344727" Y="2.407042480469" />
                  <Point X="2.014237182617" Y="2.370339111328" />
                  <Point X="2.015557495117" Y="2.351779541016" />
                  <Point X="2.021782348633" Y="2.300155517578" />
                  <Point X="2.02380065918" Y="2.289033935547" />
                  <Point X="2.029143676758" Y="2.267110595703" />
                  <Point X="2.032468261719" Y="2.256308837891" />
                  <Point X="2.040734375" Y="2.234220214844" />
                  <Point X="2.045318359375" Y="2.223889160156" />
                  <Point X="2.055681152344" Y="2.203843994141" />
                  <Point X="2.072474609375" Y="2.177897216797" />
                  <Point X="2.104417724609" Y="2.130821289063" />
                  <Point X="2.112816162109" Y="2.120168945312" />
                  <Point X="2.131044189453" Y="2.100169433594" />
                  <Point X="2.140873779297" Y="2.090822265625" />
                  <Point X="2.170406494141" Y="2.066507324219" />
                  <Point X="2.184490966797" Y="2.055966308594" />
                  <Point X="2.231566894531" Y="2.0240234375" />
                  <Point X="2.241280517578" Y="2.018244873047" />
                  <Point X="2.261325683594" Y="2.007882080078" />
                  <Point X="2.271657226562" Y="2.003297851562" />
                  <Point X="2.293745605469" Y="1.995031982422" />
                  <Point X="2.304547363281" Y="1.991707519531" />
                  <Point X="2.326470214844" Y="1.986364746094" />
                  <Point X="2.355392333984" Y="1.982200073242" />
                  <Point X="2.407016113281" Y="1.975974975586" />
                  <Point X="2.416045654297" Y="1.975320800781" />
                  <Point X="2.447575439453" Y="1.975497314453" />
                  <Point X="2.484314941406" Y="1.979822631836" />
                  <Point X="2.497749755859" Y="1.982396118164" />
                  <Point X="2.518335693359" Y="1.987901245117" />
                  <Point X="2.578036132812" Y="2.003865844727" />
                  <Point X="2.583996826172" Y="2.005671264648" />
                  <Point X="2.604404541016" Y="2.013067260742" />
                  <Point X="2.627655029297" Y="2.02357409668" />
                  <Point X="2.636033935547" Y="2.027872558594" />
                  <Point X="3.74576171875" Y="2.66857421875" />
                  <Point X="3.940404785156" Y="2.780951416016" />
                  <Point X="4.043949462891" Y="2.637048095703" />
                  <Point X="4.092630371094" Y="2.556602783203" />
                  <Point X="4.136884277344" Y="2.483472412109" />
                  <Point X="3.463821533203" Y="1.967012329102" />
                  <Point X="3.172951660156" Y="1.743819702148" />
                  <Point X="3.168138671875" Y="1.739869506836" />
                  <Point X="3.152120361328" Y="1.725217651367" />
                  <Point X="3.134668945312" Y="1.706603515625" />
                  <Point X="3.128575927734" Y="1.699422241211" />
                  <Point X="3.113760253906" Y="1.680093994141" />
                  <Point X="3.070793701172" Y="1.624040893555" />
                  <Point X="3.065635009766" Y="1.6166015625" />
                  <Point X="3.049738769531" Y="1.589370727539" />
                  <Point X="3.034762939453" Y="1.555545166016" />
                  <Point X="3.030140136719" Y="1.54267175293" />
                  <Point X="3.024621337891" Y="1.52293762207" />
                  <Point X="3.008616210938" Y="1.46570715332" />
                  <Point X="3.006225097656" Y="1.454661010742" />
                  <Point X="3.002771728516" Y="1.432362792969" />
                  <Point X="3.001709472656" Y="1.421110839844" />
                  <Point X="3.000893310547" Y="1.397540527344" />
                  <Point X="3.001174804688" Y="1.386241821289" />
                  <Point X="3.003077636719" Y="1.363757446289" />
                  <Point X="3.004698974609" Y="1.352571899414" />
                  <Point X="3.009229248047" Y="1.330615112305" />
                  <Point X="3.022367919922" Y="1.266939086914" />
                  <Point X="3.024597900391" Y="1.25823425293" />
                  <Point X="3.034684326172" Y="1.228607543945" />
                  <Point X="3.050286865234" Y="1.195370849609" />
                  <Point X="3.056918701172" Y="1.183526000977" />
                  <Point X="3.069240966797" Y="1.164796630859" />
                  <Point X="3.104976318359" Y="1.11048046875" />
                  <Point X="3.111739257813" Y="1.101424438477" />
                  <Point X="3.126292480469" Y="1.08417980957" />
                  <Point X="3.134083007812" Y="1.075991210938" />
                  <Point X="3.151326904297" Y="1.059901367188" />
                  <Point X="3.160034179688" Y="1.052696166992" />
                  <Point X="3.178243408203" Y="1.039370605469" />
                  <Point X="3.187745361328" Y="1.033250244141" />
                  <Point X="3.205601806641" Y="1.023198547363" />
                  <Point X="3.257387451172" Y="0.994047973633" />
                  <Point X="3.265478271484" Y="0.989988342285" />
                  <Point X="3.294677978516" Y="0.978084228516" />
                  <Point X="3.330274658203" Y="0.968021240234" />
                  <Point X="3.343670410156" Y="0.965257629395" />
                  <Point X="3.367813720703" Y="0.962066650391" />
                  <Point X="3.437831054688" Y="0.952813110352" />
                  <Point X="3.444029785156" Y="0.952199768066" />
                  <Point X="3.465716064453" Y="0.951222839355" />
                  <Point X="3.491217529297" Y="0.952032226562" />
                  <Point X="3.500603515625" Y="0.952797241211" />
                  <Point X="4.554770507812" Y="1.091580932617" />
                  <Point X="4.704703613281" Y="1.11132019043" />
                  <Point X="4.75268359375" Y="0.914233642578" />
                  <Point X="4.7680234375" Y="0.815707763672" />
                  <Point X="4.78387109375" Y="0.713920837402" />
                  <Point X="4.025335205078" Y="0.510671844482" />
                  <Point X="3.691991943359" Y="0.421352905273" />
                  <Point X="3.686031738281" Y="0.419544464111" />
                  <Point X="3.665626708984" Y="0.412138000488" />
                  <Point X="3.642380859375" Y="0.40161920166" />
                  <Point X="3.634003662109" Y="0.397316131592" />
                  <Point X="3.610283691406" Y="0.38360534668" />
                  <Point X="3.541493652344" Y="0.343843536377" />
                  <Point X="3.533881591797" Y="0.338945526123" />
                  <Point X="3.508773925781" Y="0.319870361328" />
                  <Point X="3.481993896484" Y="0.294350769043" />
                  <Point X="3.472796875" Y="0.2842265625" />
                  <Point X="3.458564941406" Y="0.266091766357" />
                  <Point X="3.417291015625" Y="0.213499008179" />
                  <Point X="3.410854980469" Y="0.204208892822" />
                  <Point X="3.399130859375" Y="0.184929077148" />
                  <Point X="3.393843017578" Y="0.17493939209" />
                  <Point X="3.384069091797" Y="0.153475570679" />
                  <Point X="3.380005126953" Y="0.142928695679" />
                  <Point X="3.373158935547" Y="0.121427864075" />
                  <Point X="3.370376708984" Y="0.110473907471" />
                  <Point X="3.365632568359" Y="0.085702629089" />
                  <Point X="3.351874511719" Y="0.013863518715" />
                  <Point X="3.350603515625" Y="0.004969180107" />
                  <Point X="3.348584228516" Y="-0.02626288414" />
                  <Point X="3.350280029297" Y="-0.062941680908" />
                  <Point X="3.351874511719" Y="-0.076423416138" />
                  <Point X="3.356618652344" Y="-0.101194839478" />
                  <Point X="3.370376708984" Y="-0.173033950806" />
                  <Point X="3.373158935547" Y="-0.18398789978" />
                  <Point X="3.380005126953" Y="-0.205488876343" />
                  <Point X="3.384069091797" Y="-0.216035766602" />
                  <Point X="3.393843017578" Y="-0.237499588013" />
                  <Point X="3.399130859375" Y="-0.247489273071" />
                  <Point X="3.410854736328" Y="-0.26676864624" />
                  <Point X="3.417290527344" Y="-0.276058319092" />
                  <Point X="3.431522460938" Y="-0.29419342041" />
                  <Point X="3.472796386719" Y="-0.34678616333" />
                  <Point X="3.478717773438" Y="-0.353632781982" />
                  <Point X="3.501139404297" Y="-0.375805236816" />
                  <Point X="3.530176025391" Y="-0.39872467041" />
                  <Point X="3.541494384766" Y="-0.406404174805" />
                  <Point X="3.565214599609" Y="-0.420114929199" />
                  <Point X="3.634004394531" Y="-0.45987677002" />
                  <Point X="3.639496582031" Y="-0.462815490723" />
                  <Point X="3.659158447266" Y="-0.472016876221" />
                  <Point X="3.683028076172" Y="-0.481027740479" />
                  <Point X="3.691991943359" Y="-0.48391293335" />
                  <Point X="4.658711425781" Y="-0.742944396973" />
                  <Point X="4.784876953125" Y="-0.776750427246" />
                  <Point X="4.76161328125" Y="-0.931053833008" />
                  <Point X="4.741960449219" Y="-1.017176086426" />
                  <Point X="4.727802246094" Y="-1.079219726563" />
                  <Point X="3.823366455078" Y="-0.9601484375" />
                  <Point X="3.436781982422" Y="-0.909253723145" />
                  <Point X="3.428624511719" Y="-0.908535888672" />
                  <Point X="3.400098388672" Y="-0.908042480469" />
                  <Point X="3.366721435547" Y="-0.910840881348" />
                  <Point X="3.354481201172" Y="-0.912676208496" />
                  <Point X="3.307927001953" Y="-0.922795043945" />
                  <Point X="3.172916992188" Y="-0.952139892578" />
                  <Point X="3.157874023438" Y="-0.956742492676" />
                  <Point X="3.128753662109" Y="-0.968367004395" />
                  <Point X="3.114676269531" Y="-0.975388977051" />
                  <Point X="3.086849609375" Y="-0.992281311035" />
                  <Point X="3.074124023438" Y="-1.001530273438" />
                  <Point X="3.050374023438" Y="-1.022001159668" />
                  <Point X="3.039349853516" Y="-1.03322265625" />
                  <Point X="3.011210693359" Y="-1.067065063477" />
                  <Point X="2.929605712891" Y="-1.16521081543" />
                  <Point X="2.921326171875" Y="-1.176847290039" />
                  <Point X="2.90660546875" Y="-1.201229858398" />
                  <Point X="2.9001640625" Y="-1.213975708008" />
                  <Point X="2.888820800781" Y="-1.241360839844" />
                  <Point X="2.884362792969" Y="-1.254928222656" />
                  <Point X="2.877531005859" Y="-1.282577880859" />
                  <Point X="2.875157226562" Y="-1.296660400391" />
                  <Point X="2.871124267578" Y="-1.340487792969" />
                  <Point X="2.859428222656" Y="-1.467590698242" />
                  <Point X="2.859288818359" Y="-1.483321655273" />
                  <Point X="2.861607666016" Y="-1.514590576172" />
                  <Point X="2.864065917969" Y="-1.530128662109" />
                  <Point X="2.871797607422" Y="-1.561749633789" />
                  <Point X="2.876786621094" Y="-1.576669067383" />
                  <Point X="2.889157958984" Y="-1.605479980469" />
                  <Point X="2.896540283203" Y="-1.619371337891" />
                  <Point X="2.922303955078" Y="-1.659445068359" />
                  <Point X="2.997020507812" Y="-1.775661865234" />
                  <Point X="3.001741943359" Y="-1.782353149414" />
                  <Point X="3.019792724609" Y="-1.804448730469" />
                  <Point X="3.043488769531" Y="-1.828119750977" />
                  <Point X="3.052796142578" Y="-1.836277709961" />
                  <Point X="3.949917480469" Y="-2.524663574219" />
                  <Point X="4.087170410156" Y="-2.629981689453" />
                  <Point X="4.045488769531" Y="-2.697429443359" />
                  <Point X="4.004843261719" Y="-2.755180664062" />
                  <Point X="4.001274169922" Y="-2.760251708984" />
                  <Point X="3.192760009766" Y="-2.293455810547" />
                  <Point X="2.848454589844" Y="-2.094670654297" />
                  <Point X="2.841191894531" Y="-2.090885253906" />
                  <Point X="2.815026123047" Y="-2.079513183594" />
                  <Point X="2.783119140625" Y="-2.069325927734" />
                  <Point X="2.771108398438" Y="-2.066337646484" />
                  <Point X="2.715701660156" Y="-2.056331298828" />
                  <Point X="2.555018066406" Y="-2.027311889648" />
                  <Point X="2.539359130859" Y="-2.025807495117" />
                  <Point X="2.508006835938" Y="-2.025403198242" />
                  <Point X="2.492313476562" Y="-2.026503540039" />
                  <Point X="2.460140625" Y="-2.031461303711" />
                  <Point X="2.444844482422" Y="-2.035136352539" />
                  <Point X="2.415068847656" Y="-2.044960083008" />
                  <Point X="2.400589355469" Y="-2.051108642578" />
                  <Point X="2.354559814453" Y="-2.075333496094" />
                  <Point X="2.221071289062" Y="-2.145587646484" />
                  <Point X="2.208968994141" Y="-2.153169921875" />
                  <Point X="2.186037597656" Y="-2.170063232422" />
                  <Point X="2.175208496094" Y="-2.179374267578" />
                  <Point X="2.154248779297" Y="-2.200334228516" />
                  <Point X="2.144938232422" Y="-2.211162841797" />
                  <Point X="2.128045898438" Y="-2.234093017578" />
                  <Point X="2.120464111328" Y="-2.246194580078" />
                  <Point X="2.096239013672" Y="-2.292224121094" />
                  <Point X="2.025984985352" Y="-2.425712646484" />
                  <Point X="2.019836303711" Y="-2.440192382812" />
                  <Point X="2.010012329102" Y="-2.469968505859" />
                  <Point X="2.006337280273" Y="-2.485264892578" />
                  <Point X="2.001379516602" Y="-2.5174375" />
                  <Point X="2.000279174805" Y="-2.533130859375" />
                  <Point X="2.00068347168" Y="-2.564483154297" />
                  <Point X="2.002187866211" Y="-2.580142089844" />
                  <Point X="2.012194213867" Y="-2.635548828125" />
                  <Point X="2.041213378906" Y="-2.796232421875" />
                  <Point X="2.043015014648" Y="-2.804221435547" />
                  <Point X="2.051236083984" Y="-2.83154296875" />
                  <Point X="2.064069580078" Y="-2.862479736328" />
                  <Point X="2.069546630859" Y="-2.873578613281" />
                  <Point X="2.646037353516" Y="-3.872089599609" />
                  <Point X="2.735892822266" Y="-4.027724365234" />
                  <Point X="2.723754150391" Y="-4.036083251953" />
                  <Point X="2.096651611328" Y="-3.218828125" />
                  <Point X="1.833914672852" Y="-2.876422851562" />
                  <Point X="1.828654418945" Y="-2.870147216797" />
                  <Point X="1.808830688477" Y="-2.849625732422" />
                  <Point X="1.783251220703" Y="-2.828004150391" />
                  <Point X="1.773298828125" Y="-2.820647216797" />
                  <Point X="1.718652709961" Y="-2.785514892578" />
                  <Point X="1.560175415039" Y="-2.683628417969" />
                  <Point X="1.546283935547" Y="-2.676245849609" />
                  <Point X="1.517472900391" Y="-2.663874511719" />
                  <Point X="1.502553588867" Y="-2.658885742188" />
                  <Point X="1.470932617188" Y="-2.651154052734" />
                  <Point X="1.45539440918" Y="-2.648695800781" />
                  <Point X="1.42412512207" Y="-2.646376953125" />
                  <Point X="1.408394042969" Y="-2.646516357422" />
                  <Point X="1.348629150391" Y="-2.652016113281" />
                  <Point X="1.175307128906" Y="-2.667965332031" />
                  <Point X="1.161224731445" Y="-2.670339355469" />
                  <Point X="1.133575317383" Y="-2.677171142578" />
                  <Point X="1.120008422852" Y="-2.68162890625" />
                  <Point X="1.092623168945" Y="-2.692972167969" />
                  <Point X="1.079877563477" Y="-2.699413330078" />
                  <Point X="1.055495361328" Y="-2.714133789062" />
                  <Point X="1.043859008789" Y="-2.722413085938" />
                  <Point X="0.997710144043" Y="-2.760784179688" />
                  <Point X="0.863875244141" Y="-2.872063476562" />
                  <Point X="0.852653320312" Y="-2.883087890625" />
                  <Point X="0.832182128906" Y="-2.906838134766" />
                  <Point X="0.822933044434" Y="-2.919564208984" />
                  <Point X="0.80604083252" Y="-2.947390869141" />
                  <Point X="0.799018859863" Y="-2.961468261719" />
                  <Point X="0.787394165039" Y="-2.990588867188" />
                  <Point X="0.782791748047" Y="-3.005631591797" />
                  <Point X="0.768993469238" Y="-3.069114501953" />
                  <Point X="0.728977478027" Y="-3.253219482422" />
                  <Point X="0.727584594727" Y="-3.261290039062" />
                  <Point X="0.724724487305" Y="-3.289677734375" />
                  <Point X="0.7247421875" Y="-3.323170410156" />
                  <Point X="0.725554992676" Y="-3.335520019531" />
                  <Point X="0.833091369629" Y="-4.152340332031" />
                  <Point X="0.757776123047" Y="-3.871260009766" />
                  <Point X="0.655064941406" Y="-3.487936523438" />
                  <Point X="0.652606262207" Y="-3.480124511719" />
                  <Point X="0.642145141602" Y="-3.453580566406" />
                  <Point X="0.62678717041" Y="-3.423815917969" />
                  <Point X="0.620407714844" Y="-3.413210449219" />
                  <Point X="0.578427246094" Y="-3.352724121094" />
                  <Point X="0.456680114746" Y="-3.177310058594" />
                  <Point X="0.446670654297" Y="-3.165172851562" />
                  <Point X="0.424786804199" Y="-3.142717529297" />
                  <Point X="0.41291229248" Y="-3.132399169922" />
                  <Point X="0.386657165527" Y="-3.113155273438" />
                  <Point X="0.37324230957" Y="-3.104937744141" />
                  <Point X="0.34524130249" Y="-3.090829589844" />
                  <Point X="0.330654876709" Y="-3.084938964844" />
                  <Point X="0.265692230225" Y="-3.064776855469" />
                  <Point X="0.077295753479" Y="-3.006305664062" />
                  <Point X="0.063377220154" Y="-3.003109619141" />
                  <Point X="0.035217639923" Y="-2.998840087891" />
                  <Point X="0.02097659111" Y="-2.997766601562" />
                  <Point X="-0.008664761543" Y="-2.997766601562" />
                  <Point X="-0.022905065536" Y="-2.998840087891" />
                  <Point X="-0.051064350128" Y="-3.003109375" />
                  <Point X="-0.064983184814" Y="-3.006305175781" />
                  <Point X="-0.129945953369" Y="-3.026467041016" />
                  <Point X="-0.318342285156" Y="-3.084938476562" />
                  <Point X="-0.332929046631" Y="-3.090829345703" />
                  <Point X="-0.360930633545" Y="-3.104937744141" />
                  <Point X="-0.374345336914" Y="-3.113155273438" />
                  <Point X="-0.400600463867" Y="-3.132399169922" />
                  <Point X="-0.412475128174" Y="-3.142717773438" />
                  <Point X="-0.434358978271" Y="-3.165173095703" />
                  <Point X="-0.444367980957" Y="-3.177309814453" />
                  <Point X="-0.486348602295" Y="-3.237795898438" />
                  <Point X="-0.608095703125" Y="-3.413210205078" />
                  <Point X="-0.612470458984" Y="-3.420132324219" />
                  <Point X="-0.62597668457" Y="-3.445265136719" />
                  <Point X="-0.638777893066" Y="-3.476215820312" />
                  <Point X="-0.642753112793" Y="-3.487936523438" />
                  <Point X="-0.94062298584" Y="-4.599602539062" />
                  <Point X="-0.985425109863" Y="-4.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.929357462614" Y="-2.718730575955" />
                  <Point X="4.010964841296" Y="-2.571506967631" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.847072518159" Y="-2.671223338027" />
                  <Point X="3.934759265167" Y="-2.513032258921" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.764787573704" Y="-2.623716100098" />
                  <Point X="3.858553659766" Y="-2.454557603019" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.62818554264" Y="-1.066104932334" />
                  <Point X="4.783494461316" Y="-0.785920226223" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.682502629249" Y="-2.57620886217" />
                  <Point X="3.782348054364" Y="-2.396082947117" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.526954372962" Y="-1.05277758949" />
                  <Point X="4.693526706464" Y="-0.752273145146" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.600217684794" Y="-2.528701624242" />
                  <Point X="3.706142448963" Y="-2.337608291216" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.425723203284" Y="-1.039450246647" />
                  <Point X="4.598954559266" Y="-0.726932607745" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.684442249213" Y="-3.98485103587" />
                  <Point X="2.697519345929" Y="-3.961259328895" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.51793274034" Y="-2.481194386314" />
                  <Point X="3.629936843562" Y="-2.279133635314" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.324492033606" Y="-1.026122903803" />
                  <Point X="4.504382397344" Y="-0.701592096908" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.62137946684" Y="-3.902666099589" />
                  <Point X="2.642104344724" Y="-3.865277430162" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.435647795885" Y="-2.433687148386" />
                  <Point X="3.553731238161" Y="-2.220658979412" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.223260863928" Y="-1.012795560959" />
                  <Point X="4.409810235421" Y="-0.676251586071" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.558316684466" Y="-3.820481163307" />
                  <Point X="2.586689251957" Y="-3.769295696612" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.35336285143" Y="-2.386179910458" />
                  <Point X="3.47752563276" Y="-2.16218432351" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.12202969425" Y="-0.999468218116" />
                  <Point X="4.315238073499" Y="-0.650911075233" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.495253902092" Y="-3.738296227025" />
                  <Point X="2.53127415919" Y="-3.673313963062" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.271077906975" Y="-2.33867267253" />
                  <Point X="3.401320027359" Y="-2.103709667609" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.020798524573" Y="-0.986140875272" />
                  <Point X="4.220665911576" Y="-0.625570564396" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.432191119718" Y="-3.656111290743" />
                  <Point X="2.475859066424" Y="-3.577332229512" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.188792964051" Y="-2.29116543184" />
                  <Point X="3.325114421958" Y="-2.045235011707" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.919567354895" Y="-0.972813532429" />
                  <Point X="4.126093749654" Y="-0.600230053559" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.369128337344" Y="-3.573926354461" />
                  <Point X="2.420443973657" Y="-3.481350495961" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.10650805135" Y="-2.243658136626" />
                  <Point X="3.248908816557" Y="-1.986760355805" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.818336184397" Y="-0.959486191063" />
                  <Point X="4.031521587731" Y="-0.574889542722" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.739301003003" Y="0.701978322626" />
                  <Point X="4.775549104028" Y="0.767371627913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.30606555497" Y="-3.49174141818" />
                  <Point X="2.36502888089" Y="-3.385368762411" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.02422313865" Y="-2.196150841411" />
                  <Point X="3.172703211156" Y="-1.928285699903" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.717104998228" Y="-0.946158877971" />
                  <Point X="3.936949425809" Y="-0.549549031885" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.611735495449" Y="0.667797262338" />
                  <Point X="4.751357295159" Y="0.919681656692" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.243002772596" Y="-3.409556481898" />
                  <Point X="2.309613788123" Y="-3.289387028861" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.941938225949" Y="-2.148643546197" />
                  <Point X="3.096497605755" Y="-1.869811044002" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.615873812059" Y="-0.932831564878" />
                  <Point X="3.842377263886" Y="-0.524208521048" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.484169987895" Y="0.633616202049" />
                  <Point X="4.718210790871" Y="1.055836987301" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.179939990222" Y="-3.327371545616" />
                  <Point X="2.254198695356" Y="-3.193405295311" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.859653313249" Y="-2.101136250982" />
                  <Point X="3.022571318255" Y="-1.807224389757" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.51464262589" Y="-0.919504251785" />
                  <Point X="3.747805101964" Y="-0.498868010211" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.35660448034" Y="0.599435141761" />
                  <Point X="4.635280731239" Y="1.102180406641" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.116877207848" Y="-3.245186609334" />
                  <Point X="2.198783602589" Y="-3.09742356176" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.770395249536" Y="-2.066208853199" />
                  <Point X="2.961808011456" Y="-1.720891089726" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.412260743709" Y="-0.90825284925" />
                  <Point X="3.655118325163" Y="-0.470126174577" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.229038972786" Y="0.565254081472" />
                  <Point X="4.518111475054" Y="1.086754680299" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.053814434373" Y="-3.163001656999" />
                  <Point X="2.143368509822" Y="-3.00144182821" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.671660686825" Y="-2.04837751216" />
                  <Point X="2.903479637049" Y="-1.630165055378" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.293890054303" Y="-0.925846018497" />
                  <Point X="3.572035839671" Y="-0.424057738766" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.101473465232" Y="0.531073021184" />
                  <Point X="4.400942281064" Y="1.071329066159" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.990751665099" Y="-3.080816697084" />
                  <Point X="2.087953417055" Y="-2.90546009466" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.572926170261" Y="-2.030546087871" />
                  <Point X="2.861291113032" Y="-1.510321960167" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.170242976822" Y="-0.952958043808" />
                  <Point X="3.494050032951" Y="-0.368794651058" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.973907965796" Y="0.496891975541" />
                  <Point X="4.283773087074" Y="1.055903452018" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.927688895826" Y="-2.998631737169" />
                  <Point X="2.040885874832" Y="-2.794418981289" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.46414201085" Y="-2.0308446992" />
                  <Point X="2.876927072253" Y="-1.286160735766" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.972700314997" Y="-1.113381232179" />
                  <Point X="3.428745199065" Y="-0.290654482773" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.846342478379" Y="0.46271095158" />
                  <Point X="4.166603893084" Y="1.040477837878" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.864626126552" Y="-2.916446777254" />
                  <Point X="2.014193623827" Y="-2.646619869532" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.32110229291" Y="-2.092941974001" />
                  <Point X="3.375382290194" Y="-0.190970511472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.718776990962" Y="0.42852992762" />
                  <Point X="4.049434699093" Y="1.025052223738" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.79808282068" Y="-2.840540871576" />
                  <Point X="2.019596177099" Y="-2.440920198165" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.15060797412" Y="-2.204568659836" />
                  <Point X="3.349313935758" Y="-0.042045860511" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.573456969424" Y="0.362318876233" />
                  <Point X="3.932265505103" Y="1.009626609597" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.719620641843" Y="-2.786137181916" />
                  <Point X="3.815096311113" Y="0.994200995457" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.639540267756" Y="-2.734652793764" />
                  <Point X="3.697927117123" Y="0.978775381316" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.804412613089" Y="-4.045309757266" />
                  <Point X="0.816200763313" Y="-4.024043371316" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.559425746055" Y="-2.683230009539" />
                  <Point X="3.580757923132" Y="0.963349767176" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.769017071051" Y="-3.913211798162" />
                  <Point X="0.795354247004" Y="-3.865698275003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.468776210053" Y="-2.650812894214" />
                  <Point X="3.465424501347" Y="0.95123597376" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.733621535137" Y="-3.78111382801" />
                  <Point X="0.774507730696" Y="-3.707353178691" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.360074434129" Y="-2.650962881801" />
                  <Point X="3.363151006978" Y="0.962682913085" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.698226002072" Y="-3.649015852717" />
                  <Point X="0.753661214387" Y="-3.549008082378" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.245617575461" Y="-2.661495313491" />
                  <Point X="3.268895737221" Y="0.988595112521" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.068384089929" Y="2.43091028059" />
                  <Point X="4.11633917986" Y="2.517423552934" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.662830469008" Y="-3.516917877424" />
                  <Point X="0.732814698078" Y="-3.390662986065" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.12713706751" Y="-2.679286600639" />
                  <Point X="3.185744301597" Y="1.034539159002" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.879371330544" Y="2.285875443569" />
                  <Point X="4.059648886052" Y="2.611104762908" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.61556526749" Y="-3.406233350861" />
                  <Point X="0.747422961833" Y="-3.168355773366" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.952512423083" Y="-2.798364591168" />
                  <Point X="3.113224948791" Y="1.099663990623" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.690358571159" Y="2.140840606548" />
                  <Point X="3.999586458535" Y="2.698702482635" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.555176521529" Y="-3.31922432519" />
                  <Point X="3.053990643952" Y="1.188755683207" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.501345811774" Y="1.995805769527" />
                  <Point X="3.934749187021" Y="2.777686155768" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.494787613517" Y="-3.232215591867" />
                  <Point X="3.013237652431" Y="1.311188547598" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.312333079864" Y="1.85077098207" />
                  <Point X="3.775008757729" Y="2.685460000142" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.431793350397" Y="-3.14990704358" />
                  <Point X="3.027297129697" Y="1.532505723265" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.096951853225" Y="1.658166170889" />
                  <Point X="3.615268309831" Y="2.59323381095" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.353590164489" Y="-3.095036118307" />
                  <Point X="3.455527857763" Y="2.501007614234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.262323974899" Y="-3.063731475506" />
                  <Point X="3.295787405694" Y="2.408781417519" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.169648860527" Y="-3.034968600294" />
                  <Point X="3.136046953626" Y="2.316555220803" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.076960962383" Y="-3.006228787616" />
                  <Point X="2.976306501557" Y="2.224329024088" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.005411491663" Y="-4.76292717644" />
                  <Point X="-0.964711598342" Y="-4.689502625255" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.027989332392" Y="-2.999610924056" />
                  <Point X="2.816566049489" Y="2.132102827372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.10209781089" Y="-4.741400706343" />
                  <Point X="-0.86307643111" Y="-4.310194722689" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.155972018382" Y="-3.034544594164" />
                  <Point X="2.65682559742" Y="2.039876630657" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.121346397179" Y="-4.580172867966" />
                  <Point X="-0.761441202274" Y="-3.930886708986" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.287159898069" Y="-3.075260586768" />
                  <Point X="2.519581146252" Y="1.988234293868" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.139419394873" Y="-4.416824211621" />
                  <Point X="-0.659805973438" Y="-3.551578695283" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.483988123678" Y="-3.234394898087" />
                  <Point X="2.404345563177" Y="1.976297006158" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.168103702363" Y="-4.272618864896" />
                  <Point X="2.30430966509" Y="1.991780676033" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.203378472201" Y="-4.140303026975" />
                  <Point X="2.218484165905" Y="2.032900584147" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.26528320698" Y="-4.056028917529" />
                  <Point X="2.141627886237" Y="2.090201392598" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.338372150712" Y="-3.991931655139" />
                  <Point X="2.077508009729" Y="2.170479280581" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.411461029274" Y="-3.927834275181" />
                  <Point X="2.027100360483" Y="2.275494681373" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.484549907837" Y="-3.863736895222" />
                  <Point X="2.019247243672" Y="2.457280490884" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.565938815115" Y="-3.814613163436" />
                  <Point X="2.741480337456" Y="3.956176689773" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.663369276404" Y="-3.794429161155" />
                  <Point X="2.663572502798" Y="4.011580442804" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.768179999208" Y="-3.787559503093" />
                  <Point X="2.583307325648" Y="4.062731437404" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.872990733634" Y="-3.780689865998" />
                  <Point X="2.501203464924" Y="4.110565359032" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.980073738215" Y="-3.777919512776" />
                  <Point X="2.419099604201" Y="4.15839928066" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.118084739419" Y="-3.830944742436" />
                  <Point X="2.336995748013" Y="4.206233210468" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.290598886935" Y="-3.946215295751" />
                  <Point X="2.254891948436" Y="4.254067242407" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.515034077401" Y="-4.155153890049" />
                  <Point X="2.168858695127" Y="4.294812352161" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.595898728895" Y="-4.105084375793" />
                  <Point X="2.082600859874" Y="4.335152305364" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.676763380389" Y="-4.055014861537" />
                  <Point X="1.996343024622" Y="4.375492258566" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.757039726455" Y="-4.003884016195" />
                  <Point X="1.910085176823" Y="4.415832189134" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.833167123444" Y="-3.945268268583" />
                  <Point X="1.822159449121" Y="4.453163184708" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.909294604362" Y="-3.886652672383" />
                  <Point X="1.731723195698" Y="4.485965071991" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.311404581826" Y="-2.612077312063" />
                  <Point X="1.641286942276" Y="4.518766959273" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.346617914807" Y="-2.479650639118" />
                  <Point X="1.550850688854" Y="4.551568846556" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.413225899618" Y="-2.403861417335" />
                  <Point X="1.46041458395" Y="4.584371001773" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.497044174437" Y="-2.359120380609" />
                  <Point X="1.36502072793" Y="4.608229137218" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.601555800953" Y="-2.351711138561" />
                  <Point X="1.269222495148" Y="4.631357757673" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.743414998714" Y="-2.411678698581" />
                  <Point X="1.173424262365" Y="4.654486378128" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.903155452268" Y="-2.503904897976" />
                  <Point X="1.077626029583" Y="4.677614998583" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.062895905821" Y="-2.596131097371" />
                  <Point X="0.981827796801" Y="4.700743619038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.222636359375" Y="-2.688357296766" />
                  <Point X="0.886029588875" Y="4.723872284334" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.382376812929" Y="-2.780583496161" />
                  <Point X="0.787535238302" Y="4.742136979541" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.542117266483" Y="-2.872809695556" />
                  <Point X="0.684876068796" Y="4.752888142499" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.70185767263" Y="-2.965035809428" />
                  <Point X="0.582216899289" Y="4.763639305458" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.820043860419" Y="-2.982296128949" />
                  <Point X="-3.029987791343" Y="-1.556997250994" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.957270353338" Y="-1.425811520191" />
                  <Point X="0.111397916035" Y="4.110212582843" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.277833942814" Y="4.410471123351" />
                  <Point X="0.479557729783" Y="4.774390468417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.882892835331" Y="-2.899725473795" />
                  <Point X="-3.221864669026" Y="-1.707199094201" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.970727117635" Y="-1.25413495835" />
                  <Point X="-0.010981424779" Y="4.085387615022" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.945741810243" Y="-2.81715481864" />
                  <Point X="-3.410877271721" Y="-1.852233648546" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.032995777142" Y="-1.170517386493" />
                  <Point X="-0.104318124754" Y="4.112956958212" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.008113520386" Y="-2.733723155053" />
                  <Point X="-3.599889967066" Y="-1.997268370037" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.114226676071" Y="-1.121108600099" />
                  <Point X="-0.178892636023" Y="4.17437418582" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.064407358581" Y="-2.639326720219" />
                  <Point X="-3.788902662411" Y="-2.142303091528" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.211171917148" Y="-1.100049237384" />
                  <Point X="-0.230040815325" Y="4.278053635029" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.120701229605" Y="-2.544930344611" />
                  <Point X="-3.977915357757" Y="-2.287337813019" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.32581902597" Y="-1.110924889439" />
                  <Point X="-0.265436392915" Y="4.410151529997" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.176995106012" Y="-2.450533978714" />
                  <Point X="-4.166928053102" Y="-2.43237253451" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.442988225264" Y="-1.126350513147" />
                  <Point X="-0.300831923253" Y="4.542249510208" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.560157424557" Y="-1.141776136855" />
                  <Point X="-0.336227435554" Y="4.67434752296" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.677326623851" Y="-1.157201760563" />
                  <Point X="-0.386227198707" Y="4.780098769744" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.794495823145" Y="-1.17262738427" />
                  <Point X="-0.502381236723" Y="4.766504545459" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.911665022438" Y="-1.188053007978" />
                  <Point X="-3.387905032394" Y="-0.243164973638" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.300278033778" Y="-0.085081683484" />
                  <Point X="-0.61853527474" Y="4.752910321173" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.028834221732" Y="-1.203478631686" />
                  <Point X="-3.539394652432" Y="-0.320506275351" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.317378006454" Y="0.080022356459" />
                  <Point X="-0.734689312757" Y="4.739316096888" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.146003421025" Y="-1.218904255394" />
                  <Point X="-3.66696017156" Y="-0.354687356518" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.376177544099" Y="0.169898389825" />
                  <Point X="-0.85084320458" Y="4.725722136344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.263172620319" Y="-1.234329879102" />
                  <Point X="-3.794525690687" Y="-0.388868437685" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.453514822813" Y="0.226331453026" />
                  <Point X="-0.970836307691" Y="4.705202055293" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.380341819612" Y="-1.24975550281" />
                  <Point X="-3.922091208362" Y="-0.423049516232" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.543934774283" Y="0.25916274981" />
                  <Point X="-1.099441273716" Y="4.669145762283" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.497511019449" Y="-1.265181127498" />
                  <Point X="-4.049656721935" Y="-0.457230587378" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.638506929148" Y="0.284503273378" />
                  <Point X="-1.228046239741" Y="4.633089469273" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.61468022023" Y="-1.28060675389" />
                  <Point X="-4.177222235507" Y="-0.491411658524" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.733079084014" Y="0.309843796946" />
                  <Point X="-1.637842131133" Y="4.089751318553" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.462503050238" Y="4.406071393853" />
                  <Point X="-1.356651205766" Y="4.597033176263" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.681662275105" Y="-1.205492372365" />
                  <Point X="-4.30478774908" Y="-0.52559272967" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.82765123888" Y="0.335184320513" />
                  <Point X="-1.776127952261" Y="4.036230300626" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.715925649542" Y="-1.071351928841" />
                  <Point X="-4.432353262652" Y="-0.559773800816" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.922223393746" Y="0.360524844081" />
                  <Point X="-1.885298773811" Y="4.035234132332" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.746891256153" Y="-0.931262154675" />
                  <Point X="-4.559918776225" Y="-0.593954871962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.016795548612" Y="0.385865367648" />
                  <Point X="-3.510017190465" Y="1.300117727084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.442585761734" Y="1.421767244721" />
                  <Point X="-1.973941326631" Y="4.071271941159" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.769168846229" Y="-0.775498783779" />
                  <Point X="-4.687484289797" Y="-0.628135943108" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.111367703478" Y="0.411205891216" />
                  <Point X="-3.664110002756" Y="1.218080142232" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.441736834375" Y="1.619251957481" />
                  <Point X="-2.057361038165" Y="4.116732005086" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.205939858344" Y="0.436546414783" />
                  <Point X="-3.779536185868" Y="1.205799002954" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.495537555744" Y="1.718146094129" />
                  <Point X="-2.915639668242" Y="2.764309576363" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.75037666754" Y="3.062451921808" />
                  <Point X="-2.128335806692" Y="4.184643340509" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.30051201321" Y="0.461886938351" />
                  <Point X="-3.880767291155" Y="1.219126461962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.56614570208" Y="1.786718833492" />
                  <Point X="-3.046821807084" Y="2.723603940519" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.777945524939" Y="3.208669593767" />
                  <Point X="-2.191398660867" Y="4.266828147258" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.395084168076" Y="0.487227461918" />
                  <Point X="-3.981998396442" Y="1.232453920969" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.642351292622" Y="1.8451935162" />
                  <Point X="-3.151429969826" Y="2.730839026604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.832292874057" Y="3.30657758785" />
                  <Point X="-2.346676725243" Y="4.182652311042" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.489656322942" Y="0.512567985486" />
                  <Point X="-4.083229527506" Y="1.245781333474" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.718556883164" Y="1.903668198909" />
                  <Point X="-3.245099335703" Y="2.757808224621" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.887707960892" Y="3.402559332103" />
                  <Point X="-2.503631576414" Y="4.095451471372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.584228477808" Y="0.537908509053" />
                  <Point X="-4.184460713205" Y="1.259108647414" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.794762473705" Y="1.962142881617" />
                  <Point X="-3.327652711258" Y="2.804831200025" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.943123047727" Y="3.498541076355" />
                  <Point X="-2.663070771806" Y="4.003768756088" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.678800605519" Y="0.56324908161" />
                  <Point X="-4.285691898905" Y="1.272435961354" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.870968064247" Y="2.020617564326" />
                  <Point X="-3.409937664678" Y="2.85233842178" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.998538134561" Y="3.594522820607" />
                  <Point X="-2.851967417198" Y="3.858943394255" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.773372722164" Y="0.588589674129" />
                  <Point X="-4.386923084605" Y="1.285763275294" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.947173654789" Y="2.079092247034" />
                  <Point X="-3.492222602027" Y="2.899845672528" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.053953221396" Y="3.69050456486" />
                  <Point X="-3.04086406259" Y="3.714118032421" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.750939271947" Y="0.825013896901" />
                  <Point X="-4.488154270304" Y="1.299090589234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.023379245331" Y="2.137566929743" />
                  <Point X="-3.574507536709" Y="2.947352928087" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.678945572171" Y="1.15084717664" />
                  <Point X="-4.589385456004" Y="1.312417903174" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.099584835873" Y="2.196041612451" />
                  <Point X="-3.656792471391" Y="2.994860183645" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.175790426414" Y="2.25451629516" />
                  <Point X="-3.784824729113" Y="2.959837083764" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="0.001626220703" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.574250244141" Y="-3.920435791016" />
                  <Point X="0.471539031982" Y="-3.537112304688" />
                  <Point X="0.464318664551" Y="-3.521544189453" />
                  <Point X="0.42233807373" Y="-3.461057861328" />
                  <Point X="0.300591033936" Y="-3.285643798828" />
                  <Point X="0.274335754395" Y="-3.266399902344" />
                  <Point X="0.209373123169" Y="-3.246237792969" />
                  <Point X="0.020976625443" Y="-3.187766601562" />
                  <Point X="-0.008664756775" Y="-3.187766601562" />
                  <Point X="-0.073627540588" Y="-3.207928466797" />
                  <Point X="-0.262023895264" Y="-3.266399902344" />
                  <Point X="-0.288279144287" Y="-3.285643798828" />
                  <Point X="-0.330259765625" Y="-3.346129882812" />
                  <Point X="-0.45200680542" Y="-3.521544189453" />
                  <Point X="-0.459227142334" Y="-3.537112304688" />
                  <Point X="-0.757097167969" Y="-4.648778320312" />
                  <Point X="-0.84774395752" Y="-4.987077148438" />
                  <Point X="-0.878838134766" Y="-4.981041503906" />
                  <Point X="-1.100246826172" Y="-4.938065429688" />
                  <Point X="-1.17460546875" Y="-4.918933105469" />
                  <Point X="-1.351589599609" Y="-4.873396972656" />
                  <Point X="-1.321514160156" Y="-4.644952636719" />
                  <Point X="-1.309150146484" Y="-4.551038574219" />
                  <Point X="-1.309683227539" Y="-4.5347578125" />
                  <Point X="-1.325202880859" Y="-4.456735839844" />
                  <Point X="-1.370210693359" Y="-4.230466308594" />
                  <Point X="-1.386282714844" Y="-4.20262890625" />
                  <Point X="-1.446092041016" Y="-4.150177246094" />
                  <Point X="-1.619543457031" Y="-3.998063964844" />
                  <Point X="-1.649240722656" Y="-3.985762939453" />
                  <Point X="-1.72862109375" Y="-3.980560058594" />
                  <Point X="-1.958829833984" Y="-3.965471435547" />
                  <Point X="-1.989878540039" Y="-3.973791015625" />
                  <Point X="-2.056022460938" Y="-4.017987060547" />
                  <Point X="-2.247844970703" Y="-4.146159179688" />
                  <Point X="-2.259734130859" Y="-4.157294433594" />
                  <Point X="-2.426965087891" Y="-4.375233886719" />
                  <Point X="-2.457095214844" Y="-4.414500488281" />
                  <Point X="-2.531293945312" Y="-4.36855859375" />
                  <Point X="-2.855830810547" Y="-4.16761328125" />
                  <Point X="-2.958801025391" Y="-4.088329589844" />
                  <Point X="-3.228581054688" Y="-3.880608154297" />
                  <Point X="-2.696910888672" Y="-2.959728759766" />
                  <Point X="-2.506033691406" Y="-2.629119628906" />
                  <Point X="-2.499762695312" Y="-2.597593261719" />
                  <Point X="-2.513979736328" Y="-2.568764404297" />
                  <Point X="-2.531328369141" Y="-2.551415771484" />
                  <Point X="-2.560157226562" Y="-2.537198730469" />
                  <Point X="-2.59168359375" Y="-2.543469726562" />
                  <Point X="-3.550473876953" Y="-3.097027587891" />
                  <Point X="-3.842959472656" Y="-3.265894042969" />
                  <Point X="-3.905299560547" Y="-3.183991943359" />
                  <Point X="-4.161703613281" Y="-2.847129394531" />
                  <Point X="-4.235522460938" Y="-2.723346679688" />
                  <Point X="-4.431020019531" Y="-2.395527099609" />
                  <Point X="-3.497760009766" Y="-1.679411499023" />
                  <Point X="-3.163786865234" Y="-1.423144775391" />
                  <Point X="-3.152535400391" Y="-1.411081420898" />
                  <Point X="-3.143846435547" Y="-1.388386108398" />
                  <Point X="-3.138117431641" Y="-1.366266113281" />
                  <Point X="-3.136651855469" Y="-1.350051025391" />
                  <Point X="-3.148656738281" Y="-1.321068481445" />
                  <Point X="-3.16794921875" Y="-1.306642700195" />
                  <Point X="-3.187641601562" Y="-1.295052734375" />
                  <Point X="-3.219529296875" Y="-1.288571044922" />
                  <Point X="-4.429911132812" Y="-1.447920898438" />
                  <Point X="-4.803283203125" Y="-1.497076293945" />
                  <Point X="-4.827111816406" Y="-1.40378894043" />
                  <Point X="-4.927393066406" Y="-1.011191650391" />
                  <Point X="-4.946923339844" Y="-0.874636413574" />
                  <Point X="-4.998396484375" Y="-0.514742126465" />
                  <Point X="-3.937909667969" Y="-0.23058555603" />
                  <Point X="-3.557463867188" Y="-0.128645431519" />
                  <Point X="-3.534779785156" Y="-0.116486167908" />
                  <Point X="-3.514142822266" Y="-0.102163047791" />
                  <Point X="-3.50232421875" Y="-0.090645263672" />
                  <Point X="-3.492526855469" Y="-0.06826499176" />
                  <Point X="-3.485647949219" Y="-0.046100738525" />
                  <Point X="-3.483400878906" Y="-0.031279794693" />
                  <Point X="-3.488019775391" Y="-0.008816271782" />
                  <Point X="-3.494898925781" Y="0.013347985268" />
                  <Point X="-3.502324462891" Y="0.028085382462" />
                  <Point X="-3.521258789062" Y="0.044541866302" />
                  <Point X="-3.541895751953" Y="0.05886498642" />
                  <Point X="-3.557463867188" Y="0.066085334778" />
                  <Point X="-4.660784179688" Y="0.361719116211" />
                  <Point X="-4.998186523438" Y="0.452125793457" />
                  <Point X="-4.982274902344" Y="0.55965435791" />
                  <Point X="-4.917645019531" Y="0.996418212891" />
                  <Point X="-4.878330078125" Y="1.141503662109" />
                  <Point X="-4.773516113281" Y="1.528298583984" />
                  <Point X="-4.025979248047" Y="1.429883544922" />
                  <Point X="-3.753266357422" Y="1.393980102539" />
                  <Point X="-3.731704589844" Y="1.395866699219" />
                  <Point X="-3.715954589844" Y="1.400832641602" />
                  <Point X="-3.670278564453" Y="1.41523425293" />
                  <Point X="-3.651534423828" Y="1.426056274414" />
                  <Point X="-3.639119873047" Y="1.443786132813" />
                  <Point X="-3.632800048828" Y="1.459043212891" />
                  <Point X="-3.614472412109" Y="1.503290283203" />
                  <Point X="-3.610714111328" Y="1.524605224609" />
                  <Point X="-3.616315917969" Y="1.54551159668" />
                  <Point X="-3.62394140625" Y="1.56016003418" />
                  <Point X="-3.646055664062" Y="1.602641235352" />
                  <Point X="-3.659968261719" Y="1.619221679688" />
                  <Point X="-4.292835449219" Y="2.104837890625" />
                  <Point X="-4.47610546875" Y="2.245466308594" />
                  <Point X="-4.411143554688" Y="2.356761962891" />
                  <Point X="-4.160014648438" Y="2.787006591797" />
                  <Point X="-4.05587109375" Y="2.920868164062" />
                  <Point X="-3.774671142578" Y="3.282310791016" />
                  <Point X="-3.326649658203" Y="3.023645263672" />
                  <Point X="-3.159156982422" Y="2.926943359375" />
                  <Point X="-3.138514648438" Y="2.920434814453" />
                  <Point X="-3.116579345703" Y="2.918515625" />
                  <Point X="-3.052965332031" Y="2.912950195312" />
                  <Point X="-3.031506591797" Y="2.915775390625" />
                  <Point X="-3.013252441406" Y="2.927404541016" />
                  <Point X="-2.997682617188" Y="2.942974365234" />
                  <Point X="-2.952528808594" Y="2.988127929688" />
                  <Point X="-2.940899414062" Y="3.006382324219" />
                  <Point X="-2.93807421875" Y="3.027841064453" />
                  <Point X="-2.939993408203" Y="3.049776611328" />
                  <Point X="-2.945558837891" Y="3.113390625" />
                  <Point X="-2.952067382812" Y="3.134032958984" />
                  <Point X="-3.232510009766" Y="3.619773681641" />
                  <Point X="-3.307278564453" Y="3.749276611328" />
                  <Point X="-3.190263671875" Y="3.838990966797" />
                  <Point X="-2.752876220703" Y="4.174331542969" />
                  <Point X="-2.588853759766" Y="4.265458496094" />
                  <Point X="-2.141548583984" Y="4.513971679688" />
                  <Point X="-2.01910144043" Y="4.354395507812" />
                  <Point X="-1.967826660156" Y="4.287573242188" />
                  <Point X="-1.951246948242" Y="4.273660644531" />
                  <Point X="-1.926833007812" Y="4.260951660156" />
                  <Point X="-1.856030761719" Y="4.22409375" />
                  <Point X="-1.835124267578" Y="4.2184921875" />
                  <Point X="-1.813808837891" Y="4.222250488281" />
                  <Point X="-1.788380126953" Y="4.232783691406" />
                  <Point X="-1.714634887695" Y="4.263330078125" />
                  <Point X="-1.696905273438" Y="4.275744628906" />
                  <Point X="-1.686083496094" Y="4.294488769531" />
                  <Point X="-1.677806762695" Y="4.320738769531" />
                  <Point X="-1.653804077148" Y="4.396865234375" />
                  <Point X="-1.651917602539" Y="4.418426757812" />
                  <Point X="-1.68380078125" Y="4.660604003906" />
                  <Point X="-1.689137573242" Y="4.701141113281" />
                  <Point X="-1.534319335938" Y="4.744546875" />
                  <Point X="-0.968094177246" Y="4.903296386719" />
                  <Point X="-0.769251098633" Y="4.926567871094" />
                  <Point X="-0.224199615479" Y="4.990358398438" />
                  <Point X="-0.091689376831" Y="4.495822753906" />
                  <Point X="-0.042140380859" Y="4.310903320312" />
                  <Point X="-0.024282117844" Y="4.284176757813" />
                  <Point X="0.006155934334" Y="4.273844238281" />
                  <Point X="0.036594039917" Y="4.284176757813" />
                  <Point X="0.054452251434" Y="4.310903320312" />
                  <Point X="0.198147842407" Y="4.847182617188" />
                  <Point X="0.236648422241" Y="4.990868652344" />
                  <Point X="0.365808441162" Y="4.977342285156" />
                  <Point X="0.860205810547" Y="4.925565429688" />
                  <Point X="1.024715209961" Y="4.885848144531" />
                  <Point X="1.508455932617" Y="4.769058105469" />
                  <Point X="1.615405395508" Y="4.730266601562" />
                  <Point X="1.931033691406" Y="4.615786132813" />
                  <Point X="2.034575805664" Y="4.567362792969" />
                  <Point X="2.33869921875" Y="4.425134277344" />
                  <Point X="2.438730224609" Y="4.366855957031" />
                  <Point X="2.732523681641" Y="4.19569140625" />
                  <Point X="2.826861816406" Y="4.128603515625" />
                  <Point X="3.068740234375" Y="3.956592773438" />
                  <Point X="2.450779541016" Y="2.886253173828" />
                  <Point X="2.229853515625" Y="2.503597900391" />
                  <Point X="2.224852050781" Y="2.491514892578" />
                  <Point X="2.219347167969" Y="2.470928955078" />
                  <Point X="2.203382324219" Y="2.411228515625" />
                  <Point X="2.204191162109" Y="2.374525146484" />
                  <Point X="2.210416015625" Y="2.322901123047" />
                  <Point X="2.218682128906" Y="2.3008125" />
                  <Point X="2.229696777344" Y="2.284579833984" />
                  <Point X="2.261639892578" Y="2.23750390625" />
                  <Point X="2.291172607422" Y="2.213188964844" />
                  <Point X="2.338248535156" Y="2.18124609375" />
                  <Point X="2.360336914062" Y="2.172980224609" />
                  <Point X="2.378137939453" Y="2.170833740234" />
                  <Point X="2.42976171875" Y="2.164608642578" />
                  <Point X="2.448664306641" Y="2.165946289062" />
                  <Point X="2.469250244141" Y="2.171451416016" />
                  <Point X="2.528950683594" Y="2.187416015625" />
                  <Point X="2.541033935547" Y="2.192417480469" />
                  <Point X="3.65076171875" Y="2.833119140625" />
                  <Point X="3.994248046875" Y="3.031430908203" />
                  <Point X="4.028323486328" Y="2.984073730469" />
                  <Point X="4.202591796875" Y="2.741880126953" />
                  <Point X="4.255184082031" Y="2.654970947266" />
                  <Point X="4.387512695312" Y="2.436296142578" />
                  <Point X="3.579486083984" Y="1.816275146484" />
                  <Point X="3.288616210938" Y="1.593082641602" />
                  <Point X="3.279371337891" Y="1.583833374023" />
                  <Point X="3.264555664062" Y="1.564505126953" />
                  <Point X="3.221589111328" Y="1.508451904297" />
                  <Point X="3.213119628906" Y="1.49150012207" />
                  <Point X="3.207600830078" Y="1.471765991211" />
                  <Point X="3.191595703125" Y="1.414535644531" />
                  <Point X="3.190779541016" Y="1.390965209961" />
                  <Point X="3.195309814453" Y="1.369008422852" />
                  <Point X="3.208448486328" Y="1.305332397461" />
                  <Point X="3.215646484375" Y="1.287955078125" />
                  <Point X="3.22796875" Y="1.269225830078" />
                  <Point X="3.263704101562" Y="1.214909667969" />
                  <Point X="3.280947998047" Y="1.198819824219" />
                  <Point X="3.298804443359" Y="1.188768066406" />
                  <Point X="3.350590087891" Y="1.159617431641" />
                  <Point X="3.368565429688" Y="1.153619628906" />
                  <Point X="3.392708740234" Y="1.150428710938" />
                  <Point X="3.462726074219" Y="1.141175048828" />
                  <Point X="3.475803710938" Y="1.14117175293" />
                  <Point X="4.529970703125" Y="1.279955566406" />
                  <Point X="4.848975097656" Y="1.32195324707" />
                  <Point X="4.864341308594" Y="1.258834472656" />
                  <Point X="4.939188476562" Y="0.951386047363" />
                  <Point X="4.95576171875" Y="0.84493737793" />
                  <Point X="4.997858398438" Y="0.574556152344" />
                  <Point X="4.074510986328" Y="0.327145965576" />
                  <Point X="3.741167724609" Y="0.237826919556" />
                  <Point X="3.729086914062" Y="0.232819412231" />
                  <Point X="3.705366943359" Y="0.219108703613" />
                  <Point X="3.636576904297" Y="0.179346878052" />
                  <Point X="3.622264892578" Y="0.166926574707" />
                  <Point X="3.608032958984" Y="0.148791732788" />
                  <Point X="3.566759033203" Y="0.096198890686" />
                  <Point X="3.556985107422" Y="0.07473513031" />
                  <Point X="3.552240966797" Y="0.049963748932" />
                  <Point X="3.538482910156" Y="-0.021875303268" />
                  <Point X="3.538482910156" Y="-0.04068478775" />
                  <Point X="3.543227050781" Y="-0.065456169128" />
                  <Point X="3.556985107422" Y="-0.137295227051" />
                  <Point X="3.566759033203" Y="-0.158758987427" />
                  <Point X="3.580990966797" Y="-0.176893981934" />
                  <Point X="3.622264892578" Y="-0.229486679077" />
                  <Point X="3.636576904297" Y="-0.241906967163" />
                  <Point X="3.660297119141" Y="-0.255617660522" />
                  <Point X="3.729086914062" Y="-0.295379486084" />
                  <Point X="3.741167724609" Y="-0.300387023926" />
                  <Point X="4.707887207031" Y="-0.559418579102" />
                  <Point X="4.998067871094" Y="-0.637172363281" />
                  <Point X="4.990223144531" Y="-0.689206298828" />
                  <Point X="4.948431640625" Y="-0.966399169922" />
                  <Point X="4.927198730469" Y="-1.059446533203" />
                  <Point X="4.874545898438" Y="-1.290178344727" />
                  <Point X="3.798566650391" Y="-1.148522949219" />
                  <Point X="3.411982177734" Y="-1.097628295898" />
                  <Point X="3.394836181641" Y="-1.098341186523" />
                  <Point X="3.348281982422" Y="-1.108459960938" />
                  <Point X="3.213271972656" Y="-1.137804931641" />
                  <Point X="3.1854453125" Y="-1.154697265625" />
                  <Point X="3.157306152344" Y="-1.188539794922" />
                  <Point X="3.075701171875" Y="-1.286685424805" />
                  <Point X="3.064357910156" Y="-1.314070556641" />
                  <Point X="3.060324951172" Y="-1.357897949219" />
                  <Point X="3.04862890625" Y="-1.485000854492" />
                  <Point X="3.056360595703" Y="-1.516621826172" />
                  <Point X="3.082124267578" Y="-1.556695556641" />
                  <Point X="3.156840820312" Y="-1.672912353516" />
                  <Point X="3.168460693359" Y="-1.685540649414" />
                  <Point X="4.06558203125" Y="-2.373926269531" />
                  <Point X="4.33907421875" Y="-2.583784179688" />
                  <Point X="4.321937988281" Y="-2.611512939453" />
                  <Point X="4.204133300781" Y="-2.802139404297" />
                  <Point X="4.160219238281" Y="-2.864534912109" />
                  <Point X="4.056688476562" Y="-3.011638427734" />
                  <Point X="3.097760009766" Y="-2.458000732422" />
                  <Point X="2.753454589844" Y="-2.259215576172" />
                  <Point X="2.737340820313" Y="-2.253312744141" />
                  <Point X="2.681934082031" Y="-2.243306396484" />
                  <Point X="2.521250488281" Y="-2.214287109375" />
                  <Point X="2.489077636719" Y="-2.219244873047" />
                  <Point X="2.443048095703" Y="-2.243469726562" />
                  <Point X="2.309559570312" Y="-2.313723876953" />
                  <Point X="2.288599853516" Y="-2.334683837891" />
                  <Point X="2.264374755859" Y="-2.380713378906" />
                  <Point X="2.194120849609" Y="-2.514201904297" />
                  <Point X="2.189163085938" Y="-2.546374511719" />
                  <Point X="2.199169433594" Y="-2.60178125" />
                  <Point X="2.228188720703" Y="-2.76246484375" />
                  <Point X="2.234091552734" Y="-2.778578613281" />
                  <Point X="2.810582275391" Y="-3.777089599609" />
                  <Point X="2.986673583984" Y="-4.082088623047" />
                  <Point X="2.975085205078" Y="-4.090366210938" />
                  <Point X="2.835298339844" Y="-4.190212402344" />
                  <Point X="2.786203125" Y="-4.221991210938" />
                  <Point X="2.679775878906" Y="-4.290879394531" />
                  <Point X="1.945914428711" Y="-3.334492675781" />
                  <Point X="1.683177612305" Y="-2.992087402344" />
                  <Point X="1.670549316406" Y="-2.980467529297" />
                  <Point X="1.615903198242" Y="-2.945335205078" />
                  <Point X="1.45742590332" Y="-2.843448730469" />
                  <Point X="1.425804931641" Y="-2.835717041016" />
                  <Point X="1.366040039062" Y="-2.841216796875" />
                  <Point X="1.192718017578" Y="-2.857166015625" />
                  <Point X="1.165332885742" Y="-2.868509277344" />
                  <Point X="1.119184082031" Y="-2.906880371094" />
                  <Point X="0.985349060059" Y="-3.018159667969" />
                  <Point X="0.968456726074" Y="-3.045986328125" />
                  <Point X="0.954658569336" Y="-3.109469238281" />
                  <Point X="0.91464251709" Y="-3.29357421875" />
                  <Point X="0.91392956543" Y="-3.310720214844" />
                  <Point X="1.077302490234" Y="-4.551661621094" />
                  <Point X="1.127642211914" Y="-4.934028320312" />
                  <Point X="1.126595581055" Y="-4.9342578125" />
                  <Point X="0.994346191406" Y="-4.963246582031" />
                  <Point X="0.948988586426" Y="-4.971486816406" />
                  <Point X="0.860200378418" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#142" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.049548971452" Y="4.540043761932" Z="0.65" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.65" />
                  <Point X="-0.781219238283" Y="5.007509184099" Z="0.65" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.65" />
                  <Point X="-1.553973044818" Y="4.823970338236" Z="0.65" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.65" />
                  <Point X="-1.739529051844" Y="4.685357439594" Z="0.65" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.65" />
                  <Point X="-1.731648567591" Y="4.367053989318" Z="0.65" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.65" />
                  <Point X="-1.813670313711" Y="4.310257594668" Z="0.65" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.65" />
                  <Point X="-1.909901257286" Y="4.33658213761" Z="0.65" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.65" />
                  <Point X="-1.985589778871" Y="4.41611368167" Z="0.65" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.65" />
                  <Point X="-2.619292722615" Y="4.34044628258" Z="0.65" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.65" />
                  <Point X="-3.226841738242" Y="3.909950934568" Z="0.65" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.65" />
                  <Point X="-3.281967312673" Y="3.626053758592" Z="0.65" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.65" />
                  <Point X="-2.995959124172" Y="3.076698881895" Z="0.65" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.65" />
                  <Point X="-3.039193704129" Y="3.009609863758" Z="0.65" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.65" />
                  <Point X="-3.118377583712" Y="2.999605575001" Z="0.65" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.65" />
                  <Point X="-3.307805589705" Y="3.098226636073" Z="0.65" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.65" />
                  <Point X="-4.101490492392" Y="2.982850552871" Z="0.65" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.65" />
                  <Point X="-4.460481739335" Y="2.413247024759" Z="0.65" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.65" />
                  <Point X="-4.329429504242" Y="2.096450160351" Z="0.65" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.65" />
                  <Point X="-3.674447896769" Y="1.568352761616" Z="0.65" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.65" />
                  <Point X="-3.685150159406" Y="1.50945733361" Z="0.65" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.65" />
                  <Point X="-3.737146063554" Y="1.47979848801" Z="0.65" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.65" />
                  <Point X="-4.025608932035" Y="1.510735874986" Z="0.65" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.65" />
                  <Point X="-4.932745647644" Y="1.185860937105" Z="0.65" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.65" />
                  <Point X="-5.037892524107" Y="0.598253431006" Z="0.65" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.65" />
                  <Point X="-4.679881185355" Y="0.344702860097" Z="0.65" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.65" />
                  <Point X="-3.555923849975" Y="0.034745854875" Z="0.65" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.65" />
                  <Point X="-3.54192877932" Y="0.007642666657" Z="0.65" />
                  <Point X="-3.539556741714" Y="0" Z="0.65" />
                  <Point X="-3.546435821815" Y="-0.022164284404" Z="0.65" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.65" />
                  <Point X="-3.569444745149" Y="-0.044130128259" Z="0.65" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.65" />
                  <Point X="-3.957006603418" Y="-0.151009196659" Z="0.65" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.65" />
                  <Point X="-5.002575542816" Y="-0.850435492984" Z="0.65" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.65" />
                  <Point X="-4.881712526674" Y="-1.384883157171" Z="0.65" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.65" />
                  <Point X="-4.429540672593" Y="-1.46621306238" Z="0.65" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.65" />
                  <Point X="-3.199466585888" Y="-1.318453339962" Z="0.65" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.65" />
                  <Point X="-3.198405326786" Y="-1.344569781307" Z="0.65" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.65" />
                  <Point X="-3.534354062682" Y="-1.608463855506" Z="0.65" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.65" />
                  <Point X="-4.284621619936" Y="-2.717676252689" Z="0.65" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.65" />
                  <Point X="-3.951241339277" Y="-3.182995139943" Z="0.65" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.65" />
                  <Point X="-3.531629769661" Y="-3.109048784063" Z="0.65" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.65" />
                  <Point X="-2.559939228856" Y="-2.568391240276" Z="0.65" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.65" />
                  <Point X="-2.746368229105" Y="-2.903448612831" Z="0.65" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.65" />
                  <Point X="-2.995460886088" Y="-4.096666913651" Z="0.65" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.65" />
                  <Point X="-2.563771506369" Y="-4.379789162672" Z="0.65" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.65" />
                  <Point X="-2.393453313158" Y="-4.374391832631" Z="0.65" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.65" />
                  <Point X="-2.034399973745" Y="-4.028280425266" Z="0.65" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.65" />
                  <Point X="-1.738047214159" Y="-3.99917306494" Z="0.65" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.65" />
                  <Point X="-1.485215352806" Y="-4.156489021572" Z="0.65" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.65" />
                  <Point X="-1.380398105731" Y="-4.435210154441" Z="0.65" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.65" />
                  <Point X="-1.377242541349" Y="-4.607146140631" Z="0.65" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.65" />
                  <Point X="-1.193220234479" Y="-4.936076158126" Z="0.65" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.65" />
                  <Point X="-0.894521469102" Y="-4.99884543735" Z="0.65" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.65" />
                  <Point X="-0.714956825476" Y="-4.630439301874" Z="0.65" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.65" />
                  <Point X="-0.295339764888" Y="-3.343358841085" Z="0.65" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.65" />
                  <Point X="-0.06496266539" Y="-3.224401382429" Z="0.65" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.65" />
                  <Point X="0.188396413972" Y="-3.262710662859" Z="0.65" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.65" />
                  <Point X="0.375106094544" Y="-3.458286914683" Z="0.65" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.65" />
                  <Point X="0.519798034926" Y="-3.902096727941" Z="0.65" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.65" />
                  <Point X="0.951769454981" Y="-4.989401625327" Z="0.65" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.65" />
                  <Point X="1.131145409099" Y="-4.951818206771" Z="0.65" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.65" />
                  <Point X="1.120718839102" Y="-4.513855103699" Z="0.65" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.65" />
                  <Point X="0.997361750065" Y="-3.088808860535" Z="0.65" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.65" />
                  <Point X="1.144994928741" Y="-2.91404667236" Z="0.65" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.65" />
                  <Point X="1.364465739511" Y="-2.859726192809" Z="0.65" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.65" />
                  <Point X="1.582707871373" Y="-2.956112924329" Z="0.65" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.65" />
                  <Point X="1.900090776153" Y="-3.333650567081" Z="0.65" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.65" />
                  <Point X="2.807216322698" Y="-4.232685101368" Z="0.65" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.65" />
                  <Point X="2.997990673943" Y="-4.099773059617" Z="0.65" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.65" />
                  <Point X="2.847727636269" Y="-3.720809455595" Z="0.65" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.65" />
                  <Point X="2.242217539128" Y="-2.56161496227" Z="0.65" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.65" />
                  <Point X="2.302465238736" Y="-2.372719598348" Z="0.65" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.65" />
                  <Point X="2.460178780024" Y="-2.256436071231" Z="0.65" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.65" />
                  <Point X="2.666891841243" Y="-2.261230470363" Z="0.65" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.65" />
                  <Point X="3.066604141511" Y="-2.470021864005" Z="0.65" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.65" />
                  <Point X="4.194951983017" Y="-2.86203209931" Z="0.65" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.65" />
                  <Point X="4.358315380285" Y="-2.606518163858" Z="0.65" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.65" />
                  <Point X="4.089864080339" Y="-2.302978472258" Z="0.65" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.65" />
                  <Point X="3.118026214253" Y="-1.498376252097" Z="0.65" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.65" />
                  <Point X="3.103958467322" Y="-1.331199694576" Z="0.65" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.65" />
                  <Point X="3.189596543585" Y="-1.18922664658" Z="0.65" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.65" />
                  <Point X="3.352745764072" Y="-1.126039149801" Z="0.65" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.65" />
                  <Point X="3.785884220169" Y="-1.166815217164" Z="0.65" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.65" />
                  <Point X="4.969789648516" Y="-1.039290515755" Z="0.65" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.65" />
                  <Point X="5.033508532318" Y="-0.665380410605" Z="0.65" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.65" />
                  <Point X="4.714671942979" Y="-0.479842265538" Z="0.65" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.65" />
                  <Point X="3.679162785091" Y="-0.181048833227" Z="0.65" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.65" />
                  <Point X="3.614168765949" Y="-0.114745497486" Z="0.65" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.65" />
                  <Point X="3.586178740795" Y="-0.024771473102" Z="0.65" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.65" />
                  <Point X="3.595192709629" Y="0.071839058128" Z="0.65" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.65" />
                  <Point X="3.641210672452" Y="0.149203241089" Z="0.65" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.65" />
                  <Point X="3.724232629262" Y="0.207100011015" Z="0.65" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.65" />
                  <Point X="4.081295874327" Y="0.310129667231" Z="0.65" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.65" />
                  <Point X="4.999010580588" Y="0.883909437875" Z="0.65" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.65" />
                  <Point X="4.906766374431" Y="1.301941390364" Z="0.65" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.65" />
                  <Point X="4.517288583205" Y="1.360807866533" Z="0.65" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.65" />
                  <Point X="3.393103966223" Y="1.231277741839" Z="0.65" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.65" />
                  <Point X="3.317175162106" Y="1.263619306154" Z="0.65" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.65" />
                  <Point X="3.263583218331" Y="1.32798716744" Z="0.65" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.65" />
                  <Point X="3.238122452822" Y="1.410392482505" Z="0.65" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.65" />
                  <Point X="3.249597125735" Y="1.489579580898" Z="0.65" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.65" />
                  <Point X="3.298082491123" Y="1.565366894508" Z="0.65" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.65" />
                  <Point X="3.603768093658" Y="1.807887443937" Z="0.65" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.65" />
                  <Point X="4.291805523022" Y="2.712137222266" Z="0.65" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.65" />
                  <Point X="4.062753072433" Y="3.044556552108" Z="0.65" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.65" />
                  <Point X="3.619605867174" Y="2.907700460247" Z="0.65" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.65" />
                  <Point X="2.45017749676" Y="2.251034096881" Z="0.65" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.65" />
                  <Point X="2.377967707085" Y="2.251754066833" Z="0.65" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.65" />
                  <Point X="2.313090776206" Y="2.285843520977" Z="0.65" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.65" />
                  <Point X="2.264915079258" Y="2.343934084176" Z="0.65" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.65" />
                  <Point X="2.247675635926" Y="2.411790736747" Z="0.65" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.65" />
                  <Point X="2.261493845776" Y="2.489292079122" Z="0.65" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.65" />
                  <Point X="2.487924892612" Y="2.892533214781" Z="0.65" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.65" />
                  <Point X="2.849682918537" Y="4.200630486783" Z="0.65" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.65" />
                  <Point X="2.457744198355" Y="4.441338757343" Z="0.65" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.65" />
                  <Point X="2.049601461394" Y="4.643934783701" Z="0.65" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.65" />
                  <Point X="1.62629803108" Y="4.808550627943" Z="0.65" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.65" />
                  <Point X="1.03029374554" Y="4.965731550357" Z="0.65" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.65" />
                  <Point X="0.364860388841" Y="5.058350271797" Z="0.65" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.65" />
                  <Point X="0.143695590101" Y="4.891403744019" Z="0.65" />
                  <Point X="0" Y="4.355124473572" Z="0.65" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>