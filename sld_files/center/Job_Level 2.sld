<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#119" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="337" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="0" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000475830078" Y="0.004715820312" />
                  <Width Value="9.783896972656" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.571387756348" Y="-3.542700683594" />
                  <Point X="0.563302062988" Y="-3.512524414063" />
                  <Point X="0.555903320312" Y="-3.493435546875" />
                  <Point X="0.545378295898" Y="-3.473105712891" />
                  <Point X="0.539058349609" Y="-3.462615478516" />
                  <Point X="0.378635467529" Y="-3.231476806641" />
                  <Point X="0.357109161377" Y="-3.207521728516" />
                  <Point X="0.328345794678" Y="-3.187322753906" />
                  <Point X="0.308494049072" Y="-3.178310058594" />
                  <Point X="0.297381164551" Y="-3.174081787109" />
                  <Point X="0.049135978699" Y="-3.097035888672" />
                  <Point X="0.020396892548" Y="-3.091593017578" />
                  <Point X="-0.011787228584" Y="-3.092229492188" />
                  <Point X="-0.031722167969" Y="-3.096063720703" />
                  <Point X="-0.041938423157" Y="-3.098623046875" />
                  <Point X="-0.290183288574" Y="-3.175669189453" />
                  <Point X="-0.319507995605" Y="-3.188986083984" />
                  <Point X="-0.34758480835" Y="-3.210508300781" />
                  <Point X="-0.362407470703" Y="-3.227088134766" />
                  <Point X="-0.369628417969" Y="-3.23623828125" />
                  <Point X="-0.530051330566" Y="-3.467377197266" />
                  <Point X="-0.538188964844" Y="-3.481573730469" />
                  <Point X="-0.55099005127" Y="-3.512524414063" />
                  <Point X="-0.916584594727" Y="-4.876941894531" />
                  <Point X="-1.079340087891" Y="-4.845350585938" />
                  <Point X="-1.081083496094" Y="-4.844901855469" />
                  <Point X="-1.24641796875" Y="-4.802362792969" />
                  <Point X="-1.215936523438" Y="-4.57083203125" />
                  <Point X="-1.214440063477" Y="-4.543627929688" />
                  <Point X="-1.216194580078" Y="-4.521204589844" />
                  <Point X="-1.21773046875" Y="-4.51008203125" />
                  <Point X="-1.277036132812" Y="-4.211932617188" />
                  <Point X="-1.287037719727" Y="-4.181756835937" />
                  <Point X="-1.305321655273" Y="-4.151684570312" />
                  <Point X="-1.320364013672" Y="-4.134963378906" />
                  <Point X="-1.328353149414" Y="-4.127074707031" />
                  <Point X="-1.556905395508" Y="-3.926639160156" />
                  <Point X="-1.583210327148" Y="-3.908787597656" />
                  <Point X="-1.616022705078" Y="-3.896060546875" />
                  <Point X="-1.638135742188" Y="-3.891951904297" />
                  <Point X="-1.649276611328" Y="-3.890556640625" />
                  <Point X="-1.952616577148" Y="-3.870674804688" />
                  <Point X="-1.984352661133" Y="-3.872525634766" />
                  <Point X="-2.0181328125" Y="-3.882403564453" />
                  <Point X="-2.038177490234" Y="-3.892605712891" />
                  <Point X="-2.047864990234" Y="-3.898280761719" />
                  <Point X="-2.300624267578" Y="-4.067169677734" />
                  <Point X="-2.312786621094" Y="-4.076822509766" />
                  <Point X="-2.335103515625" Y="-4.099462890625" />
                  <Point X="-2.480148925781" Y="-4.288489746094" />
                  <Point X="-2.801706298828" Y="-4.089389404297" />
                  <Point X="-2.804122558594" Y="-4.087529052734" />
                  <Point X="-3.104721923828" Y="-3.856077880859" />
                  <Point X="-2.438788085938" Y="-2.702646240234" />
                  <Point X="-2.423761474609" Y="-2.676619873047" />
                  <Point X="-2.412859375" Y="-2.647654296875" />
                  <Point X="-2.406588134766" Y="-2.616127929688" />
                  <Point X="-2.405605957031" Y="-2.584963867188" />
                  <Point X="-2.414766113281" Y="-2.555160400391" />
                  <Point X="-2.429333496094" Y="-2.525980957031" />
                  <Point X="-2.447155029297" Y="-2.501239013672" />
                  <Point X="-2.464153320312" Y="-2.484240722656" />
                  <Point X="-2.489310302734" Y="-2.466213134766" />
                  <Point X="-2.518139160156" Y="-2.45199609375" />
                  <Point X="-2.547757080078" Y="-2.443011474609" />
                  <Point X="-2.578690917969" Y="-2.444024169922" />
                  <Point X="-2.610217041016" Y="-2.450295166016" />
                  <Point X="-2.639183349609" Y="-2.461197265625" />
                  <Point X="-3.818024169922" Y="-3.141801269531" />
                  <Point X="-4.082860351562" Y="-2.793860351562" />
                  <Point X="-4.084588134766" Y="-2.790963378906" />
                  <Point X="-4.306142578125" Y="-2.419450439453" />
                  <Point X="-3.13224609375" Y="-1.5186875" />
                  <Point X="-3.105954589844" Y="-1.498513305664" />
                  <Point X="-3.084259033203" Y="-1.47510949707" />
                  <Point X="-3.066138671875" Y="-1.447377929688" />
                  <Point X="-3.053700927734" Y="-1.419232299805" />
                  <Point X="-3.046151855469" Y="-1.390085571289" />
                  <Point X="-3.043347900391" Y="-1.359653198242" />
                  <Point X="-3.045558105469" Y="-1.327979125977" />
                  <Point X="-3.052672851562" Y="-1.297963623047" />
                  <Point X="-3.069029785156" Y="-1.271809936523" />
                  <Point X="-3.090402099609" Y="-1.247538696289" />
                  <Point X="-3.113513427734" Y="-1.228448730469" />
                  <Point X="-3.139455078125" Y="-1.213180664062" />
                  <Point X="-3.168718261719" Y="-1.201956420898" />
                  <Point X="-3.200605712891" Y="-1.195474853516" />
                  <Point X="-3.231928955078" Y="-1.194383789062" />
                  <Point X="-4.732102050781" Y="-1.391885375977" />
                  <Point X="-4.834077148438" Y="-0.992657043457" />
                  <Point X="-4.834533691406" Y="-0.989464294434" />
                  <Point X="-4.892424316406" Y="-0.584698303223" />
                  <Point X="-3.562825927734" Y="-0.228433456421" />
                  <Point X="-3.532875976562" Y="-0.220408447266" />
                  <Point X="-3.516928222656" Y="-0.214563156128" />
                  <Point X="-3.487168212891" Y="-0.199080383301" />
                  <Point X="-3.459975585938" Y="-0.180207214355" />
                  <Point X="-3.437174560547" Y="-0.157848480225" />
                  <Point X="-3.417743896484" Y="-0.130991607666" />
                  <Point X="-3.403981445312" Y="-0.103465316772" />
                  <Point X="-3.394917236328" Y="-0.07426008606" />
                  <Point X="-3.390649658203" Y="-0.045514774323" />
                  <Point X="-3.390836181641" Y="-0.015271444321" />
                  <Point X="-3.395104003906" Y="0.01230280304" />
                  <Point X="-3.404168457031" Y="0.041508037567" />
                  <Point X="-3.418654296875" Y="0.070020500183" />
                  <Point X="-3.438458496094" Y="0.096664123535" />
                  <Point X="-3.460535644531" Y="0.118035758972" />
                  <Point X="-3.487728271484" Y="0.136909072876" />
                  <Point X="-3.501925292969" Y="0.145047164917" />
                  <Point X="-3.532875976562" Y="0.157848251343" />
                  <Point X="-4.89181640625" Y="0.521975158691" />
                  <Point X="-4.82448828125" Y="0.976972412109" />
                  <Point X="-4.823565917969" Y="0.980376098633" />
                  <Point X="-4.703551269531" Y="1.423267822266" />
                  <Point X="-3.787135009766" Y="1.302619384766" />
                  <Point X="-3.765666015625" Y="1.29979296875" />
                  <Point X="-3.744985839844" Y="1.299341674805" />
                  <Point X="-3.723424560547" Y="1.301228149414" />
                  <Point X="-3.703139160156" Y="1.305263183594" />
                  <Point X="-3.701899414062" Y="1.305654052734" />
                  <Point X="-3.641713134766" Y="1.324630737305" />
                  <Point X="-3.622779052734" Y="1.332961669922" />
                  <Point X="-3.604034667969" Y="1.343783691406" />
                  <Point X="-3.587353515625" Y="1.356014648438" />
                  <Point X="-3.573715087891" Y="1.37156628418" />
                  <Point X="-3.561300537109" Y="1.389296020508" />
                  <Point X="-3.551351806641" Y="1.407429931641" />
                  <Point X="-3.550854248047" Y="1.408630981445" />
                  <Point X="-3.526704345703" Y="1.466934204102" />
                  <Point X="-3.520915527344" Y="1.486793945312" />
                  <Point X="-3.517157226562" Y="1.508109130859" />
                  <Point X="-3.5158046875" Y="1.528749267578" />
                  <Point X="-3.518951171875" Y="1.549192871094" />
                  <Point X="-3.524552978516" Y="1.570099365234" />
                  <Point X="-3.532050537109" Y="1.589379150391" />
                  <Point X="-3.532650878906" Y="1.590532226562" />
                  <Point X="-3.561790283203" Y="1.646508789062" />
                  <Point X="-3.573280517578" Y="1.663704956055" />
                  <Point X="-3.587192626953" Y="1.680285400391" />
                  <Point X="-3.602135498047" Y="1.694590087891" />
                  <Point X="-4.351860351563" Y="2.269874023438" />
                  <Point X="-4.081153564453" Y="2.733659667969" />
                  <Point X="-4.078716552734" Y="2.736791992188" />
                  <Point X="-3.750504882813" Y="3.158661865234" />
                  <Point X="-3.219842773438" Y="2.852283935547" />
                  <Point X="-3.206657226562" Y="2.844671142578" />
                  <Point X="-3.187725097656" Y="2.836340820312" />
                  <Point X="-3.167082519531" Y="2.82983203125" />
                  <Point X="-3.146797119141" Y="2.825796630859" />
                  <Point X="-3.1450703125" Y="2.825645507812" />
                  <Point X="-3.061247802734" Y="2.818312011719" />
                  <Point X="-3.040564941406" Y="2.818762939453" />
                  <Point X="-3.019106201172" Y="2.821588134766" />
                  <Point X="-2.999014160156" Y="2.826504882812" />
                  <Point X="-2.980462402344" Y="2.835653564453" />
                  <Point X="-2.962208251953" Y="2.847282958984" />
                  <Point X="-2.946083984375" Y="2.860222900391" />
                  <Point X="-2.944858154297" Y="2.861448486328" />
                  <Point X="-2.885360351562" Y="2.920946289062" />
                  <Point X="-2.872406982422" Y="2.937083984375" />
                  <Point X="-2.860777587891" Y="2.955338134766" />
                  <Point X="-2.85162890625" Y="2.973889892578" />
                  <Point X="-2.846712158203" Y="2.993981933594" />
                  <Point X="-2.843886962891" Y="3.015440673828" />
                  <Point X="-2.843435791016" Y="3.036122314453" />
                  <Point X="-2.843586914062" Y="3.037849365234" />
                  <Point X="-2.850920410156" Y="3.121671875" />
                  <Point X="-2.854955566406" Y="3.141957275391" />
                  <Point X="-2.861463867188" Y="3.162599365234" />
                  <Point X="-2.869794677734" Y="3.181532714844" />
                  <Point X="-3.183333007812" Y="3.724596923828" />
                  <Point X="-2.700625" Y="4.094684326172" />
                  <Point X="-2.6967890625" Y="4.096815429688" />
                  <Point X="-2.167036865234" Y="4.391134277344" />
                  <Point X="-2.047232666016" Y="4.235002441406" />
                  <Point X="-2.043196044922" Y="4.229741699219" />
                  <Point X="-2.028894042969" Y="4.214801269531" />
                  <Point X="-2.012313232422" Y="4.200887695312" />
                  <Point X="-1.995113769531" Y="4.189395019531" />
                  <Point X="-1.993191894531" Y="4.18839453125" />
                  <Point X="-1.899897705078" Y="4.139828125" />
                  <Point X="-1.880614501953" Y="4.132329589844" />
                  <Point X="-1.859703613281" Y="4.126727539062" />
                  <Point X="-1.839255859375" Y="4.123581542969" />
                  <Point X="-1.818612060547" Y="4.124937011719" />
                  <Point X="-1.797293212891" Y="4.128698730469" />
                  <Point X="-1.777410766602" Y="4.134499511719" />
                  <Point X="-1.775409179688" Y="4.135329589844" />
                  <Point X="-1.678280273438" Y="4.175561523437" />
                  <Point X="-1.660144775391" Y="4.185510742187" />
                  <Point X="-1.642415161133" Y="4.197925292969" />
                  <Point X="-1.626863891602" Y="4.211563476563" />
                  <Point X="-1.614633056641" Y="4.228244628906" />
                  <Point X="-1.603811157227" Y="4.246988769531" />
                  <Point X="-1.595481323242" Y="4.265918457031" />
                  <Point X="-1.594829711914" Y="4.267984863281" />
                  <Point X="-1.563201904297" Y="4.368294921875" />
                  <Point X="-1.559165649414" Y="4.388583984375" />
                  <Point X="-1.557279052734" Y="4.41014453125" />
                  <Point X="-1.557730224609" Y="4.430825683594" />
                  <Point X="-1.584202026367" Y="4.631897949219" />
                  <Point X="-0.949634460449" Y="4.80980859375" />
                  <Point X="-0.94499407959" Y="4.8103515625" />
                  <Point X="-0.294710876465" Y="4.886458007812" />
                  <Point X="-0.137803939819" Y="4.300873046875" />
                  <Point X="-0.133903381348" Y="4.286315429688" />
                  <Point X="-0.12112991333" Y="4.258124023438" />
                  <Point X="-0.103271614075" Y="4.231397460938" />
                  <Point X="-0.082114608765" Y="4.20880859375" />
                  <Point X="-0.054819355011" Y="4.19421875" />
                  <Point X="-0.024381278992" Y="4.183886230469" />
                  <Point X="0.006155912876" Y="4.178844238281" />
                  <Point X="0.03669311142" Y="4.183886230469" />
                  <Point X="0.067131187439" Y="4.19421875" />
                  <Point X="0.094426551819" Y="4.20880859375" />
                  <Point X="0.115583602905" Y="4.231397460938" />
                  <Point X="0.133441741943" Y="4.258124023438" />
                  <Point X="0.146215209961" Y="4.286315429688" />
                  <Point X="0.307419342041" Y="4.8879375" />
                  <Point X="0.84403918457" Y="4.831738769531" />
                  <Point X="0.847890380859" Y="4.830809082031" />
                  <Point X="1.481031494141" Y="4.67794921875" />
                  <Point X="1.482081665039" Y="4.677568359375" />
                  <Point X="1.894644042969" Y="4.527928710938" />
                  <Point X="1.897070800781" Y="4.526793945312" />
                  <Point X="2.294574462891" Y="4.34089453125" />
                  <Point X="2.296952392578" Y="4.339509277344" />
                  <Point X="2.680979248047" Y="4.115774414062" />
                  <Point X="2.683193115234" Y="4.114200195312" />
                  <Point X="2.943260253906" Y="3.929254638672" />
                  <Point X="2.164972900391" Y="2.581221435547" />
                  <Point X="2.147581054688" Y="2.55109765625" />
                  <Point X="2.141544921875" Y="2.538620605469" />
                  <Point X="2.132643554688" Y="2.514436035156" />
                  <Point X="2.111607177734" Y="2.435770263672" />
                  <Point X="2.108539306641" Y="2.4166875" />
                  <Point X="2.107370605469" Y="2.396383544922" />
                  <Point X="2.107896728516" Y="2.379551513672" />
                  <Point X="2.116099121094" Y="2.311528076172" />
                  <Point X="2.121442138672" Y="2.289604248047" />
                  <Point X="2.129708251953" Y="2.267515869141" />
                  <Point X="2.140074462891" Y="2.247466308594" />
                  <Point X="2.140941650391" Y="2.246188476562" />
                  <Point X="2.183032226563" Y="2.184157714844" />
                  <Point X="2.195430664062" Y="2.169376708984" />
                  <Point X="2.210008544922" Y="2.155209228516" />
                  <Point X="2.222876953125" Y="2.144725097656" />
                  <Point X="2.284907714844" Y="2.102634765625" />
                  <Point X="2.304945800781" Y="2.092274658203" />
                  <Point X="2.327026855469" Y="2.084009521484" />
                  <Point X="2.348952880859" Y="2.078664550781" />
                  <Point X="2.350361572266" Y="2.078494628906" />
                  <Point X="2.418385009766" Y="2.070292236328" />
                  <Point X="2.437935546875" Y="2.0699609375" />
                  <Point X="2.458465087891" Y="2.071733886719" />
                  <Point X="2.474833007813" Y="2.074606201172" />
                  <Point X="2.553492431641" Y="2.095640625" />
                  <Point X="2.565282470703" Y="2.099637939453" />
                  <Point X="2.588533691406" Y="2.110144775391" />
                  <Point X="3.967326660156" Y="2.906191162109" />
                  <Point X="4.123270507813" Y="2.689464599609" />
                  <Point X="4.124504882813" Y="2.687425048828" />
                  <Point X="4.262198730469" Y="2.459884521484" />
                  <Point X="3.253682128906" Y="1.686021606445" />
                  <Point X="3.230783935547" Y="1.668451171875" />
                  <Point X="3.220310546875" Y="1.659107788086" />
                  <Point X="3.202807373047" Y="1.640106323242" />
                  <Point X="3.146191650391" Y="1.566246459961" />
                  <Point X="3.135983154297" Y="1.549641967773" />
                  <Point X="3.127079101562" Y="1.531136352539" />
                  <Point X="3.1211953125" Y="1.515532348633" />
                  <Point X="3.100105957031" Y="1.440121459961" />
                  <Point X="3.096652587891" Y="1.417823120117" />
                  <Point X="3.095836425781" Y="1.394253051758" />
                  <Point X="3.097739746094" Y="1.371765991211" />
                  <Point X="3.098096435547" Y="1.370037475586" />
                  <Point X="3.115408691406" Y="1.286133178711" />
                  <Point X="3.121278564453" Y="1.267564819336" />
                  <Point X="3.129446533203" Y="1.248712646484" />
                  <Point X="3.137252685547" Y="1.234265625" />
                  <Point X="3.184340332031" Y="1.162694946289" />
                  <Point X="3.198893554688" Y="1.145450439453" />
                  <Point X="3.216137451172" Y="1.129360717773" />
                  <Point X="3.23434375" Y="1.116036621094" />
                  <Point X="3.235749267578" Y="1.115245361328" />
                  <Point X="3.303985839844" Y="1.076834228516" />
                  <Point X="3.322097167969" Y="1.068991088867" />
                  <Point X="3.341973144531" Y="1.062741943359" />
                  <Point X="3.358018798828" Y="1.05918737793" />
                  <Point X="3.450278808594" Y="1.046994018555" />
                  <Point X="3.462702148438" Y="1.046175048828" />
                  <Point X="3.488203369141" Y="1.04698449707" />
                  <Point X="4.776839355469" Y="1.21663684082" />
                  <Point X="4.845935546875" Y="0.932811462402" />
                  <Point X="4.846324707031" Y="0.930312561035" />
                  <Point X="4.890864746094" Y="0.644238525391" />
                  <Point X="3.742821777344" Y="0.336621246338" />
                  <Point X="3.716579833984" Y="0.32958984375" />
                  <Point X="3.703170166016" Y="0.324896942139" />
                  <Point X="3.679678710938" Y="0.313988677979" />
                  <Point X="3.589035888672" Y="0.261595489502" />
                  <Point X="3.573129638672" Y="0.250053649902" />
                  <Point X="3.557697265625" Y="0.236205657959" />
                  <Point X="3.546410400391" Y="0.22414894104" />
                  <Point X="3.492024902344" Y="0.154848876953" />
                  <Point X="3.480301025391" Y="0.135569213867" />
                  <Point X="3.470527099609" Y="0.114105445862" />
                  <Point X="3.463681640625" Y="0.092608131409" />
                  <Point X="3.463308105469" Y="0.090658233643" />
                  <Point X="3.445179443359" Y="-0.004002220154" />
                  <Point X="3.443498291016" Y="-0.02358439827" />
                  <Point X="3.443871826172" Y="-0.044343864441" />
                  <Point X="3.445552001953" Y="-0.060503772736" />
                  <Point X="3.463680664062" Y="-0.155164230347" />
                  <Point X="3.470527099609" Y="-0.176665481567" />
                  <Point X="3.480301025391" Y="-0.198129257202" />
                  <Point X="3.492024902344" Y="-0.2174087677" />
                  <Point X="3.493145263672" Y="-0.218836502075" />
                  <Point X="3.547530761719" Y="-0.288136566162" />
                  <Point X="3.561402587891" Y="-0.302430389404" />
                  <Point X="3.57758203125" Y="-0.315930023193" />
                  <Point X="3.590903076172" Y="-0.325234924316" />
                  <Point X="3.681545654297" Y="-0.377628112793" />
                  <Point X="3.692709716797" Y="-0.38313885498" />
                  <Point X="3.716579589844" Y="-0.392150054932" />
                  <Point X="4.89147265625" Y="-0.706961425781" />
                  <Point X="4.855022460938" Y="-0.948726013184" />
                  <Point X="4.8545234375" Y="-0.950913330078" />
                  <Point X="4.801173828125" Y="-1.184698974609" />
                  <Point X="3.454815185547" Y="-1.007447509766" />
                  <Point X="3.424382080078" Y="-1.003441040039" />
                  <Point X="3.405109375" Y="-1.002877258301" />
                  <Point X="3.384298583984" Y="-1.00438684082" />
                  <Point X="3.370993896484" Y="-1.006305297852" />
                  <Point X="3.193094482422" Y="-1.044972412109" />
                  <Point X="3.161443603516" Y="-1.058188232422" />
                  <Point X="3.131401611328" Y="-1.077744750977" />
                  <Point X="3.110182128906" Y="-1.096624389648" />
                  <Point X="3.002653320312" Y="-1.225948364258" />
                  <Point X="2.986842773438" Y="-1.253082275391" />
                  <Point X="2.975181884766" Y="-1.283917724609" />
                  <Point X="2.969439941406" Y="-1.308815917969" />
                  <Point X="2.954028564453" Y="-1.476295776367" />
                  <Point X="2.955109130859" Y="-1.508482543945" />
                  <Point X="2.963745361328" Y="-1.541597045898" />
                  <Point X="2.972284912109" Y="-1.559915649414" />
                  <Point X="2.978478515625" Y="-1.571151489258" />
                  <Point X="3.076930419922" Y="-1.724287231445" />
                  <Point X="3.086932861328" Y="-1.737239013672" />
                  <Point X="3.110628662109" Y="-1.760909301758" />
                  <Point X="4.213122558594" Y="-2.606883300781" />
                  <Point X="4.124811523438" Y="-2.749783935547" />
                  <Point X="4.123779296875" Y="-2.751250732422" />
                  <Point X="4.028981445312" Y="-2.8859453125" />
                  <Point X="2.828059570312" Y="-2.192592529297" />
                  <Point X="2.800954833984" Y="-2.176943359375" />
                  <Point X="2.782961669922" Y="-2.168914306641" />
                  <Point X="2.762485839844" Y="-2.162223632813" />
                  <Point X="2.749862792969" Y="-2.159037597656" />
                  <Point X="2.538134277344" Y="-2.120799560547" />
                  <Point X="2.505973388672" Y="-2.119082275391" />
                  <Point X="2.47203515625" Y="-2.124884765625" />
                  <Point X="2.452739746094" Y="-2.131962158203" />
                  <Point X="2.441209472656" Y="-2.137083740234" />
                  <Point X="2.265315185547" Y="-2.229655761719" />
                  <Point X="2.239999755859" Y="-2.249021240234" />
                  <Point X="2.217133056641" Y="-2.273604492188" />
                  <Point X="2.202624755859" Y="-2.294062744141" />
                  <Point X="2.110052734375" Y="-2.469957275391" />
                  <Point X="2.098733642578" Y="-2.500108886719" />
                  <Point X="2.094059326172" Y="-2.534422607422" />
                  <Point X="2.095059570312" Y="-2.555285644531" />
                  <Point X="2.096463134766" Y="-2.567620117188" />
                  <Point X="2.134701171875" Y="-2.779348632812" />
                  <Point X="2.138985595703" Y="-2.795141845703" />
                  <Point X="2.151819091797" Y="-2.826078613281" />
                  <Point X="2.861283447266" Y="-4.054906738281" />
                  <Point X="2.781850830078" Y="-4.111643554688" />
                  <Point X="2.780698974609" Y="-4.112389160156" />
                  <Point X="2.701765136719" Y="-4.163481445312" />
                  <Point X="1.779229492188" Y="-2.961209960938" />
                  <Point X="1.758546020508" Y="-2.934254882812" />
                  <Point X="1.744691650391" Y="-2.919692382812" />
                  <Point X="1.727761962891" Y="-2.905306884766" />
                  <Point X="1.717622314453" Y="-2.897791503906" />
                  <Point X="1.508800537109" Y="-2.763538574219" />
                  <Point X="1.479745361328" Y="-2.749644775391" />
                  <Point X="1.445794311523" Y="-2.742003173828" />
                  <Point X="1.424632080078" Y="-2.741215820312" />
                  <Point X="1.41239465332" Y="-2.741549804688" />
                  <Point X="1.184012817383" Y="-2.762565673828" />
                  <Point X="1.152798217773" Y="-2.770960449219" />
                  <Point X="1.121779907227" Y="-2.78532421875" />
                  <Point X="1.100962768555" Y="-2.798481689453" />
                  <Point X="0.924611938477" Y="-2.945111572266" />
                  <Point X="0.902616027832" Y="-2.968636230469" />
                  <Point X="0.885033935547" Y="-2.998919921875" />
                  <Point X="0.877829528809" Y="-3.019244140625" />
                  <Point X="0.874538085938" Y="-3.030806396484" />
                  <Point X="0.821810058594" Y="-3.273396728516" />
                  <Point X="0.819724609375" Y="-3.289627441406" />
                  <Point X="0.819742370605" Y="-3.323120117188" />
                  <Point X="1.022065246582" Y="-4.859915527344" />
                  <Point X="0.975674560547" Y="-4.870083984375" />
                  <Point X="0.974602905273" Y="-4.870278808594" />
                  <Point X="0.929315551758" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058428955078" Y="-4.75263671875" />
                  <Point X="-1.141246459961" Y="-4.731328613281" />
                  <Point X="-1.121749267578" Y="-4.583231933594" />
                  <Point X="-1.121079833984" Y="-4.576049804688" />
                  <Point X="-1.119583496094" Y="-4.548845703125" />
                  <Point X="-1.119729492188" Y="-4.536217285156" />
                  <Point X="-1.121484008789" Y="-4.513793945312" />
                  <Point X="-1.124555908203" Y="-4.491548339844" />
                  <Point X="-1.183861572266" Y="-4.193398925781" />
                  <Point X="-1.186860229492" Y="-4.182044433594" />
                  <Point X="-1.196861816406" Y="-4.151868652344" />
                  <Point X="-1.205863769531" Y="-4.132403320312" />
                  <Point X="-1.224147583008" Y="-4.102331054688" />
                  <Point X="-1.234694458008" Y="-4.088148681641" />
                  <Point X="-1.249736938477" Y="-4.071427490234" />
                  <Point X="-1.265715209961" Y="-4.055649902344" />
                  <Point X="-1.494267456055" Y="-3.855214355469" />
                  <Point X="-1.503559204102" Y="-3.848031494141" />
                  <Point X="-1.529864013672" Y="-3.830179931641" />
                  <Point X="-1.548856201172" Y="-3.820216796875" />
                  <Point X="-1.581668579102" Y="-3.807489746094" />
                  <Point X="-1.598668579102" Y="-3.802659179688" />
                  <Point X="-1.620781616211" Y="-3.798550537109" />
                  <Point X="-1.643063354492" Y="-3.795760009766" />
                  <Point X="-1.946403198242" Y="-3.775878173828" />
                  <Point X="-1.958147583008" Y="-3.7758359375" />
                  <Point X="-1.989883666992" Y="-3.777686767578" />
                  <Point X="-2.01101574707" Y="-3.781343994141" />
                  <Point X="-2.044795898438" Y="-3.791221923828" />
                  <Point X="-2.061224609375" Y="-3.797739013672" />
                  <Point X="-2.081269287109" Y="-3.807941162109" />
                  <Point X="-2.100644287109" Y="-3.819291259766" />
                  <Point X="-2.353403564453" Y="-3.988180175781" />
                  <Point X="-2.359682373047" Y="-3.9927578125" />
                  <Point X="-2.380443359375" Y="-4.010132324219" />
                  <Point X="-2.402760253906" Y="-4.032772705078" />
                  <Point X="-2.410472167969" Y="-4.041630615234" />
                  <Point X="-2.503202636719" Y="-4.162479003906" />
                  <Point X="-2.747582275391" Y="-4.011165527344" />
                  <Point X="-2.980862792969" Y="-3.831547607422" />
                  <Point X="-2.356515625" Y="-2.750146240234" />
                  <Point X="-2.341489013672" Y="-2.724119873047" />
                  <Point X="-2.334850585938" Y="-2.710084228516" />
                  <Point X="-2.323948486328" Y="-2.681118652344" />
                  <Point X="-2.319684814453" Y="-2.666188720703" />
                  <Point X="-2.313413574219" Y="-2.634662353516" />
                  <Point X="-2.311635253906" Y="-2.619120605469" />
                  <Point X="-2.310653076172" Y="-2.587956542969" />
                  <Point X="-2.314798339844" Y="-2.557053955078" />
                  <Point X="-2.323958496094" Y="-2.527250488281" />
                  <Point X="-2.32976953125" Y="-2.512727294922" />
                  <Point X="-2.344336914062" Y="-2.483547851562" />
                  <Point X="-2.352248535156" Y="-2.47045703125" />
                  <Point X="-2.370070068359" Y="-2.445715087891" />
                  <Point X="-2.379979980469" Y="-2.434063964844" />
                  <Point X="-2.396978271484" Y="-2.417065673828" />
                  <Point X="-2.408817138672" Y="-2.407020751953" />
                  <Point X="-2.433974121094" Y="-2.388993164062" />
                  <Point X="-2.447292236328" Y="-2.381010498047" />
                  <Point X="-2.47612109375" Y="-2.366793457031" />
                  <Point X="-2.490561767578" Y="-2.361086914062" />
                  <Point X="-2.5201796875" Y="-2.352102294922" />
                  <Point X="-2.550865478516" Y="-2.348062255859" />
                  <Point X="-2.581799316406" Y="-2.349074951172" />
                  <Point X="-2.597224609375" Y="-2.350849609375" />
                  <Point X="-2.628750732422" Y="-2.357120605469" />
                  <Point X="-2.643680664062" Y="-2.361384033203" />
                  <Point X="-2.672646972656" Y="-2.372286132812" />
                  <Point X="-2.686683349609" Y="-2.378924804688" />
                  <Point X="-3.793089111328" Y="-3.017708251953" />
                  <Point X="-4.004016601563" Y="-2.740592285156" />
                  <Point X="-4.181265136719" Y="-2.443373779297" />
                  <Point X="-3.074413818359" Y="-1.594056030273" />
                  <Point X="-3.048122314453" Y="-1.573881835938" />
                  <Point X="-3.036284912109" Y="-1.56309765625" />
                  <Point X="-3.014589355469" Y="-1.539693847656" />
                  <Point X="-3.004731445313" Y="-1.527074462891" />
                  <Point X="-2.986611083984" Y="-1.499342895508" />
                  <Point X="-2.979244873047" Y="-1.485776855469" />
                  <Point X="-2.966807128906" Y="-1.457631225586" />
                  <Point X="-2.961735595703" Y="-1.443051513672" />
                  <Point X="-2.954186523438" Y="-1.413904785156" />
                  <Point X="-2.951552490234" Y="-1.398801635742" />
                  <Point X="-2.948748535156" Y="-1.368369262695" />
                  <Point X="-2.948578369141" Y="-1.353040039062" />
                  <Point X="-2.950788574219" Y="-1.321366088867" />
                  <Point X="-2.953119384766" Y="-1.306067871094" />
                  <Point X="-2.960234130859" Y="-1.276052368164" />
                  <Point X="-2.972128173828" Y="-1.247589599609" />
                  <Point X="-2.988485107422" Y="-1.221435791016" />
                  <Point X="-2.997731933594" Y="-1.209027709961" />
                  <Point X="-3.019104248047" Y="-1.184756469727" />
                  <Point X="-3.029902099609" Y="-1.174294189453" />
                  <Point X="-3.053013427734" Y="-1.155204223633" />
                  <Point X="-3.065327148438" Y="-1.146576416016" />
                  <Point X="-3.091268798828" Y="-1.131308349609" />
                  <Point X="-3.10543359375" Y="-1.124481567383" />
                  <Point X="-3.134696777344" Y="-1.113257446289" />
                  <Point X="-3.149795166016" Y="-1.108860107422" />
                  <Point X="-3.181682617188" Y="-1.102378540039" />
                  <Point X="-3.197298583984" Y="-1.100532470703" />
                  <Point X="-3.228621826172" Y="-1.09944140625" />
                  <Point X="-3.244328857422" Y="-1.100196533203" />
                  <Point X="-4.660920898438" Y="-1.286694458008" />
                  <Point X="-4.740761230469" Y="-0.974121582031" />
                  <Point X="-4.786452148438" Y="-0.654654418945" />
                  <Point X="-3.538238037109" Y="-0.320196350098" />
                  <Point X="-3.508288085938" Y="-0.312171447754" />
                  <Point X="-3.500182617188" Y="-0.309605773926" />
                  <Point X="-3.473082763672" Y="-0.29883996582" />
                  <Point X="-3.443322753906" Y="-0.283357177734" />
                  <Point X="-3.433001220703" Y="-0.277124816895" />
                  <Point X="-3.40580859375" Y="-0.258251647949" />
                  <Point X="-3.393461425781" Y="-0.248036941528" />
                  <Point X="-3.370660400391" Y="-0.225678268433" />
                  <Point X="-3.360206298828" Y="-0.213534133911" />
                  <Point X="-3.340775634766" Y="-0.186677230835" />
                  <Point X="-3.332772460938" Y="-0.173475204468" />
                  <Point X="-3.319010009766" Y="-0.145948913574" />
                  <Point X="-3.313250732422" Y="-0.131624633789" />
                  <Point X="-3.304186523438" Y="-0.102419494629" />
                  <Point X="-3.300947265625" Y="-0.088210990906" />
                  <Point X="-3.2966796875" Y="-0.059465686798" />
                  <Point X="-3.295651367188" Y="-0.044928878784" />
                  <Point X="-3.295837890625" Y="-0.014685605049" />
                  <Point X="-3.296954101562" Y="-0.000740763187" />
                  <Point X="-3.301221923828" Y="0.02683354187" />
                  <Point X="-3.304373535156" Y="0.040462856293" />
                  <Point X="-3.313437988281" Y="0.069668151855" />
                  <Point X="-3.319472412109" Y="0.084538024902" />
                  <Point X="-3.333958251953" Y="0.113050582886" />
                  <Point X="-3.342409667969" Y="0.126692977905" />
                  <Point X="-3.362213867188" Y="0.153336608887" />
                  <Point X="-3.3723828125" Y="0.164921020508" />
                  <Point X="-3.394459960938" Y="0.186292694092" />
                  <Point X="-3.406368408203" Y="0.196079956055" />
                  <Point X="-3.433561035156" Y="0.214953292847" />
                  <Point X="-3.440483398438" Y="0.219328292847" />
                  <Point X="-3.465616699219" Y="0.232834854126" />
                  <Point X="-3.496567382812" Y="0.245635894775" />
                  <Point X="-3.508288085938" Y="0.249611251831" />
                  <Point X="-4.785446289062" Y="0.591824645996" />
                  <Point X="-4.731331542969" Y="0.957526794434" />
                  <Point X="-4.633586425781" Y="1.318237060547" />
                  <Point X="-3.799534912109" Y="1.208432128906" />
                  <Point X="-3.778065917969" Y="1.205605712891" />
                  <Point X="-3.767738769531" Y="1.204815551758" />
                  <Point X="-3.74705859375" Y="1.204364257812" />
                  <Point X="-3.736705566406" Y="1.20470324707" />
                  <Point X="-3.715144287109" Y="1.20658972168" />
                  <Point X="-3.704890869141" Y="1.208053588867" />
                  <Point X="-3.68460546875" Y="1.212088623047" />
                  <Point X="-3.673333740234" Y="1.215050537109" />
                  <Point X="-3.613147460938" Y="1.23402722168" />
                  <Point X="-3.603453125" Y="1.23767565918" />
                  <Point X="-3.584519042969" Y="1.246006591797" />
                  <Point X="-3.575279296875" Y="1.250689208984" />
                  <Point X="-3.556534912109" Y="1.261511230469" />
                  <Point X="-3.547860839844" Y="1.267171020508" />
                  <Point X="-3.5311796875" Y="1.279402099609" />
                  <Point X="-3.515928710938" Y="1.293376831055" />
                  <Point X="-3.502290283203" Y="1.308928466797" />
                  <Point X="-3.495895751953" Y="1.317076416016" />
                  <Point X="-3.483481201172" Y="1.334806152344" />
                  <Point X="-3.47801171875" Y="1.343601806641" />
                  <Point X="-3.468062988281" Y="1.361735473633" />
                  <Point X="-3.463087402344" Y="1.372271850586" />
                  <Point X="-3.4389375" Y="1.430575073242" />
                  <Point X="-3.4355" Y="1.440349487305" />
                  <Point X="-3.429711181641" Y="1.460209228516" />
                  <Point X="-3.427358642578" Y="1.470297973633" />
                  <Point X="-3.423600341797" Y="1.49161315918" />
                  <Point X="-3.422360595703" Y="1.501896972656" />
                  <Point X="-3.421008056641" Y="1.522537231445" />
                  <Point X="-3.421910400391" Y="1.543200561523" />
                  <Point X="-3.425056884766" Y="1.563644165039" />
                  <Point X="-3.427188232422" Y="1.573780395508" />
                  <Point X="-3.432790039062" Y="1.594686889648" />
                  <Point X="-3.436012207031" Y="1.60453125" />
                  <Point X="-3.443509765625" Y="1.623811035156" />
                  <Point X="-3.448387451172" Y="1.634403442383" />
                  <Point X="-3.477526855469" Y="1.690380004883" />
                  <Point X="-3.482801025391" Y="1.699288330078" />
                  <Point X="-3.494291259766" Y="1.71648449707" />
                  <Point X="-3.500505126953" Y="1.724768432617" />
                  <Point X="-3.514417236328" Y="1.741348876953" />
                  <Point X="-3.521498779297" Y="1.74891003418" />
                  <Point X="-3.536441650391" Y="1.76321472168" />
                  <Point X="-3.544303222656" Y="1.769958618164" />
                  <Point X="-4.227614746094" Y="2.294281982422" />
                  <Point X="-4.002292724609" Y="2.6803125" />
                  <Point X="-3.726338378906" Y="3.035012695312" />
                  <Point X="-3.267342773438" Y="2.770011474609" />
                  <Point X="-3.244918212891" Y="2.757716552734" />
                  <Point X="-3.225986083984" Y="2.749386230469" />
                  <Point X="-3.21629296875" Y="2.745738037109" />
                  <Point X="-3.195650390625" Y="2.739229248047" />
                  <Point X="-3.185617675781" Y="2.736657714844" />
                  <Point X="-3.165332275391" Y="2.732622314453" />
                  <Point X="-3.153352783203" Y="2.731007324219" />
                  <Point X="-3.069530273438" Y="2.723673828125" />
                  <Point X="-3.059177001953" Y="2.723334472656" />
                  <Point X="-3.038494140625" Y="2.723785400391" />
                  <Point X="-3.028164550781" Y="2.724575683594" />
                  <Point X="-3.006705810547" Y="2.727400878906" />
                  <Point X="-2.996524902344" Y="2.729310791016" />
                  <Point X="-2.976432861328" Y="2.734227539062" />
                  <Point X="-2.956996826172" Y="2.741302001953" />
                  <Point X="-2.938445068359" Y="2.750450683594" />
                  <Point X="-2.929418212891" Y="2.755531738281" />
                  <Point X="-2.9111640625" Y="2.767161132812" />
                  <Point X="-2.902748779297" Y="2.77319140625" />
                  <Point X="-2.886624511719" Y="2.786131347656" />
                  <Point X="-2.877689697266" Y="2.794266601562" />
                  <Point X="-2.818191894531" Y="2.853764404297" />
                  <Point X="-2.811274658203" Y="2.861479492188" />
                  <Point X="-2.798321289062" Y="2.8776171875" />
                  <Point X="-2.79228515625" Y="2.886039794922" />
                  <Point X="-2.780655761719" Y="2.904293945312" />
                  <Point X="-2.775574707031" Y="2.913320800781" />
                  <Point X="-2.766426025391" Y="2.931872558594" />
                  <Point X="-2.7593515625" Y="2.95130859375" />
                  <Point X="-2.754434814453" Y="2.971400634766" />
                  <Point X="-2.752524902344" Y="2.981581542969" />
                  <Point X="-2.749699707031" Y="3.003040283203" />
                  <Point X="-2.748909667969" Y="3.013368652344" />
                  <Point X="-2.748458496094" Y="3.034050292969" />
                  <Point X="-2.748948486328" Y="3.046130615234" />
                  <Point X="-2.756281982422" Y="3.129953125" />
                  <Point X="-2.757745849609" Y="3.140206054688" />
                  <Point X="-2.761781005859" Y="3.160491455078" />
                  <Point X="-2.764352294922" Y="3.170523925781" />
                  <Point X="-2.770860595703" Y="3.191166015625" />
                  <Point X="-2.774509277344" Y="3.200860107422" />
                  <Point X="-2.782840087891" Y="3.219793457031" />
                  <Point X="-2.787522216797" Y="3.229032714844" />
                  <Point X="-3.059387451172" Y="3.699916748047" />
                  <Point X="-2.648373779297" Y="4.015036865234" />
                  <Point X="-2.192525146484" Y="4.268296386719" />
                  <Point X="-2.122601318359" Y="4.177169921875" />
                  <Point X="-2.111821533203" Y="4.164048828125" />
                  <Point X="-2.09751953125" Y="4.149108398438" />
                  <Point X="-2.089960449219" Y="4.142028320312" />
                  <Point X="-2.073379638672" Y="4.128114746094" />
                  <Point X="-2.065093505859" Y="4.121898925781" />
                  <Point X="-2.047893920898" Y="4.11040625" />
                  <Point X="-2.03705871582" Y="4.10412890625" />
                  <Point X="-1.943764648438" Y="4.0555625" />
                  <Point X="-1.934328125" Y="4.051286865234" />
                  <Point X="-1.915044921875" Y="4.043788330078" />
                  <Point X="-1.905198242188" Y="4.040565429688" />
                  <Point X="-1.884287353516" Y="4.034963378906" />
                  <Point X="-1.874149902344" Y="4.032832275391" />
                  <Point X="-1.853702148438" Y="4.029686279297" />
                  <Point X="-1.833031616211" Y="4.028785644531" />
                  <Point X="-1.812387695312" Y="4.030141113281" />
                  <Point X="-1.802104248047" Y="4.031382324219" />
                  <Point X="-1.780785522461" Y="4.035144042969" />
                  <Point X="-1.770685913086" Y="4.037500976562" />
                  <Point X="-1.750803466797" Y="4.043301757813" />
                  <Point X="-1.739017089844" Y="4.047576416016" />
                  <Point X="-1.641925537109" Y="4.08779296875" />
                  <Point X="-1.632587280273" Y="4.092271972656" />
                  <Point X="-1.614451782227" Y="4.102221191406" />
                  <Point X="-1.605654418945" Y="4.10769140625" />
                  <Point X="-1.587924926758" Y="4.120105957031" />
                  <Point X="-1.579777099609" Y="4.126500488281" />
                  <Point X="-1.564225830078" Y="4.140138671875" />
                  <Point X="-1.550250976562" Y="4.155390136719" />
                  <Point X="-1.538020141602" Y="4.172071289062" />
                  <Point X="-1.532360595703" Y="4.180744628906" />
                  <Point X="-1.521538696289" Y="4.199488769531" />
                  <Point X="-1.516857543945" Y="4.208725585937" />
                  <Point X="-1.508527709961" Y="4.227655273437" />
                  <Point X="-1.504227539062" Y="4.239414550781" />
                  <Point X="-1.472599609375" Y="4.339724609375" />
                  <Point X="-1.470027709961" Y="4.349759277344" />
                  <Point X="-1.465991455078" Y="4.370048339844" />
                  <Point X="-1.46452722168" Y="4.380302734375" />
                  <Point X="-1.462640625" Y="4.40186328125" />
                  <Point X="-1.462301635742" Y="4.412216308594" />
                  <Point X="-1.462752807617" Y="4.432897460938" />
                  <Point X="-1.46354309082" Y="4.443225585938" />
                  <Point X="-1.479266235352" Y="4.562654785156" />
                  <Point X="-0.931177307129" Y="4.716319824219" />
                  <Point X="-0.365222015381" Y="4.782557128906" />
                  <Point X="-0.229566848755" Y="4.27628515625" />
                  <Point X="-0.220435317993" Y="4.247107910156" />
                  <Point X="-0.207661911011" Y="4.218916503906" />
                  <Point X="-0.200119308472" Y="4.205344238281" />
                  <Point X="-0.182260925293" Y="4.178617675781" />
                  <Point X="-0.172608306885" Y="4.166456054688" />
                  <Point X="-0.151451400757" Y="4.1438671875" />
                  <Point X="-0.126897850037" Y="4.125026367188" />
                  <Point X="-0.099602508545" Y="4.110436523438" />
                  <Point X="-0.085356559753" Y="4.104260742188" />
                  <Point X="-0.054918586731" Y="4.093927978516" />
                  <Point X="-0.039857143402" Y="4.090155273438" />
                  <Point X="-0.009320039749" Y="4.08511328125" />
                  <Point X="0.02163187027" Y="4.08511328125" />
                  <Point X="0.052168972015" Y="4.090155273438" />
                  <Point X="0.067230415344" Y="4.093927978516" />
                  <Point X="0.097668388367" Y="4.104260742188" />
                  <Point X="0.111914344788" Y="4.110436523438" />
                  <Point X="0.139209686279" Y="4.125026367188" />
                  <Point X="0.16376322937" Y="4.143866699219" />
                  <Point X="0.184920288086" Y="4.166455566406" />
                  <Point X="0.194573196411" Y="4.178618164062" />
                  <Point X="0.212431289673" Y="4.205344726562" />
                  <Point X="0.219973754883" Y="4.218916503906" />
                  <Point X="0.232747146606" Y="4.247107910156" />
                  <Point X="0.237978225708" Y="4.261727539062" />
                  <Point X="0.378190155029" Y="4.785006347656" />
                  <Point X="0.82787689209" Y="4.737911621094" />
                  <Point X="1.453602416992" Y="4.586842285156" />
                  <Point X="1.858250854492" Y="4.440072753906" />
                  <Point X="2.25045703125" Y="4.256650878906" />
                  <Point X="2.629436767578" Y="4.035856201172" />
                  <Point X="2.817780029297" Y="3.901916748047" />
                  <Point X="2.082700439453" Y="2.628721435547" />
                  <Point X="2.06530859375" Y="2.59859765625" />
                  <Point X="2.062062744141" Y="2.592469482422" />
                  <Point X="2.052391845703" Y="2.571434326172" />
                  <Point X="2.043490478516" Y="2.547249755859" />
                  <Point X="2.040868530273" Y="2.538978027344" />
                  <Point X="2.01983215332" Y="2.460312255859" />
                  <Point X="2.017811523438" Y="2.450849365234" />
                  <Point X="2.013696289062" Y="2.422146728516" />
                  <Point X="2.012527587891" Y="2.401842773438" />
                  <Point X="2.012416992188" Y="2.393415527344" />
                  <Point X="2.013579956055" Y="2.368178710938" />
                  <Point X="2.021782348633" Y="2.300155273438" />
                  <Point X="2.023800537109" Y="2.289034179688" />
                  <Point X="2.029143676758" Y="2.267110351562" />
                  <Point X="2.032468261719" Y="2.256307617188" />
                  <Point X="2.040734619141" Y="2.234219238281" />
                  <Point X="2.0453203125" Y="2.223884765625" />
                  <Point X="2.055686523438" Y="2.203835205078" />
                  <Point X="2.062333984375" Y="2.192842285156" />
                  <Point X="2.104424560547" Y="2.130811523438" />
                  <Point X="2.110247558594" Y="2.123105224609" />
                  <Point X="2.129221435547" Y="2.101249511719" />
                  <Point X="2.143799316406" Y="2.08708203125" />
                  <Point X="2.15000390625" Y="2.081558349609" />
                  <Point X="2.169536132812" Y="2.066113769531" />
                  <Point X="2.231566894531" Y="2.0240234375" />
                  <Point X="2.24127734375" Y="2.018246459961" />
                  <Point X="2.261315429688" Y="2.007886352539" />
                  <Point X="2.271643066406" Y="2.003303222656" />
                  <Point X="2.293724121094" Y="1.995038085938" />
                  <Point X="2.30452734375" Y="1.991712158203" />
                  <Point X="2.326453369141" Y="1.98636730957" />
                  <Point X="2.338984863281" Y="1.984178344727" />
                  <Point X="2.407008300781" Y="1.975976074219" />
                  <Point X="2.416775390625" Y="1.975305908203" />
                  <Point X="2.446109375" Y="1.975313110352" />
                  <Point X="2.466638916016" Y="1.977086181641" />
                  <Point X="2.474885253906" Y="1.978163696289" />
                  <Point X="2.499374755859" Y="1.982830932617" />
                  <Point X="2.578034179688" Y="2.003865356445" />
                  <Point X="2.583995849609" Y="2.005671020508" />
                  <Point X="2.604402587891" Y="2.01306652832" />
                  <Point X="2.627653808594" Y="2.023573364258" />
                  <Point X="2.636033691406" Y="2.027872314453" />
                  <Point X="3.940405029297" Y="2.780951416016" />
                  <Point X="4.043950195312" Y="2.637047119141" />
                  <Point X="4.136884765625" Y="2.48347265625" />
                  <Point X="3.195849853516" Y="1.761390136719" />
                  <Point X="3.172951660156" Y="1.743819702148" />
                  <Point X="3.167541992188" Y="1.739341796875" />
                  <Point X="3.150437011719" Y="1.723471557617" />
                  <Point X="3.132933837891" Y="1.704470092773" />
                  <Point X="3.127409667969" Y="1.697900756836" />
                  <Point X="3.070793945312" Y="1.624040893555" />
                  <Point X="3.065263183594" Y="1.616001586914" />
                  <Point X="3.050376953125" Y="1.590831665039" />
                  <Point X="3.041472900391" Y="1.572326049805" />
                  <Point X="3.038188476562" Y="1.564654296875" />
                  <Point X="3.029705566406" Y="1.541118286133" />
                  <Point X="3.008616210938" Y="1.465707397461" />
                  <Point X="3.006225097656" Y="1.454661010742" />
                  <Point X="3.002771728516" Y="1.432362670898" />
                  <Point X="3.001709472656" Y="1.421110717773" />
                  <Point X="3.000893310547" Y="1.397540771484" />
                  <Point X="3.001174804688" Y="1.386240966797" />
                  <Point X="3.003078125" Y="1.36375378418" />
                  <Point X="3.005056640625" Y="1.350838134766" />
                  <Point X="3.022368896484" Y="1.266933837891" />
                  <Point X="3.024826904297" Y="1.257498291016" />
                  <Point X="3.034108642578" Y="1.229797119141" />
                  <Point X="3.042276611328" Y="1.210945068359" />
                  <Point X="3.045866943359" Y="1.203552124023" />
                  <Point X="3.057888916016" Y="1.18205078125" />
                  <Point X="3.1049765625" Y="1.110480224609" />
                  <Point X="3.111739257813" Y="1.101424438477" />
                  <Point X="3.126292480469" Y="1.08417980957" />
                  <Point X="3.134083251953" Y="1.075991088867" />
                  <Point X="3.151327148438" Y="1.059901245117" />
                  <Point X="3.160032470703" Y="1.052697631836" />
                  <Point X="3.178238769531" Y="1.039373535156" />
                  <Point X="3.189145019531" Y="1.032462158203" />
                  <Point X="3.257381591797" Y="0.994051086426" />
                  <Point X="3.266233886719" Y="0.989657470703" />
                  <Point X="3.293603759766" Y="0.978364807129" />
                  <Point X="3.313479736328" Y="0.972115661621" />
                  <Point X="3.321426025391" Y="0.969990478516" />
                  <Point X="3.345571533203" Y="0.965006286621" />
                  <Point X="3.437831542969" Y="0.952812988281" />
                  <Point X="3.444029785156" Y="0.952199768066" />
                  <Point X="3.465716064453" Y="0.951222839355" />
                  <Point X="3.491217285156" Y="0.952032409668" />
                  <Point X="3.500603271484" Y="0.952797241211" />
                  <Point X="4.704703613281" Y="1.11132019043" />
                  <Point X="4.752683105469" Y="0.914235168457" />
                  <Point X="4.78387109375" Y="0.713920776367" />
                  <Point X="3.718233886719" Y="0.428384246826" />
                  <Point X="3.691991943359" Y="0.421352752686" />
                  <Point X="3.685199462891" Y="0.419257324219" />
                  <Point X="3.663159912109" Y="0.41106060791" />
                  <Point X="3.639668457031" Y="0.400152435303" />
                  <Point X="3.632137451172" Y="0.39623727417" />
                  <Point X="3.541494628906" Y="0.343844116211" />
                  <Point X="3.533242919922" Y="0.338485961914" />
                  <Point X="3.509682128906" Y="0.320760314941" />
                  <Point X="3.494249755859" Y="0.306912231445" />
                  <Point X="3.488344482422" Y="0.301130187988" />
                  <Point X="3.471676269531" Y="0.28279888916" />
                  <Point X="3.417290771484" Y="0.213498855591" />
                  <Point X="3.410854492188" Y="0.204208297729" />
                  <Point X="3.399130615234" Y="0.184928634644" />
                  <Point X="3.393843017578" Y="0.174939544678" />
                  <Point X="3.384069091797" Y="0.153475723267" />
                  <Point X="3.380005615234" Y="0.142930480957" />
                  <Point X="3.37316015625" Y="0.12143321228" />
                  <Point X="3.370004638672" Y="0.108531997681" />
                  <Point X="3.351875976562" Y="0.013871543884" />
                  <Point X="3.350527587891" Y="0.004123813152" />
                  <Point X="3.348513671875" Y="-0.02529356575" />
                  <Point X="3.348887207031" Y="-0.046052913666" />
                  <Point X="3.349381103516" Y="-0.054168170929" />
                  <Point X="3.352247558594" Y="-0.078372749329" />
                  <Point X="3.370376220703" Y="-0.173033203125" />
                  <Point X="3.373158935547" Y="-0.183988204956" />
                  <Point X="3.380005371094" Y="-0.205489471436" />
                  <Point X="3.384069091797" Y="-0.216035766602" />
                  <Point X="3.393843017578" Y="-0.237499435425" />
                  <Point X="3.399130615234" Y="-0.247488967896" />
                  <Point X="3.410854492188" Y="-0.266768493652" />
                  <Point X="3.418408691406" Y="-0.277483306885" />
                  <Point X="3.472794189453" Y="-0.346783355713" />
                  <Point X="3.479356689453" Y="-0.35429788208" />
                  <Point X="3.500540527344" Y="-0.375374237061" />
                  <Point X="3.516719970703" Y="-0.388873809814" />
                  <Point X="3.523180908203" Y="-0.39381149292" />
                  <Point X="3.543361816406" Y="-0.407483459473" />
                  <Point X="3.634004394531" Y="-0.459876617432" />
                  <Point X="3.63949609375" Y="-0.462815185547" />
                  <Point X="3.659157226562" Y="-0.472016418457" />
                  <Point X="3.683027099609" Y="-0.481027740479" />
                  <Point X="3.691991699219" Y="-0.483913085938" />
                  <Point X="4.784876953125" Y="-0.776750488281" />
                  <Point X="4.761612792969" Y="-0.931055908203" />
                  <Point X="4.727801757812" Y="-1.079219604492" />
                  <Point X="3.467215087891" Y="-0.913260314941" />
                  <Point X="3.436781982422" Y="-0.909253723145" />
                  <Point X="3.427159912109" Y="-0.908481628418" />
                  <Point X="3.398236328125" Y="-0.908126281738" />
                  <Point X="3.377425537109" Y="-0.909635864258" />
                  <Point X="3.370740478516" Y="-0.910359313965" />
                  <Point X="3.35081640625" Y="-0.913472839355" />
                  <Point X="3.172916992188" Y="-0.952139892578" />
                  <Point X="3.156489990234" Y="-0.957307678223" />
                  <Point X="3.124839111328" Y="-0.970523376465" />
                  <Point X="3.109615234375" Y="-0.978571472168" />
                  <Point X="3.079573242188" Y="-0.998127990723" />
                  <Point X="3.068253662109" Y="-1.006770568848" />
                  <Point X="3.047034179688" Y="-1.025650146484" />
                  <Point X="3.037134277344" Y="-1.035887329102" />
                  <Point X="2.92960546875" Y="-1.165211303711" />
                  <Point X="2.920571289062" Y="-1.178120239258" />
                  <Point X="2.904760742188" Y="-1.20525402832" />
                  <Point X="2.897984375" Y="-1.219479125977" />
                  <Point X="2.886323486328" Y="-1.250314575195" />
                  <Point X="2.882611572266" Y="-1.262569458008" />
                  <Point X="2.876869628906" Y="-1.287467651367" />
                  <Point X="2.874839599609" Y="-1.300110839844" />
                  <Point X="2.859428222656" Y="-1.467590698242" />
                  <Point X="2.85908203125" Y="-1.479483276367" />
                  <Point X="2.860162597656" Y="-1.511670043945" />
                  <Point X="2.863183837891" Y="-1.532456542969" />
                  <Point X="2.871820068359" Y="-1.565571044922" />
                  <Point X="2.877641601562" Y="-1.581735961914" />
                  <Point X="2.886181152344" Y="-1.600054443359" />
                  <Point X="2.898568359375" Y="-1.622526245117" />
                  <Point X="2.997020263672" Y="-1.775661987305" />
                  <Point X="3.0017421875" Y="-1.782353881836" />
                  <Point X="3.019793945312" Y="-1.804450317383" />
                  <Point X="3.043489746094" Y="-1.828120605469" />
                  <Point X="3.052796386719" Y="-1.836277832031" />
                  <Point X="4.087170654297" Y="-2.629982177734" />
                  <Point X="4.04548828125" Y="-2.697430664062" />
                  <Point X="4.001274658203" Y="-2.760252197266" />
                  <Point X="2.875559570312" Y="-2.110320068359" />
                  <Point X="2.848454833984" Y="-2.094670898438" />
                  <Point X="2.839667236328" Y="-2.090188720703" />
                  <Point X="2.812468505859" Y="-2.078612792969" />
                  <Point X="2.791992675781" Y="-2.071922119141" />
                  <Point X="2.766746582031" Y="-2.065550048828" />
                  <Point X="2.555018066406" Y="-2.027311889648" />
                  <Point X="2.543199707031" Y="-2.025934570313" />
                  <Point X="2.511038818359" Y="-2.024217407227" />
                  <Point X="2.489963378906" Y="-2.025441040039" />
                  <Point X="2.456025146484" Y="-2.031243530273" />
                  <Point X="2.439321044922" Y="-2.03569519043" />
                  <Point X="2.420025634766" Y="-2.042772460938" />
                  <Point X="2.396965087891" Y="-2.053015625" />
                  <Point X="2.221070800781" Y="-2.145587646484" />
                  <Point X="2.207594970703" Y="-2.154201171875" />
                  <Point X="2.182279541016" Y="-2.173566650391" />
                  <Point X="2.170439941406" Y="-2.184318603516" />
                  <Point X="2.147573242188" Y="-2.208901855469" />
                  <Point X="2.139641113281" Y="-2.218649902344" />
                  <Point X="2.1251328125" Y="-2.239108154297" />
                  <Point X="2.118556640625" Y="-2.249818359375" />
                  <Point X="2.025984741211" Y="-2.425712890625" />
                  <Point X="2.02111328125" Y="-2.436568847656" />
                  <Point X="2.009794311523" Y="-2.466720458984" />
                  <Point X="2.004603027344" Y="-2.487286132812" />
                  <Point X="1.999928710938" Y="-2.521599853516" />
                  <Point X="1.999168334961" Y="-2.538971923828" />
                  <Point X="2.000168579102" Y="-2.559834960938" />
                  <Point X="2.002975463867" Y="-2.58450390625" />
                  <Point X="2.041213378906" Y="-2.796232421875" />
                  <Point X="2.043015014648" Y="-2.804221435547" />
                  <Point X="2.051236083984" Y="-2.83154296875" />
                  <Point X="2.064069580078" Y="-2.862479736328" />
                  <Point X="2.069546630859" Y="-2.873578613281" />
                  <Point X="2.735893310547" Y="-4.027724609375" />
                  <Point X="2.723754638672" Y="-4.036083740234" />
                  <Point X="1.854598022461" Y="-2.903377685547" />
                  <Point X="1.833914550781" Y="-2.876422607422" />
                  <Point X="1.827373779297" Y="-2.868773925781" />
                  <Point X="1.81351940918" Y="-2.854211425781" />
                  <Point X="1.806206542969" Y="-2.847298095703" />
                  <Point X="1.789276855469" Y="-2.832912597656" />
                  <Point X="1.768997070312" Y="-2.817881347656" />
                  <Point X="1.560175292969" Y="-2.683628417969" />
                  <Point X="1.549783813477" Y="-2.677833251953" />
                  <Point X="1.520728637695" Y="-2.663939453125" />
                  <Point X="1.500605834961" Y="-2.656963378906" />
                  <Point X="1.466654785156" Y="-2.649321777344" />
                  <Point X="1.449326416016" Y="-2.647068847656" />
                  <Point X="1.42816418457" Y="-2.646281494141" />
                  <Point X="1.403689575195" Y="-2.646949462891" />
                  <Point X="1.175307739258" Y="-2.667965332031" />
                  <Point X="1.159340332031" Y="-2.670825439453" />
                  <Point X="1.128125854492" Y="-2.679220214844" />
                  <Point X="1.112878662109" Y="-2.684754638672" />
                  <Point X="1.081860229492" Y="-2.699118408203" />
                  <Point X="1.07102355957" Y="-2.705019775391" />
                  <Point X="1.050206420898" Y="-2.718177246094" />
                  <Point X="1.040225708008" Y="-2.72543359375" />
                  <Point X="0.863874938965" Y="-2.872063476562" />
                  <Point X="0.855219848633" Y="-2.880229003906" />
                  <Point X="0.833223999023" Y="-2.903753662109" />
                  <Point X="0.820458740234" Y="-2.9209375" />
                  <Point X="0.802876647949" Y="-2.951221191406" />
                  <Point X="0.795493103027" Y="-2.967179931641" />
                  <Point X="0.788288574219" Y="-2.987504150391" />
                  <Point X="0.781705627441" Y="-3.01062890625" />
                  <Point X="0.728977661133" Y="-3.253219238281" />
                  <Point X="0.727584594727" Y="-3.261289794922" />
                  <Point X="0.72472467041" Y="-3.289677734375" />
                  <Point X="0.72474230957" Y="-3.323170410156" />
                  <Point X="0.725555175781" Y="-3.335520019531" />
                  <Point X="0.833091674805" Y="-4.152341308594" />
                  <Point X="0.663150756836" Y="-3.518112792969" />
                  <Point X="0.655064941406" Y="-3.487936523438" />
                  <Point X="0.651881164551" Y="-3.478191650391" />
                  <Point X="0.64448236084" Y="-3.459102783203" />
                  <Point X="0.64026776123" Y="-3.449759033203" />
                  <Point X="0.629742736816" Y="-3.429429199219" />
                  <Point X="0.617102783203" Y="-3.408448486328" />
                  <Point X="0.45667980957" Y="-3.177309814453" />
                  <Point X="0.449297119141" Y="-3.167979492188" />
                  <Point X="0.427770874023" Y="-3.144024414062" />
                  <Point X="0.411705169678" Y="-3.129776855469" />
                  <Point X="0.382941894531" Y="-3.109577880859" />
                  <Point X="0.367617980957" Y="-3.100820068359" />
                  <Point X="0.347766265869" Y="-3.091807373047" />
                  <Point X="0.325540588379" Y="-3.083351074219" />
                  <Point X="0.077295310974" Y="-3.006305175781" />
                  <Point X="0.066813682556" Y="-3.003695068359" />
                  <Point X="0.038074615479" Y="-2.998252197266" />
                  <Point X="0.018518516541" Y="-2.996611572266" />
                  <Point X="-0.013665624619" Y="-2.997248046875" />
                  <Point X="-0.029730422974" Y="-2.998939453125" />
                  <Point X="-0.049665363312" Y="-3.002773681641" />
                  <Point X="-0.070097892761" Y="-3.007892333984" />
                  <Point X="-0.31834274292" Y="-3.084938476562" />
                  <Point X="-0.329463897705" Y="-3.089170410156" />
                  <Point X="-0.358788665771" Y="-3.102487304688" />
                  <Point X="-0.377303375244" Y="-3.113589111328" />
                  <Point X="-0.405380187988" Y="-3.135111328125" />
                  <Point X="-0.41840802002" Y="-3.147191162109" />
                  <Point X="-0.433230773926" Y="-3.163770996094" />
                  <Point X="-0.44767276001" Y="-3.182071289063" />
                  <Point X="-0.608095703125" Y="-3.413210205078" />
                  <Point X="-0.612471008301" Y="-3.420133300781" />
                  <Point X="-0.62597668457" Y="-3.445265136719" />
                  <Point X="-0.638777709961" Y="-3.476215820312" />
                  <Point X="-0.642752929688" Y="-3.487936523438" />
                  <Point X="-0.985425170898" Y="-4.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.908581645609" Y="-3.887201587759" />
                  <Point X="-2.443467483089" Y="-4.084630836411" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.129533918164" Y="-4.642362544894" />
                  <Point X="-0.970202465261" Y="-4.709994734082" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.94458603325" Y="-3.768714396079" />
                  <Point X="-2.378894575466" Y="-4.008836173653" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.119646972868" Y="-4.543355068328" />
                  <Point X="-0.9453730158" Y="-4.617329974225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.896729329006" Y="-3.685824125963" />
                  <Point X="-2.287758049488" Y="-3.944317097914" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.136175038454" Y="-4.433135084873" />
                  <Point X="-0.920543566339" Y="-4.524665214367" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.848872624762" Y="-3.602933855848" />
                  <Point X="-2.193305390392" Y="-3.881205637171" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.1585968027" Y="-4.320413374762" />
                  <Point X="-0.895714116879" Y="-4.43200045451" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.801015920518" Y="-3.520043585733" />
                  <Point X="-2.098706664431" Y="-3.818156178133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.181018566947" Y="-4.20769166465" />
                  <Point X="-0.870884667418" Y="-4.339335694653" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.817803416715" Y="-2.985238664307" />
                  <Point X="-3.771150160074" Y="-3.005041796846" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.753159216274" Y="-3.437153315618" />
                  <Point X="-1.955248397331" Y="-3.775846363841" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.244074605356" Y="-4.077721728482" />
                  <Point X="-0.846055217957" Y="-4.246670934796" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.933851574653" Y="-2.832774907942" />
                  <Point X="-3.668133917447" Y="-2.945565361648" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.70530251203" Y="-3.354263045503" />
                  <Point X="-1.669243284188" Y="-3.794044095824" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.465311769155" Y="-3.880607888187" />
                  <Point X="-0.821225768497" Y="-4.154006174939" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.036598713721" Y="-2.685957099116" />
                  <Point X="-3.56511767482" Y="-2.88608892645" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.657445807786" Y="-3.271372775388" />
                  <Point X="-0.796396319036" Y="-4.061341415082" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.119005777342" Y="-2.547773140078" />
                  <Point X="-3.462101432194" Y="-2.826612491252" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.609589103542" Y="-3.188482505273" />
                  <Point X="-0.771566869575" Y="-3.968676655225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.16009350512" Y="-2.427128198527" />
                  <Point X="-3.359085189567" Y="-2.767136056054" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.561732399298" Y="-3.105592235157" />
                  <Point X="-0.746737420115" Y="-3.876011895368" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.073498428532" Y="-2.360681391893" />
                  <Point X="-3.25606894694" Y="-2.707659620856" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.513875695054" Y="-3.022701965042" />
                  <Point X="-0.721907970654" Y="-3.783347135511" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.986903351945" Y="-2.294234585259" />
                  <Point X="-3.153052704314" Y="-2.648183185658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.46601899081" Y="-2.939811694927" />
                  <Point X="-0.697078521193" Y="-3.690682375654" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.900308275357" Y="-2.227787778624" />
                  <Point X="-3.050036461687" Y="-2.58870675046" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.418162286566" Y="-2.856921424812" />
                  <Point X="-0.672249071732" Y="-3.598017615797" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.81371319877" Y="-2.16134097199" />
                  <Point X="-2.94702021906" Y="-2.529230315262" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.370305582322" Y="-2.774031154697" />
                  <Point X="-0.647419622272" Y="-3.505352855939" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.827483737273" Y="-4.131412188409" />
                  <Point X="0.830505151604" Y="-4.132694702702" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.727118122182" Y="-2.094894165356" />
                  <Point X="-2.844003976433" Y="-2.469753880064" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.32699444671" Y="-2.689211405172" />
                  <Point X="-0.610901397139" Y="-3.417649686987" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.796281380265" Y="-4.0149633378" />
                  <Point X="0.816113832179" Y="-4.023381714181" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.640523045594" Y="-2.028447358721" />
                  <Point X="-2.740987733807" Y="-2.410277444866" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.310808177309" Y="-2.592877833048" />
                  <Point X="-0.555785285044" Y="-3.337840852686" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.765079023258" Y="-3.89851448719" />
                  <Point X="0.801722512755" Y="-3.91406872566" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.553927969007" Y="-1.962000552087" />
                  <Point X="-2.624891800765" Y="-2.356353008853" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.350926608274" Y="-2.472644333584" />
                  <Point X="-0.500456330852" Y="-3.258122364494" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.73387666625" Y="-3.78206563658" />
                  <Point X="0.787331193331" Y="-3.804755737139" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.467332892419" Y="-1.895553745453" />
                  <Point X="-0.444866086496" Y="-3.178514787396" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.702674309242" Y="-3.665616785971" />
                  <Point X="0.772939873907" Y="-3.695442748618" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.661205384311" Y="-1.285580703026" />
                  <Point X="-4.659135339965" Y="-1.286459384719" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.380737815831" Y="-1.829106938818" />
                  <Point X="-0.367929358489" Y="-3.107968255024" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.671471952234" Y="-3.549167935361" />
                  <Point X="0.758548554483" Y="-3.586129760096" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.690772564015" Y="-1.169825944001" />
                  <Point X="-4.473558714868" Y="-1.262027752696" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.294142739244" Y="-1.762660132184" />
                  <Point X="-0.23788125105" Y="-3.059966165674" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.628788792463" Y="-3.427845773108" />
                  <Point X="0.744157235059" Y="-3.476816771575" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.720339743719" Y="-1.054071184977" />
                  <Point X="-4.287982089772" Y="-1.237596120673" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.207547662656" Y="-1.69621332555" />
                  <Point X="-0.097436431515" Y="-3.01637721878" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.529761953936" Y="-3.282607138171" />
                  <Point X="0.729765915635" Y="-3.367503783054" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.7456220321" Y="-0.94013525441" />
                  <Point X="-4.102405464676" Y="-1.21316448865" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.120952586068" Y="-1.629766518916" />
                  <Point X="0.24347267504" Y="-3.057880313275" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.409955239766" Y="-3.12854796934" />
                  <Point X="0.727383269353" Y="-3.263288173858" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.761336554339" Y="-0.830260599617" />
                  <Point X="-3.916828839579" Y="-1.188732856627" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.035888376055" Y="-1.562669897973" />
                  <Point X="0.747376395003" Y="-3.168570516341" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.6912192785" Y="-3.993682867053" />
                  <Point X="2.72436147896" Y="-4.007750896502" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.777051076578" Y="-0.720385944824" />
                  <Point X="-3.731252214483" Y="-1.164301224604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.9784011369" Y="-1.483867547395" />
                  <Point X="0.767913466619" Y="-3.074083750187" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.573774849294" Y="-3.840626428698" />
                  <Point X="2.645433544303" Y="-3.871043740092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.726570886907" Y="-0.638609278204" />
                  <Point X="-3.545675589387" Y="-1.139869592581" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.950954703372" Y="-1.392313631369" />
                  <Point X="0.790746098486" Y="-2.980571391549" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.456330420089" Y="-3.687569990343" />
                  <Point X="2.566505609647" Y="-3.734336583681" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.5775231619" Y="-0.598672048029" />
                  <Point X="-3.36009896429" Y="-1.115437960559" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.957831013811" Y="-1.286190574905" />
                  <Point X="0.83881691975" Y="-2.897772008716" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.338885990884" Y="-3.534513551987" />
                  <Point X="2.48757767499" Y="-3.59762942727" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.428475436894" Y="-0.558734817854" />
                  <Point X="0.917109247503" Y="-2.827800894296" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.221441561678" Y="-3.381457113632" />
                  <Point X="2.408649740334" Y="-3.46092227086" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.279427711888" Y="-0.518797587679" />
                  <Point X="0.999282017649" Y="-2.759476929948" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.103997132473" Y="-3.228400675277" />
                  <Point X="2.329721805677" Y="-3.324215114449" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.130379986881" Y="-0.478860357504" />
                  <Point X="1.090640997034" Y="-2.695052280078" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.986552703268" Y="-3.075344236922" />
                  <Point X="2.250793871021" Y="-3.187507958039" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.981332261875" Y="-0.438923127329" />
                  <Point X="1.253098121488" Y="-2.660807002269" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.869108274062" Y="-2.922287798566" />
                  <Point X="2.171865936364" Y="-3.050800801628" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.832284536869" Y="-0.398985897154" />
                  <Point X="1.472018863688" Y="-2.650529108225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.579956624145" Y="-2.696345969257" />
                  <Point X="2.092938001708" Y="-2.914093645217" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.683236811862" Y="-0.359048666979" />
                  <Point X="2.039785783225" Y="-2.788327631192" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.534189054558" Y="-0.319111450514" />
                  <Point X="2.019599769941" Y="-2.67655494106" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.416670926553" Y="-0.265790700446" />
                  <Point X="2.000798365564" Y="-2.565369982539" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.345272797693" Y="-0.192893172219" />
                  <Point X="2.010032683071" Y="-2.466085481912" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.305478043043" Y="-0.106580807531" />
                  <Point X="2.050101300064" Y="-2.379889364893" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.780850591482" Y="0.622881919662" />
                  <Point X="-4.574566998047" Y="0.535319729251" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.296435894222" Y="-0.007214736137" />
                  <Point X="2.094498613205" Y="-2.295530670375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.76648149903" Y="0.719986837638" />
                  <Point X="-3.915223345497" Y="0.35864918937" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.333223853232" Y="0.111605061856" />
                  <Point X="2.143972541832" Y="-2.213326871282" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.752112406578" Y="0.817091755614" />
                  <Point X="2.223951357155" Y="-2.144071628364" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.737743314127" Y="0.914196673589" />
                  <Point X="2.332499605108" Y="-2.08694339011" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.717442016885" Y="1.008783520028" />
                  <Point X="2.448891604072" Y="-2.033144626625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.555421706431" Y="-2.502838788454" />
                  <Point X="4.038211002118" Y="-2.707770686009" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.692360726708" Y="1.101341379843" />
                  <Point X="2.77074610846" Y="-2.066559522368" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.880335536744" Y="-2.113077474797" />
                  <Point X="4.073655866227" Y="-2.619611902334" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.667279436531" Y="1.193899239658" />
                  <Point X="3.772640042286" Y="-2.388634029937" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.642198146354" Y="1.286457099473" />
                  <Point X="3.471624218345" Y="-2.157656157539" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.402153222994" Y="1.287768310602" />
                  <Point X="3.170608394404" Y="-1.926678285142" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.049706653046" Y="1.241367853453" />
                  <Point X="2.974144409353" Y="-1.740080035343" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.723017684755" Y="1.205900849533" />
                  <Point X="2.885880784205" Y="-1.599410113426" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.579891675049" Y="1.248351698722" />
                  <Point X="2.859264141481" Y="-1.484907783045" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.496587250639" Y="1.316195304335" />
                  <Point X="2.86702823257" Y="-1.384999208329" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.451494812093" Y="1.400258935624" />
                  <Point X="2.877183713615" Y="-1.286105718426" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.423599250901" Y="1.491622208267" />
                  <Point X="2.909734350012" Y="-1.196718407974" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.434326047482" Y="1.599379699128" />
                  <Point X="2.968484312859" Y="-1.118452051803" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.508203113803" Y="1.73394288913" />
                  <Point X="3.031910061305" Y="-1.042170448864" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.784966995003" Y="1.95462642259" />
                  <Point X="3.116562250246" Y="-0.974898935353" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.08598372542" Y="2.185604679763" />
                  <Point X="3.260984200231" Y="-0.932998180176" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.202052049444" Y="2.338076996124" />
                  <Point X="3.453304104699" Y="-0.911428900425" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.153774199576" Y="2.420788500528" />
                  <Point X="3.805750632473" Y="-0.957829339672" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.105496349707" Y="2.503500004932" />
                  <Point X="4.158197160682" Y="-1.004229779104" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.057218499839" Y="2.586211509335" />
                  <Point X="4.510643688892" Y="-1.050630218535" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.008940649971" Y="2.668923013739" />
                  <Point X="4.736043677347" Y="-1.043102601355" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.95024502804" Y="2.747212436261" />
                  <Point X="4.757515101748" Y="-0.949012444428" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.8898857814" Y="2.82479569199" />
                  <Point X="3.418316082326" Y="-0.277351950937" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.269485384578" Y="-0.638651884074" />
                  <Point X="4.773445815682" Y="-0.852570395444" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.82952653476" Y="2.90237894772" />
                  <Point X="3.366366571492" Y="-0.15209645602" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.769167288119" Y="2.979962203449" />
                  <Point X="-3.226580303223" Y="2.749647692758" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.225794188457" Y="2.749314006837" />
                  <Point X="3.3488041577" Y="-0.0414374178" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.960651372771" Y="2.739971794733" />
                  <Point X="3.360127982166" Y="0.056960139744" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.868076128177" Y="2.803880170651" />
                  <Point X="3.3829186936" Y="0.150490292551" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.798406275653" Y="2.87751130866" />
                  <Point X="3.432364302891" Y="0.232706112488" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.756507531301" Y="2.962930582705" />
                  <Point X="3.496314569231" Y="0.30876507079" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.75047457833" Y="3.063573981955" />
                  <Point X="3.590320321736" Y="0.372066232126" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.765133107416" Y="3.173000394248" />
                  <Point X="3.707532537255" Y="0.42551683434" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.83086622663" Y="3.304106683799" />
                  <Point X="3.856580307039" Y="0.465454045508" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.909794214495" Y="3.440813862795" />
                  <Point X="4.005628070379" Y="0.505391259411" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.98872220236" Y="3.577521041791" />
                  <Point X="4.15467583372" Y="0.545328473314" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.050317214243" Y="3.706870808993" />
                  <Point X="3.125140896199" Y="1.085544362553" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.437837899951" Y="0.952812359356" />
                  <Point X="4.303723597061" Y="0.585265687217" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.963675781468" Y="3.773297938594" />
                  <Point X="3.03538686404" Y="1.226846924712" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.638300193524" Y="0.970925399988" />
                  <Point X="4.452771360402" Y="0.62520290112" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.877034348694" Y="3.839725068194" />
                  <Point X="3.006845981814" Y="1.342166046302" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.823876792566" Y="0.995357043071" />
                  <Point X="4.601819123743" Y="0.665140115023" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.790392915919" Y="3.906152197794" />
                  <Point X="3.004913285332" Y="1.44619066314" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.009453391608" Y="1.019788686153" />
                  <Point X="4.750866887083" Y="0.705077328926" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.703751483145" Y="3.972579327395" />
                  <Point X="3.029143830623" Y="1.539109642734" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.19502999065" Y="1.044220329235" />
                  <Point X="4.770475478529" Y="0.799958211529" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.610375568806" Y="4.036147839171" />
                  <Point X="3.071137230846" Y="1.624488737746" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.380606589692" Y="1.068651972318" />
                  <Point X="4.753269986519" Y="0.910465745442" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.505070811092" Y="4.094652857348" />
                  <Point X="2.097537263364" Y="2.140961640858" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.479150273637" Y="1.97897652846" />
                  <Point X="3.131069630244" Y="1.70225317938" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.566183188734" Y="1.0930836154" />
                  <Point X="4.72561874888" Y="1.02540723531" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.399766053378" Y="4.153157875525" />
                  <Point X="2.027489048029" Y="2.273899580042" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.622600382269" Y="2.021289805816" />
                  <Point X="3.209752911726" Y="1.772058343788" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.294461295664" Y="4.211662893702" />
                  <Point X="-2.019088357192" Y="4.094774016254" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.872601540476" Y="4.032594051652" />
                  <Point X="2.012882964364" Y="2.383303730574" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.726787686291" Y="2.080269154943" />
                  <Point X="3.296347964917" Y="1.838505160354" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.701436183583" Y="4.063142904097" />
                  <Point X="2.025414773829" Y="2.481188528909" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.829803912012" Y="2.139745597317" />
                  <Point X="3.382943018107" Y="1.904951976919" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.589651160729" Y="4.118897212919" />
                  <Point X="2.052971691242" Y="2.572695547308" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.932820137733" Y="2.199222039691" />
                  <Point X="3.469538071298" Y="1.971398793485" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.524459751364" Y="4.194429337264" />
                  <Point X="2.098725864696" Y="2.656478288794" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.035836363454" Y="2.258698482065" />
                  <Point X="3.556133124489" Y="2.037845610051" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.490425982378" Y="4.283187095282" />
                  <Point X="2.146582577925" Y="2.739368555095" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.138852589175" Y="2.318174924439" />
                  <Point X="3.642728177679" Y="2.104292426616" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.465187554979" Y="4.375678254304" />
                  <Point X="2.194439291154" Y="2.822258821396" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.241868814896" Y="2.377651366813" />
                  <Point X="3.72932323087" Y="2.170739243182" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.468417932632" Y="4.480253704118" />
                  <Point X="2.242296004383" Y="2.905149087697" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.344885040617" Y="2.437127809187" />
                  <Point X="3.815918284061" Y="2.237186059748" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.443218356341" Y="4.572761354457" />
                  <Point X="2.290152717612" Y="2.988039353999" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.447901266338" Y="2.496604251561" />
                  <Point X="3.902513337251" Y="2.303632876314" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.296796133422" Y="4.613813044148" />
                  <Point X="2.338009430841" Y="3.0709296203" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.550917492059" Y="2.556080693935" />
                  <Point X="3.989108390442" Y="2.370079692879" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.150373910504" Y="4.654864733839" />
                  <Point X="-0.225134888522" Y="4.262124070034" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.120901521321" Y="4.115240328564" />
                  <Point X="2.38586614407" Y="3.153819886601" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.65393371778" Y="2.615557136309" />
                  <Point X="4.075703443633" Y="2.436526509445" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.003951687585" Y="4.69591642353" />
                  <Point X="-0.257056589182" Y="4.378878263908" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.199026221664" Y="4.185282596598" />
                  <Point X="2.433722857299" Y="3.236710152902" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.756949943501" Y="2.675033578683" />
                  <Point X="4.112220665603" Y="2.524230104214" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.835316034289" Y="4.727539071445" />
                  <Point X="-0.288258967138" Y="4.49532712341" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.24043824548" Y="4.270908471253" />
                  <Point X="2.481579570528" Y="3.319600419204" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.859966169222" Y="2.734510021057" />
                  <Point X="4.023887677286" Y="2.664929469049" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.644730401476" Y="4.749844505838" />
                  <Point X="-0.319461345093" Y="4.611775982911" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.265267660671" Y="4.363573245657" />
                  <Point X="2.529436283757" Y="3.402490685505" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.454144768664" Y="4.772149940231" />
                  <Point X="-0.350663723049" Y="4.728224842412" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.290097075862" Y="4.45623802006" />
                  <Point X="2.577292996986" Y="3.485380951806" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.314926491053" Y="4.548902794464" />
                  <Point X="2.625149710214" Y="3.568271218107" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.339755906244" Y="4.641567568868" />
                  <Point X="2.673006423443" Y="3.651161484409" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.364585321435" Y="4.734232343271" />
                  <Point X="2.720863136672" Y="3.73405175071" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.524103416513" Y="4.769725165035" />
                  <Point X="2.768719849901" Y="3.816942017011" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.861057902663" Y="4.729900707309" />
                  <Point X="2.81657656313" Y="3.899832283312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.42487947738" Y="4.593776883859" />
                  <Point X="2.334634861393" Y="4.207608634435" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.0001640625" Y="0.001625976562" />
                  <Width Value="9.99646484375" />
                  <Height Value="9.978485351562" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.479624755859" Y="-3.567288574219" />
                  <Point X="0.471539031982" Y="-3.537112304688" />
                  <Point X="0.461013946533" Y="-3.516782470703" />
                  <Point X="0.300591033936" Y="-3.285643798828" />
                  <Point X="0.289073364258" Y="-3.273825195312" />
                  <Point X="0.269221740723" Y="-3.2648125" />
                  <Point X="0.020976625443" Y="-3.187766601562" />
                  <Point X="0.00615594244" Y="-3.18551953125" />
                  <Point X="-0.013778906822" Y="-3.189353759766" />
                  <Point X="-0.262023895264" Y="-3.266399902344" />
                  <Point X="-0.276761444092" Y="-3.273825439453" />
                  <Point X="-0.29158404541" Y="-3.290405273438" />
                  <Point X="-0.45200680542" Y="-3.521544189453" />
                  <Point X="-0.459227142334" Y="-3.537112304688" />
                  <Point X="-0.84774395752" Y="-4.987077148438" />
                  <Point X="-1.100246582031" Y="-4.938065429688" />
                  <Point X="-1.104755615234" Y="-4.936905273438" />
                  <Point X="-1.351589599609" Y="-4.873396972656" />
                  <Point X="-1.310123779297" Y="-4.558432128906" />
                  <Point X="-1.309150390625" Y="-4.5510390625" />
                  <Point X="-1.310905029297" Y="-4.528615722656" />
                  <Point X="-1.370210693359" Y="-4.230466308594" />
                  <Point X="-1.375948730469" Y="-4.215220703125" />
                  <Point X="-1.390991088867" Y="-4.198499511719" />
                  <Point X="-1.619543457031" Y="-3.998063964844" />
                  <Point X="-1.633376708984" Y="-3.989461914063" />
                  <Point X="-1.655489868164" Y="-3.985353271484" />
                  <Point X="-1.958829833984" Y="-3.965471435547" />
                  <Point X="-1.975041015625" Y="-3.967068115234" />
                  <Point X="-1.995085693359" Y="-3.977270263672" />
                  <Point X="-2.247844970703" Y="-4.146159179688" />
                  <Point X="-2.259734863281" Y="-4.157295410156" />
                  <Point X="-2.457095214844" Y="-4.414500488281" />
                  <Point X="-2.85583203125" Y="-4.167612304688" />
                  <Point X="-2.862080322266" Y="-4.162801269531" />
                  <Point X="-3.228581054688" Y="-3.880608154297" />
                  <Point X="-2.521060546875" Y="-2.655146240234" />
                  <Point X="-2.506033935547" Y="-2.629119873047" />
                  <Point X="-2.499762695312" Y="-2.597593505859" />
                  <Point X="-2.514330078125" Y="-2.5684140625" />
                  <Point X="-2.531328369141" Y="-2.551415771484" />
                  <Point X="-2.560157226562" Y="-2.537198730469" />
                  <Point X="-2.591683349609" Y="-2.543469726562" />
                  <Point X="-3.842959228516" Y="-3.265894287109" />
                  <Point X="-4.161703613281" Y="-2.847129150391" />
                  <Point X="-4.166180664062" Y="-2.839621826172" />
                  <Point X="-4.431020019531" Y="-2.395527099609" />
                  <Point X="-3.190078369141" Y="-1.443318969727" />
                  <Point X="-3.163786865234" Y="-1.423144775391" />
                  <Point X="-3.145666503906" Y="-1.395413085938" />
                  <Point X="-3.138117431641" Y="-1.366266235352" />
                  <Point X="-3.140327636719" Y="-1.334592163086" />
                  <Point X="-3.161699951172" Y="-1.310320922852" />
                  <Point X="-3.187641601562" Y="-1.295052734375" />
                  <Point X="-3.219529052734" Y="-1.288571044922" />
                  <Point X="-4.803283203125" Y="-1.497076293945" />
                  <Point X="-4.927393066406" Y="-1.011192199707" />
                  <Point X="-4.928577148438" Y="-1.002910766602" />
                  <Point X="-4.998396484375" Y="-0.514742126465" />
                  <Point X="-3.587413818359" Y="-0.136670471191" />
                  <Point X="-3.557463867188" Y="-0.128645431519" />
                  <Point X="-3.541335449219" Y="-0.121036117554" />
                  <Point X="-3.514142822266" Y="-0.102162895203" />
                  <Point X="-3.494712158203" Y="-0.075305999756" />
                  <Point X="-3.485647949219" Y="-0.046100738525" />
                  <Point X="-3.485834472656" Y="-0.015857278824" />
                  <Point X="-3.494898925781" Y="0.013347985268" />
                  <Point X="-3.514703125" Y="0.039991611481" />
                  <Point X="-3.541895751953" Y="0.05886498642" />
                  <Point X="-3.557463867188" Y="0.066085334778" />
                  <Point X="-4.998186523438" Y="0.452125701904" />
                  <Point X="-4.917645019531" Y="0.99641784668" />
                  <Point X="-4.915259277344" Y="1.005222290039" />
                  <Point X="-4.773516113281" Y="1.528298583984" />
                  <Point X="-3.774735107422" Y="1.396806640625" />
                  <Point X="-3.753266113281" Y="1.393980102539" />
                  <Point X="-3.731704833984" Y="1.395866577148" />
                  <Point X="-3.730465087891" Y="1.396257446289" />
                  <Point X="-3.670278808594" Y="1.415234130859" />
                  <Point X="-3.651534423828" Y="1.426056152344" />
                  <Point X="-3.639119873047" Y="1.443785888672" />
                  <Point X="-3.638622314453" Y="1.444987060547" />
                  <Point X="-3.614472412109" Y="1.503290161133" />
                  <Point X="-3.610714111328" Y="1.524605224609" />
                  <Point X="-3.616315917969" Y="1.54551159668" />
                  <Point X="-3.616916259766" Y="1.546664794922" />
                  <Point X="-3.646055664062" Y="1.602641235352" />
                  <Point X="-3.659967773438" Y="1.619221557617" />
                  <Point X="-4.476105957031" Y="2.245466064453" />
                  <Point X="-4.160014648438" Y="2.787006347656" />
                  <Point X="-4.153696777344" Y="2.795126953125" />
                  <Point X="-3.774671142578" Y="3.282310791016" />
                  <Point X="-3.172342773438" Y="2.934556396484" />
                  <Point X="-3.159157226562" Y="2.926943603516" />
                  <Point X="-3.138514648438" Y="2.920434814453" />
                  <Point X="-3.136787841797" Y="2.920283691406" />
                  <Point X="-3.052965332031" Y="2.912950195312" />
                  <Point X="-3.031506591797" Y="2.915775390625" />
                  <Point X="-3.013252441406" Y="2.927404785156" />
                  <Point X="-3.012026611328" Y="2.928630371094" />
                  <Point X="-2.952528808594" Y="2.988128173828" />
                  <Point X="-2.940899414062" Y="3.006382324219" />
                  <Point X="-2.93807421875" Y="3.027841064453" />
                  <Point X="-2.938225341797" Y="3.029568115234" />
                  <Point X="-2.945558837891" Y="3.113390625" />
                  <Point X="-2.952067138672" Y="3.134032714844" />
                  <Point X="-3.307278808594" Y="3.749277099609" />
                  <Point X="-2.752876220703" Y="4.174331542969" />
                  <Point X="-2.742926513672" Y="4.179859375" />
                  <Point X="-2.141548583984" Y="4.513972167969" />
                  <Point X="-1.971864135742" Y="4.292834960938" />
                  <Point X="-1.967827636719" Y="4.28757421875" />
                  <Point X="-1.951246826172" Y="4.273660644531" />
                  <Point X="-1.949324951172" Y="4.27266015625" />
                  <Point X="-1.856030761719" Y="4.22409375" />
                  <Point X="-1.835119873047" Y="4.218491699219" />
                  <Point X="-1.813801025391" Y="4.222253417969" />
                  <Point X="-1.811799316406" Y="4.223083496094" />
                  <Point X="-1.714635009766" Y="4.263330078125" />
                  <Point X="-1.696905395508" Y="4.275744628906" />
                  <Point X="-1.686083496094" Y="4.294488769531" />
                  <Point X="-1.685431884766" Y="4.296555175781" />
                  <Point X="-1.653804077148" Y="4.396865234375" />
                  <Point X="-1.651917480469" Y="4.41842578125" />
                  <Point X="-1.689137695313" Y="4.701141113281" />
                  <Point X="-0.968094604492" Y="4.903296386719" />
                  <Point X="-0.956035339355" Y="4.904707519531" />
                  <Point X="-0.22419960022" Y="4.990358398438" />
                  <Point X="-0.04604094696" Y="4.3254609375" />
                  <Point X="-0.042140380859" Y="4.310903320312" />
                  <Point X="-0.024282176971" Y="4.284176757813" />
                  <Point X="0.006155934334" Y="4.273844238281" />
                  <Point X="0.036594093323" Y="4.284176757813" />
                  <Point X="0.054452251434" Y="4.310903320312" />
                  <Point X="0.236648452759" Y="4.990868652344" />
                  <Point X="0.860205688477" Y="4.925565429688" />
                  <Point X="0.870184875488" Y="4.92315625" />
                  <Point X="1.508456787109" Y="4.769057617188" />
                  <Point X="1.514469848633" Y="4.766876953125" />
                  <Point X="1.931033447266" Y="4.615786132813" />
                  <Point X="1.93731652832" Y="4.61284765625" />
                  <Point X="2.338699462891" Y="4.425134277344" />
                  <Point X="2.344777099609" Y="4.421593261719" />
                  <Point X="2.732523681641" Y="4.19569140625" />
                  <Point X="2.738248779297" Y="4.191620117188" />
                  <Point X="3.068740478516" Y="3.956592773438" />
                  <Point X="2.247245361328" Y="2.533721435547" />
                  <Point X="2.229853515625" Y="2.50359765625" />
                  <Point X="2.224418701172" Y="2.489894042969" />
                  <Point X="2.203382324219" Y="2.411228271484" />
                  <Point X="2.202213623047" Y="2.390924316406" />
                  <Point X="2.210416015625" Y="2.322900878906" />
                  <Point X="2.218682128906" Y="2.3008125" />
                  <Point X="2.219549316406" Y="2.299534667969" />
                  <Point X="2.261639892578" Y="2.23750390625" />
                  <Point X="2.276217773438" Y="2.223336425781" />
                  <Point X="2.338248535156" Y="2.18124609375" />
                  <Point X="2.360329589844" Y="2.172980957031" />
                  <Point X="2.36173828125" Y="2.172811035156" />
                  <Point X="2.42976171875" Y="2.164608642578" />
                  <Point X="2.450291259766" Y="2.166381591797" />
                  <Point X="2.528950683594" Y="2.187416015625" />
                  <Point X="2.541033691406" Y="2.192417236328" />
                  <Point X="3.994248046875" Y="3.031430908203" />
                  <Point X="4.202591796875" Y="2.741880615234" />
                  <Point X="4.205783691406" Y="2.73660546875" />
                  <Point X="4.387512695312" Y="2.436296142578" />
                  <Point X="3.311514404297" Y="1.610653076172" />
                  <Point X="3.288616210938" Y="1.593082641602" />
                  <Point X="3.278204833984" Y="1.582311645508" />
                  <Point X="3.221589111328" Y="1.508451904297" />
                  <Point X="3.212685058594" Y="1.489946411133" />
                  <Point X="3.191595703125" Y="1.414535522461" />
                  <Point X="3.190779541016" Y="1.390965332031" />
                  <Point X="3.191136230469" Y="1.389236816406" />
                  <Point X="3.208448486328" Y="1.305332519531" />
                  <Point X="3.216616455078" Y="1.28648046875" />
                  <Point X="3.263704101562" Y="1.214909667969" />
                  <Point X="3.280947998047" Y="1.198819824219" />
                  <Point X="3.282353515625" Y="1.198028564453" />
                  <Point X="3.350590087891" Y="1.159617431641" />
                  <Point X="3.370466064453" Y="1.153368408203" />
                  <Point X="3.462726074219" Y="1.141175048828" />
                  <Point X="3.475803466797" Y="1.14117175293" />
                  <Point X="4.848975097656" Y="1.321953491211" />
                  <Point X="4.939188476562" Y="0.951386413574" />
                  <Point X="4.940194335938" Y="0.944924621582" />
                  <Point X="4.997858398438" Y="0.574556152344" />
                  <Point X="3.767409667969" Y="0.244858352661" />
                  <Point X="3.741167724609" Y="0.237826919556" />
                  <Point X="3.727219726562" Y="0.231739990234" />
                  <Point X="3.636576904297" Y="0.179346878052" />
                  <Point X="3.62114453125" Y="0.165498962402" />
                  <Point X="3.566759033203" Y="0.096198890686" />
                  <Point X="3.556985107422" Y="0.07473513031" />
                  <Point X="3.556611572266" Y="0.072785148621" />
                  <Point X="3.538482910156" Y="-0.021875303268" />
                  <Point X="3.538856445312" Y="-0.04263476944" />
                  <Point X="3.556985107422" Y="-0.137295227051" />
                  <Point X="3.566759033203" Y="-0.158758987427" />
                  <Point X="3.567879394531" Y="-0.160186599731" />
                  <Point X="3.622264892578" Y="-0.229486679077" />
                  <Point X="3.638444335938" Y="-0.24298638916" />
                  <Point X="3.729086914062" Y="-0.295379486084" />
                  <Point X="3.741167480469" Y="-0.300387023926" />
                  <Point X="4.998068359375" Y="-0.637172424316" />
                  <Point X="4.948431640625" Y="-0.966399902344" />
                  <Point X="4.947143066406" Y="-0.972047119141" />
                  <Point X="4.874545898438" Y="-1.290178344727" />
                  <Point X="3.442415283203" Y="-1.101634765625" />
                  <Point X="3.411982177734" Y="-1.097628295898" />
                  <Point X="3.391171386719" Y="-1.099137817383" />
                  <Point X="3.213271972656" Y="-1.137804931641" />
                  <Point X="3.183229980469" Y="-1.157361450195" />
                  <Point X="3.075701171875" Y="-1.286685424805" />
                  <Point X="3.064040283203" Y="-1.317520874023" />
                  <Point X="3.04862890625" Y="-1.485000854492" />
                  <Point X="3.049849121094" Y="-1.501458129883" />
                  <Point X="3.058388671875" Y="-1.519776733398" />
                  <Point X="3.156840576172" Y="-1.672912353516" />
                  <Point X="3.1684609375" Y="-1.685540893555" />
                  <Point X="4.33907421875" Y="-2.583784423828" />
                  <Point X="4.204133300781" Y="-2.802139648438" />
                  <Point X="4.201465332031" Y="-2.805930175781" />
                  <Point X="4.056688476562" Y="-3.011638427734" />
                  <Point X="2.780559570312" Y="-2.274864990234" />
                  <Point X="2.753454833984" Y="-2.259215820312" />
                  <Point X="2.732979003906" Y="-2.252525146484" />
                  <Point X="2.521250488281" Y="-2.214287109375" />
                  <Point X="2.504749267578" Y="-2.214074462891" />
                  <Point X="2.485453857422" Y="-2.221151855469" />
                  <Point X="2.309559570312" Y="-2.313723876953" />
                  <Point X="2.286692871094" Y="-2.338307128906" />
                  <Point X="2.194120849609" Y="-2.514201660156" />
                  <Point X="2.188950439453" Y="-2.529873291016" />
                  <Point X="2.189950683594" Y="-2.550736328125" />
                  <Point X="2.228188720703" Y="-2.76246484375" />
                  <Point X="2.234091552734" Y="-2.778578613281" />
                  <Point X="2.986673828125" Y="-4.082088867188" />
                  <Point X="2.835297851563" Y="-4.190212890625" />
                  <Point X="2.832319580078" Y="-4.192140625" />
                  <Point X="2.679775878906" Y="-4.290879394531" />
                  <Point X="1.703860961914" Y="-3.019042236328" />
                  <Point X="1.683177490234" Y="-2.992087158203" />
                  <Point X="1.666247558594" Y="-2.977701660156" />
                  <Point X="1.45742590332" Y="-2.843448730469" />
                  <Point X="1.442262207031" Y="-2.8369375" />
                  <Point X="1.421099975586" Y="-2.836150146484" />
                  <Point X="1.192718017578" Y="-2.857166015625" />
                  <Point X="1.161699829102" Y="-2.871529785156" />
                  <Point X="0.985349060059" Y="-3.018159667969" />
                  <Point X="0.974574951172" Y="-3.030659667969" />
                  <Point X="0.967370605469" Y="-3.050983886719" />
                  <Point X="0.91464251709" Y="-3.29357421875" />
                  <Point X="0.91392956543" Y="-3.310720214844" />
                  <Point X="1.127642211914" Y="-4.934028808594" />
                  <Point X="0.994343078613" Y="-4.963247558594" />
                  <Point X="0.991589050293" Y="-4.963748046875" />
                  <Point X="0.860200378418" Y="-4.987616699219" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#118" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.00390064914" Y="4.369681895088" Z="0.05" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.05" />
                  <Point X="-0.968003380622" Y="4.985648828303" Z="0.05" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.05" />
                  <Point X="-1.735049949642" Y="4.773202724265" Z="0.05" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.05" />
                  <Point X="-1.749657495632" Y="4.76229068577" Z="0.05" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.05" />
                  <Point X="-1.739273588101" Y="4.342870572963" Z="0.05" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.05" />
                  <Point X="-1.837097220393" Y="4.300553770622" Z="0.05" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.05" />
                  <Point X="-1.932393262754" Y="4.348290817886" Z="0.05" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.05" />
                  <Point X="-1.93835169858" Y="4.354551788394" Z="0.05" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.05" />
                  <Point X="-2.773365456428" Y="4.254846840066" Z="0.05" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.05" />
                  <Point X="-3.366716927053" Y="3.802710162212" Z="0.05" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.05" />
                  <Point X="-3.371056583724" Y="3.780360893541" Z="0.05" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.05" />
                  <Point X="-2.99419109081" Y="3.056490372642" Z="0.05" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.05" />
                  <Point X="-3.053537848399" Y="2.995265719487" Z="0.05" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.05" />
                  <Point X="-3.138586092965" Y="3.001373608363" Z="0.05" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.05" />
                  <Point X="-3.153498454756" Y="3.009137365022" Z="0.05" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.05" />
                  <Point X="-4.19931624633" Y="2.857109324516" Z="0.05" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.05" />
                  <Point X="-4.540791668474" Y="2.275656880946" Z="0.05" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.05" />
                  <Point X="-4.530474828928" Y="2.250717646477" Z="0.05" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.05" />
                  <Point X="-3.667422733659" Y="1.554857547071" Z="0.05" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.05" />
                  <Point X="-3.690972397514" Y="1.495401206497" Z="0.05" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.05" />
                  <Point X="-3.751656180949" Y="1.475223470073" Z="0.05" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.05" />
                  <Point X="-3.774364875135" Y="1.477658957397" Z="0.05" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.05" />
                  <Point X="-4.969675146706" Y="1.04957976763" Z="0.05" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.05" />
                  <Point X="-5.058560937174" Y="0.458578267408" Z="0.05" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.05" />
                  <Point X="-5.030377171099" Y="0.438617977872" Z="0.05" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.05" />
                  <Point X="-3.549368052329" Y="0.03019575693" Z="0.05" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.05" />
                  <Point X="-3.539743475892" Y="0.00060165449" Z="0.05" />
                  <Point X="-3.539556741714" Y="0" Z="0.05" />
                  <Point X="-3.548621125243" Y="-0.029205296571" Z="0.05" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.05" />
                  <Point X="-3.576000542794" Y="-0.048680226204" Z="0.05" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.05" />
                  <Point X="-3.606510617673" Y="-0.057094078884" Z="0.05" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.05" />
                  <Point X="-4.984229401837" Y="-0.978709775635" Z="0.05" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.05" />
                  <Point X="-4.849643612815" Y="-1.510431759269" Z="0.05" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.05" />
                  <Point X="-4.814047238116" Y="-1.516834302359" Z="0.05" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.05" />
                  <Point X="-3.193210857741" Y="-1.322135189324" Z="0.05" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.05" />
                  <Point X="-3.200225286589" Y="-1.351596718477" Z="0.05" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.05" />
                  <Point X="-3.226672215948" Y="-1.372371280762" Z="0.05" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.05" />
                  <Point X="-4.215280109534" Y="-2.833951380975" Z="0.05" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.05" />
                  <Point X="-3.86924543407" Y="-3.290720878224" Z="0.05" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.05" />
                  <Point X="-3.836212306232" Y="-3.284899591101" Z="0.05" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.05" />
                  <Point X="-2.555841151336" Y="-2.572489317796" Z="0.05" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.05" />
                  <Point X="-2.570517422067" Y="-2.59886607626" Z="0.05" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.05" />
                  <Point X="-2.89874028676" Y="-4.171138553667" Z="0.05" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.05" />
                  <Point X="-2.459986207747" Y="-4.444050442405" Z="0.05" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.05" />
                  <Point X="-2.446578229853" Y="-4.443625547797" Z="0.05" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.05" />
                  <Point X="-1.973463056298" Y="-3.987563618858" Z="0.05" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.05" />
                  <Point X="-1.66491591836" Y="-4.003966348519" Z="0.05" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.05" />
                  <Point X="-1.430114359069" Y="-4.204811361994" Z="0.05" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.05" />
                  <Point X="-1.366100284126" Y="-4.507090090598" Z="0.05" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.05" />
                  <Point X="-1.365851868284" Y="-4.520625426187" Z="0.05" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.05" />
                  <Point X="-1.12337049826" Y="-4.954047888065" Z="0.05" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.05" />
                  <Point X="-0.823718218124" Y="-5.012588597834" Z="0.05" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.05" />
                  <Point X="-0.809582330895" Y="-4.983586521031" Z="0.05" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.05" />
                  <Point X="-0.256663929494" Y="-3.287634335822" Z="0.05" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.05" />
                  <Point X="-0.005114063067" Y="-3.205826584888" Z="0.05" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.05" />
                  <Point X="0.248245016294" Y="-3.2812854604" Z="0.05" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.05" />
                  <Point X="0.413781929938" Y="-3.514011419946" Z="0.05" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.05" />
                  <Point X="0.425172529506" Y="-3.548949508784" Z="0.05" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.05" />
                  <Point X="0.994369946537" Y="-4.981662670772" Z="0.05" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.05" />
                  <Point X="1.173438988355" Y="-4.942547478528" Z="0.05" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.05" />
                  <Point X="1.172618176336" Y="-4.9080696609" Z="0.05" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.05" />
                  <Point X="1.010073773645" Y="-3.030323580723" Z="0.05" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.05" />
                  <Point X="1.187510772313" Y="-2.878696142601" Z="0.05" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.05" />
                  <Point X="1.419525591962" Y="-2.854659535104" Z="0.05" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.05" />
                  <Point X="1.63305199199" Y="-2.988479542273" Z="0.05" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.05" />
                  <Point X="1.658037361379" Y="-3.01820047973" Z="0.05" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.05" />
                  <Point X="2.853332915767" Y="-4.202834719354" Z="0.05" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.05" />
                  <Point X="3.042692806531" Y="-4.067843367192" Z="0.05" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.05" />
                  <Point X="3.030863632857" Y="-4.03801017363" Z="0.05" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.05" />
                  <Point X="2.232998849965" Y="-2.510569975499" Z="0.05" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.05" />
                  <Point X="2.324783132933" Y="-2.330313774627" Z="0.05" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.05" />
                  <Point X="2.502584603746" Y="-2.234118177034" Z="0.05" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.05" />
                  <Point X="2.717936828013" Y="-2.270449159526" Z="0.05" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.05" />
                  <Point X="2.749403423477" Y="-2.286885867417" Z="0.05" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.05" />
                  <Point X="4.236197797082" Y="-2.803427478795" Z="0.05" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.05" />
                  <Point X="4.395989308518" Y="-2.545556071662" Z="0.05" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.05" />
                  <Point X="4.374855987248" Y="-2.521660483069" Z="0.05" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.05" />
                  <Point X="3.094290705691" Y="-1.461457228435" Z="0.05" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.05" />
                  <Point X="3.107673967496" Y="-1.29082246569" Z="0.05" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.05" />
                  <Point X="3.2155203405" Y="-1.158048366925" Z="0.05" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.05" />
                  <Point X="3.395634976776" Y="-1.116717001991" Z="0.05" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.05" />
                  <Point X="3.429732983189" Y="-1.119927020836" Z="0.05" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.05" />
                  <Point X="4.989734132655" Y="-0.951891064569" Z="0.05" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.05" />
                  <Point X="5.04687311413" Y="-0.576735963862" Z="0.05" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.05" />
                  <Point X="5.02177330617" Y="-0.562129824021" Z="0.05" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.05" />
                  <Point X="3.657310032407" Y="-0.168417550475" Z="0.05" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.05" />
                  <Point X="3.601057114339" Y="-0.09803817216" Z="0.05" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.05" />
                  <Point X="3.581808190258" Y="-0.001950087443" Z="0.05" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.05" />
                  <Point X="3.599563260166" Y="0.094660443805" Z="0.05" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.05" />
                  <Point X="3.654322324062" Y="0.165910566415" Z="0.05" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.05" />
                  <Point X="3.746085381945" Y="0.219731293767" Z="0.05" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.05" />
                  <Point X="3.774194511136" Y="0.227842108748" Z="0.05" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.05" />
                  <Point X="4.983443123711" Y="0.983896672156" Z="0.05" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.05" />
                  <Point X="4.882830763218" Y="1.400261684516" Z="0.05" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.05" />
                  <Point X="4.852169860137" Y="1.404895836607" Z="0.05" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.05" />
                  <Point X="3.370861258359" Y="1.234217386644" Z="0.05" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.05" />
                  <Point X="3.300724279901" Y="1.272879727956" Z="0.05" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.05" />
                  <Point X="3.252231039542" Y="1.345241980211" Z="0.05" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.05" />
                  <Point X="3.233948682095" Y="1.430620703531" Z="0.05" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.05" />
                  <Point X="3.254681524036" Y="1.507760171147" Z="0.05" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.05" />
                  <Point X="3.31173182357" Y="1.583173524272" Z="0.05" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.05" />
                  <Point X="3.335796344946" Y="1.602265496211" Z="0.05" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.05" />
                  <Point X="4.242405230237" Y="2.793771726203" Z="0.05" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.05" />
                  <Point X="4.007023273151" Y="3.122008330542" Z="0.05" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.05" />
                  <Point X="3.972137346875" Y="3.11123459334" Z="0.05" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.05" />
                  <Point X="2.431212237163" Y="2.245962539392" Z="0.05" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.05" />
                  <Point X="2.361568131808" Y="2.253731551859" Z="0.05" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.05" />
                  <Point X="2.298135961221" Y="2.295990972545" Z="0.05" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.05" />
                  <Point X="2.25476762769" Y="2.358888899161" Z="0.05" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.05" />
                  <Point X="2.2456981509" Y="2.428190312024" Z="0.05" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.05" />
                  <Point X="2.266565403266" Y="2.508257338719" Z="0.05" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.05" />
                  <Point X="2.284390759519" Y="2.54000173508" Z="0.05" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.05" />
                  <Point X="2.761069827592" Y="4.263647221515" Z="0.05" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.05" />
                  <Point X="2.363791019259" Y="4.496076097035" Z="0.05" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.05" />
                  <Point X="1.952342054843" Y="4.689419755257" Z="0.05" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.05" />
                  <Point X="1.52536251974" Y="4.84516068365" Z="0.05" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.05" />
                  <Point X="0.875763290288" Y="5.003039966007" Z="0.05" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.05" />
                  <Point X="0.206754693237" Y="5.074908160767" Z="0.05" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.05" />
                  <Point X="0.189343912413" Y="5.061765610863" Z="0.05" />
                  <Point X="0" Y="4.355124473572" Z="0.05" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>