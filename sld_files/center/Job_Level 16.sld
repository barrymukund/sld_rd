<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#147" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1275" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="0" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000475830078" Y="0.004715820312" />
                  <Width Value="9.783896972656" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.681784179688" Y="-3.954705810547" />
                  <Point X="0.563302062988" Y="-3.512524414063" />
                  <Point X="0.557721191406" Y="-3.497141845703" />
                  <Point X="0.54236315918" Y="-3.467377197266" />
                  <Point X="0.493936737061" Y="-3.397603515625" />
                  <Point X="0.378635528564" Y="-3.231476806641" />
                  <Point X="0.356751586914" Y="-3.209021484375" />
                  <Point X="0.330496398926" Y="-3.189777587891" />
                  <Point X="0.302495178223" Y="-3.175669189453" />
                  <Point X="0.227557922363" Y="-3.152411376953" />
                  <Point X="0.049136188507" Y="-3.097035888672" />
                  <Point X="0.020976791382" Y="-3.092766601562" />
                  <Point X="-0.00866468811" Y="-3.092766601562" />
                  <Point X="-0.036824237823" Y="-3.097035888672" />
                  <Point X="-0.111761642456" Y="-3.120293701172" />
                  <Point X="-0.290183380127" Y="-3.175669189453" />
                  <Point X="-0.31818460083" Y="-3.189777587891" />
                  <Point X="-0.344439788818" Y="-3.209021484375" />
                  <Point X="-0.366323425293" Y="-3.2314765625" />
                  <Point X="-0.414750183105" Y="-3.30125" />
                  <Point X="-0.530051086426" Y="-3.467376953125" />
                  <Point X="-0.538189025879" Y="-3.481573730469" />
                  <Point X="-0.550990112305" Y="-3.512524414063" />
                  <Point X="-0.833089111328" Y="-4.565332519531" />
                  <Point X="-0.916584594727" Y="-4.876941894531" />
                  <Point X="-1.079341430664" Y="-4.845350097656" />
                  <Point X="-1.162575195312" Y="-4.823934570312" />
                  <Point X="-1.24641809082" Y="-4.802362792969" />
                  <Point X="-1.229225463867" Y="-4.671772460938" />
                  <Point X="-1.214963012695" Y="-4.563438476562" />
                  <Point X="-1.214201171875" Y="-4.5479296875" />
                  <Point X="-1.216508666992" Y="-4.516224121094" />
                  <Point X="-1.234411254883" Y="-4.426222167969" />
                  <Point X="-1.277036132812" Y="-4.211932617188" />
                  <Point X="-1.287938354492" Y="-4.182966308594" />
                  <Point X="-1.304010375977" Y="-4.15512890625" />
                  <Point X="-1.323644775391" Y="-4.131204101562" />
                  <Point X="-1.392637939453" Y="-4.070698730469" />
                  <Point X="-1.556905517578" Y="-3.926639160156" />
                  <Point X="-1.583188476562" Y="-3.910295410156" />
                  <Point X="-1.612885742188" Y="-3.897994384766" />
                  <Point X="-1.64302734375" Y="-3.890966308594" />
                  <Point X="-1.734596435547" Y="-3.884964599609" />
                  <Point X="-1.952616577148" Y="-3.870674804688" />
                  <Point X="-1.98341796875" Y="-3.873708496094" />
                  <Point X="-2.014466674805" Y="-3.882028076172" />
                  <Point X="-2.042657714844" Y="-3.894801513672" />
                  <Point X="-2.118958007812" Y="-3.945783691406" />
                  <Point X="-2.300624267578" Y="-4.067169677734" />
                  <Point X="-2.312785644531" Y="-4.076821533203" />
                  <Point X="-2.335102783203" Y="-4.099461914062" />
                  <Point X="-2.480149169922" Y="-4.288489746094" />
                  <Point X="-2.498580322266" Y="-4.277077636719" />
                  <Point X="-2.801707763672" Y="-4.089388916016" />
                  <Point X="-2.916963867188" Y="-4.000645507812" />
                  <Point X="-3.104721923828" Y="-3.856077880859" />
                  <Point X="-2.643947021484" Y="-3.057992431641" />
                  <Point X="-2.423761230469" Y="-2.676619628906" />
                  <Point X="-2.412859130859" Y="-2.647653320312" />
                  <Point X="-2.406588134766" Y="-2.616126953125" />
                  <Point X="-2.405575439453" Y="-2.585192871094" />
                  <Point X="-2.414560058594" Y="-2.555575195312" />
                  <Point X="-2.428777099609" Y="-2.526746337891" />
                  <Point X="-2.4468046875" Y="-2.501589355469" />
                  <Point X="-2.464153320312" Y="-2.484240722656" />
                  <Point X="-2.489310302734" Y="-2.466213134766" />
                  <Point X="-2.518139160156" Y="-2.45199609375" />
                  <Point X="-2.547756835938" Y="-2.443011474609" />
                  <Point X="-2.578690917969" Y="-2.444024169922" />
                  <Point X="-2.610217285156" Y="-2.450295166016" />
                  <Point X="-2.63918359375" Y="-2.461197265625" />
                  <Point X="-3.547209960938" Y="-2.985446533203" />
                  <Point X="-3.818024414062" Y="-3.141801269531" />
                  <Point X="-3.843372314453" Y="-3.108499511719" />
                  <Point X="-4.082859130859" Y="-2.793862792969" />
                  <Point X="-4.165486328125" Y="-2.655309326172" />
                  <Point X="-4.306142578125" Y="-2.419450439453" />
                  <Point X="-3.491208251953" Y="-1.79412878418" />
                  <Point X="-3.105954589844" Y="-1.498513427734" />
                  <Point X="-3.083062744141" Y="-1.475877441406" />
                  <Point X="-3.063636474609" Y="-1.444577026367" />
                  <Point X="-3.054644042969" Y="-1.420710571289" />
                  <Point X="-3.051577636719" Y="-1.411034301758" />
                  <Point X="-3.046151855469" Y="-1.390085449219" />
                  <Point X="-3.042037597656" Y="-1.358602905273" />
                  <Point X="-3.042736816406" Y="-1.33573449707" />
                  <Point X="-3.048883300781" Y="-1.313696411133" />
                  <Point X="-3.060888183594" Y="-1.284713867188" />
                  <Point X="-3.073802978516" Y="-1.262571655273" />
                  <Point X="-3.092167480469" Y="-1.244688354492" />
                  <Point X="-3.112502685547" Y="-1.229648803711" />
                  <Point X="-3.120804931641" Y="-1.224157226562" />
                  <Point X="-3.139454589844" Y="-1.213180908203" />
                  <Point X="-3.168718261719" Y="-1.201956542969" />
                  <Point X="-3.200605957031" Y="-1.195474853516" />
                  <Point X="-3.231929199219" Y="-1.194383789062" />
                  <Point X="-4.3782265625" Y="-1.34529675293" />
                  <Point X="-4.732102050781" Y="-1.391885253906" />
                  <Point X="-4.740411621094" Y="-1.359353027344" />
                  <Point X="-4.834077636719" Y="-0.992654296875" />
                  <Point X="-4.855937988281" Y="-0.839807250977" />
                  <Point X="-4.892424316406" Y="-0.584698242188" />
                  <Point X="-3.971737792969" Y="-0.338000915527" />
                  <Point X="-3.532875976562" Y="-0.220408447266" />
                  <Point X="-3.512112304688" Y="-0.212121444702" />
                  <Point X="-3.488335449219" Y="-0.19920362854" />
                  <Point X="-3.479519775391" Y="-0.193771957397" />
                  <Point X="-3.459975585938" Y="-0.180207199097" />
                  <Point X="-3.436020507812" Y="-0.158680587769" />
                  <Point X="-3.415095703125" Y="-0.128277282715" />
                  <Point X="-3.404934082031" Y="-0.104723350525" />
                  <Point X="-3.401431884766" Y="-0.095250419617" />
                  <Point X="-3.394917236328" Y="-0.074259628296" />
                  <Point X="-3.389474365234" Y="-0.04552053833" />
                  <Point X="-3.390444091797" Y="-0.011682662964" />
                  <Point X="-3.395427246094" Y="0.011954291344" />
                  <Point X="-3.397653564453" Y="0.020517091751" />
                  <Point X="-3.404168457031" Y="0.041507884979" />
                  <Point X="-3.417485351562" Y="0.070832427979" />
                  <Point X="-3.4403984375" Y="0.100128128052" />
                  <Point X="-3.460425537109" Y="0.117342826843" />
                  <Point X="-3.468184326172" Y="0.123344314575" />
                  <Point X="-3.487728515625" Y="0.136909072876" />
                  <Point X="-3.501924560547" Y="0.145046707153" />
                  <Point X="-3.532875976562" Y="0.157848251343" />
                  <Point X="-4.577780273438" Y="0.437829620361" />
                  <Point X="-4.89181640625" Y="0.521975341797" />
                  <Point X="-4.884854003906" Y="0.569027526855" />
                  <Point X="-4.824487792969" Y="0.97697479248" />
                  <Point X="-4.780481933594" Y="1.139369995117" />
                  <Point X="-4.703551269531" Y="1.423267822266" />
                  <Point X="-4.080253173828" Y="1.341209106445" />
                  <Point X="-3.765666259766" Y="1.29979284668" />
                  <Point X="-3.744986083984" Y="1.299341674805" />
                  <Point X="-3.723424560547" Y="1.301228149414" />
                  <Point X="-3.703137207031" Y="1.305263549805" />
                  <Point X="-3.684968994141" Y="1.31099206543" />
                  <Point X="-3.641711181641" Y="1.324631103516" />
                  <Point X="-3.622779052734" Y="1.332961669922" />
                  <Point X="-3.604034667969" Y="1.343783691406" />
                  <Point X="-3.587353271484" Y="1.356014892578" />
                  <Point X="-3.57371484375" Y="1.371566650391" />
                  <Point X="-3.561300537109" Y="1.389296264648" />
                  <Point X="-3.551351318359" Y="1.407431274414" />
                  <Point X="-3.544061279297" Y="1.42503137207" />
                  <Point X="-3.526703857422" Y="1.466935546875" />
                  <Point X="-3.520916015625" Y="1.48679296875" />
                  <Point X="-3.517157470703" Y="1.508108032227" />
                  <Point X="-3.515804443359" Y="1.528748535156" />
                  <Point X="-3.518951171875" Y="1.549192626953" />
                  <Point X="-3.524552978516" Y="1.570099121094" />
                  <Point X="-3.532050048828" Y="1.589378173828" />
                  <Point X="-3.540846435547" Y="1.606275634766" />
                  <Point X="-3.561789794922" Y="1.6465078125" />
                  <Point X="-3.57328125" Y="1.663705932617" />
                  <Point X="-3.587193847656" Y="1.680286376953" />
                  <Point X="-3.602135986328" Y="1.694590087891" />
                  <Point X="-4.201495605469" Y="2.154495117188" />
                  <Point X="-4.351860351563" Y="2.269874267578" />
                  <Point X="-4.315712402344" Y="2.331803955078" />
                  <Point X="-4.081154296875" Y="2.733658935547" />
                  <Point X="-3.964586181641" Y="2.883490478516" />
                  <Point X="-3.750504638672" Y="3.158661621094" />
                  <Point X="-3.399867431641" Y="2.956221191406" />
                  <Point X="-3.206656982422" Y="2.844670898438" />
                  <Point X="-3.187724121094" Y="2.836340332031" />
                  <Point X="-3.167081787109" Y="2.829831787109" />
                  <Point X="-3.146794921875" Y="2.825796386719" />
                  <Point X="-3.121491455078" Y="2.823582519531" />
                  <Point X="-3.061245605469" Y="2.818311767578" />
                  <Point X="-3.040564941406" Y="2.818762939453" />
                  <Point X="-3.019106201172" Y="2.821588134766" />
                  <Point X="-2.999014160156" Y="2.826504882812" />
                  <Point X="-2.980462402344" Y="2.835653564453" />
                  <Point X="-2.962208251953" Y="2.847282958984" />
                  <Point X="-2.946077636719" Y="2.860229248047" />
                  <Point X="-2.928116943359" Y="2.878189697266" />
                  <Point X="-2.885354003906" Y="2.920952636719" />
                  <Point X="-2.872406982422" Y="2.937083984375" />
                  <Point X="-2.860777587891" Y="2.955338134766" />
                  <Point X="-2.85162890625" Y="2.973889892578" />
                  <Point X="-2.846712158203" Y="2.993981933594" />
                  <Point X="-2.843886962891" Y="3.015440673828" />
                  <Point X="-2.843435791016" Y="3.036121337891" />
                  <Point X="-2.845649658203" Y="3.061424804688" />
                  <Point X="-2.850920410156" Y="3.121670898438" />
                  <Point X="-2.854955810547" Y="3.141957763672" />
                  <Point X="-2.861464355469" Y="3.162600097656" />
                  <Point X="-2.869794921875" Y="3.181532958984" />
                  <Point X="-3.135389404297" Y="3.641555908203" />
                  <Point X="-3.183333007812" Y="3.724596679688" />
                  <Point X="-3.109148925781" Y="3.781472900391" />
                  <Point X="-2.700624755859" Y="4.094684082031" />
                  <Point X="-2.517037597656" Y="4.196680664062" />
                  <Point X="-2.167036621094" Y="4.391133789062" />
                  <Point X="-2.102343017578" Y="4.306823242188" />
                  <Point X="-2.04319519043" Y="4.229740722656" />
                  <Point X="-2.028892700195" Y="4.214799804688" />
                  <Point X="-2.012312866211" Y="4.200887207031" />
                  <Point X="-1.99511328125" Y="4.18939453125" />
                  <Point X="-1.966950683594" Y="4.174733886719" />
                  <Point X="-1.899897094727" Y="4.139827636719" />
                  <Point X="-1.8806171875" Y="4.132330566406" />
                  <Point X="-1.859710571289" Y="4.126729003906" />
                  <Point X="-1.839267578125" Y="4.123582519531" />
                  <Point X="-1.818628295898" Y="4.124935546875" />
                  <Point X="-1.797312988281" Y="4.128693847656" />
                  <Point X="-1.777453491211" Y="4.134481933594" />
                  <Point X="-1.748120239258" Y="4.146632324219" />
                  <Point X="-1.678279541016" Y="4.175561523437" />
                  <Point X="-1.660146118164" Y="4.185509765625" />
                  <Point X="-1.642416381836" Y="4.197923828125" />
                  <Point X="-1.626864135742" Y="4.211562988281" />
                  <Point X="-1.614632568359" Y="4.228244628906" />
                  <Point X="-1.603810791016" Y="4.246988769531" />
                  <Point X="-1.59548046875" Y="4.265921386719" />
                  <Point X="-1.585932739258" Y="4.296202148437" />
                  <Point X="-1.563200927734" Y="4.368297851562" />
                  <Point X="-1.559165649414" Y="4.388584960938" />
                  <Point X="-1.557279174805" Y="4.410146972656" />
                  <Point X="-1.55773034668" Y="4.430826660156" />
                  <Point X="-1.584201904297" Y="4.631897949219" />
                  <Point X="-1.478494018555" Y="4.661534667969" />
                  <Point X="-0.949635009766" Y="4.80980859375" />
                  <Point X="-0.727077636719" Y="4.83585546875" />
                  <Point X="-0.294710784912" Y="4.886457519531" />
                  <Point X="-0.191060287476" Y="4.499628417969" />
                  <Point X="-0.133903305054" Y="4.286315429688" />
                  <Point X="-0.121129844666" Y="4.258124023438" />
                  <Point X="-0.10327155304" Y="4.231397460938" />
                  <Point X="-0.082114532471" Y="4.20880859375" />
                  <Point X="-0.054819446564" Y="4.19421875" />
                  <Point X="-0.024381370544" Y="4.183886230469" />
                  <Point X="0.006155906677" Y="4.178844238281" />
                  <Point X="0.03669316864" Y="4.183886230469" />
                  <Point X="0.067131248474" Y="4.19421875" />
                  <Point X="0.094426467896" Y="4.20880859375" />
                  <Point X="0.115583503723" Y="4.231397460938" />
                  <Point X="0.133441802979" Y="4.258124023438" />
                  <Point X="0.146215255737" Y="4.286315429688" />
                  <Point X="0.282302642822" Y="4.794201171875" />
                  <Point X="0.307419311523" Y="4.8879375" />
                  <Point X="0.382264526367" Y="4.880099121094" />
                  <Point X="0.844041564941" Y="4.831738769531" />
                  <Point X="1.028175170898" Y="4.787283203125" />
                  <Point X="1.481026611328" Y="4.677951171875" />
                  <Point X="1.599835693359" Y="4.634857910156" />
                  <Point X="1.894645629883" Y="4.527928222656" />
                  <Point X="2.010541015625" Y="4.473727539062" />
                  <Point X="2.294577392578" Y="4.340893066406" />
                  <Point X="2.40656640625" Y="4.2756484375" />
                  <Point X="2.680976318359" Y="4.115776367188" />
                  <Point X="2.78657421875" Y="4.040681152344" />
                  <Point X="2.943260009766" Y="3.929254882812" />
                  <Point X="2.402429443359" Y="2.992508300781" />
                  <Point X="2.147581054688" Y="2.551097900391" />
                  <Point X="2.142075683594" Y="2.539930664062" />
                  <Point X="2.133076904297" Y="2.516057128906" />
                  <Point X="2.1267265625" Y="2.492310302734" />
                  <Point X="2.111607177734" Y="2.435770751953" />
                  <Point X="2.108619384766" Y="2.417934326172" />
                  <Point X="2.107727783203" Y="2.380952880859" />
                  <Point X="2.110203857422" Y="2.360418701172" />
                  <Point X="2.116099121094" Y="2.311528076172" />
                  <Point X="2.121441894531" Y="2.289605224609" />
                  <Point X="2.129708007812" Y="2.267516357422" />
                  <Point X="2.140071044922" Y="2.247471191406" />
                  <Point X="2.152776855469" Y="2.22874609375" />
                  <Point X="2.183028808594" Y="2.184162597656" />
                  <Point X="2.194465332031" Y="2.170327880859" />
                  <Point X="2.221598876953" Y="2.145592773438" />
                  <Point X="2.240323974609" Y="2.132886962891" />
                  <Point X="2.284907470703" Y="2.102635253906" />
                  <Point X="2.304951904297" Y="2.092272460938" />
                  <Point X="2.327040283203" Y="2.084006347656" />
                  <Point X="2.348963867188" Y="2.078663330078" />
                  <Point X="2.369498046875" Y="2.076187255859" />
                  <Point X="2.418388671875" Y="2.070291748047" />
                  <Point X="2.436467529297" Y="2.069845703125" />
                  <Point X="2.473206787109" Y="2.074171142578" />
                  <Point X="2.496953613281" Y="2.080521484375" />
                  <Point X="2.553493164062" Y="2.095640869141" />
                  <Point X="2.565283447266" Y="2.099638183594" />
                  <Point X="2.588533935547" Y="2.110145019531" />
                  <Point X="3.639506591797" Y="2.716924316406" />
                  <Point X="3.967326416016" Y="2.906191162109" />
                  <Point X="4.123270019531" Y="2.689465087891" />
                  <Point X="4.182140625" Y="2.592181152344" />
                  <Point X="4.262198730469" Y="2.459884277344" />
                  <Point X="3.566315673828" Y="1.925913696289" />
                  <Point X="3.230783935547" Y="1.668451171875" />
                  <Point X="3.221425048828" Y="1.660241821289" />
                  <Point X="3.203973876953" Y="1.641627929688" />
                  <Point X="3.186883300781" Y="1.61933190918" />
                  <Point X="3.146191650391" Y="1.566246459961" />
                  <Point X="3.136605712891" Y="1.550911621094" />
                  <Point X="3.121629882812" Y="1.5170859375" />
                  <Point X="3.115263671875" Y="1.494321655273" />
                  <Point X="3.100105957031" Y="1.440121459961" />
                  <Point X="3.096652587891" Y="1.417823242188" />
                  <Point X="3.095836425781" Y="1.394252929688" />
                  <Point X="3.097739501953" Y="1.371767944336" />
                  <Point X="3.102965576172" Y="1.346439819336" />
                  <Point X="3.115408447266" Y="1.286135253906" />
                  <Point X="3.120679931641" Y="1.268977783203" />
                  <Point X="3.136282226562" Y="1.235740844727" />
                  <Point X="3.150496337891" Y="1.214135742188" />
                  <Point X="3.18433984375" Y="1.162695556641" />
                  <Point X="3.198893798828" Y="1.145450195312" />
                  <Point X="3.216137695313" Y="1.129360473633" />
                  <Point X="3.234347167969" Y="1.116034912109" />
                  <Point X="3.254945556641" Y="1.104439819336" />
                  <Point X="3.303989257812" Y="1.076832641602" />
                  <Point X="3.320520263672" Y="1.069502197266" />
                  <Point X="3.356118652344" Y="1.059438598633" />
                  <Point X="3.383968994141" Y="1.05575793457" />
                  <Point X="3.450279296875" Y="1.046994018555" />
                  <Point X="3.462702148438" Y="1.046175048828" />
                  <Point X="3.488203613281" Y="1.04698449707" />
                  <Point X="4.486557128906" Y="1.178420166016" />
                  <Point X="4.776839355469" Y="1.21663671875" />
                  <Point X="4.845936035156" Y="0.932809387207" />
                  <Point X="4.864487304688" Y="0.813657958984" />
                  <Point X="4.890864746094" Y="0.644238525391" />
                  <Point X="4.101106445313" Y="0.432623352051" />
                  <Point X="3.716579833984" Y="0.32958984375" />
                  <Point X="3.704791259766" Y="0.325586517334" />
                  <Point X="3.681545654297" Y="0.315067901611" />
                  <Point X="3.65418359375" Y="0.299252105713" />
                  <Point X="3.589035644531" Y="0.261595336914" />
                  <Point X="3.574311035156" Y="0.251096282959" />
                  <Point X="3.547530761719" Y="0.225576522827" />
                  <Point X="3.531113525391" Y="0.204657073975" />
                  <Point X="3.492024902344" Y="0.154848876953" />
                  <Point X="3.48030078125" Y="0.135568603516" />
                  <Point X="3.470527099609" Y="0.114104988098" />
                  <Point X="3.463680908203" Y="0.092604026794" />
                  <Point X="3.458208496094" Y="0.064029029846" />
                  <Point X="3.445178710938" Y="-0.004006621838" />
                  <Point X="3.443483154297" Y="-0.021874023438" />
                  <Point X="3.445178710938" Y="-0.058554176331" />
                  <Point X="3.450651367188" Y="-0.087129173279" />
                  <Point X="3.463680908203" Y="-0.155164672852" />
                  <Point X="3.470527099609" Y="-0.176665481567" />
                  <Point X="3.480301025391" Y="-0.198129257202" />
                  <Point X="3.492024902344" Y="-0.217408905029" />
                  <Point X="3.508442138672" Y="-0.238328353882" />
                  <Point X="3.547530761719" Y="-0.28813671875" />
                  <Point X="3.559999023438" Y="-0.301236083984" />
                  <Point X="3.589035888672" Y="-0.324155670166" />
                  <Point X="3.616398193359" Y="-0.339971466064" />
                  <Point X="3.681545898438" Y="-0.377628234863" />
                  <Point X="3.692710205078" Y="-0.383138977051" />
                  <Point X="3.716579833984" Y="-0.392150024414" />
                  <Point X="4.632115722656" Y="-0.63746685791" />
                  <Point X="4.89147265625" Y="-0.706961425781" />
                  <Point X="4.855022460938" Y="-0.948726257324" />
                  <Point X="4.831255371094" Y="-1.052877685547" />
                  <Point X="4.801173828125" Y="-1.184698974609" />
                  <Point X="3.870324951172" Y="-1.062150268555" />
                  <Point X="3.424382080078" Y="-1.003440979004" />
                  <Point X="3.408035644531" Y="-1.002710266113" />
                  <Point X="3.374658691406" Y="-1.005508666992" />
                  <Point X="3.320956298828" Y="-1.017181213379" />
                  <Point X="3.193094482422" Y="-1.04497253418" />
                  <Point X="3.163974121094" Y="-1.056596923828" />
                  <Point X="3.136147460938" Y="-1.073489257812" />
                  <Point X="3.112397460938" Y="-1.093960083008" />
                  <Point X="3.079937744141" Y="-1.132999145508" />
                  <Point X="3.002653320312" Y="-1.225948242188" />
                  <Point X="2.987932617188" Y="-1.250330444336" />
                  <Point X="2.976589355469" Y="-1.277715820312" />
                  <Point X="2.969757568359" Y="-1.305365356445" />
                  <Point X="2.965105224609" Y="-1.355922485352" />
                  <Point X="2.954028564453" Y="-1.476295532227" />
                  <Point X="2.956347167969" Y="-1.507564086914" />
                  <Point X="2.964078613281" Y="-1.539185058594" />
                  <Point X="2.976450195312" Y="-1.567996704102" />
                  <Point X="3.006169921875" Y="-1.614223510742" />
                  <Point X="3.076930419922" Y="-1.724287231445" />
                  <Point X="3.086932861328" Y="-1.737239013672" />
                  <Point X="3.110628417969" Y="-1.760909057617" />
                  <Point X="3.960251220703" Y="-2.412847900391" />
                  <Point X="4.213122558594" Y="-2.606883056641" />
                  <Point X="4.124811035156" Y="-2.749784912109" />
                  <Point X="4.075656982422" Y="-2.819625488281" />
                  <Point X="4.028981445312" Y="-2.8859453125" />
                  <Point X="3.198126953125" Y="-2.406250732422" />
                  <Point X="2.800954589844" Y="-2.176943115234" />
                  <Point X="2.786131591797" Y="-2.170012451172" />
                  <Point X="2.754224365234" Y="-2.159824951172" />
                  <Point X="2.690310058594" Y="-2.148282226562" />
                  <Point X="2.538134033203" Y="-2.120799316406" />
                  <Point X="2.506781982422" Y="-2.120395263672" />
                  <Point X="2.474609130859" Y="-2.125353027344" />
                  <Point X="2.444833251953" Y="-2.135176757812" />
                  <Point X="2.391736083984" Y="-2.163121337891" />
                  <Point X="2.265315185547" Y="-2.229655761719" />
                  <Point X="2.242384521484" Y="-2.246548828125" />
                  <Point X="2.221424804688" Y="-2.267508544922" />
                  <Point X="2.204531738281" Y="-2.290439208984" />
                  <Point X="2.176587158203" Y="-2.343536376953" />
                  <Point X="2.110052734375" Y="-2.469957275391" />
                  <Point X="2.100229003906" Y="-2.499733154297" />
                  <Point X="2.095271240234" Y="-2.531906005859" />
                  <Point X="2.095675292969" Y="-2.563258056641" />
                  <Point X="2.107218017578" Y="-2.627172363281" />
                  <Point X="2.134700927734" Y="-2.779348388672" />
                  <Point X="2.138985595703" Y="-2.795141845703" />
                  <Point X="2.151819091797" Y="-2.826078613281" />
                  <Point X="2.697787109375" Y="-3.77172265625" />
                  <Point X="2.861283203125" Y="-4.054906494141" />
                  <Point X="2.781845947266" Y="-4.111646484375" />
                  <Point X="2.726895019531" Y="-4.147215820312" />
                  <Point X="2.701765380859" Y="-4.163481933594" />
                  <Point X="2.061625244141" Y="-3.329235351562" />
                  <Point X="1.758546020508" Y="-2.934255126953" />
                  <Point X="1.747503540039" Y="-2.922179443359" />
                  <Point X="1.721924072266" Y="-2.900557373047" />
                  <Point X="1.658887084961" Y="-2.860030517578" />
                  <Point X="1.50880078125" Y="-2.763538574219" />
                  <Point X="1.479989746094" Y="-2.751167236328" />
                  <Point X="1.448368408203" Y="-2.743435546875" />
                  <Point X="1.417099609375" Y="-2.741116699219" />
                  <Point X="1.348158203125" Y="-2.747460693359" />
                  <Point X="1.184012695312" Y="-2.762565673828" />
                  <Point X="1.156362670898" Y="-2.769397460938" />
                  <Point X="1.128977661133" Y="-2.780740722656" />
                  <Point X="1.104595825195" Y="-2.795461181641" />
                  <Point X="1.051361083984" Y="-2.839724121094" />
                  <Point X="0.924612121582" Y="-2.945111572266" />
                  <Point X="0.904141052246" Y="-2.968861816406" />
                  <Point X="0.887248779297" Y="-2.996688476562" />
                  <Point X="0.875624206543" Y="-3.025808837891" />
                  <Point X="0.859707275391" Y="-3.099039306641" />
                  <Point X="0.821809936523" Y="-3.273396728516" />
                  <Point X="0.819724487305" Y="-3.289627441406" />
                  <Point X="0.819742248535" Y="-3.323120117188" />
                  <Point X="0.974465393066" Y="-4.498358886719" />
                  <Point X="1.022065307617" Y="-4.859915527344" />
                  <Point X="0.975686157227" Y="-4.87008203125" />
                  <Point X="0.929315551758" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058434936523" Y="-4.752635253906" />
                  <Point X="-1.138903320312" Y="-4.731931152344" />
                  <Point X="-1.141246459961" Y="-4.731328125" />
                  <Point X="-1.135038208008" Y="-4.684172363281" />
                  <Point X="-1.120775756836" Y="-4.575838378906" />
                  <Point X="-1.120077392578" Y="-4.568099609375" />
                  <Point X="-1.119451660156" Y="-4.541033691406" />
                  <Point X="-1.121759277344" Y="-4.509328125" />
                  <Point X="-1.123334106445" Y="-4.497690429688" />
                  <Point X="-1.141236694336" Y="-4.407688476562" />
                  <Point X="-1.183861572266" Y="-4.193398925781" />
                  <Point X="-1.18812512207" Y="-4.17846875" />
                  <Point X="-1.19902734375" Y="-4.149502441406" />
                  <Point X="-1.205666137695" Y="-4.135466308594" />
                  <Point X="-1.221738037109" Y="-4.10762890625" />
                  <Point X="-1.23057409668" Y="-4.094861816406" />
                  <Point X="-1.250208496094" Y="-4.070937011719" />
                  <Point X="-1.261006958008" Y="-4.059779296875" />
                  <Point X="-1.33000012207" Y="-3.999273925781" />
                  <Point X="-1.494267700195" Y="-3.855214355469" />
                  <Point X="-1.506739135742" Y="-3.84596484375" />
                  <Point X="-1.533022216797" Y="-3.82962109375" />
                  <Point X="-1.546833496094" Y="-3.822526855469" />
                  <Point X="-1.576530761719" Y="-3.810225830078" />
                  <Point X="-1.591313354492" Y="-3.805476074219" />
                  <Point X="-1.621455078125" Y="-3.798447998047" />
                  <Point X="-1.636814208984" Y="-3.796169677734" />
                  <Point X="-1.728383056641" Y="-3.79016796875" />
                  <Point X="-1.946403198242" Y="-3.775878173828" />
                  <Point X="-1.961928222656" Y="-3.776132324219" />
                  <Point X="-1.992729614258" Y="-3.779166015625" />
                  <Point X="-2.008006103516" Y="-3.781945556641" />
                  <Point X="-2.03905480957" Y="-3.790265136719" />
                  <Point X="-2.053674560547" Y="-3.795496337891" />
                  <Point X="-2.081865478516" Y="-3.808269775391" />
                  <Point X="-2.095436767578" Y="-3.815811767578" />
                  <Point X="-2.171737060547" Y="-3.866793945312" />
                  <Point X="-2.353403320312" Y="-3.988179931641" />
                  <Point X="-2.359681640625" Y="-3.992757080078" />
                  <Point X="-2.380442138672" Y="-4.010131103516" />
                  <Point X="-2.402759277344" Y="-4.032771484375" />
                  <Point X="-2.410471191406" Y="-4.041629638672" />
                  <Point X="-2.503202636719" Y="-4.162479492188" />
                  <Point X="-2.747583251953" Y="-4.011165283203" />
                  <Point X="-2.859006591797" Y="-3.925373046875" />
                  <Point X="-2.980862792969" Y="-3.831547607422" />
                  <Point X="-2.561674560547" Y="-3.105492431641" />
                  <Point X="-2.341488769531" Y="-2.724119628906" />
                  <Point X="-2.334850097656" Y="-2.710083251953" />
                  <Point X="-2.323947998047" Y="-2.681116943359" />
                  <Point X="-2.319684570312" Y="-2.666187011719" />
                  <Point X="-2.313413574219" Y="-2.634660644531" />
                  <Point X="-2.311638916016" Y="-2.619235351562" />
                  <Point X="-2.310626220703" Y="-2.588301269531" />
                  <Point X="-2.314666259766" Y="-2.557615234375" />
                  <Point X="-2.323650878906" Y="-2.527997558594" />
                  <Point X="-2.329357421875" Y="-2.513557128906" />
                  <Point X="-2.343574462891" Y="-2.484728271484" />
                  <Point X="-2.351557128906" Y="-2.47141015625" />
                  <Point X="-2.369584716797" Y="-2.446253173828" />
                  <Point X="-2.379629638672" Y="-2.434414306641" />
                  <Point X="-2.396978271484" Y="-2.417065673828" />
                  <Point X="-2.408817138672" Y="-2.407020751953" />
                  <Point X="-2.433974121094" Y="-2.388993164062" />
                  <Point X="-2.447292236328" Y="-2.381010498047" />
                  <Point X="-2.47612109375" Y="-2.366793457031" />
                  <Point X="-2.490561523438" Y="-2.361086914062" />
                  <Point X="-2.520179199219" Y="-2.352102294922" />
                  <Point X="-2.550865234375" Y="-2.348062255859" />
                  <Point X="-2.581799316406" Y="-2.349074951172" />
                  <Point X="-2.597224609375" Y="-2.350849609375" />
                  <Point X="-2.628750976562" Y="-2.357120605469" />
                  <Point X="-2.643680908203" Y="-2.361384033203" />
                  <Point X="-2.672647216797" Y="-2.372286132812" />
                  <Point X="-2.68668359375" Y="-2.378924804688" />
                  <Point X="-3.594709960938" Y="-2.903174072266" />
                  <Point X="-3.793089355469" Y="-3.017708496094" />
                  <Point X="-4.004015380859" Y="-2.740594970703" />
                  <Point X="-4.083893554688" Y="-2.606651123047" />
                  <Point X="-4.181265136719" Y="-2.443373779297" />
                  <Point X="-3.433375976562" Y="-1.869497314453" />
                  <Point X="-3.048122314453" Y="-1.573881958008" />
                  <Point X="-3.039157958984" Y="-1.566065063477" />
                  <Point X="-3.016266113281" Y="-1.543428955078" />
                  <Point X="-3.002345214844" Y="-1.525973999023" />
                  <Point X="-2.982918945312" Y="-1.494673583984" />
                  <Point X="-2.974737304688" Y="-1.478072509766" />
                  <Point X="-2.965744873047" Y="-1.454206054688" />
                  <Point X="-2.959612060547" Y="-1.434853393555" />
                  <Point X="-2.954186279297" Y="-1.413904541016" />
                  <Point X="-2.951952880859" Y="-1.402395629883" />
                  <Point X="-2.947838623047" Y="-1.370913208008" />
                  <Point X="-2.94708203125" Y="-1.355699584961" />
                  <Point X="-2.94778125" Y="-1.332831298828" />
                  <Point X="-2.951229248047" Y="-1.310212768555" />
                  <Point X="-2.957375732422" Y="-1.288174682617" />
                  <Point X="-2.961114746094" Y="-1.277341674805" />
                  <Point X="-2.973119628906" Y="-1.248359130859" />
                  <Point X="-2.978826660156" Y="-1.236850219727" />
                  <Point X="-2.991741455078" Y="-1.214708007812" />
                  <Point X="-3.007525390625" Y="-1.194510742188" />
                  <Point X="-3.025889892578" Y="-1.176627319336" />
                  <Point X="-3.035677978516" Y="-1.168308105469" />
                  <Point X="-3.056013183594" Y="-1.153268554688" />
                  <Point X="-3.072618652344" Y="-1.142284912109" />
                  <Point X="-3.091268310547" Y="-1.13130859375" />
                  <Point X="-3.105433105469" Y="-1.124481689453" />
                  <Point X="-3.134696777344" Y="-1.113257446289" />
                  <Point X="-3.149794921875" Y="-1.108860351562" />
                  <Point X="-3.181682617188" Y="-1.102378662109" />
                  <Point X="-3.197298828125" Y="-1.100532470703" />
                  <Point X="-3.228622070312" Y="-1.09944140625" />
                  <Point X="-3.244329101562" Y="-1.100196533203" />
                  <Point X="-4.390626464844" Y="-1.25110949707" />
                  <Point X="-4.660920410156" Y="-1.286694335938" />
                  <Point X="-4.74076171875" Y="-0.974118652344" />
                  <Point X="-4.761895019531" Y="-0.826357055664" />
                  <Point X="-4.786452148438" Y="-0.654654418945" />
                  <Point X="-3.947149902344" Y="-0.429763824463" />
                  <Point X="-3.508288085938" Y="-0.312171447754" />
                  <Point X="-3.497661621094" Y="-0.30864074707" />
                  <Point X="-3.476897949219" Y="-0.30035369873" />
                  <Point X="-3.466760498047" Y="-0.295597167969" />
                  <Point X="-3.442983642578" Y="-0.282679473877" />
                  <Point X="-3.425352539062" Y="-0.271816314697" />
                  <Point X="-3.405808349609" Y="-0.258251495361" />
                  <Point X="-3.396477783203" Y="-0.250868515015" />
                  <Point X="-3.372522705078" Y="-0.229341827393" />
                  <Point X="-3.357763671875" Y="-0.212540145874" />
                  <Point X="-3.336838867188" Y="-0.182136947632" />
                  <Point X="-3.3278671875" Y="-0.165909408569" />
                  <Point X="-3.317705566406" Y="-0.142355514526" />
                  <Point X="-3.310701171875" Y="-0.123409355164" />
                  <Point X="-3.304186523438" Y="-0.10241860199" />
                  <Point X="-3.301576416016" Y="-0.091937416077" />
                  <Point X="-3.296133544922" Y="-0.063198204041" />
                  <Point X="-3.294513427734" Y="-0.042799114227" />
                  <Point X="-3.295483154297" Y="-0.008961244583" />
                  <Point X="-3.297487304688" Y="0.007914588928" />
                  <Point X="-3.302470458984" Y="0.031551427841" />
                  <Point X="-3.306923095703" Y="0.048676948547" />
                  <Point X="-3.313437988281" Y="0.069667854309" />
                  <Point X="-3.317669921875" Y="0.080788711548" />
                  <Point X="-3.330986816406" Y="0.110113349915" />
                  <Point X="-3.342655029297" Y="0.129359573364" />
                  <Point X="-3.365568115234" Y="0.158655227661" />
                  <Point X="-3.378472412109" Y="0.17217098999" />
                  <Point X="-3.398499511719" Y="0.189385681152" />
                  <Point X="-3.414017089844" Y="0.201388626099" />
                  <Point X="-3.433561279297" Y="0.214953430176" />
                  <Point X="-3.440483398438" Y="0.21932800293" />
                  <Point X="-3.465615478516" Y="0.232834259033" />
                  <Point X="-3.496566894531" Y="0.245635742188" />
                  <Point X="-3.508288085938" Y="0.249611251831" />
                  <Point X="-4.553192382812" Y="0.529592590332" />
                  <Point X="-4.785446289062" Y="0.591824829102" />
                  <Point X="-4.731330566406" Y="0.957531921387" />
                  <Point X="-4.6887890625" Y="1.114523071289" />
                  <Point X="-4.633586425781" Y="1.318237060547" />
                  <Point X="-4.092653076172" Y="1.247021850586" />
                  <Point X="-3.778066162109" Y="1.20560546875" />
                  <Point X="-3.76773828125" Y="1.204815429688" />
                  <Point X="-3.747058105469" Y="1.204364257812" />
                  <Point X="-3.736705810547" Y="1.20470324707" />
                  <Point X="-3.715144287109" Y="1.20658972168" />
                  <Point X="-3.704891113281" Y="1.208053588867" />
                  <Point X="-3.684603759766" Y="1.212088989258" />
                  <Point X="-3.656401367188" Y="1.220389038086" />
                  <Point X="-3.613143554688" Y="1.234028076172" />
                  <Point X="-3.603449462891" Y="1.237676879883" />
                  <Point X="-3.584517333984" Y="1.246007446289" />
                  <Point X="-3.575279296875" Y="1.250689208984" />
                  <Point X="-3.556534912109" Y="1.261511230469" />
                  <Point X="-3.547860595703" Y="1.267171264648" />
                  <Point X="-3.531179199219" Y="1.27940246582" />
                  <Point X="-3.515928222656" Y="1.293377319336" />
                  <Point X="-3.502289794922" Y="1.308929077148" />
                  <Point X="-3.495895263672" Y="1.317077148438" />
                  <Point X="-3.483480957031" Y="1.334806884766" />
                  <Point X="-3.478011474609" Y="1.343602294922" />
                  <Point X="-3.468062255859" Y="1.361737304688" />
                  <Point X="-3.456292480469" Y="1.388677124023" />
                  <Point X="-3.438935058594" Y="1.430581420898" />
                  <Point X="-3.435499023438" Y="1.440352050781" />
                  <Point X="-3.429711181641" Y="1.460209472656" />
                  <Point X="-3.427359375" Y="1.470295898438" />
                  <Point X="-3.423600830078" Y="1.491610961914" />
                  <Point X="-3.422360839844" Y="1.501893920898" />
                  <Point X="-3.4210078125" Y="1.522534423828" />
                  <Point X="-3.42191015625" Y="1.543200683594" />
                  <Point X="-3.425056884766" Y="1.563644775391" />
                  <Point X="-3.427188232422" Y="1.573780151367" />
                  <Point X="-3.432790039062" Y="1.594686645508" />
                  <Point X="-3.436011962891" Y="1.604530273438" />
                  <Point X="-3.443509033203" Y="1.623809082031" />
                  <Point X="-3.456580566406" Y="1.650142089844" />
                  <Point X="-3.477523925781" Y="1.690374389648" />
                  <Point X="-3.482800292969" Y="1.699287109375" />
                  <Point X="-3.494291748047" Y="1.716485229492" />
                  <Point X="-3.500506835938" Y="1.724770629883" />
                  <Point X="-3.514419433594" Y="1.741351074219" />
                  <Point X="-3.521500732422" Y="1.748911621094" />
                  <Point X="-3.536442871094" Y="1.763215332031" />
                  <Point X="-3.544303710938" Y="1.769958618164" />
                  <Point X="-4.143663085938" Y="2.229863769531" />
                  <Point X="-4.227614746094" Y="2.294282226563" />
                  <Point X="-4.002292480469" Y="2.680313476562" />
                  <Point X="-3.889605712891" Y="2.825156005859" />
                  <Point X="-3.726338623047" Y="3.035012695312" />
                  <Point X="-3.447367431641" Y="2.873948730469" />
                  <Point X="-3.254156982422" Y="2.7623984375" />
                  <Point X="-3.244917480469" Y="2.757716064453" />
                  <Point X="-3.225984619141" Y="2.749385498047" />
                  <Point X="-3.216291259766" Y="2.745737304688" />
                  <Point X="-3.195648925781" Y="2.739228759766" />
                  <Point X="-3.185615722656" Y="2.736657226562" />
                  <Point X="-3.165328857422" Y="2.732621826172" />
                  <Point X="-3.155075195312" Y="2.731157958984" />
                  <Point X="-3.129771728516" Y="2.728944091797" />
                  <Point X="-3.069525878906" Y="2.723673339844" />
                  <Point X="-3.059173583984" Y="2.723334472656" />
                  <Point X="-3.038492919922" Y="2.723785644531" />
                  <Point X="-3.028164550781" Y="2.724575683594" />
                  <Point X="-3.006705810547" Y="2.727400878906" />
                  <Point X="-2.996524902344" Y="2.729310791016" />
                  <Point X="-2.976432861328" Y="2.734227539062" />
                  <Point X="-2.956996826172" Y="2.741302001953" />
                  <Point X="-2.938445068359" Y="2.750450683594" />
                  <Point X="-2.929418212891" Y="2.755531738281" />
                  <Point X="-2.9111640625" Y="2.767161132812" />
                  <Point X="-2.902745117188" Y="2.773194091797" />
                  <Point X="-2.886614501953" Y="2.786140380859" />
                  <Point X="-2.878902832031" Y="2.793053710938" />
                  <Point X="-2.860942138672" Y="2.811014160156" />
                  <Point X="-2.818179199219" Y="2.853777099609" />
                  <Point X="-2.811265625" Y="2.861489257812" />
                  <Point X="-2.798318603516" Y="2.877620605469" />
                  <Point X="-2.79228515625" Y="2.886039794922" />
                  <Point X="-2.780655761719" Y="2.904293945312" />
                  <Point X="-2.775574707031" Y="2.913320800781" />
                  <Point X="-2.766426025391" Y="2.931872558594" />
                  <Point X="-2.7593515625" Y="2.95130859375" />
                  <Point X="-2.754434814453" Y="2.971400634766" />
                  <Point X="-2.752524902344" Y="2.981581542969" />
                  <Point X="-2.749699707031" Y="3.003040283203" />
                  <Point X="-2.748909667969" Y="3.013368652344" />
                  <Point X="-2.748458496094" Y="3.034049316406" />
                  <Point X="-2.748797363281" Y="3.044401611328" />
                  <Point X="-2.751011230469" Y="3.069705078125" />
                  <Point X="-2.756281982422" Y="3.129951171875" />
                  <Point X="-2.757745849609" Y="3.140204833984" />
                  <Point X="-2.76178125" Y="3.160491699219" />
                  <Point X="-2.764352783203" Y="3.170524902344" />
                  <Point X="-2.770861328125" Y="3.191167236328" />
                  <Point X="-2.774509521484" Y="3.200860595703" />
                  <Point X="-2.782840087891" Y="3.219793457031" />
                  <Point X="-2.787522460938" Y="3.229032958984" />
                  <Point X="-3.053116943359" Y="3.689055908203" />
                  <Point X="-3.051346923828" Y="3.706081054688" />
                  <Point X="-2.648373291016" Y="4.015036621094" />
                  <Point X="-2.470900146484" Y="4.113636230469" />
                  <Point X="-2.192524658203" Y="4.268295898437" />
                  <Point X="-2.177711669922" Y="4.248991210937" />
                  <Point X="-2.118563720703" Y="4.171908691406" />
                  <Point X="-2.111820556641" Y="4.164047851562" />
                  <Point X="-2.097518066406" Y="4.149106933594" />
                  <Point X="-2.089958740234" Y="4.142026367188" />
                  <Point X="-2.07337890625" Y="4.128113769531" />
                  <Point X="-2.065093017578" Y="4.121897949219" />
                  <Point X="-2.047893432617" Y="4.110405273438" />
                  <Point X="-2.038979736328" Y="4.105128417969" />
                  <Point X="-2.010817138672" Y="4.090468017578" />
                  <Point X="-1.943763549805" Y="4.055561767578" />
                  <Point X="-1.934326782227" Y="4.051286132812" />
                  <Point X="-1.91504699707" Y="4.0437890625" />
                  <Point X="-1.905203613281" Y="4.040567138672" />
                  <Point X="-1.88429699707" Y="4.034965576172" />
                  <Point X="-1.874162475586" Y="4.032834716797" />
                  <Point X="-1.853719360352" Y="4.029688232422" />
                  <Point X="-1.833053222656" Y="4.028785888672" />
                  <Point X="-1.812413818359" Y="4.030138916016" />
                  <Point X="-1.802132446289" Y="4.031378662109" />
                  <Point X="-1.780817138672" Y="4.035136962891" />
                  <Point X="-1.770731079102" Y="4.037488525391" />
                  <Point X="-1.750871582031" Y="4.043276611328" />
                  <Point X="-1.741098144531" Y="4.046713623047" />
                  <Point X="-1.711764892578" Y="4.058864013672" />
                  <Point X="-1.641924316406" Y="4.087793212891" />
                  <Point X="-1.6325859375" Y="4.092272216797" />
                  <Point X="-1.614452514648" Y="4.102220703125" />
                  <Point X="-1.605657592773" Y="4.107689453125" />
                  <Point X="-1.587927978516" Y="4.120103515625" />
                  <Point X="-1.579777832031" Y="4.126499511719" />
                  <Point X="-1.564225708008" Y="4.140138671875" />
                  <Point X="-1.550252075195" Y="4.155388183594" />
                  <Point X="-1.538020507812" Y="4.172069824219" />
                  <Point X="-1.532359863281" Y="4.180745117188" />
                  <Point X="-1.521538085938" Y="4.199489257812" />
                  <Point X="-1.516855834961" Y="4.208728515625" />
                  <Point X="-1.508525512695" Y="4.227661132812" />
                  <Point X="-1.504877563477" Y="4.237353515625" />
                  <Point X="-1.495329956055" Y="4.267634277344" />
                  <Point X="-1.472598022461" Y="4.339729980469" />
                  <Point X="-1.470026245117" Y="4.349764648437" />
                  <Point X="-1.465990966797" Y="4.370051757812" />
                  <Point X="-1.46452722168" Y="4.380305175781" />
                  <Point X="-1.46264074707" Y="4.4018671875" />
                  <Point X="-1.462301757812" Y="4.412219238281" />
                  <Point X="-1.462752929688" Y="4.432898925781" />
                  <Point X="-1.46354309082" Y="4.4432265625" />
                  <Point X="-1.479266113281" Y="4.562654785156" />
                  <Point X="-1.452848266602" Y="4.570061523438" />
                  <Point X="-0.93117590332" Y="4.716320800781" />
                  <Point X="-0.716034729004" Y="4.741499511719" />
                  <Point X="-0.365221923828" Y="4.782557128906" />
                  <Point X="-0.28282321167" Y="4.475040527344" />
                  <Point X="-0.225666244507" Y="4.261727539062" />
                  <Point X="-0.220435317993" Y="4.247107910156" />
                  <Point X="-0.207661773682" Y="4.218916503906" />
                  <Point X="-0.200119155884" Y="4.205344726562" />
                  <Point X="-0.182260925293" Y="4.178618164062" />
                  <Point X="-0.172608306885" Y="4.166455566406" />
                  <Point X="-0.151451248169" Y="4.143866699219" />
                  <Point X="-0.126898002625" Y="4.125026367188" />
                  <Point X="-0.099602958679" Y="4.110436523438" />
                  <Point X="-0.085356712341" Y="4.104260742188" />
                  <Point X="-0.054918586731" Y="4.093927978516" />
                  <Point X="-0.039857292175" Y="4.090155273438" />
                  <Point X="-0.009319890976" Y="4.08511328125" />
                  <Point X="0.021631721497" Y="4.08511328125" />
                  <Point X="0.052168972015" Y="4.090155273438" />
                  <Point X="0.067230415344" Y="4.093927978516" />
                  <Point X="0.097668540955" Y="4.104260742188" />
                  <Point X="0.111914489746" Y="4.110436523438" />
                  <Point X="0.139209686279" Y="4.125026367188" />
                  <Point X="0.16376322937" Y="4.143866699219" />
                  <Point X="0.184920150757" Y="4.166455566406" />
                  <Point X="0.194572906494" Y="4.178617675781" />
                  <Point X="0.212431137085" Y="4.205344238281" />
                  <Point X="0.219973754883" Y="4.218916503906" />
                  <Point X="0.232747146606" Y="4.247107910156" />
                  <Point X="0.237978225708" Y="4.261727539062" />
                  <Point X="0.374065673828" Y="4.76961328125" />
                  <Point X="0.378190155029" Y="4.785006347656" />
                  <Point X="0.827877197266" Y="4.737912109375" />
                  <Point X="1.005879821777" Y="4.694936523438" />
                  <Point X="1.453595214844" Y="4.586844726562" />
                  <Point X="1.567443115234" Y="4.54555078125" />
                  <Point X="1.858257446289" Y="4.4400703125" />
                  <Point X="1.970296020508" Y="4.387673339844" />
                  <Point X="2.250449951172" Y="4.256654785156" />
                  <Point X="2.358743652344" Y="4.193562988281" />
                  <Point X="2.629430419922" Y="4.035860107422" />
                  <Point X="2.731517822266" Y="3.963261474609" />
                  <Point X="2.817779785156" Y="3.901916748047" />
                  <Point X="2.320156982422" Y="3.040008300781" />
                  <Point X="2.06530859375" Y="2.598597900391" />
                  <Point X="2.062373046875" Y="2.593104980469" />
                  <Point X="2.053181152344" Y="2.573438232422" />
                  <Point X="2.044182373047" Y="2.549564697266" />
                  <Point X="2.041301757812" Y="2.540599609375" />
                  <Point X="2.034951416016" Y="2.516852783203" />
                  <Point X="2.01983215332" Y="2.460313232422" />
                  <Point X="2.017912597656" Y="2.451465576172" />
                  <Point X="2.013646972656" Y="2.420224121094" />
                  <Point X="2.012755371094" Y="2.383242675781" />
                  <Point X="2.013411010742" Y="2.369579833984" />
                  <Point X="2.015887084961" Y="2.349045654297" />
                  <Point X="2.021782348633" Y="2.300155029297" />
                  <Point X="2.023800537109" Y="2.289034179688" />
                  <Point X="2.029143310547" Y="2.267111328125" />
                  <Point X="2.032467895508" Y="2.256309326172" />
                  <Point X="2.040734008789" Y="2.234220458984" />
                  <Point X="2.045318603516" Y="2.223888183594" />
                  <Point X="2.055681640625" Y="2.203843017578" />
                  <Point X="2.061459960938" Y="2.194130126953" />
                  <Point X="2.074165771484" Y="2.175405029297" />
                  <Point X="2.104417724609" Y="2.130821533203" />
                  <Point X="2.109807861328" Y="2.123634277344" />
                  <Point X="2.13046484375" Y="2.100121582031" />
                  <Point X="2.157598388672" Y="2.075386474609" />
                  <Point X="2.1682578125" Y="2.066981689453" />
                  <Point X="2.186982910156" Y="2.054275878906" />
                  <Point X="2.23156640625" Y="2.024024047852" />
                  <Point X="2.241279052734" Y="2.01824597168" />
                  <Point X="2.261323486328" Y="2.007883178711" />
                  <Point X="2.271655273438" Y="2.003298706055" />
                  <Point X="2.293743652344" Y="1.995032592773" />
                  <Point X="2.304546142578" Y="1.991707885742" />
                  <Point X="2.326469726562" Y="1.986364868164" />
                  <Point X="2.337590820312" Y="1.984346557617" />
                  <Point X="2.358125" Y="1.981870605469" />
                  <Point X="2.407015625" Y="1.975974975586" />
                  <Point X="2.416045410156" Y="1.975320800781" />
                  <Point X="2.447575439453" Y="1.975497314453" />
                  <Point X="2.484314697266" Y="1.979822631836" />
                  <Point X="2.497749267578" Y="1.982395996094" />
                  <Point X="2.52149609375" Y="1.988746337891" />
                  <Point X="2.578035644531" Y="2.003865722656" />
                  <Point X="2.58399609375" Y="2.005671020508" />
                  <Point X="2.604404541016" Y="2.013067260742" />
                  <Point X="2.627655029297" Y="2.02357409668" />
                  <Point X="2.636033935547" Y="2.027872558594" />
                  <Point X="3.687006591797" Y="2.634651855469" />
                  <Point X="3.940404785156" Y="2.780951416016" />
                  <Point X="4.043950439453" Y="2.637046630859" />
                  <Point X="4.100863769531" Y="2.542997070312" />
                  <Point X="4.136884765625" Y="2.483472167969" />
                  <Point X="3.508483398438" Y="2.001282226562" />
                  <Point X="3.172951660156" Y="1.743819702148" />
                  <Point X="3.168138183594" Y="1.739869018555" />
                  <Point X="3.152120117188" Y="1.725217529297" />
                  <Point X="3.134668945312" Y="1.706603637695" />
                  <Point X="3.128576416016" Y="1.699422485352" />
                  <Point X="3.111485839844" Y="1.677126464844" />
                  <Point X="3.070794189453" Y="1.624041015625" />
                  <Point X="3.065635742188" Y="1.616602661133" />
                  <Point X="3.049738525391" Y="1.589370727539" />
                  <Point X="3.034762695312" Y="1.555545043945" />
                  <Point X="3.030140136719" Y="1.54267175293" />
                  <Point X="3.023773925781" Y="1.519907470703" />
                  <Point X="3.008616210938" Y="1.46570715332" />
                  <Point X="3.006225097656" Y="1.454661010742" />
                  <Point X="3.002771728516" Y="1.432362792969" />
                  <Point X="3.001709472656" Y="1.421110839844" />
                  <Point X="3.000893310547" Y="1.397540527344" />
                  <Point X="3.001174804688" Y="1.386240966797" />
                  <Point X="3.003077880859" Y="1.363756103516" />
                  <Point X="3.004699462891" Y="1.352570556641" />
                  <Point X="3.009925537109" Y="1.327242431641" />
                  <Point X="3.022368408203" Y="1.266937866211" />
                  <Point X="3.024597900391" Y="1.25823449707" />
                  <Point X="3.03468359375" Y="1.228608886719" />
                  <Point X="3.050285888672" Y="1.195371948242" />
                  <Point X="3.05691796875" Y="1.183526733398" />
                  <Point X="3.071132080078" Y="1.161921630859" />
                  <Point X="3.104975585938" Y="1.110481567383" />
                  <Point X="3.111738769531" Y="1.101425048828" />
                  <Point X="3.126292724609" Y="1.0841796875" />
                  <Point X="3.134083496094" Y="1.075990722656" />
                  <Point X="3.151327392578" Y="1.059901000977" />
                  <Point X="3.160034912109" Y="1.052695678711" />
                  <Point X="3.178244384766" Y="1.039370117188" />
                  <Point X="3.187746337891" Y="1.03324987793" />
                  <Point X="3.208344726563" Y="1.021654663086" />
                  <Point X="3.257388427734" Y="0.994047485352" />
                  <Point X="3.265479248047" Y="0.98998815918" />
                  <Point X="3.294676757813" Y="0.978084960938" />
                  <Point X="3.330275146484" Y="0.96802142334" />
                  <Point X="3.343671875" Y="0.965257446289" />
                  <Point X="3.371522216797" Y="0.961576843262" />
                  <Point X="3.437832519531" Y="0.952812988281" />
                  <Point X="3.444030029297" Y="0.952199768066" />
                  <Point X="3.465716064453" Y="0.951222839355" />
                  <Point X="3.491217529297" Y="0.952032226562" />
                  <Point X="3.500603515625" Y="0.952797241211" />
                  <Point X="4.49895703125" Y="1.084232910156" />
                  <Point X="4.704703613281" Y="1.111320068359" />
                  <Point X="4.75268359375" Y="0.914233764648" />
                  <Point X="4.770618164062" Y="0.79904309082" />
                  <Point X="4.78387109375" Y="0.713920837402" />
                  <Point X="4.076518554688" Y="0.524386352539" />
                  <Point X="3.691991943359" Y="0.421352752686" />
                  <Point X="3.686031738281" Y="0.419544311523" />
                  <Point X="3.665626708984" Y="0.412138000488" />
                  <Point X="3.642381103516" Y="0.401619354248" />
                  <Point X="3.634004394531" Y="0.39731640625" />
                  <Point X="3.606642333984" Y="0.381500701904" />
                  <Point X="3.541494384766" Y="0.343843963623" />
                  <Point X="3.533882568359" Y="0.338945953369" />
                  <Point X="3.508773925781" Y="0.319870513916" />
                  <Point X="3.481993652344" Y="0.294350769043" />
                  <Point X="3.472796630859" Y="0.2842265625" />
                  <Point X="3.456379394531" Y="0.263307159424" />
                  <Point X="3.417290771484" Y="0.213498855591" />
                  <Point X="3.410854248047" Y="0.204207855225" />
                  <Point X="3.399130126953" Y="0.184927597046" />
                  <Point X="3.393842529297" Y="0.174938354492" />
                  <Point X="3.384068847656" Y="0.15347467041" />
                  <Point X="3.380005126953" Y="0.142928390503" />
                  <Point X="3.373158935547" Y="0.12142741394" />
                  <Point X="3.370376464844" Y="0.110472869873" />
                  <Point X="3.364904052734" Y="0.081897735596" />
                  <Point X="3.351874267578" Y="0.01386218071" />
                  <Point X="3.350603515625" Y="0.004968288422" />
                  <Point X="3.348584472656" Y="-0.026260803223" />
                  <Point X="3.350280029297" Y="-0.062940937042" />
                  <Point X="3.351874511719" Y="-0.076423706055" />
                  <Point X="3.357347167969" Y="-0.104998695374" />
                  <Point X="3.370376708984" Y="-0.173034240723" />
                  <Point X="3.373159179688" Y="-0.183988204956" />
                  <Point X="3.380005371094" Y="-0.205489028931" />
                  <Point X="3.384069091797" Y="-0.216035766602" />
                  <Point X="3.393843017578" Y="-0.237499435425" />
                  <Point X="3.399130615234" Y="-0.247488677979" />
                  <Point X="3.410854492188" Y="-0.266768341064" />
                  <Point X="3.417290771484" Y="-0.276058898926" />
                  <Point X="3.433708007812" Y="-0.29697833252" />
                  <Point X="3.472796630859" Y="-0.346786773682" />
                  <Point X="3.478718505859" Y="-0.35363381958" />
                  <Point X="3.501139648438" Y="-0.375805236816" />
                  <Point X="3.530176513672" Y="-0.398724822998" />
                  <Point X="3.541494873047" Y="-0.40640447998" />
                  <Point X="3.568857177734" Y="-0.422220184326" />
                  <Point X="3.634004882812" Y="-0.459877044678" />
                  <Point X="3.639497070313" Y="-0.462815612793" />
                  <Point X="3.659157958984" Y="-0.472016723633" />
                  <Point X="3.683027587891" Y="-0.481027740479" />
                  <Point X="3.691991943359" Y="-0.48391293335" />
                  <Point X="4.607527832031" Y="-0.729229797363" />
                  <Point X="4.784876953125" Y="-0.776750427246" />
                  <Point X="4.76161328125" Y="-0.931053039551" />
                  <Point X="4.738636230469" Y="-1.03174230957" />
                  <Point X="4.727801757812" Y="-1.079219604492" />
                  <Point X="3.882724853516" Y="-0.96796307373" />
                  <Point X="3.436781982422" Y="-0.909253723145" />
                  <Point X="3.428624511719" Y="-0.908535705566" />
                  <Point X="3.400098388672" Y="-0.908042480469" />
                  <Point X="3.366721435547" Y="-0.910840881348" />
                  <Point X="3.354480957031" Y="-0.912676208496" />
                  <Point X="3.300778564453" Y="-0.924348754883" />
                  <Point X="3.172916748047" Y="-0.952140075684" />
                  <Point X="3.157874511719" Y="-0.956742492676" />
                  <Point X="3.128754150391" Y="-0.968366882324" />
                  <Point X="3.114676269531" Y="-0.975388977051" />
                  <Point X="3.086849609375" Y="-0.992281311035" />
                  <Point X="3.074124023438" Y="-1.001530273438" />
                  <Point X="3.050374023438" Y="-1.022001159668" />
                  <Point X="3.039349365234" Y="-1.033223022461" />
                  <Point X="3.006889648438" Y="-1.072262084961" />
                  <Point X="2.929605224609" Y="-1.165211181641" />
                  <Point X="2.921326171875" Y="-1.176847167969" />
                  <Point X="2.90660546875" Y="-1.201229370117" />
                  <Point X="2.900163818359" Y="-1.213975830078" />
                  <Point X="2.888820556641" Y="-1.241361328125" />
                  <Point X="2.884362792969" Y="-1.254928222656" />
                  <Point X="2.877531005859" Y="-1.282577636719" />
                  <Point X="2.875157226562" Y="-1.29666003418" />
                  <Point X="2.870504882812" Y="-1.347217163086" />
                  <Point X="2.859428222656" Y="-1.467590209961" />
                  <Point X="2.859288574219" Y="-1.483320556641" />
                  <Point X="2.861607177734" Y="-1.514588989258" />
                  <Point X="2.864065429688" Y="-1.530127319336" />
                  <Point X="2.871796875" Y="-1.561748291016" />
                  <Point X="2.876785888672" Y="-1.57666809082" />
                  <Point X="2.889157470703" Y="-1.605479614258" />
                  <Point X="2.896540283203" Y="-1.619371704102" />
                  <Point X="2.926260009766" Y="-1.665598510742" />
                  <Point X="2.997020507812" Y="-1.775662231445" />
                  <Point X="3.0017421875" Y="-1.782353881836" />
                  <Point X="3.019793945312" Y="-1.804450317383" />
                  <Point X="3.043489501953" Y="-1.828120361328" />
                  <Point X="3.052796142578" Y="-1.83627746582" />
                  <Point X="3.902418945312" Y="-2.488216552734" />
                  <Point X="4.087170898438" Y="-2.629981933594" />
                  <Point X="4.045489257812" Y="-2.697429199219" />
                  <Point X="4.001274414063" Y="-2.760251953125" />
                  <Point X="3.245626953125" Y="-2.323978271484" />
                  <Point X="2.848454589844" Y="-2.094670654297" />
                  <Point X="2.841191894531" Y="-2.090885253906" />
                  <Point X="2.815026611328" Y="-2.079513427734" />
                  <Point X="2.783119384766" Y="-2.069325927734" />
                  <Point X="2.771107910156" Y="-2.066337158203" />
                  <Point X="2.707193603516" Y="-2.054794433594" />
                  <Point X="2.555017578125" Y="-2.027311645508" />
                  <Point X="2.539358154297" Y="-2.025807250977" />
                  <Point X="2.508006103516" Y="-2.025403198242" />
                  <Point X="2.492313476562" Y="-2.026503540039" />
                  <Point X="2.460140625" Y="-2.031461303711" />
                  <Point X="2.444844482422" Y="-2.035136108398" />
                  <Point X="2.415068603516" Y="-2.044959960938" />
                  <Point X="2.400588867188" Y="-2.051108642578" />
                  <Point X="2.347491699219" Y="-2.079053222656" />
                  <Point X="2.221070800781" Y="-2.145587646484" />
                  <Point X="2.208968261719" Y="-2.153170410156" />
                  <Point X="2.186037597656" Y="-2.170063476562" />
                  <Point X="2.175209472656" Y="-2.179373779297" />
                  <Point X="2.154249755859" Y="-2.200333496094" />
                  <Point X="2.144939453125" Y="-2.211161621094" />
                  <Point X="2.128046386719" Y="-2.234092285156" />
                  <Point X="2.120463623047" Y="-2.246194824219" />
                  <Point X="2.092519042969" Y="-2.299291992188" />
                  <Point X="2.025984619141" Y="-2.425712890625" />
                  <Point X="2.0198359375" Y="-2.440192626953" />
                  <Point X="2.010012084961" Y="-2.469968505859" />
                  <Point X="2.006337280273" Y="-2.485264648438" />
                  <Point X="2.001379516602" Y="-2.5174375" />
                  <Point X="2.000279174805" Y="-2.533130126953" />
                  <Point X="2.000683227539" Y="-2.564482177734" />
                  <Point X="2.00218762207" Y="-2.580141601562" />
                  <Point X="2.01373034668" Y="-2.644055908203" />
                  <Point X="2.041213256836" Y="-2.796231933594" />
                  <Point X="2.043015136719" Y="-2.804222167969" />
                  <Point X="2.051236083984" Y="-2.83154296875" />
                  <Point X="2.064069580078" Y="-2.862479736328" />
                  <Point X="2.069546630859" Y="-2.873578613281" />
                  <Point X="2.615514648438" Y="-3.81922265625" />
                  <Point X="2.735892822266" Y="-4.027724365234" />
                  <Point X="2.723754150391" Y="-4.036083496094" />
                  <Point X="2.136993896484" Y="-3.271403076172" />
                  <Point X="1.833914550781" Y="-2.876422851562" />
                  <Point X="1.828653442383" Y="-2.870146240234" />
                  <Point X="1.808831542969" Y="-2.849626953125" />
                  <Point X="1.783252075195" Y="-2.828004882812" />
                  <Point X="1.773298828125" Y="-2.820647216797" />
                  <Point X="1.71026184082" Y="-2.780120361328" />
                  <Point X="1.560175415039" Y="-2.683628417969" />
                  <Point X="1.546283935547" Y="-2.676245849609" />
                  <Point X="1.517472900391" Y="-2.663874511719" />
                  <Point X="1.502553344727" Y="-2.658885742188" />
                  <Point X="1.470932006836" Y="-2.651154052734" />
                  <Point X="1.455394042969" Y="-2.648695800781" />
                  <Point X="1.424125366211" Y="-2.646376953125" />
                  <Point X="1.40839453125" Y="-2.646516357422" />
                  <Point X="1.339453125" Y="-2.652860351562" />
                  <Point X="1.175307739258" Y="-2.667965332031" />
                  <Point X="1.161225341797" Y="-2.670339111328" />
                  <Point X="1.133575317383" Y="-2.677170898438" />
                  <Point X="1.120007568359" Y="-2.68162890625" />
                  <Point X="1.092622680664" Y="-2.692972167969" />
                  <Point X="1.079876708984" Y="-2.699413574219" />
                  <Point X="1.055494873047" Y="-2.714134033203" />
                  <Point X="1.043858764648" Y="-2.722413085938" />
                  <Point X="0.990624084473" Y="-2.766676025391" />
                  <Point X="0.863875061035" Y="-2.872063476562" />
                  <Point X="0.852653320312" Y="-2.883087890625" />
                  <Point X="0.832182128906" Y="-2.906838134766" />
                  <Point X="0.822933044434" Y="-2.919564208984" />
                  <Point X="0.80604083252" Y="-2.947390869141" />
                  <Point X="0.799018859863" Y="-2.961468017578" />
                  <Point X="0.787394287109" Y="-2.990588378906" />
                  <Point X="0.782791748047" Y="-3.005631347656" />
                  <Point X="0.766874816895" Y="-3.078861816406" />
                  <Point X="0.728977478027" Y="-3.253219238281" />
                  <Point X="0.727584594727" Y="-3.261289794922" />
                  <Point X="0.724724487305" Y="-3.289677734375" />
                  <Point X="0.7247421875" Y="-3.323170410156" />
                  <Point X="0.725554992676" Y="-3.335520019531" />
                  <Point X="0.833091491699" Y="-4.152340820313" />
                  <Point X="0.773547119141" Y="-3.930117919922" />
                  <Point X="0.655064941406" Y="-3.487936523438" />
                  <Point X="0.652606262207" Y="-3.480124511719" />
                  <Point X="0.642145141602" Y="-3.453580566406" />
                  <Point X="0.62678717041" Y="-3.423815917969" />
                  <Point X="0.620407714844" Y="-3.413210449219" />
                  <Point X="0.571981323242" Y="-3.343436767578" />
                  <Point X="0.456680114746" Y="-3.177310058594" />
                  <Point X="0.446670654297" Y="-3.165172851562" />
                  <Point X="0.424786804199" Y="-3.142717529297" />
                  <Point X="0.41291229248" Y="-3.132399169922" />
                  <Point X="0.386657165527" Y="-3.113155273438" />
                  <Point X="0.373242767334" Y="-3.104937988281" />
                  <Point X="0.345241607666" Y="-3.090829589844" />
                  <Point X="0.330654724121" Y="-3.084938476562" />
                  <Point X="0.255717407227" Y="-3.061680664062" />
                  <Point X="0.077295753479" Y="-3.006305175781" />
                  <Point X="0.063376476288" Y="-3.003109375" />
                  <Point X="0.035217193604" Y="-2.998840087891" />
                  <Point X="0.020976739883" Y="-2.997766601562" />
                  <Point X="-0.008664761543" Y="-2.997766601562" />
                  <Point X="-0.022904916763" Y="-2.99883984375" />
                  <Point X="-0.051064498901" Y="-3.003109130859" />
                  <Point X="-0.064983627319" Y="-3.006305175781" />
                  <Point X="-0.139921081543" Y="-3.029562988281" />
                  <Point X="-0.318342895508" Y="-3.084938476562" />
                  <Point X="-0.332929779053" Y="-3.090829589844" />
                  <Point X="-0.360930938721" Y="-3.104937988281" />
                  <Point X="-0.374345336914" Y="-3.113155273438" />
                  <Point X="-0.400600463867" Y="-3.132399169922" />
                  <Point X="-0.412474975586" Y="-3.142717529297" />
                  <Point X="-0.434358673096" Y="-3.165172607422" />
                  <Point X="-0.444367675781" Y="-3.177309326172" />
                  <Point X="-0.492794525146" Y="-3.247082763672" />
                  <Point X="-0.608095458984" Y="-3.413209716797" />
                  <Point X="-0.612470275879" Y="-3.420132324219" />
                  <Point X="-0.62597668457" Y="-3.445265136719" />
                  <Point X="-0.638777893066" Y="-3.476215820312" />
                  <Point X="-0.642753112793" Y="-3.487936523438" />
                  <Point X="-0.924851989746" Y="-4.540744628906" />
                  <Point X="-0.985425170898" Y="-4.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.361986764587" Y="4.770483333706" />
                  <Point X="-1.467624582619" Y="4.474228573244" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.337398986732" Y="4.678720371787" />
                  <Point X="-1.46506127537" Y="4.376564172212" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.137371443381" Y="4.19641920563" />
                  <Point X="-2.493797172964" Y="4.100915219227" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.412793092198" Y="4.781382496366" />
                  <Point X="0.374467569108" Y="4.771113203404" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.312811208877" Y="4.586957409868" />
                  <Point X="-1.49447991488" Y="4.270330234374" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.060325552067" Y="4.118712352849" />
                  <Point X="-2.756427938742" Y="3.93219228049" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.676698646931" Y="4.753744539496" />
                  <Point X="0.346076048727" Y="4.665154481308" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.288223431022" Y="4.495194447949" />
                  <Point X="-1.548749425221" Y="4.157437525765" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.938260383671" Y="4.053068379006" />
                  <Point X="-2.953627318957" Y="3.781001628674" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.910351012718" Y="4.718000265079" />
                  <Point X="0.317684520507" Y="4.55919575711" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.263635652376" Y="4.403431486242" />
                  <Point X="-3.036585096901" Y="3.66042192193" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.103431228318" Y="4.671384715785" />
                  <Point X="0.289292992286" Y="4.453237032913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.239047873507" Y="4.311668524595" />
                  <Point X="-2.987409460876" Y="3.575247256751" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.296511932964" Y="4.62476929753" />
                  <Point X="0.260901464066" Y="4.347278308715" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.208797411705" Y="4.221422874267" />
                  <Point X="-2.938233824852" Y="3.490072591572" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.482670004515" Y="4.576298965327" />
                  <Point X="0.229794855454" Y="4.240592080919" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.146207032512" Y="4.139842678686" />
                  <Point X="-2.889058188827" Y="3.404897926393" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.63862000788" Y="4.51973440565" />
                  <Point X="0.112733675803" Y="4.110874395228" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.016591889578" Y="4.08511328125" />
                  <Point X="-2.839882552803" Y="3.319723261214" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.794570584448" Y="4.463169999561" />
                  <Point X="-2.790706916779" Y="3.234548596035" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.665808728735" Y="3.000065772226" />
                  <Point X="-3.776628978264" Y="2.97037157586" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.937356500889" Y="4.403078133423" />
                  <Point X="-2.758656946426" Y="3.144785122569" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.54945790299" Y="2.932890644884" />
                  <Point X="-3.873297317488" Y="2.846118135292" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.071055623508" Y="4.340551468219" />
                  <Point X="-2.749196948559" Y="3.048968684219" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.433107101386" Y="2.865715511074" />
                  <Point X="-3.969965887448" Y="2.721864632899" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.204754868503" Y="4.278024835805" />
                  <Point X="-2.760727727826" Y="2.947527784088" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.316756472608" Y="2.798540330955" />
                  <Point X="-4.047585744974" Y="2.602715217619" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.326562310443" Y="4.212311804366" />
                  <Point X="-2.845492369039" Y="2.826463929789" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.182768922006" Y="2.736090949796" />
                  <Point X="-4.115635162342" Y="2.486130194051" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.44219469751" Y="4.14494417196" />
                  <Point X="-4.18368457971" Y="2.369545170483" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.557826937309" Y="4.077576500095" />
                  <Point X="-4.193943235639" Y="2.268445134772" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.667678901689" Y="4.008660008098" />
                  <Point X="-4.098943321383" Y="2.195549047939" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.768130369284" Y="3.93722466058" />
                  <Point X="-4.003943402134" Y="2.122652962444" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.783806889339" Y="3.84307393433" />
                  <Point X="-3.908943482884" Y="2.049756876949" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.716631788454" Y="3.726723183157" />
                  <Point X="-3.813943563635" Y="1.976860791454" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.649456687569" Y="3.610372431985" />
                  <Point X="-3.718943644385" Y="1.903964705959" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.582281586684" Y="3.494021680812" />
                  <Point X="-3.623943725136" Y="1.831068620464" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.515106485799" Y="3.377670929639" />
                  <Point X="-3.530687046855" Y="1.757705434959" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.447931384914" Y="3.261320178467" />
                  <Point X="-3.469860368501" Y="1.675652657164" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.380756284029" Y="3.144969427294" />
                  <Point X="-3.430926996112" Y="1.587733585715" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.501567209805" Y="1.300856405072" />
                  <Point X="-4.649001321801" Y="1.261351553826" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.31358117966" Y="3.028618675188" />
                  <Point X="-3.423649981646" Y="1.491332218725" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.255444014826" Y="1.268453679266" />
                  <Point X="-4.677739285668" Y="1.155300002476" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.246406043186" Y="2.912267914479" />
                  <Point X="-3.458490790923" Y="1.383645414877" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.009320977956" Y="1.236050911095" />
                  <Point X="-4.706477192432" Y="1.049248466428" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.179230906712" Y="2.795917153771" />
                  <Point X="-3.564575992838" Y="1.256868733556" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.759511406281" Y="1.204635946948" />
                  <Point X="-4.733378993682" Y="0.943688913369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.112055770238" Y="2.679566393062" />
                  <Point X="-4.74853344925" Y="0.841277052099" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.049827526863" Y="2.564541148365" />
                  <Point X="-4.763687904819" Y="0.738865190828" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.019334474894" Y="2.458019322576" />
                  <Point X="-4.778842360388" Y="0.636453329558" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.014754189348" Y="2.358440801624" />
                  <Point X="-4.681896326785" Y="0.564078703832" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.943351801444" Y="2.77685573717" />
                  <Point X="3.924615161209" Y="2.771835269551" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.030043549608" Y="2.264186336219" />
                  <Point X="-4.498370419112" Y="0.514903085444" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.002681003093" Y="2.694401711701" />
                  <Point X="3.606739091251" Y="2.588309396173" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.07287244096" Y="2.177311065931" />
                  <Point X="-4.314844537021" Y="0.465727460202" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.059539483343" Y="2.611285658428" />
                  <Point X="3.288862847811" Y="2.404783476311" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.135322534641" Y="2.095693280961" />
                  <Point X="-4.131318654931" Y="0.416551834959" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.110751968269" Y="2.526656765267" />
                  <Point X="2.970986604371" Y="2.22125755645" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.232601968356" Y="2.023407989526" />
                  <Point X="-3.94779277284" Y="0.367376209717" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.04043617655" Y="2.409464468522" />
                  <Point X="2.653110360931" Y="2.037731636588" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.420278390564" Y="1.975344498156" />
                  <Point X="-3.764266890749" Y="0.318200584474" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.843489003747" Y="2.258341395479" />
                  <Point X="-3.580741008658" Y="0.269024959232" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.646541830945" Y="2.107218322436" />
                  <Point X="-3.428526995027" Y="0.211459344122" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.449594531252" Y="1.956095215393" />
                  <Point X="-3.347024791052" Y="0.134946556719" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.252646934077" Y="1.804972028639" />
                  <Point X="-3.306598258808" Y="0.047427576248" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.103450493863" Y="1.666643725832" />
                  <Point X="-3.294909951165" Y="-0.047791788297" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.032514963415" Y="1.549285370594" />
                  <Point X="-3.322532730119" Y="-0.153544526749" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.004484714511" Y="1.443423450898" />
                  <Point X="-3.44839883121" Y="-0.28562148403" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.006154324033" Y="1.345519584282" />
                  <Point X="-4.774351649645" Y="-0.73926070807" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.026508527945" Y="1.252622239644" />
                  <Point X="-4.760804478621" Y="-0.833981991674" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.068723580349" Y="1.165582491705" />
                  <Point X="-4.747257165796" Y="-0.928703237282" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.127447203641" Y="1.082966202004" />
                  <Point X="-4.72852254589" Y="-1.022034548146" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.22683555748" Y="1.011245994013" />
                  <Point X="-4.705009916944" Y="-1.114085595347" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.396298718882" Y="0.958302274119" />
                  <Point X="-4.681497287998" Y="-1.206136642548" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.976754366654" Y="1.015483659042" />
                  <Point X="-4.570823034051" Y="-1.274832802719" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.698351517898" Y="1.11048379584" />
                  <Point X="-3.849226376508" Y="-1.179832798209" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.726982813396" Y="1.019804291208" />
                  <Point X="-3.189612861841" Y="-1.101441126776" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.749459874594" Y="0.927475764465" />
                  <Point X="-3.045367474694" Y="-1.161141928917" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.76527487164" Y="0.833362143013" />
                  <Point X="-2.976719746674" Y="-1.241099062771" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.779974292748" Y="0.738949603889" />
                  <Point X="-2.947947487613" Y="-1.33174079633" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.480138288345" Y="0.292308359077" />
                  <Point X="-2.959156777632" Y="-1.433095553677" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.391713973573" Y="0.170263898204" />
                  <Point X="-3.020871927711" Y="-1.54798331544" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.361438128914" Y="0.063800272938" />
                  <Point X="-3.207829828972" Y="-1.696429771241" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.349120259397" Y="-0.03785152739" />
                  <Point X="-3.404777566418" Y="-1.84755299558" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.362629837702" Y="-0.132582883932" />
                  <Point X="-3.601724731699" Y="-1.998676066607" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.387784133932" Y="-0.22419404771" />
                  <Point X="-3.798671799782" Y="-2.149799111591" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.442307246847" Y="-0.307935860775" />
                  <Point X="-3.995618867865" Y="-2.300922156574" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.515060102308" Y="-0.386793029046" />
                  <Point X="-4.178363352296" Y="-2.448239630738" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.626198553219" Y="-0.455364808015" />
                  <Point X="-4.127791890697" Y="-2.533040285581" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.789349675406" Y="-0.50999983372" />
                  <Point X="-4.077220441585" Y="-2.61784094377" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.972875671426" Y="-0.559175428435" />
                  <Point X="-4.026649074611" Y="-2.702641623969" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.156401667446" Y="-0.60835102315" />
                  <Point X="-2.451503268486" Y="-2.378933814395" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.890384849694" Y="-2.496531779653" />
                  <Point X="-3.96966518951" Y="-2.785724075113" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.339927663466" Y="-0.657526617866" />
                  <Point X="3.404693238461" Y="-0.908121926779" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.084655958775" Y="-0.993875657419" />
                  <Point X="-2.364124962213" Y="-2.453872104932" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.208261045057" Y="-2.680057686632" />
                  <Point X="-3.90748620565" Y="-2.86741450374" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.523453659486" Y="-0.706702212581" />
                  <Point X="3.658556215772" Y="-0.93845078416" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.963238734984" Y="-1.12476054162" />
                  <Point X="-2.319896908667" Y="-2.540372470841" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.526137240419" Y="-2.863583593611" />
                  <Point X="-3.845307221789" Y="-2.949104932368" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.706979527624" Y="-0.755877841562" />
                  <Point X="3.904679567796" Y="-0.970853467885" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.888211386139" Y="-1.243215296292" />
                  <Point X="-2.313902263668" Y="-2.637117447693" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.775983365947" Y="-0.835739555948" />
                  <Point X="4.150802710661" Y="-1.003256207655" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.870590379561" Y="-1.346288067914" />
                  <Point X="-2.354289006782" Y="-2.746290280034" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.759939036785" Y="-0.938389858129" />
                  <Point X="4.396925853526" Y="-1.035658947425" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.861311363661" Y="-1.44712560987" />
                  <Point X="-2.421464187338" Y="-2.862641052555" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.736033758297" Y="-1.043146495333" />
                  <Point X="4.643048996391" Y="-1.068061687195" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.867418349756" Y="-1.543840485016" />
                  <Point X="-2.488639367893" Y="-2.978991825075" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.904776014137" Y="-1.632181766153" />
                  <Point X="-2.555814548449" Y="-3.095342597595" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.95871488456" Y="-1.716080126522" />
                  <Point X="-2.622989709889" Y="-3.211693364993" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.015514455247" Y="-1.799211964565" />
                  <Point X="-2.690164869503" Y="-3.328044131902" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.102344374221" Y="-1.874297195036" />
                  <Point X="2.537013899638" Y="-2.025777039157" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.3571450326" Y="-2.073972756823" />
                  <Point X="-2.757340029117" Y="-3.444394898811" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.197344311495" Y="-1.947193275701" />
                  <Point X="2.760107517465" Y="-2.064350521563" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.128396557387" Y="-2.233616963165" />
                  <Point X="-2.82451518873" Y="-3.56074566572" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.292344248769" Y="-2.020089356367" />
                  <Point X="2.900931343467" Y="-2.124968128249" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.066608440355" Y="-2.348524276365" />
                  <Point X="-2.891690348344" Y="-3.677096432629" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.387344186043" Y="-2.092985437032" />
                  <Point X="3.017282090192" Y="-2.192143276764" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.012881416327" Y="-2.461271626204" />
                  <Point X="1.276107851403" Y="-2.65868950793" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.047223957195" Y="-2.720018762543" />
                  <Point X="0.010654910283" Y="-2.997766601562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.325574067018" Y="-3.087858884502" />
                  <Point X="-2.958865507958" Y="-3.793447199538" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.482344123317" Y="-2.165881517697" />
                  <Point X="3.133632836918" Y="-2.25931842528" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.000662796244" Y="-2.562896832727" />
                  <Point X="1.556793673578" Y="-2.68183120569" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.871976490319" Y="-2.865327416907" />
                  <Point X="0.201720411606" Y="-3.04492199192" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.479096238973" Y="-3.227346263637" />
                  <Point X="-2.917134249837" Y="-3.880616579764" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.577344060591" Y="-2.238777598363" />
                  <Point X="3.249983581402" Y="-2.326493574395" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.016088756917" Y="-2.657114696161" />
                  <Point X="1.665183933554" Y="-2.751139360201" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.789292259256" Y="-2.985833826986" />
                  <Point X="0.363364510127" Y="-3.0999608234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.562952084072" Y="-3.348166606751" />
                  <Point X="-2.822375955847" Y="-3.953577408552" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.672343997864" Y="-2.311673679028" />
                  <Point X="3.366334268289" Y="-2.393668738944" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.033031074733" Y="-2.750926252923" />
                  <Point X="1.773160586446" Y="-2.820558340396" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.764259903762" Y="-3.090892463565" />
                  <Point X="0.453977124222" Y="-3.174032483768" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.63441454897" Y="-3.465666153648" />
                  <Point X="-1.822459602212" Y="-3.784001866236" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.198712746133" Y="-3.8848185923" />
                  <Point X="-2.724223802168" Y="-4.025628855378" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.767343935138" Y="-2.384569759694" />
                  <Point X="3.482684955175" Y="-2.460843903493" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.056035837505" Y="-2.843113382455" />
                  <Point X="1.850594922693" Y="-2.898161109671" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.741560846329" Y="-3.195325894812" />
                  <Point X="0.511895385707" Y="-3.256864569515" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.665360739899" Y="-3.572309397655" />
                  <Point X="-1.56739682063" Y="-3.814009237031" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.408518951563" Y="-4.03938723275" />
                  <Point X="-2.613358480422" Y="-4.094273819086" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.862343872412" Y="-2.457465840359" />
                  <Point X="3.599035642062" Y="-2.528019068042" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.101679539842" Y="-2.929234426413" />
                  <Point X="1.913192253452" Y="-2.979739442585" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.724728984392" Y="-3.298187215764" />
                  <Point X="0.569452617788" Y="-3.339793392799" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.693752274323" Y="-3.678268123515" />
                  <Point X="-1.461469871019" Y="-3.883977433565" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.957343782448" Y="-2.530361928323" />
                  <Point X="3.715386328948" Y="-2.595194232591" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.150855172047" Y="-3.014409092616" />
                  <Point X="1.975789584211" Y="-3.061317775499" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.733286350669" Y="-3.39424551352" />
                  <Point X="0.626251509233" Y="-3.422925412844" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.722143808747" Y="-3.784226849375" />
                  <Point X="-1.37556836845" Y="-3.959311432462" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.05234367261" Y="-2.603258021612" />
                  <Point X="3.831737015835" Y="-2.66236939714" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.200030804253" Y="-3.099583758818" />
                  <Point X="2.03838691497" Y="-3.142896108413" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.745793337998" Y="-3.489245513504" />
                  <Point X="0.661469840567" Y="-3.511839926544" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.750535343171" Y="-3.890185575235" />
                  <Point X="-1.289666695556" Y="-4.034645385721" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.040270523117" Y="-2.704844249408" />
                  <Point X="3.948087702722" Y="-2.729544561688" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.249206436458" Y="-3.18475842502" />
                  <Point X="2.100984245728" Y="-3.224474441327" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.758300325327" Y="-3.584245513488" />
                  <Point X="0.68605765559" Y="-3.603602878503" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.778926877595" Y="-3.996144301095" />
                  <Point X="-1.218154899632" Y="-4.113835094892" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.298382068664" Y="-3.269933091222" />
                  <Point X="2.163581562741" Y="-3.306052777924" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.770807312656" Y="-3.679245513473" />
                  <Point X="0.710645470614" Y="-3.695365830463" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.807318412019" Y="-4.102103026954" />
                  <Point X="-1.182048914796" Y="-4.202511762553" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.347557700869" Y="-3.355107757425" />
                  <Point X="2.226178861135" Y="-3.38763111951" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.783314299985" Y="-3.774245513457" />
                  <Point X="0.735233285637" Y="-3.787128782423" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.835709946443" Y="-4.208061752814" />
                  <Point X="-1.163475552316" Y="-4.295886282214" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.396733333075" Y="-3.440282423627" />
                  <Point X="2.288776159529" Y="-3.469209461095" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.795821287314" Y="-3.869245513441" />
                  <Point X="0.759821100661" Y="-3.878891734383" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.864101480868" Y="-4.314020478674" />
                  <Point X="-1.144902189836" Y="-4.389260801876" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.44590896528" Y="-3.525457089829" />
                  <Point X="2.351373457924" Y="-3.550787802681" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.808328274644" Y="-3.964245513426" />
                  <Point X="0.78440890053" Y="-3.970654690403" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.892493015292" Y="-4.419979204534" />
                  <Point X="-1.126328771088" Y="-4.482635306461" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.495084597485" Y="-3.610631756031" />
                  <Point X="2.413970756318" Y="-3.632366144267" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.820835261973" Y="-4.05924551341" />
                  <Point X="0.808996681251" Y="-4.062417651554" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.920884549716" Y="-4.525937930393" />
                  <Point X="-1.121275257317" Y="-4.579632458666" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.544260229691" Y="-3.695806422233" />
                  <Point X="2.476568054712" Y="-3.713944485853" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.949276123942" Y="-4.631896666918" />
                  <Point X="-1.134696919166" Y="-4.681580019258" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.593435861896" Y="-3.780981088436" />
                  <Point X="2.539165353107" Y="-3.795522827439" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.977667704635" Y="-4.737855405176" />
                  <Point X="-1.043584511399" Y="-4.755517760316" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.642611419533" Y="-3.866155774619" />
                  <Point X="2.601762651501" Y="-3.877101169024" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.691786916409" Y="-3.951330477082" />
                  <Point X="2.664359949895" Y="-3.95867951061" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="0.001625976562" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978485351562" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.590021179199" Y="-3.979293701172" />
                  <Point X="0.471539031982" Y="-3.537112304688" />
                  <Point X="0.464318664551" Y="-3.521544189453" />
                  <Point X="0.415892150879" Y="-3.451770507813" />
                  <Point X="0.300591033936" Y="-3.285643798828" />
                  <Point X="0.274335754395" Y="-3.266399902344" />
                  <Point X="0.199398391724" Y="-3.243142089844" />
                  <Point X="0.02097677803" Y="-3.187766601562" />
                  <Point X="-0.008664756775" Y="-3.187766601562" />
                  <Point X="-0.083602264404" Y="-3.211024414062" />
                  <Point X="-0.262023895264" Y="-3.266399902344" />
                  <Point X="-0.288279144287" Y="-3.285643798828" />
                  <Point X="-0.336705841064" Y="-3.355417236328" />
                  <Point X="-0.45200680542" Y="-3.521544189453" />
                  <Point X="-0.459227142334" Y="-3.537112304688" />
                  <Point X="-0.74132623291" Y="-4.589920410156" />
                  <Point X="-0.84774395752" Y="-4.987077148438" />
                  <Point X="-0.89063873291" Y="-4.978750976562" />
                  <Point X="-1.100246826172" Y="-4.938065429688" />
                  <Point X="-1.186247192383" Y="-4.915937988281" />
                  <Point X="-1.35158972168" Y="-4.873396972656" />
                  <Point X="-1.323412719727" Y="-4.659372558594" />
                  <Point X="-1.309150146484" Y="-4.551038574219" />
                  <Point X="-1.309683227539" Y="-4.5347578125" />
                  <Point X="-1.32758581543" Y="-4.444755859375" />
                  <Point X="-1.370210693359" Y="-4.230466308594" />
                  <Point X="-1.386282714844" Y="-4.20262890625" />
                  <Point X="-1.455275756836" Y="-4.142123535156" />
                  <Point X="-1.619543457031" Y="-3.998063964844" />
                  <Point X="-1.649240722656" Y="-3.985762939453" />
                  <Point X="-1.740809692383" Y="-3.979761230469" />
                  <Point X="-1.958829833984" Y="-3.965471435547" />
                  <Point X="-1.989878540039" Y="-3.973791015625" />
                  <Point X="-2.066178710937" Y="-4.024773193359" />
                  <Point X="-2.247844970703" Y="-4.146159179688" />
                  <Point X="-2.259734130859" Y="-4.157294433594" />
                  <Point X="-2.418110839844" Y="-4.363694824219" />
                  <Point X="-2.457095214844" Y="-4.414500488281" />
                  <Point X="-2.548591552734" Y="-4.357848144531" />
                  <Point X="-2.855831054688" Y="-4.16761328125" />
                  <Point X="-2.974921142578" Y="-4.07591796875" />
                  <Point X="-3.228581054688" Y="-3.880608154297" />
                  <Point X="-2.726219482422" Y="-3.010492431641" />
                  <Point X="-2.506033691406" Y="-2.629119628906" />
                  <Point X="-2.499762695312" Y="-2.597593261719" />
                  <Point X="-2.513979736328" Y="-2.568764404297" />
                  <Point X="-2.531328369141" Y="-2.551415771484" />
                  <Point X="-2.560157226562" Y="-2.537198730469" />
                  <Point X="-2.59168359375" Y="-2.543469726562" />
                  <Point X="-3.499709960938" Y="-3.067718994141" />
                  <Point X="-3.842959472656" Y="-3.265894042969" />
                  <Point X="-3.918965576172" Y="-3.166037597656" />
                  <Point X="-4.161703613281" Y="-2.847129394531" />
                  <Point X="-4.247079101562" Y="-2.703967529297" />
                  <Point X="-4.431020019531" Y="-2.395527099609" />
                  <Point X="-3.549040527344" Y="-1.718760253906" />
                  <Point X="-3.163786865234" Y="-1.423144897461" />
                  <Point X="-3.152535644531" Y="-1.411081542969" />
                  <Point X="-3.143543212891" Y="-1.387215087891" />
                  <Point X="-3.138117431641" Y="-1.366266235352" />
                  <Point X="-3.136651855469" Y="-1.350051269531" />
                  <Point X="-3.148656738281" Y="-1.321068725586" />
                  <Point X="-3.168991943359" Y="-1.306029296875" />
                  <Point X="-3.187641601562" Y="-1.295052734375" />
                  <Point X="-3.219529296875" Y="-1.288571044922" />
                  <Point X="-4.365826660156" Y="-1.439484008789" />
                  <Point X="-4.803283203125" Y="-1.497076293945" />
                  <Point X="-4.832456542969" Y="-1.382864135742" />
                  <Point X="-4.927393066406" Y="-1.011191650391" />
                  <Point X="-4.949980957031" Y="-0.853257385254" />
                  <Point X="-4.998396484375" Y="-0.514742126465" />
                  <Point X="-3.996325683594" Y="-0.246238021851" />
                  <Point X="-3.557463867188" Y="-0.128645431519" />
                  <Point X="-3.533687011719" Y="-0.115727714539" />
                  <Point X="-3.514142822266" Y="-0.102162895203" />
                  <Point X="-3.50232421875" Y="-0.090645141602" />
                  <Point X="-3.492162597656" Y="-0.067091262817" />
                  <Point X="-3.485647949219" Y="-0.046100585938" />
                  <Point X="-3.483400878906" Y="-0.031279783249" />
                  <Point X="-3.488384033203" Y="-0.007642847061" />
                  <Point X="-3.494898925781" Y="0.013347985268" />
                  <Point X="-3.502324462891" Y="0.028085285187" />
                  <Point X="-3.5223515625" Y="0.045300014496" />
                  <Point X="-3.541895751953" Y="0.058864833832" />
                  <Point X="-3.557463867188" Y="0.066085334778" />
                  <Point X="-4.602368164062" Y="0.346066650391" />
                  <Point X="-4.998186523438" Y="0.452125823975" />
                  <Point X="-4.978830566406" Y="0.58293347168" />
                  <Point X="-4.917645019531" Y="0.996418151855" />
                  <Point X="-4.872174804688" Y="1.164217041016" />
                  <Point X="-4.773516113281" Y="1.528298583984" />
                  <Point X="-4.067853271484" Y="1.435396362305" />
                  <Point X="-3.753266357422" Y="1.393980102539" />
                  <Point X="-3.731704833984" Y="1.395866577148" />
                  <Point X="-3.713536621094" Y="1.401595092773" />
                  <Point X="-3.670278808594" Y="1.415234130859" />
                  <Point X="-3.651534423828" Y="1.426056152344" />
                  <Point X="-3.639120117188" Y="1.443785766602" />
                  <Point X="-3.631830078125" Y="1.461385742188" />
                  <Point X="-3.61447265625" Y="1.503290039063" />
                  <Point X="-3.610714111328" Y="1.524605102539" />
                  <Point X="-3.616315917969" Y="1.54551159668" />
                  <Point X="-3.625112304688" Y="1.562409057617" />
                  <Point X="-3.646055664062" Y="1.602641235352" />
                  <Point X="-3.659968261719" Y="1.619221679688" />
                  <Point X="-4.259328125" Y="2.079126464844" />
                  <Point X="-4.476105957031" Y="2.245466308594" />
                  <Point X="-4.397758789063" Y="2.379693603516" />
                  <Point X="-4.160014648438" Y="2.787006591797" />
                  <Point X="-4.039566894531" Y="2.941824951172" />
                  <Point X="-3.774671142578" Y="3.282310791016" />
                  <Point X="-3.352367431641" Y="3.038493652344" />
                  <Point X="-3.159156982422" Y="2.926943359375" />
                  <Point X="-3.138514648438" Y="2.920434814453" />
                  <Point X="-3.113211181641" Y="2.918220947266" />
                  <Point X="-3.052965332031" Y="2.912950195312" />
                  <Point X="-3.031506591797" Y="2.915775390625" />
                  <Point X="-3.013252441406" Y="2.927404785156" />
                  <Point X="-2.995291748047" Y="2.945365234375" />
                  <Point X="-2.952528808594" Y="2.988128173828" />
                  <Point X="-2.940899414062" Y="3.006382324219" />
                  <Point X="-2.93807421875" Y="3.027841064453" />
                  <Point X="-2.940288085938" Y="3.05314453125" />
                  <Point X="-2.945558837891" Y="3.113390625" />
                  <Point X="-2.952067382812" Y="3.134032958984" />
                  <Point X="-3.217661865234" Y="3.594055908203" />
                  <Point X="-3.307278564453" Y="3.749276611328" />
                  <Point X="-3.166951171875" Y="3.856864501953" />
                  <Point X="-2.752876220703" Y="4.174331542969" />
                  <Point X="-2.563175048828" Y="4.279725097656" />
                  <Point X="-2.141548583984" Y="4.513971679688" />
                  <Point X="-2.026974487305" Y="4.364655761719" />
                  <Point X="-1.967826660156" Y="4.287573242188" />
                  <Point X="-1.951246826172" Y="4.273660644531" />
                  <Point X="-1.923084228516" Y="4.259" />
                  <Point X="-1.856030761719" Y="4.22409375" />
                  <Point X="-1.835124145508" Y="4.2184921875" />
                  <Point X="-1.813808837891" Y="4.222250488281" />
                  <Point X="-1.784475585938" Y="4.234400878906" />
                  <Point X="-1.714634887695" Y="4.263330078125" />
                  <Point X="-1.696905273438" Y="4.275744140625" />
                  <Point X="-1.686083496094" Y="4.29448828125" />
                  <Point X="-1.676536010742" Y="4.324769042969" />
                  <Point X="-1.653804077148" Y="4.396864746094" />
                  <Point X="-1.651917602539" Y="4.418426757812" />
                  <Point X="-1.682112792969" Y="4.647781738281" />
                  <Point X="-1.689137573242" Y="4.701141113281" />
                  <Point X="-1.504139770508" Y="4.7530078125" />
                  <Point X="-0.968094177246" Y="4.903296386719" />
                  <Point X="-0.738120544434" Y="4.930211425781" />
                  <Point X="-0.224199615479" Y="4.990358398438" />
                  <Point X="-0.099297317505" Y="4.524216308594" />
                  <Point X="-0.042140380859" Y="4.310903320312" />
                  <Point X="-0.024282115936" Y="4.284176757813" />
                  <Point X="0.006155934334" Y="4.273844238281" />
                  <Point X="0.036594036102" Y="4.284176757813" />
                  <Point X="0.054452251434" Y="4.310903320312" />
                  <Point X="0.190539749146" Y="4.8187890625" />
                  <Point X="0.236648391724" Y="4.990868652344" />
                  <Point X="0.392159423828" Y="4.974582519531" />
                  <Point X="0.860205810547" Y="4.925565429688" />
                  <Point X="1.050470581055" Y="4.879629882812" />
                  <Point X="1.508455932617" Y="4.769058105469" />
                  <Point X="1.632228027344" Y="4.724165039062" />
                  <Point X="1.931034301758" Y="4.615785644531" />
                  <Point X="2.050785888672" Y="4.559781738281" />
                  <Point X="2.338699462891" Y="4.425134277344" />
                  <Point X="2.454389160156" Y="4.357733398438" />
                  <Point X="2.732523681641" Y="4.19569140625" />
                  <Point X="2.841630615234" Y="4.118100585938" />
                  <Point X="3.068740234375" Y="3.956592773438" />
                  <Point X="2.484701904297" Y="2.945008300781" />
                  <Point X="2.229853515625" Y="2.503597900391" />
                  <Point X="2.224852050781" Y="2.491514648438" />
                  <Point X="2.218501708984" Y="2.467767822266" />
                  <Point X="2.203382324219" Y="2.411228271484" />
                  <Point X="2.202044677734" Y="2.392325927734" />
                  <Point X="2.204520751953" Y="2.371791748047" />
                  <Point X="2.210416015625" Y="2.322901123047" />
                  <Point X="2.218682128906" Y="2.300812255859" />
                  <Point X="2.231387939453" Y="2.282087158203" />
                  <Point X="2.261639892578" Y="2.237503662109" />
                  <Point X="2.274939941406" Y="2.224203857422" />
                  <Point X="2.293665039062" Y="2.211498046875" />
                  <Point X="2.338248535156" Y="2.181246337891" />
                  <Point X="2.360336914062" Y="2.172980224609" />
                  <Point X="2.38087109375" Y="2.170504150391" />
                  <Point X="2.42976171875" Y="2.164608642578" />
                  <Point X="2.448664306641" Y="2.165946289062" />
                  <Point X="2.472411132813" Y="2.172296630859" />
                  <Point X="2.528950683594" Y="2.187416015625" />
                  <Point X="2.541033935547" Y="2.192417480469" />
                  <Point X="3.592006591797" Y="2.799196777344" />
                  <Point X="3.994248046875" Y="3.031430908203" />
                  <Point X="4.037611816406" Y="2.971165039062" />
                  <Point X="4.202591796875" Y="2.741880126953" />
                  <Point X="4.263417480469" Y="2.641365234375" />
                  <Point X="4.387512695312" Y="2.436296142578" />
                  <Point X="3.624147949219" Y="1.850545166016" />
                  <Point X="3.288616210938" Y="1.593082641602" />
                  <Point X="3.279371337891" Y="1.583833374023" />
                  <Point X="3.262280761719" Y="1.561537353516" />
                  <Point X="3.221589111328" Y="1.508451904297" />
                  <Point X="3.213119628906" Y="1.49150012207" />
                  <Point X="3.206753417969" Y="1.468735839844" />
                  <Point X="3.191595703125" Y="1.414535644531" />
                  <Point X="3.190779541016" Y="1.390965332031" />
                  <Point X="3.196005615234" Y="1.365637207031" />
                  <Point X="3.208448486328" Y="1.305332519531" />
                  <Point X="3.215646484375" Y="1.287954956055" />
                  <Point X="3.229860595703" Y="1.266349853516" />
                  <Point X="3.263704101562" Y="1.214909667969" />
                  <Point X="3.280947998047" Y="1.198819946289" />
                  <Point X="3.301546386719" Y="1.187224853516" />
                  <Point X="3.350590087891" Y="1.159617675781" />
                  <Point X="3.368565429688" Y="1.153619628906" />
                  <Point X="3.396415771484" Y="1.149938964844" />
                  <Point X="3.462726074219" Y="1.141175048828" />
                  <Point X="3.475803710938" Y="1.14117175293" />
                  <Point X="4.474157226562" Y="1.272607421875" />
                  <Point X="4.848975097656" Y="1.32195324707" />
                  <Point X="4.868330566406" Y="1.242447631836" />
                  <Point X="4.939188476562" Y="0.951385986328" />
                  <Point X="4.958356445312" Y="0.828272888184" />
                  <Point X="4.997858398438" Y="0.574556152344" />
                  <Point X="4.125694335938" Y="0.340860473633" />
                  <Point X="3.741167724609" Y="0.237826919556" />
                  <Point X="3.729086914062" Y="0.232819412231" />
                  <Point X="3.701724853516" Y="0.217003570557" />
                  <Point X="3.636576904297" Y="0.179346878052" />
                  <Point X="3.622264892578" Y="0.166926574707" />
                  <Point X="3.60584765625" Y="0.146007095337" />
                  <Point X="3.566759033203" Y="0.096198890686" />
                  <Point X="3.556985351562" Y="0.074735275269" />
                  <Point X="3.551512939453" Y="0.046160232544" />
                  <Point X="3.538483154297" Y="-0.021875303268" />
                  <Point X="3.538482910156" Y="-0.040684635162" />
                  <Point X="3.543955566406" Y="-0.069259681702" />
                  <Point X="3.556985107422" Y="-0.137295227051" />
                  <Point X="3.566759033203" Y="-0.158758987427" />
                  <Point X="3.583176269531" Y="-0.179678466797" />
                  <Point X="3.622264892578" Y="-0.229486679077" />
                  <Point X="3.636576904297" Y="-0.241906967163" />
                  <Point X="3.663939208984" Y="-0.257722808838" />
                  <Point X="3.729086914062" Y="-0.295379486084" />
                  <Point X="3.741167724609" Y="-0.300387023926" />
                  <Point X="4.656703613281" Y="-0.545703918457" />
                  <Point X="4.998067871094" Y="-0.637172363281" />
                  <Point X="4.987995605469" Y="-0.70398046875" />
                  <Point X="4.948431640625" Y="-0.966399169922" />
                  <Point X="4.923874511719" Y="-1.074013061523" />
                  <Point X="4.874545898438" Y="-1.290178344727" />
                  <Point X="3.857925048828" Y="-1.156337646484" />
                  <Point X="3.411982177734" Y="-1.097628295898" />
                  <Point X="3.394836181641" Y="-1.098341186523" />
                  <Point X="3.341133789062" Y="-1.110013671875" />
                  <Point X="3.213271972656" Y="-1.137804931641" />
                  <Point X="3.1854453125" Y="-1.154697265625" />
                  <Point X="3.152985595703" Y="-1.193736206055" />
                  <Point X="3.075701171875" Y="-1.286685424805" />
                  <Point X="3.064357910156" Y="-1.314070556641" />
                  <Point X="3.059705566406" Y="-1.364627685547" />
                  <Point X="3.04862890625" Y="-1.485000854492" />
                  <Point X="3.056360351562" Y="-1.516621826172" />
                  <Point X="3.086080078125" Y="-1.562848754883" />
                  <Point X="3.156840576172" Y="-1.672912353516" />
                  <Point X="3.168460693359" Y="-1.685540649414" />
                  <Point X="4.018083496094" Y="-2.337479248047" />
                  <Point X="4.33907421875" Y="-2.583784179688" />
                  <Point X="4.315659179687" Y="-2.621673339844" />
                  <Point X="4.204133300781" Y="-2.802139404297" />
                  <Point X="4.153345214844" Y="-2.874302246094" />
                  <Point X="4.056688476562" Y="-3.011638427734" />
                  <Point X="3.150626953125" Y="-2.488523193359" />
                  <Point X="2.753454589844" Y="-2.259215576172" />
                  <Point X="2.737340820313" Y="-2.253312744141" />
                  <Point X="2.673426513672" Y="-2.241770019531" />
                  <Point X="2.521250488281" Y="-2.214287109375" />
                  <Point X="2.489077636719" Y="-2.219244873047" />
                  <Point X="2.43598046875" Y="-2.247189453125" />
                  <Point X="2.309559570312" Y="-2.313723876953" />
                  <Point X="2.288599853516" Y="-2.33468359375" />
                  <Point X="2.260655273438" Y="-2.387780761719" />
                  <Point X="2.194120849609" Y="-2.514201660156" />
                  <Point X="2.189163085938" Y="-2.546374511719" />
                  <Point X="2.200705810547" Y="-2.610288818359" />
                  <Point X="2.228188720703" Y="-2.76246484375" />
                  <Point X="2.234091552734" Y="-2.778578613281" />
                  <Point X="2.780059570312" Y="-3.72422265625" />
                  <Point X="2.986673583984" Y="-4.082088623047" />
                  <Point X="2.967634521484" Y="-4.095687744141" />
                  <Point X="2.835296630859" Y="-4.190213378906" />
                  <Point X="2.778517089844" Y="-4.226966308594" />
                  <Point X="2.679775878906" Y="-4.290879394531" />
                  <Point X="1.986256591797" Y="-3.387067626953" />
                  <Point X="1.683177490234" Y="-2.992087402344" />
                  <Point X="1.670549316406" Y="-2.980467529297" />
                  <Point X="1.607512451172" Y="-2.939940673828" />
                  <Point X="1.45742590332" Y="-2.843448730469" />
                  <Point X="1.4258046875" Y="-2.835717041016" />
                  <Point X="1.35686328125" Y="-2.842061035156" />
                  <Point X="1.192717773438" Y="-2.857166015625" />
                  <Point X="1.165332885742" Y="-2.868509277344" />
                  <Point X="1.112098144531" Y="-2.912772216797" />
                  <Point X="0.985349060059" Y="-3.018159667969" />
                  <Point X="0.968456726074" Y="-3.045986328125" />
                  <Point X="0.952539855957" Y="-3.119216796875" />
                  <Point X="0.91464251709" Y="-3.29357421875" />
                  <Point X="0.91392956543" Y="-3.310720214844" />
                  <Point X="1.068652709961" Y="-4.485958984375" />
                  <Point X="1.127642211914" Y="-4.934028808594" />
                  <Point X="1.11954675293" Y="-4.935803222656" />
                  <Point X="0.99434552002" Y="-4.963247070312" />
                  <Point X="0.941888549805" Y="-4.972776855469" />
                  <Point X="0.860200378418" Y="-4.987616699219" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#146" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.057157025171" Y="4.568437406406" Z="0.75" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.75" />
                  <Point X="-0.750088547893" Y="5.011152576731" Z="0.75" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.75" />
                  <Point X="-1.523793560681" Y="4.832431607231" Z="0.75" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.75" />
                  <Point X="-1.73784097788" Y="4.672535231898" Z="0.75" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.75" />
                  <Point X="-1.730377730839" Y="4.37108455871" Z="0.75" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.75" />
                  <Point X="-1.809765829264" Y="4.311874898676" Z="0.75" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.75" />
                  <Point X="-1.906152589708" Y="4.334630690897" Z="0.75" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.75" />
                  <Point X="-1.993462792253" Y="4.426373997216" Z="0.75" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.75" />
                  <Point X="-2.593613933646" Y="4.354712856332" Z="0.75" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.75" />
                  <Point X="-3.203529206774" Y="3.927824396627" Z="0.75" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.75" />
                  <Point X="-3.267119100831" Y="3.600335902768" Z="0.75" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.75" />
                  <Point X="-2.996253796399" Y="3.08006696677" Z="0.75" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.75" />
                  <Point X="-3.036803013417" Y="3.01200055447" Z="0.75" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.75" />
                  <Point X="-3.115009498836" Y="2.999310902774" Z="0.75" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.75" />
                  <Point X="-3.333523445529" Y="3.113074847915" Z="0.75" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.75" />
                  <Point X="-4.085186200069" Y="3.003807424264" Z="0.75" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.75" />
                  <Point X="-4.447096751145" Y="2.436178715395" Z="0.75" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.75" />
                  <Point X="-4.295921950127" Y="2.070738912664" Z="0.75" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.75" />
                  <Point X="-3.675618757288" Y="1.57060196404" Z="0.75" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.75" />
                  <Point X="-3.684179786388" Y="1.511800021463" Z="0.75" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.75" />
                  <Point X="-3.734727710654" Y="1.480560991" Z="0.75" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.75" />
                  <Point X="-4.067482941519" Y="1.516248694585" Z="0.75" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.75" />
                  <Point X="-4.926590731134" Y="1.20857446535" Z="0.75" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.75" />
                  <Point X="-5.034447788596" Y="0.621532624939" Z="0.75" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.75" />
                  <Point X="-4.621465187731" Y="0.329050340468" Z="0.75" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.75" />
                  <Point X="-3.557016482916" Y="0.035504204533" Z="0.75" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.75" />
                  <Point X="-3.542292996558" Y="0.008816168685" Z="0.75" />
                  <Point X="-3.539556741714" Y="0" Z="0.75" />
                  <Point X="-3.546071604577" Y="-0.020990782376" Z="0.75" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.75" />
                  <Point X="-3.568352112208" Y="-0.043371778601" Z="0.75" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.75" />
                  <Point X="-4.015422601042" Y="-0.166661716288" Z="0.75" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.75" />
                  <Point X="-5.005633232979" Y="-0.829056445876" Z="0.75" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.75" />
                  <Point X="-4.887057345651" Y="-1.363958390155" Z="0.75" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.75" />
                  <Point X="-4.365456245006" Y="-1.45777618905" Z="0.75" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.75" />
                  <Point X="-3.200509207246" Y="-1.317839698402" Z="0.75" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.75" />
                  <Point X="-3.198102000153" Y="-1.343398625112" Z="0.75" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.75" />
                  <Point X="-3.585634370471" Y="-1.647812617964" Z="0.75" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.75" />
                  <Point X="-4.296178538336" Y="-2.698297064641" Z="0.75" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.75" />
                  <Point X="-3.964907323479" Y="-3.16504085023" Z="0.75" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.75" />
                  <Point X="-3.480866013566" Y="-3.079740316224" Z="0.75" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.75" />
                  <Point X="-2.560622241776" Y="-2.567708227356" Z="0.75" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.75" />
                  <Point X="-2.775676696944" Y="-2.954212368926" Z="0.75" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.75" />
                  <Point X="-3.011580985976" Y="-4.084254973649" Z="0.75" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.75" />
                  <Point X="-2.581069056139" Y="-4.369078949383" Z="0.75" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.75" />
                  <Point X="-2.384599160376" Y="-4.362852880104" Z="0.75" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.75" />
                  <Point X="-2.044556126652" Y="-4.035066559667" Z="0.75" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.75" />
                  <Point X="-1.750235763459" Y="-3.998374184343" Z="0.75" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.75" />
                  <Point X="-1.494398851762" Y="-4.148435298168" Z="0.75" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.75" />
                  <Point X="-1.382781075999" Y="-4.423230165082" Z="0.75" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.75" />
                  <Point X="-1.37914098686" Y="-4.621566259706" Z="0.75" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.75" />
                  <Point X="-1.204861857182" Y="-4.933080869803" Z="0.75" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.75" />
                  <Point X="-0.906322010932" Y="-4.996554910603" Z="0.75" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.75" />
                  <Point X="-0.699185907906" Y="-4.571581432015" Z="0.75" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.75" />
                  <Point X="-0.301785737454" Y="-3.352646258629" Z="0.75" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.75" />
                  <Point X="-0.074937432443" Y="-3.22749718202" Z="0.75" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.75" />
                  <Point X="0.178421646918" Y="-3.259614863269" Z="0.75" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.75" />
                  <Point X="0.368660121978" Y="-3.448999497139" Z="0.75" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.75" />
                  <Point X="0.535568952495" Y="-3.960954597801" Z="0.75" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.75" />
                  <Point X="0.944669373056" Y="-4.990691451087" Z="0.75" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.75" />
                  <Point X="1.124096479223" Y="-4.953363328145" Z="0.75" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.75" />
                  <Point X="1.112068949563" Y="-4.448152677499" Z="0.75" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.75" />
                  <Point X="0.995243079468" Y="-3.09855640717" Z="0.75" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.75" />
                  <Point X="1.137908954812" Y="-2.91993842732" Z="0.75" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.75" />
                  <Point X="1.355289097436" Y="-2.86057063576" Z="0.75" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.75" />
                  <Point X="1.574317184604" Y="-2.950718488005" Z="0.75" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.75" />
                  <Point X="1.940433011949" Y="-3.386225581639" Z="0.75" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.75" />
                  <Point X="2.799530223853" Y="-4.237660165037" Z="0.75" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.75" />
                  <Point X="2.990540318512" Y="-4.105094675021" Z="0.75" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.75" />
                  <Point X="2.817204970171" Y="-3.667942669256" Z="0.75" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.75" />
                  <Point X="2.243753987322" Y="-2.570122460065" Z="0.75" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.75" />
                  <Point X="2.298745589703" Y="-2.379787235635" Z="0.75" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.75" />
                  <Point X="2.453111142738" Y="-2.260155720264" Z="0.75" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.75" />
                  <Point X="2.658384343448" Y="-2.259694022169" Z="0.75" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.75" />
                  <Point X="3.11947092785" Y="-2.500544530103" Z="0.75" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.75" />
                  <Point X="4.188077680673" Y="-2.871799536062" Z="0.75" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.75" />
                  <Point X="4.352036392246" Y="-2.616678512558" Z="0.75" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.75" />
                  <Point X="4.042365429188" Y="-2.266531470456" Z="0.75" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.75" />
                  <Point X="3.121982132346" Y="-1.504529422708" Z="0.75" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.75" />
                  <Point X="3.103339217293" Y="-1.337929232723" Z="0.75" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.75" />
                  <Point X="3.185275910766" Y="-1.194423026522" Z="0.75" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.75" />
                  <Point X="3.345597561955" Y="-1.127592841103" Z="0.75" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.75" />
                  <Point X="3.845242759665" Y="-1.174629916552" Z="0.75" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.75" />
                  <Point X="4.966465567826" Y="-1.053857090953" Z="0.75" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.75" />
                  <Point X="5.031281102016" Y="-0.680154485062" Z="0.75" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.75" />
                  <Point X="4.663488382447" Y="-0.466127672458" Z="0.75" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.75" />
                  <Point X="3.682804910538" Y="-0.183154047019" Z="0.75" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.75" />
                  <Point X="3.616354041217" Y="-0.117530051707" Z="0.75" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.75" />
                  <Point X="3.586907165884" Y="-0.028575037378" Z="0.75" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.75" />
                  <Point X="3.59446428454" Y="0.068035493849" Z="0.75" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.75" />
                  <Point X="3.639025397183" Y="0.146418686868" Z="0.75" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.75" />
                  <Point X="3.720590503815" Y="0.204994797223" Z="0.75" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.75" />
                  <Point X="4.132479434859" Y="0.323844260311" Z="0.75" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.75" />
                  <Point X="5.001605156735" Y="0.867244898828" Z="0.75" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.75" />
                  <Point X="4.910755642967" Y="1.285554674672" Z="0.75" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.75" />
                  <Point X="4.461475037049" Y="1.35345987152" Z="0.75" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.75" />
                  <Point X="3.396811084201" Y="1.230787801038" Z="0.75" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.75" />
                  <Point X="3.319916975807" Y="1.26207590252" Z="0.75" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.75" />
                  <Point X="3.265475248129" Y="1.325111365312" Z="0.75" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.75" />
                  <Point X="3.238818081276" Y="1.407021112334" Z="0.75" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.75" />
                  <Point X="3.248749726018" Y="1.486549482523" Z="0.75" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.75" />
                  <Point X="3.295807602382" Y="1.562399122881" Z="0.75" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.75" />
                  <Point X="3.648430051777" Y="1.842157768558" Z="0.75" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.75" />
                  <Point X="4.300038905153" Y="2.698531471609" Z="0.75" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.75" />
                  <Point X="4.072041372313" Y="3.031647922369" Z="0.75" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.75" />
                  <Point X="3.560850620557" Y="2.873778104732" Z="0.75" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.75" />
                  <Point X="2.453338373359" Y="2.251879356463" Z="0.75" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.75" />
                  <Point X="2.380700969631" Y="2.251424485995" Z="0.75" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.75" />
                  <Point X="2.31558324537" Y="2.284152279049" Z="0.75" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.75" />
                  <Point X="2.266606321186" Y="2.341441615012" Z="0.75" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.75" />
                  <Point X="2.248005216763" Y="2.409057474201" Z="0.75" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.75" />
                  <Point X="2.260648586195" Y="2.486131202523" Z="0.75" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.75" />
                  <Point X="2.521847248128" Y="2.951288461398" Z="0.75" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.75" />
                  <Point X="2.864451767028" Y="4.190127697661" Z="0.75" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.75" />
                  <Point X="2.473403061537" Y="4.432215867395" Z="0.75" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.75" />
                  <Point X="2.065811362486" Y="4.636353955108" Z="0.75" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.75" />
                  <Point X="1.643120616304" Y="4.802448951991" Z="0.75" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.75" />
                  <Point X="1.056048821415" Y="4.959513481082" Z="0.75" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.75" />
                  <Point X="0.391211338109" Y="5.055590623635" Z="0.75" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.75" />
                  <Point X="0.136087536382" Y="4.863010099545" Z="0.75" />
                  <Point X="0" Y="4.355124473572" Z="0.75" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>