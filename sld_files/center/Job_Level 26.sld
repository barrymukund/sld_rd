<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#167" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1945" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="0" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="0.004716064453" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.76063885498" Y="-4.248995117188" />
                  <Point X="0.563302062988" Y="-3.512524414063" />
                  <Point X="0.557721191406" Y="-3.497141845703" />
                  <Point X="0.542363037109" Y="-3.467377197266" />
                  <Point X="0.461706634521" Y="-3.351166503906" />
                  <Point X="0.378635528564" Y="-3.231476806641" />
                  <Point X="0.356751586914" Y="-3.209021484375" />
                  <Point X="0.330496398926" Y="-3.189777587891" />
                  <Point X="0.302495178223" Y="-3.175669189453" />
                  <Point X="0.177683837891" Y="-3.136932373047" />
                  <Point X="0.049136188507" Y="-3.097035888672" />
                  <Point X="0.020976791382" Y="-3.092766601562" />
                  <Point X="-0.008664840698" Y="-3.092766601562" />
                  <Point X="-0.036824237823" Y="-3.097035888672" />
                  <Point X="-0.161635421753" Y="-3.135772460938" />
                  <Point X="-0.290183227539" Y="-3.175669189453" />
                  <Point X="-0.31818460083" Y="-3.189777587891" />
                  <Point X="-0.344439788818" Y="-3.209021484375" />
                  <Point X="-0.366323577881" Y="-3.2314765625" />
                  <Point X="-0.446979980469" Y="-3.347687011719" />
                  <Point X="-0.530051086426" Y="-3.467376953125" />
                  <Point X="-0.538189025879" Y="-3.481573730469" />
                  <Point X="-0.550990112305" Y="-3.512524414063" />
                  <Point X="-0.754234436035" Y="-4.271043457031" />
                  <Point X="-0.916584594727" Y="-4.876941894531" />
                  <Point X="-0.931539306641" Y="-4.8740390625" />
                  <Point X="-1.079340576172" Y="-4.845350585938" />
                  <Point X="-1.220783325195" Y="-4.808958007813" />
                  <Point X="-1.24641796875" Y="-4.802362304688" />
                  <Point X="-1.238717651367" Y="-4.743873046875" />
                  <Point X="-1.214963012695" Y="-4.563438476562" />
                  <Point X="-1.214201171875" Y="-4.5479296875" />
                  <Point X="-1.216508666992" Y="-4.516224121094" />
                  <Point X="-1.246326049805" Y="-4.366322265625" />
                  <Point X="-1.277036132812" Y="-4.211932617188" />
                  <Point X="-1.287938354492" Y="-4.182966308594" />
                  <Point X="-1.304010375977" Y="-4.15512890625" />
                  <Point X="-1.323644775391" Y="-4.131204101562" />
                  <Point X="-1.438555297852" Y="-4.030430175781" />
                  <Point X="-1.556905517578" Y="-3.926639160156" />
                  <Point X="-1.583188476562" Y="-3.910295410156" />
                  <Point X="-1.612885742188" Y="-3.897994384766" />
                  <Point X="-1.64302734375" Y="-3.890966308594" />
                  <Point X="-1.7955390625" Y="-3.880970214844" />
                  <Point X="-1.952616577148" Y="-3.870674804688" />
                  <Point X="-1.98341796875" Y="-3.873708496094" />
                  <Point X="-2.014466674805" Y="-3.882028076172" />
                  <Point X="-2.042657714844" Y="-3.894801513672" />
                  <Point X="-2.169738769531" Y="-3.979714355469" />
                  <Point X="-2.300624267578" Y="-4.067169677734" />
                  <Point X="-2.312785644531" Y="-4.076821533203" />
                  <Point X="-2.335102539062" Y="-4.099461914062" />
                  <Point X="-2.449208496094" Y="-4.248167480469" />
                  <Point X="-2.480148925781" Y="-4.288490234375" />
                  <Point X="-2.585067871094" Y="-4.223526855469" />
                  <Point X="-2.801708984375" Y="-4.089387939453" />
                  <Point X="-2.997564453125" Y="-3.938585693359" />
                  <Point X="-3.104721923828" Y="-3.856077880859" />
                  <Point X="-2.790489257812" Y="-3.311811279297" />
                  <Point X="-2.423761230469" Y="-2.676619628906" />
                  <Point X="-2.412859130859" Y="-2.647653320312" />
                  <Point X="-2.406588134766" Y="-2.616126953125" />
                  <Point X="-2.405575439453" Y="-2.585192871094" />
                  <Point X="-2.414560058594" Y="-2.555575195312" />
                  <Point X="-2.428777099609" Y="-2.526746337891" />
                  <Point X="-2.4468046875" Y="-2.501589355469" />
                  <Point X="-2.464153320312" Y="-2.484240722656" />
                  <Point X="-2.489310302734" Y="-2.466213134766" />
                  <Point X="-2.518139160156" Y="-2.45199609375" />
                  <Point X="-2.547756835938" Y="-2.443011474609" />
                  <Point X="-2.578690917969" Y="-2.444024169922" />
                  <Point X="-2.610217285156" Y="-2.450295166016" />
                  <Point X="-2.63918359375" Y="-2.461197265625" />
                  <Point X="-3.293391357422" Y="-2.838904296875" />
                  <Point X="-3.818024658203" Y="-3.141801269531" />
                  <Point X="-3.911702148438" Y="-3.018728271484" />
                  <Point X="-4.082858642578" Y="-2.79386328125" />
                  <Point X="-4.223270996094" Y="-2.558413085938" />
                  <Point X="-4.306142578125" Y="-2.419450439453" />
                  <Point X="-3.747609619141" Y="-1.990872680664" />
                  <Point X="-3.105954589844" Y="-1.498513549805" />
                  <Point X="-3.084577392578" Y="-1.475593505859" />
                  <Point X="-3.066612548828" Y="-1.448462280273" />
                  <Point X="-3.053856445312" Y="-1.419832885742" />
                  <Point X="-3.046151855469" Y="-1.390085693359" />
                  <Point X="-3.04334765625" Y="-1.359657348633" />
                  <Point X="-3.045556396484" Y="-1.327986328125" />
                  <Point X="-3.052557617188" Y="-1.298240722656" />
                  <Point X="-3.068640380859" Y="-1.272256835938" />
                  <Point X="-3.089473144531" Y="-1.248300415039" />
                  <Point X="-3.112972412109" Y="-1.228767089844" />
                  <Point X="-3.139455078125" Y="-1.213180664062" />
                  <Point X="-3.168718261719" Y="-1.201956420898" />
                  <Point X="-3.200605957031" Y="-1.195474853516" />
                  <Point X="-3.231929199219" Y="-1.194383789062" />
                  <Point X="-4.057804443359" Y="-1.303112548828" />
                  <Point X="-4.732102050781" Y="-1.391885375977" />
                  <Point X="-4.767135742188" Y="-1.254729248047" />
                  <Point X="-4.834077636719" Y="-0.992654418945" />
                  <Point X="-4.8712265625" Y="-0.732911987305" />
                  <Point X="-4.892424316406" Y="-0.584698181152" />
                  <Point X="-4.263817871094" Y="-0.416263519287" />
                  <Point X="-3.532875976562" Y="-0.220408340454" />
                  <Point X="-3.510328857422" Y="-0.211127578735" />
                  <Point X="-3.481088867188" Y="-0.194418182373" />
                  <Point X="-3.467896240234" Y="-0.18532244873" />
                  <Point X="-3.441996582031" Y="-0.164031570435" />
                  <Point X="-3.425828857422" Y="-0.146978515625" />
                  <Point X="-3.414341552734" Y="-0.126478622437" />
                  <Point X="-3.402358886719" Y="-0.097057388306" />
                  <Point X="-3.397795166016" Y="-0.082674942017" />
                  <Point X="-3.390854492188" Y="-0.052730796814" />
                  <Point X="-3.388400878906" Y="-0.03133246994" />
                  <Point X="-3.390830566406" Y="-0.009931515694" />
                  <Point X="-3.397634765625" Y="0.019572896957" />
                  <Point X="-3.402178955078" Y="0.033951545715" />
                  <Point X="-3.414298339844" Y="0.06381206131" />
                  <Point X="-3.425760742188" Y="0.084325691223" />
                  <Point X="-3.441907470703" Y="0.101398246765" />
                  <Point X="-3.467397705078" Y="0.122404678345" />
                  <Point X="-3.480574462891" Y="0.13151361084" />
                  <Point X="-3.510223632812" Y="0.148507156372" />
                  <Point X="-3.532875976562" Y="0.157848342896" />
                  <Point X="-4.285700195312" Y="0.359567016602" />
                  <Point X="-4.89181640625" Y="0.521975219727" />
                  <Point X="-4.867630371094" Y="0.685423522949" />
                  <Point X="-4.824487792969" Y="0.976975097656" />
                  <Point X="-4.749707519531" Y="1.2529375" />
                  <Point X="-4.703551269531" Y="1.423267822266" />
                  <Point X="-4.289623046875" Y="1.368773193359" />
                  <Point X="-3.765666259766" Y="1.29979284668" />
                  <Point X="-3.744986083984" Y="1.299341674805" />
                  <Point X="-3.723424560547" Y="1.301228149414" />
                  <Point X="-3.703137695312" Y="1.305263671875" />
                  <Point X="-3.672877685547" Y="1.31480456543" />
                  <Point X="-3.641711669922" Y="1.324631225586" />
                  <Point X="-3.622779052734" Y="1.332961669922" />
                  <Point X="-3.604034667969" Y="1.343783569336" />
                  <Point X="-3.587353271484" Y="1.356014892578" />
                  <Point X="-3.57371484375" Y="1.371566772461" />
                  <Point X="-3.561300292969" Y="1.38929675293" />
                  <Point X="-3.5513515625" Y="1.407430908203" />
                  <Point X="-3.539209472656" Y="1.436744018555" />
                  <Point X="-3.526704101563" Y="1.466934936523" />
                  <Point X="-3.520915527344" Y="1.486794067383" />
                  <Point X="-3.517157226562" Y="1.508109008789" />
                  <Point X="-3.5158046875" Y="1.528749145508" />
                  <Point X="-3.518951171875" Y="1.549192871094" />
                  <Point X="-3.524552978516" Y="1.570099121094" />
                  <Point X="-3.532049804688" Y="1.589377563477" />
                  <Point X="-3.546700439453" Y="1.617521362305" />
                  <Point X="-3.561789550781" Y="1.646507324219" />
                  <Point X="-3.57328125" Y="1.663705932617" />
                  <Point X="-3.587193847656" Y="1.680286376953" />
                  <Point X="-3.602135986328" Y="1.694590209961" />
                  <Point X="-4.033957763672" Y="2.025938720703" />
                  <Point X="-4.351859863281" Y="2.269874267578" />
                  <Point X="-4.248787109375" Y="2.446462402344" />
                  <Point X="-4.081153564453" Y="2.733659912109" />
                  <Point X="-3.883064697266" Y="2.988274902344" />
                  <Point X="-3.750504882813" Y="3.158661621094" />
                  <Point X="-3.528456787109" Y="3.030462158203" />
                  <Point X="-3.206656982422" Y="2.844670898438" />
                  <Point X="-3.187724609375" Y="2.836340332031" />
                  <Point X="-3.167082519531" Y="2.829831787109" />
                  <Point X="-3.146794433594" Y="2.825796386719" />
                  <Point X="-3.104650634766" Y="2.822109375" />
                  <Point X="-3.061245117188" Y="2.818311767578" />
                  <Point X="-3.040565185547" Y="2.818762939453" />
                  <Point X="-3.019106445312" Y="2.821588134766" />
                  <Point X="-2.999015136719" Y="2.826504394531" />
                  <Point X="-2.980463867188" Y="2.835652832031" />
                  <Point X="-2.962209472656" Y="2.847281982422" />
                  <Point X="-2.946077392578" Y="2.860229492188" />
                  <Point X="-2.916163330078" Y="2.890143554688" />
                  <Point X="-2.885353759766" Y="2.920952880859" />
                  <Point X="-2.872406738281" Y="2.937084228516" />
                  <Point X="-2.86077734375" Y="2.955338623047" />
                  <Point X="-2.851628662109" Y="2.973890136719" />
                  <Point X="-2.846712158203" Y="2.993981933594" />
                  <Point X="-2.843886962891" Y="3.015440673828" />
                  <Point X="-2.843435791016" Y="3.03612109375" />
                  <Point X="-2.847123046875" Y="3.078265136719" />
                  <Point X="-2.850920410156" Y="3.121670654297" />
                  <Point X="-2.854955810547" Y="3.141957763672" />
                  <Point X="-2.861464355469" Y="3.162600097656" />
                  <Point X="-2.869794921875" Y="3.181532958984" />
                  <Point X="-3.0611484375" Y="3.512966552734" />
                  <Point X="-3.183333007812" Y="3.724596435547" />
                  <Point X="-2.992586425781" Y="3.87083984375" />
                  <Point X="-2.700626464844" Y="4.094683349609" />
                  <Point X="-2.388643554688" Y="4.268014160156" />
                  <Point X="-2.167036865234" Y="4.391133789062" />
                  <Point X="-2.141708251953" Y="4.358125" />
                  <Point X="-2.04319543457" Y="4.229740722656" />
                  <Point X="-2.028892456055" Y="4.214799804688" />
                  <Point X="-2.01231237793" Y="4.200887207031" />
                  <Point X="-1.99511340332" Y="4.189395019531" />
                  <Point X="-1.948207519531" Y="4.164977050781" />
                  <Point X="-1.899897338867" Y="4.139828125" />
                  <Point X="-1.880619018555" Y="4.132331054688" />
                  <Point X="-1.859712280273" Y="4.126729003906" />
                  <Point X="-1.839267456055" Y="4.12358203125" />
                  <Point X="-1.818626098633" Y="4.124935058594" />
                  <Point X="-1.797310668945" Y="4.128693847656" />
                  <Point X="-1.777453491211" Y="4.134481933594" />
                  <Point X="-1.72859777832" Y="4.15471875" />
                  <Point X="-1.678279541016" Y="4.175561523437" />
                  <Point X="-1.660145751953" Y="4.185510253906" />
                  <Point X="-1.642416259766" Y="4.197924316406" />
                  <Point X="-1.626864013672" Y="4.211562988281" />
                  <Point X="-1.614632568359" Y="4.228244628906" />
                  <Point X="-1.603810791016" Y="4.246988769531" />
                  <Point X="-1.59548046875" Y="4.265920898438" />
                  <Point X="-1.579578735352" Y="4.316354492188" />
                  <Point X="-1.563201171875" Y="4.368297363281" />
                  <Point X="-1.559165649414" Y="4.388584960938" />
                  <Point X="-1.557279174805" Y="4.410146972656" />
                  <Point X="-1.55773034668" Y="4.430826660156" />
                  <Point X="-1.579484985352" Y="4.5960703125" />
                  <Point X="-1.584201904297" Y="4.631897949219" />
                  <Point X="-1.327596435547" Y="4.703841308594" />
                  <Point X="-0.949634765625" Y="4.80980859375" />
                  <Point X="-0.57142401123" Y="4.854072265625" />
                  <Point X="-0.29471081543" Y="4.886457519531" />
                  <Point X="-0.229100646973" Y="4.641596679688" />
                  <Point X="-0.133903305054" Y="4.286315429688" />
                  <Point X="-0.121129844666" Y="4.258124023438" />
                  <Point X="-0.10327155304" Y="4.231397460938" />
                  <Point X="-0.082114532471" Y="4.20880859375" />
                  <Point X="-0.054819446564" Y="4.19421875" />
                  <Point X="-0.024381370544" Y="4.183886230469" />
                  <Point X="0.006155906677" Y="4.178844238281" />
                  <Point X="0.03669316864" Y="4.183886230469" />
                  <Point X="0.067131248474" Y="4.19421875" />
                  <Point X="0.094426467896" Y="4.20880859375" />
                  <Point X="0.115583503723" Y="4.231397460938" />
                  <Point X="0.133441802979" Y="4.258124023438" />
                  <Point X="0.146215255737" Y="4.286315429688" />
                  <Point X="0.244262451172" Y="4.652232910156" />
                  <Point X="0.307419372559" Y="4.8879375" />
                  <Point X="0.514019226074" Y="4.86630078125" />
                  <Point X="0.844041320801" Y="4.831738769531" />
                  <Point X="1.156950561523" Y="4.756192871094" />
                  <Point X="1.481026611328" Y="4.677950683594" />
                  <Point X="1.683948608398" Y="4.604349609375" />
                  <Point X="1.894646484375" Y="4.527927734375" />
                  <Point X="2.091590332031" Y="4.435823730469" />
                  <Point X="2.294577148438" Y="4.340893066406" />
                  <Point X="2.484860351562" Y="4.230033691406" />
                  <Point X="2.680977050781" Y="4.115775878906" />
                  <Point X="2.860418212891" Y="3.988166992188" />
                  <Point X="2.943260009766" Y="3.929254638672" />
                  <Point X="2.572041259766" Y="3.286284667969" />
                  <Point X="2.147581054688" Y="2.551097900391" />
                  <Point X="2.142075927734" Y="2.539931152344" />
                  <Point X="2.133076660156" Y="2.516056640625" />
                  <Point X="2.122500244141" Y="2.476505615234" />
                  <Point X="2.111606933594" Y="2.435770263672" />
                  <Point X="2.108619384766" Y="2.417934326172" />
                  <Point X="2.107728027344" Y="2.380952880859" />
                  <Point X="2.111852050781" Y="2.346752441406" />
                  <Point X="2.116099365234" Y="2.311528076172" />
                  <Point X="2.121441894531" Y="2.289605224609" />
                  <Point X="2.129708007812" Y="2.267516357422" />
                  <Point X="2.140071044922" Y="2.247470947266" />
                  <Point X="2.161233154297" Y="2.216283447266" />
                  <Point X="2.183028808594" Y="2.184162353516" />
                  <Point X="2.19446484375" Y="2.170328613281" />
                  <Point X="2.221598632812" Y="2.145592529297" />
                  <Point X="2.252786132812" Y="2.124430419922" />
                  <Point X="2.284907226562" Y="2.102635009766" />
                  <Point X="2.304952880859" Y="2.092271972656" />
                  <Point X="2.327041259766" Y="2.084006103516" />
                  <Point X="2.348963867188" Y="2.078663574219" />
                  <Point X="2.383164306641" Y="2.074539550781" />
                  <Point X="2.418388671875" Y="2.070291992188" />
                  <Point X="2.436467529297" Y="2.069845703125" />
                  <Point X="2.473206542969" Y="2.074171142578" />
                  <Point X="2.5127578125" Y="2.084747802734" />
                  <Point X="2.553492919922" Y="2.095640869141" />
                  <Point X="2.565283447266" Y="2.099638183594" />
                  <Point X="2.588533935547" Y="2.110145019531" />
                  <Point X="3.34573046875" Y="2.5473125" />
                  <Point X="3.967326416016" Y="2.906191162109" />
                  <Point X="4.006940673828" Y="2.851136230469" />
                  <Point X="4.123272949219" Y="2.689460205078" />
                  <Point X="4.223307617188" Y="2.52415234375" />
                  <Point X="4.262198730469" Y="2.459884277344" />
                  <Point X="3.789625488281" Y="2.097265625" />
                  <Point X="3.230783935547" Y="1.668451171875" />
                  <Point X="3.221425048828" Y="1.660241943359" />
                  <Point X="3.203973876953" Y="1.641627929688" />
                  <Point X="3.175508789062" Y="1.604493041992" />
                  <Point X="3.146191650391" Y="1.566246459961" />
                  <Point X="3.13660546875" Y="1.550911254883" />
                  <Point X="3.121629882812" Y="1.51708605957" />
                  <Point X="3.111026611328" Y="1.479171264648" />
                  <Point X="3.100105957031" Y="1.440121704102" />
                  <Point X="3.096652587891" Y="1.417823242188" />
                  <Point X="3.095836425781" Y="1.394252929688" />
                  <Point X="3.097739501953" Y="1.371768188477" />
                  <Point X="3.106443603516" Y="1.329583129883" />
                  <Point X="3.115408447266" Y="1.286135498047" />
                  <Point X="3.1206796875" Y="1.268978027344" />
                  <Point X="3.136282470703" Y="1.235740478516" />
                  <Point X="3.159956787109" Y="1.199756347656" />
                  <Point X="3.184340087891" Y="1.162694946289" />
                  <Point X="3.198894042969" Y="1.145449707031" />
                  <Point X="3.216137939453" Y="1.129360107422" />
                  <Point X="3.234347167969" Y="1.116034912109" />
                  <Point X="3.268654541016" Y="1.09672277832" />
                  <Point X="3.303989257812" Y="1.076832397461" />
                  <Point X="3.320521240234" Y="1.069501586914" />
                  <Point X="3.356118164062" Y="1.059438598633" />
                  <Point X="3.402504150391" Y="1.053308105469" />
                  <Point X="3.450278808594" Y="1.046994018555" />
                  <Point X="3.462702148438" Y="1.046175048828" />
                  <Point X="3.488203613281" Y="1.04698449707" />
                  <Point X="4.207489746094" Y="1.141680297852" />
                  <Point X="4.776839355469" Y="1.21663659668" />
                  <Point X="4.79597265625" Y="1.13804309082" />
                  <Point X="4.845936035156" Y="0.932809204102" />
                  <Point X="4.877459960938" Y="0.730335266113" />
                  <Point X="4.890864746094" Y="0.644238464355" />
                  <Point X="4.357024414062" Y="0.501196289062" />
                  <Point X="3.716579833984" Y="0.329589935303" />
                  <Point X="3.704791259766" Y="0.325586608887" />
                  <Point X="3.681545654297" Y="0.315067993164" />
                  <Point X="3.635972900391" Y="0.288725982666" />
                  <Point X="3.589035644531" Y="0.261595428467" />
                  <Point X="3.574310791016" Y="0.251095916748" />
                  <Point X="3.547531005859" Y="0.22557661438" />
                  <Point X="3.520187255859" Y="0.190734344482" />
                  <Point X="3.492025146484" Y="0.154849121094" />
                  <Point X="3.480301025391" Y="0.135569152832" />
                  <Point X="3.470527099609" Y="0.114105384827" />
                  <Point X="3.463680908203" Y="0.092604270935" />
                  <Point X="3.454566162109" Y="0.045011604309" />
                  <Point X="3.445178710938" Y="-0.004006073952" />
                  <Point X="3.443482910156" Y="-0.021875295639" />
                  <Point X="3.445178710938" Y="-0.058553779602" />
                  <Point X="3.454293457031" Y="-0.106146751404" />
                  <Point X="3.463680908203" Y="-0.155164428711" />
                  <Point X="3.470527099609" Y="-0.176665237427" />
                  <Point X="3.480301025391" Y="-0.198129165649" />
                  <Point X="3.492024902344" Y="-0.217408966064" />
                  <Point X="3.519368408203" Y="-0.252251235962" />
                  <Point X="3.547530761719" Y="-0.288136779785" />
                  <Point X="3.559999511719" Y="-0.301236602783" />
                  <Point X="3.589035644531" Y="-0.324155426025" />
                  <Point X="3.634608398438" Y="-0.350497436523" />
                  <Point X="3.681545654297" Y="-0.377627990723" />
                  <Point X="3.692710205078" Y="-0.383139038086" />
                  <Point X="3.716579833984" Y="-0.392149932861" />
                  <Point X="4.376197753906" Y="-0.568894104004" />
                  <Point X="4.891472167969" Y="-0.706961486816" />
                  <Point X="4.882919921875" Y="-0.763688232422" />
                  <Point X="4.855022460938" Y="-0.948725646973" />
                  <Point X="4.814634765625" Y="-1.125710449219" />
                  <Point X="4.801173339844" Y="-1.184698974609" />
                  <Point X="4.167117675781" Y="-1.101223754883" />
                  <Point X="3.424382080078" Y="-1.003441040039" />
                  <Point X="3.408035644531" Y="-1.002710327148" />
                  <Point X="3.374658691406" Y="-1.005508728027" />
                  <Point X="3.285215332031" Y="-1.024949584961" />
                  <Point X="3.193094482422" Y="-1.044972412109" />
                  <Point X="3.163974121094" Y="-1.056596923828" />
                  <Point X="3.136147460938" Y="-1.073489257812" />
                  <Point X="3.112397460938" Y="-1.093960083008" />
                  <Point X="3.058334472656" Y="-1.158980712891" />
                  <Point X="3.002653320312" Y="-1.225948120117" />
                  <Point X="2.987932617188" Y="-1.250330566406" />
                  <Point X="2.976589355469" Y="-1.277715698242" />
                  <Point X="2.969757568359" Y="-1.305365356445" />
                  <Point X="2.962009033203" Y="-1.389570068359" />
                  <Point X="2.954028564453" Y="-1.476295654297" />
                  <Point X="2.956347167969" Y="-1.507564208984" />
                  <Point X="2.964078613281" Y="-1.539185180664" />
                  <Point X="2.976450195312" Y="-1.567996582031" />
                  <Point X="3.025949462891" Y="-1.644989624023" />
                  <Point X="3.076930419922" Y="-1.724287109375" />
                  <Point X="3.086932861328" Y="-1.737239013672" />
                  <Point X="3.110628417969" Y="-1.760909179688" />
                  <Point X="3.7227578125" Y="-2.230612792969" />
                  <Point X="4.213122070312" Y="-2.606883056641" />
                  <Point X="4.203450683594" Y="-2.622533203125" />
                  <Point X="4.124810058594" Y="-2.749785888672" />
                  <Point X="4.041285400391" Y="-2.868462646484" />
                  <Point X="4.028981445312" Y="-2.885945068359" />
                  <Point X="3.4624609375" Y="-2.558864257812" />
                  <Point X="2.800954589844" Y="-2.176943115234" />
                  <Point X="2.786131591797" Y="-2.170012451172" />
                  <Point X="2.754224609375" Y="-2.159825195312" />
                  <Point X="2.647772949219" Y="-2.140600097656" />
                  <Point X="2.538134277344" Y="-2.120799560547" />
                  <Point X="2.506781982422" Y="-2.120395263672" />
                  <Point X="2.474609130859" Y="-2.125353027344" />
                  <Point X="2.444833251953" Y="-2.135176757812" />
                  <Point X="2.356397949219" Y="-2.181719482422" />
                  <Point X="2.265315185547" Y="-2.229655761719" />
                  <Point X="2.242384033203" Y="-2.246549072266" />
                  <Point X="2.221424316406" Y="-2.267509033203" />
                  <Point X="2.204531982422" Y="-2.290439453125" />
                  <Point X="2.157989013672" Y="-2.378874755859" />
                  <Point X="2.110052978516" Y="-2.469957519531" />
                  <Point X="2.100229003906" Y="-2.499733398438" />
                  <Point X="2.095271240234" Y="-2.531906005859" />
                  <Point X="2.095675537109" Y="-2.563258300781" />
                  <Point X="2.114900634766" Y="-2.669709960938" />
                  <Point X="2.134701171875" Y="-2.779348632812" />
                  <Point X="2.138985595703" Y="-2.795141845703" />
                  <Point X="2.151819091797" Y="-2.826078613281" />
                  <Point X="2.545173828125" Y="-3.507388671875" />
                  <Point X="2.861283447266" Y="-4.054906738281" />
                  <Point X="2.781853515625" Y="-4.111641601562" />
                  <Point X="2.701765136719" Y="-4.163481445312" />
                  <Point X="2.263336425781" Y="-3.592110351562" />
                  <Point X="1.758546142578" Y="-2.934255126953" />
                  <Point X="1.747503540039" Y="-2.922178955078" />
                  <Point X="1.721924194336" Y="-2.900557373047" />
                  <Point X="1.616933959961" Y="-2.833058349609" />
                  <Point X="1.50880078125" Y="-2.763538574219" />
                  <Point X="1.479989746094" Y="-2.751167236328" />
                  <Point X="1.448368774414" Y="-2.743435546875" />
                  <Point X="1.417099609375" Y="-2.741116699219" />
                  <Point X="1.302274902344" Y="-2.751683105469" />
                  <Point X="1.184012695312" Y="-2.762565673828" />
                  <Point X="1.15636315918" Y="-2.769397460938" />
                  <Point X="1.128978027344" Y="-2.780740722656" />
                  <Point X="1.104595947266" Y="-2.795461181641" />
                  <Point X="1.015931213379" Y="-2.869182861328" />
                  <Point X="0.924612121582" Y="-2.945111572266" />
                  <Point X="0.904141052246" Y="-2.968861816406" />
                  <Point X="0.887248779297" Y="-2.996688476562" />
                  <Point X="0.875624328613" Y="-3.025808837891" />
                  <Point X="0.849114135742" Y="-3.147777099609" />
                  <Point X="0.821810058594" Y="-3.273396728516" />
                  <Point X="0.819724487305" Y="-3.289627441406" />
                  <Point X="0.819742248535" Y="-3.323120117188" />
                  <Point X="0.931215759277" Y="-4.169846679688" />
                  <Point X="1.022065307617" Y="-4.859915527344" />
                  <Point X="0.975686706543" Y="-4.870081542969" />
                  <Point X="0.929315551758" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058434326172" Y="-4.752635742188" />
                  <Point X="-1.141246337891" Y="-4.731328613281" />
                  <Point X="-1.120775634766" Y="-4.575838378906" />
                  <Point X="-1.120077514648" Y="-4.568099609375" />
                  <Point X="-1.119451782227" Y="-4.541033691406" />
                  <Point X="-1.121759277344" Y="-4.509328125" />
                  <Point X="-1.123334106445" Y="-4.497690429688" />
                  <Point X="-1.153151611328" Y="-4.347788574219" />
                  <Point X="-1.183861450195" Y="-4.193398925781" />
                  <Point X="-1.188125" Y="-4.17846875" />
                  <Point X="-1.19902734375" Y="-4.149502441406" />
                  <Point X="-1.205666015625" Y="-4.135466308594" />
                  <Point X="-1.221738037109" Y="-4.10762890625" />
                  <Point X="-1.23057409668" Y="-4.094861816406" />
                  <Point X="-1.250208496094" Y="-4.070937011719" />
                  <Point X="-1.261006958008" Y="-4.059779296875" />
                  <Point X="-1.375917358398" Y="-3.959005371094" />
                  <Point X="-1.494267578125" Y="-3.855214355469" />
                  <Point X="-1.506739135742" Y="-3.84596484375" />
                  <Point X="-1.533021972656" Y="-3.82962109375" />
                  <Point X="-1.546833618164" Y="-3.822526855469" />
                  <Point X="-1.576530761719" Y="-3.810225830078" />
                  <Point X="-1.591313476562" Y="-3.805476074219" />
                  <Point X="-1.621454833984" Y="-3.798447998047" />
                  <Point X="-1.636813964844" Y="-3.796169677734" />
                  <Point X="-1.789325805664" Y="-3.786173583984" />
                  <Point X="-1.946403198242" Y="-3.775878173828" />
                  <Point X="-1.961928222656" Y="-3.776132324219" />
                  <Point X="-1.992729614258" Y="-3.779166015625" />
                  <Point X="-2.008006103516" Y="-3.781945556641" />
                  <Point X="-2.03905480957" Y="-3.790265136719" />
                  <Point X="-2.053674560547" Y="-3.795496337891" />
                  <Point X="-2.081865478516" Y="-3.808269775391" />
                  <Point X="-2.095436767578" Y="-3.815812011719" />
                  <Point X="-2.222517822266" Y="-3.900724853516" />
                  <Point X="-2.353403320312" Y="-3.988180175781" />
                  <Point X="-2.359681640625" Y="-3.992757080078" />
                  <Point X="-2.380442382812" Y="-4.010131347656" />
                  <Point X="-2.402759277344" Y="-4.032771728516" />
                  <Point X="-2.410470947266" Y="-4.041629638672" />
                  <Point X="-2.503202636719" Y="-4.162479492188" />
                  <Point X="-2.535056640625" Y="-4.142756347656" />
                  <Point X="-2.747585205078" Y="-4.011163818359" />
                  <Point X="-2.939607177734" Y="-3.863313232422" />
                  <Point X="-2.980862792969" Y="-3.831547851562" />
                  <Point X="-2.708216796875" Y="-3.359311279297" />
                  <Point X="-2.341488769531" Y="-2.724119628906" />
                  <Point X="-2.334850097656" Y="-2.710083251953" />
                  <Point X="-2.323947998047" Y="-2.681116943359" />
                  <Point X="-2.319684570312" Y="-2.666187011719" />
                  <Point X="-2.313413574219" Y="-2.634660644531" />
                  <Point X="-2.311638916016" Y="-2.619235351562" />
                  <Point X="-2.310626220703" Y="-2.588301269531" />
                  <Point X="-2.314666259766" Y="-2.557615234375" />
                  <Point X="-2.323650878906" Y="-2.527997558594" />
                  <Point X="-2.329357421875" Y="-2.513557128906" />
                  <Point X="-2.343574462891" Y="-2.484728271484" />
                  <Point X="-2.351557128906" Y="-2.47141015625" />
                  <Point X="-2.369584716797" Y="-2.446253173828" />
                  <Point X="-2.379629638672" Y="-2.434414306641" />
                  <Point X="-2.396978271484" Y="-2.417065673828" />
                  <Point X="-2.408817138672" Y="-2.407020751953" />
                  <Point X="-2.433974121094" Y="-2.388993164062" />
                  <Point X="-2.447292236328" Y="-2.381010498047" />
                  <Point X="-2.47612109375" Y="-2.366793457031" />
                  <Point X="-2.490561523438" Y="-2.361086914062" />
                  <Point X="-2.520179199219" Y="-2.352102294922" />
                  <Point X="-2.550865234375" Y="-2.348062255859" />
                  <Point X="-2.581799316406" Y="-2.349074951172" />
                  <Point X="-2.597224609375" Y="-2.350849609375" />
                  <Point X="-2.628750976562" Y="-2.357120605469" />
                  <Point X="-2.643680908203" Y="-2.361384033203" />
                  <Point X="-2.672647216797" Y="-2.372286132812" />
                  <Point X="-2.68668359375" Y="-2.378924804688" />
                  <Point X="-3.340891357422" Y="-2.756631835938" />
                  <Point X="-3.79308984375" Y="-3.017708496094" />
                  <Point X="-3.836108886719" Y="-2.961190185547" />
                  <Point X="-4.004013427734" Y="-2.740597412109" />
                  <Point X="-4.141678222656" Y="-2.509754638672" />
                  <Point X="-4.181265136719" Y="-2.443373535156" />
                  <Point X="-3.68977734375" Y="-2.066241210938" />
                  <Point X="-3.048122314453" Y="-1.573882080078" />
                  <Point X="-3.036481933594" Y="-1.563309692383" />
                  <Point X="-3.015104736328" Y="-1.540389648438" />
                  <Point X="-3.005367675781" Y="-1.528041870117" />
                  <Point X="-2.987402832031" Y="-1.500910644531" />
                  <Point X="-2.979836425781" Y="-1.487126220703" />
                  <Point X="-2.967080322266" Y="-1.458496826172" />
                  <Point X="-2.961891113281" Y="-1.443652099609" />
                  <Point X="-2.954186523438" Y="-1.413904907227" />
                  <Point X="-2.951552734375" Y="-1.398803710938" />
                  <Point X="-2.948748535156" Y="-1.368375244141" />
                  <Point X="-2.948577880859" Y="-1.353048095703" />
                  <Point X="-2.950786621094" Y="-1.321377075195" />
                  <Point X="-2.953083251953" Y="-1.306220947266" />
                  <Point X="-2.960084472656" Y="-1.276475341797" />
                  <Point X="-2.971779052734" Y="-1.248242675781" />
                  <Point X="-2.987861816406" Y="-1.222258789062" />
                  <Point X="-2.996954589844" Y="-1.20991796875" />
                  <Point X="-3.017787353516" Y="-1.185961547852" />
                  <Point X="-3.028746337891" Y="-1.175243896484" />
                  <Point X="-3.052245605469" Y="-1.155710693359" />
                  <Point X="-3.064786132812" Y="-1.14689465332" />
                  <Point X="-3.091268798828" Y="-1.131308227539" />
                  <Point X="-3.10543359375" Y="-1.124481567383" />
                  <Point X="-3.134696777344" Y="-1.113257446289" />
                  <Point X="-3.149795410156" Y="-1.108860107422" />
                  <Point X="-3.181683105469" Y="-1.102378540039" />
                  <Point X="-3.197298828125" Y="-1.100532470703" />
                  <Point X="-3.228622070312" Y="-1.09944140625" />
                  <Point X="-3.244329101562" Y="-1.100196533203" />
                  <Point X="-4.070204345703" Y="-1.208925415039" />
                  <Point X="-4.660920898438" Y="-1.286694458008" />
                  <Point X="-4.675090820312" Y="-1.231218383789" />
                  <Point X="-4.740762207031" Y="-0.974118286133" />
                  <Point X="-4.77718359375" Y="-0.719461669922" />
                  <Point X="-4.786452636719" Y="-0.654654296875" />
                  <Point X="-4.239229980469" Y="-0.50802645874" />
                  <Point X="-3.508288085938" Y="-0.312171325684" />
                  <Point X="-3.496715820312" Y="-0.308257354736" />
                  <Point X="-3.474168701172" Y="-0.298976593018" />
                  <Point X="-3.463193847656" Y="-0.293609680176" />
                  <Point X="-3.433953857422" Y="-0.276900299072" />
                  <Point X="-3.427164794922" Y="-0.272630645752" />
                  <Point X="-3.407568603516" Y="-0.258709014893" />
                  <Point X="-3.381668945312" Y="-0.237418182373" />
                  <Point X="-3.373055664062" Y="-0.229393280029" />
                  <Point X="-3.356887939453" Y="-0.212340286255" />
                  <Point X="-3.342953369141" Y="-0.193418518066" />
                  <Point X="-3.331466064453" Y="-0.172918670654" />
                  <Point X="-3.326358886719" Y="-0.162312194824" />
                  <Point X="-3.314376220703" Y="-0.132890945435" />
                  <Point X="-3.311808105469" Y="-0.12579019165" />
                  <Point X="-3.305248779297" Y="-0.104126029968" />
                  <Point X="-3.298308105469" Y="-0.074181930542" />
                  <Point X="-3.296472900391" Y="-0.063553016663" />
                  <Point X="-3.294019287109" Y="-0.042154586792" />
                  <Point X="-3.294007324219" Y="-0.020615859985" />
                  <Point X="-3.296437011719" Y="0.000785095274" />
                  <Point X="-3.298260253906" Y="0.011416686058" />
                  <Point X="-3.305064453125" Y="0.040921154022" />
                  <Point X="-3.30705078125" Y="0.048200855255" />
                  <Point X="-3.314152832031" Y="0.069678352356" />
                  <Point X="-3.326272216797" Y="0.099538917542" />
                  <Point X="-3.331366699219" Y="0.110151786804" />
                  <Point X="-3.342829101562" Y="0.130665313721" />
                  <Point X="-3.356740234375" Y="0.149603286743" />
                  <Point X="-3.372886962891" Y="0.16667590332" />
                  <Point X="-3.381490478516" Y="0.174711196899" />
                  <Point X="-3.406980712891" Y="0.195717559814" />
                  <Point X="-3.413376708984" Y="0.200550170898" />
                  <Point X="-3.433333984375" Y="0.213935455322" />
                  <Point X="-3.462983154297" Y="0.230929000854" />
                  <Point X="-3.474006835938" Y="0.236332778931" />
                  <Point X="-3.496659179688" Y="0.245674026489" />
                  <Point X="-3.508288085938" Y="0.249611343384" />
                  <Point X="-4.261112304688" Y="0.451329925537" />
                  <Point X="-4.785446289062" Y="0.591824707031" />
                  <Point X="-4.773653808594" Y="0.67151739502" />
                  <Point X="-4.731330566406" Y="0.957532470703" />
                  <Point X="-4.658014648438" Y="1.228090454102" />
                  <Point X="-4.633586425781" Y="1.318237060547" />
                  <Point X="-4.302022949219" Y="1.2745859375" />
                  <Point X="-3.778066162109" Y="1.20560559082" />
                  <Point X="-3.76773828125" Y="1.204815429688" />
                  <Point X="-3.747058105469" Y="1.204364257812" />
                  <Point X="-3.736705810547" Y="1.20470324707" />
                  <Point X="-3.715144287109" Y="1.20658972168" />
                  <Point X="-3.704890136719" Y="1.208053710938" />
                  <Point X="-3.684603271484" Y="1.212089233398" />
                  <Point X="-3.674570800781" Y="1.214660522461" />
                  <Point X="-3.644310791016" Y="1.224201416016" />
                  <Point X="-3.613144775391" Y="1.234028076172" />
                  <Point X="-3.603451171875" Y="1.237676391602" />
                  <Point X="-3.584518554688" Y="1.246006958008" />
                  <Point X="-3.575279541016" Y="1.250688964844" />
                  <Point X="-3.55653515625" Y="1.261510864258" />
                  <Point X="-3.547860107422" Y="1.267171386719" />
                  <Point X="-3.531178710938" Y="1.279402832031" />
                  <Point X="-3.515927978516" Y="1.293377441406" />
                  <Point X="-3.502289550781" Y="1.308929443359" />
                  <Point X="-3.495895263672" Y="1.317077392578" />
                  <Point X="-3.483480712891" Y="1.334807373047" />
                  <Point X="-3.478011230469" Y="1.343602905273" />
                  <Point X="-3.4680625" Y="1.361737060547" />
                  <Point X="-3.463583251953" Y="1.371075439453" />
                  <Point X="-3.451441162109" Y="1.400388549805" />
                  <Point X="-3.438935791016" Y="1.430579467773" />
                  <Point X="-3.435499511719" Y="1.440350585938" />
                  <Point X="-3.4297109375" Y="1.460209472656" />
                  <Point X="-3.427358642578" Y="1.470297851562" />
                  <Point X="-3.423600341797" Y="1.491612792969" />
                  <Point X="-3.422360595703" Y="1.501896850586" />
                  <Point X="-3.421008056641" Y="1.522537109375" />
                  <Point X="-3.42191015625" Y="1.543200439453" />
                  <Point X="-3.425056640625" Y="1.563644165039" />
                  <Point X="-3.427188232422" Y="1.573780639648" />
                  <Point X="-3.432790039062" Y="1.594686889648" />
                  <Point X="-3.436011962891" Y="1.604530029297" />
                  <Point X="-3.443508789062" Y="1.62380859375" />
                  <Point X="-3.447783691406" Y="1.633243408203" />
                  <Point X="-3.462434326172" Y="1.661387207031" />
                  <Point X="-3.4775234375" Y="1.690373168945" />
                  <Point X="-3.482799804688" Y="1.699286376953" />
                  <Point X="-3.494291503906" Y="1.716484985352" />
                  <Point X="-3.500506835938" Y="1.724770629883" />
                  <Point X="-3.514419433594" Y="1.741351196289" />
                  <Point X="-3.521500488281" Y="1.748911254883" />
                  <Point X="-3.536442626953" Y="1.763215087891" />
                  <Point X="-3.544303710938" Y="1.769958618164" />
                  <Point X="-3.976125488281" Y="2.101307373047" />
                  <Point X="-4.227614257812" Y="2.294281982422" />
                  <Point X="-4.166740722656" Y="2.398572753906" />
                  <Point X="-4.002293701172" Y="2.680311523438" />
                  <Point X="-3.808083984375" Y="2.929940429688" />
                  <Point X="-3.726338378906" Y="3.035012695312" />
                  <Point X="-3.575956787109" Y="2.948189697266" />
                  <Point X="-3.254156982422" Y="2.7623984375" />
                  <Point X="-3.244918457031" Y="2.757716552734" />
                  <Point X="-3.225986083984" Y="2.749385986328" />
                  <Point X="-3.216292236328" Y="2.745737304688" />
                  <Point X="-3.195650146484" Y="2.739228759766" />
                  <Point X="-3.185615478516" Y="2.736656982422" />
                  <Point X="-3.165327392578" Y="2.732621582031" />
                  <Point X="-3.155073974609" Y="2.731157958984" />
                  <Point X="-3.112930175781" Y="2.727470947266" />
                  <Point X="-3.069524658203" Y="2.723673339844" />
                  <Point X="-3.059173095703" Y="2.723334472656" />
                  <Point X="-3.038493164062" Y="2.723785644531" />
                  <Point X="-3.028164794922" Y="2.724575683594" />
                  <Point X="-3.006706054688" Y="2.727400878906" />
                  <Point X="-2.996526611328" Y="2.729310546875" />
                  <Point X="-2.976435302734" Y="2.734226806641" />
                  <Point X="-2.956997802734" Y="2.741301513672" />
                  <Point X="-2.938446533203" Y="2.750449951172" />
                  <Point X="-2.929420898438" Y="2.755530273438" />
                  <Point X="-2.911166503906" Y="2.767159423828" />
                  <Point X="-2.902746337891" Y="2.773193359375" />
                  <Point X="-2.886614257812" Y="2.786140869141" />
                  <Point X="-2.87890234375" Y="2.793054443359" />
                  <Point X="-2.84898828125" Y="2.822968505859" />
                  <Point X="-2.818178710938" Y="2.853777832031" />
                  <Point X="-2.811265380859" Y="2.861489501953" />
                  <Point X="-2.798318359375" Y="2.877620849609" />
                  <Point X="-2.792284667969" Y="2.886040527344" />
                  <Point X="-2.780655273438" Y="2.904294921875" />
                  <Point X="-2.775574707031" Y="2.913320800781" />
                  <Point X="-2.766426025391" Y="2.931872314453" />
                  <Point X="-2.759351318359" Y="2.951309570312" />
                  <Point X="-2.754434814453" Y="2.971401367188" />
                  <Point X="-2.752524902344" Y="2.981581542969" />
                  <Point X="-2.749699707031" Y="3.003040283203" />
                  <Point X="-2.748909667969" Y="3.013368652344" />
                  <Point X="-2.748458496094" Y="3.034049072266" />
                  <Point X="-2.748797363281" Y="3.044401123047" />
                  <Point X="-2.752484619141" Y="3.086545166016" />
                  <Point X="-2.756281982422" Y="3.129950683594" />
                  <Point X="-2.757745849609" Y="3.140204345703" />
                  <Point X="-2.76178125" Y="3.160491455078" />
                  <Point X="-2.764352783203" Y="3.170524902344" />
                  <Point X="-2.770861328125" Y="3.191167236328" />
                  <Point X="-2.774509521484" Y="3.200860595703" />
                  <Point X="-2.782840087891" Y="3.219793457031" />
                  <Point X="-2.787522460938" Y="3.229032958984" />
                  <Point X="-2.978875976562" Y="3.560466552734" />
                  <Point X="-3.059387207031" Y="3.699916259766" />
                  <Point X="-2.934784423828" Y="3.795447998047" />
                  <Point X="-2.648374755859" Y="4.015036132812" />
                  <Point X="-2.342506103516" Y="4.184970214844" />
                  <Point X="-2.192525146484" Y="4.268296386719" />
                  <Point X="-2.118563964844" Y="4.171908203125" />
                  <Point X="-2.111819580078" Y="4.164046386719" />
                  <Point X="-2.097516601562" Y="4.14910546875" />
                  <Point X="-2.089958007813" Y="4.142026367188" />
                  <Point X="-2.073377929688" Y="4.128113769531" />
                  <Point X="-2.065092285156" Y="4.121897949219" />
                  <Point X="-2.047893188477" Y="4.110405761719" />
                  <Point X="-2.038979980469" Y="4.105129394531" />
                  <Point X="-1.992073974609" Y="4.080711181641" />
                  <Point X="-1.943763916016" Y="4.055562255859" />
                  <Point X="-1.934329467773" Y="4.051287597656" />
                  <Point X="-1.915051147461" Y="4.043790527344" />
                  <Point X="-1.905207275391" Y="4.040568115234" />
                  <Point X="-1.884300537109" Y="4.034966064453" />
                  <Point X="-1.874164916992" Y="4.032834716797" />
                  <Point X="-1.853720092773" Y="4.029687744141" />
                  <Point X="-1.833053588867" Y="4.028785400391" />
                  <Point X="-1.812412231445" Y="4.030138427734" />
                  <Point X="-1.802128173828" Y="4.031378662109" />
                  <Point X="-1.78081262207" Y="4.035137451172" />
                  <Point X="-1.770725830078" Y="4.037489501953" />
                  <Point X="-1.750868774414" Y="4.043277587891" />
                  <Point X="-1.741098388672" Y="4.046713378906" />
                  <Point X="-1.692242675781" Y="4.066950195312" />
                  <Point X="-1.641924438477" Y="4.08779296875" />
                  <Point X="-1.632584960938" Y="4.092272949219" />
                  <Point X="-1.614451171875" Y="4.102221679688" />
                  <Point X="-1.605656738281" Y="4.107690429687" />
                  <Point X="-1.587927246094" Y="4.120104492188" />
                  <Point X="-1.579779296875" Y="4.126499023438" />
                  <Point X="-1.564226928711" Y="4.140137695312" />
                  <Point X="-1.550251708984" Y="4.155388671875" />
                  <Point X="-1.538020263672" Y="4.1720703125" />
                  <Point X="-1.532359863281" Y="4.180745117188" />
                  <Point X="-1.521538085938" Y="4.199489257812" />
                  <Point X="-1.516856201172" Y="4.208728027344" />
                  <Point X="-1.508525878906" Y="4.22766015625" />
                  <Point X="-1.504877441406" Y="4.237353515625" />
                  <Point X="-1.488975708008" Y="4.287787109375" />
                  <Point X="-1.472598144531" Y="4.339729980469" />
                  <Point X="-1.470026611328" Y="4.349763671875" />
                  <Point X="-1.465990966797" Y="4.370051269531" />
                  <Point X="-1.46452722168" Y="4.380305175781" />
                  <Point X="-1.46264074707" Y="4.4018671875" />
                  <Point X="-1.462301757812" Y="4.412219238281" />
                  <Point X="-1.462752929688" Y="4.432898925781" />
                  <Point X="-1.46354309082" Y="4.4432265625" />
                  <Point X="-1.479266113281" Y="4.562655273438" />
                  <Point X="-1.301950561523" Y="4.612368652344" />
                  <Point X="-0.931178222656" Y="4.7163203125" />
                  <Point X="-0.560381164551" Y="4.759716308594" />
                  <Point X="-0.365222015381" Y="4.782556640625" />
                  <Point X="-0.320863616943" Y="4.617008789062" />
                  <Point X="-0.22566633606" Y="4.261727539062" />
                  <Point X="-0.220435256958" Y="4.247107910156" />
                  <Point X="-0.207661849976" Y="4.218916503906" />
                  <Point X="-0.200119247437" Y="4.205344726562" />
                  <Point X="-0.182260864258" Y="4.178618164062" />
                  <Point X="-0.17260824585" Y="4.166455566406" />
                  <Point X="-0.151451187134" Y="4.143866699219" />
                  <Point X="-0.126897941589" Y="4.125026367188" />
                  <Point X="-0.099602897644" Y="4.110436523438" />
                  <Point X="-0.085356651306" Y="4.104260742188" />
                  <Point X="-0.054918682098" Y="4.093927978516" />
                  <Point X="-0.039857234955" Y="4.090155273438" />
                  <Point X="-0.009319986343" Y="4.08511328125" />
                  <Point X="0.021631772995" Y="4.08511328125" />
                  <Point X="0.052169025421" Y="4.090155273438" />
                  <Point X="0.06723046875" Y="4.093927978516" />
                  <Point X="0.097668441772" Y="4.104260742188" />
                  <Point X="0.111914535522" Y="4.110436523438" />
                  <Point X="0.139209732056" Y="4.125026367188" />
                  <Point X="0.163763122559" Y="4.143866699219" />
                  <Point X="0.184920181274" Y="4.166455566406" />
                  <Point X="0.194572952271" Y="4.178617675781" />
                  <Point X="0.212431182861" Y="4.205344238281" />
                  <Point X="0.2199737854" Y="4.218916503906" />
                  <Point X="0.232747192383" Y="4.247107910156" />
                  <Point X="0.237978271484" Y="4.261727539062" />
                  <Point X="0.336025390625" Y="4.627645019531" />
                  <Point X="0.3781902771" Y="4.785006347656" />
                  <Point X="0.504124267578" Y="4.771817382812" />
                  <Point X="0.82787677002" Y="4.737912109375" />
                  <Point X="1.134655273438" Y="4.663846191406" />
                  <Point X="1.453596679688" Y="4.58684375" />
                  <Point X="1.651556396484" Y="4.515042480469" />
                  <Point X="1.858257446289" Y="4.4400703125" />
                  <Point X="2.051345703125" Y="4.34976953125" />
                  <Point X="2.250452636719" Y="4.256653320312" />
                  <Point X="2.437037353516" Y="4.147948730469" />
                  <Point X="2.629431884766" Y="4.035859130859" />
                  <Point X="2.805361572266" Y="3.910747558594" />
                  <Point X="2.817779785156" Y="3.901916503906" />
                  <Point X="2.489768798828" Y="3.333784667969" />
                  <Point X="2.06530859375" Y="2.598597900391" />
                  <Point X="2.062373046875" Y="2.593104980469" />
                  <Point X="2.053181396484" Y="2.573438964844" />
                  <Point X="2.044182250977" Y="2.549564453125" />
                  <Point X="2.041301391602" Y="2.540598388672" />
                  <Point X="2.030724975586" Y="2.501047363281" />
                  <Point X="2.019831665039" Y="2.460312011719" />
                  <Point X="2.017912231445" Y="2.451464355469" />
                  <Point X="2.013646972656" Y="2.420223388672" />
                  <Point X="2.012755615234" Y="2.383241943359" />
                  <Point X="2.013411254883" Y="2.369579833984" />
                  <Point X="2.01753527832" Y="2.335379394531" />
                  <Point X="2.021782592773" Y="2.300155029297" />
                  <Point X="2.023800537109" Y="2.28903515625" />
                  <Point X="2.029143066406" Y="2.267112304688" />
                  <Point X="2.032468017578" Y="2.256309326172" />
                  <Point X="2.040734130859" Y="2.234220458984" />
                  <Point X="2.045318237305" Y="2.223888671875" />
                  <Point X="2.055681396484" Y="2.203843261719" />
                  <Point X="2.061459960938" Y="2.194129638672" />
                  <Point X="2.082622070312" Y="2.162942138672" />
                  <Point X="2.104417724609" Y="2.130821044922" />
                  <Point X="2.10980859375" Y="2.123633056641" />
                  <Point X="2.130463134766" Y="2.100123291016" />
                  <Point X="2.157596923828" Y="2.075387207031" />
                  <Point X="2.168257324219" Y="2.066981445312" />
                  <Point X="2.199444824219" Y="2.045819213867" />
                  <Point X="2.231565917969" Y="2.024023925781" />
                  <Point X="2.241280029297" Y="2.018244995117" />
                  <Point X="2.261325683594" Y="2.007881958008" />
                  <Point X="2.271657226562" Y="2.003297851562" />
                  <Point X="2.293745605469" Y="1.995031982422" />
                  <Point X="2.304548095703" Y="1.991707397461" />
                  <Point X="2.326470703125" Y="1.986364868164" />
                  <Point X="2.337590820312" Y="1.984346801758" />
                  <Point X="2.371791259766" Y="1.98022277832" />
                  <Point X="2.407015625" Y="1.975975219727" />
                  <Point X="2.416044189453" Y="1.975320922852" />
                  <Point X="2.447575439453" Y="1.975497314453" />
                  <Point X="2.484314453125" Y="1.979822875977" />
                  <Point X="2.497748779297" Y="1.982395996094" />
                  <Point X="2.537300048828" Y="1.99297265625" />
                  <Point X="2.57803515625" Y="2.003865722656" />
                  <Point X="2.583995361328" Y="2.005670776367" />
                  <Point X="2.604404541016" Y="2.013067260742" />
                  <Point X="2.627655029297" Y="2.02357409668" />
                  <Point X="2.636033935547" Y="2.027872558594" />
                  <Point X="3.39323046875" Y="2.465040039062" />
                  <Point X="3.940404785156" Y="2.780951416016" />
                  <Point X="4.043952880859" Y="2.637042724609" />
                  <Point X="4.136884765625" Y="2.483472412109" />
                  <Point X="3.731793212891" Y="2.172634277344" />
                  <Point X="3.172951660156" Y="1.743819824219" />
                  <Point X="3.168138671875" Y="1.739869506836" />
                  <Point X="3.152120117188" Y="1.725217407227" />
                  <Point X="3.134668945312" Y="1.706603393555" />
                  <Point X="3.128576416016" Y="1.699422485352" />
                  <Point X="3.100111328125" Y="1.662287597656" />
                  <Point X="3.070794189453" Y="1.624041015625" />
                  <Point X="3.065635742188" Y="1.616602783203" />
                  <Point X="3.04973828125" Y="1.589370361328" />
                  <Point X="3.034762695312" Y="1.555545166016" />
                  <Point X="3.030140136719" Y="1.54267199707" />
                  <Point X="3.019536865234" Y="1.504757324219" />
                  <Point X="3.008616210938" Y="1.465707885742" />
                  <Point X="3.006225097656" Y="1.454661010742" />
                  <Point X="3.002771728516" Y="1.432362548828" />
                  <Point X="3.001709472656" Y="1.421110839844" />
                  <Point X="3.000893310547" Y="1.397540649414" />
                  <Point X="3.001174804688" Y="1.386240966797" />
                  <Point X="3.003077880859" Y="1.363756225586" />
                  <Point X="3.004699462891" Y="1.352571044922" />
                  <Point X="3.013403564453" Y="1.310385986328" />
                  <Point X="3.022368408203" Y="1.266938354492" />
                  <Point X="3.02459765625" Y="1.258235961914" />
                  <Point X="3.03468359375" Y="1.228608642578" />
                  <Point X="3.050286376953" Y="1.19537109375" />
                  <Point X="3.056918457031" Y="1.183526123047" />
                  <Point X="3.080592773438" Y="1.147541992188" />
                  <Point X="3.104976074219" Y="1.110480712891" />
                  <Point X="3.111739257813" Y="1.101424072266" />
                  <Point X="3.126293212891" Y="1.084178833008" />
                  <Point X="3.134083984375" Y="1.075989990234" />
                  <Point X="3.151327880859" Y="1.059900390625" />
                  <Point X="3.160035644531" Y="1.052694946289" />
                  <Point X="3.178244873047" Y="1.039369750977" />
                  <Point X="3.187746337891" Y="1.033249755859" />
                  <Point X="3.222053710938" Y="1.013937744141" />
                  <Point X="3.257388427734" Y="0.994047424316" />
                  <Point X="3.265479492188" Y="0.989987609863" />
                  <Point X="3.294678222656" Y="0.978084289551" />
                  <Point X="3.330275146484" Y="0.96802130127" />
                  <Point X="3.343670898438" Y="0.965257507324" />
                  <Point X="3.390056884766" Y="0.95912713623" />
                  <Point X="3.437831542969" Y="0.952812988281" />
                  <Point X="3.444029785156" Y="0.952199829102" />
                  <Point X="3.465716064453" Y="0.951222900391" />
                  <Point X="3.491217529297" Y="0.952032287598" />
                  <Point X="3.500603515625" Y="0.952797241211" />
                  <Point X="4.219889648438" Y="1.047493041992" />
                  <Point X="4.704703613281" Y="1.111320068359" />
                  <Point X="4.75268359375" Y="0.914233276367" />
                  <Point X="4.783590820312" Y="0.715720397949" />
                  <Point X="4.78387109375" Y="0.713920776367" />
                  <Point X="4.332436523438" Y="0.592959228516" />
                  <Point X="3.691991943359" Y="0.421352844238" />
                  <Point X="3.686031738281" Y="0.419544525146" />
                  <Point X="3.665626708984" Y="0.412138061523" />
                  <Point X="3.642381103516" Y="0.401619415283" />
                  <Point X="3.634004394531" Y="0.397316497803" />
                  <Point X="3.588431640625" Y="0.37097442627" />
                  <Point X="3.541494384766" Y="0.343843902588" />
                  <Point X="3.533881591797" Y="0.33894543457" />
                  <Point X="3.508773681641" Y="0.319870147705" />
                  <Point X="3.481993896484" Y="0.294350860596" />
                  <Point X="3.472797119141" Y="0.284226806641" />
                  <Point X="3.445453369141" Y="0.249384536743" />
                  <Point X="3.417291259766" Y="0.213499237061" />
                  <Point X="3.410854736328" Y="0.204208679199" />
                  <Point X="3.399130615234" Y="0.184928726196" />
                  <Point X="3.393843017578" Y="0.174939483643" />
                  <Point X="3.384069091797" Y="0.153475662231" />
                  <Point X="3.380005126953" Y="0.142928634644" />
                  <Point X="3.373158935547" Y="0.121427505493" />
                  <Point X="3.370376708984" Y="0.110473548889" />
                  <Point X="3.361261962891" Y="0.062880783081" />
                  <Point X="3.351874511719" Y="0.013863167763" />
                  <Point X="3.350603515625" Y="0.004969127178" />
                  <Point X="3.348584228516" Y="-0.026262786865" />
                  <Point X="3.350280029297" Y="-0.06294128418" />
                  <Point X="3.351874511719" Y="-0.076422866821" />
                  <Point X="3.360989257812" Y="-0.124015930176" />
                  <Point X="3.370376708984" Y="-0.173033554077" />
                  <Point X="3.373159179688" Y="-0.183987945557" />
                  <Point X="3.380005371094" Y="-0.20548878479" />
                  <Point X="3.384069091797" Y="-0.216035202026" />
                  <Point X="3.393843017578" Y="-0.237499176025" />
                  <Point X="3.399130371094" Y="-0.247488265991" />
                  <Point X="3.410854248047" Y="-0.266768066406" />
                  <Point X="3.417290771484" Y="-0.276058807373" />
                  <Point X="3.444634277344" Y="-0.310901062012" />
                  <Point X="3.472796630859" Y="-0.346786651611" />
                  <Point X="3.478718505859" Y="-0.353634002686" />
                  <Point X="3.501140380859" Y="-0.375806030273" />
                  <Point X="3.530176513672" Y="-0.398724853516" />
                  <Point X="3.541494384766" Y="-0.406403900146" />
                  <Point X="3.587067138672" Y="-0.43274597168" />
                  <Point X="3.634004394531" Y="-0.459876495361" />
                  <Point X="3.639495605469" Y="-0.462814910889" />
                  <Point X="3.659158447266" Y="-0.472016906738" />
                  <Point X="3.683028076172" Y="-0.481027770996" />
                  <Point X="3.691991943359" Y="-0.483912841797" />
                  <Point X="4.351609863281" Y="-0.660657104492" />
                  <Point X="4.784876953125" Y="-0.776750549316" />
                  <Point X="4.76161328125" Y="-0.931051147461" />
                  <Point X="4.727801757812" Y="-1.079219604492" />
                  <Point X="4.179517578125" Y="-1.007036437988" />
                  <Point X="3.436781982422" Y="-0.909253723145" />
                  <Point X="3.428624511719" Y="-0.908535888672" />
                  <Point X="3.400098388672" Y="-0.908042480469" />
                  <Point X="3.366721435547" Y="-0.910840881348" />
                  <Point X="3.354481201172" Y="-0.912676208496" />
                  <Point X="3.265037841797" Y="-0.932117126465" />
                  <Point X="3.172916992188" Y="-0.952139892578" />
                  <Point X="3.157874023438" Y="-0.956742492676" />
                  <Point X="3.128753662109" Y="-0.968367004395" />
                  <Point X="3.114676269531" Y="-0.975388977051" />
                  <Point X="3.086849609375" Y="-0.992281311035" />
                  <Point X="3.074124023438" Y="-1.001530273438" />
                  <Point X="3.050374023438" Y="-1.022001159668" />
                  <Point X="3.039349609375" Y="-1.03322277832" />
                  <Point X="2.985286621094" Y="-1.098243408203" />
                  <Point X="2.92960546875" Y="-1.16521081543" />
                  <Point X="2.921326171875" Y="-1.17684753418" />
                  <Point X="2.90660546875" Y="-1.201229858398" />
                  <Point X="2.9001640625" Y="-1.213975708008" />
                  <Point X="2.888820800781" Y="-1.241360839844" />
                  <Point X="2.884362792969" Y="-1.254927978516" />
                  <Point X="2.877531005859" Y="-1.282577636719" />
                  <Point X="2.875157226562" Y="-1.29666015625" />
                  <Point X="2.867408691406" Y="-1.380864868164" />
                  <Point X="2.859428222656" Y="-1.467590454102" />
                  <Point X="2.859288574219" Y="-1.483320678711" />
                  <Point X="2.861607177734" Y="-1.514589111328" />
                  <Point X="2.864065429688" Y="-1.530127441406" />
                  <Point X="2.871796875" Y="-1.561748413086" />
                  <Point X="2.876785888672" Y="-1.576668701172" />
                  <Point X="2.889157470703" Y="-1.605479980469" />
                  <Point X="2.896540039062" Y="-1.619371337891" />
                  <Point X="2.946039306641" Y="-1.696364379883" />
                  <Point X="2.997020263672" Y="-1.775661987305" />
                  <Point X="3.001741943359" Y="-1.782353393555" />
                  <Point X="3.019793701172" Y="-1.804450195312" />
                  <Point X="3.043489257812" Y="-1.828120361328" />
                  <Point X="3.052796142578" Y="-1.836277587891" />
                  <Point X="3.664925537109" Y="-2.305981445313" />
                  <Point X="4.087170654297" Y="-2.629981933594" />
                  <Point X="4.045487060547" Y="-2.697432373047" />
                  <Point X="4.001274658203" Y="-2.760251953125" />
                  <Point X="3.5099609375" Y="-2.476591796875" />
                  <Point X="2.848454589844" Y="-2.094670654297" />
                  <Point X="2.841191894531" Y="-2.090885253906" />
                  <Point X="2.815026123047" Y="-2.079513183594" />
                  <Point X="2.783119140625" Y="-2.069325927734" />
                  <Point X="2.771108398438" Y="-2.066337646484" />
                  <Point X="2.664656738281" Y="-2.047112426758" />
                  <Point X="2.555018066406" Y="-2.027311889648" />
                  <Point X="2.539359130859" Y="-2.025807495117" />
                  <Point X="2.508006835938" Y="-2.025403198242" />
                  <Point X="2.492313476562" Y="-2.026503540039" />
                  <Point X="2.460140625" Y="-2.031461303711" />
                  <Point X="2.444844482422" Y="-2.035136230469" />
                  <Point X="2.415068603516" Y="-2.044959960938" />
                  <Point X="2.400588867188" Y="-2.051108642578" />
                  <Point X="2.312153564453" Y="-2.097651367188" />
                  <Point X="2.221070800781" Y="-2.145587646484" />
                  <Point X="2.208968505859" Y="-2.153170166016" />
                  <Point X="2.186037353516" Y="-2.170063476562" />
                  <Point X="2.175208496094" Y="-2.179374267578" />
                  <Point X="2.154248779297" Y="-2.200334228516" />
                  <Point X="2.144937988281" Y="-2.211163330078" />
                  <Point X="2.128045654297" Y="-2.23409375" />
                  <Point X="2.120464111328" Y="-2.246195068359" />
                  <Point X="2.073921142578" Y="-2.334630371094" />
                  <Point X="2.025984985352" Y="-2.425713134766" />
                  <Point X="2.019836303711" Y="-2.440192382812" />
                  <Point X="2.010012329102" Y="-2.469968261719" />
                  <Point X="2.006337280273" Y="-2.485264892578" />
                  <Point X="2.001379516602" Y="-2.5174375" />
                  <Point X="2.000279174805" Y="-2.533130859375" />
                  <Point X="2.00068347168" Y="-2.564483154297" />
                  <Point X="2.002187866211" Y="-2.580142089844" />
                  <Point X="2.021412963867" Y="-2.68659375" />
                  <Point X="2.041213500977" Y="-2.796232421875" />
                  <Point X="2.043015014648" Y="-2.804221435547" />
                  <Point X="2.051236083984" Y="-2.83154296875" />
                  <Point X="2.064069580078" Y="-2.862479736328" />
                  <Point X="2.069546630859" Y="-2.873578613281" />
                  <Point X="2.462901367188" Y="-3.554888671875" />
                  <Point X="2.735893310547" Y="-4.027724609375" />
                  <Point X="2.723754394531" Y="-4.036083496094" />
                  <Point X="2.338705078125" Y="-3.534278076172" />
                  <Point X="1.833914550781" Y="-2.876422851562" />
                  <Point X="1.828654418945" Y="-2.870147216797" />
                  <Point X="1.808830932617" Y="-2.849625732422" />
                  <Point X="1.783251586914" Y="-2.828004150391" />
                  <Point X="1.773299072266" Y="-2.820647216797" />
                  <Point X="1.668308837891" Y="-2.753148193359" />
                  <Point X="1.56017565918" Y="-2.683628417969" />
                  <Point X="1.546283935547" Y="-2.676245849609" />
                  <Point X="1.517473022461" Y="-2.663874511719" />
                  <Point X="1.502553588867" Y="-2.658885742188" />
                  <Point X="1.470932617188" Y="-2.651154052734" />
                  <Point X="1.45539440918" Y="-2.648695800781" />
                  <Point X="1.424125244141" Y="-2.646376953125" />
                  <Point X="1.408394287109" Y="-2.646516357422" />
                  <Point X="1.293569580078" Y="-2.657082763672" />
                  <Point X="1.175307373047" Y="-2.667965332031" />
                  <Point X="1.161224975586" Y="-2.670339111328" />
                  <Point X="1.133575439453" Y="-2.677170898438" />
                  <Point X="1.120008422852" Y="-2.68162890625" />
                  <Point X="1.092623168945" Y="-2.692972167969" />
                  <Point X="1.079877563477" Y="-2.699413330078" />
                  <Point X="1.055495361328" Y="-2.714133789062" />
                  <Point X="1.043859008789" Y="-2.722413085938" />
                  <Point X="0.955194335938" Y="-2.796134765625" />
                  <Point X="0.863875244141" Y="-2.872063476562" />
                  <Point X="0.852653320312" Y="-2.883087890625" />
                  <Point X="0.832182128906" Y="-2.906838134766" />
                  <Point X="0.822933044434" Y="-2.919564208984" />
                  <Point X="0.806040710449" Y="-2.947390869141" />
                  <Point X="0.799018737793" Y="-2.961468261719" />
                  <Point X="0.787394348145" Y="-2.990588623047" />
                  <Point X="0.782791931152" Y="-3.005631347656" />
                  <Point X="0.756281616211" Y="-3.127599609375" />
                  <Point X="0.728977661133" Y="-3.253219238281" />
                  <Point X="0.727584777832" Y="-3.261289306641" />
                  <Point X="0.724724487305" Y="-3.289677734375" />
                  <Point X="0.7247421875" Y="-3.323170410156" />
                  <Point X="0.725554992676" Y="-3.335520019531" />
                  <Point X="0.833090881348" Y="-4.152337890625" />
                  <Point X="0.655064941406" Y="-3.487936523438" />
                  <Point X="0.652606262207" Y="-3.480124511719" />
                  <Point X="0.642145019531" Y="-3.453580322266" />
                  <Point X="0.62678692627" Y="-3.423815673828" />
                  <Point X="0.620407409668" Y="-3.413210205078" />
                  <Point X="0.539751098633" Y="-3.296999511719" />
                  <Point X="0.456679992676" Y="-3.177309814453" />
                  <Point X="0.446670684814" Y="-3.165172851562" />
                  <Point X="0.424786712646" Y="-3.142717529297" />
                  <Point X="0.412912322998" Y="-3.132399169922" />
                  <Point X="0.386657196045" Y="-3.113155273438" />
                  <Point X="0.373242797852" Y="-3.104937988281" />
                  <Point X="0.345241485596" Y="-3.090829589844" />
                  <Point X="0.330654602051" Y="-3.084938476562" />
                  <Point X="0.205843322754" Y="-3.046201660156" />
                  <Point X="0.077295654297" Y="-3.006305175781" />
                  <Point X="0.063376529694" Y="-3.003109375" />
                  <Point X="0.035217094421" Y="-2.998840087891" />
                  <Point X="0.020976791382" Y="-2.997766601562" />
                  <Point X="-0.008664855957" Y="-2.997766601562" />
                  <Point X="-0.022905158997" Y="-2.998840087891" />
                  <Point X="-0.051064590454" Y="-3.003109375" />
                  <Point X="-0.064983573914" Y="-3.006305175781" />
                  <Point X="-0.189794708252" Y="-3.045041748047" />
                  <Point X="-0.318342529297" Y="-3.084938476562" />
                  <Point X="-0.332929412842" Y="-3.090829589844" />
                  <Point X="-0.360930847168" Y="-3.104937988281" />
                  <Point X="-0.374345275879" Y="-3.113155273438" />
                  <Point X="-0.400600524902" Y="-3.132399169922" />
                  <Point X="-0.412474761963" Y="-3.142717285156" />
                  <Point X="-0.434358581543" Y="-3.165172363281" />
                  <Point X="-0.444368041992" Y="-3.177309570312" />
                  <Point X="-0.525024353027" Y="-3.293520019531" />
                  <Point X="-0.60809552002" Y="-3.413209960938" />
                  <Point X="-0.612470336914" Y="-3.420132324219" />
                  <Point X="-0.625976745605" Y="-3.445265136719" />
                  <Point X="-0.638777832031" Y="-3.476215820312" />
                  <Point X="-0.642752990723" Y="-3.487936523438" />
                  <Point X="-0.845997375488" Y="-4.246455566406" />
                  <Point X="-0.985425170898" Y="-4.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.770945303722" Y="3.820796782216" />
                  <Point X="1.943392230377" Y="4.400255682441" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.723263869936" Y="3.738210095653" />
                  <Point X="1.573290768848" Y="4.543429929774" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.956307695036" Y="2.758849928443" />
                  <Point X="3.931821104664" Y="2.775995623607" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.67558243615" Y="3.65562340909" />
                  <Point X="1.288861242362" Y="4.626616042376" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.10227106577" Y="2.54067169002" />
                  <Point X="3.841043558354" Y="2.7235851599" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.627901002364" Y="3.573036722528" />
                  <Point X="1.036072250774" Y="4.68764721393" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.080319814856" Y="2.440068535451" />
                  <Point X="3.750266012045" Y="2.671174696194" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.580219568578" Y="3.490450035965" />
                  <Point X="0.793520653641" Y="4.741510084715" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.00129373721" Y="2.379429604801" />
                  <Point X="3.659488465736" Y="2.618764232488" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.532538134792" Y="3.407863349402" />
                  <Point X="0.598764712839" Y="4.761906076643" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.922267659565" Y="2.318790674151" />
                  <Point X="3.568710919426" Y="2.566353768781" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.484856700733" Y="3.325276663031" />
                  <Point X="0.404008219511" Y="4.782302455454" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.84324158192" Y="2.258151743501" />
                  <Point X="3.477933373117" Y="2.513943305075" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.437175264296" Y="3.242689978324" />
                  <Point X="0.3554931227" Y="4.700299506026" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.764215504274" Y="2.197512812851" />
                  <Point X="3.387155824717" Y="2.461532842832" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.389493827859" Y="3.160103293617" />
                  <Point X="0.329327314848" Y="4.602647415995" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.685189405134" Y="2.136873897252" />
                  <Point X="3.296378247169" Y="2.409122400999" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.341812391423" Y="3.077516608911" />
                  <Point X="0.303161553889" Y="4.50499529313" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.60616329104" Y="2.076234992124" />
                  <Point X="3.205600669622" Y="2.356711959166" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.294130954986" Y="2.994929924204" />
                  <Point X="0.276995792929" Y="4.407343170264" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.527137176946" Y="2.015596086996" />
                  <Point X="3.114823092074" Y="2.304301517333" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.24644951855" Y="2.912343239497" />
                  <Point X="0.250830031969" Y="4.309691047399" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.351533547631" Y="4.731470566578" />
                  <Point X="-0.416004169716" Y="4.776613382155" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.715338586167" Y="1.067634917415" />
                  <Point X="4.66114044631" Y="1.1055848635" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.448111062851" Y="1.954957181868" />
                  <Point X="3.024045514526" Y="2.2518910755" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.198768082113" Y="2.82975655479" />
                  <Point X="0.218536261462" Y="4.216329803013" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.313281723615" Y="4.588712765119" />
                  <Point X="-0.557912681613" Y="4.760005205989" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.749373582355" Y="0.927829770589" />
                  <Point X="4.521725683559" Y="1.087230545384" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.369084948757" Y="1.89431827674" />
                  <Point X="2.933267936978" Y="2.199480633667" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.151086645676" Y="2.747169870084" />
                  <Point X="0.160248426067" Y="4.14116979881" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.275029904198" Y="4.445954966881" />
                  <Point X="-0.699821198812" Y="4.743397033535" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.770978385287" Y="0.796728338782" />
                  <Point X="4.382310920809" Y="1.068876227267" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.290058834663" Y="1.833679371612" />
                  <Point X="2.842490359431" Y="2.147070191834" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.10340520924" Y="2.664583185377" />
                  <Point X="0.063443880896" Y="4.09297948514" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.23677808478" Y="4.303197168642" />
                  <Point X="-0.841729716104" Y="4.726788861147" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.74028972725" Y="0.702243182545" />
                  <Point X="4.242896158058" Y="1.050521909151" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.211032720569" Y="1.773040466484" />
                  <Point X="2.751712781883" Y="2.094659750001" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.056821749161" Y="2.581227689347" />
                  <Point X="-0.974900138571" Y="4.704062208893" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.620501704154" Y="0.670146073372" />
                  <Point X="4.103481375911" Y="1.032167604615" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.136895504933" Y="1.708978317801" />
                  <Point X="2.660935204335" Y="2.042249308168" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.026778731715" Y="2.486290450702" />
                  <Point X="-1.09317141174" Y="4.670903059987" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.500713681058" Y="0.638048964199" />
                  <Point X="3.964066589931" Y="1.013813302764" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.078400156542" Y="1.633963615762" />
                  <Point X="2.557838419648" Y="1.998464868039" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.012909853218" Y="2.380027958039" />
                  <Point X="-1.211442684908" Y="4.637743911081" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.380925657961" Y="0.605951855026" />
                  <Point X="3.824651803951" Y="0.995459000913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.032754165788" Y="1.549951696645" />
                  <Point X="2.425190877415" Y="1.975372091103" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.035485103172" Y="2.248247011912" />
                  <Point X="-1.3297139151" Y="4.604584732082" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.261137605987" Y="0.573854766073" />
                  <Point X="3.685237017971" Y="0.977104699061" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.00593070328" Y="1.452760101362" />
                  <Point X="-1.447985005186" Y="4.571425454981" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.141349534374" Y="0.541757690872" />
                  <Point X="3.545822231992" Y="0.95875039721" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.008298375699" Y="1.335128653354" />
                  <Point X="-1.466895847394" Y="4.468693383316" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.021561462761" Y="0.509660615671" />
                  <Point X="3.377237087291" Y="0.960821400427" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.055458896096" Y="1.186132915534" />
                  <Point X="-1.469127705871" Y="4.354282561513" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.901773391148" Y="0.477563540469" />
                  <Point X="-1.498165372387" Y="4.258641368568" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.781985319535" Y="0.445466465268" />
                  <Point X="-1.538744975714" Y="4.171081926783" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.664611702594" Y="0.411678770705" />
                  <Point X="-1.609990893113" Y="4.104995269279" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.571277071796" Y="0.361058796833" />
                  <Point X="-1.710489952151" Y="4.059391882068" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.490009620811" Y="0.301989292692" />
                  <Point X="-1.832462176504" Y="4.02882416708" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.429160103153" Y="0.22862299772" />
                  <Point X="-2.274805892868" Y="4.222582985825" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.77671057132" Y="-0.830915584141" />
                  <Point X="4.646341379439" Y="-0.739630093235" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.381261236284" Y="0.146188559441" />
                  <Point X="-2.367157288794" Y="4.171274543485" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.760577000993" Y="-0.935592322512" />
                  <Point X="4.378044627014" Y="-0.667740270643" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.358110537621" Y="0.046425267228" />
                  <Point X="-2.459508670197" Y="4.119966090975" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.737758316016" Y="-1.035588093211" />
                  <Point X="4.10974756965" Y="-0.595850234529" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.350424976093" Y="-0.064166830587" />
                  <Point X="-2.5518600516" Y="4.068657638466" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.612825279481" Y="-1.06408262519" />
                  <Point X="3.841450478957" Y="-0.523960175079" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.378105660838" Y="-0.199522640641" />
                  <Point X="-2.644211433003" Y="4.017349185956" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.408845635611" Y="-1.037228126844" />
                  <Point X="-2.723870933094" Y="3.957153782477" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.204865991741" Y="-1.010373628497" />
                  <Point X="-2.802931254561" Y="3.896538829609" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.000886572828" Y="-0.983519287667" />
                  <Point X="-2.881991576028" Y="3.835923876741" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.796907185837" Y="-0.956664969189" />
                  <Point X="-2.961051937289" Y="3.775308951737" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.592927798846" Y="-0.92981065071" />
                  <Point X="-3.04011237853" Y="3.714694082736" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.396627755287" Y="-0.908333466392" />
                  <Point X="-2.974394427749" Y="3.552704292271" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.26498370208" Y="-0.932128893908" />
                  <Point X="-2.86199961488" Y="3.358031011112" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.142992231041" Y="-0.962683132222" />
                  <Point X="-2.765631215538" Y="3.174579545515" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.055620935241" Y="-1.017478678212" />
                  <Point X="-2.749022691518" Y="3.046976545866" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.992664744901" Y="-1.089369865091" />
                  <Point X="-2.763143605493" Y="2.940890530345" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.93171905377" Y="-1.162668818672" />
                  <Point X="-2.812850086518" Y="2.859721797124" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.88688575769" Y="-1.247249792726" />
                  <Point X="-2.880816271356" Y="2.791338646159" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.87011280096" Y="-1.351478827918" />
                  <Point X="-2.968831868011" Y="2.736994244485" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.860086921939" Y="-1.460432217783" />
                  <Point X="-3.121990688885" Y="2.728263619472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.883804005761" Y="-1.593012684592" />
                  <Point X="-3.722706929198" Y="3.032916073331" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.019330010474" Y="-1.803882600648" />
                  <Point X="-3.784522060112" Y="2.96022590804" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.070959060808" Y="-2.656214775024" />
                  <Point X="-3.842930424066" Y="2.885150298843" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.018651359649" Y="-2.735562114299" />
                  <Point X="-3.901338779697" Y="2.810074683818" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.35730331543" Y="-2.388454814288" />
                  <Point X="-3.959747135329" Y="2.734999068794" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.717928494576" Y="-2.056733330917" />
                  <Point X="-4.015343287126" Y="2.657954327445" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.50759792833" Y="-2.025431868849" />
                  <Point X="-4.063396178218" Y="2.575627738088" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.388058678588" Y="-2.057703170999" />
                  <Point X="-4.11144906931" Y="2.493301148731" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.293502088994" Y="-2.107467520111" />
                  <Point X="-4.159501960403" Y="2.410974559375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.201257563491" Y="-2.158850793927" />
                  <Point X="-4.20755506799" Y="2.328648121609" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.133164545338" Y="-2.227145135249" />
                  <Point X="-3.490219482146" Y="1.710390751043" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.086609871555" Y="-2.31052078766" />
                  <Point X="-3.422494243406" Y="1.546995442418" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.04200963108" Y="-2.395264949006" />
                  <Point X="-3.435562254336" Y="1.440172176248" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.006198802351" Y="-2.486163522713" />
                  <Point X="-3.473921294727" Y="1.351057879556" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.006154556497" Y="-2.602106127364" />
                  <Point X="-3.534127174489" Y="1.277240904478" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.030131272843" Y="-2.734868390823" />
                  <Point X="-3.630310604958" Y="1.228615681611" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.074258875808" Y="-2.881740456995" />
                  <Point X="-3.761761548841" Y="1.20468503749" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.186653542424" Y="-3.076413635747" />
                  <Point X="-3.963584825363" Y="1.230029631164" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.299048209041" Y="-3.271086814499" />
                  <Point X="1.948295918667" Y="-3.025487416734" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.407223077394" Y="-2.646624134554" />
                  <Point X="-4.16756443525" Y="1.256884105716" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.411442875657" Y="-3.46575999325" />
                  <Point X="2.140618280184" Y="-3.276126569968" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.260834162743" Y="-2.660095098938" />
                  <Point X="-4.371544001938" Y="1.28373855002" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.52383748805" Y="-3.660433134034" />
                  <Point X="2.332940641702" Y="-3.526765723201" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.124059229112" Y="-2.680297845304" />
                  <Point X="-4.575523485078" Y="1.310592935822" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.636232054653" Y="-3.855106242756" />
                  <Point X="2.525263133712" Y="-3.777404967806" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.032302013405" Y="-2.732022337113" />
                  <Point X="-4.652482252562" Y="1.248506459013" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.726432849772" Y="-4.034239105383" />
                  <Point X="2.717585629754" Y="-4.028044215235" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.956585114878" Y="-2.794978379927" />
                  <Point X="-4.678896900283" Y="1.151028608534" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.880868201252" Y="-2.857934412168" />
                  <Point X="-3.31969391306" Y="0.083330844991" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.58746626949" Y="0.270827067488" />
                  <Point X="-4.705311499137" Y="1.053550723838" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.817064188331" Y="-2.929231947286" />
                  <Point X="-3.294917850186" Y="-0.049991126933" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.855763147804" Y="0.342716978229" />
                  <Point X="-4.731563412627" Y="0.955958925624" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.779853520589" Y="-3.019150343163" />
                  <Point X="-3.320539654543" Y="-0.148024132312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.124060026117" Y="0.41460688897" />
                  <Point X="-4.747113471744" Y="0.850873608305" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.757975809364" Y="-3.119804990777" />
                  <Point X="-3.37182049974" Y="-0.228090483871" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.392356878443" Y="0.486496781514" />
                  <Point X="-4.762663530861" Y="0.745788290986" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.736098098066" Y="-3.220459638339" />
                  <Point X="-3.452557455128" Y="-0.287531445029" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.66065370363" Y="0.558386655055" />
                  <Point X="-4.778213558446" Y="0.640702951588" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.725108647051" Y="-3.32873832783" />
                  <Point X="-3.562319326384" Y="-0.3266489413" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.741435412916" Y="-3.456144038296" />
                  <Point X="0.563952432182" Y="-3.331869117282" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.116277388949" Y="-3.018403677342" />
                  <Point X="-3.682107381396" Y="-0.358746028125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.758253995521" Y="-3.583894122551" />
                  <Point X="0.662883243603" Y="-3.517114803133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.066123322765" Y="-3.006658909957" />
                  <Point X="-3.801895436409" Y="-0.39084311495" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.775072578126" Y="-3.711644206805" />
                  <Point X="0.701135118782" Y="-3.659872640416" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.180883997458" Y="-3.042276206379" />
                  <Point X="-3.921683491421" Y="-0.422940201776" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.791891160731" Y="-3.839394291059" />
                  <Point X="0.739386993961" Y="-3.802630477699" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.295644319731" Y="-3.077893749569" />
                  <Point X="-4.041471546433" Y="-0.455037288601" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.808709743336" Y="-3.967144375314" />
                  <Point X="0.77763886914" Y="-3.945388314982" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.392211408948" Y="-3.126250331689" />
                  <Point X="-2.949808554104" Y="-1.335401530947" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.279166062745" Y="-1.10478292063" />
                  <Point X="-4.161259601445" Y="-0.487134375426" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.825528325941" Y="-4.094894459568" />
                  <Point X="0.815890744319" Y="-4.088146152266" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.457629901968" Y="-3.19641739567" />
                  <Point X="-2.961729470439" Y="-1.443028001399" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.418580775556" Y="-1.123137273715" />
                  <Point X="-4.281047649585" Y="-0.519231467064" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.511797401255" Y="-3.274462490275" />
                  <Point X="-3.005572883762" Y="-1.528302098822" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.557995488366" Y="-1.141491626801" />
                  <Point X="-4.400835684909" Y="-0.551328567675" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.565964867698" Y="-3.352507607879" />
                  <Point X="-3.075787882601" Y="-1.595110613271" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.697410201176" Y="-1.159845979886" />
                  <Point X="-4.520623720234" Y="-0.583425668285" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.618634063278" Y="-3.431601826035" />
                  <Point X="-3.154814028125" Y="-1.655749496392" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.836824913986" Y="-1.178200332971" />
                  <Point X="-4.640411755559" Y="-0.615522768896" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.652398322394" Y="-3.523933423212" />
                  <Point X="-3.233840173649" Y="-1.716388379513" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.976239626797" Y="-1.196554686056" />
                  <Point X="-4.760199790883" Y="-0.647619869507" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.67856410297" Y="-3.621585532342" />
                  <Point X="-2.392730986726" Y="-2.421312958586" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.457426105015" Y="-2.376012949075" />
                  <Point X="-3.312866319173" Y="-1.777027262634" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.11565438697" Y="-1.214909005978" />
                  <Point X="-4.772059483114" Y="-0.755289209538" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.704729883546" Y="-3.719237641472" />
                  <Point X="-2.310833464783" Y="-2.594631806744" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.643853198893" Y="-2.361448878548" />
                  <Point X="-3.391892464697" Y="-1.837666145754" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.255069245063" Y="-1.233263257335" />
                  <Point X="-4.753626825493" Y="-0.884169481286" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.730895664122" Y="-3.816889750601" />
                  <Point X="-2.329996438695" Y="-2.697187333889" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.740307385819" Y="-2.409884515702" />
                  <Point X="-3.470918610221" Y="-1.898305028875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.394484103157" Y="-1.251617508691" />
                  <Point X="-4.729864693296" Y="-1.016781491307" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.757061444698" Y="-3.914541859731" />
                  <Point X="-2.374788796072" Y="-2.781796973531" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.831084949757" Y="-2.462294967065" />
                  <Point X="-3.549944755745" Y="-1.958943911996" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.53389896125" Y="-1.269971760048" />
                  <Point X="-4.693789131715" Y="-1.158015457403" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.783227225274" Y="-4.012193968861" />
                  <Point X="-2.422470234375" Y="-2.864383656931" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.921862513695" Y="-2.514705418428" />
                  <Point X="-3.628970901269" Y="-2.019582795117" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.80939300585" Y="-4.109846077991" />
                  <Point X="-2.470151672677" Y="-2.946970340332" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.012640077633" Y="-2.56711586979" />
                  <Point X="-3.707997033016" Y="-2.080221687885" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.835558786427" Y="-4.207498187121" />
                  <Point X="-2.517833110979" Y="-3.029557023732" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.103417641571" Y="-2.619526321153" />
                  <Point X="-3.787023118781" Y="-2.140860612849" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.861724592546" Y="-4.305150278365" />
                  <Point X="-1.454530191872" Y="-3.890063329024" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.556990969026" Y="-3.81831952049" />
                  <Point X="-2.565514549282" Y="-3.112143707132" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.194195205508" Y="-2.671936772516" />
                  <Point X="-3.866049204547" Y="-2.201499537813" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.88789041562" Y="-4.402802357737" />
                  <Point X="-1.183361750061" Y="-4.195911102037" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.766379694423" Y="-3.787677542483" />
                  <Point X="-2.613195987584" Y="-3.194730390533" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.284972769446" Y="-2.724347223879" />
                  <Point X="-3.945075290312" Y="-2.262138462777" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.914056238693" Y="-4.50045443711" />
                  <Point X="-1.156560433563" Y="-4.330651171815" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.948802295589" Y="-3.775917447939" />
                  <Point X="-2.660877425886" Y="-3.277317073933" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.375750349047" Y="-2.776757664274" />
                  <Point X="-4.024101376078" Y="-2.322777387741" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.940222061767" Y="-4.598106516483" />
                  <Point X="-1.129758808481" Y="-4.465391457666" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.073584472795" Y="-3.804517612757" />
                  <Point X="-2.708558864292" Y="-3.359903757261" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.466527953774" Y="-2.829168087076" />
                  <Point X="-4.103127461843" Y="-2.383416312705" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.966387884841" Y="-4.695758595856" />
                  <Point X="-1.12220003432" Y="-4.586657754245" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.160753159376" Y="-3.85945502725" />
                  <Point X="-2.756240317034" Y="-3.442490430551" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.5573055585" Y="-2.881578509879" />
                  <Point X="-4.179930173556" Y="-2.445612060941" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.047853281243" Y="-4.754689497124" />
                  <Point X="-1.13617957312" Y="-4.692842761729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.245505194117" Y="-3.916084599578" />
                  <Point X="-2.803921769775" Y="-3.525077103841" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.648083163227" Y="-2.933988932681" />
                  <Point X="-4.061182471432" Y="-2.644733683046" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.330257079599" Y="-3.972714276418" />
                  <Point X="-2.851603222517" Y="-3.607663777131" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.738860767953" Y="-2.986399355483" />
                  <Point X="-3.906000202283" Y="-2.869367063633" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.405544556506" Y="-4.035971003488" />
                  <Point X="-2.899284675258" Y="-3.690250450421" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.463812242405" Y="-4.111145116519" />
                  <Point X="-2.946966127999" Y="-3.772837123711" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="0.001626220703" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.668875854492" Y="-4.273583007813" />
                  <Point X="0.471539031982" Y="-3.537112304688" />
                  <Point X="0.464318664551" Y="-3.521544189453" />
                  <Point X="0.383662200928" Y="-3.405333496094" />
                  <Point X="0.300591033936" Y="-3.285643798828" />
                  <Point X="0.274335754395" Y="-3.266399902344" />
                  <Point X="0.149524475098" Y="-3.227663085938" />
                  <Point X="0.02097677803" Y="-3.187766601562" />
                  <Point X="-0.008664908409" Y="-3.187766601562" />
                  <Point X="-0.13347618103" Y="-3.226503173828" />
                  <Point X="-0.262023895264" Y="-3.266399902344" />
                  <Point X="-0.288279144287" Y="-3.285643798828" />
                  <Point X="-0.368935638428" Y="-3.401854248047" />
                  <Point X="-0.45200680542" Y="-3.521544189453" />
                  <Point X="-0.459227142334" Y="-3.537112304688" />
                  <Point X="-0.662471557617" Y="-4.295631347656" />
                  <Point X="-0.84774395752" Y="-4.987077148438" />
                  <Point X="-0.949641418457" Y="-4.967298339844" />
                  <Point X="-1.100246948242" Y="-4.938065429688" />
                  <Point X="-1.244455322266" Y="-4.900961425781" />
                  <Point X="-1.351589599609" Y="-4.873396972656" />
                  <Point X="-1.332905029297" Y="-4.731473144531" />
                  <Point X="-1.309150146484" Y="-4.551038574219" />
                  <Point X="-1.309683227539" Y="-4.5347578125" />
                  <Point X="-1.339500610352" Y="-4.384855957031" />
                  <Point X="-1.370210693359" Y="-4.230466308594" />
                  <Point X="-1.386282714844" Y="-4.20262890625" />
                  <Point X="-1.501193115234" Y="-4.101854980469" />
                  <Point X="-1.619543457031" Y="-3.998063964844" />
                  <Point X="-1.649240722656" Y="-3.985762939453" />
                  <Point X="-1.801752319336" Y="-3.975766845703" />
                  <Point X="-1.958829833984" Y="-3.965471435547" />
                  <Point X="-1.989878540039" Y="-3.973791015625" />
                  <Point X="-2.116959472656" Y="-4.058703857422" />
                  <Point X="-2.247844970703" Y="-4.146159179688" />
                  <Point X="-2.259734130859" Y="-4.157294433594" />
                  <Point X="-2.373840087891" Y="-4.306" />
                  <Point X="-2.457095214844" Y="-4.414500488281" />
                  <Point X="-2.635079101562" Y="-4.304297363281" />
                  <Point X="-2.855832275391" Y="-4.167612304688" />
                  <Point X="-3.055521728516" Y="-4.013858154297" />
                  <Point X="-3.228581054688" Y="-3.880608154297" />
                  <Point X="-2.87276171875" Y="-3.264311279297" />
                  <Point X="-2.506033691406" Y="-2.629119628906" />
                  <Point X="-2.499762695312" Y="-2.597593261719" />
                  <Point X="-2.513979736328" Y="-2.568764404297" />
                  <Point X="-2.531328369141" Y="-2.551415771484" />
                  <Point X="-2.560157226562" Y="-2.537198730469" />
                  <Point X="-2.59168359375" Y="-2.543469726562" />
                  <Point X="-3.245891357422" Y="-2.921176757812" />
                  <Point X="-3.842959472656" Y="-3.265894042969" />
                  <Point X="-3.987295410156" Y="-3.076266357422" />
                  <Point X="-4.161703613281" Y="-2.847129394531" />
                  <Point X="-4.304863769531" Y="-2.607071533203" />
                  <Point X="-4.431020019531" Y="-2.395527099609" />
                  <Point X="-3.805441894531" Y="-1.915504150391" />
                  <Point X="-3.163786865234" Y="-1.423144897461" />
                  <Point X="-3.145822021484" Y="-1.396013793945" />
                  <Point X="-3.138117431641" Y="-1.366266479492" />
                  <Point X="-3.140326171875" Y="-1.334595703125" />
                  <Point X="-3.161158935547" Y="-1.310639282227" />
                  <Point X="-3.187641601562" Y="-1.295052734375" />
                  <Point X="-3.219529296875" Y="-1.288571044922" />
                  <Point X="-4.045404541016" Y="-1.397299682617" />
                  <Point X="-4.803283203125" Y="-1.497076293945" />
                  <Point X="-4.859180664062" Y="-1.278240356445" />
                  <Point X="-4.927393066406" Y="-1.011191650391" />
                  <Point X="-4.96526953125" Y="-0.746362182617" />
                  <Point X="-4.998396484375" Y="-0.514742126465" />
                  <Point X="-4.288405761719" Y="-0.324500640869" />
                  <Point X="-3.557463867188" Y="-0.128645431519" />
                  <Point X="-3.528223876953" Y="-0.111936065674" />
                  <Point X="-3.50232421875" Y="-0.090645133972" />
                  <Point X="-3.490341552734" Y="-0.061223834991" />
                  <Point X="-3.483400878906" Y="-0.031279666901" />
                  <Point X="-3.490205078125" Y="-0.001775264859" />
                  <Point X="-3.502324462891" Y="0.028085353851" />
                  <Point X="-3.527814697266" Y="0.049091815948" />
                  <Point X="-3.557463867188" Y="0.066085334778" />
                  <Point X="-4.310288085937" Y="0.267804016113" />
                  <Point X="-4.998186523438" Y="0.452125793457" />
                  <Point X="-4.961606933594" Y="0.699329528809" />
                  <Point X="-4.917645019531" Y="0.996418151855" />
                  <Point X="-4.841400390625" Y="1.277784545898" />
                  <Point X="-4.773516113281" Y="1.528298583984" />
                  <Point X="-4.277223144531" Y="1.462960449219" />
                  <Point X="-3.753266357422" Y="1.393980102539" />
                  <Point X="-3.731704833984" Y="1.395866577148" />
                  <Point X="-3.701444824219" Y="1.405407592773" />
                  <Point X="-3.670278808594" Y="1.415234130859" />
                  <Point X="-3.651534423828" Y="1.426056152344" />
                  <Point X="-3.639119873047" Y="1.443786132813" />
                  <Point X="-3.626977783203" Y="1.473099365234" />
                  <Point X="-3.614472412109" Y="1.503290283203" />
                  <Point X="-3.610714111328" Y="1.524605224609" />
                  <Point X="-3.616315917969" Y="1.54551159668" />
                  <Point X="-3.630966552734" Y="1.573655273438" />
                  <Point X="-3.646055664062" Y="1.602641235352" />
                  <Point X="-3.659968261719" Y="1.619221679688" />
                  <Point X="-4.091790039063" Y="1.95057019043" />
                  <Point X="-4.47610546875" Y="2.245466308594" />
                  <Point X="-4.330833496094" Y="2.494352050781" />
                  <Point X="-4.160014648438" Y="2.787006591797" />
                  <Point X="-3.958045410156" Y="3.046609375" />
                  <Point X="-3.774671142578" Y="3.282310791016" />
                  <Point X="-3.480956787109" Y="3.112734619141" />
                  <Point X="-3.159156982422" Y="2.926943359375" />
                  <Point X="-3.138514892578" Y="2.920434814453" />
                  <Point X="-3.09637109375" Y="2.916747802734" />
                  <Point X="-3.052965576172" Y="2.912950195312" />
                  <Point X="-3.031506835938" Y="2.915775390625" />
                  <Point X="-3.013252441406" Y="2.927404541016" />
                  <Point X="-2.983338378906" Y="2.957318603516" />
                  <Point X="-2.952528808594" Y="2.988127929688" />
                  <Point X="-2.940899414062" Y="3.006382324219" />
                  <Point X="-2.93807421875" Y="3.027841064453" />
                  <Point X="-2.941761474609" Y="3.069985107422" />
                  <Point X="-2.945558837891" Y="3.113390625" />
                  <Point X="-2.952067382812" Y="3.134032958984" />
                  <Point X="-3.143420898438" Y="3.465466552734" />
                  <Point X="-3.307278564453" Y="3.749276611328" />
                  <Point X="-3.050388427734" Y="3.946231689453" />
                  <Point X="-2.752876220703" Y="4.174331542969" />
                  <Point X="-2.434781005859" Y="4.351058105469" />
                  <Point X="-2.141548583984" Y="4.513971679688" />
                  <Point X="-2.066339599609" Y="4.415957519531" />
                  <Point X="-1.967826904297" Y="4.287573242188" />
                  <Point X="-1.951246826172" Y="4.273660644531" />
                  <Point X="-1.904340942383" Y="4.249242675781" />
                  <Point X="-1.856030761719" Y="4.22409375" />
                  <Point X="-1.835124023438" Y="4.218491699219" />
                  <Point X="-1.81380859375" Y="4.222250488281" />
                  <Point X="-1.76495300293" Y="4.242487304688" />
                  <Point X="-1.714634765625" Y="4.263330078125" />
                  <Point X="-1.696905273438" Y="4.275744140625" />
                  <Point X="-1.686083496094" Y="4.29448828125" />
                  <Point X="-1.670181762695" Y="4.344921875" />
                  <Point X="-1.653804077148" Y="4.396864746094" />
                  <Point X="-1.651917602539" Y="4.418426757812" />
                  <Point X="-1.673672363281" Y="4.583670410156" />
                  <Point X="-1.689137573242" Y="4.701141113281" />
                  <Point X="-1.35324230957" Y="4.795314453125" />
                  <Point X="-0.968094177246" Y="4.903296386719" />
                  <Point X="-0.582466918945" Y="4.948428222656" />
                  <Point X="-0.224199615479" Y="4.990358398438" />
                  <Point X="-0.137337646484" Y="4.666184570312" />
                  <Point X="-0.042140380859" Y="4.310903320312" />
                  <Point X="-0.024282115936" Y="4.284176757813" />
                  <Point X="0.006155934334" Y="4.273844238281" />
                  <Point X="0.036594036102" Y="4.284176757813" />
                  <Point X="0.054452251434" Y="4.310903320312" />
                  <Point X="0.152499572754" Y="4.676820800781" />
                  <Point X="0.236648422241" Y="4.990868652344" />
                  <Point X="0.52391418457" Y="4.960784179688" />
                  <Point X="0.860205810547" Y="4.925565429688" />
                  <Point X="1.179245849609" Y="4.848539550781" />
                  <Point X="1.508455932617" Y="4.769058105469" />
                  <Point X="1.716340942383" Y="4.693656738281" />
                  <Point X="1.931033691406" Y="4.615786132813" />
                  <Point X="2.131835205078" Y="4.521877929688" />
                  <Point X="2.338699951172" Y="4.425133789062" />
                  <Point X="2.532683349609" Y="4.312118652344" />
                  <Point X="2.732523681641" Y="4.19569140625" />
                  <Point X="2.915474853516" Y="4.065586425781" />
                  <Point X="3.068740234375" Y="3.956592773438" />
                  <Point X="2.654313720703" Y="3.238784667969" />
                  <Point X="2.229853515625" Y="2.503597900391" />
                  <Point X="2.224852050781" Y="2.491514892578" />
                  <Point X="2.214275634766" Y="2.451963867188" />
                  <Point X="2.203382324219" Y="2.411228515625" />
                  <Point X="2.202044677734" Y="2.392325927734" />
                  <Point X="2.206168701172" Y="2.358125488281" />
                  <Point X="2.210416015625" Y="2.322901123047" />
                  <Point X="2.218682128906" Y="2.300812255859" />
                  <Point X="2.239844238281" Y="2.269624755859" />
                  <Point X="2.261639892578" Y="2.237503662109" />
                  <Point X="2.274939941406" Y="2.224203613281" />
                  <Point X="2.306127441406" Y="2.203041503906" />
                  <Point X="2.338248535156" Y="2.18124609375" />
                  <Point X="2.360336914062" Y="2.172980224609" />
                  <Point X="2.394537353516" Y="2.168856201172" />
                  <Point X="2.42976171875" Y="2.164608642578" />
                  <Point X="2.448664306641" Y="2.165946289062" />
                  <Point X="2.488215576172" Y="2.176522949219" />
                  <Point X="2.528950683594" Y="2.187416015625" />
                  <Point X="2.541033935547" Y="2.192417480469" />
                  <Point X="3.29823046875" Y="2.629584960938" />
                  <Point X="3.994248046875" Y="3.031430908203" />
                  <Point X="4.084053222656" Y="2.906621826172" />
                  <Point X="4.202591796875" Y="2.741880126953" />
                  <Point X="4.304584472656" Y="2.573336425781" />
                  <Point X="4.387513183594" Y="2.436296142578" />
                  <Point X="3.847457763672" Y="2.021896972656" />
                  <Point X="3.288616210938" Y="1.593082641602" />
                  <Point X="3.279371337891" Y="1.583833374023" />
                  <Point X="3.25090625" Y="1.546698486328" />
                  <Point X="3.221589111328" Y="1.508451904297" />
                  <Point X="3.213119628906" Y="1.4915" />
                  <Point X="3.202516357422" Y="1.453585205078" />
                  <Point X="3.191595703125" Y="1.414535522461" />
                  <Point X="3.190779541016" Y="1.390965332031" />
                  <Point X="3.199483642578" Y="1.348780273438" />
                  <Point X="3.208448486328" Y="1.305332519531" />
                  <Point X="3.215646484375" Y="1.287954833984" />
                  <Point X="3.239320800781" Y="1.251970825195" />
                  <Point X="3.263704101562" Y="1.214909423828" />
                  <Point X="3.280947998047" Y="1.198819824219" />
                  <Point X="3.315255371094" Y="1.17950769043" />
                  <Point X="3.350590087891" Y="1.159617431641" />
                  <Point X="3.368565429688" Y="1.153619628906" />
                  <Point X="3.414951416016" Y="1.147489135742" />
                  <Point X="3.462726074219" Y="1.141175048828" />
                  <Point X="3.475803710938" Y="1.14117175293" />
                  <Point X="4.19508984375" Y="1.235867553711" />
                  <Point X="4.848975097656" Y="1.32195324707" />
                  <Point X="4.888276855469" Y="1.160514160156" />
                  <Point X="4.939188476562" Y="0.951385620117" />
                  <Point X="4.971329101563" Y="0.744950134277" />
                  <Point X="4.997858398438" Y="0.574556152344" />
                  <Point X="4.381612304687" Y="0.409433410645" />
                  <Point X="3.741167724609" Y="0.237826919556" />
                  <Point X="3.729086914062" Y="0.232819412231" />
                  <Point X="3.683514160156" Y="0.206477432251" />
                  <Point X="3.636576904297" Y="0.179346878052" />
                  <Point X="3.622264892578" Y="0.166926422119" />
                  <Point X="3.594921142578" Y="0.132084182739" />
                  <Point X="3.566759033203" Y="0.096198890686" />
                  <Point X="3.556985107422" Y="0.07473513031" />
                  <Point X="3.547870361328" Y="0.027142347336" />
                  <Point X="3.538482910156" Y="-0.021875303268" />
                  <Point X="3.538482910156" Y="-0.040684635162" />
                  <Point X="3.54759765625" Y="-0.088277565002" />
                  <Point X="3.556985107422" Y="-0.137295227051" />
                  <Point X="3.566759033203" Y="-0.158759140015" />
                  <Point X="3.594102539062" Y="-0.193601379395" />
                  <Point X="3.622264892578" Y="-0.229486831665" />
                  <Point X="3.636576904297" Y="-0.241906967163" />
                  <Point X="3.682149658203" Y="-0.268248962402" />
                  <Point X="3.729086914062" Y="-0.295379486084" />
                  <Point X="3.741167724609" Y="-0.300387023926" />
                  <Point X="4.400785644531" Y="-0.477131103516" />
                  <Point X="4.998067871094" Y="-0.637172363281" />
                  <Point X="4.976858398438" Y="-0.777850891113" />
                  <Point X="4.948431640625" Y="-0.966399108887" />
                  <Point X="4.90725390625" Y="-1.146845947266" />
                  <Point X="4.874545410156" Y="-1.290178344727" />
                  <Point X="4.154717773438" Y="-1.195411132812" />
                  <Point X="3.411982177734" Y="-1.097628295898" />
                  <Point X="3.394836181641" Y="-1.098341186523" />
                  <Point X="3.305392822266" Y="-1.117782104492" />
                  <Point X="3.213271972656" Y="-1.137804931641" />
                  <Point X="3.1854453125" Y="-1.154697265625" />
                  <Point X="3.131382324219" Y="-1.219718017578" />
                  <Point X="3.075701171875" Y="-1.286685424805" />
                  <Point X="3.064357910156" Y="-1.314070556641" />
                  <Point X="3.056609375" Y="-1.398275268555" />
                  <Point X="3.04862890625" Y="-1.485000854492" />
                  <Point X="3.056360351562" Y="-1.516621826172" />
                  <Point X="3.105859619141" Y="-1.593614746094" />
                  <Point X="3.156840576172" Y="-1.672912353516" />
                  <Point X="3.168460693359" Y="-1.685540649414" />
                  <Point X="3.780590087891" Y="-2.155244140625" />
                  <Point X="4.33907421875" Y="-2.583784179688" />
                  <Point X="4.284264160156" Y="-2.672475097656" />
                  <Point X="4.204133300781" Y="-2.802139404297" />
                  <Point X="4.118973632812" Y="-2.923139404297" />
                  <Point X="4.056688476562" Y="-3.011638427734" />
                  <Point X="3.4149609375" Y="-2.64113671875" />
                  <Point X="2.753454589844" Y="-2.259215576172" />
                  <Point X="2.737340820313" Y="-2.253312744141" />
                  <Point X="2.630889160156" Y="-2.234087646484" />
                  <Point X="2.521250488281" Y="-2.214287109375" />
                  <Point X="2.489077636719" Y="-2.219244873047" />
                  <Point X="2.400642333984" Y="-2.265787597656" />
                  <Point X="2.309559570312" Y="-2.313723876953" />
                  <Point X="2.288599853516" Y="-2.334683837891" />
                  <Point X="2.242056884766" Y="-2.423119140625" />
                  <Point X="2.194120849609" Y="-2.514201904297" />
                  <Point X="2.189163085938" Y="-2.546374511719" />
                  <Point X="2.208388183594" Y="-2.652826171875" />
                  <Point X="2.228188720703" Y="-2.76246484375" />
                  <Point X="2.234091552734" Y="-2.778578613281" />
                  <Point X="2.627446289062" Y="-3.459888671875" />
                  <Point X="2.986673583984" Y="-4.082088623047" />
                  <Point X="2.930383056641" Y="-4.122295898438" />
                  <Point X="2.835298583984" Y="-4.190212402344" />
                  <Point X="2.740086425781" Y="-4.251841308594" />
                  <Point X="2.679775878906" Y="-4.290879394531" />
                  <Point X="2.187967773438" Y="-3.649942626953" />
                  <Point X="1.683177612305" Y="-2.992087402344" />
                  <Point X="1.670549316406" Y="-2.980467529297" />
                  <Point X="1.565559082031" Y="-2.912968505859" />
                  <Point X="1.45742590332" Y="-2.843448730469" />
                  <Point X="1.425804931641" Y="-2.835717041016" />
                  <Point X="1.310980224609" Y="-2.846283447266" />
                  <Point X="1.192718017578" Y="-2.857166015625" />
                  <Point X="1.165332885742" Y="-2.868509277344" />
                  <Point X="1.076668212891" Y="-2.942230957031" />
                  <Point X="0.985349060059" Y="-3.018159667969" />
                  <Point X="0.968456726074" Y="-3.045986328125" />
                  <Point X="0.941946533203" Y="-3.167954589844" />
                  <Point X="0.91464251709" Y="-3.29357421875" />
                  <Point X="0.91392956543" Y="-3.310720214844" />
                  <Point X="1.025403076172" Y="-4.157446777344" />
                  <Point X="1.127642211914" Y="-4.934028320312" />
                  <Point X="1.084302246094" Y="-4.943528808594" />
                  <Point X="0.994347290039" Y="-4.963246582031" />
                  <Point X="0.906387939453" Y="-4.979225585938" />
                  <Point X="0.860200378418" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#166" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.095197293765" Y="4.710405628776" Z="1.25" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.25" />
                  <Point X="-0.594435095944" Y="5.029369539894" Z="1.25" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.25" />
                  <Point X="-1.372896139994" Y="4.874737952206" Z="1.25" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.25" />
                  <Point X="-1.729400608057" Y="4.608424193418" Z="1.25" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.25" />
                  <Point X="-1.724023547081" Y="4.391237405672" Z="1.25" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.25" />
                  <Point X="-1.790243407028" Y="4.319961418715" Z="1.25" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.25" />
                  <Point X="-1.887409251817" Y="4.324873457334" Z="1.25" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.25" />
                  <Point X="-2.032827859162" Y="4.477675574947" Z="1.25" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.25" />
                  <Point X="-2.465219988803" Y="4.426045725093" Z="1.25" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.25" />
                  <Point X="-3.086966549432" Y="4.017191706924" Z="1.25" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.25" />
                  <Point X="-3.192878041622" Y="3.471746623644" Z="1.25" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.25" />
                  <Point X="-2.997727157535" Y="3.096907391148" Z="1.25" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.25" />
                  <Point X="-3.024849559858" Y="3.023954008028" Z="1.25" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.25" />
                  <Point X="-3.098169074459" Y="2.997837541638" Z="1.25" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.25" />
                  <Point X="-3.462112724653" Y="3.187315907124" Z="1.25" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.25" />
                  <Point X="-4.003664738454" Y="3.108591781226" Z="1.25" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.25" />
                  <Point X="-4.380171810196" Y="2.550837168573" Z="1.25" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.25" />
                  <Point X="-4.128384179555" Y="1.942182674226" Z="1.25" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.25" />
                  <Point X="-3.68147305988" Y="1.581847976161" Z="1.25" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.25" />
                  <Point X="-3.679327921298" Y="1.523513460724" Z="1.25" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.25" />
                  <Point X="-3.722635946158" Y="1.484373505948" Z="1.25" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.25" />
                  <Point X="-4.276852988936" Y="1.543812792576" Z="1.25" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.25" />
                  <Point X="-4.895816148582" Y="1.32214210658" Z="1.25" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.25" />
                  <Point X="-5.01722411104" Y="0.737928594605" Z="1.25" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.25" />
                  <Point X="-4.32938519961" Y="0.250787742322" Z="1.25" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.25" />
                  <Point X="-3.56247964762" Y="0.039295952821" Z="1.25" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.25" />
                  <Point X="-3.544114082748" Y="0.014683678824" Z="1.25" />
                  <Point X="-3.539556741714" Y="0" Z="1.25" />
                  <Point X="-3.544250518387" Y="-0.015123272237" Z="1.25" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.25" />
                  <Point X="-3.562888947503" Y="-0.039580030313" Z="1.25" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.25" />
                  <Point X="-4.307502589162" Y="-0.244924314434" Z="1.25" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.25" />
                  <Point X="-5.020921683795" Y="-0.722161210333" Z="1.25" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.25" />
                  <Point X="-4.913781440533" Y="-1.259334555074" Z="1.25" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.25" />
                  <Point X="-4.045034107071" Y="-1.4155918224" Z="1.25" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.25" />
                  <Point X="-3.205722314035" Y="-1.3147714906" Z="1.25" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.25" />
                  <Point X="-3.196585366983" Y="-1.337542844137" Z="1.25" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.25" />
                  <Point X="-3.842035909416" Y="-1.84455643025" Z="1.25" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.25" />
                  <Point X="-4.353963130338" Y="-2.601401124402" Z="1.25" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.25" />
                  <Point X="-4.033237244485" Y="-3.075269401662" Z="1.25" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.25" />
                  <Point X="-3.22704723309" Y="-2.933197977026" Z="1.25" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.25" />
                  <Point X="-2.564037306376" Y="-2.564293162756" Z="1.25" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.25" />
                  <Point X="-2.922219036142" Y="-3.208031149402" Z="1.25" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.25" />
                  <Point X="-3.092181485416" Y="-4.022195273636" Z="1.25" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.25" />
                  <Point X="-2.667556804991" Y="-4.315527882938" Z="1.25" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.25" />
                  <Point X="-2.340328396464" Y="-4.305158117465" Z="1.25" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.25" />
                  <Point X="-2.095336891191" Y="-4.068997231673" Z="1.25" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.25" />
                  <Point X="-1.811178509959" Y="-3.99437978136" Z="1.25" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.25" />
                  <Point X="-1.540316346542" Y="-4.10816668115" Z="1.25" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.25" />
                  <Point X="-1.394695927337" Y="-4.363330218285" Z="1.25" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.25" />
                  <Point X="-1.388633214414" Y="-4.693666855076" Z="1.25" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.25" />
                  <Point X="-1.263069970698" Y="-4.918104428187" Z="1.25" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.25" />
                  <Point X="-0.96532472008" Y="-4.985102276866" Z="1.25" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.25" />
                  <Point X="-0.620331320056" Y="-4.277292082717" Z="1.25" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.25" />
                  <Point X="-0.334015600283" Y="-3.399083346348" Z="1.25" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.25" />
                  <Point X="-0.124811267712" Y="-3.242976179971" Z="1.25" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.25" />
                  <Point X="0.128547811649" Y="-3.244135865317" Z="1.25" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.25" />
                  <Point X="0.336430259149" Y="-3.40256240942" Z="1.25" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.25" />
                  <Point X="0.614423540345" Y="-4.255243947098" Z="1.25" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.25" />
                  <Point X="0.909168963426" Y="-4.997140579883" Z="1.25" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.25" />
                  <Point X="1.088851829843" Y="-4.961088935014" Z="1.25" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.25" />
                  <Point X="1.068819501868" Y="-4.119640546499" Z="1.25" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.25" />
                  <Point X="0.984649726484" Y="-3.147294140346" Z="1.25" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.25" />
                  <Point X="1.102479085169" Y="-2.949397202119" Z="1.25" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.25" />
                  <Point X="1.30940588706" Y="-2.864792850515" Z="1.25" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.25" />
                  <Point X="1.532363750757" Y="-2.923746306385" Z="1.25" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.25" />
                  <Point X="2.142144190927" Y="-3.649100654432" Z="1.25" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.25" />
                  <Point X="2.761099729628" Y="-4.262535483382" Z="1.25" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.25" />
                  <Point X="2.953288541354" Y="-4.131702752042" Z="1.25" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.25" />
                  <Point X="2.664591639682" Y="-3.403608737561" Z="1.25" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.25" />
                  <Point X="2.251436228292" Y="-2.61265994904" Z="1.25" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.25" />
                  <Point X="2.280147344539" Y="-2.415125422069" Z="1.25" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.25" />
                  <Point X="2.417772956303" Y="-2.278753965428" Z="1.25" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.25" />
                  <Point X="2.615846854472" Y="-2.252011781199" Z="1.25" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.25" />
                  <Point X="3.383804859546" Y="-2.653157860593" Z="1.25" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.25" />
                  <Point X="4.153706168953" Y="-2.920636719824" Z="1.25" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.25" />
                  <Point X="4.320641452052" Y="-2.667480256054" Z="1.25" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.25" />
                  <Point X="3.80487217343" Y="-2.084296461447" Z="1.25" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.25" />
                  <Point X="3.141761722815" Y="-1.53529527576" Z="1.25" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.25" />
                  <Point X="3.100242967148" Y="-1.371576923461" Z="1.25" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.25" />
                  <Point X="3.163672746669" Y="-1.220404926234" Z="1.25" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.25" />
                  <Point X="3.309856551368" Y="-1.135361297611" Z="1.25" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.25" />
                  <Point X="4.142035457148" Y="-1.213703413492" Z="1.25" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.25" />
                  <Point X="4.949845164377" Y="-1.126689966942" Z="1.25" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.25" />
                  <Point X="5.020143950506" Y="-0.754024857348" Z="1.25" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.25" />
                  <Point X="4.407570579788" Y="-0.397554707055" Z="1.25" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.25" />
                  <Point X="3.701015537774" Y="-0.193680115979" Z="1.25" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.25" />
                  <Point X="3.627280417559" Y="-0.131452822812" Z="1.25" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.25" />
                  <Point X="3.590549291332" Y="-0.047592858761" Z="1.25" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.25" />
                  <Point X="3.590822159093" Y="0.049017672451" Z="1.25" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.25" />
                  <Point X="3.628099020842" Y="0.132495915763" Z="1.25" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.25" />
                  <Point X="3.702379876578" Y="0.194468728263" Z="1.25" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.25" />
                  <Point X="4.388397237518" Y="0.392417225714" Z="1.25" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.25" />
                  <Point X="5.014578037466" Y="0.783922203594" Z="1.25" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.25" />
                  <Point X="4.930701985644" Y="1.203621096212" Z="1.25" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.25" />
                  <Point X="4.182407306272" Y="1.316719896459" Z="1.25" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.25" />
                  <Point X="3.415346674088" Y="1.228338097034" Z="1.25" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.25" />
                  <Point X="3.333626044311" Y="1.254358884352" Z="1.25" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.25" />
                  <Point X="3.27493539712" Y="1.31073235467" Z="1.25" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.25" />
                  <Point X="3.242296223549" Y="1.39016426148" Z="1.25" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.25" />
                  <Point X="3.244512727434" Y="1.471398990648" Z="1.25" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.25" />
                  <Point X="3.284433158676" Y="1.547560264744" Z="1.25" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.25" />
                  <Point X="3.87173984237" Y="2.013509391663" Z="1.25" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.25" />
                  <Point X="4.341205815808" Y="2.630502718328" Z="1.25" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.25" />
                  <Point X="4.118482871714" Y="2.967104773675" Z="1.25" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.25" />
                  <Point X="3.267074387473" Y="2.704166327154" Z="1.25" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.25" />
                  <Point X="2.469142756356" Y="2.25610565437" Z="1.25" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.25" />
                  <Point X="2.394367282362" Y="2.249776581807" Z="1.25" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.25" />
                  <Point X="2.328045591192" Y="2.275696069408" Z="1.25" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.25" />
                  <Point X="2.275062530827" Y="2.328979269191" Z="1.25" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.25" />
                  <Point X="2.249653120951" Y="2.39539116147" Z="1.25" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.25" />
                  <Point X="2.256422288287" Y="2.470326819526" Z="1.25" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.25" />
                  <Point X="2.691459025705" Y="3.245064694482" Z="1.25" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.25" />
                  <Point X="2.938296009482" Y="4.137613752051" Z="1.25" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.25" />
                  <Point X="2.551697377451" Y="4.386601417652" Z="1.25" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.25" />
                  <Point X="2.146860867946" Y="4.598449812144" Z="1.25" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.25" />
                  <Point X="1.727233542421" Y="4.771940572235" Z="1.25" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.25" />
                  <Point X="1.184824200791" Y="4.928423134707" Z="1.25" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.25" />
                  <Point X="0.522966084445" Y="5.041792382826" Z="1.25" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.25" />
                  <Point X="0.098047267788" Y="4.721041877175" Z="1.25" />
                  <Point X="0" Y="4.355124473572" Z="1.25" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>