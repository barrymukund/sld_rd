<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#173" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2146" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="0" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000475830078" Y="0.004715820312" />
                  <Width Value="9.783896972656" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.784295227051" Y="-4.337282226563" />
                  <Point X="0.563302062988" Y="-3.512524414063" />
                  <Point X="0.557721191406" Y="-3.497141845703" />
                  <Point X="0.542363037109" Y="-3.467377197266" />
                  <Point X="0.452037719727" Y="-3.337235351562" />
                  <Point X="0.378635528564" Y="-3.231476806641" />
                  <Point X="0.356751586914" Y="-3.209021484375" />
                  <Point X="0.330496398926" Y="-3.189777587891" />
                  <Point X="0.302495178223" Y="-3.175669189453" />
                  <Point X="0.162721862793" Y="-3.132288574219" />
                  <Point X="0.049136188507" Y="-3.097035888672" />
                  <Point X="0.020976638794" Y="-3.092766601562" />
                  <Point X="-0.008664840698" Y="-3.092766601562" />
                  <Point X="-0.036824237823" Y="-3.097035888672" />
                  <Point X="-0.176597549438" Y="-3.140416259766" />
                  <Point X="-0.290183227539" Y="-3.175669189453" />
                  <Point X="-0.31818460083" Y="-3.189777587891" />
                  <Point X="-0.344439788818" Y="-3.209021484375" />
                  <Point X="-0.366323577881" Y="-3.231476806641" />
                  <Point X="-0.456649047852" Y="-3.361618408203" />
                  <Point X="-0.530051086426" Y="-3.467377197266" />
                  <Point X="-0.538189025879" Y="-3.481573730469" />
                  <Point X="-0.550990112305" Y="-3.512524414063" />
                  <Point X="-0.730578063965" Y="-4.182756347656" />
                  <Point X="-0.916584472656" Y="-4.87694140625" />
                  <Point X="-0.94924005127" Y="-4.870603027344" />
                  <Point X="-1.079342041016" Y="-4.845350097656" />
                  <Point X="-1.238245727539" Y="-4.804465332031" />
                  <Point X="-1.246418212891" Y="-4.802362792969" />
                  <Point X="-1.241565551758" Y="-4.765503417969" />
                  <Point X="-1.214963012695" Y="-4.563438476562" />
                  <Point X="-1.214201171875" Y="-4.5479296875" />
                  <Point X="-1.216508666992" Y="-4.516224121094" />
                  <Point X="-1.249900512695" Y="-4.348352050781" />
                  <Point X="-1.277036132812" Y="-4.211932617188" />
                  <Point X="-1.287938354492" Y="-4.182966308594" />
                  <Point X="-1.304010375977" Y="-4.15512890625" />
                  <Point X="-1.323644775391" Y="-4.131204101562" />
                  <Point X="-1.452330322266" Y="-4.018349609375" />
                  <Point X="-1.556905517578" Y="-3.926639404297" />
                  <Point X="-1.583188476562" Y="-3.910295410156" />
                  <Point X="-1.612885742188" Y="-3.897994384766" />
                  <Point X="-1.64302734375" Y="-3.890966308594" />
                  <Point X="-1.813822021484" Y="-3.879771728516" />
                  <Point X="-1.952616577148" Y="-3.870674804688" />
                  <Point X="-1.98341796875" Y="-3.873708496094" />
                  <Point X="-2.014466674805" Y="-3.882028076172" />
                  <Point X="-2.042657714844" Y="-3.894801513672" />
                  <Point X="-2.184972900391" Y="-3.989893554688" />
                  <Point X="-2.300624267578" Y="-4.067169677734" />
                  <Point X="-2.312785644531" Y="-4.076821533203" />
                  <Point X="-2.335102783203" Y="-4.099461914062" />
                  <Point X="-2.435927490234" Y="-4.230859375" />
                  <Point X="-2.480149169922" Y="-4.288490234375" />
                  <Point X="-2.611014648438" Y="-4.207461425781" />
                  <Point X="-2.801706542969" Y="-4.089389892578" />
                  <Point X="-3.021744384766" Y="-3.919967773438" />
                  <Point X="-3.104721923828" Y="-3.856077880859" />
                  <Point X="-2.834452148438" Y="-3.387956787109" />
                  <Point X="-2.423761230469" Y="-2.676619628906" />
                  <Point X="-2.412858886719" Y="-2.64765234375" />
                  <Point X="-2.406588134766" Y="-2.616125488281" />
                  <Point X="-2.405575927734" Y="-2.585191894531" />
                  <Point X="-2.414560546875" Y="-2.555574462891" />
                  <Point X="-2.428777832031" Y="-2.526745361328" />
                  <Point X="-2.446805175781" Y="-2.501588623047" />
                  <Point X="-2.464153808594" Y="-2.484239990234" />
                  <Point X="-2.489312744141" Y="-2.466211425781" />
                  <Point X="-2.518141601562" Y="-2.451995117188" />
                  <Point X="-2.547758544922" Y="-2.443011230469" />
                  <Point X="-2.578691650391" Y="-2.444024169922" />
                  <Point X="-2.610217529297" Y="-2.450295166016" />
                  <Point X="-2.63918359375" Y="-2.461197265625" />
                  <Point X="-3.217245605469" Y="-2.794941650391" />
                  <Point X="-3.818024414062" Y="-3.141801269531" />
                  <Point X="-3.932201171875" Y="-2.991796630859" />
                  <Point X="-4.082858398438" Y="-2.793864013672" />
                  <Point X="-4.240606445313" Y="-2.529344238281" />
                  <Point X="-4.306142578125" Y="-2.419450439453" />
                  <Point X="-3.824530029297" Y="-2.049895996094" />
                  <Point X="-3.105954589844" Y="-1.498513427734" />
                  <Point X="-3.084576904297" Y="-1.475592895508" />
                  <Point X="-3.066612060547" Y="-1.448461425781" />
                  <Point X="-3.053856689453" Y="-1.419832519531" />
                  <Point X="-3.046152099609" Y="-1.390085449219" />
                  <Point X="-3.04334765625" Y="-1.359656738281" />
                  <Point X="-3.045556396484" Y="-1.327986328125" />
                  <Point X="-3.052557617188" Y="-1.298240966797" />
                  <Point X="-3.068640136719" Y="-1.272257080078" />
                  <Point X="-3.08947265625" Y="-1.24830078125" />
                  <Point X="-3.112972167969" Y="-1.228767211914" />
                  <Point X="-3.139454833984" Y="-1.213180664062" />
                  <Point X="-3.168717773438" Y="-1.201956665039" />
                  <Point X="-3.200605712891" Y="-1.195474853516" />
                  <Point X="-3.231929199219" Y="-1.194383789062" />
                  <Point X="-3.961677734375" Y="-1.29045703125" />
                  <Point X="-4.732102050781" Y="-1.391885253906" />
                  <Point X="-4.775152832031" Y="-1.223342163086" />
                  <Point X="-4.834077636719" Y="-0.99265435791" />
                  <Point X="-4.875812988281" Y="-0.700843444824" />
                  <Point X="-4.892424316406" Y="-0.584698242188" />
                  <Point X="-4.351441894531" Y="-0.439742401123" />
                  <Point X="-3.532875976562" Y="-0.220408447266" />
                  <Point X="-3.509926025391" Y="-0.21089604187" />
                  <Point X="-3.479047119141" Y="-0.193049133301" />
                  <Point X="-3.465881103516" Y="-0.183874160767" />
                  <Point X="-3.441620605469" Y="-0.163720947266" />
                  <Point X="-3.425552978516" Y="-0.146601913452" />
                  <Point X="-3.414174316406" Y="-0.126065086365" />
                  <Point X="-3.401645263672" Y="-0.094883842468" />
                  <Point X="-3.397150146484" Y="-0.080483940125" />
                  <Point X="-3.390755615234" Y="-0.052299953461" />
                  <Point X="-3.388401611328" Y="-0.030909324646" />
                  <Point X="-3.390922363281" Y="-0.009537712097" />
                  <Point X="-3.398272949219" Y="0.021727165222" />
                  <Point X="-3.402909423828" Y="0.036162128448" />
                  <Point X="-3.414482421875" Y="0.064262481689" />
                  <Point X="-3.426037597656" Y="0.084700767517" />
                  <Point X="-3.442252197266" Y="0.101680938721" />
                  <Point X="-3.469381347656" Y="0.123825035095" />
                  <Point X="-3.482654296875" Y="0.132902252197" />
                  <Point X="-3.510664550781" Y="0.148758285522" />
                  <Point X="-3.532875976562" Y="0.157848251343" />
                  <Point X="-4.198076660156" Y="0.336088134766" />
                  <Point X="-4.89181640625" Y="0.521975219727" />
                  <Point X="-4.862462890625" Y="0.720342041016" />
                  <Point X="-4.824487792969" Y="0.976975402832" />
                  <Point X="-4.740475097656" Y="1.287008056641" />
                  <Point X="-4.703551269531" Y="1.423267822266" />
                  <Point X="-4.352434082031" Y="1.377042480469" />
                  <Point X="-3.765666259766" Y="1.29979284668" />
                  <Point X="-3.744986083984" Y="1.299341674805" />
                  <Point X="-3.723424560547" Y="1.301228149414" />
                  <Point X="-3.703137939453" Y="1.305263427734" />
                  <Point X="-3.669250488281" Y="1.315947998047" />
                  <Point X="-3.641711914063" Y="1.324630981445" />
                  <Point X="-3.622779052734" Y="1.332961669922" />
                  <Point X="-3.604034667969" Y="1.343783691406" />
                  <Point X="-3.587353271484" Y="1.356014892578" />
                  <Point X="-3.57371484375" Y="1.371566772461" />
                  <Point X="-3.561300292969" Y="1.389296875" />
                  <Point X="-3.5513515625" Y="1.407430908203" />
                  <Point X="-3.53775390625" Y="1.440258178711" />
                  <Point X="-3.526704101563" Y="1.466935058594" />
                  <Point X="-3.520915527344" Y="1.486794067383" />
                  <Point X="-3.517157226562" Y="1.508108886719" />
                  <Point X="-3.5158046875" Y="1.528749267578" />
                  <Point X="-3.518951171875" Y="1.549192993164" />
                  <Point X="-3.524552978516" Y="1.570099243164" />
                  <Point X="-3.532049804688" Y="1.589377563477" />
                  <Point X="-3.548456787109" Y="1.620895141602" />
                  <Point X="-3.561789550781" Y="1.646507324219" />
                  <Point X="-3.573281005859" Y="1.663705688477" />
                  <Point X="-3.587193603516" Y="1.680286376953" />
                  <Point X="-3.602135986328" Y="1.694590332031" />
                  <Point X="-3.983696533203" Y="1.987372192383" />
                  <Point X="-4.351859863281" Y="2.269874267578" />
                  <Point X="-4.228709960938" Y="2.480860107422" />
                  <Point X="-4.081153808594" Y="2.733659423828" />
                  <Point X="-3.858608154297" Y="3.019710205078" />
                  <Point X="-3.750504882813" Y="3.158661621094" />
                  <Point X="-3.567033447266" Y="3.052734375" />
                  <Point X="-3.206656982422" Y="2.844670898438" />
                  <Point X="-3.187724121094" Y="2.836340332031" />
                  <Point X="-3.167081787109" Y="2.829831787109" />
                  <Point X="-3.146794433594" Y="2.825796386719" />
                  <Point X="-3.099598388672" Y="2.821667236328" />
                  <Point X="-3.061245117188" Y="2.818311767578" />
                  <Point X="-3.040564697266" Y="2.818762939453" />
                  <Point X="-3.019106201172" Y="2.821588134766" />
                  <Point X="-2.999014892578" Y="2.826504638672" />
                  <Point X="-2.980463867188" Y="2.835652832031" />
                  <Point X="-2.962209472656" Y="2.847281982422" />
                  <Point X="-2.946077392578" Y="2.860229492188" />
                  <Point X="-2.912577392578" Y="2.893729492188" />
                  <Point X="-2.885353759766" Y="2.920952880859" />
                  <Point X="-2.872406738281" Y="2.937084228516" />
                  <Point X="-2.86077734375" Y="2.955338623047" />
                  <Point X="-2.851628662109" Y="2.973890136719" />
                  <Point X="-2.846712158203" Y="2.993981933594" />
                  <Point X="-2.843886962891" Y="3.015440673828" />
                  <Point X="-2.843435791016" Y="3.036120849609" />
                  <Point X="-2.847564941406" Y="3.083316894531" />
                  <Point X="-2.850920410156" Y="3.121670410156" />
                  <Point X="-2.854955810547" Y="3.141957763672" />
                  <Point X="-2.861464355469" Y="3.162600097656" />
                  <Point X="-2.869794921875" Y="3.181532958984" />
                  <Point X="-3.038875976562" Y="3.474389892578" />
                  <Point X="-3.183332763672" Y="3.724596435547" />
                  <Point X="-2.957617675781" Y="3.897650146484" />
                  <Point X="-2.700625732422" Y="4.09468359375" />
                  <Point X="-2.350125488281" Y="4.2894140625" />
                  <Point X="-2.167036621094" Y="4.391134277344" />
                  <Point X="-2.153517578125" Y="4.373515625" />
                  <Point X="-2.04319543457" Y="4.229740722656" />
                  <Point X="-2.028891845703" Y="4.214799316406" />
                  <Point X="-2.012311401367" Y="4.20088671875" />
                  <Point X="-1.99511340332" Y="4.189395019531" />
                  <Point X="-1.942584716797" Y="4.162049804688" />
                  <Point X="-1.899897460938" Y="4.139828125" />
                  <Point X="-1.8806171875" Y="4.132330566406" />
                  <Point X="-1.859710571289" Y="4.126729003906" />
                  <Point X="-1.839267578125" Y="4.123582519531" />
                  <Point X="-1.818628051758" Y="4.124935546875" />
                  <Point X="-1.797312988281" Y="4.128693847656" />
                  <Point X="-1.777453491211" Y="4.134482421875" />
                  <Point X="-1.722741088867" Y="4.157145507813" />
                  <Point X="-1.678279296875" Y="4.175562011719" />
                  <Point X="-1.660146362305" Y="4.185509765625" />
                  <Point X="-1.642416625977" Y="4.197923828125" />
                  <Point X="-1.626864379883" Y="4.2115625" />
                  <Point X="-1.6146328125" Y="4.228244140625" />
                  <Point X="-1.603810913086" Y="4.24698828125" />
                  <Point X="-1.59548046875" Y="4.265920898438" />
                  <Point X="-1.577672485352" Y="4.322400390625" />
                  <Point X="-1.563201171875" Y="4.368297363281" />
                  <Point X="-1.559165649414" Y="4.388584960938" />
                  <Point X="-1.557279174805" Y="4.410146972656" />
                  <Point X="-1.55773034668" Y="4.430826660156" />
                  <Point X="-1.57695300293" Y="4.576836914062" />
                  <Point X="-1.584201904297" Y="4.631897949219" />
                  <Point X="-1.282327270508" Y="4.716533203125" />
                  <Point X="-0.94963470459" Y="4.80980859375" />
                  <Point X="-0.524727966309" Y="4.859537597656" />
                  <Point X="-0.29471081543" Y="4.886457519531" />
                  <Point X="-0.240512695312" Y="4.684187011719" />
                  <Point X="-0.133903305054" Y="4.286315429688" />
                  <Point X="-0.121129844666" Y="4.258124023438" />
                  <Point X="-0.10327155304" Y="4.231397460938" />
                  <Point X="-0.082114532471" Y="4.20880859375" />
                  <Point X="-0.054819446564" Y="4.19421875" />
                  <Point X="-0.024381370544" Y="4.183886230469" />
                  <Point X="0.006155906677" Y="4.178844238281" />
                  <Point X="0.03669316864" Y="4.183886230469" />
                  <Point X="0.067131248474" Y="4.19421875" />
                  <Point X="0.094426467896" Y="4.20880859375" />
                  <Point X="0.115583503723" Y="4.231397460938" />
                  <Point X="0.133441802979" Y="4.258124023438" />
                  <Point X="0.146215255737" Y="4.286315429688" />
                  <Point X="0.232850402832" Y="4.609642578125" />
                  <Point X="0.307419372559" Y="4.8879375" />
                  <Point X="0.553545715332" Y="4.862161621094" />
                  <Point X="0.844041259766" Y="4.831738769531" />
                  <Point X="1.195583251953" Y="4.746865722656" />
                  <Point X="1.481026489258" Y="4.677951171875" />
                  <Point X="1.709182495117" Y="4.595196777344" />
                  <Point X="1.894645019531" Y="4.527928222656" />
                  <Point X="2.115905517578" Y="4.424452148438" />
                  <Point X="2.294576416016" Y="4.340893554688" />
                  <Point X="2.508348632812" Y="4.216349609375" />
                  <Point X="2.680977539062" Y="4.115775390625" />
                  <Point X="2.882571777344" Y="3.972413085938" />
                  <Point X="2.943260009766" Y="3.929254882812" />
                  <Point X="2.622924804688" Y="3.374417480469" />
                  <Point X="2.147581054688" Y="2.551097900391" />
                  <Point X="2.142075683594" Y="2.539930664062" />
                  <Point X="2.133076904297" Y="2.516056640625" />
                  <Point X="2.121232421875" Y="2.471764160156" />
                  <Point X="2.111607177734" Y="2.435770263672" />
                  <Point X="2.108619384766" Y="2.417934326172" />
                  <Point X="2.107728027344" Y="2.380952880859" />
                  <Point X="2.112346435547" Y="2.342652587891" />
                  <Point X="2.116099365234" Y="2.311528076172" />
                  <Point X="2.121441894531" Y="2.289605224609" />
                  <Point X="2.129708007812" Y="2.267516357422" />
                  <Point X="2.140070800781" Y="2.247471191406" />
                  <Point X="2.163769775391" Y="2.212544921875" />
                  <Point X="2.183028564453" Y="2.184162597656" />
                  <Point X="2.194465332031" Y="2.170327880859" />
                  <Point X="2.221598876953" Y="2.145592529297" />
                  <Point X="2.256524902344" Y="2.121893554688" />
                  <Point X="2.284907470703" Y="2.102635009766" />
                  <Point X="2.304952636719" Y="2.092271972656" />
                  <Point X="2.327040771484" Y="2.084006103516" />
                  <Point X="2.348963867188" Y="2.078663574219" />
                  <Point X="2.387264160156" Y="2.074045166016" />
                  <Point X="2.418388671875" Y="2.070291992188" />
                  <Point X="2.436467529297" Y="2.069845703125" />
                  <Point X="2.473206298828" Y="2.074171142578" />
                  <Point X="2.517498779297" Y="2.086015625" />
                  <Point X="2.553492675781" Y="2.095640869141" />
                  <Point X="2.565283447266" Y="2.099638183594" />
                  <Point X="2.588533935547" Y="2.110145019531" />
                  <Point X="3.257597412109" Y="2.496428955078" />
                  <Point X="3.967326416016" Y="2.906191162109" />
                  <Point X="4.020873291016" Y="2.831773193359" />
                  <Point X="4.123270019531" Y="2.689465087891" />
                  <Point X="4.235657714844" Y="2.503743652344" />
                  <Point X="4.262198730469" Y="2.459884277344" />
                  <Point X="3.856618408203" Y="2.148671142578" />
                  <Point X="3.230783935547" Y="1.668451171875" />
                  <Point X="3.221424316406" Y="1.660241088867" />
                  <Point X="3.203973876953" Y="1.641627685547" />
                  <Point X="3.172096435547" Y="1.600041381836" />
                  <Point X="3.146191650391" Y="1.566246459961" />
                  <Point X="3.136605712891" Y="1.550911621094" />
                  <Point X="3.121629882812" Y="1.517086181641" />
                  <Point X="3.109755615234" Y="1.474626220703" />
                  <Point X="3.100105957031" Y="1.440121582031" />
                  <Point X="3.096652587891" Y="1.417823242188" />
                  <Point X="3.095836425781" Y="1.394252807617" />
                  <Point X="3.097739501953" Y="1.371767944336" />
                  <Point X="3.107487060547" Y="1.324526000977" />
                  <Point X="3.115408447266" Y="1.286135253906" />
                  <Point X="3.120679931641" Y="1.268977416992" />
                  <Point X="3.136282470703" Y="1.235740356445" />
                  <Point X="3.162794921875" Y="1.195442504883" />
                  <Point X="3.184340087891" Y="1.16269519043" />
                  <Point X="3.198893798828" Y="1.145450195312" />
                  <Point X="3.216137695313" Y="1.129360473633" />
                  <Point X="3.234346923828" Y="1.11603503418" />
                  <Point X="3.272767089844" Y="1.094407714844" />
                  <Point X="3.303989013672" Y="1.076832763672" />
                  <Point X="3.320520507812" Y="1.069502075195" />
                  <Point X="3.356118408203" Y="1.059438598633" />
                  <Point X="3.408064941406" Y="1.052573120117" />
                  <Point X="3.450279052734" Y="1.046994018555" />
                  <Point X="3.462702392578" Y="1.046175048828" />
                  <Point X="3.488203613281" Y="1.04698449707" />
                  <Point X="4.123769042969" Y="1.130658203125" />
                  <Point X="4.776839355469" Y="1.21663671875" />
                  <Point X="4.801956542969" Y="1.113463012695" />
                  <Point X="4.845936035156" Y="0.932809448242" />
                  <Point X="4.881352050781" Y="0.705338500977" />
                  <Point X="4.890864746094" Y="0.644238586426" />
                  <Point X="4.433799804688" Y="0.521768310547" />
                  <Point X="3.716579833984" Y="0.32958984375" />
                  <Point X="3.704791259766" Y="0.325586517334" />
                  <Point X="3.681545654297" Y="0.315067901611" />
                  <Point X="3.630509765625" Y="0.285568206787" />
                  <Point X="3.589035644531" Y="0.26159564209" />
                  <Point X="3.574310791016" Y="0.251095977783" />
                  <Point X="3.547530761719" Y="0.225576522827" />
                  <Point X="3.516909179688" Y="0.186557434082" />
                  <Point X="3.492024902344" Y="0.154848876953" />
                  <Point X="3.48030078125" Y="0.135568603516" />
                  <Point X="3.470527099609" Y="0.114104988098" />
                  <Point X="3.463680908203" Y="0.092604179382" />
                  <Point X="3.453473632812" Y="0.0393060112" />
                  <Point X="3.445178710938" Y="-0.004006318092" />
                  <Point X="3.443483154297" Y="-0.021875236511" />
                  <Point X="3.445178710938" Y="-0.058553718567" />
                  <Point X="3.455385986328" Y="-0.111851882935" />
                  <Point X="3.463680908203" Y="-0.155164367676" />
                  <Point X="3.470527099609" Y="-0.176665023804" />
                  <Point X="3.48030078125" Y="-0.198128646851" />
                  <Point X="3.492025146484" Y="-0.217409057617" />
                  <Point X="3.522646728516" Y="-0.256428161621" />
                  <Point X="3.547531005859" Y="-0.288136871338" />
                  <Point X="3.559999023438" Y="-0.301236083984" />
                  <Point X="3.589035644531" Y="-0.324155670166" />
                  <Point X="3.640071777344" Y="-0.35365536499" />
                  <Point X="3.681545654297" Y="-0.377627929688" />
                  <Point X="3.692709228516" Y="-0.383138549805" />
                  <Point X="3.716579833984" Y="-0.392150024414" />
                  <Point X="4.299422363281" Y="-0.548322143555" />
                  <Point X="4.89147265625" Y="-0.706961486816" />
                  <Point X="4.879579101562" Y="-0.785849243164" />
                  <Point X="4.855022460938" Y="-0.948726013184" />
                  <Point X="4.809648925781" Y="-1.147560302734" />
                  <Point X="4.801173828125" Y="-1.184698974609" />
                  <Point X="4.256155273438" Y="-1.112945800781" />
                  <Point X="3.424382080078" Y="-1.003440979004" />
                  <Point X="3.408035644531" Y="-1.002710266113" />
                  <Point X="3.374658691406" Y="-1.005508666992" />
                  <Point X="3.274493164062" Y="-1.027280273438" />
                  <Point X="3.193094482422" Y="-1.04497253418" />
                  <Point X="3.163974121094" Y="-1.056596923828" />
                  <Point X="3.136147460938" Y="-1.073489257812" />
                  <Point X="3.112397460938" Y="-1.093959960937" />
                  <Point X="3.051853515625" Y="-1.166775146484" />
                  <Point X="3.002653320312" Y="-1.225948120117" />
                  <Point X="2.987932617188" Y="-1.250330688477" />
                  <Point X="2.976589355469" Y="-1.277715942383" />
                  <Point X="2.969757568359" Y="-1.305365478516" />
                  <Point X="2.961080078125" Y="-1.399664428711" />
                  <Point X="2.954028564453" Y="-1.476295776367" />
                  <Point X="2.956347167969" Y="-1.507564086914" />
                  <Point X="2.964078613281" Y="-1.539185058594" />
                  <Point X="2.976450195312" Y="-1.567996582031" />
                  <Point X="3.031883300781" Y="-1.654219116211" />
                  <Point X="3.076930419922" Y="-1.724287109375" />
                  <Point X="3.086932861328" Y="-1.737239013672" />
                  <Point X="3.110628417969" Y="-1.760909057617" />
                  <Point X="3.651510009766" Y="-2.175942382812" />
                  <Point X="4.213122558594" Y="-2.606883056641" />
                  <Point X="4.194032226563" Y="-2.637773681641" />
                  <Point X="4.124809082031" Y="-2.749787109375" />
                  <Point X="4.030974121094" Y="-2.883113769531" />
                  <Point X="4.028981445312" Y="-2.8859453125" />
                  <Point X="3.541760986328" Y="-2.604648193359" />
                  <Point X="2.800954589844" Y="-2.176943115234" />
                  <Point X="2.786131591797" Y="-2.170012451172" />
                  <Point X="2.754224609375" Y="-2.159825195312" />
                  <Point X="2.635011474609" Y="-2.138295410156" />
                  <Point X="2.538134277344" Y="-2.120799560547" />
                  <Point X="2.506781982422" Y="-2.120395263672" />
                  <Point X="2.474609130859" Y="-2.125353027344" />
                  <Point X="2.444833251953" Y="-2.135176757812" />
                  <Point X="2.345796386719" Y="-2.187299072266" />
                  <Point X="2.265315185547" Y="-2.229655761719" />
                  <Point X="2.242384033203" Y="-2.246549072266" />
                  <Point X="2.221424316406" Y="-2.267509033203" />
                  <Point X="2.204531738281" Y="-2.290439453125" />
                  <Point X="2.152409423828" Y="-2.389476074219" />
                  <Point X="2.110052734375" Y="-2.469957519531" />
                  <Point X="2.100229003906" Y="-2.499733398438" />
                  <Point X="2.095271240234" Y="-2.531906005859" />
                  <Point X="2.095675537109" Y="-2.563258300781" />
                  <Point X="2.117205322266" Y="-2.682471191406" />
                  <Point X="2.134701171875" Y="-2.779348632812" />
                  <Point X="2.138985595703" Y="-2.795141845703" />
                  <Point X="2.151819091797" Y="-2.826078613281" />
                  <Point X="2.499389892578" Y="-3.428088623047" />
                  <Point X="2.861283447266" Y="-4.054906738281" />
                  <Point X="2.781852539062" Y="-4.111642089844" />
                  <Point X="2.701765380859" Y="-4.163481445312" />
                  <Point X="2.323849853516" Y="-3.670972900391" />
                  <Point X="1.758546020508" Y="-2.934255126953" />
                  <Point X="1.747503540039" Y="-2.922179443359" />
                  <Point X="1.721924072266" Y="-2.900557373047" />
                  <Point X="1.604347900391" Y="-2.824966796875" />
                  <Point X="1.50880078125" Y="-2.763538574219" />
                  <Point X="1.479989746094" Y="-2.751167236328" />
                  <Point X="1.448368774414" Y="-2.743435546875" />
                  <Point X="1.417099731445" Y="-2.741116699219" />
                  <Point X="1.288510009766" Y="-2.752949707031" />
                  <Point X="1.184012817383" Y="-2.762565673828" />
                  <Point X="1.15636315918" Y="-2.769397460938" />
                  <Point X="1.128978027344" Y="-2.780740722656" />
                  <Point X="1.104595825195" Y="-2.795461181641" />
                  <Point X="1.005302368164" Y="-2.878020507812" />
                  <Point X="0.924612243652" Y="-2.945111572266" />
                  <Point X="0.904141479492" Y="-2.968861572266" />
                  <Point X="0.887249084473" Y="-2.996688232422" />
                  <Point X="0.875624328613" Y="-3.025808837891" />
                  <Point X="0.845935974121" Y="-3.1623984375" />
                  <Point X="0.821810058594" Y="-3.273396728516" />
                  <Point X="0.819724487305" Y="-3.289627441406" />
                  <Point X="0.819742248535" Y="-3.323120117188" />
                  <Point X="0.918241149902" Y="-4.071293212891" />
                  <Point X="1.022065307617" Y="-4.859915039062" />
                  <Point X="0.97567755127" Y="-4.870083496094" />
                  <Point X="0.929315551758" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058433959961" Y="-4.752635742188" />
                  <Point X="-1.141246582031" Y="-4.731328613281" />
                  <Point X="-1.120775756836" Y="-4.575838378906" />
                  <Point X="-1.120077392578" Y="-4.568099609375" />
                  <Point X="-1.119451660156" Y="-4.541033691406" />
                  <Point X="-1.121759277344" Y="-4.509328125" />
                  <Point X="-1.123334106445" Y="-4.497690429688" />
                  <Point X="-1.156725952148" Y="-4.329818359375" />
                  <Point X="-1.183861572266" Y="-4.193398925781" />
                  <Point X="-1.18812512207" Y="-4.17846875" />
                  <Point X="-1.19902734375" Y="-4.149502441406" />
                  <Point X="-1.205666137695" Y="-4.135466308594" />
                  <Point X="-1.221738037109" Y="-4.10762890625" />
                  <Point X="-1.23057409668" Y="-4.094861816406" />
                  <Point X="-1.250208496094" Y="-4.070937011719" />
                  <Point X="-1.261006835938" Y="-4.059779296875" />
                  <Point X="-1.389692260742" Y="-3.946924804688" />
                  <Point X="-1.494267578125" Y="-3.855214599609" />
                  <Point X="-1.506738647461" Y="-3.845965576172" />
                  <Point X="-1.533021606445" Y="-3.829621582031" />
                  <Point X="-1.546833496094" Y="-3.822526855469" />
                  <Point X="-1.576530761719" Y="-3.810225830078" />
                  <Point X="-1.591313354492" Y="-3.805476074219" />
                  <Point X="-1.621455078125" Y="-3.798447998047" />
                  <Point X="-1.636813964844" Y="-3.796169677734" />
                  <Point X="-1.807608642578" Y="-3.784975097656" />
                  <Point X="-1.946403198242" Y="-3.775878173828" />
                  <Point X="-1.961928222656" Y="-3.776132324219" />
                  <Point X="-1.992729614258" Y="-3.779166015625" />
                  <Point X="-2.008006103516" Y="-3.781945556641" />
                  <Point X="-2.03905480957" Y="-3.790265136719" />
                  <Point X="-2.053674560547" Y="-3.795496337891" />
                  <Point X="-2.081865478516" Y="-3.808269775391" />
                  <Point X="-2.095437011719" Y="-3.815812011719" />
                  <Point X="-2.237752197266" Y="-3.910904052734" />
                  <Point X="-2.353403564453" Y="-3.988180175781" />
                  <Point X="-2.359681640625" Y="-3.992757080078" />
                  <Point X="-2.380442138672" Y="-4.010131103516" />
                  <Point X="-2.402759277344" Y="-4.032771484375" />
                  <Point X="-2.410471435547" Y="-4.041629638672" />
                  <Point X="-2.503202880859" Y="-4.162479492188" />
                  <Point X="-2.561003417969" Y="-4.126690917969" />
                  <Point X="-2.74758203125" Y="-4.011166259766" />
                  <Point X="-2.963787109375" Y="-3.8446953125" />
                  <Point X="-2.980862792969" Y="-3.831547607422" />
                  <Point X="-2.7521796875" Y="-3.435456787109" />
                  <Point X="-2.341488769531" Y="-2.724119628906" />
                  <Point X="-2.334850097656" Y="-2.710083007812" />
                  <Point X="-2.323947753906" Y="-2.681115722656" />
                  <Point X="-2.319684082031" Y="-2.666185058594" />
                  <Point X="-2.313413330078" Y="-2.634658203125" />
                  <Point X="-2.311638916016" Y="-2.619232421875" />
                  <Point X="-2.310626708984" Y="-2.588298828125" />
                  <Point X="-2.314666748047" Y="-2.557614013672" />
                  <Point X="-2.323651367188" Y="-2.527996582031" />
                  <Point X="-2.329358154297" Y="-2.513556152344" />
                  <Point X="-2.343575439453" Y="-2.484727050781" />
                  <Point X="-2.351557861328" Y="-2.471409423828" />
                  <Point X="-2.369585205078" Y="-2.446252685547" />
                  <Point X="-2.379630126953" Y="-2.434413574219" />
                  <Point X="-2.396978759766" Y="-2.417064941406" />
                  <Point X="-2.408818603516" Y="-2.40701953125" />
                  <Point X="-2.433977539062" Y="-2.388990966797" />
                  <Point X="-2.447296630859" Y="-2.3810078125" />
                  <Point X="-2.476125488281" Y="-2.366791503906" />
                  <Point X="-2.490565429688" Y="-2.361085449219" />
                  <Point X="-2.520182373047" Y="-2.3521015625" />
                  <Point X="-2.550867675781" Y="-2.348062011719" />
                  <Point X="-2.58180078125" Y="-2.349074951172" />
                  <Point X="-2.597225585938" Y="-2.350849609375" />
                  <Point X="-2.628751464844" Y="-2.357120605469" />
                  <Point X="-2.643681396484" Y="-2.361384033203" />
                  <Point X="-2.672647460938" Y="-2.372286132812" />
                  <Point X="-2.68668359375" Y="-2.378924804688" />
                  <Point X="-3.264745605469" Y="-2.712669189453" />
                  <Point X="-3.793089355469" Y="-3.017708496094" />
                  <Point X="-3.856607910156" Y="-2.934258300781" />
                  <Point X="-4.004014648438" Y="-2.740596191406" />
                  <Point X="-4.159013671875" Y="-2.480686035156" />
                  <Point X="-4.181265136719" Y="-2.443373779297" />
                  <Point X="-3.766697753906" Y="-2.125264648438" />
                  <Point X="-3.048122314453" Y="-1.573881958008" />
                  <Point X="-3.036481933594" Y="-1.563309692383" />
                  <Point X="-3.015104248047" Y="-1.540389282227" />
                  <Point X="-3.005366943359" Y="-1.528040893555" />
                  <Point X="-2.987402099609" Y="-1.500909545898" />
                  <Point X="-2.979835449219" Y="-1.487124023438" />
                  <Point X="-2.967080078125" Y="-1.458495239258" />
                  <Point X="-2.961891357422" Y="-1.443651977539" />
                  <Point X="-2.954186767578" Y="-1.413904785156" />
                  <Point X="-2.951552978516" Y="-1.398804077148" />
                  <Point X="-2.948748535156" Y="-1.368375366211" />
                  <Point X="-2.948577880859" Y="-1.353047363281" />
                  <Point X="-2.950786621094" Y="-1.321377075195" />
                  <Point X="-2.953083251953" Y="-1.306220703125" />
                  <Point X="-2.960084472656" Y="-1.276475463867" />
                  <Point X="-2.971778564453" Y="-1.248243408203" />
                  <Point X="-2.987861083984" Y="-1.222259521484" />
                  <Point X="-2.996954101562" Y="-1.209918457031" />
                  <Point X="-3.017786621094" Y="-1.185962036133" />
                  <Point X="-3.028745849609" Y="-1.175244384766" />
                  <Point X="-3.052245361328" Y="-1.15571081543" />
                  <Point X="-3.064785644531" Y="-1.146895019531" />
                  <Point X="-3.091268310547" Y="-1.13130859375" />
                  <Point X="-3.10543359375" Y="-1.124481445312" />
                  <Point X="-3.134696533203" Y="-1.113257446289" />
                  <Point X="-3.149794189453" Y="-1.108860595703" />
                  <Point X="-3.181682128906" Y="-1.102378662109" />
                  <Point X="-3.197298583984" Y="-1.100532470703" />
                  <Point X="-3.228622070312" Y="-1.09944140625" />
                  <Point X="-3.244329101562" Y="-1.100196533203" />
                  <Point X="-3.974077636719" Y="-1.196269775391" />
                  <Point X="-4.660920410156" Y="-1.286694335938" />
                  <Point X="-4.683107910156" Y="-1.199831298828" />
                  <Point X="-4.740762207031" Y="-0.974118286133" />
                  <Point X="-4.781770019531" Y="-0.687393310547" />
                  <Point X="-4.786452148438" Y="-0.654654418945" />
                  <Point X="-4.326854003906" Y="-0.531505432129" />
                  <Point X="-3.508288085938" Y="-0.312171447754" />
                  <Point X="-3.496500732422" Y="-0.308168579102" />
                  <Point X="-3.47355078125" Y="-0.29865612793" />
                  <Point X="-3.462388183594" Y="-0.293146514893" />
                  <Point X="-3.431509277344" Y="-0.275299591064" />
                  <Point X="-3.424732177734" Y="-0.270990722656" />
                  <Point X="-3.405177246094" Y="-0.256949707031" />
                  <Point X="-3.380916748047" Y="-0.236796447754" />
                  <Point X="-3.372352050781" Y="-0.228735290527" />
                  <Point X="-3.356284423828" Y="-0.211616165161" />
                  <Point X="-3.342455322266" Y="-0.192643112183" />
                  <Point X="-3.331076660156" Y="-0.172106246948" />
                  <Point X="-3.326024169922" Y="-0.161485061646" />
                  <Point X="-3.313495117188" Y="-0.13030368042" />
                  <Point X="-3.3109609375" Y="-0.123192070007" />
                  <Point X="-3.304504882812" Y="-0.101503822327" />
                  <Point X="-3.298110351562" Y="-0.073319869995" />
                  <Point X="-3.296325683594" Y="-0.06269184494" />
                  <Point X="-3.293971679688" Y="-0.041301143646" />
                  <Point X="-3.294055664063" Y="-0.019781290054" />
                  <Point X="-3.296576416016" Y="0.001590238452" />
                  <Point X="-3.298443847656" Y="0.012204589844" />
                  <Point X="-3.305794433594" Y="0.043469497681" />
                  <Point X="-3.30782421875" Y="0.050779071808" />
                  <Point X="-3.315067382812" Y="0.072339347839" />
                  <Point X="-3.326640380859" Y="0.100439781189" />
                  <Point X="-3.331784423828" Y="0.111017425537" />
                  <Point X="-3.343339599609" Y="0.131455749512" />
                  <Point X="-3.357331542969" Y="0.150309158325" />
                  <Point X="-3.373546142578" Y="0.167289321899" />
                  <Point X="-3.382179931641" Y="0.175276611328" />
                  <Point X="-3.409309082031" Y="0.197420684814" />
                  <Point X="-3.415753662109" Y="0.202240966797" />
                  <Point X="-3.435854736328" Y="0.215575119019" />
                  <Point X="-3.463864990234" Y="0.231431259155" />
                  <Point X="-3.474682617188" Y="0.236680465698" />
                  <Point X="-3.496894042969" Y="0.245770401001" />
                  <Point X="-3.508288085938" Y="0.249611251831" />
                  <Point X="-4.173488769531" Y="0.427851135254" />
                  <Point X="-4.785446289062" Y="0.591824584961" />
                  <Point X="-4.768486328125" Y="0.70643572998" />
                  <Point X="-4.731330566406" Y="0.957532958984" />
                  <Point X="-4.648782226562" Y="1.262161010742" />
                  <Point X="-4.633586425781" Y="1.318237060547" />
                  <Point X="-4.364833984375" Y="1.282855224609" />
                  <Point X="-3.778066162109" Y="1.20560546875" />
                  <Point X="-3.76773828125" Y="1.204815429688" />
                  <Point X="-3.747058105469" Y="1.204364257812" />
                  <Point X="-3.736705810547" Y="1.20470324707" />
                  <Point X="-3.715144287109" Y="1.20658972168" />
                  <Point X="-3.704890869141" Y="1.208053588867" />
                  <Point X="-3.684604248047" Y="1.212088867188" />
                  <Point X="-3.674571044922" Y="1.21466027832" />
                  <Point X="-3.64068359375" Y="1.225344848633" />
                  <Point X="-3.613145019531" Y="1.234027832031" />
                  <Point X="-3.603450927734" Y="1.237676391602" />
                  <Point X="-3.584518066406" Y="1.246007080078" />
                  <Point X="-3.575279296875" Y="1.250689208984" />
                  <Point X="-3.556534912109" Y="1.261511230469" />
                  <Point X="-3.547860595703" Y="1.267171264648" />
                  <Point X="-3.531179199219" Y="1.27940246582" />
                  <Point X="-3.515927978516" Y="1.293377563477" />
                  <Point X="-3.502289550781" Y="1.30892956543" />
                  <Point X="-3.495895019531" Y="1.317077636719" />
                  <Point X="-3.48348046875" Y="1.334807739258" />
                  <Point X="-3.478011474609" Y="1.343602783203" />
                  <Point X="-3.468062744141" Y="1.361736816406" />
                  <Point X="-3.463583251953" Y="1.371075683594" />
                  <Point X="-3.449985595703" Y="1.403902954102" />
                  <Point X="-3.438935791016" Y="1.430579833984" />
                  <Point X="-3.435499511719" Y="1.440350463867" />
                  <Point X="-3.4297109375" Y="1.460209472656" />
                  <Point X="-3.427358642578" Y="1.470297851562" />
                  <Point X="-3.423600341797" Y="1.491612670898" />
                  <Point X="-3.422360595703" Y="1.501896972656" />
                  <Point X="-3.421008056641" Y="1.522537353516" />
                  <Point X="-3.42191015625" Y="1.543200561523" />
                  <Point X="-3.425056640625" Y="1.56364440918" />
                  <Point X="-3.427188232422" Y="1.573780761719" />
                  <Point X="-3.432790039062" Y="1.594687011719" />
                  <Point X="-3.436011962891" Y="1.604530395508" />
                  <Point X="-3.443508789062" Y="1.62380871582" />
                  <Point X="-3.447783691406" Y="1.633243774414" />
                  <Point X="-3.464190673828" Y="1.664761230469" />
                  <Point X="-3.4775234375" Y="1.690373413086" />
                  <Point X="-3.482799560547" Y="1.699286132812" />
                  <Point X="-3.494291015625" Y="1.71648449707" />
                  <Point X="-3.500506347656" Y="1.724769897461" />
                  <Point X="-3.514418945312" Y="1.741350585938" />
                  <Point X="-3.521500488281" Y="1.748911621094" />
                  <Point X="-3.536442871094" Y="1.763215698242" />
                  <Point X="-3.544303710938" Y="1.769958862305" />
                  <Point X="-3.925864257812" Y="2.062740722656" />
                  <Point X="-4.227614257812" Y="2.294282226563" />
                  <Point X="-4.146663574219" Y="2.432970703125" />
                  <Point X="-4.002293212891" Y="2.680312011719" />
                  <Point X="-3.783627441406" Y="2.961375732422" />
                  <Point X="-3.726338623047" Y="3.035012695312" />
                  <Point X="-3.614533447266" Y="2.970461914062" />
                  <Point X="-3.254156982422" Y="2.7623984375" />
                  <Point X="-3.244917480469" Y="2.757716064453" />
                  <Point X="-3.225984619141" Y="2.749385498047" />
                  <Point X="-3.216291259766" Y="2.745737304688" />
                  <Point X="-3.195648925781" Y="2.739228759766" />
                  <Point X="-3.185615234375" Y="2.736657226562" />
                  <Point X="-3.165327880859" Y="2.732621826172" />
                  <Point X="-3.15507421875" Y="2.731157958984" />
                  <Point X="-3.107878173828" Y="2.727028808594" />
                  <Point X="-3.069524902344" Y="2.723673339844" />
                  <Point X="-3.059173095703" Y="2.723334472656" />
                  <Point X="-3.038492675781" Y="2.723785644531" />
                  <Point X="-3.0281640625" Y="2.724575683594" />
                  <Point X="-3.006705566406" Y="2.727400878906" />
                  <Point X="-2.996525146484" Y="2.729310791016" />
                  <Point X="-2.976433837891" Y="2.734227294922" />
                  <Point X="-2.956998046875" Y="2.741301513672" />
                  <Point X="-2.938447021484" Y="2.750449707031" />
                  <Point X="-2.929420898438" Y="2.755530273438" />
                  <Point X="-2.911166503906" Y="2.767159423828" />
                  <Point X="-2.902746337891" Y="2.773193359375" />
                  <Point X="-2.886614257812" Y="2.786140869141" />
                  <Point X="-2.87890234375" Y="2.793054443359" />
                  <Point X="-2.84540234375" Y="2.826554443359" />
                  <Point X="-2.818178710938" Y="2.853777832031" />
                  <Point X="-2.811265380859" Y="2.861489501953" />
                  <Point X="-2.798318359375" Y="2.877620849609" />
                  <Point X="-2.792284667969" Y="2.886040527344" />
                  <Point X="-2.780655273438" Y="2.904294921875" />
                  <Point X="-2.775574707031" Y="2.913320800781" />
                  <Point X="-2.766426025391" Y="2.931872314453" />
                  <Point X="-2.759351318359" Y="2.951309570312" />
                  <Point X="-2.754434814453" Y="2.971401367188" />
                  <Point X="-2.752524902344" Y="2.981581542969" />
                  <Point X="-2.749699707031" Y="3.003040283203" />
                  <Point X="-2.748909667969" Y="3.013368652344" />
                  <Point X="-2.748458496094" Y="3.034048828125" />
                  <Point X="-2.748797363281" Y="3.044400634766" />
                  <Point X="-2.752926513672" Y="3.091596679688" />
                  <Point X="-2.756281982422" Y="3.129950195312" />
                  <Point X="-2.757745849609" Y="3.140203857422" />
                  <Point X="-2.76178125" Y="3.160491210938" />
                  <Point X="-2.764352783203" Y="3.170524902344" />
                  <Point X="-2.770861328125" Y="3.191167236328" />
                  <Point X="-2.774509521484" Y="3.200860595703" />
                  <Point X="-2.782840087891" Y="3.219793457031" />
                  <Point X="-2.787522460938" Y="3.229032958984" />
                  <Point X="-2.956603515625" Y="3.521889892578" />
                  <Point X="-3.059387207031" Y="3.699916259766" />
                  <Point X="-2.899815429688" Y="3.822258544922" />
                  <Point X="-2.648373779297" Y="4.015036621094" />
                  <Point X="-2.303988037109" Y="4.206370117188" />
                  <Point X="-2.192524902344" Y="4.268296386719" />
                  <Point X="-2.118564453125" Y="4.171909179688" />
                  <Point X="-2.111819335938" Y="4.164046386719" />
                  <Point X="-2.097515625" Y="4.149104980469" />
                  <Point X="-2.089956542969" Y="4.142024902344" />
                  <Point X="-2.073376220703" Y="4.128112304688" />
                  <Point X="-2.065091796875" Y="4.121897949219" />
                  <Point X="-2.047893676758" Y="4.11040625" />
                  <Point X="-2.038980224609" Y="4.105129394531" />
                  <Point X="-1.986451538086" Y="4.077784179688" />
                  <Point X="-1.943764282227" Y="4.0555625" />
                  <Point X="-1.934328735352" Y="4.051287109375" />
                  <Point X="-1.915048339844" Y="4.043789550781" />
                  <Point X="-1.905203613281" Y="4.040567138672" />
                  <Point X="-1.88429699707" Y="4.034965576172" />
                  <Point X="-1.874162475586" Y="4.032834716797" />
                  <Point X="-1.853719360352" Y="4.029688232422" />
                  <Point X="-1.833053222656" Y="4.028785888672" />
                  <Point X="-1.812413818359" Y="4.030138916016" />
                  <Point X="-1.802132080078" Y="4.031378662109" />
                  <Point X="-1.780816772461" Y="4.035136962891" />
                  <Point X="-1.770729003906" Y="4.037489257812" />
                  <Point X="-1.750869506836" Y="4.043277832031" />
                  <Point X="-1.741097900391" Y="4.046714111328" />
                  <Point X="-1.686385498047" Y="4.069377197266" />
                  <Point X="-1.641923706055" Y="4.087793701172" />
                  <Point X="-1.632586425781" Y="4.092272460938" />
                  <Point X="-1.614453491211" Y="4.102220214844" />
                  <Point X="-1.605657836914" Y="4.107689453125" />
                  <Point X="-1.587928100586" Y="4.120103515625" />
                  <Point X="-1.579779541016" Y="4.126498535156" />
                  <Point X="-1.564227294922" Y="4.140137207031" />
                  <Point X="-1.550252319336" Y="4.155387695312" />
                  <Point X="-1.538020751953" Y="4.172069335938" />
                  <Point X="-1.532360351562" Y="4.180744140625" />
                  <Point X="-1.521538574219" Y="4.19948828125" />
                  <Point X="-1.516856201172" Y="4.208727539062" />
                  <Point X="-1.508525634766" Y="4.22766015625" />
                  <Point X="-1.504877441406" Y="4.237353515625" />
                  <Point X="-1.487069458008" Y="4.293833007812" />
                  <Point X="-1.472598144531" Y="4.339729980469" />
                  <Point X="-1.470026611328" Y="4.349763671875" />
                  <Point X="-1.465991088867" Y="4.370051269531" />
                  <Point X="-1.46452722168" Y="4.380305175781" />
                  <Point X="-1.46264074707" Y="4.4018671875" />
                  <Point X="-1.462301757812" Y="4.412219238281" />
                  <Point X="-1.462752929688" Y="4.432898925781" />
                  <Point X="-1.46354309082" Y="4.4432265625" />
                  <Point X="-1.479266235352" Y="4.562655273438" />
                  <Point X="-1.256681396484" Y="4.625060546875" />
                  <Point X="-0.931178100586" Y="4.7163203125" />
                  <Point X="-0.513684936523" Y="4.765181640625" />
                  <Point X="-0.365222106934" Y="4.782557128906" />
                  <Point X="-0.332275695801" Y="4.659599121094" />
                  <Point X="-0.225666244507" Y="4.261727539062" />
                  <Point X="-0.220435317993" Y="4.247107910156" />
                  <Point X="-0.207661773682" Y="4.218916503906" />
                  <Point X="-0.200119155884" Y="4.205344726562" />
                  <Point X="-0.182260925293" Y="4.178618164062" />
                  <Point X="-0.172608306885" Y="4.166455566406" />
                  <Point X="-0.151451248169" Y="4.143866699219" />
                  <Point X="-0.126898002625" Y="4.125026367188" />
                  <Point X="-0.099602958679" Y="4.110436523438" />
                  <Point X="-0.085356712341" Y="4.104260742188" />
                  <Point X="-0.054918586731" Y="4.093927978516" />
                  <Point X="-0.039857292175" Y="4.090155273438" />
                  <Point X="-0.009319890976" Y="4.08511328125" />
                  <Point X="0.021631721497" Y="4.08511328125" />
                  <Point X="0.052168972015" Y="4.090155273438" />
                  <Point X="0.067230415344" Y="4.093927978516" />
                  <Point X="0.097668540955" Y="4.104260742188" />
                  <Point X="0.111914489746" Y="4.110436523438" />
                  <Point X="0.139209686279" Y="4.125026367188" />
                  <Point X="0.16376322937" Y="4.143866699219" />
                  <Point X="0.184920150757" Y="4.166455566406" />
                  <Point X="0.194572906494" Y="4.178617675781" />
                  <Point X="0.212431137085" Y="4.205344238281" />
                  <Point X="0.219973754883" Y="4.218916503906" />
                  <Point X="0.232747146606" Y="4.247107910156" />
                  <Point X="0.237978225708" Y="4.261727539062" />
                  <Point X="0.324613342285" Y="4.5850546875" />
                  <Point X="0.3781902771" Y="4.785006347656" />
                  <Point X="0.543650817871" Y="4.767678222656" />
                  <Point X="0.827876708984" Y="4.737912109375" />
                  <Point X="1.173287841797" Y="4.654519042969" />
                  <Point X="1.453595214844" Y="4.586844726562" />
                  <Point X="1.676790039063" Y="4.505889648438" />
                  <Point X="1.858257080078" Y="4.4400703125" />
                  <Point X="2.075660888672" Y="4.338397949219" />
                  <Point X="2.250451904297" Y="4.256653808594" />
                  <Point X="2.460525634766" Y="4.134264648438" />
                  <Point X="2.629432128906" Y="4.035858886719" />
                  <Point X="2.817779785156" Y="3.901916748047" />
                  <Point X="2.54065234375" Y="3.421917480469" />
                  <Point X="2.06530859375" Y="2.598597900391" />
                  <Point X="2.062373046875" Y="2.593104980469" />
                  <Point X="2.053180908203" Y="2.5734375" />
                  <Point X="2.044182128906" Y="2.549563476562" />
                  <Point X="2.041301757812" Y="2.540598632812" />
                  <Point X="2.02945715332" Y="2.496306152344" />
                  <Point X="2.01983215332" Y="2.460312255859" />
                  <Point X="2.017912719727" Y="2.451465576172" />
                  <Point X="2.013646972656" Y="2.420223388672" />
                  <Point X="2.012755615234" Y="2.383241943359" />
                  <Point X="2.013411254883" Y="2.369579833984" />
                  <Point X="2.018029541016" Y="2.331279541016" />
                  <Point X="2.021782592773" Y="2.300155029297" />
                  <Point X="2.023800537109" Y="2.28903515625" />
                  <Point X="2.029143066406" Y="2.267112304688" />
                  <Point X="2.032467895508" Y="2.256309326172" />
                  <Point X="2.040734008789" Y="2.234220458984" />
                  <Point X="2.045318115234" Y="2.223889160156" />
                  <Point X="2.055680908203" Y="2.203843994141" />
                  <Point X="2.061459472656" Y="2.194130126953" />
                  <Point X="2.085158447266" Y="2.159203857422" />
                  <Point X="2.104417236328" Y="2.130821533203" />
                  <Point X="2.109808105469" Y="2.123633300781" />
                  <Point X="2.130464355469" Y="2.100121826172" />
                  <Point X="2.157597900391" Y="2.075386474609" />
                  <Point X="2.168257568359" Y="2.066981445312" />
                  <Point X="2.20318359375" Y="2.043282470703" />
                  <Point X="2.231566162109" Y="2.024023925781" />
                  <Point X="2.241279296875" Y="2.018245361328" />
                  <Point X="2.261324462891" Y="2.007882446289" />
                  <Point X="2.271656494141" Y="2.003297973633" />
                  <Point X="2.293744628906" Y="1.995032104492" />
                  <Point X="2.304548095703" Y="1.991707275391" />
                  <Point X="2.326471191406" Y="1.986364746094" />
                  <Point X="2.337590820312" Y="1.984346801758" />
                  <Point X="2.375891113281" Y="1.979728393555" />
                  <Point X="2.407015625" Y="1.975975219727" />
                  <Point X="2.416044189453" Y="1.975320922852" />
                  <Point X="2.447575683594" Y="1.975497314453" />
                  <Point X="2.484314453125" Y="1.979822631836" />
                  <Point X="2.497748291016" Y="1.982395996094" />
                  <Point X="2.542040771484" Y="1.994240478516" />
                  <Point X="2.578034667969" Y="2.003865722656" />
                  <Point X="2.583994384766" Y="2.005670532227" />
                  <Point X="2.604404541016" Y="2.013067260742" />
                  <Point X="2.627655029297" Y="2.02357409668" />
                  <Point X="2.636033935547" Y="2.027872558594" />
                  <Point X="3.305097412109" Y="2.414156494141" />
                  <Point X="3.940404785156" Y="2.780951416016" />
                  <Point X="3.943760742188" Y="2.776287597656" />
                  <Point X="4.043950439453" Y="2.637046630859" />
                  <Point X="4.136884765625" Y="2.483472412109" />
                  <Point X="3.798786132812" Y="2.224039794922" />
                  <Point X="3.172951660156" Y="1.743819702148" />
                  <Point X="3.168137695312" Y="1.739868652344" />
                  <Point X="3.152118896484" Y="1.725216186523" />
                  <Point X="3.134668457031" Y="1.706602783203" />
                  <Point X="3.128576660156" Y="1.699422485352" />
                  <Point X="3.09669921875" Y="1.657836181641" />
                  <Point X="3.070794433594" Y="1.624041259766" />
                  <Point X="3.065635742188" Y="1.616602661133" />
                  <Point X="3.049738769531" Y="1.58937097168" />
                  <Point X="3.034762939453" Y="1.555545654297" />
                  <Point X="3.030140136719" Y="1.54267199707" />
                  <Point X="3.018265869141" Y="1.500212036133" />
                  <Point X="3.008616210938" Y="1.465707397461" />
                  <Point X="3.006225097656" Y="1.454661010742" />
                  <Point X="3.002771728516" Y="1.432362670898" />
                  <Point X="3.001709472656" Y="1.421110839844" />
                  <Point X="3.000893310547" Y="1.397540405273" />
                  <Point X="3.001174804688" Y="1.386240966797" />
                  <Point X="3.003077880859" Y="1.363756103516" />
                  <Point X="3.004699462891" Y="1.352570678711" />
                  <Point X="3.014447021484" Y="1.305328613281" />
                  <Point X="3.022368408203" Y="1.266937988281" />
                  <Point X="3.02459765625" Y="1.258235107422" />
                  <Point X="3.034683837891" Y="1.228608154297" />
                  <Point X="3.050286376953" Y="1.19537109375" />
                  <Point X="3.056918457031" Y="1.183525756836" />
                  <Point X="3.083430908203" Y="1.143227905273" />
                  <Point X="3.104976074219" Y="1.11048059082" />
                  <Point X="3.111739013672" Y="1.101424438477" />
                  <Point X="3.126292724609" Y="1.084179443359" />
                  <Point X="3.134083496094" Y="1.075990722656" />
                  <Point X="3.151327392578" Y="1.059901000977" />
                  <Point X="3.160034912109" Y="1.052695678711" />
                  <Point X="3.178244140625" Y="1.039370361328" />
                  <Point X="3.187745849609" Y="1.03325012207" />
                  <Point X="3.226166015625" Y="1.011622741699" />
                  <Point X="3.257387939453" Y="0.994047790527" />
                  <Point X="3.265478759766" Y="0.98998815918" />
                  <Point X="3.294677001953" Y="0.978084777832" />
                  <Point X="3.330274902344" Y="0.96802142334" />
                  <Point X="3.343671142578" Y="0.965257629395" />
                  <Point X="3.395617675781" Y="0.958392150879" />
                  <Point X="3.437831787109" Y="0.952812988281" />
                  <Point X="3.444030029297" Y="0.952199768066" />
                  <Point X="3.465716308594" Y="0.951222839355" />
                  <Point X="3.491217529297" Y="0.952032409668" />
                  <Point X="3.500603515625" Y="0.952797241211" />
                  <Point X="4.136168945312" Y="1.036471069336" />
                  <Point X="4.704703613281" Y="1.11132019043" />
                  <Point X="4.70965234375" Y="1.090991943359" />
                  <Point X="4.75268359375" Y="0.914234130859" />
                  <Point X="4.78387109375" Y="0.713920959473" />
                  <Point X="4.409211914062" Y="0.61353125" />
                  <Point X="3.691991943359" Y="0.421352752686" />
                  <Point X="3.686031738281" Y="0.419544311523" />
                  <Point X="3.665626708984" Y="0.412138000488" />
                  <Point X="3.642381103516" Y="0.401619354248" />
                  <Point X="3.634004394531" Y="0.397316558838" />
                  <Point X="3.582968505859" Y="0.367816864014" />
                  <Point X="3.541494384766" Y="0.343844268799" />
                  <Point X="3.533880859375" Y="0.338945220947" />
                  <Point X="3.508773681641" Y="0.31987020874" />
                  <Point X="3.481993652344" Y="0.294350769043" />
                  <Point X="3.472796875" Y="0.2842265625" />
                  <Point X="3.442175292969" Y="0.245207565308" />
                  <Point X="3.417291015625" Y="0.213499008179" />
                  <Point X="3.410854248047" Y="0.204207855225" />
                  <Point X="3.399130126953" Y="0.184927597046" />
                  <Point X="3.393842529297" Y="0.174938354492" />
                  <Point X="3.384068847656" Y="0.15347467041" />
                  <Point X="3.380005371094" Y="0.142928543091" />
                  <Point X="3.373159179688" Y="0.121427711487" />
                  <Point X="3.370376464844" Y="0.110473167419" />
                  <Point X="3.360169189453" Y="0.057174907684" />
                  <Point X="3.351874267578" Y="0.013862626076" />
                  <Point X="3.350603515625" Y="0.004967842579" />
                  <Point X="3.348584472656" Y="-0.026262140274" />
                  <Point X="3.350280029297" Y="-0.062940639496" />
                  <Point X="3.351874267578" Y="-0.076422668457" />
                  <Point X="3.362081542969" Y="-0.129720932007" />
                  <Point X="3.370376464844" Y="-0.173033355713" />
                  <Point X="3.373159179688" Y="-0.183988052368" />
                  <Point X="3.380005371094" Y="-0.205488739014" />
                  <Point X="3.384068847656" Y="-0.216034713745" />
                  <Point X="3.393842529297" Y="-0.237498245239" />
                  <Point X="3.399130371094" Y="-0.247488235474" />
                  <Point X="3.410854736328" Y="-0.26676864624" />
                  <Point X="3.417291259766" Y="-0.276059051514" />
                  <Point X="3.447912841797" Y="-0.315078216553" />
                  <Point X="3.472797119141" Y="-0.34678692627" />
                  <Point X="3.478718505859" Y="-0.353633666992" />
                  <Point X="3.501139160156" Y="-0.375805084229" />
                  <Point X="3.53017578125" Y="-0.39872467041" />
                  <Point X="3.541494628906" Y="-0.406404327393" />
                  <Point X="3.592530761719" Y="-0.435904022217" />
                  <Point X="3.634004638672" Y="-0.459876617432" />
                  <Point X="3.639495361328" Y="-0.462814575195" />
                  <Point X="3.659156738281" Y="-0.472016143799" />
                  <Point X="3.68302734375" Y="-0.481027587891" />
                  <Point X="3.691991943359" Y="-0.48391293335" />
                  <Point X="4.274834472656" Y="-0.640085144043" />
                  <Point X="4.784876953125" Y="-0.776750488281" />
                  <Point X="4.76161328125" Y="-0.931052368164" />
                  <Point X="4.727801757812" Y="-1.079219604492" />
                  <Point X="4.268555175781" Y="-1.018758483887" />
                  <Point X="3.436781982422" Y="-0.909253723145" />
                  <Point X="3.428624511719" Y="-0.908535705566" />
                  <Point X="3.400098388672" Y="-0.908042480469" />
                  <Point X="3.366721435547" Y="-0.910840881348" />
                  <Point X="3.354480957031" Y="-0.912676208496" />
                  <Point X="3.254315429688" Y="-0.934447814941" />
                  <Point X="3.172916748047" Y="-0.952140075684" />
                  <Point X="3.157874511719" Y="-0.956742492676" />
                  <Point X="3.128754150391" Y="-0.968366882324" />
                  <Point X="3.114676269531" Y="-0.975388977051" />
                  <Point X="3.086849609375" Y="-0.992281311035" />
                  <Point X="3.074124267578" Y="-1.001530151367" />
                  <Point X="3.050374267578" Y="-1.022000854492" />
                  <Point X="3.039349609375" Y="-1.03322265625" />
                  <Point X="2.978805664062" Y="-1.106037841797" />
                  <Point X="2.92960546875" Y="-1.16521081543" />
                  <Point X="2.921325927734" Y="-1.17684765625" />
                  <Point X="2.906605224609" Y="-1.201230224609" />
                  <Point X="2.9001640625" Y="-1.213976074219" />
                  <Point X="2.888820800781" Y="-1.241361328125" />
                  <Point X="2.884362792969" Y="-1.254928222656" />
                  <Point X="2.877531005859" Y="-1.282577880859" />
                  <Point X="2.875157226562" Y="-1.29666027832" />
                  <Point X="2.866479736328" Y="-1.390959350586" />
                  <Point X="2.859428222656" Y="-1.467590576172" />
                  <Point X="2.859288574219" Y="-1.483320800781" />
                  <Point X="2.861607177734" Y="-1.514589233398" />
                  <Point X="2.864065429688" Y="-1.530127319336" />
                  <Point X="2.871796875" Y="-1.561748291016" />
                  <Point X="2.876785888672" Y="-1.576668212891" />
                  <Point X="2.889157470703" Y="-1.605479614258" />
                  <Point X="2.896540039062" Y="-1.619371337891" />
                  <Point X="2.951973144531" Y="-1.70559387207" />
                  <Point X="2.997020263672" Y="-1.775661865234" />
                  <Point X="3.001741943359" Y="-1.782353393555" />
                  <Point X="3.019793945312" Y="-1.804450317383" />
                  <Point X="3.043489501953" Y="-1.828120361328" />
                  <Point X="3.052796142578" Y="-1.83627746582" />
                  <Point X="3.593677734375" Y="-2.251311035156" />
                  <Point X="4.087170654297" Y="-2.629981689453" />
                  <Point X="4.045487548828" Y="-2.697431152344" />
                  <Point X="4.001274414063" Y="-2.760251953125" />
                  <Point X="3.589260986328" Y="-2.522375732422" />
                  <Point X="2.848454589844" Y="-2.094670654297" />
                  <Point X="2.841191894531" Y="-2.090885253906" />
                  <Point X="2.815026123047" Y="-2.079513183594" />
                  <Point X="2.783119140625" Y="-2.069325927734" />
                  <Point X="2.771108398438" Y="-2.066337646484" />
                  <Point X="2.651895263672" Y="-2.044807861328" />
                  <Point X="2.555018066406" Y="-2.027311889648" />
                  <Point X="2.539359130859" Y="-2.025807495117" />
                  <Point X="2.508006835938" Y="-2.025403198242" />
                  <Point X="2.492313476562" Y="-2.026503540039" />
                  <Point X="2.460140625" Y="-2.031461303711" />
                  <Point X="2.444844482422" Y="-2.035136108398" />
                  <Point X="2.415068603516" Y="-2.044959960938" />
                  <Point X="2.400588867188" Y="-2.051108642578" />
                  <Point X="2.301552001953" Y="-2.103230957031" />
                  <Point X="2.221070800781" Y="-2.145587646484" />
                  <Point X="2.208968505859" Y="-2.153170166016" />
                  <Point X="2.186037353516" Y="-2.170063476562" />
                  <Point X="2.175208496094" Y="-2.179374267578" />
                  <Point X="2.154248779297" Y="-2.200334228516" />
                  <Point X="2.144938476562" Y="-2.211162841797" />
                  <Point X="2.128045898438" Y="-2.234093261719" />
                  <Point X="2.120463623047" Y="-2.246195068359" />
                  <Point X="2.068341308594" Y="-2.345231689453" />
                  <Point X="2.025984741211" Y="-2.425713134766" />
                  <Point X="2.0198359375" Y="-2.440192871094" />
                  <Point X="2.010012084961" Y="-2.46996875" />
                  <Point X="2.006337280273" Y="-2.485264892578" />
                  <Point X="2.001379516602" Y="-2.5174375" />
                  <Point X="2.000279174805" Y="-2.533130859375" />
                  <Point X="2.00068347168" Y="-2.564483154297" />
                  <Point X="2.002187866211" Y="-2.580142089844" />
                  <Point X="2.023717651367" Y="-2.699354980469" />
                  <Point X="2.041213378906" Y="-2.796232421875" />
                  <Point X="2.043015014648" Y="-2.804221435547" />
                  <Point X="2.051236083984" Y="-2.83154296875" />
                  <Point X="2.064069580078" Y="-2.862479736328" />
                  <Point X="2.069546630859" Y="-2.873578613281" />
                  <Point X="2.417117431641" Y="-3.475588623047" />
                  <Point X="2.735893066406" Y="-4.027724365234" />
                  <Point X="2.723754394531" Y="-4.036083251953" />
                  <Point X="2.399218505859" Y="-3.613140625" />
                  <Point X="1.833914550781" Y="-2.876422851562" />
                  <Point X="1.828653442383" Y="-2.870146240234" />
                  <Point X="1.808831542969" Y="-2.849626953125" />
                  <Point X="1.783252075195" Y="-2.828004882812" />
                  <Point X="1.773298828125" Y="-2.820647216797" />
                  <Point X="1.65572265625" Y="-2.745056640625" />
                  <Point X="1.560175415039" Y="-2.683628417969" />
                  <Point X="1.546283935547" Y="-2.676245849609" />
                  <Point X="1.517472900391" Y="-2.663874511719" />
                  <Point X="1.502553588867" Y="-2.658885742188" />
                  <Point X="1.470932617188" Y="-2.651154052734" />
                  <Point X="1.45539453125" Y="-2.648695800781" />
                  <Point X="1.424125488281" Y="-2.646376953125" />
                  <Point X="1.40839453125" Y="-2.646516357422" />
                  <Point X="1.27980480957" Y="-2.658349365234" />
                  <Point X="1.175307739258" Y="-2.667965332031" />
                  <Point X="1.161225097656" Y="-2.670339111328" />
                  <Point X="1.133575439453" Y="-2.677170898438" />
                  <Point X="1.120008422852" Y="-2.68162890625" />
                  <Point X="1.092623168945" Y="-2.692972167969" />
                  <Point X="1.079877563477" Y="-2.699413330078" />
                  <Point X="1.055495361328" Y="-2.714133789062" />
                  <Point X="1.043858764648" Y="-2.722413085938" />
                  <Point X="0.944565246582" Y="-2.804972412109" />
                  <Point X="0.863875244141" Y="-2.872063476562" />
                  <Point X="0.852653320312" Y="-2.883088134766" />
                  <Point X="0.832182434082" Y="-2.906838134766" />
                  <Point X="0.82293359375" Y="-2.919563720703" />
                  <Point X="0.806041137695" Y="-2.947390380859" />
                  <Point X="0.799019287109" Y="-2.961467529297" />
                  <Point X="0.787394470215" Y="-2.990588134766" />
                  <Point X="0.782791931152" Y="-3.005631347656" />
                  <Point X="0.75310345459" Y="-3.142220947266" />
                  <Point X="0.728977661133" Y="-3.253219238281" />
                  <Point X="0.727584716797" Y="-3.261289306641" />
                  <Point X="0.724724487305" Y="-3.289677734375" />
                  <Point X="0.7247421875" Y="-3.323170410156" />
                  <Point X="0.725554992676" Y="-3.335520019531" />
                  <Point X="0.824053955078" Y="-4.083693115234" />
                  <Point X="0.833091674805" Y="-4.152341308594" />
                  <Point X="0.655064941406" Y="-3.487936523438" />
                  <Point X="0.652606262207" Y="-3.480124511719" />
                  <Point X="0.642145141602" Y="-3.453580322266" />
                  <Point X="0.62678692627" Y="-3.423815673828" />
                  <Point X="0.620407409668" Y="-3.413210205078" />
                  <Point X="0.53008215332" Y="-3.283068359375" />
                  <Point X="0.456679962158" Y="-3.177309814453" />
                  <Point X="0.446670654297" Y="-3.165172851562" />
                  <Point X="0.424786804199" Y="-3.142717529297" />
                  <Point X="0.41291229248" Y="-3.132399169922" />
                  <Point X="0.386657165527" Y="-3.113155273438" />
                  <Point X="0.373242767334" Y="-3.104937988281" />
                  <Point X="0.345241607666" Y="-3.090829589844" />
                  <Point X="0.330654724121" Y="-3.084938476562" />
                  <Point X="0.190881408691" Y="-3.041557861328" />
                  <Point X="0.077295753479" Y="-3.006305175781" />
                  <Point X="0.063376476288" Y="-3.003109130859" />
                  <Point X="0.035216896057" Y="-2.99883984375" />
                  <Point X="0.02097659111" Y="-2.997766601562" />
                  <Point X="-0.008664910316" Y="-2.997766601562" />
                  <Point X="-0.02290521431" Y="-2.998840087891" />
                  <Point X="-0.051064647675" Y="-3.003109375" />
                  <Point X="-0.064983627319" Y="-3.006305175781" />
                  <Point X="-0.20475692749" Y="-3.049685546875" />
                  <Point X="-0.318342590332" Y="-3.084938476562" />
                  <Point X="-0.332929473877" Y="-3.090829589844" />
                  <Point X="-0.360930786133" Y="-3.104937988281" />
                  <Point X="-0.374345336914" Y="-3.113155273438" />
                  <Point X="-0.400600463867" Y="-3.132399169922" />
                  <Point X="-0.412475128174" Y="-3.142717773438" />
                  <Point X="-0.434358978271" Y="-3.165173095703" />
                  <Point X="-0.444367980957" Y="-3.177309814453" />
                  <Point X="-0.53469342041" Y="-3.307451416016" />
                  <Point X="-0.608095458984" Y="-3.413210205078" />
                  <Point X="-0.612469970703" Y="-3.420131835938" />
                  <Point X="-0.62597668457" Y="-3.445265136719" />
                  <Point X="-0.638777893066" Y="-3.476215820312" />
                  <Point X="-0.642753112793" Y="-3.487936523438" />
                  <Point X="-0.822341003418" Y="-4.158168457031" />
                  <Point X="-0.985425231934" Y="-4.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.128850363799" Y="-4.637170666063" />
                  <Point X="-0.961846431368" Y="-4.678809423003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.119464809257" Y="-4.541602452844" />
                  <Point X="-0.937254859887" Y="-4.587032495619" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.6756800211" Y="-4.055686127293" />
                  <Point X="-2.462115703019" Y="-4.108933692199" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.134837013354" Y="-4.439861437109" />
                  <Point X="-0.912663288406" Y="-4.495255568235" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.884558906491" Y="-3.905698477174" />
                  <Point X="-2.397262520156" Y="-4.027195111972" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.155328454324" Y="-4.336844052269" />
                  <Point X="-0.888071716925" Y="-4.403478640851" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.956753988612" Y="-3.789789926745" />
                  <Point X="-2.301133714375" Y="-3.953254420345" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.175819966083" Y="-4.233826649778" />
                  <Point X="-0.863480145444" Y="-4.311701713467" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.907339772259" Y="-3.704201979833" />
                  <Point X="-2.194422682526" Y="-3.881952174009" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.210380048834" Y="-4.12730155858" />
                  <Point X="-0.838888573963" Y="-4.219924786082" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.857925555905" Y="-3.61861403292" />
                  <Point X="-2.08663249117" Y="-3.810918992357" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.329530056615" Y="-3.999685830313" />
                  <Point X="-0.814297016987" Y="-4.128147855082" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.808511339551" Y="-3.533026086007" />
                  <Point X="-1.794571913687" Y="-3.785829578062" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.485521839752" Y="-3.862884415775" />
                  <Point X="-0.789705489851" Y="-4.036370916641" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.759097123197" Y="-3.447438139095" />
                  <Point X="-0.765113962715" Y="-3.944593978201" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.709682881216" Y="-3.361850198572" />
                  <Point X="-0.740522435578" Y="-3.85281703976" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.815532721867" Y="-2.988222571569" />
                  <Point X="-3.764190361674" Y="-3.001023659698" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.660268635062" Y="-3.276262259089" />
                  <Point X="-0.715930908442" Y="-3.76104010132" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.831631179452" Y="-4.14689066597" />
                  <Point X="0.832399296214" Y="-4.147082178988" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.907511566576" Y="-2.867381375126" />
                  <Point X="-3.645754543971" Y="-2.932644730802" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.610854388909" Y="-3.190674319606" />
                  <Point X="-0.691339381306" Y="-3.669263162879" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.803518606371" Y="-4.041973119481" />
                  <Point X="0.81907194092" Y="-4.045851001322" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.999490344581" Y="-2.746540195314" />
                  <Point X="-3.527318726268" Y="-2.864265801907" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.561440142756" Y="-3.105086380123" />
                  <Point X="-0.66674785417" Y="-3.577486224439" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.775406033291" Y="-3.937055572992" />
                  <Point X="0.805744589364" Y="-3.944619824587" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.069227169354" Y="-2.63124455728" />
                  <Point X="-3.408882908565" Y="-2.795886873012" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.512025896603" Y="-3.01949844064" />
                  <Point X="-0.642010084635" Y="-3.485745748323" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.747293460211" Y="-3.832138026503" />
                  <Point X="0.792417237808" Y="-3.843388647852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.137813345681" Y="-2.516235808126" />
                  <Point X="-3.290447090863" Y="-2.727507944116" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.46261165045" Y="-2.933910501157" />
                  <Point X="-0.598086316902" Y="-3.398788878821" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.71918088713" Y="-3.727220480014" />
                  <Point X="0.779089886252" Y="-3.742157471117" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.145972946755" Y="-2.416293096298" />
                  <Point X="-3.172011318902" Y="-2.659129003816" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.413197404297" Y="-2.848322561674" />
                  <Point X="-0.54015733085" Y="-3.315323902432" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.69106831405" Y="-3.622302933524" />
                  <Point X="0.765762534696" Y="-3.640926294382" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.049668689021" Y="-2.342396149756" />
                  <Point X="-3.053575559619" Y="-2.590750060355" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.363783158144" Y="-2.762734622191" />
                  <Point X="-0.482228109574" Y="-3.231858984691" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.66295574097" Y="-3.517385387035" />
                  <Point X="0.75243518314" Y="-3.539695117647" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.719121055837" Y="-4.030044978506" />
                  <Point X="2.72896051669" Y="-4.03249823163" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.953364431287" Y="-2.268499203214" />
                  <Point X="-2.935139800335" Y="-2.522371116894" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.322252994715" Y="-2.675180960109" />
                  <Point X="-0.419236932178" Y="-3.14965615436" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.616763552121" Y="-3.407960086054" />
                  <Point X="0.739107831584" Y="-3.438463940912" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.626219678378" Y="-3.908973768815" />
                  <Point X="2.674245723435" Y="-3.920948006714" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.857060173553" Y="-2.194602256672" />
                  <Point X="-2.816704041052" Y="-2.453992173434" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.311733103013" Y="-2.579895568909" />
                  <Point X="-0.303986786548" Y="-3.080482948209" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.534590250153" Y="-3.289563685999" />
                  <Point X="0.725780480028" Y="-3.337232764177" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.53331830092" Y="-3.787902559124" />
                  <Point X="2.608212973337" Y="-3.806575898221" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.760755913276" Y="-2.120705310763" />
                  <Point X="-2.698268281769" Y="-2.385613229973" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.351100916998" Y="-2.472171775683" />
                  <Point X="-0.129054157171" Y="-3.026190256535" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.45140646685" Y="-3.170915344651" />
                  <Point X="0.731678122645" Y="-3.240794916844" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.440416923462" Y="-3.666831349433" />
                  <Point X="2.542180223238" Y="-3.692203789729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.664451614331" Y="-2.046808374496" />
                  <Point X="0.751864808408" Y="-3.147919728101" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.347515604169" Y="-3.545760154244" />
                  <Point X="2.47614747314" Y="-3.577831681236" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.568147315386" Y="-1.972911438229" />
                  <Point X="0.77205169976" Y="-3.055044590618" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.254614331223" Y="-3.42468897061" />
                  <Point X="2.410114717114" Y="-3.463459571266" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.471843016441" Y="-1.899014501962" />
                  <Point X="0.798150651126" Y="-2.96364349525" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.161713058278" Y="-3.303617786977" />
                  <Point X="2.344081911124" Y="-3.349087448838" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.375538717495" Y="-1.825117565695" />
                  <Point X="0.855709049517" Y="-2.880086120979" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.068811785332" Y="-3.182546603344" />
                  <Point X="2.278049105133" Y="-3.23471532641" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.27923441855" Y="-1.751220629428" />
                  <Point X="0.945157632287" Y="-2.80447986269" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.975910512387" Y="-3.06147541971" />
                  <Point X="2.212016299142" Y="-3.120343203982" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.182930119605" Y="-1.677323693161" />
                  <Point X="1.035746740932" Y="-2.72915796944" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.883009239441" Y="-2.940404236077" />
                  <Point X="2.145983493151" Y="-3.005971081554" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.681594453124" Y="-1.205756413164" />
                  <Point X="-4.462003912884" Y="-1.260506484006" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.08662582066" Y="-1.603426756894" />
                  <Point X="1.180930108512" Y="-2.667447953736" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.759311421639" Y="-2.81165461142" />
                  <Point X="2.07995068716" Y="-2.891598959126" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.708304404379" Y="-1.101188579574" />
                  <Point X="-4.205013602534" Y="-1.226673070047" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.004081224864" Y="-1.526099141321" />
                  <Point X="2.038906205017" Y="-2.783457125577" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.735014362044" Y="-0.996620744385" />
                  <Point X="-3.94802329512" Y="-1.192839655356" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.960689007792" Y="-1.439009741355" />
                  <Point X="2.020390535067" Y="-2.680932355779" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.752158192725" Y="-0.894438012532" />
                  <Point X="-3.691033013727" Y="-1.159006234178" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.949211431025" Y="-1.343963127859" />
                  <Point X="2.002024819883" Y="-2.578444973903" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.766678976011" Y="-0.792909279847" />
                  <Point X="-3.434042732333" Y="-1.125172813" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.977498398515" Y="-1.23900209996" />
                  <Point X="2.00716529274" Y="-2.481818342946" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.781199759298" Y="-0.691380547162" />
                  <Point X="2.043250255781" Y="-2.392907039925" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.665643178705" Y="-0.622283743829" />
                  <Point X="2.088801370636" Y="-2.306355913631" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.476366943046" Y="-0.571567314863" />
                  <Point X="2.137917282565" Y="-2.220693591072" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.772719311828" Y="-2.628295516072" />
                  <Point X="4.046090970815" Y="-2.696454725841" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.287090692841" Y="-0.520850889524" />
                  <Point X="2.223739436208" Y="-2.144183162451" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.474239033715" Y="-2.455967729653" />
                  <Point X="4.046257591807" Y="-2.598587974332" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.097814387942" Y="-0.470134477822" />
                  <Point X="2.34997163448" Y="-2.077748089552" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.175758772879" Y="-2.283639947543" />
                  <Point X="3.857245230926" Y="-2.453553605092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.908538083043" Y="-0.419418066119" />
                  <Point X="2.534064153454" Y="-2.025739214858" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.877278512044" Y="-2.111312165433" />
                  <Point X="3.668232870045" Y="-2.308519235853" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.719261778145" Y="-0.368701654417" />
                  <Point X="3.479220626493" Y="-2.163484895867" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.529985473246" Y="-0.317985242715" />
                  <Point X="3.290208459367" Y="-2.018450574936" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.399849088751" Y="-0.25252359277" />
                  <Point X="3.101196292241" Y="-1.873416254005" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.33093034622" Y="-0.171798670415" />
                  <Point X="2.977015891408" Y="-1.744546307885" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.299990050132" Y="-0.081604657858" />
                  <Point X="2.902053980471" Y="-1.627947909554" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.299371289745" Y="0.016149362639" />
                  <Point X="2.862490962209" Y="-1.520175446436" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.339104092911" Y="0.123964157888" />
                  <Point X="2.863573982357" Y="-1.422537178898" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.283943351115" Y="0.457447337932" />
                  <Point X="2.872381455504" Y="-1.3268248338" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.772806241846" Y="0.67724284093" />
                  <Point X="2.891954202379" Y="-1.2337965729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.758833762096" Y="0.771667405247" />
                  <Point X="2.943334577616" Y="-1.148698844455" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.744861414222" Y="0.866092002445" />
                  <Point X="3.010763652456" Y="-1.06760250623" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.730545284189" Y="0.960430885124" />
                  <Point X="3.091072879502" Y="-0.989717550631" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.705693110116" Y="1.052142836984" />
                  <Point X="3.258467315987" Y="-0.933545376379" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.680840936043" Y="1.143854788844" />
                  <Point X="3.684562734212" Y="-0.941874601237" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.655988761969" Y="1.235566740705" />
                  <Point X="4.516581264415" Y="-1.051411824913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.551560719313" Y="1.307438200177" />
                  <Point X="4.743574877841" Y="-1.010099394418" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.7456499356" Y="1.204410368792" />
                  <Point X="4.763700414081" Y="-0.917208959387" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.564209312623" Y="1.257080435419" />
                  <Point X="4.777926995262" Y="-0.822847749672" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.483451919765" Y="1.334853650731" />
                  <Point X="3.572703310125" Y="-0.424443440489" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.442283558412" Y="1.422497520203" />
                  <Point X="3.428733752242" Y="-0.290639503364" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.421487503067" Y="1.515220776044" />
                  <Point X="3.371772514771" Y="-0.178529176997" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.44127422046" Y="1.618062453563" />
                  <Point X="3.351781305483" Y="-0.075636513923" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.506839942852" Y="1.732318118971" />
                  <Point X="3.353407109152" Y="0.021866422483" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.678682860845" Y="1.873071665305" />
                  <Point X="3.371587801235" Y="0.115241761624" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.867695234853" Y="2.018106037817" />
                  <Point X="3.410406464074" Y="0.203471476734" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.056707221657" Y="2.163140313788" />
                  <Point X="3.473980082644" Y="0.28552908817" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.222835691738" Y="2.302469088237" />
                  <Point X="3.569004079855" Y="0.359745239512" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.172948009057" Y="2.387938986736" />
                  <Point X="3.702888351527" Y="0.424272436232" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.123060267244" Y="2.473408870492" />
                  <Point X="3.892164622632" Y="0.47498885636" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.073172459581" Y="2.558878737829" />
                  <Point X="4.081440893738" Y="0.525705276488" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.023284651919" Y="2.644348605167" />
                  <Point X="4.270717164843" Y="0.576421696616" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.965340280667" Y="2.727809745595" />
                  <Point X="4.459993411716" Y="0.627138122786" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.901543310235" Y="2.809811669158" />
                  <Point X="3.167358204479" Y="1.047336572199" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.530662641051" Y="0.956754602604" />
                  <Point X="4.6492695925" Y="0.677854565433" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.837746339803" Y="2.891813592721" />
                  <Point X="3.065375014542" Y="1.170672132058" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.787652890372" Y="0.99058803178" />
                  <Point X="4.779289932722" Y="0.743345148465" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.773949407055" Y="2.97381552568" />
                  <Point X="-3.503585970515" Y="2.906406350005" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.920710668778" Y="2.761079215117" />
                  <Point X="3.019678642098" Y="1.279973812125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.044643139692" Y="1.024421460955" />
                  <Point X="4.76343058667" Y="0.845207622331" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.834471114907" Y="2.837485574172" />
                  <Point X="3.001498636449" Y="1.382414891413" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.301633338869" Y="1.058254902632" />
                  <Point X="4.744503556466" Y="0.94783495576" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.772330001723" Y="2.919900349316" />
                  <Point X="3.011973324743" Y="1.477711553089" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.558623510308" Y="1.092088351225" />
                  <Point X="4.719127758145" Y="1.052070147664" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.749014704282" Y="3.011995487557" />
                  <Point X="3.040501444178" Y="1.568506988833" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.754651111736" Y="3.111309096559" />
                  <Point X="3.093203150012" Y="1.65327527256" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.781086693988" Y="3.215808522274" />
                  <Point X="3.161807660614" Y="1.734078541733" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.845718429713" Y="3.329831318651" />
                  <Point X="2.147061722043" Y="2.084991414779" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.527084790853" Y="1.990241021998" />
                  <Point X="3.256941380087" Y="1.808267336242" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.911751211803" Y="3.44420343512" />
                  <Point X="2.054589217978" Y="2.205955694324" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.676089044305" Y="2.050998383858" />
                  <Point X="3.353245664519" Y="1.882164276128" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.977784019178" Y="3.558575557893" />
                  <Point X="2.020304612123" Y="2.312412101418" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.794524844142" Y="2.119377317208" />
                  <Point X="3.44954994895" Y="1.956061216014" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.0438168801" Y="3.672947694017" />
                  <Point X="2.013449478524" Y="2.412029572975" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.912960643978" Y="2.187756250558" />
                  <Point X="3.545854233382" Y="2.029958155899" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.985745051652" Y="3.756377055797" />
                  <Point X="2.031874063532" Y="2.50534410278" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.031396443815" Y="2.256135183907" />
                  <Point X="3.642158517814" Y="2.103855095785" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.889380444186" Y="3.830258955461" />
                  <Point X="2.06357236954" Y="2.595349122238" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.149832243652" Y="2.324514117257" />
                  <Point X="3.738462802245" Y="2.177752035671" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.793015824401" Y="3.904140852054" />
                  <Point X="2.112864680159" Y="2.680967463664" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.268268043489" Y="2.392893050607" />
                  <Point X="3.834767066881" Y="2.251648980492" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.696651204616" Y="3.978022748646" />
                  <Point X="2.162278900528" Y="2.766555409576" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.386703830954" Y="2.461271987041" />
                  <Point X="3.931071298329" Y="2.325545933588" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.58767421227" Y="4.048760027577" />
                  <Point X="2.211693120898" Y="2.852143355487" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.505139612835" Y="2.529650924868" />
                  <Point X="4.027375529777" Y="2.399442886684" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.466034953363" Y="4.116340248874" />
                  <Point X="2.261107341267" Y="2.937731301399" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.623575394717" Y="2.598029862695" />
                  <Point X="4.123679761225" Y="2.47333983978" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.344395694457" Y="4.183920470172" />
                  <Point X="-2.04669129267" Y="4.109694426237" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.764441994693" Y="4.039321772469" />
                  <Point X="2.310521561636" Y="3.02331924731" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.742011176598" Y="2.666408800521" />
                  <Point X="4.076676048026" Y="2.582967476506" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.222756154325" Y="4.251500621353" />
                  <Point X="-2.16943608844" Y="4.238206435814" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.617444334135" Y="4.100579434127" />
                  <Point X="2.359935782006" Y="3.108907193222" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.860446958479" Y="2.734787738348" />
                  <Point X="3.998364254024" Y="2.700401094492" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.534307872986" Y="4.177759481094" />
                  <Point X="2.409350002375" Y="3.194495139133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.495822417128" Y="4.266072274034" />
                  <Point X="2.458764222744" Y="3.280083085045" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.468551171402" Y="4.357181083591" />
                  <Point X="2.508178443114" Y="3.365671030956" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.464987902269" Y="4.454200955603" />
                  <Point X="-0.111299253088" Y="4.116688468231" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.015341904827" Y="4.08511328125" />
                  <Point X="2.557592650297" Y="3.451258980155" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.47831528851" Y="4.555432140985" />
                  <Point X="-0.217727801651" Y="4.241132380478" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.165395171536" Y="4.14560909473" />
                  <Point X="2.607006832204" Y="3.536846935657" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.30761577483" Y="4.610780266941" />
                  <Point X="-0.248433625738" Y="4.346696497061" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.224449024665" Y="4.228793610257" />
                  <Point X="2.656421014111" Y="3.622434891158" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.122776221923" Y="4.662602885157" />
                  <Point X="-0.276546220731" Y="4.451614049014" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.25345052122" Y="4.31947101983" />
                  <Point X="2.705835196018" Y="3.708022846659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.93793656024" Y="4.714425476252" />
                  <Point X="-0.304658815724" Y="4.556531600966" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.278042040866" Y="4.411247960138" />
                  <Point X="2.755249377925" Y="3.793610802161" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.67370566233" Y="4.746453608974" />
                  <Point X="-0.332771408892" Y="4.661449152464" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.302633560511" Y="4.503024900446" />
                  <Point X="2.804663559831" Y="3.879198757662" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.406462010064" Y="4.777730577671" />
                  <Point X="-0.36088390041" Y="4.766366678617" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.327225086116" Y="4.594801839268" />
                  <Point X="2.662046331464" Y="4.01266552117" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.351816661875" Y="4.686578765586" />
                  <Point X="2.380849445598" Y="4.180684073917" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.376408237633" Y="4.778355691903" />
                  <Point X="2.001067939949" Y="4.373282533025" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="0.001625976562" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978485351562" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.692532226562" Y="-4.361870117188" />
                  <Point X="0.471539031982" Y="-3.537112304688" />
                  <Point X="0.464318664551" Y="-3.521544189453" />
                  <Point X="0.373993225098" Y="-3.39140234375" />
                  <Point X="0.300591033936" Y="-3.285643798828" />
                  <Point X="0.274335754395" Y="-3.266399902344" />
                  <Point X="0.134562316895" Y="-3.223019287109" />
                  <Point X="0.020976625443" Y="-3.187766601562" />
                  <Point X="-0.008664908409" Y="-3.187766601562" />
                  <Point X="-0.148438186646" Y="-3.231146972656" />
                  <Point X="-0.262023895264" Y="-3.266399902344" />
                  <Point X="-0.288279144287" Y="-3.285643798828" />
                  <Point X="-0.378604614258" Y="-3.415785400391" />
                  <Point X="-0.45200680542" Y="-3.521544189453" />
                  <Point X="-0.459227142334" Y="-3.537112304688" />
                  <Point X="-0.638815185547" Y="-4.207344238281" />
                  <Point X="-0.84774395752" Y="-4.987077148438" />
                  <Point X="-0.967342285156" Y="-4.963862304688" />
                  <Point X="-1.100246459961" Y="-4.938065429688" />
                  <Point X="-1.261917602539" Y="-4.89646875" />
                  <Point X="-1.35158972168" Y="-4.873396972656" />
                  <Point X="-1.335752807617" Y="-4.753103515625" />
                  <Point X="-1.309150146484" Y="-4.551038574219" />
                  <Point X="-1.309683227539" Y="-4.5347578125" />
                  <Point X="-1.343075073242" Y="-4.366885742188" />
                  <Point X="-1.370210693359" Y="-4.230466308594" />
                  <Point X="-1.386282714844" Y="-4.20262890625" />
                  <Point X="-1.514968383789" Y="-4.089774169922" />
                  <Point X="-1.619543457031" Y="-3.998063964844" />
                  <Point X="-1.649240722656" Y="-3.985762939453" />
                  <Point X="-1.82003527832" Y="-3.974568359375" />
                  <Point X="-1.958829833984" Y="-3.965471435547" />
                  <Point X="-1.989878540039" Y="-3.973791015625" />
                  <Point X="-2.132193603516" Y="-4.068883056641" />
                  <Point X="-2.247844970703" Y="-4.146159179688" />
                  <Point X="-2.259734130859" Y="-4.157294433594" />
                  <Point X="-2.360558837891" Y="-4.288691894531" />
                  <Point X="-2.457095214844" Y="-4.414500488281" />
                  <Point X="-2.661025634766" Y="-4.288231933594" />
                  <Point X="-2.855831054688" Y="-4.16761328125" />
                  <Point X="-3.079701660156" Y="-3.995240234375" />
                  <Point X="-3.228581054688" Y="-3.880608154297" />
                  <Point X="-2.916724609375" Y="-3.340456787109" />
                  <Point X="-2.506033691406" Y="-2.629119628906" />
                  <Point X="-2.499762939453" Y="-2.597592773438" />
                  <Point X="-2.513980224609" Y="-2.568763671875" />
                  <Point X="-2.531328857422" Y="-2.551415039062" />
                  <Point X="-2.560157714844" Y="-2.537198730469" />
                  <Point X="-2.59168359375" Y="-2.543469726562" />
                  <Point X="-3.169745605469" Y="-2.877214111328" />
                  <Point X="-3.842959472656" Y="-3.265894042969" />
                  <Point X="-4.007794433594" Y="-3.049334716797" />
                  <Point X="-4.161703613281" Y="-2.847129394531" />
                  <Point X="-4.32219921875" Y="-2.578002685547" />
                  <Point X="-4.431020019531" Y="-2.395527099609" />
                  <Point X="-3.882362304688" Y="-1.97452734375" />
                  <Point X="-3.163786865234" Y="-1.423144897461" />
                  <Point X="-3.145822021484" Y="-1.396013427734" />
                  <Point X="-3.138117431641" Y="-1.366266235352" />
                  <Point X="-3.140326171875" Y="-1.334595703125" />
                  <Point X="-3.161158691406" Y="-1.310639404297" />
                  <Point X="-3.187641357422" Y="-1.295052856445" />
                  <Point X="-3.219529296875" Y="-1.288571044922" />
                  <Point X="-3.949277832031" Y="-1.384644287109" />
                  <Point X="-4.803283203125" Y="-1.497076293945" />
                  <Point X="-4.867197753906" Y="-1.246853149414" />
                  <Point X="-4.927393066406" Y="-1.011191589355" />
                  <Point X="-4.969855957031" Y="-0.714293640137" />
                  <Point X="-4.998396484375" Y="-0.514742126465" />
                  <Point X="-4.376029785156" Y="-0.347979492188" />
                  <Point X="-3.557463867188" Y="-0.128645431519" />
                  <Point X="-3.526584960938" Y="-0.110798538208" />
                  <Point X="-3.502324462891" Y="-0.090645362854" />
                  <Point X="-3.489795410156" Y="-0.059464000702" />
                  <Point X="-3.483400878906" Y="-0.031280042648" />
                  <Point X="-3.490751464844" Y="-1.5127151E-05" />
                  <Point X="-3.502324462891" Y="0.028085283279" />
                  <Point X="-3.529453613281" Y="0.050229343414" />
                  <Point X="-3.557463867188" Y="0.066085334778" />
                  <Point X="-4.222664550781" Y="0.244325180054" />
                  <Point X="-4.998186523438" Y="0.452125793457" />
                  <Point X="-4.956439453125" Y="0.734248291016" />
                  <Point X="-4.917645019531" Y="0.996418212891" />
                  <Point X="-4.83216796875" Y="1.311854980469" />
                  <Point X="-4.773516113281" Y="1.528298583984" />
                  <Point X="-4.340034179687" Y="1.471229736328" />
                  <Point X="-3.753266357422" Y="1.393980102539" />
                  <Point X="-3.731704833984" Y="1.395866577148" />
                  <Point X="-3.697817382812" Y="1.406551147461" />
                  <Point X="-3.670278808594" Y="1.415234130859" />
                  <Point X="-3.651534423828" Y="1.426056152344" />
                  <Point X="-3.639119873047" Y="1.443786132813" />
                  <Point X="-3.625522216797" Y="1.47661340332" />
                  <Point X="-3.614472412109" Y="1.503290283203" />
                  <Point X="-3.610714111328" Y="1.524605224609" />
                  <Point X="-3.616315917969" Y="1.545511474609" />
                  <Point X="-3.632722900391" Y="1.577029052734" />
                  <Point X="-3.646055664062" Y="1.602641235352" />
                  <Point X="-3.659968261719" Y="1.619221801758" />
                  <Point X="-4.041528808594" Y="1.912003540039" />
                  <Point X="-4.47610546875" Y="2.245466308594" />
                  <Point X="-4.310756347656" Y="2.528749511719" />
                  <Point X="-4.160014648438" Y="2.787006591797" />
                  <Point X="-3.933588867188" Y="3.078044677734" />
                  <Point X="-3.774671142578" Y="3.282310791016" />
                  <Point X="-3.519533447266" Y="3.135006835938" />
                  <Point X="-3.159156982422" Y="2.926943359375" />
                  <Point X="-3.138514648438" Y="2.920434814453" />
                  <Point X="-3.091318603516" Y="2.916305664062" />
                  <Point X="-3.052965332031" Y="2.912950195312" />
                  <Point X="-3.031506835938" Y="2.915775390625" />
                  <Point X="-3.013252441406" Y="2.927404541016" />
                  <Point X="-2.979752441406" Y="2.960904541016" />
                  <Point X="-2.952528808594" Y="2.988127929688" />
                  <Point X="-2.940899414062" Y="3.006382324219" />
                  <Point X="-2.93807421875" Y="3.027841064453" />
                  <Point X="-2.942203369141" Y="3.075037109375" />
                  <Point X="-2.945558837891" Y="3.113390625" />
                  <Point X="-2.952067382812" Y="3.134032958984" />
                  <Point X="-3.1211484375" Y="3.426889892578" />
                  <Point X="-3.307278564453" Y="3.749276611328" />
                  <Point X="-3.015419677734" Y="3.973041992188" />
                  <Point X="-2.752876220703" Y="4.174331542969" />
                  <Point X="-2.396262939453" Y="4.372458007812" />
                  <Point X="-2.141548583984" Y="4.513971679688" />
                  <Point X="-2.078149169922" Y="4.431348144531" />
                  <Point X="-1.967826904297" Y="4.287573242188" />
                  <Point X="-1.951246582031" Y="4.273660644531" />
                  <Point X="-1.898717895508" Y="4.246315429688" />
                  <Point X="-1.856030639648" Y="4.22409375" />
                  <Point X="-1.835124145508" Y="4.2184921875" />
                  <Point X="-1.813808959961" Y="4.222250488281" />
                  <Point X="-1.759096557617" Y="4.244913574219" />
                  <Point X="-1.714635009766" Y="4.263330078125" />
                  <Point X="-1.696905395508" Y="4.275744140625" />
                  <Point X="-1.686083496094" Y="4.29448828125" />
                  <Point X="-1.668275512695" Y="4.350967773438" />
                  <Point X="-1.653804077148" Y="4.396864746094" />
                  <Point X="-1.651917602539" Y="4.418426757812" />
                  <Point X="-1.671140258789" Y="4.564437011719" />
                  <Point X="-1.689137573242" Y="4.701141113281" />
                  <Point X="-1.307973144531" Y="4.808006347656" />
                  <Point X="-0.968094177246" Y="4.903296386719" />
                  <Point X="-0.535770996094" Y="4.953893554688" />
                  <Point X="-0.224199630737" Y="4.990358398438" />
                  <Point X="-0.148749725342" Y="4.708774902344" />
                  <Point X="-0.042140380859" Y="4.310903320312" />
                  <Point X="-0.024282115936" Y="4.284176757813" />
                  <Point X="0.006155934334" Y="4.273844238281" />
                  <Point X="0.036594036102" Y="4.284176757813" />
                  <Point X="0.054452251434" Y="4.310903320312" />
                  <Point X="0.141087509155" Y="4.63423046875" />
                  <Point X="0.236648422241" Y="4.990868652344" />
                  <Point X="0.563440551758" Y="4.956645019531" />
                  <Point X="0.860205749512" Y="4.925565429688" />
                  <Point X="1.217878417969" Y="4.839212402344" />
                  <Point X="1.508455932617" Y="4.769058105469" />
                  <Point X="1.741574829102" Y="4.68450390625" />
                  <Point X="1.931033691406" Y="4.615786132813" />
                  <Point X="2.156150390625" Y="4.510506347656" />
                  <Point X="2.33869921875" Y="4.425134277344" />
                  <Point X="2.556171630859" Y="4.298434570312" />
                  <Point X="2.732523681641" Y="4.19569140625" />
                  <Point X="2.937628173828" Y="4.049832519531" />
                  <Point X="3.068740234375" Y="3.956592773438" />
                  <Point X="2.705197265625" Y="3.326917480469" />
                  <Point X="2.229853515625" Y="2.503597900391" />
                  <Point X="2.224852050781" Y="2.491514648438" />
                  <Point X="2.213007568359" Y="2.447222167969" />
                  <Point X="2.203382324219" Y="2.411228271484" />
                  <Point X="2.202044677734" Y="2.392325927734" />
                  <Point X="2.206663085938" Y="2.354025634766" />
                  <Point X="2.210416015625" Y="2.322901123047" />
                  <Point X="2.218682128906" Y="2.300812255859" />
                  <Point X="2.242381103516" Y="2.265885986328" />
                  <Point X="2.261639892578" Y="2.237503662109" />
                  <Point X="2.274940185547" Y="2.224203613281" />
                  <Point X="2.309866210938" Y="2.200504638672" />
                  <Point X="2.338248779297" Y="2.18124609375" />
                  <Point X="2.360336914062" Y="2.172980224609" />
                  <Point X="2.398637207031" Y="2.168361816406" />
                  <Point X="2.42976171875" Y="2.164608642578" />
                  <Point X="2.448664306641" Y="2.165946289062" />
                  <Point X="2.492956787109" Y="2.177790771484" />
                  <Point X="2.528950683594" Y="2.187416015625" />
                  <Point X="2.541033935547" Y="2.192417480469" />
                  <Point X="3.210097412109" Y="2.578701416016" />
                  <Point X="3.994248046875" Y="3.031430908203" />
                  <Point X="4.097985839844" Y="2.887258789062" />
                  <Point X="4.202591796875" Y="2.741880126953" />
                  <Point X="4.316934570312" Y="2.552927734375" />
                  <Point X="4.387512695312" Y="2.436296142578" />
                  <Point X="3.914450683594" Y="2.073302490234" />
                  <Point X="3.288616210938" Y="1.593082641602" />
                  <Point X="3.279371337891" Y="1.583833129883" />
                  <Point X="3.247493896484" Y="1.542246704102" />
                  <Point X="3.221589111328" Y="1.508451782227" />
                  <Point X="3.213119628906" Y="1.49150012207" />
                  <Point X="3.201245361328" Y="1.449040283203" />
                  <Point X="3.191595703125" Y="1.414535644531" />
                  <Point X="3.190779541016" Y="1.390965209961" />
                  <Point X="3.200527099609" Y="1.343723266602" />
                  <Point X="3.208448486328" Y="1.305332397461" />
                  <Point X="3.215646484375" Y="1.287954956055" />
                  <Point X="3.242158935547" Y="1.247657104492" />
                  <Point X="3.263704101562" Y="1.214909667969" />
                  <Point X="3.280947998047" Y="1.198819946289" />
                  <Point X="3.319368164062" Y="1.177192626953" />
                  <Point X="3.350590087891" Y="1.159617675781" />
                  <Point X="3.368565673828" Y="1.153619628906" />
                  <Point X="3.420512207031" Y="1.146754150391" />
                  <Point X="3.462726318359" Y="1.141175048828" />
                  <Point X="3.475803710938" Y="1.14117175293" />
                  <Point X="4.111369140625" Y="1.224845581055" />
                  <Point X="4.848975097656" Y="1.32195324707" />
                  <Point X="4.894260742188" Y="1.135934082031" />
                  <Point X="4.939188476562" Y="0.951385559082" />
                  <Point X="4.975221191406" Y="0.719953308105" />
                  <Point X="4.997858398438" Y="0.574556152344" />
                  <Point X="4.458387695312" Y="0.430005340576" />
                  <Point X="3.741167724609" Y="0.237826919556" />
                  <Point X="3.729086914062" Y="0.232819412231" />
                  <Point X="3.678051025391" Y="0.203319641113" />
                  <Point X="3.636576904297" Y="0.17934703064" />
                  <Point X="3.622264892578" Y="0.166926574707" />
                  <Point X="3.591643310547" Y="0.12790737915" />
                  <Point X="3.566759033203" Y="0.096198890686" />
                  <Point X="3.556985351562" Y="0.074735275269" />
                  <Point X="3.546778076172" Y="0.021437074661" />
                  <Point X="3.538483154297" Y="-0.021875303268" />
                  <Point X="3.538483154297" Y="-0.04068478775" />
                  <Point X="3.548690429688" Y="-0.09398299408" />
                  <Point X="3.556985351562" Y="-0.13729536438" />
                  <Point X="3.566759033203" Y="-0.158758987427" />
                  <Point X="3.597380615234" Y="-0.197778030396" />
                  <Point X="3.622264892578" Y="-0.229486679077" />
                  <Point X="3.636576904297" Y="-0.241906967163" />
                  <Point X="3.687613037109" Y="-0.271406738281" />
                  <Point X="3.729086914062" Y="-0.295379333496" />
                  <Point X="3.741167724609" Y="-0.300387023926" />
                  <Point X="4.324010253906" Y="-0.456559204102" />
                  <Point X="4.998067871094" Y="-0.637172363281" />
                  <Point X="4.973517578125" Y="-0.800011779785" />
                  <Point X="4.948431640625" Y="-0.966399414063" />
                  <Point X="4.902268066406" Y="-1.168695800781" />
                  <Point X="4.874545898438" Y="-1.290178344727" />
                  <Point X="4.243755371094" Y="-1.207133056641" />
                  <Point X="3.411982177734" Y="-1.097628295898" />
                  <Point X="3.394836181641" Y="-1.098341186523" />
                  <Point X="3.294670654297" Y="-1.120112670898" />
                  <Point X="3.213271972656" Y="-1.137804931641" />
                  <Point X="3.1854453125" Y="-1.154697265625" />
                  <Point X="3.124901367188" Y="-1.227512573242" />
                  <Point X="3.075701171875" Y="-1.286685424805" />
                  <Point X="3.064357910156" Y="-1.314070678711" />
                  <Point X="3.055680419922" Y="-1.408369750977" />
                  <Point X="3.04862890625" Y="-1.485000854492" />
                  <Point X="3.056360351562" Y="-1.516621826172" />
                  <Point X="3.111793457031" Y="-1.602844360352" />
                  <Point X="3.156840576172" Y="-1.672912353516" />
                  <Point X="3.168460693359" Y="-1.685540649414" />
                  <Point X="3.709342285156" Y="-2.100573730469" />
                  <Point X="4.33907421875" Y="-2.583784179688" />
                  <Point X="4.274845703125" Y="-2.687715576172" />
                  <Point X="4.204133300781" Y="-2.802139404297" />
                  <Point X="4.108662109375" Y="-2.937790527344" />
                  <Point X="4.056688232422" Y="-3.011638427734" />
                  <Point X="3.494260986328" Y="-2.686920654297" />
                  <Point X="2.753454589844" Y="-2.259215576172" />
                  <Point X="2.737340820313" Y="-2.253312744141" />
                  <Point X="2.618127685547" Y="-2.231782958984" />
                  <Point X="2.521250488281" Y="-2.214287109375" />
                  <Point X="2.489077636719" Y="-2.219244873047" />
                  <Point X="2.390040771484" Y="-2.2713671875" />
                  <Point X="2.309559570312" Y="-2.313723876953" />
                  <Point X="2.288599853516" Y="-2.334683837891" />
                  <Point X="2.236477539062" Y="-2.433720458984" />
                  <Point X="2.194120849609" Y="-2.514201904297" />
                  <Point X="2.189163085938" Y="-2.546374511719" />
                  <Point X="2.210692871094" Y="-2.665587402344" />
                  <Point X="2.228188720703" Y="-2.76246484375" />
                  <Point X="2.234091552734" Y="-2.778578613281" />
                  <Point X="2.581662353516" Y="-3.380588623047" />
                  <Point X="2.986673583984" Y="-4.082088623047" />
                  <Point X="2.919207519531" Y="-4.130278320312" />
                  <Point X="2.835295410156" Y="-4.190214355469" />
                  <Point X="2.728557617188" Y="-4.259304199219" />
                  <Point X="2.679776123047" Y="-4.290879394531" />
                  <Point X="2.248481201172" Y="-3.728805175781" />
                  <Point X="1.683177490234" Y="-2.992087402344" />
                  <Point X="1.670549316406" Y="-2.980467529297" />
                  <Point X="1.552973144531" Y="-2.904876953125" />
                  <Point X="1.45742590332" Y="-2.843448730469" />
                  <Point X="1.425804931641" Y="-2.835717041016" />
                  <Point X="1.297215209961" Y="-2.847550048828" />
                  <Point X="1.192718017578" Y="-2.857166015625" />
                  <Point X="1.165332885742" Y="-2.868509277344" />
                  <Point X="1.066039306641" Y="-2.951068603516" />
                  <Point X="0.985349243164" Y="-3.018159667969" />
                  <Point X="0.968456726074" Y="-3.045986328125" />
                  <Point X="0.938768493652" Y="-3.182575927734" />
                  <Point X="0.91464251709" Y="-3.29357421875" />
                  <Point X="0.91392956543" Y="-3.310720214844" />
                  <Point X="1.012428405762" Y="-4.058893310547" />
                  <Point X="1.127642211914" Y="-4.934028320312" />
                  <Point X="1.073728637695" Y="-4.945846191406" />
                  <Point X="0.99434552002" Y="-4.963247070312" />
                  <Point X="0.895737976074" Y="-4.981160644531" />
                  <Point X="0.860200378418" Y="-4.987616699219" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#172" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.106609374343" Y="4.752996095487" Z="1.4" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.4" />
                  <Point X="-0.547739060359" Y="5.034834628843" Z="1.4" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.4" />
                  <Point X="-1.327626913789" Y="4.887429855699" Z="1.4" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.4" />
                  <Point X="-1.72686849711" Y="4.589190881874" Z="1.4" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.4" />
                  <Point X="-1.722117291954" Y="4.397283259761" Z="1.4" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.4" />
                  <Point X="-1.784386680358" Y="4.322387374726" Z="1.4" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.4" />
                  <Point X="-1.88178625045" Y="4.321946287265" Z="1.4" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.4" />
                  <Point X="-2.044637379235" Y="4.493066048266" Z="1.4" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.4" />
                  <Point X="-2.426701805349" Y="4.447445585722" Z="1.4" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.4" />
                  <Point X="-3.051997752229" Y="4.044001900013" Z="1.4" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.4" />
                  <Point X="-3.170605723859" Y="3.433169839907" Z="1.4" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.4" />
                  <Point X="-2.998169165875" Y="3.101959518461" Z="1.4" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.4" />
                  <Point X="-3.02126352379" Y="3.027540044096" Z="1.4" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.4" />
                  <Point X="-3.093116947145" Y="2.997395533298" Z="1.4" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.4" />
                  <Point X="-3.50068950839" Y="3.209588224887" Z="1.4" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.4" />
                  <Point X="-3.97920829997" Y="3.140027088315" Z="1.4" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.4" />
                  <Point X="-4.360094327911" Y="2.585234704526" Z="1.4" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.4" />
                  <Point X="-4.078122848383" Y="1.903615802694" Z="1.4" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.4" />
                  <Point X="-3.683229350657" Y="1.585221779797" Z="1.4" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.4" />
                  <Point X="-3.67787236177" Y="1.527027492502" Z="1.4" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.4" />
                  <Point X="-3.719008416809" Y="1.485517260433" Z="1.4" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.4" />
                  <Point X="-4.339664003161" Y="1.552082021973" Z="1.4" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.4" />
                  <Point X="-4.886583773817" Y="1.356212398948" Z="1.4" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.4" />
                  <Point X="-5.012057007773" Y="0.772847385504" Z="1.4" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.4" />
                  <Point X="-4.241761203174" Y="0.227308962878" Z="1.4" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.4" />
                  <Point X="-3.564118597031" Y="0.040433477307" Z="1.4" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.4" />
                  <Point X="-3.544660408605" Y="0.016443931866" Z="1.4" />
                  <Point X="-3.539556741714" Y="0" Z="1.4" />
                  <Point X="-3.54370419253" Y="-0.013363019195" Z="1.4" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.4" />
                  <Point X="-3.561249998092" Y="-0.038442505827" Z="1.4" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.4" />
                  <Point X="-4.395126585598" Y="-0.268403093878" Z="1.4" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.4" />
                  <Point X="-5.02550821904" Y="-0.69009263967" Z="1.4" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.4" />
                  <Point X="-4.921798668998" Y="-1.227947404549" Z="1.4" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.4" />
                  <Point X="-3.948907465691" Y="-1.402936512406" Z="1.4" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.4" />
                  <Point X="-3.207286246072" Y="-1.313851028259" Z="1.4" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.4" />
                  <Point X="-3.196130377033" Y="-1.335786109845" Z="1.4" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.4" />
                  <Point X="-3.9189563711" Y="-1.903579573936" Z="1.4" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.4" />
                  <Point X="-4.371298507939" Y="-2.572332342331" Z="1.4" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.4" />
                  <Point X="-4.053736220787" Y="-3.048337967092" Z="1.4" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.4" />
                  <Point X="-3.150901598947" Y="-2.889235275267" Z="1.4" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.4" />
                  <Point X="-2.565061825756" Y="-2.563268643375" Z="1.4" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.4" />
                  <Point X="-2.966181737902" Y="-3.284176783545" Z="1.4" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.4" />
                  <Point X="-3.116361635248" Y="-4.003577363632" Z="1.4" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.4" />
                  <Point X="-2.693503129646" Y="-4.299462563005" Z="1.4" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.4" />
                  <Point X="-2.327047167291" Y="-4.287849688674" Z="1.4" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.4" />
                  <Point X="-2.110571120553" Y="-4.079176433275" Z="1.4" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.4" />
                  <Point X="-1.829461333909" Y="-3.993181460465" Z="1.4" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.4" />
                  <Point X="-1.554091594976" Y="-4.096086096045" Z="1.4" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.4" />
                  <Point X="-1.398270382738" Y="-4.345360234246" Z="1.4" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.4" />
                  <Point X="-1.39148088268" Y="-4.715297033687" Z="1.4" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.4" />
                  <Point X="-1.280532404753" Y="-4.913611495702" Z="1.4" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.4" />
                  <Point X="-0.983025532825" Y="-4.981666486745" Z="1.4" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.4" />
                  <Point X="-0.596674943702" Y="-4.189005277928" Z="1.4" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.4" />
                  <Point X="-0.343684559131" Y="-3.413014472663" Z="1.4" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.4" />
                  <Point X="-0.139773418293" Y="-3.247619879356" Z="1.4" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.4" />
                  <Point X="0.113585661068" Y="-3.239492165932" Z="1.4" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.4" />
                  <Point X="0.326761300301" Y="-3.388631283105" Z="1.4" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.4" />
                  <Point X="0.6380799167" Y="-4.343530751887" Z="1.4" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.4" />
                  <Point X="0.898518840537" Y="-4.999075318521" Z="1.4" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.4" />
                  <Point X="1.078278435028" Y="-4.963406617075" Z="1.4" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.4" />
                  <Point X="1.05584466756" Y="-4.021086907199" Z="1.4" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.4" />
                  <Point X="0.981471720589" Y="-3.161915460299" Z="1.4" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.4" />
                  <Point X="1.091850124276" Y="-2.958234834558" Z="1.4" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.4" />
                  <Point X="1.295640923948" Y="-2.866059514941" Z="1.4" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.4" />
                  <Point X="1.519777720602" Y="-2.915654651899" Z="1.4" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.4" />
                  <Point X="2.20265754462" Y="-3.72796317627" Z="1.4" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.4" />
                  <Point X="2.74957058136" Y="-4.269998078885" Z="1.4" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.4" />
                  <Point X="2.942113008207" Y="-4.139685175149" Z="1.4" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.4" />
                  <Point X="2.618807640535" Y="-3.324308558053" Z="1.4" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.4" />
                  <Point X="2.253740900582" Y="-2.625421195733" Z="1.4" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.4" />
                  <Point X="2.274567870989" Y="-2.425726877999" Z="1.4" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.4" />
                  <Point X="2.407171500373" Y="-2.284333438978" Z="1.4" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.4" />
                  <Point X="2.60308560778" Y="-2.249707108909" Z="1.4" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.4" />
                  <Point X="3.463105039054" Y="-2.69894185974" Z="1.4" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.4" />
                  <Point X="4.143394715437" Y="-2.935287874953" Z="1.4" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.4" />
                  <Point X="4.311222969993" Y="-2.682720779103" Z="1.4" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.4" />
                  <Point X="3.733624196702" Y="-2.029625958744" Z="1.4" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.4" />
                  <Point X="3.147695599955" Y="-1.544525031676" Z="1.4" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.4" />
                  <Point X="3.099314092104" Y="-1.381671230682" Z="1.4" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.4" />
                  <Point X="3.157191797441" Y="-1.228199496147" Z="1.4" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.4" />
                  <Point X="3.299134248192" Y="-1.137691834563" Z="1.4" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.4" />
                  <Point X="4.231073266393" Y="-1.225425462574" Z="1.4" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.4" />
                  <Point X="4.944859043342" Y="-1.148539829739" Z="1.4" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.4" />
                  <Point X="5.016802805053" Y="-0.776185969034" Z="1.4" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.4" />
                  <Point X="4.33079523899" Y="-0.376982817434" Z="1.4" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.4" />
                  <Point X="3.706478725945" Y="-0.196837936667" Z="1.4" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.4" />
                  <Point X="3.630558330461" Y="-0.135629654144" Z="1.4" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.4" />
                  <Point X="3.591641928966" Y="-0.053298205175" Z="1.4" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.4" />
                  <Point X="3.589729521458" Y="0.043312326031" Z="1.4" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.4" />
                  <Point X="3.624821107939" Y="0.128319084431" Z="1.4" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.4" />
                  <Point X="3.696916688408" Y="0.191310907575" Z="1.4" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.4" />
                  <Point X="4.465172578316" Y="0.412989115334" Z="1.4" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.4" />
                  <Point X="5.018469901685" Y="0.758925395024" Z="1.4" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.4" />
                  <Point X="4.936685888448" Y="1.179041022674" Z="1.4" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.4" />
                  <Point X="4.098686987039" Y="1.30569790394" Z="1.4" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.4" />
                  <Point X="3.420907351054" Y="1.227603185832" Z="1.4" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.4" />
                  <Point X="3.337738764863" Y="1.252043778901" Z="1.4" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.4" />
                  <Point X="3.277773441818" Y="1.306418651478" Z="1.4" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.4" />
                  <Point X="3.24333966623" Y="1.385107206223" Z="1.4" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.4" />
                  <Point X="3.243241627859" Y="1.466853843086" Z="1.4" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.4" />
                  <Point X="3.281020825565" Y="1.543108607303" Z="1.4" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.4" />
                  <Point X="3.938732779548" Y="2.064914878594" Z="1.4" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.4" />
                  <Point X="4.353555889004" Y="2.610094092344" Z="1.4" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.4" />
                  <Point X="4.132415321535" Y="2.947741829066" Z="1.4" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.4" />
                  <Point X="3.178941517548" Y="2.653282793881" Z="1.4" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.4" />
                  <Point X="2.473884071255" Y="2.257373543742" Z="1.4" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.4" />
                  <Point X="2.398467176181" Y="2.249282210551" Z="1.4" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.4" />
                  <Point X="2.331784294938" Y="2.273159206516" Z="1.4" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.4" />
                  <Point X="2.277599393719" Y="2.325240565444" Z="1.4" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.4" />
                  <Point X="2.250147492208" Y="2.391291267651" Z="1.4" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.4" />
                  <Point X="2.255154398915" Y="2.465585504627" Z="1.4" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.4" />
                  <Point X="2.742342558979" Y="3.333197564407" Z="1.4" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.4" />
                  <Point X="2.960449282218" Y="4.121859568368" Z="1.4" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.4" />
                  <Point X="2.575185672225" Y="4.372917082729" Z="1.4" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.4" />
                  <Point X="2.171175719584" Y="4.587078569254" Z="1.4" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.4" />
                  <Point X="1.752467420256" Y="4.762788058308" Z="1.4" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.4" />
                  <Point X="1.223456814604" Y="4.919096030794" Z="1.4" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.4" />
                  <Point X="0.562492508346" Y="5.037652910583" Z="1.4" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.4" />
                  <Point X="0.08663518721" Y="4.678451410464" Z="1.4" />
                  <Point X="0" Y="4.355124473572" Z="1.4" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>