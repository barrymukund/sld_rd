<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#139" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1007" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="0" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000475830078" Y="0.004716064453" />
                  <Width Value="9.783896972656" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.65024230957" Y="-3.836989990234" />
                  <Point X="0.563302062988" Y="-3.512524414063" />
                  <Point X="0.557721191406" Y="-3.497141845703" />
                  <Point X="0.542363037109" Y="-3.467377197266" />
                  <Point X="0.506828430176" Y="-3.416178466797" />
                  <Point X="0.378635528564" Y="-3.231476806641" />
                  <Point X="0.356751586914" Y="-3.209021484375" />
                  <Point X="0.330496398926" Y="-3.189777587891" />
                  <Point X="0.302495483398" Y="-3.175669433594" />
                  <Point X="0.247507583618" Y="-3.158603027344" />
                  <Point X="0.049136341095" Y="-3.097036132812" />
                  <Point X="0.020976638794" Y="-3.092766601562" />
                  <Point X="-0.00866468811" Y="-3.092766601562" />
                  <Point X="-0.036824085236" Y="-3.097035888672" />
                  <Point X="-0.091811981201" Y="-3.114102050781" />
                  <Point X="-0.290183227539" Y="-3.175669189453" />
                  <Point X="-0.31818460083" Y="-3.189777587891" />
                  <Point X="-0.344439788818" Y="-3.209021484375" />
                  <Point X="-0.366323577881" Y="-3.231476806641" />
                  <Point X="-0.401858306885" Y="-3.282675537109" />
                  <Point X="-0.530051208496" Y="-3.467377197266" />
                  <Point X="-0.538189025879" Y="-3.481573730469" />
                  <Point X="-0.550990112305" Y="-3.512524414063" />
                  <Point X="-0.864630981445" Y="-4.683048339844" />
                  <Point X="-0.916584533691" Y="-4.876941894531" />
                  <Point X="-1.079342529297" Y="-4.845350097656" />
                  <Point X="-1.139291748047" Y="-4.829925292969" />
                  <Point X="-1.24641796875" Y="-4.802362792969" />
                  <Point X="-1.225428588867" Y="-4.642932128906" />
                  <Point X="-1.214963012695" Y="-4.563438476562" />
                  <Point X="-1.214201171875" Y="-4.5479296875" />
                  <Point X="-1.216508666992" Y="-4.516224121094" />
                  <Point X="-1.229645385742" Y="-4.450182128906" />
                  <Point X="-1.277036132812" Y="-4.211932617188" />
                  <Point X="-1.287938354492" Y="-4.182966308594" />
                  <Point X="-1.304010375977" Y="-4.15512890625" />
                  <Point X="-1.32364453125" Y="-4.131204101562" />
                  <Point X="-1.374270507812" Y="-4.086806396484" />
                  <Point X="-1.556905517578" Y="-3.926639404297" />
                  <Point X="-1.583188476562" Y="-3.910295410156" />
                  <Point X="-1.612885742188" Y="-3.897994384766" />
                  <Point X="-1.64302734375" Y="-3.890966308594" />
                  <Point X="-1.710219116211" Y="-3.886562255859" />
                  <Point X="-1.952616577148" Y="-3.870674804688" />
                  <Point X="-1.98341796875" Y="-3.873708496094" />
                  <Point X="-2.014466674805" Y="-3.882028076172" />
                  <Point X="-2.042657714844" Y="-3.894801269531" />
                  <Point X="-2.098645507812" Y="-3.932211181641" />
                  <Point X="-2.300624023438" Y="-4.067169433594" />
                  <Point X="-2.312785644531" Y="-4.076821533203" />
                  <Point X="-2.335102783203" Y="-4.099461914062" />
                  <Point X="-2.480148925781" Y="-4.288489746094" />
                  <Point X="-2.801706787109" Y="-4.089389404297" />
                  <Point X="-2.884723632812" Y="-4.025469238281" />
                  <Point X="-3.104721923828" Y="-3.856077880859" />
                  <Point X="-2.585330078125" Y="-2.95646484375" />
                  <Point X="-2.423761230469" Y="-2.676619628906" />
                  <Point X="-2.412859130859" Y="-2.647653320312" />
                  <Point X="-2.406588134766" Y="-2.616126953125" />
                  <Point X="-2.405575439453" Y="-2.585192871094" />
                  <Point X="-2.414560058594" Y="-2.555575195312" />
                  <Point X="-2.428777099609" Y="-2.526746337891" />
                  <Point X="-2.4468046875" Y="-2.501589355469" />
                  <Point X="-2.464153320312" Y="-2.484240722656" />
                  <Point X="-2.489310302734" Y="-2.466213134766" />
                  <Point X="-2.518139160156" Y="-2.45199609375" />
                  <Point X="-2.547756835938" Y="-2.443011474609" />
                  <Point X="-2.578690917969" Y="-2.444024169922" />
                  <Point X="-2.610217285156" Y="-2.450295166016" />
                  <Point X="-2.63918359375" Y="-2.461197265625" />
                  <Point X="-3.648737548828" Y="-3.044063476562" />
                  <Point X="-3.818024414062" Y="-3.141801269531" />
                  <Point X="-4.082859130859" Y="-2.793862792969" />
                  <Point X="-4.142372558594" Y="-2.694067626953" />
                  <Point X="-4.306142578125" Y="-2.419450439453" />
                  <Point X="-3.388647460938" Y="-1.715431274414" />
                  <Point X="-3.105954589844" Y="-1.498513305664" />
                  <Point X="-3.083062988281" Y="-1.475877807617" />
                  <Point X="-3.064016357422" Y="-1.445568603516" />
                  <Point X="-3.055630615234" Y="-1.424044311523" />
                  <Point X="-3.052184570312" Y="-1.413376831055" />
                  <Point X="-3.046152099609" Y="-1.390085693359" />
                  <Point X="-3.042037597656" Y="-1.358602783203" />
                  <Point X="-3.042736816406" Y="-1.33573449707" />
                  <Point X="-3.048883300781" Y="-1.313696533203" />
                  <Point X="-3.060888183594" Y="-1.284713989258" />
                  <Point X="-3.07348046875" Y="-1.262986694336" />
                  <Point X="-3.091325927734" Y="-1.245317993164" />
                  <Point X="-3.109575927734" Y="-1.231505615234" />
                  <Point X="-3.118719970703" Y="-1.225384399414" />
                  <Point X="-3.139454833984" Y="-1.213180664062" />
                  <Point X="-3.168718261719" Y="-1.201956420898" />
                  <Point X="-3.200605957031" Y="-1.195474853516" />
                  <Point X="-3.231929199219" Y="-1.194383789062" />
                  <Point X="-4.506395507812" Y="-1.362170288086" />
                  <Point X="-4.732102050781" Y="-1.391885253906" />
                  <Point X="-4.834077636719" Y="-0.992654846191" />
                  <Point X="-4.849822753906" Y="-0.882565307617" />
                  <Point X="-4.892424316406" Y="-0.584698181152" />
                  <Point X="-3.854905761719" Y="-0.306695892334" />
                  <Point X="-3.532875976562" Y="-0.220408340454" />
                  <Point X="-3.513105224609" Y="-0.21265322876" />
                  <Point X="-3.491513671875" Y="-0.201252243042" />
                  <Point X="-3.481704833984" Y="-0.195288711548" />
                  <Point X="-3.459975341797" Y="-0.180207107544" />
                  <Point X="-3.436020996094" Y="-0.158681106567" />
                  <Point X="-3.415524169922" Y="-0.129255477905" />
                  <Point X="-3.406091064453" Y="-0.108048835754" />
                  <Point X="-3.402160400391" Y="-0.097597915649" />
                  <Point X="-3.394917236328" Y="-0.074259986877" />
                  <Point X="-3.389474365234" Y="-0.045520904541" />
                  <Point X="-3.390243164062" Y="-0.012662074089" />
                  <Point X="-3.394498046875" Y="0.008627897263" />
                  <Point X="-3.396925048828" Y="0.018169137955" />
                  <Point X="-3.404168212891" Y="0.041506916046" />
                  <Point X="-3.417484130859" Y="0.070830696106" />
                  <Point X="-3.439570800781" Y="0.099408432007" />
                  <Point X="-3.457412841797" Y="0.115106590271" />
                  <Point X="-3.465998779297" Y="0.121827568054" />
                  <Point X="-3.487728271484" Y="0.136909164429" />
                  <Point X="-3.501925292969" Y="0.145047103882" />
                  <Point X="-3.532875976562" Y="0.157848342896" />
                  <Point X="-4.694612304688" Y="0.469134490967" />
                  <Point X="-4.89181640625" Y="0.521975158691" />
                  <Point X="-4.891743164062" Y="0.522469116211" />
                  <Point X="-4.824488769531" Y="0.976970458984" />
                  <Point X="-4.792791503906" Y="1.093942993164" />
                  <Point X="-4.703551269531" Y="1.423267822266" />
                  <Point X="-3.996505126953" Y="1.33018347168" />
                  <Point X="-3.765666259766" Y="1.29979284668" />
                  <Point X="-3.744986083984" Y="1.299341674805" />
                  <Point X="-3.723424560547" Y="1.301228149414" />
                  <Point X="-3.703136474609" Y="1.305263793945" />
                  <Point X="-3.689804931641" Y="1.309467407227" />
                  <Point X="-3.641710449219" Y="1.324631347656" />
                  <Point X="-3.622779052734" Y="1.332961669922" />
                  <Point X="-3.604034667969" Y="1.343783569336" />
                  <Point X="-3.587353515625" Y="1.356014648438" />
                  <Point X="-3.573715087891" Y="1.37156628418" />
                  <Point X="-3.561300537109" Y="1.389296020508" />
                  <Point X="-3.551351318359" Y="1.407431030273" />
                  <Point X="-3.546001953125" Y="1.420345703125" />
                  <Point X="-3.526703857422" Y="1.466935302734" />
                  <Point X="-3.520915527344" Y="1.486794067383" />
                  <Point X="-3.517157226562" Y="1.508109130859" />
                  <Point X="-3.5158046875" Y="1.528749267578" />
                  <Point X="-3.518951171875" Y="1.549192871094" />
                  <Point X="-3.524552978516" Y="1.570099121094" />
                  <Point X="-3.532049804688" Y="1.589377563477" />
                  <Point X="-3.538504394531" Y="1.601776977539" />
                  <Point X="-3.561789550781" Y="1.646507324219" />
                  <Point X="-3.573280761719" Y="1.663705322266" />
                  <Point X="-3.587193115234" Y="1.680285766602" />
                  <Point X="-3.602135742188" Y="1.694590209961" />
                  <Point X="-4.268510742188" Y="2.205917724609" />
                  <Point X="-4.351860351563" Y="2.269874267578" />
                  <Point X="-4.342482421875" Y="2.285940673828" />
                  <Point X="-4.081153320312" Y="2.73366015625" />
                  <Point X="-3.997194580078" Y="2.841576416016" />
                  <Point X="-3.750504882813" Y="3.158661621094" />
                  <Point X="-3.348431884766" Y="2.926524658203" />
                  <Point X="-3.206656982422" Y="2.844670898438" />
                  <Point X="-3.187724121094" Y="2.836340332031" />
                  <Point X="-3.167081787109" Y="2.829831787109" />
                  <Point X="-3.146794921875" Y="2.825796386719" />
                  <Point X="-3.128227539063" Y="2.824171875" />
                  <Point X="-3.061245605469" Y="2.818311767578" />
                  <Point X="-3.040564697266" Y="2.818762939453" />
                  <Point X="-3.019106201172" Y="2.821588134766" />
                  <Point X="-2.999014892578" Y="2.826504638672" />
                  <Point X="-2.980463867188" Y="2.835652832031" />
                  <Point X="-2.962209472656" Y="2.847281982422" />
                  <Point X="-2.946077392578" Y="2.860229492188" />
                  <Point X="-2.932898193359" Y="2.873408691406" />
                  <Point X="-2.885353759766" Y="2.920952880859" />
                  <Point X="-2.872406738281" Y="2.937084228516" />
                  <Point X="-2.86077734375" Y="2.955338623047" />
                  <Point X="-2.851628662109" Y="2.973890136719" />
                  <Point X="-2.846712158203" Y="2.993981933594" />
                  <Point X="-2.843886962891" Y="3.015440673828" />
                  <Point X="-2.843435791016" Y="3.036121337891" />
                  <Point X="-2.845060302734" Y="3.054688720703" />
                  <Point X="-2.850920410156" Y="3.121670898438" />
                  <Point X="-2.854955810547" Y="3.141957763672" />
                  <Point X="-2.861464355469" Y="3.162600097656" />
                  <Point X="-2.869794921875" Y="3.181532958984" />
                  <Point X="-3.1650859375" Y="3.692991455078" />
                  <Point X="-3.183333007812" Y="3.724596435547" />
                  <Point X="-3.155773925781" Y="3.745725830078" />
                  <Point X="-2.700625244141" Y="4.094683837891" />
                  <Point X="-2.568395019531" Y="4.1681484375" />
                  <Point X="-2.167036621094" Y="4.391134765625" />
                  <Point X="-2.086596923828" Y="4.286303222656" />
                  <Point X="-2.0431953125" Y="4.229741210938" />
                  <Point X="-2.028892700195" Y="4.214799804688" />
                  <Point X="-2.012312866211" Y="4.200887207031" />
                  <Point X="-1.99511340332" Y="4.189395019531" />
                  <Point X="-1.974448120117" Y="4.178637207031" />
                  <Point X="-1.899897338867" Y="4.139828125" />
                  <Point X="-1.88061730957" Y="4.132330566406" />
                  <Point X="-1.85971081543" Y="4.126729003906" />
                  <Point X="-1.839267822266" Y="4.123582519531" />
                  <Point X="-1.818628417969" Y="4.124935546875" />
                  <Point X="-1.797313110352" Y="4.128693847656" />
                  <Point X="-1.777452880859" Y="4.134482421875" />
                  <Point X="-1.755928466797" Y="4.1433984375" />
                  <Point X="-1.678278686523" Y="4.175562011719" />
                  <Point X="-1.660145141602" Y="4.185510742187" />
                  <Point X="-1.642415405273" Y="4.197925292969" />
                  <Point X="-1.626863647461" Y="4.211563964844" />
                  <Point X="-1.614632568359" Y="4.228245117188" />
                  <Point X="-1.603810791016" Y="4.246989257813" />
                  <Point X="-1.595480712891" Y="4.265920898438" />
                  <Point X="-1.588474731445" Y="4.288140136719" />
                  <Point X="-1.563201293945" Y="4.368297363281" />
                  <Point X="-1.559165649414" Y="4.388584960938" />
                  <Point X="-1.557279174805" Y="4.410146484375" />
                  <Point X="-1.55773034668" Y="4.430826660156" />
                  <Point X="-1.584201782227" Y="4.631897949219" />
                  <Point X="-1.538852905273" Y="4.644612304688" />
                  <Point X="-0.949635070801" Y="4.809808105469" />
                  <Point X="-0.789338867188" Y="4.828568847656" />
                  <Point X="-0.294710784912" Y="4.886458007812" />
                  <Point X="-0.175844223022" Y="4.442841308594" />
                  <Point X="-0.133903305054" Y="4.286315429688" />
                  <Point X="-0.121129844666" Y="4.258124023438" />
                  <Point X="-0.10327155304" Y="4.231397460938" />
                  <Point X="-0.082114555359" Y="4.20880859375" />
                  <Point X="-0.054819446564" Y="4.19421875" />
                  <Point X="-0.024381370544" Y="4.183886230469" />
                  <Point X="0.006155916691" Y="4.178844238281" />
                  <Point X="0.03669316864" Y="4.183886230469" />
                  <Point X="0.067131248474" Y="4.19421875" />
                  <Point X="0.094426452637" Y="4.20880859375" />
                  <Point X="0.115583503723" Y="4.231397460938" />
                  <Point X="0.133441650391" Y="4.258124023438" />
                  <Point X="0.146215255737" Y="4.286315429688" />
                  <Point X="0.297518890381" Y="4.850988769531" />
                  <Point X="0.307419342041" Y="4.8879375" />
                  <Point X="0.329562744141" Y="4.885618652344" />
                  <Point X="0.844041137695" Y="4.831738769531" />
                  <Point X="0.976664978027" Y="4.799719238281" />
                  <Point X="1.481026000977" Y="4.677951171875" />
                  <Point X="1.566190429688" Y="4.647061035156" />
                  <Point X="1.894646972656" Y="4.527927246094" />
                  <Point X="1.978121337891" Y="4.488889160156" />
                  <Point X="2.294576416016" Y="4.340893554688" />
                  <Point X="2.375248291016" Y="4.293894042969" />
                  <Point X="2.680977050781" Y="4.115775878906" />
                  <Point X="2.757036376953" Y="4.061686523438" />
                  <Point X="2.943260009766" Y="3.929254638672" />
                  <Point X="2.334584716797" Y="2.874998046875" />
                  <Point X="2.147581054688" Y="2.551097900391" />
                  <Point X="2.139569335937" Y="2.533157470703" />
                  <Point X="2.129908203125" Y="2.503649414062" />
                  <Point X="2.128417236328" Y="2.498631835938" />
                  <Point X="2.111607177734" Y="2.435770507813" />
                  <Point X="2.108391845703" Y="2.409888427734" />
                  <Point X="2.10887109375" Y="2.375918212891" />
                  <Point X="2.109544677734" Y="2.365885498047" />
                  <Point X="2.116099121094" Y="2.311528320312" />
                  <Point X="2.121442138672" Y="2.289604980469" />
                  <Point X="2.129708251953" Y="2.267516357422" />
                  <Point X="2.140071289062" Y="2.247470703125" />
                  <Point X="2.149394775391" Y="2.23373046875" />
                  <Point X="2.183029052734" Y="2.184162109375" />
                  <Point X="2.200678955078" Y="2.164642578125" />
                  <Point X="2.227719238281" Y="2.142018798828" />
                  <Point X="2.235339355469" Y="2.136268798828" />
                  <Point X="2.284907714844" Y="2.102634765625" />
                  <Point X="2.304952880859" Y="2.092271972656" />
                  <Point X="2.327041259766" Y="2.084006103516" />
                  <Point X="2.348963867188" Y="2.078663330078" />
                  <Point X="2.364031494141" Y="2.076846435547" />
                  <Point X="2.418388671875" Y="2.070291748047" />
                  <Point X="2.445235595703" Y="2.070877441406" />
                  <Point X="2.481563476562" Y="2.076874755859" />
                  <Point X="2.490631591797" Y="2.078830810547" />
                  <Point X="2.553492919922" Y="2.095640869141" />
                  <Point X="2.565284179688" Y="2.099638427734" />
                  <Point X="2.588533935547" Y="2.110145019531" />
                  <Point X="3.757017089844" Y="2.784769042969" />
                  <Point X="3.967326416016" Y="2.906191162109" />
                  <Point X="4.123272460938" Y="2.689461425781" />
                  <Point X="4.165673828125" Y="2.619392822266" />
                  <Point X="4.262198730469" Y="2.459884277344" />
                  <Point X="3.476991943359" Y="1.857373291016" />
                  <Point X="3.230783935547" Y="1.668451293945" />
                  <Point X="3.221424560547" Y="1.660241455078" />
                  <Point X="3.203973388672" Y="1.641627563477" />
                  <Point X="3.191432617188" Y="1.625266967773" />
                  <Point X="3.146191162109" Y="1.56624609375" />
                  <Point X="3.133095458984" Y="1.543004150391" />
                  <Point X="3.119954589844" Y="1.509348266602" />
                  <Point X="3.116958740234" Y="1.500382202148" />
                  <Point X="3.100106201172" Y="1.440121826172" />
                  <Point X="3.096652587891" Y="1.417823242188" />
                  <Point X="3.095836425781" Y="1.394252929688" />
                  <Point X="3.097739257812" Y="1.371768310547" />
                  <Point X="3.101573974609" Y="1.353182983398" />
                  <Point X="3.115408203125" Y="1.286135498047" />
                  <Point X="3.124525390625" Y="1.260813720703" />
                  <Point X="3.142153564453" Y="1.227582641602" />
                  <Point X="3.146712890625" Y="1.219886474609" />
                  <Point X="3.184340332031" Y="1.162694824219" />
                  <Point X="3.198893798828" Y="1.145450195312" />
                  <Point X="3.216137695313" Y="1.129360595703" />
                  <Point X="3.234347167969" Y="1.116034912109" />
                  <Point X="3.249461914062" Y="1.107526611328" />
                  <Point X="3.303989257812" Y="1.076832763672" />
                  <Point X="3.320520263672" Y="1.069502197266" />
                  <Point X="3.356117919922" Y="1.059438598633" />
                  <Point X="3.376553955078" Y="1.056737670898" />
                  <Point X="3.450278564453" Y="1.046994018555" />
                  <Point X="3.462702148438" Y="1.046175048828" />
                  <Point X="3.488203613281" Y="1.04698449707" />
                  <Point X="4.598184570312" Y="1.193116333008" />
                  <Point X="4.776839355469" Y="1.21663671875" />
                  <Point X="4.845936523438" Y="0.932808776855" />
                  <Point X="4.859297851562" Y="0.846987426758" />
                  <Point X="4.890864746094" Y="0.644238464355" />
                  <Point X="3.998739501953" Y="0.40519430542" />
                  <Point X="3.716579833984" Y="0.329589935303" />
                  <Point X="3.704791259766" Y="0.325586608887" />
                  <Point X="3.681545654297" Y="0.315067993164" />
                  <Point X="3.661467773438" Y="0.303462554932" />
                  <Point X="3.589035644531" Y="0.261595428467" />
                  <Point X="3.567672851562" Y="0.244747436523" />
                  <Point X="3.541314208984" Y="0.216976776123" />
                  <Point X="3.535484130859" Y="0.210226211548" />
                  <Point X="3.492024902344" Y="0.154848968506" />
                  <Point X="3.48030078125" Y="0.13556854248" />
                  <Point X="3.470527099609" Y="0.114104927063" />
                  <Point X="3.463680908203" Y="0.092604118347" />
                  <Point X="3.459665283203" Y="0.07163609314" />
                  <Point X="3.445178710938" Y="-0.004006377697" />
                  <Point X="3.443483154297" Y="-0.021875295639" />
                  <Point X="3.445178710938" Y="-0.058553779602" />
                  <Point X="3.449194335938" Y="-0.079521652222" />
                  <Point X="3.463680908203" Y="-0.155164428711" />
                  <Point X="3.470527099609" Y="-0.176665084839" />
                  <Point X="3.48030078125" Y="-0.198128707886" />
                  <Point X="3.492024658203" Y="-0.217408355713" />
                  <Point X="3.504071289062" Y="-0.232758926392" />
                  <Point X="3.547530517578" Y="-0.288136169434" />
                  <Point X="3.559999023438" Y="-0.30123614502" />
                  <Point X="3.589035644531" Y="-0.324155578613" />
                  <Point X="3.609113525391" Y="-0.335761016846" />
                  <Point X="3.681545654297" Y="-0.377627990723" />
                  <Point X="3.692710205078" Y="-0.383139038086" />
                  <Point X="3.716579833984" Y="-0.392149932861" />
                  <Point X="4.734482421875" Y="-0.664896179199" />
                  <Point X="4.89147265625" Y="-0.706961608887" />
                  <Point X="4.855022949219" Y="-0.948724182129" />
                  <Point X="4.837903320312" Y="-1.023744384766" />
                  <Point X="4.801173339844" Y="-1.184698974609" />
                  <Point X="3.751607910156" Y="-1.046520874023" />
                  <Point X="3.424382080078" Y="-1.003441040039" />
                  <Point X="3.408035644531" Y="-1.002710327148" />
                  <Point X="3.374658447266" Y="-1.005508728027" />
                  <Point X="3.335252685547" Y="-1.014073791504" />
                  <Point X="3.193094238281" Y="-1.04497253418" />
                  <Point X="3.163974121094" Y="-1.056596923828" />
                  <Point X="3.136147460938" Y="-1.073489257812" />
                  <Point X="3.112397460938" Y="-1.093960205078" />
                  <Point X="3.088579101562" Y="-1.122606323242" />
                  <Point X="3.002653320312" Y="-1.225948364258" />
                  <Point X="2.987932617188" Y="-1.250330566406" />
                  <Point X="2.976589355469" Y="-1.277715698242" />
                  <Point X="2.969757568359" Y="-1.305365234375" />
                  <Point X="2.96634375" Y="-1.342463256836" />
                  <Point X="2.954028564453" Y="-1.476295532227" />
                  <Point X="2.956347412109" Y="-1.507564819336" />
                  <Point X="2.964079101562" Y="-1.539185791016" />
                  <Point X="2.976450439453" Y="-1.567996826172" />
                  <Point X="2.998258300781" Y="-1.601917358398" />
                  <Point X="3.076930664062" Y="-1.724287109375" />
                  <Point X="3.086932373047" Y="-1.73723815918" />
                  <Point X="3.110628417969" Y="-1.760909179688" />
                  <Point X="4.055248291016" Y="-2.485741943359" />
                  <Point X="4.213122558594" Y="-2.606883056641" />
                  <Point X="4.124811035156" Y="-2.749784667969" />
                  <Point X="4.089405761719" Y="-2.800090332031" />
                  <Point X="4.028981445312" Y="-2.885945068359" />
                  <Point X="3.092393310547" Y="-2.345205566406" />
                  <Point X="2.800954589844" Y="-2.176943115234" />
                  <Point X="2.786131591797" Y="-2.170012451172" />
                  <Point X="2.754224609375" Y="-2.159825195312" />
                  <Point X="2.707325439453" Y="-2.151355224609" />
                  <Point X="2.538134277344" Y="-2.120799560547" />
                  <Point X="2.506781982422" Y="-2.120395263672" />
                  <Point X="2.474609130859" Y="-2.125353027344" />
                  <Point X="2.444833251953" Y="-2.135176757812" />
                  <Point X="2.405871337891" Y="-2.155682128906" />
                  <Point X="2.265315185547" Y="-2.229655761719" />
                  <Point X="2.242384033203" Y="-2.246549072266" />
                  <Point X="2.221424316406" Y="-2.267509033203" />
                  <Point X="2.204531982422" Y="-2.290439208984" />
                  <Point X="2.184026611328" Y="-2.329400878906" />
                  <Point X="2.110052978516" Y="-2.469957275391" />
                  <Point X="2.100229003906" Y="-2.499733398438" />
                  <Point X="2.095271240234" Y="-2.531906005859" />
                  <Point X="2.095675537109" Y="-2.563258300781" />
                  <Point X="2.104145507812" Y="-2.610157470703" />
                  <Point X="2.134701171875" Y="-2.779348632812" />
                  <Point X="2.138985595703" Y="-2.795141845703" />
                  <Point X="2.151819091797" Y="-2.826078613281" />
                  <Point X="2.758832519531" Y="-3.877456298828" />
                  <Point X="2.861283447266" Y="-4.054906494141" />
                  <Point X="2.781849853516" Y="-4.111644042969" />
                  <Point X="2.742267089844" Y="-4.137265625" />
                  <Point X="2.701765136719" Y="-4.163481445312" />
                  <Point X="1.980940795898" Y="-3.224085449219" />
                  <Point X="1.758546142578" Y="-2.934255126953" />
                  <Point X="1.747503540039" Y="-2.922178955078" />
                  <Point X="1.721924316406" Y="-2.900557373047" />
                  <Point X="1.675668945312" Y="-2.870819335938" />
                  <Point X="1.50880090332" Y="-2.763538574219" />
                  <Point X="1.479989746094" Y="-2.751167236328" />
                  <Point X="1.448368774414" Y="-2.743435546875" />
                  <Point X="1.417099487305" Y="-2.741116699219" />
                  <Point X="1.366511230469" Y="-2.745771972656" />
                  <Point X="1.184012573242" Y="-2.762565673828" />
                  <Point X="1.15636315918" Y="-2.769397460938" />
                  <Point X="1.128978027344" Y="-2.780740722656" />
                  <Point X="1.104595825195" Y="-2.795461425781" />
                  <Point X="1.065532958984" Y="-2.827940917969" />
                  <Point X="0.924611938477" Y="-2.945111816406" />
                  <Point X="0.904141052246" Y="-2.968861816406" />
                  <Point X="0.887248779297" Y="-2.996688476562" />
                  <Point X="0.875624206543" Y="-3.025809082031" />
                  <Point X="0.863944702148" Y="-3.079544433594" />
                  <Point X="0.821809936523" Y="-3.273396972656" />
                  <Point X="0.819724487305" Y="-3.289627441406" />
                  <Point X="0.819742248535" Y="-3.323120117188" />
                  <Point X="0.991765075684" Y="-4.629763671875" />
                  <Point X="1.022065185547" Y="-4.859915527344" />
                  <Point X="0.975682067871" Y="-4.87008203125" />
                  <Point X="0.939108093262" Y="-4.8767265625" />
                  <Point X="0.929315551758" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058434204102" Y="-4.752635742188" />
                  <Point X="-1.115619506836" Y="-4.737921875" />
                  <Point X="-1.141246337891" Y="-4.731328125" />
                  <Point X="-1.131241333008" Y="-4.65533203125" />
                  <Point X="-1.120775756836" Y="-4.575838378906" />
                  <Point X="-1.120077392578" Y="-4.568099609375" />
                  <Point X="-1.119451660156" Y="-4.541033691406" />
                  <Point X="-1.121759277344" Y="-4.509328125" />
                  <Point X="-1.123334106445" Y="-4.497690429688" />
                  <Point X="-1.136470825195" Y="-4.4316484375" />
                  <Point X="-1.183861572266" Y="-4.193398925781" />
                  <Point X="-1.18812512207" Y="-4.17846875" />
                  <Point X="-1.19902734375" Y="-4.149502441406" />
                  <Point X="-1.205666137695" Y="-4.135466308594" />
                  <Point X="-1.221738037109" Y="-4.10762890625" />
                  <Point X="-1.230573852539" Y="-4.094862304688" />
                  <Point X="-1.250207885742" Y="-4.0709375" />
                  <Point X="-1.261006713867" Y="-4.059779296875" />
                  <Point X="-1.31163269043" Y="-4.015381591797" />
                  <Point X="-1.494267700195" Y="-3.855214599609" />
                  <Point X="-1.506738647461" Y="-3.845965576172" />
                  <Point X="-1.533021606445" Y="-3.829621582031" />
                  <Point X="-1.546833496094" Y="-3.822526855469" />
                  <Point X="-1.576530761719" Y="-3.810225830078" />
                  <Point X="-1.591313354492" Y="-3.805476074219" />
                  <Point X="-1.621455078125" Y="-3.798447998047" />
                  <Point X="-1.636813964844" Y="-3.796169677734" />
                  <Point X="-1.704005737305" Y="-3.791765625" />
                  <Point X="-1.946403198242" Y="-3.775878173828" />
                  <Point X="-1.961928222656" Y="-3.776132324219" />
                  <Point X="-1.992729614258" Y="-3.779166015625" />
                  <Point X="-2.008006103516" Y="-3.781945556641" />
                  <Point X="-2.03905480957" Y="-3.790265136719" />
                  <Point X="-2.053673828125" Y="-3.79549609375" />
                  <Point X="-2.081864990234" Y="-3.808269287109" />
                  <Point X="-2.095437011719" Y="-3.815811767578" />
                  <Point X="-2.151424804688" Y="-3.853221679688" />
                  <Point X="-2.353403320312" Y="-3.988179931641" />
                  <Point X="-2.359681640625" Y="-3.992757080078" />
                  <Point X="-2.380442138672" Y="-4.010131103516" />
                  <Point X="-2.402759277344" Y="-4.032771484375" />
                  <Point X="-2.410471435547" Y="-4.041629638672" />
                  <Point X="-2.503202880859" Y="-4.162479492188" />
                  <Point X="-2.747581787109" Y="-4.011166259766" />
                  <Point X="-2.826766357422" Y="-3.950196777344" />
                  <Point X="-2.980862792969" Y="-3.831547607422" />
                  <Point X="-2.503057617188" Y="-3.00396484375" />
                  <Point X="-2.341488769531" Y="-2.724119628906" />
                  <Point X="-2.334850097656" Y="-2.710083251953" />
                  <Point X="-2.323947998047" Y="-2.681116943359" />
                  <Point X="-2.319684570312" Y="-2.666187011719" />
                  <Point X="-2.313413574219" Y="-2.634660644531" />
                  <Point X="-2.311638916016" Y="-2.619235351562" />
                  <Point X="-2.310626220703" Y="-2.588301269531" />
                  <Point X="-2.314666259766" Y="-2.557615234375" />
                  <Point X="-2.323650878906" Y="-2.527997558594" />
                  <Point X="-2.329357421875" Y="-2.513557128906" />
                  <Point X="-2.343574462891" Y="-2.484728271484" />
                  <Point X="-2.351557128906" Y="-2.47141015625" />
                  <Point X="-2.369584716797" Y="-2.446253173828" />
                  <Point X="-2.379629638672" Y="-2.434414306641" />
                  <Point X="-2.396978271484" Y="-2.417065673828" />
                  <Point X="-2.408817138672" Y="-2.407020751953" />
                  <Point X="-2.433974121094" Y="-2.388993164062" />
                  <Point X="-2.447292236328" Y="-2.381010498047" />
                  <Point X="-2.47612109375" Y="-2.366793457031" />
                  <Point X="-2.490561523438" Y="-2.361086914062" />
                  <Point X="-2.520179199219" Y="-2.352102294922" />
                  <Point X="-2.550865234375" Y="-2.348062255859" />
                  <Point X="-2.581799316406" Y="-2.349074951172" />
                  <Point X="-2.597224609375" Y="-2.350849609375" />
                  <Point X="-2.628750976562" Y="-2.357120605469" />
                  <Point X="-2.643680908203" Y="-2.361384033203" />
                  <Point X="-2.672647216797" Y="-2.372286132812" />
                  <Point X="-2.68668359375" Y="-2.378924804688" />
                  <Point X="-3.696237548828" Y="-2.961791015625" />
                  <Point X="-3.793089599609" Y="-3.017708496094" />
                  <Point X="-4.004014892578" Y="-2.740595703125" />
                  <Point X="-4.060779785156" Y="-2.645409423828" />
                  <Point X="-4.181265136719" Y="-2.443373779297" />
                  <Point X="-3.330815185547" Y="-1.790799804688" />
                  <Point X="-3.048122314453" Y="-1.573881958008" />
                  <Point X="-3.039158447266" Y="-1.566065429688" />
                  <Point X="-3.016266845703" Y="-1.54342980957" />
                  <Point X="-3.002626708984" Y="-1.526424804688" />
                  <Point X="-2.983580078125" Y="-1.496115600586" />
                  <Point X="-2.975497070312" Y="-1.480055175781" />
                  <Point X="-2.967111328125" Y="-1.458530883789" />
                  <Point X="-2.960219238281" Y="-1.437196289062" />
                  <Point X="-2.954186767578" Y="-1.413905151367" />
                  <Point X="-2.951953125" Y="-1.402396606445" />
                  <Point X="-2.947838623047" Y="-1.370913696289" />
                  <Point X="-2.94708203125" Y="-1.355699462891" />
                  <Point X="-2.94778125" Y="-1.332831176758" />
                  <Point X="-2.951229248047" Y="-1.310212646484" />
                  <Point X="-2.957375732422" Y="-1.288174682617" />
                  <Point X="-2.961114746094" Y="-1.277341796875" />
                  <Point X="-2.973119628906" Y="-1.24835925293" />
                  <Point X="-2.978694580078" Y="-1.237077758789" />
                  <Point X="-2.991286865234" Y="-1.215350585938" />
                  <Point X="-3.006640380859" Y="-1.195478027344" />
                  <Point X="-3.024485839844" Y="-1.177809326172" />
                  <Point X="-3.033994628906" Y="-1.169567504883" />
                  <Point X="-3.052244628906" Y="-1.155755126953" />
                  <Point X="-3.070533203125" Y="-1.143512329102" />
                  <Point X="-3.091268066406" Y="-1.13130859375" />
                  <Point X="-3.10543359375" Y="-1.124481445312" />
                  <Point X="-3.134697021484" Y="-1.113257202148" />
                  <Point X="-3.149795410156" Y="-1.108860107422" />
                  <Point X="-3.181683105469" Y="-1.102378540039" />
                  <Point X="-3.197298828125" Y="-1.100532470703" />
                  <Point X="-3.228622070312" Y="-1.09944140625" />
                  <Point X="-3.244329101562" Y="-1.100196411133" />
                  <Point X="-4.518795410156" Y="-1.267982910156" />
                  <Point X="-4.660920898438" Y="-1.286694213867" />
                  <Point X="-4.740762207031" Y="-0.974118164063" />
                  <Point X="-4.755779785156" Y="-0.86911517334" />
                  <Point X="-4.786452148438" Y="-0.65465435791" />
                  <Point X="-3.830317871094" Y="-0.398458831787" />
                  <Point X="-3.508288085938" Y="-0.312171356201" />
                  <Point X="-3.498185302734" Y="-0.308847991943" />
                  <Point X="-3.478414550781" Y="-0.30109286499" />
                  <Point X="-3.468746582031" Y="-0.296661071777" />
                  <Point X="-3.447155029297" Y="-0.285260040283" />
                  <Point X="-3.427537353516" Y="-0.27333291626" />
                  <Point X="-3.405807861328" Y="-0.258251251221" />
                  <Point X="-3.396477294922" Y="-0.250868270874" />
                  <Point X="-3.372522949219" Y="-0.229342315674" />
                  <Point X="-3.358068359375" Y="-0.212980133057" />
                  <Point X="-3.337571533203" Y="-0.183554428101" />
                  <Point X="-3.328724121094" Y="-0.167865783691" />
                  <Point X="-3.319291015625" Y="-0.146659088135" />
                  <Point X="-3.3114296875" Y="-0.12575705719" />
                  <Point X="-3.304186523438" Y="-0.102419250488" />
                  <Point X="-3.301576416016" Y="-0.091937767029" />
                  <Point X="-3.296133544922" Y="-0.063198699951" />
                  <Point X="-3.294500244141" Y="-0.043298835754" />
                  <Point X="-3.295269042969" Y="-0.010439947128" />
                  <Point X="-3.297085449219" Y="0.005955834866" />
                  <Point X="-3.301340332031" Y="0.027245769501" />
                  <Point X="-3.306194335938" Y="0.04632850647" />
                  <Point X="-3.3134375" Y="0.069666313171" />
                  <Point X="-3.317668945312" Y="0.080786277771" />
                  <Point X="-3.330984863281" Y="0.110110023499" />
                  <Point X="-3.342316894531" Y="0.1289246521" />
                  <Point X="-3.364403564453" Y="0.157502304077" />
                  <Point X="-3.376817626953" Y="0.170731826782" />
                  <Point X="-3.394659667969" Y="0.186429977417" />
                  <Point X="-3.411831298828" Y="0.19987171936" />
                  <Point X="-3.433560791016" Y="0.214953384399" />
                  <Point X="-3.440484130859" Y="0.219328842163" />
                  <Point X="-3.465616210938" Y="0.232834655762" />
                  <Point X="-3.496566894531" Y="0.24563583374" />
                  <Point X="-3.508288085938" Y="0.249611358643" />
                  <Point X="-4.670024414062" Y="0.560897399902" />
                  <Point X="-4.785446289062" Y="0.591824523926" />
                  <Point X="-4.731331542969" Y="0.957527832031" />
                  <Point X="-4.701098632812" Y="1.069095947266" />
                  <Point X="-4.633586425781" Y="1.318237060547" />
                  <Point X="-4.008905029297" Y="1.23599621582" />
                  <Point X="-3.778066162109" Y="1.20560559082" />
                  <Point X="-3.76773828125" Y="1.204815429688" />
                  <Point X="-3.747058105469" Y="1.204364379883" />
                  <Point X="-3.736705810547" Y="1.204703125" />
                  <Point X="-3.715144287109" Y="1.206589599609" />
                  <Point X="-3.704890625" Y="1.208053588867" />
                  <Point X="-3.684602539062" Y="1.212089233398" />
                  <Point X="-3.661236572266" Y="1.218864624023" />
                  <Point X="-3.613142089844" Y="1.234028564453" />
                  <Point X="-3.603448242188" Y="1.237677246094" />
                  <Point X="-3.584516845703" Y="1.246007568359" />
                  <Point X="-3.575279541016" Y="1.250688842773" />
                  <Point X="-3.55653515625" Y="1.261510864258" />
                  <Point X="-3.547860351562" Y="1.267171264648" />
                  <Point X="-3.531179199219" Y="1.27940234375" />
                  <Point X="-3.515928710938" Y="1.293376831055" />
                  <Point X="-3.502290283203" Y="1.308928466797" />
                  <Point X="-3.495895751953" Y="1.317076416016" />
                  <Point X="-3.483481201172" Y="1.334806152344" />
                  <Point X="-3.478011474609" Y="1.343602050781" />
                  <Point X="-3.468062255859" Y="1.361737060547" />
                  <Point X="-3.458233154297" Y="1.383991210938" />
                  <Point X="-3.438935058594" Y="1.430580932617" />
                  <Point X="-3.435499267578" Y="1.44035144043" />
                  <Point X="-3.4297109375" Y="1.460210205078" />
                  <Point X="-3.427358642578" Y="1.470297973633" />
                  <Point X="-3.423600341797" Y="1.491613037109" />
                  <Point X="-3.422360595703" Y="1.501897094727" />
                  <Point X="-3.421008056641" Y="1.522537231445" />
                  <Point X="-3.421910400391" Y="1.543200683594" />
                  <Point X="-3.425056884766" Y="1.563644165039" />
                  <Point X="-3.427188232422" Y="1.573780639648" />
                  <Point X="-3.432790039062" Y="1.594686889648" />
                  <Point X="-3.436011962891" Y="1.604530151367" />
                  <Point X="-3.443508789062" Y="1.62380859375" />
                  <Point X="-3.454238037109" Y="1.645642333984" />
                  <Point X="-3.477523193359" Y="1.690372680664" />
                  <Point X="-3.482799560547" Y="1.699286132812" />
                  <Point X="-3.494290771484" Y="1.716484130859" />
                  <Point X="-3.500505859375" Y="1.72476940918" />
                  <Point X="-3.514418212891" Y="1.741349853516" />
                  <Point X="-3.521499511719" Y="1.74891027832" />
                  <Point X="-3.536442138672" Y="1.76321484375" />
                  <Point X="-3.544303466797" Y="1.769958740234" />
                  <Point X="-4.210678222656" Y="2.281286376953" />
                  <Point X="-4.227614746094" Y="2.294282226563" />
                  <Point X="-4.002292724609" Y="2.680312988281" />
                  <Point X="-3.922214111328" Y="2.783241699219" />
                  <Point X="-3.726338378906" Y="3.035012451172" />
                  <Point X="-3.395931884766" Y="2.844252197266" />
                  <Point X="-3.254156982422" Y="2.7623984375" />
                  <Point X="-3.244917480469" Y="2.757716064453" />
                  <Point X="-3.225984619141" Y="2.749385498047" />
                  <Point X="-3.216291259766" Y="2.745737304688" />
                  <Point X="-3.195648925781" Y="2.739228759766" />
                  <Point X="-3.185615722656" Y="2.736657226562" />
                  <Point X="-3.165328857422" Y="2.732621826172" />
                  <Point X="-3.1365078125" Y="2.729533447266" />
                  <Point X="-3.069525878906" Y="2.723673339844" />
                  <Point X="-3.059173583984" Y="2.723334472656" />
                  <Point X="-3.038492675781" Y="2.723785644531" />
                  <Point X="-3.0281640625" Y="2.724575683594" />
                  <Point X="-3.006705566406" Y="2.727400878906" />
                  <Point X="-2.996525146484" Y="2.729310791016" />
                  <Point X="-2.976433837891" Y="2.734227294922" />
                  <Point X="-2.956998046875" Y="2.741301513672" />
                  <Point X="-2.938447021484" Y="2.750449707031" />
                  <Point X="-2.929420898438" Y="2.755530273438" />
                  <Point X="-2.911166503906" Y="2.767159423828" />
                  <Point X="-2.902746337891" Y="2.773193359375" />
                  <Point X="-2.886614257812" Y="2.786140869141" />
                  <Point X="-2.865723144531" Y="2.806233642578" />
                  <Point X="-2.818178710938" Y="2.853777832031" />
                  <Point X="-2.811265380859" Y="2.861489501953" />
                  <Point X="-2.798318359375" Y="2.877620849609" />
                  <Point X="-2.792284667969" Y="2.886040527344" />
                  <Point X="-2.780655273438" Y="2.904294921875" />
                  <Point X="-2.775574707031" Y="2.913320800781" />
                  <Point X="-2.766426025391" Y="2.931872314453" />
                  <Point X="-2.759351318359" Y="2.951309570312" />
                  <Point X="-2.754434814453" Y="2.971401367188" />
                  <Point X="-2.752524902344" Y="2.981581542969" />
                  <Point X="-2.749699707031" Y="3.003040283203" />
                  <Point X="-2.748909667969" Y="3.013368652344" />
                  <Point X="-2.748458496094" Y="3.034049316406" />
                  <Point X="-2.750421875" Y="3.062968994141" />
                  <Point X="-2.756281982422" Y="3.129951171875" />
                  <Point X="-2.757745849609" Y="3.140204833984" />
                  <Point X="-2.76178125" Y="3.160491699219" />
                  <Point X="-2.764352783203" Y="3.170524902344" />
                  <Point X="-2.770861328125" Y="3.191167236328" />
                  <Point X="-2.774509521484" Y="3.200860595703" />
                  <Point X="-2.782840087891" Y="3.219793457031" />
                  <Point X="-2.787522460938" Y="3.229032958984" />
                  <Point X="-3.059387451172" Y="3.699916259766" />
                  <Point X="-2.648373291016" Y="4.015036865234" />
                  <Point X="-2.522257324219" Y="4.085104492188" />
                  <Point X="-2.192524902344" Y="4.268296875" />
                  <Point X="-2.161965576172" Y="4.228471191406" />
                  <Point X="-2.118563964844" Y="4.171909179688" />
                  <Point X="-2.111821533203" Y="4.164049316406" />
                  <Point X="-2.097518798828" Y="4.149107910156" />
                  <Point X="-2.089958740234" Y="4.142026367188" />
                  <Point X="-2.07337890625" Y="4.128113769531" />
                  <Point X="-2.065091552734" Y="4.121897460938" />
                  <Point X="-2.047892211914" Y="4.110405273438" />
                  <Point X="-2.038979980469" Y="4.105129394531" />
                  <Point X="-2.018314697266" Y="4.094371337891" />
                  <Point X="-1.943763916016" Y="4.055562255859" />
                  <Point X="-1.934328857422" Y="4.051287353516" />
                  <Point X="-1.915048828125" Y="4.043789794922" />
                  <Point X="-1.905203857422" Y="4.040567382812" />
                  <Point X="-1.884297363281" Y="4.034965820313" />
                  <Point X="-1.874162597656" Y="4.032834716797" />
                  <Point X="-1.853719604492" Y="4.029688232422" />
                  <Point X="-1.833053344727" Y="4.028785888672" />
                  <Point X="-1.81241394043" Y="4.030138916016" />
                  <Point X="-1.802132568359" Y="4.031378662109" />
                  <Point X="-1.780817260742" Y="4.035136962891" />
                  <Point X="-1.770729980469" Y="4.037489013672" />
                  <Point X="-1.750869750977" Y="4.043277587891" />
                  <Point X="-1.741096923828" Y="4.046714355469" />
                  <Point X="-1.719572387695" Y="4.055630371094" />
                  <Point X="-1.641922729492" Y="4.087793945312" />
                  <Point X="-1.632583618164" Y="4.092273681641" />
                  <Point X="-1.614450073242" Y="4.102222167969" />
                  <Point X="-1.605655273438" Y="4.10769140625" />
                  <Point X="-1.587925537109" Y="4.120105957031" />
                  <Point X="-1.57977734375" Y="4.126500976563" />
                  <Point X="-1.564225341797" Y="4.140139648437" />
                  <Point X="-1.550251220703" Y="4.155389648437" />
                  <Point X="-1.538020141602" Y="4.172070800781" />
                  <Point X="-1.532359863281" Y="4.180745605469" />
                  <Point X="-1.521538085938" Y="4.199489746094" />
                  <Point X="-1.516856201172" Y="4.208728515625" />
                  <Point X="-1.508526000977" Y="4.22766015625" />
                  <Point X="-1.504877929688" Y="4.237353027344" />
                  <Point X="-1.497871948242" Y="4.259572265625" />
                  <Point X="-1.472598510742" Y="4.339729492188" />
                  <Point X="-1.470026855469" Y="4.349763183594" />
                  <Point X="-1.465991088867" Y="4.37005078125" />
                  <Point X="-1.46452722168" Y="4.3803046875" />
                  <Point X="-1.46264074707" Y="4.401866210938" />
                  <Point X="-1.462301757812" Y="4.41221875" />
                  <Point X="-1.462752929688" Y="4.432898925781" />
                  <Point X="-1.46354309082" Y="4.4432265625" />
                  <Point X="-1.479266113281" Y="4.562655273438" />
                  <Point X="-0.931178405762" Y="4.716319824219" />
                  <Point X="-0.778295715332" Y="4.734212890625" />
                  <Point X="-0.365221923828" Y="4.782557128906" />
                  <Point X="-0.267607177734" Y="4.418253417969" />
                  <Point X="-0.225666244507" Y="4.261727539062" />
                  <Point X="-0.220435317993" Y="4.247107910156" />
                  <Point X="-0.207661773682" Y="4.218916503906" />
                  <Point X="-0.200119155884" Y="4.205344726562" />
                  <Point X="-0.182260925293" Y="4.178618164062" />
                  <Point X="-0.172608306885" Y="4.166456054688" />
                  <Point X="-0.151451248169" Y="4.1438671875" />
                  <Point X="-0.126898002625" Y="4.125026367188" />
                  <Point X="-0.099602806091" Y="4.110436523438" />
                  <Point X="-0.085356712341" Y="4.104260742188" />
                  <Point X="-0.054918586731" Y="4.093927978516" />
                  <Point X="-0.039857292175" Y="4.090155273438" />
                  <Point X="-0.009319890976" Y="4.08511328125" />
                  <Point X="0.02163187027" Y="4.08511328125" />
                  <Point X="0.052169120789" Y="4.090155273438" />
                  <Point X="0.067230415344" Y="4.093927978516" />
                  <Point X="0.097668540955" Y="4.104260742188" />
                  <Point X="0.111914489746" Y="4.110436523438" />
                  <Point X="0.139209686279" Y="4.125026367188" />
                  <Point X="0.163763076782" Y="4.143866699219" />
                  <Point X="0.184920150757" Y="4.166455566406" />
                  <Point X="0.194573059082" Y="4.178618164062" />
                  <Point X="0.212431289673" Y="4.205344726562" />
                  <Point X="0.219973449707" Y="4.218916015625" />
                  <Point X="0.232746994019" Y="4.247107421875" />
                  <Point X="0.237978225708" Y="4.261727539062" />
                  <Point X="0.378190246582" Y="4.785006347656" />
                  <Point X="0.827875976562" Y="4.737912109375" />
                  <Point X="0.954369628906" Y="4.707372558594" />
                  <Point X="1.453599365234" Y="4.586843261719" />
                  <Point X="1.533797729492" Y="4.557754394531" />
                  <Point X="1.858255737305" Y="4.440070800781" />
                  <Point X="1.937876464844" Y="4.402834960938" />
                  <Point X="2.250450683594" Y="4.256654296875" />
                  <Point X="2.327425292969" Y="4.211809082031" />
                  <Point X="2.629433105469" Y="4.035858398438" />
                  <Point X="2.701979736328" Y="3.984267089844" />
                  <Point X="2.817779785156" Y="3.901916503906" />
                  <Point X="2.252312255859" Y="2.922498046875" />
                  <Point X="2.06530859375" Y="2.598597900391" />
                  <Point X="2.060837646484" Y="2.589835205078" />
                  <Point X="2.04928515625" Y="2.562717041016" />
                  <Point X="2.039624023437" Y="2.533208984375" />
                  <Point X="2.036642089844" Y="2.523173828125" />
                  <Point X="2.01983215332" Y="2.4603125" />
                  <Point X="2.01733190918" Y="2.447482421875" />
                  <Point X="2.014116455078" Y="2.421600341797" />
                  <Point X="2.013401123047" Y="2.408548339844" />
                  <Point X="2.013880493164" Y="2.374578125" />
                  <Point X="2.015227905273" Y="2.354512695312" />
                  <Point X="2.021782348633" Y="2.300155517578" />
                  <Point X="2.02380065918" Y="2.289033935547" />
                  <Point X="2.029143676758" Y="2.267110595703" />
                  <Point X="2.032468261719" Y="2.256308837891" />
                  <Point X="2.040734375" Y="2.234220214844" />
                  <Point X="2.045318237305" Y="2.223889160156" />
                  <Point X="2.055681396484" Y="2.203843505859" />
                  <Point X="2.070783935547" Y="2.180388671875" />
                  <Point X="2.104418212891" Y="2.1308203125" />
                  <Point X="2.112563964844" Y="2.120446533203" />
                  <Point X="2.130213867188" Y="2.100927001953" />
                  <Point X="2.139718017578" Y="2.09178125" />
                  <Point X="2.166758300781" Y="2.069157470703" />
                  <Point X="2.181998535156" Y="2.057657470703" />
                  <Point X="2.231566894531" Y="2.0240234375" />
                  <Point X="2.241280517578" Y="2.018244873047" />
                  <Point X="2.261325683594" Y="2.007882080078" />
                  <Point X="2.271657226562" Y="2.003297851562" />
                  <Point X="2.293745605469" Y="1.995031982422" />
                  <Point X="2.304547119141" Y="1.991707763672" />
                  <Point X="2.326469726562" Y="1.986364868164" />
                  <Point X="2.352658447266" Y="1.982529663086" />
                  <Point X="2.407015625" Y="1.975974975586" />
                  <Point X="2.420460693359" Y="1.975314331055" />
                  <Point X="2.447307617188" Y="1.975900024414" />
                  <Point X="2.460709472656" Y="1.977146118164" />
                  <Point X="2.497037353516" Y="1.983143432617" />
                  <Point X="2.515173583984" Y="1.987055664062" />
                  <Point X="2.578034912109" Y="2.003865600586" />
                  <Point X="2.583995117188" Y="2.005670776367" />
                  <Point X="2.604405761719" Y="2.013067626953" />
                  <Point X="2.627655517578" Y="2.02357421875" />
                  <Point X="2.636033935547" Y="2.027872558594" />
                  <Point X="3.804517089844" Y="2.702496582031" />
                  <Point X="3.940404785156" Y="2.780951416016" />
                  <Point X="4.043951904297" Y="2.637044433594" />
                  <Point X="4.084396972656" Y="2.570208740234" />
                  <Point X="4.136884765625" Y="2.483472412109" />
                  <Point X="3.419159667969" Y="1.932741821289" />
                  <Point X="3.172951660156" Y="1.743819824219" />
                  <Point X="3.168137939453" Y="1.739868896484" />
                  <Point X="3.152119628906" Y="1.725217163086" />
                  <Point X="3.134668457031" Y="1.706603393555" />
                  <Point X="3.128575439453" Y="1.69942175293" />
                  <Point X="3.116034667969" Y="1.683061157227" />
                  <Point X="3.070793212891" Y="1.624040283203" />
                  <Point X="3.063425048828" Y="1.612880737305" />
                  <Point X="3.050329345703" Y="1.589638793945" />
                  <Point X="3.044601806641" Y="1.577556396484" />
                  <Point X="3.0314609375" Y="1.543900512695" />
                  <Point X="3.025469238281" Y="1.525968383789" />
                  <Point X="3.008616699219" Y="1.465708007812" />
                  <Point X="3.006225585938" Y="1.454662109375" />
                  <Point X="3.002771972656" Y="1.43236340332" />
                  <Point X="3.001709472656" Y="1.421110717773" />
                  <Point X="3.000893310547" Y="1.397540527344" />
                  <Point X="3.001174804688" Y="1.386241943359" />
                  <Point X="3.003077636719" Y="1.363757324219" />
                  <Point X="3.008533691406" Y="1.333985961914" />
                  <Point X="3.022367919922" Y="1.266938476562" />
                  <Point X="3.026025390625" Y="1.25395300293" />
                  <Point X="3.035142578125" Y="1.228631225586" />
                  <Point X="3.040602294922" Y="1.216294921875" />
                  <Point X="3.05823046875" Y="1.183063842773" />
                  <Point X="3.067349121094" Y="1.16767175293" />
                  <Point X="3.1049765625" Y="1.110479980469" />
                  <Point X="3.111739501953" Y="1.101423950195" />
                  <Point X="3.12629296875" Y="1.084179321289" />
                  <Point X="3.134083740234" Y="1.075990600586" />
                  <Point X="3.151327636719" Y="1.059900878906" />
                  <Point X="3.160034667969" Y="1.052695922852" />
                  <Point X="3.178244140625" Y="1.039370361328" />
                  <Point X="3.202861083984" Y="1.024741699219" />
                  <Point X="3.257388427734" Y="0.994047729492" />
                  <Point X="3.265478759766" Y="0.98998840332" />
                  <Point X="3.294676269531" Y="0.978085021973" />
                  <Point X="3.330273925781" Y="0.968021484375" />
                  <Point X="3.343670410156" Y="0.965257568359" />
                  <Point X="3.364106445313" Y="0.962556640625" />
                  <Point X="3.437831054688" Y="0.952812927246" />
                  <Point X="3.444029785156" Y="0.952199707031" />
                  <Point X="3.465716064453" Y="0.95122277832" />
                  <Point X="3.491217529297" Y="0.952032348633" />
                  <Point X="3.500603515625" Y="0.952797302246" />
                  <Point X="4.610584472656" Y="1.098929199219" />
                  <Point X="4.704703613281" Y="1.11132019043" />
                  <Point X="4.752684082031" Y="0.914231872559" />
                  <Point X="4.765428710938" Y="0.83237322998" />
                  <Point X="4.78387109375" Y="0.713920837402" />
                  <Point X="3.974151611328" Y="0.496957336426" />
                  <Point X="3.691991943359" Y="0.421352844238" />
                  <Point X="3.686031738281" Y="0.419544403076" />
                  <Point X="3.665626708984" Y="0.412138092041" />
                  <Point X="3.642381103516" Y="0.401619445801" />
                  <Point X="3.634004394531" Y="0.39731652832" />
                  <Point X="3.613926513672" Y="0.385710998535" />
                  <Point X="3.541494384766" Y="0.343843933105" />
                  <Point X="3.530206787109" Y="0.336188812256" />
                  <Point X="3.508843994141" Y="0.319340759277" />
                  <Point X="3.498768798828" Y="0.310147979736" />
                  <Point X="3.47241015625" Y="0.282377349854" />
                  <Point X="3.460750244141" Y="0.268876312256" />
                  <Point X="3.417291015625" Y="0.213499099731" />
                  <Point X="3.410854003906" Y="0.204207641602" />
                  <Point X="3.399129882813" Y="0.184927246094" />
                  <Point X="3.393842529297" Y="0.174938156128" />
                  <Point X="3.384068847656" Y="0.153474624634" />
                  <Point X="3.380005371094" Y="0.142928497314" />
                  <Point X="3.373159179688" Y="0.121427658081" />
                  <Point X="3.370376464844" Y="0.110472961426" />
                  <Point X="3.366360839844" Y="0.089504943848" />
                  <Point X="3.351874267578" Y="0.013862573624" />
                  <Point X="3.350603515625" Y="0.00496778965" />
                  <Point X="3.348584472656" Y="-0.02626219368" />
                  <Point X="3.350280029297" Y="-0.062940692902" />
                  <Point X="3.351874267578" Y="-0.076422721863" />
                  <Point X="3.355889892578" Y="-0.097390594482" />
                  <Point X="3.370376464844" Y="-0.173033416748" />
                  <Point X="3.373159179688" Y="-0.183988113403" />
                  <Point X="3.380005371094" Y="-0.20548878479" />
                  <Point X="3.384068847656" Y="-0.21603477478" />
                  <Point X="3.393842529297" Y="-0.237498291016" />
                  <Point X="3.399130371094" Y="-0.247488128662" />
                  <Point X="3.410854248047" Y="-0.266767791748" />
                  <Point X="3.429336669922" Y="-0.291408294678" />
                  <Point X="3.472795898438" Y="-0.346785491943" />
                  <Point X="3.478717285156" Y="-0.353632232666" />
                  <Point X="3.501139404297" Y="-0.375805297852" />
                  <Point X="3.530176025391" Y="-0.398724731445" />
                  <Point X="3.541494384766" Y="-0.406404083252" />
                  <Point X="3.561572265625" Y="-0.41800958252" />
                  <Point X="3.634004394531" Y="-0.459876525879" />
                  <Point X="3.639495605469" Y="-0.462814788818" />
                  <Point X="3.659158447266" Y="-0.472016937256" />
                  <Point X="3.683028076172" Y="-0.481027770996" />
                  <Point X="3.691991943359" Y="-0.483912841797" />
                  <Point X="4.70989453125" Y="-0.756659057617" />
                  <Point X="4.784876953125" Y="-0.776750610352" />
                  <Point X="4.761613769531" Y="-0.931050048828" />
                  <Point X="4.745284179688" Y="-1.002608703613" />
                  <Point X="4.727801757812" Y="-1.079219726563" />
                  <Point X="3.7640078125" Y="-0.952333618164" />
                  <Point X="3.436781982422" Y="-0.90925378418" />
                  <Point X="3.428624511719" Y="-0.908535766602" />
                  <Point X="3.400098632812" Y="-0.908042480469" />
                  <Point X="3.366721435547" Y="-0.910840942383" />
                  <Point X="3.354480712891" Y="-0.912676269531" />
                  <Point X="3.315074951172" Y="-0.921241394043" />
                  <Point X="3.172916503906" Y="-0.952140136719" />
                  <Point X="3.157874023438" Y="-0.956742492676" />
                  <Point X="3.12875390625" Y="-0.968366882324" />
                  <Point X="3.114676269531" Y="-0.975389038086" />
                  <Point X="3.086849609375" Y="-0.99228137207" />
                  <Point X="3.074123779297" Y="-1.001530456543" />
                  <Point X="3.050373779297" Y="-1.022001525879" />
                  <Point X="3.039349609375" Y="-1.033223144531" />
                  <Point X="3.01553125" Y="-1.061869262695" />
                  <Point X="2.92960546875" Y="-1.165211303711" />
                  <Point X="2.921326171875" Y="-1.176847290039" />
                  <Point X="2.90660546875" Y="-1.201229370117" />
                  <Point X="2.9001640625" Y="-1.213975830078" />
                  <Point X="2.888820800781" Y="-1.241360839844" />
                  <Point X="2.884362792969" Y="-1.254927978516" />
                  <Point X="2.877531005859" Y="-1.282577636719" />
                  <Point X="2.875157226562" Y="-1.296659912109" />
                  <Point X="2.871743408203" Y="-1.33375793457" />
                  <Point X="2.859428222656" Y="-1.467590209961" />
                  <Point X="2.859288818359" Y="-1.483321166992" />
                  <Point X="2.861607666016" Y="-1.514590454102" />
                  <Point X="2.864065917969" Y="-1.530128662109" />
                  <Point X="2.871797607422" Y="-1.561749633789" />
                  <Point X="2.876786376953" Y="-1.576668823242" />
                  <Point X="2.889157714844" Y="-1.605479980469" />
                  <Point X="2.896540527344" Y="-1.619371826172" />
                  <Point X="2.918348388672" Y="-1.653292358398" />
                  <Point X="2.997020751953" Y="-1.775662231445" />
                  <Point X="3.001741943359" Y="-1.782353149414" />
                  <Point X="3.019792724609" Y="-1.804448852539" />
                  <Point X="3.043488769531" Y="-1.828119873047" />
                  <Point X="3.052796142578" Y="-1.836277709961" />
                  <Point X="3.997416015625" Y="-2.561110595703" />
                  <Point X="4.087170654297" Y="-2.629981933594" />
                  <Point X="4.045488525391" Y="-2.697429931641" />
                  <Point X="4.011717773438" Y="-2.745413330078" />
                  <Point X="4.001274414063" Y="-2.760251708984" />
                  <Point X="3.139893310547" Y="-2.262933105469" />
                  <Point X="2.848454589844" Y="-2.094670654297" />
                  <Point X="2.841191894531" Y="-2.090885253906" />
                  <Point X="2.815026123047" Y="-2.079513183594" />
                  <Point X="2.783119140625" Y="-2.069325927734" />
                  <Point X="2.771108398438" Y="-2.066337646484" />
                  <Point X="2.724209228516" Y="-2.057867675781" />
                  <Point X="2.555018066406" Y="-2.027311767578" />
                  <Point X="2.539359130859" Y="-2.025807495117" />
                  <Point X="2.508006835938" Y="-2.025403198242" />
                  <Point X="2.492313476562" Y="-2.026503662109" />
                  <Point X="2.460140625" Y="-2.031461425781" />
                  <Point X="2.444844482422" Y="-2.035136230469" />
                  <Point X="2.415068603516" Y="-2.044959838867" />
                  <Point X="2.400588867188" Y="-2.051108642578" />
                  <Point X="2.361626953125" Y="-2.071614013672" />
                  <Point X="2.221070800781" Y="-2.145587646484" />
                  <Point X="2.208968505859" Y="-2.153170166016" />
                  <Point X="2.186037353516" Y="-2.170063476562" />
                  <Point X="2.175208496094" Y="-2.179374267578" />
                  <Point X="2.154248779297" Y="-2.200334228516" />
                  <Point X="2.144938232422" Y="-2.211162841797" />
                  <Point X="2.128045898438" Y="-2.234093017578" />
                  <Point X="2.120464111328" Y="-2.246194580078" />
                  <Point X="2.099958740234" Y="-2.28515625" />
                  <Point X="2.025985107422" Y="-2.425712646484" />
                  <Point X="2.019836303711" Y="-2.440192382812" />
                  <Point X="2.010012329102" Y="-2.469968505859" />
                  <Point X="2.006337280273" Y="-2.485264892578" />
                  <Point X="2.001379516602" Y="-2.5174375" />
                  <Point X="2.000279174805" Y="-2.533130859375" />
                  <Point X="2.00068347168" Y="-2.564483154297" />
                  <Point X="2.002187866211" Y="-2.580142089844" />
                  <Point X="2.010657958984" Y="-2.627041259766" />
                  <Point X="2.041213378906" Y="-2.796232421875" />
                  <Point X="2.043015014648" Y="-2.804221435547" />
                  <Point X="2.051236083984" Y="-2.83154296875" />
                  <Point X="2.064069580078" Y="-2.862479736328" />
                  <Point X="2.069546630859" Y="-2.873578613281" />
                  <Point X="2.676560058594" Y="-3.924956298828" />
                  <Point X="2.735893310547" Y="-4.027724609375" />
                  <Point X="2.723754394531" Y="-4.036083740234" />
                  <Point X="2.056309326172" Y="-3.166253173828" />
                  <Point X="1.833914672852" Y="-2.876422851562" />
                  <Point X="1.828654418945" Y="-2.870147216797" />
                  <Point X="1.808831054688" Y="-2.849625976563" />
                  <Point X="1.783251831055" Y="-2.828004394531" />
                  <Point X="1.773299438477" Y="-2.820647216797" />
                  <Point X="1.727043945312" Y="-2.790909179688" />
                  <Point X="1.56017590332" Y="-2.683628417969" />
                  <Point X="1.546283935547" Y="-2.676245849609" />
                  <Point X="1.517472900391" Y="-2.663874511719" />
                  <Point X="1.502553588867" Y="-2.658885742188" />
                  <Point X="1.470932617188" Y="-2.651154052734" />
                  <Point X="1.45539440918" Y="-2.648695800781" />
                  <Point X="1.42412512207" Y="-2.646376953125" />
                  <Point X="1.408394042969" Y="-2.646516357422" />
                  <Point X="1.357805786133" Y="-2.651171630859" />
                  <Point X="1.175307128906" Y="-2.667965332031" />
                  <Point X="1.161224731445" Y="-2.670339355469" />
                  <Point X="1.133575317383" Y="-2.677171142578" />
                  <Point X="1.120008422852" Y="-2.68162890625" />
                  <Point X="1.092623168945" Y="-2.692972167969" />
                  <Point X="1.079876953125" Y="-2.699413574219" />
                  <Point X="1.055494750977" Y="-2.714134277344" />
                  <Point X="1.043858764648" Y="-2.722413330078" />
                  <Point X="1.004795898438" Y="-2.754892822266" />
                  <Point X="0.863874938965" Y="-2.872063720703" />
                  <Point X="0.852653137207" Y="-2.883088134766" />
                  <Point X="0.832182250977" Y="-2.906838134766" />
                  <Point X="0.822933044434" Y="-2.919564208984" />
                  <Point X="0.80604083252" Y="-2.947390869141" />
                  <Point X="0.799018859863" Y="-2.961468261719" />
                  <Point X="0.787394165039" Y="-2.990588867188" />
                  <Point X="0.782791748047" Y="-3.005631835938" />
                  <Point X="0.771112243652" Y="-3.0593671875" />
                  <Point X="0.728977478027" Y="-3.253219726562" />
                  <Point X="0.727584594727" Y="-3.261290039062" />
                  <Point X="0.724724487305" Y="-3.289677734375" />
                  <Point X="0.7247421875" Y="-3.323170410156" />
                  <Point X="0.725554992676" Y="-3.335520019531" />
                  <Point X="0.833091430664" Y="-4.152340820313" />
                  <Point X="0.742005187988" Y="-3.812402099609" />
                  <Point X="0.655064941406" Y="-3.487936523438" />
                  <Point X="0.652606262207" Y="-3.480124511719" />
                  <Point X="0.642145141602" Y="-3.453580322266" />
                  <Point X="0.62678692627" Y="-3.423815673828" />
                  <Point X="0.620407531738" Y="-3.413210205078" />
                  <Point X="0.58487286377" Y="-3.362011474609" />
                  <Point X="0.456679962158" Y="-3.177309814453" />
                  <Point X="0.446670654297" Y="-3.165172851562" />
                  <Point X="0.424786804199" Y="-3.142717529297" />
                  <Point X="0.41291229248" Y="-3.132399169922" />
                  <Point X="0.386657165527" Y="-3.113155273438" />
                  <Point X="0.373242462158" Y="-3.104937988281" />
                  <Point X="0.345241607666" Y="-3.090829833984" />
                  <Point X="0.330655151367" Y="-3.084938964844" />
                  <Point X="0.275667358398" Y="-3.067872558594" />
                  <Point X="0.077296051025" Y="-3.006305664062" />
                  <Point X="0.063377368927" Y="-3.003109619141" />
                  <Point X="0.035217639923" Y="-2.998840087891" />
                  <Point X="0.02097659111" Y="-2.997766601562" />
                  <Point X="-0.008664761543" Y="-2.997766601562" />
                  <Point X="-0.022905065536" Y="-2.998840087891" />
                  <Point X="-0.051064498901" Y="-3.003109375" />
                  <Point X="-0.064983482361" Y="-3.006305175781" />
                  <Point X="-0.119971282959" Y="-3.023371337891" />
                  <Point X="-0.318342590332" Y="-3.084938476562" />
                  <Point X="-0.332929473877" Y="-3.090829589844" />
                  <Point X="-0.360930786133" Y="-3.104937988281" />
                  <Point X="-0.374345336914" Y="-3.113155273438" />
                  <Point X="-0.400600463867" Y="-3.132399169922" />
                  <Point X="-0.412475128174" Y="-3.142717773438" />
                  <Point X="-0.434358978271" Y="-3.165173095703" />
                  <Point X="-0.444367980957" Y="-3.177309814453" />
                  <Point X="-0.479902679443" Y="-3.228508544922" />
                  <Point X="-0.608095581055" Y="-3.413210205078" />
                  <Point X="-0.612470458984" Y="-3.420132324219" />
                  <Point X="-0.62597668457" Y="-3.445265136719" />
                  <Point X="-0.638777893066" Y="-3.476215820312" />
                  <Point X="-0.642753112793" Y="-3.487936523438" />
                  <Point X="-0.956393981934" Y="-4.658460449219" />
                  <Point X="-0.985425231934" Y="-4.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.757209707749" Y="-0.646818865914" />
                  <Point X="-4.56542397877" Y="-1.274121719736" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.665390823887" Y="-0.622216058739" />
                  <Point X="-4.46992704419" Y="-1.26154927438" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.122413396943" Y="-2.398215197436" />
                  <Point X="-4.032172679856" Y="-2.693379283213" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.573571940024" Y="-0.597613251563" />
                  <Point X="-4.374430095381" Y="-1.248976875565" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.041949226893" Y="-2.336472794852" />
                  <Point X="-3.860919756788" Y="-2.928593511168" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.481753056161" Y="-0.573010444388" />
                  <Point X="-4.278933146572" Y="-1.23640447675" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.961485056844" Y="-2.274730392268" />
                  <Point X="-3.743149035932" Y="-2.988875337978" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.770147345147" Y="0.695213614781" />
                  <Point X="-4.734352521792" Y="0.578134023081" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.389934172298" Y="-0.548407637213" />
                  <Point X="-4.183436197763" Y="-1.223832077935" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.881020886795" Y="-2.212987989684" />
                  <Point X="-3.658712510081" Y="-2.94012592577" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.737747694006" Y="0.914167974893" />
                  <Point X="-4.626147645957" Y="0.549140665507" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.298115288436" Y="-0.523804830037" />
                  <Point X="-4.087939248954" Y="-1.21125967912" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.800556716745" Y="-2.1512455871" />
                  <Point X="-3.574275993492" Y="-2.891376483265" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.69389765483" Y="1.095669803317" />
                  <Point X="-4.517942761328" Y="0.520147279172" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.206296404573" Y="-0.499202022862" />
                  <Point X="-3.992442300145" Y="-1.198687280304" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.720092546696" Y="-2.089503184516" />
                  <Point X="-3.489839476904" Y="-2.84262704076" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.647220287195" Y="1.267923857049" />
                  <Point X="-4.4097378767" Y="0.491153892837" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.11447752071" Y="-0.474599215687" />
                  <Point X="-3.896945351336" Y="-1.186114881489" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.639628376646" Y="-2.027760781932" />
                  <Point X="-3.405402960315" Y="-2.793877598255" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.560312573296" Y="1.30859037736" />
                  <Point X="-4.301532992072" Y="0.462160506502" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.022658636847" Y="-0.449996408511" />
                  <Point X="-3.801448402527" Y="-1.173542482674" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.559164206597" Y="-1.966018379349" />
                  <Point X="-3.320966443727" Y="-2.74512815575" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.456805683694" Y="1.294963440358" />
                  <Point X="-4.193328107443" Y="0.433167120167" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.930839752985" Y="-0.425393601336" />
                  <Point X="-3.705951453718" Y="-1.160970083859" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.478700036548" Y="-1.904275976765" />
                  <Point X="-3.236529927138" Y="-2.696378713246" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.921113610728" Y="-3.728058997687" />
                  <Point X="-2.861337383049" Y="-3.923578228514" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.353298794092" Y="1.281336503357" />
                  <Point X="-4.085123222815" Y="0.404173733831" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.839020869122" Y="-0.40079079416" />
                  <Point X="-3.610454504909" Y="-1.148397685044" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.398235866498" Y="-1.842533574181" />
                  <Point X="-3.15209341055" Y="-2.647629270741" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.856165554237" Y="-3.615565674444" />
                  <Point X="-2.732331410815" Y="-4.020608906711" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.24979190449" Y="1.267709566355" />
                  <Point X="-3.976918338187" Y="0.375180347496" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.747201971748" Y="-0.376188031177" />
                  <Point X="-3.5149575561" Y="-1.135825286229" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.317771693246" Y="-1.780791182072" />
                  <Point X="-3.067656893961" Y="-2.598879828236" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.791217497745" Y="-3.503072351201" />
                  <Point X="-2.609794341044" Y="-4.09648075835" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.146285014888" Y="1.254082629354" />
                  <Point X="-3.868713453558" Y="0.346186961161" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.65538307296" Y="-0.351585272822" />
                  <Point X="-3.419460607291" Y="-1.123252887414" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.237307503441" Y="-1.719048844106" />
                  <Point X="-2.983220377373" Y="-2.550130385731" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.726269441254" Y="-3.390579027958" />
                  <Point X="-2.493958912348" Y="-4.150432529728" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.042778125286" Y="1.240455692352" />
                  <Point X="-3.76050856893" Y="0.317193574826" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.563564174171" Y="-0.326982514466" />
                  <Point X="-3.323963658482" Y="-1.110680488599" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.156843313636" Y="-1.657306506141" />
                  <Point X="-2.898783860784" Y="-2.501380943226" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.661321384762" Y="-3.278085704714" />
                  <Point X="-2.422921878095" Y="-4.057855355342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.939271224204" Y="1.2268287178" />
                  <Point X="-3.652303684302" Y="0.288200188491" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.472910142803" Y="-0.298569646458" />
                  <Point X="-3.228053013845" Y="-1.099461227866" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.07637912383" Y="-1.595564168176" />
                  <Point X="-2.814347344195" Y="-2.452631500721" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.59637332827" Y="-3.165592381471" />
                  <Point X="-2.346328371344" Y="-3.983452583573" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.187365797476" Y="2.363238346302" />
                  <Point X="-4.147485937726" Y="2.232797202613" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.835764317537" Y="1.213201724982" />
                  <Point X="-3.544098799673" Y="0.259206802156" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.389947246014" Y="-0.245000210774" />
                  <Point X="-3.123139096525" Y="-1.117690345162" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.999632941594" Y="-1.521660775419" />
                  <Point X="-2.729910827607" Y="-2.403882058216" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.531425271779" Y="-3.053099058228" />
                  <Point X="-2.263838893768" Y="-3.928334663414" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.122172621721" Y="2.474929920563" />
                  <Point X="-4.017697411165" Y="2.133206904548" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.733900361384" Y="1.204948581154" />
                  <Point X="-3.430600234822" Y="0.212898568003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.320109718713" Y="-0.14849962593" />
                  <Point X="-2.995678488911" Y="-1.209666363446" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.947634592613" Y="-1.366810867455" />
                  <Point X="-2.643572523442" Y="-2.361353082586" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.466477205683" Y="-2.940605766397" />
                  <Point X="-2.181349416193" Y="-3.873216743255" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.056979445967" Y="2.586621494824" />
                  <Point X="-3.887908884604" Y="2.033616606482" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.640785584769" Y="1.225312714127" />
                  <Point X="-2.548187434181" Y="-2.348414807674" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.40152913214" Y="-2.828112498927" />
                  <Point X="-2.098859919557" Y="-3.818098885441" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.990799558679" Y="2.695085681081" />
                  <Point X="-3.758120358044" Y="1.934026308417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.553180901516" Y="1.2636995504" />
                  <Point X="-2.43699407575" Y="-2.387183051371" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.336929449058" Y="-2.714479697605" />
                  <Point X="-2.010378069929" Y="-3.782581131099" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.919483787298" Y="2.786751147406" />
                  <Point X="-3.628331831483" Y="1.834436010351" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.478184005971" Y="1.343324602115" />
                  <Point X="-1.912405387665" Y="-3.77810649154" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.848168223248" Y="2.878417291878" />
                  <Point X="-3.491781418406" Y="1.712728578065" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.423812223383" Y="1.49041135855" />
                  <Point X="-1.811033326861" Y="-3.784750718175" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.776852659198" Y="2.970083436351" />
                  <Point X="-1.709661266058" Y="-3.79139494481" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.691151826898" Y="3.0146974885" />
                  <Point X="-1.607144029605" Y="-3.801784872219" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.570517502948" Y="2.945049238012" />
                  <Point X="-1.49044263107" Y="-3.858569103313" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.449883178998" Y="2.875400987525" />
                  <Point X="-1.354709155058" Y="-3.977604454859" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.136137293003" Y="-4.692520802187" />
                  <Point X="-1.122823486687" Y="-4.736068300438" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.329248856407" Y="2.805752741484" />
                  <Point X="-1.208725047953" Y="-4.130168109956" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.173560374383" Y="-4.24518657458" />
                  <Point X="-1.015893134033" Y="-4.760892880529" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.211054799366" Y="2.744086244407" />
                  <Point X="-0.952412283194" Y="-4.643600543835" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.106461166352" Y="2.726904729869" />
                  <Point X="-0.90601312778" Y="-4.470436498931" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.007250214265" Y="2.727329171338" />
                  <Point X="-0.859613972367" Y="-4.297272454027" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.91863265392" Y="2.762403035924" />
                  <Point X="-0.813214816953" Y="-4.124108409122" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.840426284758" Y="2.831530372452" />
                  <Point X="-0.76681566154" Y="-3.950944364218" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.01660582362" Y="3.732716522345" />
                  <Point X="-2.947136230053" Y="3.505491720321" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.76972065657" Y="2.925191527235" />
                  <Point X="-0.720416506126" Y="-3.777780319314" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.936128948587" Y="3.7944173688" />
                  <Point X="-0.674017350712" Y="-3.60461627441" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.855652073554" Y="3.856118215255" />
                  <Point X="-0.624331266214" Y="-3.442203290109" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.775175198521" Y="3.91781906171" />
                  <Point X="-0.556557254381" Y="-3.338953250296" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.694698323488" Y="3.979519908166" />
                  <Point X="-0.487594587553" Y="-3.239591125781" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.612337245683" Y="4.035057805052" />
                  <Point X="-0.416582488306" Y="-3.146932392666" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.52742035396" Y="4.082236011288" />
                  <Point X="-0.334198618378" Y="-3.091469045457" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.442503420203" Y="4.129414080041" />
                  <Point X="-0.243916595557" Y="-3.061839392316" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.357586483725" Y="4.176592139893" />
                  <Point X="-0.153185160412" Y="-3.033679700658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.272669547248" Y="4.223770199746" />
                  <Point X="-0.062395351629" Y="-3.00571094056" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.183244259726" Y="4.25620210778" />
                  <Point X="0.034835938108" Y="-2.998811315313" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.037476722018" Y="4.104346819262" />
                  <Point X="0.142671165857" Y="-3.026595608476" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.920238923637" Y="4.045808103329" />
                  <Point X="0.252426228124" Y="-3.0606593974" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.816035079356" Y="4.029901530289" />
                  <Point X="0.363858793723" Y="-3.10021005249" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.723999768392" Y="4.053796436312" />
                  <Point X="0.510462459982" Y="-3.254800194268" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.63593172215" Y="4.090667680542" />
                  <Point X="0.794432170714" Y="-3.8586944223" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.55484560439" Y="4.150375783827" />
                  <Point X="0.72473300224" Y="-3.305789870707" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.493311583915" Y="4.274035915716" />
                  <Point X="0.761813847652" Y="-3.102147007136" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.477039216863" Y="4.54574024522" />
                  <Point X="0.811228292511" Y="-2.938845529609" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.390480319469" Y="4.587547692908" />
                  <Point X="0.884825926602" Y="-2.854643699907" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.298982447838" Y="4.613200483782" />
                  <Point X="0.964032083801" Y="-2.788786522696" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.207484576206" Y="4.638853274656" />
                  <Point X="1.043238223853" Y="-2.722929289399" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.115986704575" Y="4.66450606553" />
                  <Point X="1.129044426232" Y="-2.678659887252" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.024488832944" Y="4.690158856404" />
                  <Point X="1.223752544057" Y="-2.663507338545" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.932990961313" Y="4.715811647278" />
                  <Point X="1.32037492267" Y="-2.654616054753" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.837169445097" Y="4.72732243394" />
                  <Point X="1.417215413835" Y="-2.646438184971" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.741260519665" Y="4.738547317938" />
                  <Point X="1.52255399407" Y="-2.666056312075" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.645351550324" Y="4.749772058317" />
                  <Point X="1.643680406849" Y="-2.737314112596" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.549442580983" Y="4.760996798696" />
                  <Point X="1.767324121429" Y="-2.816805636307" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.453533611642" Y="4.772221539076" />
                  <Point X="1.918655401846" Y="-2.986859107231" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.167239216051" Y="4.160723609484" />
                  <Point X="2.083792773348" Y="-3.202070267335" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.046859868572" Y="4.091909349452" />
                  <Point X="2.015461729714" Y="-2.653640650455" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.097512677558" Y="-2.922017208061" />
                  <Point X="2.248930073214" Y="-3.417281193128" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.052956800651" Y="4.09035257944" />
                  <Point X="2.038087436484" Y="-2.402717158804" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.308669629806" Y="-3.28775163435" />
                  <Point X="2.414067373081" Y="-3.632492118921" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.141224106685" Y="4.126572074258" />
                  <Point X="2.100924915553" Y="-2.283320447874" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.519826582055" Y="-3.653486060639" />
                  <Point X="2.579204672947" Y="-3.847703044713" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.215044180426" Y="4.21004633665" />
                  <Point X="2.170060128232" Y="-2.184522695409" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.730983620064" Y="-4.019220767437" />
                  <Point X="2.733985221272" Y="-4.029038562608" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.266285580812" Y="4.367372111908" />
                  <Point X="2.252448428692" Y="-2.129073839818" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.31268472188" Y="4.540536203734" />
                  <Point X="2.338020310588" Y="-2.084038009903" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.359083862947" Y="4.713700295561" />
                  <Point X="2.424465775014" Y="-2.041859539691" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.438556969192" Y="4.778684321785" />
                  <Point X="2.518817905878" Y="-2.025542610104" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.541183612361" Y="4.767936541135" />
                  <Point X="2.62242116608" Y="-2.039484761135" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.64381025553" Y="4.757188760484" />
                  <Point X="2.727567532148" Y="-2.058474184029" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.746436898698" Y="4.746440979834" />
                  <Point X="2.836146959003" Y="-2.088692642785" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.850019643748" Y="4.732565930844" />
                  <Point X="2.956362605248" Y="-2.156971460207" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.957277376214" Y="4.706670539637" />
                  <Point X="2.8640965589" Y="-1.530253977028" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.881996969622" Y="-1.588803582308" />
                  <Point X="3.076996967444" Y="-2.226619835788" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.064535075458" Y="4.680775257098" />
                  <Point X="2.882082814431" Y="-1.264155524143" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.05828602155" Y="-1.840490245533" />
                  <Point X="3.197631312708" Y="-2.296268155991" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.171792774701" Y="4.654879974559" />
                  <Point X="2.945374857334" Y="-1.146245624516" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.188074555522" Y="-1.940080567841" />
                  <Point X="3.318265639529" Y="-2.365916415868" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.279050473945" Y="4.628984692019" />
                  <Point X="3.018008315882" Y="-1.058890118715" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.317863089495" Y="-2.039670890149" />
                  <Point X="3.43889996635" Y="-2.435564675744" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.386308173189" Y="4.60308940948" />
                  <Point X="2.029904827577" Y="2.497979607227" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.159032750371" Y="2.075621202836" />
                  <Point X="3.095398128068" Y="-0.987091944664" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.447651623467" Y="-2.139261212458" />
                  <Point X="3.559534293171" Y="-2.505212935621" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.495232633384" Y="4.571742397518" />
                  <Point X="2.0869980019" Y="2.636165092379" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.281625391753" Y="1.999567584648" />
                  <Point X="3.183359076472" Y="-0.949870399191" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.577440157439" Y="-2.238851534766" />
                  <Point X="3.680168619991" Y="-2.574861195498" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.606963320249" Y="4.531216631703" />
                  <Point X="2.151946035301" Y="2.748658491146" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.387458070831" Y="1.97833333305" />
                  <Point X="3.276509757078" Y="-0.929623702878" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.707228691411" Y="-2.338441857074" />
                  <Point X="3.800802946812" Y="-2.644509455375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.718693964215" Y="4.490691006206" />
                  <Point X="2.216894068703" Y="2.861151889913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.485890788909" Y="1.981303263264" />
                  <Point X="3.370023367577" Y="-0.910564096758" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.837017225383" Y="-2.438032179382" />
                  <Point X="3.921437273633" Y="-2.714157715251" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.830424608181" Y="4.45016538071" />
                  <Point X="2.281842112005" Y="2.973645256297" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.578308201234" Y="2.003948372051" />
                  <Point X="3.470313108201" Y="-0.913668213601" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.966805759356" Y="-2.53762250169" />
                  <Point X="4.024696039071" Y="-2.726973074682" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.945296621823" Y="4.399364797982" />
                  <Point X="2.346790167182" Y="3.086138583841" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.665188398653" Y="2.044704894712" />
                  <Point X="3.573819972344" Y="-0.927295067333" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.061210761796" Y="4.345155573615" />
                  <Point X="2.411738222359" Y="3.198631911385" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.749624915198" Y="2.093454337361" />
                  <Point X="3.364794390374" Y="0.081325648668" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.505639260167" Y="-0.379357162494" />
                  <Point X="3.677326836488" Y="-0.940921921064" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.17712490177" Y="4.290946349248" />
                  <Point X="2.476686277535" Y="3.311125238928" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.834061431742" Y="2.14220378001" />
                  <Point X="3.024033294462" Y="1.520833815395" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.169210286438" Y="1.045981271047" />
                  <Point X="3.421921657882" Y="0.219399620038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.628651218063" Y="-0.4567823032" />
                  <Point X="3.780833706223" Y="-0.954548793083" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.294859813469" Y="4.230781648914" />
                  <Point X="2.541634332712" Y="3.423618566472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.918497948287" Y="2.190953222658" />
                  <Point X="3.085825157281" Y="1.643650582975" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.288544723085" Y="0.980584760347" />
                  <Point X="3.494797125307" Y="0.305963550472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.740238985173" Y="-0.496840599557" />
                  <Point X="3.884340604762" Y="-0.968175759316" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.415729791496" Y="4.160362608671" />
                  <Point X="2.606582387889" Y="3.536111894015" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.002934464831" Y="2.239702665307" />
                  <Point X="3.15845640334" Y="1.731013325505" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.394630561441" Y="0.958522462062" />
                  <Point X="3.576389975045" Y="0.364014208141" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.848443879561" Y="-0.525834017816" />
                  <Point X="3.9878475033" Y="-0.981802725548" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.536599837216" Y="4.089943347016" />
                  <Point X="2.671530443066" Y="3.648605221559" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.087370981376" Y="2.288452107956" />
                  <Point X="3.238503600905" Y="1.79411958363" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.495840319456" Y="0.952409103927" />
                  <Point X="3.661577873236" Y="0.410305992164" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.95664877395" Y="-0.554827436075" />
                  <Point X="4.091354401839" Y="-0.995429691781" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.658877838953" Y="4.014918868735" />
                  <Point X="2.736478498243" Y="3.761098549103" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.171807497921" Y="2.337201550604" />
                  <Point X="3.318967793323" Y="1.85586191305" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.591407470825" Y="0.964751880513" />
                  <Point X="3.752577998871" Y="0.437586836834" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.064853668338" Y="-0.583820854334" />
                  <Point X="4.194861300378" Y="-1.009056658014" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.78581768662" Y="3.924646179488" />
                  <Point X="2.80142655342" Y="3.873591876646" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.256244014465" Y="2.385950993253" />
                  <Point X="3.399431985741" Y="1.91760424247" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.686904412481" Y="0.977324302726" />
                  <Point X="3.844396882549" Y="0.462189644612" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.173058562727" Y="-0.612814272593" />
                  <Point X="4.298368198917" Y="-1.022683624246" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.34068053101" Y="2.434700435902" />
                  <Point X="3.479896159194" Y="1.97934663392" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.782401354137" Y="0.989896724938" />
                  <Point X="3.936215766228" Y="0.48679245239" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.281263457115" Y="-0.641807690852" />
                  <Point X="4.401875097456" Y="-1.036310590479" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.425117047554" Y="2.48344987855" />
                  <Point X="3.560360326488" Y="2.041089045518" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.877898295793" Y="1.00246914715" />
                  <Point X="4.028034655297" Y="0.511395242536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.389468351504" Y="-0.670801109111" />
                  <Point X="4.505381995994" Y="-1.049937556711" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.509553564099" Y="2.532199321199" />
                  <Point X="3.640824493782" Y="2.102831457116" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.973395237448" Y="1.015041569362" />
                  <Point X="4.119853548161" Y="0.535998020268" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.497673245892" Y="-0.69979452737" />
                  <Point X="4.608888894533" Y="-1.063564522944" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.593990080643" Y="2.580948763848" />
                  <Point X="3.721288661075" Y="2.164573868713" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.068892179104" Y="1.027613991574" />
                  <Point X="4.211672441025" Y="0.560600798001" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.60587814028" Y="-0.728787945629" />
                  <Point X="4.712395793072" Y="-1.077191489177" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.678426597188" Y="2.629698206496" />
                  <Point X="3.801752828369" Y="2.226316280311" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.16438912076" Y="1.040186413786" />
                  <Point X="4.30349133389" Y="0.585203575733" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.714083035979" Y="-0.757781368174" />
                  <Point X="4.763411359152" Y="-0.91912704319" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.762863113733" Y="2.678447649145" />
                  <Point X="3.882216995662" Y="2.288058691909" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.259886062416" Y="1.052758835998" />
                  <Point X="4.395310226754" Y="0.609806353466" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.847299626907" Y="2.727197102818" />
                  <Point X="3.962681162956" Y="2.349801103506" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.355383004071" Y="1.065331258211" />
                  <Point X="4.487129119619" Y="0.634409131198" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.931736136799" Y="2.775946567224" />
                  <Point X="4.04314533025" Y="2.411543515104" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.450879945727" Y="1.077903680423" />
                  <Point X="4.578948012483" Y="0.65901190893" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.103759667861" Y="2.538211794093" />
                  <Point X="4.123609497543" Y="2.473285926702" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.546376887383" Y="1.090476102635" />
                  <Point X="4.670766905347" Y="0.683614686663" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.641873832936" Y="1.103048512099" />
                  <Point X="4.762585798212" Y="0.708217464395" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="0.001626220703" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.558479431152" Y="-3.861577880859" />
                  <Point X="0.471539031982" Y="-3.537112304688" />
                  <Point X="0.464318664551" Y="-3.521544189453" />
                  <Point X="0.428783996582" Y="-3.470345458984" />
                  <Point X="0.300591033936" Y="-3.285643798828" />
                  <Point X="0.274335754395" Y="-3.266399902344" />
                  <Point X="0.219347839355" Y="-3.249333496094" />
                  <Point X="0.020976625443" Y="-3.187766601562" />
                  <Point X="-0.008664756775" Y="-3.187766601562" />
                  <Point X="-0.063652667999" Y="-3.204832763672" />
                  <Point X="-0.262023895264" Y="-3.266399902344" />
                  <Point X="-0.288279144287" Y="-3.285643798828" />
                  <Point X="-0.323813842773" Y="-3.336842529297" />
                  <Point X="-0.45200680542" Y="-3.521544189453" />
                  <Point X="-0.459227142334" Y="-3.537112304688" />
                  <Point X="-0.772867980957" Y="-4.707636230469" />
                  <Point X="-0.84774395752" Y="-4.987077148438" />
                  <Point X="-0.867037536621" Y="-4.98333203125" />
                  <Point X="-1.100246948242" Y="-4.938065429688" />
                  <Point X="-1.162963745117" Y="-4.921928710938" />
                  <Point X="-1.351589599609" Y="-4.873396972656" />
                  <Point X="-1.319615844727" Y="-4.630532226562" />
                  <Point X="-1.309150146484" Y="-4.551038574219" />
                  <Point X="-1.309683227539" Y="-4.5347578125" />
                  <Point X="-1.322819824219" Y="-4.468715820312" />
                  <Point X="-1.370210693359" Y="-4.230466308594" />
                  <Point X="-1.386282714844" Y="-4.20262890625" />
                  <Point X="-1.436908569336" Y="-4.158230957031" />
                  <Point X="-1.619543457031" Y="-3.998063964844" />
                  <Point X="-1.649240722656" Y="-3.985762939453" />
                  <Point X="-1.716432495117" Y="-3.981358886719" />
                  <Point X="-1.958829833984" Y="-3.965471435547" />
                  <Point X="-1.989878540039" Y="-3.973791015625" />
                  <Point X="-2.045866455078" Y="-4.011200927734" />
                  <Point X="-2.247844970703" Y="-4.146159179688" />
                  <Point X="-2.259734130859" Y="-4.157294433594" />
                  <Point X="-2.435819335938" Y="-4.386772949219" />
                  <Point X="-2.457095214844" Y="-4.414500488281" />
                  <Point X="-2.513996337891" Y="-4.379268554688" />
                  <Point X="-2.855831054688" Y="-4.16761328125" />
                  <Point X="-2.942680908203" Y="-4.100741699219" />
                  <Point X="-3.228581054688" Y="-3.880608154297" />
                  <Point X="-2.667602539062" Y="-2.90896484375" />
                  <Point X="-2.506033691406" Y="-2.629119628906" />
                  <Point X="-2.499762695312" Y="-2.597593261719" />
                  <Point X="-2.513979736328" Y="-2.568764404297" />
                  <Point X="-2.531328369141" Y="-2.551415771484" />
                  <Point X="-2.560157226562" Y="-2.537198730469" />
                  <Point X="-2.59168359375" Y="-2.543469726562" />
                  <Point X="-3.601237548828" Y="-3.1263359375" />
                  <Point X="-3.842959472656" Y="-3.265894042969" />
                  <Point X="-3.891633544922" Y="-3.201946289062" />
                  <Point X="-4.161703613281" Y="-2.847129394531" />
                  <Point X="-4.223965332031" Y="-2.742725830078" />
                  <Point X="-4.431020019531" Y="-2.395527099609" />
                  <Point X="-3.446479736328" Y="-1.640062744141" />
                  <Point X="-3.163786865234" Y="-1.423144775391" />
                  <Point X="-3.152535644531" Y="-1.411081542969" />
                  <Point X="-3.144149902344" Y="-1.389557495117" />
                  <Point X="-3.138117431641" Y="-1.366266357422" />
                  <Point X="-3.136651855469" Y="-1.350051269531" />
                  <Point X="-3.148656738281" Y="-1.321068725586" />
                  <Point X="-3.166906738281" Y="-1.307256469727" />
                  <Point X="-3.187641601562" Y="-1.295052734375" />
                  <Point X="-3.219529296875" Y="-1.288571044922" />
                  <Point X="-4.493995605469" Y="-1.456357666016" />
                  <Point X="-4.803283691406" Y="-1.497076293945" />
                  <Point X="-4.821767089844" Y="-1.424713745117" />
                  <Point X="-4.927393066406" Y="-1.011191650391" />
                  <Point X="-4.943865722656" Y="-0.896015563965" />
                  <Point X="-4.998396484375" Y="-0.514742126465" />
                  <Point X="-3.879493652344" Y="-0.214932937622" />
                  <Point X="-3.557463867188" Y="-0.128645431519" />
                  <Point X="-3.535872314453" Y="-0.117244468689" />
                  <Point X="-3.514142822266" Y="-0.102162895203" />
                  <Point X="-3.50232421875" Y="-0.090645256042" />
                  <Point X="-3.492891113281" Y="-0.069438575745" />
                  <Point X="-3.485647949219" Y="-0.046100738525" />
                  <Point X="-3.483400878906" Y="-0.031280042648" />
                  <Point X="-3.487655761719" Y="-0.009990153313" />
                  <Point X="-3.494898925781" Y="0.013347681046" />
                  <Point X="-3.50232421875" Y="0.028085170746" />
                  <Point X="-3.520166259766" Y="0.043783412933" />
                  <Point X="-3.541895751953" Y="0.05886498642" />
                  <Point X="-3.557463867188" Y="0.066085334778" />
                  <Point X="-4.719200195312" Y="0.377371582031" />
                  <Point X="-4.998186523438" Y="0.452125793457" />
                  <Point X="-4.985719726562" Y="0.536375244141" />
                  <Point X="-4.917645019531" Y="0.996418212891" />
                  <Point X="-4.884484863281" Y="1.118789916992" />
                  <Point X="-4.773516113281" Y="1.528298583984" />
                  <Point X="-3.984105224609" Y="1.424370727539" />
                  <Point X="-3.753266357422" Y="1.393980102539" />
                  <Point X="-3.731704833984" Y="1.395866577148" />
                  <Point X="-3.718373291016" Y="1.40007019043" />
                  <Point X="-3.670278808594" Y="1.415234130859" />
                  <Point X="-3.651534423828" Y="1.426056152344" />
                  <Point X="-3.639119873047" Y="1.443785888672" />
                  <Point X="-3.633770507812" Y="1.456700439453" />
                  <Point X="-3.614472412109" Y="1.503290161133" />
                  <Point X="-3.610714111328" Y="1.524605224609" />
                  <Point X="-3.616315917969" Y="1.54551159668" />
                  <Point X="-3.622770507812" Y="1.557910766602" />
                  <Point X="-3.646055664062" Y="1.602641235352" />
                  <Point X="-3.659968017578" Y="1.619221679688" />
                  <Point X="-4.326343261719" Y="2.130549072266" />
                  <Point X="-4.476105957031" Y="2.245466308594" />
                  <Point X="-4.424528808594" Y="2.333830322266" />
                  <Point X="-4.160014648438" Y="2.787006591797" />
                  <Point X="-4.072175048828" Y="2.899911132812" />
                  <Point X="-3.774671142578" Y="3.282310791016" />
                  <Point X="-3.300931884766" Y="3.008797119141" />
                  <Point X="-3.159156982422" Y="2.926943359375" />
                  <Point X="-3.138514648438" Y="2.920434814453" />
                  <Point X="-3.119947265625" Y="2.918810302734" />
                  <Point X="-3.052965332031" Y="2.912950195312" />
                  <Point X="-3.031506835938" Y="2.915775390625" />
                  <Point X="-3.013252441406" Y="2.927404541016" />
                  <Point X="-3.000073242188" Y="2.940583740234" />
                  <Point X="-2.952528808594" Y="2.988127929688" />
                  <Point X="-2.940899414062" Y="3.006382324219" />
                  <Point X="-2.93807421875" Y="3.027841064453" />
                  <Point X="-2.939698730469" Y="3.046408447266" />
                  <Point X="-2.945558837891" Y="3.113390625" />
                  <Point X="-2.952067382812" Y="3.134032958984" />
                  <Point X="-3.247358398438" Y="3.645491455078" />
                  <Point X="-3.307278564453" Y="3.749276611328" />
                  <Point X="-3.213576171875" Y="3.821117431641" />
                  <Point X="-2.752876220703" Y="4.174331542969" />
                  <Point X="-2.614532470703" Y="4.251192382813" />
                  <Point X="-2.141548583984" Y="4.513972167969" />
                  <Point X="-2.011228393555" Y="4.344135253906" />
                  <Point X="-1.967826660156" Y="4.287573242188" />
                  <Point X="-1.951246826172" Y="4.273660644531" />
                  <Point X="-1.930581542969" Y="4.262902832031" />
                  <Point X="-1.856030761719" Y="4.22409375" />
                  <Point X="-1.835124267578" Y="4.2184921875" />
                  <Point X="-1.813808959961" Y="4.222250488281" />
                  <Point X="-1.792284667969" Y="4.231166503906" />
                  <Point X="-1.714635009766" Y="4.263330078125" />
                  <Point X="-1.696905273438" Y="4.275744628906" />
                  <Point X="-1.686083496094" Y="4.294488769531" />
                  <Point X="-1.679077514648" Y="4.316708007812" />
                  <Point X="-1.653804077148" Y="4.396865234375" />
                  <Point X="-1.651917602539" Y="4.418426757812" />
                  <Point X="-1.685488769531" Y="4.67342578125" />
                  <Point X="-1.689137573242" Y="4.701141113281" />
                  <Point X="-1.564498657227" Y="4.736085449219" />
                  <Point X="-0.968094177246" Y="4.903296386719" />
                  <Point X="-0.800381896973" Y="4.922924804688" />
                  <Point X="-0.224199615479" Y="4.990358398438" />
                  <Point X="-0.084081283569" Y="4.467429199219" />
                  <Point X="-0.042140380859" Y="4.310903320312" />
                  <Point X="-0.02428212738" Y="4.284176757813" />
                  <Point X="0.006155934334" Y="4.273844238281" />
                  <Point X="0.036594020844" Y="4.284176757813" />
                  <Point X="0.054452251434" Y="4.310903320312" />
                  <Point X="0.205755950928" Y="4.875576660156" />
                  <Point X="0.236648406982" Y="4.990868652344" />
                  <Point X="0.339457489014" Y="4.980102050781" />
                  <Point X="0.860205749512" Y="4.925565429688" />
                  <Point X="0.998960327148" Y="4.892065917969" />
                  <Point X="1.508455932617" Y="4.769058105469" />
                  <Point X="1.598582885742" Y="4.736368164062" />
                  <Point X="1.931034301758" Y="4.615785644531" />
                  <Point X="2.018366210938" Y="4.574943359375" />
                  <Point X="2.33869921875" Y="4.425134277344" />
                  <Point X="2.423071289062" Y="4.375979003906" />
                  <Point X="2.732523681641" Y="4.19569140625" />
                  <Point X="2.812093017578" Y="4.139105957031" />
                  <Point X="3.068740234375" Y="3.956592773438" />
                  <Point X="2.416857177734" Y="2.827498046875" />
                  <Point X="2.229853515625" Y="2.503597900391" />
                  <Point X="2.220192382812" Y="2.47408984375" />
                  <Point X="2.203382324219" Y="2.411228515625" />
                  <Point X="2.203861572266" Y="2.377258300781" />
                  <Point X="2.210416015625" Y="2.322901123047" />
                  <Point X="2.218682128906" Y="2.3008125" />
                  <Point X="2.228005615234" Y="2.287072265625" />
                  <Point X="2.261639892578" Y="2.23750390625" />
                  <Point X="2.288680175781" Y="2.214880126953" />
                  <Point X="2.338248535156" Y="2.18124609375" />
                  <Point X="2.360336914062" Y="2.172980224609" />
                  <Point X="2.375404541016" Y="2.171163330078" />
                  <Point X="2.42976171875" Y="2.164608642578" />
                  <Point X="2.466089599609" Y="2.170605957031" />
                  <Point X="2.528950927734" Y="2.187416015625" />
                  <Point X="2.541033935547" Y="2.192417480469" />
                  <Point X="3.709517089844" Y="2.867041503906" />
                  <Point X="3.994248046875" Y="3.031430908203" />
                  <Point X="4.019035400391" Y="2.996982177734" />
                  <Point X="4.202591796875" Y="2.741880126953" />
                  <Point X="4.246950683594" Y="2.668576660156" />
                  <Point X="4.387512695312" Y="2.436296142578" />
                  <Point X="3.53482421875" Y="1.782004760742" />
                  <Point X="3.288616210938" Y="1.593082763672" />
                  <Point X="3.279371337891" Y="1.583833374023" />
                  <Point X="3.266830566406" Y="1.56747277832" />
                  <Point X="3.221589111328" Y="1.508451904297" />
                  <Point X="3.208448242188" Y="1.474796020508" />
                  <Point X="3.191595703125" Y="1.414535644531" />
                  <Point X="3.190779541016" Y="1.390965332031" />
                  <Point X="3.194614257812" Y="1.372380004883" />
                  <Point X="3.208448486328" Y="1.305332519531" />
                  <Point X="3.226076660156" Y="1.27210144043" />
                  <Point X="3.263704101562" Y="1.214909667969" />
                  <Point X="3.280947998047" Y="1.198819946289" />
                  <Point X="3.296062744141" Y="1.190311645508" />
                  <Point X="3.350590087891" Y="1.159617675781" />
                  <Point X="3.368565429688" Y="1.153619628906" />
                  <Point X="3.389001464844" Y="1.150918701172" />
                  <Point X="3.462726074219" Y="1.141175048828" />
                  <Point X="3.475803710938" Y="1.14117175293" />
                  <Point X="4.585784667969" Y="1.287303588867" />
                  <Point X="4.848975097656" Y="1.32195324707" />
                  <Point X="4.860352050781" Y="1.275221191406" />
                  <Point X="4.939188476562" Y="0.951386413574" />
                  <Point X="4.953166992188" Y="0.861601867676" />
                  <Point X="4.997858398438" Y="0.574556152344" />
                  <Point X="4.023327392578" Y="0.313431304932" />
                  <Point X="3.741167724609" Y="0.237826919556" />
                  <Point X="3.729086914062" Y="0.232819412231" />
                  <Point X="3.709009033203" Y="0.221213989258" />
                  <Point X="3.636576904297" Y="0.179346878052" />
                  <Point X="3.610218261719" Y="0.151576217651" />
                  <Point X="3.566759033203" Y="0.096198890686" />
                  <Point X="3.556985351562" Y="0.074735275269" />
                  <Point X="3.552969726562" Y="0.05376726532" />
                  <Point X="3.538483154297" Y="-0.021875303268" />
                  <Point X="3.538483154297" Y="-0.04068478775" />
                  <Point X="3.542498779297" Y="-0.06165265274" />
                  <Point X="3.556985351562" Y="-0.13729536438" />
                  <Point X="3.566759033203" Y="-0.158758987427" />
                  <Point X="3.578805664062" Y="-0.17410949707" />
                  <Point X="3.622264892578" Y="-0.229486679077" />
                  <Point X="3.636576904297" Y="-0.241906967163" />
                  <Point X="3.656654785156" Y="-0.253512374878" />
                  <Point X="3.729086914062" Y="-0.295379486084" />
                  <Point X="3.741167724609" Y="-0.300387023926" />
                  <Point X="4.7590703125" Y="-0.573133239746" />
                  <Point X="4.998067871094" Y="-0.637172424316" />
                  <Point X="4.992450683594" Y="-0.674432312012" />
                  <Point X="4.948431640625" Y="-0.966399108887" />
                  <Point X="4.930522460938" Y="-1.044879882813" />
                  <Point X="4.874545410156" Y="-1.290178344727" />
                  <Point X="3.739208007812" Y="-1.140708129883" />
                  <Point X="3.411982177734" Y="-1.097628295898" />
                  <Point X="3.394836181641" Y="-1.098341186523" />
                  <Point X="3.355430419922" Y="-1.10690625" />
                  <Point X="3.213271972656" Y="-1.137804931641" />
                  <Point X="3.1854453125" Y="-1.154697265625" />
                  <Point X="3.161626953125" Y="-1.183343383789" />
                  <Point X="3.075701171875" Y="-1.286685424805" />
                  <Point X="3.064357910156" Y="-1.314070556641" />
                  <Point X="3.060944091797" Y="-1.351168579102" />
                  <Point X="3.04862890625" Y="-1.485000854492" />
                  <Point X="3.056360595703" Y="-1.516621826172" />
                  <Point X="3.078168457031" Y="-1.550542480469" />
                  <Point X="3.156840820312" Y="-1.672912353516" />
                  <Point X="3.168460693359" Y="-1.685540649414" />
                  <Point X="4.113080566406" Y="-2.410373291016" />
                  <Point X="4.339074707031" Y="-2.583784179688" />
                  <Point X="4.328217285156" Y="-2.601352783203" />
                  <Point X="4.204133300781" Y="-2.802139404297" />
                  <Point X="4.16709375" Y="-2.854767333984" />
                  <Point X="4.056688476562" Y="-3.011638427734" />
                  <Point X="3.044893310547" Y="-2.427478027344" />
                  <Point X="2.753454589844" Y="-2.259215576172" />
                  <Point X="2.737340820313" Y="-2.253312744141" />
                  <Point X="2.690441650391" Y="-2.244842773438" />
                  <Point X="2.521250488281" Y="-2.214287109375" />
                  <Point X="2.489077636719" Y="-2.219244873047" />
                  <Point X="2.450115722656" Y="-2.239750244141" />
                  <Point X="2.309559570312" Y="-2.313723876953" />
                  <Point X="2.288599853516" Y="-2.334683837891" />
                  <Point X="2.268094482422" Y="-2.373645507812" />
                  <Point X="2.194120849609" Y="-2.514201904297" />
                  <Point X="2.189163085938" Y="-2.546374511719" />
                  <Point X="2.197633056641" Y="-2.593273681641" />
                  <Point X="2.228188720703" Y="-2.76246484375" />
                  <Point X="2.234091552734" Y="-2.778578613281" />
                  <Point X="2.841104980469" Y="-3.829956298828" />
                  <Point X="2.986673583984" Y="-4.082088623047" />
                  <Point X="2.98253515625" Y="-4.085044677734" />
                  <Point X="2.835296630859" Y="-4.190213378906" />
                  <Point X="2.793889160156" Y="-4.217016113281" />
                  <Point X="2.679775878906" Y="-4.290879394531" />
                  <Point X="1.905572265625" Y="-3.281917724609" />
                  <Point X="1.683177612305" Y="-2.992087402344" />
                  <Point X="1.670549316406" Y="-2.980467529297" />
                  <Point X="1.624293945312" Y="-2.950729492188" />
                  <Point X="1.45742590332" Y="-2.843448730469" />
                  <Point X="1.425804931641" Y="-2.835717041016" />
                  <Point X="1.375216674805" Y="-2.840372314453" />
                  <Point X="1.192718017578" Y="-2.857166015625" />
                  <Point X="1.165332885742" Y="-2.868509277344" />
                  <Point X="1.126270141602" Y="-2.900988769531" />
                  <Point X="0.985349060059" Y="-3.018159667969" />
                  <Point X="0.968456726074" Y="-3.045986328125" />
                  <Point X="0.95677722168" Y="-3.099721679688" />
                  <Point X="0.91464251709" Y="-3.29357421875" />
                  <Point X="0.91392956543" Y="-3.310720214844" />
                  <Point X="1.085952392578" Y="-4.617363769531" />
                  <Point X="1.127642211914" Y="-4.934028808594" />
                  <Point X="0.994346435547" Y="-4.963246582031" />
                  <Point X="0.956088623047" Y="-4.970196777344" />
                  <Point X="0.860200378418" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#138" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.041940917734" Y="4.511650117458" Z="0.55" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.55" />
                  <Point X="-0.812349928673" Y="5.003865791466" Z="0.55" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.55" />
                  <Point X="-1.584152528955" Y="4.815509069241" Z="0.55" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.55" />
                  <Point X="-1.741217125809" Y="4.69817964729" Z="0.55" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.55" />
                  <Point X="-1.732919404343" Y="4.363023419925" Z="0.55" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.55" />
                  <Point X="-1.817574798158" Y="4.30864029066" Z="0.55" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.55" />
                  <Point X="-1.913649924864" Y="4.338533584323" Z="0.55" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.55" />
                  <Point X="-1.977716765489" Y="4.405853366124" Z="0.55" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.55" />
                  <Point X="-2.644971511584" Y="4.326179708828" Z="0.55" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.55" />
                  <Point X="-3.250154269711" Y="3.892077472508" Z="0.55" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.55" />
                  <Point X="-3.296815524515" Y="3.651771614417" Z="0.55" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.55" />
                  <Point X="-2.995664451945" Y="3.073330797019" Z="0.55" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.55" />
                  <Point X="-3.04158439484" Y="3.007219173046" Z="0.55" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.55" />
                  <Point X="-3.121745668587" Y="2.999900247228" Z="0.55" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.55" />
                  <Point X="-3.28208773388" Y="3.083378424231" Z="0.55" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.55" />
                  <Point X="-4.117794784715" Y="2.961893681479" Z="0.55" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.55" />
                  <Point X="-4.473866727525" Y="2.390315334124" Z="0.55" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.55" />
                  <Point X="-4.362937058356" Y="2.122161408039" Z="0.55" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.55" />
                  <Point X="-3.673277036251" Y="1.566103559192" Z="0.55" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.55" />
                  <Point X="-3.686120532424" Y="1.507114645758" Z="0.55" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.55" />
                  <Point X="-3.739564416453" Y="1.479035985021" Z="0.55" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.55" />
                  <Point X="-3.983734922552" Y="1.505223055388" Z="0.55" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.55" />
                  <Point X="-4.938900564155" Y="1.163147408859" Z="0.55" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.55" />
                  <Point X="-5.041337259618" Y="0.574974237073" Z="0.55" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.55" />
                  <Point X="-4.738297182979" Y="0.360355379726" Z="0.55" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.55" />
                  <Point X="-3.554831217034" Y="0.033987505218" Z="0.55" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.55" />
                  <Point X="-3.541564562082" Y="0.006469164629" Z="0.55" />
                  <Point X="-3.539556741714" Y="0" Z="0.55" />
                  <Point X="-3.546800039053" Y="-0.023337786432" Z="0.55" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.55" />
                  <Point X="-3.570537378089" Y="-0.044888477916" Z="0.55" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.55" />
                  <Point X="-3.898590605794" Y="-0.13535667703" Z="0.55" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.55" />
                  <Point X="-4.999517852653" Y="-0.871814540093" Z="0.55" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.55" />
                  <Point X="-4.876367707698" Y="-1.405807924187" Z="0.55" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.55" />
                  <Point X="-4.49362510018" Y="-1.47464993571" Z="0.55" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.55" />
                  <Point X="-3.19842396453" Y="-1.319066981523" Z="0.55" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.55" />
                  <Point X="-3.19870865342" Y="-1.345740937502" Z="0.55" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.55" />
                  <Point X="-3.483073754893" Y="-1.569115093049" Z="0.55" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.55" />
                  <Point X="-4.273064701536" Y="-2.737055440736" Z="0.55" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.55" />
                  <Point X="-3.937575355076" Y="-3.200949429657" Z="0.55" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.55" />
                  <Point X="-3.582393525756" Y="-3.138357251903" Z="0.55" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.55" />
                  <Point X="-2.559256215936" Y="-2.569074253196" Z="0.55" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.55" />
                  <Point X="-2.717059761265" Y="-2.852684856736" Z="0.55" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.55" />
                  <Point X="-2.9793407862" Y="-4.109078853654" Z="0.55" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.55" />
                  <Point X="-2.546473956598" Y="-4.39049937596" Z="0.55" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.55" />
                  <Point X="-2.402307465941" Y="-4.385930785159" Z="0.55" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.55" />
                  <Point X="-2.024243820837" Y="-4.021494290865" Z="0.55" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.55" />
                  <Point X="-1.72585866486" Y="-3.999971945536" Z="0.55" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.55" />
                  <Point X="-1.47603185385" Y="-4.164542744976" Z="0.55" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.55" />
                  <Point X="-1.378015135464" Y="-4.447190143801" Z="0.55" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.55" />
                  <Point X="-1.375344095838" Y="-4.592726021557" Z="0.55" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.55" />
                  <Point X="-1.181578611776" Y="-4.939071446449" Z="0.55" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.55" />
                  <Point X="-0.882720927272" Y="-5.001135964097" Z="0.55" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.55" />
                  <Point X="-0.730727743045" Y="-4.689297171734" Z="0.55" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.55" />
                  <Point X="-0.288893792323" Y="-3.334071423541" Z="0.55" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.55" />
                  <Point X="-0.054987898336" Y="-3.221305582839" Z="0.55" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.55" />
                  <Point X="0.198371181025" Y="-3.265806462449" Z="0.55" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.55" />
                  <Point X="0.381552067109" Y="-3.467574332227" Z="0.55" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.55" />
                  <Point X="0.504027117356" Y="-3.843238858082" Z="0.55" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.55" />
                  <Point X="0.958869536907" Y="-4.988111799568" Z="0.55" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.55" />
                  <Point X="1.138194338975" Y="-4.950273085397" Z="0.55" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.55" />
                  <Point X="1.129368728641" Y="-4.579557529899" Z="0.55" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.55" />
                  <Point X="0.999480420662" Y="-3.079061313899" Z="0.55" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.55" />
                  <Point X="1.152080902669" Y="-2.9081549174" Z="0.55" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.55" />
                  <Point X="1.373642381586" Y="-2.858881749858" Z="0.55" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.55" />
                  <Point X="1.591098558143" Y="-2.961507360653" Z="0.55" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.55" />
                  <Point X="1.859748540357" Y="-3.281075552522" Z="0.55" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.55" />
                  <Point X="2.814902421543" Y="-4.227710037699" Z="0.55" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.55" />
                  <Point X="3.005441029374" Y="-4.094451444213" Z="0.55" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.55" />
                  <Point X="2.878250302367" Y="-3.773676241935" Z="0.55" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.55" />
                  <Point X="2.240681090934" Y="-2.553107464475" Z="0.55" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.55" />
                  <Point X="2.306184887768" Y="-2.365651961061" Z="0.55" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.55" />
                  <Point X="2.467246417311" Y="-2.252716422199" Z="0.55" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.55" />
                  <Point X="2.675399339038" Y="-2.262766918557" Z="0.55" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.55" />
                  <Point X="3.013737355172" Y="-2.439499197907" Z="0.55" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.55" />
                  <Point X="4.201826285361" Y="-2.852264662557" Z="0.55" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.55" />
                  <Point X="4.364594368324" Y="-2.596357815159" Z="0.55" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.55" />
                  <Point X="4.137362731491" Y="-2.33942547406" Z="0.55" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.55" />
                  <Point X="3.114070296159" Y="-1.492223081487" Z="0.55" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.55" />
                  <Point X="3.104577717351" Y="-1.324470156428" Z="0.55" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.55" />
                  <Point X="3.193917176404" Y="-1.184030266637" Z="0.55" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.55" />
                  <Point X="3.359893966189" Y="-1.1244854585" Z="0.55" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.55" />
                  <Point X="3.726525680672" Y="-1.159000517776" Z="0.55" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.55" />
                  <Point X="4.973113729206" Y="-1.024723940557" Z="0.55" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.55" />
                  <Point X="5.03573596262" Y="-0.650606336148" Z="0.55" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.55" />
                  <Point X="4.765855503511" Y="-0.493556858619" Z="0.55" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.55" />
                  <Point X="3.675520659643" Y="-0.178943619435" Z="0.55" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.55" />
                  <Point X="3.61198349068" Y="-0.111960943265" Z="0.55" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.55" />
                  <Point X="3.585450315706" Y="-0.020967908825" Z="0.55" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.55" />
                  <Point X="3.595921134719" Y="0.075642622408" Z="0.55" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.55" />
                  <Point X="3.64339594772" Y="0.15198779531" Z="0.55" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.55" />
                  <Point X="3.727874754709" Y="0.209205224807" Z="0.55" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.55" />
                  <Point X="4.030112313795" Y="0.29641507415" Z="0.55" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.55" />
                  <Point X="4.996416004442" Y="0.900573976922" Z="0.55" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.55" />
                  <Point X="4.902777105896" Y="1.318328106056" Z="0.55" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.55" />
                  <Point X="4.57310212936" Y="1.368155861545" Z="0.55" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.55" />
                  <Point X="3.389396848246" Y="1.23176768264" Z="0.55" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.55" />
                  <Point X="3.314433348405" Y="1.265162709787" Z="0.55" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.55" />
                  <Point X="3.261691188533" Y="1.330862969569" Z="0.55" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.55" />
                  <Point X="3.237426824367" Y="1.413763852676" Z="0.55" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.55" />
                  <Point X="3.250444525452" Y="1.492609679273" Z="0.55" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.55" />
                  <Point X="3.300357379864" Y="1.568334666135" Z="0.55" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.55" />
                  <Point X="3.559106135539" Y="1.773617119316" Z="0.55" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.55" />
                  <Point X="4.283572140891" Y="2.725742972922" Z="0.55" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.55" />
                  <Point X="4.053464772552" Y="3.057465181847" Z="0.55" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.55" />
                  <Point X="3.678361113791" Y="2.941622815763" Z="0.55" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.55" />
                  <Point X="2.44701662016" Y="2.2501888373" Z="0.55" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.55" />
                  <Point X="2.375234444539" Y="2.252083647671" Z="0.55" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.55" />
                  <Point X="2.310598307042" Y="2.287534762905" Z="0.55" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.55" />
                  <Point X="2.26322383733" Y="2.34642655334" Z="0.55" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.55" />
                  <Point X="2.247346055088" Y="2.414523999293" Z="0.55" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.55" />
                  <Point X="2.262339105358" Y="2.492452955722" Z="0.55" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.55" />
                  <Point X="2.454002537097" Y="2.833777968164" Z="0.55" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.55" />
                  <Point X="2.834914070046" Y="4.211133275905" Z="0.55" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.55" />
                  <Point X="2.442085335172" Y="4.450461647292" Z="0.55" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.55" />
                  <Point X="2.033391560302" Y="4.651515612293" Z="0.55" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.55" />
                  <Point X="1.609475445857" Y="4.814652303894" Z="0.55" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.55" />
                  <Point X="1.004538669664" Y="4.971949619632" Z="0.55" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.55" />
                  <Point X="0.338509439574" Y="5.061109919958" Z="0.55" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.55" />
                  <Point X="0.151303643819" Y="4.919797388493" Z="0.55" />
                  <Point X="0" Y="4.355124473572" Z="0.55" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>