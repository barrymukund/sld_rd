<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#161" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1744" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="0" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000475830078" Y="0.004716064453" />
                  <Width Value="9.783896972656" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.73698248291" Y="-4.160708496094" />
                  <Point X="0.563302062988" Y="-3.512524414063" />
                  <Point X="0.557721191406" Y="-3.497141845703" />
                  <Point X="0.542363037109" Y="-3.467377197266" />
                  <Point X="0.471375671387" Y="-3.36509765625" />
                  <Point X="0.378635528564" Y="-3.231476806641" />
                  <Point X="0.356751586914" Y="-3.209021484375" />
                  <Point X="0.330496398926" Y="-3.189777587891" />
                  <Point X="0.302495330811" Y="-3.175669433594" />
                  <Point X="0.192646133423" Y="-3.141576171875" />
                  <Point X="0.049136188507" Y="-3.097036132812" />
                  <Point X="0.020976638794" Y="-3.092766601562" />
                  <Point X="-0.00866468811" Y="-3.092766601562" />
                  <Point X="-0.036824085236" Y="-3.097035888672" />
                  <Point X="-0.146673294067" Y="-3.13112890625" />
                  <Point X="-0.290183227539" Y="-3.175669189453" />
                  <Point X="-0.31818460083" Y="-3.189777587891" />
                  <Point X="-0.344439788818" Y="-3.209021484375" />
                  <Point X="-0.366323577881" Y="-3.231476806641" />
                  <Point X="-0.437311096191" Y="-3.333756347656" />
                  <Point X="-0.530051208496" Y="-3.467377197266" />
                  <Point X="-0.538189025879" Y="-3.481573730469" />
                  <Point X="-0.550990112305" Y="-3.512524414063" />
                  <Point X="-0.777890808105" Y="-4.359330078125" />
                  <Point X="-0.916584472656" Y="-4.87694140625" />
                  <Point X="-1.079342407227" Y="-4.845350097656" />
                  <Point X="-1.203320922852" Y="-4.813451171875" />
                  <Point X="-1.24641796875" Y="-4.802362792969" />
                  <Point X="-1.235870117188" Y="-4.722243164062" />
                  <Point X="-1.214963012695" Y="-4.563438476562" />
                  <Point X="-1.214201171875" Y="-4.5479296875" />
                  <Point X="-1.216508666992" Y="-4.516224121094" />
                  <Point X="-1.242751586914" Y="-4.384291992188" />
                  <Point X="-1.277036132812" Y="-4.211932617188" />
                  <Point X="-1.287938354492" Y="-4.182966308594" />
                  <Point X="-1.304010375977" Y="-4.15512890625" />
                  <Point X="-1.323644775391" Y="-4.131204101562" />
                  <Point X="-1.424779907227" Y="-4.042510742188" />
                  <Point X="-1.556905517578" Y="-3.926639160156" />
                  <Point X="-1.583188476562" Y="-3.910295410156" />
                  <Point X="-1.612885742188" Y="-3.897994384766" />
                  <Point X="-1.64302734375" Y="-3.890966308594" />
                  <Point X="-1.777256469727" Y="-3.882168457031" />
                  <Point X="-1.952616577148" Y="-3.870674804688" />
                  <Point X="-1.98341796875" Y="-3.873708496094" />
                  <Point X="-2.014466674805" Y="-3.882028076172" />
                  <Point X="-2.042657714844" Y="-3.894801513672" />
                  <Point X="-2.154504394531" Y="-3.96953515625" />
                  <Point X="-2.300624023438" Y="-4.067169677734" />
                  <Point X="-2.312785644531" Y="-4.076821533203" />
                  <Point X="-2.335102783203" Y="-4.099461914062" />
                  <Point X="-2.462489990234" Y="-4.265476074219" />
                  <Point X="-2.480148925781" Y="-4.288489746094" />
                  <Point X="-2.559121826172" Y="-4.239591796875" />
                  <Point X="-2.801706787109" Y="-4.089389404297" />
                  <Point X="-2.973384277344" Y="-3.957203613281" />
                  <Point X="-3.104721923828" Y="-3.856077880859" />
                  <Point X="-2.746526611328" Y="-3.235665527344" />
                  <Point X="-2.423761230469" Y="-2.676619628906" />
                  <Point X="-2.412859130859" Y="-2.647653320312" />
                  <Point X="-2.406588134766" Y="-2.616126953125" />
                  <Point X="-2.405575439453" Y="-2.585192871094" />
                  <Point X="-2.414560058594" Y="-2.555575195312" />
                  <Point X="-2.428777099609" Y="-2.526746337891" />
                  <Point X="-2.4468046875" Y="-2.501589355469" />
                  <Point X="-2.464153320312" Y="-2.484240722656" />
                  <Point X="-2.489310302734" Y="-2.466213134766" />
                  <Point X="-2.518139160156" Y="-2.45199609375" />
                  <Point X="-2.547756835938" Y="-2.443011474609" />
                  <Point X="-2.578690917969" Y="-2.444024169922" />
                  <Point X="-2.610217285156" Y="-2.450295166016" />
                  <Point X="-2.63918359375" Y="-2.461197265625" />
                  <Point X="-3.369536865234" Y="-2.882866943359" />
                  <Point X="-3.818024658203" Y="-3.141801269531" />
                  <Point X="-3.891203125" Y="-3.045659667969" />
                  <Point X="-4.082858642578" Y="-2.79386328125" />
                  <Point X="-4.205935546875" Y="-2.587481933594" />
                  <Point X="-4.306142578125" Y="-2.419450439453" />
                  <Point X="-3.670689208984" Y="-1.931849609375" />
                  <Point X="-3.105954589844" Y="-1.498513549805" />
                  <Point X="-3.084576904297" Y="-1.475592895508" />
                  <Point X="-3.066612060547" Y="-1.448461181641" />
                  <Point X="-3.053856445312" Y="-1.419832275391" />
                  <Point X="-3.046151855469" Y="-1.390084960938" />
                  <Point X="-3.04334765625" Y="-1.359655761719" />
                  <Point X="-3.045556640625" Y="-1.327985473633" />
                  <Point X="-3.052557861328" Y="-1.298240722656" />
                  <Point X="-3.068639892578" Y="-1.272257446289" />
                  <Point X="-3.089472167969" Y="-1.248301147461" />
                  <Point X="-3.112972167969" Y="-1.228767089844" />
                  <Point X="-3.139454833984" Y="-1.213180664062" />
                  <Point X="-3.168718261719" Y="-1.201956420898" />
                  <Point X="-3.200606201172" Y="-1.195474731445" />
                  <Point X="-3.231929199219" Y="-1.194383789062" />
                  <Point X="-4.153931152344" Y="-1.315767700195" />
                  <Point X="-4.732102050781" Y="-1.391885375977" />
                  <Point X="-4.759118652344" Y="-1.286116577148" />
                  <Point X="-4.834077636719" Y="-0.992654785156" />
                  <Point X="-4.866640136719" Y="-0.764980407715" />
                  <Point X="-4.892424316406" Y="-0.584698181152" />
                  <Point X="-4.176193847656" Y="-0.392784729004" />
                  <Point X="-3.532875976562" Y="-0.220408340454" />
                  <Point X="-3.510783447266" Y="-0.211385620117" />
                  <Point X="-3.483182373047" Y="-0.195813598633" />
                  <Point X="-3.469870849609" Y="-0.186734710693" />
                  <Point X="-3.442332275391" Y="-0.16430645752" />
                  <Point X="-3.426106933594" Y="-0.147354446411" />
                  <Point X="-3.414531738281" Y="-0.126942489624" />
                  <Point X="-3.403095458984" Y="-0.099281578064" />
                  <Point X="-3.398431152344" Y="-0.084818222046" />
                  <Point X="-3.390944091797" Y="-0.053114067078" />
                  <Point X="-3.388402099609" Y="-0.031757570267" />
                  <Point X="-3.390729248047" Y="-0.010376565933" />
                  <Point X="-3.396987304688" Y="0.01736738205" />
                  <Point X="-3.401469726562" Y="0.031786560059" />
                  <Point X="-3.414135009766" Y="0.063407688141" />
                  <Point X="-3.425483398438" Y="0.083946334839" />
                  <Point X="-3.44151953125" Y="0.101076911926" />
                  <Point X="-3.46537109375" Y="0.120945968628" />
                  <Point X="-3.478544189453" Y="0.130150543213" />
                  <Point X="-3.509832275391" Y="0.148281600952" />
                  <Point X="-3.532875976562" Y="0.157848342896" />
                  <Point X="-4.37332421875" Y="0.383045654297" />
                  <Point X="-4.89181640625" Y="0.521975219727" />
                  <Point X="-4.872797363281" Y="0.650504760742" />
                  <Point X="-4.824488769531" Y="0.976970214844" />
                  <Point X="-4.758939453125" Y="1.218867431641" />
                  <Point X="-4.703551269531" Y="1.423267822266" />
                  <Point X="-4.2268125" Y="1.360504150391" />
                  <Point X="-3.765666259766" Y="1.29979284668" />
                  <Point X="-3.744985839844" Y="1.299341674805" />
                  <Point X="-3.723424072266" Y="1.301228271484" />
                  <Point X="-3.703137207031" Y="1.305263671875" />
                  <Point X="-3.676504638672" Y="1.313661010742" />
                  <Point X="-3.641711181641" Y="1.324631225586" />
                  <Point X="-3.622778320312" Y="1.332962036133" />
                  <Point X="-3.604034179688" Y="1.343784057617" />
                  <Point X="-3.587353271484" Y="1.356015136719" />
                  <Point X="-3.57371484375" Y="1.37156652832" />
                  <Point X="-3.561300292969" Y="1.389296508789" />
                  <Point X="-3.5513515625" Y="1.407430908203" />
                  <Point X="-3.540665039062" Y="1.433229858398" />
                  <Point X="-3.526704101563" Y="1.466934936523" />
                  <Point X="-3.520915527344" Y="1.486794067383" />
                  <Point X="-3.517157226562" Y="1.508109008789" />
                  <Point X="-3.5158046875" Y="1.528749145508" />
                  <Point X="-3.518951171875" Y="1.549192871094" />
                  <Point X="-3.524552978516" Y="1.570099121094" />
                  <Point X="-3.532049804688" Y="1.589377563477" />
                  <Point X="-3.544944091797" Y="1.614147338867" />
                  <Point X="-3.561789550781" Y="1.646507080078" />
                  <Point X="-3.57328125" Y="1.663705932617" />
                  <Point X="-3.587193847656" Y="1.680286376953" />
                  <Point X="-3.602135986328" Y="1.694590332031" />
                  <Point X="-4.084219482422" Y="2.064505615234" />
                  <Point X="-4.351859863281" Y="2.269874023438" />
                  <Point X="-4.268864746094" Y="2.412065185547" />
                  <Point X="-4.081153808594" Y="2.733659423828" />
                  <Point X="-3.907520996094" Y="2.956839599609" />
                  <Point X="-3.750504882813" Y="3.158661865234" />
                  <Point X="-3.489879882812" Y="3.008189697266" />
                  <Point X="-3.206656982422" Y="2.844670898438" />
                  <Point X="-3.187724609375" Y="2.836340332031" />
                  <Point X="-3.167082519531" Y="2.829831787109" />
                  <Point X="-3.146794677734" Y="2.825796386719" />
                  <Point X="-3.109702880859" Y="2.822551269531" />
                  <Point X="-3.061245361328" Y="2.818311767578" />
                  <Point X="-3.040565185547" Y="2.818762939453" />
                  <Point X="-3.019106201172" Y="2.821588134766" />
                  <Point X="-2.999014160156" Y="2.826504882812" />
                  <Point X="-2.980462402344" Y="2.835653564453" />
                  <Point X="-2.962208251953" Y="2.847282958984" />
                  <Point X="-2.946077636719" Y="2.860229248047" />
                  <Point X="-2.919749511719" Y="2.886557128906" />
                  <Point X="-2.885354003906" Y="2.920952636719" />
                  <Point X="-2.872406982422" Y="2.937083984375" />
                  <Point X="-2.860777587891" Y="2.955338134766" />
                  <Point X="-2.85162890625" Y="2.973889892578" />
                  <Point X="-2.846712158203" Y="2.993981933594" />
                  <Point X="-2.843886962891" Y="3.015440673828" />
                  <Point X="-2.843435791016" Y="3.036120849609" />
                  <Point X="-2.846680908203" Y="3.073212646484" />
                  <Point X="-2.850920410156" Y="3.121670410156" />
                  <Point X="-2.854955810547" Y="3.141957763672" />
                  <Point X="-2.861464355469" Y="3.162600097656" />
                  <Point X="-2.869794921875" Y="3.181532958984" />
                  <Point X="-3.083420654297" Y="3.551543457031" />
                  <Point X="-3.183332763672" Y="3.724596435547" />
                  <Point X="-3.027554931641" Y="3.844029785156" />
                  <Point X="-2.700625244141" Y="4.094683837891" />
                  <Point X="-2.427161865234" Y="4.246614257813" />
                  <Point X="-2.167036865234" Y="4.391133789062" />
                  <Point X="-2.129898681641" Y="4.342734375" />
                  <Point X="-2.04319519043" Y="4.229740722656" />
                  <Point X="-2.028892700195" Y="4.214799804688" />
                  <Point X="-2.012312866211" Y="4.200887207031" />
                  <Point X="-1.99511328125" Y="4.189395019531" />
                  <Point X="-1.953830444336" Y="4.167904296875" />
                  <Point X="-1.899897094727" Y="4.139828125" />
                  <Point X="-1.8806171875" Y="4.132330566406" />
                  <Point X="-1.859710571289" Y="4.126729003906" />
                  <Point X="-1.839267578125" Y="4.123582519531" />
                  <Point X="-1.818628295898" Y="4.124935546875" />
                  <Point X="-1.797312988281" Y="4.128693847656" />
                  <Point X="-1.777453491211" Y="4.134481933594" />
                  <Point X="-1.734454345703" Y="4.15229296875" />
                  <Point X="-1.678279296875" Y="4.175561523437" />
                  <Point X="-1.66014465332" Y="4.185510742187" />
                  <Point X="-1.642415039062" Y="4.197925292969" />
                  <Point X="-1.626863525391" Y="4.211563964844" />
                  <Point X="-1.614632568359" Y="4.228245117188" />
                  <Point X="-1.603810791016" Y="4.246989257813" />
                  <Point X="-1.59548046875" Y="4.265921386719" />
                  <Point X="-1.581484985352" Y="4.310309082031" />
                  <Point X="-1.563201171875" Y="4.368297851562" />
                  <Point X="-1.559165649414" Y="4.388584960938" />
                  <Point X="-1.557279174805" Y="4.410146484375" />
                  <Point X="-1.55773034668" Y="4.430826660156" />
                  <Point X="-1.582017089844" Y="4.615303710938" />
                  <Point X="-1.584201782227" Y="4.631897949219" />
                  <Point X="-1.372865600586" Y="4.691149414062" />
                  <Point X="-0.94963470459" Y="4.80980859375" />
                  <Point X="-0.618120056152" Y="4.848607421875" />
                  <Point X="-0.29471081543" Y="4.886458007812" />
                  <Point X="-0.217688583374" Y="4.599006347656" />
                  <Point X="-0.133903305054" Y="4.286315429688" />
                  <Point X="-0.121129844666" Y="4.258124023438" />
                  <Point X="-0.10327155304" Y="4.231397460938" />
                  <Point X="-0.0821145401" Y="4.20880859375" />
                  <Point X="-0.054819446564" Y="4.19421875" />
                  <Point X="-0.024381370544" Y="4.183886230469" />
                  <Point X="0.006155911446" Y="4.178844238281" />
                  <Point X="0.03669316864" Y="4.183886230469" />
                  <Point X="0.067131248474" Y="4.19421875" />
                  <Point X="0.094426460266" Y="4.20880859375" />
                  <Point X="0.115583503723" Y="4.231397460938" />
                  <Point X="0.133441802979" Y="4.258124023438" />
                  <Point X="0.146215255737" Y="4.286315429688" />
                  <Point X="0.255674667358" Y="4.694823730469" />
                  <Point X="0.307419372559" Y="4.8879375" />
                  <Point X="0.474492980957" Y="4.870440429688" />
                  <Point X="0.844041320801" Y="4.831738769531" />
                  <Point X="1.118317871094" Y="4.765520019531" />
                  <Point X="1.481026489258" Y="4.677950683594" />
                  <Point X="1.65871472168" Y="4.613501953125" />
                  <Point X="1.894646484375" Y="4.527927734375" />
                  <Point X="2.067275634766" Y="4.447194824219" />
                  <Point X="2.294576416016" Y="4.340893554688" />
                  <Point X="2.461372070312" Y="4.243718261719" />
                  <Point X="2.680978759766" Y="4.115774902344" />
                  <Point X="2.838265136719" Y="4.003921386719" />
                  <Point X="2.943260009766" Y="3.929254882812" />
                  <Point X="2.521157714844" Y="3.198151855469" />
                  <Point X="2.147581054688" Y="2.551097900391" />
                  <Point X="2.142075927734" Y="2.539931152344" />
                  <Point X="2.133076904297" Y="2.516056884766" />
                  <Point X="2.123768310547" Y="2.481247070312" />
                  <Point X="2.111607177734" Y="2.435770507813" />
                  <Point X="2.108619384766" Y="2.417934326172" />
                  <Point X="2.107728027344" Y="2.380952880859" />
                  <Point X="2.111357666016" Y="2.350852294922" />
                  <Point X="2.116099365234" Y="2.311528076172" />
                  <Point X="2.121442138672" Y="2.289604980469" />
                  <Point X="2.129708251953" Y="2.267516357422" />
                  <Point X="2.140071044922" Y="2.247471191406" />
                  <Point X="2.158696289062" Y="2.220022460938" />
                  <Point X="2.183028808594" Y="2.184162597656" />
                  <Point X="2.194464111328" Y="2.170329345703" />
                  <Point X="2.221598632812" Y="2.145592529297" />
                  <Point X="2.249047363281" Y="2.126967285156" />
                  <Point X="2.284907226562" Y="2.102635009766" />
                  <Point X="2.304952880859" Y="2.092271972656" />
                  <Point X="2.327041259766" Y="2.084006103516" />
                  <Point X="2.348963867188" Y="2.078663574219" />
                  <Point X="2.379064453125" Y="2.075033935547" />
                  <Point X="2.418388671875" Y="2.070291992188" />
                  <Point X="2.436467529297" Y="2.069845703125" />
                  <Point X="2.473206542969" Y="2.074171142578" />
                  <Point X="2.508016357422" Y="2.083479736328" />
                  <Point X="2.553492919922" Y="2.095640869141" />
                  <Point X="2.565284179688" Y="2.099638427734" />
                  <Point X="2.588533935547" Y="2.110145019531" />
                  <Point X="3.43386328125" Y="2.598196044922" />
                  <Point X="3.967326416016" Y="2.906191162109" />
                  <Point X="3.993008544922" Y="2.870499023438" />
                  <Point X="4.123271972656" Y="2.689462402344" />
                  <Point X="4.210957519531" Y="2.544561035156" />
                  <Point X="4.262198730469" Y="2.459884277344" />
                  <Point X="3.722632568359" Y="2.045859985352" />
                  <Point X="3.230783935547" Y="1.668451293945" />
                  <Point X="3.221424560547" Y="1.660241455078" />
                  <Point X="3.203973876953" Y="1.641627929688" />
                  <Point X="3.178921142578" Y="1.608944946289" />
                  <Point X="3.146191650391" Y="1.566246582031" />
                  <Point X="3.136605957031" Y="1.550912231445" />
                  <Point X="3.121629882812" Y="1.517086181641" />
                  <Point X="3.112297607422" Y="1.483716430664" />
                  <Point X="3.100105957031" Y="1.440121459961" />
                  <Point X="3.09665234375" Y="1.417821777344" />
                  <Point X="3.095836425781" Y="1.394251831055" />
                  <Point X="3.097739257812" Y="1.371768188477" />
                  <Point X="3.105399902344" Y="1.334640258789" />
                  <Point X="3.115408203125" Y="1.286135498047" />
                  <Point X="3.120679931641" Y="1.268977416992" />
                  <Point X="3.136282470703" Y="1.235740234375" />
                  <Point X="3.157118896484" Y="1.204069824219" />
                  <Point X="3.184340087891" Y="1.162694946289" />
                  <Point X="3.198893554688" Y="1.145450439453" />
                  <Point X="3.216137451172" Y="1.129360595703" />
                  <Point X="3.234346923828" Y="1.116034912109" />
                  <Point X="3.264541503906" Y="1.099037841797" />
                  <Point X="3.303989013672" Y="1.076832519531" />
                  <Point X="3.320521240234" Y="1.069501586914" />
                  <Point X="3.356118164062" Y="1.059438598633" />
                  <Point X="3.396943359375" Y="1.05404284668" />
                  <Point X="3.450278808594" Y="1.046994018555" />
                  <Point X="3.462702148438" Y="1.046175048828" />
                  <Point X="3.488203613281" Y="1.04698449707" />
                  <Point X="4.291209960938" Y="1.152702270508" />
                  <Point X="4.776839355469" Y="1.21663659668" />
                  <Point X="4.789988769531" Y="1.162623168945" />
                  <Point X="4.845936035156" Y="0.932809448242" />
                  <Point X="4.873568359375" Y="0.75533215332" />
                  <Point X="4.890864746094" Y="0.644238525391" />
                  <Point X="4.280249023438" Y="0.480624572754" />
                  <Point X="3.716579833984" Y="0.329589935303" />
                  <Point X="3.704791259766" Y="0.325586608887" />
                  <Point X="3.681545654297" Y="0.315067993164" />
                  <Point X="3.641436035156" Y="0.291883666992" />
                  <Point X="3.589035644531" Y="0.261595428467" />
                  <Point X="3.574311035156" Y="0.251096221924" />
                  <Point X="3.547530761719" Y="0.225576461792" />
                  <Point X="3.523465087891" Y="0.19491116333" />
                  <Point X="3.492024902344" Y="0.154848815918" />
                  <Point X="3.48030078125" Y="0.13556854248" />
                  <Point X="3.470527099609" Y="0.114104927063" />
                  <Point X="3.463680908203" Y="0.092604270935" />
                  <Point X="3.455658935547" Y="0.050716644287" />
                  <Point X="3.445178710938" Y="-0.004006377697" />
                  <Point X="3.443483154297" Y="-0.021875295639" />
                  <Point X="3.445178710938" Y="-0.058553779602" />
                  <Point X="3.453200683594" Y="-0.100441253662" />
                  <Point X="3.463680908203" Y="-0.155164276123" />
                  <Point X="3.470527099609" Y="-0.176665084839" />
                  <Point X="3.48030078125" Y="-0.198128707886" />
                  <Point X="3.492024902344" Y="-0.217408966064" />
                  <Point X="3.516090576172" Y="-0.248074432373" />
                  <Point X="3.547530761719" Y="-0.288136627197" />
                  <Point X="3.559999023438" Y="-0.30123614502" />
                  <Point X="3.589035644531" Y="-0.324155426025" />
                  <Point X="3.629145263672" Y="-0.347339752197" />
                  <Point X="3.681545654297" Y="-0.377627990723" />
                  <Point X="3.692710205078" Y="-0.383139038086" />
                  <Point X="3.716579833984" Y="-0.392149932861" />
                  <Point X="4.452973144531" Y="-0.589465942383" />
                  <Point X="4.89147265625" Y="-0.706961486816" />
                  <Point X="4.886261230469" Y="-0.741527160645" />
                  <Point X="4.855022460938" Y="-0.948725830078" />
                  <Point X="4.81962109375" Y="-1.103860717773" />
                  <Point X="4.801173828125" Y="-1.184698974609" />
                  <Point X="4.078079833984" Y="-1.089501831055" />
                  <Point X="3.424382080078" Y="-1.003441040039" />
                  <Point X="3.408035644531" Y="-1.002710327148" />
                  <Point X="3.374658691406" Y="-1.005508728027" />
                  <Point X="3.295937744141" Y="-1.022619140625" />
                  <Point X="3.193094482422" Y="-1.044972412109" />
                  <Point X="3.163974121094" Y="-1.056596923828" />
                  <Point X="3.136147460938" Y="-1.073489257812" />
                  <Point X="3.112397460938" Y="-1.093960083008" />
                  <Point X="3.064815429688" Y="-1.151186279297" />
                  <Point X="3.002653320312" Y="-1.225948120117" />
                  <Point X="2.987932617188" Y="-1.250330566406" />
                  <Point X="2.976589355469" Y="-1.277715698242" />
                  <Point X="2.969757568359" Y="-1.305365478516" />
                  <Point X="2.962937988281" Y="-1.379475952148" />
                  <Point X="2.954028564453" Y="-1.476295776367" />
                  <Point X="2.956347167969" Y="-1.507564208984" />
                  <Point X="2.964078613281" Y="-1.539185180664" />
                  <Point X="2.976450195312" Y="-1.567996826172" />
                  <Point X="3.020015625" Y="-1.635759765625" />
                  <Point X="3.076930419922" Y="-1.724287109375" />
                  <Point X="3.086932861328" Y="-1.737239013672" />
                  <Point X="3.110628417969" Y="-1.760909179688" />
                  <Point X="3.794005859375" Y="-2.285283447266" />
                  <Point X="4.213122070312" Y="-2.606883056641" />
                  <Point X="4.212869140625" Y="-2.607292724609" />
                  <Point X="4.124810058594" Y="-2.749785888672" />
                  <Point X="4.051596923828" Y="-2.853811523438" />
                  <Point X="4.028981445312" Y="-2.885945068359" />
                  <Point X="3.383160644531" Y="-2.513080078125" />
                  <Point X="2.800954589844" Y="-2.176943115234" />
                  <Point X="2.786131591797" Y="-2.170012451172" />
                  <Point X="2.754224609375" Y="-2.159825195312" />
                  <Point X="2.660534179688" Y="-2.142904785156" />
                  <Point X="2.538134277344" Y="-2.120799560547" />
                  <Point X="2.506781982422" Y="-2.120395263672" />
                  <Point X="2.474609130859" Y="-2.125353027344" />
                  <Point X="2.444833251953" Y="-2.135176757812" />
                  <Point X="2.366999267578" Y="-2.176140136719" />
                  <Point X="2.265315185547" Y="-2.229655761719" />
                  <Point X="2.242384033203" Y="-2.246549072266" />
                  <Point X="2.221424316406" Y="-2.267509033203" />
                  <Point X="2.204531738281" Y="-2.290439453125" />
                  <Point X="2.163568359375" Y="-2.368273193359" />
                  <Point X="2.110052734375" Y="-2.469957519531" />
                  <Point X="2.100229003906" Y="-2.499733398438" />
                  <Point X="2.095271240234" Y="-2.531906005859" />
                  <Point X="2.095675537109" Y="-2.563258300781" />
                  <Point X="2.112595947266" Y="-2.656948730469" />
                  <Point X="2.134701171875" Y="-2.779348632812" />
                  <Point X="2.138985595703" Y="-2.795141845703" />
                  <Point X="2.151819091797" Y="-2.826078613281" />
                  <Point X="2.590957763672" Y="-3.586688964844" />
                  <Point X="2.861283447266" Y="-4.054906738281" />
                  <Point X="2.781854492188" Y="-4.111640625" />
                  <Point X="2.701765136719" Y="-4.163481445312" />
                  <Point X="2.202823242188" Y="-3.513247802734" />
                  <Point X="1.758546020508" Y="-2.934255126953" />
                  <Point X="1.747503540039" Y="-2.922179443359" />
                  <Point X="1.721924194336" Y="-2.900557373047" />
                  <Point X="1.629519897461" Y="-2.841149902344" />
                  <Point X="1.50880078125" Y="-2.763538574219" />
                  <Point X="1.479989746094" Y="-2.751167236328" />
                  <Point X="1.448368408203" Y="-2.743435546875" />
                  <Point X="1.417099487305" Y="-2.741116699219" />
                  <Point X="1.316039794922" Y="-2.750416259766" />
                  <Point X="1.184012573242" Y="-2.762565673828" />
                  <Point X="1.156362670898" Y="-2.769397460938" />
                  <Point X="1.128977661133" Y="-2.780740722656" />
                  <Point X="1.104595825195" Y="-2.795461181641" />
                  <Point X="1.026560180664" Y="-2.860345214844" />
                  <Point X="0.924612121582" Y="-2.945111572266" />
                  <Point X="0.904141052246" Y="-2.968861816406" />
                  <Point X="0.887248779297" Y="-2.996688476562" />
                  <Point X="0.875624328613" Y="-3.025808837891" />
                  <Point X="0.852292053223" Y="-3.133155761719" />
                  <Point X="0.821810058594" Y="-3.273396728516" />
                  <Point X="0.819724487305" Y="-3.289627441406" />
                  <Point X="0.819742248535" Y="-3.323120117188" />
                  <Point X="0.944190795898" Y="-4.268400390625" />
                  <Point X="1.022065246582" Y="-4.859915039062" />
                  <Point X="0.975676452637" Y="-4.870083496094" />
                  <Point X="0.929315551758" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058434082031" Y="-4.752635742188" />
                  <Point X="-1.141246459961" Y="-4.731328613281" />
                  <Point X="-1.120775756836" Y="-4.575838378906" />
                  <Point X="-1.120077392578" Y="-4.568099609375" />
                  <Point X="-1.119451660156" Y="-4.541033691406" />
                  <Point X="-1.121759277344" Y="-4.509328125" />
                  <Point X="-1.123334106445" Y="-4.497690429688" />
                  <Point X="-1.149577026367" Y="-4.365758300781" />
                  <Point X="-1.183861572266" Y="-4.193398925781" />
                  <Point X="-1.18812512207" Y="-4.17846875" />
                  <Point X="-1.19902734375" Y="-4.149502441406" />
                  <Point X="-1.205666137695" Y="-4.135466308594" />
                  <Point X="-1.221738037109" Y="-4.10762890625" />
                  <Point X="-1.23057409668" Y="-4.094861816406" />
                  <Point X="-1.250208496094" Y="-4.070937011719" />
                  <Point X="-1.261006835938" Y="-4.059779296875" />
                  <Point X="-1.362141967773" Y="-3.9710859375" />
                  <Point X="-1.494267578125" Y="-3.855214355469" />
                  <Point X="-1.506739135742" Y="-3.84596484375" />
                  <Point X="-1.533022216797" Y="-3.82962109375" />
                  <Point X="-1.546833496094" Y="-3.822526855469" />
                  <Point X="-1.576530761719" Y="-3.810225830078" />
                  <Point X="-1.591313354492" Y="-3.805476074219" />
                  <Point X="-1.621455078125" Y="-3.798447998047" />
                  <Point X="-1.636814208984" Y="-3.796169677734" />
                  <Point X="-1.771043212891" Y="-3.787371826172" />
                  <Point X="-1.946403198242" Y="-3.775878173828" />
                  <Point X="-1.961928222656" Y="-3.776132324219" />
                  <Point X="-1.992729614258" Y="-3.779166015625" />
                  <Point X="-2.008006103516" Y="-3.781945556641" />
                  <Point X="-2.03905480957" Y="-3.790265136719" />
                  <Point X="-2.053674560547" Y="-3.795496337891" />
                  <Point X="-2.081865478516" Y="-3.808269775391" />
                  <Point X="-2.095437011719" Y="-3.815812011719" />
                  <Point X="-2.207283691406" Y="-3.890545654297" />
                  <Point X="-2.353403320312" Y="-3.988180175781" />
                  <Point X="-2.359680664062" Y="-3.992756591797" />
                  <Point X="-2.380442138672" Y="-4.010131103516" />
                  <Point X="-2.402759277344" Y="-4.032771484375" />
                  <Point X="-2.410471435547" Y="-4.041629638672" />
                  <Point X="-2.503202880859" Y="-4.162479492188" />
                  <Point X="-2.509110839844" Y="-4.158821289062" />
                  <Point X="-2.747581787109" Y="-4.011166259766" />
                  <Point X="-2.915427001953" Y="-3.881931152344" />
                  <Point X="-2.980862792969" Y="-3.831547851562" />
                  <Point X="-2.664254150391" Y="-3.283165527344" />
                  <Point X="-2.341488769531" Y="-2.724119628906" />
                  <Point X="-2.334850097656" Y="-2.710083251953" />
                  <Point X="-2.323947998047" Y="-2.681116943359" />
                  <Point X="-2.319684570312" Y="-2.666187011719" />
                  <Point X="-2.313413574219" Y="-2.634660644531" />
                  <Point X="-2.311638916016" Y="-2.619235351562" />
                  <Point X="-2.310626220703" Y="-2.588301269531" />
                  <Point X="-2.314666259766" Y="-2.557615234375" />
                  <Point X="-2.323650878906" Y="-2.527997558594" />
                  <Point X="-2.329357421875" Y="-2.513557128906" />
                  <Point X="-2.343574462891" Y="-2.484728271484" />
                  <Point X="-2.351557128906" Y="-2.47141015625" />
                  <Point X="-2.369584716797" Y="-2.446253173828" />
                  <Point X="-2.379629638672" Y="-2.434414306641" />
                  <Point X="-2.396978271484" Y="-2.417065673828" />
                  <Point X="-2.408817138672" Y="-2.407020751953" />
                  <Point X="-2.433974121094" Y="-2.388993164062" />
                  <Point X="-2.447292236328" Y="-2.381010498047" />
                  <Point X="-2.47612109375" Y="-2.366793457031" />
                  <Point X="-2.490561523438" Y="-2.361086914062" />
                  <Point X="-2.520179199219" Y="-2.352102294922" />
                  <Point X="-2.550865234375" Y="-2.348062255859" />
                  <Point X="-2.581799316406" Y="-2.349074951172" />
                  <Point X="-2.597224609375" Y="-2.350849609375" />
                  <Point X="-2.628750976562" Y="-2.357120605469" />
                  <Point X="-2.643680908203" Y="-2.361384033203" />
                  <Point X="-2.672647216797" Y="-2.372286132812" />
                  <Point X="-2.68668359375" Y="-2.378924804688" />
                  <Point X="-3.417036865234" Y="-2.800594482422" />
                  <Point X="-3.793089599609" Y="-3.017708496094" />
                  <Point X="-3.815609619141" Y="-2.988121582031" />
                  <Point X="-4.004014404297" Y="-2.740595947266" />
                  <Point X="-4.124342773438" Y="-2.538823486328" />
                  <Point X="-4.181265136719" Y="-2.443373535156" />
                  <Point X="-3.612856933594" Y="-2.007218139648" />
                  <Point X="-3.048122314453" Y="-1.573882080078" />
                  <Point X="-3.036481689453" Y="-1.563309570312" />
                  <Point X="-3.015104003906" Y="-1.540389038086" />
                  <Point X="-3.005366943359" Y="-1.528040649414" />
                  <Point X="-2.987402099609" Y="-1.500908935547" />
                  <Point X="-2.979835693359" Y="-1.487124267578" />
                  <Point X="-2.967080078125" Y="-1.458495483398" />
                  <Point X="-2.961890869141" Y="-1.443651489258" />
                  <Point X="-2.954186279297" Y="-1.413904174805" />
                  <Point X="-2.951552734375" Y="-1.398802734375" />
                  <Point X="-2.948748535156" Y="-1.368373535156" />
                  <Point X="-2.948577880859" Y="-1.353045654297" />
                  <Point X="-2.950786865234" Y="-1.321375244141" />
                  <Point X="-2.953083740234" Y="-1.306219482422" />
                  <Point X="-2.960084960938" Y="-1.276474731445" />
                  <Point X="-2.971778808594" Y="-1.248243408203" />
                  <Point X="-2.987860839844" Y="-1.222260131836" />
                  <Point X="-2.996953369141" Y="-1.209919311523" />
                  <Point X="-3.017785644531" Y="-1.185963012695" />
                  <Point X="-3.028745117188" Y="-1.175244995117" />
                  <Point X="-3.052245117188" Y="-1.15571081543" />
                  <Point X="-3.064785888672" Y="-1.14689465332" />
                  <Point X="-3.091268554688" Y="-1.131308349609" />
                  <Point X="-3.10543359375" Y="-1.124481445312" />
                  <Point X="-3.134697021484" Y="-1.113257202148" />
                  <Point X="-3.149795166016" Y="-1.108860107422" />
                  <Point X="-3.181683105469" Y="-1.102378417969" />
                  <Point X="-3.197299560547" Y="-1.100532348633" />
                  <Point X="-3.228622558594" Y="-1.09944140625" />
                  <Point X="-3.244329101562" Y="-1.100196411133" />
                  <Point X="-4.166331054688" Y="-1.221580566406" />
                  <Point X="-4.660920898438" Y="-1.286694458008" />
                  <Point X="-4.667073730469" Y="-1.26260546875" />
                  <Point X="-4.740762207031" Y="-0.974118286133" />
                  <Point X="-4.772597167969" Y="-0.751530090332" />
                  <Point X="-4.786452636719" Y="-0.65465435791" />
                  <Point X="-4.151605957031" Y="-0.484547607422" />
                  <Point X="-3.508288085938" Y="-0.312171356201" />
                  <Point X="-3.496957519531" Y="-0.30835635376" />
                  <Point X="-3.474864990234" Y="-0.299333740234" />
                  <Point X="-3.464103027344" Y="-0.294125701904" />
                  <Point X="-3.436501953125" Y="-0.278553741455" />
                  <Point X="-3.429653808594" Y="-0.274297332764" />
                  <Point X="-3.40987890625" Y="-0.260395874023" />
                  <Point X="-3.382340332031" Y="-0.237967651367" />
                  <Point X="-3.373702392578" Y="-0.229994476318" />
                  <Point X="-3.357477050781" Y="-0.213042541504" />
                  <Point X="-3.343469482422" Y="-0.19421647644" />
                  <Point X="-3.331894287109" Y="-0.173804458618" />
                  <Point X="-3.326739257812" Y="-0.163239898682" />
                  <Point X="-3.315302978516" Y="-0.135578948975" />
                  <Point X="-3.312680664062" Y="-0.128439544678" />
                  <Point X="-3.305974365234" Y="-0.106652328491" />
                  <Point X="-3.298487304688" Y="-0.0749480896" />
                  <Point X="-3.296609863281" Y="-0.064342353821" />
                  <Point X="-3.294067871094" Y="-0.042985836029" />
                  <Point X="-3.293959960938" Y="-0.021478319168" />
                  <Point X="-3.296287109375" Y="-9.7279117E-05" />
                  <Point X="-3.298057617188" Y="0.010526881218" />
                  <Point X="-3.304315673828" Y="0.03827091217" />
                  <Point X="-3.30626953125" Y="0.04556829834" />
                  <Point X="-3.313280517578" Y="0.067109107971" />
                  <Point X="-3.325945800781" Y="0.098730270386" />
                  <Point X="-3.330983886719" Y="0.109351898193" />
                  <Point X="-3.342332275391" Y="0.129890548706" />
                  <Point X="-3.356129394531" Y="0.148869537354" />
                  <Point X="-3.372165527344" Y="0.16600012207" />
                  <Point X="-3.380715087891" Y="0.174068847656" />
                  <Point X="-3.404566650391" Y="0.193937957764" />
                  <Point X="-3.410958007812" Y="0.198819030762" />
                  <Point X="-3.430912597656" Y="0.212346832275" />
                  <Point X="-3.462200683594" Y="0.230477783203" />
                  <Point X="-3.473406738281" Y="0.236020828247" />
                  <Point X="-3.496450439453" Y="0.245587677002" />
                  <Point X="-3.508288085938" Y="0.249611358643" />
                  <Point X="-4.348736328125" Y="0.474808654785" />
                  <Point X="-4.785446289062" Y="0.591824707031" />
                  <Point X="-4.778820800781" Y="0.636598632813" />
                  <Point X="-4.731331542969" Y="0.957527526855" />
                  <Point X="-4.667246582031" Y="1.194020263672" />
                  <Point X="-4.633586425781" Y="1.318237182617" />
                  <Point X="-4.239212402344" Y="1.266316894531" />
                  <Point X="-3.778066162109" Y="1.20560559082" />
                  <Point X="-3.76773828125" Y="1.204815429688" />
                  <Point X="-3.747057861328" Y="1.204364379883" />
                  <Point X="-3.736705322266" Y="1.204703125" />
                  <Point X="-3.715143554688" Y="1.20658984375" />
                  <Point X="-3.704890136719" Y="1.208053710938" />
                  <Point X="-3.684603271484" Y="1.212089111328" />
                  <Point X="-3.674569824219" Y="1.214660644531" />
                  <Point X="-3.647937255859" Y="1.223057983398" />
                  <Point X="-3.613143798828" Y="1.234028320312" />
                  <Point X="-3.603449707031" Y="1.237676757812" />
                  <Point X="-3.584516845703" Y="1.24600769043" />
                  <Point X="-3.575278076172" Y="1.250689819336" />
                  <Point X="-3.556533935547" Y="1.261511962891" />
                  <Point X="-3.547859375" Y="1.267172119141" />
                  <Point X="-3.531178466797" Y="1.279403076172" />
                  <Point X="-3.515928955078" Y="1.293376708984" />
                  <Point X="-3.502290527344" Y="1.308928100586" />
                  <Point X="-3.495895263672" Y="1.317077148438" />
                  <Point X="-3.483480712891" Y="1.334807128906" />
                  <Point X="-3.478010986328" Y="1.343603027344" />
                  <Point X="-3.468062255859" Y="1.361737426758" />
                  <Point X="-3.463583251953" Y="1.371075439453" />
                  <Point X="-3.452896728516" Y="1.396874267578" />
                  <Point X="-3.438935791016" Y="1.430579345703" />
                  <Point X="-3.435499511719" Y="1.440350463867" />
                  <Point X="-3.4297109375" Y="1.460209594727" />
                  <Point X="-3.427358642578" Y="1.470297851562" />
                  <Point X="-3.423600341797" Y="1.491612670898" />
                  <Point X="-3.422360595703" Y="1.501896972656" />
                  <Point X="-3.421008056641" Y="1.522537109375" />
                  <Point X="-3.42191015625" Y="1.543200439453" />
                  <Point X="-3.425056640625" Y="1.563644165039" />
                  <Point X="-3.427188232422" Y="1.573780639648" />
                  <Point X="-3.432790039062" Y="1.594686889648" />
                  <Point X="-3.436011962891" Y="1.604530151367" />
                  <Point X="-3.443508789062" Y="1.62380859375" />
                  <Point X="-3.447783691406" Y="1.633243530273" />
                  <Point X="-3.460677978516" Y="1.658013305664" />
                  <Point X="-3.4775234375" Y="1.690372924805" />
                  <Point X="-3.482799560547" Y="1.699285644531" />
                  <Point X="-3.494291259766" Y="1.71648449707" />
                  <Point X="-3.500506835938" Y="1.724770507812" />
                  <Point X="-3.514419433594" Y="1.741351074219" />
                  <Point X="-3.521500244141" Y="1.748911132812" />
                  <Point X="-3.536442382812" Y="1.76321496582" />
                  <Point X="-3.544303710938" Y="1.769959106445" />
                  <Point X="-4.026387207031" Y="2.139874267578" />
                  <Point X="-4.227614257812" Y="2.294281982422" />
                  <Point X="-4.186818359375" Y="2.36417578125" />
                  <Point X="-4.002293212891" Y="2.680312011719" />
                  <Point X="-3.832540283203" Y="2.898505126953" />
                  <Point X="-3.726338623047" Y="3.035012695312" />
                  <Point X="-3.537379882812" Y="2.925917236328" />
                  <Point X="-3.254156982422" Y="2.7623984375" />
                  <Point X="-3.244918457031" Y="2.757716552734" />
                  <Point X="-3.225986083984" Y="2.749385986328" />
                  <Point X="-3.216292236328" Y="2.745737304688" />
                  <Point X="-3.195650146484" Y="2.739228759766" />
                  <Point X="-3.185615722656" Y="2.736656982422" />
                  <Point X="-3.165327880859" Y="2.732621582031" />
                  <Point X="-3.155074462891" Y="2.731157958984" />
                  <Point X="-3.117982666016" Y="2.727912841797" />
                  <Point X="-3.069525146484" Y="2.723673339844" />
                  <Point X="-3.059173339844" Y="2.723334472656" />
                  <Point X="-3.038493164062" Y="2.723785644531" />
                  <Point X="-3.028164794922" Y="2.724575683594" />
                  <Point X="-3.006705810547" Y="2.727400878906" />
                  <Point X="-2.996524902344" Y="2.729310791016" />
                  <Point X="-2.976432861328" Y="2.734227539062" />
                  <Point X="-2.956996826172" Y="2.741302001953" />
                  <Point X="-2.938445068359" Y="2.750450683594" />
                  <Point X="-2.929418212891" Y="2.755531738281" />
                  <Point X="-2.9111640625" Y="2.767161132812" />
                  <Point X="-2.902745117188" Y="2.773194091797" />
                  <Point X="-2.886614501953" Y="2.786140380859" />
                  <Point X="-2.878902832031" Y="2.793053710938" />
                  <Point X="-2.852574707031" Y="2.819381591797" />
                  <Point X="-2.818179199219" Y="2.853777099609" />
                  <Point X="-2.811265625" Y="2.861489257812" />
                  <Point X="-2.798318603516" Y="2.877620605469" />
                  <Point X="-2.79228515625" Y="2.886039794922" />
                  <Point X="-2.780655761719" Y="2.904293945312" />
                  <Point X="-2.775574707031" Y="2.913320800781" />
                  <Point X="-2.766426025391" Y="2.931872558594" />
                  <Point X="-2.7593515625" Y="2.95130859375" />
                  <Point X="-2.754434814453" Y="2.971400634766" />
                  <Point X="-2.752524902344" Y="2.981581542969" />
                  <Point X="-2.749699707031" Y="3.003040283203" />
                  <Point X="-2.748909667969" Y="3.013368652344" />
                  <Point X="-2.748458496094" Y="3.034048828125" />
                  <Point X="-2.748797363281" Y="3.044400634766" />
                  <Point X="-2.752042480469" Y="3.081492431641" />
                  <Point X="-2.756281982422" Y="3.129950195312" />
                  <Point X="-2.757745849609" Y="3.140203857422" />
                  <Point X="-2.76178125" Y="3.160491210938" />
                  <Point X="-2.764352783203" Y="3.170524902344" />
                  <Point X="-2.770861328125" Y="3.191167236328" />
                  <Point X="-2.774509521484" Y="3.200860595703" />
                  <Point X="-2.782840087891" Y="3.219793457031" />
                  <Point X="-2.787522460938" Y="3.229032958984" />
                  <Point X="-3.001148193359" Y="3.599043457031" />
                  <Point X="-3.059387207031" Y="3.699916259766" />
                  <Point X="-2.969752929688" Y="3.768637939453" />
                  <Point X="-2.648373535156" Y="4.015036621094" />
                  <Point X="-2.381024414062" Y="4.1635703125" />
                  <Point X="-2.192525390625" Y="4.268296386719" />
                  <Point X="-2.118563720703" Y="4.171908203125" />
                  <Point X="-2.111820556641" Y="4.164047851562" />
                  <Point X="-2.097518066406" Y="4.149106933594" />
                  <Point X="-2.089958740234" Y="4.142026367188" />
                  <Point X="-2.07337890625" Y="4.128113769531" />
                  <Point X="-2.065091308594" Y="4.121896972656" />
                  <Point X="-2.047891845703" Y="4.110404785156" />
                  <Point X="-2.038979736328" Y="4.105129394531" />
                  <Point X="-1.997696899414" Y="4.083638427734" />
                  <Point X="-1.943763549805" Y="4.055562255859" />
                  <Point X="-1.934328735352" Y="4.051287353516" />
                  <Point X="-1.915048828125" Y="4.043789794922" />
                  <Point X="-1.905203613281" Y="4.040567138672" />
                  <Point X="-1.88429699707" Y="4.034965576172" />
                  <Point X="-1.874162475586" Y="4.032834716797" />
                  <Point X="-1.853719360352" Y="4.029688232422" />
                  <Point X="-1.833053222656" Y="4.028785888672" />
                  <Point X="-1.812413818359" Y="4.030138916016" />
                  <Point X="-1.802132446289" Y="4.031378662109" />
                  <Point X="-1.780817138672" Y="4.035136962891" />
                  <Point X="-1.770731079102" Y="4.037488525391" />
                  <Point X="-1.750871582031" Y="4.043276611328" />
                  <Point X="-1.741098266602" Y="4.046713623047" />
                  <Point X="-1.698099121094" Y="4.064524658203" />
                  <Point X="-1.641924072266" Y="4.087793212891" />
                  <Point X="-1.632584594727" Y="4.092272949219" />
                  <Point X="-1.614449951172" Y="4.102222167969" />
                  <Point X="-1.605654418945" Y="4.10769140625" />
                  <Point X="-1.587924804688" Y="4.120105957031" />
                  <Point X="-1.579776245117" Y="4.126501464844" />
                  <Point X="-1.564224853516" Y="4.140140136719" />
                  <Point X="-1.550250976562" Y="4.155390136719" />
                  <Point X="-1.538020019531" Y="4.172071289062" />
                  <Point X="-1.532359863281" Y="4.180745605469" />
                  <Point X="-1.521538085938" Y="4.199489746094" />
                  <Point X="-1.516856201172" Y="4.208728515625" />
                  <Point X="-1.508525878906" Y="4.227660644531" />
                  <Point X="-1.504877441406" Y="4.237354003906" />
                  <Point X="-1.490881958008" Y="4.281741699219" />
                  <Point X="-1.472598144531" Y="4.33973046875" />
                  <Point X="-1.470026733398" Y="4.349763671875" />
                  <Point X="-1.465991088867" Y="4.37005078125" />
                  <Point X="-1.46452722168" Y="4.3803046875" />
                  <Point X="-1.46264074707" Y="4.401866210938" />
                  <Point X="-1.462301757812" Y="4.41221875" />
                  <Point X="-1.462752929688" Y="4.432898925781" />
                  <Point X="-1.46354309082" Y="4.4432265625" />
                  <Point X="-1.479266113281" Y="4.562655273438" />
                  <Point X="-1.347219726562" Y="4.599676757812" />
                  <Point X="-0.931179016113" Y="4.7163203125" />
                  <Point X="-0.607077087402" Y="4.754251464844" />
                  <Point X="-0.365222076416" Y="4.782557128906" />
                  <Point X="-0.309451507568" Y="4.574418457031" />
                  <Point X="-0.225666244507" Y="4.261727539062" />
                  <Point X="-0.220435317993" Y="4.247107910156" />
                  <Point X="-0.207661773682" Y="4.218916503906" />
                  <Point X="-0.200119155884" Y="4.205344726562" />
                  <Point X="-0.182260925293" Y="4.178618164062" />
                  <Point X="-0.172608306885" Y="4.166456054688" />
                  <Point X="-0.151451248169" Y="4.1438671875" />
                  <Point X="-0.126898002625" Y="4.125026367188" />
                  <Point X="-0.099602806091" Y="4.110436523438" />
                  <Point X="-0.085356712341" Y="4.104260742188" />
                  <Point X="-0.054918586731" Y="4.093927978516" />
                  <Point X="-0.039857292175" Y="4.090155273438" />
                  <Point X="-0.009319890976" Y="4.08511328125" />
                  <Point X="0.021631721497" Y="4.08511328125" />
                  <Point X="0.052169120789" Y="4.090155273438" />
                  <Point X="0.067230415344" Y="4.093927978516" />
                  <Point X="0.097668540955" Y="4.104260742188" />
                  <Point X="0.111914489746" Y="4.110436523438" />
                  <Point X="0.139209686279" Y="4.125026367188" />
                  <Point X="0.16376322937" Y="4.143866699219" />
                  <Point X="0.184920150757" Y="4.166455566406" />
                  <Point X="0.194572906494" Y="4.178617675781" />
                  <Point X="0.212431137085" Y="4.205344238281" />
                  <Point X="0.219973754883" Y="4.218916503906" />
                  <Point X="0.232747146606" Y="4.247107910156" />
                  <Point X="0.237978225708" Y="4.261727539062" />
                  <Point X="0.347437652588" Y="4.670235839844" />
                  <Point X="0.378190307617" Y="4.785006347656" />
                  <Point X="0.464597991943" Y="4.77595703125" />
                  <Point X="0.827876647949" Y="4.737912109375" />
                  <Point X="1.096022583008" Y="4.673173339844" />
                  <Point X="1.453596191406" Y="4.58684375" />
                  <Point X="1.626322509766" Y="4.524194824219" />
                  <Point X="1.858258056641" Y="4.440069824219" />
                  <Point X="2.027030883789" Y="4.361140625" />
                  <Point X="2.250451904297" Y="4.256653808594" />
                  <Point X="2.413549072266" Y="4.161633300781" />
                  <Point X="2.62943359375" Y="4.035858154297" />
                  <Point X="2.783208496094" Y="3.926501953125" />
                  <Point X="2.817779785156" Y="3.901916748047" />
                  <Point X="2.438885253906" Y="3.245651855469" />
                  <Point X="2.06530859375" Y="2.598597900391" />
                  <Point X="2.062373046875" Y="2.593104980469" />
                  <Point X="2.053181396484" Y="2.573438476562" />
                  <Point X="2.044182373047" Y="2.549564208984" />
                  <Point X="2.041301513672" Y="2.540598876953" />
                  <Point X="2.031993041992" Y="2.5057890625" />
                  <Point X="2.01983190918" Y="2.4603125" />
                  <Point X="2.017912597656" Y="2.451465576172" />
                  <Point X="2.013646972656" Y="2.420223388672" />
                  <Point X="2.012755615234" Y="2.383241943359" />
                  <Point X="2.013411254883" Y="2.369579833984" />
                  <Point X="2.017041015625" Y="2.339479248047" />
                  <Point X="2.021782592773" Y="2.300155029297" />
                  <Point X="2.02380078125" Y="2.289034423828" />
                  <Point X="2.029143676758" Y="2.267111328125" />
                  <Point X="2.032468261719" Y="2.256308837891" />
                  <Point X="2.040734375" Y="2.234220214844" />
                  <Point X="2.045318359375" Y="2.223889160156" />
                  <Point X="2.055681152344" Y="2.203843994141" />
                  <Point X="2.061459960938" Y="2.194129882813" />
                  <Point X="2.080085205078" Y="2.166681152344" />
                  <Point X="2.104417724609" Y="2.130821289063" />
                  <Point X="2.109807861328" Y="2.123634277344" />
                  <Point X="2.130462402344" Y="2.100124023438" />
                  <Point X="2.157596923828" Y="2.075387207031" />
                  <Point X="2.168257324219" Y="2.066981445312" />
                  <Point X="2.195706054688" Y="2.048356201172" />
                  <Point X="2.231565917969" Y="2.024023925781" />
                  <Point X="2.241280029297" Y="2.018245117188" />
                  <Point X="2.261325683594" Y="2.007881835938" />
                  <Point X="2.271657226562" Y="2.003297851562" />
                  <Point X="2.293745605469" Y="1.995031982422" />
                  <Point X="2.304548095703" Y="1.991707397461" />
                  <Point X="2.326470703125" Y="1.986364868164" />
                  <Point X="2.337590820312" Y="1.984346801758" />
                  <Point X="2.36769140625" Y="1.980717163086" />
                  <Point X="2.407015625" Y="1.975975219727" />
                  <Point X="2.416044189453" Y="1.975321044922" />
                  <Point X="2.447575439453" Y="1.975497314453" />
                  <Point X="2.484314453125" Y="1.979822753906" />
                  <Point X="2.497748535156" Y="1.982395874023" />
                  <Point X="2.532558349609" Y="1.991704467773" />
                  <Point X="2.578034912109" Y="2.003865600586" />
                  <Point X="2.583995117188" Y="2.005670776367" />
                  <Point X="2.604405761719" Y="2.013067626953" />
                  <Point X="2.627655517578" Y="2.02357421875" />
                  <Point X="2.636033935547" Y="2.027872558594" />
                  <Point X="3.48136328125" Y="2.515923583984" />
                  <Point X="3.940405029297" Y="2.780951416016" />
                  <Point X="4.043952392578" Y="2.637044189453" />
                  <Point X="4.129680664062" Y="2.495377197266" />
                  <Point X="4.136884765625" Y="2.483472412109" />
                  <Point X="3.664800292969" Y="2.121228515625" />
                  <Point X="3.172951660156" Y="1.743819824219" />
                  <Point X="3.168137939453" Y="1.739868896484" />
                  <Point X="3.152119384766" Y="1.725216918945" />
                  <Point X="3.134668701172" Y="1.706603393555" />
                  <Point X="3.128576660156" Y="1.699422973633" />
                  <Point X="3.103523925781" Y="1.666739868164" />
                  <Point X="3.070794433594" Y="1.624041503906" />
                  <Point X="3.065635986328" Y="1.616603149414" />
                  <Point X="3.049739013672" Y="1.589371459961" />
                  <Point X="3.034762939453" Y="1.555545532227" />
                  <Point X="3.030140380859" Y="1.542672363281" />
                  <Point X="3.020808105469" Y="1.509302612305" />
                  <Point X="3.008616455078" Y="1.465707641602" />
                  <Point X="3.006225097656" Y="1.454661010742" />
                  <Point X="3.002771484375" Y="1.432361328125" />
                  <Point X="3.001709228516" Y="1.421108398438" />
                  <Point X="3.000893310547" Y="1.397538452148" />
                  <Point X="3.001174804688" Y="1.386240356445" />
                  <Point X="3.003077636719" Y="1.363756835938" />
                  <Point X="3.004698974609" Y="1.352571289063" />
                  <Point X="3.012359619141" Y="1.315443359375" />
                  <Point X="3.022367919922" Y="1.266938476562" />
                  <Point X="3.02459765625" Y="1.25823449707" />
                  <Point X="3.034683837891" Y="1.228608276367" />
                  <Point X="3.050286376953" Y="1.19537109375" />
                  <Point X="3.056918457031" Y="1.183525512695" />
                  <Point X="3.077754882812" Y="1.151855102539" />
                  <Point X="3.104976074219" Y="1.110480224609" />
                  <Point X="3.111739501953" Y="1.101423828125" />
                  <Point X="3.12629296875" Y="1.084179321289" />
                  <Point X="3.134083007812" Y="1.075991333008" />
                  <Point X="3.151326904297" Y="1.059901367188" />
                  <Point X="3.160034423828" Y="1.052695922852" />
                  <Point X="3.178243896484" Y="1.039370361328" />
                  <Point X="3.187745849609" Y="1.03325" />
                  <Point X="3.217940429688" Y="1.016252929688" />
                  <Point X="3.257387939453" Y="0.994047607422" />
                  <Point X="3.265479248047" Y="0.989987792969" />
                  <Point X="3.294678222656" Y="0.97808416748" />
                  <Point X="3.330275146484" Y="0.968021179199" />
                  <Point X="3.343670410156" Y="0.965257568359" />
                  <Point X="3.384495605469" Y="0.959861938477" />
                  <Point X="3.437831054688" Y="0.952813049316" />
                  <Point X="3.444029785156" Y="0.952199707031" />
                  <Point X="3.465716064453" Y="0.95122277832" />
                  <Point X="3.491217529297" Y="0.952032348633" />
                  <Point X="3.500603515625" Y="0.952797302246" />
                  <Point X="4.303609863281" Y="1.058515014648" />
                  <Point X="4.704703613281" Y="1.111319946289" />
                  <Point X="4.75268359375" Y="0.914233703613" />
                  <Point X="4.77969921875" Y="0.740717163086" />
                  <Point X="4.78387109375" Y="0.713920959473" />
                  <Point X="4.255661132813" Y="0.572387451172" />
                  <Point X="3.691991943359" Y="0.421352844238" />
                  <Point X="3.686031738281" Y="0.419544403076" />
                  <Point X="3.665626708984" Y="0.412138092041" />
                  <Point X="3.642381103516" Y="0.401619445801" />
                  <Point X="3.634004150391" Y="0.397316375732" />
                  <Point X="3.59389453125" Y="0.374132080078" />
                  <Point X="3.541494140625" Y="0.343843780518" />
                  <Point X="3.533881835938" Y="0.338945770264" />
                  <Point X="3.508773925781" Y="0.319870452881" />
                  <Point X="3.481993652344" Y="0.294350708008" />
                  <Point X="3.472796875" Y="0.284226531982" />
                  <Point X="3.448731201172" Y="0.253561309814" />
                  <Point X="3.417291015625" Y="0.213498962402" />
                  <Point X="3.410854248047" Y="0.204207794189" />
                  <Point X="3.399130126953" Y="0.184927536011" />
                  <Point X="3.393842529297" Y="0.174938156128" />
                  <Point X="3.384068847656" Y="0.153474624634" />
                  <Point X="3.380005371094" Y="0.142928634644" />
                  <Point X="3.373159179688" Y="0.121427955627" />
                  <Point X="3.370376464844" Y="0.110473114014" />
                  <Point X="3.362354492188" Y="0.068585525513" />
                  <Point X="3.351874267578" Y="0.013862573624" />
                  <Point X="3.350603515625" Y="0.00496778965" />
                  <Point X="3.348584472656" Y="-0.02626219368" />
                  <Point X="3.350280029297" Y="-0.062940692902" />
                  <Point X="3.351874267578" Y="-0.076422721863" />
                  <Point X="3.359896240234" Y="-0.118310157776" />
                  <Point X="3.370376464844" Y="-0.17303326416" />
                  <Point X="3.373159179688" Y="-0.183987808228" />
                  <Point X="3.380005371094" Y="-0.205488632202" />
                  <Point X="3.384068847656" Y="-0.21603477478" />
                  <Point X="3.393842529297" Y="-0.237498291016" />
                  <Point X="3.399130126953" Y="-0.247487686157" />
                  <Point X="3.410854248047" Y="-0.266767944336" />
                  <Point X="3.417290771484" Y="-0.276058807373" />
                  <Point X="3.441356445312" Y="-0.306724334717" />
                  <Point X="3.472796630859" Y="-0.346786529541" />
                  <Point X="3.478718017578" Y="-0.353633270264" />
                  <Point X="3.501139648438" Y="-0.375805450439" />
                  <Point X="3.530176269531" Y="-0.398724731445" />
                  <Point X="3.541494140625" Y="-0.406403778076" />
                  <Point X="3.581603759766" Y="-0.429588195801" />
                  <Point X="3.634004150391" Y="-0.459876373291" />
                  <Point X="3.639495605469" Y="-0.462814788818" />
                  <Point X="3.659158447266" Y="-0.472016937256" />
                  <Point X="3.683028076172" Y="-0.481027770996" />
                  <Point X="3.691991943359" Y="-0.483912841797" />
                  <Point X="4.428385253906" Y="-0.681228942871" />
                  <Point X="4.784876953125" Y="-0.776750488281" />
                  <Point X="4.76161328125" Y="-0.93105291748" />
                  <Point X="4.727801757812" Y="-1.079219726563" />
                  <Point X="4.090479736328" Y="-0.99531463623" />
                  <Point X="3.436781982422" Y="-0.90925378418" />
                  <Point X="3.428624511719" Y="-0.908535766602" />
                  <Point X="3.400098388672" Y="-0.908042480469" />
                  <Point X="3.366721435547" Y="-0.910840942383" />
                  <Point X="3.354480957031" Y="-0.912676269531" />
                  <Point X="3.275760009766" Y="-0.929786621094" />
                  <Point X="3.172916748047" Y="-0.952139953613" />
                  <Point X="3.157874023438" Y="-0.956742370605" />
                  <Point X="3.128753662109" Y="-0.968366882324" />
                  <Point X="3.114676269531" Y="-0.975389038086" />
                  <Point X="3.086849609375" Y="-0.99228137207" />
                  <Point X="3.074124023438" Y="-1.001530334473" />
                  <Point X="3.050374023438" Y="-1.022001220703" />
                  <Point X="3.039349609375" Y="-1.03322277832" />
                  <Point X="2.991767578125" Y="-1.090448974609" />
                  <Point X="2.92960546875" Y="-1.16521081543" />
                  <Point X="2.921326171875" Y="-1.176847412109" />
                  <Point X="2.90660546875" Y="-1.201229858398" />
                  <Point X="2.9001640625" Y="-1.213975830078" />
                  <Point X="2.888820800781" Y="-1.241360839844" />
                  <Point X="2.884362792969" Y="-1.254928100586" />
                  <Point X="2.877531005859" Y="-1.282577880859" />
                  <Point X="2.875157226562" Y="-1.296660400391" />
                  <Point X="2.868337646484" Y="-1.370770874023" />
                  <Point X="2.859428222656" Y="-1.467590698242" />
                  <Point X="2.859288574219" Y="-1.483320922852" />
                  <Point X="2.861607177734" Y="-1.514589355469" />
                  <Point X="2.864065429688" Y="-1.530127441406" />
                  <Point X="2.871796875" Y="-1.561748535156" />
                  <Point X="2.876785888672" Y="-1.576668334961" />
                  <Point X="2.889157470703" Y="-1.605479858398" />
                  <Point X="2.896540039062" Y="-1.619371704102" />
                  <Point X="2.94010546875" Y="-1.687134643555" />
                  <Point X="2.997020263672" Y="-1.775661987305" />
                  <Point X="3.001741943359" Y="-1.782353271484" />
                  <Point X="3.019793701172" Y="-1.804450073242" />
                  <Point X="3.043489257812" Y="-1.828120361328" />
                  <Point X="3.052796142578" Y="-1.836277709961" />
                  <Point X="3.736173583984" Y="-2.360652099609" />
                  <Point X="4.087170410156" Y="-2.629981933594" />
                  <Point X="4.04548828125" Y="-2.697430175781" />
                  <Point X="4.001274414063" Y="-2.760251708984" />
                  <Point X="3.430660644531" Y="-2.430807617188" />
                  <Point X="2.848454589844" Y="-2.094670654297" />
                  <Point X="2.841191894531" Y="-2.090885253906" />
                  <Point X="2.815026123047" Y="-2.079513183594" />
                  <Point X="2.783119140625" Y="-2.069325927734" />
                  <Point X="2.771108398438" Y="-2.066337646484" />
                  <Point X="2.67741796875" Y="-2.049417236328" />
                  <Point X="2.555018066406" Y="-2.027311767578" />
                  <Point X="2.539359130859" Y="-2.025807495117" />
                  <Point X="2.508006835938" Y="-2.025403198242" />
                  <Point X="2.492313476562" Y="-2.026503662109" />
                  <Point X="2.460140625" Y="-2.031461425781" />
                  <Point X="2.444844482422" Y="-2.035136230469" />
                  <Point X="2.415068603516" Y="-2.044959838867" />
                  <Point X="2.400588867188" Y="-2.051108642578" />
                  <Point X="2.322754882812" Y="-2.092072021484" />
                  <Point X="2.221070800781" Y="-2.145587646484" />
                  <Point X="2.208968505859" Y="-2.153170166016" />
                  <Point X="2.186037353516" Y="-2.170063476562" />
                  <Point X="2.175208496094" Y="-2.179374267578" />
                  <Point X="2.154248779297" Y="-2.200334228516" />
                  <Point X="2.144938476562" Y="-2.211162841797" />
                  <Point X="2.128045898438" Y="-2.234093261719" />
                  <Point X="2.120463623047" Y="-2.246195068359" />
                  <Point X="2.079500244141" Y="-2.324028808594" />
                  <Point X="2.025984741211" Y="-2.425713134766" />
                  <Point X="2.0198359375" Y="-2.440192871094" />
                  <Point X="2.010012084961" Y="-2.46996875" />
                  <Point X="2.006337280273" Y="-2.485264892578" />
                  <Point X="2.001379516602" Y="-2.5174375" />
                  <Point X="2.000279174805" Y="-2.533130859375" />
                  <Point X="2.00068347168" Y="-2.564483154297" />
                  <Point X="2.002187866211" Y="-2.580142089844" />
                  <Point X="2.019108276367" Y="-2.673832519531" />
                  <Point X="2.041213378906" Y="-2.796232421875" />
                  <Point X="2.043015014648" Y="-2.804221435547" />
                  <Point X="2.051236083984" Y="-2.83154296875" />
                  <Point X="2.064069580078" Y="-2.862479736328" />
                  <Point X="2.069546630859" Y="-2.873578613281" />
                  <Point X="2.508685302734" Y="-3.634188964844" />
                  <Point X="2.735893066406" Y="-4.027724365234" />
                  <Point X="2.723754150391" Y="-4.036083251953" />
                  <Point X="2.278191894531" Y="-3.455415527344" />
                  <Point X="1.833914550781" Y="-2.876422851562" />
                  <Point X="1.828653442383" Y="-2.870146240234" />
                  <Point X="1.808831665039" Y="-2.849626953125" />
                  <Point X="1.783252319336" Y="-2.828004882812" />
                  <Point X="1.773299072266" Y="-2.820647216797" />
                  <Point X="1.680894775391" Y="-2.761239746094" />
                  <Point X="1.56017565918" Y="-2.683628417969" />
                  <Point X="1.546283935547" Y="-2.676245849609" />
                  <Point X="1.517472900391" Y="-2.663874511719" />
                  <Point X="1.502553344727" Y="-2.658885742188" />
                  <Point X="1.470932006836" Y="-2.651154052734" />
                  <Point X="1.455394042969" Y="-2.648695800781" />
                  <Point X="1.42412512207" Y="-2.646376953125" />
                  <Point X="1.408394287109" Y="-2.646516357422" />
                  <Point X="1.307334594727" Y="-2.655815917969" />
                  <Point X="1.175307373047" Y="-2.667965332031" />
                  <Point X="1.161225097656" Y="-2.670339111328" />
                  <Point X="1.133575317383" Y="-2.677170898438" />
                  <Point X="1.120007568359" Y="-2.68162890625" />
                  <Point X="1.092622680664" Y="-2.692972167969" />
                  <Point X="1.079876708984" Y="-2.699413574219" />
                  <Point X="1.055494873047" Y="-2.714134033203" />
                  <Point X="1.043858764648" Y="-2.722413085938" />
                  <Point X="0.965823242188" Y="-2.787297119141" />
                  <Point X="0.863875061035" Y="-2.872063476562" />
                  <Point X="0.852653320312" Y="-2.883087890625" />
                  <Point X="0.832182128906" Y="-2.906838134766" />
                  <Point X="0.822933044434" Y="-2.919564208984" />
                  <Point X="0.80604083252" Y="-2.947390869141" />
                  <Point X="0.799018676758" Y="-2.961468261719" />
                  <Point X="0.787394287109" Y="-2.990588623047" />
                  <Point X="0.782791931152" Y="-3.005631347656" />
                  <Point X="0.759459594727" Y="-3.112978271484" />
                  <Point X="0.728977661133" Y="-3.253219238281" />
                  <Point X="0.727584716797" Y="-3.261289306641" />
                  <Point X="0.724724487305" Y="-3.289677734375" />
                  <Point X="0.7247421875" Y="-3.323170410156" />
                  <Point X="0.725554992676" Y="-3.335520019531" />
                  <Point X="0.833091430664" Y="-4.152340332031" />
                  <Point X="0.828745361328" Y="-4.136120605469" />
                  <Point X="0.655064941406" Y="-3.487936523438" />
                  <Point X="0.652606262207" Y="-3.480124511719" />
                  <Point X="0.642145141602" Y="-3.453580322266" />
                  <Point X="0.62678692627" Y="-3.423815673828" />
                  <Point X="0.620407531738" Y="-3.413210205078" />
                  <Point X="0.549420227051" Y="-3.310930664062" />
                  <Point X="0.456679962158" Y="-3.177309814453" />
                  <Point X="0.446670654297" Y="-3.165172851562" />
                  <Point X="0.424786804199" Y="-3.142717529297" />
                  <Point X="0.41291229248" Y="-3.132399169922" />
                  <Point X="0.386657165527" Y="-3.113155273438" />
                  <Point X="0.37324230957" Y="-3.104937744141" />
                  <Point X="0.34524130249" Y="-3.090829589844" />
                  <Point X="0.330654876709" Y="-3.084938720703" />
                  <Point X="0.220805587769" Y="-3.050845458984" />
                  <Point X="0.077295753479" Y="-3.006305419922" />
                  <Point X="0.063377220154" Y="-3.003109619141" />
                  <Point X="0.035217639923" Y="-2.998840087891" />
                  <Point X="0.02097659111" Y="-2.997766601562" />
                  <Point X="-0.008664761543" Y="-2.997766601562" />
                  <Point X="-0.022905065536" Y="-2.998840087891" />
                  <Point X="-0.051064498901" Y="-3.003109375" />
                  <Point X="-0.064983482361" Y="-3.006305175781" />
                  <Point X="-0.174832611084" Y="-3.040398193359" />
                  <Point X="-0.318342590332" Y="-3.084938476562" />
                  <Point X="-0.332929473877" Y="-3.090829589844" />
                  <Point X="-0.360930786133" Y="-3.104937988281" />
                  <Point X="-0.374345336914" Y="-3.113155273438" />
                  <Point X="-0.400600463867" Y="-3.132399169922" />
                  <Point X="-0.412475128174" Y="-3.142717773438" />
                  <Point X="-0.434358978271" Y="-3.165173095703" />
                  <Point X="-0.444367980957" Y="-3.177309814453" />
                  <Point X="-0.51535546875" Y="-3.279589355469" />
                  <Point X="-0.608095581055" Y="-3.413210205078" />
                  <Point X="-0.612470458984" Y="-3.420132324219" />
                  <Point X="-0.62597668457" Y="-3.445265136719" />
                  <Point X="-0.638777893066" Y="-3.476215820312" />
                  <Point X="-0.642753112793" Y="-3.487936523438" />
                  <Point X="-0.869653747559" Y="-4.3347421875" />
                  <Point X="-0.985425231934" Y="-4.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.121062020799" Y="-2.397178012835" />
                  <Point X="-3.727896792471" Y="-2.98006943453" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.045552639247" Y="-2.339237567933" />
                  <Point X="-3.645423464023" Y="-2.932453465483" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.673116546341" Y="-1.23894810728" />
                  <Point X="-4.642543178655" Y="-1.284274988888" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.970043257696" Y="-2.281297123031" />
                  <Point X="-3.562950135576" Y="-2.884837496435" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.954815985187" Y="-3.786433451421" />
                  <Point X="-2.863362178265" Y="-3.922019295985" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.741732679384" Y="-0.967332799874" />
                  <Point X="-4.537298283309" Y="-1.270419256115" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.894533876144" Y="-2.223356678128" />
                  <Point X="-3.480476807128" Y="-2.837221527388" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.901967396606" Y="-3.694896999345" />
                  <Point X="-2.646372024941" Y="-4.073832721108" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.772568758841" Y="-0.751728725302" />
                  <Point X="-4.432053387964" Y="-1.256563523343" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.819024494592" Y="-2.165416233226" />
                  <Point X="-3.398003482218" Y="-2.789605553096" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.849118808024" Y="-3.60336054727" />
                  <Point X="-2.486590869549" Y="-4.140830318849" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.733098460971" Y="-0.640358141592" />
                  <Point X="-4.326808492618" Y="-1.24270779057" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.743515113041" Y="-2.107475788324" />
                  <Point X="-3.315530169097" Y="-2.741989561325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.796270219443" Y="-3.511824095195" />
                  <Point X="-2.425607088272" Y="-4.061354785935" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.636048071912" Y="-0.614353553643" />
                  <Point X="-4.221563597273" Y="-1.228852057797" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.668005731489" Y="-2.049535343421" />
                  <Point X="-3.233056855977" Y="-2.694373569554" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.743421630861" Y="-3.420287643119" />
                  <Point X="-2.358075684635" Y="-3.991586502369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.538997682853" Y="-0.588348965693" />
                  <Point X="-4.116318717904" Y="-1.214996301337" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.592496342038" Y="-1.99159491023" />
                  <Point X="-3.150583542857" Y="-2.646757577783" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.69057304228" Y="-3.328751191044" />
                  <Point X="-2.27921765071" Y="-3.938610638773" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.441947293794" Y="-0.562344377744" />
                  <Point X="-4.011073856181" Y="-1.201140518718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.516986931191" Y="-1.93365450876" />
                  <Point X="-3.068110229736" Y="-2.599141586011" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.637724448313" Y="-3.237214746953" />
                  <Point X="-2.200227403991" Y="-3.885830788705" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.344896904735" Y="-0.536339789794" />
                  <Point X="-3.905828994457" Y="-1.187284736098" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.441477520344" Y="-1.87571410729" />
                  <Point X="-2.985636916616" Y="-2.55152559424" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.584875849002" Y="-3.145678310784" />
                  <Point X="-2.121237065321" Y="-3.833051074959" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.247846515676" Y="-0.510335201844" />
                  <Point X="-3.800584132734" Y="-1.173428953479" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.365968109497" Y="-1.81777370582" />
                  <Point X="-2.903163603496" Y="-2.503909602469" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.532027249692" Y="-3.054141874615" />
                  <Point X="-2.036049083673" Y="-3.789459744789" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.150796126366" Y="-0.484330614267" />
                  <Point X="-3.69533927101" Y="-1.159573170859" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.29045869865" Y="-1.759833304351" />
                  <Point X="-2.820690290376" Y="-2.456293610698" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.479178650381" Y="-2.962605438446" />
                  <Point X="-1.929889188773" Y="-3.776960554642" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.781242611835" Y="0.620232444573" />
                  <Point X="-4.756926934892" Y="0.584182971014" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.053745707193" Y="-0.458326070964" />
                  <Point X="-3.590094409287" Y="-1.14571738824" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.214949287802" Y="-1.701892902881" />
                  <Point X="-2.738216977255" Y="-2.408677618926" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.426330051071" Y="-2.871069002277" />
                  <Point X="-1.809998169066" Y="-3.784818594188" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.760626362149" Y="0.759555304217" />
                  <Point X="-4.617056977456" Y="0.546704938198" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.956695288019" Y="-0.432321527661" />
                  <Point X="-3.484849547563" Y="-1.131861605621" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.139439876955" Y="-1.643952501411" />
                  <Point X="-2.653127923206" Y="-2.364939622561" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.373481451761" Y="-2.779532566108" />
                  <Point X="-1.690107126025" Y="-3.792676668326" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.12748480122" Y="-4.626798567097" />
                  <Point X="-1.040222242657" Y="-4.756170630434" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.740010140597" Y="0.898878205575" />
                  <Point X="-4.477187020019" Y="0.509226905382" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.859644868846" Y="-0.406316984358" />
                  <Point X="-3.37960468584" Y="-1.118005823001" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.063930466108" Y="-1.586012099941" />
                  <Point X="-2.549829136071" Y="-2.348198665715" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.324422518398" Y="-2.68237771912" />
                  <Point X="-1.558696681856" Y="-3.81761295696" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.133102245206" Y="-4.448582657153" />
                  <Point X="-0.966385673093" Y="-4.695750139772" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.712314632532" Y="1.027705633062" />
                  <Point X="-4.337317063428" Y="0.47174887382" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.762594449673" Y="-0.380312441055" />
                  <Point X="-3.274359824116" Y="-1.104150040382" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.996982812367" Y="-1.515378371563" />
                  <Point X="-2.361330299447" Y="-2.457771976756" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.346976626018" Y="-2.479052172736" />
                  <Point X="-1.309372355446" Y="-4.017363765049" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.181028593174" Y="-4.207641217546" />
                  <Point X="-0.933806473468" Y="-4.574163082775" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.679472515766" Y="1.148902899368" />
                  <Point X="-4.19744711635" Y="0.434270856362" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.665544030499" Y="-0.354307897752" />
                  <Point X="-3.157672094822" Y="-1.107259006591" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.953498297967" Y="-1.4099591086" />
                  <Point X="-0.901227273842" Y="-4.452576025777" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.64663049863" Y="1.270100313383" />
                  <Point X="-4.057577169272" Y="0.396792838903" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.568493611326" Y="-0.328303354449" />
                  <Point X="-0.868648076053" Y="-4.330988966059" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.557776554638" Y="1.308256630868" />
                  <Point X="-3.917707222194" Y="0.359314821445" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.473793053857" Y="-0.298814997901" />
                  <Point X="-0.836068935894" Y="-4.209401820898" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.4320184455" Y="1.291700273533" />
                  <Point X="-3.777837275116" Y="0.321836803986" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.393896269208" Y="-0.247379145584" />
                  <Point X="-0.803489795736" Y="-4.087814675737" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.306260336362" Y="1.275143916198" />
                  <Point X="-3.637967328038" Y="0.284358786528" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.330650972395" Y="-0.171256447334" />
                  <Point X="-0.770910655578" Y="-3.966227530576" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.18050219963" Y="1.258587517954" />
                  <Point X="-3.49745564111" Y="0.245929350616" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.295344633504" Y="-0.053712540568" />
                  <Point X="-0.738331515419" Y="-3.844640385415" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.054744031386" Y="1.242031072991" />
                  <Point X="-0.705752375261" Y="-3.723053240254" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.928985863142" Y="1.225474628028" />
                  <Point X="-0.673173235103" Y="-3.601466095093" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.803227694897" Y="1.208918183065" />
                  <Point X="-0.640212277174" Y="-3.480445018056" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.690045603934" Y="1.211006539416" />
                  <Point X="-0.58926870596" Y="-3.386084261588" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.595734165732" Y="1.241071789" />
                  <Point X="-0.531155066473" Y="-3.302353568481" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.210880420136" Y="2.322951322453" />
                  <Point X="-4.152843537358" Y="2.236908105313" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.516234639117" Y="1.293096600572" />
                  <Point X="-0.473041443894" Y="-3.218622850309" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.157720890962" Y="2.414026786143" />
                  <Point X="-3.915315574221" Y="2.054646124982" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.460028974886" Y="1.379655983322" />
                  <Point X="-0.410693568513" Y="-3.141169670072" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.104561259853" Y="2.505102098707" />
                  <Point X="-3.677788475128" Y="1.87238542565" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.423199101357" Y="1.494941157099" />
                  <Point X="-0.330672497995" Y="-3.089918079133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.051401628744" Y="2.596177411271" />
                  <Point X="-0.236560979692" Y="-3.059556436109" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.997615731148" Y="2.686324245586" />
                  <Point X="-0.141806436291" Y="-3.030148116997" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.93623829543" Y="2.765216161791" />
                  <Point X="-0.045974147936" Y="-3.002337620488" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.874860859712" Y="2.844108077995" />
                  <Point X="0.070192825034" Y="-3.004674533697" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.825702142375" Y="-4.124763158934" />
                  <Point X="0.830372376476" Y="-4.131687065726" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.813483457823" Y="2.923000044353" />
                  <Point X="0.214634303487" Y="-3.048930125137" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.75017935762" Y="-3.842908319276" />
                  <Point X="0.802582036774" Y="-3.920598486035" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.75210613106" Y="3.00189212209" />
                  <Point X="0.363790426457" Y="-3.100175464521" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.674656572865" Y="-3.561053479617" />
                  <Point X="0.774791697072" Y="-3.709509906344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.617452064656" Y="2.972146965735" />
                  <Point X="0.74700135737" Y="-3.498421326652" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.429774507002" Y="2.863791250838" />
                  <Point X="0.724727570263" Y="-3.29551137252" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.243094034782" Y="2.75691377589" />
                  <Point X="0.748916434982" Y="-3.161485132477" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.108374983047" Y="2.72707227482" />
                  <Point X="0.776843132207" Y="-3.033000457015" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.995468539266" Y="2.729569294923" />
                  <Point X="0.819137470295" Y="-2.925816685107" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.9078403564" Y="2.769542878011" />
                  <Point X="0.885399577304" Y="-2.854166591902" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.837221892421" Y="2.834734406407" />
                  <Point X="0.958816081575" Y="-2.793123328831" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.775603671338" Y="2.913269343628" />
                  <Point X="1.032232505826" Y="-2.732079947127" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.748763454926" Y="3.043364793136" />
                  <Point X="1.114368942736" Y="-2.683964515835" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.95459230141" Y="3.518406313875" />
                  <Point X="1.215663190685" Y="-2.664251707431" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.995480325565" Y="3.748913009314" />
                  <Point X="1.323556983149" Y="-2.654323126137" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.919949552198" Y="3.806821739546" />
                  <Point X="1.433244059356" Y="-2.647053197324" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.844418793109" Y="3.864730490945" />
                  <Point X="1.581945485743" Y="-2.6976244213" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.76888803402" Y="3.922639242345" />
                  <Point X="1.786105934053" Y="-2.830417026531" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.693357274931" Y="3.980547993745" />
                  <Point X="2.642528454164" Y="-3.930227920656" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.614662442322" Y="4.033765813243" />
                  <Point X="2.218976208448" Y="-3.132398186284" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.531308174424" Y="4.080075735845" />
                  <Point X="2.017224366163" Y="-2.663401072838" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.447953906525" Y="4.126385658446" />
                  <Point X="2.007735384452" Y="-2.479445372176" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.364599633528" Y="4.17269557349" />
                  <Point X="2.05232492808" Y="-2.375664382416" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.281245339758" Y="4.219005457736" />
                  <Point X="2.102548225414" Y="-2.280235776006" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.197891045988" Y="4.265315341982" />
                  <Point X="2.159621003227" Y="-2.194961942009" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.94096472352" Y="4.054294111255" />
                  <Point X="2.235711157757" Y="-2.137882528455" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.810256588417" Y="4.030399038631" />
                  <Point X="2.320280621609" Y="-2.093374207943" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.714188602821" Y="4.05786009961" />
                  <Point X="2.405077241173" Y="-2.049202659624" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.625449895702" Y="4.096187262786" />
                  <Point X="2.503813352338" Y="-2.025697257472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.550562626148" Y="4.155050026655" />
                  <Point X="2.628436653067" Y="-2.040571192153" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.500611844737" Y="4.250882654535" />
                  <Point X="2.758922634265" Y="-2.064136908068" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.466084459691" Y="4.369581407868" />
                  <Point X="2.923227113296" Y="-2.137840608883" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.478670101552" Y="4.558128096003" />
                  <Point X="3.110904691746" Y="-2.246196354611" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.384966101617" Y="4.589093909853" />
                  <Point X="3.298582270196" Y="-2.354552100339" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.288599276313" Y="4.616111922744" />
                  <Point X="2.86803081857" Y="-1.546345616475" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.075175182846" Y="-1.853449765798" />
                  <Point X="3.486259842258" Y="-2.462907836597" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.192232417985" Y="4.643129886677" />
                  <Point X="2.867840311839" Y="-1.376175471883" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.31270284914" Y="-2.03571130604" />
                  <Point X="3.673937399147" Y="-2.571263550358" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.095865559658" Y="4.670147850609" />
                  <Point X="2.889839136547" Y="-1.238902364002" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.550230515435" Y="-2.217972846283" />
                  <Point X="3.861614956036" Y="-2.67961926412" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.99949870133" Y="4.697165814542" />
                  <Point X="2.943473865324" Y="-1.148531412695" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.787758237761" Y="-2.400234469597" />
                  <Point X="4.016245220018" Y="-2.738980351303" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.900268009692" Y="4.719937970989" />
                  <Point X="3.00674084135" Y="-1.072440855201" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.025286162063" Y="-2.582496392351" />
                  <Point X="4.072895772681" Y="-2.653080542779" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.794061349765" Y="4.732367829131" />
                  <Point X="3.073730552581" Y="-1.001869479616" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.687854689837" Y="4.744797687274" />
                  <Point X="3.157881164598" Y="-0.956740185722" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.581648046347" Y="4.757227569786" />
                  <Point X="3.25703645913" Y="-0.933856248469" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.475441455072" Y="4.76965752971" />
                  <Point X="-0.286845880384" Y="4.490053091843" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.014260782924" Y="4.085929065751" />
                  <Point X="3.357078395484" Y="-0.912286811774" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.369234863796" Y="4.782087489633" />
                  <Point X="-0.362368612822" Y="4.771907853938" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.089772937593" Y="4.10158043905" />
                  <Point X="3.472823837365" Y="-0.913998779443" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.170784649349" Y="4.151363343956" />
                  <Point X="3.598581942451" Y="-0.930555130771" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.227944450566" Y="4.23650816045" />
                  <Point X="3.724340047538" Y="-0.947111482099" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.26286840245" Y="4.354618979269" />
                  <Point X="3.850098152624" Y="-0.963667833427" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.295447560195" Y="4.476206098356" />
                  <Point X="3.349775233987" Y="-0.052020895855" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.619045031863" Y="-0.451229788186" />
                  <Point X="3.975856257711" Y="-0.980224184755" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.32802671794" Y="4.597793217444" />
                  <Point X="3.366923555544" Y="0.092443378675" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.769730797767" Y="-0.504742916478" />
                  <Point X="4.101614364635" Y="-0.996780538808" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.360605876317" Y="4.719380335594" />
                  <Point X="2.050183115056" Y="2.214479068152" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.116916394198" Y="2.115542913195" />
                  <Point X="3.408613597759" Y="0.200523056059" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.909600779801" Y="-0.542220985761" />
                  <Point X="4.227372490478" Y="-1.013336920908" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.434940082913" Y="4.779063049018" />
                  <Point X="2.015726113355" Y="2.435451380713" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.317098155774" Y="1.988648953622" />
                  <Point X="3.469542588247" Y="0.280079819657" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.049470761835" Y="-0.579699055045" />
                  <Point X="4.35313061632" Y="-1.029893303009" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.558240642208" Y="4.766150159157" />
                  <Point X="2.047467729096" Y="2.558280206886" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.440586108275" Y="1.97545824191" />
                  <Point X="3.541235994447" Y="0.343677680673" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.189340743869" Y="-0.617177124328" />
                  <Point X="4.478888742163" Y="-1.04644968511" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.681541167285" Y="4.753237320029" />
                  <Point X="2.097387063482" Y="2.654159456898" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.542436783444" Y="1.994346113035" />
                  <Point X="3.017647076456" Y="1.28981788078" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.192515177158" Y="1.030565260041" />
                  <Point X="3.623674344694" Y="0.391345507036" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.329210725903" Y="-0.654655193611" />
                  <Point X="4.604646868006" Y="-1.06300606721" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.804841692361" Y="4.740324480901" />
                  <Point X="2.15023564689" Y="2.745695916644" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.634830167311" Y="2.027254995112" />
                  <Point X="3.010076687888" Y="1.470929150135" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.351889142739" Y="0.964171346021" />
                  <Point X="3.714040011953" Y="0.427260602611" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.469080691971" Y="-0.692133239222" />
                  <Point X="4.728401401983" Y="-1.076592002214" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.939185993024" Y="4.711038571143" />
                  <Point X="2.203084230298" Y="2.837232376389" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.717341076363" Y="2.074815248622" />
                  <Point X="3.047772700977" Y="1.584930219208" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.475014656579" Y="0.951517971721" />
                  <Point X="3.811090418897" Y="0.453265164045" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.608950619128" Y="-0.729611227147" />
                  <Point X="4.757369242798" Y="-0.949650885601" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.076067484484" Y="4.67799112134" />
                  <Point X="2.255932813706" Y="2.928768836135" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.799814395303" Y="2.122431231766" />
                  <Point X="3.105469820455" Y="1.669278428622" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.581553986715" Y="0.963454625998" />
                  <Point X="3.908140825841" Y="0.479269725479" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.748820546285" Y="-0.767089215071" />
                  <Point X="4.779480428384" Y="-0.81254435957" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.212949028184" Y="4.644943594088" />
                  <Point X="2.308781397113" Y="3.02030529588" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.882287714243" Y="2.170047214909" />
                  <Point X="3.170911292023" Y="1.7421451639" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.686798875726" Y="0.977310368161" />
                  <Point X="4.005191232785" Y="0.505274286913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.349830580799" Y="4.611896053619" />
                  <Point X="2.361629980521" Y="3.111841755626" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.964761033182" Y="2.217663198053" />
                  <Point X="3.246372241635" Y="1.800157412106" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.792043764738" Y="0.991166110324" />
                  <Point X="4.102241639729" Y="0.531278848347" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.490298424092" Y="4.573531618569" />
                  <Point X="2.414478563929" Y="3.203378215372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.047234352122" Y="2.265279181197" />
                  <Point X="3.321881651859" Y="1.858097814499" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.89728865375" Y="1.005021852487" />
                  <Point X="4.199292046673" Y="0.557283409781" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.642003594354" Y="4.518507161164" />
                  <Point X="2.467327144728" Y="3.294914678986" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.129707671062" Y="2.31289516434" />
                  <Point X="3.397391062084" Y="1.916038216892" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.002533542762" Y="1.01887759465" />
                  <Point X="4.296342446274" Y="0.583287982101" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.793709033463" Y="4.463482305177" />
                  <Point X="2.520175723287" Y="3.38645114592" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.212180990001" Y="2.360511147484" />
                  <Point X="3.472900472308" Y="1.973978619285" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.107778431773" Y="1.032733336813" />
                  <Point X="4.3933928357" Y="0.609292569506" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.954427870849" Y="4.395094536711" />
                  <Point X="2.573024301846" Y="3.477987612854" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.294654308941" Y="2.408127130628" />
                  <Point X="3.548409882533" Y="2.031919021678" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.213023320785" Y="1.046589078975" />
                  <Point X="4.490443225126" Y="0.635297156911" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.12182236328" Y="4.316809702637" />
                  <Point X="2.625872880406" Y="3.569524079787" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.37712762788" Y="2.455743113771" />
                  <Point X="3.623919292757" Y="2.089859424071" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.318268210316" Y="1.060444820369" />
                  <Point X="4.587493614552" Y="0.661301744317" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.294167678046" Y="4.231184972606" />
                  <Point X="2.678721458965" Y="3.661060546721" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.45960094682" Y="2.503359096915" />
                  <Point X="3.699428677649" Y="2.147799864021" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.423513103055" Y="1.074300557005" />
                  <Point X="4.684544003978" Y="0.687306331722" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.482940357291" Y="4.121205673183" />
                  <Point X="2.731570037524" Y="3.752597013655" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.542074265151" Y="2.550975080961" />
                  <Point X="3.774938032634" Y="2.20574034831" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.528757995795" Y="1.088156293642" />
                  <Point X="4.781594393404" Y="0.713310919127" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.678758544308" Y="4.000780978933" />
                  <Point X="2.784418616084" Y="3.844133480589" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.624547583264" Y="2.59859106533" />
                  <Point X="3.850447387619" Y="2.263680832599" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.634002888534" Y="1.102012030279" />
                  <Point X="4.748178289188" Y="0.932740037705" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.707020901377" Y="2.6462070497" />
                  <Point X="3.925956742604" Y="2.321621316887" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.78949421949" Y="2.693823034069" />
                  <Point X="4.00146609759" Y="2.379561801176" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.871967537603" Y="2.741439018438" />
                  <Point X="4.076975452575" Y="2.437502285465" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="0.001626220703" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.645219482422" Y="-4.185296386719" />
                  <Point X="0.471539031982" Y="-3.537112304688" />
                  <Point X="0.464318664551" Y="-3.521544189453" />
                  <Point X="0.393331176758" Y="-3.419264648438" />
                  <Point X="0.300591033936" Y="-3.285643798828" />
                  <Point X="0.274335754395" Y="-3.266399902344" />
                  <Point X="0.16448664856" Y="-3.232306640625" />
                  <Point X="0.020976625443" Y="-3.187766601562" />
                  <Point X="-0.008664756775" Y="-3.187766601562" />
                  <Point X="-0.118513870239" Y="-3.221859619141" />
                  <Point X="-0.262023895264" Y="-3.266399902344" />
                  <Point X="-0.288279144287" Y="-3.285643798828" />
                  <Point X="-0.359266662598" Y="-3.387923339844" />
                  <Point X="-0.45200680542" Y="-3.521544189453" />
                  <Point X="-0.459227142334" Y="-3.537112304688" />
                  <Point X="-0.686127929688" Y="-4.38391796875" />
                  <Point X="-0.84774395752" Y="-4.987077148438" />
                  <Point X="-0.931940734863" Y="-4.970733886719" />
                  <Point X="-1.100246459961" Y="-4.938065429688" />
                  <Point X="-1.226992675781" Y="-4.905454589844" />
                  <Point X="-1.351589599609" Y="-4.873396972656" />
                  <Point X="-1.330057373047" Y="-4.709843261719" />
                  <Point X="-1.309150146484" Y="-4.551038574219" />
                  <Point X="-1.309683227539" Y="-4.5347578125" />
                  <Point X="-1.335926147461" Y="-4.402825683594" />
                  <Point X="-1.370210693359" Y="-4.230466308594" />
                  <Point X="-1.386282714844" Y="-4.20262890625" />
                  <Point X="-1.48741784668" Y="-4.113935546875" />
                  <Point X="-1.619543457031" Y="-3.998063964844" />
                  <Point X="-1.649240722656" Y="-3.985762939453" />
                  <Point X="-1.783469604492" Y="-3.976965087891" />
                  <Point X="-1.958829833984" Y="-3.965471435547" />
                  <Point X="-1.989878540039" Y="-3.973791015625" />
                  <Point X="-2.101725341797" Y="-4.048524658203" />
                  <Point X="-2.247844970703" Y="-4.146159179688" />
                  <Point X="-2.259734130859" Y="-4.157294433594" />
                  <Point X="-2.387121337891" Y="-4.32330859375" />
                  <Point X="-2.457095214844" Y="-4.414500488281" />
                  <Point X="-2.609133056641" Y="-4.320362304688" />
                  <Point X="-2.855831054688" Y="-4.16761328125" />
                  <Point X="-3.031341552734" Y="-4.032476074219" />
                  <Point X="-3.228581054688" Y="-3.880608154297" />
                  <Point X="-2.828799072266" Y="-3.188165527344" />
                  <Point X="-2.506033691406" Y="-2.629119628906" />
                  <Point X="-2.499762695312" Y="-2.597593261719" />
                  <Point X="-2.513979736328" Y="-2.568764404297" />
                  <Point X="-2.531328369141" Y="-2.551415771484" />
                  <Point X="-2.560157226562" Y="-2.537198730469" />
                  <Point X="-2.59168359375" Y="-2.543469726562" />
                  <Point X="-3.322036865234" Y="-2.965139404297" />
                  <Point X="-3.842959472656" Y="-3.265894042969" />
                  <Point X="-3.966796386719" Y="-3.103197753906" />
                  <Point X="-4.161703613281" Y="-2.847129394531" />
                  <Point X="-4.287528320312" Y="-2.636140380859" />
                  <Point X="-4.431020019531" Y="-2.395527099609" />
                  <Point X="-3.728521484375" Y="-1.856481079102" />
                  <Point X="-3.163786865234" Y="-1.423144897461" />
                  <Point X="-3.145822021484" Y="-1.396013305664" />
                  <Point X="-3.138117431641" Y="-1.366265869141" />
                  <Point X="-3.140326416016" Y="-1.334595581055" />
                  <Point X="-3.161158691406" Y="-1.310639282227" />
                  <Point X="-3.187641357422" Y="-1.295052734375" />
                  <Point X="-3.219529296875" Y="-1.288571044922" />
                  <Point X="-4.14153125" Y="-1.409954956055" />
                  <Point X="-4.803283691406" Y="-1.497076293945" />
                  <Point X="-4.851163574219" Y="-1.309627441406" />
                  <Point X="-4.927393066406" Y="-1.011191650391" />
                  <Point X="-4.960683105469" Y="-0.778430725098" />
                  <Point X="-4.998396484375" Y="-0.514742126465" />
                  <Point X="-4.200781738281" Y="-0.301021789551" />
                  <Point X="-3.557463867188" Y="-0.128645431519" />
                  <Point X="-3.529862792969" Y="-0.113073440552" />
                  <Point X="-3.50232421875" Y="-0.090645202637" />
                  <Point X="-3.490887939453" Y="-0.062984279633" />
                  <Point X="-3.483400878906" Y="-0.031280042648" />
                  <Point X="-3.489658935547" Y="-0.003536010981" />
                  <Point X="-3.50232421875" Y="0.028085115433" />
                  <Point X="-3.52617578125" Y="0.047954292297" />
                  <Point X="-3.557463867188" Y="0.066085334778" />
                  <Point X="-4.397912109375" Y="0.291282714844" />
                  <Point X="-4.998186523438" Y="0.452125793457" />
                  <Point X="-4.966773925781" Y="0.664410766602" />
                  <Point X="-4.917645019531" Y="0.996418151855" />
                  <Point X="-4.8506328125" Y="1.243714355469" />
                  <Point X="-4.773516113281" Y="1.528298583984" />
                  <Point X="-4.214412597656" Y="1.45469128418" />
                  <Point X="-3.753266357422" Y="1.393980102539" />
                  <Point X="-3.731704589844" Y="1.395866699219" />
                  <Point X="-3.705072021484" Y="1.404264038086" />
                  <Point X="-3.670278564453" Y="1.41523425293" />
                  <Point X="-3.651534423828" Y="1.426056274414" />
                  <Point X="-3.639119873047" Y="1.443786132813" />
                  <Point X="-3.628433349609" Y="1.469585327148" />
                  <Point X="-3.614472412109" Y="1.503290283203" />
                  <Point X="-3.610714111328" Y="1.524605224609" />
                  <Point X="-3.616315917969" Y="1.54551159668" />
                  <Point X="-3.629210205078" Y="1.57028137207" />
                  <Point X="-3.646055664062" Y="1.602641235352" />
                  <Point X="-3.659968261719" Y="1.619221679688" />
                  <Point X="-4.142051757812" Y="1.989136962891" />
                  <Point X="-4.47610546875" Y="2.245466064453" />
                  <Point X="-4.350911132813" Y="2.459954589844" />
                  <Point X="-4.160014648438" Y="2.787006591797" />
                  <Point X="-3.982501708984" Y="3.015174072266" />
                  <Point X="-3.774671142578" Y="3.282310791016" />
                  <Point X="-3.442379882813" Y="3.090462158203" />
                  <Point X="-3.159156982422" Y="2.926943359375" />
                  <Point X="-3.138514892578" Y="2.920434814453" />
                  <Point X="-3.101423095703" Y="2.917189697266" />
                  <Point X="-3.052965576172" Y="2.912950195312" />
                  <Point X="-3.031506591797" Y="2.915775390625" />
                  <Point X="-3.013252441406" Y="2.927404785156" />
                  <Point X="-2.986924316406" Y="2.953732666016" />
                  <Point X="-2.952528808594" Y="2.988128173828" />
                  <Point X="-2.940899414062" Y="3.006382324219" />
                  <Point X="-2.93807421875" Y="3.027841064453" />
                  <Point X="-2.941319335938" Y="3.064932861328" />
                  <Point X="-2.945558837891" Y="3.113390625" />
                  <Point X="-2.952067382812" Y="3.134032958984" />
                  <Point X="-3.165693115234" Y="3.504043457031" />
                  <Point X="-3.307278564453" Y="3.749276611328" />
                  <Point X="-3.085357177734" Y="3.919421630859" />
                  <Point X="-2.752876220703" Y="4.174331542969" />
                  <Point X="-2.473299316406" Y="4.329658203125" />
                  <Point X="-2.141548583984" Y="4.513971679688" />
                  <Point X="-2.054530029297" Y="4.400566894531" />
                  <Point X="-1.967826660156" Y="4.287573242188" />
                  <Point X="-1.951246826172" Y="4.273660644531" />
                  <Point X="-1.909963989258" Y="4.252169921875" />
                  <Point X="-1.856030761719" Y="4.22409375" />
                  <Point X="-1.835124145508" Y="4.2184921875" />
                  <Point X="-1.813808837891" Y="4.222250488281" />
                  <Point X="-1.770809936523" Y="4.240061523438" />
                  <Point X="-1.714634887695" Y="4.263330078125" />
                  <Point X="-1.696905273438" Y="4.275744628906" />
                  <Point X="-1.686083496094" Y="4.294488769531" />
                  <Point X="-1.672088012695" Y="4.338876464844" />
                  <Point X="-1.653804077148" Y="4.396865234375" />
                  <Point X="-1.651917602539" Y="4.418426757812" />
                  <Point X="-1.676204345703" Y="4.602903808594" />
                  <Point X="-1.689137573242" Y="4.701141113281" />
                  <Point X="-1.398511474609" Y="4.782622558594" />
                  <Point X="-0.968094177246" Y="4.903296386719" />
                  <Point X="-0.629163085938" Y="4.942963378906" />
                  <Point X="-0.224199615479" Y="4.990358398438" />
                  <Point X="-0.125925582886" Y="4.623594238281" />
                  <Point X="-0.042140380859" Y="4.310903320312" />
                  <Point X="-0.024282121658" Y="4.284176757813" />
                  <Point X="0.006155934334" Y="4.273844238281" />
                  <Point X="0.036594028473" Y="4.284176757813" />
                  <Point X="0.054452251434" Y="4.310903320312" />
                  <Point X="0.163911636353" Y="4.719411621094" />
                  <Point X="0.236648406982" Y="4.990868652344" />
                  <Point X="0.484387817383" Y="4.964923828125" />
                  <Point X="0.860205749512" Y="4.925565429688" />
                  <Point X="1.14061315918" Y="4.857866699219" />
                  <Point X="1.508455932617" Y="4.769058105469" />
                  <Point X="1.691107055664" Y="4.702809082031" />
                  <Point X="1.931033691406" Y="4.615786132813" />
                  <Point X="2.107520507812" Y="4.533249023438" />
                  <Point X="2.33869921875" Y="4.425134277344" />
                  <Point X="2.509195068359" Y="4.325803222656" />
                  <Point X="2.732523681641" Y="4.19569140625" />
                  <Point X="2.893321533203" Y="4.081340820312" />
                  <Point X="3.068740234375" Y="3.956592773438" />
                  <Point X="2.603430175781" Y="3.150651855469" />
                  <Point X="2.229853515625" Y="2.503597900391" />
                  <Point X="2.224852050781" Y="2.491514892578" />
                  <Point X="2.215543457031" Y="2.456705078125" />
                  <Point X="2.203382324219" Y="2.411228515625" />
                  <Point X="2.202044677734" Y="2.392325927734" />
                  <Point X="2.205674316406" Y="2.362225341797" />
                  <Point X="2.210416015625" Y="2.322901123047" />
                  <Point X="2.218682128906" Y="2.3008125" />
                  <Point X="2.237307373047" Y="2.273363769531" />
                  <Point X="2.261639892578" Y="2.23750390625" />
                  <Point X="2.274939941406" Y="2.224203613281" />
                  <Point X="2.302388671875" Y="2.205578369141" />
                  <Point X="2.338248535156" Y="2.18124609375" />
                  <Point X="2.360336914062" Y="2.172980224609" />
                  <Point X="2.3904375" Y="2.169350585938" />
                  <Point X="2.42976171875" Y="2.164608642578" />
                  <Point X="2.448664550781" Y="2.165946289062" />
                  <Point X="2.483474365234" Y="2.175254882813" />
                  <Point X="2.528950927734" Y="2.187416015625" />
                  <Point X="2.541033935547" Y="2.192417480469" />
                  <Point X="3.38636328125" Y="2.680468505859" />
                  <Point X="3.994248046875" Y="3.031430908203" />
                  <Point X="4.07012109375" Y="2.925984863281" />
                  <Point X="4.202591796875" Y="2.741880126953" />
                  <Point X="4.292234375" Y="2.593745117188" />
                  <Point X="4.387512695312" Y="2.436296142578" />
                  <Point X="3.78046484375" Y="1.970491455078" />
                  <Point X="3.288616210938" Y="1.593082763672" />
                  <Point X="3.279371337891" Y="1.583833374023" />
                  <Point X="3.254318603516" Y="1.551150268555" />
                  <Point X="3.221589111328" Y="1.508451904297" />
                  <Point X="3.213119384766" Y="1.4915" />
                  <Point X="3.203787109375" Y="1.458130249023" />
                  <Point X="3.191595458984" Y="1.41453527832" />
                  <Point X="3.190779541016" Y="1.390965209961" />
                  <Point X="3.198440185547" Y="1.353837280273" />
                  <Point X="3.208448486328" Y="1.305332397461" />
                  <Point X="3.215646484375" Y="1.287954956055" />
                  <Point X="3.236482910156" Y="1.256284545898" />
                  <Point X="3.263704101562" Y="1.214909667969" />
                  <Point X="3.280947998047" Y="1.198819824219" />
                  <Point X="3.311142578125" Y="1.181822753906" />
                  <Point X="3.350590087891" Y="1.159617431641" />
                  <Point X="3.368565429688" Y="1.153619628906" />
                  <Point X="3.409390625" Y="1.148223999023" />
                  <Point X="3.462726074219" Y="1.141175048828" />
                  <Point X="3.475803710938" Y="1.14117175293" />
                  <Point X="4.278810058594" Y="1.246889526367" />
                  <Point X="4.848975097656" Y="1.32195324707" />
                  <Point X="4.88229296875" Y="1.185094238281" />
                  <Point X="4.939188476562" Y="0.951385559082" />
                  <Point X="4.9674375" Y="0.76994708252" />
                  <Point X="4.997858398438" Y="0.574556152344" />
                  <Point X="4.304836914062" Y="0.388861663818" />
                  <Point X="3.741167724609" Y="0.237826919556" />
                  <Point X="3.729086914062" Y="0.232819412231" />
                  <Point X="3.688977294922" Y="0.20963520813" />
                  <Point X="3.636576904297" Y="0.179346878052" />
                  <Point X="3.622264892578" Y="0.166926574707" />
                  <Point X="3.59819921875" Y="0.136261154175" />
                  <Point X="3.566759033203" Y="0.096198890686" />
                  <Point X="3.556985351562" Y="0.074735275269" />
                  <Point X="3.548963378906" Y="0.032847774506" />
                  <Point X="3.538483154297" Y="-0.021875303268" />
                  <Point X="3.538483154297" Y="-0.04068478775" />
                  <Point X="3.546505126953" Y="-0.082572296143" />
                  <Point X="3.556985351562" Y="-0.13729536438" />
                  <Point X="3.566759033203" Y="-0.158758987427" />
                  <Point X="3.590824707031" Y="-0.189424407959" />
                  <Point X="3.622264892578" Y="-0.229486679077" />
                  <Point X="3.636576904297" Y="-0.241906967163" />
                  <Point X="3.676686523438" Y="-0.265091156006" />
                  <Point X="3.729086914062" Y="-0.295379486084" />
                  <Point X="3.741167724609" Y="-0.300387023926" />
                  <Point X="4.477561035156" Y="-0.49770300293" />
                  <Point X="4.998067871094" Y="-0.637172363281" />
                  <Point X="4.980199707031" Y="-0.75568963623" />
                  <Point X="4.948431640625" Y="-0.966399169922" />
                  <Point X="4.912240234375" Y="-1.12499609375" />
                  <Point X="4.874545898438" Y="-1.290178344727" />
                  <Point X="4.065679931641" Y="-1.183689086914" />
                  <Point X="3.411982177734" Y="-1.097628295898" />
                  <Point X="3.394836181641" Y="-1.098341186523" />
                  <Point X="3.316115234375" Y="-1.115451538086" />
                  <Point X="3.213271972656" Y="-1.137804931641" />
                  <Point X="3.1854453125" Y="-1.154697265625" />
                  <Point X="3.13786328125" Y="-1.211923461914" />
                  <Point X="3.075701171875" Y="-1.286685424805" />
                  <Point X="3.064357910156" Y="-1.314070556641" />
                  <Point X="3.057538330078" Y="-1.388181030273" />
                  <Point X="3.04862890625" Y="-1.485000854492" />
                  <Point X="3.056360351562" Y="-1.516621826172" />
                  <Point X="3.09992578125" Y="-1.584384887695" />
                  <Point X="3.156840576172" Y="-1.672912353516" />
                  <Point X="3.168460693359" Y="-1.685540649414" />
                  <Point X="3.851838134766" Y="-2.209914794922" />
                  <Point X="4.33907421875" Y="-2.583784179688" />
                  <Point X="4.293682617188" Y="-2.657234619141" />
                  <Point X="4.204133300781" Y="-2.802139404297" />
                  <Point X="4.12928515625" Y="-2.90848828125" />
                  <Point X="4.056688476562" Y="-3.011638427734" />
                  <Point X="3.335660644531" Y="-2.595352539062" />
                  <Point X="2.753454589844" Y="-2.259215576172" />
                  <Point X="2.737340820313" Y="-2.253312744141" />
                  <Point X="2.643650390625" Y="-2.236392333984" />
                  <Point X="2.521250488281" Y="-2.214287109375" />
                  <Point X="2.489077636719" Y="-2.219244873047" />
                  <Point X="2.411243652344" Y="-2.260208251953" />
                  <Point X="2.309559570312" Y="-2.313723876953" />
                  <Point X="2.288599853516" Y="-2.334683837891" />
                  <Point X="2.247636474609" Y="-2.412517578125" />
                  <Point X="2.194120849609" Y="-2.514201904297" />
                  <Point X="2.189163085938" Y="-2.546374511719" />
                  <Point X="2.206083496094" Y="-2.640064941406" />
                  <Point X="2.228188720703" Y="-2.76246484375" />
                  <Point X="2.234091552734" Y="-2.778578613281" />
                  <Point X="2.673230224609" Y="-3.539188964844" />
                  <Point X="2.986673583984" Y="-4.082088623047" />
                  <Point X="2.94155859375" Y="-4.114313476562" />
                  <Point X="2.835298339844" Y="-4.190212402344" />
                  <Point X="2.751615722656" Y="-4.24437890625" />
                  <Point X="2.679775878906" Y="-4.290879394531" />
                  <Point X="2.127454589844" Y="-3.571080078125" />
                  <Point X="1.683177490234" Y="-2.992087402344" />
                  <Point X="1.670549316406" Y="-2.980467529297" />
                  <Point X="1.578145141602" Y="-2.921060058594" />
                  <Point X="1.45742590332" Y="-2.843448730469" />
                  <Point X="1.4258046875" Y="-2.835717041016" />
                  <Point X="1.324744995117" Y="-2.845016601562" />
                  <Point X="1.192717773438" Y="-2.857166015625" />
                  <Point X="1.165332885742" Y="-2.868509277344" />
                  <Point X="1.087297241211" Y="-2.933393310547" />
                  <Point X="0.985349060059" Y="-3.018159667969" />
                  <Point X="0.968456726074" Y="-3.045986328125" />
                  <Point X="0.945124511719" Y="-3.153333251953" />
                  <Point X="0.91464251709" Y="-3.29357421875" />
                  <Point X="0.91392956543" Y="-3.310720214844" />
                  <Point X="1.038378051758" Y="-4.256000488281" />
                  <Point X="1.127642211914" Y="-4.934028320312" />
                  <Point X="1.094875610352" Y="-4.9412109375" />
                  <Point X="0.994346313477" Y="-4.963246582031" />
                  <Point X="0.917038208008" Y="-4.977291015625" />
                  <Point X="0.860200378418" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#160" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.083785213187" Y="4.667815162065" Z="1.1" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.1" />
                  <Point X="-0.641131131529" Y="5.023904450945" Z="1.1" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.1" />
                  <Point X="-1.4181653662" Y="4.862046048714" Z="1.1" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.1" />
                  <Point X="-1.731932719004" Y="4.627657504962" Z="1.1" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.1" />
                  <Point X="-1.725929802209" Y="4.385191551583" Z="1.1" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.1" />
                  <Point X="-1.796100133699" Y="4.317535462703" Z="1.1" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.1" />
                  <Point X="-1.893032253184" Y="4.327800627403" Z="1.1" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.1" />
                  <Point X="-2.021018339089" Y="4.462285101628" Z="1.1" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.1" />
                  <Point X="-2.503738172256" Y="4.404645864465" Z="1.1" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.1" />
                  <Point X="-3.121935346634" Y="3.990381513835" Z="1.1" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.1" />
                  <Point X="-3.215150359384" Y="3.510323407381" Z="1.1" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.1" />
                  <Point X="-2.997285149194" Y="3.091855263835" Z="1.1" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.1" />
                  <Point X="-3.028435595926" Y="3.020367971961" Z="1.1" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.1" />
                  <Point X="-3.103221201772" Y="2.998279549979" Z="1.1" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.1" />
                  <Point X="-3.423535940916" Y="3.165043589361" Z="1.1" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.1" />
                  <Point X="-4.028121176939" Y="3.077156474137" Z="1.1" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.1" />
                  <Point X="-4.400249292481" Y="2.516439632619" Z="1.1" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.1" />
                  <Point X="-4.178645510727" Y="1.980749545757" Z="1.1" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.1" />
                  <Point X="-3.679716769102" Y="1.578474172525" Z="1.1" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.1" />
                  <Point X="-3.680783480825" Y="1.519999428946" Z="1.1" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.1" />
                  <Point X="-3.726263475507" Y="1.483229751464" Z="1.1" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.1" />
                  <Point X="-4.214041974711" Y="1.535543563178" Z="1.1" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.1" />
                  <Point X="-4.905048523348" Y="1.288071814211" Z="1.1" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.1" />
                  <Point X="-5.022391214307" Y="0.703009803705" Z="1.1" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.1" />
                  <Point X="-4.417009196047" Y="0.274266521766" Z="1.1" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.1" />
                  <Point X="-3.560840698209" Y="0.038158428334" Z="1.1" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.1" />
                  <Point X="-3.543567756891" Y="0.012923425782" Z="1.1" />
                  <Point X="-3.539556741714" Y="0" Z="1.1" />
                  <Point X="-3.544796844244" Y="-0.016883525278" Z="1.1" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.1" />
                  <Point X="-3.564527896915" Y="-0.0407175548" Z="1.1" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.1" />
                  <Point X="-4.219878592726" Y="-0.22144553499" Z="1.1" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.1" />
                  <Point X="-5.016335148551" Y="-0.754229780996" Z="1.1" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.1" />
                  <Point X="-4.905764212068" Y="-1.290721705598" Z="1.1" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.1" />
                  <Point X="-4.141160748452" Y="-1.428247132395" Z="1.1" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.1" />
                  <Point X="-3.204158381999" Y="-1.315691952941" Z="1.1" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.1" />
                  <Point X="-3.197040356934" Y="-1.33929957843" Z="1.1" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.1" />
                  <Point X="-3.765115447733" Y="-1.785533286564" Z="1.1" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.1" />
                  <Point X="-4.336627752738" Y="-2.630469906474" Z="1.1" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.1" />
                  <Point X="-4.012738268183" Y="-3.102200836232" Z="1.1" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.1" />
                  <Point X="-3.303192867233" Y="-2.977160678785" Z="1.1" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.1" />
                  <Point X="-2.563012786996" Y="-2.565317682136" Z="1.1" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.1" />
                  <Point X="-2.878256334383" Y="-3.131885515259" Z="1.1" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.1" />
                  <Point X="-3.068001335584" Y="-4.040813183639" Z="1.1" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.1" />
                  <Point X="-2.641610480335" Y="-4.331593202872" Z="1.1" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.1" />
                  <Point X="-2.353609625638" Y="-4.322466546257" Z="1.1" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.1" />
                  <Point X="-2.08010266183" Y="-4.058818030071" Z="1.1" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.1" />
                  <Point X="-1.792895686009" Y="-3.995578102255" Z="1.1" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.1" />
                  <Point X="-1.526541098108" Y="-4.120247266256" Z="1.1" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.1" />
                  <Point X="-1.391121471935" Y="-4.381300202324" Z="1.1" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.1" />
                  <Point X="-1.385785546148" Y="-4.672036676465" Z="1.1" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.1" />
                  <Point X="-1.245607536643" Y="-4.922597360672" Z="1.1" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.1" />
                  <Point X="-0.947623907336" Y="-4.988538066987" Z="1.1" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.1" />
                  <Point X="-0.643987696411" Y="-4.365578887507" Z="1.1" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.1" />
                  <Point X="-0.324346641434" Y="-3.385152220032" Z="1.1" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.1" />
                  <Point X="-0.109849117132" Y="-3.238332480585" Z="1.1" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.1" />
                  <Point X="0.143509962229" Y="-3.248779564703" Z="1.1" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.1" />
                  <Point X="0.346099217998" Y="-3.416493535736" Z="1.1" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.1" />
                  <Point X="0.59076716399" Y="-4.166957142309" Z="1.1" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.1" />
                  <Point X="0.919819086315" Y="-4.995205841244" Z="1.1" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.1" />
                  <Point X="1.099425224657" Y="-4.958771252953" Z="1.1" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.1" />
                  <Point X="1.081794336177" Y="-4.218194185799" Z="1.1" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.1" />
                  <Point X="0.987827732379" Y="-3.132672820393" Z="1.1" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.1" />
                  <Point X="1.113108046062" Y="-2.940559569679" Z="1.1" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.1" />
                  <Point X="1.323170850173" Y="-2.863526186089" Z="1.1" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.1" />
                  <Point X="1.544949780911" Y="-2.931837960871" Z="1.1" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.1" />
                  <Point X="2.081630837233" Y="-3.570238132594" Z="1.1" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.1" />
                  <Point X="2.772628877895" Y="-4.255072887878" Z="1.1" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.1" />
                  <Point X="2.964464074502" Y="-4.123720328936" Z="1.1" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.1" />
                  <Point X="2.710375638829" Y="-3.48290891707" Z="1.1" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.1" />
                  <Point X="2.249131556001" Y="-2.599898702348" Z="1.1" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.1" />
                  <Point X="2.285726818088" Y="-2.404523966139" Z="1.1" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.1" />
                  <Point X="2.428374412234" Y="-2.273174491879" Z="1.1" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.1" />
                  <Point X="2.628608101165" Y="-2.25431645349" Z="1.1" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.1" />
                  <Point X="3.304504680037" Y="-2.607373861446" Z="1.1" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.1" />
                  <Point X="4.164017622469" Y="-2.905985564696" Z="1.1" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.1" />
                  <Point X="4.33005993411" Y="-2.652239733005" Z="1.1" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.1" />
                  <Point X="3.876120150157" Y="-2.138966964149" Z="1.1" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.1" />
                  <Point X="3.135827845674" Y="-1.526065519844" Z="1.1" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.1" />
                  <Point X="3.101171842191" Y="-1.36148261624" Z="1.1" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.1" />
                  <Point X="3.170153695898" Y="-1.21261035632" Z="1.1" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.1" />
                  <Point X="3.320578854544" Y="-1.133030760658" Z="1.1" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.1" />
                  <Point X="4.052997647903" Y="-1.20198136441" Z="1.1" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.1" />
                  <Point X="4.954831285412" Y="-1.104840104145" Z="1.1" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.1" />
                  <Point X="5.023485095959" Y="-0.731863745663" Z="1.1" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.1" />
                  <Point X="4.484345920586" Y="-0.418126596676" Z="1.1" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.1" />
                  <Point X="3.695552349603" Y="-0.190522295291" Z="1.1" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.1" />
                  <Point X="3.624002504656" Y="-0.127275991481" Z="1.1" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.1" />
                  <Point X="3.589456653697" Y="-0.041887512346" Z="1.1" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.1" />
                  <Point X="3.591914796727" Y="0.05472301887" Z="1.1" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.1" />
                  <Point X="3.631376933744" Y="0.136672747094" Z="1.1" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.1" />
                  <Point X="3.707843064749" Y="0.197626548951" Z="1.1" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.1" />
                  <Point X="4.311621896721" Y="0.371845336093" Z="1.1" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.1" />
                  <Point X="5.010686173246" Y="0.808919012165" Z="1.1" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.1" />
                  <Point X="4.924718082841" Y="1.22820116975" Z="1.1" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.1" />
                  <Point X="4.266127625506" Y="1.327741888977" Z="1.1" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.1" />
                  <Point X="3.409785997122" Y="1.229073008235" Z="1.1" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.1" />
                  <Point X="3.32951332376" Y="1.256673989802" Z="1.1" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.1" />
                  <Point X="3.272097352423" Y="1.315046057863" Z="1.1" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.1" />
                  <Point X="3.241252780867" Y="1.395221316736" Z="1.1" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.1" />
                  <Point X="3.245783827009" Y="1.475944138211" Z="1.1" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.1" />
                  <Point X="3.287845491788" Y="1.552011922185" Z="1.1" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.1" />
                  <Point X="3.804746905192" Y="1.962103904731" Z="1.1" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.1" />
                  <Point X="4.328855742611" Y="2.650911344313" Z="1.1" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.1" />
                  <Point X="4.104550421894" Y="2.986467718283" Z="1.1" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.1" />
                  <Point X="3.355207257399" Y="2.755049860427" Z="1.1" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.1" />
                  <Point X="2.464401441457" Y="2.254837764998" Z="1.1" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.1" />
                  <Point X="2.390267388543" Y="2.250270953064" Z="1.1" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.1" />
                  <Point X="2.324306887445" Y="2.2782329323" Z="1.1" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.1" />
                  <Point X="2.272525667935" Y="2.332717972937" Z="1.1" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.1" />
                  <Point X="2.249158749695" Y="2.39949105529" Z="1.1" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.1" />
                  <Point X="2.25769017766" Y="2.475068134425" Z="1.1" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.1" />
                  <Point X="2.640575492432" Y="3.156931824556" Z="1.1" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.1" />
                  <Point X="2.916142736746" Y="4.153367935734" Z="1.1" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.1" />
                  <Point X="2.528209082677" Y="4.400285752575" Z="1.1" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.1" />
                  <Point X="2.122546016308" Y="4.609821055033" Z="1.1" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.1" />
                  <Point X="1.701999664586" Y="4.781093086162" Z="1.1" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.1" />
                  <Point X="1.146191586978" Y="4.937750238619" Z="1.1" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.1" />
                  <Point X="0.483439660544" Y="5.045931855069" Z="1.1" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.1" />
                  <Point X="0.109459348366" Y="4.763632343886" Z="1.1" />
                  <Point X="0" Y="4.355124473572" Z="1.1" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>