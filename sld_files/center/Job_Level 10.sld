<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#135" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="873" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="0" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="0.004715820312" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.634471435547" Y="-3.778132324219" />
                  <Point X="0.563302062988" Y="-3.512524414063" />
                  <Point X="0.557721191406" Y="-3.497141845703" />
                  <Point X="0.54236315918" Y="-3.467377441406" />
                  <Point X="0.513274658203" Y="-3.425466064453" />
                  <Point X="0.378635528564" Y="-3.231477050781" />
                  <Point X="0.356751586914" Y="-3.209021484375" />
                  <Point X="0.330496398926" Y="-3.189777587891" />
                  <Point X="0.302495330811" Y="-3.175669433594" />
                  <Point X="0.257482330322" Y="-3.161698974609" />
                  <Point X="0.049136341095" Y="-3.097036132812" />
                  <Point X="0.020976791382" Y="-3.092766601562" />
                  <Point X="-0.00866468811" Y="-3.092766601562" />
                  <Point X="-0.036823783875" Y="-3.097035888672" />
                  <Point X="-0.081836921692" Y="-3.111006103516" />
                  <Point X="-0.290182922363" Y="-3.175669189453" />
                  <Point X="-0.31818460083" Y="-3.189777587891" />
                  <Point X="-0.344439788818" Y="-3.209021484375" />
                  <Point X="-0.366323425293" Y="-3.2314765625" />
                  <Point X="-0.395412231445" Y="-3.273387695312" />
                  <Point X="-0.530051086426" Y="-3.467376953125" />
                  <Point X="-0.538189025879" Y="-3.481573730469" />
                  <Point X="-0.550990112305" Y="-3.512524414063" />
                  <Point X="-0.880401855469" Y="-4.74190625" />
                  <Point X="-0.916584594727" Y="-4.876941894531" />
                  <Point X="-1.079335693359" Y="-4.8453515625" />
                  <Point X="-1.127650024414" Y="-4.832920410156" />
                  <Point X="-1.24641796875" Y="-4.802362304688" />
                  <Point X="-1.223530151367" Y="-4.628512207031" />
                  <Point X="-1.214963012695" Y="-4.563438476562" />
                  <Point X="-1.214201171875" Y="-4.5479296875" />
                  <Point X="-1.216508666992" Y="-4.516224121094" />
                  <Point X="-1.227262329102" Y="-4.462162109375" />
                  <Point X="-1.277036132812" Y="-4.211932617188" />
                  <Point X="-1.287938354492" Y="-4.182966308594" />
                  <Point X="-1.304010375977" Y="-4.15512890625" />
                  <Point X="-1.323644775391" Y="-4.131204101562" />
                  <Point X="-1.365087280273" Y="-4.094860107422" />
                  <Point X="-1.556905517578" Y="-3.926639404297" />
                  <Point X="-1.583188476562" Y="-3.910295410156" />
                  <Point X="-1.612885742188" Y="-3.897994384766" />
                  <Point X="-1.64302722168" Y="-3.890966308594" />
                  <Point X="-1.698030517578" Y="-3.887361083984" />
                  <Point X="-1.952616333008" Y="-3.870674804688" />
                  <Point X="-1.98341796875" Y="-3.873708496094" />
                  <Point X="-2.014466674805" Y="-3.882028076172" />
                  <Point X="-2.042657714844" Y="-3.894801269531" />
                  <Point X="-2.088489501953" Y="-3.925425048828" />
                  <Point X="-2.300624023438" Y="-4.067169433594" />
                  <Point X="-2.312785644531" Y="-4.076821533203" />
                  <Point X="-2.335102783203" Y="-4.099461914062" />
                  <Point X="-2.480149169922" Y="-4.288490234375" />
                  <Point X="-2.801707763672" Y="-4.089388671875" />
                  <Point X="-2.868603515625" Y="-4.037881347656" />
                  <Point X="-3.104721923828" Y="-3.856077880859" />
                  <Point X="-2.556021484375" Y="-2.905701171875" />
                  <Point X="-2.423761230469" Y="-2.676619628906" />
                  <Point X="-2.411279541016" Y="-2.646973144531" />
                  <Point X="-2.405864257812" Y="-2.624442871094" />
                  <Point X="-2.406064208984" Y="-2.601271728516" />
                  <Point X="-2.410208740234" Y="-2.569790039062" />
                  <Point X="-2.416626953125" Y="-2.5458359375" />
                  <Point X="-2.429025878906" Y="-2.524359375" />
                  <Point X="-2.441691894531" Y="-2.507851806641" />
                  <Point X="-2.449886962891" Y="-2.498507080078" />
                  <Point X="-2.464153320312" Y="-2.484240722656" />
                  <Point X="-2.489310302734" Y="-2.466213134766" />
                  <Point X="-2.518139160156" Y="-2.45199609375" />
                  <Point X="-2.547756835938" Y="-2.443011474609" />
                  <Point X="-2.578690917969" Y="-2.444024169922" />
                  <Point X="-2.610217285156" Y="-2.450295166016" />
                  <Point X="-2.63918359375" Y="-2.461197265625" />
                  <Point X="-3.699501220703" Y="-3.073372070313" />
                  <Point X="-3.818024414062" Y="-3.141801269531" />
                  <Point X="-4.082858154297" Y="-2.793863769531" />
                  <Point X="-4.130815917969" Y="-2.713446289062" />
                  <Point X="-4.306142578125" Y="-2.419450439453" />
                  <Point X="-3.3373671875" Y="-1.676082519531" />
                  <Point X="-3.105954589844" Y="-1.498513305664" />
                  <Point X="-3.083063232422" Y="-1.475878051758" />
                  <Point X="-3.0642421875" Y="-1.446143066406" />
                  <Point X="-3.056159912109" Y="-1.425790161133" />
                  <Point X="-3.052487548828" Y="-1.414547241211" />
                  <Point X="-3.046151855469" Y="-1.390085083008" />
                  <Point X="-3.042037597656" Y="-1.358602783203" />
                  <Point X="-3.042736816406" Y="-1.335734375" />
                  <Point X="-3.048883300781" Y="-1.313696411133" />
                  <Point X="-3.060888183594" Y="-1.284713867188" />
                  <Point X="-3.073293457031" Y="-1.263229248047" />
                  <Point X="-3.090838623047" Y="-1.245689208984" />
                  <Point X="-3.108045898438" Y="-1.232490844727" />
                  <Point X="-3.117677490234" Y="-1.225997924805" />
                  <Point X="-3.139455078125" Y="-1.213180664062" />
                  <Point X="-3.168718261719" Y="-1.201956420898" />
                  <Point X="-3.200605957031" Y="-1.195474853516" />
                  <Point X="-3.231929199219" Y="-1.194383789062" />
                  <Point X="-4.570479980469" Y="-1.370607299805" />
                  <Point X="-4.7321015625" Y="-1.391885253906" />
                  <Point X="-4.834077636719" Y="-0.992654785156" />
                  <Point X="-4.846765136719" Y="-0.903944396973" />
                  <Point X="-4.892424316406" Y="-0.584698181152" />
                  <Point X="-3.796489746094" Y="-0.291043395996" />
                  <Point X="-3.532875976562" Y="-0.220408340454" />
                  <Point X="-3.513689941406" Y="-0.212959381104" />
                  <Point X="-3.493191162109" Y="-0.20231690979" />
                  <Point X="-3.482798095703" Y="-0.196047348022" />
                  <Point X="-3.459975830078" Y="-0.180207565308" />
                  <Point X="-3.436020263672" Y="-0.158680343628" />
                  <Point X="-3.415779785156" Y="-0.129824386597" />
                  <Point X="-3.406710693359" Y="-0.109791236877" />
                  <Point X="-3.402524658203" Y="-0.098771713257" />
                  <Point X="-3.394917236328" Y="-0.074260444641" />
                  <Point X="-3.389474365234" Y="-0.045520751953" />
                  <Point X="-3.390129394531" Y="-0.013240850449" />
                  <Point X="-3.394020019531" Y="0.006875628948" />
                  <Point X="-3.396560791016" Y="0.016995950699" />
                  <Point X="-3.404168212891" Y="0.041507217407" />
                  <Point X="-3.417484863281" Y="0.070831756592" />
                  <Point X="-3.439087402344" Y="0.098980072021" />
                  <Point X="-3.455836425781" Y="0.113919891357" />
                  <Point X="-3.464906494141" Y="0.121069526672" />
                  <Point X="-3.487728759766" Y="0.136909469604" />
                  <Point X="-3.501925292969" Y="0.145047103882" />
                  <Point X="-3.532875976562" Y="0.157848342896" />
                  <Point X="-4.753028320312" Y="0.484787139893" />
                  <Point X="-4.89181640625" Y="0.521975280762" />
                  <Point X="-4.824488769531" Y="0.976970214844" />
                  <Point X="-4.798946289062" Y="1.071229492188" />
                  <Point X="-4.70355078125" Y="1.423267822266" />
                  <Point X="-3.954631103516" Y="1.324670654297" />
                  <Point X="-3.765666259766" Y="1.29979284668" />
                  <Point X="-3.744986083984" Y="1.299341674805" />
                  <Point X="-3.723424560547" Y="1.301228149414" />
                  <Point X="-3.703138427734" Y="1.305263305664" />
                  <Point X="-3.692225341797" Y="1.308704101562" />
                  <Point X="-3.641712402344" Y="1.324630859375" />
                  <Point X="-3.622779052734" Y="1.332961669922" />
                  <Point X="-3.604034667969" Y="1.343783569336" />
                  <Point X="-3.587353271484" Y="1.356014892578" />
                  <Point X="-3.57371484375" Y="1.37156652832" />
                  <Point X="-3.561300537109" Y="1.389296264648" />
                  <Point X="-3.551351318359" Y="1.407431640625" />
                  <Point X="-3.546972412109" Y="1.418003540039" />
                  <Point X="-3.526703857422" Y="1.466935913086" />
                  <Point X="-3.520916015625" Y="1.48679296875" />
                  <Point X="-3.517157470703" Y="1.508108154297" />
                  <Point X="-3.515804443359" Y="1.528748657227" />
                  <Point X="-3.518951171875" Y="1.549192749023" />
                  <Point X="-3.524552978516" Y="1.570099121094" />
                  <Point X="-3.532049560547" Y="1.589377319336" />
                  <Point X="-3.537333251953" Y="1.59952734375" />
                  <Point X="-3.561789306641" Y="1.646507080078" />
                  <Point X="-3.573281738281" Y="1.663706665039" />
                  <Point X="-3.587194335938" Y="1.680286865234" />
                  <Point X="-3.602135986328" Y="1.694590209961" />
                  <Point X="-4.302018066406" Y="2.23162890625" />
                  <Point X="-4.351860351563" Y="2.269874267578" />
                  <Point X="-4.081154541016" Y="2.733658691406" />
                  <Point X="-4.013498779297" Y="2.820619628906" />
                  <Point X="-3.750504638672" Y="3.158661865234" />
                  <Point X="-3.322713867188" Y="2.911676513672" />
                  <Point X="-3.206656982422" Y="2.844670898438" />
                  <Point X="-3.187724121094" Y="2.836340332031" />
                  <Point X="-3.167081787109" Y="2.829831787109" />
                  <Point X="-3.146794921875" Y="2.825796386719" />
                  <Point X="-3.131595703125" Y="2.824466552734" />
                  <Point X="-3.061245605469" Y="2.818311767578" />
                  <Point X="-3.040564941406" Y="2.818762939453" />
                  <Point X="-3.019106201172" Y="2.821588134766" />
                  <Point X="-2.999014160156" Y="2.826504882812" />
                  <Point X="-2.980462402344" Y="2.835653564453" />
                  <Point X="-2.962208251953" Y="2.847282958984" />
                  <Point X="-2.946078125" Y="2.860228759766" />
                  <Point X="-2.935289550781" Y="2.871017089844" />
                  <Point X="-2.885354492188" Y="2.920952148438" />
                  <Point X="-2.872406982422" Y="2.937083984375" />
                  <Point X="-2.860777587891" Y="2.955338134766" />
                  <Point X="-2.85162890625" Y="2.973889892578" />
                  <Point X="-2.846712158203" Y="2.993981933594" />
                  <Point X="-2.843886962891" Y="3.015440673828" />
                  <Point X="-2.843435791016" Y="3.036121337891" />
                  <Point X="-2.844765625" Y="3.051320556641" />
                  <Point X="-2.850920410156" Y="3.121670898438" />
                  <Point X="-2.854955810547" Y="3.141957763672" />
                  <Point X="-2.861464355469" Y="3.162600097656" />
                  <Point X="-2.869794921875" Y="3.181532958984" />
                  <Point X="-3.179934082031" Y="3.718709472656" />
                  <Point X="-3.179086425781" Y="3.727852294922" />
                  <Point X="-2.700625976563" Y="4.094683349609" />
                  <Point X="-2.594073730469" Y="4.153881835938" />
                  <Point X="-2.167036621094" Y="4.391134277344" />
                  <Point X="-2.078724121094" Y="4.27604296875" />
                  <Point X="-2.04319543457" Y="4.229741210938" />
                  <Point X="-2.028892456055" Y="4.214799804688" />
                  <Point X="-2.01231237793" Y="4.200887207031" />
                  <Point X="-1.99511315918" Y="4.18939453125" />
                  <Point X="-1.978196655273" Y="4.180588378906" />
                  <Point X="-1.899897094727" Y="4.139827636719" />
                  <Point X="-1.88061730957" Y="4.132330566406" />
                  <Point X="-1.85971081543" Y="4.126729003906" />
                  <Point X="-1.839267822266" Y="4.123582519531" />
                  <Point X="-1.818628417969" Y="4.124935546875" />
                  <Point X="-1.797313110352" Y="4.128693847656" />
                  <Point X="-1.777451904297" Y="4.134482910156" />
                  <Point X="-1.75983203125" Y="4.141781738281" />
                  <Point X="-1.678277954102" Y="4.1755625" />
                  <Point X="-1.660146362305" Y="4.185509765625" />
                  <Point X="-1.642416625977" Y="4.197923828125" />
                  <Point X="-1.626864379883" Y="4.2115625" />
                  <Point X="-1.6146328125" Y="4.228244140625" />
                  <Point X="-1.603810913086" Y="4.24698828125" />
                  <Point X="-1.59548046875" Y="4.265920898438" />
                  <Point X="-1.589745483398" Y="4.284109863281" />
                  <Point X="-1.563201171875" Y="4.368297363281" />
                  <Point X="-1.559165649414" Y="4.388584960938" />
                  <Point X="-1.557279174805" Y="4.410146972656" />
                  <Point X="-1.55773034668" Y="4.430826660156" />
                  <Point X="-1.584201904297" Y="4.631897949219" />
                  <Point X="-1.569032592773" Y="4.636150878906" />
                  <Point X="-0.94963494873" Y="4.80980859375" />
                  <Point X="-0.820469726562" Y="4.824925292969" />
                  <Point X="-0.294710784912" Y="4.886458007812" />
                  <Point X="-0.168236175537" Y="4.414447753906" />
                  <Point X="-0.133903305054" Y="4.286315429688" />
                  <Point X="-0.121129844666" Y="4.258124023438" />
                  <Point X="-0.10327155304" Y="4.231397460938" />
                  <Point X="-0.082114555359" Y="4.20880859375" />
                  <Point X="-0.054819446564" Y="4.19421875" />
                  <Point X="-0.024381370544" Y="4.183886230469" />
                  <Point X="0.006155917168" Y="4.178844238281" />
                  <Point X="0.03669316864" Y="4.183886230469" />
                  <Point X="0.067131248474" Y="4.19421875" />
                  <Point X="0.094426452637" Y="4.20880859375" />
                  <Point X="0.115583503723" Y="4.231397460938" />
                  <Point X="0.133441650391" Y="4.258124023438" />
                  <Point X="0.146215255737" Y="4.286315429688" />
                  <Point X="0.305126922607" Y="4.879382324219" />
                  <Point X="0.307419219971" Y="4.8879375" />
                  <Point X="0.844041748047" Y="4.831738769531" />
                  <Point X="0.950910095215" Y="4.8059375" />
                  <Point X="1.481026489258" Y="4.677951171875" />
                  <Point X="1.549368041992" Y="4.653163085938" />
                  <Point X="1.894645874023" Y="4.527928222656" />
                  <Point X="1.961911376953" Y="4.496470214844" />
                  <Point X="2.294573486328" Y="4.340895019531" />
                  <Point X="2.35958984375" Y="4.303016601562" />
                  <Point X="2.680980957031" Y="4.1157734375" />
                  <Point X="2.742267822266" Y="4.072189453125" />
                  <Point X="2.943260009766" Y="3.929254882812" />
                  <Point X="2.300662353516" Y="2.816242675781" />
                  <Point X="2.147581054688" Y="2.551097900391" />
                  <Point X="2.139762939453" Y="2.533742431641" />
                  <Point X="2.130947021484" Y="2.507395019531" />
                  <Point X="2.129262451172" Y="2.501792480469" />
                  <Point X="2.111607177734" Y="2.435770263672" />
                  <Point X="2.108383300781" Y="2.410773193359" />
                  <Point X="2.108532958984" Y="2.379536621094" />
                  <Point X="2.109215087891" Y="2.368618896484" />
                  <Point X="2.116099121094" Y="2.311528320312" />
                  <Point X="2.121441894531" Y="2.289605224609" />
                  <Point X="2.129708007812" Y="2.267516357422" />
                  <Point X="2.140070800781" Y="2.247471435547" />
                  <Point X="2.147702880859" Y="2.236223632812" />
                  <Point X="2.183028564453" Y="2.184162841797" />
                  <Point X="2.199999511719" Y="2.165216308594" />
                  <Point X="2.224547119141" Y="2.144284179687" />
                  <Point X="2.232846679688" Y="2.137960205078" />
                  <Point X="2.284907714844" Y="2.102634765625" />
                  <Point X="2.304952880859" Y="2.092271972656" />
                  <Point X="2.327041259766" Y="2.084006103516" />
                  <Point X="2.348964111328" Y="2.078663330078" />
                  <Point X="2.361298583984" Y="2.077176025391" />
                  <Point X="2.418388916016" Y="2.070291748047" />
                  <Point X="2.444343994141" Y="2.070734375" />
                  <Point X="2.477510742188" Y="2.075886474609" />
                  <Point X="2.487470458984" Y="2.077985595703" />
                  <Point X="2.553492675781" Y="2.095640869141" />
                  <Point X="2.565283447266" Y="2.099638183594" />
                  <Point X="2.588533935547" Y="2.110145019531" />
                  <Point X="3.815772216797" Y="2.81869140625" />
                  <Point X="3.967326660156" Y="2.906191162109" />
                  <Point X="4.123272460938" Y="2.689461669922" />
                  <Point X="4.157440429688" Y="2.632998535156" />
                  <Point X="4.262198730469" Y="2.459884277344" />
                  <Point X="3.432329833984" Y="1.823103027344" />
                  <Point X="3.230783935547" Y="1.668451171875" />
                  <Point X="3.216649169922" Y="1.655096801758" />
                  <Point X="3.197138427734" Y="1.632454711914" />
                  <Point X="3.193708007812" Y="1.628235229492" />
                  <Point X="3.146191650391" Y="1.566246582031" />
                  <Point X="3.133426757812" Y="1.543841064453" />
                  <Point X="3.121133300781" Y="1.513215209961" />
                  <Point X="3.117806152344" Y="1.503412353516" />
                  <Point X="3.100106201172" Y="1.440121826172" />
                  <Point X="3.096652587891" Y="1.417823242188" />
                  <Point X="3.095836425781" Y="1.394252929688" />
                  <Point X="3.097739501953" Y="1.371768066406" />
                  <Point X="3.100878662109" Y="1.356554077148" />
                  <Point X="3.115408447266" Y="1.286135253906" />
                  <Point X="3.124107666016" Y="1.261610351562" />
                  <Point X="3.13984375" Y="1.231255126953" />
                  <Point X="3.144820800781" Y="1.222762451172" />
                  <Point X="3.184340332031" Y="1.162694824219" />
                  <Point X="3.198893554688" Y="1.145450439453" />
                  <Point X="3.216137451172" Y="1.129360595703" />
                  <Point X="3.234346435547" Y="1.11603515625" />
                  <Point X="3.246719238281" Y="1.1090703125" />
                  <Point X="3.303988525391" Y="1.076832763672" />
                  <Point X="3.32872265625" Y="1.067168579102" />
                  <Point X="3.363427246094" Y="1.058959594727" />
                  <Point X="3.372847412109" Y="1.057227416992" />
                  <Point X="3.450279052734" Y="1.046994018555" />
                  <Point X="3.462702392578" Y="1.046175048828" />
                  <Point X="3.488203613281" Y="1.04698449707" />
                  <Point X="4.653998046875" Y="1.200464355469" />
                  <Point X="4.77683984375" Y="1.21663671875" />
                  <Point X="4.845936035156" Y="0.932812011719" />
                  <Point X="4.856703613281" Y="0.863651611328" />
                  <Point X="4.890864746094" Y="0.644238525391" />
                  <Point X="3.947555908203" Y="0.391479736328" />
                  <Point X="3.716579833984" Y="0.329589935303" />
                  <Point X="3.698090820313" Y="0.322499053955" />
                  <Point X="3.66957421875" Y="0.307991363525" />
                  <Point X="3.665109863281" Y="0.305567718506" />
                  <Point X="3.589035644531" Y="0.261595428467" />
                  <Point X="3.568300292969" Y="0.245402267456" />
                  <Point X="3.544126953125" Y="0.220416046143" />
                  <Point X="3.537669677734" Y="0.213010955811" />
                  <Point X="3.492025146484" Y="0.154849273682" />
                  <Point X="3.480301025391" Y="0.135569000244" />
                  <Point X="3.470527099609" Y="0.114105232239" />
                  <Point X="3.463680908203" Y="0.092604881287" />
                  <Point X="3.460393554688" Y="0.075440574646" />
                  <Point X="3.445178710938" Y="-0.004005618572" />
                  <Point X="3.443877197266" Y="-0.030520822525" />
                  <Point X="3.447164550781" Y="-0.066494239807" />
                  <Point X="3.448465820313" Y="-0.075717781067" />
                  <Point X="3.463680664062" Y="-0.155164123535" />
                  <Point X="3.470527099609" Y="-0.176665542603" />
                  <Point X="3.480301025391" Y="-0.198129302979" />
                  <Point X="3.492024658203" Y="-0.217408355713" />
                  <Point X="3.501885986328" Y="-0.229974334717" />
                  <Point X="3.547530517578" Y="-0.288136169434" />
                  <Point X="3.567117431641" Y="-0.306841461182" />
                  <Point X="3.597865234375" Y="-0.328762115479" />
                  <Point X="3.605471435547" Y="-0.333655822754" />
                  <Point X="3.681545654297" Y="-0.377628143311" />
                  <Point X="3.692710205078" Y="-0.383139038086" />
                  <Point X="3.716579833984" Y="-0.392149932861" />
                  <Point X="4.785666015625" Y="-0.678610595703" />
                  <Point X="4.891472167969" Y="-0.706961425781" />
                  <Point X="4.855022460938" Y="-0.948726989746" />
                  <Point X="4.841227539062" Y="-1.009178283691" />
                  <Point X="4.801173828125" Y="-1.184698974609" />
                  <Point X="3.692249267578" Y="-1.038706298828" />
                  <Point X="3.424382080078" Y="-1.003441040039" />
                  <Point X="3.408035644531" Y="-1.002710327148" />
                  <Point X="3.374658447266" Y="-1.005508728027" />
                  <Point X="3.342400878906" Y="-1.012520080566" />
                  <Point X="3.193094238281" Y="-1.04497253418" />
                  <Point X="3.163974121094" Y="-1.056596923828" />
                  <Point X="3.136147460938" Y="-1.073489257812" />
                  <Point X="3.112397460938" Y="-1.093960205078" />
                  <Point X="3.092899658203" Y="-1.11741003418" />
                  <Point X="3.002653320312" Y="-1.225948364258" />
                  <Point X="2.987932617188" Y="-1.250330566406" />
                  <Point X="2.976589355469" Y="-1.277715698242" />
                  <Point X="2.969757568359" Y="-1.305365722656" />
                  <Point X="2.966963134766" Y="-1.335734008789" />
                  <Point X="2.954028564453" Y="-1.476295898438" />
                  <Point X="2.956347412109" Y="-1.507564819336" />
                  <Point X="2.964079101562" Y="-1.539185791016" />
                  <Point X="2.976450439453" Y="-1.567996582031" />
                  <Point X="2.994302246094" Y="-1.595764038086" />
                  <Point X="3.076930664062" Y="-1.724287109375" />
                  <Point X="3.086932861328" Y="-1.737238647461" />
                  <Point X="3.110628417969" Y="-1.760909179688" />
                  <Point X="4.102747070312" Y="-2.522188964844" />
                  <Point X="4.213122070312" Y="-2.606883056641" />
                  <Point X="4.124809570313" Y="-2.749786376953" />
                  <Point X="4.096279785156" Y="-2.790322998047" />
                  <Point X="4.028981445312" Y="-2.885945068359" />
                  <Point X="3.039526611328" Y="-2.314682861328" />
                  <Point X="2.800954589844" Y="-2.176943115234" />
                  <Point X="2.786131591797" Y="-2.170012451172" />
                  <Point X="2.754224121094" Y="-2.159824951172" />
                  <Point X="2.715832275391" Y="-2.152891601563" />
                  <Point X="2.538133789062" Y="-2.120799316406" />
                  <Point X="2.506781982422" Y="-2.120395263672" />
                  <Point X="2.474609130859" Y="-2.125353027344" />
                  <Point X="2.444833496094" Y="-2.135176757812" />
                  <Point X="2.412939208984" Y="-2.151962402344" />
                  <Point X="2.265315429688" Y="-2.229655761719" />
                  <Point X="2.242384521484" Y="-2.246548828125" />
                  <Point X="2.221424804688" Y="-2.267508544922" />
                  <Point X="2.204531982422" Y="-2.290439208984" />
                  <Point X="2.187746337891" Y="-2.322333251953" />
                  <Point X="2.110052978516" Y="-2.469957275391" />
                  <Point X="2.100229003906" Y="-2.499733154297" />
                  <Point X="2.095271240234" Y="-2.531906005859" />
                  <Point X="2.095675292969" Y="-2.5632578125" />
                  <Point X="2.102608642578" Y="-2.601649658203" />
                  <Point X="2.134700927734" Y="-2.779348144531" />
                  <Point X="2.138985595703" Y="-2.795141845703" />
                  <Point X="2.151819091797" Y="-2.826078613281" />
                  <Point X="2.789355224609" Y="-3.930322998047" />
                  <Point X="2.861283203125" Y="-4.054906494141" />
                  <Point X="2.781845947266" Y="-4.111646484375" />
                  <Point X="2.749953125" Y="-4.132290527344" />
                  <Point X="2.701765136719" Y="-4.163481445312" />
                  <Point X="1.940598510742" Y="-3.171510253906" />
                  <Point X="1.758546020508" Y="-2.934255126953" />
                  <Point X="1.747503540039" Y="-2.922179443359" />
                  <Point X="1.721924194336" Y="-2.900557373047" />
                  <Point X="1.684059570312" Y="-2.876213867188" />
                  <Point X="1.50880078125" Y="-2.763538574219" />
                  <Point X="1.479989746094" Y="-2.751167236328" />
                  <Point X="1.448368774414" Y="-2.743435546875" />
                  <Point X="1.417099609375" Y="-2.741116699219" />
                  <Point X="1.375687988281" Y="-2.744927490234" />
                  <Point X="1.184012695312" Y="-2.762565673828" />
                  <Point X="1.15636315918" Y="-2.769397460938" />
                  <Point X="1.128978027344" Y="-2.780740722656" />
                  <Point X="1.104595825195" Y="-2.795461181641" />
                  <Point X="1.072619018555" Y="-2.822048828125" />
                  <Point X="0.924612121582" Y="-2.945111572266" />
                  <Point X="0.904141052246" Y="-2.968861816406" />
                  <Point X="0.887248779297" Y="-2.996688476562" />
                  <Point X="0.875624206543" Y="-3.025809082031" />
                  <Point X="0.866063354492" Y="-3.069796875" />
                  <Point X="0.821809936523" Y="-3.273396972656" />
                  <Point X="0.819724487305" Y="-3.289627441406" />
                  <Point X="0.819742248535" Y="-3.323120117188" />
                  <Point X="1.000415161133" Y="-4.695466308594" />
                  <Point X="1.022065307617" Y="-4.859915039062" />
                  <Point X="0.975676391602" Y="-4.870083984375" />
                  <Point X="0.946208679199" Y="-4.875437011719" />
                  <Point X="0.929315673828" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058426513672" Y="-4.752637207031" />
                  <Point X="-1.103977783203" Y="-4.740916992188" />
                  <Point X="-1.141246337891" Y="-4.731328125" />
                  <Point X="-1.129342895508" Y="-4.640912109375" />
                  <Point X="-1.120775634766" Y="-4.575838378906" />
                  <Point X="-1.120077514648" Y="-4.568099609375" />
                  <Point X="-1.119451782227" Y="-4.541033691406" />
                  <Point X="-1.121759277344" Y="-4.509328125" />
                  <Point X="-1.123334106445" Y="-4.497690429688" />
                  <Point X="-1.134087646484" Y="-4.443628417969" />
                  <Point X="-1.183861450195" Y="-4.193398925781" />
                  <Point X="-1.188125" Y="-4.17846875" />
                  <Point X="-1.19902734375" Y="-4.149502441406" />
                  <Point X="-1.205666015625" Y="-4.135466308594" />
                  <Point X="-1.221738037109" Y="-4.10762890625" />
                  <Point X="-1.23057409668" Y="-4.094861816406" />
                  <Point X="-1.250208496094" Y="-4.070937011719" />
                  <Point X="-1.261007080078" Y="-4.059779296875" />
                  <Point X="-1.302449707031" Y="-4.023435302734" />
                  <Point X="-1.494267822266" Y="-3.855214599609" />
                  <Point X="-1.506738647461" Y="-3.845965576172" />
                  <Point X="-1.533021606445" Y="-3.829621582031" />
                  <Point X="-1.546833618164" Y="-3.822526855469" />
                  <Point X="-1.576530761719" Y="-3.810225830078" />
                  <Point X="-1.591313476562" Y="-3.805476074219" />
                  <Point X="-1.621454833984" Y="-3.798447998047" />
                  <Point X="-1.636813720703" Y="-3.796169677734" />
                  <Point X="-1.691817016602" Y="-3.792564453125" />
                  <Point X="-1.946402832031" Y="-3.775878173828" />
                  <Point X="-1.961927978516" Y="-3.776132324219" />
                  <Point X="-1.992729614258" Y="-3.779166015625" />
                  <Point X="-2.008006103516" Y="-3.781945556641" />
                  <Point X="-2.03905480957" Y="-3.790265136719" />
                  <Point X="-2.053673828125" Y="-3.79549609375" />
                  <Point X="-2.081864990234" Y="-3.808269287109" />
                  <Point X="-2.095436767578" Y="-3.815811523438" />
                  <Point X="-2.141268554688" Y="-3.846435302734" />
                  <Point X="-2.353403076172" Y="-3.9881796875" />
                  <Point X="-2.359681640625" Y="-3.992757080078" />
                  <Point X="-2.380442138672" Y="-4.010131103516" />
                  <Point X="-2.402759277344" Y="-4.032771484375" />
                  <Point X="-2.410471435547" Y="-4.041629638672" />
                  <Point X="-2.503202880859" Y="-4.162479492188" />
                  <Point X="-2.747583007812" Y="-4.011165039062" />
                  <Point X="-2.810646240234" Y="-3.962608642578" />
                  <Point X="-2.980862792969" Y="-3.831547607422" />
                  <Point X="-2.473749023438" Y="-2.953201171875" />
                  <Point X="-2.341488769531" Y="-2.724119628906" />
                  <Point X="-2.336204833984" Y="-2.713482421875" />
                  <Point X="-2.323723144531" Y="-2.6838359375" />
                  <Point X="-2.31891015625" Y="-2.669174560547" />
                  <Point X="-2.313494873047" Y="-2.646644287109" />
                  <Point X="-2.310867675781" Y="-2.623623046875" />
                  <Point X="-2.311067626953" Y="-2.600451904297" />
                  <Point X="-2.311876953125" Y="-2.588872070312" />
                  <Point X="-2.316021484375" Y="-2.557390380859" />
                  <Point X="-2.318445556641" Y="-2.545203125" />
                  <Point X="-2.324863769531" Y="-2.521249023438" />
                  <Point X="-2.334353515625" Y="-2.498337646484" />
                  <Point X="-2.346752441406" Y="-2.476861083984" />
                  <Point X="-2.353655761719" Y="-2.466529052734" />
                  <Point X="-2.366321777344" Y="-2.450021484375" />
                  <Point X="-2.382711914062" Y="-2.43133203125" />
                  <Point X="-2.396978271484" Y="-2.417065673828" />
                  <Point X="-2.408817138672" Y="-2.407020751953" />
                  <Point X="-2.433974121094" Y="-2.388993164062" />
                  <Point X="-2.447292236328" Y="-2.381010498047" />
                  <Point X="-2.47612109375" Y="-2.366793457031" />
                  <Point X="-2.490561523438" Y="-2.361086914062" />
                  <Point X="-2.520179199219" Y="-2.352102294922" />
                  <Point X="-2.550865234375" Y="-2.348062255859" />
                  <Point X="-2.581799316406" Y="-2.349074951172" />
                  <Point X="-2.597224609375" Y="-2.350849609375" />
                  <Point X="-2.628750976562" Y="-2.357120605469" />
                  <Point X="-2.643680908203" Y="-2.361384033203" />
                  <Point X="-2.672647216797" Y="-2.372286132812" />
                  <Point X="-2.68668359375" Y="-2.378924804688" />
                  <Point X="-3.747001220703" Y="-2.991099609375" />
                  <Point X="-3.793089111328" Y="-3.017708496094" />
                  <Point X="-4.004014892578" Y="-2.740594970703" />
                  <Point X="-4.049223388672" Y="-2.664787841797" />
                  <Point X="-4.181265136719" Y="-2.443373779297" />
                  <Point X="-3.279534912109" Y="-1.751451049805" />
                  <Point X="-3.048122314453" Y="-1.573881713867" />
                  <Point X="-3.039158447266" Y="-1.566065307617" />
                  <Point X="-3.016267089844" Y="-1.543429931641" />
                  <Point X="-3.002791748047" Y="-1.526686767578" />
                  <Point X="-2.983970703125" Y="-1.496951538086" />
                  <Point X="-2.975948974609" Y="-1.481204833984" />
                  <Point X="-2.967866699219" Y="-1.460851928711" />
                  <Point X="-2.960521972656" Y="-1.438366333008" />
                  <Point X="-2.954186279297" Y="-1.413904174805" />
                  <Point X="-2.951952880859" Y="-1.402395507812" />
                  <Point X="-2.947838623047" Y="-1.370913330078" />
                  <Point X="-2.94708203125" Y="-1.355699462891" />
                  <Point X="-2.94778125" Y="-1.332831054688" />
                  <Point X="-2.951229248047" Y="-1.310212524414" />
                  <Point X="-2.957375732422" Y="-1.288174560547" />
                  <Point X="-2.961114746094" Y="-1.277341674805" />
                  <Point X="-2.973119628906" Y="-1.248359130859" />
                  <Point X="-2.978617675781" Y="-1.237210571289" />
                  <Point X="-2.991022949219" Y="-1.215726196289" />
                  <Point X="-3.006128173828" Y="-1.196044311523" />
                  <Point X="-3.023673339844" Y="-1.178504272461" />
                  <Point X="-3.033020751953" Y="-1.170309570312" />
                  <Point X="-3.050228027344" Y="-1.157111206055" />
                  <Point X="-3.069491210938" Y="-1.144125366211" />
                  <Point X="-3.091268798828" Y="-1.131308227539" />
                  <Point X="-3.10543359375" Y="-1.124481689453" />
                  <Point X="-3.134696777344" Y="-1.113257324219" />
                  <Point X="-3.149795410156" Y="-1.108859985352" />
                  <Point X="-3.181683105469" Y="-1.102378540039" />
                  <Point X="-3.197298828125" Y="-1.100532348633" />
                  <Point X="-3.228622070312" Y="-1.09944140625" />
                  <Point X="-3.244329101562" Y="-1.100196533203" />
                  <Point X="-4.582879882812" Y="-1.276420043945" />
                  <Point X="-4.660920410156" Y="-1.286694335938" />
                  <Point X="-4.740762207031" Y="-0.974117980957" />
                  <Point X="-4.752722167969" Y="-0.89049420166" />
                  <Point X="-4.786452148438" Y="-0.654654418945" />
                  <Point X="-3.771901855469" Y="-0.382806396484" />
                  <Point X="-3.508288085938" Y="-0.312171295166" />
                  <Point X="-3.498492919922" Y="-0.30896786499" />
                  <Point X="-3.479306884766" Y="-0.301518890381" />
                  <Point X="-3.469916015625" Y="-0.297273468018" />
                  <Point X="-3.449417236328" Y="-0.286630889893" />
                  <Point X="-3.428631347656" Y="-0.27409185791" />
                  <Point X="-3.405809082031" Y="-0.258252075195" />
                  <Point X="-3.396477539062" Y="-0.250868652344" />
                  <Point X="-3.372521972656" Y="-0.229341369629" />
                  <Point X="-3.358245605469" Y="-0.213233917236" />
                  <Point X="-3.338005126953" Y="-0.184378036499" />
                  <Point X="-3.329235107422" Y="-0.169003585815" />
                  <Point X="-3.320166015625" Y="-0.148970413208" />
                  <Point X="-3.311793945312" Y="-0.126931266785" />
                  <Point X="-3.304186523438" Y="-0.102419929504" />
                  <Point X="-3.301576416016" Y="-0.091937858582" />
                  <Point X="-3.296133544922" Y="-0.063198051453" />
                  <Point X="-3.294493896484" Y="-0.043593353271" />
                  <Point X="-3.295148925781" Y="-0.011313498497" />
                  <Point X="-3.296857910156" Y="0.004798264027" />
                  <Point X="-3.300748535156" Y="0.024914819717" />
                  <Point X="-3.305830078125" Y="0.045155475616" />
                  <Point X="-3.3134375" Y="0.069666809082" />
                  <Point X="-3.317669189453" Y="0.080787521362" />
                  <Point X="-3.330985839844" Y="0.110112007141" />
                  <Point X="-3.342120849609" Y="0.12867010498" />
                  <Point X="-3.363723388672" Y="0.156818389893" />
                  <Point X="-3.375850341797" Y="0.169875061035" />
                  <Point X="-3.392599365234" Y="0.18481477356" />
                  <Point X="-3.410739257812" Y="0.19911378479" />
                  <Point X="-3.433561523438" Y="0.214953872681" />
                  <Point X="-3.440484863281" Y="0.219329177856" />
                  <Point X="-3.465616210938" Y="0.232834686279" />
                  <Point X="-3.496566894531" Y="0.245635879517" />
                  <Point X="-3.508288085938" Y="0.249611251831" />
                  <Point X="-4.728440429688" Y="0.576550109863" />
                  <Point X="-4.785446289062" Y="0.591824768066" />
                  <Point X="-4.731331542969" Y="0.957527526855" />
                  <Point X="-4.707253417969" Y="1.046382446289" />
                  <Point X="-4.6335859375" Y="1.318237182617" />
                  <Point X="-3.967031005859" Y="1.230483398438" />
                  <Point X="-3.778066162109" Y="1.20560559082" />
                  <Point X="-3.76773828125" Y="1.204815429688" />
                  <Point X="-3.747058105469" Y="1.204364257812" />
                  <Point X="-3.736705810547" Y="1.204703125" />
                  <Point X="-3.715144287109" Y="1.20658984375" />
                  <Point X="-3.704891113281" Y="1.208053588867" />
                  <Point X="-3.684604980469" Y="1.212088745117" />
                  <Point X="-3.663658935547" Y="1.218100830078" />
                  <Point X="-3.613145996094" Y="1.23402746582" />
                  <Point X="-3.603451660156" Y="1.237676147461" />
                  <Point X="-3.584518310547" Y="1.246006958008" />
                  <Point X="-3.575279541016" Y="1.250689086914" />
                  <Point X="-3.55653515625" Y="1.261510864258" />
                  <Point X="-3.547860107422" Y="1.267171264648" />
                  <Point X="-3.531178710938" Y="1.279402709961" />
                  <Point X="-3.515928466797" Y="1.293377075195" />
                  <Point X="-3.502290039063" Y="1.308928710938" />
                  <Point X="-3.495895019531" Y="1.317077270508" />
                  <Point X="-3.483480712891" Y="1.334807128906" />
                  <Point X="-3.478011230469" Y="1.343603027344" />
                  <Point X="-3.468062011719" Y="1.36173840332" />
                  <Point X="-3.459203613281" Y="1.381649414062" />
                  <Point X="-3.438935058594" Y="1.430581787109" />
                  <Point X="-3.435499267578" Y="1.440352050781" />
                  <Point X="-3.429711425781" Y="1.460209106445" />
                  <Point X="-3.427359375" Y="1.470295898438" />
                  <Point X="-3.423600830078" Y="1.491611083984" />
                  <Point X="-3.422360839844" Y="1.501894042969" />
                  <Point X="-3.4210078125" Y="1.522534545898" />
                  <Point X="-3.42191015625" Y="1.543200805664" />
                  <Point X="-3.425056884766" Y="1.563644897461" />
                  <Point X="-3.427188232422" Y="1.573780395508" />
                  <Point X="-3.432790039062" Y="1.594686645508" />
                  <Point X="-3.43601171875" Y="1.604529541016" />
                  <Point X="-3.443508300781" Y="1.623807739258" />
                  <Point X="-3.453066894531" Y="1.643392944336" />
                  <Point X="-3.477522949219" Y="1.690372680664" />
                  <Point X="-3.482799804688" Y="1.699286254883" />
                  <Point X="-3.494292236328" Y="1.716485961914" />
                  <Point X="-3.5005078125" Y="1.724771850586" />
                  <Point X="-3.514420410156" Y="1.741352050781" />
                  <Point X="-3.521500976563" Y="1.748911987305" />
                  <Point X="-3.536442626953" Y="1.763215209961" />
                  <Point X="-3.544303710938" Y="1.769958740234" />
                  <Point X="-4.227614746094" Y="2.294282470703" />
                  <Point X="-4.002293457031" Y="2.680312255859" />
                  <Point X="-3.938518554688" Y="2.762284912109" />
                  <Point X="-3.726338378906" Y="3.035012939453" />
                  <Point X="-3.370213867188" Y="2.829404052734" />
                  <Point X="-3.254156982422" Y="2.7623984375" />
                  <Point X="-3.244917480469" Y="2.757716064453" />
                  <Point X="-3.225984619141" Y="2.749385498047" />
                  <Point X="-3.216291259766" Y="2.745737304688" />
                  <Point X="-3.195648925781" Y="2.739228759766" />
                  <Point X="-3.185615722656" Y="2.736657226562" />
                  <Point X="-3.165328857422" Y="2.732621826172" />
                  <Point X="-3.139875976562" Y="2.729828125" />
                  <Point X="-3.069525878906" Y="2.723673339844" />
                  <Point X="-3.059173583984" Y="2.723334472656" />
                  <Point X="-3.038492919922" Y="2.723785644531" />
                  <Point X="-3.028164550781" Y="2.724575683594" />
                  <Point X="-3.006705810547" Y="2.727400878906" />
                  <Point X="-2.996524902344" Y="2.729310791016" />
                  <Point X="-2.976432861328" Y="2.734227539062" />
                  <Point X="-2.956996826172" Y="2.741302001953" />
                  <Point X="-2.938445068359" Y="2.750450683594" />
                  <Point X="-2.929418212891" Y="2.755531738281" />
                  <Point X="-2.9111640625" Y="2.767161132812" />
                  <Point X="-2.902745605469" Y="2.773193847656" />
                  <Point X="-2.886615478516" Y="2.786139648438" />
                  <Point X="-2.868115234375" Y="2.803841064453" />
                  <Point X="-2.818180175781" Y="2.853776123047" />
                  <Point X="-2.811266113281" Y="2.861488525391" />
                  <Point X="-2.798318603516" Y="2.877620361328" />
                  <Point X="-2.79228515625" Y="2.886039794922" />
                  <Point X="-2.780655761719" Y="2.904293945312" />
                  <Point X="-2.775574707031" Y="2.913320800781" />
                  <Point X="-2.766426025391" Y="2.931872558594" />
                  <Point X="-2.7593515625" Y="2.95130859375" />
                  <Point X="-2.754434814453" Y="2.971400634766" />
                  <Point X="-2.752524902344" Y="2.981581542969" />
                  <Point X="-2.749699707031" Y="3.003040283203" />
                  <Point X="-2.748909667969" Y="3.013368652344" />
                  <Point X="-2.748458496094" Y="3.034049316406" />
                  <Point X="-2.750127197266" Y="3.059600830078" />
                  <Point X="-2.756281982422" Y="3.129951171875" />
                  <Point X="-2.757745849609" Y="3.140204833984" />
                  <Point X="-2.76178125" Y="3.160491699219" />
                  <Point X="-2.764352783203" Y="3.170524902344" />
                  <Point X="-2.770861328125" Y="3.191167236328" />
                  <Point X="-2.774509521484" Y="3.200860595703" />
                  <Point X="-2.782840087891" Y="3.219793457031" />
                  <Point X="-2.787522460938" Y="3.229032958984" />
                  <Point X="-3.059387207031" Y="3.699916259766" />
                  <Point X="-2.648373291016" Y="4.015036865234" />
                  <Point X="-2.547936035156" Y="4.070837890625" />
                  <Point X="-2.192524658203" Y="4.268296386719" />
                  <Point X="-2.154092773438" Y="4.2182109375" />
                  <Point X="-2.118563964844" Y="4.171909179688" />
                  <Point X="-2.111820800781" Y="4.164048339844" />
                  <Point X="-2.097517822266" Y="4.149106933594" />
                  <Point X="-2.089958007813" Y="4.142026367188" />
                  <Point X="-2.073377929688" Y="4.128113769531" />
                  <Point X="-2.065093261719" Y="4.121898925781" />
                  <Point X="-2.047893920898" Y="4.11040625" />
                  <Point X="-2.022062744141" Y="4.096322265625" />
                  <Point X="-1.943763061523" Y="4.055561523438" />
                  <Point X="-1.934327026367" Y="4.051286132812" />
                  <Point X="-1.915047241211" Y="4.0437890625" />
                  <Point X="-1.905203857422" Y="4.040567382812" />
                  <Point X="-1.884297363281" Y="4.034965820313" />
                  <Point X="-1.874162597656" Y="4.032834716797" />
                  <Point X="-1.853719604492" Y="4.029688232422" />
                  <Point X="-1.833053344727" Y="4.028785888672" />
                  <Point X="-1.81241394043" Y="4.030138916016" />
                  <Point X="-1.802132568359" Y="4.031378662109" />
                  <Point X="-1.780817260742" Y="4.035136962891" />
                  <Point X="-1.770729003906" Y="4.037489257812" />
                  <Point X="-1.750868041992" Y="4.043278320312" />
                  <Point X="-1.723475097656" Y="4.054013916016" />
                  <Point X="-1.641921142578" Y="4.087794677734" />
                  <Point X="-1.632584228516" Y="4.0922734375" />
                  <Point X="-1.614452636719" Y="4.102220703125" />
                  <Point X="-1.605657958984" Y="4.107689453125" />
                  <Point X="-1.587928100586" Y="4.120103515625" />
                  <Point X="-1.579779541016" Y="4.126498535156" />
                  <Point X="-1.564227294922" Y="4.140137207031" />
                  <Point X="-1.550252319336" Y="4.155387695312" />
                  <Point X="-1.538020874023" Y="4.172069335938" />
                  <Point X="-1.532360351562" Y="4.180744140625" />
                  <Point X="-1.521538330078" Y="4.19948828125" />
                  <Point X="-1.516856201172" Y="4.208727539062" />
                  <Point X="-1.508525756836" Y="4.22766015625" />
                  <Point X="-1.499142456055" Y="4.255542480469" />
                  <Point X="-1.472598144531" Y="4.339729980469" />
                  <Point X="-1.470026611328" Y="4.349763671875" />
                  <Point X="-1.465990966797" Y="4.370051269531" />
                  <Point X="-1.46452722168" Y="4.380305175781" />
                  <Point X="-1.46264074707" Y="4.4018671875" />
                  <Point X="-1.462301757812" Y="4.412219238281" />
                  <Point X="-1.462752929688" Y="4.432898925781" />
                  <Point X="-1.46354309082" Y="4.4432265625" />
                  <Point X="-1.479266113281" Y="4.562654785156" />
                  <Point X="-0.931174926758" Y="4.716320800781" />
                  <Point X="-0.809426940918" Y="4.730569335938" />
                  <Point X="-0.365221954346" Y="4.782557128906" />
                  <Point X="-0.25999911499" Y="4.389859863281" />
                  <Point X="-0.22566633606" Y="4.261727539062" />
                  <Point X="-0.220435256958" Y="4.247107910156" />
                  <Point X="-0.207661849976" Y="4.218916503906" />
                  <Point X="-0.200119247437" Y="4.205344726562" />
                  <Point X="-0.182260864258" Y="4.178618164062" />
                  <Point X="-0.17260824585" Y="4.166456054688" />
                  <Point X="-0.151451339722" Y="4.1438671875" />
                  <Point X="-0.126897941589" Y="4.125026367188" />
                  <Point X="-0.099602897644" Y="4.110436523438" />
                  <Point X="-0.085356651306" Y="4.104260742188" />
                  <Point X="-0.054918682098" Y="4.093927978516" />
                  <Point X="-0.039857234955" Y="4.090155273438" />
                  <Point X="-0.009319986343" Y="4.08511328125" />
                  <Point X="0.021631772995" Y="4.08511328125" />
                  <Point X="0.052169025421" Y="4.090155273438" />
                  <Point X="0.06723046875" Y="4.093927978516" />
                  <Point X="0.097668441772" Y="4.104260742188" />
                  <Point X="0.111914535522" Y="4.110436523438" />
                  <Point X="0.139209732056" Y="4.125026367188" />
                  <Point X="0.163763122559" Y="4.143866699219" />
                  <Point X="0.184920181274" Y="4.166455566406" />
                  <Point X="0.1945730896" Y="4.178618164062" />
                  <Point X="0.212431182861" Y="4.205344726562" />
                  <Point X="0.219973495483" Y="4.218916015625" />
                  <Point X="0.232747039795" Y="4.247107421875" />
                  <Point X="0.237978271484" Y="4.261727539062" />
                  <Point X="0.378190246582" Y="4.785006347656" />
                  <Point X="0.827877258301" Y="4.737912109375" />
                  <Point X="0.928614746094" Y="4.713590820313" />
                  <Point X="1.453596435547" Y="4.586844238281" />
                  <Point X="1.516975585938" Y="4.563855957031" />
                  <Point X="1.858257080078" Y="4.440070800781" />
                  <Point X="1.921666381836" Y="4.410416015625" />
                  <Point X="2.250446044922" Y="4.256656738281" />
                  <Point X="2.311767089844" Y="4.220931152344" />
                  <Point X="2.629434570312" Y="4.035857666016" />
                  <Point X="2.687211181641" Y="3.994770019531" />
                  <Point X="2.817779785156" Y="3.901916748047" />
                  <Point X="2.218389892578" Y="2.863742675781" />
                  <Point X="2.06530859375" Y="2.598597900391" />
                  <Point X="2.060963867188" Y="2.590116455078" />
                  <Point X="2.049672363281" Y="2.563886962891" />
                  <Point X="2.040856445312" Y="2.537539550781" />
                  <Point X="2.037487060547" Y="2.526334472656" />
                  <Point X="2.01983190918" Y="2.460312255859" />
                  <Point X="2.017387451172" Y="2.447921875" />
                  <Point X="2.014163696289" Y="2.422924804688" />
                  <Point X="2.013384399414" Y="2.410318115234" />
                  <Point X="2.013534057617" Y="2.379081542969" />
                  <Point X="2.01489831543" Y="2.35724609375" />
                  <Point X="2.021782226562" Y="2.300155517578" />
                  <Point X="2.023800537109" Y="2.289034667969" />
                  <Point X="2.029143310547" Y="2.267111572266" />
                  <Point X="2.032468017578" Y="2.256309326172" />
                  <Point X="2.040734130859" Y="2.234220458984" />
                  <Point X="2.045318237305" Y="2.223888671875" />
                  <Point X="2.055681152344" Y="2.20384375" />
                  <Point X="2.069091552734" Y="2.1828828125" />
                  <Point X="2.104417236328" Y="2.130822021484" />
                  <Point X="2.112265625" Y="2.120778320313" />
                  <Point X="2.129236572266" Y="2.101831787109" />
                  <Point X="2.138359130859" Y="2.092928955078" />
                  <Point X="2.162906738281" Y="2.071996826172" />
                  <Point X="2.179505859375" Y="2.059348876953" />
                  <Point X="2.231566894531" Y="2.0240234375" />
                  <Point X="2.241280517578" Y="2.018244873047" />
                  <Point X="2.261325683594" Y="2.007882080078" />
                  <Point X="2.271657226562" Y="2.003297851562" />
                  <Point X="2.293745605469" Y="1.995032104492" />
                  <Point X="2.304547363281" Y="1.991707519531" />
                  <Point X="2.326470214844" Y="1.986364868164" />
                  <Point X="2.34992578125" Y="1.98285925293" />
                  <Point X="2.407016113281" Y="1.975974975586" />
                  <Point X="2.420008789062" Y="1.975305541992" />
                  <Point X="2.445963867188" Y="1.975748291016" />
                  <Point X="2.458926269531" Y="1.976860229492" />
                  <Point X="2.492093017578" Y="1.982012451172" />
                  <Point X="2.512012451172" Y="1.986210449219" />
                  <Point X="2.578034667969" Y="2.003865600586" />
                  <Point X="2.583994384766" Y="2.005670654297" />
                  <Point X="2.604404541016" Y="2.013067260742" />
                  <Point X="2.627655029297" Y="2.02357409668" />
                  <Point X="2.636033935547" Y="2.027872558594" />
                  <Point X="3.863272216797" Y="2.736418945312" />
                  <Point X="3.940405029297" Y="2.780951416016" />
                  <Point X="4.043951660156" Y="2.637045166016" />
                  <Point X="4.076163330078" Y="2.583814697266" />
                  <Point X="4.136884765625" Y="2.483472412109" />
                  <Point X="3.374497558594" Y="1.898471557617" />
                  <Point X="3.172951660156" Y="1.743819702148" />
                  <Point X="3.165542236328" Y="1.737505371094" />
                  <Point X="3.144682128906" Y="1.717110961914" />
                  <Point X="3.125171386719" Y="1.69446887207" />
                  <Point X="3.118310546875" Y="1.686029907227" />
                  <Point X="3.070794189453" Y="1.624041259766" />
                  <Point X="3.063647949219" Y="1.6132734375" />
                  <Point X="3.050883056641" Y="1.590867919922" />
                  <Point X="3.045264404297" Y="1.579230102539" />
                  <Point X="3.032970947266" Y="1.548604248047" />
                  <Point X="3.026316650391" Y="1.528998535156" />
                  <Point X="3.008616699219" Y="1.465708007812" />
                  <Point X="3.006225585938" Y="1.454662109375" />
                  <Point X="3.002771972656" Y="1.432363647461" />
                  <Point X="3.001709472656" Y="1.421110961914" />
                  <Point X="3.000893310547" Y="1.397540527344" />
                  <Point X="3.001174804688" Y="1.386240966797" />
                  <Point X="3.003077880859" Y="1.363756103516" />
                  <Point X="3.007838623047" Y="1.337356811523" />
                  <Point X="3.022368408203" Y="1.266937988281" />
                  <Point X="3.025874267578" Y="1.254376464844" />
                  <Point X="3.034573486328" Y="1.22985168457" />
                  <Point X="3.039766845703" Y="1.217888305664" />
                  <Point X="3.055502929688" Y="1.187532958984" />
                  <Point X="3.06545703125" Y="1.170547607422" />
                  <Point X="3.1049765625" Y="1.110479980469" />
                  <Point X="3.111739501953" Y="1.101424072266" />
                  <Point X="3.126292724609" Y="1.0841796875" />
                  <Point X="3.134083007812" Y="1.075991333008" />
                  <Point X="3.151326904297" Y="1.059901367188" />
                  <Point X="3.160034179688" Y="1.052696289062" />
                  <Point X="3.178243164062" Y="1.039370849609" />
                  <Point X="3.200118408203" Y="1.026285400391" />
                  <Point X="3.257387695312" Y="0.994047729492" />
                  <Point X="3.269415283203" Y="0.988347167969" />
                  <Point X="3.294149414063" Y="0.978683105469" />
                  <Point X="3.306854980469" Y="0.974719665527" />
                  <Point X="3.341559570312" Y="0.966510742188" />
                  <Point X="3.360400390625" Y="0.963046325684" />
                  <Point X="3.43783203125" Y="0.952812927246" />
                  <Point X="3.444030029297" Y="0.952199707031" />
                  <Point X="3.465716308594" Y="0.951222961426" />
                  <Point X="3.491217529297" Y="0.952032348633" />
                  <Point X="3.500603515625" Y="0.952797180176" />
                  <Point X="4.666397949219" Y="1.106277099609" />
                  <Point X="4.704704101562" Y="1.11132019043" />
                  <Point X="4.75268359375" Y="0.914236022949" />
                  <Point X="4.762834472656" Y="0.849037109375" />
                  <Point X="4.78387109375" Y="0.713920959473" />
                  <Point X="3.922968017578" Y="0.483242736816" />
                  <Point X="3.691991943359" Y="0.421352874756" />
                  <Point X="3.682561523438" Y="0.418290374756" />
                  <Point X="3.655014160156" Y="0.407171447754" />
                  <Point X="3.626497558594" Y="0.392663757324" />
                  <Point X="3.617568603516" Y="0.38781628418" />
                  <Point X="3.541494384766" Y="0.343844116211" />
                  <Point X="3.530563720703" Y="0.336468841553" />
                  <Point X="3.509828369141" Y="0.320275634766" />
                  <Point X="3.500023681641" Y="0.311457550049" />
                  <Point X="3.475850341797" Y="0.286471343994" />
                  <Point X="3.462935791016" Y="0.271661071777" />
                  <Point X="3.417291259766" Y="0.213499450684" />
                  <Point X="3.410854492188" Y="0.204208282471" />
                  <Point X="3.399130371094" Y="0.184928024292" />
                  <Point X="3.393843017578" Y="0.174939239502" />
                  <Point X="3.384069091797" Y="0.153475418091" />
                  <Point X="3.380005371094" Y="0.142929275513" />
                  <Point X="3.373159179688" Y="0.121429046631" />
                  <Point X="3.367089355469" Y="0.09331048584" />
                  <Point X="3.351874511719" Y="0.013864260674" />
                  <Point X="3.35029296875" Y="0.000651833653" />
                  <Point X="3.348991455078" Y="-0.025863235474" />
                  <Point X="3.349271484375" Y="-0.039166172028" />
                  <Point X="3.352558837891" Y="-0.075139602661" />
                  <Point X="3.355161376953" Y="-0.093586532593" />
                  <Point X="3.370376220703" Y="-0.173032897949" />
                  <Point X="3.373158935547" Y="-0.18398789978" />
                  <Point X="3.380005371094" Y="-0.205489318848" />
                  <Point X="3.384069091797" Y="-0.216035751343" />
                  <Point X="3.393843017578" Y="-0.237499572754" />
                  <Point X="3.399130859375" Y="-0.247489105225" />
                  <Point X="3.410854492188" Y="-0.266768188477" />
                  <Point X="3.427151367188" Y="-0.288623474121" />
                  <Point X="3.472795898438" Y="-0.346785247803" />
                  <Point X="3.481919433594" Y="-0.356839752197" />
                  <Point X="3.501506347656" Y="-0.37554498291" />
                  <Point X="3.511969970703" Y="-0.384196166992" />
                  <Point X="3.542717773438" Y="-0.406116851807" />
                  <Point X="3.557930175781" Y="-0.415904418945" />
                  <Point X="3.634004394531" Y="-0.459876739502" />
                  <Point X="3.639496582031" Y="-0.462815460205" />
                  <Point X="3.659158447266" Y="-0.472016845703" />
                  <Point X="3.683028076172" Y="-0.481027862549" />
                  <Point X="3.691991943359" Y="-0.48391293335" />
                  <Point X="4.761078125" Y="-0.770373596191" />
                  <Point X="4.784876953125" Y="-0.776750427246" />
                  <Point X="4.76161328125" Y="-0.93105279541" />
                  <Point X="4.748608398438" Y="-0.988042724609" />
                  <Point X="4.727801757812" Y="-1.079219604492" />
                  <Point X="3.704649169922" Y="-0.944519042969" />
                  <Point X="3.436781982422" Y="-0.909253845215" />
                  <Point X="3.428624511719" Y="-0.908535827637" />
                  <Point X="3.400098632812" Y="-0.908042541504" />
                  <Point X="3.366721435547" Y="-0.910840820312" />
                  <Point X="3.354480712891" Y="-0.912676330566" />
                  <Point X="3.322223144531" Y="-0.91968762207" />
                  <Point X="3.172916503906" Y="-0.952140014648" />
                  <Point X="3.157874023438" Y="-0.956742553711" />
                  <Point X="3.12875390625" Y="-0.968366943359" />
                  <Point X="3.114676269531" Y="-0.975388916016" />
                  <Point X="3.086849609375" Y="-0.99228125" />
                  <Point X="3.074123779297" Y="-1.001530517578" />
                  <Point X="3.050373779297" Y="-1.022001403809" />
                  <Point X="3.039349609375" Y="-1.033223144531" />
                  <Point X="3.019851806641" Y="-1.056673095703" />
                  <Point X="2.92960546875" Y="-1.165211303711" />
                  <Point X="2.921326171875" Y="-1.176847290039" />
                  <Point X="2.90660546875" Y="-1.201229492188" />
                  <Point X="2.9001640625" Y="-1.213975708008" />
                  <Point X="2.888820800781" Y="-1.241360717773" />
                  <Point X="2.884362792969" Y="-1.254928466797" />
                  <Point X="2.877531005859" Y="-1.282578369141" />
                  <Point X="2.875157226562" Y="-1.296660766602" />
                  <Point X="2.872362792969" Y="-1.327029052734" />
                  <Point X="2.859428222656" Y="-1.467590942383" />
                  <Point X="2.859288818359" Y="-1.483321655273" />
                  <Point X="2.861607666016" Y="-1.514590576172" />
                  <Point X="2.864065917969" Y="-1.530128662109" />
                  <Point X="2.871797607422" Y="-1.561749633789" />
                  <Point X="2.876786621094" Y="-1.576669189453" />
                  <Point X="2.889157958984" Y="-1.605479980469" />
                  <Point X="2.896540283203" Y="-1.61937121582" />
                  <Point X="2.914392089844" Y="-1.647138793945" />
                  <Point X="2.997020507812" Y="-1.775661743164" />
                  <Point X="3.0017421875" Y="-1.782353515625" />
                  <Point X="3.019793212891" Y="-1.80444921875" />
                  <Point X="3.043488769531" Y="-1.828119873047" />
                  <Point X="3.052796142578" Y="-1.836277709961" />
                  <Point X="4.044914794922" Y="-2.597557617188" />
                  <Point X="4.087170654297" Y="-2.629981933594" />
                  <Point X="4.045489013672" Y="-2.697428955078" />
                  <Point X="4.018591796875" Y="-2.735645996094" />
                  <Point X="4.001274414063" Y="-2.760251708984" />
                  <Point X="3.087026611328" Y="-2.232410400391" />
                  <Point X="2.848454589844" Y="-2.094670654297" />
                  <Point X="2.841191894531" Y="-2.090885253906" />
                  <Point X="2.815026367188" Y="-2.079513427734" />
                  <Point X="2.783118896484" Y="-2.069325927734" />
                  <Point X="2.771107421875" Y="-2.066337158203" />
                  <Point X="2.732715576172" Y="-2.059403808594" />
                  <Point X="2.555017089844" Y="-2.027311645508" />
                  <Point X="2.539357910156" Y="-2.025807250977" />
                  <Point X="2.508006103516" Y="-2.025403198242" />
                  <Point X="2.492313476562" Y="-2.026503662109" />
                  <Point X="2.460140625" Y="-2.031461425781" />
                  <Point X="2.444844482422" Y="-2.035136352539" />
                  <Point X="2.415068847656" Y="-2.044960083008" />
                  <Point X="2.400589355469" Y="-2.051108642578" />
                  <Point X="2.368695068359" Y="-2.067894287109" />
                  <Point X="2.221071289062" Y="-2.145587646484" />
                  <Point X="2.20896875" Y="-2.153170166016" />
                  <Point X="2.186037841797" Y="-2.170063232422" />
                  <Point X="2.175209472656" Y="-2.179373779297" />
                  <Point X="2.154249755859" Y="-2.200333496094" />
                  <Point X="2.144938964844" Y="-2.211162109375" />
                  <Point X="2.128046142578" Y="-2.234092773438" />
                  <Point X="2.120464111328" Y="-2.246194824219" />
                  <Point X="2.103678466797" Y="-2.278088867188" />
                  <Point X="2.025984985352" Y="-2.425712890625" />
                  <Point X="2.019836303711" Y="-2.440192138672" />
                  <Point X="2.010012329102" Y="-2.469968017578" />
                  <Point X="2.006337280273" Y="-2.485264648438" />
                  <Point X="2.001379516602" Y="-2.5174375" />
                  <Point X="2.000279174805" Y="-2.533130126953" />
                  <Point X="2.000683227539" Y="-2.564481933594" />
                  <Point X="2.002187744141" Y="-2.580141113281" />
                  <Point X="2.00912097168" Y="-2.618532958984" />
                  <Point X="2.041213256836" Y="-2.796231445312" />
                  <Point X="2.043015014648" Y="-2.804221679688" />
                  <Point X="2.051236083984" Y="-2.83154296875" />
                  <Point X="2.064069580078" Y="-2.862479736328" />
                  <Point X="2.069546630859" Y="-2.873578613281" />
                  <Point X="2.707082763672" Y="-3.977822998047" />
                  <Point X="2.735893066406" Y="-4.027724121094" />
                  <Point X="2.723754394531" Y="-4.036083740234" />
                  <Point X="2.015967163086" Y="-3.113677978516" />
                  <Point X="1.833914550781" Y="-2.876422851562" />
                  <Point X="1.828653564453" Y="-2.870146240234" />
                  <Point X="1.808831665039" Y="-2.849626953125" />
                  <Point X="1.783252319336" Y="-2.828004882812" />
                  <Point X="1.773299072266" Y="-2.820647216797" />
                  <Point X="1.735434448242" Y="-2.796303710938" />
                  <Point X="1.56017565918" Y="-2.683628417969" />
                  <Point X="1.546283935547" Y="-2.676245849609" />
                  <Point X="1.517473022461" Y="-2.663874511719" />
                  <Point X="1.502553588867" Y="-2.658885742188" />
                  <Point X="1.470932617188" Y="-2.651154052734" />
                  <Point X="1.45539440918" Y="-2.648695800781" />
                  <Point X="1.424125244141" Y="-2.646376953125" />
                  <Point X="1.408394287109" Y="-2.646516357422" />
                  <Point X="1.366982666016" Y="-2.650327148438" />
                  <Point X="1.175307373047" Y="-2.667965332031" />
                  <Point X="1.161224975586" Y="-2.670339111328" />
                  <Point X="1.133575439453" Y="-2.677170898438" />
                  <Point X="1.120008422852" Y="-2.68162890625" />
                  <Point X="1.092623168945" Y="-2.692972167969" />
                  <Point X="1.079877563477" Y="-2.699413330078" />
                  <Point X="1.055495361328" Y="-2.714133789062" />
                  <Point X="1.043858764648" Y="-2.722413085938" />
                  <Point X="1.011882080078" Y="-2.749000732422" />
                  <Point X="0.863875061035" Y="-2.872063476562" />
                  <Point X="0.852653320312" Y="-2.883087890625" />
                  <Point X="0.832182128906" Y="-2.906838134766" />
                  <Point X="0.822933044434" Y="-2.919564208984" />
                  <Point X="0.806040710449" Y="-2.947390869141" />
                  <Point X="0.799018859863" Y="-2.961468261719" />
                  <Point X="0.787394165039" Y="-2.990588867188" />
                  <Point X="0.782791625977" Y="-3.005631835938" />
                  <Point X="0.773230834961" Y="-3.049619628906" />
                  <Point X="0.728977355957" Y="-3.253219726562" />
                  <Point X="0.727584594727" Y="-3.261290039062" />
                  <Point X="0.724724487305" Y="-3.289677734375" />
                  <Point X="0.7247421875" Y="-3.323170410156" />
                  <Point X="0.725554992676" Y="-3.335520019531" />
                  <Point X="0.833091552734" Y="-4.152340820313" />
                  <Point X="0.726234375" Y="-3.753544433594" />
                  <Point X="0.655064941406" Y="-3.487936523438" />
                  <Point X="0.652606262207" Y="-3.480124511719" />
                  <Point X="0.642145019531" Y="-3.453580322266" />
                  <Point X="0.62678704834" Y="-3.423815917969" />
                  <Point X="0.620407714844" Y="-3.413210693359" />
                  <Point X="0.591319274902" Y="-3.371299316406" />
                  <Point X="0.456680145264" Y="-3.177310302734" />
                  <Point X="0.44667098999" Y="-3.165173339844" />
                  <Point X="0.424787139893" Y="-3.142717773438" />
                  <Point X="0.412912322998" Y="-3.132399169922" />
                  <Point X="0.386657196045" Y="-3.113155273438" />
                  <Point X="0.373242340088" Y="-3.104937744141" />
                  <Point X="0.345241210938" Y="-3.090829589844" />
                  <Point X="0.330654907227" Y="-3.084938964844" />
                  <Point X="0.285641937256" Y="-3.070968505859" />
                  <Point X="0.077295951843" Y="-3.006305664062" />
                  <Point X="0.063377418518" Y="-3.003109619141" />
                  <Point X="0.035217838287" Y="-2.998840087891" />
                  <Point X="0.020976791382" Y="-2.997766601562" />
                  <Point X="-0.008664708138" Y="-2.997766601562" />
                  <Point X="-0.022905158997" Y="-2.998840087891" />
                  <Point X="-0.051064296722" Y="-3.003109375" />
                  <Point X="-0.064982826233" Y="-3.006305175781" />
                  <Point X="-0.109995948792" Y="-3.020275390625" />
                  <Point X="-0.318341918945" Y="-3.084938476562" />
                  <Point X="-0.33292868042" Y="-3.090829345703" />
                  <Point X="-0.360930419922" Y="-3.104937744141" />
                  <Point X="-0.374345275879" Y="-3.113155273438" />
                  <Point X="-0.400600524902" Y="-3.132399169922" />
                  <Point X="-0.412475067139" Y="-3.142717529297" />
                  <Point X="-0.434358734131" Y="-3.165172607422" />
                  <Point X="-0.444367614746" Y="-3.177309326172" />
                  <Point X="-0.473456512451" Y="-3.219220458984" />
                  <Point X="-0.608095336914" Y="-3.413209716797" />
                  <Point X="-0.612470336914" Y="-3.420132324219" />
                  <Point X="-0.625976745605" Y="-3.445265136719" />
                  <Point X="-0.638777832031" Y="-3.476215820312" />
                  <Point X="-0.642752990723" Y="-3.487936523438" />
                  <Point X="-0.972164855957" Y="-4.717318359375" />
                  <Point X="-0.985425231934" Y="-4.766807128906" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.799510510882" Y="3.870273426886" />
                  <Point X="1.338736625225" Y="4.614574897585" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.751945335944" Y="3.787888101451" />
                  <Point X="0.941038028536" Y="4.710591461251" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.704380161007" Y="3.705502776016" />
                  <Point X="0.639429667719" Y="4.757647604293" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.65681498607" Y="3.623117450581" />
                  <Point X="0.377984756562" Y="4.784239447592" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.609249811133" Y="3.540732125146" />
                  <Point X="0.352847666086" Y="4.690426442341" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.968917191963" Y="2.74132599919" />
                  <Point X="3.917313338508" Y="2.767619475818" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.561684636196" Y="3.458346799711" />
                  <Point X="0.327710575609" Y="4.59661343709" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.079448023749" Y="2.578386734866" />
                  <Point X="3.819214618333" Y="2.710982277734" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.514119461258" Y="3.375961474276" />
                  <Point X="0.302573485133" Y="4.502800431839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.104796384755" Y="2.458850107256" />
                  <Point X="3.72111601518" Y="2.654345020025" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.466554286321" Y="3.293576148841" />
                  <Point X="0.277436394656" Y="4.408987426588" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.350910544953" Y="4.729146183431" />
                  <Point X="-0.438828379287" Y="4.773942557488" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.021293447031" Y="2.394775986558" />
                  <Point X="3.623017412028" Y="2.597707762316" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.418989111384" Y="3.211190823406" />
                  <Point X="0.252299304179" Y="4.315174421337" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.317824411648" Y="4.605666963911" />
                  <Point X="-0.608996943475" Y="4.75402677907" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.937790509306" Y="2.330701865861" />
                  <Point X="3.524918808876" Y="2.541070504607" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.371423936447" Y="3.12880549797" />
                  <Point X="0.222222029986" Y="4.223878565414" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.284738278342" Y="4.482187744391" />
                  <Point X="-0.779165507662" Y="4.734111000652" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.854287571582" Y="2.266627745164" />
                  <Point X="3.426820205723" Y="2.484433246897" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.32385876151" Y="3.046420172535" />
                  <Point X="0.165777561671" Y="4.146017465929" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.251652187023" Y="4.358708546264" />
                  <Point X="-0.945579599982" Y="4.712282223269" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.770784633858" Y="2.202553624467" />
                  <Point X="3.328721602571" Y="2.427795989188" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.276293586572" Y="2.9640348471" />
                  <Point X="0.061547338388" Y="4.092504424723" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.213998679557" Y="4.232902133373" />
                  <Point X="-1.080561465779" Y="4.674437926537" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.687281696134" Y="2.13847950377" />
                  <Point X="3.230622999418" Y="2.371158731479" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.228728411635" Y="2.881649521665" />
                  <Point X="-1.215543331576" Y="4.636593629806" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.603778758409" Y="2.074405383072" />
                  <Point X="3.132524396266" Y="2.31452147377" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.181163248109" Y="2.799264190416" />
                  <Point X="-1.350525197373" Y="4.598749333074" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.520275820685" Y="2.010331262375" />
                  <Point X="3.034425793113" Y="2.25788421606" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.133598087752" Y="2.716878857552" />
                  <Point X="-1.478570444062" Y="4.557370652374" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.436772882961" Y="1.946257141678" />
                  <Point X="2.936327189961" Y="2.201246958351" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.086032927396" Y="2.634493524687" />
                  <Point X="-1.463532444845" Y="4.443087416488" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.353269953172" Y="1.882183016937" />
                  <Point X="2.838228586809" Y="2.144609700642" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.044673197952" Y="2.548946366848" />
                  <Point X="-1.472290835939" Y="4.340929047072" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.711470984559" Y="1.083524033341" />
                  <Point X="4.666729826523" Y="1.106320792" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.269767046666" Y="1.818108880334" />
                  <Point X="2.740129983656" Y="2.087972442933" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.018876488568" Y="2.455469454217" />
                  <Point X="-1.501312183882" Y="4.249095169852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.7411031571" Y="0.961804694732" />
                  <Point X="4.500440625743" Y="1.084428379198" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.186264140159" Y="1.754034743731" />
                  <Point X="2.642031380504" Y="2.031335185223" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.015716764571" Y="2.350458421432" />
                  <Point X="-1.54383457758" Y="4.164140419039" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.763667510386" Y="0.843686589907" />
                  <Point X="4.334151425172" Y="1.06253596629" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.116109968005" Y="1.683159087256" />
                  <Point X="2.518128381283" Y="1.98784592402" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.042528552973" Y="2.230176140319" />
                  <Point X="-1.622693109194" Y="4.097699855231" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.781697986989" Y="0.727878610635" />
                  <Point X="4.167862224601" Y="1.040643553382" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.059230456119" Y="1.605519653542" />
                  <Point X="2.296330875332" Y="1.994236405361" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.212563297024" Y="2.036918118352" />
                  <Point X="-1.736325771301" Y="4.048977595893" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.663261882655" Y="0.681603827357" />
                  <Point X="4.00157302403" Y="1.018751140474" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.023053843228" Y="1.517331565911" />
                  <Point X="-2.280304771269" Y="4.219527747792" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.526124287934" Y="0.644857929375" />
                  <Point X="3.835283823459" Y="0.996858727565" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.001752274376" Y="1.42156426478" />
                  <Point X="-2.380408720686" Y="4.16391226504" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.388986693213" Y="0.608112031392" />
                  <Point X="3.668994622888" Y="0.974966314657" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.013721664254" Y="1.308844563447" />
                  <Point X="-2.480512670103" Y="4.108296782288" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.251849098491" Y="0.57136613341" />
                  <Point X="3.502705422317" Y="0.953073901749" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.061015777674" Y="1.178126016473" />
                  <Point X="-2.580616507972" Y="4.052681242698" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.11471150377" Y="0.534620235428" />
                  <Point X="-2.675369349596" Y="3.994339234343" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.977573909049" Y="0.497874337446" />
                  <Point X="-2.758913959754" Y="3.930286346811" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.840436326261" Y="0.461128433383" />
                  <Point X="-2.842458569911" Y="3.866233459279" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.70329875137" Y="0.424382525296" />
                  <Point X="-2.926003180069" Y="3.802180571747" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.593318454094" Y="0.373799293126" />
                  <Point X="-3.009547790227" Y="3.738127684215" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.502335280599" Y="0.313536542923" />
                  <Point X="-3.024201720499" Y="3.638973242049" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.43789284902" Y="0.239750609264" />
                  <Point X="-2.936987927518" Y="3.487914602403" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.386685477325" Y="0.159221075769" />
                  <Point X="-2.849774134537" Y="3.336855962757" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.361728185099" Y="0.065316458733" />
                  <Point X="-2.770432955335" Y="3.189808620185" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.349181931116" Y="-0.034911898142" />
                  <Point X="-2.751339623605" Y="3.073459089178" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.771393089121" Y="-0.866185670275" />
                  <Point X="4.38622217797" Y="-0.669931288639" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.365983076673" Y="-0.150093501959" />
                  <Point X="-2.755084395858" Y="2.968746153368" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.754096333238" Y="-0.963993525534" />
                  <Point X="3.944866947479" Y="-0.551670559012" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.426258839032" Y="-0.287426529444" />
                  <Point X="-2.794888798332" Y="2.882406516856" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.732299816863" Y="-1.059508638307" />
                  <Point X="-2.861984040913" Y="2.809972257915" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.503868969343" Y="-1.049738300621" />
                  <Point X="-2.946563977025" Y="2.746446895306" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.221708038827" Y="-1.012591118245" />
                  <Point X="-3.119747051987" Y="2.728067086845" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.939547108311" Y="-0.97544393587" />
                  <Point X="-3.784555517338" Y="2.960182926406" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.657386260957" Y="-0.938296795867" />
                  <Point X="-3.843958171675" Y="2.883829097983" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.390356455112" Y="-0.90885930659" />
                  <Point X="-3.903360826013" Y="2.80747526956" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.238196799969" Y="-0.937951082484" />
                  <Point X="-3.962763710641" Y="2.731121558476" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.109082135162" Y="-0.978784867437" />
                  <Point X="-4.018341148416" Y="2.652818684865" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.02962411034" Y="-1.044919974199" />
                  <Point X="-4.066308836704" Y="2.570638450226" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.967353315985" Y="-1.11981241229" />
                  <Point X="-4.114276524992" Y="2.488458215587" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.909259702552" Y="-1.196833230368" />
                  <Point X="-4.16224421328" Y="2.406277980948" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.876799979532" Y="-1.286915167981" />
                  <Point X="-4.210211901569" Y="2.324097746309" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.866715601921" Y="-1.388397913522" />
                  <Point X="-3.96408551008" Y="2.092069093478" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.859898685083" Y="-1.491545513481" />
                  <Point X="-3.550509430778" Y="1.774720563196" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.894692920516" Y="-1.615895054505" />
                  <Point X="-3.438548259203" Y="1.611052504348" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.996057529402" Y="-1.774163894986" />
                  <Point X="-3.423009583191" Y="1.496514160893" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.337576461428" Y="-2.054797474913" />
                  <Point X="-3.45007638112" Y="1.403684390699" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.751153466969" Y="-2.372146477137" />
                  <Point X="-3.494175516297" Y="1.319533029797" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.077773134209" Y="-2.645188502476" />
                  <Point X="-3.572015536832" Y="1.252573508674" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.025839538956" Y="-2.725348006586" />
                  <Point X="3.302117566295" Y="-2.356593243157" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.711237738476" Y="-2.05552493329" />
                  <Point X="-3.696982984395" Y="1.20962661099" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.456480697677" Y="-2.032340730141" />
                  <Point X="-3.940253287052" Y="1.226958028725" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.34326742341" Y="-2.081276678256" />
                  <Point X="-4.222414448646" Y="1.26410532884" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.240333367512" Y="-2.135450149732" />
                  <Point X="-4.504575584787" Y="1.301252615987" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.15565691644" Y="-2.198926335513" />
                  <Point X="-4.647365455379" Y="1.267386696409" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.103274202697" Y="-2.278857002323" />
                  <Point X="-4.672752491168" Y="1.173701044655" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.059025928542" Y="-2.36293237312" />
                  <Point X="-4.698139526958" Y="1.080015392902" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.017183230097" Y="-2.448233445962" />
                  <Point X="-4.723526637633" Y="0.986329779305" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.000449265802" Y="-2.546328057858" />
                  <Point X="-4.741492198661" Y="0.888862697288" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.016845267975" Y="-2.66130323081" />
                  <Point X="-3.307887160013" Y="0.051783452998" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.904512597262" Y="0.355779297092" />
                  <Point X="-4.756163253652" Y="0.789716980602" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.038052471786" Y="-2.77872983344" />
                  <Point X="-3.295943290391" Y="-0.060923245115" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.345868060536" Y="0.474040145328" />
                  <Point X="-4.770834308644" Y="0.690571263915" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.092302240082" Y="-2.912992463591" />
                  <Point X="-3.322458377303" Y="-0.154034126113" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.179516032773" Y="-3.064051103089" />
                  <Point X="1.84844842836" Y="-2.895363733138" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.367452245185" Y="-2.6502839367" />
                  <Point X="-3.376644427525" Y="-0.233045947093" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.266729825465" Y="-3.215109742588" />
                  <Point X="1.982782987751" Y="-3.070431602469" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.190207473716" Y="-2.666594207422" />
                  <Point X="-3.464928416038" Y="-0.294684000738" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.353943618156" Y="-3.366168382086" />
                  <Point X="2.117117320579" Y="-3.245499356361" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.064080786219" Y="-2.708950442858" />
                  <Point X="-3.594517060765" Y="-0.335276280859" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.441157410848" Y="-3.517227021585" />
                  <Point X="2.251451579081" Y="-3.420567072382" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.982072683537" Y="-2.773786220051" />
                  <Point X="-3.731654639796" Y="-0.372022186836" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.52837120354" Y="-3.668285661084" />
                  <Point X="2.385785837582" Y="-3.595634788403" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.902563582399" Y="-2.83989530213" />
                  <Point X="-3.868792209879" Y="-0.408768097372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.615584996231" Y="-3.819344300582" />
                  <Point X="2.520120096083" Y="-3.770702504423" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.830157672544" Y="-2.909623640941" />
                  <Point X="-4.005929776244" Y="-0.445514009803" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.702798788923" Y="-3.970402940081" />
                  <Point X="2.654454354584" Y="-3.945770220444" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.786370676273" Y="-2.993934044559" />
                  <Point X="-4.14306734261" Y="-0.482259922233" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.764573738442" Y="-3.089448942588" />
                  <Point X="-4.280204908975" Y="-0.519005834663" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.743709781623" Y="-3.185439218187" />
                  <Point X="-2.953647628538" Y="-1.301541521833" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.327356572052" Y="-1.111127304409" />
                  <Point X="-4.417342475341" Y="-0.555751747094" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.725423200286" Y="-3.282742732187" />
                  <Point X="-2.953123832259" Y="-1.408429401943" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.493645818855" Y="-1.133019693761" />
                  <Point X="-4.554480041706" Y="-0.592497659524" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.733162832113" Y="-3.393307264148" />
                  <Point X="0.537345319143" Y="-3.293533257833" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.036867885484" Y="-3.00095701664" />
                  <Point X="-2.985110537586" Y="-1.498752354109" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.659935065658" Y="-1.154912083113" />
                  <Point X="-4.691617608072" Y="-0.629243571954" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.748209066586" Y="-3.507594696106" />
                  <Point X="0.642125448602" Y="-3.453542392969" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.171031018943" Y="-3.039218478334" />
                  <Point X="-3.047677534099" Y="-1.573493869662" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.826224312461" Y="-1.176804472465" />
                  <Point X="-4.781378535" Y="-0.690129087889" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.763255301058" Y="-3.621882128064" />
                  <Point X="0.679523980582" Y="-3.579218889362" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.301074153848" Y="-3.079579184143" />
                  <Point X="-3.131144005151" Y="-1.637586571057" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.992513559264" Y="-1.198696861817" />
                  <Point X="-4.764930940933" Y="-0.805130548225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.778301535531" Y="-3.736169560022" />
                  <Point X="0.712610146116" Y="-3.702698125303" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.402841981142" Y="-3.134346878772" />
                  <Point X="-3.214646934457" Y="-1.701660696044" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.158802806067" Y="-1.220589251168" />
                  <Point X="-4.748483336903" Y="-0.920132013636" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.793347770004" Y="-3.85045699198" />
                  <Point X="0.745696310618" Y="-3.826177360718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.466158998937" Y="-3.208706239395" />
                  <Point X="-3.298149863831" Y="-1.765734820996" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.325092052871" Y="-1.24248164052" />
                  <Point X="-4.724150710743" Y="-1.039151098493" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.808394004476" Y="-3.964744423939" />
                  <Point X="0.778782474398" Y="-3.949656595765" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.520827030697" Y="-3.287472478514" />
                  <Point X="-3.38165279344" Y="-1.829808945827" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.491381299674" Y="-1.264374029872" />
                  <Point X="-4.692841514544" Y="-1.161724923335" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.823440238949" Y="-4.079031855897" />
                  <Point X="0.811868638179" Y="-4.073135830813" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.575494997471" Y="-3.366238750746" />
                  <Point X="-2.361603227295" Y="-2.456171152099" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.572395551767" Y="-2.348767098223" />
                  <Point X="-3.46515572305" Y="-1.893883070659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.657670450597" Y="-1.286266468077" />
                  <Point X="-4.661532318346" Y="-1.284298748177" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.626616531099" Y="-3.446812020921" />
                  <Point X="-2.311981818772" Y="-2.588075515157" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.703457009203" Y="-2.388608942787" />
                  <Point X="-3.54865865266" Y="-1.957957195491" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.656256119983" Y="-3.538330888647" />
                  <Point X="-2.325414144018" Y="-2.687852396173" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.801555603215" Y="-2.445246205153" />
                  <Point X="-3.63216158227" Y="-2.022031320323" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.681393235225" Y="-3.63214388128" />
                  <Point X="-2.369220735181" Y="-2.772152815695" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.899654197227" Y="-2.50188346752" />
                  <Point X="-3.715664511879" Y="-2.086105445154" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.706530350467" Y="-3.725956873912" />
                  <Point X="-2.416785911447" Y="-2.854538140453" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.997752791239" Y="-2.558520729886" />
                  <Point X="-3.799167441489" Y="-2.150179569986" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.731667465709" Y="-3.819769866545" />
                  <Point X="-2.464351087714" Y="-2.936923465211" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.095851385251" Y="-2.615157992253" />
                  <Point X="-3.882670371099" Y="-2.214253694818" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.756804580951" Y="-3.913582859178" />
                  <Point X="-2.511916284848" Y="-3.019308779336" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.193949979263" Y="-2.671795254619" />
                  <Point X="-3.966173300709" Y="-2.27832781965" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.781941696193" Y="-4.00739585181" />
                  <Point X="-2.559481487121" Y="-3.101694090843" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.292048573276" Y="-2.728432516986" />
                  <Point X="-4.049676230318" Y="-2.342401944481" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.807078811435" Y="-4.101208844443" />
                  <Point X="-2.607046689393" Y="-3.18407940235" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.390147167288" Y="-2.785069779352" />
                  <Point X="-4.133179159928" Y="-2.406476069313" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.832215926677" Y="-4.195021837075" />
                  <Point X="-1.487532442521" Y="-3.861121394779" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.601323222394" Y="-3.803142096516" />
                  <Point X="-2.654611891666" Y="-3.266464713857" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.4882457613" Y="-2.841707041718" />
                  <Point X="-4.142525014699" Y="-2.508335111036" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.857353041919" Y="-4.288834829708" />
                  <Point X="-1.226503589619" Y="-4.10074323096" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.851935328647" Y="-3.782069843004" />
                  <Point X="-2.702177093939" Y="-3.348850025364" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.586344355312" Y="-2.898344304085" />
                  <Point X="-4.051187013413" Y="-2.661495139772" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.882490157161" Y="-4.382647822341" />
                  <Point X="-1.175961035753" Y="-4.233116941012" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.042609986051" Y="-3.791537245058" />
                  <Point X="-2.749742296212" Y="-3.431235336871" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.684442949324" Y="-2.954981566451" />
                  <Point X="-3.939911421336" Y="-2.824813878418" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.907627272403" Y="-4.476460814973" />
                  <Point X="-1.152360873857" Y="-4.351762816686" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.142503058093" Y="-3.8472601752" />
                  <Point X="-2.797307498484" Y="-3.513620648378" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.782541525818" Y="-3.011618837744" />
                  <Point X="-3.807342917266" Y="-2.998981897618" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.932764387645" Y="-4.570273807606" />
                  <Point X="-1.128760754028" Y="-4.470408670925" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.233035752353" Y="-3.907752456038" />
                  <Point X="-2.844872700757" Y="-3.596005959885" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.957901502886" Y="-4.664086800239" />
                  <Point X="-1.121424592955" Y="-4.580767624269" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.323568446614" Y="-3.968244736876" />
                  <Point X="-2.89243790303" Y="-3.678391271392" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.983038546225" Y="-4.757899829508" />
                  <Point X="-1.134579205628" Y="-4.680686006909" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.403811372425" Y="-4.033979916609" />
                  <Point X="-2.940003105302" Y="-3.760776582899" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.463196755138" Y="-4.110342545364" />
                  <Point X="-2.923149463686" Y="-3.875984934795" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="0.001626220703" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.542708435059" Y="-3.802720214844" />
                  <Point X="0.471539031982" Y="-3.537112304688" />
                  <Point X="0.464318664551" Y="-3.521544189453" />
                  <Point X="0.435230072021" Y="-3.4796328125" />
                  <Point X="0.300591033936" Y="-3.285643798828" />
                  <Point X="0.274335754395" Y="-3.266399902344" />
                  <Point X="0.22932270813" Y="-3.252429443359" />
                  <Point X="0.02097677803" Y="-3.187766601562" />
                  <Point X="-0.008664756775" Y="-3.187766601562" />
                  <Point X="-0.053677947998" Y="-3.201736816406" />
                  <Point X="-0.262023895264" Y="-3.266399902344" />
                  <Point X="-0.288279144287" Y="-3.285643798828" />
                  <Point X="-0.317367889404" Y="-3.327554931641" />
                  <Point X="-0.45200680542" Y="-3.521544189453" />
                  <Point X="-0.459227142334" Y="-3.537112304688" />
                  <Point X="-0.788638977051" Y="-4.766494140625" />
                  <Point X="-0.84774395752" Y="-4.987077148438" />
                  <Point X="-0.855236999512" Y="-4.985622558594" />
                  <Point X="-1.100246826172" Y="-4.938065429688" />
                  <Point X="-1.151322265625" Y="-4.924923828125" />
                  <Point X="-1.351589599609" Y="-4.873396972656" />
                  <Point X="-1.317717407227" Y="-4.616112304688" />
                  <Point X="-1.309150146484" Y="-4.551038574219" />
                  <Point X="-1.309683227539" Y="-4.5347578125" />
                  <Point X="-1.320436889648" Y="-4.480695800781" />
                  <Point X="-1.370210693359" Y="-4.230466308594" />
                  <Point X="-1.386282714844" Y="-4.20262890625" />
                  <Point X="-1.427725219727" Y="-4.166284667969" />
                  <Point X="-1.619543457031" Y="-3.998063964844" />
                  <Point X="-1.649240722656" Y="-3.985762939453" />
                  <Point X="-1.704244018555" Y="-3.982157714844" />
                  <Point X="-1.958829833984" Y="-3.965471435547" />
                  <Point X="-1.989878540039" Y="-3.973791015625" />
                  <Point X="-2.035710327148" Y="-4.004414794922" />
                  <Point X="-2.247844970703" Y="-4.146159179688" />
                  <Point X="-2.259734130859" Y="-4.157294433594" />
                  <Point X="-2.444673339844" Y="-4.398312011719" />
                  <Point X="-2.457095214844" Y="-4.414500488281" />
                  <Point X="-2.496698730469" Y="-4.389979003906" />
                  <Point X="-2.855832275391" Y="-4.167612304688" />
                  <Point X="-2.926560791016" Y="-4.113153808594" />
                  <Point X="-3.228581054688" Y="-3.880608154297" />
                  <Point X="-2.638293945312" Y="-2.858201171875" />
                  <Point X="-2.506033691406" Y="-2.629119628906" />
                  <Point X="-2.500251464844" Y="-2.613671386719" />
                  <Point X="-2.504395996094" Y="-2.582189697266" />
                  <Point X="-2.517062011719" Y="-2.565682128906" />
                  <Point X="-2.531328369141" Y="-2.551415771484" />
                  <Point X="-2.560157226562" Y="-2.537198730469" />
                  <Point X="-2.59168359375" Y="-2.543469726562" />
                  <Point X="-3.652001220703" Y="-3.15564453125" />
                  <Point X="-3.842959472656" Y="-3.265894042969" />
                  <Point X="-3.877967529297" Y="-3.219900634766" />
                  <Point X="-4.161703613281" Y="-2.847129150391" />
                  <Point X="-4.212408691406" Y="-2.762104980469" />
                  <Point X="-4.431020019531" Y="-2.395527099609" />
                  <Point X="-3.395199462891" Y="-1.600713989258" />
                  <Point X="-3.163786865234" Y="-1.423144775391" />
                  <Point X="-3.152535400391" Y="-1.411081420898" />
                  <Point X="-3.144453125" Y="-1.390728393555" />
                  <Point X="-3.138117431641" Y="-1.366266113281" />
                  <Point X="-3.136651855469" Y="-1.350051147461" />
                  <Point X="-3.148656738281" Y="-1.321068603516" />
                  <Point X="-3.165864013672" Y="-1.307870117188" />
                  <Point X="-3.187641601562" Y="-1.295052734375" />
                  <Point X="-3.219529296875" Y="-1.288571044922" />
                  <Point X="-4.558080078125" Y="-1.464794555664" />
                  <Point X="-4.803283203125" Y="-1.497076293945" />
                  <Point X="-4.816421875" Y="-1.445638427734" />
                  <Point X="-4.927393066406" Y="-1.011191650391" />
                  <Point X="-4.940808105469" Y="-0.917394592285" />
                  <Point X="-4.998396484375" Y="-0.514742126465" />
                  <Point X="-3.821077636719" Y="-0.199280487061" />
                  <Point X="-3.557463867188" Y="-0.128645431519" />
                  <Point X="-3.536965087891" Y="-0.118002914429" />
                  <Point X="-3.514142822266" Y="-0.102163047791" />
                  <Point X="-3.502324462891" Y="-0.090645347595" />
                  <Point X="-3.493255371094" Y="-0.0706121521" />
                  <Point X="-3.485647949219" Y="-0.046100891113" />
                  <Point X="-3.483400878906" Y="-0.031280042648" />
                  <Point X="-3.487291503906" Y="-0.011163578987" />
                  <Point X="-3.494898925781" Y="0.013347681046" />
                  <Point X="-3.502324462891" Y="0.028085264206" />
                  <Point X="-3.519073486328" Y="0.043025115967" />
                  <Point X="-3.541895751953" Y="0.05886498642" />
                  <Point X="-3.557463867188" Y="0.066085334778" />
                  <Point X="-4.777616210938" Y="0.393024169922" />
                  <Point X="-4.998186523438" Y="0.452125823975" />
                  <Point X="-4.989164550781" Y="0.513095947266" />
                  <Point X="-4.917645019531" Y="0.996418151855" />
                  <Point X="-4.890639648438" Y="1.096076416016" />
                  <Point X="-4.773516113281" Y="1.528298583984" />
                  <Point X="-3.942231201172" Y="1.418857910156" />
                  <Point X="-3.753266357422" Y="1.393980102539" />
                  <Point X="-3.731704833984" Y="1.395866577148" />
                  <Point X="-3.720791748047" Y="1.399307373047" />
                  <Point X="-3.670278808594" Y="1.415234130859" />
                  <Point X="-3.651534423828" Y="1.426056152344" />
                  <Point X="-3.639120117188" Y="1.443785766602" />
                  <Point X="-3.634741210938" Y="1.454357666016" />
                  <Point X="-3.61447265625" Y="1.503290039063" />
                  <Point X="-3.610714111328" Y="1.524605224609" />
                  <Point X="-3.616315917969" Y="1.54551171875" />
                  <Point X="-3.621599609375" Y="1.555661743164" />
                  <Point X="-3.646055664062" Y="1.602641479492" />
                  <Point X="-3.659968261719" Y="1.619221679688" />
                  <Point X="-4.359850585938" Y="2.156260253906" />
                  <Point X="-4.47610546875" Y="2.245466064453" />
                  <Point X="-4.437913574219" Y="2.3108984375" />
                  <Point X="-4.160014648438" Y="2.787006591797" />
                  <Point X="-4.088479248047" Y="2.878954345703" />
                  <Point X="-3.774671142578" Y="3.282310791016" />
                  <Point X="-3.275213867188" Y="2.993948974609" />
                  <Point X="-3.159156982422" Y="2.926943359375" />
                  <Point X="-3.138514648438" Y="2.920434814453" />
                  <Point X="-3.123315429688" Y="2.919104980469" />
                  <Point X="-3.052965332031" Y="2.912950195312" />
                  <Point X="-3.031506591797" Y="2.915775390625" />
                  <Point X="-3.013252441406" Y="2.927404785156" />
                  <Point X="-3.002463867188" Y="2.938193115234" />
                  <Point X="-2.952528808594" Y="2.988128173828" />
                  <Point X="-2.940899414062" Y="3.006382324219" />
                  <Point X="-2.93807421875" Y="3.027841064453" />
                  <Point X="-2.939404052734" Y="3.043040283203" />
                  <Point X="-2.945558837891" Y="3.113390625" />
                  <Point X="-2.952067382812" Y="3.134032958984" />
                  <Point X="-3.262206542969" Y="3.671209472656" />
                  <Point X="-3.307278564453" Y="3.749276855469" />
                  <Point X="-3.236888671875" Y="3.803244140625" />
                  <Point X="-2.752876220703" Y="4.174331542969" />
                  <Point X="-2.640211425781" Y="4.23692578125" />
                  <Point X="-2.141548583984" Y="4.513972167969" />
                  <Point X="-2.00335546875" Y="4.333875" />
                  <Point X="-1.967826904297" Y="4.287573242188" />
                  <Point X="-1.951246826172" Y="4.273660644531" />
                  <Point X="-1.934330444336" Y="4.264854492188" />
                  <Point X="-1.856030761719" Y="4.22409375" />
                  <Point X="-1.835124267578" Y="4.2184921875" />
                  <Point X="-1.813808959961" Y="4.222250488281" />
                  <Point X="-1.796189208984" Y="4.229549316406" />
                  <Point X="-1.714635009766" Y="4.263330078125" />
                  <Point X="-1.696905395508" Y="4.275744140625" />
                  <Point X="-1.686083496094" Y="4.29448828125" />
                  <Point X="-1.680348510742" Y="4.312677246094" />
                  <Point X="-1.653804077148" Y="4.396864746094" />
                  <Point X="-1.651917602539" Y="4.418426757812" />
                  <Point X="-1.687177001953" Y="4.686248046875" />
                  <Point X="-1.689137695313" Y="4.701141113281" />
                  <Point X="-1.594678222656" Y="4.727624023438" />
                  <Point X="-0.968094177246" Y="4.903296386719" />
                  <Point X="-0.831512634277" Y="4.91928125" />
                  <Point X="-0.224199615479" Y="4.990358398438" />
                  <Point X="-0.076473182678" Y="4.439035644531" />
                  <Point X="-0.042140380859" Y="4.310903320312" />
                  <Point X="-0.024282133102" Y="4.284176757813" />
                  <Point X="0.006155934334" Y="4.273844238281" />
                  <Point X="0.036594024658" Y="4.284176757813" />
                  <Point X="0.054452251434" Y="4.310903320312" />
                  <Point X="0.213364044189" Y="4.903970214844" />
                  <Point X="0.236648406982" Y="4.990868652344" />
                  <Point X="0.313106536865" Y="4.982861328125" />
                  <Point X="0.860205810547" Y="4.925565429688" />
                  <Point X="0.973205200195" Y="4.898284179688" />
                  <Point X="1.508456054688" Y="4.769058105469" />
                  <Point X="1.581760253906" Y="4.742470214844" />
                  <Point X="1.931033691406" Y="4.615786132813" />
                  <Point X="2.00215625" Y="4.582524414062" />
                  <Point X="2.338699462891" Y="4.425134277344" />
                  <Point X="2.407412597656" Y="4.385102050781" />
                  <Point X="2.732523681641" Y="4.19569140625" />
                  <Point X="2.79732421875" Y="4.149608886719" />
                  <Point X="3.068740234375" Y="3.956592773438" />
                  <Point X="2.382934814453" Y="2.768742675781" />
                  <Point X="2.229853515625" Y="2.503597900391" />
                  <Point X="2.221037597656" Y="2.477250488281" />
                  <Point X="2.203382324219" Y="2.411228271484" />
                  <Point X="2.203531982422" Y="2.379991699219" />
                  <Point X="2.210416015625" Y="2.322901123047" />
                  <Point X="2.218682128906" Y="2.300812255859" />
                  <Point X="2.226314208984" Y="2.289564453125" />
                  <Point X="2.261639892578" Y="2.237503662109" />
                  <Point X="2.2861875" Y="2.216571533203" />
                  <Point X="2.338248535156" Y="2.18124609375" />
                  <Point X="2.360336914062" Y="2.172980224609" />
                  <Point X="2.372671386719" Y="2.171492919922" />
                  <Point X="2.42976171875" Y="2.164608642578" />
                  <Point X="2.462928466797" Y="2.169760742188" />
                  <Point X="2.528950683594" Y="2.187416015625" />
                  <Point X="2.541033935547" Y="2.192417480469" />
                  <Point X="3.768272216797" Y="2.900963867188" />
                  <Point X="3.994248046875" Y="3.031430908203" />
                  <Point X="4.009747070312" Y="3.009890869141" />
                  <Point X="4.202591796875" Y="2.741880126953" />
                  <Point X="4.238717285156" Y="2.682182373047" />
                  <Point X="4.387512695312" Y="2.436296142578" />
                  <Point X="3.490162109375" Y="1.747734375" />
                  <Point X="3.288616210938" Y="1.593082641602" />
                  <Point X="3.26910546875" Y="1.570440551758" />
                  <Point X="3.221589111328" Y="1.508451904297" />
                  <Point X="3.209295654297" Y="1.477826171875" />
                  <Point X="3.191595703125" Y="1.414535644531" />
                  <Point X="3.190779541016" Y="1.390965332031" />
                  <Point X="3.193918701172" Y="1.375751342773" />
                  <Point X="3.208448486328" Y="1.305332519531" />
                  <Point X="3.224184570312" Y="1.274977294922" />
                  <Point X="3.263704101562" Y="1.214909667969" />
                  <Point X="3.280947998047" Y="1.198819824219" />
                  <Point X="3.293320800781" Y="1.191854858398" />
                  <Point X="3.350590087891" Y="1.159617431641" />
                  <Point X="3.385294677734" Y="1.151408569336" />
                  <Point X="3.462726318359" Y="1.141175048828" />
                  <Point X="3.475803710938" Y="1.14117175293" />
                  <Point X="4.641598144531" Y="1.294651611328" />
                  <Point X="4.848975585938" Y="1.32195324707" />
                  <Point X="4.856362792969" Y="1.291607910156" />
                  <Point X="4.939188476562" Y="0.951386047363" />
                  <Point X="4.950572753906" Y="0.878266479492" />
                  <Point X="4.997858398438" Y="0.574556152344" />
                  <Point X="3.972143798828" Y="0.299716796875" />
                  <Point X="3.741167724609" Y="0.237826919556" />
                  <Point X="3.712651123047" Y="0.223319137573" />
                  <Point X="3.636576904297" Y="0.179346878052" />
                  <Point X="3.612403564453" Y="0.154360702515" />
                  <Point X="3.566759033203" Y="0.096199043274" />
                  <Point X="3.556985107422" Y="0.07473513031" />
                  <Point X="3.553697753906" Y="0.057570777893" />
                  <Point X="3.538482910156" Y="-0.021875455856" />
                  <Point X="3.541770263672" Y="-0.057848983765" />
                  <Point X="3.556985107422" Y="-0.137295227051" />
                  <Point X="3.566759033203" Y="-0.158758987427" />
                  <Point X="3.576620361328" Y="-0.171324859619" />
                  <Point X="3.622264892578" Y="-0.229486679077" />
                  <Point X="3.653012695312" Y="-0.251407241821" />
                  <Point X="3.729086914062" Y="-0.295379486084" />
                  <Point X="3.741167724609" Y="-0.300387023926" />
                  <Point X="4.81025390625" Y="-0.586847717285" />
                  <Point X="4.998067871094" Y="-0.637172363281" />
                  <Point X="4.994677734375" Y="-0.659658325195" />
                  <Point X="4.948431640625" Y="-0.966399169922" />
                  <Point X="4.933846679688" Y="-1.030313476562" />
                  <Point X="4.874545898438" Y="-1.290178344727" />
                  <Point X="3.679849365234" Y="-1.132893554688" />
                  <Point X="3.411982177734" Y="-1.097628295898" />
                  <Point X="3.394836181641" Y="-1.098341186523" />
                  <Point X="3.362578613281" Y="-1.105352539062" />
                  <Point X="3.213271972656" Y="-1.137804931641" />
                  <Point X="3.1854453125" Y="-1.154697265625" />
                  <Point X="3.165947509766" Y="-1.178147094727" />
                  <Point X="3.075701171875" Y="-1.286685424805" />
                  <Point X="3.064357910156" Y="-1.314070556641" />
                  <Point X="3.061563476562" Y="-1.344438964844" />
                  <Point X="3.04862890625" Y="-1.485000854492" />
                  <Point X="3.056360595703" Y="-1.516621948242" />
                  <Point X="3.074212402344" Y="-1.544389404297" />
                  <Point X="3.156840820312" Y="-1.672912475586" />
                  <Point X="3.168460693359" Y="-1.685540649414" />
                  <Point X="4.160579589844" Y="-2.4468203125" />
                  <Point X="4.33907421875" Y="-2.583784179688" />
                  <Point X="4.33449609375" Y="-2.591192382812" />
                  <Point X="4.204133300781" Y="-2.802139160156" />
                  <Point X="4.173967773438" Y="-2.845" />
                  <Point X="4.056688476562" Y="-3.011638427734" />
                  <Point X="2.992026611328" Y="-2.396955322266" />
                  <Point X="2.753454589844" Y="-2.259215576172" />
                  <Point X="2.737340820313" Y="-2.253312744141" />
                  <Point X="2.698948974609" Y="-2.246379394531" />
                  <Point X="2.521250488281" Y="-2.214287109375" />
                  <Point X="2.489077636719" Y="-2.219244873047" />
                  <Point X="2.457183349609" Y="-2.236030517578" />
                  <Point X="2.309559570312" Y="-2.313723876953" />
                  <Point X="2.288599853516" Y="-2.33468359375" />
                  <Point X="2.271814208984" Y="-2.366577636719" />
                  <Point X="2.194120849609" Y="-2.514201660156" />
                  <Point X="2.189163085938" Y="-2.546374511719" />
                  <Point X="2.196096435547" Y="-2.584766357422" />
                  <Point X="2.228188720703" Y="-2.76246484375" />
                  <Point X="2.234091552734" Y="-2.778578613281" />
                  <Point X="2.871627685547" Y="-3.882822998047" />
                  <Point X="2.986673583984" Y="-4.082088623047" />
                  <Point X="2.835296630859" Y="-4.190213378906" />
                  <Point X="2.801575195312" Y="-4.212041015625" />
                  <Point X="2.679775878906" Y="-4.290879394531" />
                  <Point X="1.865229980469" Y="-3.229342529297" />
                  <Point X="1.683177490234" Y="-2.992087402344" />
                  <Point X="1.670549316406" Y="-2.980467529297" />
                  <Point X="1.632684692383" Y="-2.956124023438" />
                  <Point X="1.45742590332" Y="-2.843448730469" />
                  <Point X="1.425804931641" Y="-2.835717041016" />
                  <Point X="1.384393310547" Y="-2.839527832031" />
                  <Point X="1.192718017578" Y="-2.857166015625" />
                  <Point X="1.165332885742" Y="-2.868509277344" />
                  <Point X="1.133356079102" Y="-2.895096923828" />
                  <Point X="0.985349060059" Y="-3.018159667969" />
                  <Point X="0.968456726074" Y="-3.045986328125" />
                  <Point X="0.958895935059" Y="-3.089974121094" />
                  <Point X="0.91464251709" Y="-3.29357421875" />
                  <Point X="0.91392956543" Y="-3.310720214844" />
                  <Point X="1.094602416992" Y="-4.68306640625" />
                  <Point X="1.127642211914" Y="-4.934028320312" />
                  <Point X="0.994346130371" Y="-4.963247070312" />
                  <Point X="0.963188781738" Y="-4.968907226562" />
                  <Point X="0.860200378418" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#134" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.034332864015" Y="4.483256472984" Z="0.45" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.45" />
                  <Point X="-0.843480619063" Y="5.000222398833" Z="0.45" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.45" />
                  <Point X="-1.614332013093" Y="4.807047800245" Z="0.45" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.45" />
                  <Point X="-1.742905199774" Y="4.711001854986" Z="0.45" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.45" />
                  <Point X="-1.734190241094" Y="4.358992850533" Z="0.45" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.45" />
                  <Point X="-1.821479282605" Y="4.307022986653" Z="0.45" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.45" />
                  <Point X="-1.917398592442" Y="4.340485031035" Z="0.45" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.45" />
                  <Point X="-1.969843752107" Y="4.395593050578" Z="0.45" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.45" />
                  <Point X="-2.670650300553" Y="4.311913135075" Z="0.45" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.45" />
                  <Point X="-3.273466801179" Y="3.874204010449" Z="0.45" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.45" />
                  <Point X="-3.311663736356" Y="3.677489470242" Z="0.45" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.45" />
                  <Point X="-2.995369779718" Y="3.069962712144" Z="0.45" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.45" />
                  <Point X="-3.043975085552" Y="3.004828482334" Z="0.45" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.45" />
                  <Point X="-3.125113753463" Y="3.000194919455" Z="0.45" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.45" />
                  <Point X="-3.256369878055" Y="3.068530212389" Z="0.45" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.45" />
                  <Point X="-4.134099077038" Y="2.940936810086" Z="0.45" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.45" />
                  <Point X="-4.487251715715" Y="2.367383643488" Z="0.45" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.45" />
                  <Point X="-4.396444612471" Y="2.147872655727" Z="0.45" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.45" />
                  <Point X="-3.672106175733" Y="1.563854356768" Z="0.45" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.45" />
                  <Point X="-3.687090905442" Y="1.504771957906" Z="0.45" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.45" />
                  <Point X="-3.741982769352" Y="1.478273482031" Z="0.45" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.45" />
                  <Point X="-3.941860913068" Y="1.49971023579" Z="0.45" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.45" />
                  <Point X="-4.945055480665" Y="1.140433880613" Z="0.45" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.45" />
                  <Point X="-5.044781995129" Y="0.55169504314" Z="0.45" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.45" />
                  <Point X="-4.796713180603" Y="0.376007899355" Z="0.45" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.45" />
                  <Point X="-3.553738584093" Y="0.03322915556" Z="0.45" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.45" />
                  <Point X="-3.541200344844" Y="0.005295662601" Z="0.45" />
                  <Point X="-3.539556741714" Y="0" Z="0.45" />
                  <Point X="-3.547164256291" Y="-0.02451128846" Z="0.45" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.45" />
                  <Point X="-3.57163001103" Y="-0.045646827574" Z="0.45" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.45" />
                  <Point X="-3.84017460817" Y="-0.119704157401" Z="0.45" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.45" />
                  <Point X="-4.99646016249" Y="-0.893193587201" Z="0.45" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.45" />
                  <Point X="-4.871022888721" Y="-1.426732691204" Z="0.45" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.45" />
                  <Point X="-4.557709527767" Y="-1.48308680904" Z="0.45" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.45" />
                  <Point X="-3.197381343172" Y="-1.319680623083" Z="0.45" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.45" />
                  <Point X="-3.199011980054" Y="-1.346912093697" Z="0.45" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.45" />
                  <Point X="-3.431793447104" Y="-1.529766330591" Z="0.45" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.45" />
                  <Point X="-4.261507783135" Y="-2.756434628784" Z="0.45" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.45" />
                  <Point X="-3.923909370875" Y="-3.21890371937" Z="0.45" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.45" />
                  <Point X="-3.633157281851" Y="-3.167665719743" Z="0.45" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.45" />
                  <Point X="-2.558573203016" Y="-2.569757266116" Z="0.45" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.45" />
                  <Point X="-2.687751293426" Y="-2.801921100641" Z="0.45" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.45" />
                  <Point X="-2.963220686312" Y="-4.121490793656" Z="0.45" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.45" />
                  <Point X="-2.529176406828" Y="-4.401209589249" Z="0.45" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.45" />
                  <Point X="-2.411161618723" Y="-4.397469737687" Z="0.45" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.45" />
                  <Point X="-2.014087667929" Y="-4.014708156463" Z="0.45" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.45" />
                  <Point X="-1.71367011556" Y="-4.000770826133" Z="0.45" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.45" />
                  <Point X="-1.466848354894" Y="-4.172596468379" Z="0.45" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.45" />
                  <Point X="-1.375632165196" Y="-4.45917013316" Z="0.45" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.45" />
                  <Point X="-1.373445650327" Y="-4.578305902483" Z="0.45" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.45" />
                  <Point X="-1.169936989073" Y="-4.942066734772" Z="0.45" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.45" />
                  <Point X="-0.870920385443" Y="-5.003426490845" Z="0.45" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.45" />
                  <Point X="-0.746498660615" Y="-4.748155041593" Z="0.45" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.45" />
                  <Point X="-0.282447819757" Y="-3.324784005997" Z="0.45" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.45" />
                  <Point X="-0.045013131282" Y="-3.218209783249" Z="0.45" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.45" />
                  <Point X="0.208345948079" Y="-3.268902262039" Z="0.45" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.45" />
                  <Point X="0.387998039675" Y="-3.47686174977" Z="0.45" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.45" />
                  <Point X="0.488256199786" Y="-3.784380988222" Z="0.45" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.45" />
                  <Point X="0.965969618833" Y="-4.986821973809" Z="0.45" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.45" />
                  <Point X="1.145243268851" Y="-4.948727964023" Z="0.45" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.45" />
                  <Point X="1.13801861818" Y="-4.645259956099" Z="0.45" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.45" />
                  <Point X="1.001599091258" Y="-3.069313767264" Z="0.45" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.45" />
                  <Point X="1.159166876598" Y="-2.90226316244" Z="0.45" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.45" />
                  <Point X="1.382819023662" Y="-2.858037306907" Z="0.45" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.45" />
                  <Point X="1.599489244912" Y="-2.966901796977" Z="0.45" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.45" />
                  <Point X="1.819406304562" Y="-3.228500537964" Z="0.45" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.45" />
                  <Point X="2.822588520388" Y="-4.22273497403" Z="0.45" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.45" />
                  <Point X="3.012891384806" Y="-4.089129828809" Z="0.45" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.45" />
                  <Point X="2.908772968465" Y="-3.826543028274" Z="0.45" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.45" />
                  <Point X="2.23914464274" Y="-2.54459996668" Z="0.45" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.45" />
                  <Point X="2.309904536801" Y="-2.358584323774" Z="0.45" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.45" />
                  <Point X="2.474314054598" Y="-2.248996773166" Z="0.45" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.45" />
                  <Point X="2.683906836833" Y="-2.264303366751" Z="0.45" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.45" />
                  <Point X="2.960870568833" Y="-2.408976531809" Z="0.45" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.45" />
                  <Point X="4.208700587705" Y="-2.842497225805" Z="0.45" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.45" />
                  <Point X="4.370873356363" Y="-2.586197466459" Z="0.45" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.45" />
                  <Point X="4.184861382642" Y="-2.375872475861" Z="0.45" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.45" />
                  <Point X="3.110114378065" Y="-1.486069910876" Z="0.45" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.45" />
                  <Point X="3.10519696738" Y="-1.317740618281" Z="0.45" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.45" />
                  <Point X="3.198237809223" Y="-1.178833886695" Z="0.45" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.45" />
                  <Point X="3.367042168307" Y="-1.122931767198" Z="0.45" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.45" />
                  <Point X="3.667167141175" Y="-1.151185818388" Z="0.45" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.45" />
                  <Point X="4.976437809896" Y="-1.01015736536" Z="0.45" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.45" />
                  <Point X="5.037963392922" Y="-0.635832261691" Z="0.45" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.45" />
                  <Point X="4.817039064043" Y="-0.507271451699" Z="0.45" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.45" />
                  <Point X="3.671878534196" Y="-0.176838405643" Z="0.45" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.45" />
                  <Point X="3.609798215412" Y="-0.109176389044" Z="0.45" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.45" />
                  <Point X="3.584721890616" Y="-0.017164344549" Z="0.45" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.45" />
                  <Point X="3.596649559808" Y="0.079446186687" Z="0.45" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.45" />
                  <Point X="3.645581222988" Y="0.154772349531" Z="0.45" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.45" />
                  <Point X="3.731516880156" Y="0.211310438599" Z="0.45" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.45" />
                  <Point X="3.978928753264" Y="0.28270048107" Z="0.45" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.45" />
                  <Point X="4.993821428296" Y="0.917238515969" Z="0.45" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.45" />
                  <Point X="4.89878783736" Y="1.334714821748" Z="0.45" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.45" />
                  <Point X="4.628915675516" Y="1.375503856558" Z="0.45" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.45" />
                  <Point X="3.385689730269" Y="1.23225762344" Z="0.45" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.45" />
                  <Point X="3.311691534704" Y="1.266706113421" Z="0.45" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.45" />
                  <Point X="3.259799158735" Y="1.333738771697" Z="0.45" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.45" />
                  <Point X="3.236731195913" Y="1.417135222847" Z="0.45" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.45" />
                  <Point X="3.251291925168" Y="1.495639777648" Z="0.45" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.45" />
                  <Point X="3.302632268605" Y="1.571302437762" Z="0.45" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.45" />
                  <Point X="3.514444177421" Y="1.739346794695" Z="0.45" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.45" />
                  <Point X="4.27533875876" Y="2.739348723578" Z="0.45" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.45" />
                  <Point X="4.044176472672" Y="3.070373811586" Z="0.45" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.45" />
                  <Point X="3.737116360408" Y="2.975545171278" Z="0.45" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.45" />
                  <Point X="2.443855743561" Y="2.249343577718" Z="0.45" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.45" />
                  <Point X="2.372501181993" Y="2.252413228508" Z="0.45" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.45" />
                  <Point X="2.308105837878" Y="2.289226004833" Z="0.45" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.45" />
                  <Point X="2.261532595402" Y="2.348919022504" Z="0.45" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.45" />
                  <Point X="2.24701647425" Y="2.41725726184" Z="0.45" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.45" />
                  <Point X="2.263184364939" Y="2.495613832321" Z="0.45" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.45" />
                  <Point X="2.420080181581" Y="2.775022721547" Z="0.45" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.45" />
                  <Point X="2.820145221556" Y="4.221636065027" Z="0.45" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.45" />
                  <Point X="2.426426471989" Y="4.459584537241" Z="0.45" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.45" />
                  <Point X="2.01718165921" Y="4.659096440886" Z="0.45" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.45" />
                  <Point X="1.592652860634" Y="4.820753979845" Z="0.45" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.45" />
                  <Point X="0.978783593789" Y="4.978167688907" Z="0.45" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.45" />
                  <Point X="0.312158490307" Y="5.06386956812" Z="0.45" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.45" />
                  <Point X="0.158911697538" Y="4.948191032967" Z="0.45" />
                  <Point X="0" Y="4.355124473572" Z="0.45" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>