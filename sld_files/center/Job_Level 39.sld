<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#193" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2816" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="0" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000475830078" Y="0.004715820312" />
                  <Width Value="9.783896972656" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.863149597168" Y="-4.631571289062" />
                  <Point X="0.563302062988" Y="-3.512524414063" />
                  <Point X="0.557721191406" Y="-3.497141845703" />
                  <Point X="0.542363037109" Y="-3.467377197266" />
                  <Point X="0.419807922363" Y="-3.290798095703" />
                  <Point X="0.378635528564" Y="-3.231476806641" />
                  <Point X="0.356751586914" Y="-3.209021484375" />
                  <Point X="0.330496398926" Y="-3.189777587891" />
                  <Point X="0.302495178223" Y="-3.175669189453" />
                  <Point X="0.112847938538" Y="-3.116809570313" />
                  <Point X="0.049136035919" Y="-3.097035888672" />
                  <Point X="0.020976638794" Y="-3.092766601562" />
                  <Point X="-0.008664840698" Y="-3.092766601562" />
                  <Point X="-0.036824237823" Y="-3.097035888672" />
                  <Point X="-0.226471481323" Y="-3.155895263672" />
                  <Point X="-0.290183227539" Y="-3.175669189453" />
                  <Point X="-0.31818460083" Y="-3.189777587891" />
                  <Point X="-0.344439788818" Y="-3.209021484375" />
                  <Point X="-0.366323577881" Y="-3.231476806641" />
                  <Point X="-0.488878997803" Y="-3.408055664062" />
                  <Point X="-0.530051208496" Y="-3.467377197266" />
                  <Point X="-0.538189025879" Y="-3.481573730469" />
                  <Point X="-0.550990112305" Y="-3.512524414063" />
                  <Point X="-0.651723693848" Y="-3.888467041016" />
                  <Point X="-0.916584594727" Y="-4.876941894531" />
                  <Point X="-1.008243103027" Y="-4.859150390625" />
                  <Point X="-1.079341186523" Y="-4.845350097656" />
                  <Point X="-1.24641796875" Y="-4.802362792969" />
                  <Point X="-1.214963012695" Y="-4.563438476562" />
                  <Point X="-1.214201171875" Y="-4.5479296875" />
                  <Point X="-1.216508666992" Y="-4.516224121094" />
                  <Point X="-1.261815551758" Y="-4.288452148438" />
                  <Point X="-1.277036132812" Y="-4.211932617188" />
                  <Point X="-1.287938354492" Y="-4.182966308594" />
                  <Point X="-1.304010375977" Y="-4.15512890625" />
                  <Point X="-1.32364453125" Y="-4.131204101562" />
                  <Point X="-1.498247680664" Y="-3.978080810547" />
                  <Point X="-1.556905517578" Y="-3.926639404297" />
                  <Point X="-1.583188598633" Y="-3.910295410156" />
                  <Point X="-1.612885864258" Y="-3.897994384766" />
                  <Point X="-1.64302734375" Y="-3.890966308594" />
                  <Point X="-1.874764770508" Y="-3.87577734375" />
                  <Point X="-1.952616577148" Y="-3.870674804688" />
                  <Point X="-1.98341809082" Y="-3.873708496094" />
                  <Point X="-2.014466674805" Y="-3.882028076172" />
                  <Point X="-2.042657836914" Y="-3.894801513672" />
                  <Point X="-2.235753662109" Y="-4.023824462891" />
                  <Point X="-2.300624267578" Y="-4.067169677734" />
                  <Point X="-2.312785644531" Y="-4.076821533203" />
                  <Point X="-2.335102783203" Y="-4.099461914062" />
                  <Point X="-2.391656738281" Y="-4.173164550781" />
                  <Point X="-2.480148925781" Y="-4.288489746094" />
                  <Point X="-2.697502197266" Y="-4.15391015625" />
                  <Point X="-2.801707763672" Y="-4.089388671875" />
                  <Point X="-3.102344970703" Y="-3.857907958984" />
                  <Point X="-3.104721923828" Y="-3.856077880859" />
                  <Point X="-2.980994384766" Y="-3.641775634766" />
                  <Point X="-2.423761230469" Y="-2.676619628906" />
                  <Point X="-2.412859130859" Y="-2.647652832031" />
                  <Point X="-2.406588134766" Y="-2.616125732422" />
                  <Point X="-2.405575683594" Y="-2.585191162109" />
                  <Point X="-2.414560791016" Y="-2.555572753906" />
                  <Point X="-2.428778808594" Y="-2.526743408203" />
                  <Point X="-2.446805664062" Y="-2.501588134766" />
                  <Point X="-2.464154296875" Y="-2.484239501953" />
                  <Point X="-2.489313964844" Y="-2.466210693359" />
                  <Point X="-2.518142578125" Y="-2.451994873047" />
                  <Point X="-2.547759277344" Y="-2.443011230469" />
                  <Point X="-2.578691894531" Y="-2.444024169922" />
                  <Point X="-2.610217529297" Y="-2.450295166016" />
                  <Point X="-2.63918359375" Y="-2.461197265625" />
                  <Point X="-2.963426757812" Y="-2.648399169922" />
                  <Point X="-3.818024414062" Y="-3.141801269531" />
                  <Point X="-4.000531005859" Y="-2.902025390625" />
                  <Point X="-4.082858886719" Y="-2.793863037109" />
                  <Point X="-4.298391113281" Y="-2.432448486328" />
                  <Point X="-4.306142578125" Y="-2.419450439453" />
                  <Point X="-4.080931884766" Y="-2.246639892578" />
                  <Point X="-3.105954589844" Y="-1.498513427734" />
                  <Point X="-3.084577392578" Y="-1.47559375" />
                  <Point X="-3.066612548828" Y="-1.448462646484" />
                  <Point X="-3.053856445312" Y="-1.419832763672" />
                  <Point X="-3.048089355469" Y="-1.397565795898" />
                  <Point X="-3.046151855469" Y="-1.390085327148" />
                  <Point X="-3.04334765625" Y="-1.359657104492" />
                  <Point X="-3.045556396484" Y="-1.327986572266" />
                  <Point X="-3.052557373047" Y="-1.298241699219" />
                  <Point X="-3.068639404297" Y="-1.272258300781" />
                  <Point X="-3.089471679688" Y="-1.248301879883" />
                  <Point X="-3.112972412109" Y="-1.228767211914" />
                  <Point X="-3.132795654297" Y="-1.217100097656" />
                  <Point X="-3.139455078125" Y="-1.213180541992" />
                  <Point X="-3.168717285156" Y="-1.201956665039" />
                  <Point X="-3.20060546875" Y="-1.195474853516" />
                  <Point X="-3.231929443359" Y="-1.194383789062" />
                  <Point X="-3.641255859375" Y="-1.248272827148" />
                  <Point X="-4.732102539062" Y="-1.391885375977" />
                  <Point X="-4.801877441406" Y="-1.118718505859" />
                  <Point X="-4.834076660156" Y="-0.992660095215" />
                  <Point X="-4.8911015625" Y="-0.593948120117" />
                  <Point X="-4.892424316406" Y="-0.584698303223" />
                  <Point X="-4.643521972656" Y="-0.518005065918" />
                  <Point X="-3.532875976562" Y="-0.220408447266" />
                  <Point X="-3.517493408203" Y="-0.214827560425" />
                  <Point X="-3.487728515625" Y="-0.19946925354" />
                  <Point X="-3.466954589844" Y="-0.185050994873" />
                  <Point X="-3.459975585938" Y="-0.180207199097" />
                  <Point X="-3.437519775391" Y="-0.1583228302" />
                  <Point X="-3.418275634766" Y="-0.132066726685" />
                  <Point X="-3.404168212891" Y="-0.104066856384" />
                  <Point X="-3.397243408203" Y="-0.081755340576" />
                  <Point X="-3.394917236328" Y="-0.074259773254" />
                  <Point X="-3.390647705078" Y="-0.046098255157" />
                  <Point X="-3.390648193359" Y="-0.016457229614" />
                  <Point X="-3.394917480469" Y="0.011700498581" />
                  <Point X="-3.401842041016" Y="0.034011867523" />
                  <Point X="-3.404168457031" Y="0.041507278442" />
                  <Point X="-3.418276855469" Y="0.069508964539" />
                  <Point X="-3.437520507812" Y="0.095763694763" />
                  <Point X="-3.459975830078" Y="0.117647468567" />
                  <Point X="-3.480749755859" Y="0.132065582275" />
                  <Point X="-3.490097900391" Y="0.137784286499" />
                  <Point X="-3.512645019531" Y="0.149848587036" />
                  <Point X="-3.532875976562" Y="0.157848251343" />
                  <Point X="-3.905996337891" Y="0.257825469971" />
                  <Point X="-4.89181640625" Y="0.521975219727" />
                  <Point X="-4.845239257813" Y="0.83673815918" />
                  <Point X="-4.824487792969" Y="0.976975341797" />
                  <Point X="-4.709700683594" Y="1.400575561523" />
                  <Point X="-4.703551269531" Y="1.423267822266" />
                  <Point X="-4.561804199219" Y="1.404606567383" />
                  <Point X="-3.765666259766" Y="1.29979284668" />
                  <Point X="-3.744985839844" Y="1.299341674805" />
                  <Point X="-3.723424072266" Y="1.301228271484" />
                  <Point X="-3.703137451172" Y="1.305263549805" />
                  <Point X="-3.657158203125" Y="1.319760742188" />
                  <Point X="-3.641711425781" Y="1.324631103516" />
                  <Point X="-3.622778320312" Y="1.332962036133" />
                  <Point X="-3.604034179688" Y="1.343783935547" />
                  <Point X="-3.587353271484" Y="1.356015136719" />
                  <Point X="-3.57371484375" Y="1.371566650391" />
                  <Point X="-3.561300292969" Y="1.389296508789" />
                  <Point X="-3.551351318359" Y="1.407431030273" />
                  <Point X="-3.532901855469" Y="1.451971801758" />
                  <Point X="-3.526703857422" Y="1.466935180664" />
                  <Point X="-3.520915527344" Y="1.486794067383" />
                  <Point X="-3.517157226562" Y="1.508108886719" />
                  <Point X="-3.5158046875" Y="1.528749267578" />
                  <Point X="-3.518951171875" Y="1.549192993164" />
                  <Point X="-3.524552978516" Y="1.570099243164" />
                  <Point X="-3.532049804688" Y="1.589377319336" />
                  <Point X="-3.554311035156" Y="1.632140991211" />
                  <Point X="-3.561789550781" Y="1.646507202148" />
                  <Point X="-3.57328125" Y="1.663705932617" />
                  <Point X="-3.587193847656" Y="1.680286376953" />
                  <Point X="-3.602135986328" Y="1.694590087891" />
                  <Point X="-3.816158935547" Y="1.858815673828" />
                  <Point X="-4.351860351563" Y="2.269874267578" />
                  <Point X="-4.161784667969" Y="2.595518554688" />
                  <Point X="-4.081155029297" Y="2.733657470703" />
                  <Point X="-3.777086669922" Y="3.124494628906" />
                  <Point X="-3.750504882813" Y="3.158661621094" />
                  <Point X="-3.695622802734" Y="3.126975585938" />
                  <Point X="-3.206656982422" Y="2.844670898438" />
                  <Point X="-3.187724121094" Y="2.836340332031" />
                  <Point X="-3.167081787109" Y="2.829831787109" />
                  <Point X="-3.146794677734" Y="2.825796386719" />
                  <Point X="-3.082758300781" Y="2.820193847656" />
                  <Point X="-3.061245361328" Y="2.818311767578" />
                  <Point X="-3.040564941406" Y="2.818762939453" />
                  <Point X="-3.019106201172" Y="2.821588134766" />
                  <Point X="-2.999014160156" Y="2.826504882812" />
                  <Point X="-2.980462402344" Y="2.835653564453" />
                  <Point X="-2.962208251953" Y="2.847282958984" />
                  <Point X="-2.946077392578" Y="2.860229492188" />
                  <Point X="-2.900623779297" Y="2.905682861328" />
                  <Point X="-2.885353759766" Y="2.920952880859" />
                  <Point X="-2.872406982422" Y="2.937083984375" />
                  <Point X="-2.860777587891" Y="2.955338134766" />
                  <Point X="-2.85162890625" Y="2.973889892578" />
                  <Point X="-2.846712158203" Y="2.993981933594" />
                  <Point X="-2.843886962891" Y="3.015440673828" />
                  <Point X="-2.843435791016" Y="3.036120849609" />
                  <Point X="-2.849038330078" Y="3.100157470703" />
                  <Point X="-2.850920410156" Y="3.121670410156" />
                  <Point X="-2.854955810547" Y="3.141957763672" />
                  <Point X="-2.861464355469" Y="3.162600097656" />
                  <Point X="-2.869794921875" Y="3.181532958984" />
                  <Point X="-2.964635009766" Y="3.345800537109" />
                  <Point X="-3.183333007812" Y="3.724596435547" />
                  <Point X="-2.841054931641" Y="3.987017578125" />
                  <Point X="-2.700625732422" Y="4.09468359375" />
                  <Point X="-2.221731445312" Y="4.360747070312" />
                  <Point X="-2.167037109375" Y="4.391134277344" />
                  <Point X="-2.04319519043" Y="4.229740722656" />
                  <Point X="-2.028892333984" Y="4.214799804688" />
                  <Point X="-2.012312866211" Y="4.200887695312" />
                  <Point X="-1.99511340332" Y="4.189395019531" />
                  <Point X="-1.923841308594" Y="4.15229296875" />
                  <Point X="-1.899897460938" Y="4.139828125" />
                  <Point X="-1.880619506836" Y="4.132331542969" />
                  <Point X="-1.859712768555" Y="4.126729492187" />
                  <Point X="-1.839269042969" Y="4.123582519531" />
                  <Point X="-1.81862890625" Y="4.124935058594" />
                  <Point X="-1.797313232422" Y="4.128693359375" />
                  <Point X="-1.777453613281" Y="4.134481933594" />
                  <Point X="-1.70321887207" Y="4.165231445312" />
                  <Point X="-1.678279663086" Y="4.175561523437" />
                  <Point X="-1.660146362305" Y="4.185509765625" />
                  <Point X="-1.642416625977" Y="4.197923828125" />
                  <Point X="-1.626864379883" Y="4.2115625" />
                  <Point X="-1.6146328125" Y="4.228244140625" />
                  <Point X="-1.603810913086" Y="4.24698828125" />
                  <Point X="-1.59548046875" Y="4.265921386719" />
                  <Point X="-1.571318237305" Y="4.342553710938" />
                  <Point X="-1.563200927734" Y="4.368297851562" />
                  <Point X="-1.559165649414" Y="4.388584960938" />
                  <Point X="-1.557279174805" Y="4.410146972656" />
                  <Point X="-1.55773034668" Y="4.430826660156" />
                  <Point X="-1.568512695312" Y="4.512726074219" />
                  <Point X="-1.584201904297" Y="4.631897949219" />
                  <Point X="-1.131429931641" Y="4.758839355469" />
                  <Point X="-0.94963494873" Y="4.80980859375" />
                  <Point X="-0.369074615479" Y="4.877754394531" />
                  <Point X="-0.294710784912" Y="4.886457519531" />
                  <Point X="-0.278552886963" Y="4.826155273438" />
                  <Point X="-0.133903305054" Y="4.286315429688" />
                  <Point X="-0.121129844666" Y="4.258124023438" />
                  <Point X="-0.10327155304" Y="4.231397460938" />
                  <Point X="-0.082114532471" Y="4.20880859375" />
                  <Point X="-0.054819446564" Y="4.19421875" />
                  <Point X="-0.024381370544" Y="4.183886230469" />
                  <Point X="0.006155906677" Y="4.178844238281" />
                  <Point X="0.03669316864" Y="4.183886230469" />
                  <Point X="0.067131248474" Y="4.19421875" />
                  <Point X="0.094426467896" Y="4.20880859375" />
                  <Point X="0.115583503723" Y="4.231397460938" />
                  <Point X="0.133441802979" Y="4.258124023438" />
                  <Point X="0.146215255737" Y="4.286315429688" />
                  <Point X="0.194810195923" Y="4.467674316406" />
                  <Point X="0.307419372559" Y="4.8879375" />
                  <Point X="0.685300415039" Y="4.84836328125" />
                  <Point X="0.84404107666" Y="4.831738769531" />
                  <Point X="1.324358398438" Y="4.715775390625" />
                  <Point X="1.481026611328" Y="4.677951171875" />
                  <Point X="1.793295410156" Y="4.564688476562" />
                  <Point X="1.894651367188" Y="4.52792578125" />
                  <Point X="2.196954833984" Y="4.386548828125" />
                  <Point X="2.294570068359" Y="4.340896972656" />
                  <Point X="2.586643066406" Y="4.170734863281" />
                  <Point X="2.680977783203" Y="4.115775390625" />
                  <Point X="2.943260253906" Y="3.929254882812" />
                  <Point X="2.792536621094" Y="3.668193847656" />
                  <Point X="2.147581054688" Y="2.551097900391" />
                  <Point X="2.142075683594" Y="2.539930664062" />
                  <Point X="2.133076904297" Y="2.516056640625" />
                  <Point X="2.117006103516" Y="2.455959716797" />
                  <Point X="2.111607177734" Y="2.435770263672" />
                  <Point X="2.108619384766" Y="2.417934326172" />
                  <Point X="2.107728027344" Y="2.380953125" />
                  <Point X="2.113994384766" Y="2.328986572266" />
                  <Point X="2.116099365234" Y="2.311528320312" />
                  <Point X="2.121441894531" Y="2.28960546875" />
                  <Point X="2.129708007812" Y="2.267516601562" />
                  <Point X="2.140071044922" Y="2.247471191406" />
                  <Point X="2.172226318359" Y="2.200082763672" />
                  <Point X="2.177552001953" Y="2.192973632812" />
                  <Point X="2.201655029297" Y="2.163752929688" />
                  <Point X="2.221599121094" Y="2.145592285156" />
                  <Point X="2.268987548828" Y="2.113437011719" />
                  <Point X="2.284907714844" Y="2.102634765625" />
                  <Point X="2.304953125" Y="2.092271728516" />
                  <Point X="2.327040771484" Y="2.084006103516" />
                  <Point X="2.348963623047" Y="2.078663574219" />
                  <Point X="2.400930419922" Y="2.072397216797" />
                  <Point X="2.410298339844" Y="2.071735107422" />
                  <Point X="2.446659179688" Y="2.070967529297" />
                  <Point X="2.473206298828" Y="2.074171142578" />
                  <Point X="2.533303222656" Y="2.090241943359" />
                  <Point X="2.553492675781" Y="2.095640869141" />
                  <Point X="2.565283447266" Y="2.099638183594" />
                  <Point X="2.588533935547" Y="2.110145019531" />
                  <Point X="2.963821289062" Y="2.326817138672" />
                  <Point X="3.967326416016" Y="2.906191162109" />
                  <Point X="4.067314697266" Y="2.767230224609" />
                  <Point X="4.123270019531" Y="2.689465087891" />
                  <Point X="4.26219921875" Y="2.459884277344" />
                  <Point X="4.079928466797" Y="2.320022949219" />
                  <Point X="3.230783935547" Y="1.668451171875" />
                  <Point X="3.221424316406" Y="1.660241088867" />
                  <Point X="3.203973632812" Y="1.641627685547" />
                  <Point X="3.160721679688" Y="1.585202392578" />
                  <Point X="3.14619140625" Y="1.566246337891" />
                  <Point X="3.136604980469" Y="1.550910522461" />
                  <Point X="3.121629882812" Y="1.517086181641" />
                  <Point X="3.105518554688" Y="1.459475708008" />
                  <Point X="3.100105957031" Y="1.440121582031" />
                  <Point X="3.096652587891" Y="1.417823120117" />
                  <Point X="3.095836425781" Y="1.394253173828" />
                  <Point X="3.097739501953" Y="1.371768188477" />
                  <Point X="3.110965332031" Y="1.307669311523" />
                  <Point X="3.115408447266" Y="1.286135375977" />
                  <Point X="3.1206796875" Y="1.268978149414" />
                  <Point X="3.136282714844" Y="1.235740356445" />
                  <Point X="3.172255371094" Y="1.181063598633" />
                  <Point X="3.184340332031" Y="1.162694946289" />
                  <Point X="3.198893554688" Y="1.145450439453" />
                  <Point X="3.216137451172" Y="1.129360595703" />
                  <Point X="3.234346923828" Y="1.116034912109" />
                  <Point X="3.286476074219" Y="1.086690551758" />
                  <Point X="3.303989013672" Y="1.076832519531" />
                  <Point X="3.320521728516" Y="1.069501464844" />
                  <Point X="3.356118164062" Y="1.059438598633" />
                  <Point X="3.426600341797" Y="1.050123291016" />
                  <Point X="3.450278808594" Y="1.046994018555" />
                  <Point X="3.462702392578" Y="1.046175048828" />
                  <Point X="3.488203613281" Y="1.04698449707" />
                  <Point X="3.844701416016" Y="1.093918457031" />
                  <Point X="4.776839355469" Y="1.21663671875" />
                  <Point X="4.821902832031" Y="1.031529296875" />
                  <Point X="4.845936035156" Y="0.932809387207" />
                  <Point X="4.890864746094" Y="0.644238525391" />
                  <Point X="4.689717773438" Y="0.590341186523" />
                  <Point X="3.716579833984" Y="0.32958984375" />
                  <Point X="3.704790527344" Y="0.325586364746" />
                  <Point X="3.681545898438" Y="0.315068054199" />
                  <Point X="3.612299316406" Y="0.275042297363" />
                  <Point X="3.589035888672" Y="0.26159564209" />
                  <Point X="3.574311523438" Y="0.251096588135" />
                  <Point X="3.547530761719" Y="0.225576522827" />
                  <Point X="3.505982910156" Y="0.172634765625" />
                  <Point X="3.492024902344" Y="0.154848876953" />
                  <Point X="3.48030078125" Y="0.135568756104" />
                  <Point X="3.470527099609" Y="0.114105293274" />
                  <Point X="3.463680908203" Y="0.09260433197" />
                  <Point X="3.449831542969" Y="0.02028834343" />
                  <Point X="3.445178710938" Y="-0.004006318092" />
                  <Point X="3.443483154297" Y="-0.021875085831" />
                  <Point X="3.445178710938" Y="-0.058553871155" />
                  <Point X="3.459028076172" Y="-0.130869857788" />
                  <Point X="3.463680908203" Y="-0.155164367676" />
                  <Point X="3.470527099609" Y="-0.176665481567" />
                  <Point X="3.48030078125" Y="-0.198128952026" />
                  <Point X="3.492024902344" Y="-0.217408905029" />
                  <Point X="3.533572753906" Y="-0.270350830078" />
                  <Point X="3.547530761719" Y="-0.288136566162" />
                  <Point X="3.559999023438" Y="-0.301236083984" />
                  <Point X="3.589035644531" Y="-0.324155517578" />
                  <Point X="3.658282226562" Y="-0.364181427002" />
                  <Point X="3.681545654297" Y="-0.377628082275" />
                  <Point X="3.692710205078" Y="-0.383138977051" />
                  <Point X="3.716579833984" Y="-0.392150024414" />
                  <Point X="4.043504638672" Y="-0.479749176025" />
                  <Point X="4.89147265625" Y="-0.706961425781" />
                  <Point X="4.868441894531" Y="-0.859719604492" />
                  <Point X="4.855022460938" Y="-0.948725708008" />
                  <Point X="4.801173828125" Y="-1.184698974609" />
                  <Point X="4.552948242188" Y="-1.15201953125" />
                  <Point X="3.424382080078" Y="-1.003440979004" />
                  <Point X="3.408035644531" Y="-1.002710266113" />
                  <Point X="3.374658691406" Y="-1.005508666992" />
                  <Point X="3.238752197266" Y="-1.035048583984" />
                  <Point X="3.193094482422" Y="-1.044972290039" />
                  <Point X="3.163974121094" Y="-1.056596923828" />
                  <Point X="3.136147460938" Y="-1.073489257812" />
                  <Point X="3.112397460938" Y="-1.093960083008" />
                  <Point X="3.030250488281" Y="-1.192757324219" />
                  <Point X="3.002653320312" Y="-1.225948242188" />
                  <Point X="2.987932617188" Y="-1.250330444336" />
                  <Point X="2.976589355469" Y="-1.277715820312" />
                  <Point X="2.969757568359" Y="-1.305365478516" />
                  <Point X="2.957983886719" Y="-1.43331237793" />
                  <Point X="2.954028564453" Y="-1.476295776367" />
                  <Point X="2.956347167969" Y="-1.507564086914" />
                  <Point X="2.964078613281" Y="-1.539185058594" />
                  <Point X="2.976450195312" Y="-1.567996582031" />
                  <Point X="3.051662841797" Y="-1.684984863281" />
                  <Point X="3.076930419922" Y="-1.724287109375" />
                  <Point X="3.086932861328" Y="-1.737239013672" />
                  <Point X="3.110628417969" Y="-1.760909057617" />
                  <Point X="3.414016601562" Y="-1.993707275391" />
                  <Point X="4.213122070312" Y="-2.6068828125" />
                  <Point X="4.16263671875" Y="-2.688575683594" />
                  <Point X="4.124805175781" Y="-2.74979296875" />
                  <Point X="4.028981445312" Y="-2.8859453125" />
                  <Point X="3.806094970703" Y="-2.757261474609" />
                  <Point X="2.800954589844" Y="-2.176943115234" />
                  <Point X="2.786131591797" Y="-2.170012451172" />
                  <Point X="2.754224609375" Y="-2.159825195312" />
                  <Point X="2.592474121094" Y="-2.13061328125" />
                  <Point X="2.538134277344" Y="-2.120799560547" />
                  <Point X="2.506781982422" Y="-2.120395263672" />
                  <Point X="2.474609130859" Y="-2.125353027344" />
                  <Point X="2.444833251953" Y="-2.135176757812" />
                  <Point X="2.310458251953" Y="-2.205897216797" />
                  <Point X="2.265315185547" Y="-2.229655761719" />
                  <Point X="2.242384033203" Y="-2.246549072266" />
                  <Point X="2.221424316406" Y="-2.267509033203" />
                  <Point X="2.204531738281" Y="-2.290439453125" />
                  <Point X="2.133811035156" Y="-2.424814453125" />
                  <Point X="2.110052734375" Y="-2.469957519531" />
                  <Point X="2.100229003906" Y="-2.499733398438" />
                  <Point X="2.095271240234" Y="-2.531906005859" />
                  <Point X="2.095675537109" Y="-2.563258300781" />
                  <Point X="2.124887451172" Y="-2.725008789062" />
                  <Point X="2.134701171875" Y="-2.779348632812" />
                  <Point X="2.138985595703" Y="-2.795141845703" />
                  <Point X="2.151819091797" Y="-2.826078613281" />
                  <Point X="2.346776611328" Y="-3.163754638672" />
                  <Point X="2.861283203125" Y="-4.054906494141" />
                  <Point X="2.82673828125" Y="-4.079581054688" />
                  <Point X="2.781847412109" Y="-4.111645507812" />
                  <Point X="2.701765136719" Y="-4.163481445312" />
                  <Point X="2.525561035156" Y="-3.933848144531" />
                  <Point X="1.758546142578" Y="-2.934255126953" />
                  <Point X="1.747503540039" Y="-2.922178955078" />
                  <Point X="1.721924194336" Y="-2.900557373047" />
                  <Point X="1.56239453125" Y="-2.797994384766" />
                  <Point X="1.50880078125" Y="-2.763538574219" />
                  <Point X="1.479989746094" Y="-2.751167236328" />
                  <Point X="1.448368408203" Y="-2.743435546875" />
                  <Point X="1.417099487305" Y="-2.741116699219" />
                  <Point X="1.242626708984" Y="-2.757171875" />
                  <Point X="1.184012573242" Y="-2.762565673828" />
                  <Point X="1.156362670898" Y="-2.769397460938" />
                  <Point X="1.128977661133" Y="-2.780740722656" />
                  <Point X="1.104595947266" Y="-2.795461181641" />
                  <Point X="0.969872375488" Y="-2.907479248047" />
                  <Point X="0.924612121582" Y="-2.945111572266" />
                  <Point X="0.904141357422" Y="-2.968861572266" />
                  <Point X="0.887248901367" Y="-2.996687988281" />
                  <Point X="0.875624328613" Y="-3.02580859375" />
                  <Point X="0.835342651367" Y="-3.211135986328" />
                  <Point X="0.821810058594" Y="-3.273396484375" />
                  <Point X="0.819724487305" Y="-3.289627197266" />
                  <Point X="0.819742248535" Y="-3.323120117188" />
                  <Point X="0.874991516113" Y="-3.742781005859" />
                  <Point X="1.022065246582" Y="-4.859915039062" />
                  <Point X="1.018142944336" Y="-4.860774902344" />
                  <Point X="0.975677612305" Y="-4.870083496094" />
                  <Point X="0.929315490723" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-0.990140869141" Y="-4.765891113281" />
                  <Point X="-1.058436523438" Y="-4.752634765625" />
                  <Point X="-1.141246337891" Y="-4.731328613281" />
                  <Point X="-1.120775756836" Y="-4.575838378906" />
                  <Point X="-1.120077392578" Y="-4.568099609375" />
                  <Point X="-1.119451660156" Y="-4.541033691406" />
                  <Point X="-1.121759277344" Y="-4.509328125" />
                  <Point X="-1.123334106445" Y="-4.497690429688" />
                  <Point X="-1.168640991211" Y="-4.269918457031" />
                  <Point X="-1.183861572266" Y="-4.193398925781" />
                  <Point X="-1.18812512207" Y="-4.17846875" />
                  <Point X="-1.19902734375" Y="-4.149502441406" />
                  <Point X="-1.205666137695" Y="-4.135466308594" />
                  <Point X="-1.221738037109" Y="-4.10762890625" />
                  <Point X="-1.230573852539" Y="-4.094862304688" />
                  <Point X="-1.250207885742" Y="-4.0709375" />
                  <Point X="-1.261006591797" Y="-4.059779296875" />
                  <Point X="-1.435609863281" Y="-3.906656005859" />
                  <Point X="-1.494267578125" Y="-3.855214599609" />
                  <Point X="-1.506738769531" Y="-3.845965332031" />
                  <Point X="-1.533021850586" Y="-3.829621337891" />
                  <Point X="-1.546833740234" Y="-3.822526855469" />
                  <Point X="-1.576530883789" Y="-3.810225830078" />
                  <Point X="-1.591313354492" Y="-3.805476074219" />
                  <Point X="-1.621455078125" Y="-3.798447998047" />
                  <Point X="-1.636813964844" Y="-3.796169677734" />
                  <Point X="-1.868551513672" Y="-3.780980712891" />
                  <Point X="-1.946403198242" Y="-3.775878173828" />
                  <Point X="-1.961928222656" Y="-3.776132324219" />
                  <Point X="-1.992729736328" Y="-3.779166015625" />
                  <Point X="-2.008006225586" Y="-3.781945556641" />
                  <Point X="-2.03905480957" Y="-3.790265136719" />
                  <Point X="-2.053674316406" Y="-3.795496337891" />
                  <Point X="-2.081865478516" Y="-3.808269775391" />
                  <Point X="-2.095437255859" Y="-3.815812011719" />
                  <Point X="-2.288532958984" Y="-3.944834960938" />
                  <Point X="-2.353403564453" Y="-3.988180175781" />
                  <Point X="-2.359681640625" Y="-3.992757080078" />
                  <Point X="-2.380442138672" Y="-4.010131103516" />
                  <Point X="-2.402759277344" Y="-4.032771484375" />
                  <Point X="-2.410471435547" Y="-4.041629638672" />
                  <Point X="-2.467025390625" Y="-4.11533203125" />
                  <Point X="-2.503202880859" Y="-4.162479492188" />
                  <Point X="-2.647491210938" Y="-4.073139648438" />
                  <Point X="-2.747582763672" Y="-4.011165527344" />
                  <Point X="-2.980862792969" Y="-3.831547607422" />
                  <Point X="-2.898721923828" Y="-3.689275634766" />
                  <Point X="-2.341488769531" Y="-2.724119628906" />
                  <Point X="-2.334849853516" Y="-2.710082763672" />
                  <Point X="-2.323947753906" Y="-2.681115966797" />
                  <Point X="-2.319684570312" Y="-2.666186035156" />
                  <Point X="-2.313413574219" Y="-2.634658935547" />
                  <Point X="-2.311638916016" Y="-2.619233398438" />
                  <Point X="-2.310626464844" Y="-2.588298828125" />
                  <Point X="-2.314666748047" Y="-2.557612792969" />
                  <Point X="-2.323651855469" Y="-2.527994384766" />
                  <Point X="-2.329358886719" Y="-2.513552978516" />
                  <Point X="-2.343576904297" Y="-2.484723632812" />
                  <Point X="-2.351559570312" Y="-2.47140625" />
                  <Point X="-2.369586425781" Y="-2.446250976562" />
                  <Point X="-2.379630615234" Y="-2.434413085938" />
                  <Point X="-2.396979248047" Y="-2.417064453125" />
                  <Point X="-2.408819580078" Y="-2.407018554688" />
                  <Point X="-2.433979248047" Y="-2.388989746094" />
                  <Point X="-2.447298583984" Y="-2.381006835938" />
                  <Point X="-2.476127197266" Y="-2.366791015625" />
                  <Point X="-2.490566894531" Y="-2.361085205078" />
                  <Point X="-2.52018359375" Y="-2.3521015625" />
                  <Point X="-2.550868652344" Y="-2.348062011719" />
                  <Point X="-2.581801269531" Y="-2.349074951172" />
                  <Point X="-2.597225830078" Y="-2.350849609375" />
                  <Point X="-2.628751464844" Y="-2.357120605469" />
                  <Point X="-2.643681396484" Y="-2.361384033203" />
                  <Point X="-2.672647460938" Y="-2.372286132812" />
                  <Point X="-2.68668359375" Y="-2.378924804688" />
                  <Point X="-3.010926757812" Y="-2.566126708984" />
                  <Point X="-3.793089355469" Y="-3.017708496094" />
                  <Point X="-3.924937744141" Y="-2.844487060547" />
                  <Point X="-4.004014160156" Y="-2.740596435547" />
                  <Point X="-4.181265136719" Y="-2.443373535156" />
                  <Point X="-4.023099365234" Y="-2.322008300781" />
                  <Point X="-3.048122070312" Y="-1.573881958008" />
                  <Point X="-3.036482421875" Y="-1.563310180664" />
                  <Point X="-3.015105224609" Y="-1.540390625" />
                  <Point X="-3.005367919922" Y="-1.528042236328" />
                  <Point X="-2.987403076172" Y="-1.500911132812" />
                  <Point X="-2.979836181641" Y="-1.487126098633" />
                  <Point X="-2.967080078125" Y="-1.45849621582" />
                  <Point X="-2.961890869141" Y="-1.443651611328" />
                  <Point X="-2.956123779297" Y="-1.421384643555" />
                  <Point X="-2.951552734375" Y="-1.398803344727" />
                  <Point X="-2.948748535156" Y="-1.36837512207" />
                  <Point X="-2.948577880859" Y="-1.353047729492" />
                  <Point X="-2.950786621094" Y="-1.321377197266" />
                  <Point X="-2.953083251953" Y="-1.306221557617" />
                  <Point X="-2.960084228516" Y="-1.2764765625" />
                  <Point X="-2.971778076172" Y="-1.248244628906" />
                  <Point X="-2.987860107422" Y="-1.222261352539" />
                  <Point X="-2.996952880859" Y="-1.209920288086" />
                  <Point X="-3.01778515625" Y="-1.185963867188" />
                  <Point X="-3.028744628906" Y="-1.175245605469" />
                  <Point X="-3.052245361328" Y="-1.1557109375" />
                  <Point X="-3.064785888672" Y="-1.146895019531" />
                  <Point X="-3.084609130859" Y="-1.135227905273" />
                  <Point X="-3.105433349609" Y="-1.124481445312" />
                  <Point X="-3.134695556641" Y="-1.113257568359" />
                  <Point X="-3.149793945312" Y="-1.108860595703" />
                  <Point X="-3.181682128906" Y="-1.102378662109" />
                  <Point X="-3.197298583984" Y="-1.100532470703" />
                  <Point X="-3.228622558594" Y="-1.09944140625" />
                  <Point X="-3.244329589844" Y="-1.100196533203" />
                  <Point X="-3.653656005859" Y="-1.154085571289" />
                  <Point X="-4.660920898438" Y="-1.286694458008" />
                  <Point X="-4.709832519531" Y="-1.095207641602" />
                  <Point X="-4.740761230469" Y="-0.974122558594" />
                  <Point X="-4.786452636719" Y="-0.654654418945" />
                  <Point X="-4.618934082031" Y="-0.609768005371" />
                  <Point X="-3.508288085938" Y="-0.312171447754" />
                  <Point X="-3.500475830078" Y="-0.309712615967" />
                  <Point X="-3.473931884766" Y="-0.299251373291" />
                  <Point X="-3.444166992188" Y="-0.283893127441" />
                  <Point X="-3.433561279297" Y="-0.27751361084" />
                  <Point X="-3.412787353516" Y="-0.263095428467" />
                  <Point X="-3.393671630859" Y="-0.248242492676" />
                  <Point X="-3.371215820312" Y="-0.226358062744" />
                  <Point X="-3.360896972656" Y="-0.214482803345" />
                  <Point X="-3.341652832031" Y="-0.188226623535" />
                  <Point X="-3.333435791016" Y="-0.17481237793" />
                  <Point X="-3.319328369141" Y="-0.146812561035" />
                  <Point X="-3.313437744141" Y="-0.132226852417" />
                  <Point X="-3.306512939453" Y="-0.109915290833" />
                  <Point X="-3.300990478516" Y="-0.088499916077" />
                  <Point X="-3.296720947266" Y="-0.060338401794" />
                  <Point X="-3.295647705078" Y="-0.046096759796" />
                  <Point X="-3.295648193359" Y="-0.016455703735" />
                  <Point X="-3.296721679688" Y="-0.00221599412" />
                  <Point X="-3.300990966797" Y="0.025941656113" />
                  <Point X="-3.304186767578" Y="0.039859745026" />
                  <Point X="-3.311111328125" Y="0.062171016693" />
                  <Point X="-3.319328613281" Y="0.084253112793" />
                  <Point X="-3.333437011719" Y="0.112254707336" />
                  <Point X="-3.341654785156" Y="0.12566986084" />
                  <Point X="-3.3608984375" Y="0.151924545288" />
                  <Point X="-3.371216796875" Y="0.163799057007" />
                  <Point X="-3.393672119141" Y="0.185682891846" />
                  <Point X="-3.405809082031" Y="0.195692047119" />
                  <Point X="-3.426583007812" Y="0.21011026001" />
                  <Point X="-3.445278808594" Y="0.221547393799" />
                  <Point X="-3.467825927734" Y="0.233611557007" />
                  <Point X="-3.477712158203" Y="0.238192855835" />
                  <Point X="-3.497943115234" Y="0.246192489624" />
                  <Point X="-3.508288085938" Y="0.249611251831" />
                  <Point X="-3.881408447266" Y="0.349588409424" />
                  <Point X="-4.785446289062" Y="0.591824584961" />
                  <Point X="-4.751262695312" Y="0.822831970215" />
                  <Point X="-4.731331054688" Y="0.95752935791" />
                  <Point X="-4.633586425781" Y="1.318237060547" />
                  <Point X="-4.574204101562" Y="1.310419311523" />
                  <Point X="-3.778066162109" Y="1.20560546875" />
                  <Point X="-3.76773828125" Y="1.204815429688" />
                  <Point X="-3.747057861328" Y="1.204364257812" />
                  <Point X="-3.736705322266" Y="1.20470324707" />
                  <Point X="-3.715143554688" Y="1.20658984375" />
                  <Point X="-3.704890380859" Y="1.208053588867" />
                  <Point X="-3.684603759766" Y="1.212088989258" />
                  <Point X="-3.6745703125" Y="1.214660400391" />
                  <Point X="-3.628591064453" Y="1.229157592773" />
                  <Point X="-3.603449951172" Y="1.237676757812" />
                  <Point X="-3.584516845703" Y="1.246007568359" />
                  <Point X="-3.575278320312" Y="1.250689575195" />
                  <Point X="-3.556534179688" Y="1.261511474609" />
                  <Point X="-3.547858886719" Y="1.267172241211" />
                  <Point X="-3.531177978516" Y="1.279403442383" />
                  <Point X="-3.515928710938" Y="1.293376953125" />
                  <Point X="-3.502290283203" Y="1.308928466797" />
                  <Point X="-3.495895263672" Y="1.317077026367" />
                  <Point X="-3.483480712891" Y="1.334806884766" />
                  <Point X="-3.478011230469" Y="1.343602539062" />
                  <Point X="-3.468062255859" Y="1.361737060547" />
                  <Point X="-3.463582763672" Y="1.371075927734" />
                  <Point X="-3.445133300781" Y="1.415616699219" />
                  <Point X="-3.435499023438" Y="1.44035144043" />
                  <Point X="-3.429710693359" Y="1.460210449219" />
                  <Point X="-3.427358642578" Y="1.470297851562" />
                  <Point X="-3.423600341797" Y="1.491612670898" />
                  <Point X="-3.422360595703" Y="1.501896972656" />
                  <Point X="-3.421008056641" Y="1.522537353516" />
                  <Point X="-3.42191015625" Y="1.543200561523" />
                  <Point X="-3.425056640625" Y="1.56364440918" />
                  <Point X="-3.427188232422" Y="1.573780761719" />
                  <Point X="-3.432790039062" Y="1.594687011719" />
                  <Point X="-3.436012207031" Y="1.604530761719" />
                  <Point X="-3.443509033203" Y="1.623808959961" />
                  <Point X="-3.447783691406" Y="1.633243286133" />
                  <Point X="-3.470044921875" Y="1.676006958008" />
                  <Point X="-3.482799560547" Y="1.699285888672" />
                  <Point X="-3.494291259766" Y="1.716484741211" />
                  <Point X="-3.500506835938" Y="1.724770629883" />
                  <Point X="-3.514419433594" Y="1.741351074219" />
                  <Point X="-3.521500732422" Y="1.748911621094" />
                  <Point X="-3.536442871094" Y="1.763215332031" />
                  <Point X="-3.544303710938" Y="1.769958618164" />
                  <Point X="-3.758326660156" Y="1.934184204102" />
                  <Point X="-4.227614746094" Y="2.294282226563" />
                  <Point X="-4.079738525391" Y="2.54762890625" />
                  <Point X="-4.002294189453" Y="2.680310302734" />
                  <Point X="-3.726338378906" Y="3.035012695312" />
                  <Point X="-3.254156738281" Y="2.7623984375" />
                  <Point X="-3.244917480469" Y="2.757716064453" />
                  <Point X="-3.225984619141" Y="2.749385498047" />
                  <Point X="-3.216291259766" Y="2.745737304688" />
                  <Point X="-3.195648925781" Y="2.739228759766" />
                  <Point X="-3.185615478516" Y="2.736657226562" />
                  <Point X="-3.165328369141" Y="2.732621826172" />
                  <Point X="-3.155074707031" Y="2.731157958984" />
                  <Point X="-3.091038330078" Y="2.725555419922" />
                  <Point X="-3.069525390625" Y="2.723673339844" />
                  <Point X="-3.059173339844" Y="2.723334472656" />
                  <Point X="-3.038492919922" Y="2.723785644531" />
                  <Point X="-3.028164550781" Y="2.724575683594" />
                  <Point X="-3.006705810547" Y="2.727400878906" />
                  <Point X="-2.996524902344" Y="2.729310791016" />
                  <Point X="-2.976432861328" Y="2.734227539062" />
                  <Point X="-2.956996826172" Y="2.741302001953" />
                  <Point X="-2.938445068359" Y="2.750450683594" />
                  <Point X="-2.929418212891" Y="2.755531738281" />
                  <Point X="-2.9111640625" Y="2.767161132812" />
                  <Point X="-2.902745117188" Y="2.773194335938" />
                  <Point X="-2.886614257812" Y="2.786140869141" />
                  <Point X="-2.87890234375" Y="2.793054199219" />
                  <Point X="-2.833448730469" Y="2.838507568359" />
                  <Point X="-2.818178710938" Y="2.853777587891" />
                  <Point X="-2.811265136719" Y="2.861489501953" />
                  <Point X="-2.798318359375" Y="2.877620605469" />
                  <Point X="-2.79228515625" Y="2.886039794922" />
                  <Point X="-2.780655761719" Y="2.904293945312" />
                  <Point X="-2.775574707031" Y="2.913320800781" />
                  <Point X="-2.766426025391" Y="2.931872558594" />
                  <Point X="-2.7593515625" Y="2.95130859375" />
                  <Point X="-2.754434814453" Y="2.971400634766" />
                  <Point X="-2.752524902344" Y="2.981581542969" />
                  <Point X="-2.749699707031" Y="3.003040283203" />
                  <Point X="-2.748909667969" Y="3.013368652344" />
                  <Point X="-2.748458496094" Y="3.034048828125" />
                  <Point X="-2.748797363281" Y="3.044400634766" />
                  <Point X="-2.754399902344" Y="3.108437255859" />
                  <Point X="-2.756281982422" Y="3.129950195312" />
                  <Point X="-2.757745849609" Y="3.140203857422" />
                  <Point X="-2.76178125" Y="3.160491210938" />
                  <Point X="-2.764352783203" Y="3.170524902344" />
                  <Point X="-2.770861328125" Y="3.191167236328" />
                  <Point X="-2.774509521484" Y="3.200860595703" />
                  <Point X="-2.782840087891" Y="3.219793457031" />
                  <Point X="-2.787522460938" Y="3.229032958984" />
                  <Point X="-2.882362548828" Y="3.393300537109" />
                  <Point X="-3.059387207031" Y="3.699916259766" />
                  <Point X="-2.783252929688" Y="3.911625732422" />
                  <Point X="-2.648374023438" Y="4.015036376953" />
                  <Point X="-2.192525146484" Y="4.268296386719" />
                  <Point X="-2.118563720703" Y="4.171908203125" />
                  <Point X="-2.111819580078" Y="4.164046875" />
                  <Point X="-2.097516845703" Y="4.149105957031" />
                  <Point X="-2.089958007813" Y="4.142026367188" />
                  <Point X="-2.073378417969" Y="4.128114257813" />
                  <Point X="-2.065093261719" Y="4.121898925781" />
                  <Point X="-2.047893676758" Y="4.11040625" />
                  <Point X="-2.038979614258" Y="4.10512890625" />
                  <Point X="-1.967707519531" Y="4.068027099609" />
                  <Point X="-1.943763671875" Y="4.055562255859" />
                  <Point X="-1.93432824707" Y="4.051287109375" />
                  <Point X="-1.915050415039" Y="4.043790527344" />
                  <Point X="-1.905207763672" Y="4.040568603516" />
                  <Point X="-1.884301025391" Y="4.034966552734" />
                  <Point X="-1.874166259766" Y="4.032835449219" />
                  <Point X="-1.85372265625" Y="4.029688476562" />
                  <Point X="-1.833057006836" Y="4.028785888672" />
                  <Point X="-1.812416870117" Y="4.030138427734" />
                  <Point X="-1.802133422852" Y="4.031378173828" />
                  <Point X="-1.780817626953" Y="4.035136474609" />
                  <Point X="-1.770729370117" Y="4.037488769531" />
                  <Point X="-1.750869750977" Y="4.04327734375" />
                  <Point X="-1.741098266602" Y="4.046713623047" />
                  <Point X="-1.666863525391" Y="4.077463134766" />
                  <Point X="-1.641924316406" Y="4.087793212891" />
                  <Point X="-1.63258581543" Y="4.092272460938" />
                  <Point X="-1.614452514648" Y="4.102220703125" />
                  <Point X="-1.605657836914" Y="4.107689453125" />
                  <Point X="-1.587928100586" Y="4.120103515625" />
                  <Point X="-1.579779541016" Y="4.126498535156" />
                  <Point X="-1.564227294922" Y="4.140137207031" />
                  <Point X="-1.550252319336" Y="4.155387695312" />
                  <Point X="-1.538020751953" Y="4.172069335938" />
                  <Point X="-1.532360351562" Y="4.180744140625" />
                  <Point X="-1.521538574219" Y="4.19948828125" />
                  <Point X="-1.516855834961" Y="4.208728515625" />
                  <Point X="-1.508525390625" Y="4.227661621094" />
                  <Point X="-1.504877441406" Y="4.237354003906" />
                  <Point X="-1.480715209961" Y="4.313986328125" />
                  <Point X="-1.472597900391" Y="4.33973046875" />
                  <Point X="-1.470026245117" Y="4.349764648437" />
                  <Point X="-1.465990966797" Y="4.370051757812" />
                  <Point X="-1.46452722168" Y="4.380305175781" />
                  <Point X="-1.46264074707" Y="4.4018671875" />
                  <Point X="-1.462301757812" Y="4.412219238281" />
                  <Point X="-1.462752929688" Y="4.432898925781" />
                  <Point X="-1.46354309082" Y="4.4432265625" />
                  <Point X="-1.474325439453" Y="4.525125976562" />
                  <Point X="-1.479266235352" Y="4.562654785156" />
                  <Point X="-1.105784179688" Y="4.667366210938" />
                  <Point X="-0.931175720215" Y="4.716320800781" />
                  <Point X="-0.365222015381" Y="4.782557128906" />
                  <Point X="-0.225666244507" Y="4.261727539062" />
                  <Point X="-0.220435317993" Y="4.247107910156" />
                  <Point X="-0.207661773682" Y="4.218916503906" />
                  <Point X="-0.200119155884" Y="4.205344726562" />
                  <Point X="-0.182260925293" Y="4.178618164062" />
                  <Point X="-0.172608306885" Y="4.166455566406" />
                  <Point X="-0.151451248169" Y="4.143866699219" />
                  <Point X="-0.126898002625" Y="4.125026367188" />
                  <Point X="-0.099602958679" Y="4.110436523438" />
                  <Point X="-0.085356712341" Y="4.104260742188" />
                  <Point X="-0.054918586731" Y="4.093927978516" />
                  <Point X="-0.039857292175" Y="4.090155273438" />
                  <Point X="-0.009319890976" Y="4.08511328125" />
                  <Point X="0.021631721497" Y="4.08511328125" />
                  <Point X="0.052168972015" Y="4.090155273438" />
                  <Point X="0.067230415344" Y="4.093927978516" />
                  <Point X="0.097668540955" Y="4.104260742188" />
                  <Point X="0.111914489746" Y="4.110436523438" />
                  <Point X="0.139209686279" Y="4.125026367188" />
                  <Point X="0.16376322937" Y="4.143866699219" />
                  <Point X="0.184920150757" Y="4.166455566406" />
                  <Point X="0.194572906494" Y="4.178617675781" />
                  <Point X="0.212431137085" Y="4.205344238281" />
                  <Point X="0.219973754883" Y="4.218916503906" />
                  <Point X="0.232747146606" Y="4.247107910156" />
                  <Point X="0.237978225708" Y="4.261727539062" />
                  <Point X="0.286573150635" Y="4.443086425781" />
                  <Point X="0.378190307617" Y="4.785006347656" />
                  <Point X="0.675405456543" Y="4.753879882812" />
                  <Point X="0.827876342773" Y="4.737912109375" />
                  <Point X="1.302063110352" Y="4.623428710938" />
                  <Point X="1.453595703125" Y="4.586844238281" />
                  <Point X="1.760902954102" Y="4.475381347656" />
                  <Point X="1.858263427734" Y="4.440067871094" />
                  <Point X="2.156710205078" Y="4.300494628906" />
                  <Point X="2.2504453125" Y="4.256657226562" />
                  <Point X="2.538820068359" Y="4.088649658203" />
                  <Point X="2.629432617188" Y="4.035858886719" />
                  <Point X="2.817780029297" Y="3.901916748047" />
                  <Point X="2.710264160156" Y="3.715693847656" />
                  <Point X="2.06530859375" Y="2.598597900391" />
                  <Point X="2.062373046875" Y="2.593104980469" />
                  <Point X="2.053180908203" Y="2.5734375" />
                  <Point X="2.044182128906" Y="2.549563476562" />
                  <Point X="2.041301757812" Y="2.540598632812" />
                  <Point X="2.025230957031" Y="2.480501708984" />
                  <Point X="2.01983215332" Y="2.460312255859" />
                  <Point X="2.017912719727" Y="2.451465576172" />
                  <Point X="2.013646972656" Y="2.420223388672" />
                  <Point X="2.012755615234" Y="2.3832421875" />
                  <Point X="2.013411254883" Y="2.369580078125" />
                  <Point X="2.019677734375" Y="2.317613525391" />
                  <Point X="2.023800537109" Y="2.289035400391" />
                  <Point X="2.029143066406" Y="2.267112548828" />
                  <Point X="2.032467895508" Y="2.256309570312" />
                  <Point X="2.040734008789" Y="2.234220703125" />
                  <Point X="2.045318237305" Y="2.223888916016" />
                  <Point X="2.055681396484" Y="2.203843505859" />
                  <Point X="2.061459960938" Y="2.194129882813" />
                  <Point X="2.093615234375" Y="2.146741455078" />
                  <Point X="2.104266601563" Y="2.132523193359" />
                  <Point X="2.128369628906" Y="2.103302490234" />
                  <Point X="2.137693847656" Y="2.093510742188" />
                  <Point X="2.157637939453" Y="2.075350097656" />
                  <Point X="2.1682578125" Y="2.066981201172" />
                  <Point X="2.215646240234" Y="2.034825927734" />
                  <Point X="2.241280029297" Y="2.018244995117" />
                  <Point X="2.261325439453" Y="2.007881958008" />
                  <Point X="2.271657226562" Y="2.003297607422" />
                  <Point X="2.293744873047" Y="1.995031982422" />
                  <Point X="2.304547851562" Y="1.991707275391" />
                  <Point X="2.326470703125" Y="1.986364746094" />
                  <Point X="2.337590576172" Y="1.984346801758" />
                  <Point X="2.389557373047" Y="1.978080322266" />
                  <Point X="2.408293212891" Y="1.976756347656" />
                  <Point X="2.444654052734" Y="1.975988647461" />
                  <Point X="2.458040771484" Y="1.976651855469" />
                  <Point X="2.484587890625" Y="1.97985546875" />
                  <Point X="2.497748291016" Y="1.982395996094" />
                  <Point X="2.557845214844" Y="1.998466796875" />
                  <Point X="2.578034667969" Y="2.003865722656" />
                  <Point X="2.583994384766" Y="2.005670532227" />
                  <Point X="2.604404541016" Y="2.013067260742" />
                  <Point X="2.627655029297" Y="2.02357409668" />
                  <Point X="2.636033935547" Y="2.027872558594" />
                  <Point X="3.011321289062" Y="2.244544677734" />
                  <Point X="3.940404785156" Y="2.780951416016" />
                  <Point X="3.990202148438" Y="2.711744628906" />
                  <Point X="4.043950439453" Y="2.637046630859" />
                  <Point X="4.136884765625" Y="2.483472412109" />
                  <Point X="4.022096191406" Y="2.395391601562" />
                  <Point X="3.172951660156" Y="1.743819702148" />
                  <Point X="3.168137695312" Y="1.739868652344" />
                  <Point X="3.152119384766" Y="1.725216674805" />
                  <Point X="3.134668701172" Y="1.706603271484" />
                  <Point X="3.128576171875" Y="1.699422363281" />
                  <Point X="3.08532421875" Y="1.642997070312" />
                  <Point X="3.070793945312" Y="1.624041015625" />
                  <Point X="3.065635253906" Y="1.616602172852" />
                  <Point X="3.049737792969" Y="1.589369384766" />
                  <Point X="3.034762695312" Y="1.555545043945" />
                  <Point X="3.030140136719" Y="1.542672119141" />
                  <Point X="3.014028808594" Y="1.485061645508" />
                  <Point X="3.008616210938" Y="1.465707519531" />
                  <Point X="3.006225097656" Y="1.454661010742" />
                  <Point X="3.002771728516" Y="1.432362426758" />
                  <Point X="3.001709472656" Y="1.421110717773" />
                  <Point X="3.000893310547" Y="1.397540771484" />
                  <Point X="3.001174804688" Y="1.386241210938" />
                  <Point X="3.003077880859" Y="1.363756225586" />
                  <Point X="3.004699462891" Y="1.352570800781" />
                  <Point X="3.017925292969" Y="1.288471801758" />
                  <Point X="3.022368408203" Y="1.266937988281" />
                  <Point X="3.02459765625" Y="1.258235473633" />
                  <Point X="3.034683837891" Y="1.228608520508" />
                  <Point X="3.050286865234" Y="1.195370727539" />
                  <Point X="3.056918945312" Y="1.183525634766" />
                  <Point X="3.092891601562" Y="1.128848876953" />
                  <Point X="3.1049765625" Y="1.110480224609" />
                  <Point X="3.111739257813" Y="1.101424438477" />
                  <Point X="3.126292480469" Y="1.08417980957" />
                  <Point X="3.134083007812" Y="1.075991210938" />
                  <Point X="3.151326904297" Y="1.059901367188" />
                  <Point X="3.160034423828" Y="1.052696044922" />
                  <Point X="3.178243896484" Y="1.039370361328" />
                  <Point X="3.187745849609" Y="1.03325012207" />
                  <Point X="3.239875" Y="1.003905639648" />
                  <Point X="3.257387939453" Y="0.994047668457" />
                  <Point X="3.265479736328" Y="0.989987731934" />
                  <Point X="3.294678710938" Y="0.97808404541" />
                  <Point X="3.330275146484" Y="0.968021240234" />
                  <Point X="3.343670654297" Y="0.965257629395" />
                  <Point X="3.414152832031" Y="0.955942382812" />
                  <Point X="3.437831298828" Y="0.952813110352" />
                  <Point X="3.444030029297" Y="0.952199768066" />
                  <Point X="3.465716308594" Y="0.951222839355" />
                  <Point X="3.491217529297" Y="0.952032409668" />
                  <Point X="3.500603515625" Y="0.952797241211" />
                  <Point X="3.857101318359" Y="0.999731140137" />
                  <Point X="4.704703613281" Y="1.11132019043" />
                  <Point X="4.729598632812" Y="1.009058288574" />
                  <Point X="4.75268359375" Y="0.914233703613" />
                  <Point X="4.78387109375" Y="0.713920776367" />
                  <Point X="4.665129882812" Y="0.682104064941" />
                  <Point X="3.691991943359" Y="0.421352752686" />
                  <Point X="3.686032470703" Y="0.419544616699" />
                  <Point X="3.665625732422" Y="0.412137542725" />
                  <Point X="3.642381103516" Y="0.40161920166" />
                  <Point X="3.634004638672" Y="0.397316711426" />
                  <Point X="3.564758056641" Y="0.357290924072" />
                  <Point X="3.541494628906" Y="0.343844268799" />
                  <Point X="3.533882080078" Y="0.338945953369" />
                  <Point X="3.508774658203" Y="0.319870941162" />
                  <Point X="3.481993896484" Y="0.294350921631" />
                  <Point X="3.472796875" Y="0.2842265625" />
                  <Point X="3.431249023438" Y="0.231284866333" />
                  <Point X="3.417291015625" Y="0.213498855591" />
                  <Point X="3.410854248047" Y="0.204208145142" />
                  <Point X="3.399130126953" Y="0.184928039551" />
                  <Point X="3.393842529297" Y="0.174938644409" />
                  <Point X="3.384068847656" Y="0.153475128174" />
                  <Point X="3.380005126953" Y="0.142928695679" />
                  <Point X="3.373158935547" Y="0.121427711487" />
                  <Point X="3.370376464844" Y="0.110473167419" />
                  <Point X="3.356527099609" Y="0.038157268524" />
                  <Point X="3.351874267578" Y="0.013862626076" />
                  <Point X="3.350603515625" Y="0.004967842579" />
                  <Point X="3.348584472656" Y="-0.026261991501" />
                  <Point X="3.350280029297" Y="-0.062940788269" />
                  <Point X="3.351874267578" Y="-0.076422821045" />
                  <Point X="3.365723632812" Y="-0.148738708496" />
                  <Point X="3.370376464844" Y="-0.173033203125" />
                  <Point X="3.373158935547" Y="-0.183987609863" />
                  <Point X="3.380005126953" Y="-0.205488739014" />
                  <Point X="3.384068847656" Y="-0.216035308838" />
                  <Point X="3.393842529297" Y="-0.237498840332" />
                  <Point X="3.399130371094" Y="-0.247488525391" />
                  <Point X="3.410854492188" Y="-0.266768493652" />
                  <Point X="3.417290771484" Y="-0.276058898926" />
                  <Point X="3.458838623047" Y="-0.329000762939" />
                  <Point X="3.472796630859" Y="-0.346786468506" />
                  <Point X="3.478718017578" Y="-0.353633239746" />
                  <Point X="3.501139404297" Y="-0.375805236816" />
                  <Point X="3.530176025391" Y="-0.39872467041" />
                  <Point X="3.541494384766" Y="-0.406404022217" />
                  <Point X="3.610740966797" Y="-0.446429962158" />
                  <Point X="3.634004394531" Y="-0.459876617432" />
                  <Point X="3.639496582031" Y="-0.462815338135" />
                  <Point X="3.659157958984" Y="-0.472016723633" />
                  <Point X="3.683027587891" Y="-0.481027740479" />
                  <Point X="3.691991943359" Y="-0.48391293335" />
                  <Point X="4.018916748047" Y="-0.571512207031" />
                  <Point X="4.784876953125" Y="-0.776750488281" />
                  <Point X="4.774503417969" Y="-0.845556945801" />
                  <Point X="4.76161328125" Y="-0.931052001953" />
                  <Point X="4.727801757812" Y="-1.079219726563" />
                  <Point X="4.565348144531" Y="-1.057832397461" />
                  <Point X="3.436781982422" Y="-0.909253723145" />
                  <Point X="3.428624511719" Y="-0.908535705566" />
                  <Point X="3.400098388672" Y="-0.908042480469" />
                  <Point X="3.366721435547" Y="-0.910840881348" />
                  <Point X="3.354481201172" Y="-0.912676208496" />
                  <Point X="3.218574707031" Y="-0.942216064453" />
                  <Point X="3.172916992188" Y="-0.952139770508" />
                  <Point X="3.157873779297" Y="-0.956742492676" />
                  <Point X="3.128753417969" Y="-0.968367126465" />
                  <Point X="3.114676269531" Y="-0.975388977051" />
                  <Point X="3.086849609375" Y="-0.992281311035" />
                  <Point X="3.074124023438" Y="-1.001530273438" />
                  <Point X="3.050374023438" Y="-1.022001159668" />
                  <Point X="3.039349609375" Y="-1.033222900391" />
                  <Point X="2.957202636719" Y="-1.132020141602" />
                  <Point X="2.92960546875" Y="-1.16521105957" />
                  <Point X="2.921326171875" Y="-1.176847167969" />
                  <Point X="2.90660546875" Y="-1.201229370117" />
                  <Point X="2.900163818359" Y="-1.213975830078" />
                  <Point X="2.888820556641" Y="-1.241361328125" />
                  <Point X="2.884362792969" Y="-1.254928222656" />
                  <Point X="2.877531005859" Y="-1.282577880859" />
                  <Point X="2.875157226562" Y="-1.296660400391" />
                  <Point X="2.863383544922" Y="-1.424607177734" />
                  <Point X="2.859428222656" Y="-1.467590698242" />
                  <Point X="2.859288574219" Y="-1.483320800781" />
                  <Point X="2.861607177734" Y="-1.514589233398" />
                  <Point X="2.864065429688" Y="-1.530127319336" />
                  <Point X="2.871796875" Y="-1.561748291016" />
                  <Point X="2.876785888672" Y="-1.576668212891" />
                  <Point X="2.889157470703" Y="-1.605479614258" />
                  <Point X="2.896540039062" Y="-1.619371459961" />
                  <Point X="2.971752685547" Y="-1.736359741211" />
                  <Point X="2.997020263672" Y="-1.775661987305" />
                  <Point X="3.001741943359" Y="-1.782353393555" />
                  <Point X="3.019793945312" Y="-1.804450317383" />
                  <Point X="3.043489501953" Y="-1.828120361328" />
                  <Point X="3.052796142578" Y="-1.83627746582" />
                  <Point X="3.356184326172" Y="-2.069075927734" />
                  <Point X="4.087170166016" Y="-2.629981445312" />
                  <Point X="4.081823242188" Y="-2.638633789062" />
                  <Point X="4.045483398438" Y="-2.6974375" />
                  <Point X="4.001274414063" Y="-2.760251953125" />
                  <Point X="3.853594970703" Y="-2.674989013672" />
                  <Point X="2.848454589844" Y="-2.094670654297" />
                  <Point X="2.841191894531" Y="-2.090885253906" />
                  <Point X="2.815026123047" Y="-2.079513183594" />
                  <Point X="2.783119140625" Y="-2.069325927734" />
                  <Point X="2.771108398438" Y="-2.066337646484" />
                  <Point X="2.609357910156" Y="-2.037125610352" />
                  <Point X="2.555018066406" Y="-2.027311889648" />
                  <Point X="2.539359130859" Y="-2.025807495117" />
                  <Point X="2.508006835938" Y="-2.025403198242" />
                  <Point X="2.492313476562" Y="-2.026503540039" />
                  <Point X="2.460140625" Y="-2.031461303711" />
                  <Point X="2.444844482422" Y="-2.035136108398" />
                  <Point X="2.415068603516" Y="-2.044959960938" />
                  <Point X="2.400588867188" Y="-2.051108642578" />
                  <Point X="2.266213867188" Y="-2.121829101563" />
                  <Point X="2.221070800781" Y="-2.145587646484" />
                  <Point X="2.208968505859" Y="-2.153170166016" />
                  <Point X="2.186037353516" Y="-2.170063476562" />
                  <Point X="2.175208496094" Y="-2.179374267578" />
                  <Point X="2.154248779297" Y="-2.200334228516" />
                  <Point X="2.144938476562" Y="-2.211162841797" />
                  <Point X="2.128045898438" Y="-2.234093261719" />
                  <Point X="2.120463623047" Y="-2.246195068359" />
                  <Point X="2.049742919922" Y="-2.380570068359" />
                  <Point X="2.025984741211" Y="-2.425713134766" />
                  <Point X="2.0198359375" Y="-2.440192871094" />
                  <Point X="2.010012084961" Y="-2.46996875" />
                  <Point X="2.006337280273" Y="-2.485264892578" />
                  <Point X="2.001379516602" Y="-2.5174375" />
                  <Point X="2.000279174805" Y="-2.533130859375" />
                  <Point X="2.00068347168" Y="-2.564483154297" />
                  <Point X="2.002187866211" Y="-2.580142089844" />
                  <Point X="2.031399658203" Y="-2.741892578125" />
                  <Point X="2.041213378906" Y="-2.796232421875" />
                  <Point X="2.043015014648" Y="-2.804221435547" />
                  <Point X="2.051236083984" Y="-2.83154296875" />
                  <Point X="2.064069580078" Y="-2.862479736328" />
                  <Point X="2.069546630859" Y="-2.873578613281" />
                  <Point X="2.264504150391" Y="-3.211254638672" />
                  <Point X="2.735892822266" Y="-4.027724365234" />
                  <Point X="2.723754150391" Y="-4.036083740234" />
                  <Point X="2.600929443359" Y="-3.876015869141" />
                  <Point X="1.833914672852" Y="-2.876422851562" />
                  <Point X="1.828654418945" Y="-2.870147216797" />
                  <Point X="1.808830932617" Y="-2.849625732422" />
                  <Point X="1.783251586914" Y="-2.828004150391" />
                  <Point X="1.773299072266" Y="-2.820647216797" />
                  <Point X="1.613769287109" Y="-2.718084228516" />
                  <Point X="1.56017565918" Y="-2.683628417969" />
                  <Point X="1.546283935547" Y="-2.676245849609" />
                  <Point X="1.517472900391" Y="-2.663874511719" />
                  <Point X="1.502553344727" Y="-2.658885742188" />
                  <Point X="1.470932006836" Y="-2.651154052734" />
                  <Point X="1.455394042969" Y="-2.648695800781" />
                  <Point X="1.42412512207" Y="-2.646376953125" />
                  <Point X="1.408394287109" Y="-2.646516357422" />
                  <Point X="1.233921386719" Y="-2.662571533203" />
                  <Point X="1.175307373047" Y="-2.667965332031" />
                  <Point X="1.161225097656" Y="-2.670339111328" />
                  <Point X="1.133575317383" Y="-2.677170898438" />
                  <Point X="1.120007568359" Y="-2.68162890625" />
                  <Point X="1.092622680664" Y="-2.692972167969" />
                  <Point X="1.079876464844" Y="-2.699413818359" />
                  <Point X="1.055494750977" Y="-2.714134277344" />
                  <Point X="1.043859008789" Y="-2.722413085938" />
                  <Point X="0.909135375977" Y="-2.834431152344" />
                  <Point X="0.863875061035" Y="-2.872063476562" />
                  <Point X="0.852653137207" Y="-2.883088134766" />
                  <Point X="0.832182434082" Y="-2.906838134766" />
                  <Point X="0.822933776855" Y="-2.919563232422" />
                  <Point X="0.806041259766" Y="-2.947389648438" />
                  <Point X="0.799018859863" Y="-2.961467773438" />
                  <Point X="0.787394287109" Y="-2.990588378906" />
                  <Point X="0.782791931152" Y="-3.005631103516" />
                  <Point X="0.742510192871" Y="-3.190958496094" />
                  <Point X="0.728977661133" Y="-3.253218994141" />
                  <Point X="0.727584716797" Y="-3.2612890625" />
                  <Point X="0.724724487305" Y="-3.289677490234" />
                  <Point X="0.7247421875" Y="-3.323170410156" />
                  <Point X="0.725554992676" Y="-3.335520019531" />
                  <Point X="0.780804199219" Y="-3.755180908203" />
                  <Point X="0.833091430664" Y="-4.152340332031" />
                  <Point X="0.655064941406" Y="-3.487936523438" />
                  <Point X="0.652606262207" Y="-3.480124511719" />
                  <Point X="0.642145141602" Y="-3.453580322266" />
                  <Point X="0.62678692627" Y="-3.423815673828" />
                  <Point X="0.620407531738" Y="-3.413210205078" />
                  <Point X="0.497852355957" Y="-3.236631103516" />
                  <Point X="0.456679962158" Y="-3.177309814453" />
                  <Point X="0.446670654297" Y="-3.165172851562" />
                  <Point X="0.424786804199" Y="-3.142717529297" />
                  <Point X="0.41291229248" Y="-3.132399169922" />
                  <Point X="0.386657165527" Y="-3.113155273438" />
                  <Point X="0.373242767334" Y="-3.104937988281" />
                  <Point X="0.345241607666" Y="-3.090829589844" />
                  <Point X="0.330654724121" Y="-3.084938476562" />
                  <Point X="0.14100743103" Y="-3.026078857422" />
                  <Point X="0.077295455933" Y="-3.006305175781" />
                  <Point X="0.063376327515" Y="-3.003109375" />
                  <Point X="0.03521704483" Y="-2.998840087891" />
                  <Point X="0.02097659111" Y="-2.997766601562" />
                  <Point X="-0.008664910316" Y="-2.997766601562" />
                  <Point X="-0.02290521431" Y="-2.998840087891" />
                  <Point X="-0.051064647675" Y="-3.003109375" />
                  <Point X="-0.064983627319" Y="-3.006305175781" />
                  <Point X="-0.25463092041" Y="-3.065164550781" />
                  <Point X="-0.318342590332" Y="-3.084938476562" />
                  <Point X="-0.332929473877" Y="-3.090829589844" />
                  <Point X="-0.360930786133" Y="-3.104937988281" />
                  <Point X="-0.374345336914" Y="-3.113155273438" />
                  <Point X="-0.400600463867" Y="-3.132399169922" />
                  <Point X="-0.412475128174" Y="-3.142717773438" />
                  <Point X="-0.434358978271" Y="-3.165173095703" />
                  <Point X="-0.444367980957" Y="-3.177309814453" />
                  <Point X="-0.566923461914" Y="-3.353888671875" />
                  <Point X="-0.608095581055" Y="-3.413210205078" />
                  <Point X="-0.612470458984" Y="-3.420132324219" />
                  <Point X="-0.62597668457" Y="-3.445265136719" />
                  <Point X="-0.638777893066" Y="-3.476215820312" />
                  <Point X="-0.642753112793" Y="-3.487936523438" />
                  <Point X="-0.743486572266" Y="-3.863879150391" />
                  <Point X="-0.985425109863" Y="-4.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.684234807161" Y="-1.195421544543" />
                  <Point X="-4.640997347679" Y="-1.284071473821" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.104352589755" Y="-2.384356282555" />
                  <Point X="-3.799650003966" Y="-3.009089164737" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.763286237986" Y="-0.816630749134" />
                  <Point X="-4.541677602202" Y="-1.270995786408" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.027440042791" Y="-2.325339029953" />
                  <Point X="-3.712454940652" Y="-2.97115419491" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.742353403228" Y="-0.642838077547" />
                  <Point X="-4.442357856726" Y="-1.257920098996" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.950527434407" Y="-2.266321903282" />
                  <Point X="-3.629981626476" Y="-2.923538204686" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.648872938998" Y="-0.617790089362" />
                  <Point X="-4.34303811125" Y="-1.244844411583" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.873614822349" Y="-2.207304784143" />
                  <Point X="-3.5475083123" Y="-2.875922214461" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.555392479144" Y="-0.592742092208" />
                  <Point X="-4.243718365774" Y="-1.231768724171" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.796702210291" Y="-2.148287665004" />
                  <Point X="-3.465034998125" Y="-2.828306224236" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.978075836625" Y="-3.82672046375" />
                  <Point X="-2.972629607395" Y="-3.837886888464" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.46191202135" Y="-0.567694090827" />
                  <Point X="-4.144398620297" Y="-1.218693036759" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.719789598234" Y="-2.089270545866" />
                  <Point X="-3.382561683949" Y="-2.780690234011" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.920780449757" Y="-3.727482072445" />
                  <Point X="-2.803368415434" Y="-3.968212417465" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.368431563557" Y="-0.542646089446" />
                  <Point X="-4.045078874821" Y="-1.205617349346" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.642876986176" Y="-2.030253426727" />
                  <Point X="-3.300088369773" Y="-2.733074243786" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.863485081834" Y="-3.628243642294" />
                  <Point X="-2.646063705611" Y="-4.074023525205" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.274951105764" Y="-0.517598088065" />
                  <Point X="-3.945759129345" Y="-1.192541661934" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.565964374118" Y="-1.971236307588" />
                  <Point X="-3.217615055598" Y="-2.685458253561" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.806189725772" Y="-3.529005187825" />
                  <Point X="-2.499547266821" Y="-4.157715399405" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.767967142174" Y="0.709945928454" />
                  <Point X="-4.699066789718" Y="0.568679271127" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.181470647971" Y="-0.492550086684" />
                  <Point X="-3.846439383869" Y="-1.179465974522" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.48905176206" Y="-1.912219188449" />
                  <Point X="-3.135141741422" Y="-2.637842263336" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.74889436971" Y="-3.429766733357" />
                  <Point X="-2.434925376203" Y="-4.073498566881" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.743363743991" Y="0.87621282975" />
                  <Point X="-4.577479686599" Y="0.536100109623" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.087990190177" Y="-0.467502085304" />
                  <Point X="-3.747119638392" Y="-1.166390287109" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.412139150002" Y="-1.85320206931" />
                  <Point X="-3.052668427246" Y="-2.590226273111" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.691599013648" Y="-3.330528278888" />
                  <Point X="-2.366020921939" Y="-3.998062291055" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.712043186887" Y="1.028707514306" />
                  <Point X="-4.45589258348" Y="0.503520948118" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.994509732384" Y="-0.442454083923" />
                  <Point X="-3.647799894321" Y="-1.153314596816" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.335226537944" Y="-1.794184950172" />
                  <Point X="-2.97019511431" Y="-2.542610280347" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.634303657586" Y="-3.23128982442" />
                  <Point X="-2.286837108344" Y="-3.943701825153" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.674292615818" Y="1.168018716528" />
                  <Point X="-4.334305480361" Y="0.470941786613" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.901029274591" Y="-0.417406082542" />
                  <Point X="-3.548480172673" Y="-1.14023886055" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.258313925886" Y="-1.735167831033" />
                  <Point X="-2.887721802642" Y="-2.49499428498" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.577008301524" Y="-3.132051369951" />
                  <Point X="-2.207119424721" Y="-3.89043595502" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.636542044748" Y="1.307329918749" />
                  <Point X="-4.212718377242" Y="0.438362625109" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.807548816798" Y="-0.392358081161" />
                  <Point X="-3.449160451024" Y="-1.127163124284" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.181401313828" Y="-1.676150711894" />
                  <Point X="-2.805248490974" Y="-2.447378289612" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.519712945463" Y="-3.032812915483" />
                  <Point X="-2.127401741098" Y="-3.837170084886" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.529479851499" Y="1.304531235749" />
                  <Point X="-4.091131274123" Y="0.405783463604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.714068359004" Y="-0.36731007978" />
                  <Point X="-3.349840729375" Y="-1.114087388018" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.10448870177" Y="-1.617133592755" />
                  <Point X="-2.722775179307" Y="-2.399762294245" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.462417589401" Y="-2.933574461014" />
                  <Point X="-2.043760399092" Y="-3.791948906609" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.416529999727" Y="1.289661063863" />
                  <Point X="-3.969544171003" Y="0.373204302099" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.620587901211" Y="-0.342262078399" />
                  <Point X="-3.250521007727" Y="-1.101011651752" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.028970894875" Y="-1.555256699234" />
                  <Point X="-2.636760283142" Y="-2.359407623178" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.405122233339" Y="-2.834336006546" />
                  <Point X="-1.945884865367" Y="-3.775912146295" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.303580147955" Y="1.274790891976" />
                  <Point X="-3.847957074314" Y="0.340625153778" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.527107443418" Y="-0.317214077019" />
                  <Point X="-3.139539051751" Y="-1.111847039327" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.968696978672" Y="-1.462125198065" />
                  <Point X="-2.535617478711" Y="-2.350069760545" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.347826877277" Y="-2.735097552077" />
                  <Point X="-1.836697263766" Y="-3.783068562203" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.190630296184" Y="1.25992072009" />
                  <Point X="-3.726369994566" Y="0.308046040191" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.439137516764" Y="-0.280867812473" />
                  <Point X="-2.397432264892" Y="-2.416680092179" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.31082151587" Y="-2.594258443621" />
                  <Point X="-1.72750958311" Y="-3.790225140199" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.077680444412" Y="1.245050548204" />
                  <Point X="-3.604782914818" Y="0.275466926605" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.364049152033" Y="-0.218110432032" />
                  <Point X="-1.617333074402" Y="-3.799409116147" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.96473059264" Y="1.230180376318" />
                  <Point X="-3.481669069591" Y="0.239757479891" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.308305430411" Y="-0.115690655511" />
                  <Point X="-1.477055708161" Y="-3.87030899593" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.123362583211" Y="-4.595487368756" />
                  <Point X="-1.045491136134" Y="-4.755147495848" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.851780740869" Y="1.215310204431" />
                  <Point X="-1.29235738903" Y="-4.032285326072" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.163258721901" Y="-4.296976819228" />
                  <Point X="-0.967228917038" Y="-4.698897481204" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.740844099065" Y="1.204567724677" />
                  <Point X="-0.929750888261" Y="-4.559027484474" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.194394794758" Y="2.351195801513" />
                  <Point X="-4.130165836631" Y="2.219506921926" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.644667879589" Y="1.224088595523" />
                  <Point X="-0.892272859483" Y="-4.419157487743" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.136812990305" Y="2.449846949745" />
                  <Point X="-3.961252764121" Y="2.089895143571" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.557071693535" Y="1.261201141855" />
                  <Point X="-0.854794830706" Y="-4.279287491013" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.079231187713" Y="2.548498101792" />
                  <Point X="-3.79233969161" Y="1.960283365217" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.485716840575" Y="1.331613355824" />
                  <Point X="-0.817316801929" Y="-4.139417494283" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.021649594477" Y="2.647149683084" />
                  <Point X="-3.623426755076" Y="1.830671865656" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.434584716552" Y="1.443488308617" />
                  <Point X="-0.779838773152" Y="-3.999547497553" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.959164457178" Y="2.735747509144" />
                  <Point X="-0.742360744372" Y="-3.859677500828" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.894196464908" Y="2.819254728121" />
                  <Point X="-0.704882715499" Y="-3.719807504293" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.829228472638" Y="2.902761947097" />
                  <Point X="-0.667404686627" Y="-3.579937507759" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.764260480368" Y="2.986269166073" />
                  <Point X="-0.626625958574" Y="-3.446834947433" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.665090018403" Y="2.999650930042" />
                  <Point X="-0.566534815103" Y="-3.35332870663" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.517962935773" Y="2.914707050432" />
                  <Point X="-0.504459641541" Y="-3.263890330344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.370835853142" Y="2.829763170822" />
                  <Point X="-0.442245728211" Y="-3.174736412736" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.225924895301" Y="2.749363020379" />
                  <Point X="-0.368369161874" Y="-3.109494477392" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.109399469329" Y="2.727161834975" />
                  <Point X="-0.280393329042" Y="-3.073160322307" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.004060878811" Y="2.727897061275" />
                  <Point X="-0.188592440113" Y="-3.044668694432" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.916008706149" Y="2.764074696514" />
                  <Point X="-0.096791512864" Y="-3.016177145124" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.842298541167" Y="2.829657805195" />
                  <Point X="-7.3750587E-05" Y="-2.997766601562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.776556130747" Y="2.911577231664" />
                  <Point X="0.115583792744" Y="-3.018188363855" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.05645430877" Y="3.702164884421" />
                  <Point X="-3.03342685679" Y="3.654951611166" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.751673174768" Y="3.077270954537" />
                  <Point X="0.240134774963" Y="-3.056844378064" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.979524314593" Y="3.761146364936" />
                  <Point X="0.368000530412" Y="-3.102296684561" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.775730828641" Y="-3.938267681349" />
                  <Point X="0.815695819465" Y="-4.020208055562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.902594320417" Y="3.82012784545" />
                  <Point X="0.776616531152" Y="-3.723372297501" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.825664326241" Y="3.879109325965" />
                  <Point X="0.737537597805" Y="-3.426537267228" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.748734358872" Y="3.938090861444" />
                  <Point X="0.738140579784" Y="-3.211062220389" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.671804424442" Y="3.997072464456" />
                  <Point X="0.770723429252" Y="-3.061155618717" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.590540265988" Y="4.047167591303" />
                  <Point X="0.814298876656" Y="-2.933787182821" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.507377911412" Y="4.093370839346" />
                  <Point X="0.882385016658" Y="-2.856673114118" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.424215556835" Y="4.13957408739" />
                  <Point X="0.957585822694" Y="-2.794146272516" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.341053202258" Y="4.185777335433" />
                  <Point X="1.032786614214" Y="-2.731619401154" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.257890847681" Y="4.231980583477" />
                  <Point X="1.115094396881" Y="-2.683664021039" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.130448930547" Y="4.187397274305" />
                  <Point X="1.211509963641" Y="-2.664633884848" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.966130641542" Y="4.067206198222" />
                  <Point X="1.312667036814" Y="-2.655325277472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.841882624265" Y="4.029171354198" />
                  <Point X="1.414043407081" Y="-2.646466295769" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.744208000752" Y="4.045620041492" />
                  <Point X="1.531081119497" Y="-2.669717824038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.656196686824" Y="4.081881449549" />
                  <Point X="1.681654868874" Y="-2.761728417719" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.574513224251" Y="4.13111687555" />
                  <Point X="1.85965080852" Y="-2.909962833455" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.512024878432" Y="4.21970812317" />
                  <Point X="2.14972918331" Y="-3.288000296538" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.469949263127" Y="4.35015167058" />
                  <Point X="2.439807558099" Y="-3.666037759621" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.4692644502" Y="4.565458939112" />
                  <Point X="2.000478238933" Y="-2.548567825708" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.645816949283" Y="-3.87170826266" />
                  <Point X="2.72542651297" Y="-4.034932056913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.376281995093" Y="4.591527997314" />
                  <Point X="2.036505312611" Y="-2.405722930165" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.283299539987" Y="4.617597055517" />
                  <Point X="2.091363523641" Y="-2.301487587875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.19031708488" Y="4.64366611372" />
                  <Point X="2.150086219126" Y="-2.205175612908" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.097334638675" Y="4.669735190172" />
                  <Point X="2.225566424448" Y="-2.143221624737" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.004352281514" Y="4.695804449194" />
                  <Point X="2.309674009189" Y="-2.098956385731" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.909877338861" Y="4.718813454445" />
                  <Point X="2.393781635794" Y="-2.054691232558" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.809887734002" Y="4.730515726592" />
                  <Point X="2.486190925582" Y="-2.0274470113" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.709898129143" Y="4.742217998739" />
                  <Point X="2.59537720117" Y="-2.034600708479" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.609908524284" Y="4.753920270886" />
                  <Point X="2.711283934531" Y="-2.055533386047" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.509918919425" Y="4.765622543033" />
                  <Point X="2.832348861491" Y="-2.087041927767" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.409929314566" Y="4.77732481518" />
                  <Point X="-0.307605909443" Y="4.567530744571" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.080919338091" Y="4.102754396493" />
                  <Point X="2.978342234623" Y="-2.169661358438" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.032506272872" Y="4.086908773711" />
                  <Point X="3.125469333267" Y="-2.254605270881" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.123664851297" Y="4.11671733328" />
                  <Point X="2.860059998947" Y="-1.493724150026" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.998801362787" Y="-1.778186101294" />
                  <Point X="3.272596431911" Y="-2.339549183324" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.197230985989" Y="4.182595747818" />
                  <Point X="2.874281955031" Y="-1.306172138113" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.180699636945" Y="-1.934421488469" />
                  <Point X="3.419723530555" Y="-2.424493095767" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.247323086521" Y="4.296603064771" />
                  <Point X="2.918879588473" Y="-1.180899494177" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.349612764615" Y="-2.064033379918" />
                  <Point X="3.566850629199" Y="-2.50943700821" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.284801109658" Y="4.436473073065" />
                  <Point X="2.984641401034" Y="-1.099019847994" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.518525736493" Y="-2.193644951944" />
                  <Point X="3.713977727843" Y="-2.594380920653" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.322279150354" Y="4.576343045357" />
                  <Point X="3.052063723974" Y="-1.020544752618" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.687438702064" Y="-2.32325651104" />
                  <Point X="3.861104830991" Y="-2.679324842332" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.359757191922" Y="4.716213015863" />
                  <Point X="3.131732210881" Y="-0.967178014271" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.856351667635" Y="-2.452868070136" />
                  <Point X="4.004226807378" Y="-2.756057037224" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.434792814233" Y="4.779078534291" />
                  <Point X="3.224614369619" Y="-0.940903318039" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.025264633206" Y="-2.582479629233" />
                  <Point X="4.065519916642" Y="-2.665015191505" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.546179495911" Y="4.767413336054" />
                  <Point X="3.320180514537" Y="-0.920131608982" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.657566177588" Y="4.755748137816" />
                  <Point X="3.420150537525" Y="-0.908389188048" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.768952842799" Y="4.744082973341" />
                  <Point X="3.532409764725" Y="-0.921843369724" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.884304372379" Y="4.724288632218" />
                  <Point X="2.013384443779" Y="2.409331424375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.18634612001" Y="2.054707435153" />
                  <Point X="3.645359600991" Y="-0.936713509819" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.004109005075" Y="4.695364076668" />
                  <Point X="2.047017284088" Y="2.557085225794" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.325228629518" Y="1.986667435487" />
                  <Point X="3.365069250661" Y="-0.145321784672" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.447787074215" Y="-0.314918456071" />
                  <Point X="3.758309437257" Y="-0.951583649913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.123913637772" Y="4.666439521119" />
                  <Point X="2.101713407959" Y="2.661652896009" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.436045559253" Y="1.976170401847" />
                  <Point X="3.363505501584" Y="0.074595719175" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.620330033439" Y="-0.4519726051" />
                  <Point X="3.871259273523" Y="-0.966453790008" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.243718270468" Y="4.637514965569" />
                  <Point X="2.15900875728" Y="2.760891364297" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.533980645993" Y="1.992085060385" />
                  <Point X="3.408153036076" Y="0.199766050796" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.74906419244" Y="-0.499205402734" />
                  <Point X="3.984209109789" Y="-0.981323930103" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.363522859018" Y="4.608590500534" />
                  <Point X="2.216304106602" Y="2.860129832586" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.62492197932" Y="2.022339038415" />
                  <Point X="3.472710041922" Y="0.284115916816" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.870651305702" Y="-0.531784585036" />
                  <Point X="4.097158946055" Y="-0.996194070197" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.485464065979" Y="4.575285318562" />
                  <Point X="2.273599455923" Y="2.959368300874" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.707730952856" Y="2.069266824963" />
                  <Point X="3.00661521382" Y="1.45646327652" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.222599177462" Y="1.013630526147" />
                  <Point X="3.547564545604" Y="0.347352783464" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.992238418965" Y="-0.564363767338" />
                  <Point X="4.21010878232" Y="-1.011064210292" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.613878332186" Y="4.528708398351" />
                  <Point X="2.330894805245" Y="3.058606769162" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.790204278391" Y="2.116882791898" />
                  <Point X="3.048668137724" Y="1.586953348197" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.35245569198" Y="0.964096558683" />
                  <Point X="3.630016908318" Y="0.395011730552" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.113825516785" Y="-0.596942917977" />
                  <Point X="4.323058618586" Y="-1.025934350386" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.742292598393" Y="4.482131478141" />
                  <Point X="2.388190154566" Y="3.157845237451" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.872677603925" Y="2.164498758833" />
                  <Point X="3.110812945535" Y="1.676248953114" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.464402952378" Y="0.951282003741" />
                  <Point X="3.719298140563" Y="0.428669420205" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.235412610263" Y="-0.629522059716" />
                  <Point X="4.436008454852" Y="-1.040804490481" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.871532101846" Y="4.433862570833" />
                  <Point X="2.445485503888" Y="3.257083705739" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.95515092946" Y="2.212114725768" />
                  <Point X="3.180666420039" Y="1.7497394491" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.565212479617" Y="0.961303185883" />
                  <Point X="3.812778602085" Y="0.453717413941" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.356999703742" Y="-0.662101201455" />
                  <Point X="4.548958291118" Y="-1.055674630576" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.008462522791" Y="4.369824945849" />
                  <Point X="2.50278085321" Y="3.356322174028" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.037624249963" Y="2.25973070302" />
                  <Point X="3.257579018404" Y="1.808756596313" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.664532213441" Y="0.974378897186" />
                  <Point X="3.906259063607" Y="0.478765407678" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.478586797221" Y="-0.694680343194" />
                  <Point X="4.661908092638" Y="-1.070544699431" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.145392943735" Y="4.305787320864" />
                  <Point X="2.560076202531" Y="3.455560642316" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.120097559722" Y="2.307346702302" />
                  <Point X="3.334491616769" Y="1.867773743525" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.763851947265" Y="0.987454608488" />
                  <Point X="3.999739525128" Y="0.503813401414" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.6001738907" Y="-0.727259484934" />
                  <Point X="4.74183747518" Y="-1.017712876406" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.284820240641" Y="4.236630341503" />
                  <Point X="2.617371551853" Y="3.554799110605" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.20257086948" Y="2.354962701583" />
                  <Point X="3.411404215134" Y="1.926790890738" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.863171681358" Y="1.00053031924" />
                  <Point X="4.09321998665" Y="0.528861395151" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.721760984179" Y="-0.759838626673" />
                  <Point X="4.77192136606" Y="-0.862682650339" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.432473711322" Y="4.150607206451" />
                  <Point X="2.674666901174" Y="3.654037578893" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.285044179238" Y="2.402578700865" />
                  <Point X="3.4883168135" Y="1.985808037951" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.962491419581" Y="1.013606021524" />
                  <Point X="4.186700448172" Y="0.553909388887" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.580127108597" Y="4.064584221902" />
                  <Point X="2.731962254258" Y="3.753276039466" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.367517488997" Y="2.450194700147" />
                  <Point X="3.565229411865" Y="2.044825185164" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.061811157804" Y="1.026681723808" />
                  <Point X="4.280180909693" Y="0.578957382623" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.73722065259" Y="3.959206068273" />
                  <Point X="2.789257613516" Y="3.852514487383" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.449990798755" Y="2.497810699429" />
                  <Point X="3.64214201023" Y="2.103842332377" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.161130896026" Y="1.039757426092" />
                  <Point X="4.373661371215" Y="0.60400537636" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.532464108513" Y="2.54542669871" />
                  <Point X="3.719054608596" Y="2.162859479589" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.260450634249" Y="1.052833128376" />
                  <Point X="4.467141832737" Y="0.629053370096" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.614937418271" Y="2.593042697992" />
                  <Point X="3.795967206961" Y="2.221876626802" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.359770372472" Y="1.06590883066" />
                  <Point X="4.560622294259" Y="0.654101363833" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.69741072803" Y="2.640658697274" />
                  <Point X="3.872879805326" Y="2.280893774015" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.459090110695" Y="1.078984532944" />
                  <Point X="4.65410275578" Y="0.679149357569" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.779884037788" Y="2.688274696556" />
                  <Point X="3.949792403691" Y="2.339910921228" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.558409848918" Y="1.092060235228" />
                  <Point X="4.747583179619" Y="0.704197428567" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.862357347546" Y="2.735890695838" />
                  <Point X="4.026704996531" Y="2.398928079769" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.657729587141" Y="1.105135937512" />
                  <Point X="4.753548609776" Y="0.908677827306" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.958011193251" Y="2.756482591634" />
                  <Point X="4.103617502691" Y="2.457945416031" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="0.001625976562" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978485351562" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.77138671875" Y="-4.656159179688" />
                  <Point X="0.471539031982" Y="-3.537112304688" />
                  <Point X="0.464318664551" Y="-3.521544189453" />
                  <Point X="0.341763427734" Y="-3.344965087891" />
                  <Point X="0.300591033936" Y="-3.285643798828" />
                  <Point X="0.274335754395" Y="-3.266399902344" />
                  <Point X="0.084688407898" Y="-3.207540283203" />
                  <Point X="0.020976625443" Y="-3.187766601562" />
                  <Point X="-0.008664908409" Y="-3.187766601562" />
                  <Point X="-0.198312103271" Y="-3.246625976562" />
                  <Point X="-0.262023895264" Y="-3.266399902344" />
                  <Point X="-0.288279144287" Y="-3.285643798828" />
                  <Point X="-0.410834533691" Y="-3.46222265625" />
                  <Point X="-0.45200680542" Y="-3.521544189453" />
                  <Point X="-0.459227142334" Y="-3.537112304688" />
                  <Point X="-0.559960693359" Y="-3.913054931641" />
                  <Point X="-0.84774395752" Y="-4.987077148438" />
                  <Point X="-1.026345214844" Y="-4.952409667969" />
                  <Point X="-1.10024609375" Y="-4.938065429688" />
                  <Point X="-1.320125732422" Y="-4.8814921875" />
                  <Point X="-1.351589599609" Y="-4.873396972656" />
                  <Point X="-1.345244873047" Y="-4.825204101562" />
                  <Point X="-1.309150146484" Y="-4.551038574219" />
                  <Point X="-1.309683227539" Y="-4.5347578125" />
                  <Point X="-1.354989990234" Y="-4.306985839844" />
                  <Point X="-1.370210693359" Y="-4.230466308594" />
                  <Point X="-1.386282714844" Y="-4.20262890625" />
                  <Point X="-1.560885742188" Y="-4.049505371094" />
                  <Point X="-1.619543457031" Y="-3.998063964844" />
                  <Point X="-1.649240844727" Y="-3.985762939453" />
                  <Point X="-1.880978149414" Y="-3.970573974609" />
                  <Point X="-1.958829956055" Y="-3.965471435547" />
                  <Point X="-1.989878540039" Y="-3.973791015625" />
                  <Point X="-2.182974365234" Y="-4.102813964844" />
                  <Point X="-2.247844970703" Y="-4.146159179688" />
                  <Point X="-2.259734130859" Y="-4.157294433594" />
                  <Point X="-2.316288085938" Y="-4.230997070312" />
                  <Point X="-2.457095214844" Y="-4.414500488281" />
                  <Point X="-2.747513427734" Y="-4.234680664062" />
                  <Point X="-2.855831054688" Y="-4.16761328125" />
                  <Point X="-3.160302246094" Y="-3.933180419922" />
                  <Point X="-3.228581054688" Y="-3.880608154297" />
                  <Point X="-3.063266845703" Y="-3.594275634766" />
                  <Point X="-2.506033691406" Y="-2.629119628906" />
                  <Point X="-2.499762695312" Y="-2.597592529297" />
                  <Point X="-2.513980712891" Y="-2.568763183594" />
                  <Point X="-2.531329345703" Y="-2.551414550781" />
                  <Point X="-2.560157958984" Y="-2.537198730469" />
                  <Point X="-2.59168359375" Y="-2.543469726562" />
                  <Point X="-2.915926757812" Y="-2.730671630859" />
                  <Point X="-3.842959472656" Y="-3.265894042969" />
                  <Point X="-4.076124267578" Y="-2.959563476562" />
                  <Point X="-4.161703613281" Y="-2.847129394531" />
                  <Point X="-4.379983886719" Y="-2.481106933594" />
                  <Point X="-4.431020019531" Y="-2.395527099609" />
                  <Point X="-4.138764160156" Y="-2.171271240234" />
                  <Point X="-3.163786865234" Y="-1.423144897461" />
                  <Point X="-3.145822021484" Y="-1.396013916016" />
                  <Point X="-3.140054931641" Y="-1.373746948242" />
                  <Point X="-3.138117431641" Y="-1.366266479492" />
                  <Point X="-3.140326171875" Y="-1.334596069336" />
                  <Point X="-3.161158447266" Y="-1.310639526367" />
                  <Point X="-3.180981689453" Y="-1.29897253418" />
                  <Point X="-3.187641113281" Y="-1.295052978516" />
                  <Point X="-3.219529296875" Y="-1.288571044922" />
                  <Point X="-3.628855712891" Y="-1.342460083008" />
                  <Point X="-4.803284179688" Y="-1.497076416016" />
                  <Point X="-4.893922363281" Y="-1.142229370117" />
                  <Point X="-4.927392578125" Y="-1.011195678711" />
                  <Point X="-4.98514453125" Y="-0.60739831543" />
                  <Point X="-4.998396484375" Y="-0.514742126465" />
                  <Point X="-4.668109863281" Y="-0.426242095947" />
                  <Point X="-3.557463867188" Y="-0.128645431519" />
                  <Point X="-3.541895751953" Y="-0.121425079346" />
                  <Point X="-3.521121826172" Y="-0.107006736755" />
                  <Point X="-3.514142822266" Y="-0.102163047791" />
                  <Point X="-3.494898681641" Y="-0.075906860352" />
                  <Point X="-3.487973876953" Y="-0.053595355988" />
                  <Point X="-3.485647705078" Y="-0.046099822998" />
                  <Point X="-3.485648193359" Y="-0.016458747864" />
                  <Point X="-3.492572753906" Y="0.005852606773" />
                  <Point X="-3.494899169922" Y="0.013348137856" />
                  <Point X="-3.514142822266" Y="0.039602954865" />
                  <Point X="-3.534916748047" Y="0.054021144867" />
                  <Point X="-3.557463867188" Y="0.066085334778" />
                  <Point X="-3.930584228516" Y="0.166062561035" />
                  <Point X="-4.998186523438" Y="0.452125793457" />
                  <Point X="-4.939215820313" Y="0.850644348145" />
                  <Point X="-4.91764453125" Y="0.996421936035" />
                  <Point X="-4.801394042969" Y="1.425422607422" />
                  <Point X="-4.773516601562" Y="1.528298706055" />
                  <Point X="-4.549404296875" Y="1.498793823242" />
                  <Point X="-3.753266357422" Y="1.393980102539" />
                  <Point X="-3.731704589844" Y="1.395866699219" />
                  <Point X="-3.685725341797" Y="1.410363891602" />
                  <Point X="-3.670278564453" Y="1.41523425293" />
                  <Point X="-3.651534423828" Y="1.426056274414" />
                  <Point X="-3.639119873047" Y="1.443786132813" />
                  <Point X="-3.620670410156" Y="1.488326904297" />
                  <Point X="-3.614472412109" Y="1.503290283203" />
                  <Point X="-3.610714111328" Y="1.524605224609" />
                  <Point X="-3.616315917969" Y="1.545511474609" />
                  <Point X="-3.638577148438" Y="1.588275024414" />
                  <Point X="-3.646055664062" Y="1.602641235352" />
                  <Point X="-3.659968261719" Y="1.619221679688" />
                  <Point X="-3.873991210938" Y="1.783447143555" />
                  <Point X="-4.476105957031" Y="2.245466308594" />
                  <Point X="-4.243831054688" Y="2.643408203125" />
                  <Point X="-4.160015136719" Y="2.787005615234" />
                  <Point X="-3.852067382813" Y="3.182829101562" />
                  <Point X="-3.774671142578" Y="3.282310791016" />
                  <Point X="-3.648122802734" Y="3.209248046875" />
                  <Point X="-3.159156982422" Y="2.926943359375" />
                  <Point X="-3.138514648438" Y="2.920434814453" />
                  <Point X="-3.074478271484" Y="2.914832275391" />
                  <Point X="-3.052965332031" Y="2.912950195312" />
                  <Point X="-3.031506591797" Y="2.915775390625" />
                  <Point X="-3.013252441406" Y="2.927404785156" />
                  <Point X="-2.967798828125" Y="2.972858154297" />
                  <Point X="-2.952528808594" Y="2.988128173828" />
                  <Point X="-2.940899414062" Y="3.006382324219" />
                  <Point X="-2.93807421875" Y="3.027841064453" />
                  <Point X="-2.943676757813" Y="3.091877685547" />
                  <Point X="-2.945558837891" Y="3.113390625" />
                  <Point X="-2.952067382812" Y="3.134032958984" />
                  <Point X="-3.046907470703" Y="3.298300537109" />
                  <Point X="-3.307278564453" Y="3.749276611328" />
                  <Point X="-2.898856933594" Y="4.062409423828" />
                  <Point X="-2.752876220703" Y="4.174331542969" />
                  <Point X="-2.267868896484" Y="4.443791015625" />
                  <Point X="-2.141548583984" Y="4.513971679688" />
                  <Point X="-2.117514160156" Y="4.482649414062" />
                  <Point X="-1.967826660156" Y="4.287573242188" />
                  <Point X="-1.951247192383" Y="4.273661132813" />
                  <Point X="-1.879975097656" Y="4.236559082031" />
                  <Point X="-1.85603125" Y="4.224094238281" />
                  <Point X="-1.835124511719" Y="4.2184921875" />
                  <Point X="-1.813808959961" Y="4.222250488281" />
                  <Point X="-1.73957409668" Y="4.253" />
                  <Point X="-1.714635009766" Y="4.263330078125" />
                  <Point X="-1.696905395508" Y="4.275744140625" />
                  <Point X="-1.686083496094" Y="4.29448828125" />
                  <Point X="-1.661921386719" Y="4.371120605469" />
                  <Point X="-1.653804077148" Y="4.396864746094" />
                  <Point X="-1.651917602539" Y="4.418426757812" />
                  <Point X="-1.662699951172" Y="4.500326171875" />
                  <Point X="-1.689137573242" Y="4.701141113281" />
                  <Point X="-1.157075683594" Y="4.8503125" />
                  <Point X="-0.968094177246" Y="4.903296386719" />
                  <Point X="-0.380117553711" Y="4.972110351562" />
                  <Point X="-0.224199615479" Y="4.990358398438" />
                  <Point X="-0.186789901733" Y="4.850743164062" />
                  <Point X="-0.042140380859" Y="4.310903320312" />
                  <Point X="-0.024282115936" Y="4.284176757813" />
                  <Point X="0.006155934334" Y="4.273844238281" />
                  <Point X="0.036594036102" Y="4.284176757813" />
                  <Point X="0.054452251434" Y="4.310903320312" />
                  <Point X="0.103047172546" Y="4.492262207031" />
                  <Point X="0.236648406982" Y="4.990868652344" />
                  <Point X="0.6951953125" Y="4.942846679688" />
                  <Point X="0.860205871582" Y="4.925565429688" />
                  <Point X="1.346653686523" Y="4.808122070312" />
                  <Point X="1.508455932617" Y="4.769058105469" />
                  <Point X="1.825687744141" Y="4.653995605469" />
                  <Point X="1.931037963867" Y="4.615784179688" />
                  <Point X="2.237199462891" Y="4.472603027344" />
                  <Point X="2.338695800781" Y="4.425136230469" />
                  <Point X="2.634466064453" Y="4.252819824219" />
                  <Point X="2.732523681641" Y="4.19569140625" />
                  <Point X="3.011472412109" Y="3.997318603516" />
                  <Point X="3.068740234375" Y="3.956592773438" />
                  <Point X="2.874809082031" Y="3.620693847656" />
                  <Point X="2.229853515625" Y="2.503597900391" />
                  <Point X="2.224852050781" Y="2.491514648438" />
                  <Point X="2.20878125" Y="2.431417724609" />
                  <Point X="2.203382324219" Y="2.411228271484" />
                  <Point X="2.202044677734" Y="2.392326171875" />
                  <Point X="2.208311035156" Y="2.340359619141" />
                  <Point X="2.210416015625" Y="2.322901367188" />
                  <Point X="2.218682128906" Y="2.3008125" />
                  <Point X="2.250837402344" Y="2.253424072266" />
                  <Point X="2.274940429688" Y="2.224203369141" />
                  <Point X="2.322328857422" Y="2.192048095703" />
                  <Point X="2.338249023438" Y="2.181245849609" />
                  <Point X="2.360336669922" Y="2.172980224609" />
                  <Point X="2.412303466797" Y="2.166713867188" />
                  <Point X="2.448664306641" Y="2.165946289062" />
                  <Point X="2.508761230469" Y="2.182017089844" />
                  <Point X="2.528950683594" Y="2.187416015625" />
                  <Point X="2.541033935547" Y="2.192417480469" />
                  <Point X="2.916321289062" Y="2.409089599609" />
                  <Point X="3.994248046875" Y="3.031430908203" />
                  <Point X="4.144427246094" Y="2.822715820312" />
                  <Point X="4.202591796875" Y="2.741880126953" />
                  <Point X="4.3581015625" Y="2.484899169922" />
                  <Point X="4.387513183594" Y="2.436296142578" />
                  <Point X="4.137760742188" Y="2.244654296875" />
                  <Point X="3.288616210938" Y="1.593082763672" />
                  <Point X="3.27937109375" Y="1.583833007812" />
                  <Point X="3.236119140625" Y="1.527407714844" />
                  <Point X="3.221588867188" Y="1.508451660156" />
                  <Point X="3.213119628906" Y="1.4915" />
                  <Point X="3.197008300781" Y="1.433889648438" />
                  <Point X="3.191595703125" Y="1.414535522461" />
                  <Point X="3.190779541016" Y="1.390965576172" />
                  <Point X="3.204005371094" Y="1.326866577148" />
                  <Point X="3.208448486328" Y="1.305332763672" />
                  <Point X="3.215646484375" Y="1.287955078125" />
                  <Point X="3.251619140625" Y="1.233278320312" />
                  <Point X="3.263704101562" Y="1.214909667969" />
                  <Point X="3.280947998047" Y="1.198819824219" />
                  <Point X="3.333077148438" Y="1.169475463867" />
                  <Point X="3.350590087891" Y="1.159617431641" />
                  <Point X="3.368565673828" Y="1.153619628906" />
                  <Point X="3.439047851562" Y="1.144304321289" />
                  <Point X="3.462726318359" Y="1.141175048828" />
                  <Point X="3.475803710938" Y="1.14117175293" />
                  <Point X="3.832301513672" Y="1.188105712891" />
                  <Point X="4.848975097656" Y="1.32195324707" />
                  <Point X="4.91420703125" Y="1.054000366211" />
                  <Point X="4.939188476562" Y="0.951385559082" />
                  <Point X="4.988193847656" Y="0.63663067627" />
                  <Point X="4.997858398438" Y="0.574556152344" />
                  <Point X="4.714305664063" Y="0.498578308105" />
                  <Point X="3.741167724609" Y="0.237826919556" />
                  <Point X="3.729087158203" Y="0.232819412231" />
                  <Point X="3.659840576172" Y="0.192793655396" />
                  <Point X="3.636577148438" Y="0.17934703064" />
                  <Point X="3.622264892578" Y="0.166926574707" />
                  <Point X="3.580717041016" Y="0.113984779358" />
                  <Point X="3.566759033203" Y="0.096198890686" />
                  <Point X="3.556985351562" Y="0.074735427856" />
                  <Point X="3.543135986328" Y="0.002419494152" />
                  <Point X="3.538483154297" Y="-0.021875152588" />
                  <Point X="3.538483154297" Y="-0.040684940338" />
                  <Point X="3.552332519531" Y="-0.113001029968" />
                  <Point X="3.556985351562" Y="-0.137295516968" />
                  <Point X="3.566759033203" Y="-0.158758987427" />
                  <Point X="3.608306884766" Y="-0.211700927734" />
                  <Point X="3.622264892578" Y="-0.229486679077" />
                  <Point X="3.636576904297" Y="-0.241906967163" />
                  <Point X="3.705823486328" Y="-0.281932861328" />
                  <Point X="3.729086914062" Y="-0.295379486084" />
                  <Point X="3.741167724609" Y="-0.300387023926" />
                  <Point X="4.068092529297" Y="-0.387986236572" />
                  <Point X="4.998067871094" Y="-0.637172363281" />
                  <Point X="4.962380371094" Y="-0.873882202148" />
                  <Point X="4.948431640625" Y="-0.966399414063" />
                  <Point X="4.885647460938" Y="-1.241528686523" />
                  <Point X="4.874545898438" Y="-1.290178344727" />
                  <Point X="4.540548339844" Y="-1.246206787109" />
                  <Point X="3.411982177734" Y="-1.097628295898" />
                  <Point X="3.394836181641" Y="-1.098341186523" />
                  <Point X="3.2589296875" Y="-1.127881103516" />
                  <Point X="3.213271972656" Y="-1.137804931641" />
                  <Point X="3.1854453125" Y="-1.154697265625" />
                  <Point X="3.103298339844" Y="-1.253494506836" />
                  <Point X="3.075701171875" Y="-1.286685424805" />
                  <Point X="3.064357910156" Y="-1.314070556641" />
                  <Point X="3.052584228516" Y="-1.442017456055" />
                  <Point X="3.04862890625" Y="-1.485000854492" />
                  <Point X="3.056360351562" Y="-1.516621826172" />
                  <Point X="3.131572998047" Y="-1.633610229492" />
                  <Point X="3.156840576172" Y="-1.672912353516" />
                  <Point X="3.168460693359" Y="-1.685540649414" />
                  <Point X="3.471848876953" Y="-1.918338745117" />
                  <Point X="4.33907421875" Y="-2.583783935547" />
                  <Point X="4.243450195312" Y="-2.738517578125" />
                  <Point X="4.204129394531" Y="-2.802145019531" />
                  <Point X="4.074290771484" Y="-2.986627929688" />
                  <Point X="4.056688476562" Y="-3.011638427734" />
                  <Point X="3.758594970703" Y="-2.839533935547" />
                  <Point X="2.753454589844" Y="-2.259215576172" />
                  <Point X="2.737340820313" Y="-2.253312744141" />
                  <Point X="2.575590332031" Y="-2.224100830078" />
                  <Point X="2.521250488281" Y="-2.214287109375" />
                  <Point X="2.489077636719" Y="-2.219244873047" />
                  <Point X="2.354702636719" Y="-2.289965332031" />
                  <Point X="2.309559570312" Y="-2.313723876953" />
                  <Point X="2.288599853516" Y="-2.334683837891" />
                  <Point X="2.217879150391" Y="-2.469058837891" />
                  <Point X="2.194120849609" Y="-2.514201904297" />
                  <Point X="2.189163085938" Y="-2.546374511719" />
                  <Point X="2.218375" Y="-2.708125" />
                  <Point X="2.228188720703" Y="-2.76246484375" />
                  <Point X="2.234091552734" Y="-2.778578613281" />
                  <Point X="2.429049072266" Y="-3.116254638672" />
                  <Point X="2.986673583984" Y="-4.082088623047" />
                  <Point X="2.881955322266" Y="-4.156886230469" />
                  <Point X="2.835296875" Y="-4.190213378906" />
                  <Point X="2.690126953125" Y="-4.284179199219" />
                  <Point X="2.679775878906" Y="-4.290879394531" />
                  <Point X="2.450192382812" Y="-3.991680419922" />
                  <Point X="1.683177612305" Y="-2.992087402344" />
                  <Point X="1.670549316406" Y="-2.980467529297" />
                  <Point X="1.51101965332" Y="-2.877904541016" />
                  <Point X="1.45742590332" Y="-2.843448730469" />
                  <Point X="1.4258046875" Y="-2.835717041016" />
                  <Point X="1.25133190918" Y="-2.851772216797" />
                  <Point X="1.192717773438" Y="-2.857166015625" />
                  <Point X="1.165332885742" Y="-2.868509277344" />
                  <Point X="1.030609375" Y="-2.98052734375" />
                  <Point X="0.985349060059" Y="-3.018159667969" />
                  <Point X="0.968456726074" Y="-3.045986083984" />
                  <Point X="0.928175109863" Y="-3.231313476562" />
                  <Point X="0.91464251709" Y="-3.293573974609" />
                  <Point X="0.91392956543" Y="-3.310720214844" />
                  <Point X="0.969178833008" Y="-3.730381103516" />
                  <Point X="1.127642211914" Y="-4.934028320312" />
                  <Point X="1.038483886719" Y="-4.953571777344" />
                  <Point X="0.994344787598" Y="-4.963247070312" />
                  <Point X="0.860237670898" Y="-4.987609863281" />
                  <Point X="0.860200439453" Y="-4.987616699219" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#192" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.144649642936" Y="4.894964317856" Z="1.9" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.9" />
                  <Point X="-0.39208560841" Y="5.053051592006" Z="1.9" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.9" />
                  <Point X="-1.176729493102" Y="4.929736200675" Z="1.9" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.9" />
                  <Point X="-1.718428127287" Y="4.525079843394" Z="1.9" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.9" />
                  <Point X="-1.715763108196" Y="4.417436106722" Z="1.9" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.9" />
                  <Point X="-1.764864258123" Y="4.330473894765" Z="1.9" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.9" />
                  <Point X="-1.86304291256" Y="4.312189053701" Z="1.9" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.9" />
                  <Point X="-2.084002446144" Y="4.544367625996" Z="1.9" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.9" />
                  <Point X="-2.298307860506" Y="4.518778454483" Z="1.9" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.9" />
                  <Point X="-2.935435094887" Y="4.133369210309" Z="1.9" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.9" />
                  <Point X="-3.09636466465" Y="3.304580560783" Z="1.9" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.9" />
                  <Point X="-2.99964252701" Y="3.118799942839" Z="1.9" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.9" />
                  <Point X="-3.009310070231" Y="3.039493497655" Z="1.9" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.9" />
                  <Point X="-3.076276522768" Y="2.995922172163" Z="1.9" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.9" />
                  <Point X="-3.629278787514" Y="3.283829284096" Z="1.9" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.9" />
                  <Point X="-3.897686838355" Y="3.244811445277" Z="1.9" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.9" />
                  <Point X="-4.293169386962" Y="2.699893157704" Z="1.9" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.9" />
                  <Point X="-3.910585077811" Y="1.775059564256" Z="1.9" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.9" />
                  <Point X="-3.689083653249" Y="1.596467791918" Z="1.9" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.9" />
                  <Point X="-3.67302049668" Y="1.538740931764" Z="1.9" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.9" />
                  <Point X="-3.706916652312" Y="1.489329775381" Z="1.9" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.9" />
                  <Point X="-4.549034050578" Y="1.579646119964" Z="1.9" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.9" />
                  <Point X="-4.855809191265" Y="1.469780040178" Z="1.9" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.9" />
                  <Point X="-4.994833330217" Y="0.88924335517" Z="1.9" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.9" />
                  <Point X="-3.949681215054" Y="0.149046364732" Z="1.9" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.9" />
                  <Point X="-3.569581761736" Y="0.044225225595" Z="1.9" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.9" />
                  <Point X="-3.546481494795" Y="0.022311442006" Z="1.9" />
                  <Point X="-3.539556741714" Y="0" Z="1.9" />
                  <Point X="-3.54188310634" Y="-0.007495509055" Z="1.9" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.9" />
                  <Point X="-3.555786833388" Y="-0.034650757539" Z="1.9" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.9" />
                  <Point X="-4.687206573719" Y="-0.346665692024" Z="1.9" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.9" />
                  <Point X="-5.040796669856" Y="-0.583197404128" Z="1.9" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.9" />
                  <Point X="-4.94852276388" Y="-1.123323569468" Z="1.9" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.9" />
                  <Point X="-3.628485327756" Y="-1.360752145756" Z="1.9" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.9" />
                  <Point X="-3.212499352862" Y="-1.310782820458" Z="1.9" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.9" />
                  <Point X="-3.194613743864" Y="-1.32993032887" Z="1.9" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.9" />
                  <Point X="-4.175357910045" Y="-2.100323386223" Z="1.9" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.9" />
                  <Point X="-4.429083099941" Y="-2.475436402093" Z="1.9" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.9" />
                  <Point X="-4.122066141794" Y="-2.958566518524" Z="1.9" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.9" />
                  <Point X="-2.897082818471" Y="-2.742692936069" Z="1.9" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.9" />
                  <Point X="-2.568476890356" Y="-2.559853578775" Z="1.9" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.9" />
                  <Point X="-3.1127240771" Y="-3.537995564021" Z="1.9" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.9" />
                  <Point X="-3.196962134688" Y="-3.941517663618" Z="1.9" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.9" />
                  <Point X="-2.779990878498" Y="-4.24591149656" Z="1.9" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.9" />
                  <Point X="-2.282776403379" Y="-4.230154926035" Z="1.9" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.9" />
                  <Point X="-2.161351885092" Y="-4.113107105281" Z="1.9" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.9" />
                  <Point X="-1.890404080408" Y="-3.989187057482" Z="1.9" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.9" />
                  <Point X="-1.600009089756" Y="-4.055817479026" Z="1.9" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.9" />
                  <Point X="-1.410185234075" Y="-4.285460287449" Z="1.9" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.9" />
                  <Point X="-1.400973110234" Y="-4.787397629058" Z="1.9" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.9" />
                  <Point X="-1.338740518269" Y="-4.898635054087" Z="1.9" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.9" />
                  <Point X="-1.042028241973" Y="-4.970213853008" Z="1.9" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.9" />
                  <Point X="-0.517820355852" Y="-3.894715928631" Z="1.9" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.9" />
                  <Point X="-0.37591442196" Y="-3.459451560382" Z="1.9" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.9" />
                  <Point X="-0.189647253562" Y="-3.263098877307" Z="1.9" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.9" />
                  <Point X="0.063711825799" Y="-3.224013167981" Z="1.9" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.9" />
                  <Point X="0.294531437472" Y="-3.342194195386" Z="1.9" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.9" />
                  <Point X="0.716934504549" Y="-4.637820101185" Z="1.9" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.9" />
                  <Point X="0.863018430908" Y="-5.005524447317" Z="1.9" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.9" />
                  <Point X="1.043033785648" Y="-4.971132223944" Z="1.9" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.9" />
                  <Point X="1.012595219865" Y="-3.692574776198" Z="1.9" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.9" />
                  <Point X="0.970878367605" Y="-3.210653193475" Z="1.9" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.9" />
                  <Point X="1.056420254633" Y="-2.987693609357" Z="1.9" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.9" />
                  <Point X="1.249757713572" Y="-2.870281729696" Z="1.9" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.9" />
                  <Point X="1.477824286755" Y="-2.888682470279" Z="1.9" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.9" />
                  <Point X="2.404368723598" Y="-3.990838249063" Z="1.9" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.9" />
                  <Point X="2.711140087136" Y="-4.294873397231" Z="1.9" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.9" />
                  <Point X="2.90486123105" Y="-4.166293252169" Z="1.9" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.9" />
                  <Point X="2.466194310045" Y="-3.059974626358" Z="1.9" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.9" />
                  <Point X="2.261423141552" Y="-2.667958684708" Z="1.9" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.9" />
                  <Point X="2.255969625825" Y="-2.461065064434" Z="1.9" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.9" />
                  <Point X="2.371833313939" Y="-2.302931684142" Z="1.9" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.9" />
                  <Point X="2.560548118804" Y="-2.242024867939" Z="1.9" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.9" />
                  <Point X="3.727438970749" Y="-2.85155519023" Z="1.9" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.9" />
                  <Point X="4.109023203717" Y="-2.984125058715" Z="1.9" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.9" />
                  <Point X="4.279828029799" Y="-2.7335225226" Z="1.9" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.9" />
                  <Point X="3.496130940945" Y="-1.847390949735" Z="1.9" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.9" />
                  <Point X="3.167475190423" Y="-1.575290884728" Z="1.9" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.9" />
                  <Point X="3.096217841959" Y="-1.41531892142" Z="1.9" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.9" />
                  <Point X="3.135588633344" Y="-1.254181395859" Z="1.9" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.9" />
                  <Point X="3.263393237605" Y="-1.145460291072" Z="1.9" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.9" />
                  <Point X="4.527865963876" Y="-1.264498959513" Z="1.9" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.9" />
                  <Point X="4.928238639893" Y="-1.221372705728" Z="1.9" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.9" />
                  <Point X="5.005665653543" Y="-0.85005634132" Z="1.9" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.9" />
                  <Point X="4.074877436331" Y="-0.308409852032" Z="1.9" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.9" />
                  <Point X="3.724689353181" Y="-0.207364005627" Z="1.9" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.9" />
                  <Point X="3.641484706803" Y="-0.149552425249" Z="1.9" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.9" />
                  <Point X="3.595284054413" Y="-0.072316026558" Z="1.9" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.9" />
                  <Point X="3.586087396011" Y="0.024294504634" Z="1.9" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.9" />
                  <Point X="3.613894731597" Y="0.114396313326" Z="1.9" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.9" />
                  <Point X="3.678706061172" Y="0.180784838615" Z="1.9" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.9" />
                  <Point X="4.721090380975" Y="0.481562080737" Z="1.9" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.9" />
                  <Point X="5.031442782416" Y="0.67560269979" Z="1.9" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.9" />
                  <Point X="4.956632231125" Y="1.097107444214" Z="1.9" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.9" />
                  <Point X="3.819619256262" Y="1.268957928878" Z="1.9" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.9" />
                  <Point X="3.439442940941" Y="1.225153481828" Z="1.9" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.9" />
                  <Point X="3.351447833367" Y="1.244326760733" Z="1.9" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.9" />
                  <Point X="3.287233590809" Y="1.292039640836" Z="1.9" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.9" />
                  <Point X="3.246817808503" Y="1.368250355369" Z="1.9" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.9" />
                  <Point X="3.239004629274" Y="1.451703351212" Z="1.9" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.9" />
                  <Point X="3.269646381859" Y="1.528269749167" Z="1.9" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.9" />
                  <Point X="4.162042570142" Y="2.236266501699" Z="1.9" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.9" />
                  <Point X="4.394722799659" Y="2.542065339063" Z="1.9" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.9" />
                  <Point X="4.178856820936" Y="2.883198680372" Z="1.9" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.9" />
                  <Point X="2.885165284464" Y="2.483671016303" Z="1.9" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.9" />
                  <Point X="2.489688454252" Y="2.26159984165" Z="1.9" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.9" />
                  <Point X="2.412133488912" Y="2.247634306363" Z="1.9" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.9" />
                  <Point X="2.344246640759" Y="2.264702996876" Z="1.9" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.9" />
                  <Point X="2.286055603359" Y="2.312778219623" Z="1.9" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.9" />
                  <Point X="2.251795396396" Y="2.37762495492" Z="1.9" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.9" />
                  <Point X="2.250928101007" Y="2.44978112163" Z="1.9" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.9" />
                  <Point X="2.911954336556" Y="3.626973797491" Z="1.9" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.9" />
                  <Point X="3.034293524672" Y="4.069345622758" Z="1.9" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.9" />
                  <Point X="2.653479988138" Y="4.327302632986" Z="1.9" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.9" />
                  <Point X="2.252225225044" Y="4.54917442629" Z="1.9" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.9" />
                  <Point X="1.836580346373" Y="4.732279678552" Z="1.9" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.9" />
                  <Point X="1.352232193981" Y="4.888005684419" Z="1.9" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.9" />
                  <Point X="0.694247254683" Y="5.023854669775" Z="1.9" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.9" />
                  <Point X="0.048594918617" Y="4.536483188094" Z="1.9" />
                  <Point X="0" Y="4.355124473572" Z="1.9" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>